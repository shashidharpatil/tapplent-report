package com.tapplent.tenantresolver.tenant;

import java.sql.SQLException;
import java.util.Map;

public class TenantRegistry {
	private static TenantRegistry tenantRegistry = new TenantRegistry();
	
	Map<String, Tenant> tenantIdTenantMap;
	
	public static TenantRegistry getInstance(){
		return tenantRegistry;
	}

	public Map<String, Tenant> getTenantIdTenantMap() {
		return tenantIdTenantMap;
	}

	public void setTenantIdTenantMap(Map<String, Tenant> tenantIdTenantMap) {
		this.tenantIdTenantMap = tenantIdTenantMap;
	}
	
	public void setupTenantRegistry() throws SQLException{
		TenantRegistryService registryService = new TenantRegistryService();
		registryService.populateTenantDetails();
		
		this.tenantIdTenantMap = registryService.getTenantIdTenantMap();
	}
	
	public String getTenantIdFromURL(String tenantURL){
		return null;
	}
	
	public Tenant getTenant(String tenantID){
		return this.tenantIdTenantMap.get(tenantID)
;	}
}
