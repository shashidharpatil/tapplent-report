package com.tapplent.tenantresolver.jdbc;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.tapplent.tenantresolver.tenant.TenantContextHolder;

public class TenantRoutingDataSource extends AbstractRoutingDataSource{

	@Override
	protected DataSourceInstance determineCurrentLookupKey() {
		return TenantContextHolder.getCurrentDataSourceInstance(); 
	}

}
