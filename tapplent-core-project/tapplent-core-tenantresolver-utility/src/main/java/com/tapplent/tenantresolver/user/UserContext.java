package com.tapplent.tenantresolver.user;

public class UserContext {
	private String personId;
	private String personName;
	private String loggedInUserTimeZone;
	private String deviceType;
	public UserContext(String personId, String personName, String loggedInUserTimeZone, String deviceType) {
		this.personId = personId;
		this.personName = personName;
		this.loggedInUserTimeZone = loggedInUserTimeZone;
		this.deviceType = deviceType;
	}
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getLoggedInUserTimeZone() {
		return loggedInUserTimeZone;
	}
	public void setLoggedInUserTimeZone(String loggedInUserTimeZone) {
		this.loggedInUserTimeZone = loggedInUserTimeZone;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
}
