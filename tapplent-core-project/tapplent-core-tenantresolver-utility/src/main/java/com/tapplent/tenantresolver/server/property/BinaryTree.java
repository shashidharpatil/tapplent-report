package com.tapplent.tenantresolver.server.property;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;

public class BinaryTree {
		
	private static final Logger LOG = LogFactory.getLogger(BinaryTree.class);

    public Node root;
    
    public void addNode(Node parentNode, Node currentNode) {

		if (root == null) {
			root = currentNode;
		} else if(parentNode.left == null){
			parentNode.left = currentNode;
		} else if(parentNode.right == null){
			parentNode.right = currentNode;
		} else{
			LOG.debug("Parent Node already have two children. " + parentNode.actionControl);
		}
	}

	public Enum<ServerRuleCode> getNextStepRuleCode(Node currentNode, String currentActionControl) {
		if(currentNode.left.actionControl.equals(currentActionControl))
			return currentNode.left.serverRuleCode;
		else if(currentNode.right.actionControl.equals(currentActionControl))
			return currentNode.right.serverRuleCode;
		return null;
	}

	public boolean hasNextStepRuleCode(Node currentNode, Enum<ActionControlCode> currentActionControl) {
		if(currentNode.left.actionControl.equals(currentActionControl) || currentNode.right.actionControl.equals(currentActionControl))
			return true;
		return false;
	}
}
