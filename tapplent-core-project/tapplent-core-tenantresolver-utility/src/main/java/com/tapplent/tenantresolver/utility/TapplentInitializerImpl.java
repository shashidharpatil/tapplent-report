package com.tapplent.tenantresolver.utility;

import java.sql.SQLException;

import com.tapplent.tenantresolver.tenant.TenantRegistry;

public class TapplentInitializerImpl implements TapplentInitializer{
	
	public void init(){
//		setupDataSources();
//		setupTenantRegistry();
	}
	
	private void setupTenantRegistry() {
		try {
			TenantRegistry.getInstance().setupTenantRegistry();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void setupDataSources(){
		
	}
}
