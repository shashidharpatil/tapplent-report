package com.tapplent.tenantresolver.server.property;

import java.util.HashMap;
import java.util.Map;

public class ServerActionStep {
	
	private String versionId;
	private String ActionStepLookupPkId;
	private String ActionCodeFkId;
	private String ParentActionStepId;
	private String ActionStepId;
	private String ServerRuleCodeFkId;
	private Map<String, String> propertyCodeValueMap = new HashMap<>();
	
	
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getActionStepLookupPkId() {
		return ActionStepLookupPkId;
	}
	public void setActionStepLookupPkId(String actionStepLookupPkId) {
		ActionStepLookupPkId = actionStepLookupPkId;
	}
	public String getActionCodeFkId() {
		return ActionCodeFkId;
	}
	public void setActionCodeFkId(String actionCodeFkId) {
		ActionCodeFkId = actionCodeFkId;
	}
	public String getParentActionStepId() {
		return ParentActionStepId;
	}
	public void setParentActionStepId(String parentActionStepId) {
		ParentActionStepId = parentActionStepId;
	}
	public String getActionStepId() {
		return ActionStepId;
	}
	public void setActionStepId(String actionStepId) {
		ActionStepId = actionStepId;
	}
	public String getServerRuleCodeFkId() {
		return ServerRuleCodeFkId;
	}
	public void setServerRuleCodeFkId(String serverRuleCodeFkId) {
		ServerRuleCodeFkId = serverRuleCodeFkId;
	}
	public Map<String, String> getPropertyCodeValueMap() {
		return propertyCodeValueMap;
	}
	public void setPropertyCodeValueMap(Map<String, String> propertyCodeValueMap) {
		this.propertyCodeValueMap = propertyCodeValueMap;
	}
}
