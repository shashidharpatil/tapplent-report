package com.tapplent.tenantresolver.servlet;

import javax.servlet.ServletContextEvent;

import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.MDC;

import com.tapplent.tenantresolver.utility.TapplentInitializerImpl;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.tenantresolver.tenant.Tenant;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import com.tapplent.tenantresolver.tenant.TenantInfo;
import com.tapplent.tenantresolver.tenant.TenantRegistry;
import com.tapplent.tenantresolver.user.UserContext;
import com.tapplent.tenantresolver.utility.TapplentInitializer;

@WebListener
public class TapplentSetupHandler implements ServletContextListener, ServletRequestListener{

	private static Logger LOGGER;
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		//TapplentInitializer initializer = new TapplentInitializerImpl();
		//initializer.init();
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		String tenantId;
		String access_token;
		String personId;
		String personName;
		String loggedInUserTimeZone;
		String deviceType;
		String serverName;
		Logger logger = getLogger();
		MDC.put("RemoteAddress",sre.getServletRequest().getRemoteAddr() );
		HttpServletRequest request = (HttpServletRequest) sre.getServletRequest();
		serverName = request.getServerName();
		int startTenantIndex = request.getRequestURL().indexOf("/t/");
		int endTenantIdIndex = request.getRequestURL().indexOf("/u/");
		int startPersonIdIndex = request.getRequestURL().indexOf("/u/");
		int endPersonIdIndex = request.getRequestURL().indexOf("/e/");
		if(startTenantIndex==-1){
			tenantId = request.getParameter("tenantId");
			personId = request.getParameter("personId");
		}
		else{
			tenantId = request.getRequestURL().substring(startTenantIndex+3,endTenantIdIndex);
			personId = request.getRequestURL().substring(startPersonIdIndex+3, endPersonIdIndex);
		}
		logger.debug("requestInitialized() - Initializing request : {}", request.getRequestURL());
		if(null==tenantId){
			logger.debug("requestInitialized() - Failed to resolve tenant from URL : {}", request.getRequestURI());
			return;
		}
		try{
			access_token = request.getHeader("access_token");
			}catch(Exception e){
				return;
			}
		//Build UserContext
		personName=request.getParameter("personName");
		loggedInUserTimeZone=request.getParameter("timeZone");
		deviceType=request.getParameter("deviceType");
		UserContext userContext = new UserContext(personId,personName,loggedInUserTimeZone,deviceType);
		Tenant tenant = TenantRegistry.getInstance().getTenantIdTenantMap().get(tenantId);
		if(tenant != null){
			TenantInfo info = new TenantInfo(tenant.getTenantId(), tenant.getCustomerId(), tenant.getDatabaseInstanceName(), tenant.getDatbaseSchemaName(), tenant.getAuthUrl(), userContext, serverName);
			TenantContextHolder.pushCurrentTenantInfo(info);
		}else{
			TenantInfo info = new TenantInfo("ERROR", "ERROR", "ERROR", "ERROR", "ERROR", userContext, serverName);
			TenantContextHolder.pushCurrentTenantInfo(info);
		}
		//Check for Authentication/validation of token
		try{
			access_token = request.getHeader("access_token");
		}catch(Exception e){
			return;
		}
		//Add into Tenantcontextholder. Also add userdetails into TenantInfo.
	}
	
	private String getTenantId(){
		return null;
	}
	private Logger getLogger(){
		if(LOGGER == null){
			LOGGER = LogFactory.getLogger(TapplentSetupHandler.class);
		}
		return LOGGER;
	}

}
