package com.tapplent.tenantresolver.server.property;

public class Node {
	
        Node left;
        Node right;
        Enum<ActionControlCode> actionControl;
        Enum<ServerRuleCode> serverRuleCode;
        ServerActionStep serverActionStep;

        public Node(Enum<ActionControlCode> actionControl, Enum<ServerRuleCode> serverRuleCode, ServerActionStep serverActionStep) {
          this.actionControl = actionControl;
          this.serverRuleCode = serverRuleCode;
          this.serverActionStep = serverActionStep;
        }

		public Node getLeft() {
			return left;
		}

		public void setLeft(Node left) {
			this.left = left;
		}

		public Node getRight() {
			return right;
		}

		public void setRight(Node right) {
			this.right = right;
		}

		public Enum<ActionControlCode> getActionControl() {
			return actionControl;
		}

		public void setActionControl(Enum<ActionControlCode> actionControl) {
			this.actionControl = actionControl;
		}
		
		public Enum<ServerRuleCode> getServerRuleCode() {
			return serverRuleCode;
		}

		public void setServerRuleCode(Enum<ServerRuleCode> serverRuleCode) {
			this.serverRuleCode = serverRuleCode;
		}

		public ServerActionStep getServerActionStep() {
			return serverActionStep;
		}

		public void setServerActionStep(ServerActionStep serverActionStep) {
			this.serverActionStep = serverActionStep;
		}
}
