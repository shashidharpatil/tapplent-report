package com.tapplent.tenantresolver.tenant;

import java.sql.SQLException;


import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.sql.DataSource;
import javax.sql.RowSet;

import org.apache.commons.codec.binary.Hex;

import com.tapplent.tenantresolver.server.property.ServerProperty;
import com.tapplent.tenantresolver.utility.TapplentApplicationContext;
import com.tapplent.tenantresolver.jdbc.DBUtil;

public class TenantRegistryService {
	
	DataSource datasource;
	
	public DataSource getDatasource() {
		return datasource;
	}
	
	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}

	private static final String ALL_TENANT_SELECT_QUERY = "SELECT TENANT_RESOLVER_CODE_PK_ID, CUSTOMER_FK_ID, DB_INSTANCE_NAME_CODE_FK_ID, DB_SCHEMA_NAME_CODE_FK_ID, AUTH_URL_CODE_FK_ID FROM TAPPLENT_BOOT_SCHEMA_STAGING.T_SYS_AUT_TENANT_RESOLVER_INTERNAL";
	
	private Map<String, Tenant> tenantIdTenantMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

	public Map<String, Tenant> getTenantIdTenantMap() {
		return tenantIdTenantMap;
	}

	public void setTenantIdTenantMap(Map<String, Tenant> tenantIdTenantMap) {
		this.tenantIdTenantMap = tenantIdTenantMap;
	}
   
	public void populateTenantDetails() throws SQLException{
		DBUtil dbUtil = new DBUtil(datasource);
		RowSet rowSet = dbUtil.getRowSet(ALL_TENANT_SELECT_QUERY);
		while(rowSet.next()){
			String tenantId = String.valueOf(rowSet.getString("TENANT_RESOLVER_CODE_PK_ID"));
			String customerId = Hex.encodeHexString(rowSet.getBytes("CUSTOMER_FK_ID"));
			String databaseInstanceName = rowSet.getString("DB_INSTANCE_NAME_CODE_FK_ID");
			String datbaseSchemaName = rowSet.getString("DB_SCHEMA_NAME_CODE_FK_ID");
			String authUrl = rowSet.getString("AUTH_URL_CODE_FK_ID");
			addTenantDetails(tenantId,customerId, databaseInstanceName, datbaseSchemaName, authUrl);
			updateTenantIdTenantMap();
		}
	}

	private void updateTenantIdTenantMap() {
		TenantRegistry tenantRegistry = TenantRegistry.getInstance();
		tenantRegistry.tenantIdTenantMap= this.tenantIdTenantMap;
	}

	private void addTenantDetails(String tenantId,String customerId, String databaseInstanceName,
			String datbaseSchemaName, String authUrl) {
			Tenant tenant = new Tenant(tenantId, customerId, databaseInstanceName, datbaseSchemaName, authUrl);
			tenantIdTenantMap.put(tenantId, tenant);
	}
	
	public void populateGlobalVariables() {
		try {
			populateTenantDetails();
//			populateServerProperty();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	private void populateServerProperty() {
		for(Entry<String, Tenant> entry : tenantIdTenantMap.entrySet()){
//			if(entry.getKey().matches("15")){
				DataSource dataSource = getDataSource(entry.getValue().getDatabaseInstanceName());
				ServerProperty.populateTenantServerActionList(entry.getKey(), entry.getValue().getDatbaseSchemaName(), dataSource);
//			}
		}
	}
	private DataSource getDataSource(String dataSource){
		return (DataSource) TapplentApplicationContext.getApplicationContext().getBean(dataSource);
	}
//	Write here the a method to get the right datasource bean and pass that.
}
