package com.tapplent.tenantresolver.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import javax.sql.RowSet;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;

public class DBUtil {
	
	private DataSource datasource;
	
	public DBUtil(DataSource datasource){
		this.datasource= datasource;
	}
	
	public Connection getConnection() throws SQLException{
		return datasource.getConnection();
	}
	
	public Connection closeConnection(final Connection conn) throws SQLException{
		if(conn!=null){
			conn.close();
		}
		return null;
	}

	public RowSet getRowSet(String tenantSelectQuery) throws SQLException {
		Connection connection = getConnection();
		RowSet rowSet = select(connection,tenantSelectQuery);
		closeConnection(connection);
		return rowSet;
	}
	
	private RowSet select(Connection connection, String sql) throws SQLException{
		PreparedStatement statement = connection.prepareStatement(sql);
		ResultSet resultSet = statement.executeQuery();
		CachedRowSet rowSet = RowSetProvider.newFactory().createCachedRowSet();
		rowSet.populate(resultSet);
		close(resultSet);
		return rowSet;
	}

	private void close(ResultSet resultSet) throws SQLException {
		if(null != resultSet){
			resultSet.close();
		}
	}
}
