package com.tapplent.tenantresolver.server.property;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.sql.DataSource;

public class ServerProperty {
	
//	public static Map actionCodeActionRuleMap = new HashMap<String, BinaryTree>();
	
	public static Map<String, HashMap<String, ?>> tenantServerActionMap = new HashMap<>() ;

	public static void populateTenantServerActionMap(String tenantId, String currentDatabaseName, DataSource datasource) {
		HashMap<String, ServerActionStep> actionCodeActionStepMap = ServerPropertyDAOImpl.getAllServerActionStep(currentDatabaseName, datasource);
		HashMap<String, BinaryTree> actionCodeActionStepTree = new HashMap<>(); 
		for(Entry<String, ServerActionStep> entry : actionCodeActionStepMap.entrySet()){
			String actionCode = entry.getKey().split("+")[0];
			if(!actionCodeActionStepTree.containsKey(actionCode)){
				List<ServerActionStep> filteredByActionCode = actionCodeActionStepMap.values().stream()
														.filter(p -> p.getActionCodeFkId().equals(actionCode))
														.collect(Collectors.toList());
				BinaryTree bTree = new BinaryTree();
				Map<String, ServerActionStep> ParentActionStepMap = new HashMap<>();
				Map<String, Node> actionStepNodeMap = new HashMap<>();
				for(ServerActionStep SAStep : filteredByActionCode){
					ParentActionStepMap.put(SAStep.getParentActionStepId(), SAStep);
//					Enum<ActionControlCode> actionControlCode = ActionControlCode.fromString(SAStep.getActionControlCode());
					Enum<ServerRuleCode> serverRuleCode = ServerRuleCode.fromString(SAStep.getActionStepLookupPkId());
//					actionStepNodeMap.put(SAStep.getActionStepLookupPkId(), new Node(actionControlCode, serverRuleCode, SAStep));
				}
				for(Entry<String, Node> node : actionStepNodeMap.entrySet()){
					String parentActionStepLookupFkId = node.getValue().getServerActionStep().getParentActionStepId();
					bTree.addNode(actionStepNodeMap.get(parentActionStepLookupFkId), actionStepNodeMap.get(node.getKey()));
				}
				actionCodeActionStepTree.put(actionCode, bTree);
			}	
		}
		tenantServerActionMap.put(tenantId, actionCodeActionStepTree);
	}

	public static void populateTenantServerActionList(String tenantId, String datbaseSchemaName, DataSource datasource) {
		HashMap<String, ServerActionStep> actionCodeActionStepMap = ServerPropertyDAOImpl.getAllServerActionStep(datbaseSchemaName, datasource);
		Map<String, List<ServerActionStepProperty>> actionStepPropertyMap = ServerPropertyDAOImpl.getAllServerActionStepProperty(datbaseSchemaName, datasource);
		HashMap<String, LinkedList<ServerActionStep>> actionCodeActionStepListMap = new HashMap<>();
		for(Entry<String, ServerActionStep> entry : actionCodeActionStepMap.entrySet()){
			String actionCode = entry.getValue().getActionCodeFkId();
			if(!actionCodeActionStepListMap.containsKey(actionCode)){
				List<ServerActionStep> filteredByActionCode = actionCodeActionStepMap.values().stream()
														.filter(p -> p.getActionCodeFkId().equals(actionCode))
														.collect(Collectors.toList());
				LinkedList<ServerActionStep> serverActionStepLinkedList = new LinkedList<>();
				Map<String, ServerActionStep> parentActionStepMap = new HashMap<>();
				Map<String, ServerActionStep> actionStepMap = new HashMap<>();
				for(ServerActionStep SAStep : filteredByActionCode){
					parentActionStepMap.put(SAStep.getParentActionStepId(), SAStep);
					actionStepMap.put(SAStep.getActionStepLookupPkId(),  SAStep);
				}
				Optional<ServerActionStep> rootActionStep = filteredByActionCode.stream()
																				.filter(p -> p.getParentActionStepId() == null)
																				.findFirst();
				ServerActionStep currentActionStep = rootActionStep.get();
				while(currentActionStep != null){
					serverActionStepLinkedList.add(currentActionStep);
					System.out.println(currentActionStep.getActionCodeFkId() + "-->" + currentActionStep.getServerRuleCodeFkId());
					//add all properties related to current action step.
					if(actionStepPropertyMap.containsKey(currentActionStep.getActionStepLookupPkId()))
						populateAllActionStepProperties(currentActionStep, actionStepPropertyMap);
					currentActionStep = parentActionStepMap.get(currentActionStep.getActionStepId());
				}
				actionCodeActionStepListMap.put(actionCode, serverActionStepLinkedList);
			}
		}
		tenantServerActionMap.put(tenantId, actionCodeActionStepListMap);
	}

	private static void populateAllActionStepProperties(ServerActionStep currentActionStep,
			Map<String, List<ServerActionStepProperty>> actionStepPropertyMap) {
		for(ServerActionStepProperty ServerActionStepProperty : actionStepPropertyMap.get(currentActionStep.getActionStepLookupPkId()))
			currentActionStep.getPropertyCodeValueMap().put(ServerActionStepProperty.getActionStepPropertyCodeFkId(), ServerActionStepProperty.getPropertyValue());
	}
}
