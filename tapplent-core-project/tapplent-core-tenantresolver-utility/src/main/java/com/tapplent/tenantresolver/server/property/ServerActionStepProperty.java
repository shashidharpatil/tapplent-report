package com.tapplent.tenantresolver.server.property;

public class ServerActionStepProperty {
	private String versionId;
	private String actionStepPropertyDefnPkId;
	private String actionStepLookupFkId;
	private String actionStepPropertyCodeFkId;
	private String propertyValue;
	
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getActionStepPropertyDefnPkId() {
		return actionStepPropertyDefnPkId;
	}
	public void setActionStepPropertyDefnPkId(String actionStepPropertyDefnPkId) {
		this.actionStepPropertyDefnPkId = actionStepPropertyDefnPkId;
	}
	public String getActionStepLookupFkId() {
		return actionStepLookupFkId;
	}
	public void setActionStepLookupFkId(String actionStepLookupFkId) {
		this.actionStepLookupFkId = actionStepLookupFkId;
	}
	public String getActionStepPropertyCodeFkId() {
		return actionStepPropertyCodeFkId;
	}
	public void setActionStepPropertyCodeFkId(String actionStepPropertyCodeFkId) {
		this.actionStepPropertyCodeFkId = actionStepPropertyCodeFkId;
	}
	public String getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
}
