package com.tapplent.tenantresolver.server.property;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.codec.binary.Hex;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;

public class ServerPropertyDAOImpl extends SqlSessionDaoSupport {
	
	private static final Logger LOG = LogFactory.getLogger(ServerPropertyDAOImpl.class);
	
	private static final String ALL_SERVER_ACTION_STEP = " SELECT VERSION_ID, SERVER_ACTION_STEP_RULES_PK_ID ,SERVER_ACTION_CODE_DEP_FK_ID, SERVER_ACTION_STEPID_TXT, COLUMN_JSON(SERVER_ACTION_STEP_NAME_G11N_BLOB) actionStepName, PARENT_SERVER_ACTION_STEPID_TXT ,SERVER_ACTION_STEP_RULE_CODE_FK_ID FROM T_UI_ADM_SERVER_ACTION_STEP_RULES;";
	
	private static final String ALL_SERVER_ACTION_PROPERTIES = "SELECT VERSION_ID, SERVER_ACTION_STEP_PROPERTY_TRANSACTION_PK_ID, SERVER_ACTION_STEP_RULES_FK_ID, SERVER_ACTION_STEP_PROPERTY_CODE_FK_ID, SERVER_PROPERTY_VALUE_TXT FROM T_UI_ADM_SERVER_ACTION_STEP_PROPERTY_TRANSACTION;";
	
	private DataSource datasource;
	
	public ServerPropertyDAOImpl(DataSource datasource) {
		this.datasource = datasource;
	}

	public Connection getConnection(String databaseName) throws SQLException{
		Connection conn = datasource.getConnection();
		conn.setCatalog(databaseName);
		return conn;
	}

	public static HashMap<String, ServerActionStep> getAllServerActionStep(String currentDatabaseName, DataSource dataSource) {
		
		PreparedStatement ps;
		HashMap<String, ServerActionStep> actionCodeActionStepMap = new HashMap<String, ServerActionStep>();
		ServerPropertyDAOImpl spDAO = new ServerPropertyDAOImpl(dataSource);
				
		try {
			Connection con = null;
			con = spDAO.getConnection(currentDatabaseName);
			ps = con.prepareStatement(ALL_SERVER_ACTION_STEP);
			LOG.debug(ALL_SERVER_ACTION_STEP);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				ServerActionStep serverActionStep = new ServerActionStep();
				serverActionStep.setVersionId(Hex.encodeHexString(rs.getBytes("VERSION_ID")));
				serverActionStep.setActionStepLookupPkId(Hex.encodeHexString(rs.getBytes("SERVER_ACTION_STEP_RULES_PK_ID")));
				serverActionStep.setActionCodeFkId(rs.getString("SERVER_ACTION_CODE_DEP_FK_ID"));
				serverActionStep.setParentActionStepId(rs.getString("PARENT_SERVER_ACTION_STEPID_TXT"));
				serverActionStep.setActionStepId(rs.getString("SERVER_ACTION_STEPID_TXT"));
				serverActionStep.setServerRuleCodeFkId(rs.getString("SERVER_ACTION_STEP_RULE_CODE_FK_ID"));				
				actionCodeActionStepMap.put(serverActionStep.getActionStepLookupPkId(), serverActionStep);
			}
		}
		catch (SQLException e) {
		e.printStackTrace();
		LOG.error(currentDatabaseName);
//		throw new RuntimeException(e);
		}
		return actionCodeActionStepMap;
	}

	public DataSource getDatasource() {
		return datasource;
	}

	public void setDatasource(DataSource datasource) {
		this.datasource = datasource;
	}

	public static Map<String, List<ServerActionStepProperty>> getAllServerActionStepProperty(String datbaseSchemaName,
			DataSource datasource) {
		
		PreparedStatement ps;
		Map<String, List<ServerActionStepProperty>> actionStepPropertyMap = new HashMap<String, List<ServerActionStepProperty>>();
		ServerPropertyDAOImpl spDAO = new ServerPropertyDAOImpl(datasource);
				
		try {
			Connection con = null;
			con = spDAO.getConnection(datbaseSchemaName);
			ps = con.prepareStatement(ALL_SERVER_ACTION_PROPERTIES);
			LOG.debug(ALL_SERVER_ACTION_PROPERTIES);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				ServerActionStepProperty serverActionStepProperty = new ServerActionStepProperty();
				serverActionStepProperty.setVersionId(Hex.encodeHexString(rs.getBytes("VERSION_ID")));
				serverActionStepProperty.setActionStepPropertyDefnPkId(Hex.encodeHexString(rs.getBytes("SERVER_ACTION_STEP_PROPERTY_TRANSACTION_PK_ID")));
				serverActionStepProperty.setActionStepLookupFkId(Hex.encodeHexString(rs.getBytes("SERVER_ACTION_STEP_RULES_FK_ID")));
				serverActionStepProperty.setActionStepPropertyCodeFkId(rs.getString("SERVER_ACTION_STEP_PROPERTY_CODE_FK_ID"));
				serverActionStepProperty.setPropertyValue(rs.getString("SERVER_PROPERTY_VALUE_TXT"));
				if(actionStepPropertyMap.get(serverActionStepProperty.getActionStepLookupFkId()) == null){
					List<ServerActionStepProperty> serverActionStepPropertyList = new ArrayList<>();
					serverActionStepPropertyList.add(serverActionStepProperty);
					actionStepPropertyMap.put(serverActionStepProperty.getActionStepLookupFkId(), serverActionStepPropertyList);
				}
				else{
					actionStepPropertyMap.get(serverActionStepProperty.getActionStepLookupFkId()).add(serverActionStepProperty);
				}
			}
		}
		catch (SQLException e) {
		e.printStackTrace();
		LOG.error(datbaseSchemaName);
//		throw new RuntimeException(e);
		}
		return actionStepPropertyMap;
	}
}
