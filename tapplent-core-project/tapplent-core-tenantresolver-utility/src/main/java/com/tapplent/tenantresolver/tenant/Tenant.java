package com.tapplent.tenantresolver.tenant;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Tenant {
	private final String tenantId;
	private final String customerId;
	private final String databaseInstanceName;
	private final String datbaseSchemaName;
	private final String authUrl;
	private final Map<String,Object> session;
	
	public Tenant(String tenantId, String customerId, String databaseInstanceName,
			String datbaseSchemaName, String authUrl) {
		this.tenantId = tenantId;
		this.customerId = customerId;
		this.databaseInstanceName = databaseInstanceName;
		this.datbaseSchemaName = datbaseSchemaName;
		this.authUrl = authUrl;
		this.session = new ConcurrentHashMap<String, Object>();
	}
	public String getTenantId() {
		return tenantId;
	}
	public String getDatabaseInstanceName() {
		return databaseInstanceName;
	}
	public String getDatbaseSchemaName() {
		return datbaseSchemaName;
	}
	public String getAuthUrl() {
		return authUrl;
	}
	public String getCustomerId() {
		return customerId;
	}
	public Map<String, Object> getSession() {
		return session;
	}
	public void setAttribute(String name, Object attribute) {
		session.put(name, attribute);
		
	}
	public Object getAttribute(String name) {
		return session.get(name);
	}

}
