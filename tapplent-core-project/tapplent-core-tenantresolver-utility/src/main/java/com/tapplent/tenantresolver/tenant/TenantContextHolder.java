package com.tapplent.tenantresolver.tenant;

import java.util.Deque;
import java.util.LinkedList;

import com.tapplent.tenantresolver.jdbc.DataSourceInstance;
import com.tapplent.tenantresolver.user.UserContext;


public class TenantContextHolder{
	private static final CurrentTenantInfoThreadLocal currentTenantInfo = new CurrentTenantInfoThreadLocal();
	
	private static Deque<TenantInfo> getCurrentTenantInfoStack(){
		Deque<TenantInfo> result = currentTenantInfo.get();
		if(result ==null){
			result = new LinkedList<TenantInfo>();
			currentTenantInfo.set(result);
		}
		return result;
	}
	
	public static TenantInfo getCurrentTenantInfo(){
		final Deque<TenantInfo> stack = getCurrentTenantInfoStack();
		final TenantInfo result = stack.peek();
		if (result == null) {
			currentTenantInfo.remove();
		}
		return result;
	}
	
	public static void pushCurrentTenantInfo(final TenantInfo tenantInfo){
		final Deque<TenantInfo> stack = getCurrentTenantInfoStack();
		stack.push(tenantInfo);
	}
	
	public static TenantInfo popCurrentTenantInfo(){
		final Deque<TenantInfo> stack = getCurrentTenantInfoStack();
		final TenantInfo result = stack.pop();
		if (stack.isEmpty()) {
			currentTenantInfo.remove();
		} else {
			
		}
		return result;

	}
	
	public static String getCurrentTenantSchema() {
		final Tenant currentTenant = getCurrentTenant();
		final String result;
		if (currentTenant == null) {
			result = null;
		} else {
			result = currentTenant.getDatbaseSchemaName();
		}
		return result;
	}
	
	public static DataSourceInstance getCurrentDataSourceInstance(){
		final Tenant currentTenant = getCurrentTenant();
		if(currentTenant == null)
			return null;
		return DataSourceInstance.valueOf(currentTenant.getDatabaseInstanceName());
	}		
	
	public static Tenant getCurrentTenant() {
		final String tenantID = getCurrentTenantID();
		if (tenantID == null)
			return null;
		final Tenant result = TenantRegistry.getInstance().getTenant(tenantID);
		return result;
	}

	public static String getCurrentTenantID() {
		final TenantInfo info = getCurrentTenantInfo();
		final String result = info == null ? null : info.getTenantId();
		return result;
	}

	public static UserContext getUserContext() {
		final TenantInfo info = getCurrentTenantInfo();
		if (null != info) {
			return info.getUserContext();
		}
		return null;
	}

	private static final class CurrentTenantInfoThreadLocal extends InheritableThreadLocal<Deque<TenantInfo>>{
		@Override
		protected Deque<TenantInfo> childValue(final Deque<TenantInfo> parentValue) {
			if (parentValue == null)
				return null;
			return new LinkedList<TenantInfo>(parentValue);
		}
	}
	
	public static void setAttribute(String name, Object attribute) {
		Tenant currentTenant = getCurrentTenant();
		currentTenant.setAttribute(name, attribute);
	}

	public static Object getAttribute(String name) {
		Tenant currentTenant = getCurrentTenant();
		return currentTenant.getAttribute(name);
	}
}
