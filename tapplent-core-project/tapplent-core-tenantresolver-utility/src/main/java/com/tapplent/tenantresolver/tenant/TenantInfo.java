 package com.tapplent.tenantresolver.tenant;

import com.tapplent.tenantresolver.user.UserContext;

public class TenantInfo {
	private final String tenantId;
	private final String serverName;
	private final String customerId;
	private final String databaseInstanceName;
	private final String databaseSchemaName;
	private final String authUrl;
	private final UserContext userContext;
	//Include requestURL
	
	public TenantInfo(String tenantId, String customerId, String databaseInstanceName,
			String databaseSchemaName, String authUrl, UserContext userContext, String serverName) {
		this.tenantId = tenantId;
		this.customerId = customerId;
		this.databaseInstanceName = databaseInstanceName;
		this.databaseSchemaName = databaseSchemaName;
		this.authUrl = authUrl;
		this.userContext = userContext;
		this.serverName = serverName;
	}

	public TenantInfo(TenantInfo currentTenantInfo) {
		this.tenantId = currentTenantInfo.getTenantId();
		this.customerId = currentTenantInfo.getCustomerId();
		this.databaseInstanceName = currentTenantInfo.getDatabaseInstanceName();
		this.databaseSchemaName = currentTenantInfo.getDatabaseSchemaName();
		this.authUrl = currentTenantInfo.getAuthUrl();
		this.userContext = currentTenantInfo.getUserContext();
		this.serverName = currentTenantInfo.getServerName();
	}

	public String getTenantId() {
		return tenantId;
	}	
	public String getCustomerId() {
		return customerId;
	}
	public String getDatabaseInstanceName() {
		return databaseInstanceName;
	}
	public String getDatabaseSchemaName() {
		return databaseSchemaName;
	}
	public String getAuthUrl() {
		return authUrl;
	}
	public UserContext getUserContext() {
		return userContext;
	}

	public String getServerName() {
		return serverName;
	}
}
