package com.tapplent.tenantresolver.server.property;

public enum ActionControlCode {
	
	DIRTY("DIRTY"),
	
	CLEAN("CLEAN"),
	
	INVALID_DATA("INVALID_DATA"),
	
	VALID_DATA("VALID_DATA"),
	
	MANDATORY_FAIL("MANDATORY_FAIL"),
	
	MANDATORY_PASS("MANDATORY_PASS"),
	
	WORKFLOW_NOT_APPLICABLE("WORKFLOW_NOT_APPLICABLE"),
	
	WORKFLOW_APPLICABLE("WORKFLOW_APPLICABLE");
	
	String code;
	
	ActionControlCode(String code){
		this.code = code;
	}
	
	public static ActionControlCode fromString(String actionControlCodeString) {
	    if (actionControlCodeString != null) {
	      for (ActionControlCode b : ActionControlCode.values()) {
	        if (actionControlCodeString.equalsIgnoreCase(b.code)) {
	          return b;
	        }
	      }
	    }
	    return null;
	  }
}
