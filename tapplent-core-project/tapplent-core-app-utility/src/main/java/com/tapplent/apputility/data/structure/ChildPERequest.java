package com.tapplent.apputility.data.structure;

import java.util.List;
import java.util.Map;

import com.tapplent.platformutility.search.builder.FilterOperatorValue;
import com.tapplent.platformutility.search.builder.SortData;

public class ChildPERequest {
	private String mtPE;
	private String baseTemplateId;
	private String btPE;
	private Map<String,List<FilterOperatorValue>> filters;
	private String filterExpression;
	private List<SortData> sortData;
	private List<ChildPERequest> childrenPE;
	public String getMtPE() {
		return mtPE;
	}
	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getBtPE() {
		return btPE;
	}
	public void setBtPE(String btPE) {
		this.btPE = btPE;
	}
	public Map<String, List<FilterOperatorValue>> getFilters() {
		return filters;
	}
	public void setFilters(Map<String, List<FilterOperatorValue>> filters) {
		this.filters = filters;
	}
	public String getFilterExpression() {
		return filterExpression;
	}
	public void setFilterExpression(String filterExpression) {
		this.filterExpression = filterExpression;
	}
	public List<SortData> getSortData() {
		return sortData;
	}
	public void setSortData(List<SortData> sortData) {
		this.sortData = sortData;
	}
	public List<ChildPERequest> getChildrenPE() {
		return childrenPE;
	}
	public void setChildrenPE(List<ChildPERequest> childrenPE) {
		this.childrenPE = childrenPE;
	}
	
}
