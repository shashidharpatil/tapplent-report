package com.tapplent.apputility.thirdparty.service;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

public interface GoogleTaskService {

	public void taskSynch(String personpkId)throws ClientProtocolException, IOException, JSONException;
}
