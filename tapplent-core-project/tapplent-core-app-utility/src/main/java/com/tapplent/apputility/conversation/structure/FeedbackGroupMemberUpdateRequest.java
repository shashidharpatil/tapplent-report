package com.tapplent.apputility.conversation.structure;

import java.util.List;

/**
 * Created by Tapplent on 6/22/17.
 */
public class FeedbackGroupMemberUpdateRequest {
    List<GroupMember> groupMemberList;

    public List<GroupMember> getGroupMemberList() {
        return groupMemberList;
    }

    public void setGroupMemberList(List<GroupMember> groupMemberList) {
        this.groupMemberList = groupMemberList;
    }
}
