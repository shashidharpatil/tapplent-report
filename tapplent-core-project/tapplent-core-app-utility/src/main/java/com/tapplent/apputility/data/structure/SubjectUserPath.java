package com.tapplent.apputility.data.structure;

/**
 * Created by tapplent on 25/09/17.
 */
public class SubjectUserPath {
    private String mtPE;
    private String mtPEAlias;
    private String parentMTPEAlias;
    private String fkRelationshipWithParent;
    private String attributePathExpn;

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getParentMTPEAlias() {
        return parentMTPEAlias;
    }

    public void setParentMTPEAlias(String parentMTPEAlias) {
        this.parentMTPEAlias = parentMTPEAlias;
    }

    public String getFkRelationshipWithParent() {
        return fkRelationshipWithParent;
    }

    public void setFkRelationshipWithParent(String fkRelationshipWithParent) {
        this.fkRelationshipWithParent = fkRelationshipWithParent;
    }

    public String getAttributePathExpn() {
        return attributePathExpn;
    }

    public void setAttributePathExpn(String attributePathExpn) {
        this.attributePathExpn = attributePathExpn;
    }

    public SubjectUserPath(String mtPE, String mtPEAlias, String parentMTPEAlias, String fkRelationshipWithParent, String attributePathExpn) {
        this.mtPE = mtPE;
        this.mtPEAlias = mtPEAlias;
        this.parentMTPEAlias = parentMTPEAlias;
        this.fkRelationshipWithParent = fkRelationshipWithParent;
        this.attributePathExpn = attributePathExpn;
    }
}
