package com.tapplent.apputility.conversation.structure;

import java.util.List;

/**
 * Created by Tapplent on 6/15/17.
 */
public class WriteFeedbackMessageRequest {
    private List<FeedbackMessage> feedbackMessageList;
//    private String feedbackPkId;
//    private String feedbackGroupId;
//    private String message;
//    private String messageType;
//    private String messageAttach;
//    private String feedbackParentMsgId;
//    private boolean messageEdited;
//    private boolean deleted;

    public List<FeedbackMessage> getFeedbackMessageList() {
        return feedbackMessageList;
    }

    public void setFeedbackMessageList(List<FeedbackMessage> feedbackMessageList) {
        this.feedbackMessageList = feedbackMessageList;
    }
}
