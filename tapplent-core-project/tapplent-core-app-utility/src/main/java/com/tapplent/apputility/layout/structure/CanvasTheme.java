package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.CanvasThemeVO;

public class CanvasTheme {
	@JsonIgnore
	private String versionId;
	@JsonIgnore
	private String canvasThemeSelecPkId;
	private String canvasType;
	private String themeTemplateCode;
	private JsonNode themeProperty;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	
	public JsonNode getThemeProperty() {
		return themeProperty;
	}
	public void setThemeProperty(JsonNode themeProperty) {
		this.themeProperty = themeProperty;
	}
	
	public String getCanvasType() {
		return canvasType;
	}
	public void setCanvasType(String canvasType) {
		this.canvasType = canvasType;
	}
	public String getThemeTemplateCode() {
		return themeTemplateCode;
	}
	public void setThemeTemplateCode(String themeTemplateCode) {
		this.themeTemplateCode = themeTemplateCode;
	}
	public String getCanvasThemeSelecPkId() {
		return canvasThemeSelecPkId;
	}
	public void setCanvasThemeSelecPkId(String canvasThemeSelecPkId) {
		this.canvasThemeSelecPkId = canvasThemeSelecPkId;
	}
	public static CanvasTheme fromVO(CanvasThemeVO canvasThemeVO){
		CanvasTheme canvasTheme = new CanvasTheme();
		BeanUtils.copyProperties(canvasThemeVO, canvasTheme);
		return canvasTheme;
	}
}
