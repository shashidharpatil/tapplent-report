package com.tapplent.apputility.layout.structure;

import java.util.Map;

public class CanvasTransaction {
	private String canvasTransactionId;
	private String canvasRefId;
	private Map<String, String> deviceIndependentBlob;
	private Map<String, String> deviceDependentBlob;
	public CanvasTransaction(String canvasTransactionId, String canvasRefId, Map<String, String> deviceIndependentBlob,
			Map<String, String> deviceDependentBlob) {
		super();
		this.canvasTransactionId = canvasTransactionId;
		this.canvasRefId = canvasRefId;
		this.deviceIndependentBlob = deviceIndependentBlob;
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public String getCanvasTransactionId() {
		return canvasTransactionId;
	}
	public void setCanvasTransactionId(String canvasTransactionId) {
		this.canvasTransactionId = canvasTransactionId;
	}
	public String getCanvasRefId() {
		return canvasRefId;
	}
	public void setCanvasRefId(String canvasRefId) {
		this.canvasRefId = canvasRefId;
	}
	public Map<String, String> getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(Map<String, String> deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public Map<String, String> getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(Map<String, String> deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	
}
