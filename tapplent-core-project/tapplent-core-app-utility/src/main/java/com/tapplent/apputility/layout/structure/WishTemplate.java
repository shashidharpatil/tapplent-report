package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.layout.valueObject.WishTemplateVO;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;

/**
 * Created by tapplent on 12/09/17.
 */
public class WishTemplate {
    private String wishTemplateID;
    private String messageHeader;
    private String messageBody;
    private String messageFooter;

    public String getWishTemplateID() {
        return wishTemplateID;
    }

    public void setWishTemplateID(String wishTemplateID) {
        this.wishTemplateID = wishTemplateID;
    }

    public String getMessageHeader() {
        return messageHeader;
    }

    public void setMessageHeader(String messageHeader) {
        this.messageHeader = messageHeader;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageFooter() {
        return messageFooter;
    }

    public void setMessageFooter(String messageFooter) {
        this.messageFooter = messageFooter;
    }

    public static WishTemplate fromVO(WishTemplateVO wishTemplateVO) {
        WishTemplate wishTemplate = new WishTemplate();
        BeanUtils.copyProperties(wishTemplateVO, wishTemplate);
        return wishTemplate;
    }
}
