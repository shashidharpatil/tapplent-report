package com.tapplent.apputility.data.service;

import com.tapplent.apputility.data.structure.PropertyControlData;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.metadata.structure.MasterEntityAttributeMetadata;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

import java.util.List;

/**
 * This class helps in handling all the property control requirements of the user.
 * Created by tapplent on 15/11/16.
 */
public class PropertyControlUtil {
    public PropertyControlData getPropertyControlValue(String propertyControlExpn, String baseTemplateId){
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        PropertyControlData result = null;
        String functionName = propertyControlExpn.substring(0, propertyControlExpn.indexOf('('));
        switch (functionName){
            case "getMTDOAName":
                String controlAttributeExpn = propertyControlExpn.substring(propertyControlExpn.indexOf("(")+1,propertyControlExpn.indexOf(")"));
                //Now we have control attribute expression
                //We need to get the right doa code to be able to fetch its proper name
                String[] splitedDOA = controlAttributeExpn.split(":");
                String doaCode = splitedDOA[splitedDOA.length-1];
                // we need baseTemplate also here
                MasterEntityAttributeMetadata doaMeta = metadataService.getMetadataByDoa(doaCode);
                result = new PropertyControlData(doaMeta.getDoAttributeCode(), null);
                break;
        }
        return result;
    }
}
