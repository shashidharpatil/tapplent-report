package com.tapplent.apputility.conversation.service;

import com.tapplent.apputility.conversation.structure.*;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Tapplent on 6/9/17.
 */
public interface ConversationService {
    public FeedbackGroupResponse getFeedbackGroupList(String mtPECode, String objectId, int limit, int offset, String keyword);

    CreateFdbkGrpResponse createFeedbackGroup(CreateFdbkGrpRequest feedbackContactRequest) throws SQLException;

    FeedbackContactDetails getFeedbackContactDetails(String mTPECode, String objectId, int limit, int offset, String keyword, String feedbackGroupId) throws SQLException;

    WriteFeedbackMessageResponse writeFeedbackMessage(WriteFeedbackMessageRequest writeFeedbackMessageRequest);

    List<FeedbackMessage> getFeedbackMessage(String feedbackGroupId, int limit, int offset);

    List<GroupMember> getFeedbackGroupMemberDetails(String feedbackGroupId);

    List<GroupMember> updateFeedbackGroupMemberDetails(FeedbackGroupMemberUpdateRequest feedbackGroupMemberUpdateRequest) throws SQLException;

    List<GroupMember> addFeedbackGroupMemberDetails(FeedbackGroupMemberAddRequest feedbackGroupMemberAddRequest) throws SQLException;

    void deleteGroup(String feedbackGroupId);

    CreateFdbkGrpResponse updateFeedbackGroup(CreateFdbkGrpRequest feedbackContactRequest) throws SQLException;

    List<GroupMember> getFeedbackGroupMemberReadStatus(FeedbackMessageReadStatusRequest feedbackMessageReadStatusRequest);

    List<FeedbackMessage> getBookmarkedFeedbackMessage(String feedbackGroupId, int limit, int offset);
}
