package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;


import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.ControlRelationControlVO;

public class ControlRelationControl {
	private String ControlRCId;
	private String baseTemplateId;
	private String mtPE;
	private String canvasId;
	private String controlId;
	private String rcCanvasControlMasterId;
	private String rcCanvasControlAttribute;
	private String rcCanvasControlAggregateFunction;
	private JsonNode deviceIndependentBlob;
	private JsonNode deviceDependentBlob;
	
	public String getControlRCId() {
		return ControlRCId;
	}

	public void setControlRCId(String controlRCId) {
		ControlRCId = controlRCId;
	}

	public String getBaseTemplateId() {
		return baseTemplateId;
	}

	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getMtPE() {
		return mtPE;
	}

	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}

	public String getCanvasId() {
		return canvasId;
	}

	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}

	public String getControlId() {
		return controlId;
	}

	public void setControlId(String controlId) {
		this.controlId = controlId;
	}

	public String getRcCanvasControlMasterId() {
		return rcCanvasControlMasterId;
	}

	public void setRcCanvasControlMasterId(String rcCanvasControlMasterId) {
		this.rcCanvasControlMasterId = rcCanvasControlMasterId;
	}

	public String getRcCanvasControlAttribute() {
		return rcCanvasControlAttribute;
	}

	public void setRcCanvasControlAttribute(String rcCanvasControlAttribute) {
		this.rcCanvasControlAttribute = rcCanvasControlAttribute;
	}

	public String getRcCanvasControlAggregateFunction() {
		return rcCanvasControlAggregateFunction;
	}

	public void setRcCanvasControlAggregateFunction(String rcCanvasControlAggregateFunction) {
		this.rcCanvasControlAggregateFunction = rcCanvasControlAggregateFunction;
	}

	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}

	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}

	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}

	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}

	public static ControlRelationControl fromVO(ControlRelationControlVO controlRelationControlEuVO){
		ControlRelationControl controlRelationControlEU = new ControlRelationControl();
		BeanUtils.copyProperties(controlRelationControlEuVO, controlRelationControlEU);
		return controlRelationControlEU;
	}
}
