package com.tapplent.apputility.etl.controller;

import java.util.List;
import java.util.Map;

import com.tapplent.apputility.data.structure.ResponseData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tapplent.apputility.etl.service.EtlService;
import com.tapplent.apputility.layout.structure.BuildDBRequest;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.util.DBUploadErrorLog;

@Controller
@RequestMapping("tapp/etl/v1")
public class EtlController {
	@SuppressWarnings("unused")
	private static final Logger LOG = LogFactory.getLogger(EtlController.class);
	EtlService etlService;

	@RequestMapping(value={"/etl/t/{tenantId}/u/{personId}/e/h/{headerId}","/etl/t/{tenantId}/u/{personId}/e/all"}, method=RequestMethod.POST)
	public ResponseData uploadEtlDataByHeader(@PathVariable String tenantId, @PathVariable Map<String, String> pathVariables){
		ResponseData responseData = null;
		if(pathVariables.containsKey("headerId")){
			String headerId = pathVariables.get("headerId");
			responseData = etlService.uploadEtlDataByHeader(headerId);
		}
		else responseData = etlService.uploadEtlDataForAllHeaders();
		return responseData;
	}
	public EtlService getEtlService() {
		return etlService;
	}
	public void setEtlService(EtlService etlService) {
		this.etlService = etlService;
	}
	@RequestMapping(value="buildDatabase/t/{tenantId}/u/{personId}/e/",method=RequestMethod.POST)
	@ResponseBody
	List<String> buildLayout(@RequestHeader("access_token") String token,

							 @PathVariable String tenantId,
							 @PathVariable String personId,
							 @RequestParam("personName") String personName,
							 @RequestParam("timeZone") String timeZone,
							 @RequestBody BuildDBRequest request){
		etlService.buildLayout(request);
		return DBUploadErrorLog.errorList;
	}
	@RequestMapping(value="buildDatabaseExt/t/{tenantId}/u/{personId}/e/",method=RequestMethod.POST)
	@ResponseBody
	List<String> buildDBExt(@RequestHeader("access_token") String token,
							 @PathVariable String tenantId,
							 @PathVariable String personId,
							 @RequestParam("personName") String personName,
							 @RequestParam("timeZone") String timeZone,
							 @RequestBody BuildDBRequest request){
		etlService.addIndexesToDatabaseTables();
		etlService.addIndexesToAccessManagerTables();
		etlService.updateHierarchyTables();
		etlService.updateHierarchyPersonTable();
		etlService.updateRowConditionDefaultValues();
		etlService.updatePersonJobRelationShipSubjectUser();
		return DBUploadErrorLog.errorList;
	}
	@RequestMapping(value="accessControl/t/{tenantId}/u/{personId}/e/",method=RequestMethod.POST)
	@ResponseBody
	List<String> populateAccessControl(@RequestHeader("access_token") String token,

							 @PathVariable String tenantId,
							 @PathVariable String personId,
							 @RequestParam("personName") String personName,
							 @RequestParam("timeZone") String timeZone,
							 @RequestBody BuildDBRequest request){
		etlService.insertAccessControlDefaultValues();
		return DBUploadErrorLog.errorList;
	}
}