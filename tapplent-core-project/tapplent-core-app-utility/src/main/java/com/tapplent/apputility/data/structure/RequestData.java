
package com.tapplent.apputility.data.structure;

import java.util.List;

public class RequestData {
	private List<String> controlPEHierarchyList;
	private List<UnitRequestData> requestDataList;
	private String actionCodeFkId;
	private String primaryLocale;
	private String employeeCodeGenKey;
	private String usernameGenKey;

	private String previousActionCodeFkId;

	public String getPreviousActionCodeFkId() {
		return previousActionCodeFkId;
	}

	public void setPreviousActionCodeFkId(String previousActionCodeFkId) {
		this.previousActionCodeFkId = previousActionCodeFkId;
	}

	public List<String> getControlPEHierarchyList() {
		return controlPEHierarchyList;
	}

	public void setControlPEHierarchyList(List<String> controlPEHierarchyList) {
		this.controlPEHierarchyList = controlPEHierarchyList;
	}

	public String getPrimaryLocale() {
		return primaryLocale;
	}
	public void setPrimaryLocale(String primaryLocale) {
		this.primaryLocale = primaryLocale;
	}
	public List<UnitRequestData> getRequestDataList() {
		return requestDataList;
	}
	public void setRequestDataList(List<UnitRequestData> requestDataList) {
		this.requestDataList = requestDataList;
	}
	public String getActionCodeFkId() {
		return actionCodeFkId;
	}
	public void setActionCodeFkId(String actionCodeFkId) {
		this.actionCodeFkId = actionCodeFkId;
	}

	public String getEmployeeCodeGenKey() {
		return employeeCodeGenKey;
	}

	public void setEmployeeCodeGenKey(String employeeCodeGenKey) {
		this.employeeCodeGenKey = employeeCodeGenKey;
	}

	public String getUsernameGenKey() {
		return usernameGenKey;
	}

	public void setUsernameGenKey(String usernameGenKey) {
		this.usernameGenKey = usernameGenKey;
	}
}
