package com.tapplent.apputility.elasticsearch.controller;

import com.tapplent.apputility.elasticsearch.service.ElasticSearchService;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.suggest.Suggest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

/**
 * Created by tapplent on 08/05/17.
 */
@Controller
@RequestMapping("tapp/gsearch/v1")
public class ElasticsearchController {
    private static final Logger LOG = LogFactory.getLogger(ElasticsearchController.class);
    private ElasticSearchService elasticSearchService;

    @RequestMapping(value={"/perIndex/t/{tenantId}/u/{personId}/e/"}, method= RequestMethod.GET)
    public void populatePersonIndex(@RequestHeader("access_token") String token,
            @PathVariable String tenantId){
        try {
            elasticSearchService.populateIndexes();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value={"/gsearch/t/{tenantId}/u/{personId}/e/"}, method= RequestMethod.GET)
    @ResponseBody
    public Object gSearch(@RequestHeader("access_token") String token,
                           @PathVariable String tenantId,
                           @RequestParam("q") String query){
        try {
            return elasticSearchService.getGlobalSearchResult(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ElasticSearchService getElasticSearchService() {
        return elasticSearchService;
    }

    public void setElasticSearchService(ElasticSearchService elasticSearchService) {
        this.elasticSearchService = elasticSearchService;
    }
}
