package com.tapplent.apputility.layout.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.ActionRepresetationLaunchTrxnVO;

public class ActionRepresetationLaunchTrxn {
	@JsonIgnore
	String arLaunchId;
	String baseTemplateId;	
	String actionRepresentationCode;
	JsonNode deviceIndependentBlob;
	JsonNode deviceDependentBlob;
	
	public String getArLaunchId() {
		return arLaunchId;
	}
	public void setArLaunchId(String arLaunchId) {
		this.arLaunchId = arLaunchId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getActionRepresentationCode() {
		return actionRepresentationCode;
	}
	public void setActionRepresentationCode(String actionRepresentationCode) {
		this.actionRepresentationCode = actionRepresentationCode;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public static ActionRepresetationLaunchTrxn fromVO(ActionRepresetationLaunchTrxnVO actionRepresetationLaunchTrxnVO){
		ActionRepresetationLaunchTrxn actionRepresetationLaunchTrxn = new ActionRepresetationLaunchTrxn();
		BeanUtils.copyProperties(actionRepresetationLaunchTrxnVO, actionRepresetationLaunchTrxn);
		return actionRepresetationLaunchTrxn;
	}
}
