package com.tapplent.apputility.analytics.service;

import com.tapplent.apputility.analytics.structure.*;
import com.tapplent.apputility.data.structure.RelationResponse;

/**
 * Created by tapplent on 05/12/16.
 */
public interface AnalyticsService {

    AnalyticsMetricResponse getAnalyticsMetricsData(String metricId, String targetScreen, String targetSection, String deviceType);

    AnalyticsMetricDetailsResponse getDrillDownAnalyticMetricDetailsData(String metricId, String xAxisParam, String subXAxisParam);

    AnalyticsMetricResponse getAnalyticsMetricsAnalysisData(String metricId, String xAxisParam, String subXAxisParam);

    RelationResponse getAnalyticsMetricsTableData(String parentAnalysisMetricDetailId);

    AnalyticsAxisOptionsResponse getAxisData(String metricCategoryId, String metricCatMenuType, String axisAndMetricMapPKID);

    AnalyticsFiltersAndGraphTypeOptionsResponse getGraphTypeAndFilters(String metricAxisMapID);

    GraphResponse getGraph(GraphRequest graphRequest);
}
