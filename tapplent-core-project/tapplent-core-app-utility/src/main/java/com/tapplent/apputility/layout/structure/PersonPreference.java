package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.common.util.AttachmentObject;
import com.tapplent.platformutility.layout.valueObject.PersonPreferenceVO;
import org.springframework.beans.BeanUtils;

import java.net.URL;
import java.util.List;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tapplent on 15/11/16.
 */
public class PersonPreference {
    private String versionId;
    private String personPreferencesPkId;
    private String personFkId;
    private String personName;
    private String dateFormatCodeFkId;
    private String timeFormatCodeFkId;
    private String currencyCodeFkId;
    private String timezoneCodeFkId;
    private String localeCodeFkId;
    private String orgDefaultLocaleCodeFkId;
    private String globalDefalutLocaleCode;
    private boolean isShowTicker;
    private String socialStatus;
    private String moodMessage;
    private URL backGroundImage;
    private URL conversationBackGroundImage;
    private URL introductionVideo;
    private URL namePronounciationAudio;
    private String namePronounciation;
    private URL personPhoto;
    private String effectiveDateTime;
    private String lastModifiedDateTime;
    private String utcOffset;

    List<PersonTags> personTags;
    private Map<String, WishTemplate> wishMessage = new HashMap<>();


/*
To be added
IS_ENABLE_EVERNOTE_SYNC
IS_ENABLE_ONE_NOTE_SYNC
IS_ENABLE_GOOGLE_TASK_SYNC
IS_ENABLEGOOGLECALENDARSYNC
IS_ENABLE_FACEBOOK
IS_ENABLE_TWITTER
IS_ENABLE_LINKEDIN
IS_ENABLE_GOOGLE_PLUS
IS_ENABLE_YOUTUBE
IS_ENABLE_NOTIFICATION_VIA_SKYPE
IS_ENABLE_NOTIFICATION_VIA_TELEGRAM
 */

    public String getPersonPreferencesPkId() {
        return personPreferencesPkId;
    }

    public void setPersonPreferencesPkId(String personPreferencesPkId) {
        this.personPreferencesPkId = personPreferencesPkId;
    }

    public String getPersonFkId() {
        return personFkId;
    }

    public void setPersonFkId(String personFkId) {
        this.personFkId = personFkId;
    }

    public String getDateFormatCodeFkId() {
        return dateFormatCodeFkId;
    }

    public void setDateFormatCodeFkId(String dateFormatCodeFkId) {
        this.dateFormatCodeFkId = dateFormatCodeFkId;
    }

    public String getTimeFormatCodeFkId() {
        return timeFormatCodeFkId;
    }

    public void setTimeFormatCodeFkId(String timeFormatCodeFkId) {
        this.timeFormatCodeFkId = timeFormatCodeFkId;
    }

    public String getCurrencyCodeFkId() {
        return currencyCodeFkId;
    }

    public void setCurrencyCodeFkId(String currencyCodeFkId) {
        this.currencyCodeFkId = currencyCodeFkId;
    }

    public String getTimezoneCodeFkId() {
        return timezoneCodeFkId;
    }

    public void setTimezoneCodeFkId(String timezoneCodeFkId) {
        this.timezoneCodeFkId = timezoneCodeFkId;
    }

    public String getLocaleCodeFkId() {
        return localeCodeFkId;
    }

    public void setLocaleCodeFkId(String localeCodeFkId) {
        this.localeCodeFkId = localeCodeFkId;
    }

    public boolean isShowTicker() {
        return isShowTicker;
    }

    public void setShowTicker(boolean showTicker) {
        isShowTicker = showTicker;
    }

    public String getSocialStatus() {
        return socialStatus;
    }

    public void setSocialStatus(String socialStatus) {
        this.socialStatus = socialStatus;
    }

    public String getMoodMessage() {
        return moodMessage;
    }

    public void setMoodMessage(String moodMessage) {
        this.moodMessage = moodMessage;
    }

    public URL getBackGroundImage() {
        return backGroundImage;
    }

    public void setBackGroundImage(URL backGroundImage) {
        this.backGroundImage = backGroundImage;
    }

    public URL getConversationBackGroundImage() {
        return conversationBackGroundImage;
    }

    public void setConversationBackGroundImage(URL conversationBackGroundImage) {
        this.conversationBackGroundImage = conversationBackGroundImage;
    }

    public URL getIntroductionVideo() {
        return introductionVideo;
    }

    public void setIntroductionVideo(URL introductionVideo) {
        this.introductionVideo = introductionVideo;
    }

    public URL getNamePronounciationAudio() {
        return namePronounciationAudio;
    }

    public void setNamePronounciationAudio(URL namePronounciationAudio) {
        this.namePronounciationAudio = namePronounciationAudio;
    }

    public String getNamePronounciation() {
        return namePronounciation;
    }

    public void setNamePronounciation(String namePronounciation) {
        this.namePronounciation = namePronounciation;
    }

    public URL getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(URL personPhoto) {
        this.personPhoto = personPhoto;
    }

    public String getOrgDefaultLocaleCodeFkId() {
        return orgDefaultLocaleCodeFkId;
    }

    public void setOrgDefaultLocaleCodeFkId(String orgDefaultLocaleCodeFkId) {
        this.orgDefaultLocaleCodeFkId = orgDefaultLocaleCodeFkId;

    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getEffectiveDateTime() {
        return effectiveDateTime;
    }

    public void setEffectiveDateTime(String effectiveDateTime) {
        this.effectiveDateTime = effectiveDateTime;
    }

    public String getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(String lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }

    public List<PersonTags> getPersonTags() {
        return personTags;
    }

    public void setPersonTags(List<PersonTags> personTags) {
        this.personTags = personTags;
    }
    public Map<String, WishTemplate> getWishMessage() {
        return wishMessage;
    }

    public void setWishMessage(Map<String, WishTemplate> wishMessage) {
        this.wishMessage = wishMessage;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getGlobalDefalutLocaleCode() {
        return globalDefalutLocaleCode;
    }

    public void setGlobalDefalutLocaleCode(String globalDefalutLocaleCode) {
        this.globalDefalutLocaleCode = globalDefalutLocaleCode;
    }

    public String getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(String utcOffset) {
        this.utcOffset = utcOffset;
    }

    public static PersonPreference fromVO(PersonPreferenceVO personPreferenceVO){
        PersonPreference personPreference = new PersonPreference();
        BeanUtils.copyProperties(personPreferenceVO, personPreference);
        return personPreference;
    }
}
