package com.tapplent.apputility.cache.service;

public interface CacheService {
    void clearCache(String cacheName);
}
