package com.tapplent.apputility.data.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 26/12/16.
 */
public class RelationResponse {
    private Map<String, DoaMeta> labelToAliasMap = new HashMap<>();
    private List<RelationResponseInternal> recordSet = new ArrayList<>();

    public List<RelationResponseInternal> getRecordSet() {
        return recordSet;
    }

    public void setRecordSet(List<RelationResponseInternal> recordSet) {
        this.recordSet = recordSet;
    }

    public Map<String, DoaMeta> getLabelToAliasMap() {
        return labelToAliasMap;
    }

    public void setLabelToAliasMap(Map<String, DoaMeta> labelToAliasMap) {
        this.labelToAliasMap = labelToAliasMap;
    }
}
