package com.tapplent.apputility.data.service;

import com.tapplent.apputility.analytics.structure.AnalyticsMetricRequest;
import com.tapplent.apputility.data.controller.FeaturedResponse;
import com.tapplent.apputility.data.structure.ColleaguesAtWorkResponse;
import com.tapplent.apputility.data.structure.GenericQueryRequest;
import com.tapplent.apputility.data.structure.ScreenRequest;
import com.tapplent.apputility.data.structure.UnitResponseData;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by tapplent on 27/04/17.
 *
 */
public interface DataRequestService {
    UnitResponseData getScreen(ScreenRequest screen) throws IOException, SQLException;
    UnitResponseData getQueryResult(GenericQueryRequest query) throws IOException, SQLException;

    List<ColleaguesAtWorkResponse> getColleaguesAtWork(String contextID);

    FeaturedResponse getFeaturingPersonDetail(String contextID, String featuredTableName);

    UnitResponseData getAnalyticsMetricDetailsData(AnalyticsMetricRequest analyticsMetricRequest);

    void populateReportData(String groupID, String timeZone) throws SQLException, IOException;
}
