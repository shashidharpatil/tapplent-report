package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.common.util.AttachmentObject;
import com.tapplent.platformutility.layout.valueObject.AppHomePageSecnContentVO;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import org.springframework.beans.BeanUtils;

import java.net.URL;

/**
 * Created by tapplent on 23/01/17.
 */
public class AppHomePageSecnContent {
    private String appHomepageSecnContentPkId;
    private String appHomePageSecnFkId;
    private String controlContentTypeCodeFkId;
    private int controlSeqNumPosInt;
    private String controlValueG11nBigTxt;
    private AttachmentObject controlValueURL;
    private IconVO controlValueIcon;

    public String getAppHomepageSecnContentPkId() {
        return appHomepageSecnContentPkId;
    }

    public void setAppHomepageSecnContentPkId(String appHomepageSecnContentPkId) {
        this.appHomepageSecnContentPkId = appHomepageSecnContentPkId;
    }

    public String getAppHomePageSecnFkId() {
        return appHomePageSecnFkId;
    }

    public void setAppHomePageSecnFkId(String appHomePageSecnFkId) {
        this.appHomePageSecnFkId = appHomePageSecnFkId;
    }

    public String getControlContentTypeCodeFkId() {
        return controlContentTypeCodeFkId;
    }

    public void setControlContentTypeCodeFkId(String controlContentTypeCodeFkId) {
        this.controlContentTypeCodeFkId = controlContentTypeCodeFkId;
    }

    public int getControlSeqNumPosInt() {
        return controlSeqNumPosInt;
    }

    public void setControlSeqNumPosInt(int controlSeqNumPosInt) {
        this.controlSeqNumPosInt = controlSeqNumPosInt;
    }

    public String getControlValueG11nBigTxt() {
        return controlValueG11nBigTxt;
    }

    public void setControlValueG11nBigTxt(String controlValueG11nBigTxt) {
        this.controlValueG11nBigTxt = controlValueG11nBigTxt;
    }

    public AttachmentObject getControlValueURL() {
        return controlValueURL;
    }

    public void setControlValueURL(AttachmentObject controlValueURL) {
        this.controlValueURL = controlValueURL;
    }

    public IconVO getControlValueIcon() {
        return controlValueIcon;
    }

    public void setControlValueIcon(IconVO controlValueIcon) {
        this.controlValueIcon = controlValueIcon;
    }

    public static AppHomePageSecnContent fromVO(AppHomePageSecnContentVO appHomePageSecnContentVO){
        AppHomePageSecnContent appHomePageSecnContent = new AppHomePageSecnContent();
        BeanUtils.copyProperties(appHomePageSecnContentVO, appHomePageSecnContent);
        return appHomePageSecnContent;
    }
}
