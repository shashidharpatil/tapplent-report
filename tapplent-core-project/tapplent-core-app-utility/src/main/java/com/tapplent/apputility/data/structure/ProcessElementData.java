package com.tapplent.apputility.data.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.search.builder.SqlQuery;

public class ProcessElementData {
	private String baseTemplateId;
	private String mtPEAlias;
	private String mtPE;
	private int recordsExist;
	private int recordsReturned;
	private SqlQuery sqlQuery;
	private Map<String, AggregateData> aggregateResult;
	@JsonIgnore
	private Map<String, List<Record>> functionalPrimaryKeyToRecordMap;
	@JsonIgnore
	private Map<String, Record> databasePrimaryKeyToRecordMap;
	@JsonIgnore
	private List<Record> recordSetInternal;
	private Object recordSet;
	private Map<String, AttributeData> mtPEPropertyCntrl = new HashMap<>();
	private Map<String, List<RelationResponseInternal>> ctrlIdToGridData = new HashMap<>();
	public ProcessElementData(){
		this.recordSetInternal = new ArrayList<>();
		this.aggregateResult = new HashMap<>();
		this.functionalPrimaryKeyToRecordMap = new HashMap<>();
		this.databasePrimaryKeyToRecordMap = new HashMap<>();
	}
	public Map<String, Record> getDatabasePrimaryKeyToRecordMap() {
		return databasePrimaryKeyToRecordMap;
	}
	public void setDatabasePrimaryKeyToRecordMap(Map<String, Record> databasePrimaryKeyToRecordMap) {
		this.databasePrimaryKeyToRecordMap = databasePrimaryKeyToRecordMap;
	}
	public Map<String, List<Record>> getFunctionalPrimaryKeyToRecordMap() {
		return functionalPrimaryKeyToRecordMap;
	}
	public void setFunctionalPrimaryKeyToRecordMap(Map<String, List<Record>> functionalPrimaryKeyToRecordMap) {
		this.functionalPrimaryKeyToRecordMap = functionalPrimaryKeyToRecordMap;
	}

	public List<Record> getRecordSetInternal() {
		return recordSetInternal;
	}

	public void setRecordSetInternal(List<Record> recordSetInternal) {
		this.recordSetInternal = recordSetInternal;
	}

	public int getRecordsExist() {
		return recordsExist;
	}
	public void setRecordsExist(int recordsExist) {
		this.recordsExist = recordsExist;
	}
	public int getRecordsReturned() {
		return recordsReturned;
	}
	public void setRecordsReturned(int recordsReturned) {
		this.recordsReturned = recordsReturned;
	}
	public Map<String, AggregateData> getAggregateResult() {
		return aggregateResult;
	}
	public void setAggregateResult(Map<String, AggregateData> aggregateResult) {
		this.aggregateResult = aggregateResult;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPE() {
		return mtPE;
	}
	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}

	public Map<String, AttributeData> getMtPEPropertyCntrl() {
		return mtPEPropertyCntrl;
	}

	public void setMtPEPropertyCntrl(Map<String, AttributeData> mtPEPropertyCntrl) {
		this.mtPEPropertyCntrl = mtPEPropertyCntrl;
	}

	public Map<String, List<RelationResponseInternal>> getCtrlIdToGridData() {
		return ctrlIdToGridData;
	}

	public void setCtrlIdToGridData(Map<String, List<RelationResponseInternal>> ctrlIdToGridData) {
		this.ctrlIdToGridData = ctrlIdToGridData;
	}

	public Object getRecordSet() {
		return recordSet;
	}

	public void setRecordSet(Object recordSet) {
		this.recordSet = recordSet;
	}

	public String getMtPEAlias() {
		return mtPEAlias;
	}

	public void setMtPEAlias(String mtPEAlias) {
		this.mtPEAlias = mtPEAlias;
	}

	public SqlQuery getSqlQuery() {
		return sqlQuery;
	}

	public void setSqlQuery(SqlQuery sqlQuery) {
		this.sqlQuery = sqlQuery;
	}
}
