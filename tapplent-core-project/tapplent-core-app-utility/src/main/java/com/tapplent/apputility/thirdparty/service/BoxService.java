package com.tapplent.apputility.thirdparty.service;

import java.io.IOException;

import org.apache.commons.cli.ParseException;

import com.amazonaws.util.json.JSONException;

public interface BoxService {
	public String uploadTapplentAttachmentToBox(String attachmentId,String personId) throws org.json.JSONException, IOException, ParseException, JSONException;
}
