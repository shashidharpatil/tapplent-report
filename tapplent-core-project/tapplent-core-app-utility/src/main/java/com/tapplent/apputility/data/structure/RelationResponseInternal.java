package com.tapplent.apputility.data.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tapplent on 26/12/16.
 */
public class RelationResponseInternal {
    private AttributeData pk;
    Map<String, AttributeData> data;
    public RelationResponseInternal(){
        this.data = new HashMap<>();
    }
    public AttributeData getPk() {
        return pk;
    }

    public void setPk(AttributeData pk) {
        this.pk = pk;
    }

    public Map<String, AttributeData> getData() {
        return data;
    }

    public void setData(Map<String, AttributeData> data) {
        this.data = data;
    }
}
