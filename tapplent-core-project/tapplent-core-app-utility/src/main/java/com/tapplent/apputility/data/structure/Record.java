package com.tapplent.apputility.data.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.hierarchy.structure.Relation;
import com.tapplent.platformutility.metadata.structure.DataActions;

public class Record {
	private Map<String, AttributeData> attributeMap;
	private List<DataActions> dataActionsApplicability;
	private List<ProcessElementData> childrenPEData;
	private List<Relation> relations;
	private List<String> subjectUserGroups;
	@JsonIgnore
	private Record parentRecord;
	public Record(){
		this.attributeMap = new HashMap<String, AttributeData>();
		this.childrenPEData = new ArrayList<>();
		this.dataActionsApplicability = new ArrayList<>();
	}
	public Map<String, AttributeData> getAttributeMap() {
		return attributeMap;
	}
	public void setAttributeMap(Map<String, AttributeData> attributeMap) {
		this.attributeMap = attributeMap;
	}
	public List<ProcessElementData> getChildrenPEData() {
		return childrenPEData;
	}
	public void setChildrenPEData(List<ProcessElementData> childrenPEData) {
		this.childrenPEData = childrenPEData;
	}
	public List<DataActions> getDataActionsApplicability() {
		return dataActionsApplicability;
	}
	public void setDataActionsApplicability(List<DataActions> dataActionsApplicability) {
		this.dataActionsApplicability = dataActionsApplicability;
	}

	public List<Relation> getRelations() {
		return relations;
	}

	public void setRelations(List<Relation> relations) {
		this.relations = relations;
	}

	public Record getParentRecord() {
		return parentRecord;
	}

	public void setParentRecord(Record parentRecord) {
		this.parentRecord = parentRecord;
	}

	public List<String> getSubjectUserGroups() {
		return subjectUserGroups;
	}

	public void setSubjectUserGroups(List<String> subjectUserGroups) {
		this.subjectUserGroups = subjectUserGroups;
	}
}
