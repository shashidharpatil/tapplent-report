package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.ControlEventVO;
import com.tapplent.platformutility.layout.valueObject.PropertyControlEuVO;

public class PropertyControlEU {
	private String propertyControlId;
	@JsonIgnore
	private String propertyControlTransactionFkId;
	private String baseTemplateId; 
	private String mtPEId;
	@JsonIgnore
	private String propertyControlInstanceFkId; 
	private String canvasId;
	private String propertyControlCode;
	private String controlTypeCode;
	private JsonNode deviceIndependentBlob;
	private JsonNode deviceDependentBlob;
	public String getPropertyControlId() {
		return propertyControlId;
	}

	public void setPropertyControlId(String propertyControlId) {
		this.propertyControlId = propertyControlId;
	}

	public String getPropertyControlTransactionFkId() {
		return propertyControlTransactionFkId;
	}

	public void setPropertyControlTransactionFkId(String propertyControlTransactionFkId) {
		this.propertyControlTransactionFkId = propertyControlTransactionFkId;
	}

	public String getBaseTemplateId() {
		return baseTemplateId;
	}

	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getMtPEId() {
		return mtPEId;
	}

	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}

	public String getPropertyControlInstanceFkId() {
		return propertyControlInstanceFkId;
	}

	public void setPropertyControlInstanceFkId(String propertyControlInstanceFkId) {
		this.propertyControlInstanceFkId = propertyControlInstanceFkId;
	}

	public String getCanvasId() {
		return canvasId;
	}

	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}

	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}

	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}

	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}

	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	
	public String getPropertyControlCode() {
		return propertyControlCode;
	}

	public void setPropertyControlCode(String propertyControlCode) {
		this.propertyControlCode = propertyControlCode;
	}

	public String getControlTypeCode() {
		return controlTypeCode;
	}

	public void setControlTypeCode(String controlTypeCode) {
		this.controlTypeCode = controlTypeCode;
	}

	public static PropertyControlEU fromVO(PropertyControlEuVO propertyControlEuVO){
		PropertyControlEU propertyControlEU = new PropertyControlEU();
		BeanUtils.copyProperties(propertyControlEuVO, propertyControlEU);
		return propertyControlEU;
	}
}
