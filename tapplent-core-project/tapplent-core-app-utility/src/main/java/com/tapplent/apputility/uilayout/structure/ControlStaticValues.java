package com.tapplent.apputility.uilayout.structure;

import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.uilayout.valueobject.ControlStaticValuesVO;
import org.springframework.beans.BeanUtils;

import java.net.URL;

/**
 * Created by tapplent on 25/04/17.
 */
public class ControlStaticValues {

    private String staticPkId;
    private String staticText;
    private IconVO staticIcon;
    private URL staticImage;
    private int valueItemNumber;

    public String getStaticPkId() {
        return staticPkId;
    }

    public void setStaticPkId(String staticPkId) {
        this.staticPkId = staticPkId;
    }

    public String getStaticText() {
        return staticText;
    }

    public void setStaticText(String staticText) {
        this.staticText = staticText;
    }

    public IconVO getStaticIcon() {
        return staticIcon;
    }

    public void setStaticIcon(IconVO staticIcon) {
        this.staticIcon = staticIcon;
    }

    public int getValueItemNumber() {
        return valueItemNumber;
    }

    public void setValueItemNumber(int valueItemNumber) {
        this.valueItemNumber = valueItemNumber;
    }

    public URL getStaticImage() {
        return staticImage;
    }

    public void setStaticImage(URL staticImage) {
        this.staticImage = staticImage;
    }

    public static ControlStaticValues fromVO(ControlStaticValuesVO controlStaticValuesVO){
        ControlStaticValues controlStaticValues = new ControlStaticValues();
        BeanUtils.copyProperties(controlStaticValuesVO, controlStaticValues);
        return controlStaticValues;
    }
}
