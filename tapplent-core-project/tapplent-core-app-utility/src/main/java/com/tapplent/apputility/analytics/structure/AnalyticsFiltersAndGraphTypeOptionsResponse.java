package com.tapplent.apputility.analytics.structure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 23/08/17.
 */
public class AnalyticsFiltersAndGraphTypeOptionsResponse {
    private List<GraphType> graphTypes = new ArrayList<>();

    public List<GraphType> getGraphTypes() {
        return graphTypes;
    }

    public void setGraphTypes(List<GraphType> graphTypes) {
        this.graphTypes = graphTypes;
    }
}
