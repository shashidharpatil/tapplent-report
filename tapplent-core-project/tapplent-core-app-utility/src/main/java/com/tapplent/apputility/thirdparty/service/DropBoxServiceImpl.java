package com.tapplent.apputility.thirdparty.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.amazonaws.util.json.JSONException;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.thirdPartyApp.gmail.EverNoteDAO;
import com.tapplent.platformutility.thirdPartyApp.gmail.ThirdPartyUtil;

public class DropBoxServiceImpl implements DropBoxService {
	 private EverNoteDAO evernoteDAO;
		
		public EverNoteDAO getEvernoteDAO() {
			return evernoteDAO;
		}
		public void setEvernoteDAO(EverNoteDAO evernoteDAO) {
			this.evernoteDAO = evernoteDAO;
		}
	@Override	
	public String uploadTapplentAttachmentToDropBox(String attachmentId,String personId) throws org.json.JSONException, IOException, ParseException, JSONException{
		String status = "";
		
			Map<String,Object> attachmentMap =ThirdPartyUtil.getAttchment(attachmentId);
			 String mimeType = attachmentMap.get("ContentType").toString();
			 InputStream is = (InputStream) attachmentMap.get("InputStream");
			 String accessToken = "Am0oiPy44IAAAAAAAAAACEBZ9Tzb2HupeMOsevX5YAhbUeeiDpULn545MxZcko8F";
//			 Map<String,String> tokens = evernoteDAO.getToken("BOX",personId);
//				String accessToken = tokens.get("accessToken");
//				String refreshToken = tokens.get("refreshToken");
//				boolean flag = ThirdPartyUtil.isTokenValid(accessToken);
//				if(flag==false){
//					accessToken = ThirdPartyUtil.getAccessToken(refreshToken);
//				}
				String attachmentName = "130001.mp3";
			 status = uploadToDropBox(is,mimeType,accessToken,attachmentName);
			 if(status.equals("failure"))
				 return status;
		
		
		return status;
	}
	public String uploadDropBoxAttachmentToS3(String fileUrl,String mimeType) throws MalformedURLException, IOException{
		InputStream is = new URL(fileUrl).openStream();
		String attachmentArtifact = Util.uploadAttachmentToS3(mimeType, is);
		return attachmentArtifact;
	}
	private String uploadToDropBox(InputStream is, String mimeType, String accessToken, String attachmentName) throws IOException {
		String path = "{\"path\": \"/"+attachmentName+"\",\"mode\": \"add\",\"autorename\": true,\"mute\": false}" ;
		HttpPost post = new HttpPost("https://content.dropboxapi.com/2/files/upload");
		post.addHeader("Authorization", "Bearer "+accessToken);
		post.addHeader("Content-Type", mimeType);
		post.addHeader("Dropbox-API-Arg",path);
		byte [] bytes = IOUtils.toByteArray(is);
     ByteArrayEntity requestEntity = new ByteArrayEntity(bytes); 
     post.setEntity(requestEntity);
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(post);
	    String status = String.valueOf(response.getStatusLine().getStatusCode());
	    String message = "failure";
	    if(status.matches("201 | 200"))
	    	message = "successfully uploaded to dropBox";
	    
		return message;
	}
}
