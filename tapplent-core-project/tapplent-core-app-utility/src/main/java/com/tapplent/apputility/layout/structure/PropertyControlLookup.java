package com.tapplent.apputility.layout.structure;

import java.util.Map;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.PropertyControlLookupVO;

public class PropertyControlLookup {
	private String propertyControlCode;
	@JsonIgnore
	private String propertyControlName;
	private String controlTypeCode;
	private String propertyControlIcon;
	private boolean isGlocalized;
	private boolean isBooleanData;
	private String sourceCategory;
	private String layoutKey;
	@JsonIgnore
	private String sourceDomainObject;
	@JsonIgnore
	private String sourceDOA;
	@JsonIgnore
	private String returnDOA;
	@JsonIgnore
	private String booleanTrueStateLayoutKey;
	@JsonIgnore
	private String booleanFalseStateLayoutKey;
	private Map<String, Object> lookupMap;
	public String getPropertyControlCode() {
		return propertyControlCode;
	}
	public void setPropertyControlCode(String propertyControlCode) {
		this.propertyControlCode = propertyControlCode;
	}

	public String getPropertyControlName() {
		return propertyControlName;
	}

	public void setPropertyControlName(String propertyControlName) {
		this.propertyControlName = propertyControlName;
	}

	public String getControlTypeCode() {
		return controlTypeCode;
	}
	public void setControlTypeCode(String controlTypeCode) {
		this.controlTypeCode = controlTypeCode;
	}
	public String getPropertyControlIcon() {
		return propertyControlIcon;
	}
	public void setPropertyControlIcon(String propertyControlIcon) {
		this.propertyControlIcon = propertyControlIcon;
	}
	public String getSourceDomainObject() {
		return sourceDomainObject;
	}
	public void setSourceDomainObject(String sourceDomainObject) {
		this.sourceDomainObject = sourceDomainObject;
	}
	public String getSourceDOA() {
		return sourceDOA;
	}
	public void setSourceDOA(String sourceDOA) {
		this.sourceDOA = sourceDOA;
	}
	public String getReturnDOA() {
		return returnDOA;
	}
	public void setReturnDOA(String returnDOA) {
		this.returnDOA = returnDOA;
	}
	public boolean isGlocalized() {
		return isGlocalized;
	}
	public void setGlocalized(boolean isGlocalized) {
		this.isGlocalized = isGlocalized;
	}
	public String getSourceCategory() {
		return sourceCategory;
	}
	public void setSourceCategory(String sourceCategory) {
		this.sourceCategory = sourceCategory;
	}
	public String getLayoutKey() {
		return layoutKey;
	}
	public void setLayoutKey(String layoutKey) {
		this.layoutKey = layoutKey;
	}
	public Map<String, Object> getLookupMap() {
		return lookupMap;
	}
	public void setLookupMap(Map<String, Object> lookupMap) {
		this.lookupMap = lookupMap;
	}
	public boolean isBooleanData() {
		return isBooleanData;
	}
	public void setBooleanData(boolean isBooleanData) {
		this.isBooleanData = isBooleanData;
	}
	public String getBooleanTrueStateLayoutKey() {
		return booleanTrueStateLayoutKey;
	}
	public void setBooleanTrueStateLayoutKey(String booleanTrueStateLayoutKey) {
		this.booleanTrueStateLayoutKey = booleanTrueStateLayoutKey;
	}
	public String getBooleanFalseStateLayoutKey() {
		return booleanFalseStateLayoutKey;
	}
	public void setBooleanFalseStateLayoutKey(String booleanFalseStateLayoutKey) {
		this.booleanFalseStateLayoutKey = booleanFalseStateLayoutKey;
	}
	public static PropertyControlLookup fromVO(PropertyControlLookupVO propertyControlLookupVO){
		PropertyControlLookup propertyControlLookup = new PropertyControlLookup();
		BeanUtils.copyProperties(propertyControlLookupVO, propertyControlLookup);
		return propertyControlLookup;
	}
}
