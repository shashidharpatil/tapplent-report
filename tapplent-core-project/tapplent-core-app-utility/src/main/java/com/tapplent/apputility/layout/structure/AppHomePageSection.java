package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.layout.valueObject.AppHomePageSecnContentVO;
import com.tapplent.platformutility.layout.valueObject.AppHomePageSectionVO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 23/01/17.
 */
public class AppHomePageSection {

    private String appHomePageSecnPkId;
    private String appHomePageFkId;
    private String secnMasterCodeFkId;
    private String parentSectionMasterCodeFkId;
    private int sectionUiTypePosInt;
    private int themeSeqNumPosInt;
    private List<AppHomePageSecnContent> appHomePageSecnContentList = new ArrayList<>();
    public String getAppHomePageSecnPkId() {
        return appHomePageSecnPkId;
    }

    public void setAppHomePageSecnPkId(String appHomePageSecnPkId) {
        this.appHomePageSecnPkId = appHomePageSecnPkId;
    }

    public String getAppHomePageFkId() {
        return appHomePageFkId;
    }

    public void setAppHomePageFkId(String appHomePageFkId) {
        this.appHomePageFkId = appHomePageFkId;
    }

    public String getSecnMasterCodeFkId() {
        return secnMasterCodeFkId;
    }

    public void setSecnMasterCodeFkId(String secnMasterCodeFkId) {
        this.secnMasterCodeFkId = secnMasterCodeFkId;
    }

    public String getParentSectionMasterCodeFkId() {
        return parentSectionMasterCodeFkId;
    }

    public void setParentSectionMasterCodeFkId(String parentSectionMasterCodeFkId) {
        this.parentSectionMasterCodeFkId = parentSectionMasterCodeFkId;
    }

    public int getSectionUiTypePosInt() {
        return sectionUiTypePosInt;
    }

    public void setSectionUiTypePosInt(int sectionUiTypePosInt) {
        this.sectionUiTypePosInt = sectionUiTypePosInt;
    }

    public int getThemeSeqNumPosInt() {
        return themeSeqNumPosInt;
    }

    public void setThemeSeqNumPosInt(int themeSeqNumPosInt) {
        this.themeSeqNumPosInt = themeSeqNumPosInt;
    }

    public List<AppHomePageSecnContent> getAppHomePageSecnContentList() {
        return appHomePageSecnContentList;
    }

    public void setAppHomePageSecnContentList(List<AppHomePageSecnContent> appHomePageSecnContentList) {
        this.appHomePageSecnContentList = appHomePageSecnContentList;
    }

    public static AppHomePageSection fromVO(AppHomePageSectionVO appHomePageSectionVO){
        AppHomePageSection appHomePageSection = new AppHomePageSection();
        BeanUtils.copyProperties(appHomePageSectionVO, appHomePageSection);
        return appHomePageSection;
    }
}
