package com.tapplent.apputility.uilayout.structure;

import com.tapplent.apputility.layout.structure.LayoutPermissions;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.uilayout.valueobject.SectionActionInstanceVO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 25/04/17.
 */
public class SectionActionInstance {
    private String actionPkId;
    private String actionMasterCode;
    private String actionLabel;
    private IconVO actionIcon;
    private String mtPEAlias;
    private String actionGroupCode;
    private IconVO actionGroupIcon;
    private String actionGroupName;
    private String actionVisualMasterCode;
    private int seqNumber;
    private String controlPositionTxt;
    private String cnfMsgTxt;
    private String successMessage;
    private String failureMessage;
    private String themeSize;
    private String fontStyle;
    private String iconColour;
    private String fontCase;
    private String fontColour;
    private String backgroundColour;
    private String baseTemplateID;
    private boolean isActivityLogged;
    private List<ActionArgument> actionArguments = new ArrayList<>();
    private List<ActionTarget> targets = new ArrayList<>();;
    private List<LayoutPermissions> layoutPermissions = new ArrayList<>();

    public String getActionPkId() {
        return actionPkId;
    }

    public void setActionPkId(String actionPkId) {
        this.actionPkId = actionPkId;
    }

    public String getActionMasterCode() {
        return actionMasterCode;
    }

    public void setActionMasterCode(String actionMasterCode) {
        this.actionMasterCode = actionMasterCode;
    }

    public String getActionLabel() {
        return actionLabel;
    }

    public void setActionLabel(String actionLabel) {
        this.actionLabel = actionLabel;
    }

    public IconVO getActionIcon() {
        return actionIcon;
    }

    public void setActionIcon(IconVO actionIcon) {
        this.actionIcon = actionIcon;
    }

    public String getActionGroupCode() {
        return actionGroupCode;
    }

    public void setActionGroupCode(String actionGroupCode) {
        this.actionGroupCode = actionGroupCode;
    }

    public String getActionVisualMasterCode() {
        return actionVisualMasterCode;
    }

    public void setActionVisualMasterCode(String actionVisualMasterCode) {
        this.actionVisualMasterCode = actionVisualMasterCode;
    }

    public int getSeqNumber() {
        return seqNumber;
    }

    public void setSeqNumber(int seqNumber) {
        this.seqNumber = seqNumber;
    }

    public String getControlPositionTxt() {
        return controlPositionTxt;
    }

    public void setControlPositionTxt(String controlPositionTxt) {
        this.controlPositionTxt = controlPositionTxt;
    }

    public String getCnfMsgTxt() {
        return cnfMsgTxt;
    }

    public void setCnfMsgTxt(String cnfMsgTxt) {
        this.cnfMsgTxt = cnfMsgTxt;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    public String getThemeSize() {
        return themeSize;
    }

    public void setThemeSize(String themeSize) {
        this.themeSize = themeSize;
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    public String getIconColour() {
        return iconColour;
    }

    public void setIconColour(String iconColour) {
        this.iconColour = iconColour;
    }

    public String getFontCase() {
        return fontCase;
    }

    public void setFontCase(String fontCase) {
        this.fontCase = fontCase;
    }

    public String getFontColour() {
        return fontColour;
    }

    public void setFontColour(String fontColour) {
        this.fontColour = fontColour;
    }

    public String getBackgroundColour() {
        return backgroundColour;
    }

    public void setBackgroundColour(String backgroundColour) {
        this.backgroundColour = backgroundColour;
    }

    public List<ActionArgument> getActionArguments() {
        return actionArguments;
    }

    public void setActionArguments(List<ActionArgument> actionArguments) {
        this.actionArguments = actionArguments;
    }

    public List<ActionTarget> getTargets() {
        return targets;
    }

    public void setTargets(List<ActionTarget> targets) {
        this.targets = targets;
    }

    public IconVO getActionGroupIcon() {
        return actionGroupIcon;
    }

    public void setActionGroupIcon(IconVO actionGroupIcon) {
        this.actionGroupIcon = actionGroupIcon;
    }

    public String getActionGroupName() {
        return actionGroupName;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public void setActionGroupName(String actionGroupName) {
        this.actionGroupName = actionGroupName;
    }

    public boolean isActivityLogged() {
        return isActivityLogged;
    }

    public void setActivityLogged(boolean activityLogged) {
        isActivityLogged = activityLogged;
    }

    public List<LayoutPermissions> getLayoutPermissions() {
        return layoutPermissions;
    }

    public void setLayoutPermissions(List<LayoutPermissions> layoutPermissions) {
        this.layoutPermissions = layoutPermissions;
    }

    public String getBaseTemplateID() {
        return baseTemplateID;
    }

    public void setBaseTemplateID(String baseTemplateID) {
        this.baseTemplateID = baseTemplateID;
    }

    public static SectionActionInstance fromVO(SectionActionInstanceVO actionInstanceVO) {
        SectionActionInstance sectionActionInstance = new SectionActionInstance();
        BeanUtils.copyProperties(actionInstanceVO, sectionActionInstance);
        return sectionActionInstance;
    }
}
