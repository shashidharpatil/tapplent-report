package com.tapplent.apputility.analytics.controller;

import com.tapplent.apputility.analytics.structure.*;
import com.tapplent.apputility.analytics.service.AnalyticsService;
import com.tapplent.apputility.data.structure.RelationResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by tapplent on 09/12/16.
 */
@Controller
@RequestMapping("tapp/analytics/v1")
public class AnalyticsController {
    private AnalyticsService analyticsService;

    /**
     * The controller should be able to get all the data corresponding to the given analytics metric Id.
     * @param token
     * @param metricId
     * @return
     */
    @RequestMapping(value="analyticsMetric/t/{tenantId}/u/{personId}/e/", method= RequestMethod.GET)
    @ResponseBody
    AnalyticsMetricResponse getAnalyticsMetricsData(@RequestHeader("access_token") String token,
                                                    @RequestParam("metricId") String metricId,
                                                    @RequestParam(value = "targetScreen", required = false) String targetScreen,
                                                    @RequestParam(value = "targetSection", required = false) String targetSection,
                                                    @RequestParam(value = "deviceType", required = false) String deviceType){
        AnalyticsMetricResponse analyticsMetricResponse = analyticsService.getAnalyticsMetricsData(metricId, targetScreen, targetSection, deviceType);
        return analyticsMetricResponse;
    }
    /**
     * The controller should be able to get all the data corresponding to the given analytics metric Id.
     * @param token
     * @param parentAnalysisMetricDetailId
     * @return
     */
    @RequestMapping(value="analysis/t/{tenantId}/u/{personId}/e/", method= RequestMethod.GET)
    @ResponseBody
    AnalyticsMetricResponse getAnalyticsMetricsAnalysisData(@RequestHeader("access_token") String token,
                                                            @RequestParam("parMetrDtlId") String parentAnalysisMetricDetailId,
                                                            @RequestParam("xAxisParam") String xAxisParam,
                                                            @RequestParam(value = "subXAxisParam", required = false) String subXAxisParam){
        AnalyticsMetricResponse analyticsMetricResponse = analyticsService.getAnalyticsMetricsAnalysisData(parentAnalysisMetricDetailId, xAxisParam, subXAxisParam);
        return analyticsMetricResponse;
    }
    /**
     * The controller should be able to get all the data corresponding to the given analytics metric Id.
     * @param token
     * @param parentAnalysisMetricDetailId
     * @return
     */
    @RequestMapping(value="table/t/{tenantId}/u/{personId}/e/", method= RequestMethod.GET)
    @ResponseBody
    RelationResponse getAnalyticsMetricsTableData(@RequestHeader("access_token") String token,
                                                        @RequestParam("parMetrDtlId") String parentAnalysisMetricDetailId){
        RelationResponse analyticsTableResponse = analyticsService.getAnalyticsMetricsTableData(parentAnalysisMetricDetailId);
        return analyticsTableResponse;
    }
    /**
     * The controller should be able to get all the data corresponding to the given analytics metric details Id.
     * @param token
     * @param parentMetricDetailId
     * @return
     */
    @RequestMapping(value="drillDown/t/{tenantId}/u/{personId}/e/", method= RequestMethod.GET)
    @ResponseBody
    AnalyticsMetricDetailsResponse getAnalyticsMetricsDetailsData(@RequestHeader("access_token") String token,
                                                    @RequestParam("parMetrDtlId") String parentMetricDetailId,
                                                    @RequestParam("xAxisParam") String xAxisParam,
                                                    @RequestParam(value = "subXAxisParam", required = false) String subXAxisParam){
        AnalyticsMetricDetailsResponse analyticsMetricDetailsResponse = analyticsService.getDrillDownAnalyticMetricDetailsData(parentMetricDetailId, xAxisParam, subXAxisParam);
        return analyticsMetricDetailsResponse;
    }

    /**
     * The All new analytics APIs
     * This call will return all the applicable axises and graph types for the given metric ID. It will also give Calendar data.
     * @param token
     * @return
     */
    @RequestMapping(value="axis/t/{tenantId}/u/{personId}/e/", method= RequestMethod.GET)
    @ResponseBody
    AnalyticsAxisOptionsResponse getAxisData(@RequestHeader("access_token") String token,
                                             @RequestParam("metricIdCategory") String metricCategoryId,
                                             @RequestParam(value = "menuType") String metricCatMenuType,
                                             @RequestParam(value = "axisAndMetricMapPKID", required = false) String axisAndMetricMapPKID,
                                             @RequestParam(value = "deviceType", required = false) String deviceType){

        return analyticsService.getAxisData(metricCategoryId, metricCatMenuType, axisAndMetricMapPKID);
    }
    @RequestMapping(value="graphTypeFilters/t/{tenantId}/u/{personId}/e/", method= RequestMethod.GET)
    @ResponseBody
    AnalyticsFiltersAndGraphTypeOptionsResponse getGraphTypeAndFilters(@RequestHeader("access_token") String token,
                                                    @RequestParam("axisAndMetricMapPKID") String metricAxisMapID,
                                                    @RequestParam(value = "deviceType", required = false) String deviceType){

//        AnalyticsMetricResponse analyticsMetricResponse = analyticsService.getAnalyticsMetricsData(metricId, targetScreen, targetSection, deviceType);
        return analyticsService.getGraphTypeAndFilters(metricAxisMapID);
    }
    /**
     * The controller should be able to get all the data corresponding to the given analytics metric details Id.
     * @param token
     * @return
     */
    @RequestMapping(value="getGraph/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    GraphResponse getGraph(@RequestHeader("access_token") String token,
                                                                @RequestBody GraphRequest graphRequest){
        GraphResponse analyticsMetricDetailsResponse = analyticsService.getGraph(graphRequest);
        return analyticsMetricDetailsResponse;
    }



    public AnalyticsService getAnalyticsService() {
        return analyticsService;
    }

    public void setAnalyticsService(AnalyticsService analyticsService) {
        this.analyticsService = analyticsService;
    }
}
