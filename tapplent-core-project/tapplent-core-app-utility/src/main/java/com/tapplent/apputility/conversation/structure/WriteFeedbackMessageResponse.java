package com.tapplent.apputility.conversation.structure;

import java.util.List;

/**
 * Created by Tapplent on 6/15/17.
 */
public class WriteFeedbackMessageResponse {
    private List<FeedbackMessage> feedbackMessageList;

    public List<FeedbackMessage> getFeedbackMessageList() {
        return feedbackMessageList;
    }

    public void setFeedbackMessageList(List<FeedbackMessage> feedbackMessageList) {
        this.feedbackMessageList = feedbackMessageList;
    }
}
