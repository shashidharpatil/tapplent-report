package com.tapplent.apputility.analytics.structure;

import com.tapplent.platformutility.analytics.structure.TimePeriodOffsetVO;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 25/08/17.
 */
public class TimePeriodOffset {
    private String orgLocPeriodOffsetPkID;
    private String calendarPeriodType;
    private String calendarPeriodName;
    private IconVO calendarPeriodIcon;
    private int weekDaysOffset;
    private int monthOffset;


    public String getCalendarPeriodType() {
        return calendarPeriodType;
    }

    public void setCalendarPeriodType(String calendarPeriodType) {
        this.calendarPeriodType = calendarPeriodType;
    }

    public int getWeekDaysOffset() {
        return weekDaysOffset;
    }

    public void setWeekDaysOffset(int weekDaysOffset) {
        this.weekDaysOffset = weekDaysOffset;
    }

    public int getMonthOffset() {
        return monthOffset;
    }

    public void setMonthOffset(int monthOffset) {
        this.monthOffset = monthOffset;
    }

    public String getCalendarPeriodName() {
        return calendarPeriodName;
    }

    public void setCalendarPeriodName(String calendarPeriodName) {
        this.calendarPeriodName = calendarPeriodName;
    }

    public IconVO getCalendarPeriodIcon() {
        return calendarPeriodIcon;
    }

    public void setCalendarPeriodIcon(IconVO calendarPeriodIcon) {
        this.calendarPeriodIcon = calendarPeriodIcon;
    }

    public String getOrgLocPeriodOffsetPkID() {
        return orgLocPeriodOffsetPkID;
    }

    public void setOrgLocPeriodOffsetPkID(String orgLocPeriodOffsetPkID) {
        this.orgLocPeriodOffsetPkID = orgLocPeriodOffsetPkID;
    }

    public static TimePeriodOffset fromVO(TimePeriodOffsetVO timePeriodOffsetVO) {
        TimePeriodOffset timePeriodOffset = new TimePeriodOffset();
        BeanUtils.copyProperties(timePeriodOffsetVO, timePeriodOffset);
        return timePeriodOffset;
    }
}
