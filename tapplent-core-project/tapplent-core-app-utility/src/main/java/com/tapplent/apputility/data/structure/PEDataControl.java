package com.tapplent.apputility.data.structure;

/**
 * Created by tapplent on 28/04/17.
 */
public class PEDataControl {
    private String controlType;
    private String attributePathExpn;
    private boolean isAggregateControl;
    private String attributeFilterExpn;
    private String additionalAttributePathExpn;
    private boolean compressAttachment;
    private String statusColourAttrPathExpn;


    private String relnCtrlImgDispAttrPathExpn;
    private String relnCtrlFstLnDispAttrPathExpn;
    private String relnCtrlSndLnDispAttrPathExpn;
    private String relnCtrlRtnDispAttrPathExpn;

    private String defaultOnLoadSort;
    private String orderByValues;
    private boolean isGroupByControl;
    private int groupByControlSequence;
    private String groupByHavingFilterExpression;
}
