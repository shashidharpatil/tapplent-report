package com.tapplent.apputility.thirdparty.service;

import java.io.IOException;

import org.apache.commons.cli.ParseException;

import com.amazonaws.util.json.JSONException;

public interface DropBoxService {
	public String uploadTapplentAttachmentToDropBox(String attachmentId,String personId) throws org.json.JSONException, IOException, ParseException, JSONException;
}
