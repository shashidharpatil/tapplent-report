package com.tapplent.apputility.data.structure;

/**
 * Created by tapplent on 09/10/17.
 */
public class RelationControlOnDemandAttributes {
    private String mtPE;
    private String mtPEAlias;
    private String parentMTPEAlias;
    private String fkRelnWithParent;
    private String attributePathExpn;

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getParentMTPEAlias() {
        return parentMTPEAlias;
    }

    public void setParentMTPEAlias(String parentMTPEAlias) {
        this.parentMTPEAlias = parentMTPEAlias;
    }

    public String getFkRelnWithParent() {
        return fkRelnWithParent;
    }

    public void setFkRelnWithParent(String fkRelnWithParent) {
        this.fkRelnWithParent = fkRelnWithParent;
    }

    public String getAttributePathExpn() {
        return attributePathExpn;
    }

    public void setAttributePathExpn(String attributePathExpn) {
        this.attributePathExpn = attributePathExpn;
    }
}
