package com.tapplent.apputility.uilayout.structure;

import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.uilayout.valueobject.RatingScaleVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 01/10/17.
 */
public class RatingScale {
    private String ratingScalePkID;
    private String ratingName;
    private double minRatingLevel;
    private double maxRatingLevel;
    private double intervalRatingLevel;
    private IconVO ratingScaleIcon;

    public String getRatingScalePkID() {
        return ratingScalePkID;
    }

    public void setRatingScalePkID(String ratingScalePkID) {
        this.ratingScalePkID = ratingScalePkID;
    }

    public String getRatingName() {
        return ratingName;
    }

    public void setRatingName(String ratingName) {
        this.ratingName = ratingName;
    }

    public double getMinRatingLevel() {
        return minRatingLevel;
    }

    public void setMinRatingLevel(double minRatingLevel) {
        this.minRatingLevel = minRatingLevel;
    }

    public double getMaxRatingLevel() {
        return maxRatingLevel;
    }

    public void setMaxRatingLevel(double maxRatingLevel) {
        this.maxRatingLevel = maxRatingLevel;
    }

    public double getIntervalRatingLevel() {
        return intervalRatingLevel;
    }

    public void setIntervalRatingLevel(double intervalRatingLevel) {
        this.intervalRatingLevel = intervalRatingLevel;
    }

    public IconVO getRatingScaleIcon() {
        return ratingScaleIcon;
    }

    public void setRatingScaleIcon(IconVO ratingScaleIcon) {
        this.ratingScaleIcon = ratingScaleIcon;
    }

    public static RatingScale fromVO(RatingScaleVO ratingScaleVO) {
        RatingScale ratingScale = new RatingScale();
        BeanUtils.copyProperties(ratingScaleVO, ratingScale);
        return ratingScale;
    }
}
