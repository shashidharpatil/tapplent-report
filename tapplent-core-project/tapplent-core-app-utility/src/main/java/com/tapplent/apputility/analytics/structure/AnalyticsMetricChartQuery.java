package com.tapplent.apputility.analytics.structure;

import com.tapplent.platformutility.analytics.structure.AnalyticsMetricChartQueryVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 23/08/17.
 */
public class AnalyticsMetricChartQuery {
    /*
    AnalyticsMetricChartID	MetaProcessElementCode	MetaProcessElementCodeAlias	ParentMetaProcessElementCodeAlias	ForeignKeyRelationshipWithParent	ControlAttributePathExpression	ChartAttributeDisplayAxisCode	AttributeDisplaySequenceNumber	AggregateFunction	OrderBySequenceNumber	OrderByMode	OrderByValues	GroupByAttribute	GroupBySequenceNumber	GroupByHavingExpression	ClientDisplay	ClientMatch	Notes
     */
    private String queryPkID;
    private String analyticsMetricChartID;
    private String mtPE;
    private String mtPEAlias;
    private String parentMTPEAlias;
    private String fkRelnWithParent;
    private String controlAttributePath;
    private String attrDispAxizCodeFkId;
    private int attributeDisplaySeqNumberForTable;
    private boolean isAggregateFunction;
    private int orderBySequenceNumber;
    private String orderByMode;
    private String orderByValues;
    private boolean isGroupByAttribute;
    private int groupBySeqNumber;
    private String groupByHavingExpn;
    private boolean isClientDisplay;
    private boolean isClientMatch;

    public String getQueryPkID() {
        return queryPkID;
    }

    public void setQueryPkID(String queryPkID) {
        this.queryPkID = queryPkID;
    }

    public String getAnalyticsMetricChartID() {
        return analyticsMetricChartID;
    }

    public void setAnalyticsMetricChartID(String analyticsMetricChartID) {
        this.analyticsMetricChartID = analyticsMetricChartID;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getParentMTPEAlias() {
        return parentMTPEAlias;
    }

    public void setParentMTPEAlias(String parentMTPEAlias) {
        this.parentMTPEAlias = parentMTPEAlias;
    }

    public String getFkRelnWithParent() {
        return fkRelnWithParent;
    }

    public void setFkRelnWithParent(String fkRelnWithParent) {
        this.fkRelnWithParent = fkRelnWithParent;
    }

    public String getControlAttributePath() {
        return controlAttributePath;
    }

    public void setControlAttributePath(String controlAttributePath) {
        this.controlAttributePath = controlAttributePath;
    }

    public String getAttrDispAxizCodeFkId() {
        return attrDispAxizCodeFkId;
    }

    public void setAttrDispAxizCodeFkId(String attrDispAxizCodeFkId) {
        this.attrDispAxizCodeFkId = attrDispAxizCodeFkId;
    }

    public int getAttributeDisplaySeqNumberForTable() {
        return attributeDisplaySeqNumberForTable;
    }

    public void setAttributeDisplaySeqNumberForTable(int attributeDisplaySeqNumberForTable) {
        this.attributeDisplaySeqNumberForTable = attributeDisplaySeqNumberForTable;
    }

    public boolean isAggregateFunction() {
        return isAggregateFunction;
    }

    public void setAggregateFunction(boolean aggregateFunction) {
        isAggregateFunction = aggregateFunction;
    }

    public int getOrderBySequenceNumber() {
        return orderBySequenceNumber;
    }

    public void setOrderBySequenceNumber(int orderBySequenceNumber) {
        this.orderBySequenceNumber = orderBySequenceNumber;
    }

    public String getOrderByMode() {
        return orderByMode;
    }

    public void setOrderByMode(String orderByMode) {
        this.orderByMode = orderByMode;
    }

    public String getOrderByValues() {
        return orderByValues;
    }

    public void setOrderByValues(String orderByValues) {
        this.orderByValues = orderByValues;
    }

    public boolean isGroupByAttribute() {
        return isGroupByAttribute;
    }

    public void setGroupByAttribute(boolean groupByAttribute) {
        isGroupByAttribute = groupByAttribute;
    }

    public int getGroupBySeqNumber() {
        return groupBySeqNumber;
    }

    public void setGroupBySeqNumber(int groupBySeqNumber) {
        this.groupBySeqNumber = groupBySeqNumber;
    }

    public String getGroupByHavingExpn() {
        return groupByHavingExpn;
    }

    public void setGroupByHavingExpn(String groupByHavingExpn) {
        this.groupByHavingExpn = groupByHavingExpn;
    }

    public boolean isClientDisplay() {
        return isClientDisplay;
    }

    public void setClientDisplay(boolean clientDisplay) {
        isClientDisplay = clientDisplay;
    }

    public boolean isClientMatch() {
        return isClientMatch;
    }

    public void setClientMatch(boolean clientMatch) {
        isClientMatch = clientMatch;
    }

    public static AnalyticsMetricChartQuery fromVO(AnalyticsMetricChartQueryVO analyticsMetricChartQueryVO) {
        AnalyticsMetricChartQuery analyticsMetricChartQuery = new AnalyticsMetricChartQuery();
        BeanUtils.copyProperties(analyticsMetricChartQueryVO, analyticsMetricChartQuery);
        return analyticsMetricChartQuery;
    }
}
