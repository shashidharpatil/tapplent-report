package com.tapplent.apputility.layout.structure;

import java.util.Map;

public class GenericLayoutInsertClass {
	private String primaryKeyName;
	private String primaryKeyColumnName;
	private String primaryKeyId;
	private String tableName;
	private Map<String, Object> columnNameToValueMap;
	Map<String, String> columnNameToColumnTypeMap;
	
	public GenericLayoutInsertClass(String primaryKeyName, String primaryKeyId, String primaryKeyColumnName, String tableName,
			Map<String, Object> columnNameToValueMap, Map<String, String> columnNameToColumnTypeMap) {
		super();
		this.primaryKeyName = primaryKeyName;
		this.primaryKeyId = primaryKeyId;
		this.primaryKeyColumnName = primaryKeyColumnName;
		this.tableName = tableName;
		this.columnNameToValueMap = columnNameToValueMap;
		this.columnNameToColumnTypeMap = columnNameToColumnTypeMap;
	}
	public String getPrimaryKeyName() {
		return primaryKeyName;
	}
	public void setPrimaryKeyName(String primaryKeyName) {
		this.primaryKeyName = primaryKeyName;
	}
	public String getPrimaryKeyId() {
		return primaryKeyId;
	}
	public void setPrimaryKeyId(String primaryKeyId) {
		this.primaryKeyId = primaryKeyId;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public Map<String, Object> getColumnNameToValueMap() {
		return columnNameToValueMap;
	}
	public void setColumnNameToValueMap(Map<String, Object> columnNameToValueMap) {
		this.columnNameToValueMap = columnNameToValueMap;
	}
	public Map<String, String> getColumnNameToColumnTypeMap() {
		return columnNameToColumnTypeMap;
	}
	public void setColumnNameToColumnTypeMap(Map<String, String> columnNameToColumnTypeMap) {
		this.columnNameToColumnTypeMap = columnNameToColumnTypeMap;
	}
	public String getPrimaryKeyColumnName() {
		return primaryKeyColumnName;
	}
	public void setPrimaryKeyColumnName(String primaryKeyColumnName) {
		this.primaryKeyColumnName = primaryKeyColumnName;
	}
}
