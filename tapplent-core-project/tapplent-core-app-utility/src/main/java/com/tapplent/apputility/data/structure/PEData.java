package com.tapplent.apputility.data.structure;

import com.tapplent.apputility.layout.structure.PersonTags;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.workflow.structure.WorkflowDetails;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PEData {
	private String mtPE;
	private String mtPEAlias;
	private String parentMTPEAlias;
	private String baseTemplateId;
	private String actionVisualMasterCode;
	private String btPE;
	private String domainObject;
	private String businessRule;
	private String context;
	private String parentMetaProcessElement;
	private String parentDOPrimaryKey;//id
	private String parentDOVersionID;//id
	private boolean setContext;
	private boolean uniqueMerge;
	private boolean propagate;
	private boolean logActivity;
//	private String logActivityMTPE;
	private boolean returnMasterTags;
	private boolean submit;
	private boolean inline;
	private String actionCode;
	private String duplicateOfAttribute;
	private String deleteActionType;
	private String deleteActionCategory;
	private String currentActionCode;
	private String previousActionCode;
	private String compareKey;
	private String fkRelationWithParent;
	private String employeeCodeGenKey;
	private String usernameGenKey;
	private WorkflowDetails workflowDetails;
	private PersonTags personTag;
	private List<PEData> childrenPEData = new ArrayList<>();
	private Map<String, Object> data = new LinkedHashMap<String, Object>();

	public String getMtPE() {
		return mtPE;
	}

	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}

	public String getParentMTPEAlias() {
		return parentMTPEAlias;
	}

	public void setParentMTPEAlias(String parentMTPEAlias) {
		this.parentMTPEAlias = parentMTPEAlias;
	}

	public String getBaseTemplateId() {
		return baseTemplateId;
	}

	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getActionVisualMasterCode() {
		return actionVisualMasterCode;
	}

	public void setActionVisualMasterCode(String actionVisualMasterCode) {
		this.actionVisualMasterCode = actionVisualMasterCode;
	}

	public String getBtPE() {
		return btPE;
	}

	public void setBtPE(String btPE) {
		this.btPE = btPE;
	}

	public String getDomainObject() {
		return domainObject;
	}

	public void setDomainObject(String domainObject) {
		this.domainObject = domainObject;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getParentMetaProcessElement() {
		return parentMetaProcessElement;
	}

	public void setParentMetaProcessElement(String parentMetaProcessElement) {
		this.parentMetaProcessElement = parentMetaProcessElement;
	}

	public String getParentDOPrimaryKey() {
		return parentDOPrimaryKey;
	}

	public void setParentDOPrimaryKey(String parentDOPrimaryKey) {
		this.parentDOPrimaryKey = parentDOPrimaryKey;
	}

	public String getParentDOVersionID() {
		return parentDOVersionID;
	}

	public void setParentDOVersionID(String parentDOVersionID) {
		this.parentDOVersionID = parentDOVersionID;
	}

	public boolean isSetContext() {
		return setContext;
	}

	public void setSetContext(boolean setContext) {
		this.setContext = setContext;
	}

	public boolean isUniqueMerge() {
		return uniqueMerge;
	}

	public void setUniqueMerge(boolean uniqueMerge) {
		this.uniqueMerge = uniqueMerge;
	}

	public boolean isPropagate() {
		return propagate;
	}

	public void setPropagate(boolean propagate) {
		this.propagate = propagate;
	}

	public boolean isLogActivity() {
		return logActivity;
	}

	public void setLogActivity(boolean logActivity) {
		this.logActivity = logActivity;
	}

	public boolean isReturnMasterTags() {
		return returnMasterTags;
	}

	public void setReturnMasterTags(boolean returnMasterTags) {
		this.returnMasterTags = returnMasterTags;
	}

	public boolean isSubmit() {
		return submit;
	}

	public void setSubmit(boolean submit) {
		this.submit = submit;
	}

	public boolean isInline() {
		return inline;
	}

	public void setInline(boolean inline) {
		this.inline = inline;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getDuplicateOfAttribute() {
		return duplicateOfAttribute;
	}

	public void setDuplicateOfAttribute(String duplicateOfAttribute) {
		this.duplicateOfAttribute = duplicateOfAttribute;
	}

	public List<PEData> getChildrenPEData() {
		return childrenPEData;
	}

	public void setChildrenPEData(List<PEData> childrenPEData) {
		this.childrenPEData = childrenPEData;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public String getDeleteActionType() {
		return deleteActionType;
	}

	public void setDeleteActionType(String deleteActionType) {
		this.deleteActionType = deleteActionType;
	}

	public String getDeleteActionCategory() {
		return deleteActionCategory;
	}

	public void setDeleteActionCategory(String deleteActionCategory) {
		this.deleteActionCategory = deleteActionCategory;
	}

	public String getCurrentActionCode() {
		return currentActionCode;
	}

	public void setCurrentActionCode(String currentActionCode) {
		this.currentActionCode = currentActionCode;
	}

	public String getPreviousActionCode() {
		return previousActionCode;
	}

	public void setPreviousActionCode(String previousActionCode) {
		this.previousActionCode = previousActionCode;
	}

	public PersonTags getPersonTag() {
		return personTag;
	}

	public void setPersonTag(PersonTags personTag) {
		this.personTag = personTag;
	}

	public String getCompareKey() {
		return compareKey;
	}

	public void setCompareKey(String compareKey) {
		this.compareKey = compareKey;
	}

	public String getFkRelationWithParent() {
		return fkRelationWithParent;
	}

	public void setFkRelationWithParent(String fkRelationWithParent) {
		this.fkRelationWithParent = fkRelationWithParent;
	}

	public String getMtPEAlias() {
		return mtPEAlias;
	}

	public void setMtPEAlias(String mtPEAlias) {
		this.mtPEAlias = mtPEAlias;
	}

	public String getEmployeeCodeGenKey() {
		return employeeCodeGenKey;
	}

	public void setEmployeeCodeGenKey(String employeeCodeGenKey) {
		this.employeeCodeGenKey = employeeCodeGenKey;
	}

	public String getUsernameGenKey() {
		return usernameGenKey;
	}

	public String getBusinessRule() {
		return businessRule;
	}

	public void setBusinessRule(String businessRule) {
		this.businessRule = businessRule;
	}

	public void setUsernameGenKey(String usernameGenKey) {
		this.usernameGenKey = usernameGenKey;
	}

	public WorkflowDetails getWorkflowDetails() {
		return workflowDetails;
	}

	public void setWorkflowDetails(WorkflowDetails workflowDetails) {
		this.workflowDetails = workflowDetails;
	}

	public void initialize(PEData pEData, GlobalVariables globalVariables) {
		pEData.setActionCode(globalVariables.actionCode);
		pEData.setLogActivity(globalVariables.logActivity);
		pEData.setDeleteActionType(globalVariables.deleteActionType);
		pEData.setSubmit(globalVariables.isSubmit);
		pEData.setActionVisualMasterCode(globalVariables.actionVisualMasterCode);
	}
}
