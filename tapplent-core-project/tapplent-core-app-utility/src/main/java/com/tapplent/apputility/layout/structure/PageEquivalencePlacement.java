package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.PageEquivalencePlacementVO;

public class PageEquivalencePlacement {
	String pageEquivalencePlacementId;
	@JsonIgnore
	String baseTemplate;
	@JsonIgnore
	String mtPE;
	@JsonIgnore
	String viewType;
	String containerPage;
	String providerPage;
	JsonNode deviceIndependentBlob;
	JsonNode deviceDependentBlob;
	
	
	public String getPageEquivalencePlacementId() {
		return pageEquivalencePlacementId;
	}
	public void setPageEquivalencePlacementId(String pageEquivalencePlacementId) {
		this.pageEquivalencePlacementId = pageEquivalencePlacementId;
	}
	public String getBaseTemplate() {
		return baseTemplate;
	}
	public void setBaseTemplate(String baseTemplate) {
		this.baseTemplate = baseTemplate;
	}
	public String getMtPE() {
		return mtPE;
	}
	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}
	public String getViewType() {
		return viewType;
	}
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public String getContainerPage() {
		return containerPage;
	}
	public void setContainerPage(String containerPage) {
		this.containerPage = containerPage;
	}
	public String getProviderPage() {
		return providerPage;
	}
	public void setProviderPage(String providerPage) {
		this.providerPage = providerPage;
	}
	public static PageEquivalencePlacement fromVO(PageEquivalencePlacementVO equivalencePlacementVO){
		PageEquivalencePlacement equivalencePlacement = new PageEquivalencePlacement();
		BeanUtils.copyProperties(equivalencePlacementVO, equivalencePlacement);
		return equivalencePlacement;
	}
}
