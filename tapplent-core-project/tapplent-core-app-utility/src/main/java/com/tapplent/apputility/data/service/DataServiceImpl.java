package com.tapplent.apputility.data.service;

import java.io.IOException;


import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import com.tapplent.apputility.data.structure.*;
import com.tapplent.apputility.uilayout.structure.CostCentreBreakUpData;
import com.tapplent.apputility.uilayout.structure.JobFamilyBreakUpData;
import com.tapplent.apputility.uilayout.structure.LocationBreakUpData;
import com.tapplent.apputility.uilayout.service.UILayoutService;
import com.tapplent.apputility.uilayout.structure.OrganizationBreakUpData;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.activityLog.AuditMsgFormat;
import com.tapplent.platformutility.common.cache.PersonDetailsObjectRepository;
import com.tapplent.platformutility.common.util.valueObject.FilterExpn;
import com.tapplent.platformutility.expression.jeval.EvaluationException;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.hierarchy.service.HierarchyService;
import com.tapplent.platformutility.hierarchy.structure.HierarchyInfo;
import com.tapplent.platformutility.hierarchy.structure.Relation;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.person.structure.PersonGroup;
import com.tapplent.platformutility.search.builder.SqlQuery;
import com.tapplent.platformutility.uilayout.valueobject.*;
import com.tapplent.platformutility.workflow.service.WorkflowUtility;
import com.tapplent.platformutility.workflow.structure.*;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import com.tapplent.tenantresolver.user.UserContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.RequestSelectType;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.lexer.Lexer;
import com.tapplent.platformutility.lexer.LexerResult;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.builder.DbSearchTemplate;
import com.tapplent.platformutility.search.builder.SearchField;
import com.tapplent.platformutility.search.builder.SortData;
import com.tapplent.platformutility.search.builder.expression.ExpressionBuilder;
import com.tapplent.platformutility.search.builder.expression.LeafOperand;
import com.tapplent.platformutility.search.builder.expression.Operator;
import com.tapplent.platformutility.search.builder.joins.JoinLeafOperand;
import com.tapplent.platformutility.search.builder.joins.JoinOperatorsNode;
import com.tapplent.platformutility.search.builder.joins.JoinType;
import com.tapplent.platformutility.search.impl.DefaultSearchData;
import com.tapplent.platformutility.search.impl.SearchService;
import com.tapplent.platformutility.common.util.StringUtil;
/**
 * This class contains methods that retrieves data from database by making SELECT statements.
 * @author Shubham Patodi
 */
@Service
public class DataServiceImpl implements DataService{
	private SearchService searchService;
	private UILayoutService uiLayoutService;
	private ResponseDataBuilder responseDataBuilder;
	private HierarchyService hierarchyService;
	private WorkflowUtility workflowUtility;
	private static final String INVERTED_COMMA = "'";
	public static final String COLON = ":";
    private static final Logger LOG = LogFactory.getLogger(DataServiceImpl.class);
	/**
	 * Understands the request data and returns the response in the format required by client.
	 * @param requestData
	 */
	@Transactional
	@Override
	public UnitResponseData getScreen(UnitRequestData requestData) throws IOException, SQLException {
		if(requestData==null){
			return null;
		}
		return null;
//        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
//		UserContext user = TenantContextHolder.getUserContext();
//		PersonGroup group = personService.getPersonGroups(user.getPersonId()); // Get the group where all this person belongs.
//		ScreenRequest screenRequest = requestData.getScreen();
//        UnitResponseData result = new UnitResponseData();
//        ScreenInstanceVO layoutScreenInstance = null;
//        ScreenInstanceVO dataScreenInstance = null;
//		if(StringUtil.isDefined(screenRequest.getTargetScreenInstance())) { // This is in case of target screen instance is defined, we need to remove this once everything is generic.
//            List<String> targetSectionInstanceList = null;
//		    if(StringUtil.isDefined(screenRequest.getTargetSectionInstance())) {
//                targetSectionInstanceList = new ArrayList<>();
//                targetSectionInstanceList.add(screenRequest.getTargetSectionInstance());
//            }
//            layoutScreenInstance = uiLayoutService.getLayout(screenRequest.getTargetScreenInstance(), targetSectionInstanceList, screenRequest.getDeviceType(), group);
//            dataScreenInstance = layoutScreenInstance;
//            result.setLayout(layoutScreenInstance);
//        }else{
//		    /*
//		    Here get the target screen section instances and screen instance and pass it to get the layout.
//		     */
//            Map<Integer, SectionLayoutMapper> sectionLayoutMapperMap = uiLayoutService.getSectionLayoutMapper(screenRequest.getMtPE(), screenRequest.getUiDispTypeCodeFkId(), screenRequest.getTabSeqNumPosInt());
//            List<String> targetSectionInstanceList = new ArrayList<>();
//            List<String> dataSectionInstanceList = new ArrayList<>();
//            List<SectionLayoutMapper> sectionLayoutMapperList = new ArrayList<>();
//            /* Now we need to have two screen instances one for layout and another for data */
//            // Get all the sections that are applicable on the basis of section layout mapper
//            Set<Integer> tabSeqList = sectionLayoutMapperMap.keySet();
//            Integer[] sortedTabSeq = new Integer[tabSeqList.size()];
//            sortedTabSeq = tabSeqList.toArray(sortedTabSeq);
//            Arrays.sort(sortedTabSeq);
//            for(int i =0 ; i<sortedTabSeq.length; i++){
//                SectionLayoutMapper sectionLayoutMapper = sectionLayoutMapperMap.get(sortedTabSeq[i]);
//                String targetSectionPipedList = sectionLayoutMapper.getTargetSecnInstInSeqLngTxt();
//                String[] targetSectionList = targetSectionPipedList.split("\\|");
//                targetSectionInstanceList.addAll(Arrays.asList(targetSectionList));
//                sectionLayoutMapperList.add(sectionLayoutMapperMap.get(sortedTabSeq[i]));
//                // If it is the first tab we need to provide the data for the same.
//                if(i==0){
//                    dataSectionInstanceList.addAll(Arrays.asList(targetSectionList));
//                }
//            }
////            for(Map.Entry<Integer, SectionLayoutMapper> entry : sectionLayoutMapperMap.entrySet()){
////                SectionLayoutMapper sectionLayoutMapper = entry.getValue();
////                String targetSectionPipedList = sectionLayoutMapper.getTargetSecnInstInSeqLngTxt();
////                String[] targetSectionList = targetSectionPipedList.split("\\|");
////                targetSectionInstanceList.addAll(Arrays.asList(targetSectionList));
////                sectionLayoutMapperList.add(entry.getValue());
////            }
//            String targetScreenInstanceId = uiLayoutService.getScreenInstanceFromSection(targetSectionInstanceList.get(0));
//            layoutScreenInstance = uiLayoutService.getLayout(targetScreenInstanceId, targetSectionInstanceList, screenRequest.getDeviceType(), group);
//            dataScreenInstance = uiLayoutService.getLayout(targetScreenInstanceId, dataSectionInstanceList, screenRequest.getDeviceType(), group);
//            result.setSectionLayoutMapperList(sectionLayoutMapperList);
//            result.setLayout(layoutScreenInstance);
//        }
//        String targetScreenSectionCode = uiLayoutService.getSectionMasterCode(screenRequest.getTargetSectionInstance());
//        JsonLabelAliasTracker aliasTracker = new JsonLabelAliasTracker();
//        List<SectionControlVO> appLevelPropertyControls = new ArrayList<>();
//        List<PERequest> rootPEs = buildPERequest(requestData, dataScreenInstance,group, appLevelPropertyControls, targetScreenSectionCode);
//        if(screenRequest.getDataRequired()) {
//            for (PERequest rootPE : rootPEs) {
//                ProcessElementData peResponse = getDataInternal(rootPE, aliasTracker, screenRequest.getBaseTemplate());
//                result.getPEData().put(rootPE.getMtPE(), peResponse);
//            }
//        }
//        Map<String, AttributeData> appPropCntrl = resolveAppLevelPropCntrl(user.getPersonId(),appLevelPropertyControls);
//        result.setAppPropConrl(appPropCntrl);
//        if(!screenRequest.getLayoutRequired()){
//            result.setLayout(null);
//        }
//        if(!screenRequest.getDataRequired() && screenRequest.getMetaRequired()){
//            List<String> mtPEList = screenRequest.getMtPEList();
//            List<SectionControlVO> sectionControls = layoutScreenInstance.getSectionControls();
//            if(sectionControls!=null){
//                for(SectionControlVO sectionControl : sectionControls){
//                    if(mtPEList.contains(sectionControl.getProcessElementFkId())) {
//                        if (StringUtil.isDefined(sectionControl.getControlAttributePathExpn())) {
//                            AliasTrackerHelper aliasTrackerHelper = new AliasTrackerHelper(null, sectionControl.getProcessElementFkId(), false);
//                            aliasTracker.getDoaToJsonAliasMap().put(sectionControl.getControlAttributePathExpn(), aliasTrackerHelper);
//                        }
//                    }
//                }
//            }
//        }
//        MetaHelper metaHelper =getLabelToAliasMap(aliasTracker, screenRequest.getMetaRequired(), screenRequest.getMtPE(),screenRequest.getBaseTemplate(),group);
//        if(metaHelper!=null){
//            result.getLabelToAliasMap().putAll(metaHelper.getLabelToAliasMap());
//            result.setDoMetaMap(metaHelper.getDoMetaMap());
//        }
//        return result;
	}

    private MetaHelper getLabelToAliasMap(JsonLabelAliasTracker aliasTracker, Boolean metaRequired, String requestMTPE, String baseTemplate, PersonGroup group) {
//	    return null;
        Map<String, DoaMeta> result = new HashMap<>();
        Map<String, EntityMetadataVOX> doMetaMap = new HashMap<>();
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
	    for(Map.Entry<String,AliasTrackerHelper> entry : aliasTracker.getDoaToJsonAliasMap().entrySet()){
            String attrExpn = entry.getKey();
            DoaMeta aliasMeta = new DoaMeta();
            AliasTrackerHelper aliasHelper = entry.getValue();
            if(aliasHelper.getAliasNumber()!=null)
                aliasMeta.setAlias(aliasHelper.getAliasNumber().toString());
//            String mtPE = aliasHelper.getMtPECode();
//            if(metaRequired) {
//                List<String> doaList = new ArrayList<>();
//                if (aliasHelper.getInternal()) {
//                    doaList.add(attrExpn);
//                } else {
//                    doaList = Util.getAttributePathFromExpression(attrExpn);
//                }
//                if (doaList.size() == 1) {
//                    String doaCode = Util.getBaseDOAFromAtrributePath(doaList.get(0));
//                    if (!StringUtil.isDefined(baseTemplate)) {
//                        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
//                        baseTemplate = personService.getDefaultBTForGroup(mtPE, group.getPersonId());
//                    }
//                    EntityAttributeMetadata doaMeta = metadataService.getMetadataByBtDoa(baseTemplate, doaCode);
//                    EntityMetadata doMeta = metadataService.getMetadataByBtDo(baseTemplate, doaMeta.getDomainObjectCode());
//                    aliasMeta.setMeta(doaMeta);
//                    if(doaMeta.isCountrySpecific()){
//                        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
//                        Person person = personService.getPersonDetails(group.getPersonId());
//                                doaMeta.setCountryAttrDisplayName(null);
//                        doaMeta.setCountryAttrIcon(null);
//                        CountrySpecificDOAMeta countrySpecificDOAMeta = metadataService.getCountrySpecificDOAMeta(doaMeta.getBtDoSpecId(), person.getBaseCountryCodeFkId());
//                        doaMeta.setCountryAttrDisplayName(countrySpecificDOAMeta.getDoaDisplayNameOverrideG11nBigTxt());
//                        doaMeta.setCountryAttrIcon(countrySpecificDOAMeta.getDoaDisplayIconCodeFkId());
//                    }
//                    doMetaMap.put(doMeta.getDomainObjectCode(), doMeta);
//                }
//            }
            result.put(attrExpn, aliasMeta);
        }
        MetaHelper metaHelper = new MetaHelper();
	    metaHelper.setLabelToAliasMap(result);
	    if(StringUtil.isDefined(requestMTPE) && !doMetaMap.containsKey(requestMTPE)){
            if (!StringUtil.isDefined(baseTemplate)) {
                PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
                baseTemplate = personService.getDefaultBTForGroup(requestMTPE, group.getPersonId());
            }
            EntityMetadataVOX doMeta = metadataService.getMetadataByProcessElement(baseTemplate, requestMTPE);
            doMetaMap.put(doMeta.getDomainObjectCode(), doMeta);
        }
	    metaHelper.setDoMetaMap(doMetaMap);
        return metaHelper;
    }

//    @Override
//    public UnitResponseData getAnalyticsMetricDetailsData(AnalyticsMetricRequest analyticsMetricRequest) {
//	    return null;
	    /*
	    TODO apply filter expressions
	     */
//        UnitResponseData response = new UnitResponseData();
//	    List<PERequest> rootPEList = new ArrayList<>();
//        Map<String, ProcessElementData> result = new HashMap<>();
//        Map<String, LoadParameters> peToLoadParamsMap = analyticsMetricRequest.getPeToLoadParamsMap();
//        Map<String,PERequest> mtPEToPERequestMap = new HashMap<>();
//        for(SectionControlVO control : analyticsMetricRequest.getMetricQueryList()){
//            String processElement = control.getProcessElementFkId();
//            String mtPE = control.getProcessElementFkId();
//            String parentPE = null;
//            if(StringUtil.isDefined(control.getFkRelationshipWithParentLngTxt())){
//                parentPE = control.getFkRelationshipWithParentLngTxt().split("\\|")[0].split("\\.")[0];
//            }
//            if (mtPEToPERequestMap.containsKey(mtPE)) {
//                PERequest peRequest = mtPEToPERequestMap.get(mtPE);
//                addNewAttributePath(peRequest, control,  parentPE, null);
//            } else {
//                PERequest newPERequest = new PERequest(processElement);
//                newPERequest.setParentPE(parentPE);
//                newPERequest.setSelectType(RequestSelectType.LAYOUT);
//                addNewAttributePath(newPERequest, control, parentPE, null);
//                mtPEToPERequestMap.put(mtPE, newPERequest);
//            }
//        }
//        for(Map.Entry<String,PERequest> entry : mtPEToPERequestMap.entrySet()){
//            PERequest peRequest = entry.getValue();
//            // Change the group by control attribute also here
//            addGroupByDetailsToGroupByFunctions(peRequest);
//            String parentPE = peRequest.getParentPE();
//            if(peToLoadParamsMap!=null) {
//                if(peToLoadParamsMap.get(peRequest.getMtPE())!=null) {
//                    String filterExpn = peToLoadParamsMap.get(peRequest.getMtPE()).getFilterExpn();
//                    peRequest.setFilterExpression(filterExpn);
//                }
//            }
//            if(StringUtil.isDefined(parentPE)) {
//                mtPEToPERequestMap.get(parentPE).getChildrenPE().add(entry.getValue());
//            }else {
//                rootPEList.add(peRequest);
//            }
//        }
//        JsonLabelAliasTracker aliasTracker = new JsonLabelAliasTracker();
//        for(PERequest rootPE : rootPEList){
//            ProcessElementData peResponse = null;
//            try {
//                peResponse = getDataInternal(rootPE, aliasTracker, null);
//                result.put(peResponse.getMtPE(),peResponse);
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//        response.setPEData(result);
//        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
//        UserContext user = TenantContextHolder.getUserContext();
//        PersonGroup group = personService.getPersonGroups(user.getPersonId());
//        MetaHelper metaHelper = getLabelToAliasMap(aliasTracker, false, null, null,group);
//        response.setLabelToAliasMap(metaHelper.getLabelToAliasMap());
//        return response;
//    }

    @Override
    @Transactional
    public RelationResponse getRelationControlData(String controlId, JsonLabelAliasTracker aliasTracker) {
	    return null;
//        RelationResponse relationResponse = new RelationResponse();
//        SectionControlVO control = uiLayoutService.getControlData(controlId);
//        ProcessElementData peResponse = null;
//        List<String> attributeExpnList = new ArrayList<>();
//        /* Now that we have control we should be able to make PERquest from it and return it to get the apporpriate results */
//        PERequest peRequest = new PERequest();
//        String mtPE = null;
//        if(StringUtil.isDefined(control.getRelCtrlFstDispAttrPathExpn())){
//            List<String> fstAttributes = Util.getAttributePathFromExpression(control.getRelCtrlFstDispAttrPathExpn());
//            if(fstAttributes!=null && !fstAttributes.isEmpty()){
//                mtPE = fstAttributes.get(0).split("\\.")[0];
//            }
//            String[] attributePaths = control.getRelCtrlFstDispAttrPathExpn().split("\\|");
//            for(int i= 0; i< attributePaths.length; i++) {
//                peRequest.getAttributePaths().getAttributePathsMap().put(attributePaths[i], attributePaths[i]);
//                attributeExpnList.add(attributePaths[i]);
//            }
//        }
//        if(StringUtil.isDefined(control.getRelCtrlImgDispAttrPathExpn())){
//            List<String> fstAttributes = Util.getAttributePathFromExpression(control.getRelCtrlImgDispAttrPathExpn());
//            if(fstAttributes!=null && !fstAttributes.isEmpty()){
//                mtPE = fstAttributes.get(0).split("\\.")[0];
//            }
//            String[] attributePaths = control.getRelCtrlImgDispAttrPathExpn().split("\\|");
//            for(int i= 0; i< attributePaths.length; i++) {
//                peRequest.getAttributePaths().getAttributePathsMap().put(attributePaths[i], attributePaths[i]);
//                attributeExpnList.add(attributePaths[i]);
//            }
//        }
//        if(StringUtil.isDefined(control.getRelCtrlSndDispAttrPathExpn())){
//            List<String> fstAttributes = Util.getAttributePathFromExpression(control.getRelCtrlSndDispAttrPathExpn());
//            if(fstAttributes!=null && !fstAttributes.isEmpty()){
//                mtPE = fstAttributes.get(0).split("\\.")[0];
//            }
//            String[] attributePaths = control.getRelCtrlSndDispAttrPathExpn().split("\\|");
//            for(int i= 0; i< attributePaths.length; i++) {
//                peRequest.getAttributePaths().getAttributePathsMap().put(attributePaths[i], attributePaths[i]);
//                attributeExpnList.add(attributePaths[i]);
//            }
//        }
//        if(StringUtil.isDefined(control.getRelCtrlRtrnDispAttrPathExpn())){
//            List<String> fstAttributes = Util.getAttributePathFromExpression(control.getRelCtrlRtrnDispAttrPathExpn());
//            if(fstAttributes!=null && !fstAttributes.isEmpty()){
//                mtPE = fstAttributes.get(0).split("\\.")[0];
//            }
//            String[] attributePaths = control.getRelCtrlRtrnDispAttrPathExpn().split("\\|");
//            for(int i= 0; i< attributePaths.length; i++) {
//                peRequest.getAttributePaths().getAttributePathsMap().put(attributePaths[i], attributePaths[i]);
//                attributeExpnList.add(attributePaths[i]);
//            }
//        }
//        peRequest.setMtPE(mtPE);
//        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
//        MasterEntityMetadataVOX mtMeta = metadataService.getMetadataByMtPE(mtPE);
//        MasterEntityAttributeMetadata primaryDoa = mtMeta.getPrimaryKeyAttribute();
//        if(aliasTracker==null)
//            aliasTracker = new JsonLabelAliasTracker();
//        List<RelationResponseInternal> responseInternalList = new ArrayList<>();
//        try {
//            peResponse = getDataInternal(peRequest, aliasTracker, null);
//            List<Record> recordSet = peResponse.getRecordSetInternal();
//            for(Record record : recordSet){
//                RelationResponseInternal subResponse = new RelationResponseInternal();
//                /* First set the pk */
//                AliasTrackerHelper pkAliasHelper = aliasTracker.getDoaToJsonAliasMap().get(primaryDoa.getDoAttributeCode());
//                if(pkAliasHelper!=null) {
//                    AttributeData pkData = record.getAttributeMap().get(pkAliasHelper.getAliasNumber().toString());
//                    subResponse.setPk(pkData);
//                }
//                for(String attributeExpn : attributeExpnList){
//                    AliasTrackerHelper aliasHelper = aliasTracker.getDoaToJsonAliasMap().get(attributeExpn);
//                    if(aliasHelper!=null) {
//                        AttributeData data = record.getAttributeMap().get(aliasHelper.getAliasNumber().toString());
//                        subResponse.getData().put(aliasHelper.getAliasNumber().toString(), data);
//                        DoaMeta doaMeta = new DoaMeta(aliasHelper.getAliasNumber().toString());
//                        relationResponse.getLabelToAliasMap().put(attributeExpn, doaMeta);
//                    }
//                }
//                responseInternalList.add(subResponse);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        relationResponse.setRecordSet(responseInternalList);
//        return relationResponse;
    }

    @Override
    @Transactional
    public G11nResponse getG11nData(String dbPkId, String doaExpn) {
	    /* Here we need to get the column values present in the g11n format from the database */
        String returnValue = null;
        List<String> doaList = Util.getAttributePathFromExpression(doaExpn);
        if(doaList.size()==1){
            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
            String doaCode = doaList.get(0);
            String doCode = doaCode.split("\\.")[0];
            MasterEntityMetadataVOX doMeta = metadataService.getMetadataByDo(doCode);
            MasterEntityAttributeMetadata doaMeta = doMeta.getAttributeByName(doaCode);
            MasterEntityAttributeMetadata pkMeta = doMeta.getPrimaryKeyAttribute();
            if(doaMeta.isGlocalized()){
                /* Get the glocalized from here */
                String tableName = doaMeta.getDbTableName();
                String columnName = doaMeta.getDbColumnName();
                String pkColumnName = pkMeta.getDbColumnName();
                returnValue = searchService.getG11nData(tableName, columnName, pkColumnName, dbPkId, pkMeta.getDbDataTypeCode());

            }
        }
        G11nResponse g11nResponse = new G11nResponse(returnValue);
        return g11nResponse;
    }

    @Override
    @Transactional
    public UnitResponseData getRelationDownwardEffect(String mtPE, String pk, boolean isMasterTable) {
//	    return  null;
        // This is giving all the dependent fk IDs
	    PEFKHierarchy peFkHierarchy = getPEFKHierarchy(mtPE, isMasterTable);
	    UnitResponseData response = new UnitResponseData();
	    /* Now build the children hierarchy according the child pe */
        /* Here get all the places where the primary key of this mtpe is the foreign key */
        PERequest rootPERequest = new PERequest(mtPE);
        rootPERequest.setMtPE(mtPE);
        rootPERequest.setSelectType(RequestSelectType.LAYOUT);
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadataVOX rootMtPEMeta = metadataService.getMetadataByMtPE(mtPE);
        MasterEntityAttributeMetadata attributeMetadata = rootMtPEMeta.getFunctionalPrimaryKeyAttribute();
        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
        String baseTemplate = personService.getDefaultBTForGroup(mtPE, TenantContextHolder.getUserContext().getPersonId());
        EntityMetadataVOX rootBTPEMeta = metadataService.getMetadataByProcessElement(baseTemplate, mtPE);
        EntityAttributeMetadata contextAttributeMetadata = rootBTPEMeta.getIsContextObjectField();
        rootPERequest.getAttributePaths().getAttributePaths().add("!{" + attributeMetadata.getDoAttributeCode() + "}");
        if(contextAttributeMetadata!=null)
            rootPERequest.getAttributePaths().getAttributePaths().add("!{" + contextAttributeMetadata.getDoAttributeCode() + "}");
        StringBuilder filterExpn = null;
        if(rootMtPEMeta.isHierarchyTable()){
            List<HierarchyInfo> childrenPks = hierarchyService.getAllChildren(rootMtPEMeta, pk);
            filterExpn = new StringBuilder("!{" + attributeMetadata.getDoAttributeCode() + "}" + " IN(");
            for(int i = 0; i<childrenPks.size(); i++){
                HierarchyInfo info = childrenPks.get(i);
                if(i!=0){
                    filterExpn.append(",");
                }
                if(attributeMetadata.getDbDataTypeCode().equals("T_ID")){
                    filterExpn.append("0x").append(info.getPk());
                }
            }
            filterExpn.append(")");
            rootPERequest.getAttributePaths().getStickyHeaderOrderByList().add(new SortData("!{" + attributeMetadata.getDoAttributeCode() + "}", true, 1, null));
        }else {
            if (attributeMetadata.getDbDataTypeCode().equals("T_ID")) {
                filterExpn = new StringBuilder("!{" + attributeMetadata.getDoAttributeCode() + "}" + "=" + "'" + pk + "'");
            } else {
                filterExpn = new StringBuilder("!{" + attributeMetadata.getDoAttributeCode() + "}" + "=" + "'" + pk + "'");
            }
        }
        if(rootMtPEMeta.getRecordStateAttribute()!=null) {
            filterExpn.append(" AND !{"+rootMtPEMeta.getRecordStateAttribute().getDoAttributeCode()+"} = 'CURRENT'");
        }
        rootPERequest.setFilterExpression(filterExpn.toString());
        buildPERequestForDownwardMtPE(peFkHierarchy, pk, rootPERequest);
        List<PERequest> rootPEList = new ArrayList<>();
        rootPEList.add(rootPERequest);
        JsonLabelAliasTracker aliasTracker = new JsonLabelAliasTracker();
        for(PERequest rootPE : rootPEList){
            ProcessElementData peResponse = null;
            try {
                peResponse = getDataInternal(rootPE, aliasTracker, null, response);
                response.getPEData().put(peResponse.getMtPE(),peResponse);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        UserContext user = TenantContextHolder.getUserContext();
        PersonGroup group = personService.getPersonGroups(user.getPersonId());
        MetaHelper metaHelper = getLabelToAliasMap(aliasTracker, true, mtPE,null,group);
        response.setLabelToAliasMap(metaHelper.getLabelToAliasMap());
        // Now if it a hierarchical do modify the response to incorporate the root hierarchy.
//        ResponseDataBuilder.buildRootHierarchy(response, mtPE);
	    return response;
    }

    @Override
    public UnitResponseData getAttributePathData(List<String> attributeList, String pk, boolean isDbPk, boolean isResolveG11n) {
//	    return null;
        String mtPE = null;
        for(String attributeExpn : attributeList){
            mtPE = attributeExpn.substring(2, attributeExpn.indexOf("."));
            if(StringUtil.isDefined(mtPE))
                break;
        }
        PERequest rootPERequest = new PERequest(mtPE);
        rootPERequest.setMtPE(mtPE);
        rootPERequest.setSelectType(RequestSelectType.LAYOUT);
        rootPERequest.setResolveG11n(isResolveG11n);
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadataVOX rootMtPEMeta = metadataService.getMetadataByMtPE(mtPE);
        MasterEntityAttributeMetadata attributeMetadata = rootMtPEMeta.getFunctionalPrimaryKeyAttribute();
        if(isDbPk) {
            attributeMetadata = rootMtPEMeta.getVersionIdAttribute();
        }
        for(String attributeExpn : attributeList){
            rootPERequest.getAttributePaths().getAttributePaths().add(attributeExpn);
        }
        String filterExpn = "!{"+attributeMetadata.getDoAttributeCode()+"}"+"="+"'"+pk+"'";
        if(!isDbPk){
            if(rootMtPEMeta.getRecordStateAttribute()!=null){
                filterExpn = filterExpn + " AND "+"!{"+rootMtPEMeta.getRecordStateAttribute().getDoAttributeCode()+"} = 'CURRENT'";
            }
        }
        rootPERequest.setFilterExpression(filterExpn);
        JsonLabelAliasTracker aliasTracker = new JsonLabelAliasTracker();
        UnitResponseData response = new UnitResponseData();
        try {
            ProcessElementData peResponse = getDataInternal(rootPERequest, aliasTracker, null, null);
            response.getPEData().put(peResponse.getMtPE(),peResponse);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
        UserContext user = TenantContextHolder.getUserContext();
        PersonGroup group = personService.getPersonGroups(user.getPersonId());
        MetaHelper metaHelper = getLabelToAliasMap(aliasTracker, false, null,null,group);
        response.setLabelToAliasMap(metaHelper.getLabelToAliasMap());
        return response;
    }

    private void buildPERequestForDownwardMtPE(PEFKHierarchy peFkHierarchy, String pk, PERequest parentPERequest) {
	    List<PEFKHierarchy> childrenPEHierarchy = peFkHierarchy.getChildrenMTPEs();
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadataVOX rootMtPEMeta = metadataService.getMetadataByMtPE(peFkHierarchy.getMtPE());
	    for(PEFKHierarchy child: childrenPEHierarchy){
            MasterEntityMetadataVOX childMtPEMeta = metadataService.getMetadataByMtPE(child.getMtPE());
            // List all the doas which are foreign key to the parent table
            List<MasterEntityAttributeMetadata> relatedDOAList = new ArrayList<>();
            for(MasterEntityAttributeMetadata doa : childMtPEMeta.getAttributeMeta()){
                if(StringUtil.isDefined(doa.getToDomainObjectCode()) && doa.getToDomainObjectCode().equals(rootMtPEMeta.getDomainObjectCode())){
                    // Here make the child PE
                    PERequest childPERequest = new PERequest(child.getMtPE());
                    childPERequest.setMtPE(child.getMtPE());
                    childPERequest.setSelectType(RequestSelectType.LAYOUT);
                    MasterEntityAttributeMetadata attributeMetadata = childMtPEMeta.getFunctionalPrimaryKeyAttribute();
                    PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
                    String baseTemplate = personService.getDefaultBTForGroup(child.getMtPE(), TenantContextHolder.getUserContext().getPersonId());
                    EntityMetadataVOX childBTPEMeta = metadataService.getMetadataByProcessElement(baseTemplate, child.getMtPE());
                    EntityAttributeMetadata contextMetadata = childBTPEMeta.getIsContextObjectField();
                    childPERequest.getAttributePaths().getAttributePaths().add("!{"+attributeMetadata.getDoAttributeCode()+"}");
                    if(contextMetadata!=null) {
                        childPERequest.getAttributePaths().getAttributePaths().add("!{" + contextMetadata.getDoAttributeCode() + "}");
                    }else{
                        //TODO Throw error because each table should have context metadata.
                    }
                    /* Build the dependency keys helper */
                    DependencyKeysHelper keysHelper1 = new DependencyKeysHelper(doa.getDoAttributeCode(), rootMtPEMeta.getFunctionalPrimaryKeyAttribute().getDoAttributeCode(), childPERequest.getMtPEAlias(), parentPERequest.getMtPEAlias());
                    childPERequest.getDependencyKeyList().add(keysHelper1);
                    /* Now also add aggregate control */
                    if(childMtPEMeta.getRecordStateAttribute()!=null){
                        /*get the count of number of versions */
                        AttributePathDetails aggPathDetails =  new AttributePathDetails();
                        aggPathDetails.setAttributePath("DBCOUNT(!{"+childMtPEMeta.getVersionIdAttribute().getDoAttributeCode()+"})");
                        List<GroupByAttributeDetails> groupByDetails = new ArrayList<>();
                        GroupByAttributeDetails attributeDetails = new GroupByAttributeDetails("!{"+childMtPEMeta.getFunctionalPrimaryKeyAttribute().getDoAttributeCode()+"}",1, false);
                        groupByDetails.add(attributeDetails);
                        aggPathDetails.setGroupByAttributes(groupByDetails);
                        //TODO add filter as save state code not draft or it should be saved.
                        childPERequest.getAttributePaths().getAttributePathToAggDetailsMap().put(aggPathDetails.getAttributePath(),aggPathDetails);
                        buildPERequestForDownwardMtPE(child, pk, childPERequest);
                    }
                    parentPERequest.getChildrenPE().add(childPERequest);
                }
            }
        }

    }

    public PEFKHierarchy getPEFKHierarchy(String mtPE, boolean isMasterTable) {
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadataVOX mtPEMeta = metadataService.getMetadataByMtPE(mtPE);
        String primaryKey = mtPEMeta.getFunctionalPrimaryKeyAttribute().getDoAttributeCode();
        List<String> foreignRelatedPEs = null;
        if(isMasterTable) {
            foreignRelatedPEs = metadataService.getForeignKeyRelatedPEs(primaryKey);
        }else{
            foreignRelatedPEs = metadataService.getDependencyKeyRelatedPEs(primaryKey);
        }
        PEFKHierarchy result = new PEFKHierarchy(mtPE);
        result.setMtPE(mtPE);
        for(String fkPEs : foreignRelatedPEs){
            if(!mtPE.equals(fkPEs)) {
                MasterEntityMetadataVOX fkPEMeta = metadataService.getMetadataByMtPE(fkPEs);
                if(!fkPEMeta.getDoTypeCode().equals("TAPPLENT_OBJECT")) {
                    PEFKHierarchy pefkHierarchy = getPEFKHierarchy(fkPEs, isMasterTable);
                    result.getChildrenMTPEs().add(pefkHierarchy);
                }
            }
        }
        return result;
    }

    private Map<String, AttributeData> resolveAppLevelPropCntrl(String personId, List<SectionControlVO> appLevelPropertyControls) {
	    return null;
//        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
//        Map<String, AttributeData> pcResultMap = new HashMap<>();
//        for(SectionControlVO propControls : appLevelPropertyControls){
//            String propertyControlPath = propControls.getControlAttributePathExpn();
//            AttributeData attributeData = new AttributeData();
//            Person person = personService.getPersonDetails(personId);
//            switch(propertyControlPath){
//                case "LoggedInUser.getLogo":
//                    String imageId = person.getDefaultLogoImageId();
//                    AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
//                    URL imageURL = Util.getURLforS3(imageId, s3Client);
//                    attributeData.setcV(imageId);
//                    attributeData.setURL(imageURL);
//                    break;
//                case "LoggedInUser.getCompany":
//                    String companyNameIS_HIERARCHYLngTxt = person.getDefaultCompanyNameLngTxt();
//                    attributeData.setcV(companyNameLngTxt);
//                    break;
//                case "LoggedInUser.Photo":
//                    String photoImageid = person.getPhotoImageid();
//                    s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
//                    imageURL = Util.getURLforS3(photoImageid, s3Client);
//                    attributeData.setcV(photoImageid);
//                    attributeData.setURL(imageURL);
//                    break;
//                case "LoggedInUser.PrimaryJobLocation":
//                    String primaryJobDesignationG11nBigTxt = person.getPrimaryJobDesignationG11nBigTxt();
//                    String location = person.getPrimaryWorkAddressCityCspecG11nBigTxt();
//                    attributeData.setcV(primaryJobDesignationG11nBigTxt+", "+location);
//                    break;
//                case "LoggedInUser.PrimaryEmail":
//                    String workEmailIdTxt = person.getWorkEmailIdTxt();
//                    attributeData.setcV(workEmailIdTxt);
//                    break;
//                case "LoggedInUser.PrimaryPhone":
//                    String workPhoneTxt = person.getWorkPhoneTxt();
//                    attributeData.setcV(workPhoneTxt);
//                    break;
//                case "LoggedInUser.Name":
//                    String formalFullNameLngTxt = person.getFormalFullNameLngTxt();
//                    attributeData.setcV(formalFullNameLngTxt);
//                    break;
//                    default:
//            }
//            pcResultMap.put(propertyControlPath, attributeData);
//        }
//        return pcResultMap;
    }

//    private String getHardCodedElementName(ScreenInstanceVO screenInstance, String screenSectionMasterCodeFkId) {
//        String screenMasterCodeFkId = screenInstance.getScreenMasterCodeFkId();
//        /* Meeting|Task|Note|ActivityLog|Bookmark|SocialGroup|SocialPost|SocialComment|SocialRating|TaskDetails|MeetingDetails */
//        switch (screenMasterCodeFkId){
//            case "PROCESS_HOME_PG":
//                /* Inside process home page look for sectionMasterCode also */
//                if(screenSectionMasterCodeFkId!=null){
//                    switch (screenSectionMasterCodeFkId) {
//                        case "PROCESS_HOME_PG_WEB_QUICK_LINK_CLNDR_LIST_VW_SECN":
//                            return "Meeting";
//                        case "PROCESS_HOME_PG_WEB_QUICK_LINK_TASK_MAIN_VW_SECN":
//                            return "Task";
//                        case "PROCESS_HOME_PG_WEB_QUICK_LINK_TASK_NOTES_VW_SECN":
//                            return "Note";
//                        case "PROCESS_HOME_PG_WEB_QUICK_LINK_ACTIVITY_RECENT_VW_SECN":
//                            return "ActivityLog";
//                        case "PROCESS_HOME_PG_WEB_QUICK_LINK_ACTIVITY_BOOKMARK_VW_SECN":
//                            return "Bookmark";
//                        default:
//                            return "none";
//                    }
//                }else{
//                    return "none";
//                }
//            case "TASK_SCRN":
//                switch (screenSectionMasterCodeFkId) {
//                    case "TASK_SCRN_LIST_VW_SECN":
//                        return "TaskList";
//                    case "TASK_SCRN_DETAIL_SECN":
//                        return "TaskDetails";
//                    default:return "null";
//                }
//            case "NOTES_SCRN":
//                switch (screenSectionMasterCodeFkId) {
//                    case "NOTES_SCRN_LIST_VW_SECN":
//                        return "NoteList";
//                    case "NOTES_SCRN_DETAIL_SECN":
//                        return "NoteDetails";
//                    default:return "null";
//                }
////            case "CALENDAR_SCRN":
////                switch (screenSectionMasterCodeFkId) {
////                    case "CLNDR_SCRN_LIST_VW_SECN":
////                        return "MeetingList";
////                    case "CLNDR_SCRN_DETAIL_SECN":
////                        return "MeetingDetails";
////                    default: return "none";
////                }
//            case "ACTIVITY_BOOKMARK_SCRN":
//                switch (screenSectionMasterCodeFkId) {
//                    case "BOOKMARK_SCRN_LIST_VW_SECN":
//                        return "Bookmark";
//                    case "ACTIVITY_SCRN_LIST_VW_SECN":
//                        return "ActivityLog";
//                    default: return "none";
//                }
//            case "CNVRSN_SCRN":
//                switch (screenSectionMasterCodeFkId) {
//                    case "CNVRSN_SCRN_LIST_VW_SECN":
//                    case "CNVRSN_SCRN_DETAIL_SECN":
//                        return "SocialComment";
//                    default: return "none";
//                }
//            case "ANALYTICS_SCRN":
//                switch (screenSectionMasterCodeFkId) {
//                    case "ANALYTICS_SCRN_LIST_VW_SECN":
//                        return "analytics";
//                }
//                break;
//            case "POST_SCRN":
//                switch (screenSectionMasterCodeFkId) {
//                    case "POST_SCRN_LIST_VW_SECN":
//                        return "SocialGroup";
//                    case "POST_SCRN_DETAIL_SECN":
//                        return "SocialPost";
//                    case "POST_SCRN_DTL_CMNT_POPUP_VW_SECN":
//                    case "POST_SCRN_DTL_CMNT_REPLY_VW_SECN":
//                        return "SocialComment";
//                    case "POST_SCRN_DTL_POST_CMNT_RTG_POPUP_SECN":
//                        return "SocialRatingCount";
//                    case "POST_SCRN_DTL_POST_CMNT_RTG_POPUP_LST_VW_SECN":
//                        return "SocialRating";
//                    case "POST_SCRN_DTL_POST_CMNT_RCTN_POPUP_SECN":
//                        return "SocialReactionCount";
//                    case "POST_SCRN_DTL_POST_CMNT_RCTN_POPUP_LST_VW_SECN":
//                        return "SocialReaction";
//                    case "POST_SCRN_DTL_POST_CMNT_VWS_POPUP_SECN":
//                            return "SocialViews";
////                    case "POST_SCRN_DETAIL_SECN":
////                        return "SocialPost";
////                    case "POST_SCRN_DETAIL_SECN":
////                        return "SocialPost";
////                    case "":
////                        return "SocialComment";
////                    case "":
////                        return "SocialRating";
//                }
//                break;
//            default:
//                return "none";
//        }
//        return "none";
//    }

    /**
	 * This method helps in building Hierarchical PE structure which helps in making data API call.
	 * @param requestData this contains input provided by the client call
	 * @param screenInstance this has definers values
	 * @param appLevelPropertyControls
     * @param targetScreenSectionCode
     * @return the PE request object
	 */
	private List<PERequest> buildPERequest(UnitRequestData requestData, ScreenInstanceVO screenInstance, PersonGroup group, List<SectionControlVO> appLevelPropertyControls, String targetScreenSectionCode) {
	    return null;
//        List<PERequest> rootPEList = new ArrayList<>();
//        String hardCodedElementName = getHardCodedElementName(screenInstance, targetScreenSectionCode);
//		if(hardCodedElementName.matches("Task|Note|NoteDetails|ActivityLog|Bookmark|SocialGroup|SocialPost|SocialComment|SocialRating|TaskDetails|analytics|SocialRatingCount|SocialReactionCount|SocialReaction|SocialViews|TaskList|NoteList")){
//            switch (hardCodedElementName){
//                case "Meeting":
//                    Map<String, PERequest> mtPEToPERequestMap = new HashMap<>();
//                    PERequest meetingPE = new PERequest();
//                    PERequest meetingInviteePE = new PERequest();
//                    meetingPE.setMtPE("Meeting");
//                    mtPEToPERequestMap.put(meetingPE.getMtPE(), meetingPE);
//                    meetingInviteePE.setMtPE("MeetingInvitee");
//                    mtPEToPERequestMap.put(meetingInviteePE.getMtPE(), meetingPE);
//                    meetingPE.setSelectType(RequestSelectType.LAYOUT);
//                    meetingInviteePE.setSelectType(RequestSelectType.LAYOUT);
//                    meetingPE.getChildrenPE().add(meetingInviteePE);
//            /* Add control attribute paths to meeting*/
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTitle}","!{Meeting.MeetingTitle}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingDescription}","!{Meeting.MeetingDescription}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingCategoryCode}","!{Meeting.MeetingCategoryCode}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingEventBackgroundImage}","!{Meeting.MeetingEventBackgroundImage}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingStartDateTime}","!{Meeting.MeetingStartDateTime}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingEndDateTime}","!{Meeting.MeetingEndDateTime}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.Location}","!{Meeting.Location}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.GeoLocation}","!{Meeting.GeoLocation}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTypeCode}","!{Meeting.MeetingTypeCode}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingCallInfoDetails}","!{Meeting.MeetingCallInfoDetails}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.Deleted}","!{Meeting.Deleted}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.AuthoredByPersonID}","!{Meeting.AuthoredByPersonID}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.AuthoredByPersonFullName}","!{Meeting.AuthoredByPersonFullName}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.AuthoredDateTime}","!{Meeting.AuthoredDateTime}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.LastModifiedByPersonID}","!{Meeting.LastModifiedByPersonID}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.LastModifiedByPersonFullName}","!{Meeting.LastModifiedByPersonFullName}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.LastModifiedDateTime}","!{Meeting.LastModifiedDateTime}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeName}","!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeName}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeIcon}","!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeIcon}");
//            /* Add control attributes for aggregate also */
//                    AttributePathDetails aggPathDetails =  new AttributePathDetails();
//                    aggPathDetails.setAttributePath("DBCOUNT(!{Meeting.MeetingTitle})");
//                    List<GroupByAttributeDetails> groupByDetails = new ArrayList<>();
//                    GroupByAttributeDetails attributeDetails = new GroupByAttributeDetails("!{Meeting.MeetingTypeCode}",1, false);
//                    GroupByAttributeDetails attributeDetails2 = new GroupByAttributeDetails("!{Meeting.PrimaryKeyID}",2, false);
//                    groupByDetails.add(attributeDetails);
//                    groupByDetails.add(attributeDetails2);
//                    aggPathDetails.setGroupByAttributes(groupByDetails);
//                    meetingPE.getAttributePaths().getAttributePathToAggDetailsMap().put(aggPathDetails.getAttributePath(),aggPathDetails);
//                    meetingPE.setKeyWord(requestData.getScreen().getKeyWord());
//                    //Meeting.AuthoredByPersonID
//            /* Add control attribute paths to meeting invitee*/
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID}","!{MeetingInvitee.InvitedPersonID}");
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.AttendeeStatusCode}","!{MeetingInvitee.AttendeeStatusCode}");
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.AttendeeStatusCode:AttendeeStatus.AttendeeStatusName}","!{MeetingInvitee.AttendeeStatusCode:AttendeeStatus.AttendeeStatusName}");
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.AttendeeStatusCode:AttendeeStatus.AttendeeStatusIcon}","!{MeetingInvitee.AttendeeStatusCode:AttendeeStatus.AttendeeStatusIcon}");
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.Photo}","!{MeetingInvitee.InvitedPersonID:Person.Photo}");
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.FormalFullName}","!{MeetingInvitee.InvitedPersonID:Person.FormalFullName}");
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.WorkEMailID}","!{MeetingInvitee.InvitedPersonID:Person.WorkEMailID}");
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.WorkPhone}","!{MeetingInvitee.InvitedPersonID:Person.WorkPhone}");
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.MobilePhone}","!{MeetingInvitee.InvitedPersonID:Person.MobilePhone}");
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.PrimaryJobDesignation}","!{MeetingInvitee.InvitedPersonID:Person.PrimaryJobDesignation}");
//                    meetingInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.WorkCity}","!{MeetingInvitee.InvitedPersonID:Person.WorkCity}");
//            /* Add control attributes for aggregate also */
//                    AttributePathDetails inviteeAggPathDetails =  new AttributePathDetails();
//                    inviteeAggPathDetails.setAttributePath("DBCOUNT(!{MeetingInvitee.InvitedPersonID})");
//                    List<GroupByAttributeDetails> groupByDetailsForInvitee = new ArrayList<>();
//                    GroupByAttributeDetails attributeDetails3 = new GroupByAttributeDetails("!{MeetingInvitee.AttendeeStatusCode}",1, false);
//                    GroupByAttributeDetails attributeDetails4 = new GroupByAttributeDetails("!{MeetingInvitee.InvitedPersonID}",2, false);
//                    groupByDetailsForInvitee.add(attributeDetails3);
////                    groupByDetailsForInvitee.add(attributeDetails4);
//                    inviteeAggPathDetails.setGroupByAttributes(groupByDetailsForInvitee);
//                    meetingInviteePE.getAttributePaths().getAttributePathToAggDetailsMap().put(inviteeAggPathDetails.getAttributePath(),inviteeAggPathDetails);
//                    // Here populate dependency key list
//                    DependencyKeysHelper keysHelper1 = new DependencyKeysHelper("MeetingInvitee.MeetingID", "Meeting.PrimaryKeyID");
//                    meetingInviteePE.getDependencyKeyList().add(keysHelper1);
//                    rootPEList.add(meetingPE);
//                    /* Meeting Attendee Status PE*/
//                    PERequest meetingAttendeeStatusPE = new PERequest();
//                    meetingAttendeeStatusPE.setMtPE("AttendeeStatus");
//                    mtPEToPERequestMap.put(meetingAttendeeStatusPE.getMtPE(), meetingAttendeeStatusPE);
//                    meetingAttendeeStatusPE.setSelectType(RequestSelectType.LAYOUT);
//                    meetingAttendeeStatusPE.getAttributePaths().getAttributePathsMap().put("!{AttendeeStatus.PrimaryKeyID}","!{AttendeeStatus.PrimaryKeyID}");
//                    rootPEList.add(meetingAttendeeStatusPE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(),mtPEToPERequestMap);
//                    break;
//                case "MeetingList":
//                    mtPEToPERequestMap = new HashMap<>();
//                    meetingPE = new PERequest();
//                    meetingPE.setMtPE("Meeting");
//                    mtPEToPERequestMap.put(meetingPE.getMtPE(), meetingPE);
//                    meetingPE.setSelectType(RequestSelectType.LAYOUT);
//            /* Add control attribute paths to meeting*/
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTitle}","!{Meeting.MeetingTitle}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingDescription}","!{Meeting.MeetingDescription}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingCategoryCode}","!{Meeting.MeetingCategoryCode}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingEventBackgroundImage}","!{Meeting.MeetingEventBackgroundImage}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingStartDateTime}","!{Meeting.MeetingStartDateTime}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingEndDateTime}","!{Meeting.MeetingEndDateTime}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.Location}","!{Meeting.Location}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.GeoLocation}","!{Meeting.GeoLocation}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTypeCode}","!{Meeting.MeetingTypeCode}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingCallInfoDetails}","!{Meeting.MeetingCallInfoDetails}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.Deleted}","!{Meeting.Deleted}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.AuthoredByPersonID}","!{Meeting.AuthoredByPersonID}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.AuthoredByPersonFullName}","!{Meeting.AuthoredByPersonFullName}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.AuthoredDateTime}","!{Meeting.AuthoredDateTime}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.LastModifiedByPersonID}","!{Meeting.LastModifiedByPersonID}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.LastModifiedByPersonFullName}","!{Meeting.LastModifiedByPersonFullName}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.LastModifiedDateTime}","!{Meeting.LastModifiedDateTime}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeName}","!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeName}");
//                    meetingPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeIcon}","!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeIcon}");
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(),mtPEToPERequestMap);
//                    rootPEList.add(meetingPE);
//                    break;
//                case "MeetingDetails":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest meetingDetailsPE = new PERequest();
//                    PERequest meetingDetailsInviteePE = new PERequest();
//                    PERequest meetingAgenda = new PERequest();
//                    PERequest meetingTakeaway = new PERequest();
//                    PERequest meetingAttachment = new PERequest();
//                    PERequest meetingIntegration = new PERequest();
//                    meetingDetailsPE.setMtPE("Meeting");
//                    meetingDetailsInviteePE.setMtPE("MeetingInvitee");
//                    meetingAgenda.setMtPE("MeetingAgenda");
//                    meetingTakeaway.setMtPE("MeetingTakeaway");
//                    meetingAttachment.setMtPE("MeetingAttachment");
//                    meetingIntegration.setMtPE("MeetingIntegration");
//                    mtPEToPERequestMap.put(meetingDetailsPE.getMtPE(), meetingDetailsPE);
//                    mtPEToPERequestMap.put(meetingDetailsInviteePE.getMtPE(), meetingDetailsInviteePE);
//                    mtPEToPERequestMap.put(meetingAgenda.getMtPE(), meetingAgenda);
//                    mtPEToPERequestMap.put(meetingTakeaway.getMtPE(), meetingTakeaway);
//                    mtPEToPERequestMap.put(meetingAttachment.getMtPE(), meetingAttachment);
//                    mtPEToPERequestMap.put(meetingIntegration.getMtPE(), meetingIntegration);
//                    meetingDetailsPE.setSelectType(RequestSelectType.LAYOUT);
//                    meetingAgenda.setSelectType(RequestSelectType.LAYOUT);
//                    meetingDetailsInviteePE.setSelectType(RequestSelectType.LAYOUT);
//                    meetingTakeaway.setSelectType(RequestSelectType.LAYOUT);
//                    meetingAttachment.setSelectType(RequestSelectType.LAYOUT);
//                    meetingIntegration.setSelectType(RequestSelectType.LAYOUT);
//                    meetingDetailsPE.getChildrenPE().add(meetingDetailsInviteePE);
//                    meetingDetailsPE.getChildrenPE().add(meetingAgenda);
//                    meetingDetailsPE.getChildrenPE().add(meetingTakeaway);
//                    meetingDetailsPE.getChildrenPE().add(meetingAttachment);
//                    meetingDetailsPE.getChildrenPE().add(meetingIntegration);
//            /* Add control attribute paths to meeting*/
//                    meetingDetailsPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTitle}","!{Meeting.MeetingTitle}");
//                    meetingDetailsPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeName}","!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeName}");
//                    meetingDetailsPE.getAttributePaths().getAttributePathsMap().put("!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeIcon}","!{Meeting.MeetingTypeCode:MeetingType.MeetingTypeIcon}");
//            /* Add property controls to meeting Details */
//                    SectionControlVO mdPropertyControl1 = new SectionControlVO();
//                    mdPropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    mdPropertyControl1.setPropertyControl(true);
//                    mdPropertyControl1.setMTPESpecificPC(true);
//                    meetingDetailsPE.getPropertyControlAttributes().add(mdPropertyControl1);
//                    SectionControlVO mdPropertyControl2 = new SectionControlVO();
//                    mdPropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    mdPropertyControl2.setPropertyControl(true);
//                    mdPropertyControl2.setMTPESpecificPC(true);
//                    meetingDetailsPE.getPropertyControlAttributes().add(mdPropertyControl2);
//                    //Meeting.AuthoredByPersonID
//            /* Add control attribute paths to meeting invitee*/
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID}","!{MeetingInvitee.InvitedPersonID}");
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.AttendeeStatusCode}","!{MeetingInvitee.AttendeeStatusCode}");
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.AttendeeStatusCode:AttendeeStatus.AttendeeStatusName}","!{MeetingInvitee.AttendeeStatusCode:AttendeeStatus.AttendeeStatusName}");
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.AttendeeStatusCode:AttendeeStatus.AttendeeStatusIcon}","!{MeetingInvitee.AttendeeStatusCode:AttendeeStatus.AttendeeStatusIcon}");
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.Photo}","!{MeetingInvitee.InvitedPersonID:Person.Photo}");
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.FormalFullName}","!{MeetingInvitee.InvitedPersonID:Person.FormalFullName}");
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.WorkEMailID}","!{MeetingInvitee.InvitedPersonID:Person.WorkEMailID}");
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.WorkPhone}","!{MeetingInvitee.InvitedPersonID:Person.WorkPhone}");
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.MobilePhone}","!{MeetingInvitee.InvitedPersonID:Person.MobilePhone}");
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.PrimaryJobDesignation}","!{MeetingInvitee.InvitedPersonID:Person.PrimaryJobDesignation}");
//                    meetingDetailsInviteePE.getAttributePaths().getAttributePathsMap().put("!{MeetingInvitee.InvitedPersonID:Person.WorkCity}","!{MeetingInvitee.InvitedPersonID:Person.WorkCity}");
//
//            /* Add property controls to meeting Invitee */
//                    SectionControlVO mInviteePropertyControl1 = new SectionControlVO();
//                    mInviteePropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    mInviteePropertyControl1.setPropertyControl(true);
//                    mInviteePropertyControl1.setMTPESpecificPC(true);
//                    meetingDetailsInviteePE.getPropertyControlAttributes().add(mInviteePropertyControl1);
//                    SectionControlVO mInviteePropertyControl2 = new SectionControlVO();
//                    mInviteePropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    mInviteePropertyControl2.setPropertyControl(true);
//                    mInviteePropertyControl2.setMTPESpecificPC(true);
//                    meetingDetailsInviteePE.getPropertyControlAttributes().add(mInviteePropertyControl2);
//            /* Add control attribute paths to meeting Agenda*/
//                    meetingAgenda.getAttributePaths().getAttributePathsMap().put("!{MeetingAgenda.PrimaryKeyID}","!{MeetingAgenda.PrimaryKeyID}");
//            /* Add property controls to meeting Agenda */
//                    SectionControlVO mAgendaPropertyControl1 = new SectionControlVO();
//                    mAgendaPropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    mAgendaPropertyControl1.setPropertyControl(true);
//                    mAgendaPropertyControl1.setMTPESpecificPC(true);
//                    meetingAgenda.getPropertyControlAttributes().add(mAgendaPropertyControl1);
//                    SectionControlVO mAgendaPropertyControl2 = new SectionControlVO();
//                    mAgendaPropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    mAgendaPropertyControl2.setPropertyControl(true);
//                    mAgendaPropertyControl2.setMTPESpecificPC(true);
//                    meetingAgenda.getPropertyControlAttributes().add(mAgendaPropertyControl2);
//            /* Add control attribute paths to meeting Takeaway*/
//                    meetingTakeaway.getAttributePaths().getAttributePathsMap().put("!{MeetingTakeaway.PrimaryKeyID}","!{MeetingTakeaway.PrimaryKeyID}");
//                    meetingTakeaway.getAttributePaths().getAttributePathsMap().put("!{MeetingTakeaway.TakeawayTypeCode:TakeawayType.TakeawayTypeName}","!{MeetingTakeaway.TakeawayTypeCode:TakeawayType.TakeawayTypeName}");
//                    meetingTakeaway.getAttributePaths().getAttributePathsMap().put("!{MeetingTakeaway.TakeawayTypeCode:TakeawayType.TakeawayTypeIcon}","!{MeetingTakeaway.TakeawayTypeCode:TakeawayType.TakeawayTypeIcon}");
//            /* Add property controls to meeting Takeaway*/
//                    SectionControlVO mdTakeawayPropertyControl1 = new SectionControlVO();
//                    mdTakeawayPropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    mdTakeawayPropertyControl1.setPropertyControl(true);
//                    mdTakeawayPropertyControl1.setMTPESpecificPC(true);
//                    meetingTakeaway.getPropertyControlAttributes().add(mdTakeawayPropertyControl1);
//                    SectionControlVO mdTakeawayPropertyControl2 = new SectionControlVO();
//                    mdTakeawayPropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    mdTakeawayPropertyControl2.setPropertyControl(true);
//                    mdTakeawayPropertyControl2.setMTPESpecificPC(true);
//                    meetingTakeaway.getPropertyControlAttributes().add(mdTakeawayPropertyControl2);
//            /* Add control attribute paths to meeting Attachment*/
//                    meetingAttachment.getAttributePaths().getAttributePathsMap().put("!{MeetingAttachment.PrimaryKeyID}","!{MeetingAttachment.PrimaryKeyID}");
//            /* Add property controls to meeting Integration*/
//                    SectionControlVO mdAttachPropertyControl1 = new SectionControlVO();
//                    mdAttachPropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    mdAttachPropertyControl1.setPropertyControl(true);
//                    mdAttachPropertyControl1.setMTPESpecificPC(true);
//                    meetingAttachment.getPropertyControlAttributes().add(mdAttachPropertyControl1);
//                    SectionControlVO mdAttachPropertyControl2 = new SectionControlVO();
//                    mdAttachPropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    mdAttachPropertyControl2.setPropertyControl(true);
//                    mdAttachPropertyControl2.setMTPESpecificPC(true);
//                    meetingAttachment.getPropertyControlAttributes().add(mdAttachPropertyControl2);
//            /* Add control attribute paths to meeting Attachment*/
//                    meetingIntegration.getAttributePaths().getAttributePathsMap().put("!{MeetingIntegration.MeetingID}","!{MeetingIntegration.MeetingID}");
//            /* Add property controls to meeting Attachments*/
//                    SectionControlVO mdIntegrationPropertyControl1 = new SectionControlVO();
//                    mdIntegrationPropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    mdIntegrationPropertyControl1.setPropertyControl(true);
//                    mdIntegrationPropertyControl1.setMTPESpecificPC(true);
//                    meetingIntegration.getPropertyControlAttributes().add(mdIntegrationPropertyControl1);
//                    SectionControlVO mdIntegrationPropertyControl2 = new SectionControlVO();
//                    mdIntegrationPropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    mdIntegrationPropertyControl2.setPropertyControl(true);
//                    mdIntegrationPropertyControl2.setMTPESpecificPC(true);
//                    meetingIntegration.getPropertyControlAttributes().add(mdIntegrationPropertyControl2);
//            //Add join clause for all the process Elements
//                    keysHelper1 = new DependencyKeysHelper("MeetingInvitee.MeetingID", "Meeting.PrimaryKeyID");
//                    meetingDetailsInviteePE.getDependencyKeyList().add(keysHelper1);
//                    DependencyKeysHelper agendaKeysHelper = new DependencyKeysHelper("MeetingAgenda.MeetingID", "Meeting.PrimaryKeyID");
//                    meetingAgenda.getDependencyKeyList().add(agendaKeysHelper);
//                    DependencyKeysHelper takeawayKeysHelper = new DependencyKeysHelper("MeetingTakeaway.MeetingID", "Meeting.PrimaryKeyID");
//                    meetingTakeaway.getDependencyKeyList().add(takeawayKeysHelper);
//                    DependencyKeysHelper attachementKeysHelper = new DependencyKeysHelper("MeetingAttachment.MeetingID", "Meeting.PrimaryKeyID");
//                    meetingAttachment.getDependencyKeyList().add(attachementKeysHelper);
//                    DependencyKeysHelper integrationKeysHelper = new DependencyKeysHelper("MeetingIntegration.MeetingID", "Meeting.PrimaryKeyID");
//                    meetingIntegration.getDependencyKeyList().add(integrationKeysHelper);
//                    rootPEList.add(meetingDetailsPE);
//                    /* Meeting Attendee Status PE*/
//                    meetingAttendeeStatusPE = new PERequest();
//                    meetingAttendeeStatusPE.setMtPE("AttendeeStatus");
//                    mtPEToPERequestMap.put(meetingAttendeeStatusPE.getMtPE(), meetingAttendeeStatusPE);
//                    meetingAttendeeStatusPE.setSelectType(RequestSelectType.LAYOUT);
//                    meetingAttendeeStatusPE.getAttributePaths().getAttributePathsMap().put("!{AttendeeStatus.PrimaryKeyID}","!{AttendeeStatus.PrimaryKeyID}");
//                    rootPEList.add(meetingAttendeeStatusPE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "Task":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest taskPE = new PERequest();
//                    PERequest taskAssigneePE = new PERequest();
//                    PERequest tags = new PERequest();
//                    taskPE.setMtPE("Task");
//                    taskAssigneePE.setMtPE("TaskAssignee");
//                    tags.setMtPE("Tag");
//                    taskPE.setSelectType(RequestSelectType.LAYOUT);
//                    taskAssigneePE.setSelectType(RequestSelectType.LAYOUT);
//                    tags.setSelectType(RequestSelectType.LAYOUT);
//                    taskPE.getChildrenPE().add(taskAssigneePE);
//                    taskPE.getChildrenPE().add(tags);
//            /* Add control attribute paths to task*/
//                    taskPE.getAttributePaths().getAttributePathsMap().put("!{Task.PrimaryKeyID}","!{Task.PrimaryKeyID}");
//                    taskPE.getAttributePaths().getAttributePathsMap().put("!{Task.TaskStatusCode:TaskStatus.TaskStatusIcon}","!{Task.TaskStatusCode:TaskStatus.TaskStatusIcon}");
//                    taskPE.getAttributePaths().getAttributePathsMap().put("!{Task.TaskStatusCode:TaskStatus.TaskStatusName}","!{Task.TaskStatusCode:TaskStatus.TaskStatusName}");
//                    taskPE.getAttributePaths().getAttributePathsMap().put("!{Task.TaskStatusCode:TaskStatus.OverrideFontColour}","!{Task.TaskStatusCode:TaskStatus.OverrideFontColour}");
//            /* Add control attribute paths to meeting invitee*/
//                    taskAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.PrimaryKeyID}","!{TaskAssignee.PrimaryKeyID}");
//                    taskAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.Photo}","!{TaskAssignee.TaskAssigneePerson:Person.Photo}");
//                    taskAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.FormalFullName}","!{TaskAssignee.TaskAssigneePerson:Person.FormalFullName}");
//                    taskAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.WorkEMailID}","!{TaskAssignee.TaskAssigneePerson:Person.WorkEMailID}");
//                    taskAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.WorkPhone}","!{TaskAssignee.TaskAssigneePerson:Person.WorkPhone}");
//                    taskAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.MobilePhone}","!{TaskAssignee.TaskAssigneePerson:Person.MobilePhone}");
//                    taskAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.PrimaryJobDesignation}","!{TaskAssignee.TaskAssigneePerson:Person.PrimaryJobDesignation}");
//                    taskAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.WorkCity}","!{TaskAssignee.TaskAssigneePerson:Person.WorkCity}");
//            /* Add control attribute paths to tags*/
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PrimaryKeyID}","!{Tag.PrimaryKeyID}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.TagText}","!{Tag.PersonTagMasterID:PersonTagMaster.TagText}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}","!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}","!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}");
//                    // Here populate dependency key list
//                    keysHelper1 = new DependencyKeysHelper("TaskAssignee.TaskID", "Task.PrimaryKeyID");
//                    DependencyKeysHelper keysHelper2 = new DependencyKeysHelper("Tag.DOPrimaryKey", "Task.PrimaryKeyID");
//                    taskAssigneePE.getDependencyKeyList().add(keysHelper1);
//                    tags.getDependencyKeyList().add(keysHelper2);
//                    rootPEList.add(taskPE);
//                    mtPEToPERequestMap.put(taskPE.getMtPE(), taskPE);
//                    mtPEToPERequestMap.put(taskAssigneePE.getMtPE(), taskAssigneePE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "TaskList":
//                    mtPEToPERequestMap = new HashMap<>();
//                    taskPE = new PERequest();
//                    tags = new PERequest();
//                    taskPE.setMtPE("Task");
//                    tags.setMtPE("Tag");
//                    taskPE.setSelectType(RequestSelectType.LAYOUT);
//                    tags.setSelectType(RequestSelectType.LAYOUT);
//                    taskPE.getChildrenPE().add(tags);
//            /* Add control attribute paths to meeting*/
//                    taskPE.getAttributePaths().getAttributePathsMap().put("!{Task.PrimaryKeyID}","!{Task.PrimaryKeyID}");
//                    taskPE.getAttributePaths().getAttributePathsMap().put("!{Task.TaskStatusCode:TaskStatus.TaskStatusIcon}","!{Task.TaskStatusCode:TaskStatus.TaskStatusIcon}");
//                    taskPE.getAttributePaths().getAttributePathsMap().put("!{Task.TaskStatusCode:TaskStatus.TaskStatusName}","!{Task.TaskStatusCode:TaskStatus.TaskStatusName}");
//                    taskPE.getAttributePaths().getAttributePathsMap().put("!{Task.TaskStatusCode:TaskStatus.OverrideFontColour}","!{Task.TaskStatusCode:TaskStatus.OverrideFontColour}");
//            /* Add control attribute paths to tags*/
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PrimaryKeyID}","!{Tag.PrimaryKeyID}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.TagText}","!{Tag.PersonTagMasterID:PersonTagMaster.TagText}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}","!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}","!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}");
//                    // Here populate dependency key list
//                    keysHelper2 = new DependencyKeysHelper("Tag.DOPrimaryKey", "Task.PrimaryKeyID");
//                    tags.getDependencyKeyList().add(keysHelper2);
//                    rootPEList.add(taskPE);
//                    mtPEToPERequestMap.put(taskPE.getMtPE(), taskPE);
//                    mtPEToPERequestMap.put(tags.getMtPE(), tags);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "TaskDetails":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest taskDetailsPE = new PERequest();
//                    PERequest taskDetailsAssigneePE = new PERequest();
//                    tags = new PERequest();
//                    taskDetailsPE.setMtPE("Task");
//                    taskDetailsAssigneePE.setMtPE("TaskAssignee");
//                    tags.setMtPE("Tag");
//                    taskDetailsPE.setSelectType(RequestSelectType.LAYOUT);
//                    taskDetailsAssigneePE.setSelectType(RequestSelectType.LAYOUT);
//                    tags.setSelectType(RequestSelectType.LAYOUT);
//                    taskDetailsPE.getChildrenPE().add(taskDetailsAssigneePE);
//                    taskDetailsPE.getChildrenPE().add(tags);
//                    rootPEList.add(taskDetailsPE);
//            /* Add control attribute paths to task Details*/
//                    taskDetailsPE.getAttributePaths().getAttributePathsMap().put("!{Task.PrimaryKeyID}","!{Task.PrimaryKeyID}");
//                    taskDetailsPE.getAttributePaths().getAttributePathsMap().put("!{Task.TaskStatusCode:TaskStatus.TaskStatusIcon}","!{Task.TaskStatusCode:TaskStatus.TaskStatusIcon}");
//                    taskDetailsPE.getAttributePaths().getAttributePathsMap().put("!{Task.TaskStatusCode:TaskStatus.TaskStatusName}","!{Task.TaskStatusCode:TaskStatus.TaskStatusName}");
//                    taskDetailsPE.getAttributePaths().getAttributePathsMap().put("!{Task.TaskStatusCode:TaskStatus.OverrideFontColour}","!{Task.TaskStatusCode:TaskStatus.OverrideFontColour}");
//            /* Add property controls to task Details */
//                    SectionControlVO tdpropertyControl1 = new SectionControlVO();
//                    tdpropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    tdpropertyControl1.setPropertyControl(true);
//                    tdpropertyControl1.setMTPESpecificPC(true);
//                    taskDetailsPE.getPropertyControlAttributes().add(tdpropertyControl1);
//                    SectionControlVO tdpropertyControl2 = new SectionControlVO();
//                    tdpropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    tdpropertyControl2.setPropertyControl(true);
//                    tdpropertyControl2.setMTPESpecificPC(true);
//                    taskDetailsPE.getPropertyControlAttributes().add(tdpropertyControl2);
//            /* Add control attribute paths to task assignees*/
//                    taskDetailsAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.PrimaryKeyID}","!{TaskAssignee.PrimaryKeyID}");
//                    taskDetailsAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.Photo}","!{TaskAssignee.TaskAssigneePerson:Person.Photo}");
//                    taskDetailsAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.FormalFullName}","!{TaskAssignee.TaskAssigneePerson:Person.FormalFullName}");
//                    taskDetailsAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.WorkEMailID}","!{TaskAssignee.TaskAssigneePerson:Person.WorkEMailID}");
//                    taskDetailsAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.WorkPhone}","!{TaskAssignee.TaskAssigneePerson:Person.WorkPhone}");
//                    taskDetailsAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.MobilePhone}","!{TaskAssignee.TaskAssigneePerson:Person.MobilePhone}");
//                    taskDetailsAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.PrimaryJobDesignation}","!{TaskAssignee.TaskAssigneePerson:Person.PrimaryJobDesignation}");
//                    taskDetailsAssigneePE.getAttributePaths().getAttributePathsMap().put("!{TaskAssignee.TaskAssigneePerson:Person.WorkCity}","!{TaskAssignee.TaskAssigneePerson:Person.WorkCity}");
//            /* Add property controls to task Details Assignee*/
//                    SectionControlVO tdAssPropertyControl1 = new SectionControlVO();
//                    tdAssPropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    tdAssPropertyControl1.setPropertyControl(true);
//                    tdAssPropertyControl1.setMTPESpecificPC(true);
//                    taskDetailsAssigneePE.getPropertyControlAttributes().add(tdAssPropertyControl1);
//                    SectionControlVO tdAssPropertyControl2 = new SectionControlVO();
//                    tdAssPropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    tdAssPropertyControl2.setPropertyControl(true);
//                    tdAssPropertyControl2.setMTPESpecificPC(true);
//                    taskDetailsAssigneePE.getPropertyControlAttributes().add(tdAssPropertyControl2);
//            /* Add control attribute paths to tags*/
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PrimaryKeyID}","!{Tag.PrimaryKeyID}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.TagText}","!{Tag.PersonTagMasterID:PersonTagMaster.TagText}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}","!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}","!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}");
//                    // Here populate dependency key list
//                    keysHelper1 = new DependencyKeysHelper("TaskAssignee.TaskID", "Task.PrimaryKeyID");
//                    keysHelper2 = new DependencyKeysHelper("Tag.DOPrimaryKey", "Task.PrimaryKeyID");
//                    taskDetailsAssigneePE.getDependencyKeyList().add(keysHelper1);
//                    tags.getDependencyKeyList().add(keysHelper2);
//            /* Add property controls to the tags */
//                    SectionControlVO propertyControlTags = new SectionControlVO();
//                    propertyControlTags.setPropertyControlExpn("getBTPEIcon");
//                    propertyControlTags.setPropertyControl(true);
//                    propertyControlTags.setMTPESpecificPC(true);
//                    tags.getPropertyControlAttributes().add(propertyControlTags);
//                    SectionControlVO propertyControlTags2 = new SectionControlVO();
//                    propertyControlTags2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControlTags2.setPropertyControl(true);
//                    propertyControlTags2.setMTPESpecificPC(true);
//                    tags.getPropertyControlAttributes().add(propertyControlTags2);
//            /* Adding PE for Task Attachment */
//                    PERequest taskAttachmentPE = new PERequest();
//                    taskAttachmentPE.setMtPE("TaskAttachment");
//                    taskAttachmentPE.setSelectType(RequestSelectType.LAYOUT);
//                    taskDetailsPE.getChildrenPE().add(taskAttachmentPE);
//                    taskAttachmentPE.getAttributePaths().getAttributePathsMap().put("!{TaskAttachment.PrimaryKeyID}","!{TaskAttachment.PrimaryKeyID}");
//                    DependencyKeysHelper keysHelperAttachment = new DependencyKeysHelper("TaskAttachment.TaskID","Task.PrimaryKeyID");
//                    taskAttachmentPE.getDependencyKeyList().add(keysHelperAttachment);
//            /* Add property controls to task Attachments*/
//                    SectionControlVO tdAttachPropertyControl1 = new SectionControlVO();
//                    tdAttachPropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    tdAttachPropertyControl1.setPropertyControl(true);
//                    tdAttachPropertyControl1.setMTPESpecificPC(true);
//                    taskAttachmentPE.getPropertyControlAttributes().add(tdAttachPropertyControl1);
//                    SectionControlVO tdAttachPropertyControl2 = new SectionControlVO();
//                    tdAttachPropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    tdAttachPropertyControl2.setPropertyControl(true);
//                    tdAttachPropertyControl2.setMTPESpecificPC(true);
//                    taskAttachmentPE.getPropertyControlAttributes().add(tdAttachPropertyControl2);
//            /* Adding PE for Task Integration */
//                    PERequest taskIntegrationPE = new PERequest();
//                    taskIntegrationPE.setMtPE("TaskIntegration");
//                    taskIntegrationPE.setSelectType(RequestSelectType.LAYOUT);
//                    taskDetailsPE.getChildrenPE().add(taskIntegrationPE);
//                    taskIntegrationPE.getAttributePaths().getAttributePathsMap().put("!{TaskIntegration.TaskID}","!{TaskIntegration.TaskID}");
//                    DependencyKeysHelper keysHelperIntegration = new DependencyKeysHelper("TaskIntegration.TaskID","Task.PrimaryKeyID");
//                    taskIntegrationPE.getDependencyKeyList().add(keysHelperIntegration);
//            /* Add property controls to meeting Integration*/
//                    SectionControlVO taskIntegrationPropertyControl1 = new SectionControlVO();
//                    taskIntegrationPropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    taskIntegrationPropertyControl1.setPropertyControl(true);
//                    taskIntegrationPropertyControl1.setMTPESpecificPC(true);
//                    taskIntegrationPE.getPropertyControlAttributes().add(taskIntegrationPropertyControl1);
//                    SectionControlVO taskIntegrationPropertyControl2 = new SectionControlVO();
//                    taskIntegrationPropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    taskIntegrationPropertyControl2.setPropertyControl(true);
//                    taskIntegrationPropertyControl2.setMTPESpecificPC(true);
//                    taskIntegrationPE.getPropertyControlAttributes().add(taskIntegrationPropertyControl2);
//            /* Adding PE for Task Status */
//                    PERequest taskStatus = new PERequest();
//                    taskStatus.setMtPE("TaskStatus");
//                    taskStatus.setSelectType(RequestSelectType.LAYOUT);
//                    taskStatus.getAttributePaths().getAttributePathsMap().put("!{TaskStatus.PrimaryKeyID}","!{TaskStatus.PrimaryKeyID}");
//                    rootPEList.add(taskStatus);
//                    mtPEToPERequestMap.put(taskDetailsPE.getMtPE(),taskDetailsPE);
//                    mtPEToPERequestMap.put(taskDetailsAssigneePE.getMtPE(),taskDetailsAssigneePE);
//                    mtPEToPERequestMap.put(taskIntegrationPE.getMtPE(), taskIntegrationPE);
//                    mtPEToPERequestMap.put(tags.getMtPE(), tags);
//                    mtPEToPERequestMap.put(taskAttachmentPE.getMtPE(), taskAttachmentPE);
//                    mtPEToPERequestMap.put(taskStatus.getMtPE(), taskStatus);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    /* Add property controls to meeting Integration*/
//                    SectionControlVO taskStatusPropertyControl1 = new SectionControlVO();
//                    taskStatusPropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    taskStatusPropertyControl1.setPropertyControl(true);
//                    taskStatusPropertyControl1.setMTPESpecificPC(true);
//                    taskStatus.getPropertyControlAttributes().add(taskStatusPropertyControl1);
//                    SectionControlVO taskStatusPropertyControl2 = new SectionControlVO();
//                    taskStatusPropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    taskStatusPropertyControl2.setPropertyControl(true);
//                    taskStatusPropertyControl2.setMTPESpecificPC(true);
//                    taskStatus.getPropertyControlAttributes().add(taskStatusPropertyControl2);
//                    break;
//                case "Note":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest notePE = new PERequest();
//                    PERequest noteShareTeamPE = new PERequest();
//                    PERequest noteSubTaskPE = new PERequest();
//                    tags = new PERequest();
//                    notePE.setMtPE("Note");
//                    noteShareTeamPE.setMtPE("NoteShareTeam");
//                    noteSubTaskPE.setMtPE("NoteSubTask");
//                    tags.setMtPE("Tag");
//                    mtPEToPERequestMap.put(notePE.getMtPE(), notePE);
//                    mtPEToPERequestMap.put(noteShareTeamPE.getMtPE(), noteShareTeamPE);
//                    mtPEToPERequestMap.put(noteSubTaskPE.getMtPE(), noteSubTaskPE);
//                    mtPEToPERequestMap.put(tags.getMtPE(), tags);
//                    notePE.setSelectType(RequestSelectType.LAYOUT);
//                    noteShareTeamPE.setSelectType(RequestSelectType.LAYOUT);
//                    noteSubTaskPE.setSelectType(RequestSelectType.LAYOUT);
//                    tags.setSelectType(RequestSelectType.LAYOUT);
//                    notePE.getChildrenPE().add(noteShareTeamPE);
//                    notePE.getChildrenPE().add(noteSubTaskPE);
//                    notePE.getChildrenPE().add(tags);
//            /* Add control attribute paths to meeting*/
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.PrimaryKeyID}","!{Note.PrimaryKeyID}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.PrimaryKeyID}","!{Note.AboutPerson:Person.PrimaryKeyID}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.Photo}","!{Note.AboutPerson:Person.Photo}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.FormalFullName}","!{Note.AboutPerson:Person.FormalFullName}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.WorkEMailID}","!{Note.AboutPerson:Person.WorkEMailID}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.WorkPhone}","!{Note.AboutPerson:Person.WorkPhone}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.MobilePhone}","!{Note.AboutPerson:Person.MobilePhone}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.PrimaryJobDesignation}","!{Note.AboutPerson:Person.PrimaryJobDesignation}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.WorkCity}","!{Note.AboutPerson:Person.WorkCity}");
//            /* Add property controls to the NotePE */
//                    SectionControlVO propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    notePE.getPropertyControlAttributes().add(propertyControl1);
//                    SectionControlVO propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    notePE.getPropertyControlAttributes().add(propertyControl2);
//            /* Add control attribute paths to meeting invitee*/
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.PrimaryKeyID}","!{NoteShareTeam.SharedPersonID:Person.PrimaryKeyID}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.Photo}","!{NoteShareTeam.SharedPersonID:Person.Photo}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.FormalFullName}","!{NoteShareTeam.SharedPersonID:Person.FormalFullName}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.WorkEMailID}","!{NoteShareTeam.SharedPersonID:Person.WorkEMailID}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.WorkPhone}","!{NoteShareTeam.SharedPersonID:Person.WorkPhone}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.MobilePhone}","!{NoteShareTeam.SharedPersonID:Person.MobilePhone}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.PrimaryJobDesignation}","!{NoteShareTeam.SharedPersonID:Person.PrimaryJobDesignation}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.WorkCity}","!{NoteShareTeam.SharedPersonID:Person.WorkCity}");
//            /* Add property controls to the NotePEShareTeam */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    noteShareTeamPE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    noteShareTeamPE.getPropertyControlAttributes().add(propertyControl2);
//            /* Add control attribute paths to meeting invitee*/
//                    noteSubTaskPE.getAttributePaths().getAttributePathsMap().put("!{NoteSubTask.SubTaskDescription}","!{NoteSubTask.SubTaskDescription}");
//                    noteSubTaskPE.getAttributePaths().getAttributePathsMap().put("!{NoteSubTask.Complete}","!{NoteSubTask.Complete}");
//            /* Add control attribute paths to meeting invitee*/
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PrimaryKeyID}","!{Tag.PrimaryKeyID}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.TagText}","!{Tag.PersonTagMasterID:PersonTagMaster.TagText}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}","!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}","!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}");
//            /* Add property controls to the NotePEShareTeam */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    tags.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    tags.getPropertyControlAttributes().add(propertyControl2);
//                    // Here populate dependency key list
//                    keysHelper1 = new DependencyKeysHelper("NoteShareTeam.NoteID", "Note.PrimaryKeyID");
//                    keysHelper2 = new DependencyKeysHelper("NoteSubTask.NoteID", "Note.PrimaryKeyID");
//                    DependencyKeysHelper keysHelper3 = new DependencyKeysHelper("Tag.DOPrimaryKey", "Note.PrimaryKeyID");
//                    noteShareTeamPE.getDependencyKeyList().add(keysHelper1);
//                    noteSubTaskPE.getDependencyKeyList().add(keysHelper2);
//                    tags.getDependencyKeyList().add(keysHelper3);
//                    rootPEList.add(notePE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "NoteList":
//                    mtPEToPERequestMap = new HashMap<>();
//                    notePE = new PERequest();
//                    tags = new PERequest();
//                    notePE.setMtPE("Note");
//                    tags.setMtPE("Tag");
//                    notePE.setSelectType(RequestSelectType.LAYOUT);
//                    tags.setSelectType(RequestSelectType.LAYOUT);
//                    notePE.getChildrenPE().add(tags);
//            /* Add control attribute paths to meeting*/
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.PrimaryKeyID}","!{Note.PrimaryKeyID}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.PrimaryKeyID}","!{Note.AboutPerson:Person.PrimaryKeyID}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.Photo}","!{Note.AboutPerson:Person.Photo}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.FormalFullName}","!{Note.AboutPerson:Person.FormalFullName}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.WorkEMailID}","!{Note.AboutPerson:Person.WorkEMailID}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.WorkPhone}","!{Note.AboutPerson:Person.WorkPhone}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.MobilePhone}","!{Note.AboutPerson:Person.MobilePhone}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.PrimaryJobDesignation}","!{Note.AboutPerson:Person.PrimaryJobDesignation}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.WorkCity}","!{Note.AboutPerson:Person.WorkCity}");
//            /* Add property controls to the NotePE */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    notePE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    notePE.getPropertyControlAttributes().add(propertyControl2);
//            /* Add control attribute paths to meeting invitee*/
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PrimaryKeyID}","!{Tag.PrimaryKeyID}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.TagText}","!{Tag.PersonTagMasterID:PersonTagMaster.TagText}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}","!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}","!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}");
//            /* Add property controls to the NotePEShareTeam */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    tags.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    tags.getPropertyControlAttributes().add(propertyControl2);
//                    // Here populate dependency key list
//                    keysHelper3 = new DependencyKeysHelper("Tag.DOPrimaryKey", "Note.PrimaryKeyID");
//                    tags.getDependencyKeyList().add(keysHelper3);
//                    rootPEList.add(notePE);
//                    mtPEToPERequestMap.put(notePE.getMtPE(), notePE);
//                    mtPEToPERequestMap.put(tags.getMtPE(), tags);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "NoteDetails":
//                    mtPEToPERequestMap = new HashMap<>();
//                    notePE = new PERequest();
//                    noteShareTeamPE = new PERequest();
//                    noteSubTaskPE = new PERequest();
//                    tags = new PERequest();
//                    PERequest noteAttachment = new PERequest();
//                    notePE.setMtPE("Note");
//                    noteShareTeamPE.setMtPE("NoteShareTeam");
//                    noteSubTaskPE.setMtPE("NoteSubTask");
//                    tags.setMtPE("Tag");
//                    noteAttachment.setMtPE("NoteAttachment");
//                    notePE.setSelectType(RequestSelectType.LAYOUT);
//                    noteShareTeamPE.setSelectType(RequestSelectType.LAYOUT);
//                    noteSubTaskPE.setSelectType(RequestSelectType.LAYOUT);
//                    tags.setSelectType(RequestSelectType.LAYOUT);
//                    noteAttachment.setSelectType(RequestSelectType.LAYOUT);
//                    notePE.getChildrenPE().add(noteShareTeamPE);
//                    notePE.getChildrenPE().add(noteSubTaskPE);
//                    notePE.getChildrenPE().add(tags);
//                    notePE.getChildrenPE().add(noteAttachment);
//            /* Add control attribute paths to meeting*/
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.PrimaryKeyID}","!{Note.PrimaryKeyID}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.PrimaryKeyID}","!{Note.AboutPerson:Person.PrimaryKeyID}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.Photo}","!{Note.AboutPerson:Person.Photo}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.FormalFullName}","!{Note.AboutPerson:Person.FormalFullName}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.WorkEMailID}","!{Note.AboutPerson:Person.WorkEMailID}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.WorkPhone}","!{Note.AboutPerson:Person.WorkPhone}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.MobilePhone}","!{Note.AboutPerson:Person.MobilePhone}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.PrimaryJobDesignation}","!{Note.AboutPerson:Person.PrimaryJobDesignation}");
//                    notePE.getAttributePaths().getAttributePathsMap().put("!{Note.AboutPerson:Person.WorkCity}","!{Note.AboutPerson:Person.WorkCity}");
//            /* Add property controls to the NotePE */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    notePE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    notePE.getPropertyControlAttributes().add(propertyControl2);
//            /* Add control attribute paths to meeting invitee*/
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.PrimaryKeyID}","!{NoteShareTeam.SharedPersonID:Person.PrimaryKeyID}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.Photo}","!{NoteShareTeam.SharedPersonID:Person.Photo}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.FormalFullName}","!{NoteShareTeam.SharedPersonID:Person.FormalFullName}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.WorkEMailID}","!{NoteShareTeam.SharedPersonID:Person.WorkEMailID}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.WorkPhone}","!{NoteShareTeam.SharedPersonID:Person.WorkPhone}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.MobilePhone}","!{NoteShareTeam.SharedPersonID:Person.MobilePhone}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.PrimaryJobDesignation}","!{NoteShareTeam.SharedPersonID:Person.PrimaryJobDesignation}");
//                    noteShareTeamPE.getAttributePaths().getAttributePathsMap().put("!{NoteShareTeam.SharedPersonID:Person.WorkCity}","!{NoteShareTeam.SharedPersonID:Person.WorkCity}");
//            /* Add property controls to the NotePEShareTeam */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    noteShareTeamPE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    noteShareTeamPE.getPropertyControlAttributes().add(propertyControl2);
//            /* Add control attribute paths to meeting invitee*/
//                    noteSubTaskPE.getAttributePaths().getAttributePathsMap().put("!{NoteSubTask.SubTaskDescription}","!{NoteSubTask.SubTaskDescription}");
//                    noteSubTaskPE.getAttributePaths().getAttributePathsMap().put("!{NoteSubTask.Complete}","!{NoteSubTask.Complete}");
//            /* Add property controls to the NoteSubTask */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    noteSubTaskPE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    noteSubTaskPE.getPropertyControlAttributes().add(propertyControl2);
//            /* Add control attribute paths to meeting invitee*/
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PrimaryKeyID}","!{Tag.PrimaryKeyID}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.TagText}","!{Tag.PersonTagMasterID:PersonTagMaster.TagText}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}","!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}","!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}");
//            /* Add property controls to the Tags */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    tags.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    tags.getPropertyControlAttributes().add(propertyControl2);
//                    // Here populate dependency key list
//                    keysHelper1 = new DependencyKeysHelper("NoteShareTeam.NoteID", "Note.PrimaryKeyID");
//                    keysHelper2 = new DependencyKeysHelper("NoteSubTask.NoteID", "Note.PrimaryKeyID");
//                    keysHelper3 = new DependencyKeysHelper("Tag.DOPrimaryKey", "Note.PrimaryKeyID");
//                    noteShareTeamPE.getDependencyKeyList().add(keysHelper1);
//                    noteSubTaskPE.getDependencyKeyList().add(keysHelper2);
//                    tags.getDependencyKeyList().add(keysHelper3);
//            /* Add property controls to note Attachments*/
//                    noteAttachment.getAttributePaths().getAttributePathsMap().put("!{NoteAttachment.PrimaryKeyID}","!{NoteAttachment.PrimaryKeyID}");
//                    keysHelperIntegration = new DependencyKeysHelper("NoteAttachment.NoteID","Note.PrimaryKeyID");
//                    noteAttachment.getDependencyKeyList().add(keysHelperIntegration);
//                    SectionControlVO ndAttachPropertyControl1 = new SectionControlVO();
//                    ndAttachPropertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    ndAttachPropertyControl1.setPropertyControl(true);
//                    ndAttachPropertyControl1.setMTPESpecificPC(true);
//                    noteAttachment.getPropertyControlAttributes().add(ndAttachPropertyControl1);
//                    SectionControlVO ndAttachPropertyControl2 = new SectionControlVO();
//                    ndAttachPropertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    ndAttachPropertyControl2.setPropertyControl(true);
//                    ndAttachPropertyControl2.setMTPESpecificPC(true);
//                    noteAttachment.getPropertyControlAttributes().add(ndAttachPropertyControl2);
//                    /* Adding PE for Note Integration */
//                    PERequest noteIntegrationPE = new PERequest();
//                    noteIntegrationPE.setMtPE("NoteIntegration");
//                    noteIntegrationPE.setSelectType(RequestSelectType.LAYOUT);
//                    notePE.getChildrenPE().add(noteIntegrationPE);
//                    noteIntegrationPE.getAttributePaths().getAttributePathsMap().put("!{NoteIntegration.NoteID}","!{NoteIntegration.NoteID}");
//                    keysHelperIntegration = new DependencyKeysHelper("NoteIntegration.NoteID","Note.PrimaryKeyID");
//                    noteIntegrationPE.getDependencyKeyList().add(keysHelperIntegration);
//            /* Add property controls to the Tags */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    noteIntegrationPE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    noteIntegrationPE.getPropertyControlAttributes().add(propertyControl2);
//                    rootPEList.add(notePE);
//                    mtPEToPERequestMap.put(notePE.getMtPE(), notePE);
//                    mtPEToPERequestMap.put(noteShareTeamPE.getMtPE(), noteShareTeamPE);
//                    mtPEToPERequestMap.put(noteSubTaskPE.getMtPE(), noteSubTaskPE);
//                    mtPEToPERequestMap.put(tags.getMtPE(), tags);
//                    mtPEToPERequestMap.put(noteAttachment.getMtPE(), noteAttachment);
//                    mtPEToPERequestMap.put(noteIntegrationPE.getMtPE(), noteIntegrationPE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "ActivityLog":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest activityLogPE = new PERequest();
//                    activityLogPE.setMtPE("ActivityLog");
//                    activityLogPE.setSelectType(RequestSelectType.LAYOUT);
//            /* Add control attribute paths to meeting*/
//                    activityLogPE.getAttributePaths().getAttributePathsMap().put("!{ActivityLog.PrimaryKeyID}","!{ActivityLog.PrimaryKeyID}");
//                    rootPEList.add(activityLogPE);
//                    mtPEToPERequestMap.put(activityLogPE.getMtPE(), activityLogPE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "Bookmark":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest bookmarkPE = new PERequest();
//                    bookmarkPE.setMtPE("Bookmark");
//                    bookmarkPE.setSelectType(RequestSelectType.LAYOUT);
//            /* Add control attribute paths to meeting*/
//                    bookmarkPE.getAttributePaths().getAttributePathsMap().put("!{Bookmark.PrimaryKeyID}","!{Bookmark.PrimaryKeyID}");
//                    rootPEList.add(bookmarkPE);
//                    mtPEToPERequestMap.put(bookmarkPE.getMtPE(), bookmarkPE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "SocialGroup":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest socialGroupPE = new PERequest();
//                    socialGroupPE.setMtPE("SocialGroup");
//                    PERequest socialGroupMembersPE = new PERequest();
//                    socialGroupMembersPE.setMtPE("SocialGroupMember");
//                    socialGroupPE.setSelectType(RequestSelectType.LAYOUT);
//                    socialGroupMembersPE.setSelectType(RequestSelectType.LAYOUT);
//                    socialGroupPE.getChildrenPE().add(socialGroupMembersPE);
//                    socialGroupPE.getAttributePaths().getAttributePathsMap().put("!{SocialGroup.PrimaryKeyID}","!{SocialGroup.PrimaryKeyID}");
//            /* Add control attribute paths to meeting*/
//                    socialGroupMembersPE.getAttributePaths().getAttributePathsMap().put("!{SocialGroupMember.PrimaryKeyID}","!{SocialGroupMember.PrimaryKeyID}");
//                    socialGroupMembersPE.getAttributePaths().getAttributePathsMap().put("!{SocialGroupMember.MemberPerson:Person.PrimaryKeyID}","!{SocialGroupMember.MemberPerson:Person.PrimaryKeyID}");
//                    socialGroupMembersPE.getAttributePaths().getAttributePathsMap().put("!{SocialGroupMember.MemberPerson:Person.Photo}","!{SocialGroupMember.MemberPerson:Person.Photo}");
//                    socialGroupMembersPE.getAttributePaths().getAttributePathsMap().put("!{SocialGroupMember.MemberPerson:Person.FormalFullName}","!{SocialGroupMember.MemberPerson:Person.FormalFullName}");
//                    socialGroupMembersPE.getAttributePaths().getAttributePathsMap().put("!{SocialGroupMember.MemberPerson:Person.WorkEMailID}","!{SocialGroupMember.MemberPerson:Person.WorkEMailID}");
//                    socialGroupMembersPE.getAttributePaths().getAttributePathsMap().put("!{SocialGroupMember.MemberPerson:Person.WorkPhone}","!{SocialGroupMember.MemberPerson:Person.WorkPhone}");
//                    socialGroupMembersPE.getAttributePaths().getAttributePathsMap().put("!{SocialGroupMember.MemberPerson:Person.MobilePhone}","!{NoteShareTeam.SharedPersonID:Person.MobilePhone}");
//                    keysHelper1 = new DependencyKeysHelper("SocialGroupMember.SocialGroup", "SocialGroup.PrimaryKeyID");
//                    socialGroupMembersPE.getDependencyKeyList().add(keysHelper1);
//            /* Add control attributes for aggregate also */
//                    AttributePathDetails groupMemberAggPathDetails =  new AttributePathDetails();
//                    groupMemberAggPathDetails.setAttributePath("DBCOUNT(!{SocialGroupMember.PrimaryKeyID})");
////                    List<GroupByAttributeDetails> groupByDetailsForInvitee = new ArrayList<>();
////                    GroupByAttributeDetails attributeDetails3 = new GroupByAttributeDetails("!{MeetingInvitee.AttendeeStatusCode}",1, false);
////                    GroupByAttributeDetails attributeDetails4 = new GroupByAttributeDetails("!{MeetingInvitee.InvitedPersonID}",2, false);
////                    groupByDetailsForInvitee.add(attributeDetails3);
////                    groupByDetailsForInvitee.add(attributeDetails4);
////                    groupMemberAggPathDetails.setGroupByAttributes(groupByDetailsForInvitee);
//                    socialGroupMembersPE.getAttributePaths().getAttributePathToAggDetailsMap().put(groupMemberAggPathDetails.getAttributePath(),groupMemberAggPathDetails);
//            /* Add property controls to the socialGroupMembersPE */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    socialGroupMembersPE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    socialGroupMembersPE.getPropertyControlAttributes().add(propertyControl2);
//            /* Add control attribute paths to tags*/
//                    tags = new PERequest();
//                    tags.setMtPE("Tag");
//                    tags.setSelectType(RequestSelectType.LAYOUT);
//                    socialGroupPE.getChildrenPE().add(tags);
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PrimaryKeyID}","!{Tag.PrimaryKeyID}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.TagText}","!{Tag.PersonTagMasterID:PersonTagMaster.TagText}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}","!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}");
//                    tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}","!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}");
//                    keysHelper3 = new DependencyKeysHelper("Tag.DOPrimaryKey", "SocialGroup.PrimaryKeyID");
//                    tags.getDependencyKeyList().add(keysHelper3);
//            /* Add control attribute paths to posts*/
//                    PERequest socialPostPE = new PERequest();
//                    socialPostPE.setMtPE("SocialPost");
//                    socialPostPE.setSelectType(RequestSelectType.LAYOUT);
//                    socialGroupPE.getChildrenPE().add(socialPostPE);
//                    AttributePathDetails socialPostAggPathDetails =  new AttributePathDetails();
//                    socialPostAggPathDetails.setAttributePath("DBCOUNT(!{SocialPost.PrimaryKeyID})");
//                    socialPostPE.getAttributePaths().getAttributePathToAggDetailsMap().put(socialPostAggPathDetails.getAttributePath(),socialPostAggPathDetails);
//                    keysHelper3 = new DependencyKeysHelper("SocialPost.SocialGroup", "SocialGroup.PrimaryKeyID");
//                    socialPostPE.getDependencyKeyList().add(keysHelper3);
//            /* Add property controls to the socialGroupMembersPE */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    socialPostPE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    socialPostPE.getPropertyControlAttributes().add(propertyControl2);
//                    rootPEList.add(socialGroupPE);
//                    mtPEToPERequestMap.put(socialGroupMembersPE.getMtPE(), socialGroupMembersPE);
//                    mtPEToPERequestMap.put(socialGroupPE.getMtPE(), socialGroupPE);
//                    mtPEToPERequestMap.put(socialPostPE.getMtPE(), socialPostPE);
//                    mtPEToPERequestMap.put(tags.getMtPE(), tags);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "SocialPost":
//                    mtPEToPERequestMap = new HashMap<>();
//                    socialPostPE = new PERequest();
//                    socialPostPE.setMtPE("SocialPost");
//                    socialPostPE.setSelectType(RequestSelectType.LAYOUT);
//                    socialPostPE.getAttributePaths().getAttributePathsMap().put("!{SocialPost.PrimaryKeyID}","!{SocialPost.PrimaryKeyID}");
//                    socialPostPE.getAttributePaths().getAttributePathsMap().put("!{SocialPost.AuthoredByPersonID:Person.PrimaryKeyID}","!{SocialPost.AuthoredByPersonID:Person.PrimaryKeyID}");
//                    socialPostPE.getAttributePaths().getAttributePathsMap().put("!{SocialPost.AuthoredByPersonID:Person.Photo}","!{SocialPost.AuthoredByPersonID:Person.Photo}");
//                    socialPostPE.getAttributePaths().getAttributePathsMap().put("!{SocialPost.AuthoredByPersonID:Person.FormalFullName}","!{SocialPost.AuthoredByPersonID:Person.FormalFullName}");
//                    socialPostPE.getAttributePaths().getAttributePathsMap().put("!{SocialPost.AuthoredByPersonID:Person.WorkEMailID}","!{SocialPost.AuthoredByPersonID:Person.WorkEMailID}");
//                    socialPostPE.getAttributePaths().getAttributePathsMap().put("!{SocialPost.AuthoredByPersonID:Person.WorkPhone}","!{SocialPost.AuthoredByPersonID:Person.WorkPhone}");
//                    socialPostPE.getAttributePaths().getAttributePathsMap().put("!{SocialPost.AuthoredByPersonID:Person.MobilePhone}","!{SocialPost.AuthoredByPersonID:Person.MobilePhone}");
//                    /* Now adding aggregate data to be send with post */
//                    PERequest postCommentPE = new PERequest();
//                    postCommentPE.setMtPE("SocialComment");
//                    postCommentPE.setSelectType(RequestSelectType.LAYOUT);
//                    socialPostPE.getChildrenPE().add(postCommentPE);
//                    //Add the join clause
//                    DependencyKeysHelper keysHelper = new DependencyKeysHelper("SocialComment.PrimaryKey","SocialPost.PrimaryKeyID");
//                    postCommentPE.getDependencyKeyList().add(keysHelper);
//                    AttributePathDetails commentsCountAttrDetails = new AttributePathDetails();
//                    commentsCountAttrDetails.setAttributePath("DBCOUNT(!{SocialComment.PrimaryKeyID})");
////                    commentsCountAttrDetails.setAggregateFunctionWhereClause("!{SocialComment.ParentSocialComment} IS NULL");
//                    postCommentPE.getAttributePaths().getAttributePathToAggDetailsMap().put(commentsCountAttrDetails.getAttributePath(),commentsCountAttrDetails);
//                    /* Add property controls to the postCommentPE */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    postCommentPE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    postCommentPE.getPropertyControlAttributes().add(propertyControl2);
//                    /* Adding PE for Reaction */
//                    PERequest postReactionPE = new PERequest();
//                    postReactionPE.setMtPE("SocialReaction");
//                    postReactionPE.setSelectType(RequestSelectType.LAYOUT);
//                    socialPostPE.getChildrenPE().add(postReactionPE);
//                    //Add the join clause
//                    DependencyKeysHelper keysHelperReaction = new DependencyKeysHelper("SocialReaction.PrimaryKey","SocialPost.PrimaryKeyID");
//                    postReactionPE.getDependencyKeyList().add(keysHelperReaction);
//                    Map<String, AttributePathDetails> reactionFuncToDetails = new HashMap<>();
//                    AttributePathDetails reactionCountAttrDetails = new AttributePathDetails();
//                    reactionCountAttrDetails.setAttributePath("DBCOUNT(!{SocialReaction.PrimaryKeyID})");
////                    reactionCountAttrDetails.setAggregateFunctionWhereClause("!{SocialComment.ParentSocialComment} IS NULL");
//                    postReactionPE.getAttributePaths().getAttributePathToAggDetailsMap().put(reactionCountAttrDetails.getAttributePath(),reactionCountAttrDetails);
//            /* Add property controls to the socialGroupMembersPE */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    postReactionPE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    postReactionPE.getPropertyControlAttributes().add(propertyControl2);
//                    /* Adding PE for Rating */
//                    PERequest postRatingPE = new PERequest();
//                    postRatingPE.setMtPE("SocialRating");
//                    postRatingPE.setSelectType(RequestSelectType.LAYOUT);
//                    socialPostPE.getChildrenPE().add(postRatingPE);
//                    //Add the join clause
//                    DependencyKeysHelper keysHelperRating = new DependencyKeysHelper("SocialRating.PrimaryKey","SocialPost.PrimaryKeyID");
//                    postRatingPE.getDependencyKeyList().add(keysHelperRating);
//                    AttributePathDetails reationCountAttrDetails = new AttributePathDetails();
//                    reationCountAttrDetails.setAttributePath("DBCOUNT(!{SocialRating.PrimaryKeyID})");
//                    postRatingPE.getAttributePaths().getAttributePathToAggDetailsMap().put(reationCountAttrDetails.getAttributePath(),reationCountAttrDetails);
//                    AttributePathDetails ratingAvgAttrDetails = new AttributePathDetails();
//                    ratingAvgAttrDetails.setAttributePath("DBAVERAGE(!{SocialRating.Rating})");
//                    postRatingPE.getAttributePaths().getAttributePathToAggDetailsMap().put(ratingAvgAttrDetails.getAttributePath(),ratingAvgAttrDetails);
//                    /* Adding PE for Post Attachment */
//                    PERequest postAttachmentPE = new PERequest();
//                    postAttachmentPE.setMtPE("SocialAttachment");
//                    postAttachmentPE.setSelectType(RequestSelectType.LAYOUT);
//                    socialPostPE.getChildrenPE().add(postAttachmentPE);
//                    postAttachmentPE.getAttributePaths().getAttributePathsMap().put("!{SocialAttachment.PrimaryKeyID}","!{SocialAttachment.PrimaryKeyID}");
//                    keysHelperIntegration = new DependencyKeysHelper("SocialAttachment.PrimaryKey","SocialPost.PrimaryKeyID");
//                    postAttachmentPE.getDependencyKeyList().add(keysHelperIntegration);
//                    /* Adding PE for Views */
//                    PERequest postsView = new PERequest();
//                    postsView.setMtPE("ActivityLog");
//                    postsView.setSelectType(RequestSelectType.LAYOUT);
//                    socialPostPE.getChildrenPE().add(postsView);
//                    //Add the join clause
//                    keysHelperReaction = new DependencyKeysHelper("ActivityLog.DOPrimaryKey","SocialPost.PrimaryKeyID");
//                    postsView.getDependencyKeyList().add(keysHelperReaction);
//                    reactionCountAttrDetails = new AttributePathDetails();
//                    reactionCountAttrDetails.setAttributePath("DBCOUNT(DISTINCT !{ActivityLog.ActivityAuthorPerson})");
//                    postsView.getAttributePaths().getAttributePathToAggDetailsMap().put(reactionCountAttrDetails.getAttributePath(),reactionCountAttrDetails);
//            /* Add property controls to the socialGroupMembersPE */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    postsView.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    postsView.getPropertyControlAttributes().add(propertyControl2);
//                    rootPEList.add(socialPostPE);
//                    /* Adding PE for Reaction type */
//                    PERequest reactionTypePE = new PERequest();
//                    reactionTypePE.setMtPE("SocialReactionType");
//                    reactionTypePE.setSelectType(RequestSelectType.LAYOUT);
//                    reactionTypePE.getAttributePaths().getAttributePathsMap().put("!{SocialReactionType.PrimaryKeyID}","!{SocialReactionType.PrimaryKeyID}");
//                    rootPEList.add(reactionTypePE);
//                    mtPEToPERequestMap.put(postCommentPE.getMtPE(), postCommentPE);
//                    mtPEToPERequestMap.put(postRatingPE.getMtPE(), postRatingPE);
//                    mtPEToPERequestMap.put(socialPostPE.getMtPE(), socialPostPE);
//                    mtPEToPERequestMap.put(postReactionPE.getMtPE(), postReactionPE);
//                    mtPEToPERequestMap.put(postAttachmentPE.getMtPE(), postAttachmentPE);
//                    mtPEToPERequestMap.put(postsView.getMtPE(), postsView);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "SocialComment":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest commentPE = new PERequest();
//                    commentPE.setMtPE("SocialComment");
//                    commentPE.setSelectType(RequestSelectType.LAYOUT);
//                    commentPE.getAttributePaths().getAttributePathsMap().put("!{SocialComment.PrimaryKeyID}","!{SocialComment.PrimaryKeyID}");
//                    commentPE.getAttributePaths().getAttributePathsMap().put("!{SocialComment.AuthoredByPersonID:Person.PrimaryKeyID}","!{SocialComment.AuthoredByPersonID:Person.PrimaryKeyID}");
//                    commentPE.getAttributePaths().getAttributePathsMap().put("!{SocialComment.AuthoredByPersonID:Person.Photo}","!{SocialComment.AuthoredByPersonID:Person.Photo}");
//                    commentPE.getAttributePaths().getAttributePathsMap().put("!{SocialComment.AuthoredByPersonID:Person.FormalFullName}","!{SocialComment.AuthoredByPersonID:Person.FormalFullName}");
//                    commentPE.getAttributePaths().getAttributePathsMap().put("!{SocialComment.AuthoredByPersonID:Person.WorkEMailID}","!{SocialComment.AuthoredByPersonID:Person.WorkEMailID}");
//                    commentPE.getAttributePaths().getAttributePathsMap().put("!{SocialComment.AuthoredByPersonID:Person.WorkPhone}","!{SocialComment.AuthoredByPersonID:Person.WorkPhone}");
//                    commentPE.getAttributePaths().getAttributePathsMap().put("!{SocialComment.AuthoredByPersonID:Person.MobilePhone}","!{SocialComment.AuthoredByPersonID:Person.MobilePhone}");
//                    /* Now adding aggregate data to be send with post */
//                    PERequest postCommentReplyPE = new PERequest();
//                    postCommentReplyPE.setMtPE("SocialComment");
//                    postCommentReplyPE.setSelectType(RequestSelectType.LAYOUT);
//                    commentPE.getChildrenPE().add(postCommentReplyPE);
//                    //Add the join clause
//                    keysHelper = new DependencyKeysHelper("SocialComment.PrimaryKey","SocialComment.PrimaryKeyID");
//                    postCommentReplyPE.getDependencyKeyList().add(keysHelper);
//                    commentsCountAttrDetails = new AttributePathDetails();
//                    commentsCountAttrDetails.setAttributePath("DBCOUNT(!{SocialComment.PrimaryKeyID})");
////                    commentsCountAttrDetails.setAggregateFunctionWhereClause("!{SocialComment.ParentSocialComment} IS NULL");
//                    postCommentReplyPE.getAttributePaths().getAttributePathToAggDetailsMap().put(commentsCountAttrDetails.getAttributePath(),commentsCountAttrDetails);
//                    /* Add property controls to the postCommentPE */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    postCommentReplyPE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    postCommentReplyPE.getPropertyControlAttributes().add(propertyControl2);
//                    /* Adding PE for Reaction */
//                    PERequest postCommentReactionPE = new PERequest();
//                    postCommentReactionPE.setMtPE("SocialReaction");
//                    postCommentReactionPE.setSelectType(RequestSelectType.LAYOUT);
//                    commentPE.getChildrenPE().add(postCommentReactionPE);
//                    //Add the join clause
//                    keysHelperReaction = new DependencyKeysHelper("SocialReaction.PrimaryKey","SocialComment.PrimaryKeyID");
//                    postCommentReactionPE.getDependencyKeyList().add(keysHelperReaction);
//                    reactionCountAttrDetails = new AttributePathDetails();
//                    reactionCountAttrDetails.setAttributePath("DBCOUNT(!{SocialReaction.PrimaryKeyID})");
//                    postCommentReactionPE.getAttributePaths().getAttributePathToAggDetailsMap().put(reactionCountAttrDetails.getAttributePath(),reactionCountAttrDetails);
//            /* Add property controls to the socialGroupMembersPE */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    postCommentReactionPE.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    postCommentReactionPE.getPropertyControlAttributes().add(propertyControl2);
//                    rootPEList.add(commentPE);
//                    /* Adding PE for Rating */
//                    PERequest commentRatingPE = new PERequest();
//                    commentRatingPE.setMtPE("SocialRating");
//                    commentRatingPE.setSelectType(RequestSelectType.LAYOUT);
//                    commentPE.getChildrenPE().add(commentRatingPE);
//                    //Add the join clause
//                    keysHelperRating = new DependencyKeysHelper("SocialRating.PrimaryKey","SocialComment.PrimaryKeyID");
//                    commentRatingPE.getDependencyKeyList().add(keysHelperRating);
//                    reationCountAttrDetails = new AttributePathDetails();
//                    reationCountAttrDetails.setAttributePath("DBCOUNT(!{SocialRating.PrimaryKeyID})");
//                    commentRatingPE.getAttributePaths().getAttributePathToAggDetailsMap().put(reationCountAttrDetails.getAttributePath(),reationCountAttrDetails);
//                    ratingAvgAttrDetails = new AttributePathDetails();
//                    ratingAvgAttrDetails.setAttributePath("DBAVERAGE(!{SocialRating.Rating})");
//                    commentRatingPE.getAttributePaths().getAttributePathToAggDetailsMap().put(ratingAvgAttrDetails.getAttributePath(),ratingAvgAttrDetails);
//                    /* Adding PE for Post Attachment */
//                    PERequest commentAttachmentPE = new PERequest();
//                    commentAttachmentPE.setMtPE("SocialAttachment");
//                    commentAttachmentPE.setSelectType(RequestSelectType.LAYOUT);
//                    commentPE.getChildrenPE().add(commentAttachmentPE);
//                    commentAttachmentPE.getAttributePaths().getAttributePathsMap().put("!{SocialAttachment.PrimaryKeyID}","!{SocialAttachment.PrimaryKeyID}");
//                    keysHelperIntegration = new DependencyKeysHelper("SocialAttachment.PrimaryKey","SocialComment.PrimaryKeyID");
//                    commentAttachmentPE.getDependencyKeyList().add(keysHelperIntegration);
//                    AttributePathDetails attachmentCountAttrDetails = new AttributePathDetails();
//                    attachmentCountAttrDetails.setAttributePath("DBCOUNT(!{SocialAttachment.PrimaryKeyID})");
//                    commentAttachmentPE.getAttributePaths().getAttributePathToAggDetailsMap().put(attachmentCountAttrDetails.getAttributePath(),attachmentCountAttrDetails);
//                    /* Adding PE for Views */
//                    PERequest commentsView = new PERequest();
//                    commentsView.setMtPE("ActivityLog");
//                    commentsView.setSelectType(RequestSelectType.LAYOUT);
//                    commentPE.getChildrenPE().add(commentsView);
//                    //Add the join clause
//                    keysHelperReaction = new DependencyKeysHelper("ActivityLog.DOPrimaryKey","SocialComment.PrimaryKeyID");
//                    commentsView.getDependencyKeyList().add(keysHelperReaction);
//                    reactionCountAttrDetails = new AttributePathDetails();
//                    reactionCountAttrDetails.setAttributePath("DBCOUNT(DISTINCT !{ActivityLog.ActivityAuthorPerson})");
//                    commentsView.getAttributePaths().getAttributePathToAggDetailsMap().put(reactionCountAttrDetails.getAttributePath(),reactionCountAttrDetails);
//            /* Add property controls to the socialGroupMembersPE */
//                    propertyControl1 = new SectionControlVO();
//                    propertyControl1.setPropertyControlExpn("getBTPEIcon");
//                    propertyControl1.setPropertyControl(true);
//                    propertyControl1.setMTPESpecificPC(true);
//                    commentsView.getPropertyControlAttributes().add(propertyControl1);
//                    propertyControl2 = new SectionControlVO();
//                    propertyControl2.setPropertyControlExpn("getBTPESummaryTitle");
//                    propertyControl2.setPropertyControl(true);
//                    propertyControl2.setMTPESpecificPC(true);
//                    commentsView.getPropertyControlAttributes().add(propertyControl2);
//                    /* Adding PE for Reaction type */
//                    reactionTypePE = new PERequest();
//                    reactionTypePE.setMtPE("SocialReactionType");
//                    reactionTypePE.setSelectType(RequestSelectType.LAYOUT);
//                    reactionTypePE.getAttributePaths().getAttributePathsMap().put("!{SocialReactionType.PrimaryKeyID}","!{SocialReactionType.PrimaryKeyID}");
//                    rootPEList.add(reactionTypePE);
//                    mtPEToPERequestMap.put(commentPE.getMtPE(), commentPE);
//                    mtPEToPERequestMap.put(commentRatingPE.getMtPE(), commentRatingPE);
//                    mtPEToPERequestMap.put(commentAttachmentPE.getMtPE(), commentAttachmentPE);
//                    mtPEToPERequestMap.put(postCommentReactionPE.getMtPE(), postCommentReactionPE);
//                    mtPEToPERequestMap.put(reactionTypePE.getMtPE(), reactionTypePE);
//                    mtPEToPERequestMap.put(commentsView.getMtPE(), commentsView);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "SocialRating":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest socialRatingPE = new PERequest();
//                    socialRatingPE.setMtPE("SocialRating");
//                    socialRatingPE.setSelectType(RequestSelectType.LAYOUT);
//                    socialRatingPE.getAttributePaths().getAttributePathsMap().put("!{SocialRating.PrimaryKeyID}","!{SocialRating.PrimaryKeyID}");
//                    socialRatingPE.getAttributePaths().getAttributePathsMap().put("!{SocialRating.AuthoredByPersonID:Person.PrimaryKeyID}","!{SocialRating.AuthoredByPersonID:Person.PrimaryKeyID}");
//                    socialRatingPE.getAttributePaths().getAttributePathsMap().put("!{SocialRating.AuthoredByPersonID:Person.Photo}","!{SocialRating.AuthoredByPersonID:Person.Photo}");
//                    socialRatingPE.getAttributePaths().getAttributePathsMap().put("!{SocialRating.AuthoredByPersonID:Person.FormalFullName}","!{SocialRating.AuthoredByPersonID:Person.FormalFullName}");
//                    socialRatingPE.getAttributePaths().getAttributePathsMap().put("!{SocialRating.AuthoredByPersonID:Person.WorkEMailID}","!{SocialRating.AuthoredByPersonID:Person.WorkEMailID}");
//                    socialRatingPE.getAttributePaths().getAttributePathsMap().put("!{SocialRating.AuthoredByPersonID:Person.WorkPhone}","!{SocialRating.AuthoredByPersonID:Person.WorkPhone}");
//                    socialRatingPE.getAttributePaths().getAttributePathsMap().put("!{SocialRating.AuthoredByPersonID:Person.MobilePhone}","!{SocialRating.AuthoredByPersonID:Person.MobilePhone}");
//                    rootPEList.add(socialRatingPE);
//                    mtPEToPERequestMap.put(socialRatingPE.getMtPE(), socialRatingPE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "SocialReaction":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest socialReactionPE = new PERequest();
//                    socialReactionPE.setMtPE("SocialReaction");
//                    socialReactionPE.setSelectType(RequestSelectType.LAYOUT);
//                    socialReactionPE.getAttributePaths().getAttributePathsMap().put("!{SocialReaction.PrimaryKeyID}","!{SocialReaction.PrimaryKeyID}");
//                    socialReactionPE.getAttributePaths().getAttributePathsMap().put("!{SocialReaction.AuthoredByPersonID:Person.PrimaryKeyID}","!{SocialReaction.AuthoredByPersonID:Person.PrimaryKeyID}");
//                    socialReactionPE.getAttributePaths().getAttributePathsMap().put("!{SocialReaction.AuthoredByPersonID:Person.Photo}","!{SocialReaction.AuthoredByPersonID:Person.Photo}");
//                    socialReactionPE.getAttributePaths().getAttributePathsMap().put("!{SocialReaction.AuthoredByPersonID:Person.FormalFullName}","!{SocialReaction.AuthoredByPersonID:Person.FormalFullName}");
//                    socialReactionPE.getAttributePaths().getAttributePathsMap().put("!{SocialReaction.AuthoredByPersonID:Person.WorkEMailID}","!{SocialReaction.AuthoredByPersonID:Person.WorkEMailID}");
//                    socialReactionPE.getAttributePaths().getAttributePathsMap().put("!{SocialReaction.AuthoredByPersonID:Person.WorkPhone}","!{SocialReaction.AuthoredByPersonID:Person.WorkPhone}");
//                    socialReactionPE.getAttributePaths().getAttributePathsMap().put("!{SocialReaction.AuthoredByPersonID:Person.MobilePhone}","!{SocialReaction.AuthoredByPersonID:Person.MobilePhone}");
//                    SortData sortData = new SortData("!{SocialReaction.AuthoredByPersonID}",true, 1, null);
//                    socialReactionPE.getAttributePaths().getStickyHeaderOrderByList().add(sortData);
//                    rootPEList.add(socialReactionPE);
//                    mtPEToPERequestMap.put(socialReactionPE.getMtPE(), socialReactionPE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "SocialRatingCount":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest socialRatingCountPE = new PERequest();
//                    socialRatingCountPE.setMtPE("SocialRating");
//                    socialRatingCountPE.setSelectType(RequestSelectType.LAYOUT);
//                    AttributePathDetails ratingCountAttrDetails = new AttributePathDetails();
//                    ratingCountAttrDetails.setAttributePath("DBCOUNT(!{SocialRating.PrimaryKeyID})");
//                    socialRatingCountPE.getAttributePaths().getAttributePathToAggDetailsMap().put(ratingCountAttrDetails.getAttributePath(),ratingCountAttrDetails);
//                    GroupByAttributeDetails groupByAttributeDetails = new GroupByAttributeDetails("!{SocialRating.Rating}",1, false);
//                    ratingCountAttrDetails.getGroupByAttributes().add(groupByAttributeDetails);
//                    rootPEList.add(socialRatingCountPE);
//                    mtPEToPERequestMap.put(socialRatingCountPE.getMtPE(), socialRatingCountPE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "SocialReactionCount":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest socialReactionCountPE = new PERequest();
//                    socialReactionCountPE.setMtPE("SocialReaction");
//                    socialReactionCountPE.setSelectType(RequestSelectType.LAYOUT);
//                    reationCountAttrDetails = new AttributePathDetails();
//                    reationCountAttrDetails.setAttributePath("DBCOUNT(!{SocialReaction.PrimaryKeyID})");
//                    socialReactionCountPE.getAttributePaths().getAttributePathToAggDetailsMap().put(reationCountAttrDetails.getAttributePath(),reationCountAttrDetails);
//                    groupByAttributeDetails = new GroupByAttributeDetails("!{SocialReaction.Reaction}",1, false);
//                    reationCountAttrDetails.getGroupByAttributes().add(groupByAttributeDetails);
//                    rootPEList.add(socialReactionCountPE);
//                    mtPEToPERequestMap.put(socialReactionCountPE.getMtPE(), socialReactionCountPE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "SocialViews":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest socialViews = new PERequest();
//                    socialViews.setMtPE("ActivityLog");
//                    socialViews.setExternalDistinct(true);
//                    socialViews.setSelectType(RequestSelectType.LAYOUT);
//                    socialViews.getAttributePaths().getAttributePathsMap().put("!{ActivityLog.PrimaryKeyID}","!{ActivityLog.PrimaryKeyID}");
//                    socialViews.getAttributePaths().getAttributePathsMap().put("!{ActivityLog.LoggedInPerson}","!{ActivityLog.LoggedInPerson}");
//                    socialViews.getAttributePaths().getAttributePathsMap().put("!{ActivityLog.LoggedInPerson:Person.PrimaryKeyID}","!{ActivityLog.LoggedInPerson:Person.PrimaryKeyID}");
//                    socialViews.getAttributePaths().getAttributePathsMap().put("!{ActivityLog.LoggedInPerson:Person.Photo}","!{ActivityLog.LoggedInPerson:Person.Photo}");
//                    socialViews.getAttributePaths().getAttributePathsMap().put("!{ActivityLog.LoggedInPerson:Person.FormalFullName}","!{ActivityLog.LoggedInPerson:Person.FormalFullName}");
//                    socialViews.getAttributePaths().getAttributePathsMap().put("!{ActivityLog.LoggedInPerson:Person.WorkEMailID}","!{ActivityLog.LoggedInPerson:Person.WorkEMailID}");
//                    socialViews.getAttributePaths().getAttributePathsMap().put("!{ActivityLog.LoggedInPerson:Person.WorkPhone}","!{ActivityLog.LoggedInPerson:Person.WorkPhone}");
//                    socialViews.getAttributePaths().getAttributePathsMap().put("!{ActivityLog.LoggedInPerson:Person.MobilePhone}","!{ActivityLog.LoggedInPerson:Person.MobilePhone}");
//                    rootPEList.add(socialViews);
//                    mtPEToPERequestMap.put(socialViews.getMtPE(), socialViews);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//                case "analytics":
//                    mtPEToPERequestMap = new HashMap<>();
//                    PERequest analyticsMetricPE = new PERequest();
//                    analyticsMetricPE.setMtPE("AnalyticsMetric");
//                    analyticsMetricPE.setSelectType(RequestSelectType.LAYOUT);
//                    analyticsMetricPE.getAttributePaths().getAttributePathsMap().put("!{AnalyticsMetric.PrimaryKeyID}","!{AnalyticsMetric.PrimaryKeyID}");
//                    rootPEList.add(analyticsMetricPE);
//                    mtPEToPERequestMap.put(analyticsMetricPE.getMtPE(), analyticsMetricPE);
//                    setFilterExpression(requestData.getScreen().getPeAliasToLoadParamsMap(), mtPEToPERequestMap);
//                    break;
//            }
//        }else{
//            ScreenRequest request = requestData.getScreen();
//            Map<String, LoadParameters> actionPEToLoadParamsMap = null;
//            if(StringUtil.isDefined(request.getControlActionInstanceId())){
//                actionPEToLoadParamsMap = getPEToLoadParamasMap(uiLayoutService.getLoadParameters("controlActionInstance",request.getControlActionInstanceId(),group));
//            }else if (StringUtil.isDefined(request.getSectionActionInstanceId())){
//                actionPEToLoadParamsMap = getPEToLoadParamasMap(uiLayoutService.getLoadParameters("sectionActionInstances",request.getSectionActionInstanceId(),group));
//            }
//            // Else get the parameters from client in request.
//            Map<String, LoadParameters> clientPEToLoadParamsMap = request.getPeAliasToLoadParamsMap();
//            Map<String, LoadParameters> sectionLoadParams = new HashMap<>();
//            List<ScreenSectionInstanceVO> screenSectionInstanceList = screenInstance.getScreenSectionInstances();
//            Map<String,PERequest> mtPEToPERequestMap = new HashMap<>();
//            for(ScreenSectionInstanceVO sectionInstance : screenSectionInstanceList){
//                //TODO Section instance has default sort and filter parameters by process element
//                //If on action no load parameters are provided then use on launch screen load parameters
//                Map<String, LoadParameters> peToLoadParamsMap = getPEToLoadParamasMap(sectionInstance.getLoadParams());
//                if(peToLoadParamsMap!=null)
//                    sectionLoadParams.putAll(peToLoadParamsMap);
//                List<SectionControlVO> controls = sectionInstance.getControls();
//                for(SectionControlVO control : controls){
//                    String processElement = control.getProcessElementFkId();
//                    String fkRelnWithParent = control.getFkRelationshipWithParentLngTxt();
//                    String mtPE = control.getProcessElementFkId();
//                    String parentPE = null;
//                    if(StringUtil.isDefined(fkRelnWithParent)){
//                        parentPE = fkRelnWithParent.split("\\|")[1].split("\\.")[0];
//                    }
//                    if(control.getAppSpecificPC()) {
//                        appLevelPropertyControls.add(control);
//                    }else {
//                        if(!control.getStaticControl()) {
//                            if (mtPEToPERequestMap.containsKey(mtPE)) {
//                                PERequest peRequest = mtPEToPERequestMap.get(mtPE);
//                                addNewAttributePath(peRequest, control, parentPE, appLevelPropertyControls);
//                            } else {
//                                PERequest newPERequest = new PERequest(processElement);
//                                newPERequest.setSelectType(requestData.getScreen().getSelectType());
//                                newPERequest.setProcessTypeCode(requestData.getScreen().getProcessTypeCode());
//                                addNewAttributePath(newPERequest, control, parentPE, appLevelPropertyControls);
//                                mtPEToPERequestMap.put(mtPE, newPERequest);
//                            }
//                        }
//                    }
//                }
//            }
//            // now we have three params to load map.
//            Map<String, LoadParameters> peToLoadParamsMap = mergeActionSectionAndUserParams(actionPEToLoadParamsMap, sectionLoadParams, clientPEToLoadParamsMap);
//            setFilterExpression(peToLoadParamsMap, mtPEToPERequestMap);
//            for(Map.Entry<String,PERequest> entry : mtPEToPERequestMap.entrySet()){
//                PERequest peRequest = entry.getValue();
////                // Change the group by control attribute also here
//                addGroupByDetailsToGroupByFunctions(peRequest);
//                String parentPE = peRequest.getParentPE();
////                if(peToLoadParamsMap!=null) {
////                    if(peToLoadParamsMap.get(peRequest.getMtPE())!=null) {
////                        String filterExpn = peToLoadParamsMap.get(peRequest.getMtPE()).getFilterExpn();
////                        peToLoadParamsMap.remove(peRequest.getMtPE());
////                        peRequest.setFilterExpression(filterExpn);
////                    }
////                }
//                if(StringUtil.isDefined(parentPE)) {
//                    mtPEToPERequestMap.get(parentPE).getChildrenPE().add(entry.getValue());
//                }else {
//                    peRequest.setKeyWord(request.getKeyWord());
//                    rootPEList.add(peRequest);
//                }
//            }
//        }
//		return rootPEList;
	}

    @Override
    public UnitResponseData getMenuExpressionResponse(Map<String, PERequest> mtPEToPERequestMap, Map<String, LoadParameters> peToLoadParams) {
//        setFilterExpression(peToLoadParams, mtPEToPERequestMap);
//        List<PERequest> rootPEList = new ArrayList<>();
//        UnitResponseData result = new UnitResponseData();
//        JsonLabelAliasTracker aliasTracker = new JsonLabelAliasTracker();
//        for(Map.Entry<String,PERequest> entry : mtPEToPERequestMap.entrySet()){
//            PERequest peRequest = entry.getValue();
//            String parentPE = peRequest.getParentPE();
//            if(StringUtil.isDefined(parentPE)) {
//                mtPEToPERequestMap.get(parentPE).getChildrenPE().add(entry.getValue());
//            }else {
//                rootPEList.add(peRequest);
//            }
//        }
//        for (PERequest rootPE : rootPEList) {
//            ProcessElementData peResponse = null;
//            try {
//                peResponse = getDataInternal(rootPE, aliasTracker, null);
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            result.getPEData().put(rootPE.getMtPE(), peResponse);
//        }
//        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
//        UserContext user = TenantContextHolder.getUserContext();
//        PersonGroup group = personService.getPersonGroups(user.getPersonId());
//        MetaHelper metaHelper = getLabelToAliasMap(aliasTracker, false, null,null,group);
//        result.setLabelToAliasMap(metaHelper.getLabelToAliasMap());
//        return result;
        return null;
    }

    private Map<String,LoadParameters> mergeActionSectionAndUserParams(Map<String, LoadParameters> actionPEToLoadParamsMap, Map<String, LoadParameters> sectionLoadParams, Map<String, LoadParameters> clientPEToLoadParamsMap) {
        Map<String,LoadParameters> peToLoadParamsMap = new HashMap<>();
        List<String> processElementsList = new ArrayList<>();
        if(actionPEToLoadParamsMap!=null){
            processElementsList.addAll(actionPEToLoadParamsMap.keySet());
        }
        if(sectionLoadParams!=null){
            processElementsList.addAll(sectionLoadParams.keySet());
        }
        if(clientPEToLoadParamsMap!=null){
            processElementsList.addAll(clientPEToLoadParamsMap.keySet());
        }
        for(String pe : processElementsList){
            if(actionPEToLoadParamsMap!=null && actionPEToLoadParamsMap.containsKey(pe)){
                peToLoadParamsMap.put(pe, actionPEToLoadParamsMap.get(pe));
            }else{
                // If it is present in clients map just honour that
                if(clientPEToLoadParamsMap!=null && clientPEToLoadParamsMap.containsKey(pe)){
                    peToLoadParamsMap.put(pe, clientPEToLoadParamsMap.get(pe));
                }else{
                    peToLoadParamsMap.put(pe, sectionLoadParams.get(pe));
                }
            }
        }
        if(clientPEToLoadParamsMap!=null)
            for(Map.Entry<String, LoadParameters> entry : clientPEToLoadParamsMap.entrySet()){
                LoadParameters finalPELoadParam = peToLoadParamsMap.get(entry.getKey());
                LoadParameters clientPELoadParam = entry.getValue();
                finalPELoadParam.setLogActivity(clientPELoadParam.isLogActivity());
                finalPELoadParam.setLogActivityMTPE(clientPELoadParam.getLogActivityMTPE());
            }
        return peToLoadParamsMap;
    }

    private void addGroupByDetailsToGroupByFunctions(PERequest peRequest) {
        Map<String, AttributePathDetails> doaExpnToAttrDetlsMap = peRequest.getAttributePaths().getAttributePathToAggDetailsMap();
        for(Map.Entry<String, AttributePathDetails> detailsEntry : doaExpnToAttrDetlsMap.entrySet()){
            AttributePathDetails attributePathDetails = detailsEntry.getValue();
            attributePathDetails.getGroupByAttributes().addAll(peRequest.getGroupByAttributeDetailsList());
            attributePathDetails.setAggregateFunctionWhereClause(peRequest.getFilterExpression());
        }
    }

//    private void setFilterExpression(Map<String, LoadParameters> peToLoadParam, Map<String, PERequest> mtPERequestMap) {
//        if(peToLoadParam!=null){
//            Map<String, List<PERequest>> toBeAddedToParent = new HashMap<>();
//            /* Now we have all the LoadParam require to run the call */
//            /* Now the point is client should tell you how to attach it to the above */
//            /* so for that you should ask client how do I do it */
//            for(Map.Entry<String, LoadParameters> parametersEntry : peToLoadParam.entrySet()){
//                String mtPE = parametersEntry.getKey();
//                LoadParameters loadParameters = parametersEntry.getValue();
//                if(mtPERequestMap.containsKey(mtPE)){
//                    mtPERequestMap.get(mtPE).setFilterExpression(loadParameters.getFilterExpn());
//                    Map<String, AttributePathDetails> aggregateDetails = mtPERequestMap.get(mtPE).getAttributePaths().getAttributePathToAggDetailsMap();
//                    for(Map.Entry<String, AttributePathDetails> entry : aggregateDetails.entrySet()){
//                        AttributePathDetails details = entry.getValue();
//                        details.setAggregateFunctionWhereClause(loadParameters.getFilterExpn());
//                    }
//                    mtPERequestMap.get(mtPE).setLogActivity(loadParameters.isLogActivity());
////                    mtPERequestMap.get(mtPE).setLogActivityMTPE(loadParameters.getLogActivityMTPE());
//                }else{
//                    /* This PE is getting added recently */
//                    /* Make a new PE Request */
//                    PERequest peRequest = new PERequest(mtPE);
//                    peRequest.setSelectType(RequestSelectType.LAYOUT);
//                    peRequest.setFilterExpression(loadParameters.getFilterExpn());
//                    String fkRelationWithParentPE = loadParameters.getFkRelationWithParent();
//                    String parentPE = null;
//                    if(StringUtil.isDefined(fkRelationWithParentPE)) {
//                        String[] relatedDoaSplit = fkRelationWithParentPE.split("\\|");
//                        DependencyKeysHelper keysHelper = new DependencyKeysHelper(relatedDoaSplit[0], relatedDoaSplit[1], mtPE, parentPE);
//                        parentPE = (relatedDoaSplit[1].split("\\."))[0];
//                        peRequest.setParentPEAlias(parentPE);
//                        peRequest.getDependencyKeyList().add(keysHelper);
//                        if(mtPERequestMap.containsKey(parentPE)){
//                            mtPERequestMap.get(parentPE).getChildrenPE().add(peRequest);
//                        }else{
//                            if(toBeAddedToParent.containsKey(parentPE)){
//                                toBeAddedToParent.get(parentPE).add(peRequest);
//                            }else{
//                                List<PERequest> childrenPEList = new ArrayList<>();
//                                childrenPEList.add(peRequest);
//                                toBeAddedToParent.put(parentPE, childrenPEList);
//                            }
//                        }
//                    }
//                    mtPERequestMap.put(mtPE, peRequest);
//                }
//            }
//            for(Map.Entry<String, List<PERequest>> entry : toBeAddedToParent.entrySet()){
//                String parentPE = entry.getKey();
//                List<PERequest> childrenList = entry.getValue();
//                for(PERequest childRequest : childrenList){
//                    mtPERequestMap.get(parentPE).getChildrenPE().add(childRequest);
//                }
//            }
//        }
//    }

    private Map<String,LoadParameters> getPEToLoadParamasMap(List<LoadParameters> loadParams) {
		Map<String,LoadParameters> result = new HashMap<>();
		for(LoadParameters param : loadParams){
			result.put(param.getProcessElementId(),param);
		}
		return result;
	}

	private void addNewAttributePath(PERequest peRequest, SectionControlVO control, String parentPE, List<SectionControlVO> appLevelPropertyControls) {
//        String fkRelationWithParentPE = control.getFkRelationshipWithParentLngTxt();
//        if(StringUtil.isDefined(fkRelationWithParentPE)) {
//            String[] relatedDoaSplit = fkRelationWithParentPE.split("\\|");
//            DependencyKeysHelper keysHelper = new DependencyKeysHelper(relatedDoaSplit[0], relatedDoaSplit[1]);
//            boolean isDependencyKeyToBeAdded = true;
//            for(DependencyKeysHelper existingKeys : peRequest.getDependencyKeyList()){
//                if(existingKeys.getCurrentDOACode().equals(relatedDoaSplit[0]) && existingKeys.getParentDOACode().equals(relatedDoaSplit[1])){
//                    isDependencyKeyToBeAdded = false;
//                }
//            }
//            if(isDependencyKeyToBeAdded)
//                peRequest.getDependencyKeyList().add(keysHelper);
//        }
//        String filterExpn = null;
////        if(peToLoadParamsMap!=null && peToLoadParamsMap.get(peRequest.getMtPE())!=null){
////            filterExpn = peToLoadParamsMap.get(peRequest.getMtPE()).getFilterExpn();
////            peRequest.setLogActivity(peToLoadParamsMap.get(peRequest.getMtPE()).isLogActivity());
////            if(StringUtil.isDefined(peToLoadParamsMap.get(peRequest.getMtPE()).getLogActivityMTPE())){
////                String[] logMTPEs = peToLoadParamsMap.get(peRequest.getMtPE()).getLogActivityMTPE().split(",");
////                if(logMTPEs.length==2) {
////                    peRequest.setLogActivityMTPE(logMTPEs[0]);
////                    peRequest.setLogActivityDtlMTPE(logMTPEs[1]);
////                }else{
////                    peRequest.setLogActivityMTPE(logMTPEs[0]);
////                }
////            }
////        }
//        if (control.getPropertyControl()) {
//            String[] splittedExpn = control.getControlAttributePathExpn().split("\\|");
//            for(String expn : splittedExpn){
//                SectionControlVO pCControl = buildNewPropertyControlFromExpn(expn);
//                peRequest.getPropertyControlAttributes().add(pCControl);
//            }
//        } else {
//            if (!control.getStaticControl()) {
//                AttributePaths attributePaths = peRequest.getAttributePaths();
//                peRequest.setParentPE(parentPE);
//                if(control.getAggregateControl()){
//                    /* If control is an aggregate control */
//                    /* What all details are required? */
//                    String attributePathExpn = control.getControlAttributePathExpn();
//                    AttributePathDetails attributePathDetails = new AttributePathDetails();
//                    attributePathDetails.setAttributePath(control.getControlAttributePathExpn());
////                    attributePathDetails.setAggregateFunction(control.getAggFunctionExpn());
//                    attributePathDetails.setAggregateFunctionHavingClause(control.getGroupByHavingExpn());
//                    attributePathDetails.setAggregateFunctionWhereClause(filterExpn);
//                    peRequest.getAttributePaths().getAttributePathToAggDetailsMap().put(attributePathDetails.getAttributePath(),attributePathDetails);
//                }else if(control.getGroupByControl()){
//                    GroupByAttributeDetails groupByAttributeDetails = new GroupByAttributeDetails();
//                    groupByAttributeDetails.setGroupByAttributePath(control.getControlAttributePathExpn());
//                    groupByAttributeDetails.setGroupByAttributeSeqNumber(control.getGroupByControlSeqNumPosInt());
//                    groupByAttributeDetails.setInternalChildGroupBy(false);
//                    peRequest.getGroupByAttributeDetailsList().add(groupByAttributeDetails);
//                }else{
//                    switch(ControlType.valueOfControlType(control.getControlTypeTxt())){
//                        case WRITE_TEXT_CONTROL:
//                            break;
//                        case BOOKMARKED:
//                            peRequest.setBookmarkRtrn(true);
//                            break;
//                        case FEATURED:
//                            PERequest featured = new PERequest();
//                            featured.setMtPE("Featured");
//                            featured.setSelectType(RequestSelectType.LAYOUT);
//                            peRequest.getChildrenPE().add(featured);
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.PrimaryKeyID}","!{Featured.PrimaryKeyID}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson}","!{Featured.FeaturingPerson}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.PrimaryKeyID}","!{Featured.FeaturingPerson:Person.PrimaryKeyID}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.Photo}","!{Featured.FeaturingPerson:Person.Photo}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.FormalFullName}","!{Featured.FeaturingPerson:Person.FormalFullName}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.WorkEMailID}","!{Featured.FeaturingPerson:Person.WorkEMailID}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.WorkPhone}","!{Featured.FeaturingPerson:Person.WorkPhone}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.MobilePhone}","!{Featured.FeaturingPerson:Person.MobilePhone}");
//                            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
//                            MasterEntityMetadataVOX meta = metadataService.getMetadataByMtPE(peRequest.getMtPE());
//                            DependencyKeysHelper keysHelper3 = new DependencyKeysHelper("Featured.DOPrimaryKey", meta.getFunctionalPrimaryKeyAttribute().getDoAttributeCode());
//                            featured.getDependencyKeyList().add(keysHelper3);
//                            break;
//                        case TAG:
//                            PERequest tags = new PERequest();
//                            tags.setMtPE("Tag");
//                            tags.setSelectType(RequestSelectType.LAYOUT);
//                            peRequest.getChildrenPE().add(tags);
//                            tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PrimaryKeyID}","!{Tag.PrimaryKeyID}");
//                            tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.TagText}","!{Tag.PersonTagMasterID:PersonTagMaster.TagText}");
//                            tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}","!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}");
//                            tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}","!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}");
//                            metadataService = TenantAwareCache.getMetaDataRepository();
//                            meta = metadataService.getMetadataByMtPE(peRequest.getMtPE());
//                            keysHelper3 = new DependencyKeysHelper("Tag.DOPrimaryKey", meta.getFunctionalPrimaryKeyAttribute().getDoAttributeCode());
//                            tags.getDependencyKeyList().add(keysHelper3);
//                            break;
//                        case ICON_LABEL_TEXT:
//                        case ICON_TEXT:
//                        case ICON_TEXT_ICON:
//                        case LABEL_TEXT:
//                        case LABEL:
//                        case LABEL_DATE:
//                        case LABEL_DATETIME:
//                        case LABEL_TIME:
//                        case DATETIME_RANGE_ICON:
//                        case IMAGE_CIRCLE_LABEL:
//                        case VERTICAL_CHECK_BOX:
//                            String attributePath = control.getControlAttributePathExpn();
//                            List<String> attributePathExpnList = Util.getPathExpnForHardcodedControlType(attributePath);
//                            for(int i =0; i < attributePathExpnList.size(); i++) {
//                                String expn = attributePathExpnList.get(i);
//                                if(Util.isPropertyControlExpn(expn)){
//                                    SectionControlVO newPC = buildNewPropertyControlFromExpn(expn);
//                                    if(newPC!=null){
//                                        peRequest.getPropertyControlAttributes().add(newPC);
//                                    }
//                                }else{
//                                    if (StringUtil.isDefined(expn) && !expn.equals("") && expn.contains("!{"))
//                                        attributePaths.getAttributePathsMap().put(expn, expn);
//                                    if(control.isControlDispInStickyHdr()){
//                                        addStickyHdrAttribute(control, attributePaths, i, expn);
//                                    }
//                                }
//                            }
//                            break;
//                        case RATING:
//                            break;
//                        case GRID:
//                        case REACTION:
//                            if(StringUtil.isDefined(control.getRelCtrlImgDispAttrPathExpn())|| StringUtil.isDefined(control.getRelCtrlFstDispAttrPathExpn())|| StringUtil.isDefined(control.getRelCtrlSndDispAttrPathExpn())|| StringUtil.isDefined(control.getRelCtrlRtrnDispAttrPathExpn())){
//                                // If either of the above is true add this control to Grid list
//                                peRequest.getGridControlIds().add(control.getControlDefinitionPkId());
//                            }
//                        case TEAM:
//                        case SINGLE_SELECT:
//                        case ATTACHMENT:
//                        case MULTI_SELECT:
//                            String cntrlAttributePath = control.getControlAttributePathExpn();
//                            if(StringUtil.isDefined(cntrlAttributePath))
//                                attributePaths.getAttributePathsMap().put(cntrlAttributePath, cntrlAttributePath);
//                            String addlAttributePath = control.getAddlAttrPathExpn();
//                            /* Split this additional attribute path expn by piped delim and add it to the pe request */
//                            if(StringUtil.isDefined(addlAttributePath)){
//                                String[] addAttributePathList = addlAttributePath.split("\\|");
//                                for(String path : addAttributePathList){
//                                    attributePaths.getAttributePathsMap().put(path, path);
//                                }
//                            }
//                            /* Now split the paths for rtrn attributes */
//                            if(StringUtil.isDefined(control.getRelCtrlRtrnDispAttrPathExpn())){
//                                String[] rtrnAttributes = control.getRelCtrlRtrnDispAttrPathExpn().split("\\|");
//                                List<String> rtrnAttributeList = new ArrayList<>();
//                                rtrnAttributeList.addAll(Arrays.asList(rtrnAttributes));
//                                if(StringUtil.isDefined(control.getStatusColourAttrPathExpn())){
//                                    rtrnAttributeList.add(control.getStatusColourAttrPathExpn());
//                                }
//                                for(String rtrnAttr : rtrnAttributeList){
//                                    List<String> attrList = Util.getAttributePathFromExpression(rtrnAttr);
//                                    String cntrolDOA = cntrlAttributePath.substring(2,cntrlAttributePath.indexOf("}"));
//                                    for(String attr : attrList){
//                                        String finalAttributePath = "!{"+cntrolDOA+":"+attr+"}";
//                                        attributePaths.getAttributePathsMap().put(finalAttributePath, finalAttributePath);
//                                    }
//                                }
//                            }
//                            break;
//                            default:
//                                if(Util.isPropertyControlExpn(control.getControlAttributePathExpn())){
//                                    SectionControlVO newPC = buildNewPropertyControlFromExpn(control.getControlAttributePathExpn());
//                                    if(newPC!=null){
//                                        peRequest.getPropertyControlAttributes().add(newPC);
//                                    }
//                                }else{
//                                    if(StringUtil.isDefined(control.getControlAttributePathExpn()) && !control.getControlAttributePathExpn().equals(""))
//                                        attributePaths.getAttributePathsMap().put(control.getControlAttributePathExpn(),control.getControlAttributePathExpn());
//                                    if(control.isControlDispInStickyHdr()){
//                                        addStickyHdrAttribute(control, attributePaths, control.setOrderBySeqNumber(), control.getControlAttributePathExpn());
//                                    }
//                                }
//                    }
//                    if(control.getDistinct()){
//                        peRequest.setExternalDistinct(true);
//                    }
//                }
//                if(control.getOrderBySeqNumber()!=0){
//                    boolean isASC = false;
//                    if(control.getOrderByMode().equals("ASC")){
//                        isASC = true;
//                    }
//                    SortData newSortData = new SortData(control.getControlAttributePathExpn(), isASC, control.getOrderBySeqNumber(), control.getOrderByValuesTxt());
//                    peRequest.getSortData().add(newSortData);
//                }
//            }
//        }
	}

    private void addStickyHdrAttribute(SectionControlVO control, AttributePaths attributePaths, int sequenceNumber, String expn) {
//        boolean isAscending = true;
//        if(control.getOrderByMode()!=null && control.getOrderByMode().equals("DESC"))
//            isAscending = false;
//        SortData sortData = new SortData(expn, isAscending, sequenceNumber,control.getOrderByValuesTxt());
//        attributePaths.getStickyHeaderOrderByList().add(sortData);
    }

    /**
     * TODO This is incomplete right now.
     * @param expn
     * @return
     */
    private SectionControlVO buildNewPropertyControlFromExpn(String expn) {
        return null;
//        String propertyControlCode = expn;
//        //Check if it is of function kind or not.
//        if(expn.contains("(")){
//            propertyControlCode = expn.substring(0, expn.indexOf("("));
//        }
//        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
//        PropertyControlMaster propertyControl = metadataService.getPropertyControlByCode(propertyControlCode);
//	    /* get all the property controls */
//	    /* now here check if the start matches with any of the property control if it does make the proper property control */
//	    SectionControlVO propertyControlSection = null;
//	    if(propertyControl!=null){
//            propertyControlSection = new SectionControlVO();
//            propertyControlSection.setPropertyControl(true);
//            propertyControlSection.setAppSpecificPC(propertyControl.isAppSpecific());
//            propertyControlSection.setMTPESpecificPC(propertyControl.isMtPESpecific());
//            propertyControlSection.setAttributeSpecificPC(propertyControl.isAttributeSpecific());
//            propertyControlSection.setRecordSpecificPC(propertyControl.isRecordSpecific());
//            propertyControlSection.setPropertyControlExpn(expn);
//            propertyControlSection.setControlAttributePathExpn(expn);
//        }
//        return propertyControlSection;
    }
    /**
	 * this private method resolves the data internally.
	 * @param rootPE
     * @param baseTemplate
     * @param responseData
     * @return Returns the response in the format required by client.
	 * @throws IOException
	 * @throws SQLException
	 */
    @Override
    @Transactional
	public ProcessElementData getDataInternal(PERequest rootPE, JsonLabelAliasTracker aliasTracker, String baseTemplate, UnitResponseData responseData) throws IOException, SQLException {
        ProcessElementData rootPEData = getRootProcessElementData(rootPE, aliasTracker, baseTemplate,responseData);
        processHierarchicalProcessElements(rootPEData, rootPE, aliasTracker, baseTemplate, responseData);
        return rootPEData;
	}

    @Override
    @Transactional
    public Object getBreakedUpData(String contextType, String contextID) {
        if(contextType == null)
            return null;
        switch (contextType){
            case "ORGANIZATION":
            case "LABEL_ORGANIZATION":
                List<OrganizationBreakUpData> organizationBreakUpDataList =  uiLayoutService.getOrganizationBreakUpData(contextID);
                // Now if it is organization get the
                return organizationBreakUpDataList;
            case "LOCATION":
                List<LocationBreakUpData> locationBreakUpDataList = uiLayoutService.getLocationBreakUpData(contextID);
                return locationBreakUpDataList;
            case "COST_CENTRE":
                List<CostCentreBreakUpData> costCentreBreakUpDataList = uiLayoutService.getCostCentreBreakUpData(contextID);
                return costCentreBreakUpDataList;
            case "JOB_FAMILY":
                List<JobFamilyBreakUpData> jobFamilyBreakUpDataList = uiLayoutService.getJobFamilyBreakUpData(contextID);
                return jobFamilyBreakUpDataList;
            default: return null;
        }
    }

    @Override
    public List<JobRelationships> getDefaultJobRelations(String contextID1, String contextID2) {
        return uiLayoutService.getDefaultJobRelations(contextID1, contextID2);
    }

    /**
	 * @param parentPEData
	 * @param parentPE
     * @param baseTemplate
     * @param responseData
     * @throws IOException
	 */
	private void processHierarchicalProcessElements(ProcessElementData parentPEData, PERequest parentPE, JsonLabelAliasTracker aliasTracker, String baseTemplate, UnitResponseData responseData) throws IOException, SQLException {
		if(parentPE.getChildrenPE()!=null && !parentPE.getChildrenPE().isEmpty() && !parentPEData.getRecordSetInternal().isEmpty()) {
			for (PERequest childPE : parentPE.getChildrenPE()) {
				/*
				 * For this child PE prepare the searchResult
				 * Get the process element data
				 * and again call this method
				 */
				ProcessElementData childPEData = getChildProcessElementData(childPE, parentPEData, aliasTracker, baseTemplate, responseData);
				processHierarchicalProcessElements(childPEData, childPE, aliasTracker, baseTemplate, responseData);
			}
		}
	}
	/**
	 * @param childPE
	 * @param parentPEData
     * @param baseTemplate
     * @param responseData
     * @return
	 * @throws IOException 
	 */
	private ProcessElementData getChildProcessElementData(PERequest childPE, ProcessElementData parentPEData, JsonLabelAliasTracker aliasTracker, String baseTemplate, UnitResponseData responseData) throws IOException, SQLException {
		Map<DefaultSearchData, SearchResult> aggregateSearchDataToSearchResultMap = new HashMap<>();
		DefaultSearchData searchData = createChildSearchData(childPE, parentPEData, aliasTracker, responseData);
        SearchResult searchResult = null;
        ProcessElementData childPEData = new ProcessElementData();
        childPEData.setMtPE(childPE.getMtPE());
        childPEData.setMtPEAlias(childPE.getMtPEAlias());
		if(!searchData.isNoSelectToBeMade()) {
            searchResult = searchService.doSearch(searchData);
            childPEData = responseDataBuilder.createProcessElementData(searchData, searchResult, childPE, aliasTracker, baseTemplate, false);
        }
        for (DefaultSearchData aggSearchData : searchData.getAggregateStmts()) {
            SearchResult aggregateResult = searchService.doSearch(aggSearchData);
            aggregateSearchDataToSearchResultMap.put(aggSearchData, aggregateResult);
        }
        Map<String, AggregateData> aggregateDataResult = responseDataBuilder.createAggregateResult(aggregateSearchDataToSearchResultMap, baseTemplate);
        childPEData.setAggregateResult(aggregateDataResult);
        for(String gridControlId : childPE.getGridControlIds()){
            RelationResponse relationResponse = getRelationControlData(gridControlId, aliasTracker);
            childPEData.getCtrlIdToGridData().put(gridControlId, relationResponse.getRecordSet());
        }
        responseDataBuilder.mergeParentAndChildData(searchData, childPEData, aliasTracker, childPE.getPropertyControlAttributes());
		return childPEData;
	}
	/**
	 * @param processElement
     * @param baseTemplate
     * @param responseData
     * @return
	 * @throws IOException 
	 */
	private ProcessElementData getRootProcessElementData(PERequest processElement, JsonLabelAliasTracker aliasTracker, String baseTemplate, UnitResponseData responseData) throws IOException, SQLException {
		Map<DefaultSearchData, SearchResult> aggregateSearchDataToSearchResultMap = new HashMap<>();
        DefaultSearchData searchData = createRootSearchData(processElement, responseData);
        ProcessElementData rootPEData = new ProcessElementData();
        rootPEData.setMtPE(processElement.getMtPE());
        if(!searchData.isNoSelectToBeMade()) {
            if(searchData.isReturnOnlySqlQuery()){
                SqlQuery sqlQuery = searchService.getSqlQuery(searchData);
                rootPEData.setSqlQuery(sqlQuery);
            }else {
                SearchResult searchResult = searchService.doSearch(searchData);
                rootPEData = responseDataBuilder.createProcessElementData(searchData, searchResult, processElement, aliasTracker, baseTemplate, true);
            }
        }
        for (DefaultSearchData aggSearchData : searchData.getAggregateStmts()) {
            SearchResult aggregateResult = searchService.doSearch(aggSearchData);
            aggregateSearchDataToSearchResultMap.put(aggSearchData, aggregateResult);
        /* add this aggregate search result at proper place */
        }
        Map<String, AggregateData> aggregateDataResult = responseDataBuilder.createAggregateResult(aggregateSearchDataToSearchResultMap, baseTemplate);
        rootPEData.setAggregateResult(aggregateDataResult);
        for(String gridControlId : processElement.getGridControlIds()){
            RelationResponse relationResponse = getRelationControlData(gridControlId, aliasTracker);
            rootPEData.getCtrlIdToGridData().put(gridControlId, relationResponse.getRecordSet());
        }
        return rootPEData;
	}
	/**
	 * @param childPE
	 * @param parentPEData
	 * @param aliasTracker
     * @param responseData
     * @return
	 */
	private DefaultSearchData createChildSearchData(PERequest childPE, ProcessElementData parentPEData, JsonLabelAliasTracker aliasTracker, UnitResponseData responseData) throws SQLException{
        String mtPE = childPE.getMtPE();
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadataVOX childMTMeta = metadataService.getMetadataByMtPE(mtPE);
		DefaultSearchData searchData = new DefaultSearchData();
		searchData.setPage(childPE.getPage());
		searchData.setPageSize(childPE.getPageSize());
		searchData.setApplyPagination(false);
		searchData.setDependencyKeyList(childPE.getDependencyKeyList());
		searchData.setResolveG11n(childPE.isResolveG11n());
        searchData.setLogActivity(childPE.isLogActivity());
        searchData.setLogActivityMTPE(childMTMeta.getActivityPE());
        searchData.setLogActivityDtlMTPE(childMTMeta.getActivityDetailsPE());
        searchData.setSubjectUserPath(childPE.getSubjectUserPath());
//		String baseTemplateId = childPE.getBaseTemplateId();
		RequestSelectType selectType = childPE.getSelectType();
//		searchData.setBaseTemplateId(baseTemplateId);
		searchData.setMtPE(mtPE);
		searchData.setMtPEAlias(childPE.getMtPEAlias());
        List<String> allAttributePathsAddedToSelectClause = new ArrayList<>();
		/*
		 * Making of Select statement
		 */
        AttributePaths attributePaths = null;
//		if(selectType==RequestSelectType.LAYOUT){
            attributePaths = childPE.getAttributePaths();
//        }else {
//            attributePaths = metadataService.getAttributePathsForPE(null, mtPE, selectType);
//        }
		Set<String> attributePathSet = attributePaths.getAttributePaths();
		/*
		Make SELECT statement only when Attribute paths are present for it.
		 */
		if((attributePathSet!=null && !attributePathSet.isEmpty()) || attributePaths.getAttributePathToAggDetailsMap()!=null && !attributePaths.getAttributePathToAggDetailsMap().isEmpty()){
		    /*
                Making of dependency mapped parent data
            */
            DependencyMappedParentData mappedParentData = makeDependencyMappedParentData(childPE.getDependencyKeyList(),parentPEData, aliasTracker);
            if(mappedParentData!=null && mappedParentData.getDependencyResultMap()!=null && !mappedParentData.getDependencyResultMap().isEmpty() && attributePathSet!=null && !attributePathSet.isEmpty()) {
                 searchData.getAttributePaths().addAll(attributePathSet);
                for (String attributePathExpn : attributePathSet) {
                    addAttributePathExpnToJoinClause(attributePathExpn, searchData);
                    addAttributeExpnToSelectClause(searchData,attributePathExpn,false, true, true, allAttributePathsAddedToSelectClause);
                }
                /* Add the current dependecy DOA path to select clause */
                for(DependencyKeysHelper dependencyKeysHelper : childPE.getDependencyKeyList()){
                    String currentDOACode = dependencyKeysHelper.getCurrentDOACode();
                    searchData.getAttributePaths().add(currentDOACode);
                    addAttributePathToJoinClause(currentDOACode, searchData);
                    addAttributePathToSelectClause(searchData, currentDOACode, false);
                }
                /*
            Add the fk-relation parent doa to the select clause. So that when it is searched for we are able to find it.
             */
                for(PERequest childchildPE : childPE.getChildrenPE()){
                    // Get the dependencyKeyList
                    for(DependencyKeysHelper dependencyKey : childchildPE.getDependencyKeyList()){
                        String parentDOACode = dependencyKey.getParentDOACode();
                        // This is to be added to the select clause
                        searchData.getAttributePaths().add(parentDOACode);
                        addAttributePathToJoinClause(parentDOACode, searchData);
                        addAttributePathToSelectClause(searchData, parentDOACode, false);
                    }
                }
            /*
            "getBTPEIcon|getBTPESummaryTitle" -> "getBTPEIcon|getBTPESummaryTitle"
             * Add filter to Child Process Element
             */
                addFiltersToChildSelect(childPE, searchData, mappedParentData.getDependencyResultArray(), responseData);
            /*
             * Add order BY to Child Process Element
             */
                addBookmarkTable(childPE, searchData);
                addEndorseTable(childPE, searchData);
                addMenuPermissionsTable(childPE, searchData, responseData);
                addOrderByClause(searchData, childPE, attributePaths.getStickyHeaderOrderByList(), allAttributePathsAddedToSelectClause);
                if(!searchData.isExternalDistinct()) {
                    for(String attributePath : allAttributePathsAddedToSelectClause) {
                        addAttributePathToSelectClause(searchData, attributePath, true);
                    }
                }
                /* Set DB and functional primary key */
                String functionalPrimaryKeyDOACode = childMTMeta.getPrimaryKeyAttribute().getDoAttributeCode();
                String databasePrimaryKeyDOACode = childMTMeta.getVersionIdAttribute().getDoAttributeCode();
                String doCode = childMTMeta.getDomainObjectCode();
                DbSearchTemplate rootTable = searchData.getTables().get(searchData.getMtPEAlias()+"^"+doCode);
                if (rootTable != null) {
                    searchData.setDatabasePrimaryKey(rootTable.getDoAttributeCodeToColumnMap().get(databasePrimaryKeyDOACode));
                    searchData.setFunctionPrimaryKey(rootTable.getDoAttributeCodeToColumnMap().get(functionalPrimaryKeyDOACode));
                }
                searchData.setDependencyMappedParentData(mappedParentData.getDependencyResultMap());
            }else{
                searchData.setNoSelectToBeMade(true);
            }
            /*
		    *   Making of Aggregate SELECT statements
		    */
            if(mappedParentData!=null && mappedParentData.getDependencyResultMap()!=null && !mappedParentData.getDependencyResultMap().isEmpty()){
                if(attributePaths.getAttributePathToAggDetailsMap()!=null && !attributePaths.getAttributePathToAggDetailsMap().isEmpty()) {
                    searchData.setAggregateStmts(createChildAggregateSelectData(childPE, attributePaths.getAttributePathToAggDetailsMap(),mappedParentData));
                }
            }
		}else{
            searchData.setNoSelectToBeMade(true);
        }
        return searchData;
	}

    /**
	 * Builds the search data for root Process Element.
	 * Processes the root and all its aggregate SELECT statements.  
	 * @param root
	 * @param responseData
     * @return The search data for doing search
	 */
	private DefaultSearchData createRootSearchData(PERequest root, UnitResponseData responseData) throws SQLException{
        String mtPE = root.getMtPE();
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadataVOX rootMTMeta = metadataService.getMetadataByMtPE(mtPE);
		DefaultSearchData searchData = new DefaultSearchData();
		searchData.setPage(root.getPage());
		searchData.setPageSize(root.getPageSize());
		searchData.setApplyPagination(root.isApplyPagination());
		searchData.setDefaultPaginationSize(root.getDefaultPaginationSize());
		searchData.setReturnOnlySqlQuery(root.isReturnOnlySqlQuery());
		searchData.setResolveG11n(root.isResolveG11n());
		searchData.setSubjectUserPath(root.getSubjectUserPath());
        AttributePaths attributePaths = null;
        attributePaths = root.getAttributePaths();
        List<String> allAttributePathsAddedToSelectClause = new ArrayList<>();
		if(root.isLogActivity()){
            searchData.setLogActivity(root.isLogActivity());
            searchData.setLogActivityMTPE(rootMTMeta.getActivityPE());
            searchData.setLogActivityDtlMTPE(rootMTMeta.getActivityDetailsPE());
            AuditMsgFormat auditMsgFormat = metadataService.getAuditMsgFormat("READ", root.getMtPE());
            List<String> msgAttributePaths = getAttributePathExpnFromMsg(auditMsgFormat);
            if(attributePaths.getAttributePaths()!=null && !attributePaths.getAttributePaths().isEmpty()){
                for(String msgAttributePath : msgAttributePaths){
                    attributePaths.getAttributePaths().add(msgAttributePath);
                }
            }
        }
		RequestSelectType selectType = root.getSelectType();
		searchData.setMtPE(mtPE);
		searchData.setMtPEAlias(root.getMtPEAlias());
		/*
		 * All the root select statements will be Distinct
		 * For making SELECT clause get all the attribute paths.
		 * Attribute paths will be resolved according to the SELECT Type given by client
		 */
		/* Root SELECT is always distinct */
		searchData.setInternalDistinct(true);
		searchData.setExternalDistinct(root.isExternalDistinct());

//		if(selectType == RequestSelectType.LAYOUT){\
//		}else{
		    //TODO this else clause cater to the request of external API/third party API call
//			attributePaths = metadataService.getAttributePathsForPE(null, mtPE, selectType);
//		}
		/*
		Make SELECT statement only when Attribute paths are present for it.
		 */
 		if(attributePaths.getAttributePaths()!=null && !attributePaths.getAttributePaths().isEmpty()) {
            Set<String> attributePathSet = attributePaths.getAttributePaths();
            searchData.getAttributePaths().addAll(attributePathSet);
            for (String attributePathExpn : attributePathSet) {
                addAttributePathExpnToJoinClause(attributePathExpn, searchData);
                addAttributeExpnToSelectClause(searchData, attributePathExpn, false, true, true, allAttributePathsAddedToSelectClause);
            }
            /*
             * Add only those Group By clause which are not being used in Aggregate function
             */
            // Loop through all the group By controls
            for(Map.Entry<String, GroupByAttributeDetails> entry : root.getGroupByControlList().entrySet()){
                if(!root.getAggregateGroupByControlList().contains(entry.getKey())){
                    GroupByAttributeDetails attributeDetails = entry.getValue();
                    String groupByAttributePathExpn = attributeDetails.getGroupByAttributePath();
                    addAttributePathExpnToJoinClause(groupByAttributePathExpn, searchData);
                    addAttributeExpnToSelectClause(searchData, groupByAttributePathExpn, false, false, true, allAttributePathsAddedToSelectClause);
                    addAttributePathExpnToGroupByClause(searchData, groupByAttributePathExpn);
                }
            }
            /*
            Add the fk-relation parent doa to the select clause. So that when it is searched for we are able to find it.
             */
            for(PERequest childPE : root.getChildrenPE()){
                // Get the dependencyKeyList
                for(DependencyKeysHelper dependencyKey : childPE.getDependencyKeyList()){
                    String parentDOACode = dependencyKey.getParentDOACode();
                    // This is to be added to the select clause
                    searchData.getAttributePaths().add(parentDOACode);
                    addAttributePathToJoinClause(parentDOACode, searchData);
                    addAttributePathToSelectClause(searchData, parentDOACode, false);
                }
            }
		/*
		 * Filters will make modifications to JOIN and WHERE clause. 
		 */
            addFiltersToRootSelect(root, searchData);
		/*
		 * Add ORDER BY
		 */
		    addOrderByClauseToRootSelect(searchData, root, attributePaths.getStickyHeaderOrderByList(), allAttributePathsAddedToSelectClause);
//            addOrderByClause(searchData, root, attributePaths.getStickyHeaderOrderByList(), allAttributePathsAddedToSelectClause);
            /* Adding Bookmark Attribute to SELECT and JOIN Clause */
            addBookmarkTable(root, searchData);
            addEndorseTable(root, searchData);
            addMenuPermissionsTable(root, searchData, responseData);
            if(!searchData.isExternalDistinct()) {
                for(String attributePath : allAttributePathsAddedToSelectClause) {
                    addAttributePathToSelectClause(searchData, attributePath, true);
                }
            }
            searchData.setStickyHdrSortData(attributePaths.getStickyHeaderOrderByList());
            /*
		 * Add primary key attributes to searchData get the
		 * With the help of root table id and Pk do codes get the searchFields
		 */
            String functionalPrimaryKeyDOACode = rootMTMeta.getPrimaryKeyAttribute().getDoAttributeCode();
            String databasePrimaryKeyDOACode = rootMTMeta.getVersionIdAttribute().getDoAttributeCode();
            String doCode = rootMTMeta.getDomainObjectCode();
            DbSearchTemplate rootTable = searchData.getTables().get(searchData.getMtPEAlias()+"^"+doCode);
            searchData.setDatabasePrimaryKey(rootTable.getDoAttributeCodeToColumnMap().get(databasePrimaryKeyDOACode));
            searchData.setFunctionPrimaryKey(rootTable.getDoAttributeCodeToColumnMap().get(functionalPrimaryKeyDOACode));
            if(rootMTMeta.getRecordStateAttribute()!=null)
                searchData.setRecordStateColumn(rootTable.getDoAttributeCodeToColumnMap().get(rootMTMeta.getRecordStateAttribute().getDoAttributeCode()));
		/*
		 * Making of Aggregate SELECT statements	 
		 */

        }else{
            searchData.setNoSelectToBeMade(true);
        }
		searchData.setAggregateStmts(createAggregateSelectData(root, attributePaths.getAttributePathToAggDetailsMap(), null));
		return searchData;
	}

    private List<String> getAttributePathExpnFromMsg(AuditMsgFormat auditMsgFormat) {
	    List<String> attributeExpn = new ArrayList<>();
        List<String> doaList = new ArrayList<>();
        if(StringUtil.isDefined(auditMsgFormat.getMsgSyntaxYouInContextG11nBigTxt()))
            doaList.addAll(Util.getAttributePathFromExpression(auditMsgFormat.getMsgSyntaxYouInContextG11nBigTxt()));
        if(StringUtil.isDefined(auditMsgFormat.getMsgSyntaxYouOutOfContextG11nBigTxt()))
            doaList.addAll(Util.getAttributePathFromExpression(auditMsgFormat.getMsgSyntaxYouOutOfContextG11nBigTxt()));
        if(StringUtil.isDefined(auditMsgFormat.getMsgSyntaxThirdPersonInContextG11nBigTxt()))
            doaList.addAll(Util.getAttributePathFromExpression(auditMsgFormat.getMsgSyntaxThirdPersonInContextG11nBigTxt()));
        if(StringUtil.isDefined(auditMsgFormat.getMsgSyntaxThirdPersonOutOfContextG11nBigTxt()))
            doaList.addAll(Util.getAttributePathFromExpression(auditMsgFormat.getMsgSyntaxThirdPersonOutOfContextG11nBigTxt()));
        // Now loop through result.
        for(String doaPath : doaList){
            if(doaPath.startsWith("ActivityLog")){
                if(doaPath.startsWith("ActivityLog.ActivityAuthorPerson:Person.")){
                    List<String> personAttributePaths = new ArrayList<>();
                    Person person = TenantAwareCache.getPersonRepository().getPersonDetails(TenantContextHolder.getUserContext().getPersonId());
                    if(!person.getAdditionalDoaValues().containsKey(doaPath)){
                        personAttributePaths.add("!{"+doaPath.substring(doaPath.indexOf(":")+1)+"}");
                    }
                    UnitResponseData personDetailsResponse = getAttributePathData(personAttributePaths, person.getPersonPkId(), false,false);
                    Map<String, AttributeData> attributeMap = personDetailsResponse.getPEData().values().iterator().next().getRecordSetInternal().get(0).getAttributeMap();
                    for(String personAttributePath : personAttributePaths){
                        DoaMeta key = personDetailsResponse.getLabelToAliasMap().get(personAttributePath);
                        person.getAdditionalDoaValues().put(personAttributePath ,(String) attributeMap.get(key.getAlias()).getcV());
                    }
                }
            }else{
                attributeExpn.add("!{"+doaPath+"}");
            }
        }
	    return attributeExpn;
    }

    /**
     * @param childPE
     * @param attributePathToDetailsMap
     */
    private List<DefaultSearchData> createChildAggregateSelectData(PERequest childPE, Map<String,AttributePathDetails> attributePathToDetailsMap, DependencyMappedParentData mappedParentData) throws SQLException{
        /* In this method add 1st the group by clause that is always going to be the dependency key list from the child*/
        /* If more than one dependency key is present group by should be in the same order as the dependency key list */
        for(Map.Entry<String, AttributePathDetails> attrPathEntry :attributePathToDetailsMap.entrySet()){
            AttributePathDetails pathDetails = attrPathEntry.getValue();
            List<DependencyKeysHelper> dependencyKeysList = childPE.getDependencyKeyList();
            for(int index = 0; index < dependencyKeysList.size(); index++){
                DependencyKeysHelper keysHelper = dependencyKeysList.get(index);
                GroupByAttributeDetails groupByAttributeDetails = new GroupByAttributeDetails(keysHelper.getCurrentDOACode(),-dependencyKeysList.size()+index+1,true);
                pathDetails.getGroupByAttributes().add(index,groupByAttributeDetails);
            }
        }
        return createAggregateSelectData(childPE, attributePathToDetailsMap, mappedParentData);
    }
	/**
	 * @param root
	 * @param attributePathToDetailsMap
	 */
	private List<DefaultSearchData> createAggregateSelectData(PERequest root, Map<String, AttributePathDetails> attributePathToDetailsMap, DependencyMappedParentData mappedParentData) throws SQLException{
		/*
		 * 1. First make the SELECT clause.
		 * 		1a. Select the attribute path and aggregate function from control, relation control, hierarchy control, external API control
		 */
		/*
		 * Read each attribute path and 
		 */
        List<DefaultSearchData> aggSearchDataList = new ArrayList<>();
		for(Map.Entry<String, AttributePathDetails> entry : attributePathToDetailsMap.entrySet()){
			String attributePathExpn = entry.getKey();
            DefaultSearchData aggregateSearchData = new DefaultSearchData();
            aggregateSearchData.setMtPE(root.getMtPE());
            aggregateSearchData.setMtPEAlias(root.getMtPEAlias());
            aggregateSearchData.setIsFunctionStmt(true);
            aggregateSearchData.setResolveG11n(true);
            /* now we have the function */
            String aggFunction = entry.getKey();
            AttributePathDetails pathDetails = entry.getValue();
            aggregateSearchData.setAggregateAttributePathDetails(pathDetails);
            aggregateSearchData.setDependencyKeyList(root.getDependencyKeyList());
            aggregateSearchData.setExternalDistinct(true);
            List<GroupByAttributeDetails> groupByAttributes = pathDetails.getGroupByAttributes();
            String aggregateWhereExpression = pathDetails.getAggregateFunctionWhereClause();
            String aggregateHavingExpression = pathDetails.getAggregateFunctionHavingClause();
            Collections.sort(groupByAttributes, new GroupByComparator());
            Deque<PERequest> tableIdStack = new ArrayDeque<>();
            /* Add the attribute path to JOIN clause */
            addAttributePathExpnToJoinClause(attributePathExpn, aggregateSearchData);
            List<String> allAttributePathsAddedToSelectClause = new ArrayList<>();
            /*
             * Adding group by columns to Join, Select and Group By clause
             */
            for(GroupByAttributeDetails attributeDetails : groupByAttributes){
                String groupByAttributePathExpn = attributeDetails.getGroupByAttributePath();
                if(attributeDetails.isInternalChildGroupBy()) {
                    addAttributePathToJoinClause(groupByAttributePathExpn, aggregateSearchData);
                    addAttributePathToSelectClause(aggregateSearchData, groupByAttributePathExpn, false);
                    addAttributePathToGroupByClause(aggregateSearchData, groupByAttributePathExpn);
                }else{
                    addAttributePathExpnToJoinClause(groupByAttributePathExpn, aggregateSearchData);
                    addAttributeExpnToSelectClause(aggregateSearchData, groupByAttributePathExpn, false, false, true, allAttributePathsAddedToSelectClause);
                    addAttributePathExpnToGroupByClause(aggregateSearchData, groupByAttributePathExpn);
                }

            }
            /*
             * Adding function to SELECT clause
             */
            addAttributeExpnToSelectClause(aggregateSearchData, attributePathExpn, false, false, true, allAttributePathsAddedToSelectClause);
            /*
             * Add filter to Child Process Element
             */
            if(mappedParentData!=null) {
                addFiltersToChildSelect(root, aggregateSearchData, mappedParentData.getDependencyResultArray(), null);
                aggregateSearchData.setDependencyMappedParentData(mappedParentData.getDependencyResultMap());
            }
            /*
             * Adding filters to Where clause
             */
            if(!StringUtil.isDefined(root.getParentPEAlias()))
                addFiltersToRootSelect(root, aggregateSearchData);
//            addFiltersOrHavingToRootSelect(aggregateWhereExpression, root, aggregateSearchData, tableIdStack, false);
            /*
             * Adding Having expression
             */
            addFiltersOrHavingToRootSelect(aggregateHavingExpression, root, aggregateSearchData, tableIdStack, true);
            aggSearchDataList.add(aggregateSearchData);

            /*
             * 1. Sort the group by attributes - Done
             * 2. Add them to the SELECT clause - Done
             * 3. Add the function to the select clause - Done
             * 4. Build the from and Join clause - Done
             * 5. Build aggregateWhereExpression - Done
             * 6. Build Group By clause - Done
             * 7. Build aggregateHavingExpression - Done
             * 8. Add this aggregateSearchData to searchData
             */
			}
		return aggSearchDataList;
	}
	/**
	 * @param searchData
	 * @param attributePathExpn
	 */
	private void addAttributePathExpnToGroupByClause(DefaultSearchData searchData, String attributePathExpn) {
		ExpressionBuilder groupByClause = searchData.getGroupByClause();
        if(!groupByClause.isEmpty()){
            groupByClause.addExprTokenToTree(Operator.COMMA);
        }
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        Lexer lexer = new Lexer();
        LexerResult lexerResult = lexer.getTokens(attributePathExpn);
        List<Object> tokens = lexerResult.getTokens();
        for(Object token : tokens) {
            if (token instanceof LeafOperand && ((LeafOperand) token).getIsColumnName()) {
				/* Find all the places where a token is a column and add all the properties by searching for its meta and table alias. */
                LeafOperand leafToken = (LeafOperand) token;
                String attributePath = leafToken.getValue();
                String doa = attributePath;
                if (attributePath.contains(":")) {
                    doa = attributePath.substring(attributePath.lastIndexOf(":") + 1);
                }
                addAttributePathToJoinClause(attributePath, searchData);
                String tableId = searchData.getMtPEAlias()+"^"+attributePath.substring(0, attributePath.lastIndexOf('.'));
                DbSearchTemplate table = searchData.getTables().get(tableId);
                MasterEntityAttributeMetadata doaMeta = metadataService.getMetadataByDoa(doa);
                groupByClause.addExprTokenToTree(new LeafOperand(true, doaMeta.getDbColumnName(), doaMeta.getDoAttributeCode(), null,doaMeta.getDbDataTypeCode(), table.getTableAlias(),doaMeta.isGlocalized(),doaMeta.getContainerBlobDbColumnName()));
            } else {
                groupByClause.addExprTokenToTree(token);
            }
        }
	}
    /**
     * @param searchData
     * @param searchData
     */
    private void addAttributePathToGroupByClause(DefaultSearchData searchData, String attributePath) {
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		String doa = Util.getDOAFromAttributePath(attributePath);
		String groupByTableId = searchData.getMtPEAlias()+"^"+attributePath.substring(0, attributePath.lastIndexOf("."));
		DbSearchTemplate table = searchData.getTables().get(groupByTableId);
		SearchField searchField = table.getColumnLabelToColumnMap().get(attributePath);
		MasterEntityAttributeMetadata attributeMeta = metadataService.getMetadataByDoa(doa);
        ExpressionBuilder groupByClause = searchData.getGroupByClause();
        if(!attributeMeta.getDbDataTypeCode().equals("T_BLOB")){
            LeafOperand leafOperand = new LeafOperand(true,searchField.getColumnName(), searchField.getDoAtributeCode(), searchField.getColumnAlias(),searchField.getDataType(),searchField.getTableAlias(), searchField.isGlocalized(), searchField.getContainerBlobName());
            if(!groupByClause.isEmpty()){
                groupByClause.addExprTokenToTree(Operator.COMMA);
            }
            groupByClause.addExprTokenToTree(leafOperand);
        }
    }
    /**
     * @param attributePathExpn
     * @param searchData
     * @param addAllTableColumns
     */
	/**
	 * This method adds the sort order provided by customer.
     * @param searchData
     * @param processElement
     * @param stickyHeaderOrderByList
     * @param allAttributePathsAddedToSelectClause
     */
	private void addOrderByClause(DefaultSearchData searchData, PERequest processElement, List<SortData> stickyHeaderOrderByList, List<String> allAttributePathsAddedToSelectClause) {
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		ExpressionBuilder orderByClause = searchData.getOrderByClause();
		boolean isStickyHeader = processElement.isStickyHeader();
//		if(isStickyHeader){
//			Collections.sort(stickyHeaderOrderByList, new SortComparator());
//			for(SortData stickyHeaderSortData : stickyHeaderOrderByList){
//				String attributePath = stickyHeaderSortData.getAttributePathExpn();
//				String doaCode = attributePath;
//				if(doaCode.contains(":")){
//					doaCode = attributePath.substring(attributePath.lastIndexOf(":"));
//				}
//				String doCode = doaCode.substring(0, doaCode.lastIndexOf("."));
//				MasterEntityMetadataVOX doMeta = metadataService.getMetadataByDo(doCode);
//				MasterEntityAttributeMetadata primaryKeyAttributeMeta = doMeta.getPrimaryKeyAttribute();
//				/* Get the metadata to get the primary column */
//				String tableId = attributePath.substring(0, attributePath.lastIndexOf("."));
//				DbSearchTemplate table = searchData.getTables().get(tableId);
//				SearchField column = table.getDoAttributeCodeToColumnMap().get(primaryKeyAttributeMeta.getDoAttributeCode());
//				/* if order values are present */
//				if(StringUtil.isDefined(stickyHeaderSortData.getOrderByValues())){
//					addOrderSpecificValuesToOrderByClause(column, stickyHeaderSortData.getOrderByValues(), searchData);
//				}else{
//					if(!orderByClause.isEmpty()){
//						orderByClause.addExprTokenToTree(Operator.COMMA);
//					}
//					orderByClause.addExprTokenToTree(new LeafOperand(true, column.getColumnName(), column.getDoAtributeCode(), column.getDataType(), table.getTableAlias()));
//					if(stickyHeaderSortData.isAscending()){
//						orderByClause.addExprTokenToTree(Operator.ASC);
//					}else{
//						orderByClause.addExprTokenToTree(Operator.DESC);
//					}
//				}
//			}
//		}
        List<SortData> sortDataList = new ArrayList<>();
//        if(stickyHeaderOrderByList!=null)
//            sortDataList.addAll(stickyHeaderOrderByList);
        if(processElement.getSortData()!=null){
            sortDataList.addAll(processElement.getSortData());
        }
		if(!sortDataList.isEmpty()){
			/* Sort the sort order in the sequence number provided. */
			//TODO sort it before making the final sort list itself
			Collections.sort(sortDataList, new SortComparator());
            if(processElement.getDefaultSortData()!=null){
                sortDataList.addAll(processElement.getSortData());
            }
			for(SortData sortData : sortDataList){
                addOrderByClause(sortData, orderByClause, searchData, allAttributePathsAddedToSelectClause);
			}
		}
	}

	private void addOrderByClause(SortData sortData, ExpressionBuilder orderByClause, DefaultSearchData searchData, List<String> allAttributePathsAddedToSelectClause){
        if(!orderByClause.isEmpty()){
            orderByClause.addExprTokenToTree(Operator.COMMA);
        }
        String attributePathExpn = sortData.getAttributePathExpn();
        if(!searchData.getColumnLabelToColumnMap().containsKey(attributePathExpn)){
            addAttributePathExpnToJoinClause(attributePathExpn, searchData);
            addAttributeExpnToSelectClause(searchData, attributePathExpn, false, false, true, allAttributePathsAddedToSelectClause);
        }
        if(StringUtil.isDefined(sortData.getOrderByValues())){
//                    String modifiedOrderByClause ="IF(FIELD("+attributePathExpn+","+sortData.getOrderByValues()+")=0,1,0) ASC, FIELD("+attributePathExpn+","+sortData.getOrderByValues()+") ASC, "+attributePathExpn+" ASC ";
            String modifiedOrderByClause ="FIELD("+attributePathExpn+","+sortData.getOrderByValues()+")";
            Lexer lexer = new Lexer();
            LexerResult lexerResult = lexer.getTokens(modifiedOrderByClause);
            List<Object> tokens = lexerResult.getTokens();
            for(Object token : tokens){
                if(token instanceof LeafOperand && ((LeafOperand) token).getIsColumnName()){
                    LeafOperand aliasName = new LeafOperand(attributePathExpn, "TEXT");
                    aliasName.setColumnAlias(true);
                    orderByClause.addExprTokenToTree(aliasName);
                }else{
                    orderByClause.addExprTokenToTree(token);
                }
            }
        }else{
            LeafOperand aliasName = new LeafOperand(attributePathExpn, "TEXT");
            aliasName.setColumnAlias(true);
            orderByClause.addExprTokenToTree(aliasName);
            if(sortData.isAscending()){
                orderByClause.addExprTokenToTree(Operator.ASC);
            }else{
                orderByClause.addExprTokenToTree(Operator.DESC);
            }
        }
//				String doaCode = attributePathExpn;
//				if(doaCode.contains(":")){
//					doaCode = attributePathExpn.substring(attributePathExpn.lastIndexOf(":"));
//				}
//				String tableId = attributePathExpn.substring(0, attributePathExpn.lastIndexOf("."));
//				DbSearchTemplate table = searchData.getTables().get(tableId);
//				SearchField column = table.getDoAttributeCodeToColumnMap().get(doaCode);
//                SearchField column = searchData.getColumnLabelToColumnMap().get(attributePathExpn);
    }
	/**
	 * @param column
	 * @param orderByValues
	 */
	private void addOrderSpecificValuesToOrderByClause(SearchField column, String orderByValues, DefaultSearchData searchData) {
		ExpressionBuilder orderByCluase = searchData.getOrderByClause();
		if(!orderByCluase.isEmpty()){
			orderByCluase.addExprTokenToTree(Operator.COMMA);
		}
		//FIXME the attribute path and column should be of primary key column here because the values are going to be of order by. 
		String modifiedOrderByClause = new String("IF(FIELD(!{"+column.getColumnLabel()+"},"+orderByValues+")=0,1,0) ASC, FIELD(!{"+column.getColumnLabel()+"},"+orderByValues+") ASC, !{"+column.getColumnLabel()+"} ASC ");
		Lexer lexer = new Lexer();
		LexerResult lexerResult = lexer.getTokens(modifiedOrderByClause);
		List<Object> tokens = lexerResult.getTokens(); 
		for(Object token : tokens){
			if(token instanceof LeafOperand && ((LeafOperand) token).getIsColumnName()){
				orderByCluase.addExprTokenToTree(column);
			}else{
				orderByCluase.addExprTokenToTree(token);
			}
		}
	}

	/**
	 * Tables added through this method does not require you to take care of Access Manager
	 * @param childAttributePath
	 * @param tableIdStack
	 * @param childPE
	 * @param searchData
     */
	private void addChildPEAttributePathToRootJoinClause(String childAttributePath,
			Deque<PERequest> tableIdStack, PERequest childPE,
			DefaultSearchData searchData) {
		/*
		 * Here the Join clause of the root and child will be made with the help of dependency keys.
		 * Table id will start with Primary key of the root table.
		 */
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		Map<String, DbSearchTemplate> tableIdToTableMap = searchData.getTables();
		DbSearchTemplate previousTable = null;
		String tableIdStackValues = getAttributePathFromStack(tableIdStack);
		String tableId = new String(tableIdStackValues+":"+childPE.getMtPEAlias()+"^"+childAttributePath.substring(0, childAttributePath.lastIndexOf('.')));
		//If the table id that this attribute path represents is already present do nothing.
		if(!tableIdToTableMap.containsKey(tableId)){
			String[] doas = childAttributePath.split(":");
            String previousTableID = "";
			for(int doaCount = 0; doaCount< doas.length; doaCount++){
				if(doaCount == 0){
                    String doa = doas[doaCount];
                    String[] splittedDOA = doa.split("\\.");
                    String doCode = splittedDOA[0];
                    String subTableId = new String(tableIdStackValues+":"+childPE.getMtPEAlias()+"^"+childAttributePath.substring(0, childAttributePath.indexOf(doa))+doCode);
					/* The JOIN clause here will be made by using dependency keys */
//					if(!doCode.equals(childDomainObject)){
//						throw new RuntimeException("Root domain object code in attribute path {"+childAttributePath+"} is not equal to root doamin object for process element {"+childPE.getMtPE()+"}");
//					}
					if(!tableIdToTableMap.containsKey(subTableId)) {
					    /* Making of the JOIN Expression */
//                        JoinOperatorsNode operatorNode = new JoinOperatorsNode();
//                        operatorNode.setJoinType(JoinType.LEFT_OUTER_JOIN);
//                        ExpressionBuilder onExpression = new ExpressionBuilder();
                        // Here the trick is even dependency keys can have : in it, and if it has we have to function differently.
                        List<DependencyKeysHelper> dependencyKeyList = childPE.getDependencyKeyList();
                        for (int dependencyKeyCount = 0; dependencyKeyCount < dependencyKeyList.size(); dependencyKeyCount++) {
                            DependencyKeysHelper keyHelper = dependencyKeyList.get(dependencyKeyCount);
                            String parentDOAPath = keyHelper.getParentDOACode();
                            String currentDOAPath = keyHelper.getCurrentDOACode();
                            String parentMTPEAlias = keyHelper.getParentMTPEAlias();
                            String currentMTPEAlias = keyHelper.getCurrentMTPEAlias();
                            //  Parent DOA path will be added as FROM-Key
                            //  Current DOA path will be added as TO-Key
                            /*
                                For adding parent doa path, we have to take into account two things.
                                    1. Are all the table present in stack added to the select clause or not?
                                    2. and parent - DOA can have :
                             */
                            addAllStackTables(tableIdStack,tableIdToTableMap, searchData);
                            if(parentDOAPath.contains(":"))
                                addChildDOAToRoot(searchData, parentDOAPath, tableIdStack);
                            // Now that we have added all the stack tables we are good to make add the new table
                            JoinOperatorsNode operatorNode = new JoinOperatorsNode();
                            operatorNode.setJoinType(JoinType.LEFT_OUTER_JOIN);
                            ExpressionBuilder onExpression = new ExpressionBuilder();
                            String lastFromDOA = Util.getDOAFromAttributePath(parentDOAPath);
                            MasterEntityAttributeMetadata lastFromMeta = metadataService.getMetadataByDoa(lastFromDOA);
                            // Here the table ID will be build on the basis of table id stack
                            if(parentDOAPath.contains(":")){
                                previousTableID = getAttributePathFromStack(tableIdStack)+":"+parentDOAPath.substring(0, parentDOAPath.lastIndexOf("."));
                            }else{
                                previousTableID = tableIdStack.size()==1 ? parentMTPEAlias+"^"+parentDOAPath.substring(0, parentDOAPath.lastIndexOf(".")) : getAttributePathFromStack(tableIdStack).substring(0, getAttributePathFromStack(tableIdStack).lastIndexOf("."));
                            }
                            DbSearchTemplate fromTable = tableIdToTableMap.get(previousTableID);
                            LeafOperand fromKey = new LeafOperand(true,lastFromMeta.getDbColumnName(), lastFromMeta.getDoAttributeCode(),lastFromMeta.getDbDataTypeCode(), fromTable.getTableAlias());
                            String operator = Operator.EQ.toString();
                            String lastToDOA = Util.getDOAFromAttributePath(currentDOAPath);
                            MasterEntityAttributeMetadata lastToMeta = metadataService.getMetadataByDoa(lastToDOA);
                            DbSearchTemplate childDO = new DbSearchTemplate();
                            childDO.setTable(lastToMeta.getDbTableName());
                            String currentTableID = getAttributePathFromStack(tableIdStack).equals("") ? childPE.getMtPEAlias()+"^"+currentDOAPath.substring(0, currentDOAPath.lastIndexOf(".")) : getAttributePathFromStack(tableIdStack) + ":" +childPE.getMtPEAlias()+"^"+ currentDOAPath.substring(0, currentDOAPath.lastIndexOf("."));
                            childDO.setTableId(currentTableID);
                            childDO.setTableAlias(searchData.getTableAlias(childDO.getTableId(), childDO.getTable()));
                            childDO.setDomainObjectCode(lastToMeta.getDomainObjectCode());
                            tableIdToTableMap.put(childDO.getTableId(), childDO);
                            LeafOperand toKey = new LeafOperand(true, lastToMeta.getDbColumnName(),lastToMeta.getDoAttributeCode(), lastToMeta.getDbDataTypeCode(), childDO.getTableAlias());
                            onExpression.addExprTokenToTree(fromKey);
                            onExpression.addExprTokenToTree(operator);
                            onExpression.addExprTokenToTree(toKey);
                            operatorNode.setExpression(onExpression);
                            searchData.getJoinClause().addJoinTokenToTree(operatorNode);
                            JoinLeafOperand childTable = new JoinLeafOperand(childDO.getTable(),childDO.getTableAlias());
                            searchData.getJoinClause().addJoinTokenToTree(childTable);
                        }
                        previousTableID = subTableId+"."+splittedDOA[1];
                        previousTable = tableIdToTableMap.get(subTableId);
                    }
				}else{
				    // If the DOA count is not equal to 1 it means we have to go up the hierarchy from here by finding out the relationship.,./
                    String doa = doas[doaCount];
                    String[] splittedDOA = doa.split("\\.");
                    String doCode = splittedDOA[0];
                    String previousDOA = doas[doaCount - 1];
                    MasterEntityAttributeMetadata previousDOAMeta = metadataService.getMetadataByDoa(previousDOA);
                    //get related doa data
                    String relatedDOCode = previousDOAMeta.getToDomainObjectCode();
                    String relatedDOACode = previousDOAMeta.getToDoAttributeCode();
                    if (!doCode.equals(relatedDOCode)) {
                        throw new RuntimeException("Relation {" + previousDOA + "} to domain object {" + doCode + "} for attribute path {" + childAttributePath + "} does not exist.");
                    }
//                    String subTableId = searchData.getMtPEAlias()+"^"+childAttributePath.substring(0, childAttributePath.indexOf(doa)) + relatedDOCode;
                    String subTableId = previousTableID+":"+doCode;
                    if (!tableIdToTableMap.containsKey(subTableId)) {
                    /*
                    * TODO Get all access control paths
                    * Take the help of WHO(logged in users group) and WHAT (MT-PE, doCode)
                    * with that also add start date and end date
                    */
                    /* Instead of previous Column get DOA metadata */
                        MasterEntityMetadataVOX relatedMTDOMeta = metadataService.getMetadataByDo(doCode);
                        DbSearchTemplate relatedDO = new DbSearchTemplate();
                        relatedDO.setTable(relatedMTDOMeta.getDbTableName());
                        relatedDO.setTableId(subTableId);
                        relatedDO.setTableAlias(searchData.getTableAlias(subTableId, relatedDO.getTable()));
                        relatedDO.setDomainObjectCode(relatedMTDOMeta.getDomainObjectCode());
                        JoinLeafOperand relatedTable = new JoinLeafOperand(relatedDO.getTable(), relatedDO.getTableAlias());
                        JoinOperatorsNode operatorNode = new JoinOperatorsNode();
                        operatorNode.setJoinType(JoinType.LEFT_OUTER_JOIN);
                        ExpressionBuilder onExpression = new ExpressionBuilder();
                        // LeafOperand(boolean isColumnName, String columnName, String doAttributeCode, String columnAlias,String dataType, String tableAlias, boolean isGlocalized, String containerBlobName){
                        LeafOperand fromKey = new LeafOperand(true, previousDOAMeta.getDbColumnName(), previousDOAMeta.getDoAttributeCode(), null, previousDOAMeta.getDbDataTypeCode(), previousTable.getTableAlias(),previousDOAMeta.isGlocalized(),previousDOAMeta.getContainerBlobDbColumnName());
                        onExpression.addExprTokenToTree(fromKey);
                        onExpression.addExprTokenToTree(Operator.EQ.toString());
                        operatorNode.setExpression(onExpression);
                        MasterEntityAttributeMetadata relatedAttribute = metadataService.getMetadataByDoa(relatedDOACode);
                        if (relatedAttribute.getDoAttributeCode().equals(relatedDOACode)) {
                            LeafOperand toKey = new LeafOperand(true, relatedAttribute.getDbColumnName(), relatedAttribute.getDoAttributeCode(), relatedAttribute.getDbDataTypeCode(), relatedDO.getTableAlias());
                            onExpression.addExprTokenToTree(toKey);
                        }
                        MasterEntityAttributeMetadata recordStateAttribute = relatedMTDOMeta.getRecordStateAttribute();
                        if (recordStateAttribute != null) {
                            onExpression.addExprTokenToTree(Operator.AND.toString());
                            onExpression.addExprTokenToTree(new LeafOperand(true, recordStateAttribute.getDbColumnName(), recordStateAttribute.getDoAttributeCode(), recordStateAttribute.getDbDataTypeCode(), relatedDO.getTableAlias()));
                            onExpression.addExprTokenToTree(Operator.EQ);
                            onExpression.addExprTokenToTree(new LeafOperand("CURRENT", "TEXT"));
                        }
                        tableIdToTableMap.put(subTableId, relatedDO);
                        searchData.getJoinClause().addJoinTokenToTree(operatorNode);
//                        searchData.getJoinClause().addJoinTokenToTree(relatedTable);
//                        if(relatedMTDOMeta.isAccessControlGoverned()){
//                            searchData.getJoinClause().addJoinTokenToTree("(");
//                            searchData.getJoinClause().addJoinTokenToTree(relatedTable);
//                            addAccessControlToJoinClause(relatedDO, searchData);
//                            searchData.getJoinClause().addJoinTokenToTree(")");
//                        }else{
                        searchData.getJoinClause().addJoinTokenToTree(relatedTable);
//                        }
                    }
                    previousTable = tableIdToTableMap.get(subTableId);
                    previousTableID = previousTableID+":"+doa;
                }
			}
		}
	}

    /**
     * Checking for all the stack values and adding them to the join clause
     * @param tableIdStack
     * @param tableIdToTableMap
     * @param searchData
     */
    private void addAllStackTables(Deque<PERequest> tableIdStack, Map<String, DbSearchTemplate> tableIdToTableMap, DefaultSearchData searchData) {
	    MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        Iterator<PERequest> stackIterator = tableIdStack.descendingIterator();
        StringBuilder currentTableHelper = new StringBuilder("");
        String previousTableHelper = "";
        while (stackIterator.hasNext()) {
            PERequest stackPERequest = stackIterator.next();
            MasterEntityMetadataVOX doMeta = metadataService.getMetadataByMtPE(stackPERequest.getMtPE());
            MasterEntityAttributeMetadata attributeMeta = doMeta.getFunctionalPrimaryKeyAttribute();
            // Build the
            String newTableID = currentTableHelper.toString().equals("") ? stackPERequest.getMtPEAlias()+"^"+attributeMeta.getDomainObjectCode() : currentTableHelper.toString() + ":" +attributeMeta.getDomainObjectCode();
            if (!tableIdToTableMap.containsKey(newTableID)) {
                // Now this table will be added on the basis of dependency keys
                List<DependencyKeysHelper> dependencyKeysHelpers = stackPERequest.getDependencyKeyList();
                for (int i = 0; i < dependencyKeysHelpers.size(); i++) {
                    DependencyKeysHelper stackKeyHelper = dependencyKeysHelpers.get(i);
                    String parentDependencyDOAPath = stackKeyHelper.getParentDOACode();
                    String currentDependencyDOAPath = stackKeyHelper.getCurrentDOACode();
                    // Now here we have to add LEFT OUTER JOIN CLAUSE
                    // Both parent and child will be joined on the basis of last doas ID table's
                    // Now that we have got parent DOA path we need to add it to the
                    JoinOperatorsNode operatorNode = new JoinOperatorsNode();
                    operatorNode.setJoinType(JoinType.LEFT_OUTER_JOIN);
                    ExpressionBuilder onExpression = new ExpressionBuilder();
                    String lastFromDOA = Util.getDOAFromAttributePath(parentDependencyDOAPath);
                    MasterEntityAttributeMetadata lastFromMeta = metadataService.getMetadataByDoa(lastFromDOA);
                    String previousTableID = previousTableHelper.equals("") ? searchData.getMtPEAlias()+"^"+parentDependencyDOAPath.substring(0, parentDependencyDOAPath.lastIndexOf(".")) : previousTableHelper+":"+parentDependencyDOAPath.substring(0, parentDependencyDOAPath.lastIndexOf("."));
                    DbSearchTemplate fromTable = tableIdToTableMap.get(previousTableID);
                    LeafOperand fromKey = new LeafOperand(true,lastFromMeta.getDbColumnName(), lastFromMeta.getDoAttributeCode(),lastFromMeta.getDbDataTypeCode(), fromTable.getTableAlias());
                    String operator = Operator.EQ.toString();
                    String lastToDOA = Util.getDOAFromAttributePath(currentDependencyDOAPath);
                    MasterEntityAttributeMetadata lastToMeta = metadataService.getMetadataByDoa(lastToDOA);
                    DbSearchTemplate childDO = new DbSearchTemplate();
                    childDO.setTable(lastToMeta.getDbTableName());
                    childDO.setTableId(currentTableHelper.toString()+":"+currentDependencyDOAPath.substring(0, currentDependencyDOAPath.lastIndexOf(".")));
                    childDO.setTableAlias(searchData.getTableAlias(childDO.getTableId(), childDO.getTable()));
                    childDO.setDomainObjectCode(lastToMeta.getDomainObjectCode());
                    tableIdToTableMap.put(childDO.getTableId(), childDO);
                    LeafOperand toKey = new LeafOperand(true, lastToMeta.getDbColumnName(),lastToMeta.getDoAttributeCode(), lastToMeta.getDbDataTypeCode(), childDO.getTableAlias());
                    onExpression.addExprTokenToTree(fromKey);
                    onExpression.addExprTokenToTree(operator);
                    onExpression.addExprTokenToTree(toKey);
                    operatorNode.setExpression(onExpression);
                    searchData.getJoinClause().addJoinTokenToTree(operatorNode);
                    JoinLeafOperand childTable = new JoinLeafOperand(childDO.getTable(),childDO.getTableAlias());
                    searchData.getJoinClause().addJoinTokenToTree(childTable);
//                                        addChildDOAToRoot(searchData, currentDependencyDOAPath, currentTableHelper);
                }
            }
            if (!currentTableHelper.toString().equals("")) {
                previousTableHelper = currentTableHelper.toString();
                currentTableHelper.append(":").append(attributeMeta.getDoAttributeCode());
            } else {
                previousTableHelper = currentTableHelper.toString();
                currentTableHelper.append(attributeMeta.getDoAttributeCode());
            }
        }
    }

    private void addChildDOAToRoot(DefaultSearchData searchData, String parentDependencyDOAPath, Deque<PERequest> tableIdStack) {
	    MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        Map<String, DbSearchTemplate> tableIdToTableMap = searchData.getTables();
        DbSearchTemplate previousTable = null;
        String tableId = getAttributePathFromStack(tableIdStack) +":"+ parentDependencyDOAPath.substring(0, parentDependencyDOAPath.lastIndexOf('.'));
        if (!tableIdToTableMap.containsKey(tableId)) {
            String[] doas = parentDependencyDOAPath.split(":");
            for (int doaCount = 0; doaCount < doas.length; doaCount++) {
                if (doaCount == 0) {
                    String rootTableID = getAttributePathFromStack(tableIdStack).substring(0, getAttributePathFromStack(tableIdStack).lastIndexOf("."));
                    if(!tableIdToTableMap.containsKey(rootTableID)) {
                        String doa = doas[0];
                        String[] splitDOA = doa.split("\\.");
                        String doCode = splitDOA[0];
                        MasterEntityMetadataVOX baseMTDOMeta = metadataService.getMetadataByDo(doCode);
                        DbSearchTemplate baseDO = new DbSearchTemplate();
                        baseDO.setTable(baseMTDOMeta.getDbTableName());
                        baseDO.setTableId(getAttributePathFromStack(tableIdStack) + ":" + baseMTDOMeta.getDomainObjectCode());
                        baseDO.setTableAlias(searchData.getTableAlias(baseDO.getTableId(), baseDO.getTable()));
                        baseDO.setDomainObjectCode(doCode);
                        tableIdToTableMap.put(baseDO.getTableId(), baseDO);
                        JoinLeafOperand rootTable = new JoinLeafOperand(baseDO.getTable(), baseDO.getTableAlias());
                        JoinOperatorsNode operatorNode = new JoinOperatorsNode();
//                    operatorNode.setJoinType(JoinType.LEFT_OUTER_JOIN);
//                    ExpressionBuilder onExpression = new ExpressionBuilder();
//                    LeafOperand fromKey = new LeafOperand(true, previousDOAMeta.getDbColumnName(), previousDOAMeta.getDoAttributeCode(), null, previousDOAMeta.getDbDataTypeCode(), previousTable.getTableAlias(), previousDOAMeta.isGlocalized(), previousDOAMeta.getContainerBlobDbColumnName());
//                    onExpression.addExprTokenToTree(fromKey);
//                    onExpression.addExprTokenToTree(Operator.EQ.toString());
//                    searchData.getJoinClause().addJoinTokenToTree(rootTable);
//                        if (baseMTDOMeta.isAccessControlGoverned()) {
//                            addAccessControlToJoinClause(baseDO, searchData);
//                        }
                    }
                    previousTable = tableIdToTableMap.get(rootTableID);
                } else {
                    //e.g. GOAL.GOALEVENTFKID:GOALEVENT.OWNERID:PERSON.NAME
                    String doa = doas[doaCount];
                    String[] splitdDOA = doa.split("\\.");
                    String doCode = splitdDOA[0];
                    String previousDOA = doas[doaCount - 1];
                    MasterEntityAttributeMetadata previousDOAMeta = metadataService.getMetadataByDoa(previousDOA);
                    //get related doa data
                    String relatedDOCode = previousDOAMeta.getToDomainObjectCode();
                    String relatedDOACode = previousDOAMeta.getToDoAttributeCode();
                    if (!doCode.equals(relatedDOCode)) {
                        throw new RuntimeException("Relation {" + previousDOA + "} to domain object {" + doCode + "} for attribute path {" + parentDependencyDOAPath + "} does not exist.");
                    }
                    String subTableId = getAttributePathFromStack(tableIdStack)+":" + parentDependencyDOAPath.substring(0, parentDependencyDOAPath.indexOf(doa)) + relatedDOCode;
                    /*
                    * TODO Get all access control paths
                    * Take the help of WHO(logged in users group) and WHAT (MT-PE, doCode)
                    * with that also add start date and end date
                    */
                    /* Instead of previous Column get DOA metadata */
                    if (!tableIdToTableMap.containsKey(subTableId)) {
                        MasterEntityMetadataVOX relatedMTDOMeta = metadataService.getMetadataByDo(doCode);
                        DbSearchTemplate relatedDO = new DbSearchTemplate();
                        relatedDO.setTable(relatedMTDOMeta.getDbTableName());
                        relatedDO.setTableId(subTableId);
                        relatedDO.setTableAlias(searchData.getTableAlias(subTableId, relatedDO.getTable()));
                        relatedDO.setDomainObjectCode(relatedMTDOMeta.getDomainObjectCode());
                        JoinLeafOperand relatedTable = new JoinLeafOperand(relatedDO.getTable(), relatedDO.getTableAlias());
                        JoinOperatorsNode operatorNode = new JoinOperatorsNode();
                        operatorNode.setJoinType(JoinType.LEFT_OUTER_JOIN);
                        ExpressionBuilder onExpression = new ExpressionBuilder();
                        // LeafOperand(boolean isColumnName, String columnName, String doAttributeCode, String columnAlias,String dataType, String tableAlias, boolean isGlocalized, String containerBlobName){
                        LeafOperand fromKey = new LeafOperand(true, previousDOAMeta.getDbColumnName(), previousDOAMeta.getDoAttributeCode(), null, previousDOAMeta.getDbDataTypeCode(), previousTable.getTableAlias(), previousDOAMeta.isGlocalized(), previousDOAMeta.getContainerBlobDbColumnName());
                        onExpression.addExprTokenToTree(fromKey);
                        onExpression.addExprTokenToTree(Operator.EQ.toString());
                        operatorNode.setExpression(onExpression);
                        MasterEntityAttributeMetadata relatedAttribute = metadataService.getMetadataByDoa(relatedDOACode);
                        if (relatedAttribute.getDoAttributeCode().equals(relatedDOACode)) {
                            LeafOperand toKey = new LeafOperand(true, relatedAttribute.getDbColumnName(), relatedAttribute.getDoAttributeCode(), relatedAttribute.getDbDataTypeCode(), relatedDO.getTableAlias());
                            onExpression.addExprTokenToTree(toKey);
                        }
                        MasterEntityAttributeMetadata recordStateAttribute = relatedMTDOMeta.getRecordStateAttribute();
                        if (recordStateAttribute != null) {
                            onExpression.addExprTokenToTree(Operator.AND.toString());
                            onExpression.addExprTokenToTree(new LeafOperand(true, recordStateAttribute.getDbColumnName(), recordStateAttribute.getDoAttributeCode(), recordStateAttribute.getDbDataTypeCode(), relatedDO.getTableAlias()));
                            onExpression.addExprTokenToTree(Operator.EQ);
                            onExpression.addExprTokenToTree(new LeafOperand("CURRENT", "TEXT"));
                        }
                        tableIdToTableMap.put(subTableId, relatedDO);
                        searchData.getJoinClause().addJoinTokenToTree(operatorNode);
//                        searchData.getJoinClause().addJoinTokenToTree(relatedTable);
//                        if (relatedMTDOMeta.isAccessControlGoverned()) {
//                            searchData.getJoinClause().addJoinTokenToTree("(");
//                            searchData.getJoinClause().addJoinTokenToTree(relatedTable);
//                            addAccessControlToJoinClause(relatedDO, searchData);
//                            searchData.getJoinClause().addJoinTokenToTree(")");
//                        } else {
                            searchData.getJoinClause().addJoinTokenToTree(relatedTable);
//                        }
                    }
                    previousTable = tableIdToTableMap.get(subTableId);
                }
            }
        }
    }

    /**
	 * This method iterates the stack in First In order. Appends the domain object attribute code to result string
	 * @param tableIdStack Stack having primary key attributes as values. Helps in forming the table IDs in case of Hierarchical Process Element request
	 * @return 
	 */
	private String getAttributePathFromStack(Deque<PERequest> tableIdStack) {
		if(tableIdStack == null || tableIdStack.isEmpty())
			return "";
		StringBuilder attributePath = new StringBuilder();
		Iterator<PERequest> stackIterator = tableIdStack.descendingIterator();
		while (stackIterator.hasNext()) {
            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
            MasterEntityMetadataVOX parentMTMeta = metadataService.getMetadataByMtPE(stackIterator.next().getMtPE());
			MasterEntityAttributeMetadata attributeMeta = parentMTMeta.getFunctionalPrimaryKeyAttribute();
			attributePath.append(attributeMeta.getDoAttributeCode());
			attributePath.append(":");
		}
		if(attributePath.toString().endsWith(":")){
			return attributePath.substring(0, attributePath.length()-1);
		}
		return attributePath.toString();
	}
	/**
	 * This method should take care of Access Manager
	 * @param attributePathExpn because of which Join clause is to be made
	 * @param searchData add required table to Join clause of this search data
	 */
	/* TODO this might return all the attribute paths required to run Access Control for all the table this attribute path has added */
	private void addAttributePathExpnToJoinClause(String attributePathExpn, DefaultSearchData searchData) {
        List<String> doasOfExpn = Util.getAttributePathFromExpression(attributePathExpn);
        for(String attributePath : doasOfExpn) {
            addAttributePathToJoinClause(attributePath, searchData);
        }
	}

    private void addMenuPermissionsTable(PERequest processElement, DefaultSearchData searchData, UnitResponseData responseData) throws SQLException{
	    if(processElement.getMtPE().equals("Menu")){
	        // Here add the Menu Permissions table and corresponding filters to this PE
            JoinOperatorsNode operatorNode = new JoinOperatorsNode();
            operatorNode.setJoinType(JoinType.INNER_JOIN);
            ExpressionBuilder onExpression = new ExpressionBuilder();
            operatorNode.setExpression(onExpression);
            /* from is the base table's primary key */
            Map<String, DbSearchTemplate> tables = searchData.getTables();
            /* Get the root domain object */
            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
            MasterEntityMetadataVOX rootMeta = metadataService.getMetadataByMtPE(processElement.getMtPE());
            MasterEntityAttributeMetadata functionPk = rootMeta.getFunctionalPrimaryKeyAttribute();
            DbSearchTemplate rootTable = tables.get(processElement.getMtPEAlias()+"^"+rootMeta.getDomainObjectCode());
            LeafOperand fromKey = new LeafOperand(true, functionPk.getDbColumnName(), functionPk.getDoAttributeCode(), functionPk.getDbDataTypeCode(), rootTable.getTableAlias());
            onExpression.addExprTokenToTree(fromKey);
            onExpression.addExprTokenToTree(Operator.EQ.toString());
            /* Add Menu Permission as to key */
            DbSearchTemplate menuPermTable = new DbSearchTemplate();
            MasterEntityMetadataVOX menuPermissionsMeta = metadataService.getMetadataByMtPE("MenuPermission");
            MasterEntityAttributeMetadata menuPermRelatedPk = menuPermissionsMeta.getAttributeNameMap().get("MenuPermission.Menu");
            menuPermTable.setTable(menuPermissionsMeta.getDbTableName());
            menuPermTable.setTableId("MenuPermission");
            menuPermTable.setTableAlias(searchData.getTableAlias("MenuPermission", menuPermissionsMeta.getDbTableName()));
            menuPermTable.setDomainObjectCode(menuPermissionsMeta.getDomainObjectCode());
            JoinLeafOperand menuPermJoinExpn = new JoinLeafOperand(menuPermTable.getTable(), menuPermTable.getTableAlias());
            LeafOperand toKey = new LeafOperand(true, menuPermRelatedPk.getDbColumnName(), menuPermRelatedPk.getDoAttributeCode(), menuPermRelatedPk.getDbDataTypeCode(), menuPermTable.getTableAlias());
            onExpression.addExprTokenToTree(toKey);
            MasterEntityAttributeMetadata recordStateAttribute = menuPermissionsMeta.getRecordStateAttribute();
            if (recordStateAttribute != null) {
                onExpression.addExprTokenToTree(Operator.AND.toString());
                onExpression.addExprTokenToTree(new LeafOperand(true, recordStateAttribute.getDbColumnName(), recordStateAttribute.getDoAttributeCode(), recordStateAttribute.getDbDataTypeCode(), menuPermTable.getTableAlias()));
                onExpression.addExprTokenToTree(Operator.EQ);
                // Hardcoded here
                onExpression.addExprTokenToTree(new LeafOperand("CURRENT", "TEXT"));
            }
            // Now we have to add the filters to the where clause gor all of these
            MasterEntityAttributeMetadata isDeleteAttributeMeta = menuPermissionsMeta.getIsDeletedAttribute();
            onExpression.addExprTokenToTree(Operator.AND.toString());
            onExpression.addExprTokenToTree(new LeafOperand(true, isDeleteAttributeMeta.getDbColumnName(), isDeleteAttributeMeta.getDoAttributeCode(), isDeleteAttributeMeta.getDbDataTypeCode(), menuPermTable.getTableAlias()));
            onExpression.addExprTokenToTree(Operator.EQ);
            onExpression.addExprTokenToTree(new LeafOperand("0", "BOOLEAN"));
            tables.put(menuPermTable.getTableId(), menuPermTable);
            searchData.getJoinClause().addJoinTokenToTree(operatorNode);
            searchData.getJoinClause().addJoinTokenToTree(menuPermJoinExpn);
            String menuFilterExpression = "";
            menuFilterExpression = prepareMenuFilterExpn(processElement, responseData);
            if (!StringUtil.isDefined(menuFilterExpression)) {
                try {
                    throw new Exception("Menu filter expression not ready");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Deque<PERequest> tableIdStack = new ArrayDeque<>();
//            addFiltersOrHavingToRootSelect(menuFilterExpression, processElement, searchData, tableIdStack, false);
            ExpressionBuilder expression = searchData.getWhereClause();
            if(!expression.isEmpty()){
                expression.addExprTokenToTree(Operator.AND);
            }
            Lexer lexer = new Lexer();
            LexerResult lexerResult = lexer.getTokens(menuFilterExpression);
            List<Object> tokens = lexerResult.getTokens();
            for(Object token : tokens) {
                if (token instanceof LeafOperand && ((LeafOperand) token).getIsColumnName()) {
                    LeafOperand leafToken = (LeafOperand) token;
                    String attributePath = leafToken.getValue();
                    String doa = attributePath;
                    if(attributePath.contains(":")){
                        doa = attributePath.substring(attributePath.lastIndexOf(":")+1);
                    }
                    DbSearchTemplate table = searchData.getTables().get(menuPermTable.getTableId());
                    MasterEntityAttributeMetadata doaMeta = metadataService.getMetadataByDoa(doa);
                    expression.addExprTokenToTree(new LeafOperand(true, doaMeta.getDbColumnName(), doaMeta.getDoAttributeCode(), null, doaMeta.getDbDataTypeCode(), table.getTableAlias(), doaMeta.isGlocalized(),doaMeta.getContainerBlobDbColumnName()));
                }else{
                    expression.addExprTokenToTree(token);
                }
            }
        }
    }

    /**
     * This methods adds the Endorse PK to the corresponding MTPEAlias if the action called ENDORSE ot UN-Endorse is present.
     * @param root
     * @param searchData
     */
    private void addEndorseTable(PERequest root, DefaultSearchData searchData) {
        if(root.isEndorseRtrn()){
            JoinOperatorsNode operatorNode = new JoinOperatorsNode();
            operatorNode.setJoinType(JoinType.LEFT_OUTER_JOIN);
            ExpressionBuilder onExpression = new ExpressionBuilder();
            operatorNode.setExpression(onExpression);
            /* from is the base table's primary key */
            Map<String, DbSearchTemplate> tables = searchData.getTables();
            /* Get the root domain object */
            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
            MasterEntityMetadataVOX rootMeta = metadataService.getMetadataByMtPE(root.getMtPE());
            MasterEntityAttributeMetadata functionPk = rootMeta.getFunctionalPrimaryKeyAttribute();
            DbSearchTemplate rootTable = tables.get(root.getMtPEAlias()+"^"+rootMeta.getDomainObjectCode());
            LeafOperand fromKey = new LeafOperand(true, functionPk.getDbColumnName(), functionPk.getDoAttributeCode(), functionPk.getDbDataTypeCode(), rootTable.getTableAlias());
            onExpression.addExprTokenToTree(fromKey);
            onExpression.addExprTokenToTree(Operator.EQ.toString());
            /* Add Endorse as to key */
            DbSearchTemplate endorseTable = new DbSearchTemplate();
            MasterEntityMetadataVOX endorseMeta = metadataService.getMetadataByMtPE(rootMeta.getEndorseMtPE());
            MasterEntityAttributeMetadata endorseRelatedPk = endorseMeta.getAttributeNameMap().get(endorseMeta.getDomainObjectCode()+".ObjectID");
            endorseTable.setTable(endorseMeta.getDbTableName());
            endorseTable.setTableId("Endorse");
            endorseTable.setTableAlias(searchData.getTableAlias("Endorse", endorseMeta.getDbTableName()));
            endorseTable.setDomainObjectCode(endorseMeta.getDomainObjectCode());
            JoinLeafOperand endorseJoinExpn = new JoinLeafOperand(endorseTable.getTable(), endorseTable.getTableAlias());
            LeafOperand toKey = new LeafOperand(true, endorseRelatedPk.getDbColumnName(), endorseRelatedPk.getDoAttributeCode(), endorseRelatedPk.getDbDataTypeCode(), endorseTable.getTableAlias());
            onExpression.addExprTokenToTree(toKey);
            MasterEntityAttributeMetadata recordStateAttribute = endorseMeta.getRecordStateAttribute();
            if (recordStateAttribute != null) {
                onExpression.addExprTokenToTree(Operator.AND.toString());
                onExpression.addExprTokenToTree(new LeafOperand(true, recordStateAttribute.getDbColumnName(), recordStateAttribute.getDoAttributeCode(), recordStateAttribute.getDbDataTypeCode(), endorseTable.getTableAlias()));
                onExpression.addExprTokenToTree(Operator.EQ);
                // Hardcoded here
                onExpression.addExprTokenToTree(new LeafOperand("CURRENT", "TEXT"));
            }
        /* Add the logged in person as id here to the Join expression clause*/
            MasterEntityAttributeMetadata loggedInPersonMeta = endorseMeta.getAttributeNameMap().get(endorseMeta.getDomainObjectCode()+".AuthoredByPersonID");
            onExpression.addExprTokenToTree(Operator.AND.toString());
            onExpression.addExprTokenToTree(new LeafOperand(true, loggedInPersonMeta.getDbColumnName(), loggedInPersonMeta.getDoAttributeCode(), loggedInPersonMeta.getDbDataTypeCode(), endorseTable.getTableAlias()));
            onExpression.addExprTokenToTree(Operator.EQ);
            /* get the person Id */
            UserContext user = TenantContextHolder.getUserContext();
            onExpression.addExprTokenToTree(new LeafOperand(user.getPersonId(), "T_ID"));
        /* Add the IS_DELETED=0 clause here*/
            MasterEntityAttributeMetadata isDeleteAttributeMeta = endorseMeta.getIsDeletedAttribute();
            onExpression.addExprTokenToTree(Operator.AND.toString());
            onExpression.addExprTokenToTree(new LeafOperand(true, isDeleteAttributeMeta.getDbColumnName(), isDeleteAttributeMeta.getDoAttributeCode(), isDeleteAttributeMeta.getDbDataTypeCode(), endorseTable.getTableAlias()));
            onExpression.addExprTokenToTree(Operator.EQ);
            onExpression.addExprTokenToTree(new LeafOperand("0", "BOOLEAN"));

            tables.put(endorseTable.getTableAlias(), endorseTable);
            searchData.getJoinClause().addJoinTokenToTree(operatorNode);
            searchData.getJoinClause().addJoinTokenToTree(endorseJoinExpn);

            /* Now add SELECT clause endorse is the alias */
            ExpressionBuilder selectClause = searchData.getSelectClause();
            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata endorsePKMeta = endorseMeta.getPrimaryKeyAttribute();

            /* Now add the Alias*/
            selectClause.addExprTokenToTree(new LeafOperand(true, endorsePKMeta.getDbColumnName(), endorsePKMeta.getDoAttributeCode(), endorsePKMeta.getDbDataTypeCode(), endorseTable.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            LeafOperand aliasName = new LeafOperand("#ENDORSE#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            SearchField searchField = new SearchField("#ENDORSE#", "BOOLEAN");
            searchData.getColumnLabelToColumnMap().put("#ENDORSE#", searchField);

            selectClause.addExprTokenToTree(Operator.COMMA);
            selectClause.addExprTokenToTree(new LeafOperand(true, endorsePKMeta.getDbColumnName(), endorsePKMeta.getDoAttributeCode(), endorsePKMeta.getDbDataTypeCode(), endorseTable.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            LeafOperand pkAliasName = new LeafOperand("#ENDORSE-PK#", "TEXT");
            pkAliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(pkAliasName);
            SearchField pkSearchField = new SearchField("#ENDORSE-PK#", "T_ID");
            searchData.getColumnLabelToColumnMap().put("#ENDORSE-PK#", pkSearchField);

            MasterEntityAttributeMetadata lastModifiedMeta = endorseMeta.getAttributeByName(endorsePKMeta.getDomainObjectCode()+".LastModifiedDateTime");

            selectClause.addExprTokenToTree(Operator.COMMA);
            /* Now add the Alias*/
            selectClause.addExprTokenToTree(new LeafOperand(true, lastModifiedMeta.getDbColumnName(), lastModifiedMeta.getDoAttributeCode(), lastModifiedMeta.getDbDataTypeCode(), endorseTable.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            LeafOperand lastModifiedAliasName = new LeafOperand("#ENDORSE-lastMOD#", "TEXT");
            lastModifiedAliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(lastModifiedAliasName);
            SearchField lastMODSearchField = new SearchField("#ENDORSE-lastMOD#", "DATE_TIME");
            searchData.getColumnLabelToColumnMap().put("#ENDORSE-lastMOD#", lastMODSearchField);

            MasterEntityAttributeMetadata dbPrimaryKeyMeta = endorseMeta.getVersionIdAttribute();

            selectClause.addExprTokenToTree(Operator.COMMA);
            /* Now add the Alias*/
            selectClause.addExprTokenToTree(new LeafOperand(true, dbPrimaryKeyMeta.getDbColumnName(), dbPrimaryKeyMeta.getDoAttributeCode(), dbPrimaryKeyMeta.getDbDataTypeCode(), endorseTable.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            LeafOperand versionIDAliasName = new LeafOperand("#ENDORSE-versionID#", "TEXT");
            versionIDAliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(versionIDAliasName);
            SearchField versionIDSearchField = new SearchField("#ENDORSE-versionID#", "T_ID");
            searchData.getColumnLabelToColumnMap().put("#ENDORSE-versionID#", versionIDSearchField);
        }
    }

    /**
     * This table adds the bookmark table to the join and select clause
     * @param root
     * @param searchData
     */
    private void addBookmarkTable(PERequest root, DefaultSearchData searchData) {
        if(root.isBookmarkRtrn() || root.isOnlyBookmarkedRecords()) {
            JoinOperatorsNode operatorNode = new JoinOperatorsNode();
            if(root.isOnlyBookmarkedRecords()){
                operatorNode.setJoinType(JoinType.INNER_JOIN);
            }else {
                operatorNode.setJoinType(JoinType.LEFT_OUTER_JOIN);
            }
            ExpressionBuilder onExpression = new ExpressionBuilder();
            operatorNode.setExpression(onExpression);
        /* from is the base table's primary key */
            Map<String, DbSearchTemplate> tables = searchData.getTables();
        /* Get the root domain object */
            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
            MasterEntityMetadataVOX rootMeta = metadataService.getMetadataByMtPE(root.getMtPE());
            MasterEntityAttributeMetadata functionPk = rootMeta.getFunctionalPrimaryKeyAttribute();
            DbSearchTemplate rootTable = tables.get(root.getMtPEAlias()+"^"+rootMeta.getDomainObjectCode());
            LeafOperand fromKey = new LeafOperand(true, functionPk.getDbColumnName(), functionPk.getDoAttributeCode(), functionPk.getDbDataTypeCode(), rootTable.getTableAlias());
            onExpression.addExprTokenToTree(fromKey);
            onExpression.addExprTokenToTree(Operator.EQ.toString());
        /* Add Bookmark as to key */
            DbSearchTemplate bookmarkTable = new DbSearchTemplate();
            // Here we need to add table and all received from domain object of the root mt pe.

            MasterEntityMetadataVOX bookmarkMeta = metadataService.getMetadataByMtPE(rootMeta.getBookmarkPE());
            MasterEntityAttributeMetadata bookmarkRelatedPk = bookmarkMeta.getAttributeNameMap().get(bookmarkMeta.getDomainObjectCode()+".DOPrimaryKey");
            bookmarkTable.setTable(bookmarkMeta.getDbTableName());
            bookmarkTable.setTableId("Bookmark");
            bookmarkTable.setTableAlias(searchData.getTableAlias("Bookmark", bookmarkMeta.getDbTableName()));
            bookmarkTable.setDomainObjectCode(bookmarkMeta.getDomainObjectCode());
            JoinLeafOperand bookmarkJoinExpn = new JoinLeafOperand(bookmarkTable.getTable(), bookmarkTable.getTableAlias());
            LeafOperand toKey = new LeafOperand(true, bookmarkRelatedPk.getDbColumnName(), bookmarkRelatedPk.getDoAttributeCode(), bookmarkRelatedPk.getDbDataTypeCode(), bookmarkTable.getTableAlias());
            onExpression.addExprTokenToTree(toKey);
            MasterEntityAttributeMetadata recordStateAttribute = bookmarkMeta.getRecordStateAttribute();
            if (recordStateAttribute != null) {
                onExpression.addExprTokenToTree(Operator.AND.toString());
                onExpression.addExprTokenToTree(new LeafOperand(true, recordStateAttribute.getDbColumnName(), recordStateAttribute.getDoAttributeCode(), recordStateAttribute.getDbDataTypeCode(), bookmarkTable.getTableAlias()));
                onExpression.addExprTokenToTree(Operator.EQ);
                // Hardcoded here
                onExpression.addExprTokenToTree(new LeafOperand("CURRENT", "TEXT"));
            }
        /* Add the logged in person as id here to the Join expression clause*/
            MasterEntityAttributeMetadata loggedInPersonMeta = bookmarkMeta.getAttributeNameMap().get(bookmarkMeta.getDomainObjectCode()+".LoggedInPerson");
            onExpression.addExprTokenToTree(Operator.AND.toString());
            onExpression.addExprTokenToTree(new LeafOperand(true, loggedInPersonMeta.getDbColumnName(), loggedInPersonMeta.getDoAttributeCode(), loggedInPersonMeta.getDbDataTypeCode(), bookmarkTable.getTableAlias()));
            onExpression.addExprTokenToTree(Operator.EQ);
            /* get the person Id */
            UserContext user = TenantContextHolder.getUserContext();
            onExpression.addExprTokenToTree(new LeafOperand(user.getPersonId(), "T_ID"));
        /* Add the IS_DELETED=0 clause here*/
            MasterEntityAttributeMetadata isDeleteAttributeMeta = bookmarkMeta.getIsDeletedAttribute();
            onExpression.addExprTokenToTree(Operator.AND.toString());
            onExpression.addExprTokenToTree(new LeafOperand(true, isDeleteAttributeMeta.getDbColumnName(), isDeleteAttributeMeta.getDoAttributeCode(), isDeleteAttributeMeta.getDbDataTypeCode(), bookmarkTable.getTableAlias()));
            onExpression.addExprTokenToTree(Operator.EQ);
            onExpression.addExprTokenToTree(new LeafOperand("0", "BOOLEAN"));

            tables.put(bookmarkTable.getTableAlias(), bookmarkTable);
            searchData.getJoinClause().addJoinTokenToTree(operatorNode);
            searchData.getJoinClause().addJoinTokenToTree(bookmarkJoinExpn);


        /* Now add SELECT clause bookmark is the alias */
            ExpressionBuilder selectClause = searchData.getSelectClause();
            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata bookmarkPKMeta = bookmarkMeta.getPrimaryKeyAttribute();
            /* Now add the Alias*/
            selectClause.addExprTokenToTree(new LeafOperand(true, bookmarkPKMeta.getDbColumnName(), bookmarkPKMeta.getDoAttributeCode(), bookmarkPKMeta.getDbDataTypeCode(), bookmarkTable.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            LeafOperand aliasName = new LeafOperand("#BOOKMARK#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            SearchField searchField = new SearchField("#BOOKMARK#", "BOOLEAN");
            searchData.getColumnLabelToColumnMap().put("#BOOKMARK#", searchField);

            selectClause.addExprTokenToTree(Operator.COMMA);
            selectClause.addExprTokenToTree(new LeafOperand(true, bookmarkPKMeta.getDbColumnName(), bookmarkPKMeta.getDoAttributeCode(), bookmarkPKMeta.getDbDataTypeCode(), bookmarkTable.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            LeafOperand pkAliasName = new LeafOperand("#BOOKMARK-PK#", "TEXT");
            pkAliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(pkAliasName);
            SearchField pkSearchField = new SearchField("#BOOKMARK-PK#", "T_ID");
            searchData.getColumnLabelToColumnMap().put("#BOOKMARK-PK#", pkSearchField);

            MasterEntityAttributeMetadata lastModifiedMeta = bookmarkMeta.getAttributeByName(bookmarkPKMeta.getDomainObjectCode()+".LastModifiedDateTime");

            selectClause.addExprTokenToTree(Operator.COMMA);
            /* Now add the Alias*/
            selectClause.addExprTokenToTree(new LeafOperand(true, lastModifiedMeta.getDbColumnName(), lastModifiedMeta.getDoAttributeCode(), lastModifiedMeta.getDbDataTypeCode(), bookmarkTable.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            LeafOperand lastModifiedAliasName = new LeafOperand("#BOOKMARK-lastMOD#", "TEXT");
            lastModifiedAliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(lastModifiedAliasName);
            SearchField lastMODSearchField = new SearchField("#BOOKMARK-lastMOD#", "DATE_TIME");
            searchData.getColumnLabelToColumnMap().put("#BOOKMARK-lastMOD#", lastMODSearchField);
        }
    }
    /**
     * This method should take care of Access Manager
     * @param attributePath because of which Join clause is to be made
     * @param searchData add required table to Join clause of this search data
     */
	/* TODO this might return all the attribute paths required to run Access Control for all the table this attribute path has added */
    private void addAttributePathToJoinClause(String attributePath, DefaultSearchData searchData) {
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadata rootMTDOMetadata = metadataService.getMetadataByMtPE(searchData.getMtPE());
        String rootDomainObject = rootMTDOMetadata.getDomainObjectCode();
        Map<String, DbSearchTemplate> tableIdToTableMap = searchData.getTables();
        DbSearchTemplate previousTable = null;
        String tableId = searchData.getMtPEAlias()+"^"+attributePath.substring(0, attributePath.lastIndexOf('.'));
        if (!searchData.getTables().containsKey(tableId)) {
            String[] doas = attributePath.split(":");
            for (int doaCount = 0; doaCount < doas.length; doaCount++) {
                if (doaCount == 0) {
                    String doa = doas[0];
                    String[] splittedDOA = doa.split("\\.");
                    //Here table id is equivalent to do code.
                    String doCode = splittedDOA[0];
                    if (!doCode.equals(rootDomainObject)) {
                        throw new RuntimeException("Root domain object code in attribute path {" + attributePath + "} is not equal to root doamin object for process element {" + searchData.getMtPE() + "}");
                    }
                    if (!tableIdToTableMap.containsKey(searchData.getMtPEAlias()+"^"+doCode)) {
                        MasterEntityMetadataVOX rootMTDOMeta = metadataService.getMetadataByDo(doCode);
                        DbSearchTemplate rootDO = new DbSearchTemplate();
                        rootDO.setTable(rootMTDOMeta.getDbTableName());
                        rootDO.setTableId(searchData.getMtPEAlias()+"^"+rootMTDOMeta.getDomainObjectCode());
                        rootDO.setTableAlias(searchData.getTableAlias(rootDO.getTableId(), rootDO.getTable()));
                        rootDO.setDomainObjectCode(doCode);
                        tableIdToTableMap.put(searchData.getMtPEAlias()+"^"+doCode, rootDO);
                        JoinLeafOperand rootTable = new JoinLeafOperand(rootDO.getTable(), rootDO.getTableAlias());
                        searchData.getJoinClause().addJoinTokenToTree(rootTable);
//                        if(rootMTDOMeta.isAccessControlGoverned()){
//                            addAccessControlToJoinClause(rootDO, searchData);
//                        }
                    }
                    previousTable = tableIdToTableMap.get(searchData.getMtPEAlias()+"^"+doCode);
                } else {
                    //e.g. GOAL.GOALEVENTFKID:GOALEVENT.OWNERID:PERSON.NAME
                    String doa = doas[doaCount];
                    String[] splittedDOA = doa.split("\\.");
                    String doCode = splittedDOA[0];
                    String previousDOA = doas[doaCount - 1];
                    MasterEntityAttributeMetadata previousDOAMeta = metadataService.getMetadataByDoa(previousDOA);
                    //get related doa data
                    String relatedDOCode = previousDOAMeta.getToDomainObjectCode();
                    String relatedDOACode = previousDOAMeta.getToDoAttributeCode();
                    if (!doCode.equals(relatedDOCode)) {
                        throw new RuntimeException("Relation {" + previousDOA + "} to domain object {" + doCode + "} for attribute path {" + attributePath + "} does not exist.");
                    }
//                    String subTableId = searchData.getMtPEAlias()+"^"+attributePath.substring(0, attributePath.indexOf(doa)) + relatedDOCode;
                    StringBuilder subTableIDBuilderHelper = new StringBuilder();
                    for(int subTableCount = 0;subTableCount < doaCount ;subTableCount++ ){
                        if(subTableCount!=0)
                            subTableIDBuilderHelper.append(":");
                        subTableIDBuilderHelper.append(doas[subTableCount]);
                    }
                    String subTableId = searchData.getMtPEAlias()+"^"+subTableIDBuilderHelper.toString()+":" + relatedDOCode;
                    if (!tableIdToTableMap.containsKey(subTableId)) {
                    /*
                    * TODO Get all access control paths
                    * Take the help of WHO(logged in users group) and WHAT (MT-PE, doCode)
                    * with that also add start date and end date
                    */
                    /* Instead of previous Column get DOA metadata */
                        MasterEntityMetadataVOX relatedMTDOMeta = metadataService.getMetadataByDo(doCode);
                        DbSearchTemplate relatedDO = new DbSearchTemplate();
                        relatedDO.setTable(relatedMTDOMeta.getDbTableName());
                        relatedDO.setTableId(subTableId);
                        relatedDO.setTableAlias(searchData.getTableAlias(subTableId, relatedDO.getTable()));
                        relatedDO.setDomainObjectCode(relatedMTDOMeta.getDomainObjectCode());
                        JoinLeafOperand relatedTable = new JoinLeafOperand(relatedDO.getTable(), relatedDO.getTableAlias());
                        JoinOperatorsNode operatorNode = new JoinOperatorsNode();
                        operatorNode.setJoinType(JoinType.LEFT_OUTER_JOIN);
                        ExpressionBuilder onExpression = new ExpressionBuilder();
                        // LeafOperand(boolean isColumnName, String columnName, String doAttributeCode, String columnAlias,String dataType, String tableAlias, boolean isGlocalized, String containerBlobName){
                        LeafOperand fromKey = new LeafOperand(true, previousDOAMeta.getDbColumnName(), previousDOAMeta.getDoAttributeCode(), null, previousDOAMeta.getDbDataTypeCode(), previousTable.getTableAlias(),previousDOAMeta.isGlocalized(),previousDOAMeta.getContainerBlobDbColumnName());
                        onExpression.addExprTokenToTree(fromKey);
                        onExpression.addExprTokenToTree(Operator.EQ.toString());
                        operatorNode.setExpression(onExpression);
                        MasterEntityAttributeMetadata relatedAttribute = metadataService.getMetadataByDoa(relatedDOACode);
                        if (relatedAttribute.getDoAttributeCode().equals(relatedDOACode)) {
                            LeafOperand toKey = new LeafOperand(true, relatedAttribute.getDbColumnName(), relatedAttribute.getDoAttributeCode(), relatedAttribute.getDbDataTypeCode(), relatedDO.getTableAlias());
                            onExpression.addExprTokenToTree(toKey);
                        }
                        MasterEntityAttributeMetadata recordStateAttribute = relatedMTDOMeta.getRecordStateAttribute();
                        if (recordStateAttribute != null) {
                            onExpression.addExprTokenToTree(Operator.AND.toString());
                            onExpression.addExprTokenToTree(new LeafOperand(true, recordStateAttribute.getDbColumnName(), recordStateAttribute.getDoAttributeCode(), recordStateAttribute.getDbDataTypeCode(), relatedDO.getTableAlias()));
                            onExpression.addExprTokenToTree(Operator.EQ);
                            onExpression.addExprTokenToTree(new LeafOperand("CURRENT", "TEXT"));
                        }
                        tableIdToTableMap.put(subTableId, relatedDO);
                        searchData.getJoinClause().addJoinTokenToTree(operatorNode);
//                        searchData.getJoinClause().addJoinTokenToTree(relatedTable);
//                        if(relatedMTDOMeta.isAccessControlGoverned()){
//                            searchData.getJoinClause().addJoinTokenToTree("(");
//                            searchData.getJoinClause().addJoinTokenToTree(relatedTable);
//                            addAccessControlToJoinClause(relatedDO, searchData);
//                            searchData.getJoinClause().addJoinTokenToTree(")");
//                        }else{
                            searchData.getJoinClause().addJoinTokenToTree(relatedTable);
//                        }
                    }
                    previousTable = tableIdToTableMap.get(subTableId);
                }
            }
        }
    }

    private void addAccessControlToJoinClause(DbSearchTemplate rootDO, DefaultSearchData searchData) {
        Map<String, DbSearchTemplate> tableIdToTableMap = searchData.getTables();
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadataVOX rootMTDOMeta = metadataService.getMetadataByDo(rootDO.getDomainObjectCode());
        MasterEntityMetadataVOX accessControlROWDOMeta = metadataService.getMetadataByDo("AccessManagerRowResolved");
        DbSearchTemplate accessControlROWDO = new DbSearchTemplate();
        accessControlROWDO.setTable(accessControlROWDOMeta.getDbTableName());
        accessControlROWDO.setTableId(searchData.getMtPEAlias()+"^"+rootDO.getTableId()+"AM");
        accessControlROWDO.setTableAlias(searchData.getTableAlias(rootDO.getTableId(), accessControlROWDO.getTable()));
        accessControlROWDO.setDomainObjectCode(accessControlROWDOMeta.getDomainObjectCode());
        JoinLeafOperand accessROWTable = new JoinLeafOperand(accessControlROWDO.getTable(), accessControlROWDO.getTableAlias());
        JoinOperatorsNode operatorNode = new JoinOperatorsNode();
        operatorNode.setJoinType(JoinType.INNER_JOIN);
        ExpressionBuilder onExpression = new ExpressionBuilder();
        MasterEntityAttributeMetadata dbPrimaryKeyColumn = rootMTDOMeta.getVersionIdAttribute();
        MasterEntityAttributeMetadata accessControlWhatDBKey = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.DOVersion");
        LeafOperand fromKey = new LeafOperand(true, dbPrimaryKeyColumn.getDbColumnName(), dbPrimaryKeyColumn.getDoAttributeCode(), dbPrimaryKeyColumn.getDbDataTypeCode(), rootDO.getTableAlias());
        onExpression.addExprTokenToTree(fromKey);
        onExpression.addExprTokenToTree(Operator.EQ.toString());
        LeafOperand toKey = new LeafOperand(true, accessControlWhatDBKey.getDbColumnName(), accessControlWhatDBKey.getDoAttributeCode(), accessControlWhatDBKey.getDbDataTypeCode(), accessControlROWDO.getTableAlias());
        onExpression.addExprTokenToTree(toKey);
        onExpression.addExprTokenToTree(Operator.AND.toString());
        MasterEntityAttributeMetadata accessControlWHOId = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.WhoPerson");
        onExpression.addExprTokenToTree(new LeafOperand(true, accessControlWHOId.getDbColumnName(), accessControlWHOId.getDoAttributeCode(), accessControlWHOId.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
        onExpression.addExprTokenToTree(Operator.EQ);
        UserContext user = TenantContextHolder.getUserContext();
        onExpression.addExprTokenToTree(new LeafOperand(user.getPersonId(), "T_ID"));
        operatorNode.setExpression(onExpression);
        tableIdToTableMap.put(accessControlROWDO.getTableId(), accessControlROWDO);
        searchData.getJoinClause().addJoinTokenToTree(operatorNode);
        searchData.getJoinClause().addJoinTokenToTree(accessROWTable);

        // TODO Here also add the SELECT clause values for row access control.
        if(!searchData.isExternalDistinct()) {
            ExpressionBuilder selectClause = searchData.getSelectClause();
            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlPKMeta = accessControlROWDOMeta.getVersionIdAttribute();
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlPKMeta.getDbColumnName(), accessControlPKMeta.getDoAttributeCode(), accessControlPKMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            LeafOperand aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMROWPK#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            SearchField searchField = new SearchField(rootDO.getTableAlias() + "#AMROWPK#", "T_ID");
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMROWPK#", searchField);

            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlSubjectUserMeta = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.SubjectUser");
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlSubjectUserMeta.getDbColumnName(), accessControlSubjectUserMeta.getDoAttributeCode(), accessControlSubjectUserMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMSUBUSRPK#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            searchField = new SearchField(rootDO.getTableAlias() + "#AMSUBUSRPK#", accessControlSubjectUserMeta.getDbDataTypeCode());
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMSUBUSRPK#", searchField);

            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlUpdateCurPermMeta = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.EditCurrentRow");
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlUpdateCurPermMeta.getDbColumnName(), accessControlUpdateCurPermMeta.getDoAttributeCode(), accessControlUpdateCurPermMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMUPDCURPERM#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            searchField = new SearchField(rootDO.getTableAlias() + "#AMUPDCURPERM#", accessControlUpdateCurPermMeta.getDbDataTypeCode());
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMUPDCURPERM#", searchField);

            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlUpdateHisPermMeta = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.EditHistoryRow");
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlUpdateHisPermMeta.getDbColumnName(), accessControlUpdateHisPermMeta.getDoAttributeCode(), accessControlUpdateHisPermMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMUPDHISPERM#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            searchField = new SearchField(rootDO.getTableAlias() + "#AMUPDHISPERM#", accessControlUpdateHisPermMeta.getDbDataTypeCode());
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMUPDHISPERM#", searchField);

            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlUpdateFutPermMeta = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.EditFutureRow");
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlUpdateFutPermMeta.getDbColumnName(), accessControlUpdateFutPermMeta.getDoAttributeCode(), accessControlUpdateFutPermMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMUPDFUTPERM#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            searchField = new SearchField(rootDO.getTableAlias() + "#AMUPDFUTPERM#", accessControlUpdateFutPermMeta.getDbDataTypeCode());
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMUPDFUTPERM#", searchField);

            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlReadCurPermMeta = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.ViewCurrentRow");
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlReadCurPermMeta.getDbColumnName(), accessControlReadCurPermMeta.getDoAttributeCode(), accessControlReadCurPermMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMREDCURPERM#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            searchField = new SearchField(rootDO.getTableAlias() + "#AMREDCURPERM#", accessControlReadCurPermMeta.getDbDataTypeCode());
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMREDCURPERM#", searchField);

            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlReadHisPermMeta = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.ViewHistoryRow");
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlReadHisPermMeta.getDbColumnName(), accessControlReadHisPermMeta.getDoAttributeCode(), accessControlReadHisPermMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMREDHISPERM#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            searchField = new SearchField(rootDO.getTableAlias() + "#AMREDHISPERM#", accessControlReadHisPermMeta.getDbDataTypeCode());
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMREDHISPERM#", searchField);

            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlReadFutPermMeta = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.ViewFutureRow");
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlReadFutPermMeta.getDbColumnName(), accessControlReadFutPermMeta.getDoAttributeCode(), accessControlReadFutPermMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMREDFUTPERM#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            searchField = new SearchField(rootDO.getTableAlias() + "#AMREDFUTPERM#", accessControlReadFutPermMeta.getDbDataTypeCode());
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMREDFUTPERM#", searchField);

            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlDeleteCurPermMeta = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.DeleteCurrentRow");
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlDeleteCurPermMeta.getDbColumnName(), accessControlDeleteCurPermMeta.getDoAttributeCode(), accessControlDeleteCurPermMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMDELCURPERM#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            searchField = new SearchField(rootDO.getTableAlias() + "#AMDELCURPERM#", accessControlDeleteCurPermMeta.getDbDataTypeCode());
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMDELCURPERM#", searchField);

            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlDeleteHisPermMeta = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.DeleteHistoryRow");
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlDeleteHisPermMeta.getDbColumnName(), accessControlDeleteHisPermMeta.getDoAttributeCode(), accessControlDeleteHisPermMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMDELHISPERM#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            searchField = new SearchField(rootDO.getTableAlias() + "#AMDELHISPERM#", accessControlDeleteHisPermMeta.getDbDataTypeCode());
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMDELHISPERM#", searchField);

            if (!selectClause.isEmpty()) {
                selectClause.addExprTokenToTree(Operator.COMMA);
            }
            MasterEntityAttributeMetadata accessControlDeleteFutPermMeta = accessControlROWDOMeta.getAttributeNameMap().get("AccessManagerRowResolved.DeleteFutureRow");
            selectClause.addExprTokenToTree(new LeafOperand(true, accessControlDeleteFutPermMeta.getDbColumnName(), accessControlDeleteFutPermMeta.getDoAttributeCode(), accessControlDeleteFutPermMeta.getDbDataTypeCode(), accessControlROWDO.getTableAlias()));
            selectClause.addExprTokenToTree(Operator.AS);
            aliasName = new LeafOperand(rootDO.getTableAlias() + "#AMDELFUTPERM#", "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
            searchField = new SearchField(rootDO.getTableAlias() + "#AMDELFUTPERM#", accessControlDeleteFutPermMeta.getDbDataTypeCode());
            searchData.getColumnLabelToColumnMap().put(rootDO.getTableAlias() + "#AMDELFUTPERM#", searchField);
        }
    }

    /**
     *  @param searchData
     * @param attributePath
     */
    private void addAttributePathToSelectClause(DefaultSearchData searchData, String attributePath, boolean addAllAttributesFromTheDO){
        Map<String, DbSearchTemplate> tableIdToTableMap = searchData.getTables();
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        String[] doas = attributePath.split(":");
        for (int doaCount = 0; doaCount < doas.length; doaCount++) {
            String doa = doas[doaCount];
            String[] splittedDOA = doa.split("\\.");
            String doCode = splittedDOA[0];
            String subTableId = searchData.getMtPEAlias()+"^"+attributePath.substring(0, attributePath.indexOf(doa)) + doCode;
            if (subTableId.startsWith("."))
                subTableId = subTableId.substring(1);
            if (tableIdToTableMap.containsKey(subTableId)) {
                DbSearchTemplate table = tableIdToTableMap.get(subTableId);
                if (!table.isColumnsAdded()) {
                    MasterEntityMetadataVOX doMeta = metadataService.getMetadataByDo(doCode);
                    if (addAllAttributesFromTheDO) {
                        List<MasterEntityAttributeMetadata> attributes = doMeta.getAttributeMeta();
                        for (MasterEntityAttributeMetadata attributeMeta : attributes) {
                            addAttributeToSelectClauseInternal(doMeta, attributeMeta, searchData, table, subTableId);
                        }
                        table.setColumnsAdded(true);
                    }else{
                        MasterEntityAttributeMetadata attributeMeta = doMeta.getAttributeByName(doa);
                        addAttributeToSelectClauseInternal(doMeta, attributeMeta, searchData, table, subTableId);
                    }
                }
            } else {
                throw new RuntimeException("Table does not exists for attribute path {" + attributePath + "}");
            }
        }
    }

    private void addAttributeToSelectClauseInternal(MasterEntityMetadataVOX doMeta, MasterEntityAttributeMetadata attributeMeta, DefaultSearchData searchData, DbSearchTemplate table, String subTableId) {
        String doaCode = attributeMeta.getDoAttributeCode();
        String doaName = doaCode.substring(doaCode.lastIndexOf(".") + 1);
        String dbPrimaryKeyDoaName = doMeta.getVersionIdAttribute().getDoAttributeCode().substring(doMeta.getVersionIdAttribute().getDoAttributeCode().lastIndexOf(".") + 1);
        String recordStateDoaName = null;
        if (doMeta.getRecordStateAttribute() != null)
            recordStateDoaName = doMeta.getRecordStateAttribute().getDoAttributeCode().substring(doMeta.getRecordStateAttribute().getDoAttributeCode().lastIndexOf(".") + 1);
        SearchField searchField = new SearchField(attributeMeta.getDoAttributeCode());
        searchField.setColumnName(attributeMeta.getDbColumnName());
        searchField.setGlocalized(attributeMeta.isGlocalized());
        searchField.setContainerBlobName(attributeMeta.getContainerBlobDbColumnName());
        searchField.setColumnAlias(INVERTED_COMMA + subTableId.substring(subTableId.indexOf("^")+1) + "." + doaName + INVERTED_COMMA);
        searchField.setColumnLabel(subTableId.substring(subTableId.indexOf("^")+1) + "." + doaName);
        searchField.setDbPrimaryKeyColumnLabel(subTableId.substring(subTableId.indexOf("^")+1) + "." + dbPrimaryKeyDoaName);
        searchField.setRecordStateColumnLabel(subTableId.substring(subTableId.indexOf("^")+1) + "." + recordStateDoaName);
        searchField.setInternal(true);
//								if(subTableId.contains(COLON))
//									searchField.setColumnAlias(INVERTED_COMMA+subColumnLabel+COLON+attributeMeta.getDoAttributeCode()+INVERTED_COMMA);
//								if(subTableId.contains(COLON))
//									searchField.setColumnLabel(subColumnLabel+COLON+attributeMeta.getDoAttributeCode());
        searchField.setDataType(attributeMeta.getDbDataTypeCode());
        searchField.setTableAlias(table.getTableAlias());
        searchData.addColumn(subTableId, searchField);
        if (attributeMeta.isFunctionalPrimaryKey()) {
            table.setFunctionPrimaryKey(searchField);
        }
        if (attributeMeta.isDbPrimaryKey()) {
            table.setDatabasePrimaryKey(searchField);
        }
        addColumnToSelectClause(searchData, searchField);
    }

    private void addColumnToSelectClause(DefaultSearchData searchData, SearchField searchField) {
		ExpressionBuilder selectClause = searchData.getSelectClause();
//		if(!searchField.getDataType().equals("T_BLOB")){
			LeafOperand leafOperand = new LeafOperand(true,searchField.getColumnName(), searchField.getDoAtributeCode(), searchField.getColumnAlias(),searchField.getDataType(),searchField.getTableAlias(), searchField.isGlocalized(), searchField.getContainerBlobName());
			if(!selectClause.isEmpty()){
				selectClause.addExprTokenToTree(Operator.COMMA);				
			}
//			if(StringUtil.isDefined(aggFunction)){
//				selectClause.addExprTokenToTree(Operator.valueOf(aggFunction));
//				selectClause.addExprTokenToTree(Operator.OPEN_PARENTHESIS);
//				selectClause.addExprTokenToTree(leafOperand);
//				selectClause.addExprTokenToTree(Operator.CLOSE_PARENTHESIS);
//			}else{
				selectClause.addExprTokenToTree(leafOperand);
//			}
            selectClause.addExprTokenToTree(Operator.AS);
            LeafOperand aliasName = new LeafOperand(searchField.getColumnLabel(), "TEXT");
            aliasName.setColumnAlias(true);
            selectClause.addExprTokenToTree(aliasName);
//        }
	}

	private void addAttributeExpnToSelectClause(DefaultSearchData searchData, String attributePathExpn, Boolean isAggFunction, boolean addAllTableColumns, boolean isResolveGlocalized, List<String> allAttributePathsAddedToSelectClause){
        ExpressionBuilder expression = searchData.getSelectClause();
        List<String> attributePathsInExpn = new ArrayList<>();
        if(!expression.isEmpty()){
            expression.addExprTokenToTree(Operator.COMMA);
        }
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        String processedDatatype = null;
        String processedTableAlias = null;
        Lexer lexer = new Lexer();
        LexerResult lexerResult = lexer.getTokens(attributePathExpn);
        if(lexerResult.isDistinct()){
            searchData.setExternalDistinct(lexerResult.isDistinct());
        }
        List<Object> tokens = lexerResult.getTokens();
        boolean isGlocalizedColumn = false;
        for(Object token : tokens){
            if(token instanceof LeafOperand && ((LeafOperand) token).getIsColumnName()){
				/* Find all the places where a token is a column and add all the properties by searching for its meta and table alias. */
                LeafOperand leafToken = (LeafOperand) token;
                String attributePath = leafToken.getValue();
                String doa = attributePath;
                if(attributePath.contains(":")){
                    doa = attributePath.substring(attributePath.lastIndexOf(":")+1);
                }
                addAttributePathToJoinClause(attributePath, searchData);
                attributePathsInExpn.add(attributePath);
                allAttributePathsAddedToSelectClause.add(attributePath);
                String tableId = searchData.getMtPEAlias()+"^"+attributePath.substring(0, attributePath.lastIndexOf('.'));
                DbSearchTemplate table = searchData.getTables().get(tableId);
                MasterEntityAttributeMetadata doaMeta = metadataService.getMetadataByDoa(doa);
                // LeafOperand(boolean isColumnName, String columnName, String doAttributeCode, String columnAlias,String dataType, String tableAlias, boolean isGlocalized, String containerBlobName)
                LeafOperand column = new LeafOperand(true, doaMeta.getDbColumnName(), doaMeta.getDoAttributeCode(),null, doaMeta.getDbDataTypeCode(), table.getTableAlias(), doaMeta.isGlocalized(), doaMeta.getContainerBlobDbColumnName());
                if(doaMeta.isGlocalized())
                    isGlocalizedColumn = true;
                if(doaMeta.isGlocalized() && isResolveGlocalized) {
                    column.setGlocalized(doaMeta.isGlocalized());
                }else{
                    column.setGlocalized(false);
                }
                expression.addExprTokenToTree(column);
                processedTableAlias = table.getTableAlias();
                if(tokens.size()==1){
                    processedDatatype = doaMeta.getDbDataTypeCode();
                }
            }else{
                expression.addExprTokenToTree(token);
            }
        }
        /* Now once the tokens have been added we shall add the alias here */
        expression.addExprTokenToTree(Operator.AS);
        LeafOperand aliasName = new LeafOperand(attributePathExpn, "TEXT");
        if(isGlocalizedColumn && !isResolveGlocalized)
            aliasName = new LeafOperand(attributePathExpn+"G11n", "TEXT");
        aliasName.setColumnAlias(true);
        expression.addExprTokenToTree(aliasName);
        /* Here add values to columnLabel to SearchField */
        if(!StringUtil.isDefined(processedDatatype)){
            if(attributePathExpn.trim().startsWith("CONCAT"))
                processedDatatype = "TEXT";
            else
                processedDatatype = "DEFAULT";
        }

        SearchField searchField = new SearchField(attributePathExpn, processedDatatype);
        searchField.setAggFunction(isAggFunction);
        searchField.setTableAlias(processedTableAlias);
        if(isGlocalizedColumn && !isResolveGlocalized) {
            searchData.getColumnLabelToColumnMap().put(attributePathExpn + "G11n", searchField);
        }else{
            searchData.getColumnLabelToColumnMap().put(attributePathExpn, searchField);
        }

//        if(addAllTableColumns && !searchData.isExternalDistinct()) {
//            for(String attributePath : attributePathsInExpn) {
//                addAttributePathToSelectClause(searchData, attributePath, true);
//            }
//        }
        if(isGlocalizedColumn && isResolveGlocalized)
            addAttributeExpnToSelectClause(searchData, attributePathExpn, isAggFunction, false, false, allAttributePathsAddedToSelectClause);
    }

	/**
	 * 
	 * @param filterExpression
	 * @param searchData
	 * @param tableIdStack
	 * @deprecated
	 */
	@SuppressWarnings("unused")
	private void addFilterToWhereClause(String filterExpression, DefaultSearchData searchData, Deque<PERequest> tableIdStack){
		ExpressionBuilder whereClause = searchData.getWhereClause();
		if(!whereClause.isEmpty()){
			whereClause.addExprTokenToTree(Operator.AND);
		}
		Lexer lexer = new Lexer();
		LexerResult lexerResult = lexer.getTokens(filterExpression);
		List<Object> tokens = lexerResult.getTokens();
		for(Object token : tokens){
			if(token instanceof LeafOperand && ((LeafOperand) token).getIsColumnName()){
				/* Find all the places where a token is a column and add all the properties by searching for its meta and table alias. */
				LeafOperand leafToken = (LeafOperand) token;
				String attributePath = leafToken.getValue();
				String doa = attributePath;
				if(attributePath.contains(":")){
					doa = attributePath.substring(attributePath.lastIndexOf(":"));
				}
				String tableIdStackValue = getAttributePathFromStack(tableIdStack);
				String tableId = new String(searchData.getMtPEAlias()+"^"+tableIdStackValue+":"+attributePath.substring(attributePath.lastIndexOf(".")));
				DbSearchTemplate table = searchData.getTables().get(tableId);
				SearchField column = table.getDoAttributeCodeToColumnMap().get(doa);
				whereClause.addExprTokenToTree(new LeafOperand(true, column.getColumnName(), column.getDoAtributeCode(), column.getDataType(), table.getTableAlias()));
			}else{
				whereClause.addExprTokenToTree(token);
			}
		}
	}
    private DependencyMappedParentData makeDependencyMappedParentData(List<DependencyKeysHelper> dependencyKeyList, ProcessElementData parentPEData, JsonLabelAliasTracker aliasTracker){
        DependencyMappedParentData mappedParentData = new DependencyMappedParentData();
	    Map<String, Object> dependencyResultMap = new HashMap<>();
        List<String[]> dependencyResultArray = new ArrayList<>();
        for(Record parentRecord : parentPEData.getRecordSetInternal()){
			/*
			 * For each record we need to find out if this set of dependency key values exist or not
			 * For identifying it we will find it in the dependecyResultMap
			 *
			 * If
			 * 	that combination already exists we will not add it in the array list
			 * Else
			 * 	We will add it at both the places array as well as map.
			 */
			/* Now for each parent record get all the process elements it has */
            Map<String, Object> tempDependencyMap = dependencyResultMap;
            String[] tempDependencyArray = new String[dependencyKeyList.size()];
			/* For each dependency key attribute */
            for(int i = 0; i<dependencyKeyList.size(); i++) {
                DependencyKeysHelper dependencyAttribute = dependencyKeyList.get(i);
                String parentDOACode = dependencyAttribute.getParentDOACode();
                String parentMtPEalias = dependencyAttribute.getParentMTPEAlias();
                AliasTrackerHelper trackerHelper = aliasTracker.getDoaToJsonAliasMap().get(parentDOACode);
                Integer columnJsonLabel = trackerHelper.getAliasNumber();
                AttributeData parentResult = parentRecord.getAttributeMap().get(columnJsonLabel.toString());
                if (parentResult.getcV()!=null) {
                    String currentValue = parentResult.getcV().toString();
                    tempDependencyArray[i] = currentValue;
                    if (tempDependencyMap.containsKey(currentValue)) {
                        if (i == dependencyKeyList.size() - 1) {
                            List<Record> mappedRecordList = (List<Record>) tempDependencyMap.get(currentValue);
                            mappedRecordList.add(parentRecord);
						/* This is the last value and this combination is already present. So do nothing */
                        } else {
                            tempDependencyMap = (Map<String, Object>) tempDependencyMap.get(currentValue);
                        }
                    } else {
                        if (i == dependencyKeyList.size() - 1) {
						/* This is the last value and this combination is not present. So add this array */
                            List<Record> mappedRecordList = new ArrayList<>();
                            mappedRecordList.add(parentRecord);
                            tempDependencyMap.put(currentValue, mappedRecordList);
                            dependencyResultArray.add(tempDependencyArray);
                        } else {
                            Map<String, Object> newDependencyMap = new HashMap<>();
                            tempDependencyMap.put(currentValue, newDependencyMap);
                            tempDependencyMap = newDependencyMap;
                        }
                    }
                }
            }
        }
        mappedParentData.setDependencyResultArray(dependencyResultArray);
        mappedParentData.setDependencyResultMap(dependencyResultMap);
        return mappedParentData;
    }
	/**
     * @param childPE
     * @param searchData
     * @param responseData
     */
	@SuppressWarnings("unchecked")
	private void addFiltersToChildSelect(PERequest childPE, DefaultSearchData searchData,
                                         List<String[]> dependencyResultArray, UnitResponseData responseData) throws SQLException{
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		ExpressionBuilder whereClause = searchData.getWhereClause();
		MasterEntityMetadataVOX childPEMeta = metadataService.getMetadataByMtPE(childPE.getMtPE());
		DbSearchTemplate table = searchData.getTables().get(searchData.getMtPEAlias()+"^"+childPEMeta.getDomainObjectCode());
		/*
		 * Here only dependency keys will be added to filters
		 * 0. First get list of all the dependency keys. 
		 * 1. Loop through the result set of parent process Element data.
		 */
        List<DependencyKeysHelper> dependencyKeyList = childPE.getDependencyKeyList();
		/* Now we have unique combinations of all the dependency key values */
		if(dependencyResultArray.size()!=0){
            whereClause.addExprTokenToTree(Operator.OPEN_PARENTHESIS);
        }
		for(String[] dependencyArray : dependencyResultArray){
			if(!whereClause.isEmpty()){
				whereClause.addExprTokenToTree(Operator.OR);
			}
			whereClause.addExprTokenToTree(Operator.OPEN_PARENTHESIS);
			/* Now for each String[] make the where clause with AND clause */
			for(int i= 0 ; i<dependencyArray.length; i++) {
				String dependencyValue = dependencyArray[i];
				if(i != 0){
					whereClause.addExprTokenToTree(Operator.AND);
				}
				whereClause.addExprTokenToTree(Operator.OPEN_PARENTHESIS);
				MasterEntityAttributeMetadata meta = metadataService.getMetadataByDoa(dependencyKeyList.get(i).getCurrentDOACode());
//				SearchField column = table.getDoAttributeCodeToColumnMap().get(dependencyKeyList.get(i).getCurrentDOACode());
				whereClause.addExprTokenToTree(new LeafOperand(true, meta.getDbColumnName(), meta.getDoAttributeCode(), meta.getDbDataTypeCode(), table.getTableAlias()));
				whereClause.addExprTokenToTree(Operator.EQ);
				whereClause.addExprTokenToTree(new LeafOperand(dependencyValue, meta.getDbDataTypeCode()));
				whereClause.addExprTokenToTree(Operator.CLOSE_PARENTHESIS);
			}
			whereClause.addExprTokenToTree(Operator.CLOSE_PARENTHESIS);
		}
        if(dependencyResultArray.size()!=0){
            whereClause.addExprTokenToTree(Operator.CLOSE_PARENTHESIS);
        }
		/* Adding the record state clause */
        MasterEntityAttributeMetadata recordStateAttribute = childPEMeta.getRecordStateAttribute();
//        if(recordStateAttribute!=null) {
//            whereClause.addExprTokenToTree(Operator.AND);
//            whereClause.addExprTokenToTree(Operator.OPEN_PARENTHESIS);
//            whereClause.addExprTokenToTree(new LeafOperand(true, recordStateAttribute.getDbColumnName(), recordStateAttribute.getDoAttributeCode(), recordStateAttribute.getDbDataTypeCode(), table.getTableAlias()));
//            whereClause.addExprTokenToTree(Operator.EQ);
//            // XXX Hardcoded here
//            whereClause.addExprTokenToTree(new LeafOperand("CURRENT", "TEXT"));
//            whereClause.addExprTokenToTree(Operator.CLOSE_PARENTHESIS);
//        }
        // TODO we shall not need hard coded record state where clause because we can introduce it through control filters
        // Here we are adding control filters to the child pe which will be directly applicable to the child PE
        FilterExpn controlFilterExpn = Util.getFilterExpnFromFilters(childPE.getControlFilters());
        if(controlFilterExpn.isDeleteFilterAdded())
            childPE.setDeleteFilterAdded(true);
        Deque<PERequest> tableIdStack = new ArrayDeque<>();
        String childFilterExpn = "";
        if (StringUtil.isDefined(controlFilterExpn.getFilterExpn()))
            childFilterExpn = controlFilterExpn.getFilterExpn();
        addFiltersOrHavingToRootSelect(childFilterExpn, childPE, searchData, tableIdStack, false);
        FilterExpn controlHavingExpn = Util.getFilterExpnFromFilters(childPE.getControlHavingClause());
        if(controlHavingExpn.isDeleteFilterAdded())
            childPE.setDeleteFilterAdded(true);
        Deque<PERequest> havingTableIdStack = new ArrayDeque<>();
        addFiltersOrHavingToRootSelect(controlHavingExpn.getFilterExpn(), childPE, searchData, havingTableIdStack, true);
	}
	/**
	 * Adds filter to WHERE clause and table to JOIN clause in case filter's table is not present.
	 * Method also adds the dependency keys of root to WHERE clause. 
	 * Filters can come from the filter applied at root process element or children process elements. 
	 * @param root
	 * @param searchData
	 */
	private void addFiltersToRootSelect(PERequest root, DefaultSearchData searchData) throws SQLException{
		/* Add all the Dependency keys and values for the root BT-PE to the WHERE clause */
//		Map<String, Object> dependencyKeyMap = root.getDependencyKeys();
//		addDependencyFieldToFilter(rootPEMeta.getDependencyKeyList(), searchData.getTables().get(rootPEMeta.getDomainObjectCode()), dependencyKeyMap, searchData);
		Deque<PERequest> tableIdStack = new ArrayDeque<>();
		/* Add the filters of root process element to the SELECT clause */
		String rootFilterExpn = root.getFilterExpression();
        FilterExpn controlFilterExpn = Util.getFilterExpnFromFilters(root.getControlFilters());
        if(controlFilterExpn.isDeleteFilterAdded())
            root.setDeleteFilterAdded(true);
        if(StringUtil.isDefined(rootFilterExpn)){
            if(StringUtil.isDefined(controlFilterExpn.getFilterExpn())){
                rootFilterExpn = rootFilterExpn + " AND " + controlFilterExpn.getFilterExpn();
            }
        }else{
            rootFilterExpn = controlFilterExpn.getFilterExpn();
        }
        root.setFilterExpression(rootFilterExpn);
		addFiltersToRootSelect(root, searchData, tableIdStack);

        Deque<PERequest> havingTableIdStack = new ArrayDeque<>();
		/* Add the filters of root process element to the SELECT clause */
        String rootHavingExpn = root.getHavingExpression();
        FilterExpn controlHavingExpn = Util.getFilterExpnFromFilters(root.getControlHavingClause());
        if(controlHavingExpn.isDeleteFilterAdded())
            root.setDeleteFilterAdded(true);
        if(StringUtil.isDefined(rootHavingExpn)){
            if(StringUtil.isDefined(controlHavingExpn.getFilterExpn())){
                rootHavingExpn = rootHavingExpn + " AND " + controlHavingExpn;
            }
        }else{
            rootHavingExpn = controlHavingExpn.getFilterExpn();
        }
        root.setHavingExpression(rootHavingExpn);
        addHavingToRootSelect(root, searchData, havingTableIdStack);
		/* Add the filters of children process element to WHERE clause */
		if(root.getChildrenPE()!=null){	
			for(PERequest childPE : root.getChildrenPE()){
                addChildPEFilterToRootSelect(root, childPE, searchData, tableIdStack);
			}
		}
		/* Add the filters of children process element to HAVING clause */
        if(root.getChildrenPE()!=null){
            for(PERequest childPE : root.getChildrenPE()){
                addChildPEHavingToRootSelect(root, childPE, searchData, tableIdStack);
            }
        }
		addSearchColumnsToFilters(root, searchData);
	}

    private void addOrderByClauseToRootSelect(DefaultSearchData searchData, PERequest root, List<SortData> stickyHeaderOrderByList, List<String> allAttributePathsAddedToSelectClause) {
	    // Now loop through all the sort data list. Get all the sort list which should be applied to root.
        Deque<PERequest> tableIdStack = new ArrayDeque<>();
        List<SortData> rootOrderByClause = new ArrayList<>();
        rootOrderByClause.addAll(root.getSortData());
        getTheRootOrderByClauseFields(root, rootOrderByClause);
        Collections.sort(rootOrderByClause, new SortComparator());
        rootOrderByClause.addAll(root.getDefaultSortData());
        // Now we have got all the sort request sorted.
        // Now loop through all the sort and add it the required join clause and table.
        for(SortData sort : rootOrderByClause){
            String mtPEAlias = sort.getMtPEAlias();
            if(StringUtil.isDefined(mtPEAlias)){
                 tableIdStack.push(root);
                 for(PERequest childPE : root.getChildrenPE()) {
                     addTheChildJoinClauseToTheRoot(sort, tableIdStack, childPE, searchData, mtPEAlias, allAttributePathsAddedToSelectClause);
                 }
                 tableIdStack.pop();
            }else{
                ExpressionBuilder orderByClause = searchData.getOrderByClause();
                addOrderByClause(sort, orderByClause, searchData, allAttributePathsAddedToSelectClause);
//	        addOrderByClause();
            }
        }
        // This can be done at the time of building the response is it?
        // Or loop here and find out the same.


//        addChildPEAttributePathToRootJoinClause(attributePath, tableIdStack, processElement, searchData);
        /* Add the filters of children process element to HAVING clause */
//        if(root.getChildrenPE()!=null){
//            for(PERequest childPE : root.getChildrenPE()){
//                addChildPESortToRootSelect(root, childPE, searchData, tableIdStack);
//            }
//        }
    }

    private void addTheChildJoinClauseToTheRoot(SortData sortData, Deque<PERequest> tableIdStack, PERequest peRequest, DefaultSearchData searchData, String mtPEAlias, List<String> allAttributePathsAddedToSelectClause) {
	    if(peRequest.getMtPEAlias().equals(mtPEAlias)){
	        String attributePath = sortData.getAttributePathExpn().substring(2, sortData.getAttributePathExpn().length()-1);
            addChildPEAttributePathToRootJoinClause(attributePath, tableIdStack, peRequest, searchData);
            FilterExpn controlFilterExpn = Util.getFilterExpnFromFilters(peRequest.getControlFilters());
            addFiltersOrHavingToRootSelect(controlFilterExpn.getFilterExpn(), peRequest, searchData, tableIdStack, false);
            String tableIdStackValues = getAttributePathFromStack(tableIdStack);
            String tableId = new String(tableIdStackValues+":"+peRequest.getMtPEAlias()+"^"+ attributePath.substring(0, attributePath.lastIndexOf('.')));
            DbSearchTemplate table = searchData.getTables().get(tableId);
            ExpressionBuilder orderByClause = searchData.getOrderByClause();
            if(!orderByClause.isEmpty()){
                orderByClause.addExprTokenToTree(Operator.COMMA);
            }
            List<String> doas = Util.getAttributePathFromExpression(sortData.getAttributePathExpn());
            String doa = doas.get(doas.size()-1);
            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
            MasterEntityAttributeMetadata doaMeta = metadataService.getMetadataByDoa(doa);
            orderByClause.addExprTokenToTree(new LeafOperand(true, doaMeta.getDbColumnName(), doaMeta.getDoAttributeCode(),null, doaMeta.getDbDataTypeCode(), table.getTableAlias(), doaMeta.isGlocalized(), null));
            if(sortData.isAscending()){
                orderByClause.addExprTokenToTree(Operator.ASC);
            }else{
                orderByClause.addExprTokenToTree(Operator.DESC);
            }
            // Now add this to the order by clause;
            return;
        }
        tableIdStack.push(peRequest);
        for(PERequest childPE : peRequest.getChildrenPE()) {
            addTheChildJoinClauseToTheRoot(sortData, tableIdStack, childPE, searchData, mtPEAlias, allAttributePathsAddedToSelectClause);
        }
        tableIdStack.pop();
    }

    private void getTheRootOrderByClauseFields(PERequest root, List<SortData> rootOrderByClause) {
        if(root.getSortData()!=null && root.getSortData().size()!=0){
            for(SortData sortData : root.getSortData()){
                if(sortData.isApplySortToRoot()){
                    sortData.setMtPEAlias(root.getMtPEAlias());
                    rootOrderByClause.add(sortData);
                }
            }
        }
        for(PERequest childPE : root.getChildrenPE()){
            getTheRootOrderByClauseFields(childPE, rootOrderByClause);
        }
    }

    private String prepareMenuFilterExpn(PERequest root, UnitResponseData responseData) throws SQLException{
	    // Get the group IDs this person belong to
        PersonGroup group = TenantAwareCache.getPersonRepository().getPersonGroups(TenantContextHolder.getUserContext().getPersonId());
        StringBuilder groupFilterExpn = new StringBuilder("");
        StringBuilder subjectUserMenuRelationExpn = new StringBuilder();
        StringBuilder additionalSubjectUserQualificationFilterExpn = new StringBuilder();
        // Now build the logged in user groups Filter expression
        if(group.getGroupIds() != null && !group.getGroupIds().isEmpty()){
            List<String> groupIDs = group.getGroupIds();
            groupFilterExpn = new StringBuilder("(");
            for(int i =0; i< groupIDs.size(); i++){
                if(i!=0)
                    groupFilterExpn.append(" OR ");
                groupFilterExpn.append("(");
                groupFilterExpn.append("!{MenuPermission.PermissionValue} like ");
                groupFilterExpn.append("'%").append(groupIDs.get(i)).append("%'");
                groupFilterExpn.append(")");
            }
            groupFilterExpn.append(")");
        }
        // Now build the logged in user and subject user relations Filter expression
        String menuApplicableSubjectUserMTPEAlias = root.getMenuApplicableSubjectUserMTPEAlias();
        String menuApplicableSubjectUserAttributePath = root.getMenuApplicableSubjectUserPath();
        if(StringUtil.isDefined(menuApplicableSubjectUserMTPEAlias)){
            List<Relation> relations = new ArrayList<>();
            // It means Menu is Subject User driven
            ProcessElementData subjectUserData = responseData.getPEData().get(menuApplicableSubjectUserMTPEAlias);
            if(subjectUserData != null){
                List<Record> recordList = (List<Record>) subjectUserData.getRecordSet();
                if(recordList != null){
                    Record record = recordList.get(0);
                    AttributeData subjectUserIDAttributeData = record.getAttributeMap().get(menuApplicableSubjectUserAttributePath);
                    if(record != null){
                        relations = record.getRelations();
                    }else{
                        // Here we should throw error
                    }
                }else{
                    // Here we should throw error
                }
            }else{
                // Here we should throw error
            }
            if(relations!=null && !relations.isEmpty()){
                subjectUserMenuRelationExpn.append("(");
                for(int i = 0; i< relations.size(); i++){
                    if(i!=0)
                        subjectUserMenuRelationExpn.append(" OR ");
                    subjectUserMenuRelationExpn.append("!{MenuPermission.PermissionValue} like ");
                    subjectUserMenuRelationExpn.append("'%|").append(relations.get(i).getRelationshipType()).append("|%'");
                }
                subjectUserMenuRelationExpn.append(")");
            }

        }
        // Now build the subject user qualification Filter expression
        if(StringUtil.isDefined(menuApplicableSubjectUserMTPEAlias)){
            ProcessElementData subjectUserData = responseData.getPEData().get(menuApplicableSubjectUserMTPEAlias);
            if(subjectUserData != null){
                List<Record> recordList = (List<Record>) subjectUserData.getRecordSet();
                if(recordList != null){
                    AttributeData subjectUserIDAttributeData = recordList.get(0).getAttributeMap().get(String.valueOf(TenantAwareCache.getMetaDataRepository().getAlias(root.getMenuApplicableSubjectUserPath())));
                    if(subjectUserIDAttributeData!=null){
                        String subjectUserID = subjectUserIDAttributeData.getcV().toString();
                        // Now we need to get all the groups that this subject user belongs to.
                        Person subjectUserPerson = TenantAwareCache.getPersonRepository().getPersonDetails(subjectUserID);
                        List<String> subjectUserGroupsIDs = subjectUserPerson.getPersonGroups().getGroupIds();
                        // Now we also have all the groups that this person belongs too.
                        // Now we have to check if !{Menu.AdditionalSubjectUserApplicableGroups} is not null and !{Menu.AdditionalSubjectUserApplicableGroups} in (subject user groups then this Menu is applicable)
                        additionalSubjectUserQualificationFilterExpn.append("(!{MenuPermission.SubjectUserApplicableGroup} IS NOT NULL AND (");
                        for(int i=0; i<subjectUserGroupsIDs.size(); i++){
                            if(i!=0)
                                additionalSubjectUserQualificationFilterExpn.append(" OR ");
                            additionalSubjectUserQualificationFilterExpn.append("(");
                            additionalSubjectUserQualificationFilterExpn.append("!{MenuPermission.SubjectUserApplicableGroup} like ");
                            additionalSubjectUserQualificationFilterExpn.append("'%").append(subjectUserGroupsIDs.get(i)).append("%'");
                            additionalSubjectUserQualificationFilterExpn.append(")");
                        }
                        additionalSubjectUserQualificationFilterExpn.append(")");
                        additionalSubjectUserQualificationFilterExpn.append(")");
                    }
                }
            }
        }
        StringBuilder menuFilterExpn = new StringBuilder("");
        if(StringUtil.isDefined(groupFilterExpn.toString())){
            menuFilterExpn.append(groupFilterExpn);
            if(StringUtil.isDefined(subjectUserMenuRelationExpn.toString())){
                menuFilterExpn.append(" OR ");
                menuFilterExpn.append(subjectUserMenuRelationExpn);
            }
        }else{
            menuFilterExpn.append(subjectUserMenuRelationExpn);
        }
        if(StringUtil.isDefined(additionalSubjectUserQualificationFilterExpn.toString())){
            menuFilterExpn.append(" AND ");
            menuFilterExpn.append(additionalSubjectUserQualificationFilterExpn);
        }
        if(StringUtil.isDefined(menuFilterExpn.toString()))
            return "("+menuFilterExpn.toString()+")";
	    // First add all the groups.
        return menuFilterExpn.toString();
    }

    private void addSearchColumnsToFilters(PERequest root, DefaultSearchData searchData) {
	    /*
             If keyword is defined we need to add it to the filter
             Get all the columns which are local searchable
             now add these columns as MATCH(columnName) AGAINST_IN_BOOLEAN_MODE ('keyword')
        */
        if(StringUtil.isDefined(root.getKeyWord())){
            ExpressionBuilder whereClause = searchData.getWhereClause();
            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
            boolean isSearchFilterAlreadyAdded = false;
            int aliasCount=0;
            for(String searchableAttributePathExpn : root.getAttributePaths().getSearchableAttributePaths()) {
                // From this attribute path get the last DOA
                List<String> attributePaths = Util.getAttributePathFromExpression(searchableAttributePathExpn);
                if (attributePaths != null && !attributePaths.isEmpty()) {
                    String attributePath = attributePaths.get(0);
                    String lastDOA = attributePaths.get(attributePaths.size() - 1);
                    MasterEntityAttributeMetadata doaMeta = metadataService.getMetadataByDoa(Util.getDOAFromAttributePath(lastDOA));
                    if (doaMeta.isLocalSearchable()) {
                        if(aliasCount == 0) {
                            root.getSortData().clear();
                            SortData sortData = new SortData();
                            sortData.setAttributePathExpn(searchableAttributePathExpn);
                            sortData.setOrderByValues("'"+root.getKeyWord()+"'");
                            sortData.setSeqNumber(0);
                            root.getSortData().add(sortData);
                        }
                        // Now check if this is the first time
                        if (!isSearchFilterAlreadyAdded) {
                            if (!whereClause.isEmpty()) {
                                whereClause.addExprTokenToTree(Operator.AND);
                            }
                            whereClause.addExprTokenToTree("OPEN_PARENTHESIS");
                            isSearchFilterAlreadyAdded = true;
                        } else {
                            whereClause.addExprTokenToTree(Operator.OR);
                        }
                        whereClause.addExprTokenToTree(Operator.MATCH);
                        whereClause.addExprTokenToTree("OPEN_PARENTHESIS");
                        addAttributePathToJoinClause(attributePath, searchData);
                        String tableId = searchData.getMtPEAlias()+"^"+attributePath.substring(0, attributePath.lastIndexOf("."));
                        DbSearchTemplate table = searchData.getTables().get(tableId);
                        whereClause.addExprTokenToTree(new LeafOperand(true, doaMeta.getDbColumnName(), doaMeta.getDoAttributeCode(), doaMeta.getDbDataTypeCode(), table.getTableAlias()));
                        whereClause.addExprTokenToTree("CLOSE_PARENTHESIS");
                        whereClause.addExprTokenToTree(Operator.AGAINST_IN_BOOLEAN_MODE);
                        whereClause.addExprTokenToTree("OPEN_PARENTHESIS");
                        whereClause.addExprTokenToTree(new LeafOperand(root.getKeyWord()+"*", "TEXT"));
                        whereClause.addExprTokenToTree("CLOSE_PARENTHESIS");

                        // Add the expression to select clause to get the proper relevance.
                        ExpressionBuilder selectClause = searchData.getSelectClause();
                        if (!selectClause.isEmpty()) {
                            selectClause.addExprTokenToTree(Operator.COMMA);
                        }
                        selectClause.addExprTokenToTree(Operator.MATCH);
                        selectClause.addExprTokenToTree("OPEN_PARENTHESIS");
                        addAttributePathToJoinClause(attributePath, searchData);
                        selectClause.addExprTokenToTree(new LeafOperand(true, doaMeta.getDbColumnName(), doaMeta.getDoAttributeCode(), doaMeta.getDbDataTypeCode(), table.getTableAlias()));
                        selectClause.addExprTokenToTree("CLOSE_PARENTHESIS");
                        selectClause.addExprTokenToTree(Operator.AGAINST_IN_BOOLEAN_MODE);
                        selectClause.addExprTokenToTree("OPEN_PARENTHESIS");
                        selectClause.addExprTokenToTree(new LeafOperand(root.getKeyWord() + "*", "TEXT"));
                        selectClause.addExprTokenToTree("CLOSE_PARENTHESIS");
                        /* Now add the Alias*/
                        selectClause.addExprTokenToTree(Operator.AS);
                        LeafOperand aliasName = new LeafOperand("relevance-"+aliasCount, "TEXT");
                        aliasName.setColumnAlias(true);
                        selectClause.addExprTokenToTree(aliasName);
                        SearchField searchField = new SearchField("relevance-"+aliasCount, "BOOLEAN");
                        searchData.getColumnLabelToColumnMap().put("relevance-"+aliasCount, searchField);
                        // Add this alias to orderby clause
                        SortData sortData = new SortData();
                        sortData.setAttributePathExpn("relevance-"+aliasCount);
                        sortData.setAscending(false);
                        sortData.setIsAscending(false);
                        sortData.setSeqNumber(aliasCount+1);
                        root.getSortData().add(sortData);
                        aliasCount++;
                    }
                }
            }
            if(isSearchFilterAlreadyAdded)
                whereClause.addExprTokenToTree("CLOSE_PARENTHESIS");


//                List<String> attributePaths = Util.getAttributePathFromExpression(searchableAttributePathExpn);
//                if(attributePaths!= null && !attributePaths.isEmpty()) {
//
//                    // Check if that is searchable or not and build the string accordingly.
//
//
//
//
//                    if(!whereClause.isEmpty()){
//                        whereClause.addExprTokenToTree(Operator.AND);
//                    }
//                    whereClause.addExprTokenToTree("OPEN_PARENTHESIS");
//                    for (int i=0; i<attributePaths.size(); i++ ){
//                        String attributePath = attributePaths.get(i);
////                    for (String attributePath : attributePaths){
//                        // Now we have attribute path ..
//                        String doa = Util.getDOAFromAttributePath(attributePath);
//                        // If this doa is searchable or not is what we need to find out rightnow
//                        // TODO here we should have btDOA as searchable or not.. If the client needs to modify it.
//                        MasterEntityAttributeMetadata doaMeta = metadataService.getMetadataByDoa(doa);
//                        if(doaMeta.isLocalSearchable()){
//                            if(i!=0){
//                                whereClause.addExprTokenToTree(Operator.OR);
//                            }else{
//
//                            }
//                            whereClause.addExprTokenToTree(Operator.MATCH);
//                            whereClause.addExprTokenToTree("OPEN_PARENTHESIS");
//                            addAttributePathToJoinClause(attributePath, searchData);
//                            String tableId = attributePath.substring(0,attributePath.lastIndexOf("."));
//                            DbSearchTemplate table = searchData.getTables().get(tableId);
//                            whereClause.addExprTokenToTree(new LeafOperand(true, doaMeta.getDbColumnName(), doaMeta.getDoAttributeCode(), doaMeta.getDbDataTypeCode(), table.getTableAlias()));
//                            whereClause.addExprTokenToTree("CLOSE_PARENTHESIS");
//                            whereClause.addExprTokenToTree(Operator.AGAINST_IN_BOOLEAN_MODE);
//                            whereClause.addExprTokenToTree("OPEN_PARENTHESIS");
//                            whereClause.addExprTokenToTree(new LeafOperand(root.getKeyWord()+"*", "TEXT"));
//                            whereClause.addExprTokenToTree("CLOSE_PARENTHESIS");
//                        }
//                    }
//                    whereClause.addExprTokenToTree("CLOSE_PARENTHESIS");
//                }
//            }
//            MasterEntityMetadataVOX meta = metadataService.getMetadataByMtPE(root.getMtPE());
//            List<MasterEntityAttributeMetadata> localSearchableFields = meta.getLocalSearchableFields();
////            ExpressionBuilder whereClause = searchData.getWhereClause();
//            for(MasterEntityAttributeMetadata textField : localSearchableFields){
//                if(!whereClause.isEmpty()){
//                    whereClause.addExprTokenToTree(Operator.AND);
//                }
//                whereClause.addExprTokenToTree(Operator.MATCH);
//                whereClause.addExprTokenToTree("OPEN_PARENTHESIS");
//                String attributePath = textField.getDoAttributeCode();
//                addAttributePathToJoinClause(attributePath, searchData);
//                String tableId = attributePath.substring(0,attributePath.lastIndexOf("."));
//                DbSearchTemplate table = searchData.getTables().get(tableId);
//                whereClause.addExprTokenToTree(new LeafOperand(true, textField.getDbColumnName(), textField.getDoAttributeCode(), textField.getDbDataTypeCode(), table.getTableAlias()));
//                whereClause.addExprTokenToTree("CLOSE_PARENTHESIS");
//                whereClause.addExprTokenToTree(Operator.AGAINST_IN_BOOLEAN_MODE);
//                whereClause.addExprTokenToTree("OPEN_PARENTHESIS");
//                whereClause.addExprTokenToTree(new LeafOperand(root.getKeyWord()+"*", "TEXT"));
//                whereClause.addExprTokenToTree("CLOSE_PARENTHESIS");
//            }
        }
    }

    private void addChildPEFilterToRootSelect(PERequest parentPE, PERequest childPE, DefaultSearchData searchData, Deque<PERequest> tableIdStack) {
		tableIdStack.push(parentPE);
		addFiltersToRootSelect(childPE, searchData, tableIdStack);
		/*
		 * Now traverse the children of this child.
		 */
		if(childPE.getChildrenPE()!=null){
			for(PERequest childPEChild : childPE.getChildrenPE()){
                addChildPEFilterToRootSelect(childPE, childPEChild, searchData, tableIdStack);
			}
		}
		tableIdStack.pop();
	}

    private void addChildPEHavingToRootSelect(PERequest parentPE, PERequest childPE, DefaultSearchData searchData, Deque<PERequest> tableIdStack) {
        tableIdStack.push(parentPE);
        addHavingToRootSelect(childPE, searchData, tableIdStack);
		/*
		 * Now traverse the children of this child.
		 */
        if(childPE.getChildrenPE()!=null){
            for(PERequest childPEChild : childPE.getChildrenPE()){
                addChildPEHavingToRootSelect(childPE, childPEChild, searchData, tableIdStack);
            }
        }
        tableIdStack.pop();
    }
	
	private void addFiltersToRootSelect(PERequest processElement, DefaultSearchData searchData, Deque<PERequest> tableIdStack) {
		String rootFilterExpn = processElement.getFilterExpression();
		addFiltersOrHavingToRootSelect(rootFilterExpn, processElement, searchData, tableIdStack, false);
	}
    private void addHavingToRootSelect(PERequest processElement, DefaultSearchData searchData, Deque<PERequest> tableIdStack) {
        String rootHavingExpn = processElement.getHavingExpression();
        addFiltersOrHavingToRootSelect(rootHavingExpn, processElement, searchData, tableIdStack, true);
    }
	/**
	 * @param filterExpn
	 * @param searchData
	 * @param tableIdStack
	 */
	private void addFiltersOrHavingToRootSelect(String filterExpn, PERequest processElement, DefaultSearchData searchData,
			Deque<PERequest> tableIdStack, boolean isHavingClause) {
		if(StringUtil.isDefined(filterExpn)){
			MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
			ExpressionBuilder expression = null;
			if(isHavingClause){
				expression = searchData.getHavingClause();
			}else{
				expression = searchData.getWhereClause();
			}
			if(!expression.isEmpty()){
				expression.addExprTokenToTree(Operator.AND);
			}
			Lexer lexer = new Lexer();
			LexerResult lexerResult = lexer.getTokens(filterExpn);
			List<Object> tokens = lexerResult.getTokens();
			for(Object token : tokens){
				if(token instanceof LeafOperand && ((LeafOperand) token).getIsColumnName()){
					/* Find all the places where a token is a column and add all the properties by searching for its meta and table alias. */
					LeafOperand leafToken = (LeafOperand) token;
					String attributePath = leafToken.getValue();
					String doa = attributePath;
					if(attributePath.contains(":")){
						doa = attributePath.substring(attributePath.lastIndexOf(":")+1);
					}
					if(tableIdStack.isEmpty()){
						/* If it is going up in the table order */
						addAttributePathToJoinClause(attributePath, searchData);
					}else{
						/* If it is going down the table order */
						addChildPEAttributePathToRootJoinClause(attributePath, tableIdStack, processElement, searchData);
					}
					String tableIdStackValue = getAttributePathFromStack(tableIdStack);
					String tableId = null;
					if(StringUtil.isDefined(tableIdStackValue)){
						tableId = tableIdStackValue+":"+processElement.getMtPEAlias()+"^"+attributePath.substring(0,attributePath.lastIndexOf("."));
					}else{
						tableId = searchData.getMtPEAlias()+"^"+attributePath.substring(0,attributePath.lastIndexOf("."));
					}
					DbSearchTemplate table = searchData.getTables().get(tableId);
					MasterEntityAttributeMetadata doaMeta = metadataService.getMetadataByDoa(doa);
					expression.addExprTokenToTree(new LeafOperand(true, doaMeta.getDbColumnName(), doaMeta.getDoAttributeCode(), null, doaMeta.getDbDataTypeCode(), table.getTableAlias(), doaMeta.isGlocalized(),doaMeta.getContainerBlobDbColumnName()));
				}else{
					expression.addExprTokenToTree(token);
				}
			}
		}
        String tableIdStackValue = getAttributePathFromStack(tableIdStack);
        if(!processElement.isDeleteFilterAdded() && !StringUtil.isDefined(tableIdStackValue) && !isHavingClause){
            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
            ExpressionBuilder expression = null;
            MasterEntityMetadataVOX metadataVOX = metadataService.getMetadataByMtPE(processElement.getMtPE());
            MasterEntityAttributeMetadata deletedAttribute =metadataVOX.getIsDeletedAttribute();
            if(deletedAttribute!=null){
                expression = searchData.getWhereClause();
                if(!expression.isEmpty()){
                    expression.addExprTokenToTree(Operator.AND);
                }
                String tableId = searchData.getMtPEAlias()+"^"+metadataVOX.getDomainObjectCode();
                DbSearchTemplate table = searchData.getTables().get(tableId);
                LeafOperand deleteAttributeOperand = new LeafOperand(true,deletedAttribute.getDbColumnName(), deletedAttribute.getDoAttributeCode(), deletedAttribute.getDbDataTypeCode(), table.getTableAlias());
                expression.addExprTokenToTree(deleteAttributeOperand);
                expression.addExprTokenToTree(Operator.EQ);
                LeafOperand operand = new LeafOperand("0", "BOOLEAN");
                expression.addExprTokenToTree(operand);
            }
        }
	}
	private void addDependencyFieldToFilter(List<EntityAttributeMetadata> dependencyKeysList,DbSearchTemplate primaryDo, Map<String, Object> dependencyKeyMap, DefaultSearchData searchData){
		ExpressionBuilder whereClause = searchData.getWhereClause();
		for(int i = 0; i<dependencyKeysList.size(); i++){
			String doAttributeCode = dependencyKeysList.get(i).getDoAttributeCode();
			SearchField dependencyColumn = primaryDo.getDoAttributeCodeToColumnMap().get(doAttributeCode); //columnLabelToColumnMap.get(dependencyKeysList.get(i));
			if(i==0 && whereClause.getExpressionTree()==null){
				whereClause.addExprTokenToTree(Operator.OPEN_PARENTHESIS);
				addDependencyFieldToFilter(dependencyKeyMap,primaryDo,dependencyColumn, searchData);
			}else{
				whereClause.addExprTokenToTree(Operator.AND);
				addDependencyFieldToFilter(dependencyKeyMap,primaryDo,dependencyColumn, searchData);
			}
		}
		if(dependencyKeysList.size()!=0)
			whereClause.addExprTokenToTree(Operator.CLOSE_PARENTHESIS);
	}
	
	private void addDependencyFieldToFilter(Map<String, Object> dependencyKeysMap, DbSearchTemplate primaryDo, SearchField searchField, DefaultSearchData searchData) {
		if(dependencyKeysMap == null)
			throw new IllegalArgumentException("Dependency key map is not provided.");
		ExpressionBuilder whereClause = searchData.getWhereClause();
		String columnLabel = searchField.getDoAtributeCode();
		if(dependencyKeysMap.containsKey(columnLabel)){
			@SuppressWarnings("unchecked")
			ArrayList<String> values = ((ArrayList<String>) dependencyKeysMap.get(columnLabel));
			LeafOperand filterOperand = new LeafOperand(true,searchField.getColumnName(), searchField.getDoAtributeCode(), searchField.getDataType(), primaryDo.getTableAlias());
			whereClause.addExprTokenToTree(filterOperand);
			whereClause.addExprTokenToTree(Operator.IN);
			whereClause.addExprTokenToTree(Operator.OPEN_PARENTHESIS);
			for(int i=0; i<values.size(); i++){
				String inValue = values.get(i);
				if(i==0){
					LeafOperand operand = new LeafOperand(inValue, searchField.getDataType());
					whereClause.addExprTokenToTree(operand);
				}else{
					whereClause.addExprTokenToTree(Operator.COMMA);
					LeafOperand operand = new LeafOperand(inValue, searchField.getDataType());
					whereClause.addExprTokenToTree(operand);
				}
			}
			whereClause.addExprTokenToTree(Operator.CLOSE_PARENTHESIS);
		}else{
			throw new IllegalArgumentException("Dependency key {} "+columnLabel+" is not provided.");
		}
	}
	/**
	 * Inner class for defining compare logic for Sorting sort request by sequence number.
	 * @author Shubham Patodi
	 *
	 */
	class SortComparator implements Comparator<SortData>{
		@Override
		public int compare(SortData o1, SortData o2) {
			if(o1.getSeqNumber() > o2.getSeqNumber()){
				return 1;
			}else{
				return -1;
			}	
		}	
	}
	/**
	 * Inner class for defining compare logic for Group By sequence number.
	 * @author Shubham Patodi
	 *
	 */
	class GroupByComparator implements Comparator<GroupByAttributeDetails>{
		@Override
		public int compare(GroupByAttributeDetails o1, GroupByAttributeDetails o2) {
			if(o1.getGroupByAttributeSeqNumber() > o2.getGroupByAttributeSeqNumber()){
				return 1;
			}else{
				return -1;
			}
		}
	}
	public void addParameters(DefaultSearchData searchData, Object parameter, String dbDatatype, Boolean isCountParameter ){
		if(parameter instanceof String){
			parameter = getData((String)parameter, dbDatatype);
		}
		searchData.getParametersQue().add(parameter);
		if(isCountParameter)
			searchData.getCountParameterQue().add(parameter);
	}
	private Object getData(String value, String dataType) {
        Object data = null;
        switch (dataType) {
        	case "T_ID":
        		data = "0x"+value;
            case "CHAR":
            case "VARCHAR":
                data = value;
                break;
            case "DATETIME":
                data = new Timestamp(Long.parseLong(value));
                break;
            case "BOOLEAN":
                data = Boolean.getBoolean(value);
                break;
            case "INTEGER":
            case "NUMERIC":
                data = Long.valueOf(value);
                break;
            default:
                data = value;
        }
        return data;
    }
	public SearchService getSearchService() {
		return searchService;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	public UILayoutService getUiLayoutService() {
		return uiLayoutService;
	}

	public void setUiLayoutService(UILayoutService uiLayoutService) {
		this.uiLayoutService = uiLayoutService;
	}

    public ResponseDataBuilder getResponseDataBuilder() {
        return responseDataBuilder;
    }

    public void setResponseDataBuilder(ResponseDataBuilder responseDataBuilder) {
        this.responseDataBuilder = responseDataBuilder;
    }

    public HierarchyService getHierarchyService() {
        return hierarchyService;
    }

    public void setHierarchyService(HierarchyService hierarchyService) {
        this.hierarchyService = hierarchyService;
    }
    /*	/**
	 * @deprecated
	 * @param entityMetadata
	 * @param requestData
	 * @return
	 */
//	@SuppressWarnings("unused")
//	private DefaultSearchData createSearchData(EntityMetadata entityMetadata, UnitRequestData requestData) {
//		DefaultSearchData searchData = new DefaultSearchData();
//		BeanUtils.copyProperties(requestData, searchData);
//		//copy filters to search data
//		Map<String, List<FilterOperatorValue>> filterMap = requestData.getFilters();
//		if(filterMap!=null && !filterMap.isEmpty()){
//			for(Map.Entry<String,List<FilterOperatorValue>> entry : filterMap.entrySet()){
//				String attributeName = entry.getKey();
//				List<FilterOperatorValue> opValList = entry.getValue();
//				List<DomainObjectFilter> searchDataOpVal = new ArrayList<>();
//				for(FilterOperatorValue opVal : opValList){
//					DomainObjectFilter dOfilter = new DomainObjectFilter();
//					dOfilter.setOperator(opVal.getOperator());
////					dOfilter.setValue(opVal.getValue());
//					searchDataOpVal.add(dOfilter);
//				}
//				searchData.getExternalFilters().put(attributeName, searchDataOpVal);
//			}
//		}
//		//Copy entityMetadata Values into Searchdata.
//		DomainObjectTemplate domainObject = new DomainObjectTemplate();
//		BeanUtils.copyProperties(entityMetadata, domainObject);
//		for(EntityAttributeMetadata attribute : entityMetadata.getAttributeMeta()){
//			DomainObjectAttributeTemplate DOattribute = new DomainObjectAttributeTemplate();
//			BeanUtils.copyProperties(attribute, DOattribute);
//			for(EntityAttributeRelationMetadata relationTableField : attribute.getRelationMeta()){
//				DomainObjectAttributeRelationFields DOArelationField = new DomainObjectAttributeRelationFields();
//				BeanUtils.copyProperties(relationTableField, DOArelationField);
//				DOattribute.getRelationTableFields().put(DOArelationField.getMtDoAttributeSpecG11nText(),DOArelationField);
//			}
//			domainObject.getSearchFields().put(DOattribute.getMtDoAttributeSpecFkG11nText(),DOattribute);
//		}
//		searchData.setDomainObjectTemplate(domainObject);
//		return searchData;
//	}

    @Override
    public WorkflowActionAttributePermission getControlAndActionPermissionForWorkflow(String workflowTxnId) throws EvaluationException, SQLException {
//        MasterEntityMetadataVOX masterEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(mtPE);
        String mtPE = getUnderlyingRecordMTPE(workflowTxnId);
//        String doName = masterEntityMetadataVOX.getDomainObjectCode();
//        if (recordList != null && !recordList.isEmpty()) {
//            Map<String, Object> row = recordList.get(0);
//            String workflowTxnId = (String) row.get(doName + ".RefWorkflowTxn");
            WorkflowActionAttributePermission permission = getPermissionsForRecord(workflowTxnId, mtPE);// FIXME check if status is being check for underlying record or not!
//        }
        return permission;
    }

    @Override
    public List<WorkflowActorAction> getWorkflowActionsForLoggedInUser(String workflowTxnId) throws SQLException {
        return workflowUtility.getWorkflowActionsForLoggedInUser(workflowTxnId);
    }

    private WorkflowActionAttributePermission getPermissionsForRecord(String workflowTxnId, String mtPE) throws SQLException, EvaluationException {
        String loggedInPersonId = TenantContextHolder.getUserContext().getPersonId();
        Map<String, String> workflowActionToIsActionedMap = workflowUtility.getWorkflowActionToIsActionedMap(workflowTxnId);
        Map<String, Map<Integer, WorkflowActionPermissionDefinition>> workflowActionPermissionMap = workflowUtility.getPermissionDefinitionForActorPerson(workflowTxnId, loggedInPersonId);
        WorkflowActionAttributePermission permission = null;
        if (workflowActionPermissionMap != null) {
            permission = new WorkflowActionAttributePermission();
            permission.setActorExists(true);
            Map<String, Map<String, ActorActionPermission>> mtPEToActorActionPermissionMapFinal = new HashMap<>();
            Map<String, Map<String, ActorAttributePermission>> mtPEToActorAttributePermissionMapFinal = new HashMap<>();
            for (Map<Integer, WorkflowActionPermissionDefinition> priorityToActionPermissionMap : workflowActionPermissionMap.values()) {
                int i = 1;
                Evaluator evaluator = new Evaluator();
                evaluator.setVariables(workflowActionToIsActionedMap);
                while (priorityToActionPermissionMap.containsKey(i)) {
                    String expn = priorityToActionPermissionMap.get(i).getWorkflowActionedExpn();
                    expn = replaceIdToBoolean(expn, workflowActionToIsActionedMap);
                    if (evaluator.getBooleanResult(expn)) {
                        WorkflowActionPermissionDefinition permissionDefinition = priorityToActionPermissionMap.get(i);
                        if (permissionDefinition.getWorkflowActorDefinitionId() != null) {
                            Map<String, Map<String, ActorActionPermission>> mtPEToActorActionPermissionMap = workflowUtility.getActorActionPermissions(permissionDefinition.getWorkflowActionPermDefPkId());
                            updateActionPermissions(mtPEToActorActionPermissionMapFinal, mtPEToActorActionPermissionMap);
                            Map<String, Map<String, ActorAttributePermission>> mtPEToActorAttributePermissionMap = workflowUtility.getActorAttributePermissions(permissionDefinition.getWorkflowActionPermDefPkId());
                            updateAttributePermissions(mtPEToActorAttributePermissionMapFinal, mtPEToActorAttributePermissionMap);
                        } else {
                            //permission for people other than actor
                            Map<String, Map<String, NonActorActionPermission>> mtPEToNonActorActionPermissionMap = workflowUtility.getNonActorActionPermission(permissionDefinition.getWorkflowActionPermDefPkId());
                            Map<String, Map<String, NonActorAttributePermission>> mtPEToNonActorAttributePermissionMap = workflowUtility.getNonActorAttributePermission(permissionDefinition.getWorkflowActionPermDefPkId());
                            permission.setNonActorActionPermissionMap(mtPEToNonActorActionPermissionMap);
                            permission.setNonActorAttributePermissionMap(mtPEToNonActorAttributePermissionMap);
                            permission.setActorExists(false);
                        }
                        break;
                    }
                    i++;
                }
            }
            permission.setMtPEToActorActionPermissionMap(mtPEToActorActionPermissionMapFinal);
            permission.setMtPEToActorAttributePermissionMap(mtPEToActorAttributePermissionMapFinal);
        }
        return permission;
    }

    private String replaceIdToBoolean(String expn, Map<String, String> workflowActionToIsActionedMap) {
        for (Map.Entry<String, String> entry : workflowActionToIsActionedMap.entrySet()){
            expn = expn.replace(entry.getKey(), entry.getValue());
        }
        return expn;
    }

    private void updateAttributePermissions(Map<String, Map<String, ActorAttributePermission>> mtPEToActorAttributePermissionMapFinal, Map<String, Map<String, ActorAttributePermission>> mtPEToActorAttributePermissionMap) {
        if (mtPEToActorAttributePermissionMap != null) {
            for (Map.Entry<String, Map<String, ActorAttributePermission>> mtPEToAttributePermissionEntryMap : mtPEToActorAttributePermissionMap.entrySet()) {
                if (mtPEToActorAttributePermissionMapFinal.containsKey(mtPEToAttributePermissionEntryMap.getKey())) {
                    for (Map.Entry<String, ActorAttributePermission> entry : mtPEToAttributePermissionEntryMap.getValue().entrySet()) {
                        Map<String, ActorAttributePermission> attributePermissionMapFinal = mtPEToActorAttributePermissionMapFinal.get(mtPEToAttributePermissionEntryMap.getKey());
                        if (attributePermissionMapFinal.containsKey(entry.getKey())) {
                            ActorAttributePermission attrPermission = attributePermissionMapFinal.get(entry.getKey());
                            if (!attrPermission.isAttrEditable())
                                attrPermission.setAttrEditable(entry.getValue().isAttrEditable());
                            if (!attrPermission.isAttrVisible())
                                attrPermission.setAttrVisible(entry.getValue().isAttrVisible());
                        } else {
                            attributePermissionMapFinal.put(entry.getKey(), entry.getValue());
                        }
                    }
                }
                else {
                    mtPEToActorAttributePermissionMapFinal.put(mtPEToAttributePermissionEntryMap.getKey(), mtPEToAttributePermissionEntryMap.getValue());
                }
            }
        }
    }

    private void updateActionPermissions(Map<String, Map<String, ActorActionPermission>> mtPEToActorActionPermissionMapFinal, Map<String, Map<String, ActorActionPermission>> mtPEToActorActionPermissionMap) {
        if (mtPEToActorActionPermissionMap != null){
            for (Map.Entry<String, Map<String, ActorActionPermission>> mtPEToActionPermissionEntryMap : mtPEToActorActionPermissionMap.entrySet()) {
                if (mtPEToActorActionPermissionMapFinal.containsKey(mtPEToActionPermissionEntryMap.getKey())) {
                    for (Map.Entry<String, ActorActionPermission> entry : mtPEToActionPermissionEntryMap.getValue().entrySet()) {
                        Map<String, ActorActionPermission> actionPermissionMapFinal = mtPEToActorActionPermissionMapFinal.get(mtPEToActionPermissionEntryMap.getKey());
                        if (actionPermissionMapFinal.containsKey(entry.getKey())) {
                            ActorActionPermission actorActionPermission = actionPermissionMapFinal.get(entry.getKey());
                            if (!actorActionPermission.isActionApplicable()) {
                                actorActionPermission.setActionApplicable(entry.getValue().isActionApplicable());
                            }
                        } else {
                            actionPermissionMapFinal.put(entry.getKey(), entry.getValue());
                        }
                    }
                }
                else{
                    mtPEToActorActionPermissionMapFinal.put(mtPEToActionPermissionEntryMap.getKey(), mtPEToActionPermissionEntryMap.getValue());
                }
            }
        }
    }

    private String getUnderlyingRecordMTPE(String workflowTxnId) throws SQLException {
        String mtPE = null;
        String sql = "SELECT MT_PE_CODE_FK_ID FROM T_PFM_IEU_WORKFLOW_TXN INNER JOIN T_PFM_ADM_BUSINESS_RULE ON BUSS_RULE_CODE_PK_ID = BUSS_RULE_CODE_FK_ID WHERE WORKFLOW_TXN_PK_ID = x?;";
        Deque<Object> parameter = new ArrayDeque<>();
        parameter.add(Util.remove0x(workflowTxnId));
        List<Map<String, Object>> recordList = searchService.search(sql, parameter);
        if (recordList != null && !recordList.isEmpty()) {
            Map<String, Object> row = recordList.get(0);
            mtPE = (String) row.get("MT_PE_CODE_FK_ID");
        }
        return mtPE;
    }

    public WorkflowUtility getWorkflowUtility() {
        return workflowUtility;
    }

    public void setWorkflowUtility(WorkflowUtility workflowUtility) {
        this.workflowUtility = workflowUtility;
    }
}