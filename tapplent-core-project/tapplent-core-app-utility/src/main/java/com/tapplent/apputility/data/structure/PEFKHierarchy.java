package com.tapplent.apputility.data.structure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 06/01/17.
 */
public class PEFKHierarchy {
    private String mtPE;
    private List<PEFKHierarchy> childrenMTPEs = new ArrayList<>();

    public PEFKHierarchy(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public List<PEFKHierarchy> getChildrenMTPEs() {
        return childrenMTPEs;
    }

    public void setChildrenMTPEs(List<PEFKHierarchy> childrenMTPEs) {
        this.childrenMTPEs = childrenMTPEs;
    }
}
