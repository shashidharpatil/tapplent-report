package com.tapplent.apputility.analytics.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tapplent on 23/08/17.
 */
public class AnalyticsAxisOptionsResponse {
    private List<AnalyticsAxisAndMetricMap> axisAndMetricMap = new ArrayList<>();
    private List<TimePeriodOffset> timePeriods = new ArrayList<>();
    private List<CalendarPeriodUnit> calendarPeriodUnits = new ArrayList<>();
    // Here also add the applicable periods

    public List<AnalyticsAxisAndMetricMap> getAxisAndMetricMap() {
        return axisAndMetricMap;
    }

    public void setAxisAndMetricMap(List<AnalyticsAxisAndMetricMap> axisAndMetricMap) {
        this.axisAndMetricMap = axisAndMetricMap;
    }

    public List<TimePeriodOffset> getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(List<TimePeriodOffset> timePeriods) {
        this.timePeriods = timePeriods;
    }

    public List<CalendarPeriodUnit> getCalendarPeriodUnits() {
        return calendarPeriodUnits;
    }

    public void setCalendarPeriodUnits(List<CalendarPeriodUnit> calendarPeriodUnits) {
        this.calendarPeriodUnits = calendarPeriodUnits;
    }
}
