package com.tapplent.apputility.uilayout.structure;

import com.tapplent.platformutility.uilayout.valueobject.ControlEventCriteriaVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 05/10/17.
 */
public class ControlEventCriteria {
    private String criteriaControlInstance;
    private String operator;
    private String filterValue;

    public String getCriteriaControlInstance() {
        return criteriaControlInstance;
    }

    public void setCriteriaControlInstance(String criteriaControlInstance) {
        this.criteriaControlInstance = criteriaControlInstance;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public static ControlEventCriteria fromVO(ControlEventCriteriaVO controlEventCriteriaVO) {
        ControlEventCriteria controlEventCriteria = new ControlEventCriteria();
        BeanUtils.copyProperties(controlEventCriteriaVO, controlEventCriteria);
        return controlEventCriteria;
    }
}
