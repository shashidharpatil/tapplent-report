package com.tapplent.apputility.layout.structure;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.PropertyControlVO;

public class PropertyControl {
	private String propertyControlId;
	@JsonIgnore
	private String propertyControlTransactionFkId;
	private String baseTemplateId;
	private String mtPEId;
	@JsonIgnore
	private String propertyControlInstanceFkId; 
	private String canvasId;
	private String propertyControlCode;
	private String controlTypeCode;
	private String propertyControlAttribute;
	private JsonNode deviceIndependentBlob; 
	private JsonNode deviceDependentBlob;
	@JsonIgnore
	private JsonNode mstDeviceIndependentBlob;
	@JsonIgnore
	private JsonNode txnDeviceIndependentBlob;
	@JsonIgnore
	private JsonNode mstDeviceDependentBlob;
	@JsonIgnore
	private JsonNode txnDeviceDependentBlob;
	private List<ActionStandAlone> propertyControlActionList;
	public PropertyControl(){
		this.propertyControlActionList = new ArrayList<>();
	}
	public String getPropertyControlId() {
		return propertyControlId;
	}
	public void setPropertyControlId(String propertyControlId) {
		this.propertyControlId = propertyControlId;
	}
	public String getPropertyControlTransactionFkId() {
		return propertyControlTransactionFkId;
	}
	public void setPropertyControlTransactionFkId(String propertyControlTransactionFkId) {
		this.propertyControlTransactionFkId = propertyControlTransactionFkId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPEId() {
		return mtPEId;
	}
	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}
	public String getPropertyControlInstanceFkId() {
		return propertyControlInstanceFkId;
	}
	public void setPropertyControlInstanceFkId(String propertyControlInstanceFkId) {
		this.propertyControlInstanceFkId = propertyControlInstanceFkId;
	}
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public List<ActionStandAlone> getPropertyControlActionList() {
		return propertyControlActionList;
	}
	public void setPropertyControlActionList(List<ActionStandAlone> propertyControlActionList) {
		this.propertyControlActionList = propertyControlActionList;
	}
	public String getPropertyControlCode() {
		return propertyControlCode;
	}
	public void setPropertyControlCode(String propertyControlCode) {
		this.propertyControlCode = propertyControlCode;
	}
	public String getControlTypeCode() {
		return controlTypeCode;
	}
	public void setControlTypeCode(String controlTypeCode) {
		this.controlTypeCode = controlTypeCode;
	}
	public JsonNode getMstDeviceIndependentBlob() {
		return mstDeviceIndependentBlob;
	}
	public void setMstDeviceIndependentBlob(JsonNode mstDeviceIndependentBlob) {
		this.mstDeviceIndependentBlob = mstDeviceIndependentBlob;
	}
	public JsonNode getTxnDeviceIndependentBlob() {
		return txnDeviceIndependentBlob;
	}
	public void setTxnDeviceIndependentBlob(JsonNode txnDeviceIndependentBlob) {
		this.txnDeviceIndependentBlob = txnDeviceIndependentBlob;
	}
	public JsonNode getMstDeviceDependentBlob() {
		return mstDeviceDependentBlob;
	}
	public void setMstDeviceDependentBlob(JsonNode mstDeviceDependentBlob) {
		this.mstDeviceDependentBlob = mstDeviceDependentBlob;
	}
	public JsonNode getTxnDeviceDependentBlob() {
		return txnDeviceDependentBlob;
	}
	public void setTxnDeviceDependentBlob(JsonNode txnDeviceDependentBlob) {
		this.txnDeviceDependentBlob = txnDeviceDependentBlob;
	}
	public String getPropertyControlAttribute() {
		return propertyControlAttribute;
	}
	public void setPropertyControlAttribute(String propertyControlAttribute) {
		this.propertyControlAttribute = propertyControlAttribute;
	}
	public static PropertyControl fromVO(PropertyControlVO propertyControlVO){
		PropertyControl propertyControl = new PropertyControl();
		BeanUtils.copyProperties(propertyControlVO, propertyControl);
		return propertyControl;
	}
}
