package com.tapplent.apputility.data.structure;

import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;

/**
 * Created by tapplent on 14/12/16.
 */
public class DoaMeta {
    private String alias;
    private EntityAttributeMetadata meta;
    private int seqNumber;

    public DoaMeta(){

    }
    public DoaMeta(String alias){
        this.alias = alias;
    }
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public EntityAttributeMetadata getMeta() {
        return meta;
    }

    public void setMeta(EntityAttributeMetadata meta) {
        this.meta = meta;
    }

    public int getSeqNumber() {
        return seqNumber;
    }

    public void setSeqNumber(int seqNumber) {
        this.seqNumber = seqNumber;
    }
}
