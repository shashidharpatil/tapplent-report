/**
 * 
 */
package com.tapplent.apputility.layout.structure;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.CanvasMasterVO;

/**
 * @author Shubham Patodi
 *
 */
public class CanvasMaster {
	private String canvasId;
	private String parentCanvasId;
	private String canvasTypeCode;
	private JsonNode deviceIndependentBlob;
	private JsonNode deviceDependentBlob;
	private JsonNode deviceApplicableBlob;
	List<ControlMaster> controlList = new ArrayList<>();
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public String getParentCanvasId() {
		return parentCanvasId;
	}
	public void setParentCanvasId(String parentCanvasId) {
		this.parentCanvasId = parentCanvasId;
	}
	public String getCanvasTypeCode() {
		return canvasTypeCode;
	}
	public void setCanvasTypeCode(String canvasTypeCode) {
		this.canvasTypeCode = canvasTypeCode;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public List<ControlMaster> getControlList() {
		return controlList;
	}
	public void setControlList(List<ControlMaster> controlList) {
		this.controlList = controlList;
	}
	public JsonNode getDeviceApplicableBlob() {
		return deviceApplicableBlob;
	}
	public void setDeviceApplicableBlob(JsonNode deviceApplicableBlob) {
		this.deviceApplicableBlob = deviceApplicableBlob;
	}
	public static CanvasMaster fromVO(CanvasMasterVO canvasMasterVO){
		CanvasMaster canvasMaster = new CanvasMaster();
		BeanUtils.copyProperties(canvasMasterVO, canvasMaster);
		return canvasMaster;
	}
}
