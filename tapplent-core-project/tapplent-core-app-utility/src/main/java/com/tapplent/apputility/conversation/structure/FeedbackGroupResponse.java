package com.tapplent.apputility.conversation.structure;

import java.util.List;
import java.util.Map;

/**
 * Created by Tapplent on 6/9/17.
 */
public class FeedbackGroupResponse {
    List<FeedbackGroup> feedbackGroups;
    PersonDetail loggedinPersonDetails;
    Map<String, Integer> groupToUnreadMessageCount;

    public List<FeedbackGroup> getFeedbackGroups() {
        return feedbackGroups;
    }

    public void setFeedbackGroups(List<FeedbackGroup> feedbackGroups) {
        this.feedbackGroups = feedbackGroups;
    }

    public Map<String, Integer> getGroupToUnreadMessageCount() {
        return groupToUnreadMessageCount;
    }

    public void setGroupToUnreadMessageCount(Map<String, Integer> groupToUnreadMessageCount) {
        this.groupToUnreadMessageCount = groupToUnreadMessageCount;
    }

    public PersonDetail getLoggedinPersonDetails() {
        return loggedinPersonDetails;
    }

    public void setLoggedinPersonDetails(PersonDetail loggedinPersonDetails) {
        this.loggedinPersonDetails = loggedinPersonDetails;
    }
}
