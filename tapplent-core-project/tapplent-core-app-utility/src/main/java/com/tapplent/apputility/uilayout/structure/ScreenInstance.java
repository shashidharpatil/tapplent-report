package com.tapplent.apputility.uilayout.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.apputility.data.structure.SubjectUserPath;
import com.tapplent.platformutility.uilayout.valueobject.FilterCriteria;
import com.tapplent.platformutility.uilayout.valueobject.ScreenInstanceVO;
import org.springframework.beans.BeanUtils;

import java.util.*;

/**
 * Created by tapplent on 25/04/17.
 */
public class ScreenInstance {
    private String screenInstancePkId;
    private int themeSeqNumPosInt;
    private String screenMasterCodeFkId;
    private List<ScreenSectionInstance> sections = new ArrayList<>();
    @JsonIgnore
    private List<List<SectionControl>> listOfListOfControls;
    @JsonIgnore
    private Map<String, SectionControl> controlMap = new HashMap<>();
    @JsonIgnore
    private List<TargetSectionFilterParams> listOfTargetSectionsFilters = new ArrayList<>();
    @JsonIgnore
    private List<ActionArgument> listOfActionArguments = new ArrayList<>();
    @JsonIgnore
    private List<TargetFilterCriteria> listOfFilterCriteria = new ArrayList<>();
    @JsonIgnore
    private List<SectionActionInstance> nonInteractiveLoadActions = new ArrayList<>();
    @JsonIgnore
    private Map<String, SubjectUserPath> mtPEAliasToSubjectUserPathMap = new HashMap<>();
    @JsonIgnore
    private Set<String> endorseMTPEAlias = new HashSet<>();

    public String getScreenInstancePkId() {
        return screenInstancePkId;
    }

    public void setScreenInstancePkId(String screenInstancePkId) {
        this.screenInstancePkId = screenInstancePkId;
    }

    public int getThemeSeqNumPosInt() {
        return themeSeqNumPosInt;
    }

    public void setThemeSeqNumPosInt(int themeSeqNumPosInt) {
        this.themeSeqNumPosInt = themeSeqNumPosInt;
    }

    public String getScreenMasterCodeFkId() {
        return screenMasterCodeFkId;
    }

    public void setScreenMasterCodeFkId(String screenMasterCodeFkId) {
        this.screenMasterCodeFkId = screenMasterCodeFkId;
    }

    public List<ScreenSectionInstance> getSections() {
        return sections;
    }

    public void setSections(List<ScreenSectionInstance> sections) {
        this.sections = sections;
    }

    public List<List<SectionControl>> getListOfListOfControls() {
        return listOfListOfControls;
    }

    public void setListOfListOfControls(List<List<SectionControl>> listOfListOfControls) {
        this.listOfListOfControls = listOfListOfControls;
    }

    public List<TargetSectionFilterParams> getListOfTargetSectionsFilters() {
        return listOfTargetSectionsFilters;
    }

    public void setListOfTargetSectionsFilters(List<TargetSectionFilterParams> listOfTargetSectionsFilters) {
        this.listOfTargetSectionsFilters = listOfTargetSectionsFilters;
    }

    public List<ActionArgument> getListOfActionArguments() {
        return listOfActionArguments;
    }

    public void setListOfActionArguments(List<ActionArgument> listOfActionArguments) {
        this.listOfActionArguments = listOfActionArguments;
    }

    public List<TargetFilterCriteria> getListOfFilterCriteria() {
        return listOfFilterCriteria;
    }

    public void setListOfFilterCriteria(List<TargetFilterCriteria> listOfFilterCriteria) {
        this.listOfFilterCriteria = listOfFilterCriteria;
    }

    public List<SectionActionInstance> getNonInteractiveLoadActions() {
        return nonInteractiveLoadActions;
    }

    public void setNonInteractiveLoadActions(List<SectionActionInstance> nonInteractiveLoadActions) {
        this.nonInteractiveLoadActions = nonInteractiveLoadActions;
    }

    public Map<String, SectionControl> getControlMap() {
        return controlMap;
    }

    public void setControlMap(Map<String, SectionControl> controlMap) {
        this.controlMap = controlMap;
    }

    public Map<String, SubjectUserPath> getMtPEAliasToSubjectUserPathMap() {
        return mtPEAliasToSubjectUserPathMap;
    }

    public void setMtPEAliasToSubjectUserPathMap(Map<String, SubjectUserPath> mtPEAliasToSubjectUserPathMap) {
        this.mtPEAliasToSubjectUserPathMap = mtPEAliasToSubjectUserPathMap;
    }

    public Set<String> getEndorseMTPEAlias() {
        return endorseMTPEAlias;
    }

    public void setEndorseMTPEAlias(Set<String> endorseMTPEAlias) {
        this.endorseMTPEAlias = endorseMTPEAlias;
    }

    public static ScreenInstance fromVO(ScreenInstanceVO screenInstanceVO){
        ScreenInstance screenInstance = new ScreenInstance();
        BeanUtils.copyProperties(screenInstanceVO, screenInstance);
        return screenInstance;
    }
}
