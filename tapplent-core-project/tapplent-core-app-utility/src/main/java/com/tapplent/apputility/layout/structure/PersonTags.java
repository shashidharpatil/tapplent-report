package com.tapplent.apputility.layout.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.layout.valueObject.PersonTagsVO;
import com.tapplent.platformutility.person.structure.Person;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 28/12/16.
 */
public class PersonTags {
    private String personTagsPkId;
    @JsonIgnore
    private String loggedInUserFkId;
    private String tagTxt;
    private String backgroundColourTxt;
    private String fontColourTxt;
    private String lastModifiedDateTime;

    public String getPersonTagsPkId() {
        return personTagsPkId;
    }

    public void setPersonTagsPkId(String personTagsPkId) {
        this.personTagsPkId = personTagsPkId;
    }

    public String getLoggedInUserFkId() {
        return loggedInUserFkId;
    }

    public void setLoggedInUserFkId(String loggedInUserFkId) {
        this.loggedInUserFkId = loggedInUserFkId;
    }

    public String getTagTxt() {
        return tagTxt;
    }

    public void setTagTxt(String tagTxt) {
        this.tagTxt = tagTxt;
    }

    public String getBackgroundColourTxt() {
        return backgroundColourTxt;
    }

    public void setBackgroundColourTxt(String backgroundColourTxt) {
        this.backgroundColourTxt = backgroundColourTxt;
    }

    public String getFontColourTxt() {
        return fontColourTxt;
    }

    public void setFontColourTxt(String fontColourTxt) {
        this.fontColourTxt = fontColourTxt;
    }

    public String getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(String lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }

    public static PersonTags fromVO(PersonTagsVO persontagsVO){
        PersonTags personTags = new PersonTags();
        BeanUtils.copyProperties(persontagsVO, personTags);
        return personTags;
    }
}
