package com.tapplent.apputility.utility.structure;

/**
 * Created by Shubham Patodi on 05/08/16.
 */
public class LogErrorRequest {
    private String errorDescription;
    private String createDateTime;
    private String createAuthor;

    public String getCreateAuthor() {
        return createAuthor;
    }

    public void setCreateAuthor(String createAuthor) {
        this.createAuthor = createAuthor;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(String createDateTime) {
        this.createDateTime = createDateTime;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
