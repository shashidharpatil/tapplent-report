package com.tapplent.apputility.layout.structure;

import java.util.Map;

public class CanvasEU {
	private String canvasEuFkId;
	private String parentCanvasEuFkId;
	private String canvasDefnMasterPkId;
	private String canvasTransactionPkId;
	private String baseTemplateFkId;
	private String mtProcessElementFkId;
	private Map<String, String> deviceIndependentBlob;
	private Map<String, String> deviceDependentBlob;
	private Map<String, String> deviceApplicableBlob;
	
}
