package com.tapplent.apputility.etl.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.etl.valueObject.EtlMapVO;

public class EtlMap {
//	VERSION_ID                           
//	ETL_MAP_PK_ID                        
//	MAP_NAME_G11N_BLOB                   
//	ETL_HEADER_DEP_CODE_FK_ID            
//	MAP_SEQUENCE_NUMBER_POS_INT          
//	TARGET_DO_CODE_FK_ID                 
//	TARGET_BASE_TEMPLATE_CODE_FK_ID      
//	TARGET_DATA_MODE_CODE_FK_ID          
	private String versionId; 
	private String etlMapPkId;
	private String mapName;
	private String etlHeaderDepFkId;
	private int mapSequenceNumberPosInt;
	private String targetDoCodeFkId;
	private String targetBaseTemplateCodeFkId;
	private String targetDataModeCodeFkId;
	
	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getEtlMapPkId() {
		return etlMapPkId;
	}

	public void setEtlMapPkId(String etlMapPkId) {
		this.etlMapPkId = etlMapPkId;
	}

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public String getEtlHeaderDepFkId() {
		return etlHeaderDepFkId;
	}

	public void setEtlHeaderDepFkId(String etlHeaderDepFkId) {
		this.etlHeaderDepFkId = etlHeaderDepFkId;
	}

	public int getMapSequenceNumberPosInt() {
		return mapSequenceNumberPosInt;
	}

	public void setMapSequenceNumberPosInt(int mapSequenceNumberPosInt) {
		this.mapSequenceNumberPosInt = mapSequenceNumberPosInt;
	}

	public String getTargetDoCodeFkId() {
		return targetDoCodeFkId;
	}

	public void setTargetDoCodeFkId(String targetDoCodeFkId) {
		this.targetDoCodeFkId = targetDoCodeFkId;
	}

	public String getTargetBaseTemplateCodeFkId() {
		return targetBaseTemplateCodeFkId;
	}

	public void setTargetBaseTemplateCodeFkId(String targetBaseTemplateCodeFkId) {
		this.targetBaseTemplateCodeFkId = targetBaseTemplateCodeFkId;
	}

	public String getTargetDataModeCodeFkId() {
		return targetDataModeCodeFkId;
	}

	public void setTargetDataModeCodeFkId(String targetDataModeCodeFkId) {
		this.targetDataModeCodeFkId = targetDataModeCodeFkId;
	}

	public static EtlMap fromVO(EtlMapVO etlMapVO){
		EtlMap etlMap = new EtlMap();
		BeanUtils.copyProperties(etlMapVO, etlMap);
		return etlMap;
	}
}
