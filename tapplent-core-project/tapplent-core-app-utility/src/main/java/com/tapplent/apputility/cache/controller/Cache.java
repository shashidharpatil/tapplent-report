package com.tapplent.apputility.cache.controller;

public enum Cache {
    Person("PERSON"),
    System("SYSTEM"),
    Meta("META"),
    All("ALL");

    private String cacheName;

    Cache(String cacheName) {
        this.cacheName = cacheName;
    }

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }
}
