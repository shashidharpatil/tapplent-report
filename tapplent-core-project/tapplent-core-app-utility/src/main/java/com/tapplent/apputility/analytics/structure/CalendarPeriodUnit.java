package com.tapplent.apputility.analytics.structure;

import com.tapplent.platformutility.analytics.structure.CalendarPeriodUnitVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 25/08/17.
 */
public class CalendarPeriodUnit {
    private String periodUnit;
    private String periodUnitName;

    public String getPeriodUnit() {
        return periodUnit;
    }

    public void setPeriodUnit(String periodUnit) {
        this.periodUnit = periodUnit;
    }

    public String getPeriodUnitName() {
        return periodUnitName;
    }

    public void setPeriodUnitName(String periodUnitName) {
        this.periodUnitName = periodUnitName;
    }

    public static CalendarPeriodUnit fromVO(CalendarPeriodUnitVO periodUnitVO) {
        CalendarPeriodUnit calendarPeriodUnit = new CalendarPeriodUnit();
        BeanUtils.copyProperties(periodUnitVO, calendarPeriodUnit);
        return calendarPeriodUnit;
    }
}
