package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.ActionStepVO;

public class ActionStep {
	@JsonIgnore
	private String versionId;
	private String clientActionStepRuleId;
	private String actionCode;
	@JsonIgnore
	private String actionStepName;
	private String actionStepidText;
	private String parentActionStepidText;
	private String actionControlCode;
	private String ruleCode;
	
	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getClientActionStepRuleId() {
		return clientActionStepRuleId;
	}

	public void setClientActionStepRuleId(String clientActionStepRuleId) {
		this.clientActionStepRuleId = clientActionStepRuleId;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getActionStepName() {
		return actionStepName;
	}

	public void setActionStepName(String actionStepName) {
		this.actionStepName = actionStepName;
	}

	public String getActionStepidText() {
		return actionStepidText;
	}

	public void setActionStepidText(String actionStepidText) {
		this.actionStepidText = actionStepidText;
	}

	public String getParentActionStepidText() {
		return parentActionStepidText;
	}

	public void setParentActionStepidText(String parentActionStepidText) {
		this.parentActionStepidText = parentActionStepidText;
	}

	public String getActionControlCode() {
		return actionControlCode;
	}

	public void setActionControlCode(String actionControlCode) {
		this.actionControlCode = actionControlCode;
	}

	public String getRuleCode() {
		return ruleCode;
	}

	public void setRuleCode(String ruleCode) {
		this.ruleCode = ruleCode;
	}

	public static ActionStep fromVO(ActionStepVO actionStepVO){
		ActionStep actionStep = new ActionStep();
		BeanUtils.copyProperties(actionStepVO, actionStep);
		return actionStep;
	}
}
