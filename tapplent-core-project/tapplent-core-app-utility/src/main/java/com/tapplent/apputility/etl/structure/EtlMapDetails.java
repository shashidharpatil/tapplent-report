package com.tapplent.apputility.etl.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.etl.valueObject.EtlMapDetailsVO;

public class EtlMapDetails {
	private String versionId;
	private String etlMapDetailsPkId;
	private String detailsMapName;
	private String etlMapDepFkId;
	private String etlHeaderDepFkId; 
	private String targetDoaCodeFkId; 
	private String mapSourceDoaExpn;
	private String mapLookupDoaExpn;
	private String associatedArtefactLocationDoaCodeFkId;
	private boolean propogateVerion;

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getEtlMapDetailsPkId() {
		return etlMapDetailsPkId;
	}

	public void setEtlMapDetailsPkId(String etlMapDetailsPkId) {
		this.etlMapDetailsPkId = etlMapDetailsPkId;
	}

	public String getDetailsMapName() {
		return detailsMapName;
	}

	public void setDetailsMapName(String detailsMapName) {
		this.detailsMapName = detailsMapName;
	}

	public String getEtlMapDepFkId() {
		return etlMapDepFkId;
	}

	public void setEtlMapDepFkId(String etlMapDepFkId) {
		this.etlMapDepFkId = etlMapDepFkId;
	}

	public String getEtlHeaderDepFkId() {
		return etlHeaderDepFkId;
	}

	public void setEtlHeaderDepFkId(String etlHeaderDepFkId) {
		this.etlHeaderDepFkId = etlHeaderDepFkId;
	}

	public String getTargetDoaCodeFkId() {
		return targetDoaCodeFkId;
	}

	public void setTargetDoaCodeFkId(String targetDoaCodeFkId) {
		this.targetDoaCodeFkId = targetDoaCodeFkId;
	}

	public String getMapSourceDoaExpn() {
		return mapSourceDoaExpn;
	}

	public void setMapSourceDoaExpn(String mapSourceDoaExpn) {
		this.mapSourceDoaExpn = mapSourceDoaExpn;
	}

	public String getMapLookupDoaExpn() {
		return mapLookupDoaExpn;
	}

	public void setMapLookupDoaExpn(String mapLookupDoaExpn) {
		this.mapLookupDoaExpn = mapLookupDoaExpn;
	}

	public String getAssociatedArtefactLocationDoaCodeFkId() {
		return associatedArtefactLocationDoaCodeFkId;
	}

	public void setAssociatedArtefactLocationDoaCodeFkId(String associatedArtefactLocationDoaCodeFkId) {
		this.associatedArtefactLocationDoaCodeFkId = associatedArtefactLocationDoaCodeFkId;
	}

	public boolean isPropogateVerion() {
		return propogateVerion;
	}

	public void setPropogateVerion(boolean propogateVerion) {
		this.propogateVerion = propogateVerion;
	}

	public static EtlMapDetails fromVO(EtlMapDetailsVO etlMapDetailsVO){
		EtlMapDetails etlMapDetails = new EtlMapDetails();
		BeanUtils.copyProperties(etlMapDetailsVO, etlMapDetails);
		return etlMapDetails;
	}
}
