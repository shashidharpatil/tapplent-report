package com.tapplent.apputility.layout.structure;

import java.io.Serializable;
import java.net.URL;
import java.util.List;
import java.util.Map;

import com.tapplent.apputility.data.structure.UnitResponseData;
import com.tapplent.apputility.uilayout.structure.ScreenInstance;
import com.tapplent.platformutility.common.util.AttachmentObject;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.layout.valueObject.StateVO;
import com.tapplent.platformutility.metadata.structure.PropertyControlMaster;
import com.tapplent.platformutility.uilayout.valueobject.ScreenInstanceVO;

public class ResponseSysEssentials implements Serializable{
	private PersonPreference personPreference;
	private List<String> groupIds;
	private List<PersonTags> personTags;
	private List<AppMainMenu> appMainMenuList;
    private List<AppMainMenu> personMainMenuList;
	private List<AppMainMenu> logoutList;
	private List<AppMainMenu> changePasswordList;
    private List<AppMainMenu> getWorkDoneMenu;
	private List<AppMainMenu> socialMenu;
	private String hpConvID;
	private List<Theme> themeList;
	private List<SocialStatus> socialStatusList;
	private Map<String,Map<String,StateVO>> stateIcons;
	private Map<String, JobRelationshipType> jobRelationshipTypes;
	private List<LicencedLocales> licencedLocales;
	private List<AppHomePage> appHomePageList;
    private UnitResponseData appHomePageInstance;
	private UnitResponseData appMainMenuInstance;
	private AttachmentObject iconSvgURL;
	private AttachmentObject iconTxtURL;
	private AttachmentObject iconEotURL;
	private AttachmentObject iconWoffURL;
	private AttachmentObject styleCss;
	private UiSettings uiSettings;
	private Map<String, PropertyControlMaster> propertyControlList;

	

	public UiSettings getUiSettings() {
		return uiSettings;
	}
	public void setUiSettings(UiSettings uiSettings) {
		this.uiSettings = uiSettings;
	}

	public PersonPreference getPersonPreference() {
		return personPreference;
	}

	public void setPersonPreference(PersonPreference personPreference) {
		this.personPreference = personPreference;
	}

    public List<AppMainMenu> getAppMainMenuList() {
        return appMainMenuList;
    }

    public void setAppMainMenuList(List<AppMainMenu> appMainMenuList) {
        this.appMainMenuList = appMainMenuList;
    }

	public UnitResponseData getAppHomePageInstance() {
		return appHomePageInstance;
	}

	public void setAppHomePageInstance(UnitResponseData appHomePageInstance) {
		this.appHomePageInstance = appHomePageInstance;
	}

	public List<AppMainMenu> getPersonMainMenuList() {
        return personMainMenuList;
    }

    public void setPersonMainMenuList(List<AppMainMenu> personMainMenuList) {
        this.personMainMenuList = personMainMenuList;
    }

	public AttachmentObject getIconSvgURL() {
		return iconSvgURL;
	}

	public void setIconSvgURL(AttachmentObject iconSvgURL) {
		this.iconSvgURL = iconSvgURL;
	}

	public AttachmentObject getIconTxtURL() {
		return iconTxtURL;
	}

	public void setIconTxtURL(AttachmentObject iconTxtURL) {
		this.iconTxtURL = iconTxtURL;
	}

	public AttachmentObject getIconEotURL() {
		return iconEotURL;
	}

	public void setIconEotURL(AttachmentObject iconEotURL) {
		this.iconEotURL = iconEotURL;
	}

	public AttachmentObject getIconWoffURL() {
		return iconWoffURL;
	}

	public void setIconWoffURL(AttachmentObject iconWoffURL) {
		this.iconWoffURL = iconWoffURL;
	}

	public AttachmentObject getStyleCss() {
		return styleCss;
	}

	public void setStyleCss(AttachmentObject styleCss) {
		this.styleCss = styleCss;
	}

	public List<PersonTags> getPersonTags() {
		return personTags;
	}

	public void setPersonTags(List<PersonTags> personTags) {
		this.personTags = personTags;
	}

	public List<SocialStatus> getSocialStatusList() {
		return socialStatusList;
	}

	public void setSocialStatusList(List<SocialStatus> socialStatusList) {
		this.socialStatusList = socialStatusList;
	}

	public List<LicencedLocales> getLicencedLocales() {
		return licencedLocales;
	}

	public void setLicencedLocales(List<LicencedLocales> licencedLocales) {
		this.licencedLocales = licencedLocales;
	}

	public Map<String, PropertyControlMaster> getPropertyControlList() {
		return propertyControlList;
	}

	public void setPropertyControlList(Map<String, PropertyControlMaster> propertyControlList) {
		this.propertyControlList = propertyControlList;
	}

	public List<AppHomePage> getAppHomePageList() {
		return appHomePageList;
	}

	public void setAppHomePageList(List<AppHomePage> appHomePageList) {
		this.appHomePageList = appHomePageList;
	}

	public UnitResponseData getAppMainMenuInstance() {
		return appMainMenuInstance;
	}

	public void setAppMainMenuInstance(UnitResponseData appMainMenuInstance) {
		this.appMainMenuInstance = appMainMenuInstance;
	}

	public List<AppMainMenu> getGetWorkDoneMenu() {
		return getWorkDoneMenu;
	}

	public void setGetWorkDoneMenu(List<AppMainMenu> getWorkDoneMenu) {
		this.getWorkDoneMenu = getWorkDoneMenu;
	}

	public List<AppMainMenu> getSocialMenu() {
		return socialMenu;
	}

	public void setSocialMenu(List<AppMainMenu> socialMenu) {
		this.socialMenu = socialMenu;
	}

	public List<Theme> getThemeList() {
		return themeList;
	}

	public void setThemeList(List<Theme> themeList) {
		this.themeList = themeList;
	}

	public Map<String, Map<String, StateVO>> getStateIcons() {
		return stateIcons;
	}

	public void setStateIcons(Map<String, Map<String, StateVO>> stateIcons) {
		this.stateIcons = stateIcons;
	}

	public Map<String, JobRelationshipType> getJobRelationshipTypes() {
		return jobRelationshipTypes;
	}

	public void setJobRelationshipTypes(Map<String, JobRelationshipType> jobRelationshipTypes) {
		this.jobRelationshipTypes = jobRelationshipTypes;
	}

	public List<AppMainMenu> getLogoutList() {
		return logoutList;
	}

	public void setLogoutList(List<AppMainMenu> logoutList) {
		this.logoutList = logoutList;
	}

	public List<AppMainMenu> getChangePasswordList() {
		return changePasswordList;
	}

	public void setChangePasswordList(List<AppMainMenu> changePasswordList) {
		this.changePasswordList = changePasswordList;
	}

	public String getHpConvID() {
		return hpConvID;
	}

	public void setHpConvID(String hpConvID) {
		this.hpConvID = hpConvID;
	}

	public List<String> getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(List<String> groupIds) {
		this.groupIds = groupIds;
	}
}