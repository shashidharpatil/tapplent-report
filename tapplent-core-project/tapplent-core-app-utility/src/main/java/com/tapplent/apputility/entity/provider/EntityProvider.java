package com.tapplent.apputility.entity.provider;

import com.tapplent.apputility.entity.structure.PersonExitRule;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface EntityProvider {
//	SearchResult createEntity(EntityMetadata entityMetadata, DefaultInsertData insertData) throws SQLException;
	void validate(GlobalVariables globalVariables) throws Exception;
	void operateOnUnderlyingRecord(GlobalVariables globalVariables) throws Exception;
//	void indexData(String doName);

//	void validateDefault(GlobalVariables globalVariables) throws Exception;

//	void operateOnUnderlyingRecordDefault(GlobalVariables globalVariables) throws Exception;

	void setRelatedRecordToDuplicate(GlobalVariables globalVariables);

	void setUnderlyingRelatedDataFromDb(GlobalVariables globalVariables) throws Exception;

    void createUser(GlobalVariables globalVariables) throws SQLException;

	String generateEmployeeCode(String employeeCodeGenKey, Map<String, Object> data) throws SQLException;

    void executeEventsForJobAssignment(GlobalVariables globalVariables) throws SQLException;

    void inactivateUser(GlobalVariables globalVariables) throws SQLException, ParseException;

    void updateExistingRelationships(GlobalVariables globalVariables) throws SQLException;

    Map<String,List<PersonExitRule>> getCriteriaToExitRuleMap() throws SQLException;

	Map<String,Map<Integer,List<String>>> getAllRelationsForPerson(String personId);

	List<Map<String, Object>> getAllRecordsByExitRule(PersonExitRule personExitRule, String personId) throws SQLException;

	List<Map<String,Object>> getAllRecordsByRelationshipType(String relationshipType, String personId);

	Map<String,Object> getDirectManagerRecord(String personId);

    Map<String,Object> getUnderlyingPersonRecord(String personId, MasterEntityMetadataVOX masterEntityMetadataVOX);
}
