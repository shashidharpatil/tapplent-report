package com.tapplent.apputility.data.structure;

import java.util.List;
import java.util.Map;

/**
 * Created by Manas on 12/29/16.
 */
public class WriteResponseData {
    private List<UnitWriteResponseData> responseDataList;

    public List<UnitWriteResponseData> getResponseDataList() {
        return responseDataList;
    }

    public void setResponseDataList(List<UnitWriteResponseData> responseDataList) {
        this.responseDataList = responseDataList;
    }
}
