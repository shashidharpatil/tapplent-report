package com.tapplent.apputility.data.structure;

/**
 * Created by tapplent on 15/12/16.
 */
public class AliasTrackerHelper {
    private Integer aliasNumber;
    private String mtPECode;
    private Boolean isInternal;
    public AliasTrackerHelper(Integer aliasNumber, String mtPECode, Boolean isInternal) {
        this.aliasNumber = aliasNumber;
        this.mtPECode = mtPECode;
        this.isInternal = isInternal;
    }

    public Integer getAliasNumber() {
        return aliasNumber;
    }

    public void setAliasNumber(Integer aliasNumber) {
        this.aliasNumber = aliasNumber;
    }

    public String getMtPECode() {
        return mtPECode;
    }

    public void setMtPECode(String mtPECode) {
        this.mtPECode = mtPECode;
    }

    public Boolean getInternal() {
        return isInternal;
    }

    public void setInternal(Boolean internal) {
        isInternal = internal;
    }
}
