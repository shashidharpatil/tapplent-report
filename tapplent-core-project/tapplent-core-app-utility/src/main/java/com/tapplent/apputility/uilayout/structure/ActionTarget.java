package com.tapplent.apputility.uilayout.structure;

import com.tapplent.platformutility.uilayout.valueobject.ControlActionTargetsVO;
import com.tapplent.platformutility.uilayout.valueobject.SectionActionTargetsVO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 25/04/17.
 */
public class ActionTarget {
    private String actionTargetPkId;
    private String targetSections;
    private List<TargetFilterCriteria> filterCriteria = new ArrayList<>();
    private List<TargetSectionFilterParams> targetSectionFilterParams = new ArrayList<>();
    private List<TargetSectionSortParams> targetSectionSortParams = new ArrayList<>();

    public String getTargetSections() {
        return targetSections;
    }

    public void setTargetSections(String targetSections) {
        this.targetSections = targetSections;
    }

    public List<TargetFilterCriteria> getFilterCriteria() {
        return filterCriteria;
    }

    public void setFilterCriteria(List<TargetFilterCriteria> filterCriteria) {
        this.filterCriteria = filterCriteria;
    }

    public List<TargetSectionFilterParams> getTargetSectionFilterParams() {
        return targetSectionFilterParams;
    }

    public void setTargetSectionFilterParams(List<TargetSectionFilterParams> targetSectionFilterParams) {
        this.targetSectionFilterParams = targetSectionFilterParams;
    }

    public List<TargetSectionSortParams> getTargetSectionSortParams() {
        return targetSectionSortParams;
    }

    public void setTargetSectionSortParams(List<TargetSectionSortParams> targetSectionSortParams) {
        this.targetSectionSortParams = targetSectionSortParams;
    }

    public String getActionTargetPkId() {
        return actionTargetPkId;
    }

    public void setActionTargetPkId(String actionTargetPkId) {
        this.actionTargetPkId = actionTargetPkId;
    }

    public static ActionTarget fromVO(ControlActionTargetsVO actionTargetsVO) {
        ActionTarget actionTarget =  new ActionTarget();
        BeanUtils.copyProperties(actionTargetsVO, actionTarget);
        return actionTarget;
    }

    public static ActionTarget fromVO(SectionActionTargetsVO actionTargetsVO) {
        ActionTarget actionTarget =  new ActionTarget();
        BeanUtils.copyProperties(actionTargetsVO, actionTarget);
        return actionTarget;
    }
}
