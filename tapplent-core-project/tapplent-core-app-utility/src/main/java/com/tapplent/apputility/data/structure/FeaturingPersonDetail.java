package com.tapplent.apputility.data.structure;

import java.net.URL;

public class FeaturingPersonDetail {
    private String personId;
    private String personPhoto;
    private URL personPhotoUrl;
    private String personFullName;
    private String nameInitials;
    private String lastModifiedDatetime;
    private String featuredPkId;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(String personPhoto) {
        this.personPhoto = personPhoto;
    }

    public URL getPersonPhotoUrl() {
        return personPhotoUrl;
    }

    public void setPersonPhotoUrl(URL personPhotoUrl) {
        this.personPhotoUrl = personPhotoUrl;
    }

    public String getPersonFullName() {
        return personFullName;
    }

    public void setPersonFullName(String personFullName) {
        this.personFullName = personFullName;
    }

    public String getNameInitials() {
        return nameInitials;
    }

    public void setNameInitials(String nameInitials) {
        this.nameInitials = nameInitials;
    }

    public String getLastModifiedDatetime() {
        return lastModifiedDatetime;
    }

    public void setLastModifiedDatetime(String lastModifiedDatetime) {
        this.lastModifiedDatetime = lastModifiedDatetime;
    }

    public String getFeaturedPkId() {
        return featuredPkId;
    }

    public void setFeaturedPkId(String featuredPkId) {
        this.featuredPkId = featuredPkId;
    }
}
