package com.tapplent.apputility.analytics.service;

import com.google.api.client.util.ArrayMap;
import com.tapplent.apputility.analytics.structure.*;
import com.tapplent.apputility.data.service.DataRequestService;
import com.tapplent.apputility.data.structure.*;
import com.tapplent.apputility.uilayout.service.UILayoutService;
import com.tapplent.apputility.uilayout.structure.SectionControl;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.analytics.dao.AnalyticsDAO;
import com.tapplent.platformutility.analytics.structure.*;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.PersonDetailsObjectRepository;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.MasterEntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;
import com.tapplent.platformutility.person.structure.PersonGroup;
import com.tapplent.platformutility.uilayout.valueobject.LoadParameters;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import com.tapplent.tenantresolver.user.UserContext;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by tapplent on 05/12/16.
 */
public class AnalyticsServiceImpl implements AnalyticsService{
    private static final Logger LOG = LogFactory.getLogger(AnalyticsServiceImpl.class);
    private AnalyticsDAO analyticsDAO;
    private DataRequestService dataRequestService;
    private UILayoutService uiLayoutService;
    @Transactional
    public AnalyticsMetric getAnalyticMetric(String analyticsMetricId, String analysisTypeCode) {
        AnalyticsMetric analyticsMetric = new AnalyticsMetric();
        Map<String, AnalyticsMetricDetails> analyticsMetricDetailsMap = analyticsDAO.getAnalyticMetricDetails(analyticsMetricId, analysisTypeCode);
        Map<String, AnalyticsMetricDetailsQuery> analyticsMetricDetailsQueryMap = analyticsDAO.getAnalyticMetricDetailsQuery(analyticsMetricId);
        List<AnalyticsMetricDetailsFilter> analyticsMetricDetailsFilterList = analyticsDAO.getAnalyticMetricDetailsFilter(analyticsMetricId);
        /*
        Add the children to the corresponding parents.
         */
        for(Map.Entry<String, AnalyticsMetricDetails> detailsEntry : analyticsMetricDetailsMap.entrySet()){
            AnalyticsMetricDetails metricDetails = detailsEntry.getValue();
            analyticsMetric.getAnalyticsMetricDetailsList().add(metricDetails);
        }
        for(Map.Entry<String, AnalyticsMetricDetailsQuery> detailsQueryEntry : analyticsMetricDetailsQueryMap.entrySet()){
            AnalyticsMetricDetailsQuery analyticsMetricDetailsQuery = detailsQueryEntry.getValue();
            String analyticsMetricDetailsFkId = analyticsMetricDetailsQuery.getAnalyticsMetricDetailFkId();
            if(analyticsMetricDetailsMap.containsKey(analyticsMetricDetailsFkId)) {
                analyticsMetricDetailsMap.get(analyticsMetricDetailsFkId).getAnalyticsMetricDetailsQueryList().add(analyticsMetricDetailsQuery);
            }
        }
        for(AnalyticsMetricDetailsFilter analyticsMetricDetailsFilter : analyticsMetricDetailsFilterList){
            String analyticsMetricDetailsFkId = analyticsMetricDetailsFilter.getAnalyticsMetricDetailFkId();
            if(analyticsMetricDetailsMap.containsKey(analyticsMetricDetailsFkId)) {
                analyticsMetricDetailsMap.get(analyticsMetricDetailsFkId).getAnalyticsMetricDetailsFilterList().add(analyticsMetricDetailsFilter);
            }
        }
        return analyticsMetric;
    }

    @Override
    @Transactional
    public AnalyticsMetricResponse getAnalyticsMetricsData(String metricId, String targetScreen, String targetSection, String deviceType) {
        AnalyticsMetricResponse response = new AnalyticsMetricResponse();
        AnalyticsMetric analyticsMetric = getAnalyticMetric(metricId, "TREND");
        /* Each analytics Metric data has metric details
        *  Metric details is at the same level as that of unit request data.
        */
        if(StringUtil.isDefined(targetScreen)) {
            List<String> targetScreenInstances = new ArrayList<>();
            targetScreenInstances.add(targetSection);
            UserContext user = TenantContextHolder.getUserContext();
            PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
            PersonGroup personGroup = personService.getPersonGroups(user.getPersonId());
//            ScreenInstanceVO screenInstance = uiLayoutService.getLayout(targetScreen, targetScreenInstances, deviceType, personGroup);
//            response.setScreenInstance(screenInstance);
        }
        List<AnalyticsMetricDetails> analyticsMetricDetailsList = analyticsMetric.getAnalyticsMetricDetailsList();
        for(AnalyticsMetricDetails metricDetails : analyticsMetricDetailsList){
            AnalyticsMetricDetailsResponse detailsResponse = getAnalyticMetricDetailsData(metricDetails, null, null, false);
            response.getDetailsResponseList().add(detailsResponse);
        }
        return response;
    }

    @Override
    @Transactional
    public AnalyticsMetricResponse getAnalyticsMetricsAnalysisData(String parentAnalysisMetricDetailId, String xAxisParam, String subXAxisParam) {
        AnalyticsMetricResponse response = new AnalyticsMetricResponse();
        List<String> analysisMetricDetailsList = analyticsDAO.getAnalysisMetricDetailsPks(parentAnalysisMetricDetailId);
        for(String metricDetailsId : analysisMetricDetailsList){
            response.getDetailsResponseList().add(getAnalyticMetricDetailsData(metricDetailsId, xAxisParam, subXAxisParam));
        }
//
//        AnalyticsMetric analyticsMetric = getAnalyticMetric(parentAnalysisMetricDetailId, "ANALYSIS");
//        List<AnalyticsMetricDetails> analyticsMetricDetailsList = analyticsMetric.getAnalyticsMetricDetailsList();
//        for(AnalyticsMetricDetails metricDetails : analyticsMetricDetailsList){
//            AnalyticsMetricDetailsResponse detailsResponse = getDrillDownAnalyticMetricDetailsData(metricDetails, xAxisParam, subXAxisParam);
//            response.getDetailsResponseList().add(detailsResponse);
//        }
        return response;
    }

    @Override
    @Transactional
    public RelationResponse getAnalyticsMetricsTableData(String parentAnalysisMetricDetailId) {
        //TODO
//        AnalyticsMetric analyticsMetric = getAnalyticMetric(parentAnalysisMetricDetailId, "TABLE");
//        List<AnalyticsMetricDetails> analyticsMetricDetailsList = analyticsMetric.getAnalyticsMetricDetailsList();
//        for(AnalyticsMetricDetails metricDetails : analyticsMetricDetailsList){
//            AnalyticsMetricDetailsResponse detailsResponse = getAnalyticMetricDetailsData(metricDetails, null, null);
//        }
        AnalyticsMetricDetails metricDetails =  analyticsDAO.getAnalyticsMetricDetails(parentAnalysisMetricDetailId, false, true);
        AnalyticsMetricDetailsResponse detailsResponse = getAnalyticMetricDetailsData(metricDetails, null, null, true);
        UnitResponseData tableData = detailsResponse.getTableData();
        Map<String, DoaMeta> labelToAliasMap = tableData.getLabelToAliasMap();
        /* tableData also has labelToAliasMap */
        List<AnalyticsMetricDetailsQuery> detailsQueryList = metricDetails.getAnalyticsMetricDetailsQueryList();
        for(AnalyticsMetricDetailsQuery metricDetailsQuery : detailsQueryList){
            ProcessElementData peData = tableData.getPEData().get(metricDetailsQuery.getMtPeCodeFkId());
            /*
            Now we need to loop through each of the dataset
             */
        }
        RelationResponse relationResponse = new RelationResponse();
        Map<String, DoaMeta> doaALiasMap = detailsResponse.getTableData().getLabelToAliasMap();
        ProcessElementData tablePEData = null;
        String mtPE = null;
        List<AnalyticsMetricDetailsQuery> detailsQueriesList = metricDetails.getAnalyticsMetricDetailsQueryList();
        for(Map.Entry<String, ProcessElementData> peData : detailsResponse.getTableData().getPEData().entrySet()){
            tablePEData = peData.getValue();
            mtPE = tablePEData.getMtPE();
        }
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
        UserContext user = TenantContextHolder.getUserContext();
        String baseTemplate = personService.getDefaultBTForGroup(mtPE, user.getPersonId());
        MasterEntityMetadataVOX mtMeta = metadataService.getMetadataByMtPE(mtPE);
        MasterEntityAttributeMetadata pkMeta = mtMeta.getPrimaryKeyAttribute();
        List<Record> records = tablePEData.getRecordSetInternal();
        /* for each record prepare RelationInternalResponse */
        for(Record record : records){
            RelationResponseInternal relationResponseInternal = new RelationResponseInternal();
            Map<String, AttributeData> data = new HashMap<>();
            relationResponseInternal.setData(data);
            /* get PK record */
            DoaMeta pkAliasMeta =  doaALiasMap.get(pkMeta.getDoAttributeCode());
            AttributeData pkData = record.getAttributeMap().get(pkAliasMeta.getAlias());
            relationResponseInternal.setPk(pkData);
            for(AnalyticsMetricDetailsQuery query : detailsQueriesList){
                String attributePath = query.getAttrPathExpn();
                DoaMeta doaAliasMeta =  doaALiasMap.get(attributePath);
                /* TODO Inside doa meta also add the metadata of the attribute */
                List<String> doaList = Util.getAttributePathFromExpression(attributePath);
                if(doaList.size()==1){
                    String doa = Util.getBaseDOAFromAtrributePath(doaList.get(0));
                    EntityAttributeMetadata meta = metadataService.getMetadataByBtDoa(baseTemplate, doa);
                    doaAliasMeta.setMeta(meta);
                    doaAliasMeta.setSeqNumber(query.getAttrDisplaySeqNumPosInt());
                }
                relationResponse.getLabelToAliasMap().put(attributePath, doaAliasMeta);
                AttributeData attributeData = record.getAttributeMap().get(doaAliasMeta.getAlias());
                data.put(doaAliasMeta.getAlias(), attributeData);
            }
            relationResponse.getRecordSet().add(relationResponseInternal);
        }
        return relationResponse;
    }

    @Override
    @Transactional
    public AnalyticsAxisOptionsResponse getAxisData(String metricCategoryId, String metricCatMenuType, String axisAndMetricMapPKID) {
        AnalyticsAxisOptionsResponse analyticsAxisOptionsResponse = new AnalyticsAxisOptionsResponse();
        // Adding Axis Data
        List<AnalyticsAxisAndMetricMapVO> axisAndMetricMapList = analyticsDAO.getAxisAndMetricMapList(metricCategoryId, metricCatMenuType, axisAndMetricMapPKID);
        for(AnalyticsAxisAndMetricMapVO analyticsAxisAndMetricMapVO : axisAndMetricMapList){
            AnalyticsAxisAndMetricMap analyticsAxisAndMetricMap = AnalyticsAxisAndMetricMap.fromVO(analyticsAxisAndMetricMapVO);
            analyticsAxisOptionsResponse.getAxisAndMetricMap().add(analyticsAxisAndMetricMap);
        }
        // Adding Time period offset data
        if(!StringUtil.isDefined(axisAndMetricMapPKID)) {
            List<TimePeriodOffsetVO> timePeriodOffsets = analyticsDAO.getTimeperiodOffsetsForLoggedInUser();
            for (TimePeriodOffsetVO timePeriodOffsetVO : timePeriodOffsets) {
                TimePeriodOffset timePeriodOffset = TimePeriodOffset.fromVO(timePeriodOffsetVO);
                timePeriodOffset.setCalendarPeriodName(Util.getG11nValue(timePeriodOffsetVO.getCalendarPeriodName(), null));
                timePeriodOffset.setCalendarPeriodIcon(Util.getIcon(timePeriodOffsetVO.getCalendarPeriodIcon()));
                analyticsAxisOptionsResponse.getTimePeriods().add(timePeriodOffset);
            }
            List<CalendarPeriodUnitVO> periodUnitsVO = analyticsDAO.getCalendarPeriodUnits();
            for(CalendarPeriodUnitVO periodUnitVO : periodUnitsVO){
                CalendarPeriodUnit calendarPeriodUnit = CalendarPeriodUnit.fromVO(periodUnitVO);
                calendarPeriodUnit.setPeriodUnitName(Util.getG11nValue(periodUnitVO.getPeriodUnitName(), null));
                analyticsAxisOptionsResponse.getCalendarPeriodUnits().add(calendarPeriodUnit);
            }
        }
        // Adding Time unit
        return analyticsAxisOptionsResponse;
    }

    @Override
    @Transactional
    public AnalyticsFiltersAndGraphTypeOptionsResponse getGraphTypeAndFilters(String metricAxisMapID) {
        AnalyticsFiltersAndGraphTypeOptionsResponse analyticsFiltersAndGraphTypeOptionsResponse = new AnalyticsFiltersAndGraphTypeOptionsResponse();
        List<GraphTypeVO> applicableGraphTypes = analyticsDAO.getApplicableGraphTypes(metricAxisMapID);
        for(GraphTypeVO graphTypeVO : applicableGraphTypes){
            GraphType graphType = GraphType.fromVO(graphTypeVO);
            analyticsFiltersAndGraphTypeOptionsResponse.getGraphTypes().add(graphType);
        }
        return analyticsFiltersAndGraphTypeOptionsResponse;
    }

    @Override
    @Transactional
    public GraphResponse getGraph(GraphRequest graphRequest) {
        // Now what do we have to do?
        // On the basis of Metric metricAxisMapID get the chart
        GraphResponse graphResponse = new GraphResponse();
        Map<ChartAttributeAxisCode, AnalyticsMetricRequest> analyticsMetricRequestMap = new HashMap<>();
        AnalyticsAxisAndMetricMapVO analyticsAxisAndMetricMapVO = analyticsDAO.getAxisAndMetricMap(graphRequest.getMetricAxisMapID());
        String xAxisID = analyticsAxisAndMetricMapVO.getParentAnalyticsMetricAxisID();
        String subXAxisID = analyticsAxisAndMetricMapVO.getMetricAxisID();
        if(!StringUtil.isDefined(analyticsAxisAndMetricMapVO.getParentAnalyticsMetricAxisID())){
            xAxisID = subXAxisID;
            subXAxisID = null;
        }
        // Now try to build the logic for getting proper TIME Axis
        // If the graph type is End of period
        LOG.debug("Graph Request :" +graphRequest.isEOP());
        Map<String, Date> labelToDateMap = null;
        if(graphRequest.isEOP()){
            /*
                Get all the end dates that lie in that particular start and end dates
             */
            labelToDateMap = getAllTheApplicableDates(graphRequest);
            for(Map.Entry<String, Date> entry : labelToDateMap.entrySet()){
                LOG.debug(entry.getKey());
            }
        }
        // Now we have got x and sub-x axises ID. From this get the actual IDs
        // Build AnalyticsMetricRequest for Axises
        MetricAxisVO xAxisVO = analyticsDAO.getAxisDetails(xAxisID);
        MetricAxis xAxis = MetricAxis.fromVO(xAxisVO);
        AnalyticsMetricRequest xAxisMetricRequest = buildAnalyticsMetricRequestForXAxises(xAxis);
        xAxisMetricRequest.setxAxis(xAxis);
        analyticsMetricRequestMap.put(ChartAttributeAxisCode.X_AXIS, xAxisMetricRequest);

        if(StringUtil.isDefined(subXAxisID)){
            MetricAxisVO subXAxisVO = analyticsDAO.getAxisDetails(subXAxisID);
            MetricAxis subXAxis = MetricAxis.fromVO(subXAxisVO);
            AnalyticsMetricRequest subXAxisMetricRequest = buildAnalyticsMetricRequestForXAxises(subXAxis);
            subXAxisMetricRequest.setxAxis(subXAxis);
            analyticsMetricRequestMap.put(ChartAttributeAxisCode.SUB_X_AXIS, subXAxisMetricRequest);
        }
        AnalyticsMetricChartVO analyticsMetricChartVO = analyticsDAO.getAnalyticsMetricChart(graphRequest.getMetricAxisMapID());
        AnalyticsMetricChart analyticsMetricChart = AnalyticsMetricChart.fromVO(analyticsMetricChartVO);
        BeanUtils.copyProperties(analyticsMetricChart, graphResponse);
        List<AnalyticsMetricChartQueryVO> analyticsMetricChartQueryVOS = analyticsDAO.getAnalyticsMetricChartFilters(analyticsMetricChart.getChartPkID());
        for(AnalyticsMetricChartQueryVO analyticsMetricChartQueryVO : analyticsMetricChartQueryVOS){
            AnalyticsMetricChartQuery analyticsMetricChartQuery = AnalyticsMetricChartQuery.fromVO(analyticsMetricChartQueryVO);
            analyticsMetricChart.getAnalyticsMetricChartQueries().add(analyticsMetricChartQuery);
        }
        List<AnalyticsMetricChartQuery> queries = analyticsMetricChart.getAnalyticsMetricChartQueries();
        if(queries!=null){
            for(AnalyticsMetricChartQuery detailsQuery : queries){
                AnalyticsMetricRequest analyticsMetricRequest = null;
                String attrAxizCode = detailsQuery.getAttrDispAxizCodeFkId();
                ChartAttributeAxisCode attributeAxisCodeEnum = Enum.valueOf(ChartAttributeAxisCode.class, attrAxizCode);
                if(analyticsMetricRequestMap.containsKey(attributeAxisCodeEnum)){
                    analyticsMetricRequest = analyticsMetricRequestMap.get(attributeAxisCodeEnum);
                }else{
                    analyticsMetricRequest = new AnalyticsMetricRequest();
                    analyticsMetricRequestMap.put(attributeAxisCodeEnum, analyticsMetricRequest);
                }
                    /* Each details query should be converted to section control */
                SectionControl control = new SectionControl();
                control.setAttributePathExpn(detailsQuery.getControlAttributePath());
                control.setMtPE(detailsQuery.getMtPE());
                control.setMtPEAlias(detailsQuery.getMtPEAlias());
                control.setParentMTPEAlias(detailsQuery.getParentMTPEAlias());
                control.setOrderByValues(detailsQuery.getOrderByValues());
                if(detailsQuery.isAggregateFunction())
                    control.setAggregateControl(true);
                control.setGroupByControl(detailsQuery.isGroupByAttribute());
                control.setGroupByControlSequence(detailsQuery.getGroupBySeqNumber());
                control.setGroupByHavingFilterExpression(detailsQuery.getGroupByHavingExpn());
                control.setOrderByValues(detailsQuery.getOrderByValues());
                control.setControlType("INVISIBLE");
                analyticsMetricRequest.getMetricQueryList().add(control);
                analyticsMetricRequest.getMetricQuerys().add(detailsQuery);
            }
        }
        /*
        Now here we can get the correct control for the TIME_SERIES_AXIS
         */
        AnalyticsMetricRequest timeAxisControl = analyticsMetricRequestMap.get(ChartAttributeAxisCode.TIME_X_AXIS);
        AnalyticsMetricRequest yAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.Y_AXIS);
        modifyYAxisAccordingToTimeSeries(yAxisRequest, timeAxisControl, labelToDateMap, graphRequest);
        // Now we have to modify the y Axis Request so that proper group by and where clause.

        if(yAxisRequest!=null){
            yAxisRequest.setPeToLoadParamsMap(yAxisRequest.getPeToLoadParamsMap());
            UnitResponseData yAxisResult = dataRequestService.getAnalyticsMetricDetailsData(yAxisRequest);
            graphResponse.setyAxisData(yAxisResult);
        }
        AnalyticsMetricRequest minYAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.MIN_Y_AXIS);
        if(minYAxisRequest!=null){
            minYAxisRequest.setPeToLoadParamsMap(minYAxisRequest.getPeToLoadParamsMap());
            UnitResponseData minYAxisResult = dataRequestService.getAnalyticsMetricDetailsData(minYAxisRequest);
            graphResponse.setMinYAxisData(minYAxisResult);
        }
        AnalyticsMetricRequest avgYAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.AVG_Y_AXIS);
        if(avgYAxisRequest!=null){
            avgYAxisRequest.setPeToLoadParamsMap(avgYAxisRequest.getPeToLoadParamsMap());
            UnitResponseData avgYAxisResult = dataRequestService.getAnalyticsMetricDetailsData(avgYAxisRequest);
            graphResponse.setAvgYAxisData(avgYAxisResult);
        }
        AnalyticsMetricRequest maxYAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.MAX_Y_AXIS);
        if(maxYAxisRequest!=null){
            maxYAxisRequest.setPeToLoadParamsMap(maxYAxisRequest.getPeToLoadParamsMap());
            UnitResponseData maxYAxisResult = dataRequestService.getAnalyticsMetricDetailsData(maxYAxisRequest);
            graphResponse.setMaxYAxisData(maxYAxisResult);
        }
        if(false) {
            AnalyticsMetricRequest tableRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.TABLE_AXIS);
            if (tableRequest != null) {
                tableRequest.setPeToLoadParamsMap(tableRequest.getPeToLoadParamsMap());
                UnitResponseData tableResult = dataRequestService.getAnalyticsMetricDetailsData(tableRequest);
                graphResponse.setTableData(tableResult);
            }
        }
        AnalyticsMetricRequest xAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.X_AXIS);
        if(xAxisRequest!=null){
            xAxisRequest.setPeToLoadParamsMap(xAxisRequest.getPeToLoadParamsMap());
            UnitResponseData xAxisResponse = dataRequestService.getAnalyticsMetricDetailsData(xAxisRequest);
            graphResponse.setxAxisData(getXAxisResult(xAxisResponse, xAxisRequest.getMetricQueryDetails(), xAxisRequest.getxAxis()));
        }

        AnalyticsMetricRequest subXAxis = analyticsMetricRequestMap.get(ChartAttributeAxisCode.SUB_X_AXIS);
        if(subXAxis!=null){
            subXAxis.setPeToLoadParamsMap(subXAxis.getPeToLoadParamsMap());
            UnitResponseData subXAxisResponse = dataRequestService.getAnalyticsMetricDetailsData(subXAxis);
            graphResponse.setSubXAxisData(getXAxisResult(subXAxisResponse, subXAxis.getMetricQueryDetails(), subXAxis.getxAxis()));
        }
        // Now we have got queries
        /* Modify data according to the client requirement */
        if(graphResponse.getxAxisData()!=null) {
            for (XAxisData xAxisSeries : graphResponse.getxAxisData()) {
                String mapperValue = xAxisSeries.getMappedData();
                if (graphResponse.getSubXAxisData() != null && !graphResponse.getSubXAxisData().isEmpty()) {
                    for (XAxisData subXAxisSeriesData : graphResponse.getSubXAxisData()) {
                        String subXAxisMapperValue = subXAxisSeriesData.getMappedData();
                        buildClientResponse(xAxisSeries, subXAxisSeriesData, graphResponse);
                    }
                } else {
                    buildClientResponse(xAxisSeries, null, graphResponse);
                }
            }
        }
        return graphResponse;
    }

    private void modifyYAxisAccordingToTimeSeries(AnalyticsMetricRequest yAxisRequest, AnalyticsMetricRequest timeAxisControl, Map<String, Date> labelToDateMap, GraphRequest graphRequest) {
        // Make time series as the
        // get all the
    }

    private Map<String, Date> getAllTheApplicableDates(GraphRequest graphRequest) {
        Map<String, Date> labelToActualEOPDates = new LinkedHashMap<>();
        List<TimePeriodOffsetVO> timePeriodOffsets = analyticsDAO.getTimeperiodOffsetsForLoggedInUser();
        TimePeriodOffsetVO applicableTimePeriod = null;
        for(TimePeriodOffsetVO  timePeriodOffsetVO: timePeriodOffsets){
            if(timePeriodOffsetVO.getCalendarPeriodType().equals(graphRequest.getCalendarPeriodType()))
                applicableTimePeriod = timePeriodOffsetVO;
        }
        // Now that we have got time period offsets we have to do something.
        CalendarPeriodUnitEnum calendarPeriodUnitEnum = CalendarPeriodUnitEnum.valueOf(graphRequest.getCalendarPeriodUnit());
        switch (calendarPeriodUnitEnum){
            case DAY:
                // get list of all the days
                Calendar calendar = new GregorianCalendar();
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodStartDate()));
                while (calendar.getTime().before(Util.getDateFromString(graphRequest.getPeriodEndDate())))
                {
                    Date result = calendar.getTime();
                    labelToActualEOPDates.put(result.toString(), result);
                    calendar.add(Calendar.DATE, 1);
                }
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodEndDate()));
                Date result = calendar.getTime();
                labelToActualEOPDates.put(result.toString(), result);
                return labelToActualEOPDates;

            case WEEK:
                int weekOffset = applicableTimePeriod.getWeekDaysOffset();
                calendar = new GregorianCalendar();
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodStartDate()));
                while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY){
                    calendar.add(Calendar.DATE, 1);
//                    LOG.debug(calendar.get(Calendar.DAY_OF_WEEK));
//                calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY)
                };
                while (calendar.getTime().before(Util.getDateFromString(graphRequest.getPeriodEndDate())) ||
                        calendar.getTime().equals(Util.getDateFromString(graphRequest.getPeriodEndDate())))
                {
                    result = calendar.getTime();
                    LOG.debug(result);
                    labelToActualEOPDates.put(result.toString(), result);
                    calendar.add(Calendar.DATE, 7);
                }
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodEndDate()));
                result = calendar.getTime();
                if (!labelToActualEOPDates.containsKey(result.toString()))
                    labelToActualEOPDates.put(result.toString(), result);
                return labelToActualEOPDates;

            case MONTH:
                calendar = new GregorianCalendar();
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodStartDate()));
                while (calendar.get(Calendar.DAY_OF_MONTH) != calendar.getActualMaximum(Calendar.DATE)){
                    calendar.add(Calendar.DATE, 1);
                }
                if (!calendar.getTime().before(Util.getDateFromString(graphRequest.getPeriodEndDate())))
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                while (calendar.getTime().before(Util.getDateFromString(graphRequest.getPeriodEndDate())))
                {
                    result = calendar.getTime();
                    LOG.debug(result);
                    labelToActualEOPDates.put(result.toString(), result);
                    calendar.add(Calendar.DATE, 1);
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                }
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodEndDate()));
                result = calendar.getTime();
                if (!labelToActualEOPDates.containsKey(result.toString()))
                    labelToActualEOPDates.put(result.toString(), result);
                return labelToActualEOPDates;

            case QUARTER:
                calendar = new GregorianCalendar();
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodStartDate()));
                if (applicableTimePeriod.getMonthOffset()%4 == 0){
                    while ((calendar.get(Calendar.MONTH) != 3) && (calendar.get(Calendar.MONTH) != 6) &&
                            (calendar.get(Calendar.MONTH) != 9) && (calendar.get(Calendar.MONTH) != 12)){
                        calendar.add(Calendar.DATE, 1);
                    }
                }
                else if (applicableTimePeriod.getMonthOffset()%3 == 0){
                    while ((calendar.get(Calendar.MONTH) != 2) && (calendar.get(Calendar.MONTH) != 5) &&
                            (calendar.get(Calendar.MONTH) != 8) && (calendar.get(Calendar.MONTH) != 9)){
                        LOG.debug(calendar.get(Calendar.DATE)+ " "+ calendar.get(Calendar.MONTH));
                        calendar.add(Calendar.MONTH, 1);
                    }
                }
                else {
                    while ((calendar.get(Calendar.MONTH) != 1) && (calendar.get(Calendar.APRIL) != 6) &&
                            (calendar.get(Calendar.MONTH) != 7) && (calendar.get(Calendar.MONTH) != 10)){
                        calendar.add(Calendar.DATE, 1);
                    }
                }

                while (calendar.getTime().before(Util.getDateFromString(graphRequest.getPeriodEndDate())) ||
                        calendar.getTime().equals(Util.getDateFromString(graphRequest.getPeriodEndDate())))
                {
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    result = calendar.getTime();
                    LOG.debug(result);
                    labelToActualEOPDates.put(result.toString(), result);
                    calendar.add(Calendar.MONTH, 3);
                }
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodEndDate()));
                result = calendar.getTime();
                if (!labelToActualEOPDates.containsKey(result.toString()))
                    labelToActualEOPDates.put(result.toString(), result);
                return labelToActualEOPDates;

            case HALF_YEAR:
                calendar = new GregorianCalendar();
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodStartDate()));
                if (applicableTimePeriod.getMonthOffset()%6 == 0){
                    while ((calendar.get(Calendar.MONTH) != 6) && (calendar.get(Calendar.MONTH) != 12) ){
                        calendar.add(Calendar.MONTH, 1);
                    }
                }
                else if (applicableTimePeriod.getMonthOffset()%5 == 0){
                    while ((calendar.get(Calendar.MONTH) != 1) && (calendar.get(Calendar.MONTH) != 11)){
                        calendar.add(Calendar.MONTH, 1);
                    }
                }
                else if (applicableTimePeriod.getMonthOffset()%4 == 0){
                    while ((calendar.get(Calendar.MONTH) != 2) && (calendar.get(Calendar.MONTH) != 10)) {
                        calendar.add(Calendar.MONTH, 1);
                    }
                }
                else if (applicableTimePeriod.getMonthOffset()%3 == 0){
                    while ((calendar.get(Calendar.MONTH) != 3) && (calendar.get(Calendar.MONTH) != 9)){
                        calendar.add(Calendar.MONTH, 1);
                    }
                }
                else if (applicableTimePeriod.getMonthOffset()%2 == 0){
                    while ((calendar.get(Calendar.MONTH) != 4) && (calendar.get(Calendar.MONTH) != 8)){
                        calendar.add(Calendar.MONTH, 1);
                    }
                }
                else {
                    while ((calendar.get(Calendar.MONTH) != 5) && (calendar.get(Calendar.APRIL) != 7)){
                        calendar.add(Calendar.DATE, 1);
                    }
                }

                while (calendar.getTime().before(Util.getDateFromString(graphRequest.getPeriodEndDate())) ||
                        calendar.getTime().equals(Util.getDateFromString(graphRequest.getPeriodEndDate())))
                {
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    result = calendar.getTime();
                    LOG.debug(result);
                    labelToActualEOPDates.put(result.toString(), result);
                    calendar.add(Calendar.MONTH, 6);
                }
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodEndDate()));
                result = calendar.getTime();
                if (!labelToActualEOPDates.containsKey(result.toString()))
                    labelToActualEOPDates.put(result.toString(), result);
                return labelToActualEOPDates;

            case YEAR:
                calendar = new GregorianCalendar();
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodStartDate()));
                while (calendar.get(Calendar.MONTH) != applicableTimePeriod.getMonthOffset()){
                    calendar.add(Calendar.MONTH, 1);
                }
                while (calendar.getTime().before(Util.getDateFromString(graphRequest.getPeriodEndDate())) ||
                        calendar.getTime().equals(Util.getDateFromString(graphRequest.getPeriodEndDate())))
                {
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    result = calendar.getTime();
                    LOG.debug(result);
                    labelToActualEOPDates.put(result.toString(), result);
                    calendar.add(Calendar.MONTH, 12);
                }
                calendar.setTime(Util.getDateFromString(graphRequest.getPeriodEndDate()));
                result = calendar.getTime();
                if (!labelToActualEOPDates.containsKey(result.toString()))
                    labelToActualEOPDates.put(result.toString(), result);
                return labelToActualEOPDates;
            default:
                return null;
        }
    }

    private AnalyticsMetricRequest buildAnalyticsMetricRequestForXAxises(MetricAxis xAxis) {
        AnalyticsMetricRequest analyticsMetricRequest = new AnalyticsMetricRequest();
        // Will the
            /* Each details query should be converted to section control */
        SectionControl control = new SectionControl();
        control.setAttributePathExpn(xAxis.getControlAttributePath());
        control.setMtPE(xAxis.getMtPE());
        control.setMtPEAlias(xAxis.getMtPEAlias());
        control.setParentMTPEAlias(xAxis.getParentMTPEAlias());
//        control.setOrderByValues(xAxis.getOrderByValues());
//        if(xAxis.isAggregateFunction())
//            control.setAggregateControl(true);
//        control.setGroupByControl(xAxis.isGroupByAttribute());
//        control.setGroupByControlSequence(detailsQuery.getGroupBySeqNumber());
//        control.setGroupByHavingFilterExpression(detailsQuery.getGroupByHavingExpn());
//        control.setOrderByValues(detailsQuery.getOrderByValues());
        control.setControlType("INVISIBLE");
        analyticsMetricRequest.getMetricQueryList().add(control);
//        analyticsMetricRequest.getMetricQuerys().add(detailsQuery);
        return analyticsMetricRequest;
    }

    @Override
    @Transactional
    public AnalyticsMetricDetailsResponse getDrillDownAnalyticMetricDetailsData(String metricDetailsId, String xAxisParam, String subXAxisParam) {
        AnalyticsMetricDetails analyticsMetricDetails = analyticsDAO.getAnalyticsMetricDetails(metricDetailsId, true, false);
        return getAnalyticMetricDetailsData(analyticsMetricDetails, xAxisParam, subXAxisParam, false);
    }

    private AnalyticsMetricDetailsResponse getAnalyticMetricDetailsData(String metricDetailsId, String xAxisParam, String subXAxisParam) {
        AnalyticsMetricDetails analyticsMetricDetails = analyticsDAO.getAnalyticsMetricDetails(metricDetailsId, false, false);
        return getAnalyticMetricDetailsData(analyticsMetricDetails, xAxisParam, subXAxisParam, false);
    }
    /**
     *
     * @param metricDetails
     * @return
     */
    private AnalyticsMetricDetailsResponse getAnalyticMetricDetailsData(AnalyticsMetricDetails metricDetails,  String xAxisParam, String subXAxisParam, boolean isTableRequired) {
        /*
        Metric details has got list of analyticsMetricQueries. Loop through these queries and build the AnalyticsMetricRequest accordingly.
         */
        AnalyticsMetricDetailsResponse response = new AnalyticsMetricDetailsResponse();
        Map<ChartAttributeAxisCode, AnalyticsMetricRequest> analyticsMetricRequestMap = new HashMap<>();
            /*
            Now each metric Details has got analyticsMetricDetailsQueryList and analyticsMetricDetailsFilterList
            this will help us in building analytics request.
             */
        List<AnalyticsMetricDetailsQuery> detailsQueries = metricDetails.getAnalyticsMetricDetailsQueryList();
        if(detailsQueries!=null){
            for(AnalyticsMetricDetailsQuery detailsQuery : detailsQueries){
                AnalyticsMetricRequest analyticsMetricRequest = null;
                String attrAxizCode = detailsQuery.getAttrDispAxizCodeFkId();
                ChartAttributeAxisCode attributeAxisCodeEnum = Enum.valueOf(ChartAttributeAxisCode.class, attrAxizCode);
                if(analyticsMetricRequestMap.containsKey(attributeAxisCodeEnum)){
                    analyticsMetricRequest = analyticsMetricRequestMap.get(attributeAxisCodeEnum);
                }else{
                    analyticsMetricRequest = new AnalyticsMetricRequest();
                    analyticsMetricRequestMap.put(attributeAxisCodeEnum, analyticsMetricRequest);
                }
                    /* Each details query should be converted to section control */
//                SectionControlVO control = new SectionControlVO();
//                control.setControlAttributePathExpn(detailsQuery.getAttrPathExpn());
//                control.setProcessElementFkId(detailsQuery.getMtPeCodeFkId());
////                control.setSequenceNumber(detailsQuery.getMtPeSeqNumPosInt());
////                control.setParentSeqNumber(detailsQuery.getParentMtPeSeqNumPosInt());
//                control.setOrderByValuesTxt(detailsQuery.getOrderByValuesBigTxt());
//                if(detailsQuery.isAggregateAttr())
//                    control.setAggregateControl(true);
////                control.setAggFunctionExpn(detailsQuery.getAggregateFunctionExpn());
//                control.setGroupByControl(detailsQuery.isGroupByAttribute());
//                control.setGroupByControlSeqNumPosInt(detailsQuery.getGroupBySeqNumPosInt());
//                control.setGroupByHavingExpn(detailsQuery.getGroupByHavingExpn());
//                control.setOrderBySeqNumber(detailsQuery.getOrderBySeqNumPosInt());
//                control.setOrderByMode(detailsQuery.getOrderByModeCodeFkId());
//                control.setOrderByValuesTxt(detailsQuery.getOrderByValuesBigTxt());
//                if(attributeAxisCodeEnum.equals(ChartAttributeAxisCode.SUB_X_AXIS) || attributeAxisCodeEnum.equals(ChartAttributeAxisCode.X_AXIS))
//                    control.setDistinct(true);
//                analyticsMetricRequest.getMetricQueryList().add(control);
                analyticsMetricRequest.getMetricQueryDetails().add(detailsQuery);
            }
        }
        List<AnalyticsMetricDetailsFilter> detailsFilter = metricDetails.getAnalyticsMetricDetailsFilterList();
        Map<String, LoadParameters> peToLoadParameters = new HashMap<>();
        if(detailsFilter!=null){
            for(AnalyticsMetricDetailsFilter metricDetailsFilter : detailsFilter){
                LoadParameters parameters = new LoadParameters();
                String filterExpn = metricDetailsFilter.getFilterExpn();
                if(filterExpn.contains("${X-Axis}")){
                    if(xAxisParam.startsWith("0x")){
                        filterExpn = filterExpn.replaceAll("\\$\\{X-Axis}", xAxisParam);
                    }else{
                        filterExpn = filterExpn.replaceAll("\\$\\{X-Axis}", "'"+xAxisParam+"'");
                    }
                }
                parameters.setFilterExpn(filterExpn);
                parameters.setProcessElementId(metricDetailsFilter.getMtPeCodeFkId());
                peToLoadParameters.put(metricDetailsFilter.getMtPeCodeFkId(), parameters);
            }
        }
        /* Now make call to data serviceImpl to be able to make the PE Requests and get the result with the help of controls provided. */
//        List<ProcessElementData> result = dataRequestService.getAnalyticsMetricDetailsData(analyticsMetricRequest);
//        response.setyAxisData(result);
        BeanUtils.copyProperties(metricDetails, response);
        AnalyticsMetricRequest yAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.Y_AXIS);
        if(yAxisRequest!=null){
            yAxisRequest.setPeToLoadParamsMap(peToLoadParameters);
            UnitResponseData yAxisResult = dataRequestService.getAnalyticsMetricDetailsData(yAxisRequest);
            response.setyAxisData(yAxisResult);
        }
        AnalyticsMetricRequest minYAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.MIN_Y_AXIS);
        if(minYAxisRequest!=null){
            minYAxisRequest.setPeToLoadParamsMap(peToLoadParameters);
            UnitResponseData minYAxisResult = dataRequestService.getAnalyticsMetricDetailsData(minYAxisRequest);
            response.setMinYAxisData(minYAxisResult);
        }
        AnalyticsMetricRequest avgYAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.AVG_Y_AXIS);
        if(avgYAxisRequest!=null){
            avgYAxisRequest.setPeToLoadParamsMap(peToLoadParameters);
            UnitResponseData avgYAxisResult = dataRequestService.getAnalyticsMetricDetailsData(avgYAxisRequest);
            response.setAvgYAxisData(avgYAxisResult);
        }
        AnalyticsMetricRequest maxYAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.MAX_Y_AXIS);
        if(maxYAxisRequest!=null){
            maxYAxisRequest.setPeToLoadParamsMap(peToLoadParameters);
            UnitResponseData maxYAxisResult = dataRequestService.getAnalyticsMetricDetailsData(maxYAxisRequest);
            response.setMaxYAxisData(maxYAxisResult);
        }
        if(isTableRequired) {
            AnalyticsMetricRequest tableRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.TABLE_AXIS);
            if (tableRequest != null) {
                tableRequest.setPeToLoadParamsMap(peToLoadParameters);
                UnitResponseData tableResult = dataRequestService.getAnalyticsMetricDetailsData(tableRequest);
                response.setTableData(tableResult);
            }
        }
        AnalyticsMetricRequest subXAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.SUB_X_AXIS);
        if(subXAxisRequest!=null){
            subXAxisRequest.setPeToLoadParamsMap(peToLoadParameters);
            UnitResponseData subXAxisResult = dataRequestService.getAnalyticsMetricDetailsData(subXAxisRequest);
            response.setSubXAxisData(getXAxisResult(subXAxisResult, subXAxisRequest.getMetricQueryDetails(), null));
        }
        AnalyticsMetricRequest xAxisRequest = analyticsMetricRequestMap.get(ChartAttributeAxisCode.X_AXIS);
        if(xAxisRequest!=null){
            xAxisRequest.setPeToLoadParamsMap(peToLoadParameters);
            UnitResponseData xAxisResponse = dataRequestService.getAnalyticsMetricDetailsData(xAxisRequest);
            response.setxAxisData(getXAxisResult(xAxisResponse, xAxisRequest.getMetricQueryDetails(), xAxisRequest.getxAxis()));
        }
        /* Modify data according to the client requirement */
        if(response.getxAxisData()!=null) {
            for (XAxisData xAxisSeries : response.getxAxisData()) {
                String mapperValue = xAxisSeries.getMappedData();
                if (response.getSubXAxisData() != null && !response.getSubXAxisData().isEmpty()) {
                    for (XAxisData subXAxisSeriesData : response.getSubXAxisData()) {
                        String subXAxisMapperValue = subXAxisSeriesData.getMappedData();
//                        buildClientResponse(xAxisSeries, subXAxisSeriesData, response);
                    }
                } else {
//                    buildClientResponse(xAxisSeries, null, response);
                }
            }
        }
        return response;
    }

    private void buildClientResponse(XAxisData xAxisSeries, XAxisData subXAxisSeriesData, GraphResponse response) {
        /* Now for this xAxis and subXAxis get the Y-Axis Response */
        UnitResponseData yAxisResultSet = response.getyAxisData();
        double contextualYAxisResult = getResultForXAndSubXValue(xAxisSeries, subXAxisSeriesData, yAxisResultSet);
        double contextualMinYAxisResult = getResultForXAndSubXValue(xAxisSeries, subXAxisSeriesData, response.getMinYAxisData());
        double contextualMaxYAxisResult = getResultForXAndSubXValue(xAxisSeries, subXAxisSeriesData, response.getMaxYAxisData());
        double contextualAvgYAxisResult = getResultForXAndSubXValue(xAxisSeries, subXAxisSeriesData, response.getAvgYAxisData());
        /* If Sub X exists then the response will be different than that of only X axis */
        if(subXAxisSeriesData!=null && StringUtil.isDefined(subXAxisSeriesData.getMappedData())){
            subXAxisSeriesData.setyAxisResult(contextualYAxisResult);
            subXAxisSeriesData.setMinYAxisResult(contextualMinYAxisResult);
            subXAxisSeriesData.setMaxYAxisResult(contextualMaxYAxisResult);
            subXAxisSeriesData.setAvgYAxisResult(contextualAvgYAxisResult);
            if(response.getDataSeriesMap().containsKey(xAxisSeries.getMappedData())){
                XAxisDataInternal xAxisResult = (XAxisDataInternal) response.getDataSeriesMap().get(xAxisSeries.getMappedData());
                xAxisResult.getSubXDataSeries().add(subXAxisSeriesData);
            }else{
                XAxisDataInternal xAxisResult = new XAxisDataInternal(xAxisSeries.getMappedData(),xAxisSeries.getDisplayData());
                xAxisResult.getSubXDataSeries().add(subXAxisSeriesData);
                response.getDataSeriesMap().put(xAxisSeries.getMappedData(), xAxisResult);
                response.getDataSeries().add(xAxisResult);
            }
        }else{
            /* Now if only x-Axis is there */
            xAxisSeries.setyAxisResult(contextualYAxisResult);
            xAxisSeries.setMinYAxisResult(contextualMinYAxisResult);
            xAxisSeries.setMaxYAxisResult(contextualMaxYAxisResult);
            xAxisSeries.setAvgYAxisResult(contextualAvgYAxisResult);
            response.getDataSeriesMap().put(xAxisSeries.getMappedData(), xAxisSeries);
            response.getDataSeries().add(xAxisSeries);
        }
    }

    private double getResultForXAndSubXValue(XAxisData xAxisData, XAxisData subXAxisData, UnitResponseData yAxisResultSet) {
        if(yAxisResultSet == null)
            return 0;
        String xAxismappedData = xAxisData.getMappedData();
        String subXAxismappedData = null;
        if(subXAxisData!=null){
            subXAxismappedData = subXAxisData.getMappedData();
        }
        Map<String, ProcessElementData> peResult = yAxisResultSet.getPEData();
        if(peResult.size()==1) {
            for (Map.Entry<String, ProcessElementData> entry : peResult.entrySet()) {
                ProcessElementData peData = entry.getValue();
                Map<String, AggregateData> aggregateDataMap = peData.getAggregateResult();
                if(aggregateDataMap!=null){
                    if(aggregateDataMap.size()==1){
                        for(Map.Entry<String, AggregateData> aggregateDataEntry : aggregateDataMap.entrySet()){
                            AggregateData aggData = aggregateDataEntry.getValue();
                            AggregateData xAxisResult = aggData.getGroupByValue().get(xAxismappedData);
                            if(xAxisResult!=null){
                                /* Now check if subXAxis result exists or not */
                                if(StringUtil.isDefined(subXAxismappedData)){
                                    AggregateData subXAxisResult = xAxisResult.getGroupByValue().get(subXAxismappedData);
                                    if(subXAxisResult== null){
                                        return 0;
                                    }else{
                                        return (double)subXAxisResult.getValue();
                                    }
                                }else{
                                    return (double)xAxisResult.getValue();
                                }
                            }else{
                                return 0;
                            }
                        }
                    }else{
                        //TODO throw error
                    }
                }
            }
        }else{
            //TODO throw error
        }
        return 0;
    }

    private List<XAxisData> getXAxisResult(UnitResponseData xAxisResponse, List<AnalyticsMetricDetailsQuery> metricQueryDetails, MetricAxis metricAxis) {
        List<XAxisData> xAxisResult = new ArrayList<>();
        Map<String, ProcessElementData> xAxisPEResult = xAxisResponse.getPEData();
        Map<String, DoaMeta> doaMetaMap = xAxisResponse.getLabelToAliasMap();
        for(Map.Entry<String, ProcessElementData> peData : xAxisPEResult.entrySet()){
            List<Record> records = peData.getValue().getRecordSetInternal();
            for(Record record :records) {
                XAxisData xAxisData = new XAxisData();
                String attributePathExpn = metricAxis.getControlAttributePath();
                if (metricAxis.isClientDisplay()) {
                    DoaMeta meta = doaMetaMap.get(attributePathExpn);
                    AttributeData result = record.getAttributeMap().get(meta.getAlias());
                    xAxisData.setDisplayData(result.getcV().toString());
                }
                if (metricAxis.isClientMatch()) {
                    DoaMeta meta = doaMetaMap.get(attributePathExpn);
                    AttributeData result = record.getAttributeMap().get(meta.getAlias());
                    xAxisData.setMappedData(result.getcV().toString());
                }
//                for (AnalyticsMetricDetailsQuery detailsQuery : metricQueryDetails) {
//                    String attributePathExpn = detailsQuery.getAttrPathExpn();
//                    if (detailsQuery.isClientDisplay()) {
//                        DoaMeta meta = doaMetaMap.get(attributePathExpn);
//                        AttributeData result = record.getAttributeMap().get(meta.getAlias());
//                        xAxisData.setDisplayData(result.getcV().toString());
//                    }
//                    if (detailsQuery.isClientMatch()) {
//                        DoaMeta meta = doaMetaMap.get(attributePathExpn);
//                        AttributeData result = record.getAttributeMap().get(meta.getAlias());
//                        xAxisData.setMappedData(result.getcV().toString());
//                    }
//                }
                xAxisResult.add(xAxisData);
            }
                /* Now on the basis of control attribute path get the result */
        }
        return xAxisResult;
    }

    public AnalyticsDAO getAnalyticsDAO() {
        return analyticsDAO;
    }

    public void setAnalyticsDAO(AnalyticsDAO analyticsDAO) {
        this.analyticsDAO = analyticsDAO;
    }

    public DataRequestService getDataRequestService() {
        return dataRequestService;
    }

    public void setDataRequestService(DataRequestService dataRequestService) {
        this.dataRequestService = dataRequestService;
    }

    public UILayoutService getUiLayoutService() {
        return uiLayoutService;
    }

    public void setUiLayoutService(UILayoutService uiLayoutService) {
        this.uiLayoutService = uiLayoutService;
    }
}
