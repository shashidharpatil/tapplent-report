package com.tapplent.apputility.entity.provider;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.tapplent.apputility.data.service.DataService;
import com.tapplent.apputility.data.structure.*;
import com.tapplent.apputility.entity.structure.ExitRuleCriteria;
import com.tapplent.apputility.entity.structure.PersonExitRule;
import com.tapplent.apputility.groupResolver.service.GroupResolverService;
import com.tapplent.apputility.uilayout.structure.CostCentreBreakUpData;
import com.tapplent.apputility.uilayout.structure.JobFamilyBreakUpData;
import com.tapplent.apputility.uilayout.structure.LocationBreakUpData;
import com.tapplent.apputility.uilayout.structure.OrganizationBreakUpData;
import com.tapplent.platformutility.accessmanager.dao.AccessManagerService;
import com.tapplent.platformutility.accessmanager.structure.DOAPermission;
import com.tapplent.platformutility.accessmanager.structure.Permission;
import com.tapplent.platformutility.activityLog.ActivityLog;
import com.tapplent.platformutility.activityLog.ActivityLogDtl;
import com.tapplent.platformutility.activityLog.AuditMsgFormat;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.data.insertEntity.InsertEntityPK;
import com.tapplent.platformutility.data.insertEntity.InsertEntityVersion;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.g11n.service.G11nService;
import com.tapplent.platformutility.hierarchy.service.HierarchyService;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.layout.valueObject.PersonTagsVO;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.person.service.PersonService;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

import com.tapplent.platformutility.insert.impl.ValidationError;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.insert.impl.DefaultInsertData;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.impl.SearchDAO;
import com.tapplent.platformutility.sql.context.builder.InsertContextBuilder;
import com.tapplent.platformutility.sql.context.builder.SQLContextBuilder;
import com.tapplent.platformutility.sql.context.model.InsertContextModel;
import com.tapplent.platformutility.sql.statement.builder.InsertStatementBuilder;
import com.tapplent.platformutility.sql.statement.builder.UpdateStatementBuilder;
import com.tapplent.platformutility.workflow.service.WorkflowExecutor;
import com.tapplent.platformutility.workflow.service.WorkflowProvider;

public class DefaultEntityProvider implements EntityProvider{
	
	private static final Logger LOG = LogFactory.getLogger(DefaultEntityProvider.class);
	
	InsertService insertService;
	SearchDAO searchDAO;
	DataService dataService;
	WorkflowProvider workflowProvider;
	WorkflowExecutor workflowExecutor;
	AccessManagerService accessManagerService;
	ValidationError validationError;
	HierarchyService hierarchyService;
	GroupResolverService groupResolverService;
	PersonService personService;
	G11nService g11nService;


	private void executePropogateChanges(GlobalVariables globalVariables) throws SQLException {
		//update table for those records in underlying table : records after insertData's 'EFFECTIVE_DATETIME' using field PROPOGATE_CHANGES and convert using data-type.
		//change version changes for all the above described records(by deleting 'CHANGED' fields in those records' 'VERSION_CHANGES' field).
//		if(!globalVariables.workflowDetails.isWorkflowApplicable)
			//doPropagateAndUpdateVersionChanges(globalVariables);
//		else {
//			String workflowStatus = (String) globalVariables.relatedUnderlyingRecord.get("CURRENT").get("REFERENCE_WORKFLOW_TRANSACTION_STATUS");
//			String serverStatusString = propertyCodeMap.get("WORKFLOW_STATUS_PROPERTY");//FIXME use correct proporty name.
//			if(serverStatusString != null){
//				for(String status : serverStatusString.split(",")){
//					if(workflowStatus.equalsIgnoreCase(status.trim())){
//						doPropagateAndUpdateVersionChanges(globalVariables);
//						break;
//					}
//				}
//			}
//		}
		//based on the rule and properties provided flush all the fields from PROPOGATE_CHANGES fields of those records(conditionally). Ensure 'empty blob' not null.
		
	}

	private void doPropagateAndUpdateVersionChanges(GlobalVariables globalVariables) throws SQLException {
		Map<String, Object> map = globalVariables.insertData.getPropogateData();
		Map<String, Object> existingRecord = globalVariables.relatedUnderlyingRecord.get("CURRENT");
		Object existingPropagationObject = existingRecord.get("PROPOGATION_CHANGES");
		if(existingPropagationObject != null){
			JsonNode existingPropagationFieldsNode = (JsonNode) existingPropagationObject;
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> existingPropagationFieldsMap = mapper.convertValue(existingPropagationFieldsNode, Map.class);
			for(Entry<String, Object> entry : existingPropagationFieldsMap.entrySet()){
				if(!map.containsKey(entry.getKey())){
					map.put(entry.getKey(), entry.getValue());
				}
			}
		}
		if(!map.isEmpty()){
			StringBuilder propagationQuery = new StringBuilder();
			MasterEntityAttributeMetadata pkMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(globalVariables.model.getInsertEntityPK().getDoaName());
			Object pkValue = Util.getStringTobeInserted(pkMeta.getDbDataTypeCode(), existingRecord.get(globalVariables.model.getInsertEntityPK().getColumn()), false);
			propagationQuery.append("UPDATE "+globalVariables.model.getEntityName()+" SET ''");
			for(Entry<String, Object> field : map.entrySet()){
				EntityAttributeMetadata attributeProperty = globalVariables.entityMetadataVOX.getAttributeByColumnName(field.getKey());
				propagationQuery.append(", "+field.getKey()+" = "+Util.getStringTobeInserted(attributeProperty, field.getValue()));
			}
			propagationQuery.append(" WHERE " + globalVariables.model.getInsertEntityPK().getColumn() + " = "+pkValue);
			propagationQuery.append(" AND EFFECTIVE_DATETIME > " + existingRecord.get("EFFECTIVE_DATETIME") + ";");
			insertService.insertRow(propagationQuery.toString());
			StringBuilder updateVersionChangesQuery = new StringBuilder();
			//UPDATE assets SET dynamic_cols=COLUMN_DELETE(dynamic_cols, "price") WHERE COLUMN_GET(dynamic_cols, 'color' as char)='black'; 
			updateVersionChangesQuery.append("UPDATE "+ globalVariables.model.getEntityName()+" SET VERSION_CHANGES = COLUMN_DELETE(VERSION_CHANGES, ''");
			for(Entry<String, Object> field : map.entrySet()){
				updateVersionChangesQuery.append(", '"+field.getKey()+"'");
			}
			updateVersionChangesQuery.append(") WHERE " + globalVariables.model.getInsertEntityPK().getColumn() + " ="+pkValue);
			propagationQuery.append(" AND EFFECTIVE_DATETIME > " + existingRecord.get("EFFECTIVE_DATETIME") + ";");
			insertService.insertRow(updateVersionChangesQuery.toString());
			//UPDATE JSON_TEST_TABLE SET VBLOB = COLUMN_CREATE('a',null);
			String emptyPropagationBlobQuery = "UPDATE "+ globalVariables.model.getEntityName() + " SET PROPOGATION_CHANGES = COLUMN_CREATE('null', null)";
			insertService.insertRow(emptyPropagationBlobQuery);
		}
	}

	private void updatePropagateChanges(GlobalVariables globalVariables) throws SQLException, ParseException {
		Map<String, Object> existingRecord = globalVariables.relatedUnderlyingRecord.get("CURRENT");
		Map<String, Object> changedFields;
		Map<String, Object> propagatableChangedFields;
		if(existingRecord != null){
			changedFields = getNonStandardChangedPeviousFields(existingRecord, globalVariables.insertData.getPropogateData(), globalVariables);
			if(!changedFields.isEmpty()){
				propagatableChangedFields = new HashMap<>();
				for (Entry<String, Object> entry : changedFields.entrySet()){
					EntityAttributeMetadata attributeMetadata = globalVariables.entityMetadataVOX.getAttributeByName(entry.getKey());
					if (attributeMetadata.isPropogatable())
						propagatableChangedFields.put(entry.getKey(), entry.getValue());
				}
				if (propagatableChangedFields.isEmpty()) {
					String propagationUpdateQuery = buildQueryForPropogationChangesFields(propagatableChangedFields, existingRecord, globalVariables);
					insertService.insertRow(propagationUpdateQuery);
				}
			}
		}
	}

	private String buildQueryForPropogationChangesFields(Map<String, Object> changedFields, Map<String, Object> existingRecord, GlobalVariables globalVariables) {
		//UPDATE JSON_TEST_TABLE SET VBLOB = COLUMN_ADD(VBLOB, 'color', 'black','timestamp', current_timestamp, 'shape','circle') where BASE_TEMPLATE_ID = 0x11E5E45E8F591028A81312244D68BFD1;
		StringBuilder propogationFields = new StringBuilder();
		propogationFields.append("UPDATE " + globalVariables.model.getEntityName() + " SET PROPOGATION_CHANGES = COLUMN_ADD(PROPOGATION_CHANGES, ");
		for(Entry<String, Object> field : changedFields.entrySet()){
			propogationFields.append("'" + field.getKey() + "'");
			propogationFields.append(", '" + field.getValue().toString() + "', ");
		}
		propogationFields.replace(propogationFields.length()-2, propogationFields.length()-1, "");
		MasterEntityAttributeMetadata pkMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(globalVariables.model.getInsertEntityPK().getDoaName());
		propogationFields.append(") WHERE " + globalVariables.model.getInsertEntityPK().getColumn() + " = "+Util.getStringTobeInserted(pkMeta.getDbDataTypeCode(), existingRecord.get(globalVariables.model.getInsertEntityPK().getColumn()), false));
		propogationFields.append("AND EFFECTIVE_DATETIME > " + existingRecord.get("EFFECTIVE_DATETIME") + ";");
		return propogationFields.toString();
	}

	@Override
	public void setUnderlyingRelatedDataFromDb(GlobalVariables globalVariables) throws Exception {
//		Map<String, Map<String, List<Relation>>> reMap = hierarchyService.getAllRelationBetweenBaseAndRelatedPerson("0x4CD3D44894FB0E379CB3E42317EAD98C", "0x486420836D049E1D81B480802F6C03B5");
//		Map<String, Map<String, List<Relation>>> repMap = TenantAwareCache.getPersonRepository().getPersonRelationMap("0x4391EA95CEF3721C9DA9D99B9EFD07EA", "0x4D8EC242F5E73B088050200D3D70D2E0");
//		Map<String, Map<Integer, List<String>>> repMap = hierarchyService.getAllRelationsForPerson("0x4A28F82F8998EA64925FDF5B19AE2B66");
		String doName = globalVariables.entityMetadataVOX.getDomainObjectCode();
		EntityAttributeMetadata pkMeta = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
		String pkValue = (String) globalVariables.insertData.getData().get(pkMeta.getDoAttributeCode());
		String pkColumnName = pkMeta.getDbColumnName().trim();
		String versionId = null;
		String versionColumnName = null;
		EntityAttributeMetadata vMeta = null;
		if(globalVariables.entityMetadataVOX.isVersioned()) {
			vMeta = globalVariables.entityMetadataVOX.getVersoinIdAttribute();
			versionId = (String) globalVariables.insertData.getData().get(vMeta.getDoAttributeCode());
			versionColumnName = vMeta.getDbColumnName().trim();
		}
		if(globalVariables.model != null){
			pkValue = pkValue != null ? pkValue : (String) globalVariables.model.getInsertValueMap().get(pkMeta.getDbColumnName());
			if (globalVariables.entityMetadataVOX.isVersioned())
				versionId = versionId != null ? versionId : (String) globalVariables.model.getInsertValueMap().get(vMeta.getDbColumnName());
		}
		Deque<Object> arguments  =  new ArrayDeque<>();
		StringBuilder sql = new StringBuilder();
		String selectString =  Util.getSqlStringForTable(globalVariables.entityMetadataVOX);
		if(pkValue != null && globalVariables.entityMetadataVOX.isVersioned() && versionId != null){
				sql.append("(" + selectString + " WHERE " + pkColumnName +
						" = "+Util.getStringTobeInserted(pkMeta, pkValue)+" AND " + versionColumnName + " = ?);");
//				arguments.add(pkValue);
				arguments.add(Util.convertStringIdToByteId(versionId));
			LOG.debug("----------UNDERLYING_RECORD_QUERY----------");
			SearchResult currentSr = searchDAO.search(sql.toString(), arguments);
			if(currentSr.getData() != null && !currentSr.getData().isEmpty()){
				List<Map<String, Object>> typedData = Util.convertRawDataIntoTypedData(currentSr.getData(), globalVariables.entityMetadataVOX);
				globalVariables.relatedUnderlyingRecord.put("CURRENT", typedData.get(0));
				globalVariables.underlyingRecord = typedData.get(0);
			}
			if( !globalVariables.relatedUnderlyingRecord.isEmpty()){
				Timestamp effectiveDatetime = (Timestamp) globalVariables.relatedUnderlyingRecord.get("CURRENT").get(doName+"."+"EffectiveDateTime");//get("EFFECTIVE_DATETIME");
				if (effectiveDatetime == null){
					validationError.addError("EFFECTIVE_DATETIME_INVALID", "Effective Datetime is null for "+pkMeta.getDoAttributeCode()+" : "+pkValue);
					throw new Exception();
				}
				sql = new StringBuilder();
				sql.append("(" + selectString + " WHERE " + pkColumnName +
						" = "+Util.getStringTobeInserted(pkMeta, pkValue)+" AND EFFECTIVE_DATETIME < ? ORDER BY EFFECTIVE_DATETIME DESC LIMIT 1) UNION ");
				sql.append("(" + selectString + " WHERE " + pkColumnName +
						" = "+Util.getStringTobeInserted(pkMeta, pkValue)+" AND EFFECTIVE_DATETIME > ? ORDER BY EFFECTIVE_DATETIME ASC LIMIT 1);");
				arguments  =  new ArrayDeque<>();
				arguments.add(effectiveDatetime);
				arguments.add(effectiveDatetime);
				LOG.debug("----------UNDERLYING_RELATED_VERSIONED_RECORD_QUERY----------");
				SearchResult sr = searchDAO.search(sql.toString(), arguments);
				if(sr.getData() != null && !sr.getData().isEmpty()){
					List<Map<String, Object>> typedData = Util.convertRawDataIntoTypedData(sr.getData(), globalVariables.entityMetadataVOX);
					for(Map<String, Object> record : typedData){
						Timestamp timestamp = (Timestamp) record.get(doName+"."+"EffectiveDateTime");
						if(effectiveDatetime.after(timestamp))
							globalVariables.relatedUnderlyingRecord.put("HISTORY", record);
						else if(effectiveDatetime.before(timestamp))
							globalVariables.relatedUnderlyingRecord.put("FUTUTRE", record);
					}
				}
			}
		}else if (pkValue != null && !globalVariables.entityMetadataVOX.isVersioned()){
			sql.append("(" + selectString + " WHERE " + pkColumnName +
					" = "+Util.getStringTobeInserted(pkMeta, pkValue)+");");
			LOG.debug("----------UNDERLYING_RECORD_QUERY----------");
			SearchResult currentSr = searchDAO.search(sql.toString(), arguments);
			if(currentSr.getData() != null && !currentSr.getData().isEmpty()){
				List<Map<String, Object>> typedData = Util.convertRawDataIntoTypedData(currentSr.getData(), globalVariables.entityMetadataVOX);
				globalVariables.relatedUnderlyingRecord.put("CURRENT", typedData.get(0));
				globalVariables.underlyingRecord = typedData.get(0);
			}
		}
		else {
			LOG.debug("Primary Key or Version Id or both are not present.: PK-"+pkValue+" Version-" + versionId);
		}
	}

	@Override
	public void createUser(GlobalVariables globalVariables) throws SQLException {
		insertService.createUser(globalVariables);
	}

	@Override
	public String generateEmployeeCode(String employeeCodeGenKey, Map<String, Object> data) throws SQLException {
		return insertService.generateEmployeeCode(employeeCodeGenKey, data);
	}

	@Override
	public void executeEventsForJobAssignment(GlobalVariables globalVariables) throws SQLException {
		updateBreakUpData(globalVariables);
	}

	@Override
	public void inactivateUser(GlobalVariables globalVariables) throws SQLException, ParseException {
		insertService.inactivateUser(globalVariables);
	}

	@Override
	public void updateExistingRelationships(GlobalVariables globalVariables) throws SQLException {
		Map<String, Object> data = globalVariables.insertData.getData();
		String personId = (String) data.get("PersonTxn.Person");
		Map<String, ExitRuleCriteria> exitRuleCriteriaMap = insertService.getExitRuleCriteria();
		Map<String, List<PersonExitRule>> criteriaToExitRuleMap = insertService.getCriteriaToExitRuleMap();
		Map<String, Map<Integer, List<String>>> allRelationForPerson = hierarchyService.getAllRelationsForPerson(personId);

	}

	@Override
	public Map<String, List<PersonExitRule>> getCriteriaToExitRuleMap() throws SQLException {
		return insertService.getCriteriaToExitRuleMap();
	}

	@Override
	public Map<String, Map<Integer, List<String>>> getAllRelationsForPerson(String personId) {
		return hierarchyService.getAllRelationsForPerson(personId);
	}

	@Override
	public List<Map<String, Object>> getAllRecordsByExitRule(PersonExitRule personExitRule, String personId) throws SQLException {
		String relationshipType = getRelationshipTypeByExitRule(personExitRule);
		if (relationshipType != null){
			if (relationshipType.equals("REPL_APPR_PENDING")){
				//get all workflow actor pks
			}
			else {
				List<Map<String, Object>> records = getAllRecordsByRelationshipType(relationshipType, personId);
				return records;
			}
		}
		return null;
	}

	@Override
	public List<Map<String,Object>> getAllRecordsByRelationshipType(String relationshipType, String personId) {
		MasterEntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE("PersonJobRelationship");
		String sql = Util.getSqlStringForMasterTable(entityMetadataVOX) + " WHERE RELATED_PERSON_FK_ID = x? AND RELATIONSHIP_TYPE_CODE_FK_ID = ? " +
				"AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND RECORD_STATE_CODE_FK_ID = 'CURRENT' AND IS_DELETED = FALSE;";
		Deque<Object> params = new ArrayDeque<>();
		params.add(Util.remove0x(personId));
		params.add(relationshipType);
		SearchResult sr = searchDAO.search(sql, params);
		if (sr.getData() != null){
			for (Map<String, Object> data : sr.getData()){
				for (Entry<String, Object> entry : data.entrySet()){
					if (entry.getValue() instanceof byte[]){
						entry.setValue(Util.convertByteToString((byte[]) entry.getValue()));
					}
				}
			}
		}
		return sr.getData();
	}

	@Override
	public Map<String, Object> getDirectManagerRecord(String personId) {
		MasterEntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE("PersonJobRelationship");
		String sql = Util.getSqlStringForMasterTable(entityMetadataVOX) + " P INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE R \n" +
				" ON P.RELATIONSHIP_TYPE_CODE_FK_ID = R.RELATIONSHIP_TYPE_CODE_PK_ID AND R.IS_DIRECT_MANAGER INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT A \n" +
				" ON A.PERSON_JOB_ASSIGNMENT_PK_ID = P.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND A.IS_PRIMARY = TRUE WHERE P.SUBJECT_PERSON_FK_ID = x? AND " +
				" SAVE_STATE_CODE_FK_ID = 'SAVED' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND RECORD_STATE_CODE_FK_ID = 'CURRENT' IS_DELETED = FALSE;";
		Deque<Object> params = new ArrayDeque<>();
		params.add(Util.remove0x(personId));
		SearchResult sr = searchDAO.search(sql, params);
		if (sr.getData() != null){
			for (Map<String, Object> data : sr.getData()){
				for (Entry<String, Object> entry : data.entrySet()){
					if (entry.getValue() instanceof byte[]){
						entry.setValue(Util.convertByteToString((byte[]) entry.getValue()));
					}
				}
			}
		}
		return sr.getData().get(0);
	}

	@Override
	public Map<String, Object> getUnderlyingPersonRecord(String personId, MasterEntityMetadataVOX masterEntityMetadataVOX) {
		String sql = "SELECT * FROM T_PRN_EU_PERSON WHERE PERSON_PK_ID = x? AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' \n" +
				"AND RECORD_STATE_CODE_FK_ID = 'CURRENT' AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND IS_DELETED = FALSE;";
		Deque<Object> parameter = new ArrayDeque<>();
		parameter.add(Util.remove0x(personId));
		SearchResult searchResult = searchDAO.search(sql, parameter);
		if (searchResult.getData() != null && !searchResult.getData().isEmpty()){
			return searchResult.getData().get(0);
		}
		return null;
	}

	private String getRelationshipTypeByExitRule(PersonExitRule personExitRule) {
		if (personExitRule != null) {
			switch (personExitRule.getExitRuleCodeId()) {
				case "REPL_DIRECT_MGR":
					return "DIRECT_MANAGER";
				case "REPL_MATRIX_MGR":
					return "MATRIX_MANAGER";
				case "REPL_PRIMARY_HR_MGR":
					return "PRIMARY_HR_MANAGER";
				case "REPL_SECONDARY_HR_MGR":
					return "SECONDARY_HR_MANAGER";
				case "REPL_PROJ_MGR":
					return "PROJECT_MANAGER";
				case "REPL_FAC_MGR":
					return "FACILITY_MANAGER";
				case "REPL_IT_MGR":
					return "IT_MANAGER";
				case "REPL_APPR_PENDING":
					return "REPL_APPR_PENDING";
				default:
					return null;
			}
		}
		return null;
	}

	private void updateBreakUpData(GlobalVariables globalVariables) throws SQLException {
		Map<String, Object> data = globalVariables.insertData.getData();
		Map<String, String> breakupIdMap = getBreakUpIdMap(data);
		List<OrganizationBreakUpData> organizationBreakUpData = (List<OrganizationBreakUpData>) dataService.getBreakedUpData("ORGANIZATION", breakupIdMap.get("ORGANIZATION"));
		insertService.updateBreakUpDataForOrg(organizationBreakUpData, globalVariables);
		List<LocationBreakUpData> locationBreakUpData = (List<LocationBreakUpData>) dataService.getBreakedUpData("LOCATION", breakupIdMap.get("LOCATION"));
		insertService.updateBreakUpDataForLoc(locationBreakUpData, globalVariables);
		List<CostCentreBreakUpData> cCBreakUpData = (List<CostCentreBreakUpData>) dataService.getBreakedUpData("COST_CENTRE", breakupIdMap.get("COST_CENTRE"));
		insertService.updateBreakUpDataForCC(cCBreakUpData, globalVariables);
		List<JobFamilyBreakUpData> jobFamilyBreakUpData = (List<JobFamilyBreakUpData>) dataService.getBreakedUpData("JOB_FAMILY", breakupIdMap.get("JOB_FAMILY"));
		insertService.updateBreakUpDataForJobFamily(jobFamilyBreakUpData, globalVariables);
	}

	private Map<String,String> getBreakUpIdMap(Map<String, Object> data) {
		Map<String, String> breakUpIdMap = new HashMap<>();
		String sql = "SELECT ORGANIZATION_FK_ID, LOCATION_FK_ID, COST_CENTER_FK_ID, JOB_FAMILY_FK_ID FROM T_PRN_EU_PERSON_JOB_ASSIGNMENT\n" +
				"  INNER JOIN T_ORG_ADM_ORG_JOB_PAY_GRADE_MAP ON ORG_JOB_PAY_GRADE_MAP_PK_ID = ORG_JOB_PAY_GRADE_MAP_FK_ID\n" +
				"  INNER JOIN T_ORG_ADM_ORG_JOB_PAY_GRADE_BAND_MAP ON ORG_JOB_PAY_GRADE_BAND_MAP_PK_ID = ORG_JOB_PAY_GRADE_BAND_MAP_FK_ID\n" +
				"  INNER JOIN T_ORG_ADM_ORG_JOB_MAP ON ORG_JOB_MAP_PK_ID = ORG_JOB_MAP_FK_ID\n" +
				"  INNER JOIN T_ORG_EU_ORG_LOC_MAP ON ORG_LOC_PK_ID = ORG_LOC_FK_ID\n" +
				"  INNER JOIN T_JOB_ADM_JOB_ROLE ON JOB_ROLE_PK_ID = JOB_ROLE_FK_ID\n" +
				"  WHERE ORG_JOB_PAY_GRADE_MAP_FK_ID = ? AND PERSON_DEP_FK_ID = ?;";
		Deque<Object> parameters = new ArrayDeque<>();
		parameters.add(Util.convertStringIdToByteId((String) data.get("PersonJobAssignment.OrgJobPayGradeMap")));
		parameters.add(Util.convertStringIdToByteId((String) data.get("PersonJobAssignment.Person")));
		SearchResult sr = searchDAO.search(sql, parameters);
		if (sr.getData() != null && !sr.getData().isEmpty()){
			Map<String, Object> record = sr.getData().get(0);
			breakUpIdMap.put("ORGANIZATION", Util.convertByteToString((byte[]) record.get("ORGANIZATION_FK_ID")));
			breakUpIdMap.put("LOCATION", Util.convertByteToString((byte[]) record.get("LOCATION_FK_ID")));
			breakUpIdMap.put("COST_CENTRE", Util.convertByteToString((byte[]) record.get("COST_CENTER_FK_ID")));
			breakUpIdMap.put("JOB_FAMILY", Util.convertByteToString((byte[]) record.get("JOB_FAMILY_FK_ID")));
		}
		return breakUpIdMap;
	}

	private void updateVersionDiffRecords(GlobalVariables globalVariables) throws SQLException, ParseException {
		if(globalVariables.relatedUnderlyingRecord != null){
			Map<String, Object> historyRecord = globalVariables.relatedUnderlyingRecord.get("HISTORY");
			Map<String, Object> currentRecord = globalVariables.relatedUnderlyingRecord.get("CURRENT");
			Map<String, Object> futureRecord = globalVariables.relatedUnderlyingRecord.get("FUTUTRE");
			Map<String, Object> changedFields;
			if(historyRecord != null && currentRecord != null){
				changedFields = getNonStandardChangedPeviousFields(historyRecord, currentRecord, globalVariables);
				if(!changedFields.isEmpty()){
					String updateQuery = buildQueryForVersionDiff(changedFields, currentRecord, globalVariables);
					insertService.insertRow(updateQuery);
				}
			}
			if(currentRecord != null && futureRecord != null){
				changedFields = getNonStandardChangedPeviousFields(historyRecord, currentRecord, globalVariables);
				if(!changedFields.isEmpty()){
					String updateQuery = buildQueryForVersionDiff(changedFields, futureRecord, globalVariables);
					insertService.insertRow(updateQuery);
				}
			}
		}
	}

	private String buildQueryForVersionDiff(Map<String, Object> changedFields, Map<String, Object> record, GlobalVariables globalVariables) {//FIXME Use this for propogate true blob too.
		StringBuilder versionDiffQuery = new StringBuilder();
		versionDiffQuery.append("UPDATE " + globalVariables.model.getEntityName() + " SET VERSION_CHANGES_BLOB = COLUMN_CREATE(");
		for(Entry<String, Object> field : changedFields.entrySet()){
			versionDiffQuery.append("'" + field.getKey() + "'");
			versionDiffQuery.append(", '" + field.getValue().toString() + "', ");
		}
		versionDiffQuery.replace(versionDiffQuery.length()-2, versionDiffQuery.length()-1, "");
		Object pkValue = record.get(globalVariables.model.getInsertEntityPK().getDoaName());
		String pkColumnName = globalVariables.model.getInsertEntityPK().getColumn(); 
		String pkName = globalVariables.model.getInsertEntityPK().getDoaName();
		MasterEntityAttributeMetadata pkMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(pkName);
		versionDiffQuery.append(") WHERE " + pkColumnName + " = "+Util.getStringTobeInserted(pkMeta.getDbDataTypeCode(), pkValue, false));
		versionDiffQuery.append(" AND " + globalVariables.model.getInsertEntityVersion().getColumn() + " = " + record.get(globalVariables.model.getInsertEntityVersion().getDoaName()) + " ;");
		return versionDiffQuery.toString();
	}

	private Map<String, Object> getNonStandardChangedPeviousFields(Map<String, Object> previousRecord, Map<String, Object> nextRecord, GlobalVariables globalVariables) throws ParseException {
		Map<String, Object> nonStandardPreviousRecord = new HashMap<>();
		Map<String, Object> nonStandardNextRecord = new HashMap<>();
		for(Entry<String, Object> entry : previousRecord.entrySet()){
			if(globalVariables.entityMetadataVOX.getAttribute(entry.getKey()) != null && 
					!globalVariables.entityMetadataVOX.getAttribute(entry.getKey()).isNonVersionTrackableFlag() &&
					!globalVariables.entityMetadataVOX.getStandardDoaFields().containsKey(entry.getKey()))
				nonStandardPreviousRecord.put(entry.getKey(), entry.getValue());
		}
		for(Entry<String, Object> entry : nextRecord.entrySet()){
			if(globalVariables.entityMetadataVOX.getAttribute(entry.getKey()) != null &&
					!globalVariables.entityMetadataVOX.getAttribute(entry.getKey()).isNonVersionTrackableFlag() &&
					!globalVariables.entityMetadataVOX.getStandardDoaFields().containsKey(entry.getKey()))
				nonStandardNextRecord.put(entry.getKey(), entry.getValue());
		}
		return Util.getChangedPreviousFields(nonStandardPreviousRecord, nonStandardNextRecord, globalVariables.entityMetadataVOX);
	}
	
	@Override
	public void validate(GlobalVariables globalVariables) throws Exception {
		Map<String, Object> data = globalVariables.insertData.getData();
        try{
        	String pkDOA = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode();
        	Object primaryKeyId = globalVariables.insertData.getData().get(pkDOA);
			String versionId = null;
			String versionDOA = null;
        	if(globalVariables.entityMetadataVOX.isVersioned()){
				versionDOA = globalVariables.entityMetadataVOX.getVersoinIdAttribute().getDoAttributeCode();
				versionId =(String) globalVariables.insertData.getData().get(versionDOA);
			}
        	
        	if(primaryKeyId != null){
        		// PRIMARY KEY check :
        		String columnName = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName();
        		if(!checkKeyExistenceInDb(globalVariables.entityMetadataVOX.getDbTableName(), columnName, primaryKeyId, globalVariables.entityMetadataVOX.getAttributeByName(pkDOA),globalVariables)) {//FIXME metaDataMap.getAttributeByColumnName(columnName)) not working!
					validationError.addError(pkDOA+" : "+primaryKeyId, "Primary Key provided is not present.");
				}
				if(globalVariables.entityMetadataVOX.isVersioned() && versionId != null){
					String versionColumnName = globalVariables.entityMetadataVOX.getVersoinIdAttribute().getDbColumnName();
					if(!checkKeyExistenceInDb(globalVariables.entityMetadataVOX.getDbTableName(), versionColumnName, versionId, globalVariables.entityMetadataVOX.getAttributeByName(versionDOA),globalVariables)){
						validationError.addError(versionDOA+" : "+versionId, "Version Id provided is not present.");
					}
				}
        		if(globalVariables.entityMetadataVOX.isVersioned()) {
					checkDuplicateEffectiveDatetime(globalVariables);
				}
        	}
			LinkedHashMap<String, EntityAttributeMetadata> uniqueFieldMap = new LinkedHashMap<>();
			LinkedHashMap<String, Object> uniqueKeyValueMap = new LinkedHashMap<>();
			Map<String, EntityAttributeMetadata> attributeMetadataMap = globalVariables.entityMetadataVOX.getAttributeNameMap();
        	for (Entry<String, EntityAttributeMetadata> attribute : attributeMetadataMap.entrySet()){
        		String attributeName = attribute.getKey();
        		Object attributeValue = data.get(attributeName);
        		EntityAttributeMetadata attributeProperty = attribute.getValue();
        		// DATA_TYPE Check :
				Map.Entry<String, Object> entry = new AbstractMap.SimpleEntry<String, Object>(attributeName, attributeValue);
        		Object typedValue = Util.convertStringToTypedValue(attributeProperty.getDbDataTypeCode(), entry);//fixme
//        		entry.setValue(typedValue); //FIXME check for the bug.
        		// MANDATORY Check :
        		if(attributeProperty.isMandatory() && (attributeValue == null || attributeValue.equals("")))
					validationError.addError("MANDATORY Check", "Attribute - '"+attributeName+"' is mandatory; it can't be null.");
        		//UNIQUE Attribute check :
				if(attributeProperty.isUniqueAttribute() && attributeValue != null && duplicateAttributeExist(attributeValue, attributeProperty, primaryKeyId, globalVariables)){
					validationError.addError("UNIQUE Attribute Check", "Field :"+ attributeProperty.getDoAttributeCode() + " for value "+ attributeValue +" is not unique. It must be unique.");
				}
        		// UNIQUE Record Check :
        		if(attributeProperty.isUniqueAttributeForRecord()){
					uniqueFieldMap.put(attributeName, attributeProperty);
					uniqueKeyValueMap.put(attributeName, attributeValue);
				}
        		// FOREIGN KEY check :
        		if(attributeProperty.isRelationFlag() && attributeValue != null){
        			String tableName = attributeProperty.getToDbTableName();
        			String columnName = attributeProperty.getToDbColumnName();
        			if(tableName != null && columnName != null){
        				if(!checkKeyExistenceInDb(tableName, columnName, attributeValue, attributeProperty, globalVariables)){
							validationError.addError("FOREIGN KEY check", "Foreign Key : " + attributeValue +" for "+attributeName+ " does not exist."); //FIXME uncomment when correct data entered.
        				}
        			} else {
						validationError.addError("FOREIGN KEY check", "ToDbTableName and ToDbColumnName can not be null. Check meta data for "+attributeName);
        			}
        		}
        		//G11N field modifications, should it be done from insertStatementBuilder?
				if (attributeProperty.isGlocalizedFlag() && attributeProperty.isAutoTranslationRequired()){

        			String modifiedG11nString = Util.getG11nStringToInsert((String) attributeValue, true);
        			data.put(attributeName, modifiedG11nString);
				}
        	}
    		// UNIQUE Record Check (Actual):
    		if(!uniqueFieldMap.isEmpty()){
				if(duplicateRecordExist(uniqueFieldMap, uniqueKeyValueMap, globalVariables, primaryKeyId)) {
					if(!globalVariables.setContext && !globalVariables.uniqueMerge)//FIXME always inserting tag bookmark. why?
						validationError.addError("UNIQUE Record Check", "Some fields in the record :" + uniqueFieldMap.entrySet() + " must be unique.");
				}
    		}
    		//CARDINALITY Check :
//    		if(currentActionStep.getPropertyCodeValueMap().containsKey("UPDATE_CARDINALITY") && currentActionStep.getPropertyCodeValueMap().get("UPDATE_CARDINALITY").equalsIgnoreCase("TRUE")){
//    			checkCardinalityForAttributes(globalVariables.entityMetadataVOX, data);
//    		}
        }catch(Exception e){
        	e.printStackTrace();
        }
		if (validationError.hasError()){
			validationError.logValidationErrors();
//			validationError.clear();
			throw new Exception();
		}
	}

	private boolean duplicateAttributeExist(Object attributeValue, EntityAttributeMetadata attributeProperty, Object primaryKeyValue, GlobalVariables globalVariables) {
		Deque<Object> arguments = new ArrayDeque<>();
		StringBuilder sql =  new StringBuilder();
		sql.append("SELECT COUNT(*) AS COUNT FROM ");
		sql.append(globalVariables.entityMetadataVOX.getDbTableName() + " WHERE ");
		if(attributeProperty.isGlocalizedFlag()){
			sql.append("COLUMN_GET("+attributeProperty.getDbColumnName()+", '"+globalVariables.primaryLocale+"' AS CHAR) ");
		}
		else if(attributeProperty.getContainerBlobDbColumnName() != null){
			sql.append("COLUMN_GET("+attributeProperty.getContainerBlobDbColumnName()+", '"+attributeProperty.getDoAttributeCode()+" AS "+
					Util.getBlobDataTypeFromEntityDataType(attributeProperty.getDbDataTypeCode()));
		}
		else {
			sql.append(attributeProperty.getDbColumnName());
		}
		arguments.push(Util.getStringTobuildWhereClauseAsParameter(attributeProperty, attributeValue, attributeProperty.getToDbColumnName(), globalVariables.primaryLocale));//FIXME check if the function is redundant; getStringToBeInserted could be used.
		sql.append(" = ? ");
		if(primaryKeyValue != null){
			EntityAttributeMetadata primaryAttribute = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
			sql.append("AND " + primaryAttribute.getDbColumnName() + " <> ?");
			arguments.push(Util.getStringTobeInserted(primaryAttribute, primaryKeyValue));
		}
		sql.append(" AND RECORD_STATE_CODE_FK_ID IN ('CURRENT', 'FUTURE') AND IS_DELETED = FALSE AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE';");
		LOG.debug("----------DUPLICATE_ATTRIBUTE_CHECK----------");
		SearchResult sr = searchDAO.search(sql.toString(), arguments);
		return (Long) sr.getData().get(0).get("COUNT") > 0;
	}

	private void checkCardinalityForAttributes(EntityMetadataVOX entityMetadataVOX, Map<String, Object> data) throws SQLException {
		Map<String, Object> filteredData = data.entrySet().stream()
											.filter(p -> entityMetadataVOX.getAttributeByName(p.getKey()).getBtDataTypeCode().equals("T_BLOB"))
											.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));									
		for(Entry<String, Object> field : filteredData.entrySet()){
			String attributeName = field.getKey();
    		Object attributeValue = field.getValue();
    		int count = 0;
    		EntityAttributeMetadata attributeProperty = entityMetadataVOX.getAttributeByName(attributeName);
			Map<String, Object> blobMap = (Map<String, Object>) attributeValue;
    		if(!attributeProperty.isCardinalityManyFlag()){
    			for(Entry<String, Object> entry : blobMap.entrySet()){
    				count++;
    				if(count > 1){
						validationError.addError("CARDINALITY Check", "This attribute can not have more than 1 values as its cardinality is not many. "+attributeName);
    					break;
    				}
    			}
    		}
//    		if(attributeProperty.getIsCardinalityManyToMany()){//FIXME uncomment when entitymetadata is correct.
    			//syntax : UPDATE JSON_TEST_TABLE SET VBLOB = COLUMN_ADD(VBLOB,'size', 36, 'color', 'white', 'null', 'null','timestamp', current_timestamp) WHERE a=b;
    			StringBuilder updateRelatedTableQuery = new StringBuilder();
    			updateRelatedTableQuery.append("UPDATE INTO " + attributeProperty.getToDbTableName() + " SET " + attributeProperty.getToDbColumnName());
    			updateRelatedTableQuery.append(" = COLUMN_ADD(" + attributeProperty.getToDbColumnName());
    			for(Entry<String, Object> entry : blobMap.entrySet()){
    				updateRelatedTableQuery.append(", '" + entry.getKey() + "'");
    				updateRelatedTableQuery.append(", '" + entry.getValue().toString() + "'");
    			}
    			updateRelatedTableQuery.append(") ; TO_DOMAIN_OBJECT_FK_ID = " + entityMetadataVOX.getDbTableName() + ";");
    			insertService.insertRow(updateRelatedTableQuery.toString());
    		}
		}
//	}

	private boolean checkKeyExistenceInDb(String tableName, String dbColumnName, Object keyValue, EntityAttributeMetadata attributeProperty, GlobalVariables globalVariables) {
		if (keyValue == null || keyValue.equals(""))
			return true;
		Object KeyValueToBeInserted =  Util.getStringTobuildWhereClauseAsParameter(attributeProperty, keyValue, dbColumnName, globalVariables.primaryLocale);
		StringBuilder sql =  new StringBuilder();
		String dataType = attributeProperty.getDbDataTypeCode();
		sql.append("SELECT EXISTS(SELECT 1 FROM " + tableName + "  WHERE ");
		if(dataType.matches(("T_ID|T_IMAGEID|AUDIO|VIDEO|T_ATTACH"))){
			if(attributeProperty.getContainerBlobDbColumnName() == null)
				sql.append(dbColumnName);
			else sql.append("COLUMN_GET("+attributeProperty.getContainerBlobDbColumnName()+", '"+attributeProperty.getDoAttributeCode()+"' AS "+Util.getBlobDataTypeFromEntityDataType(dataType));
			sql.append(" = ? ");
		}
		else{
			if(attributeProperty.getContainerBlobDbColumnName() == null)
				sql.append(dbColumnName);
			else sql.append("COLUMN_GET("+attributeProperty.getContainerBlobDbColumnName()+", '"+attributeProperty.getDoAttributeCode()+"' AS "+Util.getBlobDataTypeFromEntityDataType(dataType));
			sql.append(" = ? ");
		}
		sql.append("LIMIT 1) AS COUNT;");
		Deque<Object> arguments = new ArrayDeque<>();
		arguments.add(KeyValueToBeInserted);
		LOG.debug("----------FOREIGN_KEY_CHECK----------");
		SearchResult sr = searchDAO.search(sql.toString(), arguments);
		if(sr.getData().get(0).get("COUNT") != null && (Long) sr.getData().get(0).get("COUNT") > 0)
			return true;
		LOG.error("No entry found for this query -" + sql.toString() + "argument - "+ KeyValueToBeInserted);
		return false;
	}

	private boolean duplicateRecordExist(LinkedHashMap<String, EntityAttributeMetadata> uniqueFieldMap, LinkedHashMap<String, Object> uniqueKeyValueMap, GlobalVariables globalVariables, Object primaryKeyValue) {
		Deque<Object> arguments = new ArrayDeque<>();
		EntityAttributeMetadata primaryAttribute = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
		StringBuilder sql =  new StringBuilder();
		sql.append(Util.getSqlStringForTable(globalVariables.entityMetadataVOX) + " WHERE ");
		for(Entry<String, EntityAttributeMetadata> entry : uniqueFieldMap.entrySet()){
//			if(entry.getValue().isGlocalizedFlag()){
//				sql.append("COLUMN_GET("+entry.getValue().getDbColumnName()+", '"+globalVariables.primaryLocale+"' AS CHAR) ");
//			}
//			else
			if(entry.getValue().getContainerBlobDbColumnName() != null){
			sql.append("COLUMN_GET("+entry.getValue().getContainerBlobDbColumnName()+", '"+entry.getValue().getDoAttributeCode()+" AS "+
					Util.getBlobDataTypeFromEntityDataType(entry.getValue().getDbDataTypeCode()));//FIXME uniqueKeyValueMap.get(entry.getKey()) required somewhere.
			}
			else {
				sql.append(entry.getValue().getDbColumnName());
			}
			Object parameterTopush = null;
			if(entry.getValue().getDbDataTypeCode().equals("T_ID") && uniqueKeyValueMap.get(entry.getKey()) != null)
				parameterTopush = Util.convertStringIdToByteId(uniqueKeyValueMap.get(entry.getKey()).toString()) ;
			else parameterTopush = Util.getStringTobuildWhereClauseAsParameter(entry.getValue(), uniqueKeyValueMap.get(entry.getKey()), entry.getValue().getToDbColumnName(), globalVariables.primaryLocale);
			if(parameterTopush != null) {
				arguments.add(parameterTopush);//FIXME check if the function is redundant; getStringToBeInserted could be used.
				sql.append(" = ? AND ");
			}else {
				if (entry.getValue().getDbDataTypeCode().equals("BOOLEAN"))
					sql.append(" = FALSE AND ");
				else sql.append(" IS NULL AND ");
			}
		}
		sql.replace(sql.length()-4, sql.length(), "");
		if(primaryKeyValue != null && globalVariables.entityMetadataVOX.isVersioned()){
			sql.append("AND " + primaryAttribute.getDbColumnName() + " <> x?");
			arguments.add(Util.remove0x((String) primaryKeyValue));
			sql.append(" AND RECORD_STATE_CODE_FK_ID IN ('CURRENT', 'FUTURE') AND IS_DELETED = FALSE AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE';");
		}
		LOG.debug("----------DUPLICATE_RECORD_FOR_UNIQUE_ATTRIBUTE_CHECK----------");
		SearchResult sr = searchDAO.search(sql.toString(), arguments);
		if(sr.getData().size() > 0) {
			if (globalVariables.setContext) {//FIXME write in separate function
				globalVariables.isUpdate = true;
//				globalVariables.contextPK = sr.getData().get(0).get(primaryAttribute.getDoAttributeCode()).toString();
				Map<String, Object> insertData = globalVariables.insertData.getData();
				insertData.put(globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode(), Util.convertByteToString((byte[]) sr.getData().get(0).get(primaryAttribute.getDoAttributeCode())));
//				insertData.put(globalVariables.entityMetadataVOX.getAttributeByColumnName("IS_DELETED").getDoAttributeCode(), false); //FIXME same api might be used to delete record.
				String lastModifiedDatetimeDoa = globalVariables.entityMetadataVOX.getAttributeByColumnName("LAST_MODIFIED_DATETIME").getDoAttributeCode();
				insertData.put(lastModifiedDatetimeDoa, sr.getData().get(0).get(lastModifiedDatetimeDoa));
			}
			return true;
		}
		return false;
	}

	private String generateQuery(EntityMetadataVOX entityMetadataVOX, DefaultInsertData insertData, Map<String, Object> underlyingRecord, InsertContextModel model, GlobalVariables globalVariables) throws Exception {
		String query = null;
//		String currentServerRuleCode = currentActionStep.getServerRuleCodeFkId();
//		if(currentServerRuleCode.contains("DELETE")){// FIXME: fix for propogate and workflow and for SQL delete. This is SQL delete only. convert to PreparedStatement.
//			String whereExpression =  valueobject.getInsertEntityPK().getColumn() + "=" + "x'" + valueobject.getInsertEntityPK().getValue() + "'";
//			if(currentActionStep.equals(ServerRuleCode.DELETE_VERSION_RECORD.getCode())){
//				whereExpression = whereExpression + " AND VERSION_ID = x'" + insertData.getData().get("versionId") + "'";
//			}
//			valueobject.setWhereExpression(whereExpression);
//			DeleteStatementBuilder statementBuilder = new DeleteStatementBuilder(valueobject, entityMetadataVOX);
//			query = statementBuilder.build();
//			return query;
// 	    } else {
	        if (globalVariables.isUpdate) {
	        	{
		        	UpdateStatementBuilder statementBuilder = new UpdateStatementBuilder(model, entityMetadataVOX);
		            query = statementBuilder.build();
	        	}
	        } else {
	        	InsertStatementBuilder statementBuilder = new InsertStatementBuilder(model, entityMetadataVOX);
	            query = statementBuilder.build();
	        }
// 	    }
        return query;
	}

	private boolean checkDirtyUpdate(EntityMetadataVOX entityMetadataVOX, DefaultInsertData insertData, Map<String, Object> underlyingRecord) throws Exception {
		String doName = entityMetadataVOX.getDomainObjectCode();
		String pkColumnName = entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName().trim();
		EntityAttributeMetadata attribute = entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
		Object pkValue = insertData.getData().get(attribute.getDoAttributeCode());
		String versionColumnName = entityMetadataVOX.getVersoinIdAttribute().getDbColumnName().trim();//FIXME no need to sql query make use of underlying record.
		StringBuilder sql = new StringBuilder(Util.getSqlStringForTable(entityMetadataVOX) + " WHERE " + pkColumnName + " = "+Util.getStringTobeInserted(attribute, pkValue));
		Deque<Object> arguments = null;
		if(entityMetadataVOX.isVersioned()) {
			sql.append(" AND " + versionColumnName + " = ?;");
			arguments = new ArrayDeque<>();
			arguments.add(Util.convertStringIdToByteId((String)insertData.getData().get(entityMetadataVOX.getVersoinIdAttribute().getDoAttributeCode())));
		}
		LOG.debug("----------DIRTY_UPDATE_CHECK----------");
		LOG.debug(sql);
		if (underlyingRecord == null || underlyingRecord.isEmpty()) {
			SearchResult sr = searchDAO.search(sql.toString(), arguments);// required for deleted context records
			underlyingRecord = sr.getData().get(0);
		}
		Object lastModifiedDatetime= insertData.getData().get(entityMetadataVOX.getAttributeByColumnName("LAST_MODIFIED_DATETIME").getDoAttributeCode());
		if (lastModifiedDatetime == null){
			validationError.addError("CheckDirtyUpdates", "LastModifiedDatetime field is not present in "+entityMetadataVOX.getMtPE()+" : "+ pkValue);
		}
		else {
			if (!(lastModifiedDatetime instanceof Timestamp))
				lastModifiedDatetime = Timestamp.valueOf((String) lastModifiedDatetime);
			if (Util.getDatetimeUntilSeconds((Timestamp) lastModifiedDatetime).equals(Util.getDatetimeUntilSeconds((Timestamp) underlyingRecord.get(doName+".LastModifiedDateTime"))))
				return true;
			validationError.addError("CheckDirtyUpdates", "Data is dirty for "+entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode()+" : "+ pkValue);
		}
		if(validationError.hasError()){
			validationError.logValidationErrors();
//			validationError.clear();
			throw new Exception();
		}
		return false;
	}

	public InsertService getInsertService() {
		return insertService;
	}

	public void setInsertService(InsertService insertService) {
		this.insertService = insertService;
	}
	
	public SearchDAO getSearchDAO() {
		return searchDAO;
	}

	public void setSearchDAO(SearchDAO searchDAO) {
		this.searchDAO = searchDAO;
	}

	public DataService getDataService() {
		return dataService;
	}

	public void setDataService(DataService dataService) {
		this.dataService = dataService;
	}

	public WorkflowProvider getWorkflowProvider() {
		return workflowProvider;
	}

	public void setWorkflowProvider(WorkflowProvider workflowProvider) {
		this.workflowProvider = workflowProvider;
	}

	public WorkflowExecutor getWorkflowExecutor() {
		return workflowExecutor;
	}

	public void setWorkflowExecutor(WorkflowExecutor workflowExecutor) {
		this.workflowExecutor = workflowExecutor;
	}

	public AccessManagerService getAccessManagerService() {
		return accessManagerService;
	}

	public void setAccessManagerService(AccessManagerService accessManagerService) {
		this.accessManagerService = accessManagerService;
	}

	public ValidationError getValidationError() {
		return validationError;
	}

	public void setValidationError(ValidationError validationError) {
		this.validationError = validationError;
	}

	@Override
	public void operateOnUnderlyingRecord(GlobalVariables globalVariables) throws Exception {
		String sqlStatement = null;

	    try{
//			setUnderlyingRelatedDataFromDb(globalVariables);//before valueobject creation as valueobject always contain pk and version. //FIXME should move to globalVariables.
			if(globalVariables.setContext){
				if (globalVariables.context.equalsIgnoreCase("Tag") && globalVariables.personTag != null){
					List<PersonTagsVO> personTagsVOS = addNewTagToPersonTagMaster(globalVariables);
					globalVariables.personTagsList = personTagsVOS;
				}
				AddContextualFields(globalVariables);
				validate(globalVariables);
			}
			SQLContextBuilder contextBuilder = new InsertContextBuilder();
			globalVariables.model = (InsertContextModel) contextBuilder.build(globalVariables);
			modelValidationCheck(globalVariables);
			sqlStatement = generateQuery(globalVariables.entityMetadataVOX, globalVariables.insertData, globalVariables.underlyingRecord, globalVariables.model, globalVariables);

			insertService.insertRow(sqlStatement);
//			if (globalVariables.entityMetadataVOX.isVersioned()) {
//				insertService.CalculateAndUpdateRecordState(globalVariables.entityMetadataVOX, globalVariables.model, globalVariables);//Is it right?
//			}
//			updatePermissions(globalVariables);
			if (globalVariables.setContext && globalVariables.context.equals("Featured"))
				updateFeaturedAttributeForRecord(globalVariables);
			if (globalVariables.isSubmit && globalVariables.makePrimaryUnique)
				updateForMakePrimary(globalVariables);
			if (globalVariables.entityMetadataVOX.getInlineChangeBlobExist())
				updateInlineChangesBlob(globalVariables);
			if (globalVariables.isSubmit && globalVariables.logActivity)
				updateActivityLog(globalVariables);
			if (globalVariables.isSubmit && !globalVariables.setContext) {
				updateHierarchyIfRequired(globalVariables);
//				updatePermissions(globalVariables);
				resolveGroupsIfRequired(globalVariables);
			}
			setUnderlyingRelatedDataFromDb(globalVariables);

 			if(globalVariables.entityMetadataVOX.isVersioned() && globalVariables.isSubmit) {
				updateWorkflowDetails(globalVariables);
				insertService.CalculateAndUpdateRecordState(globalVariables.entityMetadataVOX, globalVariables.model, globalVariables);// ensure that its called only when update is happening and _DONE.
		    	updateVersionDiffRecords(globalVariables);//ensure before or after record state calculation. and check when to call it.
				updatePropagateChanges(globalVariables);
				executePropogateChanges(globalVariables);
			}
	    }catch (Exception e) {
	    	e.printStackTrace();
			validationError.logValidationErrors();
	    	LOG.error("operateOnUnderlyingRecord Failed to create entity {} with process element Id {}",
	    		   globalVariables.insertData.getProcessElementId(), globalVariables.insertData.getProcessElementId());
	    	LOG.error(sqlStatement);
	    	throw e;
	    }
	}

	private void resolveGroupsIfRequired(GlobalVariables globalVariables) throws IOException, SQLException {
		groupResolverService.resolveGroupByMtPE(globalVariables.mtPE);
	}

	private void updateHierarchyIfRequired(GlobalVariables globalVariables) throws SQLException {
	 	if (globalVariables.entityMetadataVOX.getMtPE().matches("PersonJobAssignment|PersonJobRelationship")){
	 		if (globalVariables.changedFields.containsKey("PersonJobRelationship.RelatedPerson")){
				Map<String, Object> underlyingRecord = globalVariables.underlyingRecord;
				String rootRelatedPerson = null;
				String hierarchyId = (String) underlyingRecord.get("PersonJobRelationship.InternalHierarchyID");
				if (!underlyingRecord.get("PersonJobRelationship.InternalLeft").equals(1)) {
					rootRelatedPerson = getPersonIdForRootPerson(hierarchyId);
				}
				else rootRelatedPerson = (String) underlyingRecord.get("PersonJobRelationship.RelatedPerson");
				hierarchyService.updateLeftRightAndHierachyValuesForPerson(hierarchyId, rootRelatedPerson, 1);
			}
		}
	}

	private String getPersonIdForRootPerson(String hierarchyId) {
	 	return hierarchyService.getPersonIdForRootPerson(hierarchyId);
	}

	private void updateFeaturedAttributeForRecord(GlobalVariables globalVariables) throws SQLException {
		String contextMtPE = globalVariables.context;
		String mtPE = globalVariables.mtPE;
		MasterEntityMetadata entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(mtPE);
		String tableName = entityMetadataVOX.getDbTableName();
		String pkColumnName = null;
		for (MasterEntityAttributeMetadata attributeMetadata : entityMetadataVOX.getAttributeMeta()) {
			if (attributeMetadata.getDoAttributeCode().endsWith(".PrimaryKeyID")) {
				pkColumnName = attributeMetadata.getDbColumnName();
				break;
			}
		}
		Map<String, Object> data = globalVariables.insertData.getData();
		String doPrimaryKey = (String) data.get(contextMtPE+".DOPrimaryKey");
		String sql = "UPDATE "+tableName+" SET IS_FEATURED = (SELECT EXISTS(SELECT * FROM T_PFM_EU_FEATURED WHERE DO_ROW_PRIMARY_KEY_FK_ID = ? AND MT_PE_CODE_FK_ID = ?)) WHERE "+pkColumnName+" = ? ;";
		Deque<Object> parameters = new ArrayDeque<>();
		parameters.add(Util.convertStringIdToByteId(doPrimaryKey));
		parameters.add(mtPE);
		parameters.add(Util.convertStringIdToByteId(doPrimaryKey));
		insertService.insertUpdateRow(sql, parameters);
	}

	private void updateForMakePrimary(GlobalVariables globalVariables) throws SQLException {
	 	Map<String, Object> data = globalVariables.insertData.getData();
	 	String tableName = globalVariables.entityMetadataVOX.getDbTableName();
	 	String doName = globalVariables.entityMetadataVOX.getDomainObjectCode();
	 	String pkColumn = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName();
	 	String pkDoa = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode();
	 	EntityAttributeMetadata primaryCheckColumn = globalVariables.entityMetadataVOX.getPrimaryCheckerColumn();
	 	if (globalVariables.compareKeyAttribute != null) {
			String compareKey = globalVariables.compareKeyAttribute.getDoAttributeCode();
			String compareKeyColumn = globalVariables.entityMetadataVOX.getCompareKeyAttribute().getDbColumnName();
			String compareKeyValue = (String) data.get(compareKey);
//			if (StringUtil.isDefined(globalVariables.compareKey)) {
//				String sql = "UPDATE " + tableName + " SET " + primaryCheckColumn.getDbColumnName() + " =  FALSE WHERE " + pkColumn + " <> " + data.get(pkDoa) + " AND " + compareKeyColumn + " = " + compareKeyValue + " AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND IS_DELETED = FALSE;";//fixme add ACTIVE_STATE also here
				/* same as selecting the records where pk != given pk and compare key = given key and AND SAVE_STATE_CODE_FK_ID = 'SAVED' IS_DELETED = FALSE */
//				int recordsUpdated = insertService.insertRow(sql);
//				sql = "SELECT COUNT(*) AS COUNT FROM "+tableName+" WHERE "+primaryCheckColumn.getDbColumnName() + " = TRUE AND " + pkColumn + " <> " + data.get(pkDoa) + " AND " + compareKeyColumn + " = " + compareKeyValue + " AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND IS_DELETED = FALSE;";
//				SearchResult sr = searchDAO.search(sql, null);
//				if (/*!(boolean)data.get(doName+".Primary") &&*/ (long)sr.getData().get(0).get("COUNT") == 0){
//					String makeForcedPrimarySql = "UPDATE " + tableName + " SET " + primaryCheckColumn.getDbColumnName() + " = TRUE WHERE " + pkColumn + " = " + data.get(pkDoa) + ";";
//					insertService.insertRow(makeForcedPrimarySql);

			String sql = "SELECT COUNT(*) AS COUNT FROM "+tableName+" WHERE "+primaryCheckColumn.getDbColumnName() + " = TRUE AND " +
					compareKeyColumn + " = " + compareKeyValue + " AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND IS_DELETED = FALSE;";
			SearchResult sr = searchDAO.search(sql, null);
			if (((long)sr.getData().get(0).get("COUNT")) == 0){
				String makeForcedPrimarySql = "UPDATE " + tableName + " SET " + primaryCheckColumn.getDbColumnName() + " = TRUE WHERE " + pkColumn + " = " + data.get(pkDoa) + ";";
				insertService.insertRow(makeForcedPrimarySql);
			}
			else if (((long)sr.getData().get(0).get("COUNT")) > 1){// will always be 2
				String makePrimaryUniqueSql = "UPDATE " + tableName + " SET " + primaryCheckColumn.getDbColumnName() + " =  FALSE WHERE " + pkColumn + " <> " + data.get(pkDoa) + " AND " +
						compareKeyColumn + " = " + compareKeyValue + " AND SAVE_STATE_CODE_FK_ID = 'SAVED'  AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND IS_DELETED = FALSE;";
				insertService.insertRow(makePrimaryUniqueSql);
			}
		}
//
	}

	private void updatePermissions(GlobalVariables globalVariables) throws SQLException {
	 	if (globalVariables.entityMetadataVOX.isDoAccessControlGovernedFlag())
	 		accessManagerService.updatePermissionsForData(globalVariables);
	}

	private List<PersonTagsVO> addNewTagToPersonTagMaster(GlobalVariables globalVariables) throws SQLException {
		PersonTagsVO personTagsVO = globalVariables.personTag;
	 	String loggedInPerson = TenantContextHolder.getUserContext().getPersonId();
		personTagsVO.setPersonTagsPkId((Common.getUUID()).toString());
		personTagsVO.setLoggedInUserFkId(loggedInPerson);
		insertIntoPersonTagMaster(personTagsVO, globalVariables);
		globalVariables.insertData.getData().put("Tag.PersonTagMasterID", personTagsVO.getPersonTagsPkId());
		List<PersonTagsVO> personTagsVOs = getPersonTagsByPersonId(loggedInPerson);
		return personTagsVOs;
	}

	private List<PersonTagsVO> getPersonTagsByPersonId(String loggedInPerson) {
	 	List<PersonTagsVO> personTagsVOs = null;
		String sql = "SELECT * FROM T_PFM_IEU_PERSON_TAG_MASTER WHERE LOGGED_IN_USER_FK_ID = "+loggedInPerson+";";
		SearchResult sr = searchDAO.search(sql.toString(), null);
		if(sr.getData().size() > 0) {
			personTagsVOs = new ArrayList<>();
			PersonTagsVO personTagsVO = null;
			for (Map<String, Object> ptm : sr.getData()) {
				personTagsVO = new PersonTagsVO();
				personTagsVO.setPersonTagsPkId(Util.convertByteToString((byte[])ptm.get("PERSON_TAG_MASTER_PK_ID")));
				personTagsVO.setLoggedInUserFkId(ptm.get("LOGGED_IN_USER_FK_ID").toString());
				personTagsVO.setTagTxt(ptm.get("TAG_TXT").toString());
				personTagsVO.setBackgroundColourTxt(ptm.get("BACKGROUND_COLOUR_TXT").toString());
				personTagsVO.setFontColourTxt(ptm.get("FONT_COLOUR_TXT").toString());
				personTagsVO.setLastModifiedDateTime(ptm.get("LAST_MODIFIED_DATETIME").toString());
				personTagsVOs.add(personTagsVO);
			}
		}
		return personTagsVOs;
	}

	private void insertIntoPersonTagMaster(PersonTagsVO personTagsVO, GlobalVariables globalVariables) throws SQLException {
		StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_PERSON_TAG_MASTER (PERSON_TAG_MASTER_PK_ID, LOGGED_IN_USER_FK_ID, TAG_TXT, BACKGROUND_COLOUR_TXT, " +
				"FONT_COLOUR_TXT, IS_DELETED, CREATED_DATETIME, LAST_MODIFIED_DATETIME) VALUES (");
		sql.append(personTagsVO.getPersonTagsPkId()+", ");
		sql.append(personTagsVO.getLoggedInUserFkId()+", ");
		sql.append("'"+personTagsVO.getTagTxt()+"', ");
		sql.append("'"+personTagsVO.getBackgroundColourTxt()+"', ");
		sql.append("'"+personTagsVO.getFontColourTxt()+"', ");
		sql.append("FALSE, ");
		sql.append("'"+globalVariables.currentTimestampString+"', ");
		sql.append("'"+globalVariables.currentTimestampString+"');");
		insertService.insertRow(sql.toString());
	}

	private void modelValidationCheck(GlobalVariables globalVariables) throws Exception {
		if (globalVariables.isUpdate){
			if(!checkDirtyUpdate(globalVariables.entityMetadataVOX, globalVariables.insertData, globalVariables.underlyingRecord))
				return;
		}
		if(globalVariables.entityMetadataVOX.isVersioned() && globalVariables.model.getInsertValueMap().get("SAVE_STATE_CODE_FK_ID").equals("SAVED")){
			checkDuplicateEffectiveDatetime(globalVariables);
			if(validationError.hasError()){
				validationError.logValidationErrors();
				throw new Exception();
			}
		}

//		if(globalVariables.entityMetadataVOX.isDoAccessControlGovernedFlag() && globalVariables.isSubmit/*&& (globalVariables.isSubmit || (globalVariables.underlyingRecord != null && !globalVariables.underlyingRecord.isEmpty() && globalVariables.underlyingRecord.get(globalVariables.entityMetadataVOX.getSaveStateAttribute().getDoAttributeCode()).equals("SAVED")))*/){
//			Permission permission = accessManagerService.getPermissionsForData(globalVariables.insertData.getData(), globalVariables.mtPE, globalVariables.entityMetadataVOX);
//			checkForPermissions(globalVariables, permission);
//			permission.setAttributePermission(permission.getAttributePermission().entrySet().stream().filter(p -> p.getValue() != null).collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue())));
//			globalVariables.permission = permission;
//			permission.getAttributePermission();
//		}
	}

//	private void checkForPermissions(GlobalVariables globalVariables, Permission permission) throws Exception {
//	 	Map<String, Object> data = globalVariables.insertData.getData();
//	 	Map<String, Object> underlyingRecord = globalVariables.underlyingRecord;
//	 	EntityMetadataVOX entityMetadataVOX = globalVariables.entityMetadataVOX;
//	 	Map<String, DOAPermission> doaPermissionMap = permission.getAttributePermission();
//		String action = null;
//	 	if (underlyingRecord == null){
//	 		if (globalVariables.recordState == null) action = "CREATE";
//	 		else if (globalVariables.recordState.equals("CURRENT")) action = "CREATE_CURRENT";
//	 		else if (globalVariables.recordState.equals("HISTORY")) action = "CREATE_HISTORY";
//	 		else if (globalVariables.recordState.equals("FUTURE")) action = "CREATE_FUTURE";
//			checkForPermissionsByAction(data, doaPermissionMap, action);
//		}else {
//	 		Map<String, Object> changedFields = Util.getChangedCurrentFields(underlyingRecord, data, entityMetadataVOX);
//			if (globalVariables.recordState == null) action = "UPDATE";
//			else if (globalVariables.recordState.equals("CURRENT")) action = "UPDATE_CURRENT";
//			else if (globalVariables.recordState.equals("HISTORY")) action = "UPDATE_HISTORY";
//			else if (globalVariables.recordState.equals("FUTURE")) action = "UPDATE_FUTURE";
//			if (changedFields != null && !changedFields.isEmpty()){
//				checkForPermissionsByAction(changedFields, doaPermissionMap, action);
//			}
//		}
//	}

	private void checkForPermissionsByAction(Map<String, Object> data, Map<String, DOAPermission> doaPermissionMap, String action) throws Exception {
	 	switch (action){
			case "CREATE":
			for (Entry< String, Object> entry : data.entrySet()){
				if(doaPermissionMap.get(entry.getKey()).getCreateCurrentPermitted() != true){
					validationError.addError("DOAPermission", "Creating a record with DOA : "+entry.getKey()+" is not permitted.");
				}
			}
			break;
			case "CREATE_CURRENT":
				for (Entry< String, Object> entry : data.entrySet()){
					if(doaPermissionMap.get(entry.getKey()).getCreateCurrentPermitted() != true){
						validationError.addError("DOAPermission", "Creating a current record with DOA : "+entry.getKey()+" is not permitted.");
					}
				}
				break;
			case "CREATE_HISTORY":
				for (Entry< String, Object> entry : data.entrySet()){
					if(doaPermissionMap.get(entry.getKey()).getCreateHistoryPermitted() != true){
						validationError.addError("DOAPermission", "Creating a history record with DOA : "+entry.getKey()+" is not permitted.");
					}
				}
				break;
			case "CREATE_FUTURE":
				for (Entry< String, Object> entry : data.entrySet()){
					if(doaPermissionMap.get(entry.getKey()).getCreateFuturePermitted() != true){
						validationError.addError("DOAPermission", "Creating a future record with DOA : "+entry.getKey()+" is not permitted.");
					}
				}
				break;
			case "UPDATE":
				for (Entry< String, Object> entry : data.entrySet()){
					if(doaPermissionMap.get(entry.getKey()).getUpdateCurrentPermitted() != true){
						validationError.addError("DOAPermission", "Updating DOA : "+entry.getKey()+" is not permitted.");
					}
				}
				break;
			case "UPDATE_CURRENT":
				for (Entry< String, Object> entry : data.entrySet()){
					if(doaPermissionMap.get(entry.getKey()).getUpdateCurrentPermitted() != true){
						validationError.addError("DOAPermission", "Updating DOA : "+entry.getKey()+" for current record is not permitted.");
					}
				}
				break;
			case "UPDATE_HISTORY":
				for (Entry< String, Object> entry : data.entrySet()){
					if(doaPermissionMap.get(entry.getKey()).getUpdateHistoryPermitted() != true){
						validationError.addError("DOAPermission", "Updating DOA : "+entry.getKey()+" for history record is not permitted.");
					}
				}
				break;
			case "UPDATE_FUTURE":
				for (Entry< String, Object> entry : data.entrySet()){
					if(doaPermissionMap.get(entry.getKey()).getUpdateFuturePermitted() != true){
						validationError.addError("DOAPermission", "Updating DOA : "+entry.getKey()+" for future record is not permitted.");
					}
				}
				break;
		}
		if(validationError.hasError()){
			throw new Exception();
		}
	}

	private void updateInlineChangesBlob(GlobalVariables globalVariables) throws SQLException {
	 	Map<String, Object> oldValueRecord = null;
	 	if(globalVariables.isUpdate)
	 		oldValueRecord = globalVariables.underlyingRecord;
	 	else oldValueRecord = globalVariables.insertData.getData();
	 	InsertEntityPK pkEntity = globalVariables.model.getInsertEntityPK();
	 	InsertEntityVersion versionEntity = null;
	 	if(globalVariables.entityMetadataVOX.isVersioned())
			versionEntity = globalVariables.model.getInsertEntityVersion();
	 	String updateSql = buildQueryForInLineChanges(oldValueRecord, pkEntity, versionEntity, globalVariables.entityMetadataVOX);
	 	insertService.insertRow(updateSql);
	}

	private String buildQueryForInLineChanges(Map<String, Object> oldValueRecord, InsertEntityPK pkEntity, InsertEntityVersion versionEntity, EntityMetadataVOX entityMetadataVOX) {
		Map<String, EntityAttributeMetadata> attributeMetadataMap = entityMetadataVOX.getAttributeNameMap();
	 	StringBuilder inLineDiffQuery = new StringBuilder();
		inLineDiffQuery.append("UPDATE " + entityMetadataVOX.getDbTableName() + " SET INLINE_CHANGES_BLOB = COLUMN_CREATE(");
		for(Entry<String, Object> field : oldValueRecord.entrySet()){
			inLineDiffQuery.append("'" + field.getKey() + "'");
			String attributeDataType = attributeMetadataMap.get(field.getKey()).getDbDataTypeCode();
			inLineDiffQuery.append(", " + Util.getStringTobeInserted(attributeDataType, field.getValue(), true) + ", ");
		}
		inLineDiffQuery.replace(inLineDiffQuery.length()-2, inLineDiffQuery.length()-1, "");
		Object pkValue = pkEntity.getValue();
		String pkColumnName = pkEntity.getColumn();
		String pkName = pkEntity.getDoaName();
		MasterEntityAttributeMetadata pkMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(pkName);
		inLineDiffQuery.append(") WHERE " + pkColumnName + " = "+Util.getStringTobeInserted(pkMeta.getDbDataTypeCode(), pkValue, false));
		if (entityMetadataVOX.isVersioned())
			inLineDiffQuery.append(" AND " + versionEntity.getColumn() + " = x'" + versionEntity.getValue() + "' ;");
		return inLineDiffQuery.toString();
	}

	private void updateActivityLog(GlobalVariables globalVariables) throws SQLException{
	 	EntityMetadataVOX entityMetadataVOX = globalVariables.entityMetadataVOX;
		Map<String, Object> previousRecord = (globalVariables.underlyingRecord == null || globalVariables.underlyingRecord.isEmpty()) ? null : globalVariables.underlyingRecord;
		Map<String, Object> currentRecord = globalVariables.insertData.getData();
		Map<String, Object> changedFields = null;
		if(previousRecord != null && currentRecord != null &&
				!(globalVariables.actionVisualMasterCode.equals("ADD") ||
						(globalVariables.actionVisualMasterCode.equals("EDIT") && !globalVariables.isInline))){
			changedFields = globalVariables.changedFields;//changedFields = getNonStandardChangedCurrentFields(previousRecord, currentRecord, globalVariables);
		}else if(currentRecord != null){
			changedFields = Util.getNonStandardNotNullFields(currentRecord, globalVariables.entityMetadataVOX);//why not null? find and null can't come from android!
		}
		if(changedFields != null && !changedFields.isEmpty()){
			ActivityLog activityLog = new ActivityLog();
			activityLog.setDoNameG11nBigTxt(entityMetadataVOX.getBtDOG11nName());
			activityLog.setProcessTypeCodeFkId(entityMetadataVOX.getProcessCode());
			if (entityMetadataVOX.getDoSummaryTitleIcon() != null)
				activityLog.setDoIconCodeFkId(entityMetadataVOX.getDoSummaryTitleIcon().getCode());
			activityLog.setDoRowPrimaryKeyFkId(currentRecord.get(entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode()).toString());
			if(entityMetadataVOX.isVersioned())
				activityLog.setDoRowVersionFkId(currentRecord.get(entityMetadataVOX.getVersoinIdAttribute().getDoAttributeCode()).toString());
			if(entityMetadataVOX.getIsContextObjectField() != null) {
				String displayContextValue = (String) currentRecord.get(entityMetadataVOX.getIsContextObjectField().getDoAttributeCode());
				if (entityMetadataVOX.getIsContextObjectField().isGlocalizedFlag())
					activityLog.setDisplayContextValueBigTxt(displayContextValue);
				else activityLog.setDisplayContextValueBigTxt(Util.getDummyG11nStringForInstanceLocal(displayContextValue));
			}
			activityLog.setActionVisualMasterCode(globalVariables.actionVisualMasterCode);
			activityLog.setActivityDatetime(globalVariables.currentTimestamp);
			activityLog.setMtPeCodeFkId(globalVariables.mtPE);
			String personId = TenantContextHolder.getUserContext().getPersonId();
			activityLog.setActivityAuthorPerson(TenantContextHolder.getUserContext().getPersonId());
			activityLog.setDeleted(false);
			activityLog.setActivityLogPkId((String) Util.remove0x(Common.getUUID()));
			String logActivityMTPE = globalVariables.masterEntityMetadataVOX.getActivityPE();
			String logActivityDtlMTPE = globalVariables.masterEntityMetadataVOX.getActivityDetailsPE();
			String btActivityLog = TenantAwareCache.getPersonRepository().getDefaultBTForGroup(logActivityMTPE, personId);
			String btActivityLogDtl = TenantAwareCache.getPersonRepository().getDefaultBTForGroup(logActivityDtlMTPE, personId);
			EntityMetadataVOX activityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(btActivityLog, logActivityMTPE, null);
			EntityMetadataVOX activityDtlMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(btActivityLogDtl, logActivityDtlMTPE, null);
			Map<String, Object> doaValueMap = globalVariables.insertData.getData();
			Map<String, Object> contextDoaValueMap = Util.buildContextDoaValueMap(doaValueMap, activityLog, activityMetadataVOX, globalVariables.currentTimestampString);
			AuditMsgFormat auditMsgFormat = getAuditMsgFormat(globalVariables);
			Map<String, String> expressionToValueMap = getExpressionToValueMap(contextDoaValueMap, auditMsgFormat);
			changedFields = modifyChangedFieldsForActivityLog(changedFields, globalVariables, expressionToValueMap, contextDoaValueMap);
			populateExpressionToValueMapWithStaticValues(expressionToValueMap, globalVariables);
			Util.buildActivityLogMsg(activityLog, auditMsgFormat, expressionToValueMap);
			Map<String, ActivityLogDtl> activityLogDtlMap = constructActivityLogDetailMap(activityLog.getActivityLogPkId(), changedFields, globalVariables);
			activityLog.setActivityLogDtlMap(activityLogDtlMap);
			boolean isCreateDetails = auditMsgFormat.isCreateDetails();
			insertService.insertActivityLog(activityLog, activityMetadataVOX, activityDtlMetadataVOX, isCreateDetails);
		}
	}

	private Map<String, Object> modifyChangedFieldsForActivityLog(Map<String, Object> changedFields, GlobalVariables globalVariables, Map<String, String> expressionToValueMap, Map<String, Object> contextDoaValueMap) {
		Map<String, Object> modifiedChangedFields = new HashMap<>();
		for (Entry<String, Object> entry : changedFields.entrySet()){
			if (entry.getValue() != null && !entry.getValue().equals("")) {//for no relation attr path be existing this
				EntityAttributeMetadata fieldMeta = globalVariables.entityMetadataVOX.getAttributeByName(entry.getKey());
				String attributePathExpn = fieldMeta.getAuditAttributePathExpn();
				String attrPathExpnFormatted = attributePathExpn.replaceAll("[!{|}]", "");
				if (!entry.getKey().equals(attrPathExpnFormatted)) {
					populateExpressionToValueMap(attributePathExpn, expressionToValueMap, contextDoaValueMap);
					String relationValue = expressionToValueMap.get(attrPathExpnFormatted);
					if (attrPathExpnFormatted.contains(":")) {
						String[] strings = attrPathExpnFormatted.split(":", 2);
						modifiedChangedFields.put(attrPathExpnFormatted, relationValue);
					}
				} else modifiedChangedFields.put(entry.getKey(), entry.getValue());
			}
		}
		return modifiedChangedFields;
	}

	private Map<String,String> getExpressionToValueMap(Map<String, Object> contextDoaValueMap, AuditMsgFormat auditMsgFormat) {
		String msgYouInContext = auditMsgFormat.getMsgSyntaxYouInContextG11nBigTxt();
		String msgYouOutContext = auditMsgFormat.getMsgSyntaxYouOutOfContextG11nBigTxt();
		String msgSyntaxThirdPersonInContext = auditMsgFormat.getMsgSyntaxThirdPersonInContextG11nBigTxt();
		String msgSyntaxThirdPersonOutOfContext = auditMsgFormat.getMsgSyntaxThirdPersonOutOfContextG11nBigTxt();

		Map<String, String> expressionToValueMap = new HashMap<>();
		populateExpressionToValueMap(msgYouInContext, expressionToValueMap, contextDoaValueMap);
		populateExpressionToValueMap(msgYouOutContext, expressionToValueMap, contextDoaValueMap);
		populateExpressionToValueMap(msgSyntaxThirdPersonInContext, expressionToValueMap, contextDoaValueMap);
		populateExpressionToValueMap(msgSyntaxThirdPersonOutOfContext, expressionToValueMap, contextDoaValueMap);
		return expressionToValueMap;
	}

	private void populateExpressionToValueMapWithStaticValues(Map<String, String> expressionToValueMap, GlobalVariables globalVariables) {
		expressionToValueMap.put("${BTPEName}", globalVariables.btPE);
	}

	private void populateExpressionToValueMap(String expression, Map<String, String> expressionToValueMap, Map<String, Object> contextDoaValueMap) {
	 	if (expression == null)
	 		return;
		Pattern doPattern = Pattern.compile("\\!\\{(.*?)\\}");
		Matcher doMatcher = doPattern.matcher(expression);
		while (doMatcher.find()) {
			String doaName = doMatcher.group(1);
			Map<String, String> doaValueMap = new HashMap<>();
			List<String> attributeList = null;
			if (!expressionToValueMap.containsKey(doaName)) {
				if (doaName.contains(":")) {
					String[] strings = doaName.split(":", 2);
					String pk = contextDoaValueMap.get(strings[0]).toString();
					String keyDoa = "!{" + strings[1] + "}";
					attributeList = new ArrayList<>();
					attributeList.add(keyDoa);
					UnitResponseData response;
//					if (keyDoa.contains("Person."))
//						response = dataService.getAttributePathData(attributeList, pk,false, false);
//					else
						response = dataService.getAttributePathData(attributeList, pk,false, false);
					Map<String, AttributeData> attributeMap = response.getPEData().values().iterator().next().getRecordSetInternal().get(0).getAttributeMap();
					DoaMeta key = response.getLabelToAliasMap().get(keyDoa);
					expressionToValueMap.put(doaName,(String) attributeMap.get(key.getAlias()).getcV());
				} else {
					expressionToValueMap.put(doaName,(String) contextDoaValueMap.get(doaName));
				}
			}
		}
	}

	private AuditMsgFormat getAuditMsgFormat(GlobalVariables globalVariables) {
//	 	String activityLogActionCode = null;
//		if(globalVariables.actionCode.equals("ADD_EDIT")) {
//			if (globalVariables.entityMetadataVOX.isVersioned()) {
//				//Fixme wirte for versioned table select based on the existence of the action in table.
//				if (globalVariables.isUpdate)
//					activityLogActionCode = "EDIT";
//				else activityLogActionCode = "ADD";
//			} else {
//				if (globalVariables.isUpdate && !globalVariables.setContext)//tag may edit(->restore) but msg will be only for ADD
//					activityLogActionCode = "EDIT";
//				else activityLogActionCode = "ADD";
//			}
//		}
//		else activityLogActionCode = globalVariables.actionCode;
		AuditMsgFormat auditMsgFormat = null;
		if (globalVariables.setContext)
			auditMsgFormat = TenantAwareCache.getMetaDataRepository().getAuditMsgFormat(globalVariables.actionVisualMasterCode, globalVariables.context);
		else auditMsgFormat = TenantAwareCache.getMetaDataRepository().getAuditMsgFormat(globalVariables.actionVisualMasterCode, globalVariables.mtPE);
		return auditMsgFormat;
	}


	private Map<String,ActivityLogDtl> constructActivityLogDetailMap(String activityLogPkId, Map<String, Object> changedFields, GlobalVariables globalVariables) {
		Map<String, ActivityLogDtl> activityLogDtlMap = new HashMap<>();
	 	for (Entry<String, Object> changedField : changedFields.entrySet()){
			ActivityLogDtl activityLogDtl = new ActivityLogDtl();
			activityLogDtl.setActivityLogDtlPkId(Common.getUUID());
			activityLogDtl.setActivityLogFkId(activityLogPkId);
			activityLogDtl.setMtDoaCodeFkId(changedField.getKey());
			if (changedField.getKey().startsWith(globalVariables.domainObject))
				activityLogDtl.setNearLabel(true);
			MasterEntityAttributeMetadata attributeMeta = null;
	 		if (changedField.getKey().contains(":")) {
				String farAttribute = changedField.getKey().split(":")[1];
				attributeMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(farAttribute);
			}
			else {
	 			attributeMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(changedField.getKey());
			}
			if (attributeMeta.isGlocalized()) {
				String g11nValue = Util.getStringTobeInsertedForMasterMeta(attributeMeta, changedField.getValue());
				g11nValue = g11nValue.substring(1, g11nValue.length() - 1);// for removing begin and end quote(')
				activityLogDtl.setNewValueG11nBigTxt(g11nValue);
			} else
				activityLogDtl.setNewValueG11nBigTxt(Util.getDummyG11nStringForInstanceLocal(changedField.getValue()));
			activityLogDtlMap.put(changedField.getKey(), activityLogDtl);
			}
		return activityLogDtlMap;
	}

	private Map<String,Object> getNonStandardChangedCurrentFields(Map<String, Object> previousRecord, Map<String, Object> currentRecord, GlobalVariables globalVariables) throws ParseException {
		Map<String, Object> nonStandardPreviousRecord = new HashMap<>();
		Map<String, Object> nonStandardNextRecord = new HashMap<>();
		for(Entry<String, Object> entry : previousRecord.entrySet()){
			if(globalVariables.entityMetadataVOX.getAttribute(entry.getKey()) != null &&
					!globalVariables.entityMetadataVOX.getAttribute(entry.getKey()).isNonVersionTrackableFlag() &&
					!globalVariables.entityMetadataVOX.getStandardDoaFields().containsKey(entry.getKey()))
				nonStandardPreviousRecord.put(entry.getKey(), entry.getValue());
		}
		for(Entry<String, Object> entry : currentRecord.entrySet()){
			if(globalVariables.entityMetadataVOX.getAttribute(entry.getKey()) != null &&
					!globalVariables.entityMetadataVOX.getAttribute(entry.getKey()).isNonVersionTrackableFlag() &&
					!globalVariables.entityMetadataVOX.getStandardDoaFields().containsKey(entry.getKey()))
				nonStandardNextRecord.put(entry.getKey(), entry.getValue());
		}
		return Util.getChangedCurrentFields(nonStandardPreviousRecord, nonStandardNextRecord, globalVariables.entityMetadataVOX);
	}

	private void updateWorkflowDetails(GlobalVariables globalVariables) throws Exception {
		if (globalVariables.businessRule != null && globalVariables.baseTemplateId != null)
			workflowProvider.CreateLaunchWorkflowIfApplicable(globalVariables);
//		WorkflowActionAttributePermission permission = dataService.getControlAndActionPermissionForWorkflow(globalVariables.workflowDetails.workflowTxnId);
		workflowExecutor.executeWorkflow(globalVariables, globalVariables.workflowDetails.workflowActionId);
		//need to update underlying record for workflow related fields.
	}

	private boolean checkDuplicateEffectiveDatetime(GlobalVariables globalVariables) {
		Map<String, Object> underlyingRecord = globalVariables.underlyingRecord;//globalVariables.relatedUnderlyingRecord.get("CURRENT");
		if((underlyingRecord == null || underlyingRecord.isEmpty()) && !globalVariables.isUpdate)//insert will be without underlying record and there should be a check of it for all the existing versions.
			return true;
		else{
			EntityAttributeMetadata pkMeta = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
			Object pkValue = globalVariables.insertData.getData().get(pkMeta.getDoAttributeCode());
			EntityAttributeMetadata versionMeta =  globalVariables.entityMetadataVOX.getVersoinIdAttribute();
			String effectiveDatetime = globalVariables.insertData.getData().get(globalVariables.entityMetadataVOX.getAttributeByColumnName("EFFECTIVE_DATETIME").getDoAttributeCode()).toString();
			StringBuilder sql = new StringBuilder("SELECT EXISTS(SELECT 1 FROM " + globalVariables.entityMetadataVOX.getDbTableName() + "  WHERE "
						+ pkMeta.getDbColumnName() + " = "+Util.getStringTobeInserted(pkMeta, pkValue));
			if(globalVariables.isUpdate)
				sql.append(" AND "+versionMeta.getDbColumnName() + " <> "+Util.getStringTobeInserted(versionMeta, globalVariables.insertData.getData().get(versionMeta.getDoAttributeCode())));
			sql.append(" AND EFFECTIVE_DATETIME = ? AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND IS_DELETED = FALSE LIMIT 1) AS COUNT;");
			Deque<Object> argument = new ArrayDeque<>();
			argument.push(effectiveDatetime);
			LOG.debug("----------DUPLICATE_EFFECTIVE_DATE_TIME_CHECK----------");
			LOG.debug("UPDATE_INLINE : " + globalVariables.isInline);
			SearchResult sr = searchDAO.search(sql.toString(), argument);
			if(sr != null && !sr.getData().isEmpty() && (Long) sr.getData().get(0).get("COUNT") > 0
//					&& globalVariables.underlyingRecord.get(globalVariables.domainObject+".SaveState").toString().equalsIgnoreCase("SAVED")
					&& globalVariables.isSubmit
					&& !globalVariables.isInline/*due to version not needed!..? */
					&& (globalVariables.workflowDetails.workflowStatus == null
					|| !globalVariables.workflowDetails.workflowStatus.equals("IN_PROGRESS"))){ //fixme move this whole logic to calling function
				LOG.debug("Duplicate Record Count(Effective datetime) : "+sr.getData().get(0).get("COUNT"));
				validationError.addError("Duplicate EffectiveDatetime Check", "Effective datetime should be unique for a particular Primary Key ("+pkMeta.getDoAttributeCode()+" : "+pkValue+").");
				return false;
			}
			return true;
		}
	}

	/*
	check this before you delete the record.
	 */
	private void checkForWorkflowThenUpdate(String sqlStatement, GlobalVariables globalVariables) throws SQLException {
		Map<String, Object> underlyingRecord = globalVariables.relatedUnderlyingRecord.get("CURRENT");
		if(underlyingRecord != null && underlyingRecord.get("REF_WORKFLOW_TXN_STATUS_CODE_FK_ID") != null &&
				underlyingRecord.get("REF_WORKFLOW_TXN_STATUS_CODE_FK_ID").toString().matches("COMPLETED|CANCELLED|WITHDRAWN")){//FIXME shouldn't be hardcoded.
    		insertService.insertRow(sqlStatement);
		}
		else{
			validationError.addError("Workflow Error", "can not delete a record which is under workflow. Check workflow status of the record.");
			throw new SQLException("Workflow Error", "can not delete a record which is under workflow. Check workflow status of the record.");
		}	
	}

	private void executeDoEventRule(String sqlStatement) {
		if(sqlStatement.startsWith("INSERT"));
		//need to update underlying record for event rule related fields.
	}

//	@Override
//	@Transactional
//	public void indexData(String doName) {
//		Analyzer analyzer = new StandardAnalyzer();
//
////		Directory directory = new RAMDirectory();
//		Path filePath = Paths.get("/Users/Tapplent/Documents/Tapplent_Core/TapplentV1.2/testindex");
//		Directory directory = null;
//		try {
//			directory = FSDirectory.open(filePath);
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		IndexWriterConfig config = new IndexWriterConfig(analyzer);
//		IndexWriter iwriter = null;
//	    try {
//			 iwriter = new IndexWriter(directory, config);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	    MasterEntityMetadataVOX doMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDo(doName);
//	    List<Map<String, Object>> dataList = searchDAO.search(Util.getSqlStringForMasterTable(doMeta), null).getData();
//	    Map<String, MasterEntityAttributeMetadata> attrMetaMap = doMeta.getAttributeMeta().stream()
//	    															.collect(Collectors.toMap(MasterEntityAttributeMetadata::getDoAttributeCode, p->p));
//	    for(Map<String, Object> row : dataList){
//	    	Document doc = new Document();
//	    	for(Entry<String, Object> entry : row.entrySet()){
//			    MasterEntityAttributeMetadata attrMeta = attrMetaMap.get(entry.getKey());
//		    	if(attrMeta.isLocalSearchable() && entry.getValue() != null){
//		    		doc.add(new Field(attrMeta.getDoAttributeCode(), entry.getValue().toString(), TextField.TYPE_STORED));
//		    		doc.add(new SortedDocValuesField(attrMeta.getDoAttributeCode(), new BytesRef(entry.getValue().toString())));
//		    	}
//	    	}
//	    	try{
//	    	iwriter.addDocument(doc);
//	    	}catch(IOException e){
//	    		e.printStackTrace();
//	    	}
//	    }
////	    final String groupField = "author";
//	    try {
//	    	iwriter.close();
//	    } catch (IOException e) {
//	    	e.printStackTrace();
//	    }
//
//	    try {
//		    DirectoryReader ireader = DirectoryReader.open(directory);
//		    IndexSearcher isearcher = new IndexSearcher(ireader);
//		    // Parse a simple query that searches for "text":
////		    TermQuery que = new TermQuery(new Term("Organization.Type", "departme*"));
////		    TermQuery que1 = new TermQuery(new Term("Organization.SubType", "departme*"));
//////		    TermQuery que1 = new TermQuery(new Term("Organization.Icon", "support"));
////		    BooleanQuery.Builder builder = new BooleanQuery.Builder();
////		    builder.add(que, Occur.SHOULD);
////		    builder.add(que1, Occur.SHOULD);
////		    BooleanQuery bq = builder.build();
////		    Query q2 = new ConstantScoreQuery(bq);
//		    AnalyzingQueryParser parser = new AnalyzingQueryParser("Organization.Icon", analyzer);
//		    Query query = parser.parse("Investment");
//		    parser.setAllowLeadingWildcard(true);
////		    Query query = parser.parse("*artment");
//
//		    MultiFieldQueryParser queryParser = new MultiFieldQueryParser(
//                    new String[] {"Organization.Type", "Organization.SubType"}, analyzer);
//		    queryParser.setAllowLeadingWildcard(true);
//		    Query mquery = queryParser.parse("*artment");
//
//		    ScoreDoc[] hits = isearcher.search(mquery, 1000).scoreDocs;
//		    LOG.info("Hits' Length : "+ hits.length);
//		    // Iterate through the results:
//		    for (int i = 0; i < hits.length; i++) {
//		      Document hitDoc = isearcher.doc(hits[i].doc);
////		      LOG.info(hitDoc.getValues("Organization.Type"));
//		      LOG.info(hitDoc.getFields());
//		    }
//
//		    //grouping
//		    final Sort groupSort = Sort.RELEVANCE;
//		    GroupingSearch groupingSearch = new GroupingSearch("Organization.Icon");
//		    groupingSearch.setGroupSort(groupSort);
//		    groupingSearch.setFillSortFields(true);
////		    ScoreDoc[] hits = isearcher.search(mquery, c1);
//		    groupingSearch.setAllGroups(true);
//		    groupingSearch.setCachingInMB(4.0, true);
////		    TermQuery query1 = new TermQuery(new Term("Organization.Icon", "Department"));
//		    TopGroups<BytesRef> result = groupingSearch.search(isearcher, mquery, 1, 1);
//		    // Render groupsResult...
//		    Integer totalGroupCount = result.totalGroupCount;
//		    LOG.info(totalGroupCount.toString());
//		    for(GroupDocs<BytesRef> g : result.groups){
//		    	LOG.debug(g.groupSortValues);
//		    	for(Object s : g.groupSortValues){
//		    		LOG.debug(s.toString());
//		    	}
//		    	LOG.debug(g.groupValue);
//		    	LOG.debug(g.scoreDocs);
//		    	for(ScoreDoc scoredoc : g.scoreDocs){
////		    		LOG.debug(scoredoc.toString());
//		    		Document doc = isearcher.doc(scoredoc.doc);
//		    		LOG.debug(doc.getFields());
//		    	}
//
//		    }
		    
//		    TermFirstPassGroupingCollector c1 = new TermFirstPassGroupingCollector("Organization.Type", groupSort, 10);
//		    Collection<SearchGroup<BytesRef>> a = c1.getTopGroups(1, true);
//		    final TermSecondPassGroupingCollector c2 = new TermSecondPassGroupingCollector("Organization.Type", c1.getTopGroups(0, true), groupSort, groupSort, 5, true, false, true);
//		    isearcher.search(que, c2);
//		    final TopGroups groups = c2.getTopGroups(0);
//		    GroupDocs group = groups.groups[0];
//		    LOG.info(group.groupValue);
		    
//		    if (false) {
//		      // Sets cache in MB
//		      groupingSearch.setCachingInMB(4.0, true);
//		    }
		  
//		    boolean requiredTotalGroupCount = true;
//			if (requiredTotalGroupCount ) {
//		      groupingSearch.setAllGroups(true);
//		    }
		  
//		    TermQuery query = new TermQuery(new Term("content", searchTerm));
//		    int groupOffset = 100;
//			int groupLimit = 100;
//			SortField sortfield = new SortField("Organization.Type", SortField.Type.STRING, true);
//			Sort sort = new Sort(sortfield);
//			TopGroups<BytesRef> result1 = groupingSearch.search(isearcher, query, 0, 1);
//
//		    // Render groupsResult...
//		    if (true) {
//		      int totalGroupCount1 = result1.totalGroupCount;
//		      LOG.info("COUNT : " + totalGroupCount1);
//		      for(GroupDocs<BytesRef> g : result1.groups){
//			    	for(Object s : g.groupSortValues){
//			    		LOG.debug(s.toString());
//			    	}
//			    	LOG.debug(g.groupValue);
//			    	LOG.debug(g.scoreDocs);
//			    	for(ScoreDoc scoredoc : g.scoreDocs){
////			    		LOG.debug(scoredoc.toString());
//			    		Document doc = isearcher.doc(scoredoc.doc);
//			    		LOG.debug(doc.getFields());
//			    	}
//		      }
//		    }
//
		    
//
//			ireader.close();
//			directory.close();
//		} catch (IOException | ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

//	@Override
//	public void validateDefault(GlobalVariables globalVariables) throws Exception {//Fixme Merge with validate.
//		Map<String, Object> data = globalVariables.insertData.getData();
//		String pk = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode();//fixme
//		Object primaryKeyValue = globalVariables.insertData.getData().get(pk);
//
//		if(primaryKeyValue != null){
//			// PRIMARY KEY check :
//			String columnName = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName();
//			if(!checkKeyExistenceInDb(globalVariables.entityMetadataVOX.getDbTableName(), columnName, primaryKeyValue, globalVariables.entityMetadataVOX.getAttributeByName(pk),globalVariables)){//FIXME metaDataMap.getAttributeByColumnName(columnName)) not working!
//				validationError.addError(pk, "Primary Key provided is not present.");
//				return;//check if return or not ?
//			}//FIXME uncomment as ETL requirement eliminated!
//		}
//		boolean checkUniqueRecord = true;
//		LinkedHashMap<String, EntityAttributeMetadata> uniqueFieldMap = null;
//		LinkedHashMap<String, Object> uniqueKeyValueMap = null;
//		if(checkUniqueRecord){
//			uniqueFieldMap = new LinkedHashMap<>();
//			uniqueKeyValueMap = new LinkedHashMap<>();
//		}
//		Map<String, EntityAttributeMetadata> attributeMetadataMap = globalVariables.entityMetadataVOX.getAttributeNameMap();
//		for (Entry<String, EntityAttributeMetadata> attribute : attributeMetadataMap.entrySet()){
//			String attributeName = attribute.getKey();
//			Object attributeValue = data.get(attributeName);
//			EntityAttributeMetadata attributeProperty = attribute.getValue();
//			// DATA_TYPE Check :
//			Map.Entry<String, Object> entry = new AbstractMap.SimpleEntry<String, Object>(attributeName, attributeValue);
//			Object typedValue = Util.convertStringToTypedValue(attributeProperty.getDbDataTypeCode(), entry);
////        		entry.setValue(typedValue); //FIXME check for the bug.
//			// MANDATORY Check :
//			if(attributeProperty.isMandatory() && (attributeValue == null || attributeValue.equals("")))
//				validationError.addError("MANDATORY Check", "Attribute - "+attributeName+" is mandatory; it can't be null.");
//			//UNIQUE Attribute check :
//			if(attributeProperty.isUniqueAttribute() && attributeValue != null && duplicateAttributeExist(attributeValue, attributeProperty, primaryKeyValue, globalVariables)){
//				validationError.addError("UNIQUE Attribute Check", "Field :"+ attributeProperty.getDoAttributeCode() + " for value "+ attributeValue +" is not unique. It must be unique.");
//			}
//			// UNIQUE Record Check :
//			if(checkUniqueRecord && attributeProperty.isUniqueAttributeForRecord() && attributeValue != null) {
//				uniqueFieldMap.put(attributeName, attributeProperty);
//				uniqueKeyValueMap.put(attributeName, attributeValue);
//			}
//			// FOREIGN KEY check :
//			if(attributeProperty.isRelationFlag() && attributeValue != null){
//				String tableName = attributeProperty.getToDbTableName();
//				String columnName = attributeProperty.getToDbColumnName();
//				if(tableName != null && columnName != null){
//					if(!checkKeyExistenceInDb(tableName, columnName, attributeValue, attributeProperty, globalVariables)){
//						validationError.addError("FOREIGN KEY check", "Foreign Key : " + attributeValue +" for "+attributeName+ " does not exist."); //FIXME uncomment when correct data entered.
//					}
//				} else {
//					validationError.addError("FOREIGN KEY check", "ToDbTableName and ToDbColumnName can not be null. Check meta data for "+attributeName);
//					throw new Exception();
//				}
//			}
//		}
//		// UNIQUE Record Check (Actual):
//		if(checkUniqueRecord && !uniqueFieldMap.isEmpty() ){
//			if(duplicateNonVersionedRecordExist(uniqueFieldMap, uniqueKeyValueMap, globalVariables)) {
//				if(!globalVariables.setContext)
//					validationError.addError("UNIQUE Record Check", "Some fields in the record :" + uniqueFieldMap.entrySet() + " must be unique.");
//			}
//		}
//		//CARDINALITY Check :
////    		if(currentActionStep.getPropertyCodeValueMap().containsKey("UPDATE_CARDINALITY") && currentActionStep.getPropertyCodeValueMap().get("UPDATE_CARDINALITY").equalsIgnoreCase("TRUE")){
////			checkCardinalityForAttributes(globalVariables.entityMetadataVOX, data);
////    		}
//
//		if (validationError.hasError()){
////			validationError.logValidationErrors();
////			validationError.clear();
//			throw new Exception();
//		}
//	}

	private boolean duplicateNonVersionedRecordExist(LinkedHashMap<String, EntityAttributeMetadata> uniqueFieldMap, LinkedHashMap<String, Object> uniqueKeyValueMap, GlobalVariables globalVariables) {
		Deque<Object> arguments = new ArrayDeque<>();
		EntityAttributeMetadata primaryAttribute = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
		StringBuilder sql =  new StringBuilder();
		sql.append("SELECT COUNT(*) AS COUNT, "+primaryAttribute.getDbColumnName()+" AS PK FROM ");
		sql.append(globalVariables.entityMetadataVOX.getDbTableName() + " WHERE ");
		for(Entry<String, EntityAttributeMetadata> entry : uniqueFieldMap.entrySet()){
			if(entry.getValue().isGlocalizedFlag()){
				sql.append("COLUMN_GET("+entry.getValue().getDbColumnName()+", '"+globalVariables.primaryLocale+"' AS CHAR) ");
			}
			else if(entry.getValue().getContainerBlobDbColumnName() != null){
				sql.append("COLUMN_GET("+entry.getValue().getContainerBlobDbColumnName()+", '"+entry.getValue().getDoAttributeCode()+" AS "+
						Util.getBlobDataTypeFromEntityDataType(entry.getValue().getDbDataTypeCode()));//FIXME uniqueKeyValueMap.get(entry.getKey()) required somewhere.
			}
			else {
				sql.append(entry.getValue().getDbColumnName());
			}
			arguments.push(Util.getStringTobuildWhereClauseAsParameter(entry.getValue(), uniqueKeyValueMap.get(entry.getKey()), entry.getValue().getToDbColumnName(), globalVariables.primaryLocale));//FIXME check if the function is redundant; getStringToBeInserted could be used.
			sql.append(" = ? AND ");
		}
		sql.replace(sql.length()-4, sql.length(), ";");
		SearchResult sr = searchDAO.search(sql.toString(), arguments);
		if((Long) sr.getData().get(0).get("COUNT") > 0) {
			if(globalVariables.setContext){
				globalVariables.isUpdate = true;
				globalVariables.contextPK = sr.getData().get(0).get("PK").toString();
				globalVariables.insertData.getData().put(globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode(), sr.getData().get(0).get("PK").toString());
			}
			return true;
		}
		return false;
	}

//	@Override
//	public void operateOnUnderlyingRecordDefault(GlobalVariables globalVariables) throws Exception{
//		String sqlStatement = null;
//		try{
//			setUnderlyingRelatedDataFromDbDefault(globalVariables);//before valueobject creation as valueobject always contain pk and version. //FIXME should move to globalVariables.
//			if(globalVariables.setContext){
//				AddContextualFields(globalVariables);
//				validateDefault(globalVariables);
//			}
//			SQLContextBuilder contextBuilder = new InsertContextBuilder();
////			globalVariables.model = (InsertContextModel) contextBuilder.buildDefault(globalVariables);
//			sqlStatement = generateQueryDefalut(globalVariables.entityMetadataVOX, globalVariables.insertData, globalVariables.underlyingRecord, globalVariables.model, globalVariables);
//			insertService.insertRow(sqlStatement);
//			setUnderlyingRelatedDataFromDbDefault(globalVariables);
//
//		}catch (Exception e) {
//			e.printStackTrace();
//			validationError.logValidationErrors();
//			LOG.error(sqlStatement);
//			throw e;
//		}
//	}

	@Override
	public void setRelatedRecordToDuplicate(GlobalVariables globalVariables) {
	 	List<String> attributeList = getAttributeListToDuplicate(globalVariables.entityMetadataVOX);
	 	Map<String, EntityAttributeMetadata> attributeMetadataMap = globalVariables.entityMetadataVOX.getAttributeNameMap();
	 	EntityAttributeMetadata dbPkMeta = globalVariables.entityMetadataVOX.getVersoinIdAttribute();
	 	String pk = (String) globalVariables.insertData.getData().get(dbPkMeta.getDoAttributeCode());
		UnitResponseData response = dataService.getAttributePathData(attributeList, pk,true, false);
		Map<String, AttributeData> attributeMap = response.getPEData().values().iterator().next().getRecordSetInternal().get(0).getAttributeMap();
		Map<String, Object> dataToInsert = globalVariables.insertData.getData();

		for(Entry<String, AttributeData> entry : attributeMap.entrySet()){
			if (entry.getValue().getColumnLabel().startsWith("!{")) {
				String doaNameExpression = entry.getValue().getColumnLabel();
				String doaName = doaNameExpression.replaceAll("[(!{)|}]", "");
				if (attributeList.contains(doaNameExpression)) {
					Object doaValue = entry.getValue().getcV();
					if(attributeMetadataMap.get(doaName).getDbDataTypeCode().equals("T_ICON")){
						doaValue = ((IconVO) doaValue).getCode();
						dataToInsert.put(doaName, doaValue);
					}
					else if (globalVariables.duplicateOfAttribute.equals(doaName)) {
						if (attributeMetadataMap.get(doaName).isGlocalizedFlag()) {
							doaValue = concateDuplicateMarkerForAllLocale(doaValue);
							dataToInsert.put(doaName, doaValue);
						}
						else
							dataToInsert.put(doaName, globalVariables.actionCode + "-" + doaValue);
					} else
						dataToInsert.put(doaName, doaValue);
				}
			}
		}
		dataToInsert.remove(dbPkMeta.getDoAttributeCode());
		dataToInsert.remove(globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode());
	}

	private Object concateDuplicateMarkerForAllLocale(Object doaValue) {
		return null;
//	 	UiSetting uiSetting = TenantAwareCache.getMetaDataRepository().getUiStetting();
//	 	JsonNode duplicateStringBlob = uiSetting.getUiPropertyBlob().get("UISetting.DuplicateAsText");
//	 	Map<String, Object> localeDuplicateValueMap = Util.getG11nMapFromJsonNode(duplicateStringBlob);
//	 	Map<String, Object> localeDoaValueMap = Util.getG11nMapFromString((String) doaValue);
//	 	for (Entry<String, Object> entry : localeDoaValueMap.entrySet()){
//	 		localeDoaValueMap.replace(entry.getKey(), localeDuplicateValueMap.get(entry.getKey())+"-"+entry.getValue());
//		}
//		String concatedDoaValue = Util.getStringFromG11nMap(localeDoaValueMap);
//	 	return concatedDoaValue;
	}

	private List<String> getAttributeListToDuplicate(EntityMetadataVOX entityMetadataVOX) {
		List<String> attributeList = new ArrayList<>();
		Map<String, EntityAttributeMetadata> standardDoaFieldsMap = entityMetadataVOX.getStandardDoaFields();
		List<EntityAttributeMetadata> attributeMetadatas = entityMetadataVOX.getAttributeMeta().stream()
															.filter(p -> !p.isNonVersionTrackableFlag() && !standardDoaFieldsMap.containsKey(p.getDoAttributeCode()))
															.collect(Collectors.toList());
		attributeMetadatas.forEach(attr -> attributeList.add("!{"+attr.getDoAttributeCode()+"}"));
		return attributeList;
	}

	private void AddContextualFields(GlobalVariables globalVariables) throws Exception {
	 	Map<String, Object> insertData = globalVariables.insertData.getData();
	 	EntityMetadataVOX targetEntityMetadataVOX = globalVariables.entityMetadataVOX;
	 	Map<String, EntityAttributeMetadata> standardFields = globalVariables.entityMetadataVOX.getStandardColumnFields();
	 	if(standardFields.containsKey("LOGGED_IN_PERSON_FK_ID")){
	 		insertData.put(standardFields.get("LOGGED_IN_PERSON_FK_ID").getDoAttributeCode(), TenantContextHolder.getUserContext().getPersonId());
		}
//	 	EntityAttributeMetadata tDoIconCodeAttribute = targetEntityMetadataVOX.getAttributeByColumnName("DO_ICON_CODE_FK_ID");
//	 	EntityAttributeMetadata tDoNameG11nAttribute = targetEntityMetadataVOX.getAttributeByColumnName("DO_NAME_G11N_BIG_TXT");
//	 	EntityAttributeMetadata tIsContextObjectField = targetEntityMetadataVOX.getAttributeByColumnName("DISPLAY_CONTEXT_VALUE_G11N_BIG_TXT");
//	 	String doIconCode = tDoIconCodeAttribute.getDoAttributeCode();
//	 	String doNameG11n = tDoNameG11nAttribute.getDoAttributeCode();
		String srcMtPE = insertData.get(targetEntityMetadataVOX.getDomainObjectCode()+".MetaProcessElement").toString();
		String srcBt = TenantAwareCache.getPersonRepository().getDefaultBTForGroup(srcMtPE, TenantContextHolder.getUserContext().getPersonId());
		EntityMetadataVOX sourceEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(srcBt, srcMtPE, null);
	 	EntityAttributeMetadata sIsContextObjectField = sourceEntityMetadataVOX.getIsContextObjectField();
	 	EntityAttributeMetadata doRowPKAttribute = sourceEntityMetadataVOX.getFunctionalPrimaryKeyAttribute();
		String sql = "SELECT "+sIsContextObjectField.getDbColumnName()+" FROM "+sIsContextObjectField.getDbTableName()+
				" WHERE "+doRowPKAttribute.getDbColumnName()+" = "+globalVariables.insertData.getData().get(targetEntityMetadataVOX.getDomainObjectCode()+".DOPrimaryKey");
		SearchResult sr = searchDAO.search(sql, null);
		if(sr.getData() != null && !sr.getData().isEmpty()){
			Map<String, Object> data = sr.getData().get(0);
//			String doIconCodeValue = (String) data.get("DO_ICON_CODE_FK_ID");
//			String doNameG11nValue = (String) data.get("DO_NAME_G11N_BIG_TXT");
//			String isContextObjectValue = (String) data.get(sIsContextObjectField.getDbColumnName());
//			String doIconCodeValue = sourceEntityMetadataVOX.getDoSummaryTitleIcon().getCode();
//			String doNameG11nValue = sourceEntityMetadataVOX.getBtDOName();
//			insertData.put(doIconCode,doIconCodeValue);
//			insertData.put(doNameG11n,doNameG11nValue);
//			insertData.put(tIsContextObjectField.getDoAttributeCode(), isContextObjectValue);
		}
		else {
			validationError.addError("CONTEXTUAL_FIELD", "Contextual PK not found!");
			throw new Exception();
		}
	}

//	private String generateQueryDefalut(EntityMetadataVOX entityMetadataVOX, DefaultInsertData insertData, Map<String, Object> underlyingRecord, InsertContextModel model, GlobalVariables globalVariables) throws Exception {
//		String query = null;
//		if (globalVariables.isUpdate) {
//			if(checkDirtyUpdate(entityMetadataVOX, insertData, underlyingRecord)){
//				UpdateStatementBuilder statementBuilder = new UpdateStatementBuilder(model, entityMetadataVOX);
//				query = statementBuilder.buildDefault();
//			} else {
//				return null;
//			}
//		} else {
//			InsertStatementBuilder statementBuilder = new InsertStatementBuilder(model, entityMetadataVOX);
//			query = statementBuilder.build();
//		}
//// 	    }
//		return query;
//	}

//	private void setUnderlyingRelatedDataFromDbDefault(GlobalVariables globalVariables) {
//		EntityAttributeMetadata pkMeta = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
//		String pkValue = (String) globalVariables.insertData.getData().get(pkMeta.getDoAttributeCode());
//		String pkColumnName = pkMeta.getDbColumnName().trim();
//
//		if(globalVariables.model != null){
//			pkValue = pkValue != null ? pkValue : (String) globalVariables.model.getInsertValueMap().get(pkMeta.getDbColumnName());
//		}
//		if(pkValue != null){
//			Deque<Object> arguments  =  new ArrayDeque<>();
//			StringBuilder sql = new StringBuilder();
//			String selectString =  Util.getSqlStringForTable(globalVariables.entityMetadataVOX);
//				sql.append("(" + selectString + " WHERE " + pkColumnName +
//						" = "+Util.getStringTobeInserted(pkMeta, pkValue)+");");
//				SearchResult currentSr = searchDAO.search(sql.toString(), arguments);
//				if(currentSr.getData() != null && !currentSr.getData().isEmpty()){
//					List<Map<String, Object>> typedData = Util.convertRawDataIntoTypedData(currentSr.getData(), globalVariables.entityMetadataVOX);
//					globalVariables.relatedUnderlyingRecord.put("CURRENT", typedData.get(0));
//					globalVariables.underlyingRecord = typedData.get(0);
//				}
//		}
//		else {
//			LOG.debug("Primary Key is not present for " + globalVariables.btPE);
//		}
//	}

	public HierarchyService getHierarchyService() {
		return hierarchyService;
	}

	public void setHierarchyService(HierarchyService hierarchyService) {
		this.hierarchyService = hierarchyService;
	}

	public GroupResolverService getGroupResolverService() {
		return groupResolverService;
	}

	public void setGroupResolverService(GroupResolverService groupResolverService) {
		this.groupResolverService = groupResolverService;
	}

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	public G11nService getG11nService() {
		return g11nService;
	}

	public void setG11nService(G11nService g11nService) {
		this.g11nService = g11nService;
	}
}
