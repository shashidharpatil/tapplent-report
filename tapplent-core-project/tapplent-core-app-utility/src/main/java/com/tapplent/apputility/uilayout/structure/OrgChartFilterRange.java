package com.tapplent.apputility.uilayout.structure;

import com.tapplent.apputility.layout.structure.Icon;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.uilayout.valueobject.OrgChartFilterRangeVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 06/07/17.
 */
public class OrgChartFilterRange {

    private String orgChartFilterRangePKID;
    private String rangeName;
    private IconVO rangeIcon;
    private double rangeMin;
    private String rangeMinName;
    private IconVO rangeMinIcon;
    private double rangeMax;
    private String rangeMaxName;
    private IconVO rangeMaxIcon;

    public String getOrgChartFilterRangePKID() {
        return orgChartFilterRangePKID;
    }

    public void setOrgChartFilterRangePKID(String orgChartFilterRangePKID) {
        this.orgChartFilterRangePKID = orgChartFilterRangePKID;
    }

    public String getRangeName() {
        return rangeName;
    }

    public void setRangeName(String rangeName) {
        this.rangeName = rangeName;
    }

    public IconVO getRangeIcon() {
        return rangeIcon;
    }

    public void setRangeIcon(IconVO rangeIcon) {
        this.rangeIcon = rangeIcon;
    }

    public double getRangeMin() {
        return rangeMin;
    }

    public void setRangeMin(double rangeMin) {
        this.rangeMin = rangeMin;
    }

    public String getRangeMinName() {
        return rangeMinName;
    }

    public void setRangeMinName(String rangeMinName) {
        this.rangeMinName = rangeMinName;
    }

    public IconVO getRangeMinIcon() {
        return rangeMinIcon;
    }

    public void setRangeMinIcon(IconVO rangeMinIcon) {
        this.rangeMinIcon = rangeMinIcon;
    }

    public double getRangeMax() {
        return rangeMax;
    }

    public void setRangeMax(double rangeMax) {
        this.rangeMax = rangeMax;
    }

    public String getRangeMaxName() {
        return rangeMaxName;
    }

    public void setRangeMaxName(String rangeMaxName) {
        this.rangeMaxName = rangeMaxName;
    }

    public IconVO getRangeMaxIcon() {
        return rangeMaxIcon;
    }

    public void setRangeMaxIcon(IconVO rangeMaxIcon) {
        this.rangeMaxIcon = rangeMaxIcon;
    }

    public static OrgChartFilterRange fromVO(OrgChartFilterRangeVO orgChartFilterRangeVO){
        OrgChartFilterRange orgChartFilterRange = new OrgChartFilterRange();
        BeanUtils.copyProperties(orgChartFilterRangeVO, orgChartFilterRange);
        return orgChartFilterRange;
    }
}
