package com.tapplent.apputility.data.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ResponseData {
	private List<Object> writeResponse = new ArrayList<>();
	private List<UnitResponseData> responseDataList;
	private Map<String, List<String>> errorLog;

	public List<Object> getWriteResponse() {
		return writeResponse;
	}

	public void setWriteResponse(List<Object> writeResponse) {
		this.writeResponse = writeResponse;
	}

	public ResponseData(){
		this.responseDataList = new ArrayList<>();
	}
	public List<UnitResponseData> getResponseDataList() {
		return responseDataList;
	}

	public void setResponseDataList(List<UnitResponseData> responseDataList) {
		this.responseDataList = responseDataList;
	}

	public Map<String, List<String>> getErrorLog() {
		return errorLog;
	}

	public void setErrorLog(Map<String, List<String>> errorLog) {
		this.errorLog = errorLog;
	}
}
