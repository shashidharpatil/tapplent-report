/**
 * 
 */
package com.tapplent.apputility.etl.structure;

/**
 * @author Shubham Patodi
 *
 */
public class EtlIcon {
	private String icon;
	private String tableName;
	private String columnName;
	public EtlIcon(String icon, String tableName, String columnName) {
		super();
		this.icon = icon;
		this.tableName = tableName;
		this.columnName = columnName;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
}
