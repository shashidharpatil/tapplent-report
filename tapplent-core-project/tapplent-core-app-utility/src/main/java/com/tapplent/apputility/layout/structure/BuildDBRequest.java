package com.tapplent.apputility.layout.structure;

import java.util.List;

import com.tapplent.platformutility.common.util.G11nDetails;
import com.tapplent.platformutility.common.util.GoogleSheetsDetails;

public class BuildDBRequest {
	private List<GoogleSheetsDetails> fileInfoList;
	private List<G11nDetails> g11nDetailsList;
	public List<G11nDetails> getG11nDetailsList() {
		return g11nDetailsList;
	}
	public void setG11nDetailsList(List<G11nDetails> g11nDetailsList) {
		this.g11nDetailsList = g11nDetailsList;
	}
	public List<GoogleSheetsDetails> getFileInfoList() {
		return fileInfoList;
	}
	public void setFileInfoList(List<GoogleSheetsDetails> fileInfoList) {
		this.fileInfoList = fileInfoList;
	}
}
