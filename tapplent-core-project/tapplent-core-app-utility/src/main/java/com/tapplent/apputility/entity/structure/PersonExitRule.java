package com.tapplent.apputility.entity.structure;

public class PersonExitRule {
    private String versionId;
    private String personExitRuleId;
    private String exitRuleCodeId;
    private String replaceJobRelationTypeCode;
    private String replaceJobRelationHierarchyLevel;
    private String replacementPerson;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getPersonExitRuleId() {
        return personExitRuleId;
    }

    public void setPersonExitRuleId(String personExitRuleId) {
        this.personExitRuleId = personExitRuleId;
    }

    public String getReplaceJobRelationTypeCode() {
        return replaceJobRelationTypeCode;
    }

    public String getExitRuleCodeId() {
        return exitRuleCodeId;
    }

    public void setExitRuleCodeId(String exitRuleCodeId) {
        this.exitRuleCodeId = exitRuleCodeId;
    }

    public void setReplaceJobRelationTypeCode(String replaceJobRelationTypeCode) {
        this.replaceJobRelationTypeCode = replaceJobRelationTypeCode;
    }

    public String getReplaceJobRelationHierarchyLevel() {
        return replaceJobRelationHierarchyLevel;
    }

    public void setReplaceJobRelationHierarchyLevel(String replaceJobRelationHierarchyLevel) {
        this.replaceJobRelationHierarchyLevel = replaceJobRelationHierarchyLevel;
    }

    public String getReplacementPerson() {
        return replacementPerson;
    }

    public void setReplacementPerson(String replacementPerson) {
        this.replacementPerson = replacementPerson;
    }
}
