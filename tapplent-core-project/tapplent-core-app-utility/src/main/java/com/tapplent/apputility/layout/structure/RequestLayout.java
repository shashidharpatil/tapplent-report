package com.tapplent.apputility.layout.structure;

public class RequestLayout {
	private String metaProcessCode;
	private String baseTemplateId;
	private String deviceType;
	private boolean includeArchived;
	public String getMetaProcessCode() {
		return metaProcessCode;
	}
	public void setMetaProcessCode(String metaProcessCode) {
		this.metaProcessCode = metaProcessCode;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public boolean isIncludeArchived() {
		return includeArchived;
	}
	public void setIncludeArchived(boolean includeArchived) {
		this.includeArchived = includeArchived;
	}
}