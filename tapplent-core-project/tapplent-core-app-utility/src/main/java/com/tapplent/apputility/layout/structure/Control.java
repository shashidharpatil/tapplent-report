package com.tapplent.apputility.layout.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.ControlVO;

public class Control {
	private String controlId;
	private String canvasId;
	@JsonIgnore
	private String controlTransactionFkId;
	@JsonIgnore
	private String controlinstanceFkId;
	@JsonIgnore
	private String baseObjectFkId;
	private String baseTemplateId;
	@JsonIgnore
	private String btProcessElementFkId;
	@JsonIgnore
	private String domainObjectFkId;
	private String mtPEId;
	private String controlAttribute;
	private String aggregateControlFunction;
	private String relationControlCanvasMasterId;
	private String hierarchyControlCanvasMasterId;
	private String tapCardCanvasMasterId;
	JsonNode deviceIndependentBlob;
	JsonNode deviceDependentBlob;
	@JsonIgnore
	JsonNode mstDeviceIndependentBlob;
	@JsonIgnore
	JsonNode txnDeviceIndependentBlob;
	@JsonIgnore
	JsonNode mstDeviceDependentBlob;
	@JsonIgnore
	JsonNode txnDeviceDependentBlob;
	private List<ControlEvent> controlEventList;
	private List<ControlHierarchyControl> controlHCList;
	private Map<String, ControlRelationControl> rcCanvasControlMasterIdToControlRCMap;
	private List<ControlRelationControl> controlRCList;
	private List<ControlGroupByControl> controlGroupByList;
	private List<CanvasMaster> relationControlCanvasList;
	private List<ActionStandAlone> actionStandAloneList;
	public Control(){
		this.controlRCList = new ArrayList<>();
		this.controlHCList = new ArrayList<>();
		this.controlGroupByList = new ArrayList<>();
		this.controlEventList = new ArrayList<>();
		this.rcCanvasControlMasterIdToControlRCMap = new HashMap<>();
		this.actionStandAloneList=new ArrayList<>();
	}
	public String getControlId() {
		return controlId;
	}

	public void setControlId(String controlId) {
		this.controlId = controlId;
	}

	public String getCanvasId() {
		return canvasId;
	}

	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}

	public String getControlTransactionFkId() {
		return controlTransactionFkId;
	}

	public void setControlTransactionFkId(String controlTransactionFkId) {
		this.controlTransactionFkId = controlTransactionFkId;
	}

	public List<CanvasMaster> getRelationControlCanvasList() {
		return relationControlCanvasList;
	}

	public void setRelationControlCanvasList(List<CanvasMaster> relationControlCanvasList) {
		this.relationControlCanvasList = relationControlCanvasList;
	}

	public String getControlinstanceFkId() {
		return controlinstanceFkId;
	}

	public void setControlinstanceFkId(String controlinstanceFkId) {
		this.controlinstanceFkId = controlinstanceFkId;
	}

	public String getBaseObjectFkId() {
		return baseObjectFkId;
	}

	public void setBaseObjectFkId(String baseObjectFkId) {
		this.baseObjectFkId = baseObjectFkId;
	}

	public String getBaseTemplateId() {
		return baseTemplateId;
	}

	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getBtProcessElementFkId() {
		return btProcessElementFkId;
	}

	public void setBtProcessElementFkId(String btProcessElementFkId) {
		this.btProcessElementFkId = btProcessElementFkId;
	}

	public String getDomainObjectFkId() {
		return domainObjectFkId;
	}

	public void setDomainObjectFkId(String domainObjectFkId) {
		this.domainObjectFkId = domainObjectFkId;
	}

	public String getMtPEId() {
		return mtPEId;
	}

	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}

	public String getControlAttribute() {
		return controlAttribute;
	}

	public void setControlAttribute(String controlAttribute) {
		this.controlAttribute = controlAttribute;
	}

	public List<ControlEvent> getControlEventList() {
		return controlEventList;
	}

	public void setControlEventList(List<ControlEvent> controlEventList) {
		this.controlEventList = controlEventList;
	}

	public List<ControlHierarchyControl> getControlHCList() {
		return controlHCList;
	}

	public void setControlHCList(List<ControlHierarchyControl> controlHCList) {
		this.controlHCList = controlHCList;
	}

	public List<ControlRelationControl> getControlRCList() {
		return controlRCList;
	}

	public void setControlRCList(List<ControlRelationControl> controlRCList) {
		this.controlRCList = controlRCList;
	}

	public List<ControlGroupByControl> getControlGroupByList() {
		return controlGroupByList;
	}

	public void setControlGroupByList(List<ControlGroupByControl> controlGroupByList) {
		this.controlGroupByList = controlGroupByList;
	}

	public List<ActionStandAlone> getActionStandAloneList() {
		return actionStandAloneList;
	}

	public void setActionStandAloneList(List<ActionStandAlone> actionStandAloneList) {
		this.actionStandAloneList = actionStandAloneList;
	}
	
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public String getAggregateControlFunction() {
		return aggregateControlFunction;
	}
	public void setAggregateControlFunction(String aggregateControlFunction) {
		this.aggregateControlFunction = aggregateControlFunction;
	}
	public JsonNode getMstDeviceIndependentBlob() {
		return mstDeviceIndependentBlob;
	}
	public void setMstDeviceIndependentBlob(JsonNode mstDeviceIndependentBlob) {
		this.mstDeviceIndependentBlob = mstDeviceIndependentBlob;
	}
	public JsonNode getTxnDeviceIndependentBlob() {
		return txnDeviceIndependentBlob;
	}
	public void setTxnDeviceIndependentBlob(JsonNode txnDeviceIndependentBlob) {
		this.txnDeviceIndependentBlob = txnDeviceIndependentBlob;
	}
	public JsonNode getMstDeviceDependentBlob() {
		return mstDeviceDependentBlob;
	}
	public void setMstDeviceDependentBlob(JsonNode mstDeviceDependentBlob) {
		this.mstDeviceDependentBlob = mstDeviceDependentBlob;
	}
	public JsonNode getTxnDeviceDependentBlob() {
		return txnDeviceDependentBlob;
	}
	public void setTxnDeviceDependentBlob(JsonNode txnDeviceDependentBlob) {
		this.txnDeviceDependentBlob = txnDeviceDependentBlob;
	}
	public String getRelationControlCanvasMasterId() {
		return relationControlCanvasMasterId;
	}
	public void setRelationControlCanvasMasterId(String relationControlCanvasMasterId) {
		this.relationControlCanvasMasterId = relationControlCanvasMasterId;
	}
	public String getHierarchyControlCanvasMasterId() {
		return hierarchyControlCanvasMasterId;
	}
	public void setHierarchyControlCanvasMasterId(String hierarchyControlCanvasMasterId) {
		this.hierarchyControlCanvasMasterId = hierarchyControlCanvasMasterId;
	}

	public Map<String, ControlRelationControl> getRcCanvasControlMasterIdToControlRCMap() {
		return rcCanvasControlMasterIdToControlRCMap;
	}

	public void setRcCanvasControlMasterIdToControlRCMap(Map<String, ControlRelationControl> rcCanvasControlMasterIdToControlRCMap) {
		this.rcCanvasControlMasterIdToControlRCMap = rcCanvasControlMasterIdToControlRCMap;
	}

	public String getTapCardCanvasMasterId() {
		return tapCardCanvasMasterId;
	}
	public void setTapCardCanvasMasterId(String tapCardCanvasMasterId) {
		this.tapCardCanvasMasterId = tapCardCanvasMasterId;
	}
	public static Control fromVO(ControlVO controlVO){
		Control control = new Control();
		BeanUtils.copyProperties(controlVO, control);
		return control;
	}
}
