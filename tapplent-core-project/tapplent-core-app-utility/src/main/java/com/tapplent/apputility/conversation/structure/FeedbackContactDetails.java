package com.tapplent.apputility.conversation.structure;


import java.util.List;

/**
 * Created by Manas on 6/12/17.
 */
public class FeedbackContactDetails {
    List<PersonDetail> personDetails;

    public List<PersonDetail> getPersonDetails() {
        return personDetails;
    }

    public void setPersonDetails(List<PersonDetail> personDetails) {
        this.personDetails = personDetails;
    }
}
