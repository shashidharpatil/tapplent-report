package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.CanvasElementThemeVO;

public class CanvasElementTheme {
	@JsonIgnore
	private String versionId;
	@JsonIgnore
	private String canvasElementThemeSelecPkId;
	private String canvasTypeCodeFkId;
	private String themeTemplateCodeFkId;
	private String groupId;
	private String themeSizeCode;
	private JsonNode elementThemeProperty;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getCanvasElementThemeSelecPkId() {
		return canvasElementThemeSelecPkId;
	}
	public void setCanvasElementThemeSelecPkId(String canvasElementThemeSelecPkId) {
		this.canvasElementThemeSelecPkId = canvasElementThemeSelecPkId;
	}
	public String getCanvasTypeCodeFkId() {
		return canvasTypeCodeFkId;
	}
	public void setCanvasTypeCodeFkId(String canvasTypeCodeFkId) {
		this.canvasTypeCodeFkId = canvasTypeCodeFkId;
	}
	public JsonNode getElementThemeProperty() {
		return elementThemeProperty;
	}
	public void setElementThemeProperty(JsonNode elementThemeProperty) {
		this.elementThemeProperty = elementThemeProperty;
	}
	public String getThemeTemplateCodeFkId() {
		return themeTemplateCodeFkId;
	}
	public void setThemeTemplateCodeFkId(String themeTemplateCodeFkId) {
		this.themeTemplateCodeFkId = themeTemplateCodeFkId;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getThemeSizeCode() {
		return themeSizeCode;
	}
	public void setThemeSizeCode(String themeSizeCode) {
		this.themeSizeCode = themeSizeCode;
	}
	public static CanvasElementTheme fromVO(CanvasElementThemeVO canvasElementThemeVO){
		CanvasElementTheme canvasElementTheme = new CanvasElementTheme();
		BeanUtils.copyProperties(canvasElementThemeVO,canvasElementTheme);
		return canvasElementTheme;
	}
}