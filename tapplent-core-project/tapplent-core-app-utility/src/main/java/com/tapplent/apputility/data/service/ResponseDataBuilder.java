package com.tapplent.apputility.data.service;



import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import com.amazonaws.services.s3.AmazonS3;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.apputility.data.structure.*;
import com.tapplent.apputility.uilayout.service.UILayoutService;
import com.tapplent.platformutility.accessmanager.dao.AccessManagerDAO;
import com.tapplent.platformutility.accessmanager.structure.ResolvedColumnCondition;
import com.tapplent.platformutility.activityLog.ActivityLog;
import com.tapplent.platformutility.activityLog.AuditMsgFormat;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.PersonDetailsObjectRepository;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.hierarchy.service.HierarchyService;
import com.tapplent.platformutility.hierarchy.structure.Relation;
import com.tapplent.apputility.entity.provider.InsertService;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.builder.DbSearchTemplate;
import com.tapplent.platformutility.search.builder.SearchField;
import com.tapplent.platformutility.search.impl.DefaultSearchData;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.uilayout.valueobject.SectionControlVO;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import com.tapplent.tenantresolver.user.UserContext;

/*
 * Class will help to convert the raw data into as required by the client.
 */
public class ResponseDataBuilder {
	public static final String COLON = ":";
	private static final String AGGREGATE_ALIAS = "###AGGREGATE_ALIAS###";
	private static final String VERSION_CHANGES_DOA = "VersionChanges";
    private static final String INLINE_CHANGES_DOA = "InlineChanges";
	private AccessManagerDAO accessManagerDAO;
	private UILayoutService uiLayoutService;
	private InsertService insertService;
	private HierarchyService hierarchyService;
	/**
	 * This method should make the process Element data. It consists of several steps.
	 * 1. Prepare result for the root process element
	 * 2. Prepare result for all hierarchical process element
	 * 3. Prepare result for aggregate select statements (to be included in both the steps)  
	 * So the parameters should be:
	 * 		Result set of all hierarchical data. Here we shall take help of tableId
	 */
	public ProcessElementData createProcessElementData(DefaultSearchData rootSearchData, SearchResult rootSearchResult, PERequest peRequest, JsonLabelAliasTracker aliasTracker, String baseTemplate, boolean isRootPE) {
        ProcessElementData processElementData = new ProcessElementData();
        processElementData.setMtPE(rootSearchData.getMtPE());
        processElementData.setMtPEAlias(rootSearchData.getMtPEAlias());
//		processElementData.setBaseTemplateId(rootSearchData.getBaseTemplateId());
        processElementData.setRecordsExist(rootSearchResult.getNumberOfRecords());
        processElementData.setRecordsReturned(rootSearchResult.getData().size());
        List<Map<String, Object>> data = rootSearchResult.getData();
        Set<String> attributePaths = rootSearchData.getAttributePaths();
        List<DependencyKeysHelper> dependencyKeysList = rootSearchData.getDependencyKeyList();
        Map<String, String> currentDependencyDOAs = new HashMap<>();
        Map<String, Map<String, List<AttributeData>>> amRowPkToRSToAttributeData = new HashMap<>();
        if (dependencyKeysList != null) {
            for (DependencyKeysHelper keysHelper : dependencyKeysList) {
                String currentValue = keysHelper.getCurrentDOACode();
                currentDependencyDOAs.put(currentValue, currentValue);
            }
        }
        UserContext user = TenantContextHolder.getUserContext();
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadataVOX rootMetadata = metadataService.getMetadataByMtPE(rootSearchData.getMtPE());
        DbSearchTemplate primaryTable = rootSearchData.getTables().get(rootSearchData.getMtPEAlias()+"^"+rootMetadata.getDomainObjectCode());
		/* Now loop through the complete result and add one to one relation in it */
        String functionalPrimaryKeyColumnLabel = null;
        String dbPrimaryKeyColumnLabel = null;
        if (rootSearchData.getFunctionPrimaryKey() != null) {
            functionalPrimaryKeyColumnLabel = rootSearchData.getFunctionPrimaryKey().getColumnLabel();
        }
        if (rootSearchData.getDatabasePrimaryKey() != null) {
            dbPrimaryKeyColumnLabel = rootSearchData.getDatabasePrimaryKey().getColumnLabel();
        }
		/*
		Add property control values at the proper level
		 */
        List<Record> recordSet = new ArrayList<>();
        Map<String, Object> stickyHdrMap = new LinkedHashMap<String, Object>();
//        addPCValuesToMTPE(processElementData, propertyControlAttributes);
		/* Loop through all the rows retrieved */
        for (Map<String, Object> dbRow : data) {
            String functionalPrimaryKeyValue = null;
            String dbPrimaryKeyValue = null;
            Record responseRow = new Record();
            boolean isViewAllowed = true;
            String recordState = null;
//            if (rootMetadata.isAccessControlGoverned()) {
//                String rootRecordStateLabel = (String) dbRow.get(rootSearchData.getRecordStateColumn().getColumnLabel());
//                recordState = dbRow.get(rootRecordStateLabel).toString();
//                // Now that we have record state we should get the correct permissions
//                isViewAllowed = isViewAllowed(dbRow, recordState, primaryTable.getTableAlias());
//            }
            if(isViewAllowed){
                // Change the recordList here. Or get the correct record list.
                // Change the sticky header response to be map<String, Map<String, ... upto Map<String, > --> Records child PE having the record set.
//                if (rootSearchData.getStickyHdrSortData() != null && !rootSearchData.getStickyHdrSortData().isEmpty()) {
//                    List<SortData> stickyHdrList = rootSearchData.getStickyHdrSortData();
//                    Map<String, Object> currentStickyHdrMap = stickyHdrMap;
//                    for (int sortIndex = 0; sortIndex < stickyHdrList.size(); sortIndex++) {
//                        SortData sortData = stickyHdrList.get(sortIndex);
//                        // get the attribute element data.
//                        Object elementData = dbRow.get(sortData.getAttributePathExpn());
//                        // If it is the last element
//                        if (sortIndex == stickyHdrList.size() - 1) {
//                            if (currentStickyHdrMap.containsKey(elementData.toString())) {
//                                // Here if the last value of the sticky header is also present we should give back Record as return value
//                                Record record = (Record) currentStickyHdrMap.get(elementData.toString());
//                                recordSet = (List<Record>) record.getChildrenPEData().get(0).getRecordSet();
//        //                            recordSet = (List<Record>)currentStickyHdrMap.get(elementData.toString());
//                            } else {
//
//                                Record stickyRecord = new Record();
//                                // We have new stickyRecord add this to the map
//                                currentStickyHdrMap.put(elementData.toString(), stickyRecord);
//                                // So this stickyRecord should have attribute to data map...
//                                // And the fields we want to add should got to in a special way.
//                                // Loop through the stocky header sort data
//                                for (int innerSortIndex = 0; innerSortIndex < stickyHdrList.size(); innerSortIndex++) {
//                                    SortData innerSortData = stickyHdrList.get(sortIndex);
//                                    Object innerElementData = dbRow.get(innerSortData.getAttributePathExpn());
//                                    // Now we need to call the function which will add this attribute data
//                                    SearchField column = rootSearchData.getColumnLabelToColumnMap().get(innerSortData.getAttributePathExpn());
//                                    addColumnValueToResponseRow(stickyRecord, innerSortData.getAttributePathExpn(), innerElementData, dbRow, column, aliasTracker, processElementData.getMtPE(), baseTemplate, primaryTable.getTableAlias(), amRowPkToRSToAttributeData, recordState);
//                                }
//                                // Now this sticky record should give the correct record set
//                                // For that we need a child Process element
//                                ProcessElementData stickyHdrChildPE = new ProcessElementData();
//                                stickyHdrChildPE.setMtPE(rootSearchData.getMtPE());
//                                stickyHdrChildPE.setMtPEAlias(rootSearchData.getMtPEAlias());
//                                stickyRecord.getChildrenPEData().add(stickyHdrChildPE);
//                                recordSet = new ArrayList<>();
//                                stickyRecord.getChildrenPEData().get(0).setRecordSet(recordSet);
//                                // Now get the correct stickyRecord
//                                //recordSet = new ArrayList<>();
//
//                            }
//                        } else {
//                            if (currentStickyHdrMap.containsKey(elementData.toString())) {
//                                currentStickyHdrMap = (Map<String, Object>) currentStickyHdrMap.get(elementData.toString());
//                            } else {
//                                currentStickyHdrMap.put(elementData.toString(), new LinkedHashMap<String, Object>());
//                            }
//                        }
//                    }
//                    processElementData.setRecordSet(stickyHdrMap.values());
//                } else {
                    processElementData.setRecordSet(recordSet);
//                }
                /* For each row create a record
                 * 1. Each will have a db Primary key and functional primary key
                 * 2.
                 */
                // If is View is allowed we should get the correct permissions.
                /* Loop through all the columns */
                for (Map.Entry<String, Object> entry : dbRow.entrySet()) {
                    String columnLabel = entry.getKey();
                    Object columnValue = entry.getValue();
                    SearchField column = rootSearchData.getColumnLabelToColumnMap().get(columnLabel);
                    if (
//                            !columnLabel.contains(":") /* This is adding all the base columns */
                            attributePaths.contains(columnLabel) /* If the column is present in foreign table */
                            || columnLabel.equals(dbPrimaryKeyColumnLabel)
                            || columnLabel.equals(functionalPrimaryKeyColumnLabel)
                            || currentDependencyDOAs.containsKey(columnLabel)
                            || columnLabel.equals(column.getDbPrimaryKeyColumnLabel())
                            || isAccessControlAttribute(columnLabel)
                            || isBookmarkControlAttribute(columnLabel)
                            || isEndorseControlAttribute(columnLabel)
                            || columnLabel.endsWith("G11n")
                            || Util.isBaseStandardField(columnLabel)
                            ) {
                    /* Now we have column here */
                    /* Now resolve this column label and add it to the appropriate position in the processElementData */
                    /* Here we need to add the permissions, but the permissions are on the basis of record state*/
                        addColumnValueToResponseRow(responseRow, columnLabel, columnValue, dbRow, column, aliasTracker, processElementData.getMtPE(), baseTemplate, primaryTable.getTableAlias(), amRowPkToRSToAttributeData, recordState);
                        if (columnLabel.equals(dbPrimaryKeyColumnLabel)) {
                            dbPrimaryKeyValue = columnValue.toString();
                        }
                        if (columnLabel.equals(functionalPrimaryKeyColumnLabel)) {
                            functionalPrimaryKeyValue = columnValue.toString();
                        }
                        if(StringUtil.isDefined(rootSearchData.getSubjectUserPath())){
                            if(columnLabel.equals(rootSearchData.getSubjectUserPath())){
                                int alias = 0;
                                try {
                                    alias = TenantAwareCache.getMetaDataRepository().getAlias(columnLabel);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                                if(responseRow.getAttributeMap().get(String.valueOf(alias))!=null){
                                    String subjectUserID = responseRow.getAttributeMap().get(String.valueOf(alias)).getcV().toString();
                                    String loggedInUserID = TenantContextHolder.getUserContext().getPersonId();
                                    Map<String, Map<String, List<Relation>>> relationsMap = hierarchyService.getAllRelationBetweenBaseAndRelatedPerson(loggedInUserID, subjectUserID);
                                    List<Relation> relations = relationsMap.get(subjectUserID).get(loggedInUserID);
                                    responseRow.setRelations(relations);
                                    List<String> groupIDs = TenantAwareCache.getPersonRepository().getPersonDetails(subjectUserID).getPersonGroups().getGroupIds();
                                    responseRow.setSubjectUserGroups(groupIDs);
                                }
                            }
                        }
                    }
                }
                prepareResponseRow(responseRow, rootSearchData, aliasTracker, isRootPE);
                processElementData.getRecordSetInternal().add(responseRow);
                recordSet.add(responseRow);
                if (StringUtil.isDefined(functionalPrimaryKeyValue)) {
                    if (processElementData.getFunctionalPrimaryKeyToRecordMap().containsKey(functionalPrimaryKeyValue)) {
                        processElementData.getFunctionalPrimaryKeyToRecordMap().get(functionalPrimaryKeyValue).add(responseRow);
                    } else {
                        List<Record> recordList = new ArrayList<>();
                        recordList.add(responseRow);
                        processElementData.getFunctionalPrimaryKeyToRecordMap().put(functionalPrimaryKeyValue, recordList);
                    }
                }
                if (StringUtil.isDefined(dbPrimaryKeyValue)) {
                    processElementData.getDatabasePrimaryKeyToRecordMap().put(dbPrimaryKeyValue, responseRow);
                }
                // Loop through the children PE and build their ProcessElementData here itself
                for(PERequest childPERequest : peRequest.getChildrenPE()){
                    if(StringUtil.isDefined(childPERequest.getMtPEAlias()) && StringUtil.isDefined(childPERequest.getMtPE())) {
                        ProcessElementData childElementData = new ProcessElementData();
                        childElementData.setMtPE(childPERequest.getMtPE());
                        childElementData.setMtPEAlias(childPERequest.getMtPEAlias());
                        responseRow.getChildrenPEData().add(childElementData);
                    }
                }
            }
        }
        Map<String, Map<String, ResolvedColumnCondition>> columnConditions = uiLayoutService.getColumnConditions(user.getPersonId(), amRowPkToRSToAttributeData.keySet());
		// Now we have all the column conditions we need to loop through map and then list to update all the final data.
        for(Map.Entry<String,Map<String, List<AttributeData>>> entry : amRowPkToRSToAttributeData.entrySet()){
            String amRowId = entry.getKey();
            Map<String, List<AttributeData>> rsToAttributeData = entry.getValue();
            for(Map.Entry <String, List<AttributeData>> rsToAdEntry : rsToAttributeData.entrySet()) {
                String recordState = rsToAdEntry.getKey();
                for (AttributeData attributeData : rsToAdEntry.getValue()) {
                    if (StringUtil.isDefined(attributeData.getRowAccessPkId())) {
                        String columnLabel = attributeData.getColumnLabel();
                        if (attributeData.isInternal()) {
                            // If it is internal we can directly add it according treating column label as the doa
                            String doa = Util.getDOAFromAttributePath(columnLabel);
                            ResolvedColumnCondition columnCondition = columnConditions.get(amRowId).get(doa);
                            // Now that we have record state we need correct permissions to be added.
                            setColumnPermissions(attributeData, recordState, columnCondition);
                        } else {
                            List<String> doaPaths = Util.getAttributePathFromExpression(attributeData.getColumnLabel());
                            // Now here get the column label
                            if (doaPaths.size() >= 1) {
                                String doaPath = doaPaths.get(doaPaths.size() - 1);
                                String doa = Util.getDOAFromAttributePath(doaPath);
                                ResolvedColumnCondition columnCondition = columnConditions.get(amRowId).get(doa);
                                setColumnPermissions(attributeData, recordState, columnCondition);
                            }

                        }
                    }
                }
            }
        }
		return processElementData;
	}

    private void setColumnPermissions(AttributeData attributeData, String recordState, ResolvedColumnCondition columnCondition) {
        switch (recordState){
            case "CURRENT":
                attributeData.setRP(columnCondition.isCRp());
                attributeData.setUP(columnCondition.isCUp());
                break;
            case "HISTORY":
                attributeData.setRP(columnCondition.isHRp());
                attributeData.setUP(columnCondition.isHUp());
                break;
            case "FUTURE":
                attributeData.setRP(columnCondition.isFRp());
                attributeData.setUP(columnCondition.isFUp());
                break;
        }
    }

    private boolean isViewAllowed(Map<String, Object> dbRow, String recordState, String tableAlias) {
        boolean isViewAllowed = false;
        switch (recordState){
            case "CURRENT":
                 // Get the current view permission
                isViewAllowed = (boolean)dbRow.get(tableAlias+"#AMREDCURPERM#");
                 break;
            case "HISTORY":
                isViewAllowed = (boolean)dbRow.get(tableAlias+"#AMREDHISPERM#");
                break;
            case "FUTURE":
                isViewAllowed = (boolean)dbRow.get(tableAlias+"#AMREDFUTPERM#");
                break;
        }
        return isViewAllowed;
    }

    private boolean isAccessControlAttribute(String columnLabel) {
	    if(columnLabel.contains("#AMROWPK#") || columnLabel.contains("#AMSUBUSRPK#") || columnLabel.contains("#AMUPDCURPERM#") || columnLabel.contains("#AMUPDHISPERM#") || columnLabel.contains("#AMUPDFUTPERM#")|| columnLabel.contains("#AMREDCURPERM#")|| columnLabel.contains("#AMREDHISPERM#")|| columnLabel.contains("#AMREDFUTPERM#")|| columnLabel.contains("#AMDELCURPERM#")|| columnLabel.contains("#AMDELHISPERM#")|| columnLabel.contains("#AMDELFUTPERM#")){
	        return true;
        }
        return false;
    }
    private boolean isBookmarkControlAttribute(String columnLabel) {
        if(columnLabel.contains("#BOOKMARK#") || columnLabel.contains("#BOOKMARK-PK#") || columnLabel.contains("#BOOKMARK-lastMOD#") ){
            return true;
        }
        return false;
    }
    private boolean isEndorseControlAttribute(String columnLabel) {
        if(columnLabel.contains("#ENDORSE#") || columnLabel.contains("#ENDORSE-PK#") || columnLabel.contains("#ENDORSE-lastMOD#") || columnLabel.contains("#ENDORSE-versionID#")){
            return true;
        }
        return false;
    }

    /**
	 * This method adds all the property controls which are at MT_PE level.
	 *
	 * @param processElementData
	 * @param propertyControlAttributes
	 */
	private void addPCValuesToMTPE(ProcessElementData processElementData, List<SectionControlVO> propertyControlAttributes) {
		for(SectionControlVO control : propertyControlAttributes){
//			if(control.getMTPESpecificPC() || control.getAttributeSpecificPC()){
//				String expn = control.getPropertyControlExpn();
//				if(expn.contains("(")){
//					expn = expn.substring(0, expn.indexOf("("));
//				}
//				switch (expn){
//					case "getBTPEIcon":
//						AttributeData attributeData = new AttributeData();
//						processElementData.getMtPEPropertyCntrl().put(control.getPropertyControlExpn(), attributeData);
//						MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
//						MTPE mtPEDetails = metadataService.getMTPE(processElementData.getMtPE());
//						attributeData.setcV(mtPEDetails.getMtProcessElementIconCodeFkId());
//						IconVO icon = metadataService.getIconDetailsByIconCode(mtPEDetails.getMtProcessElementIconCodeFkId());
//						if(icon!=null) {
//							attributeData.setcV(icon);
//						}
//						/* Here get Mt PE Icon on the basis of metPE */
//						break;
//					case "getBTPESummaryTitle":
//						attributeData = new AttributeData();
//						processElementData.getMtPEPropertyCntrl().put(control.getPropertyControlExpn(), attributeData);
//						metadataService = TenantAwareCache.getMetaDataRepository();
//						mtPEDetails = metadataService.getMTPE(processElementData.getMtPE());
//						attributeData.setcV(mtPEDetails.getMtProcessElementNameG11nBigTxt());
//						/* Here get Mt PE Icon on the basis of metPE */
//						break;
//					case "getTotalRecordCount":
//						attributeData = new AttributeData();
//						attributeData.setcV(processElementData.getRecordsExist());
//						processElementData.getMtPEPropertyCntrl().put(control.getPropertyControlExpn(), attributeData);
//						break;
//					case "getBTPEHelpText":
//						attributeData = new AttributeData();
//						processElementData.getMtPEPropertyCntrl().put(control.getPropertyControlExpn(), attributeData);
//						metadataService = TenantAwareCache.getMetaDataRepository();
//						mtPEDetails = metadataService.getMTPE(processElementData.getMtPE());
//						attributeData.setcV(mtPEDetails.getMtProcessElementNameG11nBigTxt());
//						break;
//					default:
//				}
//			}
		}
	}

	public Map<String, AggregateData> createAggregateResult(Map<DefaultSearchData, SearchResult> aggregateSearchDataToSearchResultMap, String baseTemplate){
        Map<String, AggregateData> aggregateResult = new HashMap<>();
        for(Map.Entry<DefaultSearchData, SearchResult> entry : aggregateSearchDataToSearchResultMap.entrySet()){
			/* Get the search data and its corresponding result */
            DefaultSearchData aggregateSearchData = entry.getKey();
            SearchResult aggregateSearchResult = entry.getValue();
            AttributePathDetails aggregateAttributePathDetails = aggregateSearchData.getAggregateAttributePathDetails();
			/* This is the first attribute path */
            String aggregateAttributePath = aggregateAttributePathDetails.getAttributePath();
			/* This is the first result object */
            AggregateData rootAggregateData = new AggregateData();
            aggregateResult.put(aggregateAttributePath, rootAggregateData);
            List<GroupByAttributeDetails> groupByAttributes = aggregateAttributePathDetails.getGroupByAttributes();
            List<Map<String, Object>> aggregateRows = aggregateSearchResult.getData();
			/*
			 * Now we will loop through the result rows
			 * Each row might have group by columns and aggregate result
			 */
            for (Map<String,Object> aggregateRow : aggregateRows){
//                double result = (Double)aggregateRow.get(AGGREGATE_ALIAS);
                int resultType = 0;
				double result = 0;
                Object result1 = null;
				if(aggregateRow.get(aggregateAttributePathDetails.getAttributePath()) instanceof Long){
					result = ((Long)aggregateRow.get(aggregateAttributePathDetails.getAttributePath())).doubleValue();
					resultType = 1;
				}else if(aggregateRow.get(aggregateAttributePathDetails.getAttributePath()) instanceof BigDecimal){
					result = ((BigDecimal)aggregateRow.get(aggregateAttributePathDetails.getAttributePath())).doubleValue();
                    resultType = 1;
				}else if(aggregateRow.get(aggregateAttributePathDetails.getAttributePath()) instanceof byte[]){
                    result1 = new String((byte[]) aggregateRow.get(aggregateAttributePathDetails.getAttributePath()));
                    resultType = 2;
                }else if(aggregateRow.get(aggregateAttributePathDetails.getAttributePath()) instanceof Timestamp){
                    result1 = aggregateRow.get(aggregateAttributePathDetails.getAttributePath()).toString();
                    resultType = 2;
                }else{
				    result1 = aggregateRow.get(aggregateAttributePathDetails.getAttributePath());
                    resultType = 2;
                }

                /*
					 * If group by column exists
					 * Considering that groupByAttribute are in correct sequence
					 * 1. Loop through each group by attribute
					 * 2. Get its value see if the value exists in result
					 */
                if(resultType == 1) {
                    if (groupByAttributes != null && !groupByAttributes.isEmpty()) {
                        AggregateData previousAggregateData = rootAggregateData;
                        double updatedRootAggValue = (double)rootAggregateData.getValue() + result;
                        rootAggregateData.setValue(updatedRootAggValue);
                        for (int groupBySequence = 0; groupBySequence < groupByAttributes.size(); groupBySequence++) {
                            GroupByAttributeDetails groupByAttributeDetails = groupByAttributes.get(groupBySequence);
                            String groupByAttributePath = groupByAttributeDetails.getGroupByAttributePath();
                            String columnValue = "null";
                            if (aggregateRow.get(groupByAttributePath) != null)
                                columnValue = aggregateRow.get(groupByAttributePath).toString();
                            if (previousAggregateData.getGroupByValue().containsKey(columnValue)) {
                                previousAggregateData = previousAggregateData.getGroupByValue().get(columnValue);
                                double updatedAggValue = (double)previousAggregateData.getValue() + result;
                                previousAggregateData.setValue(updatedAggValue);
                            } else {
                                AggregateData newAggregateData = new AggregateData();
                                previousAggregateData.getGroupByValue().put(columnValue, newAggregateData);
                                newAggregateData.setValue(result);
                                previousAggregateData.setGroupByAttribute(groupByAttributePath);
                                previousAggregateData = newAggregateData;
                            }
                        }
                    } else {
                        rootAggregateData.setValue(result);
                    }
                }else{
                    if (groupByAttributes != null && !groupByAttributes.isEmpty()) {
                        AggregateData previousAggregateData = rootAggregateData;
                        for (int groupBySequence = 0; groupBySequence < groupByAttributes.size(); groupBySequence++) {
                            GroupByAttributeDetails groupByAttributeDetails = groupByAttributes.get(groupBySequence);
                            String groupByAttributePath = groupByAttributeDetails.getGroupByAttributePath();
                            String columnValue = "null";
                            if (aggregateRow.get(groupByAttributePath) != null)
                                columnValue = aggregateRow.get(groupByAttributePath).toString();
                            if (previousAggregateData.getGroupByValue().containsKey(columnValue)) {
                                previousAggregateData = previousAggregateData.getGroupByValue().get(columnValue);
                            } else {
                                AggregateData newAggregateData = new AggregateData();
                                previousAggregateData.getGroupByValue().put(columnValue, newAggregateData);
                                newAggregateData.setValue(result1);
                                previousAggregateData.setGroupByAttribute(groupByAttributePath);
                                previousAggregateData = newAggregateData;
                            }
                        }
                    }else {
                        rootAggregateData.setValue(result1);
                    }
                }
            }
        }
        return aggregateResult;
    }
	/**
     * @param responseRow
     * @param rootSearchData
     * @param aliasTracker
     * @param isRootPE
     */
	private void prepareResponseRow(Record responseRow, DefaultSearchData rootSearchData, JsonLabelAliasTracker aliasTracker, boolean isRootPE) {
        if(rootSearchData.isLogActivity() && isRootPE){
            logReadActivity(responseRow, rootSearchData, aliasTracker, isRootPE, null, null);
        }
//		addDataActionsToResponseRow(responseRow, rootSearchData, aliasTracker);
        // Add the workflow action steps
	}

    private void logReadActivity(Record responseRow, DefaultSearchData rootSearchData, JsonLabelAliasTracker aliasTracker, boolean isRootPE, Record parentRecord, String parentDomainObjectCode) {
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        /*
            private String activityLogActionCodeFkId;
            private String actionMsgInContextG11nBigText;
            private String actionMsgGenericG11nBigText;
         */
        UserContext user = TenantContextHolder.getUserContext();
        ActivityLog activityLog = new ActivityLog();
        activityLog.setActivityLogPkId(Common.getUUID());
        activityLog.setActivityAuthorPerson(user.getPersonId());
        activityLog.setMtPeCodeFkId(rootSearchData.getMtPE());
        activityLog.setProcessTypeCodeFkId(rootSearchData.getProcessTypeCode());
        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
        String baseTemplate = personService.getDefaultBTForGroup(rootSearchData.getMtPE(), user.getPersonId());
        EntityMetadataVOX metadataVOX = metadataService.getMetadataByProcessElement(baseTemplate,rootSearchData.getMtPE());
        activityLog.setDoIconCodeFkId(metadataVOX.getDoSummaryTitleIcon().getCode());
        activityLog.setDoRowPrimaryKeyFkId(responseRow.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get(metadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode()).getAliasNumber().toString()).getcV().toString());
        activityLog.setDoRowVersionFkId(responseRow.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get(metadataVOX.getVersoinIdAttribute().getDoAttributeCode()).getAliasNumber().toString()).getcV().toString());
        activityLog.setDisplayContextValueBigTxt(responseRow.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get(metadataVOX.getIsContextObjectField().getDoAttributeCode()).getAliasNumber().toString()).getcV().toString());
        activityLog.setDoNameG11nBigTxt(metadataVOX.getBtDOName());
        activityLog.setActionVisualMasterCode("READ");
        activityLog.setActivityDatetime(Common.getCurrentTimeStamp());
        activityLog.setDeleted(false);
        activityLog.setDtlLaunchTrgtMtPeCodeFkId(null);
        if(isRootPE) {
//            activityLog.setParentMetaProcessElement(null);
//            activityLog.setParentDOPrimaryKey(null);
//            activityLog.setParentDOVersionID(null);
        }else{
            // Set the parent data.
            MasterEntityMetadataVOX parentDOMeta = metadataService.getMetadataByDo(parentDomainObjectCode);
            MasterEntityAttributeMetadata primaryKeyAttribute = parentDOMeta.getPrimaryKeyAttribute();
            MasterEntityAttributeMetadata dbPkAttribute = parentDOMeta.getVersionIdAttribute();
            String parentDOPrimaryKey = parentRecord.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get(primaryKeyAttribute.getDoAttributeCode()).getAliasNumber().toString()).getcV().toString();
            String parentDOVersionId = parentRecord.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get(dbPkAttribute.getDoAttributeCode()).getAliasNumber().toString()).getcV().toString();
//            activityLog.setParentDOPrimaryKey(parentDOPrimaryKey);
//            activityLog.setParentDOVersionID(parentDOVersionId);
//            activityLog.setParentMetaProcessElement(parentDomainObjectCode);
        }
        Map<String, Object> doaValueMap = new HashMap<>();
        EntityMetadataVOX activityMetadataVOX = metadataService.getMetadataByProcessElement(null, rootSearchData.getLogActivityMTPE(), null);
        EntityMetadataVOX activityDtlMetadataVOX = metadataService.getMetadataByProcessElement(null, rootSearchData.getLogActivityMTPE(), null);
        Map<String, Object> contextDoaValueMap = Util.buildContextDoaValueMap(doaValueMap, activityLog, activityMetadataVOX, activityLog.getActivityDatetime().toString());
        AuditMsgFormat auditMsgFormat = metadataService.getAuditMsgFormat("READ", rootSearchData.getMtPE());
        Map<String, String> expressionToValueMap = getExpressionToValueMap(contextDoaValueMap, auditMsgFormat, responseRow, aliasTracker);
        Util.buildActivityLogMsg(activityLog, auditMsgFormat, expressionToValueMap);
        try {
            insertService.insertActivityLog(activityLog, activityMetadataVOX, activityDtlMetadataVOX, false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Map<String, String> getExpressionToValueMap(Map<String, Object> contextDoaValueMap, AuditMsgFormat auditMsgFormat, Record responseRow, JsonLabelAliasTracker aliasTracker) {
        Map<String, String> expressionToValueMap = new HashMap<>();
        populateExpressionToValueMap(auditMsgFormat.getMsgSyntaxYouInContextG11nBigTxt(), expressionToValueMap, contextDoaValueMap, responseRow, aliasTracker);
        populateExpressionToValueMap(auditMsgFormat.getMsgSyntaxYouOutOfContextG11nBigTxt(), expressionToValueMap, contextDoaValueMap, responseRow, aliasTracker);
        populateExpressionToValueMap(auditMsgFormat.getMsgSyntaxThirdPersonInContextG11nBigTxt(), expressionToValueMap, contextDoaValueMap, responseRow, aliasTracker);
        populateExpressionToValueMap(auditMsgFormat.getMsgSyntaxThirdPersonOutOfContextG11nBigTxt(), expressionToValueMap, contextDoaValueMap, responseRow, aliasTracker);
        return expressionToValueMap;
    }

    private void populateExpressionToValueMap(String message, Map<String, String> expressionToValueMap, Map<String, Object> contextDoaValueMap, Record responseRow, JsonLabelAliasTracker aliasTracker) {
	    if(StringUtil.isDefined(message)){
	        List<String> doas= Util.getAttributePathFromExpression(message);
	        for(String doaPath : doas){
	            if(!contextDoaValueMap.containsKey(doaPath)){
                    // This attribute's value is not present
                    if(doaPath.startsWith("ActivityLog.ActivityAuthorPerson:Person.")){
                        String personAttributePath = "!{"+doaPath.substring(doaPath.indexOf(":")+1)+"}";
                        Person person = TenantAwareCache.getPersonRepository().getPersonDetails(TenantContextHolder.getUserContext().getPersonId());
                        String value = person.getAdditionalDoaValues().get(personAttributePath);
                        expressionToValueMap.put(doaPath, value);
                    }else{
                        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
                        MasterEntityAttributeMetadata meta = metadataService.getMetadataByDoa(Util.getDOAFromAttributePath(doaPath));
                        if(meta.isGlocalized()) {
                            expressionToValueMap.put(doaPath, (String) responseRow.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get("!{" + doaPath + "}G11n").getAliasNumber().toString()).getcV());
                        }else{
                            expressionToValueMap.put(doaPath, (String) responseRow.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get("!{" + doaPath + "}").getAliasNumber().toString()).getcV());
                        }
                    }
                }else{
	                expressionToValueMap.put(doaPath, contextDoaValueMap.get(doaPath).toString());
                }
            }
        }
    }

    /**
     * @param responseRow
     * @param rootSearchData
     * @param aliasTracker
     */
	private void addDataActionsToResponseRow(Record responseRow, DefaultSearchData rootSearchData, JsonLabelAliasTracker aliasTracker) {
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		MasterEntityMetadataVOX rootMTMeta = metadataService.getMetadataByMtPE(rootSearchData.getMtPE());
		/*
		 * For getting data driven actions we need
		 * 1. Record State
		 * 2. Active State
		 * 3. Save State
		 * 4. isDeleted
		 * 5. workFlow Transaction status code
		 */
//        String recordStateValue = null;
        String activeStateValue = null;
        String saveStateValue = null;
        Boolean isDeleted = false;
        String workFlowTransactionValue = null;
//        if(rootMTMeta.getRecordStateAttribute()!=null) {
//            recordStateValue = responseRow.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get(rootMTMeta.getRecordStateAttribute().getDoAttributeCode()).getAliasNumber().toString()).getcV().toString();
//        }
        try {
            if (rootMTMeta.getActiveStateAttribute() != null) {
                activeStateValue = responseRow.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get(rootMTMeta.getActiveStateAttribute().getDoAttributeCode()).getAliasNumber().toString()).getcV().toString();
            }
            if (rootMTMeta.getSaveStateAttribute() != null) {
                saveStateValue = responseRow.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get(rootMTMeta.getSaveStateAttribute().getDoAttributeCode()).getAliasNumber().toString()).getcV().toString();
            }
            if (rootMTMeta.getIsDeletedAttribute() != null) {
                isDeleted = (Boolean) responseRow.getAttributeMap().get(aliasTracker.getDoaToJsonAliasMap().get(rootMTMeta.getIsDeletedAttribute().getDoAttributeCode()).getAliasNumber().toString()).getcV();
            }
            if (rootMTMeta.getWorkFlowTransactionStatusCodeAttribute() != null) {
                if (responseRow.getAttributeMap().get(rootMTMeta.getWorkFlowTransactionStatusCodeAttribute().getDoAttributeCode()).getcV() != null) {
                    workFlowTransactionValue = responseRow.getAttributeMap().get(rootMTMeta.getWorkFlowTransactionStatusCodeAttribute().getDoAttributeCode()).getcV().toString();
                }
            }
            List<DataActions> dataActions = metadataService.getDataActions(activeStateValue, saveStateValue, isDeleted, workFlowTransactionValue);
            responseRow.getDataActionsApplicability().addAll(dataActions);
        }catch (NullPointerException np){

        }
	}
	/**
     * @param responseRow
     * @param columnLabel
     * @param columnValue
     * @param dBrow
     * @param column
     * @param mtPE
     * @param baseTemplate
     * @param amRowPkToRSToAttributeData
     * @param recordState
     *
	 */
	private void addColumnValueToResponseRow(Record responseRow, String columnLabel, Object columnValue, Map<String, Object> dBrow, SearchField column, JsonLabelAliasTracker aliasTracker, String mtPE, String baseTemplate, String primaryTableAlias, Map<String, Map<String, List<AttributeData>>> amRowPkToRSToAttributeData, String recordState) {
		Map<String,AttributeData> resultMap = responseRow.getAttributeMap();
		AttributeData finalData = new AttributeData();
		finalData.setColumnLabel(columnLabel);
		finalData.setInternal(column.getInternal());
		String versionChangesColumnLabel = columnLabel.substring(0, columnLabel.lastIndexOf(".")+1)+VERSION_CHANGES_DOA;
		JsonNode versionChangesBlob = null;
		if(dBrow.containsKey(versionChangesColumnLabel)){
			versionChangesBlob = (JsonNode) dBrow.get(versionChangesColumnLabel);
		}
        String inlineChangesColumnLabel = columnLabel.substring(0, columnLabel.lastIndexOf(".")+1)+INLINE_CHANGES_DOA;
        JsonNode inlineBlob = null;
        if(dBrow.containsKey(versionChangesColumnLabel)){
            inlineBlob = (JsonNode) dBrow.get(inlineChangesColumnLabel);
        }
//		String dbPKValue = (String) DBrow.get(column.getDbPrimaryKeyColumnLabel());
//		ResolvedRowCondition rowCondition = dbPkToAccessConditions.get(dbPKValue);
//		ResolvedColumnCondition columnCondition = rowCondition.getDoaToColumnConditionMap().get(column.getDoAtributeCode());
//		Permission columnPermission = getCorrectColumnPermission(columnCondition, recordState);
//		ResolvedRowCondition rowCondition = new ResolvedRowCondition();
//		ResolvedColumnCondition columnCondition = new ResolvedColumnCondition();
//		Permission columnPermission = new Permission();
//        prepareAttributeData(column,columnLabel, columnValue, finalData, baseTemplate, mtPE, dBrow, amRowPkId);
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        if(columnValue!=null)
            finalData.setcV(columnValue);
        if(column.getDataType()!=null) {
            if (column.getDataType().matches("T_IMAGEID|T_ATTACH")) {
                if(columnValue!=null) {
                    AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
                    finalData.setURL(Util.getURLforS3(columnValue, s3Client));
                }
            }
            if (column.getDataType().equals("T_ICON")) {
                if (columnValue != null) {
                    IconVO icon = metadataService.getIconDetailsByIconCode(columnValue.toString());
                    if (icon != null) {
                        finalData.setcV(icon);
                    }
                }
            }
        }
        /* In final data add the time zone applicable if it is subject user timezone driven */
        List<String> doaList = new ArrayList<>();
        if (column.getInternal()) {
            doaList.add(column.getColumnLabel());
        } else {
            doaList = Util.getAttributePathFromExpression(column.getColumnLabel());
        }
        if (doaList.size() == 1) {
            String doaCode = Util.getDOAFromAttributePath(doaList.get(0));
            if (!StringUtil.isDefined(baseTemplate)) {
                UserContext user = TenantContextHolder.getUserContext();
                PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
                String modifiedMTPE = doaCode.split("\\.")[0];
                baseTemplate = personService.getDefaultBTForGroup(modifiedMTPE, user.getPersonId());
            }
            EntityAttributeMetadata doaMeta = metadataService.getMetadataByBtDoa(baseTemplate, doaCode);
            if(doaMeta.isUseSubjectPersonTimezone()){
                String subjectUserLabel = column.getTableAlias()+"#AMSUBUSRPK#";
                String subjectUserId = dBrow.get(subjectUserLabel).toString();
                PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
                Person person = personService.getPersonDetails(subjectUserId);
                finalData.setsUTZ(person.getPersonPreferences().getTimezoneCodeFkId());
            }
            MasterEntityMetadataVOX doMeta = metadataService.getMetadataByDo(doaMeta.getDomainObjectCode());
//            if(doMeta.isAccessControlGoverned()){
//                String accessControlRowPkLabel = column.getTableAlias()+"#AMROWPK#";
//                if(dBrow.get(accessControlRowPkLabel) != null) {
//                    String acRowPkId = dBrow.get(accessControlRowPkLabel).toString();
//                    finalData.setRowAccessPkId(acRowPkId);
//                    if (amRowPkToRSToAttributeData.containsKey(acRowPkId)) {
//                        amRowPkToRSToAttributeData.get(acRowPkId).get(recordState).add(finalData);
//                    } else {
//                        List<AttributeData> attributeDataList = new ArrayList<>();
//                        Map<String, List<AttributeData>> recordStateToAttributeList = new HashMap<>();
//                        recordStateToAttributeList.put(recordState, attributeDataList);
//                        attributeDataList.add(finalData);
//                        amRowPkToRSToAttributeData.put(acRowPkId, recordStateToAttributeList);
//                    }
//                }
//            }
//            if(versionChangesBlob!=null){
//			    Object oldValue = versionChangesBlob.get(doaCode);
//			    if(oldValue!=null)
//				    finalData.setoV(oldValue);
//		    }
            if(inlineBlob!=null){
                Object oldValue = inlineBlob.get(doaCode);
                if(oldValue!=null)
                    finalData.setoV(oldValue);
            }
        }
        addToResultMap(aliasTracker, columnLabel, finalData, resultMap, mtPE, column);
        // TODO only the correct permissions should be added as update and delete permission.
//		if(columnLabel.equals(primaryTableAlias+"#AMUPDPERM#")){
        if(updatePermissionColumnLabelCorrect(recordState, columnLabel, primaryTableAlias)){
            addToResultMap(aliasTracker, "#AMUPDPERM#", finalData, resultMap, mtPE, column);
        }
//        if(columnLabel.equals(primaryTableAlias+"#AMDELPERM#")){
        if(deletePermissionColumnLabelCorrect(recordState, columnLabel, primaryTableAlias)){
            addToResultMap(aliasTracker, "#AMDELPERM#", finalData, resultMap, mtPE, column);
        }
	}

    private boolean updatePermissionColumnLabelCorrect(String recordState, String columnLabel, String primaryTableAlias) {
	    boolean result = false;
	    if(StringUtil.isDefined(recordState)) {
            switch (recordState) {
                case "CURRENT":
                    if (columnLabel.equals(primaryTableAlias + "#AMUPDCURPERM#"))
                        result = true;
                    break;
                case "HISTORY":
                    if (columnLabel.equals(primaryTableAlias + "#AMUPDHISPERM#"))
                        result = true;
                    break;
                case "FUTURE":
                    if (columnLabel.equals(primaryTableAlias + "#AMUPDFUTPERM#"))
                        result = true;
                    break;
            }
        }
        return result;
    }

    private boolean deletePermissionColumnLabelCorrect(String recordState, String columnLabel, String primaryTableAlias) {
        boolean result = false;
        if(StringUtil.isDefined(recordState)) {
            switch (recordState) {
                case "CURRENT":
                    if (columnLabel.equals(primaryTableAlias + "#AMDELCURPERM#"))
                        result = true;
                    break;
                case "HISTORY":
                    if (columnLabel.equals(primaryTableAlias + "#AMDELHISPERM#"))
                        result = true;
                    break;
                case "FUTURE":
                    if (columnLabel.equals(primaryTableAlias + "#AMDELFUTPERM#"))
                        result = true;
                    break;
            }
        }
        return result;
    }

    private void addToResultMap(JsonLabelAliasTracker aliasTracker, String columnLabel, AttributeData finalData, Map<String, AttributeData> resultMap, String mtPE, SearchField column) {
        Map<String, AliasTrackerHelper> doaToJsonAliasMap = aliasTracker.getDoaToJsonAliasMap();
        if(doaToJsonAliasMap.containsKey(columnLabel)){
            AliasTrackerHelper trackerHelper = doaToJsonAliasMap.get(columnLabel);
            Integer columnLabelAlias = trackerHelper.getAliasNumber();
            resultMap.put(columnLabelAlias.toString(), finalData);
        }else{
            int aliasTrackerNumber = 0;
            try {
                aliasTrackerNumber = TenantAwareCache.getMetaDataRepository().getAlias(columnLabel);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            resultMap.put(Integer.toString(aliasTrackerNumber), finalData);
            AliasTrackerHelper trackerHelper = new AliasTrackerHelper(aliasTrackerNumber, mtPE, column.getInternal());
            doaToJsonAliasMap.put(columnLabel, trackerHelper);
//            aliasTracker.increaseCurrentCount();
        }
    }

	/**
     * @param column
     * @param doa
     * @param columnValue
     * @param finalData
     * @param baseTemplate
     * @param mtPE
     * @param dBrow
     * @param amRowPkId
     *//*
	private void prepareAttributeData(SearchField column, String doa, Object columnValue, AttributeData finalData, String baseTemplate, String mtPE, Map<String, Object> dBrow, Set<String> amRowPkId) {
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		if(columnValue!=null)
			finalData.setcV(columnValue);
        if(column.getDataType()!=null) {
            if (column.getDataType().matches("T_IMAGEID|T_ATTACH")) {
                AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
                finalData.setURL(Util.getURLforS3(columnValue, s3Client));
            }
            if (column.getDataType().equals("T_ICON")) {
                if (columnValue != null) {
                    IconVO icon = metadataService.getIconDetailsByIconCode(columnValue.toString());
                    if (icon != null) {
                    	finalData.setcV(icon);
                    }
                }
            }
        }
        *//* In final data add the time zone applicable if it is subject user timezone driven *//*
		List<String> doaList = new ArrayList<>();
		if (column.getInternal()) {
			doaList.add(column.getColumnLabel());
		} else {
			doaList = Util.getAttributePathFromExpression(column.getColumnLabel());
		}
		if (doaList.size() == 1) {
			String doaCode = Util.getDOAFromAttributePath(doaList.get(0));
			if (!StringUtil.isDefined(baseTemplate)) {
				UserContext user = TenantContextHolder.getUserContext();
				PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
				String modifiedMTPE = doaCode.split("\\.")[0];
				baseTemplate = personService.getDefaultBTForGroup(modifiedMTPE, user.getPersonId());
			}
			EntityAttributeMetadata doaMeta = metadataService.getMetadataByBtDoa(baseTemplate, doaCode);
			if(doaMeta.isUseSubjectPersonTimezone()){
                String subjectUserLabel = column.getTableAlias()+"#AMSUBUSRPK#";
                String subjectUserId = dBrow.get(subjectUserLabel).toString();
                PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
                Person person = personService.getPersonDetails(subjectUserId);
                finalData.setsUTZ(person.getTimezoneCodeFkId());
			}
            MasterEntityMetadataVOX doMeta = metadataService.getMetadataByDo(doaMeta.getDomainObjectCode());
			if(doMeta.isAccessControlGoverned()){
                String accessControlRowPkLabel = column.getTableAlias()+"#AMROWPK#";
                String acRowPkId = dBrow.get(accessControlRowPkLabel).toString();
            }
		}
//		if(versionChangesBlob!=null){
//			Object oldValue = versionChangesBlob.get(doa);
//			if(oldValue!=null)
//				finalData.setoV(oldValue);
//		}
	}*/
	/**
	 * This method merges the child data with parent
	 * @param childSearchData
     * @param childPEData
	 * @param aliasTracker
	 * @param propertyControlAttributes
	 */
	@SuppressWarnings("unchecked")
	public void mergeParentAndChildData(DefaultSearchData childSearchData, ProcessElementData childPEData, JsonLabelAliasTracker aliasTracker, List<SectionControlVO> propertyControlAttributes) {
		/*
		 * loop through the child PE records
		 */
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		Map<String, Object> dependencyMappedParentData = childSearchData.getDependencyMappedParentData();
		String mtPE = childSearchData.getMtPE();
		String mtPEAlias = childPEData.getMtPEAlias();
//		String baseTemplateId = childSearchData.getBaseTemplateId();
		MasterEntityMetadataVOX childBTMeta = metadataService.getMetadataByMtPE(mtPE);
		List<DependencyKeysHelper> dependencyKeyList = childSearchData.getDependencyKeyList();
		Map<String, Object> tempDependencyMap = dependencyMappedParentData;
		for(Record childRecord : childPEData.getRecordSetInternal()){
			for(int i = 0; i<dependencyKeyList.size(); i++){
				DependencyKeysHelper dependencyAttribute = dependencyKeyList.get(i);
				String parentDOACode = dependencyAttribute.getParentDOACode();
				MasterEntityAttributeMetadata parentDoaMeta =  metadataService.getMetadataByDoa(parentDOACode);
				AliasTrackerHelper aliasTrackerHelper = aliasTracker.getDoaToJsonAliasMap().get(dependencyAttribute.getCurrentDOACode());
				Integer jsonLabelAlias = aliasTrackerHelper.getAliasNumber();
				AttributeData dependencyKeyValue = childRecord.getAttributeMap().get(jsonLabelAlias.toString());
				String currentValue = dependencyKeyValue.getcV().toString();
				if(tempDependencyMap.containsKey(currentValue)){
					if(i == dependencyKeyList.size()-1){
						/* This is the last record add the value here */
						List<Record> mappedRecordList = (List<Record>) tempDependencyMap.get(currentValue);
						for(Record parentRecord : mappedRecordList){
							/* To each parent record add these attributes */
							List<ProcessElementData> childrenPEData = parentRecord.getChildrenPEData();
							ProcessElementData childPEForTheRecord = null;
							/* Loop through the process elements and check if this PE exists or not */
							for(ProcessElementData pEData : childrenPEData){
								if(pEData.getMtPEAlias().equals(mtPEAlias)){
									childPEForTheRecord = pEData;
									break;
								}
							}
							if(childPEForTheRecord == null){
								childPEForTheRecord = new ProcessElementData();
//								childPEForTheRecord.setBaseTemplateId(baseTemplateId);
								childPEForTheRecord.setMtPE(mtPE);
								childPEForTheRecord.setMtPEAlias(mtPEAlias);
								childPEForTheRecord.setMtPEPropertyCntrl(childPEData.getMtPEPropertyCntrl());
                                childPEForTheRecord.setCtrlIdToGridData(childPEData.getCtrlIdToGridData());
                                childPEForTheRecord.setRecordSet(new ArrayList<>());
								childrenPEData.add(childPEForTheRecord);
							}
							childPEForTheRecord.getRecordSetInternal().add(childRecord);
							if(childPEForTheRecord.getRecordSet()==null) {
                                childPEForTheRecord.setRecordSet(new ArrayList<>());
                                ((List) childPEForTheRecord.getRecordSet()).add(childRecord);
                            }else{
                                ((List) childPEForTheRecord.getRecordSet()).add(childRecord);
                            }
                            if(childSearchData.isLogActivity()) {
                                logReadActivity(childRecord, childSearchData, aliasTracker, false, parentRecord, parentDoaMeta.getDomainObjectCode());
                            }
						}
                        tempDependencyMap = dependencyMappedParentData;
						/* Loop through this result and add the values here */
					}else{
						tempDependencyMap = (Map<String, Object>) tempDependencyMap.get(currentValue);
					}
				}else{
					/* TODO Throw error */
				}
			}
		}
		if(childPEData.getAggregateResult()!=null && !childPEData.getAggregateResult().isEmpty()) {
			if(dependencyMappedParentData== null)
				dependencyMappedParentData = childSearchData.getAggregateStmts().get(0).getDependencyMappedParentData();
            for (Map.Entry<String, AggregateData> entry : childPEData.getAggregateResult().entrySet()) {
                Map<String, AggregateData> startingMap = entry.getValue().getGroupByValue();
                String attributePath = entry.getKey();
                addAggregateResultToDependencyMap(startingMap, dependencyKeyList.size(), dependencyMappedParentData, 0, attributePath, propertyControlAttributes, mtPE, mtPEAlias);
            }
        }
//        }
//            for (Map.Entry<String, AggregateData> entry : childPEData.getAggregateResult().entrySet()){
//                String attributePath = entry.getKey();
//                Map<String, AggregateData> rootAggResultMap = entry.getValue().getGroupByValue();
//                for (int i = 0; i < dependencyKeyList.size(); i++) {
//
//                }
////                for(Map.Entry<String, AggregateData> subEntry : rootAggResultMap.entrySet()) {
////
////                    /* the current value should be fetched from root Map*/
////                        if (i == 0) {
////                            String currentValue = subEntry.getKey();
////
////                        }
////                    }
//            }
////                Map<String, AggregateData> previousAggResultMap = rootAggResultMap;
//
////
////                        DependencyKeysHelper dependencyAttribute = dependencyKeyList.get(i);
////                        String currentValue = subEntry.getKey();
////                        AttributeData
////                    }
////                    previousAggResultMap = subEntry.getValue().getGroupByValue();
////                }
//            }
//        }
	}

    private void addAggregateResultToDependencyMap(Map<String, AggregateData> startingMap, int dependencyKeySize, Map<String, Object> dependencyMappedParentData, int currentIndex, String path, List<SectionControlVO> propertyControlAttributes, String mtPE, String mtPEAlias) {
        for(String key : startingMap.keySet()){
            if(dependencyKeySize-1 == currentIndex){
                // Here this is the place where we shall add aggregtate result to the dependency map.
                AggregateData finalAggregateData = startingMap.get(key);
                List<Record> mappedRecordList = (List<Record>) dependencyMappedParentData.get(key);
                for(Record parentRecord : mappedRecordList) {
                	List<ProcessElementData> childrenDataPE = parentRecord.getChildrenPEData();
                	boolean added = false;
                	for(ProcessElementData childPE : childrenDataPE){
						if(childPE.getMtPEAlias().equals(mtPEAlias)){
							childPE.getAggregateResult().put(path, finalAggregateData);
							added = true;
						}
					}
					if(!added){
                		ProcessElementData newPEData = new ProcessElementData();
                		newPEData.setMtPE(mtPE);
                		newPEData.setMtPEAlias(mtPEAlias);
						parentRecord.getChildrenPEData().add(newPEData);
						newPEData.getAggregateResult().put(path, finalAggregateData);
						addPCValuesToMTPE(newPEData, propertyControlAttributes);
					}
//                    parentRecord.getAggregateResult().put(attributePath, finalAggregateData);
                }
            }else {
                Map<String, AggregateData> destMap = startingMap.get(key).getGroupByValue();
                Map<String, Object> currentDependencyMap = (Map<String, Object>)dependencyMappedParentData.get(key);
                addAggregateResultToDependencyMap(destMap, dependencyKeySize, currentDependencyMap,currentIndex+1, path, propertyControlAttributes, mtPE, mtPEAlias);
            }
        }
    }

    /**
     * This method response and build the hierarchical response.
     * It assumes that the response received is a sticky header response with first primary key as the first sticky header.
     * It only works for building root hierarchy.
     * @param response
     * @param mtPE
     * @return
     */
    public static void buildRootHierarchy(UnitResponseData response, String mtPE) {
        ProcessElementData peData = response.getPEData().get(mtPE);
        Map<String,List<Record>> recordMap = (Map<String,List<Record>>)peData.getRecordSet();
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadataVOX rootMetadata = metadataService.getMetadataByMtPE(mtPE);
        Map<String, DoaMeta> labelToAliasMap = response.getLabelToAliasMap();
        MasterEntityAttributeMetadata hierarchyKeyAttribute =rootMetadata.getHierarchyKeyAttribute();
        List<Record> hierarchicalRecordSet = new ArrayList<>();
        for(Map.Entry<String,List<Record>> entry : recordMap.entrySet() ){
            List<Record> recordList = entry.getValue();
            for(Record record : recordList){
                // Now that we have record we need to find out its hierarchical parent.
                AttributeData parentData = record.getAttributeMap().get(labelToAliasMap.get(hierarchyKeyAttribute.getDoAttributeCode()).getAlias());
                if(parentData.getcV()!=null){
                    String parentId = parentData.getcV().toString();
                    // If the parent exist. Find out if the response has the result for its parent or not.
                    if(recordMap.containsKey(parentId)){
                        // Add this record to parent's children PE
                        List<Record> parentRecords = recordMap.get(parentId);
                        for(Record parentRecord : parentRecords){
                            List<ProcessElementData> childrenPEData = parentRecord.getChildrenPEData();
                                boolean isRecordAdded = false;
                                for(ProcessElementData childPEData : childrenPEData){
                                    if(childPEData.getMtPE().equals(mtPE)){
                                        ((List<Record>)childPEData.getRecordSet()).add(record);
                                        isRecordAdded = true;
                                    }
                                }
                                if(!isRecordAdded){
                                    ProcessElementData childPEData = new ProcessElementData();
                                    List<Record> childRecordSet = new ArrayList<>();
                                    childRecordSet.add(record);
                                    childPEData.setMtPE(mtPE);
                                    childPEData.setRecordSet(childRecordSet);
                                    childrenPEData.add(childPEData);
                                }
                            }
                    }else{
                        hierarchicalRecordSet.add(record);
                    }
                }else{
                    hierarchicalRecordSet.add(record);
                }
            }
        }
        peData.setRecordSet(hierarchicalRecordSet);
    }

	public AccessManagerDAO getAccessManagerDAO() {
		return accessManagerDAO;
	}

	public void setAccessManagerDAO(AccessManagerDAO accessManagerDAO) {
		this.accessManagerDAO = accessManagerDAO;
	}

	public UILayoutService getUiLayoutService() {
		return uiLayoutService;
	}

	public void setUiLayoutService(UILayoutService uiLayoutService) {
		this.uiLayoutService = uiLayoutService;
	}

    public InsertService getInsertService() {
        return insertService;
    }

    public void setInsertService(InsertService insertService) {
        this.insertService = insertService;
    }

    public void setHierarchyService(HierarchyService hierarchyService) {
        this.hierarchyService = hierarchyService;
    }
}
