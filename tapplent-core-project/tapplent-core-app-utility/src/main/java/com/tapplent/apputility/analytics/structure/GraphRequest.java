package com.tapplent.apputility.analytics.structure;

/**
 * Created by tapplent on 23/08/17.
 */
public class GraphRequest {
    private String metricAxisMapID;
    private String periodStartDate;
    private String periodEndDate;
    private String calendarPeriodType;
    private String calendarPeriodUnit;
    private boolean isEOP;


    public String getMetricAxisMapID() {
        return metricAxisMapID;
    }

    public void setMetricAxisMapID(String metricAxisMapID) {
        this.metricAxisMapID = metricAxisMapID;
    }

    public String getPeriodStartDate() {
        return periodStartDate;
    }

    public void setPeriodStartDate(String periodStartDate) {
        this.periodStartDate = periodStartDate;
    }

    public String getPeriodEndDate() {
        return periodEndDate;
    }

    public void setPeriodEndDate(String periodEndDate) {
        this.periodEndDate = periodEndDate;
    }

    public String getCalendarPeriodType() {
        return calendarPeriodType;
    }

    public void setCalendarPeriodType(String calendarPeriodType) {
        this.calendarPeriodType = calendarPeriodType;
    }

    public String getCalendarPeriodUnit() {
        return calendarPeriodUnit;
    }

    public void setCalendarPeriodUnit(String calendarPeriodUnit) {
        this.calendarPeriodUnit = calendarPeriodUnit;
    }

    public boolean isEOP() {
        return isEOP;
    }

    public void setEOP(boolean EOP) {
        isEOP = EOP;
    }
}
