package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.tapplent.platformutility.layout.valueObject.GroupActionTxnPropertiesVO;

public class GroupActionTxnProperties {
	private String groupActionTxnPropertiesId;
	private String groupActionTxnId;
	private String rule;
	private String property;
	private String propertyValue;
	public String getGroupActionTxnPropertiesId() {
		return groupActionTxnPropertiesId;
	}
	public void setGroupActionTxnPropertiesId(String groupActionTxnPropertiesId) {
		this.groupActionTxnPropertiesId = groupActionTxnPropertiesId;
	}
	public String getGroupActionTxnId() {
		return groupActionTxnId;
	}
	public void setGroupActionTxnId(String groupActionTxnId) {
		this.groupActionTxnId = groupActionTxnId;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
	public static GroupActionTxnProperties fromVO(GroupActionTxnPropertiesVO actionTxnPropertiesVO){
		GroupActionTxnProperties actionTxnProperties = new GroupActionTxnProperties();
		BeanUtils.copyProperties(actionTxnPropertiesVO, actionTxnProperties);
		return actionTxnProperties;
	}
}
