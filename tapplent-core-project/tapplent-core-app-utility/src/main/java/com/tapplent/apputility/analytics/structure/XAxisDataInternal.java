package com.tapplent.apputility.analytics.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 19/12/16.
 */
public class XAxisDataInternal {
    private String displayLabel;
    private String mappedLabel;
    private List<XAxisData> subXDataSeries = new ArrayList<>();

    public XAxisDataInternal(String mappedLabel,String displayLabel) {
        this.mappedLabel = mappedLabel;
        this.displayLabel = displayLabel;
    }

    public String getDisplayLabel() {
        return displayLabel;
    }

    public void setDisplayLabel(String displayLabel) {
        this.displayLabel = displayLabel;
    }

    public List<XAxisData> getSubXDataSeries() {
        return subXDataSeries;
    }

    public void setSubXDataSeries(List<XAxisData> subXDataSeries) {
        this.subXDataSeries = subXDataSeries;
    }

    public String getMappedLabel() {
        return mappedLabel;
    }

    public void setMappedLabel(String mappedLabel) {
        this.mappedLabel = mappedLabel;
    }
}
