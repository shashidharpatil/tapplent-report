package com.tapplent.apputility.thirdparty.service;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.transaction.annotation.Transactional;
import com.tapplent.apputility.data.service.ServerRuleManager;
import com.tapplent.apputility.data.structure.PEData;
import com.tapplent.apputility.data.structure.RequestData;
import com.tapplent.apputility.data.structure.UnitRequestData;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.thirdPartyApp.gmail.EverNoteDAO;
import com.tapplent.platformutility.thirdPartyApp.gmail.Task;
import com.tapplent.platformutility.thirdPartyApp.gmail.ThirdPartyUtil;

public class GoogleTaskServiceImpl implements GoogleTaskService {
	 private EverNoteDAO evernoteDAO;
	 private ServerRuleManager serverRuleManager;
	 public ServerRuleManager getServerRuleManager() {
		return serverRuleManager;
	}


	public void setServerRuleManager(ServerRuleManager serverRuleManager) {
		this.serverRuleManager = serverRuleManager;
	}


	public void setEvernoteDAO(EverNoteDAO evernoteDAO) {
			this.evernoteDAO = evernoteDAO;
		}

	
public static String getDefaultListId(String access_token) throws ClientProtocolException, IOException, JSONException{
	
	HttpGet get = new HttpGet("https://www.googleapis.com/tasks/v1/users/@me/lists");
	get.addHeader("Content-Type", "application/json");
	get.addHeader("Authorization", "Bearer "+access_token);
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(get);
    String json = EntityUtils.toString(response.getEntity());
    JSONObject responseObject = new JSONObject(json);
   JSONArray taskListArray = responseObject.getJSONArray("items");
   JSONObject taskList = taskListArray.getJSONObject(0);
   String defaultListId = taskList.getString("id");
   
    //int status_code = response.getStatusLine().getStatusCode();
	return defaultListId;
}
private String createNewTask(Task task, String access_token, String taskId,String personPkId) throws ClientProtocolException, IOException, JSONException{
	
	String defaultTaskList = getDefaultListId(access_token);
	StringBuilder url = new StringBuilder("https://www.googleapis.com/tasks/v1/lists/");
	url.append(defaultTaskList).append("/tasks");
	HttpPost post = new HttpPost(url.toString());
	post.addHeader("Content-Type", "application/json");
	post.addHeader("Authorization", "Bearer "+access_token);
	JSONObject jsonRequestBody = new JSONObject();
//	String title = Util.getG11nValue(task.getTaskTitleG11nBigTxt(),personPkId);
//	String notes = Util.getG11nValue(task.getTaskDescCommentG11nBigTxt(),personPkId);
	String title = task.getTaskTitleG11nBigTxt();
	String notes = task.getTaskDescCommentG11nBigTxt();		
	String taskStatusCode = task.getTaskStatusCodeFkId();
	Timestamp taskDueDate = task.getTaskEndDatetime();
	 SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
	 String taskDueDateTime =  fmt.format(taskDueDate).toString();
	 if(taskStatusCode.equals("COMPLETED")){
		 jsonRequestBody.put("status","completed");
	 }
	jsonRequestBody.put("title", title);
	if(notes.contains("#tapplent")){
		jsonRequestBody.put("notes", notes);
	}else{
		jsonRequestBody.put("notes", notes+"\n"+"#tapplent");
	}
	jsonRequestBody.put("due",taskDueDateTime);
	StringEntity entity = new StringEntity(jsonRequestBody.toString());
	post.setEntity(entity);
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(post);
    JSONObject responseObject = new  JSONObject(EntityUtils.toString(response.getEntity()));
     String task_id = (String) responseObject.get("id");
    
    return task_id;
}
private  void updateTheGoogleTask(String access_token,Task tapplentTask,String googleTaskId,String personPkId) throws ClientProtocolException, IOException, JSONException{
	//https://www.googleapis.com/tasks/v1/lists/tasklist/tasks/task
	deleteTheGoogleTask(access_token, googleTaskId);
	String newGoogleTaskId = createNewTask(tapplentTask, access_token, tapplentTask.getTaskPkId(), personPkId);

	
	String taskList = getDefaultListId(access_token);
	StringBuilder url = new StringBuilder("https://www.googleapis.com/tasks/v1/lists/");
	url.append(taskList);
	HttpPut put = new HttpPut(url.toString());
	put.addHeader("Content-Type", "application/json");
	put.addHeader("Authorization", "Bearer "+access_token);
	JSONObject jsonRequestBody = new JSONObject();
	String title = Util.getG11nValue(tapplentTask.getTaskTitleG11nBigTxt(),personPkId);
	String notes = Util.getG11nValue(tapplentTask.getTaskDescCommentG11nBigTxt(),personPkId);
	String taskStatusCode = tapplentTask.getTaskStatusCodeFkId();
	Timestamp taskDueDate = tapplentTask.getTaskEndDatetime();
	 SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
	 String taskDueDateTime =  fmt.format(taskDueDate).toString();
	 if(taskStatusCode.equals("COMPLETED")){
		 jsonRequestBody.put("status","completed");
	 }else{
		 jsonRequestBody.put("status","needsAction");
	 }
	jsonRequestBody.put("title", title);
	jsonRequestBody.put("notes", notes);
	jsonRequestBody.put("due",taskDueDateTime);
	StringEntity entity = new StringEntity(jsonRequestBody.toString());
	put.setEntity(entity);
	HttpClient client = HttpClientBuilder.create().build();
	HttpResponse response;
	response = client.execute(put);
		
}
public static void deleteTheGoogleTask(String access_token,String googleTaskId) throws ClientProtocolException, IOException, JSONException{
	//https://www.googleapis.com/tasks/v1/lists/tasklist/tasks/task

String defaultTaskListId = getDefaultListId(access_token);
StringBuilder url = new StringBuilder("https://www.googleapis.com/tasks/v1/lists/");
url.append(defaultTaskListId).append("/tasks/").append(googleTaskId);
HttpDelete delete = new HttpDelete(url.toString());
delete.addHeader("Content-Type", "application/json");
delete.addHeader("Authorization", "Bearer "+access_token);
HttpClient client = HttpClientBuilder.create().build();
HttpResponse response;
response = client.execute(delete);
	
}
private JSONObject getGoogleTasks(String access_token) throws ClientProtocolException, IOException, JSONException{
	String defaultTaskListId = getDefaultListId(access_token);
	HttpGet get = new HttpGet("https://www.googleapis.com/tasks/v1/lists/"+defaultTaskListId+"/tasks?showDeleted=true");
	get.addHeader("Content-Type", "application/json");
	get.addHeader("Authorization", "Bearer "+access_token);
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(get);
    String json = EntityUtils.toString(response.getEntity());
    JSONObject responseObject = new JSONObject(json);
    return responseObject;
}
@Override
@Transactional
public void taskSynch(String personPkId) throws ClientProtocolException, IOException, JSONException{
	
	
	Map<String,Object> columnNameValueMap = evernoteDAO.isSynchNeeded(personPkId,"T_PFM_TAP_TASK_SYNCH","TASK_SYNCH_PK_ID");
	boolean isSyncNeeded = (boolean) columnNameValueMap.get("isSynchNeeded");
	String integrationAppFkId = (String) columnNameValueMap.get("integrationAppName");
	String taskSyncPkId = (String) columnNameValueMap.get("syncPkId");
	Timestamp lastModifiedDateTime1 = (Timestamp) columnNameValueMap.get("lastModifiedDateTime");
	 SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 String lastModifiedDateTime =  fmt.format(lastModifiedDateTime1).toString();
	List<String> taskPkId = evernoteDAO.getEventPkId(taskSyncPkId,"T_PFM_TAP_TASK_SYNCH_DTLS","TASK_FK_ID","TASK_SYNCH_FK_ID");
	Map<String,String> tokens = evernoteDAO.getToken(integrationAppFkId,personPkId);
	String accesstoken = tokens.get("accessToken");
	String refreshToken = tokens.get("refreshToken");
	boolean flag = ThirdPartyUtil.isTokenValid(accesstoken);
	if(flag==false){
		accesstoken = ThirdPartyUtil.getAccessToken(refreshToken);
	}
	
				
				JSONObject tasks = getGoogleTasks(accesstoken);
				JSONArray tasksArray = tasks.getJSONArray("items");
				
		for(int i = 0; i<=tasksArray.length()-1;i++){
			
		    		JSONObject googleTask = (JSONObject) tasksArray.get(i);
		    		String googleTaskId = googleTask.getString("id");
		    		String googleTaskTitle = "";
		    		String googleTaskNotes = "";
		    if(googleTask.getString("title") != null && !googleTask.getString("title").isEmpty()){
		    	
		    	googleTaskTitle = googleTask.getString("title");
		    	if(googleTask.has("notes")){
		     		 googleTaskNotes = googleTask.getString("notes");
		     		}
		     	if(googleTaskNotes.contains("#tapplent")){
		     		
		     		
		    		String tapplentTaskId = evernoteDAO.getEventFkIdFromIntegration(googleTaskId,"T_PFM_TAP_TASK_INTGN","TASK_FK_ID");
		    		
		    		
			    		if(tapplentTaskId != null && !tapplentTaskId.isEmpty() ){
			    			String mtPE = "Task";
				    		EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
				    		Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
				    		 Task tapplentTask = evernoteDAO.getTask(tapplentTaskId,attributeMetaMap,personPkId);
							
								if(googleTask.has("deleted")){
									deleteTheTapplentTask(tapplentTaskId,personPkId);
									
								}else if(tapplentTask.isDeleted()){
									deleteTheGoogleTask(accesstoken,googleTaskId);
					    			
					    		}else{
								
									//Retrieve the lastModifiedtime of meeting in tapplent and GoogleCalender
									Timestamp tapplentTaskLastModifiedTime = tapplentTask.getLastModifiedDatetime();
									DateTime dateTime = new DateTime(googleTask.getString("updated"), DateTimeZone.forOffsetHoursMinutes(5, 30));
									DateTimeFormatter dateFormatter1 = ISODateTimeFormat.hourMinuteSecondFraction();
									DateTimeFormatter dateFormatter2 = ISODateTimeFormat.date();
									String dateString = dateFormatter2.print(dateTime);
									String timeString = dateFormatter1.print(dateTime);
									String timeStamp = dateString+" "+timeString;
									Timestamp googleTaskLastModifiedTime = Timestamp.valueOf(timeStamp);
									if(tapplentTaskLastModifiedTime.after(googleTaskLastModifiedTime)){
										//Update the tapplent Task record onto the GoogleTask
										updateTheGoogleTask(accesstoken,tapplentTask,googleTaskId,personPkId);
									}else if(googleTaskLastModifiedTime.after(tapplentTaskLastModifiedTime)){
										//Update the google Task record onto Tapplent Task record
										updateTheTapplentTask(tapplentTask,googleTask);
	
									}else{
										//Everything is in Sync Don't Do anything...
									}
								}
								
							}else{
								insertIntoTapplent(googleTask,personPkId);
								 
					   }
					}
				}
			}
		//Fetch the IS_SYNCH_NEEDED against person again
				if(isSyncNeeded){
					
					for(String taskPkId1:taskPkId){
						if(taskPkId1 !=null){
							String mtPE = "Task";
				    		EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
				    		Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
				    		 Task tapplentTask = evernoteDAO.getTask(taskPkId1,attributeMetaMap,personPkId);
							String googleTaskId = evernoteDAO.getThirdPartyEventId(taskPkId1, "T_PFM_TAP_TASK_INTGN","GOOGLE_TASK", "TASK_FK_ID");	
				   		if(googleTaskId!=null && !googleTaskId.isEmpty()){
				   			
					   			
					    		String defaultList = getDefaultListId(accesstoken);
					    		 HttpGet get = new HttpGet("https://www.googleapis.com/tasks/v1/lists/"+defaultList+"/tasks/"+googleTaskId+"");
					    		 HttpClient client1 = HttpClientBuilder.create().build();
					    		    HttpResponse response1;
					    		    response1 = client1.execute(get);
					    		    JSONObject GoogleTask = new JSONObject(EntityUtils.toString(response1.getEntity()));
					   			Timestamp tapplentTaskLastModifiedTime = tapplentTask.getLastModifiedDatetime();
					   			DateTime dateTime = new DateTime(GoogleTask.getString("updated"), DateTimeZone.forOffsetHoursMinutes(5, 30));
								DateTimeFormatter dateFormatter1 = ISODateTimeFormat.hourMinuteSecondFraction();
								DateTimeFormatter dateFormatter2 = ISODateTimeFormat.date();
								String dateString = dateFormatter2.print(dateTime);
								String timeString = dateFormatter1.print(dateTime);
								String timeStamp = dateString+" "+timeString;
								Timestamp googleTaskLastModifiedTime = Timestamp.valueOf(timeStamp);
	
								if(tapplentTaskLastModifiedTime.after(googleTaskLastModifiedTime)){
									//Update the tapplent Task record onto the GoogleTask
									updateTheGoogleTask(accesstoken,tapplentTask,googleTaskId,personPkId);
								}else if(googleTaskLastModifiedTime.after(tapplentTaskLastModifiedTime)){
									//Update the google Task record onto Tapplent Task record
									updateTheTapplentTask(tapplentTask,GoogleTask);

								}else{
									//Everything is in Sync Don't Do anything...
								}
								//Update Synch details table
						 		PEData child2PEData = new PEData();
						 		child2PEData.setMtPE("TaskSynchDetails");
						 		child2PEData.setActionCode("ADD_EDIT");
//						 		child2PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
						 		child2PEData.setLogActivity(false);
						 		child2PEData.setSubmit(true);
						 		child2PEData.setInline(true);
						 		
						    	 Map<String ,Object> columnNameToValueMap3 = new LinkedHashMap<String,Object>();
						    	 Map<String,Object> synchDetailsPkId = evernoteDAO.getSynchDetailsPkId("T_PFM_TAP_TASK_SYNCH_DTLS","TASK_FK_ID",taskPkId1,"TASK_SYNCH_DTLS_PK_ID");
						    	 SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
								String lastModifiedDateTime3 =  fmt1.format(synchDetailsPkId.get("lastModifiedDateTime")).toString();
						    	 columnNameToValueMap3.put("TaskSynchDetails.PrimaryKeyID",synchDetailsPkId.get("synchDetailsPkId"));
						    	 columnNameToValueMap3.put("TaskSynchDetails.SynchComplete", true);
						    	 columnNameToValueMap3.put("TaskSynchDetails.LastModifiedDateTime",lastModifiedDateTime3);
						    	 
						    	 child2PEData.setData(columnNameToValueMap3);
						    	 UnitRequestData finalRequestData1 = new UnitRequestData();
						 		finalRequestData1.setRootPEData(child2PEData);
						 		List<UnitRequestData> requestDataList1 = new ArrayList<>();
						 		requestDataList1.add(finalRequestData1);
						 		RequestData request1 = new RequestData();
						 		request1.setActionCodeFkId("ADD_EDIT");
						 		request1.setRequestDataList(requestDataList1);
						 		try {
						 			
						 			serverRuleManager.applyServerRulesOnRequest(request1);
								} catch (Exception e) {
									e.printStackTrace();
								}
				   			
				   		}else{
		
							String googleNewTaskId = createNewTask(tapplentTask,accesstoken,taskPkId1,personPkId);
							 PEData child1PEData = new PEData();
						    	child1PEData.setMtPE("TaskIntegration");
								child1PEData.setActionCode("ADD_EDIT");
//								child1PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
								child1PEData.setLogActivity(false);
								child1PEData.setSubmit(true);
								child1PEData.setInline(true);
						    	 Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
						    	 columnNameToValueMap2.put("TaskIntegration.TaskID", taskPkId1);
						    	 columnNameToValueMap2.put("TaskIntegration.IntegrationApp", "GOOGLE_TASK");
						    	 columnNameToValueMap2.put("TaskIntegration.IntegrationAppReferenceID",googleNewTaskId);
						    	 
						    	 child1PEData.setData(columnNameToValueMap2);
						    	 UnitRequestData finalRequestData = new UnitRequestData();
						 		finalRequestData.setRootPEData(child1PEData);
						 		List<UnitRequestData> requestDataList = new ArrayList<>();
						 		requestDataList.add(finalRequestData);
						 		RequestData request = new RequestData();
						 		request.setActionCodeFkId("ADD_EDIT");
						 		request.setRequestDataList(requestDataList);
						 		try {
						 			
						 			serverRuleManager.applyServerRulesOnRequest(request);
								} catch (Exception e) {
									e.printStackTrace();
								}
							//Update Synch details table
					 		PEData child2PEData = new PEData();
					 		child2PEData.setMtPE("TaskSynchDetails");
					 		child2PEData.setActionCode("ADD_EDIT");
					    	 Map<String ,Object> columnNameToValueMap3 = new LinkedHashMap<String,Object>();
					    	 Map<String,Object> synchDetailsPkId = evernoteDAO.getSynchDetailsPkId("T_PFM_TAP_TASK_SYNCH_DTLS","TASK_FK_ID",taskPkId1,"TASK_SYNCH_DTLS_PK_ID");
					    	 SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
							String lastModifiedDateTime3 =  fmt1.format(synchDetailsPkId.get("lastModifiedDateTime")).toString();
					    	 columnNameToValueMap3.put("TaskSynchDetails.PrimaryKeyID",synchDetailsPkId.get("synchDetailsPkId"));
					    	 columnNameToValueMap3.put("TaskSynchDetails.SynchComplete", true);
					    	 columnNameToValueMap3.put("TaskSynchDetails.LastModifiedDateTime",lastModifiedDateTime3);
					    	 
					    	 child2PEData.setData(columnNameToValueMap3);
					    	 UnitRequestData finalRequestData1 = new UnitRequestData();
					 		finalRequestData1.setRootPEData(child2PEData);
					 		List<UnitRequestData> requestDataList1 = new ArrayList<>();
					 		requestDataList1.add(finalRequestData1);
					 		RequestData request1 = new RequestData();
					 		request1.setActionCodeFkId("ADD_EDIT");
					 		request1.setRequestDataList(requestDataList1);
					 		try {
					 			
					 			serverRuleManager.applyServerRulesOnRequest(request1);
							} catch (Exception e) {
								e.printStackTrace();
							}
					}
				}		
					
				}		
				}
				//UPDATE IS_SYNCH_NEEDED = FALSE 
				updateSyncNeeded(taskSyncPkId,lastModifiedDateTime);
				
	    	}



private void insertIntoTapplent(JSONObject googleTask, String personPkId) {
	if(!googleTask.has("deleted")){
		
		Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
		
		Map<String,String> personInfo = evernoteDAO.getPersonInfo(personPkId);
		String personLocale = personInfo.get("locale");
		String googleTaskTitle = googleTask.getString("title");
		String googleTaskNotes = googleTask.getString("notes");
		String taskTitle = Util.getG11nString(personLocale, googleTaskTitle);
		columnNameToValueMap.put("Task.TaskTitle",taskTitle);
		String taskDesc = Util.getG11nString(personLocale, googleTaskNotes); 
		columnNameToValueMap.put("Task.TaskDescription",taskDesc);
		columnNameToValueMap.put("Task.DOName","Task");
		
		DateTime dateTime = new DateTime(googleTask.getString("due"), DateTimeZone.forOffsetHoursMinutes(5, 30));
		DateTimeFormatter dateFormatter1 = ISODateTimeFormat.hourMinuteSecondFraction();
		DateTimeFormatter dateFormatter2 = ISODateTimeFormat.date();
		String dateString = dateFormatter2.print(dateTime);
		String timeString = dateFormatter1.print(dateTime);
		String timeStamp = dateString+" "+timeString;
		columnNameToValueMap.put("Task.TaskEndDateTime",timeStamp);
		
		DateTime dateTime1 = new DateTime(googleTask.getString("updated"), DateTimeZone.forOffsetHoursMinutes(5, 30));
		DateTimeFormatter dateFormatter4 = ISODateTimeFormat.hourMinuteSecondFraction();
		DateTimeFormatter dateFormatter3 = ISODateTimeFormat.date();
		String dateString1 = dateFormatter3.print(dateTime1);
		String timeString1 = dateFormatter4.print(dateTime1);
		String timeStamp1 = dateString1+" "+timeString1;
		columnNameToValueMap.put("Task.TaskStartDateTime", timeStamp1);
		if(googleTask.getString("status").equals("completed")){
			columnNameToValueMap.put("Task.TaskStatusCode", "COMPLETED");
		}else{
			columnNameToValueMap.put("Task.TaskStatusCode", "YET_TO_START");
		}
		
		PEData rootPEData = new PEData();
		rootPEData.setMtPE("Task");
		rootPEData.setActionCode("ADD_EDIT");
//		rootPEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
		rootPEData.setLogActivity(false);
		rootPEData.setSubmit(true);
		rootPEData.setInline(true);
		rootPEData.setData(columnNameToValueMap);
		List<PEData> childrenPEData = new ArrayList<PEData>();

		PEData child2PEData = new PEData();
		child2PEData.setActionCode("ADD_EDIT");
		child2PEData.setMtPE("TaskIntegration");
//		rootPEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
		rootPEData.setLogActivity(false);
		rootPEData.setSubmit(true);
		rootPEData.setInline(true);
		Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
		columnNameToValueMap2.put("TaskIntegration.IntegrationApp","GOOGLE_TASK"); 
		columnNameToValueMap2.put("TaskIntegration.IntegrationAppReferenceID",googleTask.getString("id"));
		child2PEData.setData(columnNameToValueMap2);
		childrenPEData.add(child2PEData);

		rootPEData.setChildrenPEData(childrenPEData);

		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(rootPEData);
		List<UnitRequestData> requestDataList = new ArrayList<>();
		requestDataList.add(finalRequestData);
		RequestData request = new RequestData();
		request.setActionCodeFkId("ADD_EDIT");
		request.setRequestDataList(requestDataList);
		try {
			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
 }
	
}


private void updateTheTapplentTask(Task tapplentTask, JSONObject googleTask) throws JSONException {
	String googleTaskTitle = googleTask.getString("title");
	String googleTaskDesc = googleTask.getString("notes");
	String googleTaskDue = googleTask.getString("due");
	DateTime dateTime = new DateTime(googleTaskDue, DateTimeZone.forOffsetHoursMinutes(5, 30));
	DateTimeFormatter dateFormatter1 = ISODateTimeFormat.hourMinuteSecondFraction();
	DateTimeFormatter dateFormatter2 = ISODateTimeFormat.date();
	String dateString = dateFormatter2.print(dateTime);
	String timeString = dateFormatter1.print(dateTime);
	String timeStamp = dateString+" "+timeString;
	String googleTaskStatusCode = googleTask.getString("status");
	Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
	columnNameToValueMap.put("Task.PrimaryKeyID",tapplentTask.getTaskPkId());
	 if(!googleTaskTitle.equals(""))
	columnNameToValueMap.put("Task.TaskTitle", googleTaskTitle);
	 if(!googleTaskDesc.equals(""))
	columnNameToValueMap.put("Task.TaskDescription", googleTaskDesc);
	columnNameToValueMap.put("Task.DOName", "tapplent");
	 
	columnNameToValueMap.put("Task.TaskEndDateTime", timeStamp);
	if(googleTaskStatusCode.equals("completed")){
		columnNameToValueMap.put("Task.TaskStatusCode", "COMPLETED");
	}else{
		columnNameToValueMap.put("Task.TaskStatusCode", "YET_TO_START");
	}
	
	columnNameToValueMap.put("Task.LastModifiedDateTime",tapplentTask.getLastModifiedDatetime().toString());

	PEData rootPEData = new PEData();
	rootPEData.setMtPE("Task");
	rootPEData.setActionCode("ADD_EDIT");
//	rootPEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
	rootPEData.setLogActivity(false);
	rootPEData.setSubmit(true);
	rootPEData.setInline(true);
	rootPEData.setData(columnNameToValueMap);
	UnitRequestData finalRequestData = new UnitRequestData();
	finalRequestData.setRootPEData(rootPEData);
	List<UnitRequestData> requestDataList = new ArrayList<>();
	requestDataList.add(finalRequestData);
	RequestData request = new RequestData();
	request.setActionCodeFkId("ADD_EDIT");
	request.setRequestDataList(requestDataList);
	try {
		serverRuleManager.applyServerRulesOnRequest(request);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
}


private void deleteTheTapplentTask(String tapplentTaskId,String personId) {
	Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
	columnNameToValueMap.put("Task.PrimaryKeyID",tapplentTaskId);
	columnNameToValueMap.put("Task.Deleted", true);
	String mtPE = "Task";
	EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
	Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
	 Task tapplentTask = evernoteDAO.getTask(tapplentTaskId,attributeMetaMap,personId);
	columnNameToValueMap.put("Task.LastModifiedDateTime",tapplentTask.getLastModifiedDatetime().toString());

	PEData rootPEData = new PEData();
	rootPEData.setMtPE("Task");
	rootPEData.setActionCode("ADD_EDIT");
	rootPEData.setData(columnNameToValueMap);
//	rootPEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
	rootPEData.setLogActivity(false);
	rootPEData.setSubmit(true);
	rootPEData.setInline(true);
	UnitRequestData finalRequestData = new UnitRequestData();
	finalRequestData.setRootPEData(rootPEData);
	List<UnitRequestData> requestDataList = new ArrayList<>();
	requestDataList.add(finalRequestData);
	RequestData request = new RequestData();
	request.setActionCodeFkId("ADD_EDIT");
	request.setRequestDataList(requestDataList);
	try {
		serverRuleManager.applyServerRulesOnRequest(request);
	} catch (Exception e) {
		e.printStackTrace();
	}
	evernoteDAO.getIntegrationPkId("T_PFM_TAP_TASK_INTGN","TASK_FK_ID","TASK_INTGN_PK_ID",tapplentTaskId);
	Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
	columnNameToValueMap1.put("TaskIntegration.PrimaryKeyID",tapplentTaskId);
	columnNameToValueMap1.put("TaskIntegration.Deleted", true);

	PEData rootPEData1 = new PEData();
	rootPEData1.setMtPE("TaskIntegration");
	rootPEData1.setActionCode("ADD_EDIT");
//	rootPEData1.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
	rootPEData1.setLogActivity(false);
	rootPEData1.setSubmit(true);
	rootPEData1.setInline(true);
	rootPEData1.setData(columnNameToValueMap1);
	UnitRequestData finalRequestData1 = new UnitRequestData();
	finalRequestData.setRootPEData(rootPEData);
	List<UnitRequestData> requestDataList1 = new ArrayList<>();
	requestDataList.add(finalRequestData1);
	RequestData request1 = new RequestData();
	request.setActionCodeFkId("ADD_EDIT");
	request.setRequestDataList(requestDataList1);
	try {
		serverRuleManager.applyServerRulesOnRequest(request1);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
}


private void updateSyncNeeded(String taskSyncPkId,String lastModifiedTime){
	Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
	columnNameToValueMap.put("TaskSynch.PrimaryKeyID",taskSyncPkId);
	columnNameToValueMap.put("TaskSynch.SynchNeeded",false);
	columnNameToValueMap.put("TaskSynch.LastModifiedDateTime",lastModifiedTime);
	 SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	 String lastSynchDateTime =  lastModifiedTime;
//	 String lastSynchDateTime =  (new Timestamp(System.currentTimeMillis())).toString();
	columnNameToValueMap.put("TaskSynch.LastSynchDateTime",lastSynchDateTime);
	
	PEData rootPEData = new PEData();
	rootPEData.setMtPE("TaskSynch");
	rootPEData.setActionCode("ADD_EDIT");
//	rootPEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
	rootPEData.setLogActivity(false);
	rootPEData.setSubmit(true);
	rootPEData.setInline(true);
	rootPEData.setData(columnNameToValueMap);
	rootPEData.setData(columnNameToValueMap);
	UnitRequestData finalRequestData = new UnitRequestData();
	finalRequestData.setRootPEData(rootPEData);
	List<UnitRequestData> requestDataList = new ArrayList<>();
	requestDataList.add(finalRequestData);
	RequestData request = new RequestData();
	request.setActionCodeFkId("ADD_EDIT");
	request.setRequestDataList(requestDataList);
	try {
		serverRuleManager.applyServerRulesOnRequest(request);
	} catch (Exception e) {
		e.printStackTrace();
	}	
	
}
}
