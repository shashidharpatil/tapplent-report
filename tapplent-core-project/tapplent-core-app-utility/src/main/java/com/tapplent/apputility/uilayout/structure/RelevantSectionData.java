package com.tapplent.apputility.uilayout.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 27/04/17.
 */
public class RelevantSectionData {
    private Map<String,ScreenSectionInstance> sectionIdMap = new HashMap<>();
    private List<List<SectionControl>> controls = new ArrayList<>();

    public Map<String, ScreenSectionInstance> getSectionIdMap() {
        return sectionIdMap;
    }

    public void setSectionIdMap(Map<String, ScreenSectionInstance> sectionIdMap) {
        this.sectionIdMap = sectionIdMap;
    }

    public List<List<SectionControl>> getControls() {
        return controls;
    }

    public void setControls(List<List<SectionControl>> controls) {
        this.controls = controls;
    }
}
