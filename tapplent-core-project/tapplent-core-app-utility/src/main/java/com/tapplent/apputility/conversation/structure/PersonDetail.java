package com.tapplent.apputility.conversation.structure;

import com.tapplent.apputility.layout.structure.Icon;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.conversation.valueobject.Email;
import com.tapplent.platformutility.conversation.valueobject.PersonDetailsVO;
import com.tapplent.platformutility.conversation.valueobject.Phone;
import com.tapplent.platformutility.layout.valueObject.SocialStatusVO;
import org.springframework.beans.BeanUtils;

import java.net.URL;
import java.util.List;

/**
 * Created by Tapplent on 6/10/17.
 */
public class PersonDetail {
    private String personId;
    private String feedbackGroupMemberId;
    private String personPhoto;
    private URL personPhotoUrl;
    private String personFullName;
    private String personDesignation;
    private String location;
    private boolean memberAdmin;
    private boolean memberNtfnMuted;
    private SocialStatusVO socialStatus;
    private String nameInitials;
    private List<Phone> phoneList;
    private List<Email> emailList;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getFeedbackGroupMemberId() {
        return feedbackGroupMemberId;
    }

    public void setFeedbackGroupMemberId(String feedbackGroupMemberId) {
        this.feedbackGroupMemberId = feedbackGroupMemberId;
    }

    public String getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(String personPhoto) {
        this.personPhoto = personPhoto;
    }

    public URL getPersonPhotoUrl() {
        return personPhotoUrl;
    }

    public void setPersonPhotoUrl(URL personPhotoUrl) {
        this.personPhotoUrl = personPhotoUrl;
    }

    public String getPersonFullName() {
        return personFullName;
    }

    public void setPersonFullName(String personFullName) {
        this.personFullName = personFullName;
    }

    public String getPersonDesignation() {
        return personDesignation;
    }

    public void setPersonDesignation(String personDesignation) {
        this.personDesignation = personDesignation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isMemberAdmin() {
        return memberAdmin;
    }

    public void setMemberAdmin(boolean memberAdmin) {
        this.memberAdmin = memberAdmin;
    }

    public boolean isMemberNtfnMuted() {
        return memberNtfnMuted;
    }

    public void setMemberNtfnMuted(boolean memberNtfnMuted) {
        this.memberNtfnMuted = memberNtfnMuted;
    }

    public SocialStatusVO getSocialStatus() {
        return socialStatus;
    }

    public void setSocialStatus(SocialStatusVO socialStatus) {
        this.socialStatus = socialStatus;
    }

    public String getNameInitials() {
        return nameInitials;
    }

    public void setNameInitials(String nameInitials) {
        this.nameInitials = nameInitials;
    }

    public List<Phone> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<Phone> phoneList) {
        this.phoneList = phoneList;
    }

    public List<Email> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<Email> emailList) {
        this.emailList = emailList;
    }

    public static PersonDetail fromVO(PersonDetailsVO personDetailsVO){
        PersonDetail personDetail = new PersonDetail();
        BeanUtils.copyProperties(personDetailsVO, personDetail);
        personDetail.setPersonPhotoUrl(Util.preSignedGetUrl(personDetailsVO.getPersonPhoto(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
        return personDetail;
    }
}
