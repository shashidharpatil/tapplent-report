/**
 * 
 */
package com.tapplent.apputility.data.structure;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Shubham Patodi
 *
 */
public class AggregateData {
	private Object value;
	private String groupByAttribute;
	private Map<String,AggregateData> groupByValue;
	public AggregateData(){
		this.groupByValue = new HashMap<>();
		value = 0.0;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Map<String, AggregateData> getGroupByValue() {
		return groupByValue;
	}
	public void setGroupByValue(Map<String, AggregateData> groupByValue) {
		this.groupByValue = groupByValue;
	}

	public String getGroupByAttribute() {
		return groupByAttribute;
	}

	public void setGroupByAttribute(String groupByAttribute) {
		this.groupByAttribute = groupByAttribute;
	}
}
