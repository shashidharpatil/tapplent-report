package com.tapplent.apputility.layout.structure;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.CanvasActionForAgVO;

public class ActionGroup {
	private String baseTemplateId;
	private String actionGroupCode;
	private String parentActionGroupCode;
	@JsonIgnore
	private String actionCategoryCode;
	private String arCode;
	private int arSeqNumber;
	private boolean allowAutoExpand;
	private JsonNode actionGroupLabel;
	private String actionGroupIcon;
	private boolean showActionGroupLabel;
	private boolean showActionGroupIconId;
	private int actionGroupSeqNum;
	List<ActionGroup> actionGroupList;
	List<CanvasActionForAG> canvasActionForAGList;
	public ActionGroup(){
		
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getActionGroupCode() {
		return actionGroupCode;
	}
	public void setActionGroupCode(String actionGroupCode) {
		this.actionGroupCode = actionGroupCode;
	}
	public String getParentActionGroupCode() {
		return parentActionGroupCode;
	}
	public void setParentActionGroupCode(String parentActionGroupCode) {
		this.parentActionGroupCode = parentActionGroupCode;
	}
	public String getActionCategoryCode() {
		return actionCategoryCode;
	}
	public void setActionCategoryCode(String actionCategoryCode) {
		this.actionCategoryCode = actionCategoryCode;
	}
	public boolean isAllowAutoExpand() {
		return allowAutoExpand;
	}
	public void setAllowAutoExpand(boolean allowAutoExpand) {
		this.allowAutoExpand = allowAutoExpand;
	}
	public JsonNode getActionGroupLabel() {
		return actionGroupLabel;
	}
	public void setActionGroupLabel(JsonNode actionGroupLabel) {
		this.actionGroupLabel = actionGroupLabel;
	}
	public String getActionGroupIcon() {
		return actionGroupIcon;
	}
	public void setActionGroupIcon(String actionGroupIcon) {
		this.actionGroupIcon = actionGroupIcon;
	}
	public boolean isShowActionGroupLabel() {
		return showActionGroupLabel;
	}
	public void setShowActionGroupLabel(boolean showActionGroupLabel) {
		this.showActionGroupLabel = showActionGroupLabel;
	}
	public boolean isShowActionGroupIconId() {
		return showActionGroupIconId;
	}
	public void setShowActionGroupIconId(boolean showActionGroupIconId) {
		this.showActionGroupIconId = showActionGroupIconId;
	}
	public int getActionGroupSeqNum() {
		return actionGroupSeqNum;
	}
	public void setActionGroupSeqNum(int actionGroupSeqNum) {
		this.actionGroupSeqNum = actionGroupSeqNum;
	}
	public List<ActionGroup> getActionGroupList() {
		return actionGroupList;
	}
	public void setActionGroupList(List<ActionGroup> actionGroupList) {
		this.actionGroupList = actionGroupList;
	}
	public List<CanvasActionForAG> getCanvasActionForAGList() {
		return canvasActionForAGList;
	}
	public void setCanvasActionForAGList(List<CanvasActionForAG> canvasActionForAGList) {
		this.canvasActionForAGList = canvasActionForAGList;
	}
	public String getArCode() {
		return arCode;
	}
	public void setArCode(String arCode) {
		this.arCode = arCode;
	}
	public int getArSeqNumber() {
		return arSeqNumber;
	}
	public void setArSeqNumber(int arSeqNumber) {
		this.arSeqNumber = arSeqNumber;
	}
	public static ActionGroup fromVO(CanvasActionForAgVO canvasActionForAgVO){
		ActionGroup actionGroup = new ActionGroup();
		BeanUtils.copyProperties(canvasActionForAgVO, actionGroup);
		return actionGroup;
	}
}
