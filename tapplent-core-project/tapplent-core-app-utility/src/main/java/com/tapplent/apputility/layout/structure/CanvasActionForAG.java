package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.CanvasActionForAgVO;

public class CanvasActionForAG {
	private String canavsActionsForAgId;
	private String baseTemplateId;
	private String mtPE;
	private String canvasId;
	private String actionCode;
	private String actionGroupCode;
	private String groupActionMasterId;
	private String controlTypeCode;
	private JsonNode actionLabel;
	private String actionIcon;
	private boolean isShowActionLabel;
	private boolean isShowActionIcon;
	private String actionOnStateIcon;
	private String actionOffStateIcon;
	private boolean isLicenced;
	private boolean isCreateAccessControlEquivalent;
	private boolean isReadAccessControlEquivalent;
	private boolean isUpdateAccessControlEquivalent;
	private boolean isDeleteAccessControlEquivalent;
	private int actionSeqNo;
	@JsonIgnore
	private JsonNode deviceApplicableBlob;
	@JsonIgnore
	private JsonNode mstDeviceIndependentBlob;
	@JsonIgnore
	private JsonNode txnDeviceIndependentBlob;
	@JsonIgnore
	private JsonNode mstDeviceDependentBlob;
	@JsonIgnore
	private JsonNode txnDeviceDependentBlob;
	private JsonNode deviceIndependentBlob; 
	private JsonNode deviceDependentBlob;
	
	public String getCanavsActionsForAgId() {
		return canavsActionsForAgId;
	}

	public void setCanavsActionsForAgId(String canavsActionsForAgId) {
		this.canavsActionsForAgId = canavsActionsForAgId;
	}

	public String getMtPE() {
		return mtPE;
	}

	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}

	public String getGroupActionMasterId() {
		return groupActionMasterId;
	}

	public void setGroupActionMasterId(String groupActionMasterId) {
		this.groupActionMasterId = groupActionMasterId;
	}

	public String getBaseTemplateId() {
		return baseTemplateId;
	}

	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getCanvasId() {
		return canvasId;
	}

	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	public String getActionGroupCode() {
		return actionGroupCode;
	}

	public void setActionGroupCode(String actionGroupCode) {
		this.actionGroupCode = actionGroupCode;
	}

	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}

	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}

	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}

	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}

	public String getControlTypeCode() {
		return controlTypeCode;
	}

	public void setControlTypeCode(String controlTypeCode) {
		this.controlTypeCode = controlTypeCode;
	}

	public JsonNode getActionLabel() {
		return actionLabel;
	}

	public void setActionLabel(JsonNode actionLabel) {
		this.actionLabel = actionLabel;
	}

	public String getActionIcon() {
		return actionIcon;
	}

	public void setActionIcon(String actionIcon) {
		this.actionIcon = actionIcon;
	}

	public boolean isShowActionLabel() {
		return isShowActionLabel;
	}

	public void setShowActionLabel(boolean isShowActionLabel) {
		this.isShowActionLabel = isShowActionLabel;
	}

	public boolean isShowActionIcon() {
		return isShowActionIcon;
	}

	public void setShowActionIcon(boolean isShowActionIcon) {
		this.isShowActionIcon = isShowActionIcon;
	}

	public String getActionOnStateIcon() {
		return actionOnStateIcon;
	}

	public void setActionOnStateIcon(String actionOnStateIcon) {
		this.actionOnStateIcon = actionOnStateIcon;
	}

	public String getActionOffStateIcon() {
		return actionOffStateIcon;
	}

	public void setActionOffStateIcon(String actionOffStateIcon) {
		this.actionOffStateIcon = actionOffStateIcon;
	}

	public boolean isLicenced() {
		return isLicenced;
	}

	public void setLicenced(boolean isLicenced) {
		this.isLicenced = isLicenced;
	}

	public boolean isCreateAccessControlEquivalent() {
		return isCreateAccessControlEquivalent;
	}

	public void setCreateAccessControlEquivalent(boolean isCreateAccessControlEquivalent) {
		this.isCreateAccessControlEquivalent = isCreateAccessControlEquivalent;
	}

	public boolean isReadAccessControlEquivalent() {
		return isReadAccessControlEquivalent;
	}

	public void setReadAccessControlEquivalent(boolean isReadAccessControlEquivalent) {
		this.isReadAccessControlEquivalent = isReadAccessControlEquivalent;
	}

	public boolean isUpdateAccessControlEquivalent() {
		return isUpdateAccessControlEquivalent;
	}

	public void setUpdateAccessControlEquivalent(boolean isUpdateAccessControlEquivalent) {
		this.isUpdateAccessControlEquivalent = isUpdateAccessControlEquivalent;
	}

	public boolean isDeleteAccessControlEquivalent() {
		return isDeleteAccessControlEquivalent;
	}

	public void setDeleteAccessControlEquivalent(boolean isDeleteAccessControlEquivalent) {
		this.isDeleteAccessControlEquivalent = isDeleteAccessControlEquivalent;
	}

	public int getActionSeqNo() {
		return actionSeqNo;
	}

	public void setActionSeqNo(int actionSeqNo) {
		this.actionSeqNo = actionSeqNo;
	}

	public JsonNode getMstDeviceIndependentBlob() {
		return mstDeviceIndependentBlob;
	}

	public void setMstDeviceIndependentBlob(JsonNode mstDeviceIndependentBlob) {
		this.mstDeviceIndependentBlob = mstDeviceIndependentBlob;
	}

	public JsonNode getTxnDeviceIndependentBlob() {
		return txnDeviceIndependentBlob;
	}

	public void setTxnDeviceIndependentBlob(JsonNode txnDeviceIndependentBlob) {
		this.txnDeviceIndependentBlob = txnDeviceIndependentBlob;
	}

	public JsonNode getMstDeviceDependentBlob() {
		return mstDeviceDependentBlob;
	}

	public void setMstDeviceDependentBlob(JsonNode mstDeviceDependentBlob) {
		this.mstDeviceDependentBlob = mstDeviceDependentBlob;
	}

	public JsonNode getTxnDeviceDependentBlob() {
		return txnDeviceDependentBlob;
	}

	public void setTxnDeviceDependentBlob(JsonNode txnDeviceDependentBlob) {
		this.txnDeviceDependentBlob = txnDeviceDependentBlob;
	}

	public JsonNode getDeviceApplicableBlob() {
		return deviceApplicableBlob;
	}

	public void setDeviceApplicableBlob(JsonNode deviceApplicableBlob) {
		this.deviceApplicableBlob = deviceApplicableBlob;
	}

	public static CanvasActionForAG fromVO(CanvasActionForAgVO canvasActionForAgVO){
		CanvasActionForAG canvasActionForAG = new CanvasActionForAG();
		BeanUtils.copyProperties(canvasActionForAgVO, canvasActionForAG);
		return canvasActionForAG;
	}
}
