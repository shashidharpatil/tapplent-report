package com.tapplent.apputility.layout.structure;

public class NameCalculationResponse {
    private String nameType;
    private String name;

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
