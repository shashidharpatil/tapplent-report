package com.tapplent.apputility.groupResolver.Controller;

import com.tapplent.apputility.groupResolver.service.GroupResolverService;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by tapplent on 12/06/17.
 */
@Controller
@RequestMapping("tapp/groupResolver/v1")
public class GroupResolverController {
    private GroupResolverService groupResolverService;
    private static final Logger LOG = LogFactory.getLogger(GroupResolverController.class);
    @RequestMapping(value="refreshAll/t/{tenantId}/u/{personId}/e/", method= RequestMethod.GET)
    void resolveAllGroups(@RequestHeader("access_token") String token,
                         @PathVariable("tenantId") String tenantId,
                         @PathVariable("personId") String userName,
                         @RequestParam("personName") String personName,
                         @RequestParam("timeZone") String timeZone) throws IOException, SQLException {
        try {
            groupResolverService.resolveAllGroups();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setGroupResolverService(GroupResolverService groupResolverService) {
        this.groupResolverService = groupResolverService;
    }
}
