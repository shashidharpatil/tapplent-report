package com.tapplent.apputility.layout.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.common.util.AttachmentObject;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.layout.valueObject.JobRelationshipTypeVO;
import org.springframework.beans.BeanUtils;

import java.net.URL;

/**
 * Created by tapplent on 19/07/17.
 */
public class JobRelationshipType {
    @JsonIgnore
    private String relationshipTypeCode;
    private String name;
    private IconVO icon;
    private AttachmentObject imageID;
    private String borderType;
    private String borderColour;
    @JsonIgnore
    private String colleaguesAtWorkCountQuery;

    public String getRelationshipTypeCode() {
        return relationshipTypeCode;
    }

    public void setRelationshipTypeCode(String relationshipTypeCode) {
        this.relationshipTypeCode = relationshipTypeCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IconVO getIcon() {
        return icon;
    }

    public void setIcon(IconVO icon) {
        this.icon = icon;
    }

    public AttachmentObject getImageID() {
        return imageID;
    }

    public void setImageID(AttachmentObject imageID) {
        this.imageID = imageID;
    }

    public String getBorderType() {
        return borderType;
    }

    public void setBorderType(String borderType) {
        this.borderType = borderType;
    }

    public String getBorderColour() {
        return borderColour;
    }

    public void setBorderColour(String borderColour) {
        this.borderColour = borderColour;
    }

    public String getColleaguesAtWorkCountQuery() {
        return colleaguesAtWorkCountQuery;
    }

    public void setColleaguesAtWorkCountQuery(String colleaguesAtWorkCountQuery) {
        this.colleaguesAtWorkCountQuery = colleaguesAtWorkCountQuery;
    }

    public static JobRelationshipType fromVO(JobRelationshipTypeVO jobRelationshipTypeVO) {
        JobRelationshipType jobRelationshipType = new JobRelationshipType();
        BeanUtils.copyProperties(jobRelationshipTypeVO, jobRelationshipType);
        return jobRelationshipType;
    }
}
