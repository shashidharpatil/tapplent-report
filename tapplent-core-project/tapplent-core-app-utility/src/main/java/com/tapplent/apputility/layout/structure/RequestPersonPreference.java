package com.tapplent.apputility.layout.structure;

public class RequestPersonPreference {
    private String personId;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
