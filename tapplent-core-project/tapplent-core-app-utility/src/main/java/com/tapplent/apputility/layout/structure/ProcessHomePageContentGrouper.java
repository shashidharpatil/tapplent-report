package com.tapplent.apputility.layout.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 22/11/16.
 */
public class ProcessHomePageContentGrouper {
    private List<ProcessHomePageContent> processHomePageContentList = new ArrayList<>();
    private Map<Integer, List<ProcessHomePageContent>> recordNumToContentMap = new HashMap<>();

    public List<ProcessHomePageContent> getProcessHomePageContentList() {
        return processHomePageContentList;
    }

    public void setProcessHomePageContentList(List<ProcessHomePageContent> processHomePageContentList) {
        this.processHomePageContentList = processHomePageContentList;
    }

    public Map<Integer, List<ProcessHomePageContent>> getRecordNumToContentMap() {
        return recordNumToContentMap;
    }

    public void setRecordNumToContentMap(Map<Integer, List<ProcessHomePageContent>> recordNumToContentMap) {
        this.recordNumToContentMap = recordNumToContentMap;
    }
}
