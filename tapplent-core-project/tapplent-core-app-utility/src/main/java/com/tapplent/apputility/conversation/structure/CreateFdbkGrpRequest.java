package com.tapplent.apputility.conversation.structure;

import java.util.List;
import java.util.Map;

/**
 * Created by Tapplent on 6/10/17.
 */
public class CreateFdbkGrpRequest {
    List<PersonDetail> personDetailList;
    private String feedbackGroupPkId;
    private String mTPECode;
    private String objectId;
    private String feedbackGroupImageId;
    private String feedbackGroupName;
    private boolean bilateralConversation;

    public List<PersonDetail> getPersonDetailList() {
        return personDetailList;
    }

    public void setPersonDetailList(List<PersonDetail> personDetailList) {
        this.personDetailList = personDetailList;
    }

    public String getFeedbackGroupPkId() {
        return feedbackGroupPkId;
    }

    public void setFeedbackGroupPkId(String feedbackGroupPkId) {
        this.feedbackGroupPkId = feedbackGroupPkId;
    }

    public String getFeedbackGroupImageId() {
        return feedbackGroupImageId;
    }

    public void setFeedbackGroupImageId(String feedbackGroupImageId) {
        this.feedbackGroupImageId = feedbackGroupImageId;
    }

    public String getFeedbackGroupName() {
        return feedbackGroupName;
    }

    public void setFeedbackGroupName(String feedbackGroupName) {
        this.feedbackGroupName = feedbackGroupName;
    }

    public boolean isBilateralConversation() {
        return bilateralConversation;
    }

    public void setBilateralConversation(boolean bilateralConversation) {
        this.bilateralConversation = bilateralConversation;
    }

    public String getmTPECode() {
        return mTPECode;
    }

    public void setmTPECode(String mTPECode) {
        this.mTPECode = mTPECode;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
}
