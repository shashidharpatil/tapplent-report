package com.tapplent.apputility.data.structure;

import com.tapplent.platformutility.common.util.AttachmentObject;

/**
 * Created by tapplent on 28/07/17.
 */
public class ColleaguesAtWorkResponse {

    private String relationshipType;
    private String relationshipName;
    private int count;
    private AttachmentObject image;

    public String getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(String relationshipType) {
        this.relationshipType = relationshipType;
    }

    public String getRelationshipName() {
        return relationshipName;
    }

    public void setRelationshipName(String relationshipName) {
        this.relationshipName = relationshipName;
    }



    public AttachmentObject getImage() {
        return image;
    }

    public void setImage(AttachmentObject image) {
        this.image = image;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
