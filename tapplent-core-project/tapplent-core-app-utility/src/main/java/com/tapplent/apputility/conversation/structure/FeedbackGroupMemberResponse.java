package com.tapplent.apputility.conversation.structure;

import java.util.List;

/**
 * Created by Tapplent on 6/22/17.
 */
public class FeedbackGroupMemberResponse {
    private List<GroupMember> groupMembers;

    public List<GroupMember> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(List<GroupMember> groupMembers) {
        this.groupMembers = groupMembers;
    }
}
