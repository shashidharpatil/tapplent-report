package com.tapplent.apputility.data.structure;

import java.util.*;

import com.tapplent.platformutility.common.util.RequestSelectType;
import com.tapplent.platformutility.common.util.valueObject.FilterExpn;
import com.tapplent.platformutility.metadata.structure.AttributePaths;
import com.tapplent.platformutility.metadata.structure.DependencyKeysHelper;
import com.tapplent.platformutility.metadata.structure.GroupByAttributeDetails;
import com.tapplent.platformutility.search.builder.FilterOperatorValue;
import com.tapplent.platformutility.search.builder.SortData;
import com.tapplent.platformutility.uilayout.valueobject.LoadParameters;
import com.tapplent.platformutility.uilayout.valueobject.SectionControlVO;

public class PERequest {
	// From initialization
	private String mtPEAlias;
	private String parentPEAlias;
	private String baseTemplate;
	private String mtPE;
	private String keyWord;
	private PERequest parentMTPEAliasNode;

	// From control
	private boolean isStickyHeader;
	private boolean isExternalDistinct = false;
	private boolean isBookmarkRtrn = false;
	private boolean isEndorseRtrn = false;
	private String subjectUserPath;
	// Here get control filters
	private Map<String,List<FilterOperatorValue>> controlFilters = new HashMap<>();
	private Map<String,List<FilterOperatorValue>> controlHavingClause = new HashMap<>();
	// From Load Parameters
	private int page;
	private int pageSize;
	private boolean applyPagination = true;
	private int defaultPaginationSize;
	private boolean returnOnlySqlQuery = false;
	private String filterExpression;
	private String havingExpression;
	private List<SortData> sortData = new ArrayList<>();
	private List<SortData> defaultSortData = new ArrayList<>();
	private boolean isResolveG11n = true;
	private boolean isHierarchicalResponseRequired = false;
	private boolean logActivity = false;
	private boolean onlyBookmarkedRecords = false;
	//
	private Map<String, Object> dependencyKeys;
	private List<PERequest> childrenPE;
	private String menuApplicableSubjectUserMTPEAlias;
	private String menuApplicableSubjectUserPath;
	private boolean isDeleteFilterAdded = false;





	private List<FilterPERequest> additonalFilterPE;
	private RequestSelectType selectType = RequestSelectType.LAYOUT;
	private AttributePaths attributePaths;
	private List<SectionControlVO> propertyControlAttributes = new ArrayList<>();
	private List<String> gridControlIds = new ArrayList<>();
	private List<DependencyKeysHelper> dependencyKeyList = new ArrayList<>();
    private List<GroupByAttributeDetails> groupByAttributeDetailsList = new ArrayList<>();
    private Map<String, GroupByAttributeDetails> groupByControlList = new HashMap<>();
    private Set<String> aggregateGroupByControlList = new HashSet<>();






	public PERequest(){
		this.childrenPE = new ArrayList<>();
		this.attributePaths = new AttributePaths();
        this.propertyControlAttributes = new ArrayList<>();
        this.sortData = new ArrayList<>();
	}
	public PERequest(String mtPEAlias){
        this.childrenPE = new ArrayList<>();
        this.attributePaths = new AttributePaths();
        this.propertyControlAttributes = new ArrayList<>();
		this.sortData = new ArrayList<>();
		this.mtPEAlias = mtPEAlias;
	}

	public PERequest(String mtPEAlias, String parentPEAlias, String baseTemplate, String mtPE, LoadParameters loadParameters, boolean returnOnlySqlQuery, boolean applyPagination, Set<String> endorseMTPEAlias) {
		this.mtPEAlias = mtPEAlias;
		this.parentPEAlias = parentPEAlias;
		this.baseTemplate = baseTemplate;
		this.mtPE = mtPE;
		this.childrenPE = new ArrayList<>();
		this.attributePaths = new AttributePaths();
		this.sortData = new ArrayList<>();
		this.returnOnlySqlQuery = returnOnlySqlQuery;
		this.applyPagination = applyPagination;
		if(loadParameters!=null){
//			this.filterExpression = loadParameters.getFilterExpn();
//			String filterExpn = Util.getFilterExpnFromFilters(filters);
			this.page = loadParameters.getPage();
			this.pageSize = loadParameters.getPageSize();
			this.applyPagination = loadParameters.isApplyPagination();
			this.keyWord = loadParameters.getKeyWord();
			this.onlyBookmarkedRecords = loadParameters.isOnlyBookmarkedRecords();
			if(loadParameters.getSorts()!=null)
				this.sortData.addAll(loadParameters.getSorts());
		}
		if(endorseMTPEAlias!=null){
			if(endorseMTPEAlias.contains(mtPEAlias)){
				isEndorseRtrn = true;
			}
		}
	}

	public String getMtPE() {
		return mtPE;
	}
	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}
	public Map<String, Object> getDependencyKeys() {
		return dependencyKeys;
	}
	public void setDependencyKeys(Map<String, Object> dependencyKeys) {
		this.dependencyKeys = dependencyKeys;
	}
	public String getFilterExpression() {
		return filterExpression;
	}
	public void setFilterExpression(FilterExpn filterExpression) {
		if(filterExpression!=null) {
			this.filterExpression = filterExpression.getFilterExpn();
			this.isDeleteFilterAdded = filterExpression.isDeleteFilterAdded();
		}
	}
	public List<SortData> getSortData() {
		return sortData;
	}
	public void setSortData(List<SortData> sortData) {
		this.sortData = sortData;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public List<PERequest> getChildrenPE() {
		return childrenPE;
	}
	public void setChildrenPE(List<PERequest> childrenPE) {
		this.childrenPE = childrenPE;
	}
	public List<FilterPERequest> getAdditonalFilterPE() {
		return additonalFilterPE;
	}
	public void setAdditonalFilterPE(List<FilterPERequest> additonalFilterPE) {
		this.additonalFilterPE = additonalFilterPE;
	}
	public boolean isStickyHeader() {
		return isStickyHeader;
	}
	public void setStickyHeader(boolean isStickyHeader) {
		this.isStickyHeader = isStickyHeader;
	}
	public RequestSelectType getSelectType() {
		return selectType;
	}
	public void setSelectType(RequestSelectType selectType) {
		this.selectType = selectType;
	}
	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public AttributePaths getAttributePaths() {
		return attributePaths;
	}

	public List<SectionControlVO> getPropertyControlAttributes() {
		return propertyControlAttributes;
	}

	public void setPropertyControlAttributes(List<SectionControlVO> propertyControlAttributes) {
		this.propertyControlAttributes = propertyControlAttributes;
	}

	public void setAttributePaths(AttributePaths attributePaths) {
		this.attributePaths = attributePaths;
	}

	public List<DependencyKeysHelper> getDependencyKeyList() {
		return dependencyKeyList;
	}

	public void setDependencyKeyList(List<DependencyKeysHelper> dependencyKeyList) {
		this.dependencyKeyList = dependencyKeyList;
	}

	public String getParentPEAlias() {
		return parentPEAlias;
	}

	public void setParentPEAlias(String parentPEAlias) {
		this.parentPEAlias = parentPEAlias;
	}

	public List<GroupByAttributeDetails> getGroupByAttributeDetailsList() {
        return groupByAttributeDetailsList;
    }

    public void setGroupByAttributeDetailsList(List<GroupByAttributeDetails> groupByAttributeDetailsList) {
        this.groupByAttributeDetailsList = groupByAttributeDetailsList;
    }

	public boolean isExternalDistinct() {
		return isExternalDistinct;
	}

	public void setExternalDistinct(boolean externalDistinct) {
		isExternalDistinct = externalDistinct;
	}


	public List<String> getGridControlIds() {
		return gridControlIds;
	}

	public void setGridControlIds(List<String> gridControlIds) {
		this.gridControlIds = gridControlIds;
	}

	public boolean isResolveG11n() {
		return isResolveG11n;
	}

	public void setResolveG11n(boolean resolveG11n) {
		isResolveG11n = resolveG11n;
	}

	public boolean isLogActivity() {
		return logActivity;
	}

	public void setLogActivity(boolean logActivity) {
		this.logActivity = logActivity;
	}

	public boolean isHierarchicalResponseRequired() {
		return isHierarchicalResponseRequired;
	}

	public void setHierarchicalResponseRequired(boolean hierarchicalResponseRequired) {
		isHierarchicalResponseRequired = hierarchicalResponseRequired;
	}

	public String getMtPEAlias() {
		return mtPEAlias;
	}

	public void setMtPEAlias(String mtPEAlias) {
		this.mtPEAlias = mtPEAlias;
	}

	public String getBaseTemplate() {
		return baseTemplate;
	}

	public void setBaseTemplate(String baseTemplate) {
		this.baseTemplate = baseTemplate;
	}

	public void setBookmarkRtrn(boolean bookmarkRtrn) {
		this.isBookmarkRtrn = bookmarkRtrn;
	}

	public boolean isBookmarkRtrn() {
		return isBookmarkRtrn;
	}

	public boolean isOnlyBookmarkedRecords() {
		return onlyBookmarkedRecords;
	}

	public void setOnlyBookmarkedRecords(boolean onlyBookmarkedRecords) {
		this.onlyBookmarkedRecords = onlyBookmarkedRecords;
	}

	public Map<String, List<FilterOperatorValue>> getControlFilters() {
		return controlFilters;
	}

	public void setControlFilters(Map<String, List<FilterOperatorValue>> controlFilters) {
		this.controlFilters = controlFilters;
	}

	public PERequest getParentMTPEAliasNode() {
		return parentMTPEAliasNode;
	}

	public void setParentMTPEAliasNode(PERequest parentMTPEAliasNode) {
		this.parentMTPEAliasNode = parentMTPEAliasNode;
	}

	public boolean isApplyPagination() {
		return applyPagination;
	}

	public void setApplyPagination(boolean applyPagination) {
		this.applyPagination = applyPagination;
	}

	public boolean isReturnOnlySqlQuery() {
		return returnOnlySqlQuery;
	}

	public void setReturnOnlySqlQuery(boolean returnOnlySqlQuery) {
		this.returnOnlySqlQuery = returnOnlySqlQuery;
	}

	public int getDefaultPaginationSize() {
		return defaultPaginationSize;
	}

	public void setDefaultPaginationSize(int defaultPaginationSize) {
		this.defaultPaginationSize = defaultPaginationSize;
	}

	public Set<String> getAggregateGroupByControlList() {
		return aggregateGroupByControlList;
	}

	public void setAggregateGroupByControlList(Set<String> aggregateGroupByControlList) {
		this.aggregateGroupByControlList = aggregateGroupByControlList;
	}

	public Map<String, GroupByAttributeDetails> getGroupByControlList() {
		return groupByControlList;
	}

	public void setGroupByControlList(Map<String, GroupByAttributeDetails> groupByControlList) {
		this.groupByControlList = groupByControlList;
	}

	public Map<String, List<FilterOperatorValue>> getControlHavingClause() {
		return controlHavingClause;
	}

	public void setControlHavingClause(Map<String, List<FilterOperatorValue>> controlHavingClause) {
		this.controlHavingClause = controlHavingClause;
	}

	public String getHavingExpression() {
		return havingExpression;
	}

	public void setHavingExpression(String havingExpression) {
		this.havingExpression = havingExpression;
	}

	public String getSubjectUserPath() {
		return subjectUserPath;
	}

	public void setSubjectUserPath(String subjectUserPath) {
		this.subjectUserPath = subjectUserPath;
	}

	public String getMenuApplicableSubjectUserMTPEAlias() {
		return menuApplicableSubjectUserMTPEAlias;
	}

	public void setMenuApplicableSubjectUserMTPEAlias(String menuApplicableSubjectUserMTPEAlias) {
		this.menuApplicableSubjectUserMTPEAlias = menuApplicableSubjectUserMTPEAlias;
	}

	public void setFilterExpression(String filterExpression) {
		this.filterExpression = filterExpression;
	}

	public boolean isDeleteFilterAdded() {
		return isDeleteFilterAdded;
	}

	public void setDeleteFilterAdded(boolean deleteFilterAdded) {
		isDeleteFilterAdded = deleteFilterAdded;
	}

	public String getMenuApplicableSubjectUserPath() {
		return menuApplicableSubjectUserPath;
	}

	public void setMenuApplicableSubjectUserPath(String menuApplicableSubjectUserPath) {
		this.menuApplicableSubjectUserPath = menuApplicableSubjectUserPath;
	}

	public boolean isEndorseRtrn() {
		return isEndorseRtrn;
	}

	public void setEndorseRtrn(boolean endorseRtrn) {
		isEndorseRtrn = endorseRtrn;
	}

	public List<SortData> getDefaultSortData() {
		return defaultSortData;
	}

	public void setDefaultSortData(List<SortData> defaultSortData) {
		this.defaultSortData = defaultSortData;
	}
}
