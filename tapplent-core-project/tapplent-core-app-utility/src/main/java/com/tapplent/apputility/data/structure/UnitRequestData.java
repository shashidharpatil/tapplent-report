package com.tapplent.apputility.data.structure;

import com.tapplent.apputility.analytics.structure.AnalyticsMetricRequest;

import java.util.List;

public class UnitRequestData {
	//Later Make it to cater multiple Process ELements
	private String employeeCodeGenKey;
	private String usernameGenKey;
	private PEData rootPEData;
//	private PERequest rootPE;
	private ScreenRequest screen;
	private AnalyticsMetricRequest analytics;

//	private List<UnitRequestData> childRequestDataList;
	public UnitRequestData(){
		
	}
	public PEData getRootPEData() {
		return rootPEData;
	}
	public void setRootPEData(PEData rootPEData) {
		this.rootPEData = rootPEData;
	}

	public String getEmployeeCodeGenKey() {
		return employeeCodeGenKey;
	}

	public void setEmployeeCodeGenKey(String employeeCodeGenKey) {
		this.employeeCodeGenKey = employeeCodeGenKey;
	}

	public String getUsernameGenKey() {
		return usernameGenKey;
	}

	public void setUsernameGenKey(String usernameGenKey) {
		this.usernameGenKey = usernameGenKey;
	}

	public ScreenRequest getScreen() {
		return screen;
	}

	public void setScreen(ScreenRequest screen) {
		this.screen = screen;
	}

	public AnalyticsMetricRequest getAnalytics() {
		return analytics;
	}

	public void setAnalytics(AnalyticsMetricRequest analytics) {
		this.analytics = analytics;
	}

}
