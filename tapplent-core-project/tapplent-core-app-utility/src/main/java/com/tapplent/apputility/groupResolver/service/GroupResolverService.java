package com.tapplent.apputility.groupResolver.service;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by tapplent on 12/06/17.
 */
public interface GroupResolverService {
    public void resolveAllGroups() throws IOException, SQLException;

    void resolveGroupByMtPE(String mtPE) throws IOException, SQLException;
}
