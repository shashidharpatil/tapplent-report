package com.tapplent.apputility.data.service;

import com.tapplent.apputility.analytics.structure.AnalyticsMetricRequest;
import com.tapplent.apputility.data.controller.FeaturedResponse;
import com.tapplent.apputility.data.structure.*;
import com.tapplent.apputility.uilayout.service.UILayoutService;
import com.tapplent.apputility.uilayout.structure.*;
import com.tapplent.platformutility.accessmanager.dao.AccessManagerService;
import com.tapplent.platformutility.accessmanager.structure.DOAPermission;
import com.tapplent.platformutility.accessmanager.structure.Permission;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.PersonDetailsObjectRepository;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.common.util.valueObject.AnalyticsDOAControlMap;
import com.tapplent.platformutility.conversation.valueobject.PersonDetailsVO;
import com.tapplent.platformutility.layout.valueObject.JobRelationshipTypeVO;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.person.structure.PersonGroup;
import com.tapplent.platformutility.search.builder.FilterOperatorValue;
import com.tapplent.platformutility.search.builder.SortData;
import com.tapplent.platformutility.uilayout.valueobject.ControlType;
import com.tapplent.platformutility.uilayout.valueobject.LoadParameters;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import com.tapplent.tenantresolver.user.UserContext;
import org.springframework.beans.BeanUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by tapplent on 27/04/17.
 */
public class DataRequestServiceImpl implements DataRequestService{
    private UILayoutService uiLayoutService;
    private DataService dataService;
    private AccessManagerService accessManagerService;
    @Override
    public UnitResponseData getScreen(ScreenRequest screen) throws IOException, SQLException {
        // So here on the basis of action target pk id get the target sections
        ActionTarget actionTarget = null;
        if(StringUtil.isDefined(screen.getControlActionInstanceId())) {
            actionTarget = uiLayoutService.getActionTarget(screen.getActionTargetPkId(), "CONTROL");
        }else{
            actionTarget = uiLayoutService.getActionTarget(screen.getActionTargetPkId(), "SECTION");
        }
        ScreenInstance layout = getScreenLayout(actionTarget.getTargetSections(), screen);
        return getScreen(layout,screen);
    }

    private UnitResponseData getScreen(ScreenInstance layout, ScreenRequest screen)throws IOException, SQLException {
        UnitResponseData responseData = new UnitResponseData();
        responseData.setLayout(layout);
        // Now we have target section list
        // We need to have target screen
        // Now we have layout we need to loop through controls and get the response out.
        JsonLabelAliasTracker aliasTracker = new JsonLabelAliasTracker();
        List<PERequest> rootPEs = buildPERequest(screen, layout);
        if(screen.getDataRequired()) {
            for (PERequest rootPE : rootPEs) {
                ProcessElementData peResponse = dataService.getDataInternal(rootPE, aliasTracker, screen.getBaseTemplate(), responseData);
                responseData.getPEData().put(rootPE.getMtPEAlias(), peResponse);
            }
        }
        if(!screen.getLayoutRequired()) {
            responseData.setLayout(null);
        }
        MetaHelper metaHelper =getLabelToAliasMap(aliasTracker, screen.getMetaRequired(), screen.getMtPE(),screen.getBaseTemplate(), layout);
        if(metaHelper!=null){
            responseData.getLabelToAliasMap().putAll(metaHelper.getLabelToAliasMap());
            responseData.setDoMetaMap(metaHelper.getDoMetaMap());
        }
        // Now you have Data Set as well as Layout. The permission are available with layout.
        // The subject user is available with data.
        // We get subject user from section.
        // loop through all the sections and get all the actions associated with it.
        // Now for that section find out the subject user ID from Data.
//        ScreenSectionInstance rootSection = null;
//        List<ScreenSectionInstance> sections = layout.getSections();
//        for(ScreenSectionInstance section : sections){
//            if(section.getShowAtInitialLoad()){
//                rootSection = section;
//            }
//        }
//        if(rootSection!=null)
//            applyLayoutPermissions(rootSection, responseData);
        return responseData;
    }

    private ScreenInstance getScreenLayout(String targetSections, ScreenRequest screen) {
        String[] targetSectionList = targetSections.split("\\|");
        List<String> targetSectionInstanceList = new ArrayList<>();
        targetSectionInstanceList.addAll(Arrays.asList(targetSectionList));
        ScreenInstance layout = uiLayoutService.getLayout(targetSectionInstanceList, screen.getDeviceType(), screen.getSubjectUserID());
        List<String> nonInteractiveTargetSectionInstanceList = new ArrayList<>();
        for(SectionActionInstance nonInteractiveAction :layout.getNonInteractiveLoadActions()){
            for(ActionTarget nonInteractiveActionTarget : nonInteractiveAction.getTargets()){
                nonInteractiveTargetSectionInstanceList.add(nonInteractiveActionTarget.getTargetSections());
            }
        }
        if(!nonInteractiveTargetSectionInstanceList.isEmpty()) {
            ScreenInstance nonInteractiveLayout = uiLayoutService.getLayout(nonInteractiveTargetSectionInstanceList,screen.getDeviceType(), screen.getSubjectUserID());
            layout.getSections().addAll(nonInteractiveLayout.getSections());
            layout.getListOfListOfControls().addAll(nonInteractiveLayout.getListOfListOfControls());
            layout.getListOfTargetSectionsFilters().addAll(nonInteractiveLayout.getListOfTargetSectionsFilters());
            layout.getListOfActionArguments().addAll(nonInteractiveLayout.getListOfActionArguments());
            layout.getListOfFilterCriteria().addAll(nonInteractiveLayout.getListOfFilterCriteria());
        }
        return layout;
    }

    private void applyLayoutPermissions(ScreenSectionInstance rootSection, UnitResponseData responseData) {
        if(Util.isRepetitiveSection(rootSection.getSectionMasterCodeFkId())){
            // now find out the root MT-PE of the section and loop through it
            String rootMTPE = findRootMTPEForRecord(rootSection);
        }else{

        }
    }

    private String findRootMTPEForRecord(ScreenSectionInstance rootSection) {
        // Loop through all the section inside this and find out the least hierarchy
        String currentMinimumMTPEHierarchy = "";
        for(SectionControl control : rootSection.getControls()){
            if(currentMinimumMTPEHierarchy == ""){
                if(StringUtil.isDefined(control.getParentMTPEAliasHierarchy()))
                currentMinimumMTPEHierarchy = control.getParentMTPEAliasHierarchy();
            }
            String mtPEHierachy = control.getParentMTPEAliasHierarchy();
            String[] splitHierarchy = mtPEHierachy.split("");
        }
        return null;
    }

    @Override
    public UnitResponseData getQueryResult(GenericQueryRequest queryRequest) throws IOException, SQLException {
        UnitResponseData responseData = new UnitResponseData();
        JsonLabelAliasTracker aliasTracker = new JsonLabelAliasTracker();
        // Here get the complete generic Query Object on the basis of select statement
        GenericQuery query = uiLayoutService.getGenericQuery(queryRequest.getGenericQueryID());
        List<PERequest> rootPEs = buildPERequest(queryRequest, query);
        for (PERequest rootPE : rootPEs) {
            ProcessElementData peResponse = dataService.getDataInternal(rootPE, aliasTracker, null, responseData);
            responseData.getPEData().put(rootPE.getMtPEAlias(), peResponse);
        }
        MetaHelper metaHelper =getLabelToAliasMap(aliasTracker, true, null,null, null);
        if(metaHelper!=null){
            responseData.getLabelToAliasMap().putAll(metaHelper.getLabelToAliasMap());
            responseData.setDoMetaMap(metaHelper.getDoMetaMap());
        }
        return responseData;
    }

    @Override
    public List<ColleaguesAtWorkResponse> getColleaguesAtWork(String contextID) {
        // Get all the job relationships from the job relationship type lookup.
        List<ColleaguesAtWorkResponse> colleaguesAtWorkResponses = new ArrayList<>();
        List<JobRelationshipTypeVO> relationshipTypeVOS = uiLayoutService.getJobRelationshipTypes();
        for(JobRelationshipTypeVO jobRelationshipTypeVO : relationshipTypeVOS){
            populateJobRelationshipCountData(jobRelationshipTypeVO, colleaguesAtWorkResponses, contextID);
        }
//        for(Map.Entry<String, JobRelationshipType> jobRelationshipTypeEntry : jobRelationshipTypesMap.entrySet()){
//            JobRelationshipType jobRelationshipType = jobRelationshipTypeEntry.getValue();
//            populateJobRelationshipCountData(jobRelationshipType, colleaguesAtWorkResponses);
//        }
        return colleaguesAtWorkResponses;
    }

    @Override
    public FeaturedResponse getFeaturingPersonDetail(String contextID, String featuredTableName) {
        List<PersonDetailsVO> detailsVOs = uiLayoutService.getFeaturingPersonDetail(contextID, featuredTableName);
        List<FeaturingPersonDetail> featuringPersonDetails = new ArrayList<>();
        for (PersonDetailsVO personDetailsVO : detailsVOs){
            FeaturingPersonDetail featuringPersonDetail = new FeaturingPersonDetail();
            BeanUtils.copyProperties(personDetailsVO, featuringPersonDetail);
            featuringPersonDetail.setPersonPhotoUrl(Util.preSignedGetUrl(featuringPersonDetail.getPersonPhoto(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
            featuringPersonDetails.add(featuringPersonDetail);
        }
        FeaturedResponse featuredResponse = new FeaturedResponse();
        featuredResponse.setFeaturingPersonDetailList(featuringPersonDetails);
        return featuredResponse;
    }

    private void populateJobRelationshipCountData(JobRelationshipTypeVO jobRelationshipTypeVO, List<ColleaguesAtWorkResponse> colleaguesAtWorkResponses, String contextID) {
        if(StringUtil.isDefined(jobRelationshipTypeVO.getColleaguesAtWorkCountQuery())){
            String colleaguesAtWorkQueryID = jobRelationshipTypeVO.getColleaguesAtWorkCountQuery();
            GenericQuery query = uiLayoutService.getGenericQuery(colleaguesAtWorkQueryID);
            // Now that we have the query get the attribute which is actually marked as isAggregate = 1
            GenericQueryAttribute aggregateAttribute = null;
            for(GenericQueryAttribute attribute : query.getAttributes()){
                if(attribute.isAggregate())
                    aggregateAttribute = attribute;
            }
            GenericQueryRequest queryRequest = new GenericQueryRequest(colleaguesAtWorkQueryID, false, true);
            // Here read the filters from the query and apply it properly if it is an interactive filter
            for(GenericQueryFilters queryFilter : query.getFilters()){
                if(queryFilter.isInteractiveFilter()){
                    if(queryRequest.getPeAliasToLoadParamsMap().containsKey(queryFilter.getTargetMtPEAlias())){
                        LoadParameters loadParameters = queryRequest.getPeAliasToLoadParamsMap().get(queryFilter.getTargetMtPEAlias());
                        // Now prepare filters from each of the above.
                        if(loadParameters.getFilters().containsKey(queryFilter.getAttributePathExpn())){
                            List<FilterOperatorValue> filterOperatorValues = loadParameters.getFilters().get(queryFilter.getAttributePathExpn());
                            prepareFinalAttributeFilterPath(queryFilter, filterOperatorValues, contextID);
                        }else{
                            List<FilterOperatorValue> filterOperatorValues = new ArrayList<>();
                            loadParameters.getFilters().put(queryFilter.getAttributePathExpn(), filterOperatorValues);
                            prepareFinalAttributeFilterPath(queryFilter, filterOperatorValues, contextID);
                        }
                    }else{
                        LoadParameters loadParameters = new LoadParameters();
                        queryRequest.getPeAliasToLoadParamsMap().put(queryFilter.getTargetMtPEAlias(), loadParameters);
                        List<FilterOperatorValue> filterOperatorValues = new ArrayList<>();
                        loadParameters.getFilters().put(queryFilter.getAttributePathExpn(), filterOperatorValues);
                        prepareFinalAttributeFilterPath(queryFilter, filterOperatorValues, contextID);
                    }
                }
            }
            try {
                UnitResponseData queryResponse = getQueryResult(queryRequest);
                Map<String, ProcessElementData> rootPEs = queryResponse.getPEData();
                for(Map.Entry<String, ProcessElementData> rootPE : rootPEs.entrySet()){
                    rootPE.getValue().setMtPEAlias(rootPE.getKey());
                    getCountFromDataFromChildPE(rootPE.getValue(), aggregateAttribute, jobRelationshipTypeVO, colleaguesAtWorkResponses);
                }
                // Now that we have query response we should be able to reach the appropriate attribute.

            } catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void prepareFinalAttributeFilterPath(GenericQueryFilters filterParams, List<FilterOperatorValue> filterOperatorValues, String contextID) {
        String finalAttributeValue = null;
        if(filterParams.getAttributeValue().equals("!{Person.PrimaryKeyID}") || filterParams.getAttributeValue().equals("!{PersonEventReason.PrimaryKeyID}")){
            finalAttributeValue = "'"+contextID+"'";
        }
        if(filterParams.getAttributeValue().equals("!{GroupMember.Group}")){
            Person person = TenantAwareCache.getPersonRepository().getPersonDetails(contextID);
            PersonGroup personGroup = person.getPersonGroups();
            StringBuilder attributeValue = new StringBuilder();
            for(int i =0; i< personGroup.getGroupIds().size(); i++){
                if(i!=0)
                    attributeValue.append(",");
                attributeValue.append("'"+personGroup.getGroupIds().get(i)+"'");
            }
            if(StringUtil.isDefined(attributeValue.toString())){
                finalAttributeValue = attributeValue.toString();
            }else{
                finalAttributeValue = "null";
            }

        }
        FilterOperatorValue filterOperatorValue = new FilterOperatorValue(filterParams.getOperator(), finalAttributeValue);
        filterOperatorValues.add(filterOperatorValue);
    }

    private void getCountFromDataFromChildPE(ProcessElementData rootPE, GenericQueryAttribute aggregateAttribute, JobRelationshipTypeVO jobRelationshipTypeVO, List<ColleaguesAtWorkResponse> colleaguesAtWorkResponses) {
        if(rootPE.getMtPEAlias().equals(aggregateAttribute.getMtPEAlias())){
            // Go to the aggregate result and get the response
            Map<String, AggregateData> aggregateResult = rootPE.getAggregateResult();
            AggregateData aggregateData = aggregateResult.get(aggregateAttribute.getAttributePath());
            if(aggregateData!=null) {
                Object value = aggregateData.getValue();
                if ((Double) value > 0) {
                    ColleaguesAtWorkResponse colleaguesAtWorkResponse = new ColleaguesAtWorkResponse();
                    colleaguesAtWorkResponse.setRelationshipName(jobRelationshipTypeVO.getName());
                    colleaguesAtWorkResponse.setRelationshipType(jobRelationshipTypeVO.getRelationshipTypeCode());
                    colleaguesAtWorkResponse.setCount(((Double) value).intValue());
                    colleaguesAtWorkResponse.setImage(Util.getURLforS3(jobRelationshipTypeVO.getImageID(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
                    colleaguesAtWorkResponses.add(colleaguesAtWorkResponse);
                }
            }
        }else{
            // get the first child of the first record set.
            List<Record> recordList = (List<Record>)rootPE.getRecordSet();
            Record record = recordList.get(0);
            ProcessElementData childPEData = record.getChildrenPEData().get(0);
            getCountFromDataFromChildPE(childPEData, aggregateAttribute, jobRelationshipTypeVO, colleaguesAtWorkResponses);
        }
    }

    private MetaHelper getLabelToAliasMap(JsonLabelAliasTracker aliasTracker, Boolean metaRequired, String requestMTPE, String baseTemplate, ScreenInstance layout) throws SQLException {
        Map<String, DoaMeta> result = new HashMap<>();
        Map<String, EntityMetadataVOX> doMetaMap = new HashMap<>();
        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        UserContext userContext = TenantContextHolder.getUserContext();
	    for(Map.Entry<String,AliasTrackerHelper> entry : aliasTracker.getDoaToJsonAliasMap().entrySet()){
            baseTemplate = null;
            String attrExpn = entry.getKey();
            DoaMeta aliasMeta = new DoaMeta();
            AliasTrackerHelper aliasHelper = entry.getValue();
            if(aliasHelper.getAliasNumber()!=null)
                aliasMeta.setAlias(aliasHelper.getAliasNumber().toString());
            if(metaRequired) {
                List<String> doaList = new ArrayList<>();
//                if (aliasHelper.getInternal()) {
//                    doaList.add(attrExpn);
//                } else {
//                    doaList = Util.getAttributePathFromExpression(attrExpn);
//                }
                if (!aliasHelper.getInternal()) {
                    doaList = Util.getAttributePathFromExpression(attrExpn);
                }
                // Meta is applicable only when we have only one attribute doa in the expn
                if (doaList.size() == 1) {
                    String doaCode = Util.getDOAFromAttributePath(doaList.get(0));
                    if (!StringUtil.isDefined(baseTemplate)) {
                        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
                        baseTemplate = metadataService.getBtAttributeMetaListByMtDoa(doaCode).get(0).getBaseTemplateId();
//                        baseTemplate = personService.getDefaultBTForGroup(mtPE, userContext.getPersonId());
                    }
                    EntityAttributeMetadata doaMeta = metadataService.getMetadataByBtDoa(baseTemplate, doaCode);
                    EntityMetadataVOX doMeta = metadataService.getMetadataByBtDo(baseTemplate, doaMeta.getDomainObjectCode());
                    aliasMeta.setMeta(doaMeta);
//                    if(doaMeta.getCountryCodeToMetaMap()!=null){
//                        PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
//                        Person person = personService.getPersonDetails(userContext.getPersonId());
//                        CountrySpecificDOAMeta specificDOAMeta = doaMeta.getCountryCodeToMetaMap().get(person.getBaseCountryCodeFkId());
//                        if(specificDOAMeta!=null){
//                            doaMeta.setCountryAttrDisplayName(specificDOAMeta.getDoaDisplayNameOverrideG11nBigTxt());
//                            doaMeta.setAttrIcon(specificDOAMeta.getDoaDisplayIconCodeFkId());
//                            //TODO add here the visible, editable, and sensitive data
//                        }
//                    }
                    doMetaMap.put(doMeta.getDomainObjectCode(), doMeta);
                }
            }
            result.put(attrExpn, aliasMeta);
        }
        if(layout!=null){
	        if(layout.getListOfListOfControls()!=null && !layout.getListOfListOfControls().isEmpty()){
	            for(List<SectionControl> controlList : layout.getListOfListOfControls()){
	                for(SectionControl control : controlList){
	                    if(StringUtil.isDefined(control.getMtPEAlias()) && control.getControlType().equals("EFFECTIVE_DATE_TIME")){
	                        /* Get create access permissions for logged in user for this MTPE.EffectiveDateTime */
                            // Logged in USER groups -- AM-defn+MTPE will give us all the row conditions then get column conditions in that search for .EffectiveDatetime. If present
                            // Is mtpe acess control governed
                            /* Prepare for the response */
                            MasterEntityMetadataVOX meta = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(control.getMtPE());
                            if(meta.isAccessControlGoverned()){
                                Permission permission = accessManagerService.getEffectiveDatedPermissionForPerson(control.getMtPE());
                                String effectiveDateTimeDOACode = meta.getDomainObjectCode()+"."+"EffectiveDateTime";
                                MasterEntityAttributeMetadata effectiveDataMeta = meta.getAttributeNameMap().get(effectiveDateTimeDOACode);
                                if(effectiveDataMeta!=null){
                                    DOAPermission effectiveDateTimePermission = permission.getAttributePermission().get(effectiveDateTimeDOACode);
                                    control.setAccessControlGoverned(true);
                                    control.setCurrentCreateAllowed(effectiveDateTimePermission.getCreateCurrentPermitted());
                                    control.setFutureCreateAllowed(effectiveDateTimePermission.getCreateFuturePermitted());
                                    control.setHistoryCreateAllowed(effectiveDateTimePermission.getCreateHistoryPermitted());
                                }
                            }
                        }
                       if(StringUtil.isDefined(control.getAttributePathExpn())){
                           String attrExpn = control.getAttributePathExpn();
                           if(!aliasTracker.getDoaToJsonAliasMap().containsKey(attrExpn)){
                               List<String> doaList = new ArrayList<>();
                               doaList = Util.getAttributePathFromExpression(attrExpn);
                               if (doaList.size() == 1) {
                                   DoaMeta aliasMeta = new DoaMeta();
                                   baseTemplate = null;
                                   String doaCode = Util.getDOAFromAttributePath(doaList.get(0));
                                   if (!StringUtil.isDefined(baseTemplate)) {
                                       PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
                                       baseTemplate = metadataService.getBtAttributeMetaListByMtDoa(doaCode).get(0).getBaseTemplateId();
                                   }
                                   EntityAttributeMetadata doaMeta = metadataService.getMetadataByBtDoa(baseTemplate, doaCode);
                                   EntityMetadataVOX doMeta = metadataService.getMetadataByBtDo(baseTemplate, doaMeta.getDomainObjectCode());
                                   aliasMeta.setMeta(doaMeta);
                                   doMetaMap.put(doMeta.getDomainObjectCode(), doMeta);
                                   int aliasTrackerNumber = TenantAwareCache.getMetaDataRepository().getAlias(attrExpn);
                                   aliasMeta.setAlias(String.valueOf(aliasTrackerNumber));
                                   result.put(attrExpn, aliasMeta);
                               }
                           }
                       }

                    }
                }
            }
        }
        MetaHelper metaHelper = new MetaHelper();
	    metaHelper.setLabelToAliasMap(result);
	    if(StringUtil.isDefined(requestMTPE) && !doMetaMap.containsKey(requestMTPE)){
            if (!StringUtil.isDefined(baseTemplate)) {
                PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
                baseTemplate = personService.getDefaultBTForGroup(requestMTPE, userContext.getPersonId());
            }
            EntityMetadataVOX doMeta = metadataService.getMetadataByProcessElement(baseTemplate, requestMTPE);
            doMetaMap.put(doMeta.getDomainObjectCode(), doMeta);
        }
	    metaHelper.setDoMetaMap(doMetaMap);
        return metaHelper;
    }

    @Override
    public UnitResponseData getAnalyticsMetricDetailsData(AnalyticsMetricRequest analyticsMetricRequest) {
	    /*
	    TODO apply filter expressions
	     */
        UnitResponseData response = new UnitResponseData();
        List<PERequest> rootPEList = new ArrayList<>();
        Map<String, ProcessElementData> result = new HashMap<>();
        Map<String, LoadParameters> peToLoadParamsMap = analyticsMetricRequest.getPeToLoadParamsMap();
        Map<String, PERequest> peAliasToRequestMap = new HashMap<>();
        for (SectionControl control : analyticsMetricRequest.getMetricQueryList()) {
            String mtPEAlias = control.getMtPEAlias();
            String parentPEAlias = control.getParentMTPEAlias();
            // Only if it is a non static control it has the potential to make a SELECT statement
            if (!control.isStaticControl() && !Util.isHardcodedControlType(control.getControlType())) {
                if (peAliasToRequestMap.containsKey(mtPEAlias)) {
                    PERequest peRequest = peAliasToRequestMap.get(mtPEAlias);
                    addNewAttributePath(peRequest, control);
                } else {
                    PERequest newPERequest = new PERequest(mtPEAlias, parentPEAlias, null, control.getMtPE(), analyticsMetricRequest.getPeToLoadParamsMap().get(mtPEAlias), false, true, null);
                    Map<String, List<FilterOperatorValue>> finalFilters = segregateFilters(analyticsMetricRequest.getPeToLoadParamsMap().get(mtPEAlias), newPERequest);
                    newPERequest.setFilterExpression(Util.getFilterExpnFromFilters(finalFilters));
                    newPERequest.setHavingExpression(Util.getHavingExpnFromFilters(finalFilters));
                    peAliasToRequestMap.put(mtPEAlias, newPERequest);
                    if(StringUtil.isDefined(parentPEAlias)){
                        String[] relatedDoaSplit = control.getFkRelnWithParent().split("\\|");
                        DependencyKeysHelper keysHelper = new DependencyKeysHelper(relatedDoaSplit[0], relatedDoaSplit[1], mtPEAlias, parentPEAlias);
                        newPERequest.getDependencyKeyList().add(keysHelper);
                    }
                    addNewAttributePath(newPERequest, control);
                }
//                    if(control.isStickyHdr()){
//                        if(mtPEAliasToStickyHeaderControls.containsKey(control.getMtPEAlias())){
//                            mtPEAliasToStickyHeaderControls.get(control.getMtPEAlias()).add(control);
//                        }else{
//                            List<SectionControl> stickyControlList = new ArrayList<>();
//                            stickyControlList.add(control);
//                            mtPEAliasToStickyHeaderControls.put(control.getMtPEAlias(), stickyControlList);
//                        }
//
//                    }
            }
        }
//        for(SectionControl control : analyticsMetricRequest.getMetricQueryList()){
//            String processElement = control.getProcessElementFkId();
//            String mtPE = control.getProcessElementFkId();
//            String parentPE = null;
//            if(StringUtil.isDefined(control.getFkRelationshipWithParentLngTxt())){
//                parentPE = control.getFkRelationshipWithParentLngTxt().split("\\|")[0].split("\\.")[0];
//            }
//            if (mtPEToPERequestMap.containsKey(mtPE)) {
//                PERequest peRequest = mtPEToPERequestMap.get(mtPE);
//                addNewAttributePath(peRequest, control,  parentPE, null);
//            } else {
//                PERequest newPERequest = new PERequest(processElement);
//                newPERequest.setParentPE(parentPE);
//                newPERequest.setSelectType(RequestSelectType.LAYOUT);
//                addNewAttributePath(newPERequest, control, parentPE, null);
//                mtPEToPERequestMap.put(mtPE, newPERequest);
//            }
//        }
        for(Map.Entry<String,PERequest> entry : peAliasToRequestMap.entrySet()){
            PERequest peRequest = entry.getValue();
            String parentPE = peRequest.getParentPEAlias();
            addGroupByDetailsToGroupByFunctions(peRequest);
            if(StringUtil.isDefined(parentPE)) {
                peAliasToRequestMap.get(parentPE).getChildrenPE().add(entry.getValue());
                entry.getValue().setParentMTPEAliasNode(peAliasToRequestMap.get(parentPE));
            }else {
                peRequest.setKeyWord(null);
                rootPEList.add(peRequest);
            }
        }
//        for(Map.Entry<String,PERequest> entry : mtPEToPERequestMap.entrySet()){
//            PERequest peRequest = entry.getValue();
//            // Change the group by control attribute also here
//            addGroupByDetailsToGroupByFunctions(peRequest);
//            String parentPE = peRequest.getParentPE();
//            if(peToLoadParamsMap!=null) {
//                if(peToLoadParamsMap.get(peRequest.getMtPE())!=null) {
//                    String filterExpn = peToLoadParamsMap.get(peRequest.getMtPE()).getFilterExpn();
//                    peRequest.setFilterExpression(filterExpn);
//                }
//            }
//            if(StringUtil.isDefined(parentPE)) {
//                mtPEToPERequestMap.get(parentPE).getChildrenPE().add(entry.getValue());
//            }else {
//                rootPEList.add(peRequest);
//            }
//        }
        JsonLabelAliasTracker aliasTracker = new JsonLabelAliasTracker();
        for(PERequest rootPE : rootPEList){
            ProcessElementData peResponse = null;
            try {
                peResponse = dataService.getDataInternal(rootPE, aliasTracker, null, response);
                result.put(peResponse.getMtPE(),peResponse);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        response.setPEData(result);
        MetaHelper metaHelper = null;
        try {
            metaHelper = getLabelToAliasMap(aliasTracker, false, null, null, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        response.setLabelToAliasMap(metaHelper.getLabelToAliasMap());
        return response;
    }

    @Override
    public void populateReportData(String groupID, String timeZone) throws SQLException, IOException {
        // Now we have to populate the particular relation control data.
        // We should first get the target section instance ID
        // for this we need to
        // REPORTING_DATA_SECN
        ScreenRequest screen = new ScreenRequest();
        screen.setDeviceType("MOBILE");
        screen.setLayoutRequired(true);
        screen.setDataRequired(true);
        screen.setMetaRequired(true);
        String reportingSectionID = uiLayoutService.getReportingSectionID();
        if(StringUtil.isDefined(reportingSectionID)){
            UnitResponseData responseData = getScreen(screen);
            ScreenInstance layout = responseData.getLayout();
            // We need a mapping of DOA to Control Instance ID for analytics
            List<AnalyticsDOAControlMap> doaToControlMap = TenantAwareCache.getMetaDataRepository().getAnalyticsDoaToControlMap();
            // Now we have got doa=> control map
            for(AnalyticsDOAControlMap analyticsDOAControlMap : doaToControlMap){
                if(layout.getControlMap().containsKey(analyticsDOAControlMap.getControlInstanceID())){
                    SectionControl control = layout.getControlMap().get(analyticsDOAControlMap.getControlInstanceID());
                    // Now here we have to get the current value of the record.. that is the main motive.
                    Object currentValue = getDataForControl(control, responseData);
                }
            }
            // Person
            // Now get the data from multiple tables and populate it over here.
            // Now that we have got layout
            // Now we have to read this response populate
        }else{
            try {
                throw new Exception("Reporting section no found.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Object getDataForControl(SectionControl control, UnitResponseData responseData) {
        String controlMTPEAliasHierarchy = control.getParentMTPEAliasHierarchy();
        String[] splitHierarchy = controlMTPEAliasHierarchy.split("\\|");
        ProcessElementData currentPEData = responseData.getPEData().get(splitHierarchy[splitHierarchy.length-1]);
        for(int i = splitHierarchy.length-1; i>=0; i--){
            if(i == 0){
                // fetch the value from this PEData
                // Now look at this current PE DATA
                if(currentPEData!=null){
                    if(currentPEData.getRecordSetInternal()!=null && !currentPEData.getRecordSetInternal().isEmpty()) {
                        Record record = currentPEData.getRecordSetInternal().get(0);
                        AttributeData attributeData = record.getAttributeMap().get(responseData.getLabelToAliasMap().get(control.getAttributePathExpn()).getAlias());
                        if(attributeData!=null){
                            return attributeData.getcV();
                        }
                    }
                }
            }else{
                // Loop through the children PE make it the current PE
                String mtPEAlias = splitHierarchy[i-1];
                if(currentPEData!=null){
                    if(currentPEData.getRecordSetInternal()!=null && !currentPEData.getRecordSetInternal().isEmpty()) {
                        Record record = currentPEData.getRecordSetInternal().get(0);
                        for(ProcessElementData processElementData : record.getChildrenPEData()){
                            if(processElementData.getMtPEAlias().equals(mtPEAlias)){
                                currentPEData = processElementData;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Making of Process Element Request on the basis of Screen Request and layout
     * @param screen
     * @param layout
     * @return
     */
    private List<PERequest> buildPERequest(ScreenRequest screen, ScreenInstance layout) {
        List<PERequest> rootPEList = new ArrayList<>();
        List<List<SectionControl>> listOfListOfControls = layout.getListOfListOfControls();
        Map<String, List<SectionControl>> mtPEAliasToStickyHeaderControls = new HashMap<>();
        Map<String, PERequest> peAliasToRequestMap = new HashMap<>();
        // Now from each one of these make a POJO which will actually have all the required fields for this particular controller.
        for (List<SectionControl> controlList : listOfListOfControls) {
            for (SectionControl control : controlList) {
                control.setParentMTPEAliasHierarchy(control.getMtPEAlias());
                String mtPEAlias = control.getMtPEAlias();
                String parentPEAlias = control.getParentMTPEAlias();
                // Only if it is a non static control it has the potential to make a SELECT statement
                if (!control.isStaticControl() && !Util.isHardcodedControlType(control.getControlType()) && !Util.isOnlyFilterControl(control.getSectionMasterCode(), screen.getPeAliasToLoadParamsMap(), control.getMtPEAlias())) {
                    if (peAliasToRequestMap.containsKey(mtPEAlias)) {
                        PERequest peRequest = peAliasToRequestMap.get(mtPEAlias);
                        addNewAttributePath(peRequest, control);
                    } else {
                        PERequest newPERequest = new PERequest(mtPEAlias, parentPEAlias, screen.getBaseTemplate(), control.getMtPE(), screen.getPeAliasToLoadParamsMap().get(mtPEAlias), false, true, layout.getEndorseMTPEAlias());
                        Map<String, List<FilterOperatorValue>> finalFilters = segregateFilters(screen.getPeAliasToLoadParamsMap().get(mtPEAlias), newPERequest);
                        newPERequest.setFilterExpression(Util.getFilterExpnFromFilters(finalFilters));
                        newPERequest.setHavingExpression(Util.getHavingExpnFromFilters(finalFilters));
                        peAliasToRequestMap.put(mtPEAlias, newPERequest);
                        if(StringUtil.isDefined(parentPEAlias)){
                            String[] relatedDoaSplit = control.getFkRelnWithParent().split("\\|");
                            DependencyKeysHelper keysHelper = new DependencyKeysHelper(relatedDoaSplit[0], relatedDoaSplit[1], mtPEAlias, parentPEAlias);
                            newPERequest.getDependencyKeyList().add(keysHelper);
                        }
                        addNewAttributePath(newPERequest, control);
                    }
//                    if(control.isStickyHdr()){
//                        if(mtPEAliasToStickyHeaderControls.containsKey(control.getMtPEAlias())){
//                            mtPEAliasToStickyHeaderControls.get(control.getMtPEAlias()).add(control);
//                        }else{
//                            List<SectionControl> stickyControlList = new ArrayList<>();
//                            stickyControlList.add(control);
//                            mtPEAliasToStickyHeaderControls.put(control.getMtPEAlias(), stickyControlList);
//                        }
//
//                    }
                }
            }
        }
        for(Map.Entry<String, List<SectionControl>> entry : mtPEAliasToStickyHeaderControls.entrySet()){
            List<SectionControl> controlList = entry.getValue();
            Collections.sort(controlList, new Comparator<SectionControl>()
                    {
                        public int compare(SectionControl control1, SectionControl control2)
                        {
                            int seq1 = control1.getStickyHdrSeqNum();
                            int seq2 = control2.getStickyHdrSeqNum();
                            if(seq1 == seq2)
                                return 0;
                            else if(seq1 > seq2)
                                return 1;
                            else
                                return -1;
                        }
                    }
            );
        }
        //TODO Add MTPEs for the statement to be able to complete the target filters.
        for(TargetSectionFilterParams targetSectionFilterParams : layout.getListOfTargetSectionsFilters()){
            String mtPEAlias = targetSectionFilterParams.getCurrentMTPEAlias();
            String parentPEAlias = targetSectionFilterParams.getCurrentParentMTPEAlias();
            if(StringUtil.isDefined(mtPEAlias)) {
                if (peAliasToRequestMap.containsKey(mtPEAlias)) {
                    PERequest peRequest = peAliasToRequestMap.get(mtPEAlias);
                    AttributePaths attributePaths = peRequest.getAttributePaths();
                    attributePaths.getAttributePaths().add(targetSectionFilterParams.getAttributeValue());
                } else {
                    PERequest newPERequest = new PERequest(mtPEAlias, parentPEAlias, screen.getBaseTemplate(), targetSectionFilterParams.getCurrentMTPE(),  screen.getPeAliasToLoadParamsMap().get(mtPEAlias), false, true, layout.getEndorseMTPEAlias());
                    Map<String, List<FilterOperatorValue>> finalFilters = segregateFilters(screen.getPeAliasToLoadParamsMap().get(mtPEAlias), newPERequest);
                    newPERequest.setFilterExpression(Util.getFilterExpnFromFilters(finalFilters));
                    newPERequest.setHavingExpression(Util.getHavingExpnFromFilters(finalFilters));
                    peAliasToRequestMap.put(mtPEAlias, newPERequest);
                    if (StringUtil.isDefined(parentPEAlias)) {
                        String[] relatedDoaSplit = targetSectionFilterParams.getCurrentFkRelnWithParent().split("\\|");
                        DependencyKeysHelper keysHelper = new DependencyKeysHelper(relatedDoaSplit[0], relatedDoaSplit[1], mtPEAlias, parentPEAlias);
                        newPERequest.getDependencyKeyList().add(keysHelper);
                    }
                    AttributePaths attributePaths = newPERequest.getAttributePaths();
                    attributePaths.getAttributePaths().add(targetSectionFilterParams.getAttributeValue());
                }
            }
        }
        if(screen.getOnDemandAttributes() != null && !screen.getOnDemandAttributes().isEmpty()) {
            for (RelationControlOnDemandAttributes onDemandAttribute : screen.getOnDemandAttributes()) {
                String mtPEAlias = onDemandAttribute.getMtPEAlias();
                String parentPEAlias = onDemandAttribute.getParentMTPEAlias();
                if (StringUtil.isDefined(mtPEAlias)) {
                    if (peAliasToRequestMap.containsKey(mtPEAlias)) {
                        PERequest peRequest = peAliasToRequestMap.get(mtPEAlias);
                        AttributePaths attributePaths = peRequest.getAttributePaths();
                        attributePaths.getAttributePaths().add(onDemandAttribute.getAttributePathExpn());
                    } else {
                        PERequest newPERequest = new PERequest(mtPEAlias, parentPEAlias, screen.getBaseTemplate(), onDemandAttribute.getMtPE(), screen.getPeAliasToLoadParamsMap().get(mtPEAlias), false, true, layout.getEndorseMTPEAlias());
                        Map<String, List<FilterOperatorValue>> finalFilters = segregateFilters(screen.getPeAliasToLoadParamsMap().get(mtPEAlias), newPERequest);
                        newPERequest.setFilterExpression(Util.getFilterExpnFromFilters(finalFilters));
                        newPERequest.setHavingExpression(Util.getHavingExpnFromFilters(finalFilters));
                        peAliasToRequestMap.put(mtPEAlias, newPERequest);
                        if (StringUtil.isDefined(parentPEAlias)) {
                            String[] relatedDoaSplit = onDemandAttribute.getFkRelnWithParent().split("\\|");
                            DependencyKeysHelper keysHelper = new DependencyKeysHelper(relatedDoaSplit[0], relatedDoaSplit[1], mtPEAlias, parentPEAlias);
                            newPERequest.getDependencyKeyList().add(keysHelper);
                        }
                        AttributePaths attributePaths = newPERequest.getAttributePaths();
                        attributePaths.getAttributePaths().add(onDemandAttribute.getAttributePathExpn());
                    }
                }
            }
        }

        for(ActionArgument actionArgument : layout.getListOfActionArguments()){
            String mtPEAlias = actionArgument.getMtPEAlias();
            String parentPEAlias = actionArgument.getParentMTPEAlias();
            if(StringUtil.isDefined(mtPEAlias)) {
                if (peAliasToRequestMap.containsKey(mtPEAlias)) {
                    PERequest peRequest = peAliasToRequestMap.get(mtPEAlias);
                    AttributePaths attributePaths = peRequest.getAttributePaths();
                    attributePaths.getAttributePaths().add(actionArgument.getAttributeValue());
                } else {
                    PERequest newPERequest = new PERequest(mtPEAlias, parentPEAlias, screen.getBaseTemplate(), actionArgument.getMtPE(),  screen.getPeAliasToLoadParamsMap().get(mtPEAlias), false, true, layout.getEndorseMTPEAlias());
                    Map<String, List<FilterOperatorValue>> finalFilters = segregateFilters(screen.getPeAliasToLoadParamsMap().get(mtPEAlias), newPERequest);
                    newPERequest.setFilterExpression(Util.getFilterExpnFromFilters(finalFilters));
                    newPERequest.setHavingExpression(Util.getHavingExpnFromFilters(finalFilters));
                    peAliasToRequestMap.put(mtPEAlias, newPERequest);
                    if (StringUtil.isDefined(parentPEAlias)) {
                        String[] relatedDoaSplit = actionArgument.getFkRelnWithParent().split("\\|");
                        DependencyKeysHelper keysHelper = new DependencyKeysHelper(relatedDoaSplit[0], relatedDoaSplit[1], mtPEAlias, parentPEAlias);
                        newPERequest.getDependencyKeyList().add(keysHelper);
                    }
                    AttributePaths attributePaths = newPERequest.getAttributePaths();
                    attributePaths.getAttributePaths().add(actionArgument.getAttributeValue());
                }
            }
        }
        for(TargetFilterCriteria filterCriteria : layout.getListOfFilterCriteria()){
            String mtPEAlias = filterCriteria.getMtPEAlias();
            String parentPEAlias = filterCriteria.getParentMTPEAlias();
            if(StringUtil.isDefined(mtPEAlias)) {
                if (peAliasToRequestMap.containsKey(mtPEAlias)) {
                    PERequest peRequest = peAliasToRequestMap.get(mtPEAlias);
                    AttributePaths attributePaths = peRequest.getAttributePaths();
                    attributePaths.getAttributePaths().add(filterCriteria.getFilterParameter());
                } else {
                    PERequest newPERequest = new PERequest(mtPEAlias, parentPEAlias, screen.getBaseTemplate(), filterCriteria.getMtPEAlias(),  screen.getPeAliasToLoadParamsMap().get(mtPEAlias), false, true, layout.getEndorseMTPEAlias());
                    Map<String, List<FilterOperatorValue>> finalFilters = segregateFilters(screen.getPeAliasToLoadParamsMap().get(mtPEAlias), newPERequest);
                    newPERequest.setFilterExpression(Util.getFilterExpnFromFilters(finalFilters));
                    newPERequest.setHavingExpression(Util.getHavingExpnFromFilters(finalFilters));
                    peAliasToRequestMap.put(mtPEAlias, newPERequest);
                    if (StringUtil.isDefined(parentPEAlias)) {
                        String[] relatedDoaSplit = filterCriteria.getFkRelnWithParent().split("\\|");
                        DependencyKeysHelper keysHelper = new DependencyKeysHelper(relatedDoaSplit[0], relatedDoaSplit[1], mtPEAlias, parentPEAlias);
                        newPERequest.getDependencyKeyList().add(keysHelper);
                    }
                    AttributePaths attributePaths = newPERequest.getAttributePaths();
                    attributePaths.getAttributePaths().add(filterCriteria.getFilterParameter());
                }
            }
        }
        for(Map.Entry<String, SubjectUserPath> entry : layout.getMtPEAliasToSubjectUserPathMap().entrySet()){
            String mtPEAlias = entry.getKey();
            SubjectUserPath subjectUserPath = entry.getValue();
            String parentPEAlias = subjectUserPath.getParentMTPEAlias();
            if(StringUtil.isDefined(mtPEAlias)) {
                if (peAliasToRequestMap.containsKey(mtPEAlias)) {
                    PERequest peRequest = peAliasToRequestMap.get(mtPEAlias);
                    peRequest.setSubjectUserPath(subjectUserPath.getAttributePathExpn());
                    AttributePaths attributePaths = peRequest.getAttributePaths();
                    attributePaths.getAttributePaths().add(subjectUserPath.getAttributePathExpn());
                } else {
                    PERequest newPERequest = new PERequest(mtPEAlias, parentPEAlias, screen.getBaseTemplate(), subjectUserPath.getMtPEAlias(),  screen.getPeAliasToLoadParamsMap().get(mtPEAlias), false, true, layout.getEndorseMTPEAlias());
                    Map<String, List<FilterOperatorValue>> finalFilters = segregateFilters(screen.getPeAliasToLoadParamsMap().get(mtPEAlias), newPERequest);
                    newPERequest.setFilterExpression(Util.getFilterExpnFromFilters(finalFilters));
                    newPERequest.setHavingExpression(Util.getHavingExpnFromFilters(finalFilters));
                    peAliasToRequestMap.put(mtPEAlias, newPERequest);
                    if (StringUtil.isDefined(parentPEAlias)) {
                        String[] relatedDoaSplit = subjectUserPath.getFkRelationshipWithParent().split("\\|");
                        DependencyKeysHelper keysHelper = new DependencyKeysHelper(relatedDoaSplit[0], relatedDoaSplit[1], mtPEAlias, parentPEAlias);
                        newPERequest.getDependencyKeyList().add(keysHelper);
                    }
                    newPERequest.setSubjectUserPath(subjectUserPath.getAttributePathExpn());
                    AttributePaths attributePaths = newPERequest.getAttributePaths();
                    attributePaths.getAttributePaths().add(subjectUserPath.getAttributePathExpn());
                }
            }
        }
        // Make the process element alias parent child relationship
        for(Map.Entry<String,PERequest> entry : peAliasToRequestMap.entrySet()){
            PERequest peRequest = entry.getValue();
            String parentPE = peRequest.getParentPEAlias();
            addGroupByDetailsToGroupByFunctions(peRequest);
            if(StringUtil.isDefined(parentPE)) {
                peAliasToRequestMap.get(parentPE).getChildrenPE().add(entry.getValue());
                entry.getValue().setParentMTPEAliasNode(peAliasToRequestMap.get(parentPE));
            }else {
                // If the root PE Menu add it at last else add it at second last
                if(peRequest.getMtPE().equals("Menu")){
                    rootPEList.add(peRequest);
                }else{
                    rootPEList.add(0, peRequest);
                }
            }
        }
        for (List<SectionControl> controlList : listOfListOfControls) {
            for (SectionControl control : controlList) {
                if (!control.isStaticControl() && !Util.isHardcodedControlType(control.getControlType()) && !control.getControlType().equals("COLLEG_IMAGE") && !control.getControlType().equals("COLLEG_TEXT")&& !control.getControlType().equals("COLLEG_COUNT") && !Util.isOnlyFilterControl(control.getSectionMasterCode(), screen.getPeAliasToLoadParamsMap(), control.getMtPEAlias())) {
                    String mtPEAlias = control.getMtPEAlias();
//                    if(mtPEAliasToStickyHeaderControls.containsKey(mtPEAlias)){
//                        List<SectionControl> stickyControls = mtPEAliasToStickyHeaderControls.get(mtPEAlias);
//                        StringBuilder stickyAliasHierarchy = new StringBuilder(mtPEAlias);
//                        for(int i =0 ; i< stickyControls.size(); i++){
//                            SectionControl stickyControl = stickyControls.get(i);
//                            // If this is not a sticky control
//                            if(control.isStickyHdr()){
//                                if(control.getStickyHdrSeqNum() == i-1){
//                                    control.setParentMTPEAliasHierarchy(stickyAliasHierarchy.toString());
//                                }
//                            }
//                            stickyAliasHierarchy.append("|");
//                            stickyAliasHierarchy.append(stickyControl.getMtPEAlias());
//                        }
//                        control.setParentMTPEAliasHierarchy(stickyAliasHierarchy.toString());
//                    }
                    PERequest peRequest = peAliasToRequestMap.get(mtPEAlias);
                    StringBuilder parentPEHierarchy = null;
                    if(StringUtil.isDefined(control.getParentMTPEAliasHierarchy())){
                        parentPEHierarchy = new StringBuilder(control.getParentMTPEAliasHierarchy());
                    }else{
                        parentPEHierarchy = new StringBuilder(peRequest.getMtPEAlias());
                    }

                    PERequest parentPERequest = peRequest.getParentMTPEAliasNode();
                    while (parentPERequest != null) {
                        parentPEHierarchy.append("|").append(parentPERequest.getMtPEAlias());
                        parentPERequest = parentPERequest.getParentMTPEAliasNode();
                    }
                    control.setParentMTPEAliasHierarchy(parentPEHierarchy.toString());
                }
            }
        }
        return rootPEList;
    }
    private Map<String, List<FilterOperatorValue>> segregateFilters(LoadParameters loadParameters, PERequest newPERequest) {
        if(loadParameters == null)
            return null;
        Map<String,List<FilterOperatorValue>> filters= loadParameters.getFilters();
        Map<String,List<FilterOperatorValue>> finalFilters= new HashMap<>();
        finalFilters.putAll(filters);
        for(Map.Entry<String,List<FilterOperatorValue>> entry : filters.entrySet()){
            String doaExpn = entry.getKey();
            List<FilterOperatorValue> filterOperatorValues = entry.getValue();
            List<FilterOperatorValue> finalFilterOperatorValues = new ArrayList<>();
            finalFilterOperatorValues.addAll(filterOperatorValues);
            for(FilterOperatorValue filterOperatorValue : filterOperatorValues){
                // If parent exists and isEliminate from the root is false ==> It should become control attribute filter
                if(!filterOperatorValue.isEliminateResultFromRoot() && StringUtil.isDefined(newPERequest.getParentPEAlias())){
                    addNonInteractiveFilterToPE(newPERequest, doaExpn, filterOperatorValue.getOperator(), filterOperatorValue.getValue(), filterOperatorValue.isHavingClause());
                    // Now remove this from the loadParameters filter list
                    finalFilterOperatorValues.remove(filterOperatorValue);
                }
            }
            if(finalFilterOperatorValues.isEmpty()){
                finalFilters.remove(entry.getKey());
            }else{
                finalFilters.get(entry.getKey()).retainAll(finalFilterOperatorValues);
            }
        }
        return finalFilters;
    }

    private List<PERequest> buildPERequest(GenericQueryRequest queryRequest, GenericQuery query) {
        List<PERequest> rootPEList = new ArrayList<>();
        Map<String, PERequest> peAliasToRequestMap = new HashMap<>();
        for(GenericQueryAttribute attribute : query.getAttributes()){
            String mtPEAlias = attribute.getMtPEAlias();
            String parentPEAlias = attribute.getParentMtPEAlias();
            if (peAliasToRequestMap.containsKey(mtPEAlias)) {
                PERequest peRequest = peAliasToRequestMap.get(mtPEAlias);
                addNewAttributePath(peRequest, attribute);
            } else {
                PERequest newPERequest = new PERequest(mtPEAlias, parentPEAlias, null, attribute.getMtPE(), queryRequest.getPeAliasToLoadParamsMap().get(mtPEAlias), queryRequest.isReturnOnlySqlQuery(), queryRequest.isApplyPagination(), null);
                Map<String, List<FilterOperatorValue>> finalFilters = segregateFilters(queryRequest.getPeAliasToLoadParamsMap().get(mtPEAlias), newPERequest);
                newPERequest.setFilterExpression(Util.getFilterExpnFromFilters(finalFilters));
                newPERequest.setHavingExpression(Util.getHavingExpnFromFilters(finalFilters));
                for(GenericQueryFilters queryFilters : query.getFilters()){
                    if(queryFilters.getTargetMtPEAlias().equals(newPERequest.getMtPEAlias())){
                        if(!queryFilters.isInteractiveFilter()){
                            addNonInteractiveFilterToPE(newPERequest, queryFilters.getAttributePathExpn(), queryFilters.getOperator(), queryFilters.getAttributeValue(), queryFilters.isHavingClause());
                        }
                    }
                }
                peAliasToRequestMap.put(mtPEAlias, newPERequest);
                if(StringUtil.isDefined(parentPEAlias)){
                    String[] relatedDoaSplit = attribute.getFkRelnWithParent().split("\\|");
                    DependencyKeysHelper keysHelper = new DependencyKeysHelper(relatedDoaSplit[0], relatedDoaSplit[1], mtPEAlias, parentPEAlias);
                    newPERequest.getDependencyKeyList().add(keysHelper);
                }
                addNewAttributePath(newPERequest, attribute);
            }
        }
        for(Map.Entry<String,PERequest> entry : peAliasToRequestMap.entrySet()){
            PERequest peRequest = entry.getValue();
            String parentPE = peRequest.getParentPEAlias();
            addGroupByDetailsToGroupByFunctions(peRequest);
            if(StringUtil.isDefined(parentPE)) {
                peAliasToRequestMap.get(parentPE).getChildrenPE().add(entry.getValue());
                entry.getValue().setParentMTPEAliasNode(peAliasToRequestMap.get(parentPE));
            }else {
                rootPEList.add(peRequest);
            }
        }
        return rootPEList;
    }

    private void addNonInteractiveFilterToPE(PERequest newPERequest, String attributePathExpn, String operator, Object attributeValue, boolean havingClause){
        if(havingClause){
            if(newPERequest.getControlHavingClause().containsKey(attributePathExpn)){
                FilterOperatorValue filterOperatorValue = new FilterOperatorValue();
                filterOperatorValue.setOperator(operator);
                filterOperatorValue.setValue(attributeValue);
                newPERequest.getControlHavingClause().get(attributePathExpn).add(filterOperatorValue);
            }else{
                List<FilterOperatorValue> filterOperatorValues = new ArrayList<>();
                FilterOperatorValue filterOperatorValue = new FilterOperatorValue();
                filterOperatorValue.setOperator(operator);
                filterOperatorValue.setValue(attributeValue);
                filterOperatorValues.add(filterOperatorValue);
                newPERequest.getControlHavingClause().put(attributePathExpn, filterOperatorValues);
            }
        }else{
            if(newPERequest.getControlFilters().containsKey(attributePathExpn)){
                FilterOperatorValue filterOperatorValue = new FilterOperatorValue();
                filterOperatorValue.setOperator(operator);
                filterOperatorValue.setValue(attributeValue);
                newPERequest.getControlFilters().get(attributePathExpn).add(filterOperatorValue);
            }else{
                List<FilterOperatorValue> filterOperatorValues = new ArrayList<>();
                FilterOperatorValue filterOperatorValue = new FilterOperatorValue();
                filterOperatorValue.setOperator(operator);
                filterOperatorValue.setValue(attributeValue);
                filterOperatorValues.add(filterOperatorValue);
                newPERequest.getControlFilters().put(attributePathExpn, filterOperatorValues);
            }
        }
    }
    private void addGroupByDetailsToGroupByFunctions(PERequest peRequest) {
        Map<String, AttributePathDetails> doaExpnToAttrDetlsMap = peRequest.getAttributePaths().getAttributePathToAggDetailsMap();
        for(Map.Entry<String, AttributePathDetails> detailsEntry : doaExpnToAttrDetlsMap.entrySet()){
            AttributePathDetails attributePathDetails = detailsEntry.getValue();
            attributePathDetails.getGroupByAttributes().addAll(peRequest.getGroupByAttributeDetailsList());
//            attributePathDetails.setAggregateFunctionWhereClause(peRequest.getFilterExpression());
        }
    }

    private void addNewAttributePath(PERequest peRequest, GenericQueryAttribute attribute) {
        AttributePaths attributePaths = peRequest.getAttributePaths();
        if(attribute.isAggregate()){
            AttributePathDetails aggregateDetails = new AttributePathDetails(attribute.getAttributePath());
            attributePaths.getAttributePathToAggDetailsMap().put(aggregateDetails.getAttributePath(), aggregateDetails);
        }else{
            if(StringUtil.isDefined(attribute.getAttributePath()) && !attribute.getAttributePath().equals("")) {
                attributePaths.getAttributePaths().add(attribute.getAttributePath());
            }
        }
    }
    private void addNewAttributePath(PERequest peRequest, SectionControl control) {
        // We need a set of attribute paths.. But to be honest we don't care right now. We need a POJO which will be equivalent of Layout Control having all the fields required to make a SELECT statement.
        AttributePaths attributePaths = peRequest.getAttributePaths();
        if(StringUtil.isDefined(control.getMtPE())){
            if(StringUtil.isDefined(control.getsUMtPEAlias())){
                if(control.getMtPE().equals("Menu")){
                    peRequest.setMenuApplicableSubjectUserMTPEAlias(control.getsUMtPEAlias());
                    peRequest.setMenuApplicableSubjectUserPath(control.getsUAttributePathExpn());
                }
            }
        }
        if (control.isAggregateControl() && !control.isDataEmpty()) {
            AttributePathDetails aggregateDetails = new AttributePathDetails(control.getAttributePathExpn());
            attributePaths.getAttributePathToAggDetailsMap().put(aggregateDetails.getAttributePath(), aggregateDetails);
            if(StringUtil.isDefined(control.getApplicableGroupByControls())){
                String[] applicableGroupByControls = control.getApplicableGroupByControls().split("\\|");
                peRequest.getAggregateGroupByControlList().addAll(Arrays.asList(applicableGroupByControls));
            }
        } else if (control.isGroupByControl() && !control.isDataEmpty()) {
            GroupByAttributeDetails groupByAttributeDetails = new GroupByAttributeDetails(control.getAttributePathExpn(), control.getGroupByControlSequence(), false);
            peRequest.getGroupByAttributeDetailsList().add(groupByAttributeDetails);
            peRequest.getGroupByControlList().put(control.getControlPkId(), groupByAttributeDetails);
        } else {
            switch (ControlType.valueOfControlType(control.getControlType())) {
                case WRITE_TEXT_CONTROL:
                case RATING:
                case SAVED_ACTIVE_STATE:
                case COLLEG_IMAGE:
                case COLLEG_TEXT:
                case COLLEG_COUNT:
                case TAG_MASTER:
                case LOGGED_IN_PERSON_NAME:
                    break;
                case BOOKMARKED:
                    peRequest.setBookmarkRtrn(true);
                    break;
                case FEATURED:
                    // TODO
                    break;
                case TAG:
                    MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
                    MasterEntityMetadataVOX peMeta = metadataService.getMetadataByMtPE(peRequest.getMtPE());
                    PERequest tags = new PERequest();
                    tags.setMtPE(metadataService.getMetadataByMtPE(peRequest.getMtPE()).getTagPE());
                    tags.setMtPEAlias("Tags");
                    MasterEntityMetadataVOX tagMeta = metadataService.getMetadataByMtPE(tags.getMtPE());
                    peRequest.getChildrenPE().add(tags);
                    tags.getAttributePaths().getAttributePaths().add("!{"+tagMeta.getDomainObjectCode()+"."+"PrimaryKeyID"+"}");
                    tags.getAttributePaths().getAttributePaths().add("!{"+tagMeta.getDomainObjectCode()+"."+"PersonTagMasterID:PersonTagMaster.TagText"+"}");
                    tags.getAttributePaths().getAttributePaths().add("!{"+tagMeta.getDomainObjectCode()+"."+"PersonTagMasterID:PersonTagMaster.BackgroundColour"+"}");
                    tags.getAttributePaths().getAttributePaths().add("!{"+tagMeta.getDomainObjectCode()+"."+"PersonTagMasterID:PersonTagMaster.FontColour"+"}");
                    tags.getAttributePaths().getSearchableAttributePaths().add("!{"+tagMeta.getDomainObjectCode()+"."+"PersonTagMasterID:PersonTagMaster.TagText"+"}");
                    // private Map<String,List<FilterOperatorValue>> controlFilters = new HashMap<>();
                    FilterOperatorValue deleteFilter = new FilterOperatorValue("=", "0");
                    List<FilterOperatorValue> deleteFilterList = new ArrayList<>();
                    deleteFilterList.add(deleteFilter);
                    tags.getControlFilters().put("!{"+tagMeta.getDomainObjectCode()+"."+"Deleted}", deleteFilterList);
                    DependencyKeysHelper keysHelper = new DependencyKeysHelper(tagMeta.getDomainObjectCode()+".DOPrimaryKey", peMeta.getFunctionalPrimaryKeyAttribute().getDoAttributeCode(), tags.getMtPEAlias(),peRequest.getMtPEAlias());
                    tags.getDependencyKeyList().add(keysHelper);
                    break;
                case GRID:
                    //TODO copy it from data service IMPL
                    break;
                default:
                    if(StringUtil.isDefined(control.getAttributePathExpn()) && !control.getAttributePathExpn().equals("") && !control.isDataEmpty()) {
                        attributePaths.getAttributePaths().add(control.getAttributePathExpn());
                        attributePaths.getSearchableAttributePaths().add(control.getAttributePathExpn());
                    }
                    String addlAttributePath = control.getAdditionalAttributePathExpn();
                    /* Split this additional attribute path expn by piped delim and add it to the pe request */
                    if(StringUtil.isDefined(addlAttributePath) && (addlAttributePath.startsWith("!{") || addlAttributePath.startsWith("DISTINCT "))){
                        String[] addAttributePathList = addlAttributePath.split("\\|");
                        for(String path : addAttributePathList){
                            attributePaths.getAttributePaths().add(path);
                        }
                    }
                    List<String> rtrnAttributeList = new ArrayList<>();
                    if(StringUtil.isDefined(control.getStatusColourAttrPathExpn())){
                        attributePaths.getAttributePaths().add(control.getStatusColourAttrPathExpn());
                    }
                    /* Now split the paths for rtrn attributes */
//                    if(StringUtil.isDefined(control.getRelnCtrlRtnDispAttrPathExpn())){
//                    String[] rtrnAttributes = control.getRelnCtrlRtnDispAttrPathExpn().split("\\|");
//                    rtrnAttributeList.addAll(Arrays.asList(rtrnAttributes));

//                    for(String rtrnAttr : rtrnAttributeList){
//                        List<String> attrList = Util.getAttributePathFromExpression(rtrnAttr);
//                        String cntrolDOA = control.getAttributePathExpn().substring(2,control.getAttributePathExpn().indexOf("}"));
//                        for(String attr : attrList){
//                            String finalAttributePath = "!{"+cntrolDOA+":"+attr+"}";
//                            attributePaths.getAttributePaths().add(finalAttributePath);
//                        }
//                    }
//                }
                if(control.isStickyHdr())
                    addStickyHdrAttribute(control, attributePaths, 1, control.getAttributePathExpn());
            }
        }
        // Add the Sort data if any provided in this Control.
        int currentSortSequence = 0;
        if(peRequest.getSortData()!= null && peRequest.getSortData().size()!=0){
            Collections.sort(peRequest.getSortData(), new SortComparator());
            currentSortSequence = peRequest.getSortData().get(peRequest.getSortData().size()-1).getSeqNumber();
        }
        if(StringUtil.isDefined(control.getDefaultOnLoadSort())){
            if(!StringUtil.isDefined(control.getOrderByValues())) {
                // Split the attribute paths by AND.
                String[] splittedSortExpn = control.getDefaultOnLoadSort().split(" AND ");
                for (int i = 0; i < splittedSortExpn.length; i++) {
                    String[] sortFields = splittedSortExpn[i].trim().split(" ");
                    int attributeIndex = 0;
                    if (sortFields[0].equalsIgnoreCase("DISTINCT"))
                        attributeIndex = 1;
                    if (sortFields[attributeIndex + 1].trim().equalsIgnoreCase("ASC")) {
                        SortData sortData = new SortData(sortFields[attributeIndex], true, currentSortSequence + i + 1, null);
                        peRequest.getDefaultSortData().add(sortData);
                    } else {
                        SortData sortData = new SortData(sortFields[attributeIndex], false, currentSortSequence + i + 1, null);
                        peRequest.getDefaultSortData().add(sortData);
                    }
                }
            }else{
                SortData sortData = new SortData(control.getDefaultOnLoadSort(), true, currentSortSequence+1, control.getOrderByValues());
                peRequest.getDefaultSortData().add(sortData);
            }
        }
        if(!control.isApplyPagination()) {
            peRequest.setApplyPagination(control.isApplyPagination());
        }
        peRequest.setDefaultPaginationSize(control.getDefaultPaginationSize());
        for(ControlInstanceFilters controlFilters : control.getControlFilters()){
            addNonInteractiveFilterToPE(peRequest, controlFilters.getAttributePathExpn(), controlFilters.getOperator(), controlFilters.getAttributeValue(), controlFilters.isHavingClause());
        }
        // We have filters inside PE right now.. Now we have to convert it to the filter expn and append it with the filters passed by client.
    }
    private void addStickyHdrAttribute(SectionControl control, AttributePaths attributePaths, int sequenceNumber, String expn) {
        boolean isAscending = true;
        if(control.getStickyHeaderOrder()!=null && control.getStickyHeaderOrder().equals("DESC"))
            isAscending = false;
        SortData sortData = new SortData(expn, isAscending, sequenceNumber,control.getOrderByValues());
        attributePaths.getStickyHeaderOrderByList().add(sortData);
    }

    /**
     * Inner class for defining compare logic for Sorting sort request by sequence number.
     * @author Shubham Patodi
     *
     */
    class SortComparator implements Comparator<SortData>{
        @Override
        public int compare(SortData o1, SortData o2) {
            if(o1.getSeqNumber() > o2.getSeqNumber()){
                return 1;
            }else{
                return -1;
            }
        }
    }

//                    switch(ControlType.valueOfControlType(control.getControlTypeTxt())){
//                        case WRITE_TEXT_CONTROL:
//                            break;
//                        case BOOKMARKED:
//                            peRequest.setBookmarkRtrn(true);
//                            break;
//                        case FEATURED:
//                            PERequest featured = new PERequest();
//                            featured.setMtPE("Featured");
//                            featured.setSelectType(RequestSelectType.LAYOUT);
//                            peRequest.getChildrenPE().add(featured);
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.PrimaryKeyID}","!{Featured.PrimaryKeyID}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson}","!{Featured.FeaturingPerson}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.PrimaryKeyID}","!{Featured.FeaturingPerson:Person.PrimaryKeyID}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.Photo}","!{Featured.FeaturingPerson:Person.Photo}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.FormalFullName}","!{Featured.FeaturingPerson:Person.FormalFullName}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.WorkEMailID}","!{Featured.FeaturingPerson:Person.WorkEMailID}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.WorkPhone}","!{Featured.FeaturingPerson:Person.WorkPhone}");
//                            featured.getAttributePaths().getAttributePathsMap().put("!{Featured.FeaturingPerson:Person.MobilePhone}","!{Featured.FeaturingPerson:Person.MobilePhone}");
//                            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
//                            MasterEntityMetadataVOX meta = metadataService.getMetadataByMtPE(peRequest.getMtPE());
//                            DependencyKeysHelper keysHelper3 = new DependencyKeysHelper("Featured.DOPrimaryKey", meta.getFunctionalPrimaryKeyAttribute().getDoAttributeCode());
//                            featured.getDependencyKeyList().add(keysHelper3);
//                            break;
//                        case TAG:
//                            PERequest tags = new PERequest();
//                            tags.setMtPE("Tag");
//                            tags.setSelectType(RequestSelectType.LAYOUT);
//                            peRequest.getChildrenPE().add(tags);
//                            tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PrimaryKeyID}","!{Tag.PrimaryKeyID}");
//                            tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.TagText}","!{Tag.PersonTagMasterID:PersonTagMaster.TagText}");
//                            tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}","!{Tag.PersonTagMasterID:PersonTagMaster.BackgroundColour}");
//                            tags.getAttributePaths().getAttributePathsMap().put("!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}","!{Tag.PersonTagMasterID:PersonTagMaster.FontColour}");
//                            metadataService = TenantAwareCache.getMetaDataRepository();
//                            meta = metadataService.getMetadataByMtPE(peRequest.getMtPE());
//                            keysHelper3 = new DependencyKeysHelper("Tag.DOPrimaryKey", meta.getFunctionalPrimaryKeyAttribute().getDoAttributeCode());
//                            tags.getDependencyKeyList().add(keysHelper3);
//                            break;
//                        case ICON_LABEL_TEXT:
//                        case ICON_TEXT:
//                        case ICON_TEXT_ICON:
//                        case LABEL_TEXT:
//                        case LABEL:
//                        case LABEL_DATE:
//                        case LABEL_DATETIME:
//                        case LABEL_TIME:
//                        case DATETIME_RANGE_ICON:
//                        case IMAGE_CIRCLE_LABEL:
//                        case VERTICAL_CHECK_BOX:
//                            String attributePath = control.getControlAttributePathExpn();
//                            List<String> attributePathExpnList = Util.getPathExpnForHardcodedControlType(attributePath);
//                            for(int i =0; i < attributePathExpnList.size(); i++) {
//                                String expn = attributePathExpnList.get(i);
//                                if(Util.isPropertyControlExpn(expn)){
//                                    SectionControlVO newPC = buildNewPropertyControlFromExpn(expn);
//                                    if(newPC!=null){
//                                        peRequest.getPropertyControlAttributes().add(newPC);
//                                    }
//                                }else{
//                                    if (StringUtil.isDefined(expn) && !expn.equals("") && expn.contains("!{"))
//                                        attributePaths.getAttributePathsMap().put(expn, expn);
//                                    if(control.isControlDispInStickyHdr()){
//                                        addStickyHdrAttribute(control, attributePaths, i, expn);
//                                    }
//                                }
//                            }
//                            break;
//                        case RATING:
//                            break;
//                        case GRID:
//                        case REACTION:
//                            if(StringUtil.isDefined(control.getRelCtrlImgDispAttrPathExpn())|| StringUtil.isDefined(control.getRelCtrlFstDispAttrPathExpn())|| StringUtil.isDefined(control.getRelCtrlSndDispAttrPathExpn())|| StringUtil.isDefined(control.getRelCtrlRtrnDispAttrPathExpn())){
//                                // If either of the above is true add this control to Grid list
//                                peRequest.getGridControlIds().add(control.getControlDefinitionPkId());
//                            }
//                        case TEAM:
//                        case SINGLE_SELECT:
//                        case ATTACHMENT:
//                        case MULTI_SELECT:
//                            String cntrlAttributePath = control.getControlAttributePathExpn();
//                            if(StringUtil.isDefined(cntrlAttributePath))
//                                attributePaths.getAttributePathsMap().put(cntrlAttributePath, cntrlAttributePath);
//                            String addlAttributePath = control.getAddlAttrPathExpn();
//                            /* Split this additional attribute path expn by piped delim and add it to the pe request */
//                            if(StringUtil.isDefined(addlAttributePath)){
//                                String[] addAttributePathList = addlAttributePath.split("\\|");
//                                for(String path : addAttributePathList){
//                                    attributePaths.getAttributePathsMap().put(path, path);
//                                }
//                            }
//                            /* Now split the paths for rtrn attributes */
//                            if(StringUtil.isDefined(control.getRelCtrlRtrnDispAttrPathExpn())){
//                                String[] rtrnAttributes = control.getRelCtrlRtrnDispAttrPathExpn().split("\\|");
//                                List<String> rtrnAttributeList = new ArrayList<>();
//                                rtrnAttributeList.addAll(Arrays.asList(rtrnAttributes));
//                                if(StringUtil.isDefined(control.getStatusColourAttrPathExpn())){
//                                    rtrnAttributeList.add(control.getStatusColourAttrPathExpn());
//                                }
//                                for(String rtrnAttr : rtrnAttributeList){
//                                    List<String> attrList = Util.getAttributePathFromExpression(rtrnAttr);
//                                    String cntrolDOA = cntrlAttributePath.substring(2,cntrlAttributePath.indexOf("}"));
//                                    for(String attr : attrList){
//                                        String finalAttributePath = "!{"+cntrolDOA+":"+attr+"}";
//                                        attributePaths.getAttributePathsMap().put(finalAttributePath, finalAttributePath);
//                                    }
//                                }
//                            }
//                            break;
//                            default:
//                                if(Util.isPropertyControlExpn(control.getControlAttributePathExpn())){
//                                    SectionControlVO newPC = buildNewPropertyControlFromExpn(control.getControlAttributePathExpn());
//                                    if(newPC!=null){
//                                        peRequest.getPropertyControlAttributes().add(newPC);
//                                    }
//                                }else{
//                                    if(StringUtil.isDefined(control.getControlAttributePathExpn()) && !control.getControlAttributePathExpn().equals(""))
//                                        attributePaths.getAttributePathsMap().put(control.getControlAttributePathExpn(),control.getControlAttributePathExpn());
//                                    if(control.isControlDispInStickyHdr()){
//                                        addStickyHdrAttribute(control, attributePaths, control.getOrderBySeqNumber(), control.getControlAttributePathExpn());
//                                    }
//                                }
//                    }
//                    if(control.getDistinct()){
//                        peRequest.setExternalDistinct(true);
//                    }
//                }
//                if(control.getOrderBySeqNumber()!=0){
//                    boolean isASC = false;
//                    if(control.getOrderByMode().equals("ASC")){
//                        isASC = true;
//                    }
//                    SortData newSortData = new SortData(control.getControlAttributePathExpn(), isASC, control.getOrderBySeqNumber(), control.getOrderByValuesTxt());
//                    peRequest.getSortData().add(newSortData);
//                }
//            }
//        }

    public UILayoutService getUiLayoutService() {
        return uiLayoutService;
    }

    public void setUiLayoutService(UILayoutService uiLayoutService) {
        this.uiLayoutService = uiLayoutService;
    }

    public DataService getDataService() {
        return dataService;
    }

    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }

    public void setAccessManagerService(AccessManagerService accessManagerService) {
        this.accessManagerService = accessManagerService;
    }
}
