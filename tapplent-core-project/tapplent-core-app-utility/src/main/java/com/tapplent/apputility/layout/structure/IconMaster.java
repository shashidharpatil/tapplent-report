/**
 * 
 */
package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.tapplent.platformutility.layout.valueObject.IconMasterVO;

/**
 * @author Shubham Patodi
 *
 */
public class IconMaster {
	private String iconCode;
	private String iconClass;
	private String iconUnicode;
	public String getIconCode() {
		return iconCode;
	}
	public void setIconCode(String iconCode) {
		this.iconCode = iconCode;
	}
	public String getIconClass() {
		return iconClass;
	}
	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}
	public String getIconUnicode() {
		return iconUnicode;
	}
	public void setIconUnicode(String iconUnicode) {
		this.iconUnicode = iconUnicode;
	}
	public static IconMaster fromVO(IconMasterVO iconMasterVO){
		IconMaster iconMaster = new IconMaster();
		BeanUtils.copyProperties(iconMasterVO, iconMaster);
		return iconMaster;
	}
}
