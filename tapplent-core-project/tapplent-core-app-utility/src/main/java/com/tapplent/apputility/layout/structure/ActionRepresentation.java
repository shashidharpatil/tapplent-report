package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.ActionRepresentationVO;

public class ActionRepresentation {
	private String actionRepresentationCode;
	@JsonIgnore
	private String actionRepresentationName;
	private String actionRepresentationIconId;
	private String parentActionRepresentationCode; 
	private int minNoMenuItems;
	private String associatedActionGestureCode;
	private String associatedActionGestureAnimationCode;
	private JsonNode actionPlacement;
	private JsonNode actionRepresentationIconPlacement; 
	private JsonNode actionIlIconPlacement;
	private JsonNode actionIlLabelPlacement;
	private JsonNode actionIconOnlyPlacement;
	private JsonNode actionLabelOnlyPlacement;
	
	

	public String getActionRepresentationCode() {
		return actionRepresentationCode;
	}

	public void setActionRepresentationCode(String actionRepresentationCode) {
		this.actionRepresentationCode = actionRepresentationCode;
	}

	public String getActionRepresentationName() {
		return actionRepresentationName;
	}

	public void setActionRepresentationName(String actionRepresentationName) {
		this.actionRepresentationName = actionRepresentationName;
	}

	public String getActionRepresentationIconId() {
		return actionRepresentationIconId;
	}

	public void setActionRepresentationIconId(String actionRepresentationIconId) {
		this.actionRepresentationIconId = actionRepresentationIconId;
	}

	public String getParentActionRepresentationCode() {
		return parentActionRepresentationCode;
	}



	public void setParentActionRepresentationCode(String parentActionRepresentationCode) {
		this.parentActionRepresentationCode = parentActionRepresentationCode;
	}



	public int getMinNoMenuItems() {
		return minNoMenuItems;
	}



	public void setMinNoMenuItems(int minNoMenuItems) {
		this.minNoMenuItems = minNoMenuItems;
	}



	public String getAssociatedActionGestureCode() {
		return associatedActionGestureCode;
	}



	public void setAssociatedActionGestureCode(String associatedActionGestureCode) {
		this.associatedActionGestureCode = associatedActionGestureCode;
	}



	public String getAssociatedActionGestureAnimationCode() {
		return associatedActionGestureAnimationCode;
	}



	public void setAssociatedActionGestureAnimationCode(String associatedActionGestureAnimationCode) {
		this.associatedActionGestureAnimationCode = associatedActionGestureAnimationCode;
	}



	public JsonNode getActionPlacement() {
		return actionPlacement;
	}



	public void setActionPlacement(JsonNode actionPlacement) {
		this.actionPlacement = actionPlacement;
	}



	public JsonNode getActionRepresentationIconPlacement() {
		return actionRepresentationIconPlacement;
	}



	public void setActionRepresentationIconPlacement(JsonNode actionRepresentationIconPlacement) {
		this.actionRepresentationIconPlacement = actionRepresentationIconPlacement;
	}



	public JsonNode getActionIlIconPlacement() {
		return actionIlIconPlacement;
	}



	public void setActionIlIconPlacement(JsonNode actionIlIconPlacement) {
		this.actionIlIconPlacement = actionIlIconPlacement;
	}



	public JsonNode getActionIlLabelPlacement() {
		return actionIlLabelPlacement;
	}



	public void setActionIlLabelPlacement(JsonNode actionIlLabelPlacement) {
		this.actionIlLabelPlacement = actionIlLabelPlacement;
	}



	public JsonNode getActionIconOnlyPlacement() {
		return actionIconOnlyPlacement;
	}



	public void setActionIconOnlyPlacement(JsonNode actionIconOnlyPlacement) {
		this.actionIconOnlyPlacement = actionIconOnlyPlacement;
	}



	public JsonNode getActionLabelOnlyPlacement() {
		return actionLabelOnlyPlacement;
	}



	public void setActionLabelOnlyPlacement(JsonNode actionLabelOnlyPlacement) {
		this.actionLabelOnlyPlacement = actionLabelOnlyPlacement;
	}



	public static ActionRepresentation fromVO(ActionRepresentationVO actionRepresentationVO){
		ActionRepresentation actionRepresentation = new ActionRepresentation();
		BeanUtils.copyProperties(actionRepresentationVO, actionRepresentation);
		return actionRepresentation;
	}
}
