package com.tapplent.apputility.conversation.structure;

import java.util.List;

/**
 * Created by Tapplent on 6/10/17.
 */
public class CreateFdbkGrpResponse {
    private FeedbackGroup feedbackGroup;

    public FeedbackGroup getFeedbackGroup() {
        return feedbackGroup;
    }

    public void setFeedbackGroup(FeedbackGroup feedbackGroup) {
        this.feedbackGroup = feedbackGroup;
    }
}
