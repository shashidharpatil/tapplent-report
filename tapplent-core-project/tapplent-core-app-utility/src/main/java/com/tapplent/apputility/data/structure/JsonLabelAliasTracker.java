package com.tapplent.apputility.data.structure;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by tapplent on 14/12/16.
 */
public class JsonLabelAliasTracker {
    private Map<String, AliasTrackerHelper> doaToJsonAliasMap = new LinkedHashMap<>();
    private int currentDoaCount = 0;

    public Map<String, AliasTrackerHelper> getDoaToJsonAliasMap() {
        return doaToJsonAliasMap;
    }

    public void setDoaToJsonAliasMap(Map<String, AliasTrackerHelper> doaToJsonAliasMap) {
        this.doaToJsonAliasMap = doaToJsonAliasMap;
    }

    public int getCurrentDoaCount() {
        return currentDoaCount;
    }

    public void setCurrentDoaCount(int currentDoaCount) {
        this.currentDoaCount = currentDoaCount;
    }

    public void increaseCurrentCount(){
        currentDoaCount++;
    }
}
