package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.CanvasEventVO;

public class CanvasEvent {
	private String canvasEventId;
	private String canvasEventTransactionFkId;
	private String baseTemplateId;
	private String mtPEId;
	private String canvasId;
	private JsonNode deviceIndependentBlob;
	private JsonNode deviceDependentBlob;
	public String getCanvasEventId() {
		return canvasEventId;
	}
	public void setCanvasEventId(String canvasEventId) {
		this.canvasEventId = canvasEventId;
	}
	public String getCanvasEventTransactionFkId() {
		return canvasEventTransactionFkId;
	}
	public void setCanvasEventTransactionFkId(String canvasEventTransactionFkId) {
		this.canvasEventTransactionFkId = canvasEventTransactionFkId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPEId() {
		return mtPEId;
	}
	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public static CanvasEvent fromVO(CanvasEventVO canvasEventVO){
		CanvasEvent canvasEvent = new CanvasEvent();
		BeanUtils.copyProperties(canvasEventVO, canvasEvent);
		return canvasEvent;
	}
}
