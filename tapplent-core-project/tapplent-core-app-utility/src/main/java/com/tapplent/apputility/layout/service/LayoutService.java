package com.tapplent.apputility.layout.service;

import com.tapplent.apputility.layout.structure.*;
import com.tapplent.platformutility.activityLog.ActivityLog;
import com.tapplent.platformutility.activityLog.ActivityLogRequest;
import com.tapplent.platformutility.layout.valueObject.Tutorial;
import com.tapplent.platformutility.layout.valueObject.VisualArtefactLibraryVO;

import java.sql.SQLException;
import java.util.Map;
import java.util.List;

public interface LayoutService {

	ResponseLayout getLayout(RequestLayout requestLayout);
	/**
	 * Tags
	 * Person Preferences
	 * Menu
	 * App home page : Intro, body, footer
	 * Theme
	 */
	ResponseSysEssentials getSysEssentials(RequestSysEssentials requestSysEssentials);

    void resolveAppHomePageForAllUsers();

	Map<String, JobRelationshipType> getJobRelationshipTypes();

    PersonPreference getPersonPreferences(String personId);

	List<VisualArtefactLibraryVO> getAllTheImages(String libraryType);
	List<Tutorial> getAllTutorials(String personId,String tutorialCatId);
	Map<String, ActivityLog> getActivityLogs(ActivityLogRequest activityLogRequest) throws SQLException;

    RatingLevel getRatingLevel(String ratingScaleId, double anchor);

    NameCalculationResponse calculateName(NameCalculationRequest nameCalculationRequest);

    WorkflowActionResponse getWorkflowActions(WorkflowActionRequest workflowActionRequest) throws SQLException;
}
