package com.tapplent.apputility.etl.service;

import com.tapplent.apputility.data.structure.ResponseData;
import com.tapplent.apputility.layout.structure.BuildDBRequest;

public interface EtlService {
	
	public ResponseData uploadEtlDataByHeader(String headerId);

	public ResponseData uploadEtlDataForAllHeaders();
	
	public void buildLayout(BuildDBRequest request);

	public String getSubjectUser(String subjectUserDrivenDO, String pk);
	
	public void updateHierarchyPersonTable();

	void insertAccessControlDefaultValues();

	public void updateHierarchyTables();

	public void addIndexesToDatabaseTables();

	public void addIndexesToAccessManagerTables();

    void updateRowConditionDefaultValues();

    void updatePersonJobRelationShipSubjectUser();
}
