package com.tapplent.apputility.conversation.controller;

import com.tapplent.apputility.conversation.service.ConversationService;
import com.tapplent.apputility.conversation.structure.*;
import com.tapplent.apputility.data.controller.DataController;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.Exception.TapplentException;
import com.tapplent.platformutility.common.util.Util;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tapplent on 6/9/17.
 */
@Controller
@RequestMapping("tapp/conversation/v1")
public class ConversationController {
    private static final Logger LOG = LogFactory.getLogger(DataController.class);
    private ConversationService conversationService;

    @RequestMapping(value="feedbackGroupList/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    FeedbackGroupResponse getFeedbackGroupResponse(@RequestHeader("access_token") String token,
                                                   @PathVariable("tenantId") String tenantId,
                                                   @PathVariable("personId") String userName,
                                                   @RequestParam("personName") String personName,
                                                   @RequestParam("timeZone") String timeZone,
                                                   @RequestBody FeedbackGroupRequest feedbackGroupRequest){
        try {
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else
                return conversationService.getFeedbackGroupList(feedbackGroupRequest.getmTPECode(), feedbackGroupRequest.getObjectId(), feedbackGroupRequest.getLimit(), feedbackGroupRequest.getOffset(), feedbackGroupRequest.getKeyword());
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="contactDetails/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    FeedbackContactDetails getFeedbackContactDetails(@RequestHeader("access_token") String token,
                                                   @PathVariable("tenantId") String tenantId,
                                                   @PathVariable("personId") String userName,
                                                   @RequestParam("personName") String personName,
                                                   @RequestParam("timeZone") String timeZone,
                                                   @RequestBody FeedbackContactRequest feedbackContactRequest){
        try {
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else{
                int offset = feedbackContactRequest.getLimit()*feedbackContactRequest.getOffset();
                return conversationService.getFeedbackContactDetails(feedbackContactRequest.getmTPECode(), feedbackContactRequest.getObjectId(),
                        feedbackContactRequest.getLimit(), offset, feedbackContactRequest.getKeyword(), feedbackContactRequest.getFeedbackGroupId());
            }
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="createFeedbackGroup/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    CreateFdbkGrpResponse createFeedbackGroupOneToOneResponse(@RequestHeader("access_token") String token,
                                                              @PathVariable("tenantId") String tenantId,
                                                              @PathVariable("personId") String userName,
                                                              @RequestParam("personName") String personName,
                                                              @RequestParam("timeZone") String timeZone,
                                                              @RequestBody CreateFdbkGrpRequest feedbackContactRequest){
        try{
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else {
                CreateFdbkGrpResponse createFdbkGrpResponse = null;
                if (feedbackContactRequest.getFeedbackGroupPkId() != null) {
                    createFdbkGrpResponse = conversationService.updateFeedbackGroup(feedbackContactRequest);
                } else {
                    createFdbkGrpResponse = conversationService.createFeedbackGroup(feedbackContactRequest);
                }
                return createFdbkGrpResponse;
            }
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="writeFeedbackMessage/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    WriteFeedbackMessageResponse writeFeedbackMessage(@RequestHeader("access_token") String token,
                                                     @PathVariable("tenantId") String tenantId,
                                                     @PathVariable("personId") String userName,
                                                     @RequestParam("personName") String personName,
                                                     @RequestParam("timeZone") String timeZone,
                                                     @RequestBody WriteFeedbackMessageRequest writeFeedbackMessageRequest){
        try {
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else
                return conversationService.writeFeedbackMessage(writeFeedbackMessageRequest);
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="feedbackMessage/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    FeedbackMessageResponse getFeedbackMessage(@RequestHeader("access_token") String token,
                                                      @PathVariable("tenantId") String tenantId,
                                                      @PathVariable("personId") String userName,
                                                      @RequestParam("personName") String personName,
                                                      @RequestParam("timeZone") String timeZone,
                                                      @RequestBody FeedbackMessageRequest feedbackMessageRequest){
        try {
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else {
                if (feedbackMessageRequest.getLimit() == 0)
                    feedbackMessageRequest.setLimit(20);
                List<FeedbackMessage> feedbackMessageList = conversationService.getFeedbackMessage(feedbackMessageRequest.getFeedbackGroupId(), feedbackMessageRequest.getLimit(), feedbackMessageRequest.getOffset());
                FeedbackMessageResponse feedbackMessageResponse = new FeedbackMessageResponse();
                feedbackMessageResponse.setFeedbackMessageList(feedbackMessageList);
                return feedbackMessageResponse;
            }
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="feedbackGroupMemberDetails/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    FeedbackGroupMemberResponse getFeedbackGroupMemeberDetails(@RequestHeader("access_token") String token,
                                               @PathVariable("tenantId") String tenantId,
                                               @PathVariable("personId") String userName,
                                               @RequestParam("personName") String personName,
                                               @RequestParam("timeZone") String timeZone,
                                               @RequestBody FeedbackGroupMemberRequest feedbackGroupMemberRequest){
        try {
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else {
                List<GroupMember> feedbackGroupMemberDetails = conversationService.getFeedbackGroupMemberDetails(feedbackGroupMemberRequest.getFeedbackGroupId());
                FeedbackGroupMemberResponse feedbackGroupMemberResponse = new FeedbackGroupMemberResponse();
                feedbackGroupMemberResponse.setGroupMembers(feedbackGroupMemberDetails);
                return feedbackGroupMemberResponse;
            }
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="updateGroupMemberDetails/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    FeedbackGroupMemberUpdateResponse updateFeedbackGroupMemberDetails(@RequestHeader("access_token") String token,
                                                               @PathVariable("tenantId") String tenantId,
                                                               @PathVariable("personId") String userName,
                                                               @RequestParam("personName") String personName,
                                                               @RequestParam("timeZone") String timeZone,
                                                               @RequestBody FeedbackGroupMemberUpdateRequest feedbackGroupMemberUpdateRequest){
        try {
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else {
                List<GroupMember> feedbackGroupMemberDetails = conversationService.updateFeedbackGroupMemberDetails(feedbackGroupMemberUpdateRequest);
                FeedbackGroupMemberUpdateResponse feedbackGroupMemberUpdateResponse = new FeedbackGroupMemberUpdateResponse();
                feedbackGroupMemberUpdateResponse.setGroupMember(feedbackGroupMemberDetails);
                return feedbackGroupMemberUpdateResponse;
            }
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="addGroupMemberDetails/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    FeedbackGroupMemberAddResponse addFeedbackGroupMemberDetails(@RequestHeader("access_token") String token,
                                                                       @PathVariable("tenantId") String tenantId,
                                                                       @PathVariable("personId") String userName,
                                                                       @RequestParam("personName") String personName,
                                                                       @RequestParam("timeZone") String timeZone,
                                                                       @RequestBody FeedbackGroupMemberAddRequest feedbackGroupMemberAddRequest){
        try {
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else {
                List<GroupMember> feedbackGroupMemberDetails = conversationService.addFeedbackGroupMemberDetails(feedbackGroupMemberAddRequest);
                FeedbackGroupMemberAddResponse feedbackGroupMemberAddResponse = new FeedbackGroupMemberAddResponse();
                feedbackGroupMemberAddResponse.setGroupMembers(feedbackGroupMemberDetails);
                return feedbackGroupMemberAddResponse;
            }
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="deleteGroup/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    DeleteGroupResponse deleteGroup(@RequestHeader("access_token") String token,
                                    @PathVariable("tenantId") String tenantId,
                                    @PathVariable("personId") String userName,
                                    @RequestParam("personName") String personName,
                                    @RequestParam("timeZone") String timeZone,
                                    @RequestBody DeleteGroupRequest deleteGroupRequest){
        try {
//            Map<String, Object> map = new HashMap<>();
//            Map<String, Object> innerMap1 = new HashMap<>();
//            innerMap1.put("en_US","example4");
//            G11n g11nValue = new G11n();
//            g11nValue.setVL("This is an example1");
//            g11nValue.setIM("I");
//            g11nValue.setTSL("NULL");
//            G11n g11nValue1 = new G11n();
//            g11nValue1.setVL("This is an example2");
//            g11nValue1.setIM("I");
//            g11nValue1.setTSL(null);
//            G11n g11nValue2 = new G11n();
//            g11nValue2.setVL("This is an example3");
//            g11nValue2.setIM("I");

//            innerMap1.put("VL", "This is an example");
//            innerMap1.put("IM", "I");
//            innerMap1.put("TSL", "null");
//            Map<String, String> innerMap2 = new HashMap<>();
//            innerMap2.put("VL", "This is an example");
//            innerMap2.put("IM", "I");
//            innerMap2.put("TSL", null);
//            Map<String, String> innerMap3 = new HashMap<>();
//            innerMap3.put("VL", "This is an example");
//            innerMap3.put("IM", "I");
//            map.put("en_US", g11nValue);
//            map.put("hu_HU", g11nValue1);
//            map.put("en_IN", g11nValue2);
//            String g11nString = Util.getStringFromG11nMap(map);
//            String g11nStringIntrmdte = Util.getStringFromG11nMapIntrmdte(innerMap1);
//            Map<String, Object> objectMap = Util.getG11nMapFromStringIntrmdte(g11nStringIntrmdte);
//            String g11nStringValue = Util.getStringFromG11nMaps(map);
//            Map<String, Object> g11nMap = Util.getG11nMapFromStringNew(g11nStringValue);
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else {
                DeleteGroupResponse deleteGroupResponse = new DeleteGroupResponse();
                List<String> deletedIds = new ArrayList<>();
                for (String feedbackGroupId : deleteGroupRequest.getFeedbackGroupIds()) {
                    conversationService.deleteGroup(feedbackGroupId);
                    deletedIds.add(feedbackGroupId);
                }
                deleteGroupResponse.setFeedbackGroupIds(deletedIds);
                return deleteGroupResponse;
            }
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="messageReadStatus/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    FeedbackMessageReadStatusResponse getFeedbackGroupMemberReadStatus(@RequestHeader("access_token") String token,
                                                                    @PathVariable("tenantId") String tenantId,
                                                                    @PathVariable("personId") String userName,
                                                                    @RequestParam("personName") String personName,
                                                                    @RequestParam("timeZone") String timeZone,
                                                                    @RequestBody FeedbackMessageReadStatusRequest feedbackMessageReadStatusRequest){
        try {
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else {
                List<GroupMember> groupMembers = conversationService.getFeedbackGroupMemberReadStatus(feedbackMessageReadStatusRequest);
                FeedbackMessageReadStatusResponse feedbackMessageReadStatusResponse = new FeedbackMessageReadStatusResponse();
                feedbackMessageReadStatusResponse.setGroupMembers(groupMembers);
                return feedbackMessageReadStatusResponse;
            }
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping(value="bookmarkedFeedbackMessage/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    FeedbackMessageResponse getBookemarkedFeedbackMessage(@RequestHeader("access_token") String token,
                                               @PathVariable("tenantId") String tenantId,
                                               @PathVariable("personId") String userName,
                                               @RequestParam("personName") String personName,
                                               @RequestParam("timeZone") String timeZone,
                                               @RequestBody BookmarkedFeedbackMessageRequest bookmarkedFeedbackMessageRequest){
        try {
            if (!Util.validateAccessTokenAndSetUserContext(token))
                throw new TapplentException("Token Not Valid!");
            else {
                if (bookmarkedFeedbackMessageRequest.getLimit() == 0)
                    bookmarkedFeedbackMessageRequest.setLimit(20);
                List<FeedbackMessage> feedbackMessageList = conversationService.getBookmarkedFeedbackMessage(bookmarkedFeedbackMessageRequest.getFeedbackGroupId(), bookmarkedFeedbackMessageRequest.getLimit(), bookmarkedFeedbackMessageRequest.getOffset());
                FeedbackMessageResponse feedbackMessageResponse = new FeedbackMessageResponse();
                feedbackMessageResponse.setFeedbackMessageList(feedbackMessageList);
                return feedbackMessageResponse;
            }
        } catch (TapplentException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ConversationService getConversationService() {
        return conversationService;
    }

    public void setConversationService(ConversationService conversationService) {
        this.conversationService = conversationService;
    }
}
