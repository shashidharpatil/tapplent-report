/**
 * 
 */
package com.tapplent.apputility.data.structure;

/**
 * @author Shubham Patodi
 *
 */
public class DataActionsResponse {
	private String actionCode;
	private boolean isApplicable;
	public DataActionsResponse(String actionCode, boolean isApplicable){
		this.actionCode = actionCode;
		this.isApplicable = isApplicable;
	}
	public String getActionCode() {
		return actionCode;
	}
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public boolean isApplicable() {
		return isApplicable;
	}
	public void setApplicable(boolean isApplicable) {
		this.isApplicable = isApplicable;
	}
}
