package com.tapplent.apputility.etl.structure;

public class EtlTransactionSummary {
//	 VERSION ID                           
//	 ETL TRANSACTION SUMMARY PK ID        
//	 ETL HEADER DEP FK ID                 
//	 ETL SOURCE FILE NAME TXT             
//	 TARGET BT DO FK ID                   
//	 SOURCE COUNT POS INT                 
//	 SUCCESS COUNT POS INT 
	private String versionId;
	private String etlTransactionSummaryPkId ;
	private String etlHeaderDepFkId; 
	private String etlSourceFileNameTxt; 
	private String targetBtDoFkId; 
	private String sourceCountPosInt; 
	private String successCountPosInt;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getEtlTransactionSummaryPkId() {
		return etlTransactionSummaryPkId;
	}
	public void setEtlTransactionSummaryPkId(String etlTransactionSummaryPkId) {
		this.etlTransactionSummaryPkId = etlTransactionSummaryPkId;
	}
	public String getEtlHeaderDepFkId() {
		return etlHeaderDepFkId;
	}
	public void setEtlHeaderDepFkId(String etlHeaderDepFkId) {
		this.etlHeaderDepFkId = etlHeaderDepFkId;
	}
	public String getEtlSourceFileNameTxt() {
		return etlSourceFileNameTxt;
	}
	public void setEtlSourceFileNameTxt(String etlSourceFileNameTxt) {
		this.etlSourceFileNameTxt = etlSourceFileNameTxt;
	}
	public String getTargetBtDoFkId() {
		return targetBtDoFkId;
	}
	public void setTargetBtDoFkId(String targetBtDoFkId) {
		this.targetBtDoFkId = targetBtDoFkId;
	}
	public String getSourceCountPosInt() {
		return sourceCountPosInt;
	}
	public void setSourceCountPosInt(String sourceCountPosInt) {
		this.sourceCountPosInt = sourceCountPosInt;
	}
	public String getSuccessCountPosInt() {
		return successCountPosInt;
	}
	public void setSuccessCountPosInt(String successCountPosInt) {
		this.successCountPosInt = successCountPosInt;
	}
}
