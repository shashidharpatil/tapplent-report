package com.tapplent.apputility.entity.provider;

import java.sql.Timestamp;

public class User {
    private String versionId;
    private String username;
    private String password;
    private String personId;
    private Timestamp effectiveDatetime;
    private boolean isLocked;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public Timestamp getEffectiveDatetime() {
        return effectiveDatetime;
    }

    public void setEffectiveDatetime(Timestamp effectiveDatetime) {
        this.effectiveDatetime = effectiveDatetime;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }
}
