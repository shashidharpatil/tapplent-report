package com.tapplent.apputility.analytics.structure;

import com.tapplent.platformutility.analytics.structure.AnalyticsAxisAndMetricMapVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 23/08/17.
 */
public class AnalyticsAxisAndMetricMap {
    private String pkID;
    private String metricCategory;
    private String metricCategoryMenu;
    private String metricAxisID;
    private String metricAxisName;
    private String parentAnalyticsMetricAxisMap;
    private String parentAnalyticsMetricAxisID;

    public String getPkID() {
        return pkID;
    }

    public void setPkID(String pkID) {
        this.pkID = pkID;
    }

    public String getMetricCategory() {
        return metricCategory;
    }

    public void setMetricCategory(String metricCategory) {
        this.metricCategory = metricCategory;
    }

    public String getMetricAxisID() {
        return metricAxisID;
    }

    public void setMetricAxisID(String metricAxisID) {
        this.metricAxisID = metricAxisID;
    }

    public String getMetricAxisName() {
        return metricAxisName;
    }

    public void setMetricAxisName(String metricAxisName) {
        this.metricAxisName = metricAxisName;
    }

    public String getParentAnalyticsMetricAxisMap() {
        return parentAnalyticsMetricAxisMap;
    }

    public void setParentAnalyticsMetricAxisMap(String parentAnalyticsMetricAxisMap) {
        this.parentAnalyticsMetricAxisMap = parentAnalyticsMetricAxisMap;
    }

    public String getMetricCategoryMenu() {
        return metricCategoryMenu;
    }

    public void setMetricCategoryMenu(String metricCategoryMenu) {
        this.metricCategoryMenu = metricCategoryMenu;
    }

    public String getParentAnalyticsMetricAxisID() {
        return parentAnalyticsMetricAxisID;
    }

    public void setParentAnalyticsMetricAxisID(String parentAnalyticsMetricAxisID) {
        this.parentAnalyticsMetricAxisID = parentAnalyticsMetricAxisID;
    }

    public static AnalyticsAxisAndMetricMap fromVO(AnalyticsAxisAndMetricMapVO analyticsAxisAndMetricMapVO) {
        AnalyticsAxisAndMetricMap analyticsAxisAndMetricMap = new AnalyticsAxisAndMetricMap();
        BeanUtils.copyProperties(analyticsAxisAndMetricMapVO, analyticsAxisAndMetricMap);
        return analyticsAxisAndMetricMap;
    }
}
