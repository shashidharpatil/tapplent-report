package com.tapplent.apputility.uilayout.structure;

import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.uilayout.valueobject.OrgChartFilterLabelVO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 06/07/17.
 */
public class OrgChartFilterLabel {
    private String orgChartFilterPKID;
    private String filterSectionID;
    private String labelName;
    private IconVO labelIcon;
    private String controlType;
    private IconVO controlTypeOnStateIcon;
    private IconVO controlTypeOffStateIcon;
    private List<OrgChartFilterRange> orgChartFilterRangeList = new ArrayList<>();

    public String getOrgChartFilterPKID() {
        return orgChartFilterPKID;
    }

    public void setOrgChartFilterPKID(String orgChartFilterPKID) {
        this.orgChartFilterPKID = orgChartFilterPKID;
    }

    public String getFilterSectionID() {
        return filterSectionID;
    }

    public void setFilterSectionID(String filterSectionID) {
        this.filterSectionID = filterSectionID;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public IconVO getLabelIcon() {
        return labelIcon;
    }

    public void setLabelIcon(IconVO labelIcon) {
        this.labelIcon = labelIcon;
    }

    public String getControlType() {
        return controlType;
    }

    public void setControlType(String controlType) {
        this.controlType = controlType;
    }

    public List<OrgChartFilterRange> getOrgChartFilterRangeList() {
        return orgChartFilterRangeList;
    }

    public void setOrgChartFilterRangeList(List<OrgChartFilterRange> orgChartFilterRangeList) {
        this.orgChartFilterRangeList = orgChartFilterRangeList;
    }

    public IconVO getControlTypeOnStateIcon() {
        return controlTypeOnStateIcon;
    }

    public void setControlTypeOnStateIcon(IconVO controlTypeOnStateIcon) {
        this.controlTypeOnStateIcon = controlTypeOnStateIcon;
    }

    public IconVO getControlTypeOffStateIcon() {
        return controlTypeOffStateIcon;
    }

    public void setControlTypeOffStateIcon(IconVO controlTypeOffStateIcon) {
        this.controlTypeOffStateIcon = controlTypeOffStateIcon;
    }

    public static OrgChartFilterLabel fromVO(OrgChartFilterLabelVO orgChartFilterLabelVO){
        OrgChartFilterLabel orgChartFilterLabel = new OrgChartFilterLabel();
        BeanUtils.copyProperties(orgChartFilterLabelVO, orgChartFilterLabel);
        return orgChartFilterLabel;
    }
}
