package com.tapplent.apputility.entity.provider;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Deque;
import java.util.List;
import java.util.Map;

import com.tapplent.apputility.entity.structure.ExitRuleCriteria;
import com.tapplent.apputility.entity.structure.PersonExitRule;
import com.tapplent.apputility.uilayout.structure.CostCentreBreakUpData;
import com.tapplent.apputility.uilayout.structure.JobFamilyBreakUpData;
import com.tapplent.apputility.uilayout.structure.LocationBreakUpData;
import com.tapplent.apputility.uilayout.structure.OrganizationBreakUpData;
import com.tapplent.platformutility.activityLog.ActivityLog;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.sql.context.model.InsertContextModel;

public interface InsertService {
	int insertRow(String insertQuery) throws SQLException;

	void insertG11Values(Map<String, Object> labels, String g11nTableName) throws Exception;

	void CalculateAndUpdateRecordState(EntityMetadataVOX entityMetadataVOX, InsertContextModel model, GlobalVariables globalVariables) throws SQLException;

    void insertActivityLog(ActivityLog activityLog, EntityMetadataVOX activityMetadataVOX, EntityMetadataVOX activityDtlMetadataVOX, boolean isCreateDetails) throws SQLException;

	void insertUpdateRow(String sql, Deque<Object> parameters) throws SQLException;

    void createUser(GlobalVariables globalVariables) throws SQLException;

	String generateEmployeeCode(String employeeCodeGenKey, Map<String, Object> data) throws SQLException;

    void deleteGenericRecord(String tableName, Map<String, Object> deleteParameters) throws SQLException;

	void updateBreakUpDataForOrg(List<OrganizationBreakUpData> organizationBreakUpData, GlobalVariables globalVariables) throws SQLException;

	void updateBreakUpDataForLoc(List<LocationBreakUpData> locationBreakUpData, GlobalVariables globalVariables) throws SQLException;

	void updateBreakUpDataForCC(List<CostCentreBreakUpData> cCBreakUpData, GlobalVariables globalVariables) throws SQLException;

	void updateBreakUpDataForJobFamily(List<JobFamilyBreakUpData> jobFamilyBreakUpData, GlobalVariables globalVariables) throws SQLException;

    void inactivateUser(GlobalVariables globalVariables) throws SQLException, ParseException;

    Map<String, ExitRuleCriteria> getExitRuleCriteria() throws SQLException;

	Map<String,List<PersonExitRule>> getCriteriaToExitRuleMap() throws SQLException;

    List<Map<String,Object>> getAllRecordsByRelationshipType(String relationshipType, String personId) throws SQLException;
}
