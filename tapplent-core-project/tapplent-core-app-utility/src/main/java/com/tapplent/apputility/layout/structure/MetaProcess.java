package com.tapplent.apputility.layout.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadata;

public class MetaProcess {
	private String metaProcessCode;
	private List<BaseTemplate> baseTemplateList;
	private List<EntityMetadata> doMetaList;
	private List<EntityAttributeMetadata> attrMetaList;
	@JsonIgnore
	private Map<String, EntityAttributeMetadata> doaMetaMap;
	@JsonIgnore
	private Map<String, EntityMetadata> doMetaMap;
	public MetaProcess(){
		this.attrMetaList = new ArrayList<>();
		this.doMetaList = new ArrayList<>();
		this.doaMetaMap = new HashMap<>();
		this.doMetaMap = new HashMap<>();
	}
	public List<EntityAttributeMetadata> getAttrMetaList() {
		return attrMetaList;
	}
	public void setAttrMetaList(List<EntityAttributeMetadata> attrMetaList) {
		this.attrMetaList = attrMetaList;
	}
	public List<EntityMetadata> getDoMetaList() {
		return doMetaList;
	}
	public void setDoMetaList(List<EntityMetadata> doMetaList) {
		this.doMetaList = doMetaList;
	}
	public String getMetaProcessCode() {
		return metaProcessCode;
	}
	public void setMetaProcessCode(String metaProcessCode) {
		this.metaProcessCode = metaProcessCode;
	}
	public List<BaseTemplate> getBaseTemplateList() {
		return baseTemplateList;
	}
	public void setBaseTemplateList(List<BaseTemplate> baseTemplateList) {
		this.baseTemplateList = baseTemplateList;
	}
	public Map<String, EntityAttributeMetadata> getDoaMetaMap() {
		return doaMetaMap;
	}
	public void setDoaMetaMap(Map<String, EntityAttributeMetadata> doaMetaMap) {
		this.doaMetaMap = doaMetaMap;
	}
	public Map<String, EntityMetadata> getDoMetaMap() {
		return doMetaMap;
	}
	public void setDoMetaMap(Map<String, EntityMetadata> doMetaMap) {
		this.doMetaMap = doMetaMap;
	}
}