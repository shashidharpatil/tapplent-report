package com.tapplent.apputility.uilayout.structure;

import com.tapplent.platformutility.uilayout.valueobject.ControlActionTargetSectionSortParamsVO;
import com.tapplent.platformutility.uilayout.valueobject.SectionActionTargetSectionSortParamsVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 25/04/17.
 */
public class TargetSectionSortParams {
    /*
    UISectionActionSectionMapperID	MetaProcessElementAlias	DefaultOnLoadSort
    */
    private String sortParamsPkId;
    private String mtPEAlias;
    private String defaultOnLoadSort;

    public String getSortParamsPkId() {
        return sortParamsPkId;
    }

    public void setSortParamsPkId(String sortParamsPkId) {
        this.sortParamsPkId = sortParamsPkId;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getDefaultOnLoadSort() {
        return defaultOnLoadSort;
    }

    public void setDefaultOnLoadSort(String defaultOnLoadSort) {
        this.defaultOnLoadSort = defaultOnLoadSort;
    }

    public static TargetSectionSortParams fromVO(ControlActionTargetSectionSortParamsVO sortParamsVO) {
        TargetSectionSortParams targetSectionSortParams = new TargetSectionSortParams();
        BeanUtils.copyProperties(sortParamsVO, targetSectionSortParams);
        return targetSectionSortParams;
    }

    public static TargetSectionSortParams fromVO(SectionActionTargetSectionSortParamsVO sortParamsVO) {
        TargetSectionSortParams targetSectionSortParams = new TargetSectionSortParams();
        BeanUtils.copyProperties(sortParamsVO, targetSectionSortParams);
        return targetSectionSortParams;
    }
}
