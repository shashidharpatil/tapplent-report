package com.tapplent.apputility.data.structure;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 29/11/16.
 */
public class DependencyMappedParentData {
    private Map<String, Object> dependencyResultMap = new HashMap<>();
    private List<String[]> dependencyResultArray = new ArrayList<>();

    public Map<String, Object> getDependencyResultMap() {
        return dependencyResultMap;
    }

    public void setDependencyResultMap(Map<String, Object> dependencyResultMap) {
        this.dependencyResultMap = dependencyResultMap;
    }

    public List<String[]> getDependencyResultArray() {
        return dependencyResultArray;
    }

    public void setDependencyResultArray(List<String[]> dependencyResultArray) {
        this.dependencyResultArray = dependencyResultArray;
    }

}
