package com.tapplent.apputility.layout.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.BaseTemplateVO;

public class BaseTemplate {
	@JsonIgnore
	private String versionId;
	private String baseTemplateId;
	@JsonIgnore
	private String baseTemplateName;
	@JsonIgnore
	private String baseTemplateNameG11nText;
	private String metaProcessCode;
	private String baseTemplateIcon;
//	private String baseTemplateThemeTemplateCode;
	private boolean	isArchived;
	private boolean isBtStandard;
	@JsonIgnore
	private boolean showNonPermissionedActions;
	private JsonNode btPropertyBlob;
	private List<PageEquivalence> pageEquivalenceList;
	private List<PageEquivalencePlacement> pageEquivalencePlacements;
	private List<ActionRepresetationLaunchTrxn> arLaunchList;
//	private List<PageRelation> pageRelationList;
//	private List<Page> pageList; 
	private List<Canvas> canvasList;
	@JsonIgnore
	private Map<String, Canvas> canvasMap;
	private List<ActionStepProperty> actionStepPropertyList;
	public BaseTemplate(){
		this.actionStepPropertyList = new ArrayList<>();
	}
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getBaseTemplateName() {
		return baseTemplateName;
	}

	public void setBaseTemplateName(String baseTemplateName) {
		this.baseTemplateName = baseTemplateName;
	}

	public String getBaseTemplateNameG11nText() {
		return baseTemplateNameG11nText;
	}
	public void setBaseTemplateNameG11nText(String baseTemplateNameG11nText) {
		this.baseTemplateNameG11nText = baseTemplateNameG11nText;
	}
	public String getMetaProcessCode() {
		return metaProcessCode;
	}
	public void setMetaProcessCode(String metaProcessCode) {
		this.metaProcessCode = metaProcessCode;
	}
	public String getBaseTemplateIcon() {
		return baseTemplateIcon;
	}
	public void setBaseTemplateIcon(String baseTemplateIcon) {
		this.baseTemplateIcon = baseTemplateIcon;
	}
	public boolean isArchived() {
		return isArchived;
	}
	public void setArchived(boolean isArchived) {
		this.isArchived = isArchived;
	}
	public boolean isBtStandard() {
		return isBtStandard;
	}
	public void setBtStandard(boolean isBtStandard) {
		this.isBtStandard = isBtStandard;
	}
	public boolean isShowNonPermissionedActions() {
		return showNonPermissionedActions;
	}
	public void setShowNonPermissionedActions(boolean showNonPermissionedActions) {
		this.showNonPermissionedActions = showNonPermissionedActions;
	}
//	public JsonNode getBtPropertyBlob() {
//		return btPropertyBlob;
//	}
//	public void setBtPropertyBlob(JsonNode btPropertyBlob) {
//		this.btPropertyBlob = btPropertyBlob;
//	}
	public List<Canvas> getCanvasList() {
		return canvasList;
	}
	public void setCanvasList(List<Canvas> canvasList) {
		this.canvasList = canvasList;
	}
	public Map<String, Canvas> getCanvasMap() {
		return canvasMap;
	}
	public void setCanvasMap(Map<String, Canvas> canvasMap) {
		this.canvasMap = canvasMap;
	}
	public List<ActionStepProperty> getActionStepPropertyList() {
		return actionStepPropertyList;
	}
	public void setActionStepPropertyList(List<ActionStepProperty> actionStepPropertyList) {
		this.actionStepPropertyList = actionStepPropertyList;
	}
	public List<PageEquivalence> getPageEquivalenceList() {
		return pageEquivalenceList;
	}
	public void setPageEquivalenceList(List<PageEquivalence> pageEquivalenceList) {
		this.pageEquivalenceList = pageEquivalenceList;
	}
	public List<PageEquivalencePlacement> getPageEquivalencePlacements() {
		return pageEquivalencePlacements;
	}
	public void setPageEquivalencePlacements(List<PageEquivalencePlacement> pageEquivalencePlacements) {
		this.pageEquivalencePlacements = pageEquivalencePlacements;
	}
	public List<ActionRepresetationLaunchTrxn> getArLaunchList() {
		return arLaunchList;
	}
	public void setArLaunchList(List<ActionRepresetationLaunchTrxn> arLaunchList) {
		this.arLaunchList = arLaunchList;
	}
	public JsonNode getBtPropertyBlob() {
		return btPropertyBlob;
	}
	public void setBtPropertyBlob(JsonNode btPropertyBlob) {
		this.btPropertyBlob = btPropertyBlob;
	}
	public static BaseTemplate fromVO(BaseTemplateVO btVO) {
		BaseTemplate bt = new BaseTemplate();
		BeanUtils.copyProperties(btVO, bt);
		//Also write the logic to convert the G11n values here.
		return bt;
	}
}
