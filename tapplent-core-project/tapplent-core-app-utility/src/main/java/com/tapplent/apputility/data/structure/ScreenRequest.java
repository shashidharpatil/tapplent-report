package com.tapplent.apputility.data.structure;

import com.tapplent.platformutility.common.util.RequestSelectType;
import com.tapplent.platformutility.uilayout.valueobject.LoadParameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 07/11/16.
 */
public class ScreenRequest {
    private String baseTemplate;
    private String controlActionInstanceId;
    private String sectionActionInstanceId;
    private String actionTargetPkId;
    private Map<String, LoadParameters> peAliasToLoadParamsMap = new HashMap<>();
    private List<RelationControlOnDemandAttributes> onDemandAttributes = new ArrayList<>();
    private String subjectUserID;
    private Boolean isLayoutRequired;
    private Boolean isDataRequired = true;
    private Boolean isMetaRequired = true;
    private String targetScreenInstance;
    private String targetSectionInstance;
    private String mtPE;
    private String uiDispTypeCodeFkId;
    private String tabSeqNumPosInt;
    private String deviceType;
    private String sourceScreenInstance;
    private String sourceSection;
    private String actionCode;
    private String processTypeCode;
    private List<String> mtPEList;
    private String keyWord;
    private RequestSelectType selectType;

    public String getTargetScreenInstance() {
        return targetScreenInstance;
    }

    public String getBaseTemplate() {
        return baseTemplate;
    }

    public void setBaseTemplate(String baseTemplate) {
        this.baseTemplate = baseTemplate;
    }

    public void setTargetScreenInstance(String targetScreenInstance) {
        this.targetScreenInstance = targetScreenInstance;
    }

    public String getTargetSectionInstance() {
        return targetSectionInstance;
    }

    public void setTargetSectionInstance(String targetSectionInstance) {
        this.targetSectionInstance = targetSectionInstance;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getSourceScreenInstance() {
        return sourceScreenInstance;
    }

    public void setSourceScreenInstance(String sourceScreenInstance) {
        this.sourceScreenInstance = sourceScreenInstance;
    }

    public String getSourceSection() {
        return sourceSection;
    }

    public void setSourceSection(String sourceSection) {
        this.sourceSection = sourceSection;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getControlActionInstanceId() {
        return controlActionInstanceId;
    }

    public void setControlActionInstanceId(String controlActionInstanceId) {
        this.controlActionInstanceId = controlActionInstanceId;
    }

    public String getSectionActionInstanceId() {
        return sectionActionInstanceId;
    }

    public void setSectionActionInstanceId(String sectionActionInstanceId) {
        this.sectionActionInstanceId = sectionActionInstanceId;
    }

    public Boolean getLayoutRequired() {
        return isLayoutRequired;
    }

    public void setLayoutRequired(Boolean layoutRequired) {
        isLayoutRequired = layoutRequired;
    }

    public Boolean getMetaRequired() {
        return isMetaRequired;
    }

    public void setMetaRequired(Boolean metaRequired) {
        isMetaRequired = metaRequired;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public RequestSelectType getSelectType() {
        return selectType;
    }

    public void setSelectType(RequestSelectType selectType) {
        this.selectType = selectType;
    }

    public Map<String, LoadParameters> getPeAliasToLoadParamsMap() {
        return peAliasToLoadParamsMap;
    }

    public void setPeAliasToLoadParamsMap(Map<String, LoadParameters> peAliasToLoadParamsMap) {
        this.peAliasToLoadParamsMap = peAliasToLoadParamsMap;
    }

    public Boolean getDataRequired() {
        return isDataRequired;
    }

    public void setDataRequired(Boolean dataRequired) {
        isDataRequired = dataRequired;
    }

    public List<String> getMtPEList() {
        return mtPEList;
    }

    public void setMtPEList(List<String> mtPEList) {
        this.mtPEList = mtPEList;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getUiDispTypeCodeFkId() {
        return uiDispTypeCodeFkId;
    }

    public void setUiDispTypeCodeFkId(String uiDispTypeCodeFkId) {
        this.uiDispTypeCodeFkId = uiDispTypeCodeFkId;
    }

    public String getTabSeqNumPosInt() {
        return tabSeqNumPosInt;
    }

    public void setTabSeqNumPosInt(String tabSeqNumPosInt) {
        this.tabSeqNumPosInt = tabSeqNumPosInt;
    }

    public String getProcessTypeCode() {
        return processTypeCode;
    }

    public void setProcessTypeCode(String processTypeCode) {
        this.processTypeCode = processTypeCode;
    }

    public String getActionTargetPkId() {
        return actionTargetPkId;
    }

    public void setActionTargetPkId(String actionTargetPkId) {
        this.actionTargetPkId = actionTargetPkId;
    }

    public List<RelationControlOnDemandAttributes> getOnDemandAttributes() {
        return onDemandAttributes;
    }

    public void setOnDemandAttributes(List<RelationControlOnDemandAttributes> onDemandAttributes) {
        this.onDemandAttributes = onDemandAttributes;
    }

    public String getSubjectUserID() {
        return subjectUserID;
    }

    public void setSubjectUserID(String subjectUserID) {
        this.subjectUserID = subjectUserID;
    }
}
