package com.tapplent.apputility.uilayout.service;

import com.amazonaws.services.s3.AmazonS3;
import com.tapplent.apputility.data.structure.*;
import com.tapplent.apputility.layout.structure.LayoutPermissions;
import com.tapplent.apputility.uilayout.structure.LocationBreakUpData;
import com.tapplent.apputility.uilayout.structure.*;
import com.tapplent.apputility.utility.structure.AppUtil;
import com.tapplent.platformutility.accessmanager.structure.ResolvedColumnCondition;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.AttachmentObject;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.conversation.valueobject.PersonDetailsVO;
import com.tapplent.platformutility.layout.valueObject.JobRelationshipTypeVO;
import com.tapplent.platformutility.layout.valueObject.LayoutPermissionsVO;
import com.tapplent.platformutility.layout.valueObject.StateVO;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.uilayout.dao.UILayoutDAO;
import com.tapplent.platformutility.uilayout.valueobject.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by sripad on 01/11/16.
 */
public class UILayoutServiceImpl implements UILayoutService {

    private UILayoutDAO uiLayoutDAO;

    public void setUiLayoutDAO(UILayoutDAO uiLayoutDAO) {
        this.uiLayoutDAO = uiLayoutDAO;
    }

    @Override
    public ScreenInstance getLayout(List<String> targetSectionInstanceList, String deviceType, String subjectUserID) {
        String targetScreenInstanceId = getScreenInstanceFromSection(targetSectionInstanceList.get(0));
        return getLayout(targetScreenInstanceId, targetSectionInstanceList, deviceType, subjectUserID);
    }

    @Transactional
    @Override
    public ScreenInstance getLayout(String targetScreenInstance, List<String> targetSectionInstanceList, String deviceType, String subjectUserID) {
        ScreenInstanceVO screenInstanceVO = uiLayoutDAO.getScreenInstance(targetScreenInstance, deviceType);
        ScreenInstance screenInstance = ScreenInstance.fromVO(screenInstanceVO);
//        if(screenInstance.getThemeSeqNumPosInt() == 0) {
//            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
//            int themeSeqNumber = metadataService.getThemeSeqNumber(screenInstance.getScreenMasterCodeFkId());
//            screenInstance.setThemeSeqNumPosInt(themeSeqNumber);
//        }
        addSections(screenInstance, targetSectionInstanceList, deviceType, subjectUserID);
        return screenInstance;
    }

    private void addSections(ScreenInstance screenInstance, List<String> targetSectionInstanceList, String deviceType, String subjectUserID) {
        // Now on the basis of target screen and target sections get all the sections and add them to screens
        // get the list of all the section belonging to the given screen and build the hierarchy.
        // After building the hierarchy select only those sections which are present in target section list.
        List<ScreenSectionInstanceVO> sectionInstanceVOS = uiLayoutDAO.getSectionInstances(screenInstance.getScreenInstancePkId(), deviceType);
        Map<String, ScreenSectionInstance> idToSectionMap = new HashMap<>();
        // Making of section object
        for(ScreenSectionInstanceVO instanceVO : sectionInstanceVOS){
            ScreenSectionInstance sectionInstance = ScreenSectionInstance.fromVO(instanceVO);
            AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
            AttachmentObject imageURL = Util.getURLforS3(instanceVO.getNoDataMessageImageId(), s3Client);
            sectionInstance.setNoDataMessageImageId(imageURL);
            sectionInstance.setNoDataMessageIconCode(Util.getIcon(instanceVO.getNoDataMessageIconCode()));
            idToSectionMap.put(sectionInstance.getScreenSectionInstancePkId(), sectionInstance);
        }
        // Making of parent child relationship in section
        for(ScreenSectionInstance instance : idToSectionMap.values()){
            String parentSectionId = instance.getParentSectionInstanceFkId();
            if(StringUtil.isDefined(parentSectionId)){
                idToSectionMap.get(parentSectionId).getSections().add(instance);
            }
        }
        // Adding the relevant sections
        if(targetSectionInstanceList!=null && !targetSectionInstanceList.isEmpty()) {
            for (String targetSection : targetSectionInstanceList) {
                if (idToSectionMap.containsKey(targetSection))
                    screenInstance.getSections().add(idToSectionMap.get(targetSection));
            }
        }else{
            screenInstance.getSections().addAll(idToSectionMap.values());
        }
        // Now visit all the sections that are required of this particular screen. traverse the map and get the list of all the sections which are present inside the tree
        // Now before this get all the sections permissions for all the above section.
        RelevantSectionData relevantSectionData = getRelevantSectionsAccordingToTarget(screenInstance.getSections(), screenInstance);
        idToSectionMap = relevantSectionData.getSectionIdMap();
        addLayoutPermissionsToSections(relevantSectionData.getSectionIdMap(), deviceType);
//        applyLayoutPermissionsToSections(screenInstance.getSections(), subjectUserID, idToSectionMap);
        screenInstance.setListOfListOfControls(relevantSectionData.getControls());
        addControls(screenInstance, idToSectionMap, deviceType);
        addSectionActions(screenInstance, idToSectionMap, deviceType);
        addOrgChartFilters(idToSectionMap);
    }

    private void addLayoutPermissionsToSections(Map<String, ScreenSectionInstance> idToSectionMap, String deviceType) {
        // This section id list should be passed to this new method and get all the permissions.
        List<LayoutPermissionsVO> layoutPermissionsVOS = uiLayoutDAO.getLayoutPermissionsForSections(idToSectionMap.keySet(), deviceType);
        for(LayoutPermissionsVO layoutPermissionsVO : layoutPermissionsVOS){
            LayoutPermissions layoutPermissions = LayoutPermissions.fromVO(layoutPermissionsVO);
            idToSectionMap.get(layoutPermissionsVO.getFkId()).getLayoutPermissions().add(layoutPermissions);
        }
    }

    private void addLayoutPermissionsToControls(Map<String, SectionControl> controlIdMap, Set<String> sectionIDs, String deviceType) {
        List<LayoutPermissionsVO> layoutPermissionsVOS = uiLayoutDAO.getLayoutPermissionsForControls(sectionIDs, deviceType);
        for(LayoutPermissionsVO layoutPermissionsVO : layoutPermissionsVOS){
            LayoutPermissions layoutPermissions = LayoutPermissions.fromVO(layoutPermissionsVO);
            controlIdMap.get(layoutPermissionsVO.getFkId()).getLayoutPermissions().add(layoutPermissions);
        }
    }

    private void addOrgChartFilters(Map<String, ScreenSectionInstance> idToSectionMap) {
        List<OrgChartFilterLabelVO> orgChartFilterLabelVOS = uiLayoutDAO.getOrgChartFilterLabels(idToSectionMap.keySet());
        Map<String, OrgChartFilterLabel> orgChartFilterLabelMap = new HashMap<>();
        for(OrgChartFilterLabelVO orgChartFilterLabelVO : orgChartFilterLabelVOS){
            OrgChartFilterLabel orgChartFilterLabel = OrgChartFilterLabel.fromVO(orgChartFilterLabelVO);
            orgChartFilterLabel.setLabelIcon(Util.getIcon(orgChartFilterLabelVO.getLabelIcon()));
            orgChartFilterLabel.setControlTypeOnStateIcon(Util.getIcon(orgChartFilterLabelVO.getControlTypeOnStateIcon()));
            orgChartFilterLabel.setControlTypeOffStateIcon(Util.getIcon(orgChartFilterLabelVO.getControlTypeOffStateIcon()));
            if(StringUtil.isDefined(orgChartFilterLabelVO.getBaseTemplate()) &&  StringUtil.isDefined(orgChartFilterLabelVO.getLabelDOA())){
                EntityAttributeMetadata doaMeta = TenantAwareCache.getMetaDataRepository().getMetadataByBtDoa(orgChartFilterLabelVO.getBaseTemplate(), orgChartFilterLabelVO.getLabelDOA());
                orgChartFilterLabel.setLabelName(doaMeta.getAttrDisplayName(false));
            }else{
                orgChartFilterLabel.setLabelName(Util.getG11nValue(orgChartFilterLabel.getLabelName(), null));
            }
            orgChartFilterLabelMap.put(orgChartFilterLabel.getOrgChartFilterPKID(), orgChartFilterLabel);
            idToSectionMap.get(orgChartFilterLabelVO.getFilterSectionID()).getOrgChartFilterLabels().add(orgChartFilterLabel);
        }
        addOrgChartFilterRange(idToSectionMap, orgChartFilterLabelMap);
//        addOrgChartFilterAttrPath(idToSectionMap, orgChartFilterLabelMap);
//        screenInstance.getListOfOrgChartLabels().addAll(orgChartFilterLabelMap.values());
    }

//    private void addOrgChartFilterAttrPath(Map<String, ScreenSectionInstance> idToSectionMap, Map<String, OrgChartFilterLabel> orgChartFilterLabelMap) {
//        List<OrgChartFilterAttrPathVO> orgChartFilterAttrPathVOS = uiLayoutDAO.getOrgChartFilterAttrPaths(idToSectionMap.keySet());
//        for(OrgChartFilterAttrPathVO orgChartFilterAttrPathVO : orgChartFilterAttrPathVOS){
//            OrgChartFilterAttrPath orgChartFilterAttrPath = OrgChartFilterAttrPath.fromVO(orgChartFilterAttrPathVO);
//            orgChartFilterLabelMap.get(orgChartFilterAttrPathVO.getOrgChartLabel()).getOrgChartFilterAttrPathList().add(orgChartFilterAttrPath);
//        }
//    }

    private void addOrgChartFilterRange(Map<String, ScreenSectionInstance> idToSectionMap, Map<String, OrgChartFilterLabel> orgChartFilterLabelMap) {
        List<OrgChartFilterRangeVO> orgChartFilterRangeVOS = uiLayoutDAO.getOrgChartFilterRanges(idToSectionMap.keySet());
        for(OrgChartFilterRangeVO orgChartFilterRangeVO : orgChartFilterRangeVOS){
            OrgChartFilterRange orgChartFilterRange = OrgChartFilterRange.fromVO(orgChartFilterRangeVO);
            orgChartFilterLabelMap.get(orgChartFilterRangeVO.getOrgChartLabel()).getOrgChartFilterRangeList().add(orgChartFilterRange);
        }
    }

    private void applyLayoutPermissionsToSections(List<ScreenSectionInstance> sections, String subjectUserID, Map<String, ScreenSectionInstance> idToSectionMap) {
        for(ScreenSectionInstance sectionInstance : sections){
            List<LayoutPermissions> layoutPermissions = sectionInstance.getLayoutPermissions();
            if(layoutPermissions != null){
                for(LayoutPermissions layoutPermission : layoutPermissions){
                    // now see if this section is applicable for the logged in user and the subject user.
                    // Write a function to Util which will return true or false on the basis of layout permission logged in user and subject user.
                    Boolean permissionResult = AppUtil.isElementApplicableForTheUser(layoutPermission, subjectUserID);
                    if(permissionResult){
                        applyLayoutPermissionsToSections(sectionInstance.getSections(), subjectUserID, idToSectionMap);
                    }else{
                        sectionInstance.setIgnoreAllChildrenFromThisSection(true);
                        // From here remove all children and self from idToSectionMap
                        idToSectionMap.remove(sectionInstance.getScreenSectionInstancePkId());
                        removeFromSectionMap(idToSectionMap, sectionInstance.getSections());
                    }
                }
            }else{
                try {
                    throw new Exception("permission not defined for section "+ sectionInstance.getScreenSectionInstanceName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void removeFromSectionMap(Map<String, ScreenSectionInstance> idToSectionMap, List<ScreenSectionInstance> sections) {
        for(ScreenSectionInstance sectionInstance : sections){
            idToSectionMap.remove(sectionInstance.getScreenSectionInstancePkId());
            removeFromSectionMap(idToSectionMap, sectionInstance.getSections());
        }
    }

    private RelevantSectionData getRelevantSectionsAccordingToTarget(List<ScreenSectionInstance> sections, ScreenInstance screenInstance) {
        RelevantSectionData relevantSectionData = new RelevantSectionData();
        updateRelevantSectionData(relevantSectionData, sections, screenInstance);
        return relevantSectionData;
    }

    private void updateRelevantSectionData(RelevantSectionData relevantSectionData, List<ScreenSectionInstance> sections, ScreenInstance screenInstance) {
        for(ScreenSectionInstance sectionInstance : sections){
            // here check if we have the permission to access this particular section or not.
            // else don't go to the child of this section itself.
            // The permission is decided on the basis of subject user, logged in user and relationship between them. Before this we should get all the permissions for all the sections in this screen. append it here.
            if(StringUtil.isDefined(sectionInstance.getsUMtPEAlias())){
                if(!screenInstance.getMtPEAliasToSubjectUserPathMap().containsKey(sectionInstance.getsUAttributePathExpn())){
                    SubjectUserPath subjectUserPath = new SubjectUserPath(sectionInstance.getsUMtPE(), sectionInstance.getsUMtPEAlias(), sectionInstance.getsUParentMTPEAlias(), sectionInstance.getSuFkRelnWithParent(), sectionInstance.getsUAttributePathExpn());
                    screenInstance.getMtPEAliasToSubjectUserPathMap().put(sectionInstance.getsUMtPEAlias(), subjectUserPath);
                }
            }
            relevantSectionData.getSectionIdMap().put(sectionInstance.getScreenSectionInstancePkId(), sectionInstance);
            relevantSectionData.getControls().add(sectionInstance.getControls());
            updateRelevantSectionData(relevantSectionData,sectionInstance.getSections(), screenInstance);
        }
    }

    private void addSectionActions(ScreenInstance screenInstance, Map<String, ScreenSectionInstance> idToSectionMap, String deviceType) {
        List<SectionActionInstanceVO> sectionActionInstanceVOS = uiLayoutDAO.getSectionActionInstance(screenInstance.getScreenInstancePkId(), idToSectionMap.keySet());
        Map<String, SectionActionInstance> actionInstanceMap = new HashMap<>();
        for(SectionActionInstanceVO actionInstanceVO : sectionActionInstanceVOS){
            SectionActionInstance sectionActionInstance = SectionActionInstance.fromVO(actionInstanceVO);
            if(StringUtil.isDefined(actionInstanceVO.getOverrideActionIcon())){
                sectionActionInstance.setActionIcon(Util.getIcon(actionInstanceVO.getOverrideActionIcon()));
            }else{
                sectionActionInstance.setActionIcon(Util.getIcon(actionInstanceVO.getActionVisualMasterActionIcon()));
            }
            if(StringUtil.isDefined(actionInstanceVO.getOverrideActionLabel())){
                sectionActionInstance.setActionLabel(Util.getG11nValue(actionInstanceVO.getOverrideActionLabel(), null));
            }else {
                sectionActionInstance.setActionLabel(Util.getG11nValue(actionInstanceVO.getActionVisualMasterActionLabel(), null));
            }
            if(sectionActionInstance.getActionMasterCode().equals("NON_INTERACTIVE_LOAD_SECN")){
                screenInstance.getNonInteractiveLoadActions().add(sectionActionInstance);
            }
            if(sectionActionInstance.getActionVisualMasterCode()!= null && sectionActionInstance.getActionVisualMasterCode().matches("ENDORSE|UNENDORSE")){
                screenInstance.getEndorseMTPEAlias().add(sectionActionInstance.getMtPEAlias());
            }
            sectionActionInstance.setActionGroupIcon(Util.getIcon(actionInstanceVO.getActionGroupIcon()));
            sectionActionInstance.setActionGroupName(Util.getG11nValue(actionInstanceVO.getActionGroupName(), null));
            idToSectionMap.get(actionInstanceVO.getSectionInstanceFkId()).getActions().add(sectionActionInstance);
            actionInstanceMap.put(sectionActionInstance.getActionPkId(), sectionActionInstance);
        }
        addSectionActionArguments(screenInstance, actionInstanceMap, idToSectionMap.keySet());
        addSectionActionTarget(screenInstance, actionInstanceMap, idToSectionMap.keySet(), deviceType);
        addLayoutPermissionsToSectionActions(actionInstanceMap, idToSectionMap.keySet(), deviceType);
    }

    private void addLayoutPermissionsToSectionActions(Map<String, SectionActionInstance> actionInstanceMap, Set<String> sectionIDs, String deviceType) {
        List<LayoutPermissionsVO> layoutPermissionsVOS = uiLayoutDAO.getLayoutPermissionsForSectionActions(sectionIDs, deviceType);
        for(LayoutPermissionsVO layoutPermissionsVO : layoutPermissionsVOS){
            LayoutPermissions layoutPermissions = LayoutPermissions.fromVO(layoutPermissionsVO);
            actionInstanceMap.get(layoutPermissionsVO.getFkId()).getLayoutPermissions().add(layoutPermissions);
        }
    }

    private void addSectionActionTarget(ScreenInstance screenInstance, Map<String, SectionActionInstance> actionInstanceMap, Set<String> relevantSectionIdList, String deviceType) {
        List<SectionActionTargetsVO> actionTargetsVOS = uiLayoutDAO.getSectionActionTargets(screenInstance.getScreenInstancePkId(), relevantSectionIdList, deviceType);
        Map<String, ActionTarget> actionTargetsMap = new HashMap<>();
        for(SectionActionTargetsVO actionTargetsVO : actionTargetsVOS){
            ActionTarget actionTarget = ActionTarget.fromVO(actionTargetsVO);
            actionTargetsMap.put(actionTarget.getActionTargetPkId(), actionTarget);
            actionInstanceMap.get(actionTargetsVO.getSectionActionFkId()).getTargets().add(actionTarget);
        }
        addSectionActionTargetFilterCriteria(screenInstance, actionTargetsMap, relevantSectionIdList, deviceType);
        addSectionActionTargetFilterParams(screenInstance, actionTargetsMap, relevantSectionIdList, deviceType);
        addSectionActionTargetSortParams(screenInstance.getScreenInstancePkId(), actionTargetsMap, relevantSectionIdList, deviceType);
    }

    private void addSectionActionTargetSortParams(String screenInstancePkId, Map<String, ActionTarget> actionTargetsMap, Set<String> relevantSectionIdList, String deviceType) {
        List<SectionActionTargetSectionSortParamsVO> sectionSortParamsVOS = uiLayoutDAO.getSectionActionTargetSortParams(screenInstancePkId, relevantSectionIdList, deviceType);
        for(SectionActionTargetSectionSortParamsVO sortParamsVO : sectionSortParamsVOS){
            TargetSectionSortParams targetSectionSortParams = TargetSectionSortParams.fromVO(sortParamsVO);
            actionTargetsMap.get(sortParamsVO.getActionTargetFkId()).getTargetSectionSortParams().add(targetSectionSortParams);
        }
    }

    private void addSectionActionTargetFilterParams(ScreenInstance screenInstance, Map<String, ActionTarget> actionTargetsMap, Set<String> relevantSectionIdList, String deviceType) {
        List<SectionActionTargetSectionFilterParamsVO> filterParamsVOS = uiLayoutDAO.getSectionActionTargetFilterParams(screenInstance.getScreenInstancePkId(), relevantSectionIdList, deviceType);
        for(SectionActionTargetSectionFilterParamsVO filterParamsVO : filterParamsVOS){
            TargetSectionFilterParams filterParams = TargetSectionFilterParams.fromVO(filterParamsVO);
            actionTargetsMap.get(filterParamsVO.getActionTargetFkId()).getTargetSectionFilterParams().add(filterParams);
            screenInstance.getListOfTargetSectionsFilters().add(filterParams);
        }
    }

    private void addSectionActionTargetFilterCriteria(ScreenInstance screenInstance, Map<String, ActionTarget> actionTargetsMap, Set<String> relevantSectionIdList, String deviceType) {
        List<SectionActionTargetFilterCriteriaVO> filterCriteriaVOS = uiLayoutDAO.getSectionActionTargetFilterCriteria(screenInstance.getScreenInstancePkId(), relevantSectionIdList, deviceType);
        for(SectionActionTargetFilterCriteriaVO filterCriteriaVO : filterCriteriaVOS){
            TargetFilterCriteria filterCriteria = TargetFilterCriteria.fromVO(filterCriteriaVO);
            actionTargetsMap.get(filterCriteriaVO.getActionTargetFkId()).getFilterCriteria().add(filterCriteria);
            screenInstance.getListOfFilterCriteria().add(filterCriteria);
        }
    }

    private void addSectionActionArguments(ScreenInstance screenInstance, Map<String, SectionActionInstance> actionInstanceMap, Set<String> relevantSectionIdList) {
        List<SectionActionArgumentVO> actionArgumentVOS = uiLayoutDAO.getSectionActionArguments(screenInstance.getScreenInstancePkId(), relevantSectionIdList);
        for(SectionActionArgumentVO sectionActionArgumentVO : actionArgumentVOS){
            ActionArgument actionArgument = ActionArgument.fromVO(sectionActionArgumentVO);
            actionInstanceMap.get(sectionActionArgumentVO.getSectionActionFkId()).getActionArguments().add(actionArgument);
            screenInstance.getListOfActionArguments().add(actionArgument);
        }
    }

    private void addControls(ScreenInstance screenInstance, Map<String, ScreenSectionInstance> idToSectionMap, String deviceType) {
        // Add the controls to the screen sections
        // Here get all the controls for the screen
        List<SectionControlVO> controlVOS = uiLayoutDAO.getControlList(screenInstance.getScreenInstancePkId(), idToSectionMap.keySet());
        Map<String, SectionControl> controlIdMap = new HashMap<>();
        Map<String, String> controlIDToRatingScaleMap = new HashMap<>();
        Set<String> ratingScaleIDs = new HashSet<>();
        List<SectionControl> labelPhoneNumberControls = new ArrayList<>();
        for(SectionControlVO sectionControlVO : controlVOS){
            SectionControl sectionControl = SectionControl.fromVO(sectionControlVO);
            sectionControl.setControlTypeIcon(Util.getIcon(sectionControlVO.getControlTypeIcon()));
            sectionControl.setControlOnStateIcon(Util.getIcon(sectionControlVO.getControlOnStateIcon()));
            sectionControl.setControlOffStateIcon(Util.getIcon(sectionControlVO.getControlOffStateIcon()));
            if(StringUtil.isDefined(sectionControlVO.getRatingScaleID())){
                ratingScaleIDs.add(sectionControlVO.getRatingScaleID());
                controlIDToRatingScaleMap.put(sectionControlVO.getControlPkId(), sectionControlVO.getRatingScaleID());
            }
            //
            MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
            String baseTemplate = sectionControlVO.getBaseTemplateCode();
            if(StringUtil.isDefined(sectionControlVO.getStaticLabelText())) {
                sectionControl.setDispLabel(Util.getG11nValue(sectionControlVO.getStaticLabelText(), null));
            }else{
                if(StringUtil.isDefined(sectionControlVO.getDispLabelAttribute())){
                    EntityAttributeMetadata btDOAMeta = metadataService.getMetadataByBtDoa(sectionControlVO.getDispLabelAttribute());
                    sectionControl.setDispIcon(btDOAMeta.getAttrIcon());
                    if(sectionControlVO.isFarLabel()){
                        sectionControl.setDispLabel(btDOAMeta.getAttrRelnDisplayName());
                    }else{
                        sectionControl.setDispLabel(btDOAMeta.getAttrDisplayName(false));
                    }
                }
            }

            if(sectionControl.getControlType() != null && sectionControl.getControlType().equals("LABEL_PHONE_NUMBER")){
                labelPhoneNumberControls.add(sectionControl);
            }
            //TODO fix it according to the base Template.
            //TODO Use the far and near marker to decide the proper label value.
//            sectionControl.setLabelValue(metadataService.getbt);
//            sectionControl.setLabelIcon();
            controlIdMap.put(sectionControl.getControlPkId(), sectionControl);
            idToSectionMap.get(sectionControlVO.getSectionInstanceFkId()).getControls().add(sectionControl);
            addSubjectUserDataFromSectionToControl(idToSectionMap.get(sectionControlVO.getSectionInstanceFkId()), sectionControl);
            screenInstance.getControlMap().put(sectionControl.getControlPkId(), sectionControl);
        }
        for(SectionControl labelPhoneNumberControl : labelPhoneNumberControls){
            if(StringUtil.isDefined(labelPhoneNumberControl.getAssociatedRelnControlId())){
                if(screenInstance.getControlMap().containsKey(labelPhoneNumberControl.getAssociatedRelnControlId()))
                    screenInstance.getControlMap().get(labelPhoneNumberControl.getAssociatedRelnControlId()).setProbableRelationFilter(true);
            }
        }
        addStaticControlValues(screenInstance.getScreenInstancePkId(), controlIdMap, idToSectionMap.keySet());
        addControlInstanceFilters(screenInstance, controlIdMap, idToSectionMap.keySet());
        addControlActions(screenInstance, controlIdMap, idToSectionMap.keySet(), deviceType);
        addControlEvents(screenInstance, controlIdMap, idToSectionMap.keySet(), deviceType);
        addRatingLevel(controlIdMap, ratingScaleIDs, controlIDToRatingScaleMap);
        addLayoutPermissionsToControls(controlIdMap, idToSectionMap.keySet(), deviceType);
    }

    private void addRatingLevel(Map<String, SectionControl> controlIdMap, Set<String> ratingScaleIDs, Map<String, String> controlIDToRatingScaleMap) {
        if(ratingScaleIDs == null || ratingScaleIDs.isEmpty())
            return;
        Map<String, RatingScaleVO> ratingScaleVOS = uiLayoutDAO.getRatingScaleVOs(ratingScaleIDs);
        for(Map.Entry<String, String> entry : controlIDToRatingScaleMap.entrySet()){
            SectionControl sectionControl = controlIdMap.get(entry.getKey());
            RatingScaleVO ratingScaleVO = ratingScaleVOS.get(sectionControl.getRatingScaleID());
            RatingScale ratingScale = RatingScale.fromVO(ratingScaleVO);
            ratingScale.setRatingScaleIcon(Util.getIcon(ratingScaleVO.getRatingScaleIcon()));
            sectionControl.setRatingScale(ratingScale);
        }
    }

    private void addControlEvents(ScreenInstance screenInstance, Map<String, SectionControl> controlIdMap, Set<String> relevantSectionIdList, String deviceType) {
        List<ControlEventInstanceVO> controlEventInstanceVOS = uiLayoutDAO.getControlEventVO(screenInstance.getScreenInstancePkId(), relevantSectionIdList);
        Map<String, ControlEventInstance> idToControlEventMap = new HashMap<>();
        for(ControlEventInstanceVO controlEventInstanceVO : controlEventInstanceVOS){
            ControlEventInstance controlEventInstance = ControlEventInstance.fromVO(controlEventInstanceVO);
            controlIdMap.get(controlEventInstance.getControlId()).getEvents().add(controlEventInstance);
            idToControlEventMap.put(controlEventInstance.getControlEventPkId(), controlEventInstance);
        }
        addControlEventCriteria(idToControlEventMap);
        addControlEventInputParametersDetails(idToControlEventMap);
    }

    private void addControlEventInputParametersDetails(Map<String, ControlEventInstance> idToControlEventMap) {
        if(idToControlEventMap!=null && !idToControlEventMap.isEmpty()){
            List<FormulaInputParametersDetailsVO> formulaInputParametersDetailsVOS = uiLayoutDAO.getInputParametersDetails(idToControlEventMap.keySet());
            for(FormulaInputParametersDetailsVO formulaInputParametersDetailsVO : formulaInputParametersDetailsVOS){
                FormulaInputParametersDetails formulaInputParametersDetails = FormulaInputParametersDetails.fromVO(formulaInputParametersDetailsVO);
                idToControlEventMap.get(formulaInputParametersDetailsVO.getControlEvent()).getFormulaInputParametersDetails().add(formulaInputParametersDetails);
            }
        }
    }

    private void addControlEventCriteria(Map<String, ControlEventInstance> idToControlEventMap) {
        if(idToControlEventMap!=null && !idToControlEventMap.isEmpty()){
            List<ControlEventCriteriaVO> controlEventCriteriaVOS = uiLayoutDAO.getControlEventCriteriaVOs(idToControlEventMap.keySet());
            for(ControlEventCriteriaVO controlEventCriteriaVO : controlEventCriteriaVOS){
                ControlEventCriteria controlEventCriteria = ControlEventCriteria.fromVO(controlEventCriteriaVO);
                idToControlEventMap.get(controlEventCriteriaVO.getControlEvent()).getCriteriaList().add(controlEventCriteria);
            }
        }
    }

    private void addSubjectUserDataFromSectionToControl(ScreenSectionInstance screenSectionInstance, SectionControl sectionControl) {
        sectionControl.setsUMtPE(screenSectionInstance.getsUMtPE());
        sectionControl.setsUMtPEAlias(screenSectionInstance.getsUMtPEAlias());
        sectionControl.setsUParentMTPEAlias(screenSectionInstance.getsUParentMTPEAlias());
        sectionControl.setSuFkRelnWithParent(screenSectionInstance.getSuFkRelnWithParent());
        sectionControl.setsUAttributePathExpn(screenSectionInstance.getsUAttributePathExpn());
    }

    private void addControlActions(ScreenInstance screenInstance, Map<String, SectionControl> controlIdMap, Set<String> relevantSectionIdList, String deviceType) {
        List<ControlActionInstanceVO> actionInstanceVOS = uiLayoutDAO.getControlActionInstances(screenInstance.getScreenInstancePkId(), relevantSectionIdList);
        Map<String, ControlActionInstance> controlActionIdMap = new HashMap<>();
        for(ControlActionInstanceVO controlActionInstanceVO : actionInstanceVOS){
            ControlActionInstance controlActionInstance = ControlActionInstance.fromVO(controlActionInstanceVO);
            controlActionIdMap.put(controlActionInstance.getActionPkId(), controlActionInstance);
            controlIdMap.get(controlActionInstanceVO.getControlFkId()).getActions().add(controlActionInstance);
        }
        addActionArguments(screenInstance, controlActionIdMap, relevantSectionIdList);
        addControlActionTargets(screenInstance, controlActionIdMap, relevantSectionIdList, deviceType);
        addLayoutPermissionsToControlActions(controlActionIdMap,relevantSectionIdList, deviceType);
    }

    private void addLayoutPermissionsToControlActions(Map<String, ControlActionInstance> controlActionIdMap, Set<String> relevantSectionIdList, String deviceType) {
        List<LayoutPermissionsVO> layoutPermissionsVOS = uiLayoutDAO.getLayoutPermissionsForControlActions(relevantSectionIdList, deviceType);
        for(LayoutPermissionsVO layoutPermissionsVO : layoutPermissionsVOS){
            LayoutPermissions layoutPermissions = LayoutPermissions.fromVO(layoutPermissionsVO);
            controlActionIdMap.get(layoutPermissionsVO.getFkId()).getLayoutPermissions().add(layoutPermissions);
        }
    }

    private void addControlActionTargets(ScreenInstance screenInstance, Map<String, ControlActionInstance> controlActionIdMap, Set<String> relevantSectionIdList, String deviceType) {
        List<ControlActionTargetsVO> actionTargetsVOS = uiLayoutDAO.getControlActionTargets(screenInstance.getScreenInstancePkId(),relevantSectionIdList, deviceType);
        Map<String, ActionTarget> actionTargetsMap = new HashMap<>();
        for(ControlActionTargetsVO actionTargetsVO : actionTargetsVOS){
            ActionTarget actionTarget = ActionTarget.fromVO(actionTargetsVO);
            actionTargetsMap.put(actionTarget.getActionTargetPkId(), actionTarget);
            controlActionIdMap.get(actionTargetsVO.getControlActionFkId()).getTargets().add(actionTarget);
        }
        addActionTargetFilterCriteria(screenInstance, actionTargetsMap, relevantSectionIdList, deviceType);
        addActionTargetFilterParams(screenInstance, actionTargetsMap, relevantSectionIdList, deviceType);
        addActionTargetSortParams(screenInstance, actionTargetsMap, relevantSectionIdList, deviceType);
    }

    private void addActionTargetSortParams(ScreenInstance screenInstance, Map<String, ActionTarget> actionTargetsMap, Set<String> relevantSectionIdList, String deviceType) {
        List<ControlActionTargetSectionSortParamsVO> sectionSortParamsVOS = uiLayoutDAO.getControlActionTargetSortParams(screenInstance.getScreenInstancePkId(), relevantSectionIdList, deviceType);
        for(ControlActionTargetSectionSortParamsVO sortParamsVO : sectionSortParamsVOS){
            TargetSectionSortParams targetSectionSortParams = TargetSectionSortParams.fromVO(sortParamsVO);
            actionTargetsMap.get(sortParamsVO.getActionTargetFkId()).getTargetSectionSortParams().add(targetSectionSortParams);
        }
    }

    private void addActionTargetFilterParams(ScreenInstance screenInstance, Map<String, ActionTarget> actionTargetsMap, Set<String> relevantSectionIdList, String deviceType) {
        List<ControlActionTargetSectionFilterParamsVO> filterParamsVOS = uiLayoutDAO.getControlActionTargetFilterParams(screenInstance.getScreenInstancePkId(), relevantSectionIdList, deviceType);
        for(ControlActionTargetSectionFilterParamsVO filterParamsVO : filterParamsVOS){
            TargetSectionFilterParams filterParams = TargetSectionFilterParams.fromVO(filterParamsVO);
            actionTargetsMap.get(filterParamsVO.getActionTargetFkId()).getTargetSectionFilterParams().add(filterParams);
            screenInstance.getListOfTargetSectionsFilters().add(filterParams);
            String attributeValue = filterParams.getAttributeValue();
            if(StringUtil.isDefined(attributeValue)){
                if(attributeValue.startsWith("Control|")){
                    String controlID = attributeValue.substring(8);
                    if(screenInstance.getControlMap().containsKey(controlID)){
                        SectionControl sectionControl = screenInstance.getControlMap().get(controlID);
                        sectionControl.setProbableRelationFilter(true);
                    }else{
                        //TODO throw error that probele relation control filter is not found.
                    }
                }
            }
        }
    }

    private void addActionTargetFilterCriteria(ScreenInstance screenInstance, Map<String, ActionTarget> actionTargetsMap, Set<String> relevantSectionIdList, String deviceType) {
        List<ControlActionTargetFilterCriteriaVO> filterCriteriaVOS = uiLayoutDAO.getControlActionTargetFilterCriteria(screenInstance.getScreenInstancePkId(), relevantSectionIdList, deviceType);
        for(ControlActionTargetFilterCriteriaVO filterCriteriaVO : filterCriteriaVOS){
            TargetFilterCriteria filterCriteria = TargetFilterCriteria.fromVO(filterCriteriaVO);
            actionTargetsMap.get(filterCriteriaVO.getActionTargetFkId()).getFilterCriteria().add(filterCriteria);
            screenInstance.getListOfFilterCriteria().add(filterCriteria);
        }
    }

    private void addActionArguments(ScreenInstance screenInstance, Map<String, ControlActionInstance> controlActionIdMap, Set<String> relevantSectionIdList) {
        List<ControlActionArgumentVO> actionArgumentVOS = uiLayoutDAO.getControlActionArguments(screenInstance.getScreenInstancePkId(),relevantSectionIdList);
        for(ControlActionArgumentVO controlActionArgumentVO : actionArgumentVOS){
            ActionArgument actionArgument = ActionArgument.fromVO(controlActionArgumentVO);
            controlActionIdMap.get(controlActionArgumentVO.getControlActionFkId()).getActionArguments().add(actionArgument);
            screenInstance.getListOfActionArguments().add(actionArgument);
        }
    }

    private void addControlInstanceFilters(ScreenInstance screenInstance, Map<String, SectionControl> controlIdMap, Set<String> relevantSectionIdList) {
        List<ControlInstanceFiltersVO> controlInstanceFiltersVOS = uiLayoutDAO.getControlInstanceFilters(screenInstance.getScreenInstancePkId(), relevantSectionIdList);
        for(ControlInstanceFiltersVO instanceFiltersVO : controlInstanceFiltersVOS){
            ControlInstanceFilters controlInstanceFilters = ControlInstanceFilters.fromVO(instanceFiltersVO);
            controlIdMap.get(instanceFiltersVO.getControlFkId()).getControlFilters().add(controlInstanceFilters);
        }
    }

    private void addStaticControlValues(String screenInstancePkId, Map<String, SectionControl> controlIdMap, Set<String> relevantSectionIdList) {
        List<ControlStaticValuesVO> staticValuesVOS = uiLayoutDAO.getStaticControlValues(screenInstancePkId, relevantSectionIdList);
        for(ControlStaticValuesVO staticValuesVO : staticValuesVOS){
            ControlStaticValues controlStaticValues = ControlStaticValues.fromVO(staticValuesVO);
            controlStaticValues.setStaticIcon(Util.getIcon(staticValuesVO.getStaticIcon()));
            AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
            controlStaticValues.setStaticImage(Util.preSignedGetUrl(staticValuesVO.getStaticImage(), s3Client));
            controlIdMap.get(staticValuesVO.getControlFkId()).getStaticValues().add(controlStaticValues);
        }
    }

    @Override
    public ActionTarget getActionTarget(String actionTargetPkId, String whatFor) {
        ActionTarget actionTarget = null;

        switch (whatFor){
            case "CONTROL":
                ControlActionTargetsVO controlTargetVO = null;
                controlTargetVO = uiLayoutDAO.getControlActionTargets(actionTargetPkId);
                actionTarget = ActionTarget.fromVO(controlTargetVO);
                break;
            case "SECTION":
                SectionActionTargetsVO sectionTargetsVO = null;
                sectionTargetsVO = uiLayoutDAO.getSectionActionTargets(actionTargetPkId);
                actionTarget = ActionTarget.fromVO(sectionTargetsVO);
                break;
            default:
        }
        return actionTarget;
    }

    @Override
    public GenericQuery getGenericQuery(String queryID) {
        GenericQuery genericQuery = new GenericQuery(queryID);
        List<GenericQueryAttributesVO> genericQueryAttributesVOS = uiLayoutDAO.getGenericQueryAttributes(queryID);
        List<GenericQueryFiltersVO> genericQueryFiltersVOS = uiLayoutDAO.getGenericQueryFilters(queryID);
        for(GenericQueryAttributesVO genericQueryAttributesVO : genericQueryAttributesVOS){
            GenericQueryAttribute genericQueryAttribute = GenericQueryAttribute.fromVO(genericQueryAttributesVO);
            genericQuery.getAttributes().add(genericQueryAttribute);
        }
        for(GenericQueryFiltersVO genericQueryFiltersVO : genericQueryFiltersVOS){
            GenericQueryFilters genericQueryFilters = GenericQueryFilters.fromVO(genericQueryFiltersVO);
            genericQuery.getFilters().add(genericQueryFilters);
        }
        return genericQuery;
    }

    @Override
    public List<String> getAllPersonIDs() {
        return uiLayoutDAO.getAllPersonIDs();
    }

    @Override
    public Map<String, StateVO> getActiveStateIcons() {
        Map<String, StateVO> activeStateIcon = uiLayoutDAO.getActiveStateIcons();
        return activeStateIcon;
    }

    @Override
    public Map<String, StateVO> getRecordStateIcons() {
        Map<String, StateVO> recordStateIcon = uiLayoutDAO.getRecordStateIcons();
        return recordStateIcon;
    }

    @Override
    public Map<String, StateVO> getSavedStateIcons() {
        Map<String, StateVO> savedStateIcon = uiLayoutDAO.getSavedStateIcons();
        return savedStateIcon;
    }

    @Override
    public List<JobRelationshipTypeVO> getJobRelationshipTypes() {
        List<JobRelationshipTypeVO> relationshipTypeVOS = uiLayoutDAO.getJobRelationshipTypes();
        return relationshipTypeVOS;
    }

    @Override
    public List<PersonDetailsVO> getFeaturingPersonDetail(String contextID, String featuredTableName) {
        List<PersonDetailsVO> detailsVOs = uiLayoutDAO.getFeaturingPersonDetail(contextID, featuredTableName);
        return detailsVOs;
    }

    @Override
    public List<OrganizationBreakUpData> getOrganizationBreakUpData(String contextID) {
        List<OrganizationBreakUpData> organizationBreakUpDataList = new ArrayList<>();
        List<OrganizationBreakUpDataVO> organizationBreakUpDataVOS = uiLayoutDAO.getOrganizationBreakUpData(contextID);
        for(OrganizationBreakUpDataVO organizationBreakUpDataVO : organizationBreakUpDataVOS){
            OrganizationBreakUpData organizationBreakUpData = OrganizationBreakUpData.fromVO(organizationBreakUpDataVO);
            organizationBreakUpDataList.add(organizationBreakUpData);
        }
        return organizationBreakUpDataList;
    }

    @Override
    public List<LocationBreakUpData> getLocationBreakUpData(String contextID) {
        List<LocationBreakUpData> locationBreakUpDataList = new ArrayList<>();
        List<LocationBreakUpDataVO> locationBreakUpDataVOS = uiLayoutDAO.getLocationBreakUpData(contextID);
        for (LocationBreakUpDataVO locationBreakUpDataVO : locationBreakUpDataVOS) {
            LocationBreakUpData locationBreakUpData = LocationBreakUpData.fromVO(locationBreakUpDataVO);
            locationBreakUpDataList.add(locationBreakUpData);
        }
        return locationBreakUpDataList;
    }

    public List<JobRelationships> getDefaultJobRelations(String contextID1, String contextID2) {
        List<JobRelationships> result = new ArrayList<>();
        List<JobRelationshipsVO> relationshipsVOS = uiLayoutDAO.getDefaultJobRelations(contextID1, contextID2);
        for (JobRelationshipsVO relationshipsVO : relationshipsVOS) {
            JobRelationships jobRelationships = JobRelationships.fromVO(relationshipsVO);
            result.add(jobRelationships);
        }
        return result;
    }

    @Override
    public List<CostCentreBreakUpData> getCostCentreBreakUpData(String contextID) {
        List<CostCentreBreakUpData> costCentreBreakUpDataList = new ArrayList<>();
        List<CostCentreBreakUpDataVO> costCentreBreakUpDataVOS = uiLayoutDAO.getCostCentreBreakUpData(contextID);
        for(CostCentreBreakUpDataVO costCentreBreakUpDataVO : costCentreBreakUpDataVOS){
            CostCentreBreakUpData costCentreBreakUpData = CostCentreBreakUpData.fromVO(costCentreBreakUpDataVO);
            costCentreBreakUpDataList.add(costCentreBreakUpData);
        }
        return costCentreBreakUpDataList;
    }

    @Override
    public List<JobFamilyBreakUpData> getJobFamilyBreakUpData(String contextID) {
        List<JobFamilyBreakUpData> jobFamilyBreakUpDataList = new ArrayList<>();
        List<JobFamilyBreakUpDataVO> jobFamilyBreakUpDataVOS = uiLayoutDAO.getJobFamilyBreakUpData(contextID);
        for(JobFamilyBreakUpDataVO jobFamilyBreakUpDataVO : jobFamilyBreakUpDataVOS){
            JobFamilyBreakUpData jobFamilyBreakUpData = JobFamilyBreakUpData.fromVO(jobFamilyBreakUpDataVO);
            jobFamilyBreakUpDataList.add(jobFamilyBreakUpData);
        }
        return jobFamilyBreakUpDataList;
    }

    @Override
    public String getReportingSectionID() {
        return uiLayoutDAO.getReportingSectionID();
    }

    //    @Override
//    public List<LoadParameters> getLoadParameters(String callFor, String fkId, PersonGroup group) {
//        Set<String> fkIdSet = new HashSet<>();
//        fkIdSet.add(fkId);
////        List<LoadParameters> result = uiLayoutDAO.getLoadParameters(callFor,fkIdSet,group).get(fkId);
//        return result;
//    }
//
//    @Override
//    public String getSectionMasterCode(String targetSectionInstance) {
//        if(StringUtil.isDefined(targetSectionInstance)){
//            return uiLayoutDAO.getSectionMasterCode(targetSectionInstance);
//        }
//        return null;
//    }
//
//    @Override
//    public SectionControlVO getControlData(String controlId) {
//        return uiLayoutDAO.getControlData(controlId);
//    }
//
//    @Override
//    public Map<Integer, SectionLayoutMapper> getSectionLayoutMapper(String mtPE, String uiDispTypeCodeFkId, String tabSeqNumPosInt) {
//        return uiLayoutDAO.getSectionLayoutMapper(mtPE, uiDispTypeCodeFkId, tabSeqNumPosInt);
//    }
//
    @Override
    public String getScreenInstanceFromSection(String sectionId) {
        return uiLayoutDAO.getScreenInstanceFromSection(sectionId);
    }

    @Override
    public Map<String, Map<String, ResolvedColumnCondition>> getColumnConditions(String personId, Set<String> amRowPkIds) {
        return uiLayoutDAO.getColumnConditions(personId, amRowPkIds);
    }

    @Override
    public String getScreenId(String screenMasterCode) {
        return uiLayoutDAO.getScreenId(screenMasterCode);
    }
}
