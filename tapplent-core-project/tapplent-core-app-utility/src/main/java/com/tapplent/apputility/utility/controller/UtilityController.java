package com.tapplent.apputility.utility.controller;

/**
 * Created by Shubham Patodi on 01/08/16.
 */

import com.tapplent.apputility.data.service.DataRequestService;
import com.tapplent.apputility.layout.controller.LayoutController;
import com.tapplent.apputility.utility.service.UtilityService;
import com.tapplent.apputility.utility.structure.LogErrorRequest;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * All the common controllers used for internal purposes of Tapplent
 */
@Controller
@RequestMapping("tapp/utility/v1")
public class UtilityController {
    private static final Logger LOG = LogFactory.getLogger(LayoutController.class);
    private UtilityService utilityService;
    @RequestMapping(value="errorLog/t/{tenantId}/u/{personId}/e/", method= RequestMethod.POST)
    @ResponseBody
    void logTapplentError(@RequestHeader("access_token") String token,
                          @PathVariable("tenantId") String tenantId,
                          @PathVariable("personId") String userName,
                          @RequestParam("personName") String personName,
                          @RequestParam("timeZone") String timeZone,
                          @RequestBody LogErrorRequest errorRequest
                          ){
        utilityService.insertError(errorRequest);
    }

    public UtilityService getUtilityService() {
        return utilityService;
    }

    public void setUtilityService(UtilityService utilityService) {
        this.utilityService = utilityService;
    }
}
