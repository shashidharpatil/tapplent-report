package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.tapplent.platformutility.layout.valueObject.ActionStandAlonePropertiesVO;

public class ActionStandAloneProperties {
	private String actionStandalonePropertyId;
	private String actionStandaloneTxnId;
	private String rule;
	private String property;
	private String propertyValue;
	public String getActionStandalonePropertyId() {
		return actionStandalonePropertyId;
	}
	public void setActionStandalonePropertyId(String actionStandalonePropertyId) {
		this.actionStandalonePropertyId = actionStandalonePropertyId;
	}
	public String getActionStandaloneTxnId() {
		return actionStandaloneTxnId;
	}
	public void setActionStandaloneTxnId(String actionStandaloneTxnId) {
		this.actionStandaloneTxnId = actionStandaloneTxnId;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
	public static ActionStandAloneProperties fromVO(ActionStandAlonePropertiesVO actionStandAlonePropertiesVO){
		ActionStandAloneProperties actionStandAloneProperties = new ActionStandAloneProperties();
		BeanUtils.copyProperties(actionStandAlonePropertiesVO, actionStandAloneProperties);
		return actionStandAloneProperties;
	}
}
