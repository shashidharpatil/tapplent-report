package com.tapplent.apputility.thirdparty.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.ParseException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.transaction.annotation.Transactional;
import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.tapplent.apputility.data.service.ServerRuleManager;
import com.tapplent.apputility.data.structure.PEData;
import com.tapplent.apputility.data.structure.RequestData;
import com.tapplent.apputility.data.structure.UnitRequestData;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.thirdPartyApp.gmail.Encoder;
import com.tapplent.platformutility.thirdPartyApp.gmail.EventAttchment;
import com.tapplent.platformutility.thirdPartyApp.gmail.EverNoteDAO;
import com.tapplent.platformutility.thirdPartyApp.gmail.Meeting;
import com.tapplent.platformutility.thirdPartyApp.gmail.MeetingIntegration;
import com.tapplent.platformutility.thirdPartyApp.gmail.MeetingInvitee;
import com.tapplent.platformutility.thirdPartyApp.gmail.SingleNote;
import com.tapplent.platformutility.thirdPartyApp.gmail.ThirdPartyUtil;

public class GoogleCalenderServiceImpl implements GoogleCalenderService{
	 private EverNoteDAO evernoteDAO;
	 private ServerRuleManager serverRuleManager;
	
	public EverNoteDAO getEvernoteDAO() {
		return evernoteDAO;
	}
	public void setEvernoteDAO(EverNoteDAO evernoteDAO) {
		this.evernoteDAO = evernoteDAO;
	}
	public ServerRuleManager getServerRuleManager() {
		return serverRuleManager;
	}
	public void setServerRuleManager(ServerRuleManager serverRuleManager) {
		this.serverRuleManager = serverRuleManager;
	}
	
	//This function will create the events in the specified calendar
	private String createMeeting(String tapplentmeetingId,String accessToken,String personPkId) throws Exception{
		
		StringBuilder url = new StringBuilder("https://www.googleapis.com/calendar/v3/calendars/primary/events?maxAttendees=20&sendNotifications=false&supportsAttachments=true");
		HttpPost post = new HttpPost(url.toString());
		String mtPE = "Meeting";
		EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
		Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
		 Meeting meeting = evernoteDAO.getMeeting(tapplentmeetingId,attributeMetaMap,personPkId);
		 Map<String,String> personInfo = evernoteDAO.getPersonInfo(personPkId);
		 String formattedAddress = getTheAddressFromCoordinates(meeting.getGeoLoclatlng());
		 String meetingLoc = formattedAddress+" "+"("+meeting.getLocationTxt()+")";
		 String meetingTitle =meeting.getMeetingTitleG11NBigTxt();
		 String meetingDesc ="";
		 
		 if(meeting.getMeetingDescG11NBigTxt() != null && !meeting.getMeetingDescG11NBigTxt().isEmpty()){
			 meetingDesc = meeting.getMeetingDescG11NBigTxt();
			 if(!meetingDesc.contains("#tapplent")){
				 meetingDesc = meetingDesc+"\n"+"#tapplent";
			 }
		 }else{
			 meetingDesc = "#tapplent";
		 }
		 
		 
		 SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		 String meetingStartDateTime =  fmt.format(meeting.getMeetingStartDatetime()).toString();
		 String meetingEndDateTime =  fmt.format(meeting.getMeetingEndDatetime()).toString();
		 
		 //Retrieve all the attachments to the meeting
		 List<EventAttchment> attachments =evernoteDAO.getMeetingAttachments(tapplentmeetingId);
		 meeting.setAttachmentIds(attachments);
		JSONArray googleAttachments = new JSONArray();
		 for(EventAttchment attachment:attachments){
			 String attachmentId = attachment.getArtefactAttach();
			 String attachmentName = attachment.getAttachmentNameTxt();
			 Map<String,Object> attachmentMap = ThirdPartyUtil.getAttchment(attachmentId);
			 String mimeType = attachmentMap.get("ContentType").toString();
			 InputStream is = (InputStream) attachmentMap.get("InputStream");
			 Map<String,String> fileInfo = uploadToDrive(is,mimeType,accessToken,attachmentName);
			 
			 JSONObject gogleattachment = new JSONObject ();
			 gogleattachment.put("fileUrl", fileInfo.get("fileUrl"));
			 gogleattachment.put("mimeType", fileInfo.get("mimeType"));
			 gogleattachment.put("title",fileInfo.get("title"));
			 googleAttachments.put((JSONObject)gogleattachment);
		 }
		 //Retrieve all the invitee to the meeting
		 JSONArray attendees = new JSONArray();
		 List<MeetingInvitee> invitees =evernoteDAO.getMeetingInvitee(tapplentmeetingId);
		 if(invitees !=null && !invitees.isEmpty()){
			 for(MeetingInvitee invitee:invitees){
				
					 JSONObject meetingInvitee = new JSONObject ();
					 String personId = invitee.getInvitedPersonFkId();
					 Map<String,String> personEmailInfo = evernoteDAO.getPersonInfo(personId);
					 String personEmail = personEmailInfo.get("workEmail");
					 if(personEmail!= null && !personEmail.isEmpty()){
						 meetingInvitee.put("email", personEmail);
						 attendees.put((JSONObject)meetingInvitee); 
					 }
					 
				 
			 }
		 }	 
		post.addHeader("Content-Type", "application/json");
		post.addHeader("Authorization", "Bearer "+accessToken);
		JSONObject jsonRequestBody = new JSONObject();
		JSONObject endDateObject = new JSONObject();
		endDateObject.put("dateTime",meetingEndDateTime);
		jsonRequestBody.put("end", endDateObject);
		JSONObject startDateObject = new JSONObject();
		startDateObject.put("dateTime",meetingStartDateTime);
		jsonRequestBody.put("start", startDateObject);
		if(meetingDesc !=null && !meetingDesc.isEmpty()){
			if(!meetingDesc.contains("#tapplent")){
				jsonRequestBody.put("description", meetingDesc+"\n"+"#tapplent");
			}else{
				jsonRequestBody.put("description", meetingDesc);
			}
			
		}
		jsonRequestBody.put("location",meetingLoc);
		if(meeting.isPrivate())
		jsonRequestBody.put("visibility","private");
		jsonRequestBody.put("summary", meetingTitle);
		jsonRequestBody.put("attachments",googleAttachments);
		jsonRequestBody.put("attendees",attendees);
		jsonRequestBody.put("visibility","public");
		StringEntity entity = new StringEntity(jsonRequestBody.toString());
		post.setEntity(entity);
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(post);
	    int status_code = response.getStatusLine().getStatusCode();
	    JSONObject res = new JSONObject(EntityUtils.toString(response.getEntity()));
	   String meetingId = res.getString("id");
	    return meetingId;
		
	}


	//This function will update the specific event 
	private void updateGoogleMeeting(Meeting tapplentMeeting,String googleMtgId,String accessToken,String personId) throws Exception{
		deleteTheGoogleMtg(googleMtgId, accessToken,tapplentMeeting.getMeetingPkId());
		String newGoogleMtgId = createMeeting(tapplentMeeting.getMeetingPkId(), accessToken, personId);
		//Now update the note integration table with the newly created evernoteId
				//First get the lastmodifiedDateTime of the integrated note from the noteIntegration table
				Map<String, String> intgInfo = evernoteDAO.getIntegrationInfo(googleMtgId, "T_PFM_TAP_MTG_INTGN",tapplentMeeting.getMeetingPkId());
				PEData child2PEData = new PEData();
				child2PEData.setMtPE("MeetingIntegration");
				child2PEData.setActionCode("ADD_EDIT");
				Map<String, Object> columnNameToValueMap = new LinkedHashMap<String, Object>();
				columnNameToValueMap.put("MeetingIntegration.PrimaryKeyID",intgInfo.get("IntgPkId"));
				columnNameToValueMap.put("MeetingIntegration.IntegrationAppReferenceID",newGoogleMtgId);
				columnNameToValueMap.put("MeetingIntegration.LastModifiedDateTime",intgInfo.get("LastModifiedTime"));

				child2PEData.setData(columnNameToValueMap);
				UnitRequestData finalRequestData = new UnitRequestData();
				finalRequestData.setRootPEData(child2PEData);
				List<UnitRequestData> requestDataList1 = new ArrayList<>();
				requestDataList1.add(finalRequestData);
				RequestData request = new RequestData();
				request.setActionCodeFkId("ADD_EDIT");
				request.setRequestDataList(requestDataList1);
				try {

					serverRuleManager.applyServerRulesOnRequest(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
	}
	//This function will delete the specific event
	private void deleteTheGoogleMtg(String googleEventId,String accessToken,String tapplentMtgId) throws JSONException, ClientProtocolException, IOException{
		
		StringBuilder url = new StringBuilder("https://www.googleapis.com/calendar/v3/calendars/primary/events/");
		url.append(googleEventId);
		HttpDelete delete = new HttpDelete(url.toString());
		delete.addHeader("Content-Type", "application/json");
		delete.addHeader("Authorization", "Bearer "+accessToken);
		delete.addHeader("sendNotifications", "true");
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(delete);
	    int status_code = response.getStatusLine().getStatusCode();	
	    Map<String, String> intgInfo = evernoteDAO.getIntegrationInfo(googleEventId, "T_PFM_TAP_MTG_INTGN",tapplentMtgId);
	    PEData child2PEData = new PEData();
		child2PEData.setMtPE("MeetingIntegration");
		child2PEData.setActionCode("ADD_EDIT");
		Map<String, Object> columnNameToValueMap = new LinkedHashMap<String, Object>();
		columnNameToValueMap.put("MeetingIntegration.PrimaryKeyID",intgInfo.get("IntgPkId"));
		columnNameToValueMap.put("MeetingIntegration.Deleted",true);
		columnNameToValueMap.put("MeetingIntegration.LastModifiedDateTime",intgInfo.get("LastModifiedTime"));

		child2PEData.setData(columnNameToValueMap);
		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(child2PEData);
		List<UnitRequestData> requestDataList1 = new ArrayList<>();
		requestDataList1.add(finalRequestData);
		RequestData request = new RequestData();
		request.setActionCodeFkId("ADD_EDIT");
		request.setRequestDataList(requestDataList1);
		try {

			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	@Transactional
	public String calenderSynch(String personPkId) throws Exception{
		
		Map<String,Object> columnNameToValueMap = evernoteDAO.isSynchNeeded(personPkId,"T_PFM_TAP_MTG_SYNCH","MTG_SYNCH_PK_ID");
		String lastSynchToken = (String) columnNameToValueMap.get("lastSyncId");
		String integrationAppFkId = (String) columnNameToValueMap.get("integrationAppName");
		String meetingSyncPkId = (String) columnNameToValueMap.get("syncPkId");
		Timestamp lastModifiedDateTime1 = (Timestamp) columnNameToValueMap.get("lastModifiedDateTime");
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String lastModifiedDateTime =  fmt.format(lastModifiedDateTime1).toString();
		boolean isSynchNeeded = (boolean) columnNameToValueMap.get("isSynchNeeded");
		//Use this meeting Sync id to retrieve list of meeting Ids which have to be fetched
		List<String> meetingPkId = evernoteDAO.getEventPkId(meetingSyncPkId,"T_PFM_TAP_MTG_SYNCH_DTLS","MTG_FK_ID","MTG_SYNCH_FK_ID");
		Map<String,String> tokens = evernoteDAO.getToken(integrationAppFkId,personPkId);
		String accessToken = tokens.get("accessToken");
		String refreshToken = tokens.get("refreshToken");
		boolean flag = ThirdPartyUtil.isTokenValid(accessToken);
		if(flag==false){
			accessToken = ThirdPartyUtil.getAccessToken(refreshToken);
		}
		

			
			if(lastSynchToken != null && !lastSynchToken.isEmpty()){
					//// Something in the Calender account has changed, so search for meetongs...Perform Incremental Synch
					lastSynchToken = performIncrementalSync(lastSynchToken,accessToken,false,personPkId);
			    	  //Strore this token on to the database					
			        
		      }else{
			    
			    	  	//Perform the full Synch
		    	  lastSynchToken = performFullSynch(lastSynchToken,accessToken,false,personPkId);
		    	  
		    	  }
			   if(isSynchNeeded){
				   
				   for(String meetingId:meetingPkId){
					   		String googleMeetingId = evernoteDAO.getThirdPartyEventId(meetingId, "T_PFM_TAP_MTG_INTGN","GOOGLE_CALENDAR", "MTG_FK_ID");	
					   		if(googleMeetingId!=null && !googleMeetingId.isEmpty()){
					   			
						   			String mtPE = "Meeting";
						    		EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
						    		Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
						    		 Meeting tapplentMeeting = evernoteDAO.getMeeting(meetingId,attributeMetaMap,personPkId);
						    		 HttpGet get = new HttpGet("https://www.googleapis.com/calendar/v3/calendars/primary/events/"+googleMeetingId+"");
						    		 HttpClient client1 = HttpClientBuilder.create().build();
						    		 HttpResponse response1;
						    		 response1 = client1.execute(get);
						    		 JSONObject Googlemeeting = new JSONObject(EntityUtils.toString(response1.getEntity()));
						   			Timestamp tapplentMeetingLastModifiedTime = tapplentMeeting.getLastModifiedDateTime();
									Timestamp googleMeetingLastModifiedTime = ThirdPartyUtil.isoToSqlTimestamp(Googlemeeting.getString("updated"));
									if(tapplentMeetingLastModifiedTime.after(googleMeetingLastModifiedTime)){
										//Update the tapplent Task record onto the GoogleTask
										updateGoogleMeeting(tapplentMeeting,googleMeetingId,accessToken,personPkId);
									}else if(googleMeetingLastModifiedTime.after(tapplentMeetingLastModifiedTime)){
										//Update the google Task record onto Tapplent Task record
										updateTheTapplentMeeting(tapplentMeeting,Googlemeeting,personPkId);
	
									}else{
										//Everything is in Sync Don't Do anything...
									}
					   			
					   		}else {
							    String googleMtgId = createMeeting(meetingId, accessToken,personPkId);
						        PEData child1PEData = new PEData();
						    	child1PEData.setMtPE("MeetingIntegration");
								child1PEData.setActionCode("ADD_EDIT");
						    	 Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
						    	 columnNameToValueMap2.put("MeetingIntegration.IntegrationApp", "GOOGLE_CALENDAR");
						    	 columnNameToValueMap2.put("MeetingIntegration.MeetingID",meetingId);
						    	
						    	 columnNameToValueMap2.put("MeetingIntegration.IntegrationAppReferenceID",googleMtgId);
						    	 
						    	 child1PEData.setData(columnNameToValueMap2);
						    	 UnitRequestData finalRequestData = new UnitRequestData();
						 		finalRequestData.setRootPEData(child1PEData);
						 		List<UnitRequestData> requestDataList = new ArrayList<>();
						 		requestDataList.add(finalRequestData);
						 		RequestData request = new RequestData();
						 		request.setActionCodeFkId("ADD_EDIT");
						 		request.setRequestDataList(requestDataList);
						 		try {
						 			
						 			serverRuleManager.applyServerRulesOnRequest(request);
								} catch (Exception e) {
									e.printStackTrace();
								}
						 		//Update Synch details table
						 		PEData child2PEData = new PEData();
						 		child2PEData.setMtPE("MeetingSynchDetails");
						 		child2PEData.setActionCode("ADD_EDIT");
						 		
						    	 Map<String ,Object> columnNameToValueMap3 = new LinkedHashMap<String,Object>();
						    	 Map<String,Object> synchDetailsPkId = evernoteDAO.getSynchDetailsPkId("T_PFM_TAP_MTG_SYNCH_DTLS","MTG_FK_ID",meetingId,"MTG_SYNCH_DTLS_PK_ID");
						    	 SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
								 String lastModifiedDateTime2 =  fmt1.format((Timestamp)synchDetailsPkId.get("lastModifiedDateTime")).toString();
						    	 columnNameToValueMap3.put("MeetingSynchDetails.PrimaryKeyID",synchDetailsPkId.get("synchDetailsPkId"));
						    	 columnNameToValueMap3.put("MeetingSynchDetails.SynchComplete", true);
						    	 columnNameToValueMap3.put("MeetingSynchDetails.LastModifiedDateTime",lastModifiedDateTime2);
						    	 
						    	 child2PEData.setData(columnNameToValueMap3);
						    	 UnitRequestData finalRequestData1 = new UnitRequestData();
						 		finalRequestData1.setRootPEData(child2PEData);
						 		List<UnitRequestData> requestDataList1 = new ArrayList<>();
						 		requestDataList1.add(finalRequestData1);
						 		RequestData request1 = new RequestData();
						 		request1.setActionCodeFkId("DEFAULT");
						 		request1.setActionCodeFkId("ADD_EDIT");
						 		request1.setRequestDataList(requestDataList1);
						 		try {
						 			
						 			serverRuleManager.applyServerRulesOnRequest(request1);
								} catch (Exception e) {
									e.printStackTrace();
								}
				      }
				   }
				   
				 }
			   lastSynchToken = getTheSynchToken(accessToken,false,null);
			updateLastSyncToken(lastSynchToken,meetingSyncPkId,lastModifiedDateTime);
		      
		return null;
		
		      
	}
	private String getTheSynchToken(String accessToken,boolean flag,String nextPageToken) throws Exception  {
		StringBuilder url = new StringBuilder("https://www.googleapis.com/calendar/v3/calendars/primary/events?maxResults=10");
		if(flag){
			url.append("&pageToken=");
			url.append(nextPageToken);
		}
		HttpGet get = new HttpGet(url.toString());
		get.addHeader("Content-Type", "application/json");
		get.addHeader("Authorization", "Bearer "+accessToken);
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(get);
	    String synchToken = "";
	    JSONObject res = new JSONObject(EntityUtils.toString(response.getEntity()));
	    if(res.has("nextPageToken")){
	    	//recursive call should be made with nextPageToken
	    	String pageToken = res.getString("nextPageToken");
	    	synchToken = getTheSynchToken(accessToken,true,pageToken);
	    	
	    }else{
	    	
	    	synchToken =res.getString("nextSyncToken");
	    }
	     
		return synchToken;
	}
	private String performFullSynch(String nextPageToken,String accessToken,boolean flag,String personId) throws ClientProtocolException, IOException, org.apache.http.ParseException, JSONException, ParseException {
		StringBuilder url = new StringBuilder("https://www.googleapis.com/calendar/v3/calendars/primary/events?maxResults=10");
		if(flag){
			url.append("&pageToken=");
			url.append(nextPageToken);
		}
		HttpGet get = new HttpGet(url.toString());
		get.addHeader("Content-Type", "application/json");
		get.addHeader("Authorization", "Bearer "+accessToken);
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(get);
	    String firstSynchToken = "";
	    JSONObject res = new JSONObject(EntityUtils.toString(response.getEntity()));
	    if(res.has("nextPageToken")){
	    	//recursive call should be made with nextPageToken
	    	JSONArray eventsArray = res.getJSONArray("items");
	    	//retrieve all the events
	    	for(int i = 0; i<=eventsArray.length()-1;i++){
	    		JSONObject Googlemeeting = (JSONObject) eventsArray.get(i);
	    		String googleMeetingDesc = "";
	    		if(Googlemeeting.has("description")){
	    			googleMeetingDesc = Googlemeeting.getString("description");
	    		}
	    		if(googleMeetingDesc.contains("#tapplent")){
	    			insertMeetingIntoTapplent(Googlemeeting,personId);
	    		}
	    	}
	    	String pageToken = res.getString("nextPageToken");
	    	firstSynchToken = performFullSynch(pageToken,accessToken,true,personId);
	    	
	    }else{
	    	
	    	JSONArray eventsArray = res.getJSONArray("items");
	    	//retrieve all the events
	    	for(int i = 0; i<=eventsArray.length()-1;i++){
	    		JSONObject Googlemeeting = (JSONObject) eventsArray.get(i);
	    		String googleMeetingDesc = "";
	    		if(Googlemeeting.has("description")){
	    			googleMeetingDesc = Googlemeeting.getString("description");
	    		}
	    		if(googleMeetingDesc.contains("#tapplent")){
	    			insertMeetingIntoTapplent(Googlemeeting,personId);
	    		}
	    	 }
	    	firstSynchToken =res.getString("nextSyncToken");
	    }
	     
		return firstSynchToken;
	}
	    
	public String performIncrementalSync(String syncToken, String accessToken,boolean isNextPage,String personId) throws Exception {
		
		StringBuilder url = new StringBuilder("https://www.googleapis.com/calendar/v3/calendars/primary/events?maxResults=10&singleEvents=true&");
		if(isNextPage){
			url.append("pageToken=");
		}else{
			url.append("syncToken=");
		}
		url.append(syncToken);
		HttpGet get = new HttpGet(url.toString());
		get.addHeader("Content-Type", "application/json");
		get.addHeader("Authorization", "Bearer "+accessToken);
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(get);
	    JSONObject res = new JSONObject(EntityUtils.toString(response.getEntity()));
	    if(res.has("nextPageToken")){
	    	//recursive call should be made with nextPageToken
	    	String pageToken = res.getString("nextPageToken");
	    	
	    	//Retrive the all the events
	    	
	    	JSONArray eventsArray = res.getJSONArray("items");
	    	//retrieve all the events
	    	for(int i = 0; i<=eventsArray.length()-1;i++){
	    		JSONObject Googlemeeting = (JSONObject) eventsArray.get(i);
	    		String googlemeetingId = Googlemeeting.getString("id");
	    		String tapplentMeetingId = evernoteDAO.getEventFkIdFromIntegration(googlemeetingId,"T_PFM_TAP_MTG_INTGN","MTG_FK_ID");
	    		String mtPE = "Meeting";
	    		EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
	    		Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
	    		 Meeting tapplentMeeting = evernoteDAO.getMeeting(tapplentMeetingId,attributeMetaMap,personId);
	    		 String meetingDesc ="";
	    		 if(Googlemeeting.has("description")){
		    		 meetingDesc =  Googlemeeting.getString("description");
	    	    if(meetingDesc.contains("#tapplent")){
	    		if(tapplentMeetingId != null && !tapplentMeetingId.isEmpty()){
					
					if(Googlemeeting.getString("status").equals("cancelled")){
						deleteTheTapplentMeeting(tapplentMeetingId,personId);
						
					}else if(tapplentMeeting.isDeleted()){
						deleteTheGoogleMtg(googlemeetingId,accessToken,tapplentMeetingId);
		    			
		    		}else{
						
							//Retrieve the lastModifiedtime of meeting in tapplent and GoogleCalender
							Timestamp tapplentMeetingLastModifiedTime = tapplentMeeting.getLastModifiedDateTime();
							Timestamp googleMeetingLastModifiedTime = Timestamp.valueOf(Googlemeeting.getString("updated"));
							if(tapplentMeetingLastModifiedTime.after(googleMeetingLastModifiedTime)){
								//Update the tapplent Task record onto the GoogleTask
								updateGoogleMeeting(tapplentMeeting,googlemeetingId,accessToken,personId);
							}else if(googleMeetingLastModifiedTime.after(tapplentMeetingLastModifiedTime)){
								//Update the google Task record onto Tapplent Task record
							   updateTheTapplentMeeting(tapplentMeeting,Googlemeeting,personId);

							}else{
								//Everything is in Sync Don't Do anything...
							}
						}
						
					}else{
						
						insertMeetingIntoTapplent(Googlemeeting,personId);
					 }
	    		
					
				}
	    	}
	    }
	    	performIncrementalSync(pageToken,accessToken,true,personId);
	    	
	    	
	    }else{
	    	JSONArray eventsArray = res.getJSONArray("items");
	    	//retrieve all the events
	    	for(int i = 0; i<=eventsArray.length()-1;i++){
	    		JSONObject Googlemeeting = (JSONObject) eventsArray.get(i);
	    		String googlemeetingId = Googlemeeting.getString("id");
	    		String tapplentMeetingId = evernoteDAO.getEventFkIdFromIntegration(googlemeetingId,"T_PFM_TAP_MTG_INTGN","MTG_FK_ID");
	    		String mtPE = "Meeting";
	    		EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
	    		Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
	    		 Meeting tapplentMeeting = evernoteDAO.getMeeting(tapplentMeetingId,attributeMetaMap,personId);
	    	 if(Googlemeeting.has("description")){
	    		 String meetingDesc =  Googlemeeting.getString("description");
	 	    	if(meetingDesc.contains("#tapplent")){
	    		
	    		if(tapplentMeetingId != null && !tapplentMeetingId.isEmpty()){
					
					if(Googlemeeting.getString("status").equals("cancelled")){
						deleteTheTapplentMeeting(tapplentMeetingId,accessToken);
						
					}else if(tapplentMeeting.isDeleted()){
						deleteTheGoogleMtg(googlemeetingId,personId,tapplentMeetingId);
		    			
		    		}else{
						
							//Retrieve the lastModifiedtime of meeting in tapplent and GoogleCalender
							Timestamp tapplentMeetingLastModifiedTime = tapplentMeeting.getLastModifiedDateTime();
							Timestamp googleMeetingLastModifiedTime = ThirdPartyUtil.isoToSqlTimestamp(Googlemeeting.getString("updated"));
							if(tapplentMeetingLastModifiedTime.after(googleMeetingLastModifiedTime)){
								//Update the tapplent Task record onto the GoogleTask
								updateGoogleMeeting(tapplentMeeting,googlemeetingId,accessToken,personId);
							}else if(googleMeetingLastModifiedTime.after(tapplentMeetingLastModifiedTime)){
								//Update the google Task record onto Tapplent Task record
								updateTheTapplentMeeting(tapplentMeeting,Googlemeeting,personId);

							}else{
								//Everything is in Sync Don't Do anything...
							}
						}
						
					}else{
						
						insertMeetingIntoTapplent(Googlemeeting,personId);
					 }
	    	     }
					
				}
	    	}
	    	syncToken = res.getString("nextSyncToken");
	  
	    }
	    	return syncToken;
	    }
	    
		
  
	private void updateTheTapplentMeeting(Meeting tapplentMeeting, JSONObject googlemeeting,String personId) throws JSONException, MalformedURLException, IOException {
		//Retrieve the attachments and all the fields from the google meeting and update it onto tapplent meeting
		String googlemeetingId = googlemeeting.getString("id");
		String tapplentMeetingId = evernoteDAO.getEventFkIdFromIntegration(googlemeetingId,"T_PFM_TAP_MTG_INTGN","MTG_FK_ID");
		List<EventAttchment> attachments = evernoteDAO.getMeetingAttachments(tapplentMeetingId);
		if(attachments != null && !attachments.isEmpty()){
		 for(EventAttchment attachment :attachments){
			String attachmentId = attachment.getAttachmentPkId();
			PEData child1PEData = new PEData();
	    	child1PEData.setMtPE("MeetingAttachment");
	    	child1PEData.setActionCode("ADD_EDIT");
	    	 Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
	    	 columnNameToValueMap1.put("MeetingAttachment.PrimaryKeyID", attachmentId);
	    	 columnNameToValueMap1.put("MeetingAttachment.Deleted", true);
	    	 columnNameToValueMap1.put("MeetingAttachment.LastModifiedDateTime",attachment.getLastModifiedDatetime().toString());
	    	 child1PEData.setData(columnNameToValueMap1);
	    	 UnitRequestData finalRequestData = new UnitRequestData();
	 		finalRequestData.setRootPEData(child1PEData);
	 		List<UnitRequestData> requestDataList = new ArrayList<>();
	 		requestDataList.add(finalRequestData);
	 		RequestData request = new RequestData();
	 		request.setActionCodeFkId("ADD_EDIT");
	 		request.setRequestDataList(requestDataList);
	 		try {
	 			
	 			serverRuleManager.applyServerRulesOnRequest(request);
			} catch (Exception e) {
				e.printStackTrace();
			}
		  }
		}	 
		List<MeetingInvitee> invitees = evernoteDAO.getAllMeetingInvitee(tapplentMeetingId);
		if(invitees != null && !invitees.isEmpty()){
			
		  for(MeetingInvitee invitee :invitees){
			 String meetingInviteePkId = invitee.getMeetingInviteePkId();
			PEData child2PEData = new PEData();
	    	child2PEData.setMtPE("MeetingInvitee");
	    	child2PEData.setActionCode("ADD_EDIT");
	    	
	    	 Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
	    	 columnNameToValueMap1.put("MeetingInvitee.PrimaryKeyID", meetingInviteePkId);
	    	 columnNameToValueMap1.put("MeetingInvitee.Deleted", true);
	    	 columnNameToValueMap1.put("MeetingInvitee.LastModifiedDateTime", invitee.getLastModifiedDatetime().toString());
	    	 UnitRequestData finalRequestData = new UnitRequestData();
		 		finalRequestData.setRootPEData(child2PEData);
		 		List<UnitRequestData> requestDataList = new ArrayList<>();
		 		requestDataList.add(finalRequestData);
		 		RequestData request = new RequestData();
		 		
		 		request.setActionCodeFkId("ADD_EDIT");
		 		request.setRequestDataList(requestDataList);
		 		try {
		 			
		 			serverRuleManager.applyServerRulesOnRequest(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
	    	 
		  }
		}
	//Now update the tapplent meeting
		Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
		JSONObject meetingStart = googlemeeting.getJSONObject("start");
		JSONObject meetingEnd = googlemeeting.getJSONObject("end");
		columnNameToValueMap.put("Meeting.PrimaryKeyID",tapplentMeetingId);
		Map<String,String> personInfo = evernoteDAO.getPersonInfo(personId);
		if(googlemeeting.has("summary"))
			columnNameToValueMap.put("Meeting.MeetingTitle",Util.getG11nString(personInfo.get("locale"),googlemeeting.getString("summary")));
		if(googlemeeting.has("description"))
		columnNameToValueMap.put("Meeting.MeetingDescription",Util.getG11nString(personInfo.get("locale"),googlemeeting.getString("description"))); 
		SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		if(meetingStart.has("date")){
			columnNameToValueMap.put("Meeting.MeetingStartDateTime", fmt1.format(meetingStart.getString("date")).toString()); 
		}else if(meetingStart.has("dateTime")){
			
			Timestamp mtgStartTimestamp = ThirdPartyUtil.isoToSqlTimestamp(meetingStart.getString("dateTime"));
			String mtgStartTime = mtgStartTimestamp.toString();
			

			columnNameToValueMap.put("Meeting.MeetingStartDateTime",mtgStartTime);
		}
		if(meetingEnd.has("date")){
			columnNameToValueMap.put("Meeting.MeetingEndDateTime",fmt1.format(meetingEnd.getString("date")).toString()); 
		}else if(meetingEnd.has("dateTime")){
			
			Timestamp mtgEndTimestamp = ThirdPartyUtil.isoToSqlTimestamp(meetingEnd.getString("dateTime"));
			String mtgEndTime = mtgEndTimestamp.toString();

			columnNameToValueMap.put("Meeting.MeetingEndDateTime",mtgEndTime);
		}
		
           columnNameToValueMap.put("Meeting.Location", googlemeeting.getString("location"));
           columnNameToValueMap.put("Meeting.GeoLocation", getTheGeoLocation(googlemeeting.getString("location")));
           columnNameToValueMap.put("Meeting.LastModifiedDateTime",tapplentMeeting.getLastModifiedDateTime().toString());
           PEData child1PEData = new PEData();
	    	child1PEData.setMtPE("Meeting");
	    	child1PEData.setActionCode("ADD_EDIT");
	    	 child1PEData.setData(columnNameToValueMap);
	    	 UnitRequestData finalRequestData = new UnitRequestData();
	 		finalRequestData.setRootPEData(child1PEData);
	 		List<UnitRequestData> requestDataList = new ArrayList<>();
	 		requestDataList.add(finalRequestData);
	 		RequestData request = new RequestData();
	 		request.setActionCodeFkId("ADD_EDIT");
	 		request.setRequestDataList(requestDataList);
	 		try {
	 			
	 			serverRuleManager.applyServerRulesOnRequest(request);
			} catch (Exception e) {
				e.printStackTrace();
			}
	 		
         //Now Insert into  meeting attachment and meeting attendee

			if(googlemeeting.has("attachments")){
			    JSONArray attachmentsArray = googlemeeting.getJSONArray("attachments");
			    for(int i = 0; i<=attachmentsArray.length()-1;i++){
			    	JSONObject attachment = (JSONObject) attachmentsArray.get(i);
			    	PEData child1PEData1 = new PEData();
			    	child1PEData1.setActionCode("ADD_EDIT");
			    	child1PEData1.setMtPE("MeetingAttachment");
					Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
					String attachmentType =attachment.getString("mimeType");
					InputStream is = new URL(attachment.getString("fileUrl")).openStream();
					String attachmentArtifact = Util.uploadAttachmentToS3(attachmentType, is);
					columnNameToValueMap1.put("MeetingAttachment.MeetingID",tapplentMeetingId);
					columnNameToValueMap1.put("MeetingAttachment.AttachmentArtefactID",attachmentArtifact); 
					columnNameToValueMap1.put("MeetingAttachment.AttachmentName",attachment.getString("title"));
					if(attachmentType.equals("iamge/png") || attachmentType.equals("image/jpeg"))
					columnNameToValueMap1.put("MeetingAttachment.AttachmentTypeCode","IMAGE");
					if(attachmentType.equals("audio/mpeg")||attachmentType.equals("audio/mp3"))
						columnNameToValueMap1.put("MeetingAttachment.AttachmentTypeCode","AUDIO");
					if(attachmentType.equals("video/mp4"))
						columnNameToValueMap1.put("MeetingAttachment.AttachmentTypeCode","VIDEO");
					child1PEData1.setData(columnNameToValueMap1);
					 UnitRequestData finalRequestData1 = new UnitRequestData();
					 finalRequestData1.setRootPEData(child1PEData);
				 		List<UnitRequestData> requestDataList1 = new ArrayList<>();
				 		requestDataList.add(finalRequestData1);
				 		RequestData request1 = new RequestData();
				 		request1.setActionCodeFkId("ADD_EDIT");
				 		request1.setRequestDataList(requestDataList1);
				 		try {
				 			
				 			serverRuleManager.applyServerRulesOnRequest(request1);
						} catch (Exception e) {
							e.printStackTrace();
						}
			    }
			 }
			
			              if(googlemeeting.has("attendees")){
			            	  
			      		    JSONArray attendeesArray = googlemeeting.getJSONArray("attendees");
			      		    for(int i = 0; i<=attendeesArray.length()-1;i++){
			      		    	JSONObject attendee = (JSONObject) attendeesArray.get(i);
			      		    	PEData child1PEData2 = new PEData();
			      		    	child1PEData2.setMtPE("MeetingInvitee");
			      		    	child1PEData2.setActionCode("ADD_EDIT");
			      				Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
			      				String attendeeEmail = attendee.getString("email");
			      				//Find this email in the PERSON table If present then only it should be inserted
			      				 String InvitedPersonId = evernoteDAO.getAttendeeInfo(attendeeEmail);
			      				 if(InvitedPersonId !=null && InvitedPersonId.isEmpty()){
			      					columnNameToValueMap1.put("MeetingInvitee.InvitedPersonID",personId);  
			      				 }
			      				columnNameToValueMap1.put("MeetingInvitee.AttendeeStatusCode","YES");
			      				columnNameToValueMap1.put("MeetingInvitee.MeetingID", tapplentMeetingId);
			      				
			      				child1PEData2.setData(columnNameToValueMap1);
			      				UnitRequestData finalRequestData1 = new UnitRequestData();
								 finalRequestData1.setRootPEData(child1PEData);
							 		List<UnitRequestData> requestDataList1 = new ArrayList<>();
							 		requestDataList.add(finalRequestData1);
							 		RequestData request1 = new RequestData();
							 		request1.setActionCodeFkId("ADD_EDIT");
							 		request1.setRequestDataList(requestDataList1);
							 		try {
							 			
							 			serverRuleManager.applyServerRulesOnRequest(request1);
									} catch (Exception e) {
										e.printStackTrace();
									}
			      				
			      		    }
			      		 }
           
		
	}
	private void deleteTheTapplentMeeting(String tapplentMeetingId,String personId) {
		// I have to delete the tapplent Meeting table and its child tables where tapplentMeetingId = ?
		String mtPE = "Meeting";
		EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
		Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
		 Meeting tapplentMeeting = evernoteDAO.getMeeting(tapplentMeetingId,attributeMetaMap,personId);
	  
		//First delete all the child table entries  
	List<EventAttchment> attachments = evernoteDAO.getMeetingAttachments(tapplentMeetingId);
		if(attachments != null && !attachments.isEmpty()){
		 for(EventAttchment attachment :attachments){
			String attachmentId = attachment.getAttachmentPkId();
			PEData child1PEData = new PEData();
	    	child1PEData.setMtPE("MeetingAttachment");
	    	child1PEData.setActionCode("Add_EDIT");
	    	 Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
	    	 columnNameToValueMap1.put("MeetingAttachment.PrimaryKeyID", attachmentId);
	    	 columnNameToValueMap1.put("MeetingAttachment.Deleted", true);
	    	 columnNameToValueMap1.put("MeetingAttachment.LastModifiedDateTime",attachment.getLastModifiedDatetime().toString());
	    	 child1PEData.setData(columnNameToValueMap1);
	    	 UnitRequestData finalRequestData = new UnitRequestData();
	 		finalRequestData.setRootPEData(child1PEData);
	 		List<UnitRequestData> requestDataList = new ArrayList<>();
	 		requestDataList.add(finalRequestData);
	 		RequestData request = new RequestData();
	 		request.setPreviousActionCodeFkId("ADD_EDIT");
	 		request.setRequestDataList(requestDataList);
	 		try {
	 			
	 			serverRuleManager.applyServerRulesOnRequest(request);
			} catch (Exception e) {
				e.printStackTrace();
			}
		  }
		}	 
		List<MeetingInvitee> invitees = evernoteDAO.getAllMeetingInvitee(tapplentMeetingId);
		if(invitees != null && !invitees.isEmpty()){
			
		  for(MeetingInvitee invitee :invitees){
			 String meetingInviteePkId = invitee.getMeetingInviteePkId();
			PEData child2PEData = new PEData();
	    	child2PEData.setMtPE("MeetingInvitee");
	    	child2PEData.setActionCode("ADD_EDIT");	    	
	    	 Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
	    	 columnNameToValueMap1.put("MeetingInvitee.PrimaryKeyID", meetingInviteePkId);
	    	 columnNameToValueMap1.put("MeetingInvitee.Deleted", true);
	    	 columnNameToValueMap1.put("MeetingInvitee.LastModifiedDateTime", invitee.getLastModifiedDatetime().toString());
	    	 UnitRequestData finalRequestData = new UnitRequestData();
		 		finalRequestData.setRootPEData(child2PEData);
		 		List<UnitRequestData> requestDataList = new ArrayList<>();
		 		requestDataList.add(finalRequestData);
		 		RequestData request = new RequestData();
		 		request.setPreviousActionCodeFkId("ADD_EDIT");
		 		request.setRequestDataList(requestDataList);
		 		try {
		 			
		 			serverRuleManager.applyServerRulesOnRequest(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
	    	 
		  }
		}
		//MeetingIntegration.PrimaryKeyID
		MeetingIntegration mtgIntg = evernoteDAO.getIntegrationPkId("T_PFM_TAP_MTG_INTGN","MTG_FK_ID","MTG_INTGN_PK_ID",tapplentMeetingId);
		
		if(mtgIntg != null && !mtgIntg.equals("")){
			String mtgIntgPkId =  mtgIntg.getMtgIntgnPkId();
			PEData child2PEData = new PEData();
	    	child2PEData.setMtPE("MeetingIntegration");
	    	child2PEData.setActionCode("ADD_EDIT");
	    	 Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
	    	 columnNameToValueMap1.put("MeetingIntegration.MeetingID", mtgIntgPkId);
	    	 columnNameToValueMap1.put("MeetingIntegration.Deleted", true);
	    	 columnNameToValueMap1.put("MeetingIntegration.LastModifiedDateTime",mtgIntg.getLastModifiedDatetime().toString());
	    	 UnitRequestData finalRequestData = new UnitRequestData();
	 		finalRequestData.setRootPEData(child2PEData);
	 		List<UnitRequestData> requestDataList = new ArrayList<>();
	 		requestDataList.add(finalRequestData);
	 		RequestData request = new RequestData();
	 		request.setActionCodeFkId("ADD_EDIT");
	 		request.setRequestDataList(requestDataList);
	 		try {
	 			serverRuleManager.applyServerRulesOnRequest(request);
	 		} catch (Exception e) {
	 			e.printStackTrace();
	 		}	
		 }
		
		//Delete the parent table Entries...
		Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
		columnNameToValueMap.put("Meeting.PrimaryKeyID",tapplentMeetingId);
		columnNameToValueMap.put("Meeting.Deleted", true);
		columnNameToValueMap.put("Meeting.LastModifiedDateTime",tapplentMeeting.getLastModifiedDateTime().toString());

		PEData rootPEData = new PEData();
		rootPEData.setMtPE("Meeting");
		rootPEData.setActionCode("ADD_EDIT");
		rootPEData.setData(columnNameToValueMap);
		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(rootPEData);
		List<UnitRequestData> requestDataList = new ArrayList<>();
		requestDataList.add(finalRequestData);
		RequestData request = new RequestData();
		request.setActionCodeFkId("ADD_EDIT");
		request.setRequestDataList(requestDataList);
		try {
			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		
	}
private void insertMeetingIntoTapplent(JSONObject googlemeeting,String personId) throws JSONException, MalformedURLException, IOException {
	    	
		//Insert the EverNote into Tapplent...So that u have to insert into Note,NoteAttachment and Note Integration
		Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
		JSONObject meetingStart = googlemeeting.getJSONObject("start");
		JSONObject meetingEnd = googlemeeting.getJSONObject("end");
		Map<String,String> personInfo = evernoteDAO.getPersonInfo(personId);
		
		if(googlemeeting.has("summary"))
			columnNameToValueMap.put("Meeting.MeetingTitle",Util.getG11nString(personInfo.get("locale"),googlemeeting.getString("summary")));
			columnNameToValueMap.put("Meeting.MeetingDescription",Util.getG11nString(personInfo.get("locale"),googlemeeting.getString("description"))); 
		if(meetingStart.has("date")){
			columnNameToValueMap.put("Meeting.MeetingStartDateTime",meetingStart.getString("date")); 
		}else if(meetingStart.has("dateTime")){			
			Timestamp mtgStartTimestamp = ThirdPartyUtil.isoToSqlTimestamp(meetingStart.getString("dateTime"));
			String mtgStartTime = mtgStartTimestamp.toString();

			columnNameToValueMap.put("Meeting.MeetingStartDateTime",mtgStartTime);
		}
		if(meetingEnd.has("date")){
			columnNameToValueMap.put("Meeting.MeetingEndDateTime",meetingEnd.getString("date")); 
		}else if(meetingEnd.has("dateTime")){
//			SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Timestamp mtgEndTimestamp = ThirdPartyUtil.isoToSqlTimestamp(meetingEnd.getString("dateTime"));
			String mtgEndTime = mtgEndTimestamp.toString();
//			String lastModifiedDateTime2 =   fmt1.format(meetingEnd.getString("dateTime")).toString();
			columnNameToValueMap.put("Meeting.MeetingEndDateTime",mtgEndTime);
		}
           columnNameToValueMap.put("Meeting.Location", googlemeeting.getString("location"));
             String geoLocation = getTheGeoLocation(googlemeeting.getString("location"));
             columnNameToValueMap.put("Meeting.GeoLocation", geoLocation);
		
		PEData rootPEData = new PEData();
		rootPEData.setActionCode("ADD_EDIT");
		rootPEData.setMtPE("Meeting");
		rootPEData.setData(columnNameToValueMap);
		List<PEData> childrenPEData = new ArrayList<PEData>();
		if(googlemeeting.has("attachments")){
		    JSONArray attachmentsArray = googlemeeting.getJSONArray("attachments");
		    for(int i = 0; i<=attachmentsArray.length()-1;i++){
		    	JSONObject attachment = (JSONObject) attachmentsArray.get(i);
		    	PEData child1PEData = new PEData();
		    	child1PEData.setActionCode("ADD_EDIT");
		    	child1PEData.setMtPE("MeetingAttachment");
				Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
				String attachmentType =attachment.getString("mimeType");
				InputStream is = new URL(attachment.getString("fileUrl")).openStream();
				String attachmentArtifact = Util.uploadAttachmentToS3(attachmentType, is);
				
				columnNameToValueMap1.put("MeetingAttachment.AttachmentArtefactID",attachmentArtifact); 
				columnNameToValueMap1.put("MeetingAttachment.AttachmentName",attachment.getString("title"));
				if(attachmentType.equals("iamge/png") || attachmentType.equals("image/jpeg"))
					columnNameToValueMap1.put("MeetingAttachment.AttachmentTypeCode","IMAGE");
					if(attachmentType.equals("audio/mpeg")||attachmentType.equals("audio/mp3"))
						columnNameToValueMap1.put("MeetingAttachment.AttachmentTypeCode","AUDIO");
					if(attachmentType.equals("video/mp4"))
						columnNameToValueMap1.put("MeetingAttachment.AttachmentTypeCode","VIDEO");
					else
						columnNameToValueMap1.put("MeetingAttachment.AttachmentTypeCode","DOCUMENT");
				child1PEData.setData(columnNameToValueMap1);
				childrenPEData.add(child1PEData);
		    }
		 }
		
		              if(googlemeeting.has("attendees")){
		            	  
		      		    JSONArray attendeesArray = googlemeeting.getJSONArray("attendees");
		      		    for(int i = 0; i<=attendeesArray.length()-1;i++){
		      		    	JSONObject attendee = (JSONObject) attendeesArray.get(i);
		      		    	String attendeeEmail = attendee.getString("email");
		      				//Find this email in the PERSON table If present then only it should be inserted
		      				 String invitedPersonId = evernoteDAO.getAttendeeInfo(attendeeEmail);
		      				 if(invitedPersonId !=null && invitedPersonId.isEmpty()){
		      					 
		      					Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
		      					columnNameToValueMap1.put("MeetingInvitee.InvitedPersonID",personId);  
			      		    	PEData child1PEData = new PEData();
			      		    	child1PEData.setMtPE("MeetingInvitee");
			      		    	child1PEData.setActionCode("ADD_EDIT");
			      				
			      				
			      				columnNameToValueMap1.put("MeetingInvitee.AttendeeStatusCode","YES");
			      				child1PEData.setData(columnNameToValueMap1);
			      				childrenPEData.add(child1PEData);
		      				 }
		      		    }
		      		 }
		PEData child2PEData = new PEData();
		child2PEData.setActionCode("ADD_EDIT");
		child2PEData.setMtPE("MeetingIntegration");
		Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
		columnNameToValueMap2.put("MeetingIntegration.IntegrationApp","GOOGLE_CALENDAR"); 
		columnNameToValueMap2.put("MeetingIntegration.IntegrationAppReferenceID",googlemeeting.getString("id"));
		child2PEData.setData(columnNameToValueMap2);
		childrenPEData.add(child2PEData);

		rootPEData.setChildrenPEData(childrenPEData);

		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(rootPEData);
 		List<UnitRequestData> requestDataList = new ArrayList<>();
 		requestDataList.add(finalRequestData);
 		RequestData request = new RequestData();
 		request.setActionCodeFkId("ADD_EDIT");
 		request.setRequestDataList(requestDataList);
 		try {
 			
 			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}


	}
	private String getTheGeoLocation(String address) throws ClientProtocolException, IOException, JSONException {
		String requestAddress = "";
	if(address.contains("(")){
		String addressArray[] = address.split("\\(");
		String addressLoc = addressArray[0];
		requestAddress = addressLoc.replace(" ", "+");
	}else{
		requestAddress = address.replace(" ", "+");
	}
	 
	StringBuilder url = new StringBuilder("https://maps.googleapis.com/maps/api/geocode/json?address=");
	url.append(requestAddress).append("&key=");
	url.append("AIzaSyCfj3tDEn63cAFo9hBjCQTSA7Kj5GlsQxQ");
	HttpGet get = new HttpGet(url.toString());
	get.addHeader("Content-Type", "application/json");
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(get);
    JSONObject responseObject = new JSONObject(EntityUtils.toString(response.getEntity()));
    JSONArray results = responseObject.getJSONArray("results");
    
    	JSONObject entry = results.getJSONObject(0);
    JSONObject  geometryObject = entry.getJSONObject("geometry");
    JSONObject locationObject = geometryObject.getJSONObject("location");
    String latitude = locationObject.getString("lat");
    String longitude = locationObject.getString("lng");
    String geoLocation = latitude+","+longitude;
  
	return geoLocation;
}
	public String getTheAddressFromCoordinates(String geoCode) throws Exception{
		StringBuilder url = new StringBuilder("https://maps.googleapis.com/maps/api/geocode/json?latlng=");
		url.append(geoCode).append("&key=");
		url.append("AIzaSyCfj3tDEn63cAFo9hBjCQTSA7Kj5GlsQxQ");
		HttpGet get = new HttpGet(url.toString());
		get.addHeader("Content-Type", "application/json");
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(get);
	    JSONObject responseObject = new JSONObject(EntityUtils.toString(response.getEntity()));
	    JSONArray results = responseObject.getJSONArray("results");
	    
	    	JSONObject entry = results.getJSONObject(0);
	    	String formatted_address = entry.getString("formatted_address");
	   
		return formatted_address;
	}
	
	private void updateLastSyncToken(String lastSynchToken,String meetingSyncPkId,String lastModifiedTime){
		Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
		columnNameToValueMap.put("MeetingSynch.PrimaryKeyID",meetingSyncPkId);
		columnNameToValueMap.put("MeetingSynch.SynchNeeded",false);
		columnNameToValueMap.put("MeetingSynch.LastModifiedDateTime",lastModifiedTime);
		columnNameToValueMap.put("MeetingSynch.LastSynchExternalAppReference", lastSynchToken);
		//columnNameToValueMap.put("MeetingSynch.LastSynchDateTime","");
		
		PEData rootPEData = new PEData();
		rootPEData.setMtPE("MeetingSynch");
		rootPEData.setActionCode("ADD_EDIT");
		rootPEData.setData(columnNameToValueMap);
		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(rootPEData);
		List<UnitRequestData> requestDataList = new ArrayList<>();
		requestDataList.add(finalRequestData);
		RequestData request = new RequestData();
		request.setActionCodeFkId("ADD_EDIT");
		request.setRequestDataList(requestDataList);
		try {
			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
	}
private Map<String,String> uploadToDrive(InputStream is, String mimeType,String accesstoken,String attachmentName) throws IOException, ParseException, JSONException, org.json.JSONException {
		
		StringBuilder url = new StringBuilder("https://www.googleapis.com/drive/v2/files");
		String title = "";
		if(attachmentName.contains("Audio")){
			title = "tapplent.mp3";
		}else if(attachmentName.contains("Video")){
			title = "tapplent.mp4";
		}else if (attachmentName.contains("Image")){
			title = "tapplent.jpeg";
			
		}else if(attachmentName.contains("Document")){
			title = "tapplent.pdf";
		}
		HttpPost post = new HttpPost(url.toString());
		post.addHeader("Authorization", "Bearer "+accesstoken);
		post.addHeader("Content-Type", "application/json");
		JSONObject request = new JSONObject();
		request.put("title", title);
		request.put("mimeType", mimeType);
		StringEntity entity1 = new StringEntity(request.toString());
		post.setEntity(entity1);
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(post);
	    JSONObject res = new JSONObject(EntityUtils.toString(response.getEntity()));
	    String fileId = res.getString("id");
	    HttpPut put = new HttpPut("https://www.googleapis.com/upload/drive/v2/files/"+fileId+"?uploadType=media");
	    put.addHeader("Authorization", "Bearer "+accesstoken);
	    put.addHeader("Content-Type", mimeType);
	    byte [] bytes = IOUtils.toByteArray(is);
        ByteArrayEntity requestEntity = new ByteArrayEntity(bytes); 
        put.setEntity(requestEntity); 
		HttpClient client1 = HttpClientBuilder.create().build();
	    HttpResponse response1;
	    response1 = client1.execute(put);
	    Map<String,String> fileInfo = new HashMap<String,String>();
	    JSONObject res1 = new JSONObject(EntityUtils.toString(response1.getEntity()));
	    if(res1.has("alternateLink")){
	 	
	    fileInfo.put("fileUrl", res1.getString("alternateLink"));
	    }
	    if(res1.has("mimeType")){
	    	
	    	fileInfo.put("mimeType", res1.getString("mimeType"));
	    }
		
		

		return fileInfo;
	}
}
