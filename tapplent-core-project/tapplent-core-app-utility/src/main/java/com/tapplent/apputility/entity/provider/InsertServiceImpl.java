package com.tapplent.apputility.entity.provider;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.tapplent.apputility.entity.structure.ExitRuleCriteria;
import com.tapplent.apputility.entity.structure.PersonExitRule;
import com.tapplent.apputility.uilayout.structure.CostCentreBreakUpData;
import com.tapplent.apputility.uilayout.structure.JobFamilyBreakUpData;
import com.tapplent.apputility.uilayout.structure.LocationBreakUpData;
import com.tapplent.apputility.uilayout.structure.OrganizationBreakUpData;
import com.tapplent.platformutility.activityLog.ActivityLog;
import com.tapplent.platformutility.activityLog.ActivityLogDtl;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.insert.impl.InsertDAO;
import com.tapplent.platformutility.insert.structure.EmployeeCodeGenRuleVO;
import com.tapplent.platformutility.insert.structure.UsernameGenRuleVo;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;
import com.tapplent.platformutility.sql.context.model.InsertContextModel;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class InsertServiceImpl implements InsertService {
	
	InsertDAO insertDAO;
	
	public InsertDAO getInsertDAO() {
		return insertDAO;
	}

	public void setInsertDAO(InsertDAO insertDAO) {
		this.insertDAO = insertDAO;
	}

	@Override
	public int insertRow(String insertQuery) throws SQLException {
		return insertDAO.insertRow(insertQuery);
	}

	@Override
	public void insertG11Values(Map<String, Object> labels, String g11nTableName) throws Exception {
		insertDAO.insertG11Values(labels, g11nTableName);
	}

	@Override
//	@Transactional(rollbackFor = Exception.class)
	public void CalculateAndUpdateRecordState(EntityMetadataVOX entityMetadataVOX, InsertContextModel model, GlobalVariables globalVariables) throws SQLException {
		EntityAttributeMetadata pkMeta = entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
		String primaryKeyColumn = pkMeta.getDbColumnName();
		String pkDataType = pkMeta.getDbDataTypeCode();
		Object primaryKeyValue = model.getInsertValueMap().get(entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName());// insertData not valueobject because this should be called only when update sql.
		Timestamp currentRecordTimestamp = null;
		if (globalVariables.recordStateClient != null && globalVariables.recordStateClient.equalsIgnoreCase("CURRENT"))
			currentRecordTimestamp = Timestamp.valueOf((String) globalVariables.insertData.getData().get(globalVariables.domainObject+".EffectiveDateTime"));
		else
			currentRecordTimestamp = insertDAO.findCurrentRecord(entityMetadataVOX.getDbTableName(), primaryKeyColumn, pkDataType, primaryKeyValue, globalVariables.currentTimestampString);
		if(currentRecordTimestamp != null){
			String currentRecordTimestampString = (String) Util.getStringTobuildWhereClauseAsParameter(entityMetadataVOX.getAttributeByColumnName("EFFECTIVE_DATETIME"),
																	currentRecordTimestamp, "EFFECTIVE_DATETIME", globalVariables.primaryLocale);
			insertDAO.updateRecordState(entityMetadataVOX.getDbTableName(), primaryKeyColumn, primaryKeyValue, pkDataType, currentRecordTimestampString);
		}
		else {// if the record is inserted for the first time 'current-timestamp' decides whether its history, current or future record!
			insertDAO.updateRecordState(entityMetadataVOX.getDbTableName(), primaryKeyColumn, primaryKeyValue, pkDataType, "'"+globalVariables.currentTimestampString+"'");
		}
	}

	@Override
	public void insertActivityLog(ActivityLog activityLog, EntityMetadataVOX activityMetadataVOX, EntityMetadataVOX activityDtlMetadataVOX, boolean isCreateDetails) throws SQLException {
		String activityTable = activityMetadataVOX.getDbTableName();
		StringBuilder sql = new StringBuilder("INSERT INTO "+activityTable+" (ACTIVITY_LOG_PK_ID,ACT_AUTH_PERSON_FK_ID,MT_PE_CODE_FK_ID,DO_ROW_PRIMARY_KEY_FK_ID,DO_ROW_VERSION_FK_ID,\n" +
				"DISPLAY_CONTEXT_VALUE_G11N_BIG_TXT,BT_DO_ICON_CODE_FK_ID,BT_DO_NAME_G11N_BIG_TXT,ACTION_VISUAL_MASTER_CODE_FK_ID,MSG_SYNTAX_YOU_IN_CONTEXT_G11N_BIG_TXT,\n" +
				"MSG_SYN_YOU_OUT_OF_CONTEXT_G11N_BIG_TXT, MSG_SYNTAX_THIRD_PRSN_IN_CONTEXT_G11N_BIG_TXT, MSG_SYNTAX_THIRD_PRSN_OUT_OF_CONTEXT_G11N_BIG_TXT, ACTIVITY_DATETIME, IS_DELETED ) \n VALUE (");
		sql.append("x'"+Util.remove0x(activityLog.getActivityLogPkId())+"', ");
		sql.append("x'"+Util.remove0x(activityLog.getActivityAuthorPerson())+"', ");
//		sql.append("'"+activityLog.getProcessTypeCodeFkId()+"', ");
		sql.append("'"+activityLog.getMtPeCodeFkId()+"', ");
		if(activityLog.getDoRowPrimaryKeyFkId() == null) sql.append("NULL, "); else sql.append("x'"+Util.remove0x(activityLog.getDoRowPrimaryKeyFkId())+"', ");
		if(activityLog.getDoRowVersionFkId() == null) sql.append("NULL, "); else sql.append("x'"+Util.remove0x(activityLog.getDoRowVersionFkId())+"', ");
		if(activityLog.getDisplayContextValueBigTxt() == null) sql.append("NULL, "); else sql.append(Util.getStringTobeInserted("TEXT",activityLog.getDisplayContextValueBigTxt())+", ");//fixme for ' in text btDOname
		if(activityLog.getDoIconCodeFkId() == null) sql.append("NULL, "); else sql.append("'"+activityLog.getDoIconCodeFkId()+"', ");
		if(activityLog.getDoNameG11nBigTxt() == null) sql.append("NULL, "); else sql.append(Util.getStringTobeInserted("TEXT",activityLog.getDoNameG11nBigTxt())+", ");
		if(activityLog.getActionVisualMasterCode() == null) sql.append("NULL, "); else sql.append("'"+activityLog.getActionVisualMasterCode()+"', ");
		if(activityLog.getMsgYouInContextG11nBigTxt() == null) sql.append("NULL, "); else sql.append(Util.getStringTobeInserted("TEXT",activityLog.getMsgYouInContextG11nBigTxt())+", ");
		if(activityLog.getMsgYouOutOfContextG11nBigTxt() == null) sql.append("NULL, "); else sql.append(Util.getStringTobeInserted("TEXT",activityLog.getMsgYouOutOfContextG11nBigTxt())+", ");
		if(activityLog.getMsgThirdPersonInContextG11nBigTxt() == null) sql.append("NULL, "); else sql.append(Util.getStringTobeInserted("TEXT",activityLog.getMsgThirdPersonInContextG11nBigTxt())+", ");
		if(activityLog.getMsgThirdPersonOutOfContextG11nBigTxt() == null) sql.append("NULL, "); else sql.append(Util.getStringTobeInserted("TEXT",activityLog.getMsgThirdPersonOutOfContextG11nBigTxt())+", ");
		DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String datetimeString = destDf.format(activityLog.getActivityDatetime());
		sql.append("'"+datetimeString+"', ");
//		if(activityLog.getDtlLaunchTrgtMtPeCodeFkId() == null) sql.append("NULL, "); else sql.append("'"+activityLog.getDtlLaunchTrgtMtPeCodeFkId()+"', ");
		sql.append(activityLog.isDeleted()+") ");
		if(isCreateDetails)
			InsertIntoActivityLogDtl(activityLog.getActivityLogDtlMap(), activityDtlMetadataVOX);
		insertRow(sql.toString());

	}

	@Override
	public void insertUpdateRow(String sql, Deque<Object> parameters) throws SQLException {
		insertDAO.insertRowWithParameter(sql, parameters);
	}

	@Override
	public void createUser(GlobalVariables globalVariables) throws SQLException {
		String username = getUsername(globalVariables.usernameGenKey, globalVariables.insertData.getData());
		String password = generateRandomPassword();
		String encryptedPassword = encryptPassword(password);
		Map<String, Object> personData = globalVariables.insertData.getData();
		String userEffectiveDatetime = getEffectiveDatetimeForUser(personData, globalVariables);
		insertDAO.insertUser(globalVariables.insertData.getData(), username, encryptedPassword, globalVariables.currentTimestampString, userEffectiveDatetime);
	}

	private String getEffectiveDatetimeForUser(Map<String, Object> personData, GlobalVariables globalVariables) {
		String personEffectiveDatetime = (String) personData.get(globalVariables.domainObject+".EffectiveDateTime");
		if (personEffectiveDatetime.equals(globalVariables.currentTimestampString)){
			return personEffectiveDatetime;
		} else if (personData.get(globalVariables.domainObject+".AccessPreboarding")!= null &&
				(boolean)personData.get(globalVariables.domainObject+".AccessPreboarding")){
			return globalVariables.currentTimestampString;
		}
		else return personEffectiveDatetime;
	}

	private String encryptPassword(String password) {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder.encode(password);
	}

	private String getUsername(String usernameGenKey, Map<String, Object> data) throws SQLException {
		UsernameGenRuleVo usernameGenRuleVo = insertDAO.getUsernameGenRule(usernameGenKey);
		String formula = Util.replaceDoaWithValue(usernameGenRuleVo.getFormula(), data);
		return insertDAO.calculateFormula(formula);
	}

	private String generateRandomPassword() {
		RandomStringGenerator generator = new RandomStringGenerator.Builder()
				.withinRange('0', 'z')
				.filteredBy(Character::isLetterOrDigit)
				.build();
		return generator.generate(12);
	}

	@Override
	public String generateEmployeeCode(String employeeCodeGenKey, Map<String, Object> data) throws SQLException {
		EmployeeCodeGenRuleVO employeeCodeGenRuleVO = insertDAO.getEmployeeCodeGenRule(employeeCodeGenKey);
		insertDAO.updateEmployeeCodeSequence(employeeCodeGenKey);
		String formula = employeeCodeGenRuleVO.getFormula();
		String nextSequenceNum = Long.toString(employeeCodeGenRuleVO.getEmployeeCodeSequence()+employeeCodeGenRuleVO.getEmployeeSequenceStep());
		formula = Util.replaceDoaWithValue(formula, data);
		formula = formula.replace("NEXTSEQNUM", nextSequenceNum);
//		formula = "SELECT "+formula+" AS EmployeeCode;";
		return insertDAO.calculateFormula(formula);
	}

	@Override
	public void deleteGenericRecord(String tableName, Map<String, Object> deleteParameters) throws SQLException {
		Deque<Object> parameters = new ArrayDeque<>();
		StringBuilder SQL = new StringBuilder("DELETE FROM " + tableName + " WHERE ");
		for (Map.Entry entry : deleteParameters.entrySet()){
			SQL.append(entry.getKey() + " = ? AND ");
			parameters.add(entry.getValue());
		}
		SQL.replace(SQL.length()-4, SQL.length(), ";");
		insertDAO.insertRowWithParameter(SQL.toString(), parameters);
	}

	@Override
	public void updateBreakUpDataForOrg(List<OrganizationBreakUpData> organizationBreakUpData, GlobalVariables globalVariables) throws SQLException {
		String doName = globalVariables.entityMetadataVOX.getDomainObjectCode();
		Map<String, Object> deleteParameters = new HashMap<>();
		deleteParameters.put("PERSON_DEP_FK_ID", globalVariables.insertData.getData().get(doName+".Person"));
		deleteGenericRecord("T_PRN_IEU_ORG_BRK_UP", deleteParameters);
		List<String> pkList = organizationBreakUpData.stream()
										.map(OrganizationBreakUpData::getOrgPkId)
										.collect(Collectors.toList());
		insertGenericRecord(pkList, "T_PRN_IEU_ORG_BRK_UP", "PRN_ORG_BRK_UP_PK_ID", "PRN_ORG_FK_ID", globalVariables);
	}

	private void insertGenericRecord(List<String> pkList, String tableName, String primaryColumn, String foreignKeyColumn, GlobalVariables globalVariables) throws SQLException {
		Map<String, Object> data = globalVariables.insertData.getData();
		String doName = globalVariables.domainObject;
		StringBuilder sql = new StringBuilder("INSERT INTO "+tableName+" (VERSION_ID, "+primaryColumn+", PERSON_DEP_FK_ID, "+foreignKeyColumn+", EFFECTIVE_DATETIME, SAVE_STATE_CODE_FK_ID, " +
				"ACTIVE_STATE_CODE_FK_ID, RECORD_STATE_CODE_FK_ID, "+Util.getStandardColumns()+") VALUES ");
		for (String pk : pkList){
			sql.append("(ordered_uuid(UUID()), ordered_uuid(UUID()),")
					.append(data.get(doName+".Person")+",")
					.append(pk+",")
					.append("'"+data.get(doName+".EffectiveDateTime")+"',")
					.append("'SAVED', 'ACTIVE', 'CURRENT',")
					.append(Util.getStandardColumnValues(globalVariables.currentTimestampString)+"), ");
		}
		sql.replace(sql.length()-2, sql.length(), ";");
		insertDAO.insertRow(sql.toString());
	}

	@Override
	public void updateBreakUpDataForLoc(List<LocationBreakUpData> locationBreakUpData, GlobalVariables globalVariables) throws SQLException {
		String doName = globalVariables.entityMetadataVOX.getDomainObjectCode();
		Map<String, Object> deleteParameters = new HashMap<>();
		deleteParameters.put("PERSON_DEP_FK_ID", globalVariables.insertData.getData().get(doName+".Person"));
		deleteGenericRecord("T_PRN_IEU_LOC_BRK_UP", deleteParameters);
		List<String> pkList = locationBreakUpData.stream()
									.map(LocationBreakUpData::getLocationId)
									.collect(Collectors.toList());
		insertGenericRecord(pkList, "T_PRN_IEU_LOC_BRK_UP", "PRN_LOC_BRK_UP_PK_ID", "PRN_LOC_FK_ID", globalVariables);
	}

	@Override
	public void updateBreakUpDataForCC(List<CostCentreBreakUpData> cCBreakUpData, GlobalVariables globalVariables) throws SQLException {
		String doName = globalVariables.entityMetadataVOX.getDomainObjectCode();
		Map<String, Object> deleteParameters = new HashMap<>();
		deleteParameters.put("PERSON_DEP_FK_ID", globalVariables.insertData.getData().get(doName+".Person"));
		deleteGenericRecord("T_PRN_IEU_CC_BRK_UP", deleteParameters);
		List<String> pkList = cCBreakUpData.stream()
				.map(CostCentreBreakUpData::getCostCentreId)
				.collect(Collectors.toList());
		insertGenericRecord(pkList, "T_PRN_IEU_CC_BRK_UP", "PRN_CC_BRK_UP_PK_ID", "PRN_CC_FK_ID", globalVariables);
	}

	@Override
	public void updateBreakUpDataForJobFamily(List<JobFamilyBreakUpData> jobFamilyBreakUpData, GlobalVariables globalVariables) throws SQLException {
		String doName = globalVariables.entityMetadataVOX.getDomainObjectCode();
		Map<String, Object> deleteParameters = new HashMap<>();
		deleteParameters.put("PERSON_DEP_FK_ID", globalVariables.insertData.getData().get(doName+".Person"));
		deleteGenericRecord("T_PRN_IEU_JOB_FMLY_BRK_UP", deleteParameters);
		List<String> pkList = jobFamilyBreakUpData.stream()
				.map(JobFamilyBreakUpData::getJobFamilyId)
				.collect(Collectors.toList());
		insertGenericRecord(pkList, "T_PRN_IEU_JOB_FMLY_BRK_UP", "PRN_JOB_FAMILY_BRK_UP_PK_ID", "PRN_JOB_FAMILY_FK_ID", globalVariables);
	}

	@Override
	public void inactivateUser(GlobalVariables globalVariables) throws SQLException, ParseException {
		String personId = (String) globalVariables.parentInsertData.getData().get("PersonTxn.Person");
		String effectiveDatetimeString = (String) globalVariables.parentInsertData.getData().get("PersonTxn.EffectiveDateTime");
		Timestamp effectiveDatetime = Util.getTimestampFromString(effectiveDatetimeString);
		Timestamp currentTimestamp = Util.getTimestampFromString(globalVariables.currentTimestampString);
		User user = getUserDetails(personId);
		String recordState = null;
		if (currentTimestamp.before(effectiveDatetime))
			recordState = "FUTURE";
		else if (effectiveDatetime.before(user.getEffectiveDatetime()))
			recordState = "HISTORY";
		else recordState = "CURRENT";
		StringBuilder sql = new StringBuilder("INSERT INTO T_USR_ADM_USER (VERSION_ID, USER_NAME_PK_TXT, USER_PASSWORD_TXT, IS_LOCKED, PERSON_FK_ID, EFFECTIVE_DATETIME, " +
				"SAVE_STATE_CODE_FK_ID, ACTIVE_STATE_CODE_FK_ID, RECORD_STATE_CODE_FK_ID, "+Util.getStandardColumns()+") VALUE (");
		sql.append("ordered_uuid(uuid()), '"+user.getUsername()+"', ")
				.append("'"+user.getPassword()+"', "+user.isLocked()+", ")
				.append(personId+", '"+effectiveDatetimeString+"', ")
				.append("'SAVED', 'INACTIVE', '"+recordState+"', ")
				.append(Util.getStandardColumnValues(globalVariables.currentTimestampString)+");");
		insertDAO.insertRow(sql.toString());
	}

	@Override
	public Map<String, ExitRuleCriteria> getExitRuleCriteria() throws SQLException {
		String sql = "SELECT * FROM T_PRN_LKP_EXIT_RULE_CRITERIA ;";
		ResultSet rs = insertDAO.search(sql, null);
		Map<String, ExitRuleCriteria> exitRuleCriteriaMap = new HashMap<>();
		ExitRuleCriteria exitRuleCriteria = null;
		while (rs.next()){
			exitRuleCriteria = new ExitRuleCriteria();
			exitRuleCriteria.setVersionId(Util.convertByteToString(rs.getBytes("VERSION_ID")));
			exitRuleCriteria.setExitRuleCriteriaCode(rs.getString("EXIT_RULE_CRITERIA_CODE_PK_ID"));
			exitRuleCriteria.setExitRuleCriteriaName(rs.getString("EXIT_RULE_CRITERIA_NAME_G11N_BIG_TXT"));
			exitRuleCriteria.setExitRuleCriteriaIcon(rs.getString("EXIT_RULE_CRITERIA_ICON_CODE_FK_ID"));
			exitRuleCriteriaMap.put(exitRuleCriteria.getExitRuleCriteriaCode(), exitRuleCriteria);
		}
		return exitRuleCriteriaMap;
	}

	@Override
	public Map<String, List<PersonExitRule>> getCriteriaToExitRuleMap() throws SQLException {
		String sql = "SELECT * FROM T_PRN_LKP_EXIT_RULE_CRITERIA JOIN T_PRN_EU_PERSON_EXIT_RULE ON EXIT_RULE_CRITERIA_CODE_PK_ID = EXIT_RULE_CODE_FK_ID;";
		ResultSet rs = insertDAO.search(sql, null);
		Map<String, List<PersonExitRule>> criteriaToExitRuleMap = new HashMap<>();
		PersonExitRule personExitRule = null;
		while (rs.next()){
			personExitRule = new PersonExitRule();
			personExitRule.setVersionId(Util.convertByteToString(rs.getBytes("VERSION_ID")));
			personExitRule.setPersonExitRuleId(Util.convertByteToString(rs.getBytes("PERSON_EXIT_RULE_PK_ID")));
			personExitRule.setExitRuleCodeId(rs.getString("EXIT_RULE_CODE_FK_ID"));
			personExitRule.setReplaceJobRelationTypeCode(rs.getString("REPL_JOB_REL_TYPE_CODE_FK_ID"));
			personExitRule.setReplaceJobRelationHierarchyLevel(rs.getString("REPL_JOB_REL_TYPE_HCY_LVL_POS_INT"));
//			personExitRule.setReplacementPerson(Util.convertByteToString(rs.getBytes("REPLACEMENT_PERSON_FK_ID")));
			if (criteriaToExitRuleMap.containsKey(personExitRule.getExitRuleCodeId())){
				criteriaToExitRuleMap.get(personExitRule.getExitRuleCodeId()).add(personExitRule);
			}
			else {
				List<PersonExitRule> personExitRules = new ArrayList<>();
				personExitRules.add(personExitRule);
				criteriaToExitRuleMap.put(personExitRule.getExitRuleCodeId(), personExitRules);
			}
		}
		return criteriaToExitRuleMap;
	}

	@Override
	public List<Map<String, Object>> getAllRecordsByRelationshipType(String relationshipType, String personId) throws SQLException {
		MasterEntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE("PersonJobRelationship");
		String sql = Util.getSqlStringForMasterTable(entityMetadataVOX) + " WHERE RELATED_PERSON_FK_ID = x? AND RELATIONSHIP_TYPE_CODE_FK_ID = ?;";
		List<Object> params = new ArrayList<>();
		params.add(Util.convertStringIdToByteId(personId));
		ResultSet rs = insertDAO.search(sql, params);
		while (rs.next()){

		}
		return null;
	}

	private User getUserDetails(String personId) throws SQLException {
		String sql = "SELECT * FROM T_USR_ADM_USER WHERE PERSON_FK_ID = ? AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> params = new ArrayList<>();
		params.add(Util.convertStringIdToByteId(personId));
		ResultSet rs = insertDAO.search(sql, params);
		User user = null;
		if (rs.next()){
			user = new User();
			user.setVersionId(Util.convertByteToString(rs.getBytes("VERSION_ID")));
			user.setUsername(rs.getString("USER_NAME_PK_TXT"));
			user.setPassword(rs.getString("USER_PASSWORD_TXT"));
			user.setLocked(rs.getBoolean("IS_LOCKED"));
			user.setPersonId(rs.getString("PERSON_FK_ID"));
			user.setEffectiveDatetime(rs.getTimestamp("EFFECTIVE_DATETIME"));
		}
		return user;
	}

	private void InsertIntoActivityLogDtl(Map<String, ActivityLogDtl> activityLogDtlMap, EntityMetadataVOX activityDtlMetadataVOX) throws SQLException {
		Map<String, EntityAttributeMetadata> attributeColumnNameMap = activityDtlMetadataVOX.getAttributeColumnNameMap();
		StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_ACTIVITY_LOG_DTL (ACTIVITY_LOG_DTL_PK_ID, ACTIVITY_LOG_FK_ID, MT_DOA_CODE_FK_ID, IS_NEAR_LABEL, NEW_VALUE_G11N_BIG_TXT) VALUES ");
		if(activityLogDtlMap != null && !activityLogDtlMap.isEmpty()){
			ActivityLogDtl activityLogDtl = null;
			for(Map.Entry<String, ActivityLogDtl> entry : activityLogDtlMap.entrySet()){
				activityLogDtl = entry.getValue();
				sql.append("\n(");
				sql.append(Util.getStringTobeInserted(attributeColumnNameMap.get("ACTIVITY_LOG_DTL_PK_ID"), activityLogDtl.getActivityLogDtlPkId())+", ");
				sql.append(Util.getStringTobeInserted(attributeColumnNameMap.get("ACTIVITY_LOG_FK_ID"), activityLogDtl.getActivityLogFkId())+", ");
//				sql.append(Util.getStringTobeInserted(attributeColumnNameMap.get("BT_CODE_FK_ID"), activityLogDtl.getBtCodeFkId())+", ");
				sql.append(Util.getStringTobeInserted(attributeColumnNameMap.get("MT_DOA_CODE_FK_ID"), activityLogDtl.getMtDoaCodeFkId())+", ");
				sql.append(Util.getStringTobeInserted(attributeColumnNameMap.get("IS_NEAR_LABEL"), activityLogDtl.isNearLabel())+", ");
				sql.append(Util.getStringTobeInserted(attributeColumnNameMap.get("NEW_VALUE_G11N_BIG_TXT"), activityLogDtl.getNewValueG11nBigTxt()));
				sql.append("),");
			}
			sql.replace(sql.length()-1, sql.length(), "");
			insertRow(sql.toString());
		}
	}

}
