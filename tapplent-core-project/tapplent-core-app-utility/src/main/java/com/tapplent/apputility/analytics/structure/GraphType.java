package com.tapplent.apputility.analytics.structure;

import com.tapplent.platformutility.analytics.structure.GraphTypeVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 23/08/17.
 */
public class GraphType {
    private String graphTypeCode;
    private String graphTypeName;

    public String getGraphTypeCode() {
        return graphTypeCode;
    }

    public void setGraphTypeCode(String graphTypeCode) {
        this.graphTypeCode = graphTypeCode;
    }

    public String getGraphTypeName() {
        return graphTypeName;
    }

    public void setGraphTypeName(String graphTypeName) {
        this.graphTypeName = graphTypeName;
    }

    public static GraphType fromVO(GraphTypeVO graphTypeVO) {
        GraphType graphType = new GraphType();
        BeanUtils.copyProperties(graphTypeVO, graphType);
        return graphType;
    }
}
