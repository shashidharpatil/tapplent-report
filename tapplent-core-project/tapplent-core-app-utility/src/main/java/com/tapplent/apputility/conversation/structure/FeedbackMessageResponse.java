package com.tapplent.apputility.conversation.structure;

import java.util.List;

/**
 * Created by Tapplent on 6/16/17.
 */
public class FeedbackMessageResponse {
    List<FeedbackMessage> feedbackMessageList;

    public List<FeedbackMessage> getFeedbackMessageList() {
        return feedbackMessageList;
    }

    public void setFeedbackMessageList(List<FeedbackMessage> feedbackMessageList) {
        this.feedbackMessageList = feedbackMessageList;
    }
}
