package com.tapplent.apputility.data.structure;

public class RootVariables {
    private String currentTimestampString;
    private String actionCode;
    private String primaryLocale;
    private String employeeCodeGenKey;
    private String usernameGenKey;

    public String getCurrentTimestampString() {
        return currentTimestampString;
    }

    public void setCurrentTimestampString(String currentTimestampString) {
        this.currentTimestampString = currentTimestampString;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getPrimaryLocale() {
        return primaryLocale;
    }

    public void setPrimaryLocale(String primaryLocale) {
        this.primaryLocale = primaryLocale;
    }

    public String getEmployeeCodeGenKey() {
        return employeeCodeGenKey;
    }

    public void setEmployeeCodeGenKey(String employeeCodeGenKey) {
        this.employeeCodeGenKey = employeeCodeGenKey;
    }

    public String getUsernameGenKey() {
        return usernameGenKey;
    }

    public void setUsernameGenKey(String usernameGenKey) {
        this.usernameGenKey = usernameGenKey;
    }
}
