package com.tapplent.apputility.data.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.tapplent.apputility.data.structure.*;
import com.tapplent.platformutility.expression.jeval.EvaluationException;
import com.tapplent.platformutility.uilayout.valueobject.LoadParameters;
import com.tapplent.platformutility.workflow.structure.WorkflowActionAttributePermission;
import com.tapplent.platformutility.workflow.structure.WorkflowActorAction;

public interface DataService {

	UnitResponseData getScreen(UnitRequestData requestData) throws IOException, SQLException;

    RelationResponse getRelationControlData(String controlId, JsonLabelAliasTracker aliasTracker);

    G11nResponse getG11nData(String dbPkId, String doaExpn);

    UnitResponseData getRelationDownwardEffect(String mtPE, String pk , boolean isMasterTable);

    UnitResponseData getAttributePathData(List<String> attributeList, String pk, boolean isDbPk, boolean isResolveG11n);

    UnitResponseData getMenuExpressionResponse(Map<String, PERequest> mtPEToPERequestMap, Map<String, LoadParameters> peToLoadParams);

    PEFKHierarchy getPEFKHierarchy(String mtPE, boolean isMasterTable);

    public ProcessElementData getDataInternal(PERequest rootPE, JsonLabelAliasTracker aliasTracker, String baseTemplate, UnitResponseData responseData) throws IOException, SQLException;

    Object getBreakedUpData(String contextType, String contextID);

    List<JobRelationships> getDefaultJobRelations(String contextID, String contextID2);

    WorkflowActionAttributePermission getControlAndActionPermissionForWorkflow(String workflowTxnId) throws SQLException, EvaluationException;

    List<WorkflowActorAction> getWorkflowActionsForLoggedInUser(String workflowTxnId) throws SQLException;
}
