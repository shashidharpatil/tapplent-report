package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.layout.valueObject.ThemeAlphaVO;

public class ThemeAlpha {
	@JsonIgnore
	private String versionId;
	private String themeAlpha;
	private String themeStateCode;
	private String themeTemplateCode;
	private int themeAlphaPosInt;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getThemeAlpha() {
		return themeAlpha;
	}
	public void setThemeAlpha(String themeAlpha) {
		this.themeAlpha = themeAlpha;
	}
	public String getThemeStateCode() {
		return themeStateCode;
	}
	public void setThemeStateCode(String themeStateCode) {
		this.themeStateCode = themeStateCode;
	}
	public String getThemeTemplateCode() {
		return themeTemplateCode;
	}
	public void setThemeTemplateCode(String themeTemplateCode) {
		this.themeTemplateCode = themeTemplateCode;
	}
	public int getThemeAlphaPosInt() {
		return themeAlphaPosInt;
	}
	public void setThemeAlphaPosInt(int themeAlphaPosInt) {
		this.themeAlphaPosInt = themeAlphaPosInt;
	}
	public static ThemeAlpha fromVO(ThemeAlphaVO themeAlphaVO){
		ThemeAlpha themeAlpha = new ThemeAlpha();
		BeanUtils.copyProperties(themeAlphaVO, themeAlpha);
		return themeAlpha;
	}
}