package com.tapplent.apputility.analytics.structure;

/**
 * Created by tapplent on 13/12/16.
 */
public enum ChartAttributeAxisCode {
    X_AXIS,
    SUB_X_AXIS,
    Y_AXIS,
    MIN_Y_AXIS,
    AVG_Y_AXIS,
    MAX_Y_AXIS,
    TIME_X_AXIS,
    TABLE_AXIS;
}
