package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.layout.valueObject.ActionStepPropertyVO;

public class ActionStepProperty {
	@JsonIgnore
	private String actionStepPropertyId;
	private String baseTemplateId;
	private String mtPEId;
	private String actionStepLookupId;
	private String propertyCode;
	private String propertyValue;
	
	public ActionStepProperty(String baseTemplateId, String mtPEId, String actionStepLookupId, String propertyCode,
			String propertyValue) {
		this.baseTemplateId = baseTemplateId;
		this.mtPEId = mtPEId;
		this.actionStepLookupId = actionStepLookupId;
		this.propertyCode = propertyCode;
		this.propertyValue = propertyValue;
	}
	
	public ActionStepProperty(){
		
	}
	
	public String getActionStepPropertyId() {
		return actionStepPropertyId;
	}

	public void setActionStepPropertyId(String actionStepPropertyId) {
		this.actionStepPropertyId = actionStepPropertyId;
	}

	public String getBaseTemplateId() {
		return baseTemplateId;
	}

	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getMtPEId() {
		return mtPEId;
	}

	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}

	public String getActionStepLookupId() {
		return actionStepLookupId;
	}

	public void setActionStepLookupId(String actionStepLookupId) {
		this.actionStepLookupId = actionStepLookupId;
	}

	
	public String getPropertyCode() {
		return propertyCode;
	}

	public void setPropertyCode(String propertyCode) {
		this.propertyCode = propertyCode;
	}

	public String getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public static ActionStepProperty fromVO(ActionStepPropertyVO actionStepPropertyVO){
		ActionStepProperty actionStepProperty = new ActionStepProperty();
		BeanUtils.copyProperties(actionStepPropertyVO, actionStepProperty);
		return actionStepProperty;
	}
}
