package com.tapplent.apputility.analytics.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.apputility.data.structure.AttributeData;

/**
 * Created by tapplent on 15/12/16.
 */
public class XAxisData {
    private String mappedData;
    private String displayData;
    private double minYAxisResult;
    private double avgYAxisResult;
    private double maxYAxisResult;
    private double yAxisResult;
    public String getMappedData() {
        return mappedData;
    }

    public void setMappedData(String mappedData) {
        this.mappedData = mappedData;
    }

    public String getDisplayData() {
        return displayData;
    }

    public void setDisplayData(String displayData) {
        this.displayData = displayData;
    }

    public double getyAxisResult() {
        return yAxisResult;
    }

    public void setyAxisResult(double yAxisResult) {
        this.yAxisResult = yAxisResult;
    }

    public double getMinYAxisResult() {
        return minYAxisResult;
    }

    public void setMinYAxisResult(double minYAxisResult) {
        this.minYAxisResult = minYAxisResult;
    }

    public double getAvgYAxisResult() {
        return avgYAxisResult;
    }

    public void setAvgYAxisResult(double avgYAxisResult) {
        this.avgYAxisResult = avgYAxisResult;
    }

    public double getMaxYAxisResult() {
        return maxYAxisResult;
    }

    public void setMaxYAxisResult(double maxYAxisResult) {
        this.maxYAxisResult = maxYAxisResult;
    }
}
