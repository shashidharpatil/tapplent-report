package com.tapplent.apputility.utility.service;

import com.tapplent.apputility.utility.structure.LogErrorRequest;

/**
 * Created by Shubham Patodi on 05/08/16.
 */
public interface UtilityService {
    void insertError(LogErrorRequest errorRequest);
}
