package com.tapplent.apputility.data.controller;

import com.tapplent.apputility.data.structure.FeaturingPersonDetail;

import java.util.List;

public class FeaturedResponse {
    List<FeaturingPersonDetail> featuringPersonDetailList;

    public List<FeaturingPersonDetail> getFeaturingPersonDetailList() {
        return featuringPersonDetailList;
    }

    public void setFeaturingPersonDetailList(List<FeaturingPersonDetail> featuringPersonDetailList) {
        this.featuringPersonDetailList = featuringPersonDetailList;
    }
}
