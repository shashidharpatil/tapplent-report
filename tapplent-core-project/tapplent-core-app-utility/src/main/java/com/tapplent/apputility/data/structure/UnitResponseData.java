package com.tapplent.apputility.data.structure;

import com.tapplent.apputility.uilayout.structure.ScreenInstance;
import com.tapplent.platformutility.metadata.structure.EntityMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.uilayout.valueobject.SectionLayoutMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UnitResponseData {
	private Map<String, ProcessElementData> PEData = new HashMap<>();
	private ScreenInstance layout;
 	private Map<String, DoaMeta> labelToAliasMap = new HashMap<>();
 	private Map<String, EntityMetadataVOX> doMetaMap = new HashMap<>();
	public Map<String, ProcessElementData> getPEData() {
		return PEData;
	}

	public void setPEData(Map<String, ProcessElementData> PEData) {
		this.PEData = PEData;
	}

	public ScreenInstance getLayout() {
		return layout;
	}

	public void setLayout(ScreenInstance layout) {
		this.layout = layout;
	}

	public Map<String, DoaMeta> getLabelToAliasMap() {
		return labelToAliasMap;
	}

	public void setLabelToAliasMap(Map<String, DoaMeta> labelToAliasMap) {
		this.labelToAliasMap = labelToAliasMap;
	}

	public Map<String, EntityMetadataVOX> getDoMetaMap() {
		return doMetaMap;
	}

	public void setDoMetaMap(Map<String, EntityMetadataVOX> doMetaMap) {
		this.doMetaMap = doMetaMap;
	}
}
