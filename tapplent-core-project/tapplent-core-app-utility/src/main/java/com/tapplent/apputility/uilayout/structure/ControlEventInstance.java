package com.tapplent.apputility.uilayout.structure;

import com.tapplent.platformutility.uilayout.valueobject.ControlEventInstanceVO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 01/10/17.
 */
public class ControlEventInstance {
    private String controlEventPkId;
    private String controlId;
    private String event;
    private String rule;
    private String targetControlInstance;
    private String targetFormatQuery;
    private String targetFormatText;
    private List<ControlEventCriteria> criteriaList = new ArrayList<>();
    private List<FormulaInputParametersDetails> formulaInputParametersDetails = new ArrayList<>();

    public String getControlEventPkId() {
        return controlEventPkId;
    }

    public void setControlEventPkId(String controlEventPkId) {
        this.controlEventPkId = controlEventPkId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getTargetControlInstance() {
        return targetControlInstance;
    }

    public void setTargetControlInstance(String targetControlInstance) {
        this.targetControlInstance = targetControlInstance;
    }

    public String getTargetFormatQuery() {
        return targetFormatQuery;
    }

    public void setTargetFormatQuery(String targetFormatQuery) {
        this.targetFormatQuery = targetFormatQuery;
    }

    public String getTargetFormatText() {
        return targetFormatText;
    }

    public void setTargetFormatText(String targetFormatText) {
        this.targetFormatText = targetFormatText;
    }

    public String getControlId() {
        return controlId;
    }

    public void setControlId(String controlId) {
        this.controlId = controlId;
    }

    public List<ControlEventCriteria> getCriteriaList() {
        return criteriaList;
    }

    public void setCriteriaList(List<ControlEventCriteria> criteriaList) {
        this.criteriaList = criteriaList;
    }

    public List<FormulaInputParametersDetails> getFormulaInputParametersDetails() {
        return formulaInputParametersDetails;
    }

    public void setFormulaInputParametersDetails(List<FormulaInputParametersDetails> formulaInputParametersDetails) {
        this.formulaInputParametersDetails = formulaInputParametersDetails;
    }

    public static ControlEventInstance fromVO(ControlEventInstanceVO controlEventInstanceVO) {
        ControlEventInstance controlEventInstance = new ControlEventInstance();
        BeanUtils.copyProperties(controlEventInstanceVO, controlEventInstance);
        return controlEventInstance;
    }
}
