package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.tapplent.platformutility.layout.valueObject.ActionStandAloneVO;

public class PropertyControlAction {
	private String propertyControlActionEuPkId;
	private String basetemplateFkId;
	private String propertyControlActionDefnFkId;
	private String propertyControlDefnFkId;
	private String propertyControlEuFkId;
	private String actionCodeFkId;
	private String actionGestureCodeFkId;
	private String actionGestureAnimationCodeFkId;
	public String getPropertyControlActionEuPkId() {
		return propertyControlActionEuPkId;
	}
	public void setPropertyControlActionEuPkId(String propertyControlActionEuPkId) {
		this.propertyControlActionEuPkId = propertyControlActionEuPkId;
	}
	public String getBasetemplateFkId() {
		return basetemplateFkId;
	}
	public void setBasetemplateFkId(String basetemplateFkId) {
		this.basetemplateFkId = basetemplateFkId;
	}
	public String getPropertyControlActionDefnFkId() {
		return propertyControlActionDefnFkId;
	}
	public void setPropertyControlActionDefnFkId(String propertyControlActionDefnFkId) {
		this.propertyControlActionDefnFkId = propertyControlActionDefnFkId;
	}
	public String getPropertyControlDefnFkId() {
		return propertyControlDefnFkId;
	}
	public void setPropertyControlDefnFkId(String propertyControlDefnFkId) {
		this.propertyControlDefnFkId = propertyControlDefnFkId;
	}
	public String getPropertyControlEuFkId() {
		return propertyControlEuFkId;
	}
	public void setPropertyControlEuFkId(String propertyControlEuFkId) {
		this.propertyControlEuFkId = propertyControlEuFkId;
	}
	public String getActionCodeFkId() {
		return actionCodeFkId;
	}
	public void setActionCodeFkId(String actionCodeFkId) {
		this.actionCodeFkId = actionCodeFkId;
	}
	public String getActionGestureCodeFkId() {
		return actionGestureCodeFkId;
	}
	public void setActionGestureCodeFkId(String actionGestureCodeFkId) {
		this.actionGestureCodeFkId = actionGestureCodeFkId;
	}
	public String getActionGestureAnimationCodeFkId() {
		return actionGestureAnimationCodeFkId;
	}
	public void setActionGestureAnimationCodeFkId(String actionGestureAnimationCodeFkId) {
		this.actionGestureAnimationCodeFkId = actionGestureAnimationCodeFkId;
	}
	public static PropertyControlAction fromVO(ActionStandAloneVO propertyControlActionVO){
		PropertyControlAction propertyControlAction = new PropertyControlAction();
		BeanUtils.copyProperties(propertyControlActionVO, propertyControlAction);
		return propertyControlAction;
	}
}
