package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.common.util.AttachmentObject;
import com.tapplent.platformutility.layout.valueObject.ProcessHomePageIntroContentVO;
import org.springframework.beans.BeanUtils;

import java.net.URL;
import java.util.Date;

/**
 * Created by tapplent on 15/11/16.
 */
public class ProcessHomePageIntroContent {
    private String prcIntroContentPkId;
    private String loggedInPersonPkId;
    private String prcIntroContentRuleFkId;
    private String processTypeCodeFkId;
    private AttachmentObject introImageURL;
    private String introTitleG11nBigTxt;
    private String introSubTextG11nBigTxt;
    private String linkedStaticExtUrlTxt;
    private String linkedResolvedTargetScreenInstanceFkId;
    private String linkedResolvedTargetScreenSectionInstanceFkId;
    private String linkedResolvedTargetTabSeqNumPosInt;
    private String linkedDynamicActionFilterExpn;

    public String getPrcIntroContentPkId() {
        return prcIntroContentPkId;
    }

    public void setPrcIntroContentPkId(String prcIntroContentPkId) {
        this.prcIntroContentPkId = prcIntroContentPkId;
    }

    public String getLoggedInPersonPkId() {
        return loggedInPersonPkId;
    }

    public void setLoggedInPersonPkId(String loggedInPersonPkId) {
        this.loggedInPersonPkId = loggedInPersonPkId;
    }

    public String getPrcIntroContentRuleFkId() {
        return prcIntroContentRuleFkId;
    }

    public void setPrcIntroContentRuleFkId(String prcIntroContentRuleFkId) {
        this.prcIntroContentRuleFkId = prcIntroContentRuleFkId;
    }

    public String getProcessTypeCodeFkId() {
        return processTypeCodeFkId;
    }

    public void setProcessTypeCodeFkId(String processTypeCodeFkId) {
        this.processTypeCodeFkId = processTypeCodeFkId;
    }

    public AttachmentObject getIntroImageURL() {
        return introImageURL;
    }

    public void setIntroImageURL(AttachmentObject introImageURL) {
        this.introImageURL = introImageURL;
    }

    public String getIntroTitleG11nBigTxt() {
        return introTitleG11nBigTxt;
    }

    public void setIntroTitleG11nBigTxt(String introTitleG11nBigTxt) {
        this.introTitleG11nBigTxt = introTitleG11nBigTxt;
    }

    public String getIntroSubTextG11nBigTxt() {
        return introSubTextG11nBigTxt;
    }

    public void setIntroSubTextG11nBigTxt(String introSubTextG11nBigTxt) {
        this.introSubTextG11nBigTxt = introSubTextG11nBigTxt;
    }

    public String getLinkedStaticExtUrlTxt() {
        return linkedStaticExtUrlTxt;
    }

    public void setLinkedStaticExtUrlTxt(String linkedStaticExtUrlTxt) {
        this.linkedStaticExtUrlTxt = linkedStaticExtUrlTxt;
    }

    public String getLinkedResolvedTargetScreenInstanceFkId() {
        return linkedResolvedTargetScreenInstanceFkId;
    }

    public void setLinkedResolvedTargetScreenInstanceFkId(String linkedResolvedTargetScreenInstanceFkId) {
        this.linkedResolvedTargetScreenInstanceFkId = linkedResolvedTargetScreenInstanceFkId;
    }

    public String getLinkedResolvedTargetScreenSectionInstanceFkId() {
        return linkedResolvedTargetScreenSectionInstanceFkId;
    }

    public void setLinkedResolvedTargetScreenSectionInstanceFkId(String linkedResolvedTargetScreenSectionInstanceFkId) {
        this.linkedResolvedTargetScreenSectionInstanceFkId = linkedResolvedTargetScreenSectionInstanceFkId;
    }

    public String getLinkedResolvedTargetTabSeqNumPosInt() {
        return linkedResolvedTargetTabSeqNumPosInt;
    }

    public void setLinkedResolvedTargetTabSeqNumPosInt(String linkedResolvedTargetTabSeqNumPosInt) {
        this.linkedResolvedTargetTabSeqNumPosInt = linkedResolvedTargetTabSeqNumPosInt;
    }

    public String getLinkedDynamicActionFilterExpn() {
        return linkedDynamicActionFilterExpn;
    }

    public void setLinkedDynamicActionFilterExpn(String linkedDynamicActionFilterExpn) {
        this.linkedDynamicActionFilterExpn = linkedDynamicActionFilterExpn;
    }

    public static ProcessHomePageIntroContent fromVO(ProcessHomePageIntroContentVO processHomePageIntroContentVO){
        ProcessHomePageIntroContent processHomePageIntroContent = new ProcessHomePageIntroContent();
        BeanUtils.copyProperties(processHomePageIntroContentVO, processHomePageIntroContent);
        return processHomePageIntroContent;
    }
}
