/**
 * 
 */
package com.tapplent.apputility.etl.structure;

/**
 * @author Shubham Patodi
 *
 */
public class DBIndex {
	private String doaCode;
	private String columnName;
	private String tableName;
	private String indexReason;
	public DBIndex(String doaCode, String columnName, String tableName, String indexReason) {
		super();
		this.doaCode = doaCode;
		this.columnName = columnName;
		this.tableName = tableName;
		this.indexReason = indexReason;
	}
	public String getDoaCode() {
		return doaCode;
	}
	public void setDoaCode(String doaCode) {
		this.doaCode = doaCode;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getIndexReason() {
		return indexReason;
	}
	public void setIndexReason(String indexReason) {
		this.indexReason = indexReason;
	}
}
