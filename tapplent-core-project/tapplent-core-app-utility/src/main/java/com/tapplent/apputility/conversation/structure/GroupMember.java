package com.tapplent.apputility.conversation.structure;

import com.tapplent.platformutility.common.util.AttachmentObject;
import com.tapplent.platformutility.conversation.valueobject.Email;
import com.tapplent.platformutility.conversation.valueobject.Phone;
import com.tapplent.platformutility.layout.valueObject.SocialStatusVO;

import java.net.URL;
import java.util.List;

/**
 * Created by Tapplent on 6/22/17.
 */
public class GroupMember {
    private String feedbackGroupMemberId;
    private String groupId;
    private String personId;
    private String personPhoto;
    private URL personPhotoUrl;
    private String personFullName;
    private String personDesignation;
    private String location;
    private String readDatetime;
    private boolean memberAdmin;
    private boolean memberNtfnMuted;
    private boolean memberLeft;
    private boolean memberRemoved;
    private boolean memberAccessRemoved;
    private boolean deleted;
    private SocialStatusVO socialStatus;
    private String nameInitials;
    private List<Phone> phoneList;
    private List<Email> emailList;

    public String getFeedbackGroupMemberId() {
        return feedbackGroupMemberId;
    }

    public void setFeedbackGroupMemberId(String feedbackGroupMemberId) {
        this.feedbackGroupMemberId = feedbackGroupMemberId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(String personPhoto) {
        this.personPhoto = personPhoto;
    }

    public String getPersonFullName() {
        return personFullName;
    }

    public void setPersonFullName(String personFullName) {
        this.personFullName = personFullName;
    }

    public String getPersonDesignation() {
        return personDesignation;
    }

    public void setPersonDesignation(String personDesignation) {
        this.personDesignation = personDesignation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isMemberAdmin() {
        return memberAdmin;
    }

    public void setMemberAdmin(boolean memberAdmin) {
        this.memberAdmin = memberAdmin;
    }

    public boolean isMemberNtfnMuted() {
        return memberNtfnMuted;
    }

    public void setMemberNtfnMuted(boolean memberNtfnMuted) {
        this.memberNtfnMuted = memberNtfnMuted;
    }

    public boolean isMemberLeft() {
        return memberLeft;
    }

    public void setMemberLeft(boolean memberLeft) {
        this.memberLeft = memberLeft;
    }

    public boolean isMemberRemoved() {
        return memberRemoved;
    }

    public void setMemberRemoved(boolean memberRemoved) {
        this.memberRemoved = memberRemoved;
    }

    public boolean isMemberAccessRemoved() {
        return memberAccessRemoved;
    }

    public void setMemberAccessRemoved(boolean memberAccessRemoved) {
        this.memberAccessRemoved = memberAccessRemoved;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public SocialStatusVO getSocialStatus() {
        return socialStatus;
    }

    public void setSocialStatus(SocialStatusVO socialStatus) {
        this.socialStatus = socialStatus;
    }

    public String getReadDatetime() {
        return readDatetime;
    }

    public void setReadDatetime(String readDatetime) {
        this.readDatetime = readDatetime;
    }

    public String getNameInitials() {
        return nameInitials;
    }

    public void setNameInitials(String nameInitials) {
        this.nameInitials = nameInitials;
    }

    public List<Phone> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<Phone> phoneList) {
        this.phoneList = phoneList;
    }

    public List<Email> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<Email> emailList) {
        this.emailList = emailList;
    }

    public URL getPersonPhotoUrl() {
        return personPhotoUrl;
    }

    public void setPersonPhotoUrl(URL personPhotoUrl) {
        this.personPhotoUrl = personPhotoUrl;
    }
}
