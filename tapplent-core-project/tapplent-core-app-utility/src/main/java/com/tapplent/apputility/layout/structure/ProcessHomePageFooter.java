package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.layout.valueObject.ProcessHomePageFooterVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 15/11/16.
 */
public class ProcessHomePageFooter {
    private String PrcFooterContentPkId;
    private String LoggedInPersonPkId;
    private String PrcFooterContentRuleFkId;
    private String ProcessTypeCodeFkId;
    private String FooterTextG11NBigTxt;

    public String getPrcFooterContentPkId() {
        return PrcFooterContentPkId;
    }

    public void setPrcFooterContentPkId(String prcFooterContentPkId) {
        PrcFooterContentPkId = prcFooterContentPkId;
    }

    public String getLoggedInPersonPkId() {
        return LoggedInPersonPkId;
    }

    public void setLoggedInPersonPkId(String loggedInPersonPkId) {
        LoggedInPersonPkId = loggedInPersonPkId;
    }

    public String getPrcFooterContentRuleFkId() {
        return PrcFooterContentRuleFkId;
    }

    public void setPrcFooterContentRuleFkId(String prcFooterContentRuleFkId) {
        PrcFooterContentRuleFkId = prcFooterContentRuleFkId;
    }

    public String getProcessTypeCodeFkId() {
        return ProcessTypeCodeFkId;
    }

    public void setProcessTypeCodeFkId(String processTypeCodeFkId) {
        ProcessTypeCodeFkId = processTypeCodeFkId;
    }

    public String getFooterTextG11NBigTxt() {
        return FooterTextG11NBigTxt;
    }

    public void setFooterTextG11NBigTxt(String footerTextG11NBigTxt) {
        FooterTextG11NBigTxt = footerTextG11NBigTxt;
    }

    public static ProcessHomePageFooter fromVO(ProcessHomePageFooterVO processHomePageFooterVO){
        ProcessHomePageFooter processHomePageFooter = new ProcessHomePageFooter();
        BeanUtils.copyProperties(processHomePageFooterVO,processHomePageFooter);
        return processHomePageFooter;
    }
}
