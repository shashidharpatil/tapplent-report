package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.layout.valueObject.SocialStatusVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 28/12/16.
 */
public class SocialStatus {
    private String socialStatusCodePkId;
    private String socialStatusNameG11nBigTxt;
    private IconVO socialStatusIconCodeFKId;
    private String overrideFontColourTxt;

    public String getSocialStatusCodePkId() {
        return socialStatusCodePkId;
    }

    public void setSocialStatusCodePkId(String socialStatusCodePkId) {
        this.socialStatusCodePkId = socialStatusCodePkId;
    }

    public String getSocialStatusNameG11nBigTxt() {
        return socialStatusNameG11nBigTxt;
    }

    public void setSocialStatusNameG11nBigTxt(String socialStatusNameG11nBigTxt) {
        this.socialStatusNameG11nBigTxt = socialStatusNameG11nBigTxt;
    }

    public IconVO getSocialStatusIconCodeFKId() {
        return socialStatusIconCodeFKId;
    }

    public void setSocialStatusIconCodeFKId(IconVO socialStatusIconCodeFKId) {
        this.socialStatusIconCodeFKId = socialStatusIconCodeFKId;
    }

    public String getOverrideFontColourTxt() {
        return overrideFontColourTxt;
    }

    public void setOverrideFontColourTxt(String overrideFontColourTxt) {
        this.overrideFontColourTxt = overrideFontColourTxt;
    }

    public static SocialStatus fromVO(SocialStatusVO statusVO) {
        SocialStatus socialStatus =  new SocialStatus();
        BeanUtils.copyProperties(statusVO, socialStatus);
        return socialStatus;
    }
}
