package com.tapplent.apputility.uilayout.structure;

import com.tapplent.platformutility.uilayout.valueobject.ControlActionTargetSectionFilterParamsVO;
import com.tapplent.platformutility.uilayout.valueobject.SectionActionTargetSectionFilterParamsVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 25/04/17.
 */
public class TargetSectionFilterParams {

    private String filterParamsPkId;
    private String targetMTPE;
    private String targetMTPEAlias;
    private String targetParentMTPEAlias;
    private String targetFkRelnWithParent;
    private String attributePathExpn;
    private String operator;
    private String currentMTPE;
    private String currentMTPEAlias;
    private String currentParentMTPEAlias;
    private String currentFkRelnWithParent;
    private String attributeValue;
    private boolean isEliminateResultFromRoot;
    private boolean isAlwaysApplied;
    private boolean isFilter;
    private boolean isHavingClause;

    public String getFilterParamsPkId() {
        return filterParamsPkId;
    }

    public void setFilterParamsPkId(String filterParamsPkId) {
        this.filterParamsPkId = filterParamsPkId;
    }

    public String getTargetMTPE() {
        return targetMTPE;
    }

    public void setTargetMTPE(String targetMTPE) {
        this.targetMTPE = targetMTPE;
    }

    public String getTargetMTPEAlias() {
        return targetMTPEAlias;
    }

    public void setTargetMTPEAlias(String targetMTPEAlias) {
        this.targetMTPEAlias = targetMTPEAlias;
    }

    public String getTargetParentMTPEAlias() {
        return targetParentMTPEAlias;
    }

    public void setTargetParentMTPEAlias(String targetParentMTPEAlias) {
        this.targetParentMTPEAlias = targetParentMTPEAlias;
    }

    public String getTargetFkRelnWithParent() {
        return targetFkRelnWithParent;
    }

    public void setTargetFkRelnWithParent(String targetFkRelnWithParent) {
        this.targetFkRelnWithParent = targetFkRelnWithParent;
    }

    public String getAttributePathExpn() {
        return attributePathExpn;
    }

    public void setAttributePathExpn(String attributePathExpn) {
        this.attributePathExpn = attributePathExpn;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getCurrentMTPE() {
        return currentMTPE;
    }

    public void setCurrentMTPE(String currentMTPE) {
        this.currentMTPE = currentMTPE;
    }

    public String getCurrentMTPEAlias() {
        return currentMTPEAlias;
    }

    public void setCurrentMTPEAlias(String currentMTPEAlias) {
        this.currentMTPEAlias = currentMTPEAlias;
    }

    public String getCurrentParentMTPEAlias() {
        return currentParentMTPEAlias;
    }

    public void setCurrentParentMTPEAlias(String currentParentMTPEAlias) {
        this.currentParentMTPEAlias = currentParentMTPEAlias;
    }

    public String getCurrentFkRelnWithParent() {
        return currentFkRelnWithParent;
    }

    public void setCurrentFkRelnWithParent(String currentFkRelnWithParent) {
        this.currentFkRelnWithParent = currentFkRelnWithParent;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public boolean isAlwaysApplied() {
        return isAlwaysApplied;
    }

    public void setAlwaysApplied(boolean alwaysApplied) {
        isAlwaysApplied = alwaysApplied;
    }

    public boolean isFilter() {
        return isFilter;
    }

    public void setFilter(boolean filter) {
        isFilter = filter;
    }

    public boolean isEliminateResultFromRoot() {
        return isEliminateResultFromRoot;
    }

    public void setEliminateResultFromRoot(boolean eliminateResultFromRoot) {
        isEliminateResultFromRoot = eliminateResultFromRoot;
    }

    public boolean isHavingClause() {
        return isHavingClause;
    }

    public void setHavingClause(boolean havingClause) {
        isHavingClause = havingClause;
    }

    public static TargetSectionFilterParams fromVO(ControlActionTargetSectionFilterParamsVO filterParamsVO) {
        TargetSectionFilterParams filterParams = new TargetSectionFilterParams();
        BeanUtils.copyProperties(filterParamsVO, filterParams);
        return filterParams;
    }

    public static TargetSectionFilterParams fromVO(SectionActionTargetSectionFilterParamsVO filterParamsVO) {
        TargetSectionFilterParams filterParams = new TargetSectionFilterParams();
        BeanUtils.copyProperties(filterParamsVO, filterParams);
        return filterParams;
    }
}
