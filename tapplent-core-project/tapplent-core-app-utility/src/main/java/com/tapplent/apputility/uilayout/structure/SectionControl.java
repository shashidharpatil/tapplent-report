package com.tapplent.apputility.uilayout.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.apputility.layout.structure.ControlEvent;
import com.tapplent.apputility.layout.structure.LayoutPermissions;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.uilayout.valueobject.SectionControlVO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 25/04/17.
 */
public class SectionControl {

    private String controlPkId;
    private String sectionInstanceFkId;
    @JsonIgnore
    private String sectionMasterCode;
    private String controlType;
    private IconVO controlTypeIcon;
    private IconVO controlOnStateIcon;
    private IconVO controlOffStateIcon;
    private String attributePathExpn;
    private boolean isStaticControl;
    private boolean isAggregateControl;
    private String applicableGroupByControls;
    private String itemStatusValue;
    private String attributeFilterExpn;
    private String additionalAttributePathExpn;
    private String associatedRelnControlId;
    private boolean compressAttachment;
    private String statusColourAttrPathExpn;
    private String baseTemplateCode;
    private String dispLabel;
    private IconVO dispIcon;
    private String labelValue;
    private IconVO labelIcon;
    private String labelValueSeparator;
    private boolean showLabelBelow;
    private String mtPE;
    private String mtPEAlias;
    private String parentMTPEAlias;
    private String fkRelnWithParent;
    private String parentMTPEAliasHierarchy;
    private String seqNumberTxt;
    private int detailsDisplayRowNumber;
    private boolean displayInForeground;
    private String defaultOnLoadSort;
    private String orderByValues;
    private boolean isGroupByControl;
    private int groupByControlSequence;
    private String groupByHavingFilterExpression;
    private String associatedGroupByCtrlForAddlAttrPath;
    private boolean isStickyHdr;
    private int stickyHdrSeqNum;
    private String stickyHeaderOrder;
    private boolean isEditable;
    private boolean isDataEmpty;
    private boolean isApplyPagination;
    private int defaultPaginationSize;
    private boolean isEliminateResultFromRoot;
    private boolean isHavingClause;
    private boolean isProbableRelationFilter;
    private boolean isHideAtInitialLoad;
    private String orgChartFilterLabelID;
    private String textJustificationCode;
    private String themeSize;
    private String fontStyle;
    private String fontTextOverflowCode;
    private String fontCase;
    private String fontColour;
    private String onStateFontColour;
    private String detailsLabelFontColour;
    private String backgroundColour;
    private String onStateBackgroundColour;
    private String offStateBackgroundColour;
    private boolean isMandatory;
    private String defaultValue;
    private boolean applySortToRoot;
    @JsonIgnore
    private String ratingScaleID;
    private RatingScale ratingScale;
    @JsonIgnore
    private String sUMtPE;
    @JsonIgnore
    private String sUMtPEAlias;
    @JsonIgnore
    private String sUParentMTPEAlias;
    @JsonIgnore
    private String suFkRelnWithParent;
    @JsonIgnore
    private String sUAttributePathExpn;
    private boolean isAccessControlGoverned;
    private boolean isCurrentCreateAllowed;
    private boolean isFutureCreateAllowed;
    private boolean isHistoryCreateAllowed;
    private String artefactsType;
    private List<ControlStaticValues> staticValues = new ArrayList<>();;
    private List<ControlInstanceFilters> controlFilters = new ArrayList<>();;
    private List<ControlActionInstance> actions = new ArrayList<>();
    private List<ControlEventInstance> events = new ArrayList<>();
    private List<LayoutPermissions> layoutPermissions = new ArrayList<>();

    public String getControlPkId() {
        return controlPkId;
    }

    public void setControlPkId(String controlPkId) {
        this.controlPkId = controlPkId;
    }

    public String getSectionInstanceFkId() {
        return sectionInstanceFkId;
    }

    public void setSectionInstanceFkId(String sectionInstanceFkId) {
        this.sectionInstanceFkId = sectionInstanceFkId;
    }

    public String getControlType() {
        return controlType;
    }

    public void setControlType(String controlType) {
        this.controlType = controlType;
    }

    public String getAttributePathExpn() {
        return attributePathExpn;
    }

    public void setAttributePathExpn(String attributePathExpn) {
        this.attributePathExpn = attributePathExpn;
    }

    public String getSectionMasterCode() {
        return sectionMasterCode;
    }

    public void setSectionMasterCode(String sectionMasterCode) {
        this.sectionMasterCode = sectionMasterCode;
    }

    public boolean isStaticControl() {
        return isStaticControl;
    }

    public void setStaticControl(boolean staticControl) {
        isStaticControl = staticControl;
    }

    public boolean isAggregateControl() {
        return isAggregateControl;
    }

    public void setAggregateControl(boolean aggregateControl) {
        isAggregateControl = aggregateControl;
    }

    public String getLabelValueSeparator() {
        return labelValueSeparator;
    }

    public void setLabelValueSeparator(String labelValueSeparator) {
        this.labelValueSeparator = labelValueSeparator;
    }

    public String getItemStatusValue() {
        return itemStatusValue;
    }

    public void setItemStatusValue(String itemStatusValue) {
        this.itemStatusValue = itemStatusValue;
    }

    public String getAttributeFilterExpn() {
        return attributeFilterExpn;
    }

    public void setAttributeFilterExpn(String attributeFilterExpn) {
        this.attributeFilterExpn = attributeFilterExpn;
    }

    public String getAdditionalAttributePathExpn() {
        return additionalAttributePathExpn;
    }

    public void setAdditionalAttributePathExpn(String additionalAttributePathExpn) {
        this.additionalAttributePathExpn = additionalAttributePathExpn;
    }

    public String getAssociatedRelnControlId() {
        return associatedRelnControlId;
    }

    public void setAssociatedRelnControlId(String associatedRelnControlId) {
        this.associatedRelnControlId = associatedRelnControlId;
    }

    public boolean isCompressAttachment() {
        return compressAttachment;
    }

    public void setCompressAttachment(boolean compressAttachment) {
        this.compressAttachment = compressAttachment;
    }

    public String getStatusColourAttrPathExpn() {
        return statusColourAttrPathExpn;
    }

    public void setStatusColourAttrPathExpn(String statusColourAttrPathExpn) {
        this.statusColourAttrPathExpn = statusColourAttrPathExpn;
    }

    public String getBaseTemplateCode() {
        return baseTemplateCode;
    }

    public void setBaseTemplateCode(String baseTemplateCode) {
        this.baseTemplateCode = baseTemplateCode;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getParentMTPEAlias() {
        return parentMTPEAlias;
    }

    public void setParentMTPEAlias(String parentMTPEAlias) {
        this.parentMTPEAlias = parentMTPEAlias;
    }

    public String getFkRelnWithParent() {
        return fkRelnWithParent;
    }

    public void setFkRelnWithParent(String fkRelnWithParent) {
        this.fkRelnWithParent = fkRelnWithParent;
    }

    public String getSeqNumberTxt() {
        return seqNumberTxt;
    }

    public void setSeqNumberTxt(String seqNumberTxt) {
        this.seqNumberTxt = seqNumberTxt;
    }

    public int getDetailsDisplayRowNumber() {
        return detailsDisplayRowNumber;
    }

    public void setDetailsDisplayRowNumber(int detailsDisplayRowNumber) {
        this.detailsDisplayRowNumber = detailsDisplayRowNumber;
    }

    public boolean isDisplayInForeground() {
        return displayInForeground;
    }

    public void setDisplayInForeground(boolean displayInForeground) {
        this.displayInForeground = displayInForeground;
    }

    public String getDefaultOnLoadSort() {
        return defaultOnLoadSort;
    }

    public void setDefaultOnLoadSort(String defaultOnLoadSort) {
        this.defaultOnLoadSort = defaultOnLoadSort;
    }

    public String getOrderByValues() {
        return orderByValues;
    }

    public void setOrderByValues(String orderByValues) {
        this.orderByValues = orderByValues;
    }

    public boolean isGroupByControl() {
        return isGroupByControl;
    }

    public void setGroupByControl(boolean groupByControl) {
        isGroupByControl = groupByControl;
    }

    public int getGroupByControlSequence() {
        return groupByControlSequence;
    }

    public void setGroupByControlSequence(int groupByControlSequence) {
        this.groupByControlSequence = groupByControlSequence;
    }

    public String getGroupByHavingFilterExpression() {
        return groupByHavingFilterExpression;
    }

    public void setGroupByHavingFilterExpression(String groupByHavingFilterExpression) {
        this.groupByHavingFilterExpression = groupByHavingFilterExpression;
    }

    public boolean isStickyHdr() {
        return isStickyHdr;
    }

    public void setStickyHdr(boolean stickyHdr) {
        isStickyHdr = stickyHdr;
    }

    public String getTextJustificationCode() {
        return textJustificationCode;
    }

    public void setTextJustificationCode(String textJustificationCode) {
        this.textJustificationCode = textJustificationCode;
    }

    public String getThemeSize() {
        return themeSize;
    }

    public void setThemeSize(String themeSize) {
        this.themeSize = themeSize;
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    public String getFontTextOverflowCode() {
        return fontTextOverflowCode;
    }

    public void setFontTextOverflowCode(String fontTextOverflowCode) {
        this.fontTextOverflowCode = fontTextOverflowCode;
    }

    public String getFontCase() {
        return fontCase;
    }

    public void setFontCase(String fontCase) {
        this.fontCase = fontCase;
    }

    public String getFontColour() {
        return fontColour;
    }

    public void setFontColour(String fontColour) {
        this.fontColour = fontColour;
    }

    public String getBackgroundColour() {
        return backgroundColour;
    }

    public void setBackgroundColour(String backgroundColour) {
        this.backgroundColour = backgroundColour;
    }

    public List<ControlStaticValues> getStaticValues() {
        return staticValues;
    }

    public void setStaticValues(List<ControlStaticValues> staticValues) {
        this.staticValues = staticValues;
    }

    public List<ControlInstanceFilters> getControlFilters() {
        return controlFilters;
    }

    public void setControlFilters(List<ControlInstanceFilters> controlFilters) {
        this.controlFilters = controlFilters;
    }

    public List<ControlActionInstance> getActions() {
        return actions;
    }

    public void setActions(List<ControlActionInstance> actions) {
        this.actions = actions;
    }

    public String getParentMTPEAliasHierarchy() {
        return parentMTPEAliasHierarchy;
    }

    public void setParentMTPEAliasHierarchy(String parentMTPEAliasHierarchy) {
        this.parentMTPEAliasHierarchy = parentMTPEAliasHierarchy;
    }

    public String getAssociatedGroupByCtrlForAddlAttrPath() {
        return associatedGroupByCtrlForAddlAttrPath;
    }

    public void setAssociatedGroupByCtrlForAddlAttrPath(String associatedGroupByCtrlForAddlAttrPath) {
        this.associatedGroupByCtrlForAddlAttrPath = associatedGroupByCtrlForAddlAttrPath;
    }

    public String getApplicableGroupByControls() {
        return applicableGroupByControls;
    }

    public void setApplicableGroupByControls(String applicableGroupByControls) {
        this.applicableGroupByControls = applicableGroupByControls;
    }

    public IconVO getControlTypeIcon() {
        return controlTypeIcon;
    }

    public void setControlTypeIcon(IconVO controlTypeIcon) {
        this.controlTypeIcon = controlTypeIcon;
    }

    public String getOnStateFontColour() {
        return onStateFontColour;
    }

    public void setOnStateFontColour(String onStateFontColour) {
        this.onStateFontColour = onStateFontColour;
    }

    public String getDetailsLabelFontColour() {
        return detailsLabelFontColour;
    }

    public void setDetailsLabelFontColour(String detailsLabelFontColour) {
        this.detailsLabelFontColour = detailsLabelFontColour;
    }

    public int getStickyHdrSeqNum() {
        return stickyHdrSeqNum;
    }

    public void setStickyHdrSeqNum(int stickyHdrSeqNum) {
        this.stickyHdrSeqNum = stickyHdrSeqNum;
    }

    public String getStickyHeaderOrder() {
        return stickyHeaderOrder;
    }

    public void setStickyHeaderOrder(String stickyHeaderOrder) {
        this.stickyHeaderOrder = stickyHeaderOrder;
    }

    public String getOrgChartFilterLabelID() {
        return orgChartFilterLabelID;
    }

    public void setOrgChartFilterLabelID(String orgChartFilterLabelID) {
        this.orgChartFilterLabelID = orgChartFilterLabelID;
    }

    public String getDispLabel() {
        return dispLabel;
    }

    public void setDispLabel(String dispLabel) {
        this.dispLabel = dispLabel;
    }

    public IconVO getDispIcon() {
        return dispIcon;
    }

    public void setDispIcon(IconVO dispIcon) {
        this.dispIcon = dispIcon;
    }

    public String getLabelValue() {
        return labelValue;
    }

    public void setLabelValue(String labelValue) {
        this.labelValue = labelValue;
    }

    public IconVO getLabelIcon() {
        return labelIcon;
    }

    public void setLabelIcon(IconVO labelIcon) {
        this.labelIcon = labelIcon;
    }

    public IconVO getControlOnStateIcon() {
        return controlOnStateIcon;
    }

    public void setControlOnStateIcon(IconVO controlOnStateIcon) {
        this.controlOnStateIcon = controlOnStateIcon;
    }

    public IconVO getControlOffStateIcon() {
        return controlOffStateIcon;
    }

    public void setControlOffStateIcon(IconVO controlOffStateIcon) {
        this.controlOffStateIcon = controlOffStateIcon;
    }

    public String getOnStateBackgroundColour() {
        return onStateBackgroundColour;
    }

    public void setOnStateBackgroundColour(String onStateBackgroundColour) {
        this.onStateBackgroundColour = onStateBackgroundColour;
    }

    public String getOffStateBackgroundColour() {
        return offStateBackgroundColour;
    }

    public void setOffStateBackgroundColour(String offStateBackgroundColour) {
        this.offStateBackgroundColour = offStateBackgroundColour;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    public boolean isDataEmpty() {
        return isDataEmpty;
    }

    public void setDataEmpty(boolean dataEmpty) {
        isDataEmpty = dataEmpty;
    }

    public boolean isApplyPagination() {
        return isApplyPagination;
    }

    public void setApplyPagination(boolean applyPagination) {
        isApplyPagination = applyPagination;
    }

    public boolean isEliminateResultFromRoot() {
        return isEliminateResultFromRoot;
    }

    public void setEliminateResultFromRoot(boolean eliminateResultFromRoot) {
        isEliminateResultFromRoot = eliminateResultFromRoot;
    }

    public int getDefaultPaginationSize() {
        return defaultPaginationSize;
    }

    public void setDefaultPaginationSize(int defaultPaginationSize) {
        this.defaultPaginationSize = defaultPaginationSize;
    }

    public boolean isHavingClause() {
        return isHavingClause;
    }

    public void setHavingClause(boolean havingClause) {
        isHavingClause = havingClause;
    }

    public boolean isProbableRelationFilter() {
        return isProbableRelationFilter;
    }

    public void setProbableRelationFilter(boolean probableRelationFilter) {
        isProbableRelationFilter = probableRelationFilter;
    }

    public String getsUMtPE() {
        return sUMtPE;
    }

    public void setsUMtPE(String sUMtPE) {
        this.sUMtPE = sUMtPE;
    }

    public String getsUMtPEAlias() {
        return sUMtPEAlias;
    }

    public void setsUMtPEAlias(String sUMtPEAlias) {
        this.sUMtPEAlias = sUMtPEAlias;
    }

    public String getsUParentMTPEAlias() {
        return sUParentMTPEAlias;
    }

    public void setsUParentMTPEAlias(String sUParentMTPEAlias) {
        this.sUParentMTPEAlias = sUParentMTPEAlias;
    }

    public String getSuFkRelnWithParent() {
        return suFkRelnWithParent;
    }

    public void setSuFkRelnWithParent(String suFkRelnWithParent) {
        this.suFkRelnWithParent = suFkRelnWithParent;
    }

    public String getsUAttributePathExpn() {
        return sUAttributePathExpn;
    }

    public void setsUAttributePathExpn(String sUAttributePathExpn) {
        this.sUAttributePathExpn = sUAttributePathExpn;
    }

    public List<ControlEventInstance> getEvents() {
        return events;
    }

    public void setEvents(List<ControlEventInstance> events) {
        this.events = events;
    }

    public RatingScale getRatingScale() {
        return ratingScale;
    }

    public void setRatingScale(RatingScale ratingScale) {
        this.ratingScale = ratingScale;
    }

    public String getRatingScaleID() {
        return ratingScaleID;
    }

    public void setRatingScaleID(String ratingScaleID) {
        this.ratingScaleID = ratingScaleID;
    }

    public boolean isHideAtInitialLoad() {
        return isHideAtInitialLoad;
    }

    public void setHideAtInitialLoad(boolean hideAtInitialLoad) {
        isHideAtInitialLoad = hideAtInitialLoad;
    }

    public boolean isAccessControlGoverned() {
        return isAccessControlGoverned;
    }

    public void setAccessControlGoverned(boolean accessControlGoverned) {
        isAccessControlGoverned = accessControlGoverned;
    }

    public boolean isCurrentCreateAllowed() {
        return isCurrentCreateAllowed;
    }

    public void setCurrentCreateAllowed(boolean currentCreateAllowed) {
        isCurrentCreateAllowed = currentCreateAllowed;
    }

    public boolean isFutureCreateAllowed() {
        return isFutureCreateAllowed;
    }

    public void setFutureCreateAllowed(boolean futureCreateAllowed) {
        isFutureCreateAllowed = futureCreateAllowed;
    }

    public boolean isHistoryCreateAllowed() {
        return isHistoryCreateAllowed;
    }

    public void setHistoryCreateAllowed(boolean historyCreateAllowed) {
        isHistoryCreateAllowed = historyCreateAllowed;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public List<LayoutPermissions> getLayoutPermissions() {
        return layoutPermissions;
    }

    public void setLayoutPermissions(List<LayoutPermissions> layoutPermissions) {
        this.layoutPermissions = layoutPermissions;
    }

    public boolean isApplySortToRoot() {
        return applySortToRoot;
    }

    public void setApplySortToRoot(boolean applySortToRoot) {
        this.applySortToRoot = applySortToRoot;
    }

    public String getArtefactsType() {
        return artefactsType;
    }

    public void setArtefactsType(String artefactsType) {
        this.artefactsType = artefactsType;
    }

    public boolean isShowLabelBelow() {
        return showLabelBelow;
    }

    public void setShowLabelBelow(boolean showLabelBelow) {
        this.showLabelBelow = showLabelBelow;
    }

    public static SectionControl fromVO(SectionControlVO sectionControlVO){
        SectionControl sectionControl = new SectionControl();
        BeanUtils.copyProperties(sectionControlVO, sectionControl);
        return sectionControl;
    }
}
