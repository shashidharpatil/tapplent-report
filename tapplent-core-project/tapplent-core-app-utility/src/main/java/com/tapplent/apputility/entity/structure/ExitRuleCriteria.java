package com.tapplent.apputility.entity.structure;

public class ExitRuleCriteria {
    private String versionId;
    private String exitRuleCriteriaCode;
    private String exitRuleCriteriaIcon;
    private String exitRuleCriteriaName;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getExitRuleCriteriaCode() {
        return exitRuleCriteriaCode;
    }

    public void setExitRuleCriteriaCode(String exitRuleCriteriaCode) {
        this.exitRuleCriteriaCode = exitRuleCriteriaCode;
    }

    public String getExitRuleCriteriaIcon() {
        return exitRuleCriteriaIcon;
    }

    public void setExitRuleCriteriaIcon(String exitRuleCriteriaIcon) {
        this.exitRuleCriteriaIcon = exitRuleCriteriaIcon;
    }

    public String getExitRuleCriteriaName() {
        return exitRuleCriteriaName;
    }

    public void setExitRuleCriteriaName(String exitRuleCriteriaName) {
        this.exitRuleCriteriaName = exitRuleCriteriaName;
    }
}
