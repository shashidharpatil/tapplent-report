package com.tapplent.apputility.utility.structure;

import com.tapplent.apputility.layout.structure.LayoutPermissions;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.hierarchy.structure.Relation;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

import java.util.List;
import java.util.Map;

import static com.tapplent.platformutility.common.util.TapplentServiceLookupUtil.getPersonRepository;

/**
 * Created by tapplent on 23/11/17.
 */
public class AppUtil {

    public static Boolean isElementApplicableForTheUser(LayoutPermissions layoutPermission, String subjectUserID) {
        // Now what do we have to do?
        // We have to read the permission type and all
        String loggedInUser = TenantContextHolder.getUserContext().getPersonId();
        Boolean finalResult = false;
        if(layoutPermission.getPermissionType().equals("APPLICABLE_GROUPS")){
            // Now check for logged in user group
            List<String> loggedInUserGroupIDs = TenantAwareCache.getPersonRepository().getPersonDetails(loggedInUser).getPersonGroups().getGroupIds();
            String[] accessGroupValues = layoutPermission.getPermissionValue().split("\\|");
            for(String group : loggedInUserGroupIDs){
                for(String accessGroup : accessGroupValues){
                    if(group.equals(accessGroup)){
                        // Here check for subject user qualifier group.
                        if(StringUtil.isDefined(layoutPermission.getSubjectUserQualifier())){
                            String[] subjectUserQualifierGroups = layoutPermission.getSubjectUserQualifier().split("\\|");
                            if(StringUtil.isDefined(subjectUserID)){
                                List<String> subjectUserGroupIDs = TenantAwareCache.getPersonRepository().getPersonDetails(subjectUserID).getPersonGroups().getGroupIds();
                                // Now we have to use subject user id.
                                for(String subjectUserQualifierGroup : subjectUserQualifierGroups){
                                    for(String subjectUserGroupID : subjectUserGroupIDs){
                                        if(subjectUserGroupID.equals(subjectUserQualifierGroup)){
                                            return true;
                                        }
                                    }
                                }
                            }else{
                                try {
                                    throw new Exception("Subject user not defined, required in permission");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }else{
                            return true;
                        }
                    }
                }
            }
        }else if(layoutPermission.getPermissionType().equals("SUBJECT_USER_RELATIONSHIP_TYPE")){
            if(StringUtil.isDefined(subjectUserID)){
                Map<String, Map<String, List<Relation>>> relationMap = TenantAwareCache.getPersonRepository().getPersonRelationMap(loggedInUser, subjectUserID);
                List<Relation> relations = relationMap.get(loggedInUser).get(subjectUserID);
                if(relations!=null){
                    // Now what are the relations between two people.
                    // Now check if this relation ship exists or not.
                    String[] permissionRelations = layoutPermission.getPermissionValue().split("\\|");
                    for(String permissionRelation : permissionRelations){
                        for(Relation relation : relations){
                            if(permissionRelation.equals(relation)){
                                if(StringUtil.isDefined(layoutPermission.getSubjectUserQualifier())){
                                    String[] subjectUserQualifierGroups = layoutPermission.getSubjectUserQualifier().split("\\|");
                                    List<String> subjectUserGroupIDs = TenantAwareCache.getPersonRepository().getPersonDetails(subjectUserID).getPersonGroups().getGroupIds();
                                    // Now we have to use subject user id.
                                    for(String subjectUserQualifierGroup : subjectUserQualifierGroups){
                                        for(String subjectUserGroupID : subjectUserGroupIDs){
                                            if(subjectUserGroupID.equals(subjectUserQualifierGroup)){
                                                return true;
                                            }
                                        }
                                    }
                                }else{
                                    return true;
                                }
                            }
                        }
                    }
                }
            }else{
                try {
                    throw new Exception("Subject user not defined, required in permission");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return finalResult;
    }
}
