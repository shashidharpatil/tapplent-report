package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.layout.valueObject.LicencedLocalesVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 09/08/17.
 */
public class LicencedLocales {
    private String locale;
    private String localeName;
    private String languageCode;
    private String langMapTxt;
    private String oemName;
    private boolean isGlobalDefault;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocaleName() {
        return localeName;
    }

    public void setLocaleName(String localeName) {
        this.localeName = localeName;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getLangMapTxt() {
        return langMapTxt;
    }

    public void setLangMapTxt(String langMapTxt) {
        this.langMapTxt = langMapTxt;
    }

    public String getOemName() {
        return oemName;
    }

    public void setOemName(String oemName) {
        this.oemName = oemName;
    }

    public boolean isGlobalDefault() {
        return isGlobalDefault;
    }

    public void setGlobalDefault(boolean globalDefault) {
        isGlobalDefault = globalDefault;
    }

    public static LicencedLocales fromVO(LicencedLocalesVO licencedLocalesVO) {
        LicencedLocales licencedLocales = new LicencedLocales();
        BeanUtils.copyProperties(licencedLocalesVO, licencedLocales);
        return licencedLocales;
    }
}
