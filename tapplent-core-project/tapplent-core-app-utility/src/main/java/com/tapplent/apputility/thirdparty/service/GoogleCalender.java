package com.tapplent.apputility.thirdparty.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.Base64;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.Resource;
import com.tapplent.apputility.data.structure.PEData;
import com.tapplent.apputility.data.structure.UnitRequestData;
import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.GmailTokenUtil;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.etl.dao.EtlDAOImpl;
import com.tapplent.platformutility.thirdPartyApp.gmail.Encoder;
import com.tapplent.platformutility.thirdPartyApp.gmail.EventAttchment;
import com.tapplent.platformutility.thirdPartyApp.gmail.Meeting;

public class GoogleCalender {
	EtlDAOImpl etlDAO = null;
	//This function will get the all events for the specific calendar..
	public String getSyncToken(String access_token) throws ClientProtocolException, IOException, ParseException, JSONException{	
		
		String calenderId = "primary";
		StringBuilder url = new StringBuilder("https://www.googleapis.com/calendar/v3/calendars/");
		url.append(calenderId).append("/events");
		HttpGet get = new HttpGet(url.toString());
		get.addHeader("Content-Type", "application/json");
		get.addHeader("Authorization", "Bearer "+access_token);
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(get);
	    JSONObject res = new JSONObject(EntityUtils.toString(response.getEntity()));
	    String syncToken = res.getString("nextSyncToken");
	    return syncToken;
	}
	//This function will create the events in the specified calendar
	private String createMeeting(Meeting meeting,String accessToken) throws JSONException, ClientProtocolException, IOException{
		//https://www.googleapis.com/calendar/v3/calendars/primary/events?maxAttendees=2&sendNotifications=true&supportsAttachments=true&key={YOUR_API_KEY}
		StringBuilder url = new StringBuilder("https://www.googleapis.com/calendar/v3/calendars/primary/events?maxAttendees=2&sendNotifications=true&supportsAttachments=true");
		HttpPost post = new HttpPost(url.toString());
		 String meetingId = meeting.getMeetingPkId();
		 String meetingTitle =meeting.getMeetingTitleG11NBigTxt();
		 String meetingDesc = meeting.getMeetingDescG11NBigTxt();
		 Timestamp meetingStartDate = meeting.getMeetingStartDatetime();
		 Timestamp meetingEndDate = meeting.getMeetingEndDatetime();
		 String eventLocation = meeting.getLocationTxt();
//		 List<EventAttchment> attachments =etlDAO.getMeetingAttachments(meetingId);
//		 meeting.setAttachmentIds(attachments);
//		
//		 for(EventAttchment attachment:attachments){
//			 String attachmentId = attachment.getArtefactAttach();
//			 String attachmentName = attachment.getAttachmentNameG11nBigTxt();
//			 Map<String,Object> attachmentMap = getAttchment(attachmentId);
//			 String mimeType = attachmentMap.get("ContentType").toString();
//			 InputStream is = (InputStream) attachmentMap.get("InputStream");
//			 String fileUrl = uploadToDrive(is,mimeType,attachmentName,accessToken);
//			 
//		 }
		post.addHeader("Content-Type", "application/json");
		post.addHeader("Authorization", "Bearer "+accessToken);
		JSONObject jsonRequestBody = new JSONObject();
		JSONObject endDateObject = new JSONObject();
		endDateObject.put("date",meetingEndDate);
		jsonRequestBody.put("end", endDateObject);
		JSONObject startDateObject = new JSONObject();
		startDateObject.put("date",meetingStartDate);
		jsonRequestBody.put("start", startDateObject);
		jsonRequestBody.put("description", meetingDesc);
		jsonRequestBody.put("summary", meetingTitle);
		//jsonRequestBody.put("location", meetingLocation);
		JSONArray attachmentss = new JSONArray();
		String fileUrls[] = 
			{"https://docs.google.com/document/d/1mIZNYyiqFb5VJl9fGqC-0wzcLix4HEP7uMLrfmZTScA/edit?usp=drivesdk"};
		for(int i = 0;i<fileUrls.length;i++){
			JSONObject attachmentsss = new JSONObject ();
			attachmentsss.put("fileUrl", fileUrls[i]);
			attachmentsss.put("mimeType", "application/vnd.google-apps.document");
			attachmentsss.put("title","shashi");
			//attachmentss.add(attachment);
		}
		jsonRequestBody.put("attachments","");
		JSONArray attendees = new JSONArray();
		String emailIds[] = {"ram.subramanian@tapplent.com"};
		for(int i = 0;i<emailIds.length;i++){
			JSONObject attende = new JSONObject ();
			attende.put("email", emailIds[i]);
			attendees.add(attende);
		}
		jsonRequestBody.put("attendees",attendees);
		StringEntity entity = new StringEntity(jsonRequestBody.toString());
		post.setEntity(entity);
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(post);
	    int status_code = response.getStatusLine().getStatusCode();
	    JSONObject res = new JSONObject(EntityUtils.toString(response.getEntity()));
	   // String meetingId = res.getString("id");
	    return meetingId;
		
	}

	private String uploadToDrive(InputStream is, String mimeType,String fileName,String accesstoken) throws IOException, ParseException, JSONException {
		
		HttpGet get = new HttpGet("https://www.googleapis.com/drive/v3/files/generateIds?count=1");
		get.addHeader("Authorization","Bearer "+accesstoken);
		get.addHeader("Content-Type","application/json");
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response;
		response = client.execute(get);
		JSONObject responseJson = new JSONObject(EntityUtils.toString(response.getEntity()));
		String fileId = "";
		if(responseJson.has("ids")){
			org.json.JSONArray fileIds = responseJson.getJSONArray("ids");
			 fileId = fileIds.getString(0);
		}

		StringBuilder url = new StringBuilder("https://www.googleapis.com/upload/drive/v3/files/");
		url.append(fileId).append("?uploadType=multipart");
		HttpPut post = new HttpPut(url.toString());
		post.addHeader("Content-Type", "message/rfc822");
		post.addHeader("Authorization", "Bearer "+accesstoken);
		byte[] bytes = IOUtils.toByteArray(is);
		String rawData = Base64.encodeAsString(bytes);
//		StringBuilder sb = new StringBuilder(
//				  "--foo_bar\r\n" +
//				  "Content-Type: application/json; charset=UTF-8\r\n\r\n" +
//				  "{\r\n"+
//				  "fileName:\r"+fileName+"\r\n\r\n" +
//				  "--foo_bar\r\n" + 
//				  "}\r\n"+
//				  "Content-Type:" +mimeType+"\r\n"+
//				  "MIME-Version: 1.0\r\n"+
//				  "Content-Transfer-Encoding: base64\r\n"+
//				  "Content-Disposition: attachment; filename=\""+fileName+"\"\r\n\r\n"+
//				  
//				  rawData +"\r\n\r\n"+
//
//				  "--foo_bar--" );
		StringBuilder sb = new StringBuilder(
				  "Content-Type: multipart/mixed; boundary=\"foo_bar\"\r\n\r\n" +

				  "--foo_bar\r\n" +
				  "Content-Type: text/plain; charset=UTF-8\r\n\r\n" +

				  "ShashiImage\r\n\r\n" +

				  "--foo_bar\r\n" +
				  
				  "Content-Type: "+mimeType+"\r\n" +
				  "MIME-Version: 1.0\r\n"+
				  "Content-Transfer-Encoding: base64\r\n"+
				  "Content-Disposition: attachment; filename=\""+fileName+"\"\r\n\r\n"+
				  
					rawData +"\r\n\r\n"+

				  "--foo_bar--" );
		StringEntity entity = new StringEntity(sb.toString());
		post.setEntity(entity);
		HttpClient client2 = HttpClientBuilder.create().build();
	    HttpResponse response2;
	    response2 = client2.execute(post);
	    int status_code = response2.getStatusLine().getStatusCode();
		return null;
	}
	//This function will update the specific event 
	private void updateGoogleMeeting(String id,Meeting meeting) throws JSONException, ClientProtocolException, IOException{
		
//		String access_token = "";
//		String eventId = "";
//		StringBuilder url = new StringBuilder("https://www.googleapis.com/calendar/v3/calendars/primary/events/");
//		url.append(eventId);
//		HttpPut put = new HttpPut(url.toString());
//		put.addHeader("Content-Type", "application/json");
//		put.addHeader("Authorization", "Bearer "+access_token);
//		JSONObject jsonRequestBody = EntityUtils.
////		StringEntity entity = new StringEntity(jsonRequestBody.toString());
//		put.setEntity(entity);
//		HttpClient client = HttpClientBuilder.create().build();
//	    HttpResponse response;
//	    response = client.execute(put);
//	    int status_code = response.getStatusLine().getStatusCode();	
	}
	//This function will delete the specific event
	public static void deleteEvent() throws JSONException, ClientProtocolException, IOException{
		String access_token = "";
		String eventId = "3i2q2u2mmkrr16u97u1iktnfk0";
		StringBuilder url = new StringBuilder("https://www.googleapis.com/calendar/v3/calendars/primary/events/");
		url.append(eventId);
		HttpDelete delete = new HttpDelete(url.toString());
		delete.addHeader("Content-Type", "application/json");
		delete.addHeader("Authorization", "Bearer "+access_token);
		delete.addHeader("sendNotifications", "true");
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(delete);
	    int status_code = response.getStatusLine().getStatusCode();	
	}
	
//	public String calenderSynch(String personPkId) throws ClientProtocolException, IOException, ParseException, JSONException{
//		shinga();
//		Map<String,Object> columnNameToValueMap = everNoteDAO.isSynchNeeded(personPkId,"T_PFM_TAP_MTG_SYNCH");
//		boolean isSyncNeeded = (boolean) columnNameToValueMap.get("isSynchNeeded");
//		String lastSynchToken = (String) columnNameToValueMap.get("lastSyncId");
//		String integrationAppFkId = (String) columnNameToValueMap.get("integrationAppName");
//		String meetingSyncPkId = (String) columnNameToValueMap.get("syncPkId");
//		//Use this meeting Sync id to retrieve list of meeting Ids which have to be fetched
//		List<String> meetingPkId = etlDAO.getEventPkId(meetingSyncPkId,"T_PFM_TAP_MTG_SYNCH_DTLS","MTG_FK_ID");
//		String accessToken = etlDAO.getToken(integrationAppFkId,personPkId);
//
//
//		
//		if(isSyncNeeded){
//			
//			if(lastSynchToken != null){
//					//// Something in the Calender account has changed, so search for meetongs...Perform Incremental Synch
//					lastSynchToken = performIncrementalSync(lastSynchToken,accessToken,false);
//			    	  //Strore this token on to the database
//					//Fetch all the notes from the tapplent whose EXTERNAL_REFERENCE_TXT == null
//					
//			    	  etlDAO.updateLastSyncId(personPkId,lastSynchToken,integrationAppFkId,"T_PFM_TAP_MTG_SYNCH");
//			        
//		      }else{
//			    
//			    	  	//Get the Initial Synch Token 
//			    	  lastSynchToken = getSyncToken(accessToken);
//			    	  for(String tapplentMeetingId :meetingPkId){
//			    		  Meeting meeting = etlDAO.getMeeting(tapplentMeetingId);
//			    		  String calenderMeetingId = createMeeting(meeting,accessToken);
//			    		  Map<String ,Object> columnNameValueMap = new HashMap<String,Object>();
//							 columnNameValueMap.put("MTG_INTGN_PK_ID", Common.getUUID());
//							 columnNameValueMap.put("MTG_FK_ID", Util.getStringTobeInserted("TEXT", calenderMeetingId,false));
//							 columnNameValueMap.put("EXTERNAL_INTEGRATION_APP_FK_ID",Util.getStringTobeInserted("TEXT", "GOOGLE_CALENDAR",false));
//							 columnNameValueMap.put("EXTERNAL_REFERENCE_TXT", Util.getStringTobeInserted("TEXT", calenderMeetingId,false));
//							 columnNameValueMap.put("IS_DELETED",Util.getStringTobeInserted("BOOLEAN",false,false));
//							 columnNameValueMap.put("CREATED_DATETIME",Util.getStringTobeInserted("DATE_TIME", "NOW()",false));
//							 etlDAO.insertIntoEvent("T_PFM_TAP_NOTE_INTGN", columnNameValueMap);
//							 etlDAO.updateTheSyncDetails(tapplentMeetingId,"T_PFM_TAP_NOTE_SYNCH_DTLS");
//			    	     }
//						
//						 //Store This SynchToken against the personPkId
//			    	     lastSynchToken = getSyncToken(accessToken);
//						 etlDAO.updateLastSyncId(personPkId,lastSynchToken,integrationAppFkId,"T_PFM_TAP_MTG_SYNCH");
//						 
//		    	  
//		    	  }
//		}
//		
//		return null;
//	}
//	public String performIncrementalSync(String syncToken, String accessToken,boolean isNextPage) throws ClientProtocolException, IOException, ParseException, JSONException {
//		
//		StringBuilder url = new StringBuilder("https://www.googleapis.com/calendar/v3/calendars/primary/events?maxResults=10&singleEvents=true&");
//		if(isNextPage){
//			url.append("pageToken=");
//		}else{
//			url.append("syncToken=");
//		}
//		url.append(syncToken);
//		HttpGet get = new HttpGet(url.toString());
//		get.addHeader("Content-Type", "application/json");
//		get.addHeader("Authorization", "Bearer "+accessToken);
//		HttpClient client = HttpClientBuilder.create().build();
//	    HttpResponse response;
//	    response = client.execute(get);
//	    JSONObject res = new JSONObject(EntityUtils.toString(response.getEntity()));
//	    if(res.has("nextPageToken")){
//	    	//recursive call should be made with nextPageToken
//	    	String pageToken = res.getString("nextPageToken");
//	    	//Retrive the all the events
//	    	performIncrementalSync(pageToken,accessToken,true);
//	    	
//	    	
//	    }else{
//	    	org.json.JSONArray eventsArray = res.getJSONArray("items");
//	    	//retrieve all the events
//	    	for(int i = 0; i<=eventsArray.length()-1;i++){
//	    		JSONObject Googlemeeting = (JSONObject) eventsArray.get(i);
//	    		String googlemeetingId = Googlemeeting.getString("id");
//	    		String tapplentMeetingId = etlDAO.getEventFkIdFromIntegration(googlemeetingId,"T_PFM_TAP_MTG_INTGN","MTG_FK_ID");
//	    		Meeting tapplentMeeting = etlDAO.getMeeting(tapplentMeetingId);
//	    		
//	    		if(tapplentMeetingId != null ){
//					
//					if(Googlemeeting.getString("status").equals("cancelled")){
//						//deleteTheTapplentMeeting(tapplentMeetingId);
//						
//					}else if(tapplentMeeting.isDeleted()){
//						//deleteTheGoogleMeeting(googlemeetingId)
//		    			
//		    		}else{
//						
//							//Retrieve the lastModifiedtime of meeting in tapplent and GoogleCalender
//							Timestamp tapplentMeetingLastModifiedTime = tapplentMeeting.getLastModifiedDateTime();
//							Timestamp googleMeetingLastModifiedTime = Timestamp.valueOf(Googlemeeting.getString("updated"));
//							if(tapplentMeetingLastModifiedTime.after(googleMeetingLastModifiedTime)){
//								//Update the tapplent Task record onto the GoogleTask
//								//updateTheGoogleMeeting(tapplentMeeting,Googlemeeting,accessToken);
//							}else if(googleMeetingLastModifiedTime.after(tapplentMeetingLastModifiedTime)){
//								//Update the google Task record onto Tapplent Task record
//								//updateTheTapplentMeeting(tapplentMeeting,Googlemeeting);
//
//							}else{
//								//Everything is in Sync Don't Do anything...
//							}
//						}
//						
//					}else{
//						
//						insertMeetingIntoTapplent(Googlemeeting,tapplentMeeting);
//					 }
//					
//				}
//	    	syncToken = res.getString("nextSyncToken");
//	  
//	    }
//	    	return syncToken;
//	    }
//	    
		
   private void insertMeetingIntoTapplent(JSONObject googlemeeting, Meeting tapplentMeeting) throws JSONException, MalformedURLException, IOException {
	    	
		//Insert the EverNote into Tapplent...So that u have to insert into Note,NoteAttachment and Note Integration
		Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
		JSONObject meetingStart = googlemeeting.getJSONObject("start");
		JSONObject meetingEnd = googlemeeting.getJSONObject("end");
		if(!googlemeeting.getString("summary").equals("")||googlemeeting.getString("summary") !=null)
			columnNameToValueMap.put("Meeting.MeetingTitle",googlemeeting.getString("summary"));
			columnNameToValueMap.put("Meeting.MeetingDescription",googlemeeting.getString("description")); 
		if(meetingStart.getString("date") != null){
			columnNameToValueMap.put("Meeting.MeetingStartDateTime",meetingStart.getString("date")); 
		}else if(meetingStart.getString("dateTime") != null){
			columnNameToValueMap.put("Meeting.MeetingStartDateTime",meetingStart.getString("dateTime"));
		}
		if(meetingEnd.getString("date") != null){
			columnNameToValueMap.put("Meeting.MeetingEndDateTime",meetingEnd.getString("date")); 
		}else if(meetingEnd.getString("dateTime") != null){
			columnNameToValueMap.put("Meeting.MeetingEndDateTime",meetingEnd.getString("dateTime"));
		}
           columnNameToValueMap.put("Meeting.Location", googlemeeting.getString("location"));
		
		
		PEData rootPEData = new PEData();
		rootPEData.setBtPE("PlatformTemplate.Meeting");
		rootPEData.setData(columnNameToValueMap);
		List<PEData> childrenPEData = new ArrayList<PEData>();
		if(googlemeeting.has("attachments")){
		    org.json.JSONArray attachmentsArray = googlemeeting.getJSONArray("attachments");
		    for(int i = 0; i<=attachmentsArray.length()-1;i++){
		    	JSONObject attachment = (JSONObject) attachmentsArray.get(i);
		    	PEData child1PEData = new PEData();
		    	child1PEData.setBtPE("PlatformTemplate.MeetingAttachment");
				Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
				String attachmentType =attachment.getString("mimeType");
				InputStream is = new URL(attachment.getString("fileUrl")).openStream();
				String attachmentArtifact = Util.uploadAttachmentToS3(attachmentType, is);
				
				columnNameToValueMap1.put("MeetingAttachment.AttachmentArtefactID",attachmentArtifact); 
				columnNameToValueMap1.put("MeetingAttachment.AttachmentThumbnailImage","");
				columnNameToValueMap1.put("MeetingAttachment.AttachmentName",attachment.getString("title"));
				columnNameToValueMap1.put("MeetingAttachment.AttachmentTypeCode",attachmentType);
				child1PEData.setData(columnNameToValueMap1);
				childrenPEData.add(child1PEData);
		    }
		 }
		
		              if(googlemeeting.has("attendees")){
		            	  
		      		    org.json.JSONArray attendeesArray = googlemeeting.getJSONArray("attendees");
		      		    for(int i = 0; i<=attendeesArray.length()-1;i++){
		      		    	JSONObject attendee = (JSONObject) attendeesArray.get(i);
		      		    	PEData child1PEData = new PEData();
		      		    	child1PEData.setBtPE("PlatformTemplate.MeetingAttachment");
		      				Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
		      				String attendeeEmail = attendee.getString("email");
		      				//Find this email in the PERSON table If present then only it should be inserted
		      				//SELECT INVITED_PERSON_FK_ID FROM T_PRN_EU_PERSON WHERE email = "";
		      				columnNameToValueMap1.put("MeetingInvitee.InvitedPersonID",""); 
		      				columnNameToValueMap1.put("MeetingInvitee.AttendeeStatusCode","");
		      				child1PEData.setData(columnNameToValueMap1);
		      				childrenPEData.add(child1PEData);
		      		    }
		      		 }
		PEData child2PEData = new PEData();
		child2PEData.setBtPE("PlatformTemplate.NoteIntegration");
		Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
		columnNameToValueMap2.put("NoteIntegration.IntegrationApp",""); 
		columnNameToValueMap2.put("NoteIntegration.IntegrationAppReferenceID",googlemeeting.getString("id"));
		child2PEData.setData(columnNameToValueMap2);
		childrenPEData.add(child2PEData);

		rootPEData.setChildrenPEData(childrenPEData);

		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(rootPEData);


	}
	public Map<String,Object> getAttchment(String id){
		AmazonS3 amazonS3 = SystemAwareCache.getSystemRepository().getAmazonS3Client();
		S3Object attachment = amazonS3.getObject("testingtapplent/22", id);
		S3ObjectInputStream is = attachment.getObjectContent();
		ObjectMetadata metadata	= attachment.getObjectMetadata();
		Map<String,Object> attachmentMap = new HashMap<String,Object>();
		attachmentMap.put("contentLength", metadata.getContentLength());
		attachmentMap.put("ContentType", metadata.getContentType());
//		attachmentMap.put("MD5Data", metadata.getContentMD5());
		attachmentMap.put("InputStream", is);
		return attachmentMap;
		
	}
	public void shinga() throws ParseException, IOException, JSONException{
		String id = "49A1052C56AFFE78BAB06E669D7F978B";
		 Map<String,Object> attachmentMap = getAttchment(id);
		 String mimeType = attachmentMap.get("ContentType").toString();
		 InputStream is = (InputStream) attachmentMap.get("InputStream");
		 String accessToken = "ya29.Ci_CA9cugHIyticD-d1AAjgoIwErN-0wS0muPZf91hAJ-tnSZVWolv8gm0zUSXMHMA";
		 String fileUrl = uploadToDrive(is,mimeType,"shashi",accessToken);
	}
}
