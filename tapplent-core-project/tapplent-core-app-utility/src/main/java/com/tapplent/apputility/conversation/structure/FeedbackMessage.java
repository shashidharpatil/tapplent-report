package com.tapplent.apputility.conversation.structure;

import com.tapplent.platformutility.common.util.AttachmentObject;

import java.util.Map;

/**
 * Created by Tapplent on 6/16/17.
 */
public class FeedbackMessage {
    private String feedbackPkId;
    private String feedbackGroupId;
    private String feedbackGroupName;
    private String message;
    private String messageAttachId;
    private AttachmentObject messageAttachUrl;
    private boolean isMessageEdited;
    private String messageType;
    private String createdByPersonId;
    private String createdByPersonName;
    private String createdDatetime;
    private String lastModifiedDatetime;
    private String bookmarkedLastModifiedDatetime;
    private String feedbackParentMessageId;
    private FeedbackMessage parentFeedbackMessage;
    private boolean bookmarked;
    private String bookmarkId;
    private boolean memAccessRemoved;
    private boolean memRemoved;
    private boolean memLeft;
    private boolean deleted;

    public String getFeedbackPkId() {
        return feedbackPkId;
    }

    public void setFeedbackPkId(String feedbackPkId) {
        this.feedbackPkId = feedbackPkId;
    }

    public String getFeedbackGroupId() {
        return feedbackGroupId;
    }

    public void setFeedbackGroupId(String feedbackGroupId) {
        this.feedbackGroupId = feedbackGroupId;
    }

    public String getFeedbackGroupName() {
        return feedbackGroupName;
    }

    public void setFeedbackGroupName(String feedbackGroupName) {
        this.feedbackGroupName = feedbackGroupName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageAttachId() {
        return messageAttachId;
    }

    public void setMessageAttachId(String messageAttachId) {
        this.messageAttachId = messageAttachId;
    }

    public AttachmentObject getMessageAttachUrl() {
        return messageAttachUrl;
    }

    public void setMessageAttachUrl(AttachmentObject messageAttachUrl) {
        this.messageAttachUrl = messageAttachUrl;
    }

    public boolean isMessageEdited() {
        return isMessageEdited;
    }

    public void setMessageEdited(boolean messageEdited) {
        isMessageEdited = messageEdited;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getFeedbackParentMessageId() {
        return feedbackParentMessageId;
    }

    public FeedbackMessage getParentFeedbackMessage() {
        return parentFeedbackMessage;
    }

    public void setParentFeedbackMessage(FeedbackMessage parentFeedbackMessage) {
        this.parentFeedbackMessage = parentFeedbackMessage;
    }

    public void setFeedbackParentMessageId(String feedbackParentMessageId) {
        this.feedbackParentMessageId = feedbackParentMessageId;
    }

    public String getCreatedByPersonId() {
        return createdByPersonId;
    }

    public void setCreatedByPersonId(String createdByPersonId) {
        this.createdByPersonId = createdByPersonId;
    }

    public String getCreatedByPersonName() {
        return createdByPersonName;
    }

    public void setCreatedByPersonName(String createdByPersonName) {
        this.createdByPersonName = createdByPersonName;
    }

    public String getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(String createdDatetime) {
        this.createdDatetime = createdDatetime;
    }

    public String getLastModifiedDatetime() {
        return lastModifiedDatetime;
    }

    public void setLastModifiedDatetime(String lastModifiedDatetime) {
        this.lastModifiedDatetime = lastModifiedDatetime;
    }

    public boolean isBookmarked() {
        return bookmarked;
    }

    public void setBookmarked(boolean bookmarked) {
        this.bookmarked = bookmarked;
    }

    public String getBookmarkId() {
        return bookmarkId;
    }

    public void setBookmarkId(String bookmarkId) {
        this.bookmarkId = bookmarkId;
    }

    public boolean isMemAccessRemoved() {
        return memAccessRemoved;
    }

    public void setMemAccessRemoved(boolean memAccessRemoved) {
        this.memAccessRemoved = memAccessRemoved;
    }

    public boolean isMemRemoved() {
        return memRemoved;
    }

    public void setMemRemoved(boolean memRemoved) {
        this.memRemoved = memRemoved;
    }

    public boolean isMemLeft() {
        return memLeft;
    }

    public void setMemLeft(boolean memLeft) {
        this.memLeft = memLeft;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getBookmarkedLastModifiedDatetime() {
        return bookmarkedLastModifiedDatetime;
    }

    public void setBookmarkedLastModifiedDatetime(String bookmarkedLastModifiedDatetime) {
        this.bookmarkedLastModifiedDatetime = bookmarkedLastModifiedDatetime;
    }
}
