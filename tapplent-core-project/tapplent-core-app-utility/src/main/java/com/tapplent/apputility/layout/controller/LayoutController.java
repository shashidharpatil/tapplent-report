package com.tapplent.apputility.layout.controller;

import java.io.*;

import java.net.FileNameMap;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.amazonaws.AmazonClientException;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.tapplent.apputility.layout.structure.*;
import com.tapplent.platformutility.Exception.TapplentException;
import com.tapplent.platformutility.activityLog.ActivityLog;
import com.tapplent.platformutility.activityLog.ActivityLogRequest;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.Tutorial;
import com.tapplent.platformutility.layout.valueObject.VisualArtefactLibraryVO;
import com.tapplent.platformutility.metadata.structure.CountrySpecificDOAMeta;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.apputility.layout.service.LayoutService;

@Controller
@RequestMapping("tapp/layouts/v1")
public class LayoutController {
	private static final Logger LOG = LogFactory.getLogger(LayoutController.class);
	private static final String ROOT = "/Users/tapplent/Desktop";
	private LayoutService layoutService;

	@RequestMapping(value = "layout/t/{tenantId}/u/{personId}/e/", method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<ResponseLayout> getLayout(@RequestHeader("access_token") String token,
											 @PathVariable String tenantId,
											 @PathVariable String personId,
											 @RequestParam("personName") String personName,
											 @RequestParam("timeZone") String timeZone,
											 @RequestBody RequestLayout requestLayout) {
		ResponseLayout layoutResponse = layoutService.getLayout(requestLayout);
		return new ResponseEntity<ResponseLayout>(layoutResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "sysEssentials/t/{tenantId}/u/{personId}/e/", method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<ResponseSysEssentials> getSysEssentials(@RequestHeader("access_token") String token,
														   @PathVariable String tenantId,
														   @PathVariable String personId,
														   @RequestParam("personName") String personName,
														   @RequestParam("timeZone") String timeZone,
														   @RequestBody RequestSysEssentials requestSysEssentials) {
		ResponseSysEssentials resonse = layoutService.getSysEssentials(requestSysEssentials);
		return new ResponseEntity<ResponseSysEssentials>(resonse, HttpStatus.OK);
	}

	public void setLayoutService(LayoutService layoutService) {
		this.layoutService = layoutService;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/upload")
	public String handleFileUpload(@RequestParam("name") String name,
								   @RequestParam("file") MultipartFile file,
								   RedirectAttributes redirectAttributes) {
		if (name.contains("/")) {
			redirectAttributes.addFlashAttribute("message", "Folder separators not allowed");
			return "redirect:upload";
		}
		if (name.contains("/")) {
			redirectAttributes.addFlashAttribute("message", "Relative pathnames not allowed");
			return "redirect:upload";
		}

		if (!file.isEmpty()) {
			try {
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(LayoutController.ROOT + "/" + name)));
				FileCopyUtils.copy(file.getInputStream(), stream);
				stream.close();
				redirectAttributes.addFlashAttribute("message",
						"You successfully uploaded " + name + "!");
			} catch (Exception e) {
				redirectAttributes.addFlashAttribute("message",
						"You failed to upload " + name + " => " + e.getMessage());
			}
		} else {
			redirectAttributes.addFlashAttribute("message",
					"You failed to upload " + name + " because the file was empty");
		}

		String existingBucketName = "mytapplent";
		String keyName = Common.getUUID();
		String filePath = LayoutController.ROOT + "/" + name;

		TransferManager tm = new TransferManager(SystemAwareCache.getSystemRepository().getAmazonS3Client());
		InputStream is = null;
		try {
			is = new FileInputStream(filePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
//		BufferedInputStream stream = new BufferedInputStream(filePath);
		ObjectMetadata objectMetadata = new ObjectMetadata();
		FileNameMap fileNameMap = URLConnection.getFileNameMap();
		String contentType = fileNameMap.getContentTypeFor(filePath);
		objectMetadata.setContentDisposition("attachment; filename=" + filePath);
		objectMetadata.setContentType(contentType);
		PutObjectRequest request = new PutObjectRequest(existingBucketName, keyName, is, objectMetadata);

		request.setGeneralProgressListener(new ProgressListener() {
			@Override
			public void progressChanged(ProgressEvent progressEvent) {
				System.out.println("Transferred bytes: " +
						progressEvent.getBytesTransferred());
			}
		});

		Upload upload = tm.upload(request);

		try {
			// You can block and wait for the upload to finish
			upload.waitForCompletion();
		} catch (AmazonClientException amazonClientException) {
			System.out.println("Unable to upload file, upload aborted.");
			amazonClientException.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "success";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/upload")
	public String provideUploadInfo(Model model) {
		File rootFolder = new File(LayoutController.ROOT);
		List<String> fileNames = Arrays.stream(rootFolder.listFiles())
				.map(f -> f.getName())
				.collect(Collectors.toList());

		model.addAttribute("files",
				Arrays.stream(rootFolder.listFiles())
						.sorted(Comparator.comparingLong(f -> -1 * f.lastModified()))
						.map(f -> f.getName())
						.collect(Collectors.toList())
		);

		return "uploadForm";
	}

	@RequestMapping(value = "appHomePageResolve/t/{tenantId}/u/{personId}/e/", method = RequestMethod.GET)
	void resolveAppHomePage(@RequestHeader("access_token") String token,
							@PathVariable("tenantId") String tenantId,
							@PathVariable("personId") String userName,
							@RequestParam("personName") String personName,
							@RequestParam("timeZone") String timeZone
	) {
//        utilityService.insertError(errorRequest);
		layoutService.resolveAppHomePageForAllUsers();
	}

	@RequestMapping(value = "getRatingLevel/t/{tenantId}/u/{personId}/e/", method = RequestMethod.GET)
	ResponseEntity<RatingLevel> getRatingLevel(@RequestHeader("access_token") String token,
											   @PathVariable("tenantId") String tenantId,
											   @PathVariable("personId") String userName,
											   @RequestParam("ratingScaleId") String ratingScaleId,
											   @RequestParam("anchor") double anchor
	) {
		RatingLevel ratingLevel = layoutService.getRatingLevel(ratingScaleId, anchor);
		return new ResponseEntity<>(ratingLevel, HttpStatus.OK);
	}

	@RequestMapping(value = "/retrieveImages/t/{tenantId}/u/{personId}/e/", method = RequestMethod.GET)
	@ResponseBody
	List<VisualArtefactLibraryVO> getAllTheImages(@RequestHeader("access_token") String token,
												  @PathVariable("tenantId") String tenantId,
												  @PathVariable("personId") String userName,
												  @RequestParam("personName") String personName,
												  @RequestParam("timeZone") String timeZone,
												  @RequestParam("imgLibTypeId") String imgLibTypeId) {
		List<VisualArtefactLibraryVO> visualArtefactLibraryVOList = layoutService.getAllTheImages(imgLibTypeId);
		return visualArtefactLibraryVOList;
	}

	@RequestMapping(value = "/retrieveTutorials/t/{tenantId}/u/{personId}/e/", method = RequestMethod.GET)
	@ResponseBody
	List<Tutorial> getTheTutorials(@RequestHeader("access_token") String token,
								   @PathVariable("tenantId") String tenantId,
								   @PathVariable("personId") String userName,
								   @RequestParam("personName") String personName,
								   @RequestParam("timeZone") String timeZone,
								   @RequestParam("tutorialCatId") String tutorialCatId) {
		List<Tutorial> tutorialList = layoutService.getAllTutorials(userName, tutorialCatId);
		return tutorialList;
	}
	@RequestMapping(value = "activityLogs/t/{tenantId}/u/{personId}/e/", method = RequestMethod.POST)
	@ResponseBody
	Map<String, ActivityLog> getActivityLogs(@RequestHeader("access_token") String token,
									 @PathVariable String tenantId,
									 @PathVariable String personId,
									 @RequestParam("personName") String personName,
									 @RequestParam("timeZone") String timeZone,
									 @RequestBody ActivityLogRequest requestActivityLog) {
		Map<String, ActivityLog> response = null;
		try {
			if (!Util.isValidTenant()) {
				throw new TapplentException("Not Valid Tenant!");
			} else if (!Util.validateAccessTokenAndSetUserContext(token)) {
				throw new TapplentException("Token not valid!");
			}
			else response = layoutService.getActivityLogs(requestActivityLog);
		} catch (SQLException | IOException | TapplentException e) {
			e.printStackTrace();
		}
		return response;
	}
	@RequestMapping(value = "personPreferences/t/{tenantId}/u/{personId}/e/", method = RequestMethod.GET)
	@ResponseBody
	ResponseEntity<PersonPreference> getPersonPreferences(@RequestHeader("access_token") String token,
														  @PathVariable String tenantId,
														  @PathVariable String personId,
														  @RequestParam("personName") String personName,
														  @RequestParam("timeZone") String timeZone) {
//		String person = requestPersonPreference.getPersonId();
		if (personId != null) {
			PersonPreference response = layoutService.getPersonPreferences(personId);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		return null;
	}

	@RequestMapping(value = "countrySpecificMeta/t/{tenantId}/u/{personId}/e/", method = RequestMethod.GET)
	@ResponseBody
	ResponseEntity<CountrySpecificDOAMeta> getCountrySpecificMeta(@RequestHeader("access_token") String token,
																  @PathVariable String tenantId,
																  @PathVariable String personId,
																  @RequestParam String baseTemplate,
																  @RequestParam String doa,
																  @RequestParam String countryCode) {
		if (personId != null) {
			CountrySpecificDOAMeta response = TenantAwareCache.getMetaDataRepository().getCountrySpecificDOAMeta(baseTemplate, doa, countryCode);
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		return null;
	}

	@RequestMapping(value = "nameCalculation/t/{tenantId}/u/{personId}/e/", method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<NameCalculationResponse> calculateName(@RequestHeader("access_token") String token,
														  @PathVariable String tenantId,
														  @PathVariable String personId,
														  @RequestBody NameCalculationRequest nameCalculationRequest) {
		NameCalculationResponse response = layoutService.calculateName(nameCalculationRequest);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "workflowActions/t/{tenantId}/u/{personId}/e/", method = RequestMethod.POST)
	@ResponseBody
	ResponseEntity<WorkflowActionResponse> getWorkflowActions(@RequestHeader("access_token") String token,
															  @PathVariable String tenantId,
															  @PathVariable String personId,
															  @RequestBody WorkflowActionRequest workflowActionRequest) {
		WorkflowActionResponse response = null;
		try {
			if (!Util.isValidTenant()) {
				throw new TapplentException("Not Valid Tenant!");
			} else if (!Util.validateAccessTokenAndSetUserContext(token)) {
				throw new TapplentException("Token not valid!");
			} else response = layoutService.getWorkflowActions(workflowActionRequest);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TapplentException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}