package com.tapplent.apputility.analytics.structure;

import com.tapplent.apputility.uilayout.structure.SectionControl;
import com.tapplent.platformutility.analytics.structure.AnalyticsMetricDetailsQuery;
import com.tapplent.platformutility.uilayout.valueobject.LoadParameters;
import com.tapplent.platformutility.uilayout.valueobject.SectionControlVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 09/12/16.
 */
public class AnalyticsMetricRequest {
    private List<SectionControl> metricQueryList = new ArrayList<>();
    private Map<String, LoadParameters> peToLoadParamsMap = new HashMap<>();
    private List<AnalyticsMetricDetailsQuery> metricQueryDetails = new ArrayList<>();
    private List<AnalyticsMetricChartQuery> metricQuerys = new ArrayList<>();
    private MetricAxis xAxis;
    public List<SectionControl> getMetricQueryList() {
        return metricQueryList;
    }

    public void setMetricQueryList(List<SectionControl> metricQueryList) {
        this.metricQueryList = metricQueryList;
    }

    public Map<String, LoadParameters> getPeToLoadParamsMap() {
        return peToLoadParamsMap;
    }

    public void setPeToLoadParamsMap(Map<String, LoadParameters> peToLoadParamsMap) {
        this.peToLoadParamsMap = peToLoadParamsMap;
    }

    public List<AnalyticsMetricDetailsQuery> getMetricQueryDetails() {
        return metricQueryDetails;
    }

    public void setMetricQueryDetails(List<AnalyticsMetricDetailsQuery> metricQueryDetails) {
        this.metricQueryDetails = metricQueryDetails;
    }

    public List<AnalyticsMetricChartQuery> getMetricQuerys() {
        return metricQuerys;
    }

    public MetricAxis getxAxis() {
        return xAxis;
    }

    public void setxAxis(MetricAxis xAxis) {
        this.xAxis = xAxis;
    }

    public void setMetricQuerys(List<AnalyticsMetricChartQuery> metricQuerys) {
        this.metricQuerys = metricQuerys;
    }
}
