package com.tapplent.apputility.data.structure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 10/06/17.
 */
public class GenericQuery {
    private String queryID;
    private List<GenericQueryAttribute> attributes = new ArrayList<>();
    private List<GenericQueryFilters> filters = new ArrayList<>();

    public GenericQuery(String queryID) {
        this.queryID = queryID;
    }

    public String getQueryID() {
        return queryID;
    }

    public void setQueryID(String queryID) {
        this.queryID = queryID;
    }

    public List<GenericQueryAttribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<GenericQueryAttribute> attributes) {
        this.attributes = attributes;
    }

    public List<GenericQueryFilters> getFilters() {
        return filters;
    }

    public void setFilters(List<GenericQueryFilters> filters) {
        this.filters = filters;
    }
}
