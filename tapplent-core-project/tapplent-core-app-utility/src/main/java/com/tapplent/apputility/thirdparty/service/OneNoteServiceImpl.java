package com.tapplent.apputility.thirdparty.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import org.springframework.transaction.annotation.Transactional;

import com.tapplent.apputility.data.service.ServerRuleManager;
import com.tapplent.apputility.data.structure.PEData;
import com.tapplent.apputility.data.structure.RequestData;
import com.tapplent.apputility.data.structure.UnitRequestData;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.thirdPartyApp.gmail.EventAttchment;
import com.tapplent.platformutility.thirdPartyApp.gmail.EverNoteDAO;
import com.tapplent.platformutility.thirdPartyApp.gmail.NoteSubTask;
import com.tapplent.platformutility.thirdPartyApp.gmail.SingleNote;
import com.tapplent.platformutility.thirdPartyApp.gmail.ThirdPartyUtil;

public class OneNoteServiceImpl implements OneNoteService {
	private final static String PAGES_ENDPOINT = "https://www.onenote.com/api/v1.0/me/notes/pages";
	private OutputStream mOutputStream = null;
	private final String DELIMITER = "--";
	private final String BOUNDARY = "Asdfs"+Long.toString(System.currentTimeMillis()) + "aBc";
	private HttpsURLConnection mUrlConnection = null;
	private String mAccessToken = "EwAAA61DBAAUGCCXc8wU/zFu9QnLdZXy+YnElFkAAfEcwNFOTbfI/1U86GW0bcULDDvVKse3bCTz6785kroo4b+FvroQG6fY/Xvks3tOxy7xkrdhVUfsGVUiSooTwsWbOHzJKhmmfzn4xIJl+gLvuEgx1fCHdKOWXN9duMCVgvzVvGJ4S92yKtSsosRu56ZHbCfMZsv1/TtzhbvNSw4wTvESdKooTZk89Of72XqXEju0rf5Vjqu4U2Y19LqlBhAVp35TdG47ehDbEGxDRIPLO7usIV6dn0B5xeTRn47jwd85v19NmzRKu57o5atHiR7j/kHAKeYZDshX2zCwJjiSwNLlZQeMXz80ILHCFbvyU7wMM+7e2hyF8h6r1FCFGKgDZgAACOrb7VYGNaUM0AHcoKWsmEQpVlG//BJLyCVagYmbVlJV3IglpbEwPXu9I4eStx8KJhDcOcZJHjcXpiIYWjvfKkCnLf/Q9znUdOe8oxuMCjKStPSZGPuiS2aVEe/pL2hOz3yINP5N8xWvUcu0ewGlek04ls3HoeP2I8du1WwIoHWvtlCacfXA+G5/t9BuUMAjAxnwZHzHGwJwI5BvD8OI1ZM7tUZdsyFby0+xgh/7bTECErgAqMwFSed7BXUFD/KY9ahUOZCmFGh7LuDK3TmAM0t6n9zgWE8rBWlltfE5E0odC3Q9ZWr1aLXmxj/E9Fa2O/GKNscpKxqvFoZhqnwhWYcNLVBcsWe71XsVewtg0a1pX7oDPkXb/sYz5EDz7veOgYxmItiOOX6Dx0nfAD3O1ZZh3n49j8uFNv3x4IKI3o/mpenHvg/c/P8PMcep0yyH+EoKYjBncmSr36S/F7edFJqJHMX2ghXB71XdPKG/3diHjoY5kOHgk/xKKHgF39ubOq6NA94VowSkd+yvo6gt34S8BMma8cE8hMJyu6vZaSr8TSI4jJGoqj16EhvL/1UF0IF2NpFbzPEC2mnbz5a1/5Fjuzsn+JPKp4Hmm0RO5lAiloNwAXhJIL9Skg0C";

	 private EverNoteDAO evernoteDAO;
	 private ServerRuleManager serverRuleManager;
	 public ServerRuleManager getServerRuleManager() {
		return serverRuleManager;
	}


	public void setServerRuleManager(ServerRuleManager serverRuleManager) {
		this.serverRuleManager = serverRuleManager;
	}


	public void setEvernoteDAO(EverNoteDAO evernoteDAO) {
			this.evernoteDAO = evernoteDAO;
		}

	@Override
	@Transactional
	public String oneNoteSynch(String personId) throws Exception {
		Map<String,Object> columnNameValueMap = evernoteDAO.isSynchNeeded(personId,"T_PFM_TAP_NOTE_SYNCH","NOTE_SYNCH_PK_ID");
		boolean isSyncNeeded = (boolean) columnNameValueMap.get("isSynchNeeded");
		String integrationAppFkId = (String) columnNameValueMap.get("integrationAppName");
		String taskSyncPkId = (String) columnNameValueMap.get("syncPkId");
		Timestamp lastModifiedDateTime1 = (Timestamp) columnNameValueMap.get("lastModifiedDateTime");
		 SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		 String lastModifiedDateTime =  fmt.format(lastModifiedDateTime1).toString();
		List<String> notePkId = evernoteDAO.getEventPkId(taskSyncPkId,"T_PFM_TAP_NOTE_SYNCH_DTLS","NOTE_FK_ID","NOTE_SYNCH_FK_ID");

		String accesstoken = "EwAAA61DBAAUGCCXc8wU/zFu9QnLdZXy+YnElFkAAfEcwNFOTbfI/1U86GW0bcULDDvVKse3bCTz6785kroo4b+FvroQG6fY/Xvks3tOxy7xkrdhVUfsGVUiSooTwsWbOHzJKhmmfzn4xIJl+gLvuEgx1fCHdKOWXN9duMCVgvzVvGJ4S92yKtSsosRu56ZHbCfMZsv1/TtzhbvNSw4wTvESdKooTZk89Of72XqXEju0rf5Vjqu4U2Y19LqlBhAVp35TdG47ehDbEGxDRIPLO7usIV6dn0B5xeTRn47jwd85v19NmzRKu57o5atHiR7j/kHAKeYZDshX2zCwJjiSwNLlZQeMXz80ILHCFbvyU7wMM+7e2hyF8h6r1FCFGKgDZgAACOrb7VYGNaUM0AHcoKWsmEQpVlG//BJLyCVagYmbVlJV3IglpbEwPXu9I4eStx8KJhDcOcZJHjcXpiIYWjvfKkCnLf/Q9znUdOe8oxuMCjKStPSZGPuiS2aVEe/pL2hOz3yINP5N8xWvUcu0ewGlek04ls3HoeP2I8du1WwIoHWvtlCacfXA+G5/t9BuUMAjAxnwZHzHGwJwI5BvD8OI1ZM7tUZdsyFby0+xgh/7bTECErgAqMwFSed7BXUFD/KY9ahUOZCmFGh7LuDK3TmAM0t6n9zgWE8rBWlltfE5E0odC3Q9ZWr1aLXmxj/E9Fa2O/GKNscpKxqvFoZhqnwhWYcNLVBcsWe71XsVewtg0a1pX7oDPkXb/sYz5EDz7veOgYxmItiOOX6Dx0nfAD3O1ZZh3n49j8uFNv3x4IKI3o/mpenHvg/c/P8PMcep0yyH+EoKYjBncmSr36S/F7edFJqJHMX2ghXB71XdPKG/3diHjoY5kOHgk/xKKHgF39ubOq6NA94VowSkd+yvo6gt34S8BMma8cE8hMJyu6vZaSr8TSI4jJGoqj16EhvL/1UF0IF2NpFbzPEC2mnbz5a1/5Fjuzsn+JPKp4Hmm0RO5lAiloNwAXhJIL9Skg0C";
		
		String a= getOneNotePages(accesstoken);
		JSONObject notes =new JSONObject(a);
			
		JSONArray notesArray = notes.getJSONArray("value");
		
		for(int i = 0; i<=notesArray.length()-1;i++){
			
		    		JSONObject microsoftNote = (JSONObject) notesArray.get(i);
		    		String microsoftNoteId = microsoftNote.getString("id");
		    		String microsoftNoteTitle = microsoftNote.getString("title");
		    		if(microsoftNoteTitle.contains("#tapplent")){
		    			String htmlContent = getMicrosoftNoteContent(accesstoken,microsoftNoteId);		    			
		    			String tapplentNoteId = evernoteDAO.getEventFkIdFromIntegration(microsoftNoteId,"T_PFM_TAP_NOTE_INTGN","NOTE_FK_ID");
		    			if(tapplentNoteId != null && !tapplentNoteId.isEmpty() ){
						 	String mtPE = "Note";
				    		EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
				    		Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
				    		SingleNote tapplentNote= evernoteDAO.getNote(tapplentNoteId,attributeMetaMap,personId);
								if(tapplentNote.isDeleted())
								deleteTheMicrosoftNote(accesstoken, microsoftNoteId);	
								//Retrieve the lastModifiedtime of note in tapplent and everNote
								Timestamp tapplentNoteLastModifiedTime = tapplentNote.getLastModifiedDateTime();
								Timestamp microsoftNoteLastModifiedTime = ThirdPartyUtil.isoToSqlTimestamp(microsoftNote.getString("lastModifiedTime"));
								
								if(tapplentNoteLastModifiedTime.after(microsoftNoteLastModifiedTime)){
									//Update the tapplent Task record onto the GoogleTask
									updateTheOneNote(microsoftNoteId, tapplentNote,personId);
								
								}else if(microsoftNoteLastModifiedTime.after(tapplentNoteLastModifiedTime)){
									//Update the google Task record onto Tapplent Task record 
									updateTheTapplentNote(tapplentNote,microsoftNoteId,personId);

								}
						}else{
							
						insertNoteIntoTapplent(htmlContent,personId,accesstoken,microsoftNoteId);
						
						}
		    		
		    	}
		}
		//Fetch the IS_SYNCH_NEEDED against person again
		if(isSyncNeeded){
			
			for(String notePkId1:notePkId){
				if(notePkId1 !=null){
					String mtPE = "Note";
		    		EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
		    		Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
		    		 SingleNote tapplentNote = evernoteDAO.getNote(notePkId1,attributeMetaMap,personId);
					String oneNoteId = evernoteDAO.getThirdPartyEventId(notePkId1, "T_PFM_TAP_NOTE_INTGN","ONENOTE", "NOTE_FK_ID");	
		   		if(oneNoteId!=null && !oneNoteId.isEmpty()){
		   				
			   			
			   			Timestamp tapplentTaskLastModifiedTime = tapplentNote.getLastModifiedDateTime();
			   			String metadata= getPageMetadata(accesstoken, oneNoteId);
			   			JSONObject oneNote =new JSONObject(metadata);
						Timestamp oneNoteLastModifiedTime = ThirdPartyUtil.isoToSqlTimestamp(oneNote.getString("lastModifiedTime"));
						if(tapplentTaskLastModifiedTime.after(oneNoteLastModifiedTime)){
							//Update the tapplent Task record onto the GoogleTask
							updateTheOneNote(oneNoteId, tapplentNote, personId);
						}else if(oneNoteLastModifiedTime.after(tapplentTaskLastModifiedTime)){
							//Update the google Task record onto Tapplent Task record
							updateTheTapplentNote(tapplentNote,oneNoteId,personId);

						}else{
							//Everything is in Sync Don't Do anything...
						}
						//Update Synch details table
				 		PEData child2PEData = new PEData();
				 		child2PEData.setMtPE("NoteSynchDetails");
				 		child2PEData.setActionCode("ADD_EDIT");
				    	 Map<String ,Object> columnNameToValueMap3 = new LinkedHashMap<String,Object>();
				    	 Map<String,Object> synchDetailsPkId = evernoteDAO.getSynchDetailsPkId("T_PFM_TAP_NOTE_SYNCH_DTLS","NOTE_FK_ID",notePkId1,"NOTE_SYNCH_DTLS_PK_ID");
				    	 SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
				    	 String lastModifiedDateTime3 =  fmt1.format(synchDetailsPkId.get("lastModifiedDateTime")).toString();
				    	 columnNameToValueMap3.put("NoteSynchDetails.PrimaryKeyID",synchDetailsPkId.get("synchDetailsPkId"));
				    	 columnNameToValueMap3.put("NoteSynchDetails.SynchComplete", true);
				    	 columnNameToValueMap3.put("NoteSynchDetails.LastModifiedDateTime",lastModifiedDateTime3);
				    	 
				    	 child2PEData.setData(columnNameToValueMap3);
				    	 UnitRequestData finalRequestData1 = new UnitRequestData();
				 		finalRequestData1.setRootPEData(child2PEData);
				 		List<UnitRequestData> requestDataList1 = new ArrayList<>();
				 		requestDataList1.add(finalRequestData1);
				 		RequestData request1 = new RequestData();
				 		request1.setActionCodeFkId("ADD_EDIT");
				 		request1.setRequestDataList(requestDataList1);
				 		try {
				 			
				 			serverRuleManager.applyServerRulesOnRequest(request1);
						} catch (Exception e) {
							e.printStackTrace();
						}
		   			
		   		}else{

					String newOneNoteId = createNewOneNote(tapplentNote,accesstoken,notePkId1,personId);
					 PEData child1PEData = new PEData();
				    	child1PEData.setMtPE("NoteIntegration");
						child1PEData.setActionCode("ADD_EDIT");
				    	 Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
				    	 columnNameToValueMap2.put("NoteIntegration.NoteID", notePkId1);
				    	 columnNameToValueMap2.put("NoteIntegration.IntegrationApp", "ONENOTE");
				    	 columnNameToValueMap2.put("NoteIntegration.IntegrationAppReferenceID",newOneNoteId);
				    	 
				    	 child1PEData.setData(columnNameToValueMap2);
				    	 UnitRequestData finalRequestData = new UnitRequestData();
				 		finalRequestData.setRootPEData(child1PEData);
				 		List<UnitRequestData> requestDataList = new ArrayList<>();
				 		requestDataList.add(finalRequestData);
				 		RequestData request = new RequestData();
				 		request.setActionCodeFkId("ADD_EDIT");
				 		request.setRequestDataList(requestDataList);
				 		try {
				 			
				 			serverRuleManager.applyServerRulesOnRequest(request);
						} catch (Exception e) {
							e.printStackTrace();
						}
					//Update Synch details table
			 		PEData child2PEData = new PEData();
			 		child2PEData.setMtPE("NoteSynchDetails");
			 		child2PEData.setActionCode("ADD_EDIT");
			    	 Map<String ,Object> columnNameToValueMap3 = new LinkedHashMap<String,Object>();
			    	 Map<String,Object> synchDetailsPkId = evernoteDAO.getSynchDetailsPkId("T_PFM_TAP_NOTE_SYNCH_DTLS","NOTE_FK_ID",notePkId1,"NOTE_SYNCH_DTLS_PK_ID");
			    	 SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
					String lastModifiedDateTime3 =  fmt1.format(synchDetailsPkId.get("lastModifiedDateTime")).toString();
			    	 columnNameToValueMap3.put("NoteSynchDetails.PrimaryKeyID",synchDetailsPkId.get("synchDetailsPkId"));
			    	 columnNameToValueMap3.put("NoteSynchDetails.SynchComplete", true);
			    	 columnNameToValueMap3.put("NoteSynchDetails.LastModifiedDateTime",lastModifiedDateTime3);
			    	 
			    	 child2PEData.setData(columnNameToValueMap3);
			    	 UnitRequestData finalRequestData1 = new UnitRequestData();
			 		finalRequestData1.setRootPEData(child2PEData);
			 		List<UnitRequestData> requestDataList1 = new ArrayList<>();
			 		requestDataList1.add(finalRequestData1);
			 		RequestData request1 = new RequestData();
			 		request1.setActionCodeFkId("ADD_EDIT");
			 		request1.setRequestDataList(requestDataList1);
			 		try {
			 			
			 			serverRuleManager.applyServerRulesOnRequest(request1);
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}		
			
		}		
		}
		return null;
	}


	private void updateTheOneNote(String microsoftNoteId, SingleNote tapplentNote, String personId) throws Exception {
		
		deleteTheMicrosoftNote(mAccessToken, microsoftNoteId);
		String tapplentNoteId = tapplentNote.getNotePkId();
		String newOneNoteId = createNewOneNote(tapplentNote, mAccessToken, tapplentNoteId, personId);
		PEData child1PEData = new PEData();
    	child1PEData.setMtPE("NoteIntegration");
		child1PEData.setActionCode("ADD_EDIT");
    	 Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
    	 columnNameToValueMap2.put("NoteIntegration.NoteID", tapplentNoteId);
    	 columnNameToValueMap2.put("NoteIntegration.IntegrationApp", "ONENOTE");
    	 columnNameToValueMap2.put("NoteIntegration.IntegrationAppReferenceID",newOneNoteId);
    	 
    	 child1PEData.setData(columnNameToValueMap2);
    	 UnitRequestData finalRequestData = new UnitRequestData();
 		finalRequestData.setRootPEData(child1PEData);
 		List<UnitRequestData> requestDataList = new ArrayList<>();
 		requestDataList.add(finalRequestData);
 		RequestData request = new RequestData();
 		request.setActionCodeFkId("ADD_EDIT");
 		request.setRequestDataList(requestDataList);
 		try {
 			
 			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
	//Update Synch details table
		PEData child2PEData = new PEData();
		child2PEData.setMtPE("NoteSynchDetails");
		child2PEData.setActionCode("ADD_EDIT");
	 Map<String ,Object> columnNameToValueMap3 = new LinkedHashMap<String,Object>();
	 Map<String,Object> synchDetailsPkId = evernoteDAO.getSynchDetailsPkId("T_PFM_TAP_NOTE_SYNCH_DTLS","NOTE_FK_ID",tapplentNoteId,"NOTE_SYNCH_DTLS_PK_ID");
	 SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	String lastModifiedDateTime3 =  fmt1.format(synchDetailsPkId.get("lastModifiedDateTime")).toString();
	 columnNameToValueMap3.put("NoteSynchDetails.PrimaryKeyID",synchDetailsPkId.get("synchDetailsPkId"));
	 columnNameToValueMap3.put("NoteSynchDetails.SynchComplete", true);
	 columnNameToValueMap3.put("NoteSynchDetails.LastModifiedDateTime",lastModifiedDateTime3);
	 
	 child2PEData.setData(columnNameToValueMap3);
	 UnitRequestData finalRequestData1 = new UnitRequestData();
		finalRequestData1.setRootPEData(child2PEData);
		List<UnitRequestData> requestDataList1 = new ArrayList<>();
		requestDataList1.add(finalRequestData1);
		RequestData request1 = new RequestData();
		request1.setActionCodeFkId("ADD_EDIT");
		request1.setRequestDataList(requestDataList1);
		try {
			
			serverRuleManager.applyServerRulesOnRequest(request1);
	} catch (Exception e) {
		e.printStackTrace();
	}
		
	}


	private void updateTheTapplentNote(SingleNote tapplentNote, String oneNoteId,String personId) throws ParseException, JSONException, IOException {
		//Delete All the attachments and update with the attachments which we get from everNote
				//DELETE FROM T_PFM_EU_NOTE_ATTACHMENT WHERE NOTE_FK_ID = x?
				List<EventAttchment> attachments = evernoteDAO.getNoteAttachments(tapplentNote.getNotePkId());
				
				if(attachments != null && !attachments.isEmpty()){
				 for(EventAttchment attachment :attachments){
					String attachmentId = attachment.getAttachmentPkId();
					PEData child1PEData = new PEData();
			    	child1PEData.setMtPE("NoteAttachment");
					child1PEData.setActionCode("ADD_EDIT");
//					child1PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
					child1PEData.setLogActivity(false);
					child1PEData.setSubmit(true);
					child1PEData.setInline(true);
			    	 Map<String ,Object> columnNameToValueMap1 = new LinkedHashMap<String,Object>();
			    	 columnNameToValueMap1.put("NoteAttachment.PrimaryKeyID", attachmentId);
			    	 columnNameToValueMap1.put("NoteAttachment.Deleted", true);
			    	 columnNameToValueMap1.put("NoteAttachment.LastModifiedDateTime",attachment.getLastModifiedDatetime().toString());
			    	 child1PEData.setData(columnNameToValueMap1);
			    	 UnitRequestData finalRequestData = new UnitRequestData();
			 		finalRequestData.setRootPEData(child1PEData);
			 		List<UnitRequestData> requestDataList = new ArrayList<>();
			 		requestDataList.add(finalRequestData);
			 		RequestData request = new RequestData();
			 		request.setActionCodeFkId("ADD_EDIT");
			 		request.setRequestDataList(requestDataList);
			 		try {
			 			
			 			serverRuleManager.applyServerRulesOnRequest(request);
					} catch (Exception e) {
						e.printStackTrace();
					}
				  }
				}
				Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
				String pageContent = getMicrosoftNoteContent(mAccessToken, oneNoteId);
				Document doc = Jsoup.parse(pageContent);
				Elements divisions = doc.getElementsByTag("div");
				String noteDesc = "";
				for(Element division:divisions){
					//Retrieve all the text in the paragraph
					Elements paragraphs = division.getElementsByTag("p");
						if(paragraphs !=null && !paragraphs.isEmpty()){
							for(Element paragraph:paragraphs){
								if(!paragraph.hasAttr("data-tag"))
								noteDesc+= paragraph.text()+"\r\n";
							}
						}
				}	
				
				String noteTitle = doc.select("title").text();
				Map<String,String> personInfo = evernoteDAO.getPersonInfo(personId);
				columnNameToValueMap.put("Note.NoteDescription",Util.getG11nString(personInfo.get("locale"), noteDesc)); 
				columnNameToValueMap.put("Note.PrimaryKeyID",tapplentNote.getNotePkId());
				columnNameToValueMap.put("Note.NoteTitle", Util.getG11nString(personInfo.get("locale"),noteTitle ));
				columnNameToValueMap.put("Note.LastModifiedDateTime",tapplentNote.getLastModifiedDateTime().toString());

				PEData rootPEData = new PEData();
				rootPEData.setMtPE("Note");
				
				rootPEData.setActionCode("ADD_EDIT");
//				rootPEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
				rootPEData.setLogActivity(false);
				rootPEData.setSubmit(true);
				rootPEData.setInline(true);
				rootPEData.setData(columnNameToValueMap);
				UnitRequestData finalRequestData = new UnitRequestData();
				finalRequestData.setRootPEData(rootPEData);
				List<UnitRequestData> requestDataList = new ArrayList<>();
				requestDataList.add(finalRequestData);
				RequestData request = new RequestData();
				request.setActionCodeFkId("ADD_EDIT");
				request.setRequestDataList(requestDataList);
				try {
					serverRuleManager.applyServerRulesOnRequest(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Elements images = doc.getElementsByTag("img");
				if(images !=null && !images.isEmpty()){
					for(Element image:images){
						if(image.hasAttr("data-fullres-src")){
							String imageUrl = image.attr("data-fullres-src");
							String attachmentType = image.attr("data-fullres-src-type");
								String binary = getTheBinaryContent(imageUrl,mAccessToken);
								PEData child1PEData = new PEData();
								child1PEData.setActionCode("ADD_EDIT");
								child1PEData.setMtPE("NoteAttachment");
//								child1PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
								child1PEData.setLogActivity(false);
								child1PEData.setSubmit(true);
								child1PEData.setInline(true);
								Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
								InputStream is1 = new ByteArrayInputStream(binary.getBytes());
								String attachmentId1 = Util.uploadAttachmentToS3(attachmentType, is1);
								columnNameToValueMap2.put("NoteAttachment.AttachmentArtefactID",attachmentId1); 
								columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode","IMAGE");
								columnNameToValueMap2.put("NoteAttachment.NoteID", tapplentNote.getNotePkId());
					            //columnNameToValueMap2.put("NoteAttachment.AttachmentName","");
					            child1PEData.setData(columnNameToValueMap2);
								UnitRequestData finalRequestData1 = new UnitRequestData();
								finalRequestData1.setRootPEData(child1PEData);
								List<UnitRequestData> requestDataList1 = new ArrayList<>();
								requestDataList.add(finalRequestData);
								RequestData request1 = new RequestData();
								request1.setActionCodeFkId("ADD_EDIT");
								request1.setRequestDataList(requestDataList1);
								try {
									serverRuleManager.applyServerRulesOnRequest(request1);
								} catch (Exception e) {
									e.printStackTrace();
								}
						}
					}
				}
				Elements files = doc.getElementsByTag("object");
				if(files !=null && !files.isEmpty()){
					for(Element file:files){
							
							String fileUrl = file.attr("data");
							String attachmentType = file.attr("type");
							String attachmentName = file.attr("data-attachment");
								String binary = getTheBinaryContent(fileUrl,mAccessToken);
								PEData child1PEData = new PEData();
								child1PEData.setActionCode("ADD_EDIT");
								child1PEData.setMtPE("NoteAttachment");
//								child1PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
								child1PEData.setLogActivity(false);
								child1PEData.setSubmit(true);
								child1PEData.setInline(true);
								Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
								InputStream is1 = new ByteArrayInputStream(binary.getBytes());
								String attachmentId1 = Util.uploadAttachmentToS3(attachmentType, is1);
								columnNameToValueMap2.put("NoteAttachment.AttachmentArtefactID",attachmentId1); 
								columnNameToValueMap2.put("NoteAttachment.AttachmentName",attachmentName);
								if(attachmentName.contains(".jpg") ||attachmentName.contains(".png"))  
									columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode","IMAGE");
									if(attachmentName.contains(".mp4") ||attachmentName.contains(".flv"))  
										columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode","VIDEO");
									else
										columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode","DOCUMENT");
								child1PEData.setData(columnNameToValueMap2);
								UnitRequestData finalRequestData1 = new UnitRequestData();
								finalRequestData1.setRootPEData(child1PEData);
								List<UnitRequestData> requestDataList1 = new ArrayList<>();
								requestDataList1.add(finalRequestData1);
								RequestData request1 = new RequestData();
								request1.setActionCodeFkId("ADD_EDIT");
								request1.setRequestDataList(requestDataList1);
								try {
									serverRuleManager.applyServerRulesOnRequest(request1);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
					}
				}
				Elements subNotes = doc.getElementsByAttribute("data-tag");
				
				if(subNotes !=null && !subNotes.isEmpty()){
					for(Element subNote:subNotes){
						PEData child1PEData = new PEData();
						String subNoteContent = subNote.text();
						String noteStatus = subNote.attr("data-tag");
						child1PEData.setActionCode("ADD_EDIT");
						child1PEData.setMtPE("NoteSubTask");
//						child1PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
						child1PEData.setLogActivity(false);
						child1PEData.setSubmit(true);
						child1PEData.setInline(true);
						
						Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
						
						columnNameToValueMap2.put("NoteSubTask.SubTaskDescription",Util.getG11nString(personInfo.get("locale"),subNoteContent));
						if(noteStatus.contains("completed"))
						columnNameToValueMap2.put("NoteSubTask.Complete",true);
						else
						columnNameToValueMap2.put("NoteSubTask.Complete",false);	
						
						
						child1PEData.setData(columnNameToValueMap2);
						UnitRequestData finalRequestData1 = new UnitRequestData();
						finalRequestData1.setRootPEData(child1PEData);
						List<UnitRequestData> requestDataList1 = new ArrayList<>();
						requestDataList1.add(finalRequestData1);
						RequestData request1 = new RequestData();
						request1.setActionCodeFkId("ADD_EDIT");
						request1.setRequestDataList(requestDataList1);
						try {
							serverRuleManager.applyServerRulesOnRequest(request1);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			//Update Synch details table
				 Map<String,Object> synchDetailsPkId = evernoteDAO.getSynchDetailsPkId("T_PFM_TAP_NOTE_SYNCH_DTLS","NOTE_FK_ID",tapplentNote.getNotePkId(),"NOTE_SYNCH_DTLS_PK_ID");
		if(synchDetailsPkId !=null && !synchDetailsPkId.isEmpty()){
				 PEData child2PEData = new PEData();
				child2PEData.setMtPE("NoteSynchDetails");
				child2PEData.setActionCode("ADD_EDIT");
//				child2PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
				child2PEData.setLogActivity(false);
				child2PEData.setSubmit(true);
				child2PEData.setInline(true);
			 Map<String ,Object> columnNameToValueMap3 = new LinkedHashMap<String,Object>();
			
			 columnNameToValueMap3.put("NoteSynchDetails.PrimaryKeyID",synchDetailsPkId.get("synchDetailsPkId"));
			 columnNameToValueMap3.put("NoteSynchDetails.SynchComplete", true);
			 columnNameToValueMap3.put("NoteSynchDetails.LastModifiedDateTime",synchDetailsPkId.get("lastModifiedDateTime").toString());
			 
			 child2PEData.setData(columnNameToValueMap3);
			 UnitRequestData finalRequestData1 = new UnitRequestData();
				finalRequestData1.setRootPEData(child2PEData);
				List<UnitRequestData> requestDataList1 = new ArrayList<>();
				requestDataList1.add(finalRequestData1);
				RequestData request1 = new RequestData();
				request1.setActionCodeFkId("ADD_EDIT");
				request1.setRequestDataList(requestDataList1);
				try {
					
					serverRuleManager.applyServerRulesOnRequest(request);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
		
	}


	private String createNewOneNote(SingleNote tapplentNote, String accesstoken, String tapplentNoteId, String personId) throws Exception {
		
		 String noteTitle =tapplentNote.getNotetTitleG11NBigTxt();
		 if(!noteTitle.contains("#tapplent"))
			 noteTitle = "#tapplent "+noteTitle ;
		 String reminderLoc = tapplentNote.getReminderLoclatlng();
		 String address = ThirdPartyUtil.getTheAddressFromCoordinates(reminderLoc);
		 String mapUrl = "http://www.google.com/maps/place/"+reminderLoc+"";
		 String noteDesc =tapplentNote.getNoteDescG11NBigTxt();
		 
		 Timestamp reminderDateTime = tapplentNote.getReminderDateTime();
		 
		
		 List<EventAttchment> attachments = evernoteDAO.getNoteAttachments(tapplentNoteId);
		 List<NoteSubTask> subTasks = evernoteDAO.getNoteSubTasks(tapplentNoteId,personId);
		 this.postMultipartRequest(PAGES_ENDPOINT);
			
			String date = getDate();
			String requestHtml = "<html>" +
					"<head>" +
					"<title>"+noteTitle+"</title>" +
					"<meta name=\"created\" content=\"" + date + "\" />" +
					"</head>" +
					"<body>" ;
			requestHtml +="<p>"+noteDesc+"</p>" ;			
		
			if(subTasks !=null && !subTasks.isEmpty()) { 
				 for(NoteSubTask task : subTasks){
					 if(task.isComplete()){
						 requestHtml += "<p data-tag=\"to-do:completed\" style=\"margin-top:0pt;margin-bottom:0pt\">"+task.getSubTaskDescG11nBigTxt()+"</p>";
						 
					 }else{
						 requestHtml += "<p data-tag=\"to-do\" style=\"margin-top:0pt;margin-bottom:0pt\">"+task.getSubTaskDescG11nBigTxt()+"</p>";
					 }
					   
				 	}
				}
			requestHtml += "<p style=\"margin-top:0pt;margin-bottom:0pt\"><a href=\""+mapUrl+"\">"+address+"</a></p>";
		 for(EventAttchment attachment:attachments){
			 String attachmentPartName = attachment.getArtefactAttach();
			 		
			 		String attachmentName = attachment.getAttachmentNameTxt();
				 	Map<String,Object>map = ThirdPartyUtil.getAttchment(attachment.getArtefactAttach());
					InputStream is =(InputStream) map.get("InputStream");
					byte[] attachmentContents = ThirdPartyUtil.readFromStreamToByteArray(is);
					String contentType = (String) map.get("ContentType");
					requestHtml += "<object data-attachment=\""+attachmentName+"\" data=\"name:" + attachmentPartName + "\" />";
					
				    
				  }
		 
		 requestHtml +="</body>" +
				 		"</html>";
		 this.addPart("Presentation", "text/html", requestHtml);
		 for(EventAttchment attachment:attachments){
			 String attachmentPartName = attachment.getArtefactAttach();
		 		
		 		String attachmentName = attachment.getAttachmentNameTxt();
			 	Map<String,Object>map = ThirdPartyUtil.getAttchment(attachment.getArtefactAttach());
				InputStream is =(InputStream) map.get("InputStream");
				byte[] attachmentContents = ThirdPartyUtil.readFromStreamToByteArray(is);
				String contentType = (String) map.get("ContentType");
				this.addBinaryPart(attachmentPartName, contentType, attachmentContents);
		 }
		
		 
		 
		 this.finishMultipart();
			ApiResponse response = this.getResponse();
			String oneNoteId = response.getOneNoteId();
				 
		return oneNoteId;
	}


	private void insertNoteIntoTapplent(String htmlContent, String personId,String accessToken,String microsoftNoteId) throws Exception {
		
		Map<String ,Object> columnNameToValueMap = new LinkedHashMap<String,Object>();
		Map<String,String> personInfo = evernoteDAO.getPersonInfo(personId);
		String personLocale = personInfo.get("locale");
		//Parse the html content and insert into the corresponding fields in the tapplent...
		Document doc = Jsoup.parse(htmlContent);
		String noteTitle = doc.select("title").text();
		columnNameToValueMap.put("Note.NoteTitle",Util.getG11nString(personLocale,noteTitle));
		PEData rootPEData = new PEData();
		rootPEData.setMtPE("Note");
		rootPEData.setActionCode("ADD_EDIT");
//		rootPEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
		rootPEData.setLogActivity(false);
		rootPEData.setSubmit(true);
		rootPEData.setInline(true);
		rootPEData.setData(columnNameToValueMap);
		List<PEData> childrenPEData = new ArrayList<PEData>();
		Elements divisions = doc.getElementsByTag("div");
		String noteDesc = "";
		for(Element division:divisions){
			//Retrieve all the text in the paragraph
			Elements paragraphs = division.getElementsByTag("p");
				if(paragraphs !=null && !paragraphs.isEmpty()){
					for(Element paragraph:paragraphs){
						if(!paragraph.hasAttr("data-tag"))
						noteDesc+= paragraph.text()+"\r\n";
					}
				}
		}	
			//Retrieve all the attachments..First fetch the all the images
				Elements images = doc.getElementsByTag("img");
				if(images !=null && !images.isEmpty()){
					for(Element image:images){
						if(image.hasAttr("data-fullres-src")){
							String imageUrl = image.attr("data-fullres-src");
							String attachmentType = image.attr("data-fullres-src-type");
								String binary = getTheBinaryContent(imageUrl,accessToken);
								PEData child1PEData = new PEData();
								child1PEData.setActionCode("ADD_EDIT");
								child1PEData.setMtPE("NoteAttachment");
//								child1PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
								child1PEData.setLogActivity(false);
								child1PEData.setSubmit(true);
								child1PEData.setInline(true);
								Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
								InputStream is1 = new ByteArrayInputStream(binary.getBytes());
								String attachmentId1 = Util.uploadAttachmentToS3(attachmentType, is1);
								columnNameToValueMap2.put("NoteAttachment.AttachmentArtefactID",attachmentId1); 
								columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode","IMAGE");
								child1PEData.setData(columnNameToValueMap2);
								childrenPEData.add(child1PEData);
								
						}
						
					}
				}
				//
				Elements files = doc.getElementsByTag("object");
				if(files !=null && !files.isEmpty()){
					for(Element file:files){
							
							String fileUrl = file.attr("data");
							String attachmentType = file.attr("type");
							String attachmentName = file.attr("data-attachment");
								String binary = getTheBinaryContent(fileUrl,accessToken);
								PEData child1PEData = new PEData();
								child1PEData.setActionCode("ADD_EDIT");
								child1PEData.setMtPE("NoteAttachment");
//								child1PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
								child1PEData.setLogActivity(false);
								child1PEData.setSubmit(true);
								child1PEData.setInline(true);
								Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
								InputStream is1 = new ByteArrayInputStream(binary.getBytes());
								String attachmentId1 = Util.uploadAttachmentToS3(attachmentType, is1);
								columnNameToValueMap2.put("NoteAttachment.AttachmentArtefactID",attachmentId1); 
								columnNameToValueMap2.put("NoteAttachment.AttachmentName",attachmentName);
								if(attachmentName.contains(".jpg") ||attachmentName.contains(".png"))  
									columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode","IMAGE");
									if(attachmentName.contains(".mp4") ||attachmentName.contains(".flv"))  
										columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode","VIDEO");
									else
										columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode","DOCUMENT");
								child1PEData.setData(columnNameToValueMap2);
								childrenPEData.add(child1PEData);
								
					}
				}
				Elements subNotes = doc.getElementsByAttribute("data-tag");
				
				if(subNotes !=null && !subNotes.isEmpty()){
					for(Element subNote:subNotes){
						PEData child1PEData = new PEData();
						String subNoteContent = subNote.text();
						String noteStatus = subNote.attr("data-tag");
						child1PEData.setActionCode("ADD_EDIT");
						child1PEData.setMtPE("NoteSubTask");
//						child1PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
						child1PEData.setLogActivity(false);
						child1PEData.setSubmit(true);
						child1PEData.setInline(true);
						
						Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
						
						columnNameToValueMap2.put("NoteSubTask.SubTaskDescription",Util.getG11nString(personInfo.get("locale"),subNoteContent));
						if(noteStatus.contains("completed"))
						columnNameToValueMap2.put("NoteSubTask.Complete",true);
						else
						columnNameToValueMap2.put("NoteSubTask.Complete",false);	
						
						
						child1PEData.setData(columnNameToValueMap2);
						childrenPEData.add(child1PEData);
					}
				}
				
				PEData child2PEData = new PEData();
				child2PEData.setActionCode("ADD_EDIT");
				child2PEData.setMtPE("NoteIntegration");
//				child2PEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
				child2PEData.setLogActivity(false);
				child2PEData.setSubmit(true);
				child2PEData.setInline(true);
				Map<String ,Object> columnNameToValueMap2 = new LinkedHashMap<String,Object>();
				columnNameToValueMap2.put("NoteIntegration.IntegrationApp","ONENOTE"); 
				columnNameToValueMap2.put("NoteIntegration.IntegrationAppReferenceID",microsoftNoteId);
				child2PEData.setData(columnNameToValueMap2);
				childrenPEData.add(child2PEData);		
			

			columnNameToValueMap.put("Note.NoteDescription",Util.getG11nString(personInfo.get("locale"), noteDesc)); 
			rootPEData.setChildrenPEData(childrenPEData);	
			UnitRequestData finalRequestData = new UnitRequestData();
			finalRequestData.setRootPEData(rootPEData);
			List<UnitRequestData> requestDataList = new ArrayList<>();
			requestDataList.add(finalRequestData);
			RequestData request = new RequestData();
			request.setActionCodeFkId("ADD_EDIT");
		
			request.setRequestDataList(requestDataList);
			serverRuleManager.applyServerRulesOnRequest(request);
		
	}


	private String getTheBinaryContent(String resourceUrl,String accesstoken) throws ParseException, JSONException, IOException {
		Client client = ClientBuilder.newClient();
		Response response = client.target(resourceUrl)
		  .request(MediaType.TEXT_PLAIN_TYPE)
		  .header("Authorization", "Bearer "+accesstoken)
		  .get();

		System.out.println("status: " + response.getStatus());
		System.out.println("headers: " + response.getHeaders());
		String body = response.readEntity(String.class);
		return body;
	}

	private String getMicrosoftNoteContent(String accesstoken,String noteId) throws ClientProtocolException, IOException{
		Client client = ClientBuilder.newClient();
		Response response = client.target("https://www.onenote.com/api/v1.0/me/notes/pages/"+noteId+"/content")
		  .request(MediaType.TEXT_PLAIN_TYPE)
		  .header("Authorization", "Bearer "+accesstoken)
		  .get();

		System.out.println("status: " + response.getStatus());
		System.out.println("headers: " + response.getHeaders());
		String body = response.readEntity(String.class);
		return body;
	}
private String getOneNotePages(String accesstoken) throws ClientProtocolException, IOException, JSONException{
	Client client = ClientBuilder.newClient();
	Response response = client.target("https://www.onenote.com/api/v1.0/me/notes/pages?filter=contains(title%2C'%23tapplent')&orderby=createdTime&select=title%2Cself%2CcreatedTime%2Cid%2C%20lastModifiedTime")
	  .request(MediaType.TEXT_PLAIN_TYPE)
	  .header("Authorization", "Bearer "+accesstoken)
	  .get();

	System.out.println("status: " + response.getStatus());
	System.out.println("headers: " + response.getHeaders());
	String body = response.readEntity(String.class);
	return body;
	}
private String getPageMetadata(String accesstoken,String noteId) throws ClientProtocolException, IOException{
	Client client = ClientBuilder.newClient();
	Response response = client.target("https://www.onenote.com/api/v1.0/me/notes/pages/"+noteId+"?select=title%2Cid%2ClastModifiedTime")
	  .request(MediaType.TEXT_PLAIN_TYPE)
	  .header("Authorization", "Bearer tokenString")
	  .get();

	System.out.println("status: " + response.getStatus());
	System.out.println("headers: " + response.getHeaders());
	String body = response.readEntity(String.class);
	return body;
}
private void createNewPageWithAttachment() throws Exception{
	this.postMultipartRequest(PAGES_ENDPOINT);
	String attachmentPartName = "pdfattachment1";
	String date = getDate();
	String requestHtml = "<html>" +
			"<head>" +
			"<title>Naveen Page</title>" +
			"<meta name=\"created\" content=\"" + date + "\" />" +
			"</head>" +
			"<body>" +
			"<p>This page contains a pdf file attachment</p>" + 
			"<object data-attachment=\"attachment.pdf\" data=\"name:" + attachmentPartName + "\" />" +
			"<p>Here's the content of the PDF document :</p>" +
			"<img data-render-src=\"name:" + attachmentPartName + "\" alt=\"A beautiful logo\" width=\"1500\" />" +
			"</body>" +
			"</html>";
	InputStream is = new FileInputStream(new File("/Users/tapplent/Desktop/PrintPreview.pdf"));
	byte[] attachmentContents = ThirdPartyUtil.readFromStreamToByteArray(is);
	//Add a part that contains the HTML content for this request and refers to another part for the file attachment
	this.addPart("Presentation", "text/html", requestHtml);
	//Add the content of the document file attachment in a separate part referenced by the part name
	this.addBinaryPart("pdfattachment1", "application/pdf", attachmentContents);
	this.finishMultipart();
	ApiResponse response = this.getResponse();
	if(is!=null)
	is.close();	
	return;
}
private void postMultipartRequest(String endpoint) throws Exception {

	mUrlConnection = (HttpsURLConnection) ( new URL(endpoint)).openConnection();
	mUrlConnection.setDoOutput(true);
	mUrlConnection.setRequestMethod("POST");
	mUrlConnection.setDoInput(true);
	mUrlConnection.setRequestProperty("Connection", "Keep-Alive");
	mUrlConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
	mUrlConnection.setRequestProperty("Authorization", "Bearer " + mAccessToken);
	mUrlConnection.connect();
	mOutputStream = mUrlConnection.getOutputStream();
}

private void addBinaryPart(String paramName, String contentType, byte[] contents) throws Exception {
	writePartData(paramName, contentType, contents);
}

private void addPart(String paramName, String contentType, String contents) throws Exception {
	writePartData(paramName, contentType, contents.getBytes());
}

private void writePartData(String paramName, String contentType, byte[] contents) throws Exception {
	mOutputStream.write( (DELIMITER + BOUNDARY + "\r\n").getBytes());
	mOutputStream.write( ("Content-Type: " + contentType + "\r\n").getBytes());
	mOutputStream.write( ("Content-Disposition: form-data; name=\"" + paramName + "\"\r\n").getBytes());
	mOutputStream.write(("\r\n").getBytes());
	mOutputStream.write(contents);
	mOutputStream.write(("\r\n").getBytes());
}

private void finishMultipart() throws Exception {
	mOutputStream.write( (DELIMITER + BOUNDARY + DELIMITER + "\r\n").getBytes());
	mOutputStream.close();
}

public ApiResponse getResponse() throws Exception {

	int responseCode = mUrlConnection.getResponseCode();
	String responseMessage = mUrlConnection.getResponseMessage();
	String responseBody = null;
	if ( responseCode == 201) {
		InputStream is = null;
		try
		{
			is = mUrlConnection.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			String lineSeparator = System.getProperty("line.separator");
			StringBuffer buffer = new StringBuffer();
			while((line = reader.readLine()) != null)
			{
				buffer.append(line);
				buffer.append(lineSeparator);
			}
			responseBody = buffer.toString();
		}
		finally
		{
			if(is != null)
			{
				is.close();
			}
			mUrlConnection.disconnect();
		}
	}
	JSONObject responseObject = null;
	
	ApiResponse response = new ApiResponse();
	try {			
		response.setResponseCode(responseCode);
		response.setResponseMessage(responseMessage);
		if ( responseCode == 201) {
			responseObject = new JSONObject(responseBody);
			String clientUrl = responseObject.getJSONObject("links").getJSONObject("oneNoteClientUrl").getString("href");		
			String webUrl = responseObject.getJSONObject("links").getJSONObject("oneNoteWebUrl").getString("href");
			String oneNoteId = responseObject.getString("id");
			response.setOneNoteClientUrl(clientUrl);
			response.setOneNoteWebUrl(webUrl);
			response.setOneNoteId(oneNoteId);
		}
	} catch (JSONException ex) {
		ex.printStackTrace();
	}
	return response;

}

/* Gets the Created Date in ISO-8601 with timezone format */
private String getDate() {

	Date d = new Date();
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
	return df.format(d);
}
private void deleteTheMicrosoftNote(String access_token,String oneNoteId) throws ClientProtocolException, IOException, JSONException{

StringBuilder url = new StringBuilder("https://www.onenote.com/api/v1.0/me/notes/pages/");
url.append(oneNoteId).append("/");
HttpDelete delete = new HttpDelete(url.toString());
delete.addHeader("Content-Type", "application/json");
delete.addHeader("Authorization", "Bearer "+access_token);
HttpClient client = HttpClientBuilder.create().build();
HttpResponse response;
response = client.execute(delete);
int status_code = response.getStatusLine().getStatusCode();	
}

}

