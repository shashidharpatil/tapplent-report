package com.tapplent.apputility.utility.service;

import com.tapplent.apputility.utility.structure.LogErrorRequest;
import com.tapplent.platformutility.common.util.dao.UtilityDAO;
import com.tapplent.platformutility.common.util.valueObject.ErrorLogVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;

/**
 * Created by Shubham Patodi on 05/08/16.
 */
@Service
public class UtilityServiceImpl implements UtilityService {
    private UtilityDAO utilityDAO;
    @Override
    @Transactional
    public void insertError(LogErrorRequest errorRequest) {
        ErrorLogVO logVO = new ErrorLogVO();
        BeanUtils.copyProperties(errorRequest, logVO);
        try {
            utilityDAO.insertError(logVO);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public UtilityDAO getUtilityDAO() {
        return utilityDAO;
    }

    public void setUtilityDAO(UtilityDAO utilityDAO) {
        this.utilityDAO = utilityDAO;
    }
}
