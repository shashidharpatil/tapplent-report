package com.tapplent.apputility.conversation.structure;

/**
 * Created by Tapplent on 6/22/17.
 */
public class FeedbackGroupMemberRequest {
    String feedbackGroupId;

    public String getFeedbackGroupId() {
        return feedbackGroupId;
    }

    public void setFeedbackGroupId(String feedbackGroupId) {
        this.feedbackGroupId = feedbackGroupId;
    }
}
