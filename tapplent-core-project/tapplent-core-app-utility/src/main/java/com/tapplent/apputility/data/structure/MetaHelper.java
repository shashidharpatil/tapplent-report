package com.tapplent.apputility.data.structure;

import com.tapplent.platformutility.metadata.structure.EntityMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;

import java.util.Map;

/**
 * Created by tapplent on 29/12/16.
 */
public class MetaHelper {
    private Map<String, DoaMeta> labelToAliasMap;
    private Map<String, EntityMetadataVOX> doMetaMap;

    public Map<String, DoaMeta> getLabelToAliasMap() {
        return labelToAliasMap;
    }

    public void setLabelToAliasMap(Map<String, DoaMeta> labelToAliasMap) {
        this.labelToAliasMap = labelToAliasMap;
    }

    public Map<String, EntityMetadataVOX> getDoMetaMap() {
        return doMetaMap;
    }

    public void setDoMetaMap(Map<String, EntityMetadataVOX> doMetaMap) {
        this.doMetaMap = doMetaMap;
    }
}
