package com.tapplent.apputility.data.structure;

import com.tapplent.platformutility.uilayout.valueobject.LoadParameters;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tapplent on 10/06/17.
 */
public class GenericQueryRequest {
    private String genericQueryID;
    private boolean returnOnlySqlQuery;
    private boolean applyPagination;
    private Map<String, LoadParameters> peAliasToLoadParamsMap = new HashMap<>();

    public GenericQueryRequest(String genericQueryID, boolean returnOnlySqlQuery, boolean applyPagination) {
        this.genericQueryID = genericQueryID;
        this.returnOnlySqlQuery = returnOnlySqlQuery;
        this.applyPagination = applyPagination;
    }

    public GenericQueryRequest() {

    }

    public String getGenericQueryID() {
        return genericQueryID;
    }

    public void setGenericQueryID(String genericQueryID) {
        this.genericQueryID = genericQueryID;
    }

    public Map<String, LoadParameters> getPeAliasToLoadParamsMap() {
        return peAliasToLoadParamsMap;
    }

    public void setPeAliasToLoadParamsMap(Map<String, LoadParameters> peAliasToLoadParamsMap) {
        this.peAliasToLoadParamsMap = peAliasToLoadParamsMap;
    }

    public boolean isReturnOnlySqlQuery() {
        return returnOnlySqlQuery;
    }

    public void setReturnOnlySqlQuery(boolean returnOnlySqlQuery) {
        this.returnOnlySqlQuery = returnOnlySqlQuery;
    }

    public boolean isApplyPagination() {
        return applyPagination;
    }

    public void setApplyPagination(boolean applyPagination) {
        this.applyPagination = applyPagination;
    }
}
