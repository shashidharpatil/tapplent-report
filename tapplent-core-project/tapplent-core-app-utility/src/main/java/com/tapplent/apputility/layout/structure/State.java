package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.layout.valueObject.IconVO;

/**
 * Created by tapplent on 19/07/17.
 */
public class State {
    private IconVO stateIcon;
    private String iconColor;

    public IconVO getStateIcon() {
        return stateIcon;
    }

    public void setStateIcon(IconVO stateIcon) {
        this.stateIcon = stateIcon;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }
}
