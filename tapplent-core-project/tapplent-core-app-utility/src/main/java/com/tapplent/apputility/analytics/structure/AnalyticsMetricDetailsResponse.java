package com.tapplent.apputility.analytics.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.apputility.data.structure.UnitResponseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 09/12/16.
 */
public class AnalyticsMetricDetailsResponse {
    private String analyticsMetricDetailPkId;
    private String analyticsMetricFkId;
    private String analysisTypeCodeFkId;
    private int seqNumPosInt;
    private String helpExplanationG11nBigTxt;
    private String chartTitleG11nBigTxt;
    private String chartTypeCodeFkId;
    private String xAxizTitleG11nBigTxt;
    private String yAxizTitle1G11nBigTxt;
    private String yAxizTitle2G11nBigTxt;
    private boolean isLegendVisible;
    private boolean isDrillDownEnabled;
    private boolean isTableAvailable;
    private String legendPositionCodeFkId;
    private String parentAnalyticsMetricDetailFkId;
    @JsonIgnore
    private String parentDrilldownAnalyticsMetricDetailFkId;
    @JsonIgnore
    private AnalyticsMetricDetailsResponse minMetricDetails;
    @JsonIgnore
    private AnalyticsMetricDetailsResponse midMetricDetails;
    @JsonIgnore
    private AnalyticsMetricDetailsResponse maxMetricDetails;
    @JsonIgnore
    private String relatedTabularAnalyticsMetricDetailFkId;
    @JsonIgnore
    private String relatedFilterReceipientDoaCodeFkId;
    @JsonIgnore
    private UnitResponseData yAxisData;
    @JsonIgnore
    private List<XAxisData> xAxisData;
    @JsonIgnore
    private List<XAxisData> subXAxisData;
    @JsonIgnore
    private UnitResponseData minYAxisData;
    @JsonIgnore
    private UnitResponseData avgYAxisData;
    @JsonIgnore
    private UnitResponseData maxYAxisData;
    @JsonIgnore
    private UnitResponseData tableData;
    @JsonIgnore
    private Map<String, Object> dataSeriesMap = new HashMap<>();
    private List<Object> dataSeries = new ArrayList<>();
    public UnitResponseData getyAxisData() {
        return yAxisData;
    }

    public void setyAxisData(UnitResponseData yAxisData) {
        this.yAxisData = yAxisData;
    }

    public String getAnalyticsMetricDetailPkId() {
        return analyticsMetricDetailPkId;
    }

    public void setAnalyticsMetricDetailPkId(String analyticsMetricDetailPkId) {
        this.analyticsMetricDetailPkId = analyticsMetricDetailPkId;
    }

    public String getAnalyticsMetricFkId() {
        return analyticsMetricFkId;
    }

    public void setAnalyticsMetricFkId(String analyticsMetricFkId) {
        this.analyticsMetricFkId = analyticsMetricFkId;
    }

    public String getAnalysisTypeCodeFkId() {
        return analysisTypeCodeFkId;
    }

    public void setAnalysisTypeCodeFkId(String analysisTypeCodeFkId) {
        this.analysisTypeCodeFkId = analysisTypeCodeFkId;
    }

    public int getSeqNumPosInt() {
        return seqNumPosInt;
    }

    public void setSeqNumPosInt(int seqNumPosInt) {
        this.seqNumPosInt = seqNumPosInt;
    }

    public String getHelpExplanationG11nBigTxt() {
        return helpExplanationG11nBigTxt;
    }

    public void setHelpExplanationG11nBigTxt(String helpExplanationG11nBigTxt) {
        this.helpExplanationG11nBigTxt = helpExplanationG11nBigTxt;
    }

    public String getChartTitleG11nBigTxt() {
        return chartTitleG11nBigTxt;
    }

    public void setChartTitleG11nBigTxt(String chartTitleG11nBigTxt) {
        this.chartTitleG11nBigTxt = chartTitleG11nBigTxt;
    }

    public String getChartTypeCodeFkId() {
        return chartTypeCodeFkId;
    }

    public void setChartTypeCodeFkId(String chartTypeCodeFkId) {
        this.chartTypeCodeFkId = chartTypeCodeFkId;
    }

    public String getxAxizTitleG11nBigTxt() {
        return xAxizTitleG11nBigTxt;
    }

    public void setxAxizTitleG11nBigTxt(String xAxizTitleG11nBigTxt) {
        this.xAxizTitleG11nBigTxt = xAxizTitleG11nBigTxt;
    }

    public String getyAxizTitle1G11nBigTxt() {
        return yAxizTitle1G11nBigTxt;
    }

    public void setyAxizTitle1G11nBigTxt(String yAxizTitle1G11nBigTxt) {
        this.yAxizTitle1G11nBigTxt = yAxizTitle1G11nBigTxt;
    }

    public String getyAxizTitle2G11nBigTxt() {
        return yAxizTitle2G11nBigTxt;
    }

    public void setyAxizTitle2G11nBigTxt(String yAxizTitle2G11nBigTxt) {
        this.yAxizTitle2G11nBigTxt = yAxizTitle2G11nBigTxt;
    }

    public boolean isLegendVisible() {
        return isLegendVisible;
    }

    public void setLegendVisible(boolean legendVisible) {
        isLegendVisible = legendVisible;
    }

    public String getLegendPositionCodeFkId() {
        return legendPositionCodeFkId;
    }

    public void setLegendPositionCodeFkId(String legendPositionCodeFkId) {
        this.legendPositionCodeFkId = legendPositionCodeFkId;
    }

    public String getParentAnalyticsMetricDetailFkId() {
        return parentAnalyticsMetricDetailFkId;
    }

    public void setParentAnalyticsMetricDetailFkId(String parentAnalyticsMetricDetailFkId) {
        this.parentAnalyticsMetricDetailFkId = parentAnalyticsMetricDetailFkId;
    }

    public String getParentDrilldownAnalyticsMetricDetailFkId() {
        return parentDrilldownAnalyticsMetricDetailFkId;
    }

    public void setParentDrilldownAnalyticsMetricDetailFkId(String parentDrilldownAnalyticsMetricDetailFkId) {
        this.parentDrilldownAnalyticsMetricDetailFkId = parentDrilldownAnalyticsMetricDetailFkId;
    }

    public String getRelatedTabularAnalyticsMetricDetailFkId() {
        return relatedTabularAnalyticsMetricDetailFkId;
    }

    public void setRelatedTabularAnalyticsMetricDetailFkId(String relatedTabularAnalyticsMetricDetailFkId) {
        this.relatedTabularAnalyticsMetricDetailFkId = relatedTabularAnalyticsMetricDetailFkId;
    }

    public String getRelatedFilterReceipientDoaCodeFkId() {
        return relatedFilterReceipientDoaCodeFkId;
    }

    public void setRelatedFilterReceipientDoaCodeFkId(String relatedFilterReceipientDoaCodeFkId) {
        this.relatedFilterReceipientDoaCodeFkId = relatedFilterReceipientDoaCodeFkId;
    }

    public AnalyticsMetricDetailsResponse getMinMetricDetails() {
        return minMetricDetails;
    }

    public void setMinMetricDetails(AnalyticsMetricDetailsResponse minMetricDetails) {
        this.minMetricDetails = minMetricDetails;
    }

    public AnalyticsMetricDetailsResponse getMidMetricDetails() {
        return midMetricDetails;
    }

    public void setMidMetricDetails(AnalyticsMetricDetailsResponse midMetricDetails) {
        this.midMetricDetails = midMetricDetails;
    }

    public AnalyticsMetricDetailsResponse getMaxMetricDetails() {
        return maxMetricDetails;
    }

    public void setMaxMetricDetails(AnalyticsMetricDetailsResponse maxMetricDetails) {
        this.maxMetricDetails = maxMetricDetails;
    }

    public List<XAxisData> getxAxisData() {
        return xAxisData;
    }

    public void setxAxisData(List<XAxisData> xAxisData) {
        this.xAxisData = xAxisData;
    }

    public List<XAxisData> getSubXAxisData() {
        return subXAxisData;
    }

    public void setSubXAxisData(List<XAxisData> subXAxisData) {
        this.subXAxisData = subXAxisData;
    }

    public UnitResponseData getMinYAxisData() {
        return minYAxisData;
    }

    public void setMinYAxisData(UnitResponseData minYAxisData) {
        this.minYAxisData = minYAxisData;
    }

    public UnitResponseData getAvgYAxisData() {
        return avgYAxisData;
    }

    public void setAvgYAxisData(UnitResponseData avgYAxisData) {
        this.avgYAxisData = avgYAxisData;
    }

    public UnitResponseData getMaxYAxisData() {
        return maxYAxisData;
    }

    public void setMaxYAxisData(UnitResponseData maxYAxisData) {
        this.maxYAxisData = maxYAxisData;
    }

    public UnitResponseData getTableData() {
        return tableData;
    }

    public void setTableData(UnitResponseData tableData) {
        this.tableData = tableData;
    }

    public Map<String, Object> getDataSeriesMap() {
        return dataSeriesMap;
    }

    public void setDataSeriesMap(Map<String, Object> dataSeriesMap) {
        this.dataSeriesMap = dataSeriesMap;
    }

    public List<Object> getDataSeries() {
        return dataSeries;
    }

    public void setDataSeries(List<Object> dataSeries) {
        this.dataSeries = dataSeries;
    }

    public boolean isDrillDownEnabled() {
        return isDrillDownEnabled;
    }

    public void setDrillDownEnabled(boolean drillDownEnabled) {
        isDrillDownEnabled = drillDownEnabled;
    }

    public boolean isTableAvailable() {
        return isTableAvailable;
    }

    public void setTableAvailable(boolean tableAvailable) {
        isTableAvailable = tableAvailable;
    }
}
