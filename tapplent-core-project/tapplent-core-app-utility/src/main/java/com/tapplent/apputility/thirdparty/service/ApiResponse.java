package com.tapplent.apputility.thirdparty.service;

public class ApiResponse {

	private String mOneNoteClientUrl;
	private String mOneNoteWebUrl;
	private String mResponseMessage;
	private int mResponseCode;
	private String oneNoteId;
	
	public String getOneNoteId() {
		return oneNoteId;
	}

	public void setOneNoteId(String oneNoteId) {
		this.oneNoteId = oneNoteId;
	}

	public String getOneNoteClientUrl() {
		return mOneNoteClientUrl;
	}
	
	public String getOneNoteWebUrl() {
		return mOneNoteWebUrl;
	}
	
	public String getResponseMessage() {
		return mResponseMessage;
	}
	
	public int getReseponseCode() {
		return mResponseCode;
	}
	
	public void setOneNoteClientUrl(String url) {
		mOneNoteClientUrl = url;
	}
	
	public void setOneNoteWebUrl(String url) {
		mOneNoteWebUrl = url;
	}
	
	public void setResponseCode(int code) {
		mResponseCode= code;
	}
	
	public void setResponseMessage(String message) {
		mResponseMessage = message;
	}

}
