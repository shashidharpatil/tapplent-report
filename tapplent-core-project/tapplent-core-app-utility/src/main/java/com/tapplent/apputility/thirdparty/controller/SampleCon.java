package com.tapplent.apputility.thirdparty.controller;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.authy.AuthyApiClient;
import com.authy.api.Token;
import com.authy.api.Tokens;
import com.authy.api.User;
import com.authy.api.Users;
import com.tapplent.apputility.thirdparty.service.BoxService;
import com.tapplent.apputility.thirdparty.service.DropBoxService;
import com.tapplent.apputility.thirdparty.service.EverNoteService;
import com.tapplent.apputility.thirdparty.service.GoogleCalenderService;
import com.tapplent.apputility.thirdparty.service.GoogleTaskService;
import com.tapplent.apputility.thirdparty.service.OneNoteService;
import com.tapplent.apputility.thirdparty.service.TwilioServiceImpl;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.twilio.twiml.Body;
import com.twilio.twiml.Media;
import com.twilio.twiml.Message;
import com.twilio.twiml.MessagingResponse;
import com.twilio.twiml.TwiMLException;

@Controller
@RequestMapping("testintegration")
public class SampleCon {
	@SuppressWarnings("unused")
	private static final Logger LOG = LogFactory.getLogger(SampleCon.class);
	private EverNoteService everNoteService;
	private OneNoteService oneNoteService;
	private BoxService boxService;
	private DropBoxService dropBoxService;
	public DropBoxService getDropBoxService() {
		return dropBoxService;
	}

	public void setDropBoxService(DropBoxService dropBoxService) {
		this.dropBoxService = dropBoxService;
	}

	public OneNoteService getOneNoteService() {
		return oneNoteService;
	}

	public void setOneNoteService(OneNoteService oneNoteService) {
		this.oneNoteService = oneNoteService;
	}

	public EverNoteService getEverNoteService() {
		return everNoteService;
	}
	
	@RequestMapping(value={"/do/t/{tenantId}/u/{personId}/e"}, method=RequestMethod.GET)
	@ResponseBody
	public int testIntegration() throws Exception{
		dropBoxService.uploadTapplentAttachmentToDropBox("4FD5EE278D27F2B68296DBCD5A8B5F30","46CBF6B6A18EE564B53215ED96137D8C");
		AuthyApiClient client = new AuthyApiClient("2pAI632dxOOPw4ZTsIclkL6c18VcjoOq", "https://api.authy.com/");
		Users users = client.getUsers();
		Tokens tokens = client.getTokens();
		User user = users.createUser("shashidharpatil1234@email.com", "9739042694", "91");
		int authyUserId = 0;
		 if(user.isOk()){
			    // Store user.getId() in your database
			 authyUserId = user.getId();
			 
            // Request SMS authentication
			client.getUsers().requestSms(authyUserId);
		
		 	}
		 return authyUserId; 
	}
	@RequestMapping(value={"/do/t/{tenantId}/u/{personId}/e/verify/{authyUserId}/{verificationCode}"},method =RequestMethod.GET)
	public void verificationOfCode(@PathVariable int authyUserId,@PathVariable String verificationCode ){
		AuthyApiClient client = new AuthyApiClient("2pAI632dxOOPw4ZTsIclkL6c18VcjoOq", "https://api.authy.com/");
		Token token = client.getTokens().verify(authyUserId, verificationCode);
		if (token.isOk()) {
			TwilioServiceImpl.sendMessage("+919900599705");
			
		}
	}
	@RequestMapping(value={"/do/t/{tenantId}/u/{personId}/e"},method =RequestMethod.POST)
	public void twilioChat(HttpServletRequest request, HttpServletResponse response){
		TwilioServiceImpl.sendMessage("+919660898118");
		String msgText = request.getParameter("Body");
	if(msgText.contains("tapplent")){
		Message message = new Message.Builder()
                .body(new Body("Hello,Welcome to Tapplent..How can I help you"))
                .build();

        MessagingResponse twiml = new MessagingResponse.Builder()
                .message(message)
                .build();
        response.setContentType("application/xml");

        try {
        	response.getWriter().print(twiml.toXml());
        } catch (TwiMLException | IOException e) {
            e.printStackTrace();
        }
	}
		

       
	}
	public void setEverNoteService(EverNoteService everNoteService) {
		this.everNoteService = everNoteService;
	}

	public BoxService getBoxService() {
		return boxService;
	}

	public void setBoxService(BoxService boxService) {
		this.boxService = boxService;
	}
}
