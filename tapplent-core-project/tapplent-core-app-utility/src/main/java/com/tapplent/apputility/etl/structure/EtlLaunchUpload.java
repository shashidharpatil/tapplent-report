package com.tapplent.apputility.etl.structure;

import com.fasterxml.jackson.databind.JsonNode;

public class EtlLaunchUpload {
//	VERSION ID                          
//	LAUNCH IMAGE                        
//	LAUNCH TITLE G11N BLOB              
//	LAUNCH DESCRIPTION G11N BLOB        
//	LAUNCH URL G11N BLOB                
//	LAUNCH NOTES G11N BLOB              
//	LAUNCH EFFECTIVE DATETIME           
//	LAUNCH ACTIVE STATE CODE FK ID      
//	LAUNCH EXTERNAL SYSTEM REFERENCE TXT
//	CUSTOM TEXT BLOB                    
//	CUSTOM NUMBER BLOB                  
//	CUSTOM DATETIME BLOB                
//	CREATEDBY PERSON FK ID              
//	CREATED BY PERSON FULLNAME TXT   
//	CREATED_DATETIME
	private String versionId; 
	private String launchImage;
	private JsonNode launchTitleG11nBlob; 
	private JsonNode launchDescriptionG11nBlob; 
	private JsonNode launchUrlG11nBlob; 
	private JsonNode launchNotesG11nBlob; 
	private String launchEffectiveDatetime; 
	private String launchActiveStateCodeFkId; 
	private String launchExternalSystemReferenceTxt;
	private JsonNode customTextBlob; 
	private JsonNode customNumberBlob; 
	private JsonNode customDatetimeBlob; 
	private String createdbyPersonFkId; 
	private String createdByPersonFullnameTxt;
	private String createdDatetime;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getLaunchImage() {
		return launchImage;
	}
	public void setLaunchImage(String launchImage) {
		this.launchImage = launchImage;
	}
	public JsonNode getLaunchTitleG11nBlob() {
		return launchTitleG11nBlob;
	}
	public void setLaunchTitleG11nBlob(JsonNode launchTitleG11nBlob) {
		this.launchTitleG11nBlob = launchTitleG11nBlob;
	}
	public JsonNode getLaunchDescriptionG11nBlob() {
		return launchDescriptionG11nBlob;
	}
	public void setLaunchDescriptionG11nBlob(JsonNode launchDescriptionG11nBlob) {
		this.launchDescriptionG11nBlob = launchDescriptionG11nBlob;
	}
	public JsonNode getLaunchUrlG11nBlob() {
		return launchUrlG11nBlob;
	}
	public void setLaunchUrlG11nBlob(JsonNode launchUrlG11nBlob) {
		this.launchUrlG11nBlob = launchUrlG11nBlob;
	}
	public JsonNode getLaunchNotesG11nBlob() {
		return launchNotesG11nBlob;
	}
	public void setLaunchNotesG11nBlob(JsonNode launchNotesG11nBlob) {
		this.launchNotesG11nBlob = launchNotesG11nBlob;
	}
	public String getLaunchEffectiveDatetime() {
		return launchEffectiveDatetime;
	}
	public void setLaunchEffectiveDatetime(String launchEffectiveDatetime) {
		this.launchEffectiveDatetime = launchEffectiveDatetime;
	}
	public String getLaunchActiveStateCodeFkId() {
		return launchActiveStateCodeFkId;
	}
	public void setLaunchActiveStateCodeFkId(String launchActiveStateCodeFkId) {
		this.launchActiveStateCodeFkId = launchActiveStateCodeFkId;
	}
	public String getLaunchExternalSystemReferenceTxt() {
		return launchExternalSystemReferenceTxt;
	}
	public void setLaunchExternalSystemReferenceTxt(String launchExternalSystemReferenceTxt) {
		this.launchExternalSystemReferenceTxt = launchExternalSystemReferenceTxt;
	}
	public JsonNode getCustomTextBlob() {
		return customTextBlob;
	}
	public void setCustomTextBlob(JsonNode customTextBlob) {
		this.customTextBlob = customTextBlob;
	}
	public JsonNode getCustomNumberBlob() {
		return customNumberBlob;
	}
	public void setCustomNumberBlob(JsonNode customNumberBlob) {
		this.customNumberBlob = customNumberBlob;
	}
	public JsonNode getCustomDatetimeBlob() {
		return customDatetimeBlob;
	}
	public void setCustomDatetimeBlob(JsonNode customDatetimeBlob) {
		this.customDatetimeBlob = customDatetimeBlob;
	}
	public String getCreatedbyPersonFkId() {
		return createdbyPersonFkId;
	}
	public void setCreatedbyPersonFkId(String createdbyPersonFkId) {
		this.createdbyPersonFkId = createdbyPersonFkId;
	}
	public String getCreatedByPersonFullnameTxt() {
		return createdByPersonFullnameTxt;
	}
	public void setCreatedByPersonFullnameTxt(String createdByPersonFullnameTxt) {
		this.createdByPersonFullnameTxt = createdByPersonFullnameTxt;
	}
	public String getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(String createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
}
