package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.tapplent.platformutility.layout.valueObject.ClientEventRuleToPropertyVO;

public class ClientEventRuleToProperty {
	String clientEventRulePropertyMapId;
	String rule;
	String property;
	public String getClientEventRulePropertyMapId() {
		return clientEventRulePropertyMapId;
	}
	public void setClientEventRulePropertyMapId(String clientEventRulePropertyMapId) {
		this.clientEventRulePropertyMapId = clientEventRulePropertyMapId;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public static ClientEventRuleToProperty fromVO(ClientEventRuleToPropertyVO clientEventRuleToPropertyVO){
		ClientEventRuleToProperty clientEventRuleToProperty = new ClientEventRuleToProperty();
		BeanUtils.copyProperties(clientEventRuleToPropertyVO, clientEventRuleToProperty);
		return clientEventRuleToProperty;
	}
}
