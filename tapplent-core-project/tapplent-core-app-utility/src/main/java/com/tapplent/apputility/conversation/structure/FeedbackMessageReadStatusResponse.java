package com.tapplent.apputility.conversation.structure;

import java.util.List;

/**
 * Created by Tapplent on 7/10/17.
 */
public class FeedbackMessageReadStatusResponse {
    List<GroupMember> groupMembers;

    public List<GroupMember> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(List<GroupMember> groupMembers) {
        this.groupMembers = groupMembers;
    }
}
