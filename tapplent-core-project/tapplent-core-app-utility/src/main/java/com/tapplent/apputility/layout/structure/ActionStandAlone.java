package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.ActionStandAloneVO;

public class ActionStandAlone {
	private String actionStandaloneId;
	private String canvasId;
	private String controlId;
	private String propertyControlId;
	private String baseTemplateId;
	private String mtPEId; 
	private String actionCode;
	private String controlTypeCode;
	private String actionLabel;
	private String actionIcon;
	private boolean isShowActionLabel;
	private boolean isShowActionIcon;
	private String actionOnStateIcon;
	private String actionOffStateIcon;
	private boolean isLicenced;
	private boolean isCreateAccessControlEquivalent;
	private boolean isReadAccessControlEquivalent;
	private boolean isUpdateAccessControlEquivalent;
	private boolean isDeleteAccessControlEquivalent;
	private int actionSeqNo;
	@JsonIgnore
	private JsonNode mstDeviceIndependentBlob;
	@JsonIgnore
	private JsonNode txnDeviceIndependentBlob;
	@JsonIgnore
	private JsonNode mstDeviceDependentBlob;
	@JsonIgnore
	private JsonNode txnDeviceDependentBlob;
	@JsonIgnore
	private JsonNode deviceApplicableBlob;
	@JsonIgnore
	private JsonNode mstActionStepPropertyBlob;
	@JsonIgnore
	private JsonNode txnActionStepPropertyBlob;
	private JsonNode deviceIndependentBlob; 
	private JsonNode deviceDependentBlob;
	@JsonIgnore
	private JsonNode actionStepPropertyBlob;
	public String getActionStandaloneId() {
		return actionStandaloneId;
	}

	public void setActionStandaloneId(String actionStandaloneId) {
		this.actionStandaloneId = actionStandaloneId;
	}

	public String getCanvasId() {
		return canvasId;
	}

	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}

	public String getControlId() {
		return controlId;
	}

	public void setControlId(String controlId) {
		this.controlId = controlId;
	}

	public String getPropertyControlId() {
		return propertyControlId;
	}

	public void setPropertyControlId(String propertyControlId) {
		this.propertyControlId = propertyControlId;
	}

	public String getBaseTemplateId() {
		return baseTemplateId;
	}

	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getMtPEId() {
		return mtPEId;
	}

	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}

	public String getActionCode() {
		return actionCode;
	}

	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	

	public JsonNode getMstDeviceIndependentBlob() {
		return mstDeviceIndependentBlob;
	}

	public void setMstDeviceIndependentBlob(JsonNode mstDeviceIndependentBlob) {
		this.mstDeviceIndependentBlob = mstDeviceIndependentBlob;
	}

	public JsonNode getTxnDeviceIndependentBlob() {
		return txnDeviceIndependentBlob;
	}

	public void setTxnDeviceIndependentBlob(JsonNode txnDeviceIndependentBlob) {
		this.txnDeviceIndependentBlob = txnDeviceIndependentBlob;
	}

	public JsonNode getMstDeviceDependentBlob() {
		return mstDeviceDependentBlob;
	}

	public void setMstDeviceDependentBlob(JsonNode mstDeviceDependentBlob) {
		this.mstDeviceDependentBlob = mstDeviceDependentBlob;
	}

	public JsonNode getTxnDeviceDependentBlob() {
		return txnDeviceDependentBlob;
	}

	public void setTxnDeviceDependentBlob(JsonNode txnDeviceDependentBlob) {
		this.txnDeviceDependentBlob = txnDeviceDependentBlob;
	}

	public JsonNode getMstActionStepPropertyBlob() {
		return mstActionStepPropertyBlob;
	}

	public void setMstActionStepPropertyBlob(JsonNode mstActionStepPropertyBlob) {
		this.mstActionStepPropertyBlob = mstActionStepPropertyBlob;
	}

	public JsonNode getTxnActionStepPropertyBlob() {
		return txnActionStepPropertyBlob;
	}

	public void setTxnActionStepPropertyBlob(JsonNode txnActionStepPropertyBlob) {
		this.txnActionStepPropertyBlob = txnActionStepPropertyBlob;
	}

	public JsonNode getDeviceApplicableBlob() {
		return deviceApplicableBlob;
	}

	public void setDeviceApplicableBlob(JsonNode deviceApplicableBlob) {
		this.deviceApplicableBlob = deviceApplicableBlob;
	}

	public String getControlTypeCode() {
		return controlTypeCode;
	}

	public void setControlTypeCode(String controlTypeCode) {
		this.controlTypeCode = controlTypeCode;
	}

	public String getActionLabel() {
		return actionLabel;
	}

	public void setActionLabel(String actionLabel) {
		this.actionLabel = actionLabel;
	}

	public String getActionIcon() {
		return actionIcon;
	}

	public void setActionIcon(String actionIcon) {
		this.actionIcon = actionIcon;
	}

	public String getActionOnStateIcon() {
		return actionOnStateIcon;
	}

	public void setActionOnStateIcon(String actionOnStateIcon) {
		this.actionOnStateIcon = actionOnStateIcon;
	}

	public String getActionOffStateIcon() {
		return actionOffStateIcon;
	}

	public void setActionOffStateIcon(String actionOffStateIcon) {
		this.actionOffStateIcon = actionOffStateIcon;
	}

	public boolean isShowActionLabel() {
		return isShowActionLabel;
	}

	public void setShowActionLabel(boolean isShowActionLabel) {
		this.isShowActionLabel = isShowActionLabel;
	}

	public boolean isShowActionIcon() {
		return isShowActionIcon;
	}

	public void setShowActionIcon(boolean isShowActionIcon) {
		this.isShowActionIcon = isShowActionIcon;
	}

	

	public boolean isLicenced() {
		return isLicenced;
	}

	public void setLicenced(boolean isLicenced) {
		this.isLicenced = isLicenced;
	}

	public boolean isCreateAccessControlEquivalent() {
		return isCreateAccessControlEquivalent;
	}

	public void setCreateAccessControlEquivalent(boolean isCreateAccessControlEquivalent) {
		this.isCreateAccessControlEquivalent = isCreateAccessControlEquivalent;
	}

	public boolean isReadAccessControlEquivalent() {
		return isReadAccessControlEquivalent;
	}

	public void setReadAccessControlEquivalent(boolean isReadAccessControlEquivalent) {
		this.isReadAccessControlEquivalent = isReadAccessControlEquivalent;
	}

	public boolean isUpdateAccessControlEquivalent() {
		return isUpdateAccessControlEquivalent;
	}

	public void setUpdateAccessControlEquivalent(boolean isUpdateAccessControlEquivalent) {
		this.isUpdateAccessControlEquivalent = isUpdateAccessControlEquivalent;
	}

	public boolean isDeleteAccessControlEquivalent() {
		return isDeleteAccessControlEquivalent;
	}

	public void setDeleteAccessControlEquivalent(boolean isDeleteAccessControlEquivalent) {
		this.isDeleteAccessControlEquivalent = isDeleteAccessControlEquivalent;
	}

	public int getActionSeqNo() {
		return actionSeqNo;
	}

	public void setActionSeqNo(int actionSeqNo) {
		this.actionSeqNo = actionSeqNo;
	}
	
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}

	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}

	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}

	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	
	public JsonNode getActionStepPropertyBlob() {
		return actionStepPropertyBlob;
	}

	public void setActionStepPropertyBlob(JsonNode actionStepPropertyBlob) {
		this.actionStepPropertyBlob = actionStepPropertyBlob;
	}

	//	private String canvasActionStandAloneEuPkId;
//	private String canvasEuFkId;
//	private String canavsActionDefnFkId;
//	private String baseTemplateDepFkId;
//	private String btProcessElementDepFkId;
//	private String viewTypeCodeDepFkId;
//	private String canvasDefnDepFkId;
//	private String actionCodeFkId;
//	private boolean showAtInitialLoad;
//	private String actionGestureCodeFkId;
//	private String actionGestureAnimationCodeFkId;
//	private String controlTypeCodeFkId;
//	private JsonNode actionLabelG11nBlob;
//	private String actionIconFkId;
//	private boolean isShowActionLabel;
//	private boolean isShowActionIcon;
//	private String actionOnStateIconFkId;
//	private String actionOffStateIconFkId;
//	private boolean isLicenced;
//	private boolean isCreateAccessControlEquivalent;
//	private boolean isReadAccessControlEquivalent;
//	private boolean isUpdateAccessControlEquivalent;
//	private boolean isDeleteAccessControlEquivalent;
//	private int actionSeqNo;
//	private JsonNode canvasActionPlacement;
//	private JsonNode canvasActionIconPlacement;
//	private JsonNode canvasActionLabelPlacement;
//	private JsonNode canvasActionStandAloneTheme;
//	private JsonNode isApplicable;
//	public String getCanvasActionStandAloneEuPkId() {
//		return canvasActionStandAloneEuPkId;
//	}
//	public void setCanvasActionStandAloneEuPkId(String canvasActionStandAloneEuPkId) {
//		this.canvasActionStandAloneEuPkId = canvasActionStandAloneEuPkId;
//	}
//	public String getCanvasEuFkId() {
//		return canvasEuFkId;
//	}
//	public void setCanvasEuFkId(String canvasEuFkId) {
//		this.canvasEuFkId = canvasEuFkId;
//	}
//	public String getCanavsActionDefnFkId() {
//		return canavsActionDefnFkId;
//	}
//	public void setCanavsActionDefnFkId(String canavsActionDefnFkId) {
//		this.canavsActionDefnFkId = canavsActionDefnFkId;
//	}
//	public String getBaseTemplateDepFkId() {
//		return baseTemplateDepFkId;
//	}
//	public void setBaseTemplateDepFkId(String baseTemplateDepFkId) {
//		this.baseTemplateDepFkId = baseTemplateDepFkId;
//	}
//	public String getBtProcessElementDepFkId() {
//		return btProcessElementDepFkId;
//	}
//	public void setBtProcessElementDepFkId(String btProcessElementDepFkId) {
//		this.btProcessElementDepFkId = btProcessElementDepFkId;
//	}
//	public String getViewTypeCodeDepFkId() {
//		return viewTypeCodeDepFkId;
//	}
//	public void setViewTypeCodeDepFkId(String viewTypeCodeDepFkId) {
//		this.viewTypeCodeDepFkId = viewTypeCodeDepFkId;
//	}
//	public String getCanvasDefnDepFkId() {
//		return canvasDefnDepFkId;
//	}
//	public void setCanvasDefnDepFkId(String canvasDefnDepFkId) {
//		this.canvasDefnDepFkId = canvasDefnDepFkId;
//	}
//	public String getActionCodeFkId() {
//		return actionCodeFkId;
//	}
//	public void setActionCodeFkId(String actionCodeFkId) {
//		this.actionCodeFkId = actionCodeFkId;
//	}
//	public boolean isShowAtInitialLoad() {
//		return showAtInitialLoad;
//	}
//	public void setShowAtInitialLoad(boolean showAtInitialLoad) {
//		this.showAtInitialLoad = showAtInitialLoad;
//	}
//	public String getActionGestureCodeFkId() {
//		return actionGestureCodeFkId;
//	}
//	public void setActionGestureCodeFkId(String actionGestureCodeFkId) {
//		this.actionGestureCodeFkId = actionGestureCodeFkId;
//	}
//	public String getActionGestureAnimationCodeFkId() {
//		return actionGestureAnimationCodeFkId;
//	}
//	public void setActionGestureAnimationCodeFkId(String actionGestureAnimationCodeFkId) {
//		this.actionGestureAnimationCodeFkId = actionGestureAnimationCodeFkId;
//	}
//	public String getControlTypeCodeFkId() {
//		return controlTypeCodeFkId;
//	}
//	public void setControlTypeCodeFkId(String controlTypeCodeFkId) {
//		this.controlTypeCodeFkId = controlTypeCodeFkId;
//	}
//	public JsonNode getActionLabelG11nBlob() {
//		return actionLabelG11nBlob;
//	}
//	public void setActionLabelG11nBlob(JsonNode actionLabelG11nBlob) {
//		this.actionLabelG11nBlob = actionLabelG11nBlob;
//	}
//	public String getActionIconFkId() {
//		return actionIconFkId;
//	}
//	public void setActionIconFkId(String actionIconFkId) {
//		this.actionIconFkId = actionIconFkId;
//	}
//	public boolean isShowActionLabel() {
//		return isShowActionLabel;
//	}
//	public void setShowActionLabel(boolean isShowActionLabel) {
//		this.isShowActionLabel = isShowActionLabel;
//	}
//	public boolean isShowActionIcon() {
//		return isShowActionIcon;
//	}
//	public void setShowActionIcon(boolean isShowActionIcon) {
//		this.isShowActionIcon = isShowActionIcon;
//	}
//	public String getActionOnStateIconFkId() {
//		return actionOnStateIconFkId;
//	}
//	public void setActionOnStateIconFkId(String actionOnStateIconFkId) {
//		this.actionOnStateIconFkId = actionOnStateIconFkId;
//	}
//	public String getActionOffStateIconFkId() {
//		return actionOffStateIconFkId;
//	}
//	public void setActionOffStateIconFkId(String actionOffStateIconFkId) {
//		this.actionOffStateIconFkId = actionOffStateIconFkId;
//	}
//	public boolean isLicenced() {
//		return isLicenced;
//	}
//	public void setLicenced(boolean isLicenced) {
//		this.isLicenced = isLicenced;
//	}
//	public boolean isCreateAccessControlEquivalent() {
//		return isCreateAccessControlEquivalent;
//	}
//	public void setCreateAccessControlEquivalent(boolean isCreateAccessControlEquivalent) {
//		this.isCreateAccessControlEquivalent = isCreateAccessControlEquivalent;
//	}
//	public boolean isReadAccessControlEquivalent() {
//		return isReadAccessControlEquivalent;
//	}
//	public void setReadAccessControlEquivalent(boolean isReadAccessControlEquivalent) {
//		this.isReadAccessControlEquivalent = isReadAccessControlEquivalent;
//	}
//	public boolean isUpdateAccessControlEquivalent() {
//		return isUpdateAccessControlEquivalent;
//	}
//	public void setUpdateAccessControlEquivalent(boolean isUpdateAccessControlEquivalent) {
//		this.isUpdateAccessControlEquivalent = isUpdateAccessControlEquivalent;
//	}
//	public boolean isDeleteAccessControlEquivalent() {
//		return isDeleteAccessControlEquivalent;
//	}
//	public void setDeleteAccessControlEquivalent(boolean isDeleteAccessControlEquivalent) {
//		this.isDeleteAccessControlEquivalent = isDeleteAccessControlEquivalent;
//	}
//	public int getActionSeqNo() {
//		return actionSeqNo;
//	}
//	public void setActionSeqNo(int actionSeqNo) {
//		this.actionSeqNo = actionSeqNo;
//	}
//	public JsonNode getCanvasActionPlacement() {
//		return canvasActionPlacement;
//	}
//	public void setCanvasActionPlacement(JsonNode canvasActionPlacement) {
//		this.canvasActionPlacement = canvasActionPlacement;
//	}
//	public JsonNode getCanvasActionIconPlacement() {
//		return canvasActionIconPlacement;
//	}
//	public void setCanvasActionIconPlacement(JsonNode canvasActionIconPlacement) {
//		this.canvasActionIconPlacement = canvasActionIconPlacement;
//	}
//	public JsonNode getCanvasActionLabelPlacement() {
//		return canvasActionLabelPlacement;
//	}
//	public void setCanvasActionLabelPlacement(JsonNode canvasActionLabelPlacement) {
//		this.canvasActionLabelPlacement = canvasActionLabelPlacement;
//	}
//	public JsonNode getCanvasActionStandAloneTheme() {
//		return canvasActionStandAloneTheme;
//	}
//	public void setCanvasActionStandAloneTheme(JsonNode canvasActionStandAloneTheme) {
//		this.canvasActionStandAloneTheme = canvasActionStandAloneTheme;
//	}
//	public JsonNode getIsApplicable() {
//		return isApplicable;
//	}
//	public void setIsApplicable(JsonNode isApplicable) {
//		this.isApplicable = isApplicable;
//	}
	public static ActionStandAlone fromVO(ActionStandAloneVO actionStandAloneVO){
		ActionStandAlone actionStandAlone = new ActionStandAlone();
		BeanUtils.copyProperties(actionStandAloneVO, actionStandAlone);
		return actionStandAlone;
	}
}
