package com.tapplent.apputility.data.structure;

/**
 * Created by tapplent on 15/11/16.
 */
public class PropertyControlData {
    private String trueValue;
    private String falseValue;
    public PropertyControlData(String trueValue, String falseValue){
        this.trueValue = trueValue;
        this.falseValue = falseValue;
    }

    public String getTrueValue() {
        return trueValue;
    }

    public void setTrueValue(String trueValue) {
        this.trueValue = trueValue;
    }

    public String getFalseValue() {
        return falseValue;
    }

    public void setFalseValue(String falseValue) {
        this.falseValue = falseValue;
    }
}
