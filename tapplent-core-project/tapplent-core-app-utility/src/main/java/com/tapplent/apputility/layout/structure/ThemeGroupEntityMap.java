package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.layout.valueObject.ThemeGroupEntityMapVO;

public class ThemeGroupEntityMap {
	@JsonIgnore
	private String versionId;
	@JsonIgnore
	private String themeGroupEntityMapId;
	private String themeGroupCode;
	private String themeTemplateCode;
	private String themeEntityTypeCode;
	private String themeEntityCode;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getThemeGroupCode() {
		return themeGroupCode;
	}
	public void setThemeGroupCode(String themeGroupCode) {
		this.themeGroupCode = themeGroupCode;
	}
	public String getThemeTemplateCode() {
		return themeTemplateCode;
	}
	public void setThemeTemplateCode(String themeTemplateCode) {
		this.themeTemplateCode = themeTemplateCode;
	}
	public String getThemeEntityTypeCode() {
		return themeEntityTypeCode;
	}
	public void setThemeEntityTypeCode(String themeEntityTypeCode) {
		this.themeEntityTypeCode = themeEntityTypeCode;
	}
	public String getThemeEntityCode() {
		return themeEntityCode;
	}
	public void setThemeEntityCode(String themeEntityCode) {
		this.themeEntityCode = themeEntityCode;
	}
	public String getThemeGroupEntityMapId() {
		return themeGroupEntityMapId;
	}
	public void setThemeGroupEntityMapId(String themeGroupEntityMapId) {
		this.themeGroupEntityMapId = themeGroupEntityMapId;
	}
	public static ThemeGroupEntityMap fromVO(ThemeGroupEntityMapVO themeGroupVO){
		ThemeGroupEntityMap themeGroup = new ThemeGroupEntityMap();
		BeanUtils.copyProperties(themeGroupVO, themeGroup);
		return themeGroup;
	}
}
