package com.tapplent.apputility.uilayout.structure;

import com.tapplent.apputility.layout.structure.LayoutPermissions;
import com.tapplent.platformutility.uilayout.valueobject.ControlActionInstanceVO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 25/04/17.
 */
public class ControlActionInstance {

    private String actionPkId;
    private String actionMasterCode;
    private String actionGroupCode;
    private String cnfMsgTxt;
    private String successMessage;
    private String failureMessage;
    private String applicableToControlState;
    private String mtPEAlias;
    private List<ActionArgument> actionArguments = new ArrayList<>();
    private List<ActionTarget> targets = new ArrayList<>();;
    private List<LayoutPermissions> layoutPermissions = new ArrayList<>();

    public String getActionPkId() {
        return actionPkId;
    }

    public void setActionPkId(String actionPkId) {
        this.actionPkId = actionPkId;
    }

    public String getActionMasterCode() {
        return actionMasterCode;
    }

    public void setActionMasterCode(String actionMasterCode) {
        this.actionMasterCode = actionMasterCode;
    }

    public String getActionGroupCode() {
        return actionGroupCode;
    }

    public void setActionGroupCode(String actionGroupCode) {
        this.actionGroupCode = actionGroupCode;
    }

    public String getCnfMsgTxt() {
        return cnfMsgTxt;
    }

    public void setCnfMsgTxt(String cnfMsgTxt) {
        this.cnfMsgTxt = cnfMsgTxt;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    public String getApplicableToControlState() {
        return applicableToControlState;
    }

    public void setApplicableToControlState(String applicableToControlState) {
        this.applicableToControlState = applicableToControlState;
    }

    public List<ActionArgument> getActionArguments() {
        return actionArguments;
    }

    public void setActionArguments(List<ActionArgument> actionArguments) {
        this.actionArguments = actionArguments;
    }

    public List<ActionTarget> getTargets() {
        return targets;
    }

    public void setTargets(List<ActionTarget> targets) {
        this.targets = targets;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public List<LayoutPermissions> getLayoutPermissions() {
        return layoutPermissions;
    }

    public void setLayoutPermissions(List<LayoutPermissions> layoutPermissions) {
        this.layoutPermissions = layoutPermissions;
    }

    public static ControlActionInstance fromVO(ControlActionInstanceVO controlActionInstanceVO) {
        ControlActionInstance controlActionInstance = new ControlActionInstance();
        BeanUtils.copyProperties(controlActionInstanceVO, controlActionInstance);
        return controlActionInstance;
    }
}
