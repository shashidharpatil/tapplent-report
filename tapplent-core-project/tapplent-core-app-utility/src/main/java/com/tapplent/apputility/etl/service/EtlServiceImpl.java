package com.tapplent.apputility.etl.service;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;


import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.*;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;
import com.tapplent.apputility.data.structure.*;
import com.tapplent.apputility.etl.structure.*;
import com.tapplent.platformutility.accessmanager.dao.AccessManagerService;
import com.tapplent.platformutility.accessmanager.structure.RowCondition;
import com.tapplent.platformutility.common.util.*;
import com.tapplent.platformutility.etl.valueObject.*;
import com.tapplent.platformutility.g11n.structure.OEMObject;
import com.tapplent.platformutility.hierarchy.service.HierarchyService;
import com.tapplent.platformutility.insert.impl.ValidationError;


import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.thirdPartyApp.gmail.GmailNotification;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;


import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.DeleteObjectsRequest.KeyVersion;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
//import com.jcraft.jsch.ChannelSftp;
//import com.jcraft.jsch.SftpException;
import com.tapplent.apputility.data.service.ServerRuleManager;
import com.tapplent.apputility.layout.structure.BuildDBRequest;
import com.tapplent.apputility.layout.structure.ColumnName;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.etl.dao.EtlDAO;
import com.tapplent.platformutility.expression.jeval.EvaluationException;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.layout.valueObject.DOADetails;
import com.tapplent.platformutility.metadata.MetadataService;
import com.tapplent.platformutility.metadatamap.MetaDataMap;
import com.tapplent.platformutility.metadatamap.MetaDataMapBuilder;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import com.tapplent.tenantresolver.user.UserContext;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
//import org.json.simple.JSONObject;


public class EtlServiceImpl implements EtlService {

	private static final Logger LOG = LogFactory.getLogger(EtlServiceImpl.class);
	private EtlDAO etlDAO;
	private MetadataService metadataService;
	private MetaDataMapBuilder metaDataMapBuilder;
	private ServerRuleManager serverRuleManager;
	private AccessManagerService accessManagerService;
	private HierarchyService hierarchyService;
	private ValidationError validationError;

	private static Map<String, Map<String, String>> columnTypeMap = new HashMap<>();
	private static Map<String, Object> stdColumnNameValueMap = new HashMap<>();
	 private static Map<String, String> iconMap = new HashMap<>();
	private static List<String> hierarchialDOs = new ArrayList<>();
	private static List<EtlIcon> iconList = new ArrayList<>();
	private static Map<String, ETLMetaHelperVO> pkIdsToMetaMap = new HashMap<>();
	private static Map<String, String> attachmentToUuidMap = new HashMap<>();
	private static Map<String,Map<String,String>> g11ValueToLocaleMap = new HashMap<>();
	private static String langCodes[] = {"fr","zh","hu","cs","de","id","en","vi"};
	private static String localeCodes[] = {"fr","zh","hu","cs","de","id", "en"};
	private static Map<String,String> localeCodeTolangCodeMap = new HashMap<>();
	private static Map<String, String> g11nValuesMap = new HashMap<>();
	private static Map<String, Map<String, SheetIDToDBIDHelper>> sheetFKNameToSheetIDToDBIDMap = new HashMap<>();
	private static Set<String> foreignKeyNameToCurrentOccurenceCountMap = new HashSet<>();
	private static boolean isPersonUpdateRequired = false;
	private static String productionKey =null;

	@Override
	@Transactional
	public ResponseData uploadEtlDataByHeader(String headerId) {
		// parse();
		EtlHeader etlHeader = EtlHeader.fromVO(etlDAO.getHeaderByHeaderId(headerId));
		String primaryLocale = etlHeader.getPrimaryLocaleCodeFkId();
		List<EtlMap> etlMaps = getEtlMapByHeaderId(headerId);
		Map<String, List<EtlMapDetails>> etlMapIdMapDetailsMap = getEtlMapDetailsByHeaderId(headerId);
		EntityMetadataVOX sourceEntityMetadataVOX = TenantAwareCache.getMetaDataRepository()
				.getMetadataByBtDo(etlHeader.getBaseTemplateFkId(), etlHeader.getSourceDoCodeFkId());
		List<Map<String, Object>> sourceDataList = getDataListFromEtlUpload(sourceEntityMetadataVOX);
		String existingBucketName = "mytapplent";
//		updateUploadS3ImageFolder(false);
		List<KeyVersion> keys = new ArrayList<>();
		for (EtlMap etlMap : etlMaps) {
			EntityMetadataVOX targetEntityMetadataVOX = TenantAwareCache.getMetaDataRepository()
					.getMetadataByBtDo(etlMap.getTargetBaseTemplateCodeFkId(), etlMap.getTargetDoCodeFkId());
			List<EtlMapDetails> currentMapDetailsList = etlMapIdMapDetailsMap.get(etlMap.getEtlMapPkId());
			boolean isUpdate = etlMap.getTargetDataModeCodeFkId().equals("UPDATE");
			boolean isReplace = etlMap.getTargetDataModeCodeFkId().equals("REPLACE");
			if (isReplace)
				deleteAllFromTargetTable(targetEntityMetadataVOX, existingBucketName, keys);
			List<UnitRequestData> unitRequestDataList = new ArrayList<>();
			for (Map<String, Object> sRecord : sourceDataList) {
				UnitRequestData unitRequestData = new UnitRequestData();
				PEData rootPEData = new PEData();
				Map<Object, EntityAttributeMetadata> uniqueKeyAttributes = new HashMap<>();
				Map<String, Object> tRecord = rootPEData.getData();
				Map<String, Object> tRecordPropogateTrueMap = new HashMap<>();
				Map<String, Object> tRecordPropogateFalseMap = new HashMap<>();
				Map<String, Object> doaValueMap = new HashMap<>();
				Evaluator evaluator = new Evaluator();
				sRecord.entrySet().stream().filter(p -> (p.getValue() != null)).forEach(p -> {
					if (p.getValue() instanceof ObjectNode) {
						Iterator<Entry<String, JsonNode>> nodeIterator = ((ObjectNode) p.getValue()).fields();
						while (nodeIterator.hasNext()) {
							Entry<String, JsonNode> entry = nodeIterator.next();
							doaValueMap.put(entry.getKey(), entry.getValue());
						}
					} else if (p.getValue() instanceof Map) {
						for (Entry<String, Object> entry : ((Map<String, Object>) p.getValue()).entrySet()) {
							doaValueMap.put(entry.getKey(), entry.getValue());
						}
					} else
						doaValueMap.put(p.getKey(), p.getValue());
				});
				sRecord.entrySet()
						.stream().filter(p -> !sourceEntityMetadataVOX.getAttributeByName(p.getKey())
						.getDbDataTypeCode().matches("T_BLOB|T_IMAGE|BOOLEAN") && (p.getValue() != null))
						.forEach(p -> {
							if (p.getValue() instanceof TextNode)
								evaluator.putVariable(p.getKey(), ((TextNode) p.getValue()).asText());
							else
								evaluator.putVariable(p.getKey(), (String) p.getValue());
						});
				for (EtlMapDetails etlMapDetails : currentMapDetailsList) {
					EntityAttributeMetadata targetAttributeMetadata = targetEntityMetadataVOX
							.getAttributeByName(etlMapDetails.getTargetDoaCodeFkId());// FIXME resolve for standard fields
					Object valueToInsert = getValueFromSourceRecord(sRecord, etlMapDetails, targetAttributeMetadata,
							sourceEntityMetadataVOX, existingBucketName, evaluator, doaValueMap, primaryLocale);// for recursion tRecord should go inside.
					if (targetAttributeMetadata.isUniqueAttributeForRecord())
						uniqueKeyAttributes.put(valueToInsert, targetAttributeMetadata);
//					if (etlMapDetails.isPropogateVerion())
//						tRecordPropogateTrueMap.put(targetAttributeMetadata.getDoAttributeCode(), valueToInsert);
//					else
//						tRecordPropogateFalseMap.put(targetAttributeMetadata.getDoAttributeCode(), valueToInsert);
					tRecord.put(targetAttributeMetadata.getDoAttributeCode(), valueToInsert);
				}
//               if(isUpdate){
				Map<String, Object> underlyingExistingRecord = getUnderlyingExistingRecord(targetEntityMetadataVOX, uniqueKeyAttributes);


				if(underlyingExistingRecord != null){
					tRecordPropogateFalseMap.putAll(underlyingExistingRecord);
					//FIXME set the actioncode to EDIT_SUBMIT
				}
//				tRecord.put("propogateTrue", tRecordPropogateTrueMap);
//				tRecord.put("propogateFalse", tRecordPropogateFalseMap);
				rootPEData.setBaseTemplateId(etlMap.getTargetBaseTemplateCodeFkId());
				rootPEData.setDomainObject(etlMap.getTargetDoCodeFkId());
				rootPEData.setInline(false);
				rootPEData.setSubmit(true);
				rootPEData.setLogActivity(true);
//				rootPEData.setLogActivityMTPE("ActivityLog,ActivityLogDetail");
				rootPEData.setUniqueMerge(true);
				unitRequestData.setRootPEData(rootPEData);
				unitRequestDataList.add(unitRequestData);
				buildAndExecuteRequestData(unitRequestDataList, etlHeader, targetEntityMetadataVOX, uniqueKeyAttributes,
						primaryLocale);
				unitRequestDataList.clear();
			}
		}
		// DeleteDataFromS3(s3Client, existingBucketName, keys);
		return getErrorLog();
	}

	public ResponseData getErrorLog() {
		ResponseData responseData = new ResponseData();
//		Map<String, List<String>> errorsMap = new HashMap<>();
//		errorsMap.putAll(validationError.getErrorsToReturn());
//		validationError.clearErrorsToReturn();
		responseData.setErrorLog(validationError.getErrors());
		return responseData;
	}

	@SuppressWarnings("unchecked")
	private void determineActionCode(RequestData requestData, UnitRequestData unitRequestData, EtlHeader etlHeader,
									 EntityMetadataVOX targetEntityMetadataVOX, Map<Object, EntityAttributeMetadata> uniqueKeyAttributes) {
		Map<String, Object> insertData = new HashMap<>();
//		insertData.putAll((Map<? extends String, ? extends Object>) unitRequestData.getRootPEData().getData()
//				.get("propogateTrue"));
//		insertData.putAll((Map<? extends String, ? extends Object>) unitRequestData.getRootPEData().getData()
//				.get("propogateFalse"));
		insertData.putAll(unitRequestData.getRootPEData().getData());
		EntityAttributeMetadata pkMeta = targetEntityMetadataVOX.getFunctionalPrimaryKeyAttribute();
		String saveState = null;
		String pkValue = (String) insertData.get(pkMeta.getDoAttributeCode());
		if (insertData.get(pkMeta.getDoAttributeCode()) != null) {
//			if (pkMeta.getDbDataTypeCode().equals("T_CODE")) {
//				String sql = "SELECT SAVE_STATE_CODE_FK_ID AS '" + targetEntityMetadataVOX.getDomainObjectCode()
//						+ ".SaveState' FROM " + targetEntityMetadataVOX.getDbTableName() + " WHERE "
//						+ pkMeta.getDbColumnName() + " = " + Util.getStringTobeInserted(pkMeta, pkValue)
//						+ " AND RECORD_STATE_CODE_FK_ID = 'CURRENT';";
//				List<Map<String, Object>> dataList = etlDAO.getDataListFromTable(sql, targetEntityMetadataVOX);
//				if (!dataList.isEmpty()) {
//					saveState = (String) dataList.iterator().next().values().iterator().next();
//					if (saveState.equals("SAVED")) {
//						// actionCode = "EDIT_DONE"; //new version
//						requestData.setPreviousActionCodeFkId("EDIT");
//						requestData.setActionCodeFkId("DONE");
//						return;
//					} else {
//						return;
//					}
//				}
//			}
		}
//		if (!uniqueKeyAttributes.isEmpty() && duplicateRecordExist(targetEntityMetadataVOX, uniqueKeyAttributes,
//				pkValue, requestData.getPrimaryLocale())) {
//			LOG.error("DUPLICATE RECORD EXISTS! :" + pkValue + " : " + uniqueKeyAttributes.entrySet());
//			return;
//		} else {
		saveState = etlHeader.getDefaultSaveStateCodeFkId();
		if (saveState.equals("SAVED")) {
			requestData.setActionCodeFkId("ADD_EDIT");
		} else if (saveState.equals("DRAFT")) {
			requestData.setActionCodeFkId("ADD_EDIT");
		}
//		}
		return;
	}

	private boolean duplicateRecordExist(EntityMetadataVOX targetEntityMetadataVOX,
										 Map<Object, EntityAttributeMetadata> uniqueFieldMap, String pkValue, String primaryLocale) {
		Deque<Object> arguments = new ArrayDeque<>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(*) AS COUNT FROM ");
		sql.append(targetEntityMetadataVOX.getDbTableName() + " WHERE ");
		for (Entry<Object, EntityAttributeMetadata> entry : uniqueFieldMap.entrySet()) {
			if (entry.getValue().isGlocalizedFlag()) {
				sql.append("COLUMN_GET(" + entry.getValue().getDbColumnName() + ", '" + primaryLocale + "' AS CHAR) ");
			} else if (entry.getValue().getContainerBlobDbColumnName() != null) {
				sql.append("COLUMN_GET(" + entry.getValue().getContainerBlobDbColumnName() + ", '"
						+ entry.getValue().getDoAttributeCode() + " AS "
						+ Util.getBlobDataTypeFromEntityDataType(entry.getValue().getDbDataTypeCode()));
			} else {
				sql.append(entry.getValue().getDbColumnName());
			}
			arguments.push(Util.getStringTobuildWhereClauseAsParameter(entry.getValue(), entry.getKey(),
					entry.getValue().getToDbColumnName(), primaryLocale));
			sql.append(" = ? AND ");
		}
		sql.replace(sql.length() - 4, sql.length(), "");
		if (pkValue != null) {
			EntityAttributeMetadata primaryAttribute = targetEntityMetadataVOX.getFunctionalPrimaryKeyAttribute();
			sql.append("AND " + primaryAttribute.getDbColumnName() + " <> ?");
			arguments.push(Util.getStringTobeInserted(primaryAttribute, pkValue));
		}
		sql.append(
				" AND RECORD_STATE_CODE_FK_ID IN ('CURRENT', 'FUTURE') AND IS_DELETED = FALSE AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE';");
		SearchResult sr = etlDAO.search(sql.toString(), arguments);
		if (!sr.getData().isEmpty() && (Long) sr.getData().get(0).get("COUNT") > 0)
			return true;
		return false;
	}

	private void DeleteDataFromS3(AmazonS3 s3Client, String existingBucketName, List<KeyVersion> keys) {
		String bucketPath = existingBucketName + "/" + TenantContextHolder.getCurrentTenantID();
		DeleteObjectsRequest multiObjectDeleteRequest = new DeleteObjectsRequest(bucketPath);
		multiObjectDeleteRequest.setKeys(keys);
		try {
			DeleteObjectsResult delObjRes = s3Client.deleteObjects(multiObjectDeleteRequest);
			System.out.format("Successfully deleted all the %s items.\n", delObjRes.getDeletedObjects().size());
		} catch (MultiObjectDeleteException e) {
			e.printStackTrace();
		}
	}

	private void deleteAllFromTargetTable(EntityMetadataVOX targetEntityMetadataVOX, String existingBucketName,
										  List<KeyVersion> keys) {
		List<EntityAttributeMetadata> s3Fields = targetEntityMetadataVOX
				.getAttributeMeta().stream().filter(p -> p.getDbDataTypeCode().matches("T_IMAGEID|T_ATTACH"))
				.collect(Collectors.toList());
		StringBuilder sql = new StringBuilder("SELECT ''");
		for (EntityAttributeMetadata attr : s3Fields) {
			if (attr.getContainerBlobDbColumnName() == null) {
				sql.append(", " + attr.getDbColumnName() + " as '" + attr.getDoAttributeCode() + "'");
			} else
				sql.append(", COLUMN_GET(" + attr.getContainerBlobDbColumnName() + ", '" + attr.getDoAttributeCode()
						+ "' AS BINARY) AS '" + attr.getDoAttributeCode() + "'");
		}
		sql.append(" FROM " + targetEntityMetadataVOX.getDbTableName() + ";");
		List<Map<String, Object>> s3DataList = etlDAO.getDataListFromTable(sql.toString(), targetEntityMetadataVOX);
		for (Map<String, Object> record : s3DataList) {
			for (Entry<String, Object> entry : record.entrySet()) {
				EntityAttributeMetadata attr = targetEntityMetadataVOX.getAttributeByName(entry.getKey());
				if (entry.getValue() != null && attr.getDbDataTypeCode().matches("T_IMAGEID|T_ATTACH"))
					keys.add(new KeyVersion((String) entry.getValue()));
				// else if(attr.getDbDataTypeCode().matches("T_BLOB")){
				// Iterator<Entry<String, JsonNode>> nodeIterator =
				// ((ObjectNode) entry.getValue()).fields();
				// while (nodeIterator.hasNext()){
				// Entry<String, JsonNode> innerEntry = nodeIterator.next();
				// EntityAttributeMetadata innerAttr =
				// targetEntityMetadataVOX.getAttributeByName(innerEntry.getKey());
				// if(innerAttr.getDbDataTypeCode().matches("T_IMAGEID|T_ATTACH"))
				// keys.add(new KeyVersion(innerEntry.getValue().asText()));
				// }
				// }
			}
		}
		try {
			etlDAO.deleteAllFromTargetTable(targetEntityMetadataVOX.getDbTableName());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private Map<String, Object> getUnderlyingExistingRecord(EntityMetadataVOX targetEntityMetadataVOX,
															Map<Object, EntityAttributeMetadata> uniqueKeyAttributes) {
		if (uniqueKeyAttributes.isEmpty())
			return null;
		StringBuilder SQL = new StringBuilder("SELECT ");
		SQL.append(targetEntityMetadataVOX.getVersoinIdAttribute().getDbColumnName() + ", ");
		SQL.append(targetEntityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName() + " FROM ");
		SQL.append(targetEntityMetadataVOX.getDbTableName() + " WHERE ");
		for (Entry<Object, EntityAttributeMetadata> entry : uniqueKeyAttributes.entrySet()) {

			SQL.append(entry.getKey() + " = " + Util.getStringTobeInserted(entry.getValue(), entry.getKey()));
			SQL.append(" AND ");
		}
		SQL.append(
				"SAVE_STATE_CODE_FK_ID = 'SAVED' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND RECORD_STATE_CODE_FK_ID = 'CURRENT' AND IS_DELETED = FALSE ;");
		Map<String, Object> resultSet = etlDAO.getUnderlyingExistingRecord(SQL.toString(), targetEntityMetadataVOX);
		return resultSet;
	}

	// private Map<String, Object> getUnderlyingRelatedRecord(EntityMetadataVOX
	// targetEntityMetadataVOX,
	// List<EtlMapDetails> currentMapDetailsList, EntityMetadataVOX
	// sourceEntityMetadataVOX, List<Map<String, Object>> sourceDataList) {
	// Map<String, Object> underlyingExistingRecords = new HashMap<>();
	// for(Map<String, Object> sRecord : sourceDataList){
	// Map<String, Object> uniqueKeyAttributes = new HashMap<>();
	// for(EtlMapDetails etlMapDetails : currentMapDetailsList){
	// EntityAttributeMetadata attributeMetadata =
	// targetEntityMetadataVOX.getAttributeByName(etlMapDetails.getTargetDoaCodeFkId());
	// if(attributeMetadata.isUniqueAttribute()){
	// Object valueToInsert = getValueFromSourceRecord(sRecord, etlMapDetails,
	// attributeMetadata, sourceEntityMetadataVOX, null);
	// uniqueKeyAttributes.put(attributeMetadata.getDbColumnName(),
	// valueToInsert);
	// }
	// }
	//
	// }
	// return null;
	// }

	private List<Map<String, Object>> getDataListFromEtlUpload(EntityMetadataVOX sourceEntityMetadataVOX) {
		// String sql = Util.getSqlStringForTable(sourceEntityMetadataVOX);//new
		// StringBuilder("SELECT ''");
		StringBuilder sql = new StringBuilder("SELECT ''");
		for (EntityAttributeMetadata attr : sourceEntityMetadataVOX.getAttributeColumnNameMap().values()) {
			// if(!attr.isNonVersionTrackableFlag())
			if (attr.getContainerBlobDbColumnName() == null) {
				if (!attr.getDbDataTypeCode().matches("T_BLOB"))
					sql.append(", " + attr.getDbColumnName() + " as '" + attr.getDoAttributeCode() + "'");
				else
					sql.append(", COLUMN_JSON(" + attr.getDbColumnName() + ") AS '" + attr.getDoAttributeCode() + "'");
			}
		}
		sql.append(" FROM " + sourceEntityMetadataVOX.getDbTableName() + ";");

		List<Map<String, Object>> dataList = etlDAO.getDataListFromTable(sql.toString(), sourceEntityMetadataVOX);
		return dataList;
	}

	private void buildAndExecuteRequestData(List<UnitRequestData> unitRequestDataList, EtlHeader etlHeader,
											EntityMetadataVOX targetEntityMetadataVOX, Map<Object, EntityAttributeMetadata> uniqueKeyAttributes,
											String primaryLocale) {
		RequestData requestData = new RequestData();
		requestData.setPrimaryLocale(primaryLocale);
		determineActionCode(requestData, unitRequestDataList.get(0), etlHeader, targetEntityMetadataVOX,
				uniqueKeyAttributes);
		requestData.setRequestDataList(unitRequestDataList);
		try {
			if (requestData.getActionCodeFkId() != null)
				serverRuleManager.applyServerRulesOnETLWriteRequest(requestData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Object getValueFromSourceRecord(Map<String, Object> sRecord, EtlMapDetails etlMapDetails,
											EntityAttributeMetadata targetAttributeMetadata, EntityMetadataVOX sourceEntityMetadataVOX,
											String existingBucketName, Evaluator evaluator, Map<String, Object> doaValueMap, String primaryLocale) {
		Object valueToReturn = null;
		if (etlMapDetails.getMapSourceDoaExpn() != null) {
			try {
				String exp = etlMapDetails.getMapSourceDoaExpn();
				String[] doas = exp.split("}");
				if (doas.length == 1) {
					String doa = doas[0].substring(2);
					if (targetAttributeMetadata.getDbDataTypeCode().matches("T_IMAGEID|T_ATTACH"))
						valueToReturn = uploadImageToS3(sRecord, doa, etlMapDetails, targetAttributeMetadata, existingBucketName);
//						valueToReturn = Common.getUUID();
					else if (targetAttributeMetadata.getDbDataTypeCode().matches("T_BLOB|T_IMAGE"))
						valueToReturn = ((ObjectNode) sRecord.get(doa)).findValue(doa);
					else
						valueToReturn = sRecord.get(doa);
				} else {
					valueToReturn = evaluator.evaluate(etlMapDetails.getMapSourceDoaExpn());
				}
			} catch (EvaluationException e) {
				e.printStackTrace();
			}
		} else if (etlMapDetails.getMapLookupDoaExpn() != null) {
			String SQL = etlMapDetails.getMapLookupDoaExpn();
			Pattern doaPattern = Pattern.compile("\\!\\{(.*?)\\}");
			Pattern rePattern = Pattern.compile("(\\!\\{(.*?)\\})");
			Matcher doaMatcher = doaPattern.matcher(SQL);
			Matcher reMatcher = rePattern.matcher(SQL);
			boolean isPriDoNameSet = false;
			String pDoName = null;
			while (doaMatcher.find() && reMatcher.find()) {
				if (!isPriDoNameSet) {
					pDoName = doaMatcher.group(1).split("\\.")[0];
					isPriDoNameSet = true;
				}
				String doaName = doaMatcher.group(1);
				if (doaValueMap.get(doaName) != null) {
//					if (sourceEntityMetadataVOX.getAttributeByName(doaName).isGlocalizedFlag()) {
//						String gValue = ((ObjectNode) doaValueMap.get(doaName)).findValue(primaryLocale).asText();
//						SQL = SQL.replace(reMatcher.group(1), "'" + gValue + "'");
//					} else
					SQL = SQL.replace(reMatcher.group(1), Util.getStringTobeInserted(
							sourceEntityMetadataVOX.getAttributeByName(doaName), doaValueMap.get(doaName)));
				} else {
					if (doaName.split("\\.")[0].equals(sourceEntityMetadataVOX.getDomainObjectCode())) {
						SQL = SQL.replace(reMatcher.group(1), " IS NULL ");
					} else {
						MasterEntityAttributeMetadata attributeMeta = TenantAwareCache.getMetaDataRepository()
								.getMetadataByDoa(doaName);
						if (attributeMeta.getContainerBlobDbColumnName() != null) {
							SQL = SQL
									.replace(reMatcher.group(1),
											"COLUMN_GET(" + attributeMeta.getDbTableName() + "."
													+ attributeMeta.getContainerBlobDbColumnName() + ", '"
													+ attributeMeta
													.getDoAttributeCode()
													+ "' AS " + Util.getBlobDataTypeFromEntityDataType(
													attributeMeta.getDbDataTypeCode())
													+ ")");
						} else if (attributeMeta.isGlocalized()) {
							SQL = SQL.replace(reMatcher.group(1), "COLUMN_GET(" + attributeMeta.getDbTableName() + "."
									+ attributeMeta.getDbColumnName() + ", '" + primaryLocale + "' AS CHAR)");
						} else
							SQL = SQL.replace(reMatcher.group(1),
									attributeMeta.getDbTableName() + "." + attributeMeta.getDbColumnName());
					}
				}
			}
			Pattern doPattern = Pattern.compile("\\_\\{(.*?)\\}");
			rePattern = Pattern.compile("(\\_\\{(.*?)\\})");
			Matcher doMatcher = doPattern.matcher(SQL);
			reMatcher = rePattern.matcher(SQL);
			while (doMatcher.find() && reMatcher.find()) {
				String doName = doMatcher.group(1);
				if (doName.equals(sourceEntityMetadataVOX.getDomainObjectCode()))
					SQL = SQL.replace(reMatcher.group(1), sourceEntityMetadataVOX.getDbTableName());
				else {
					MasterEntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository()
							.getMetadataByDo(doName);
					SQL = SQL.replace(reMatcher.group(1), entityMetadataVOX.getDbTableName());
				}
			}
			SQL = SQL.replaceAll("((=)\\s*(IS)\\s*(NULL))", " IS NULL ");
			List<Map<String, Object>> dataList = etlDAO.getDataListFromTableMaster(SQL,
					TenantAwareCache.getMetaDataRepository().getMetadataByDo(pDoName));
			if (!dataList.isEmpty())
				valueToReturn = dataList.iterator().next().values().iterator().next();
		}
		return valueToReturn;
	}

	private Object uploadImageToS3(Map<String, Object> sRecord, String doa, EtlMapDetails etlMapDetails,
								   EntityAttributeMetadata targetAttributeMetadata, String existingBucketName) {
		String doaValue = sRecord.get(doa).toString();
		if(attachmentToUuidMap.containsKey(doaValue))
			return  attachmentToUuidMap.get(doaValue);
		String keyName = null;
		StringBuilder locationDoa = new StringBuilder(etlMapDetails.getAssociatedArtefactLocationDoaCodeFkId());
		locationDoa.replace(0,2, "");
		locationDoa.replace(locationDoa.length()-1, locationDoa.length(), "");
		String imageRelativeLocation = sRecord.get(locationDoa.toString()).toString();
//		String doaValue = ((TextNode) sRecord.get(doa)).asText();

		// String filePath =
		// "/Users/Tapplent/Desktop/Images/"+imageRelativeLocation+"/"+doaValue;//FIXME
		// for ftp and data for file type
//		String filePath = "/home/bitnami/Images/" + imageRelativeLocation;// old Address
		String filePath = "/home/ubuntu/Images/" + imageRelativeLocation;// FIXME for ftp and data for file type
		String amazonFileUploadLocationOriginal = existingBucketName + "/23/" + TenantContextHolder.getCurrentTenantID();
		BufferedInputStream stream = null;
		ChannelSftp channelSftp = null;
		try {
			channelSftp = SystemAwareCache.getSystemRepository().getSftpServerConnection();
			channelSftp.cd(filePath);
			stream = new BufferedInputStream(channelSftp.get(doaValue));
			ObjectMetadata objectMetadata = new ObjectMetadata();
			FileNameMap fileNameMap = URLConnection.getFileNameMap();
			String contentType = fileNameMap.getContentTypeFor(doaValue);
//			objectMetadata.setContentDisposition("attachment; filename=" + doaValue);
			objectMetadata.setContentType(contentType);
			keyName = Common.getUUID();
			AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
			PutObjectRequest putObjectRequest = new PutObjectRequest(amazonFileUploadLocationOriginal, keyName, stream, objectMetadata);
			PutObjectResult result = s3Client.putObject(putObjectRequest);
			stream.close();
			attachmentToUuidMap.put(doaValue, keyName);
			insertIntoAttachTable(doaValue, keyName);
			System.out.println("Etag:" + result.getETag() + "-->" + result);
		} catch (SftpException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return null;
		}

		// InputStream is = channel.getInputStream();

		// AmazonS3 s3Client = new AmazonS3Client(new
		// PropertiesCredentials(EtlServiceImpl.class.getResourceAsStream("AwsCredentials.properties")));
		// try {
		// stream = new FileInputStream(filePath);
		// ObjectMetadata objectMetadata = new ObjectMetadata();
		// FileNameMap fileNameMap = URLConnection.getFileNameMap();
		// String contentType = fileNameMap.getContentTypeFor(filePath);
		//// objectMetadata.addUserMetadata("DisplayName", doaValue);
		// objectMetadata.setContentDisposition("attachment;
		// filename="+doaValue);
		// objectMetadata.setContentType(contentType);
		// objectMetadata.setHeader("Filename", value);
		// keyName = Common.getUUID();
		// PutObjectRequest putObjectRequest = new
		// PutObjectRequest(amazonFileUploadLocationOriginal, keyName, stream,
		// objectMetadata);
		// PutObjectResult result = s3Client.putObject(putObjectRequest);
		// System.out.println("Etag:" + result.getETag() + "-->" + result);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		return keyName;
	}

	@Deprecated
	private MetaDataMap getMetadataMapByBtDoFkId(String sourceDoCodeFkId, String bt) {
		String processElementFkId = etlDAO.getprocessElementIdByBtDo(sourceDoCodeFkId, bt);
		EntityMetadata entityMetadata = metadataService.getMetadataByProcessElement(null, null, processElementFkId);
		MetaDataMap metaDataMap = metaDataMapBuilder.getMetaDataMap(entityMetadata);
		return metaDataMap;
	}

	private Map<String, List<EtlMapDetails>> getEtlMapDetailsByHeaderId(String headerId) {
		List<EtlMapDetailsVO> etlMapDetailsVOs = etlDAO.getEtlMapDetailsByHeaderId(headerId);
		Map<String, List<EtlMapDetails>> etlMapIdMapDetailsMap = new HashMap<>();
		for (EtlMapDetailsVO etlMapDetailsVO : etlMapDetailsVOs) {
			EtlMapDetails etlMapDetails = EtlMapDetails.fromVO(etlMapDetailsVO);
			if (etlMapIdMapDetailsMap.containsKey(etlMapDetails.getEtlMapDepFkId())) {
				etlMapIdMapDetailsMap.get(etlMapDetails.getEtlMapDepFkId()).add(etlMapDetails);
			} else {
				List<EtlMapDetails> etlMapDetailsList = new ArrayList<>();
				etlMapDetailsList.add(etlMapDetails);
				etlMapIdMapDetailsMap.put(etlMapDetails.getEtlMapDepFkId(), etlMapDetailsList);
			}
		}
		return etlMapIdMapDetailsMap;
	}

	private List<EtlMap> getEtlMapByHeaderId(String headerId) {
		List<EtlMapVO> etlMapVOs = etlDAO.getEtlMapByHeaderId(headerId);
		List<EtlMap> etlMaps = new ArrayList<>();
		etlMapVOs.stream().forEach(p -> etlMaps.add(EtlMap.fromVO(p)));
		return etlMaps;
	}

	@Override
	@Transactional
	public ResponseData uploadEtlDataForAllHeaders() {
		@SuppressWarnings("unused")
		List<EtlHeaderVO> etlHeaders = etlDAO.getAllHeaders();
		for (EtlHeaderVO etlHeaderVO : etlHeaders) {
			uploadEtlDataByHeader(etlHeaderVO.getEtlHeaderPkId());
		}
		return null;//FIXME
	}

	@Transactional
	@Override
	public void buildLayout(BuildDBRequest request) {
		DBUploadErrorLog.errorList.clear();
		hierarchialDOs.clear();
		 iconMap.clear();
		columnTypeMap.clear();
		pkIdsToMetaMap.clear();
		g11nValuesMap.clear();
		attachmentToUuidMap.clear();
		g11ValueToLocaleMap.clear();
		foreignKeyNameToCurrentOccurenceCountMap.clear();
		isPersonUpdateRequired = false;

		//generateG11NJsonFile();
		updateColumnTypeMap();
		//g11nForForgotPassword1();
		updateG11ValueMap();
		System.out.println(columnTypeMap.get("PERSON"));
		updateAttachmentToUuidMap();
		updateUploadS3ImageFolder(true);
		deleteAllFromPersonToIdTable();
		List<UpdateColumn> toBeUpdatedColumns = new ArrayList<>();
		List<UpdateColumn> returnedUpdateColumns = null;
		UserContext user = TenantContextHolder.getUserContext();
		Map<Integer, GoogleSheetsDetails> seqNumberToSheetsMap = new HashMap<>();
		List<GoogleSheetsDetails> fileInfoList = request.getFileInfoList();
		List<G11nDetails> g11nDetails = request.getG11nDetailsList();
		for(G11nDetails g11nDetail:g11nDetails){
			localeCodeTolangCodeMap.put(g11nDetail.getLocaleCode(),g11nDetail.getLangCode());
		}
		for (GoogleSheetsDetails sheetDetails : fileInfoList) {
			if (!sheetDetails.getFormatNumber().equals("3")) {
				int seqNumber = sheetDetails.getSeqNumber();
				seqNumberToSheetsMap.put(seqNumber, sheetDetails);
			}
		}
		for (int sheetNumber = 0; sheetNumber < 50; sheetNumber++) {
			if (seqNumberToSheetsMap.containsKey(sheetNumber)) {
				GoogleSheetsDetails sheet = seqNumberToSheetsMap.get(sheetNumber);
				if(sheet.getFolderPath().contains("MTBT")){
					try {
						// Now there are no folders. We just have to read the sheets
						FileInputStream excelFile = new FileInputStream(new File(sheet.getFolderPath()));
						Workbook workbook = new XSSFWorkbook(excelFile);
						int noOfSheets = workbook.getNumberOfSheets();
						for(int i =0; i< noOfSheets; i++){
							String sheetName = workbook.getSheetName(i);
							if(!sheetName.startsWith(".")) {
								if (sheetName.contains("CreateTable")) {
									List<Object> rawData = null;
									Sheet createTableSheet = workbook.getSheet(sheetName);
									// here pass the sheet
									rawData = extractDataFromCsvFormat1(createTableSheet);
									insertIntoTable(rawData, true);
								}
							}
						}
//						Files.walk(Paths.get(sheet.getFolderPath().toString())).forEach(filePath -> {
//							if (Files.isRegularFile(filePath)) {
//								if (!filePath.getFileName().toString().startsWith(".")) {
//									try{
//										if(filePath.getFileName().toString().contains("CreateTable")){
//											List<Object> rawData = null;
//											rawData = extractDataFromCsvFormat1(filePath.toString());
//											insertIntoTable(rawData, true);
//										}
//									}catch(NumberFormatException e){
//										LOG.debug("File does not start with a number ->", filePath.getFileName());
//									}
//								}
//							}
//						});
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		for (int sheetNumber = 0; sheetNumber < 50; sheetNumber++) {
			if (seqNumberToSheetsMap.containsKey(sheetNumber)) {
				GoogleSheetsDetails sheet = seqNumberToSheetsMap.get(sheetNumber);
				returnedUpdateColumns = uploadTheFolder(Paths.get(sheet.getFolderPath()),
						Integer.parseInt(sheet.getFormatNumber()));
				toBeUpdatedColumns.addAll(returnedUpdateColumns);
			}
		}
//		returnedUpdateColumns = uploadETL();

		toBeUpdatedColumns.addAll(returnedUpdateColumns);
		if(isPersonUpdateRequired)
			updatePersonTxn();
		updateValues(toBeUpdatedColumns);
//		insertAccessControlDefaultValues();
//		populateSubjectUser();
//		addIndexesToDatabaseTables();
//		addIndexesToAccessManagerTables();
//		updateHierarchyPersonTable();
//		generateIconFileWithoutDuplicates();
//		addG11nStoredProcedure();
//		updateHierarchyTables();
	}

	private void updateColumnTypeMap() {
		List<SheetIDToDBIDHelper> sheetIDToDBIDHelpers = etlDAO.getAllColumnTypeToIDValues();
		// Now with the the help of this we have to populate the columnType To ID Map.
		for(SheetIDToDBIDHelper sheetIDToDBIDHelper : sheetIDToDBIDHelpers){
			if(columnTypeMap.containsKey(sheetIDToDBIDHelper.getSheetForeignKeyName())){
				Map<String, String> sheetIDToDBIDMap =  columnTypeMap.get(sheetIDToDBIDHelper.getSheetForeignKeyName());
				if(sheetIDToDBIDMap.containsKey(sheetIDToDBIDHelper.getSheetID())){
					// THROW ERROR HERE
				}else{
					sheetIDToDBIDMap.put(sheetIDToDBIDHelper.getSheetID(), sheetIDToDBIDHelper.getDatabasePrimaryKeyID());
				}
			}else{
				Map<String, String> sheetIDToDBIDMap = new HashMap<>();
				sheetIDToDBIDMap.put(sheetIDToDBIDHelper.getSheetID(), sheetIDToDBIDHelper.getDatabasePrimaryKeyID());
				columnTypeMap.put(sheetIDToDBIDHelper.getSheetForeignKeyName(), sheetIDToDBIDMap);
			}
		}

	}

	private void addG11nStoredProcedure() {
		try {
			etlDAO.addG11nStoredProcedure();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void updateUploadS3ImageFolder(boolean isLocal) {
		if(! isLocal){
			ChannelSftp channelSftp =  SystemAwareCache.getSystemRepository().getSftpServerConnection();
			String parentFolderPath = "/home/ubuntu/Images/";
			String tempFolder = "/home/ubuntu/";
			String destinationFolderPath = "/home/ubuntu/ImagesFH";
			File tempFolderFile = new File("ImagesTemp");
			try {
				InputStream inputStream = channelSftp.get(parentFolderPath);
				channelSftp.cd(tempFolder);
//				channelSftp.put(inputStream, tempFolderFile.getName());
				channelSftp.put( inputStream , "ImagesTemp");
			} catch (SftpException e) {
				e.printStackTrace();
			}
			File[] listOfAttachFolders = tempFolderFile.listFiles();
			String[] subDirectories = tempFolderFile.list(new FilenameFilter() {
				@Override
				public boolean accept(File current, String name) {
					return new File(current, name).isDirectory();
				}
			});
			for(String subDirectory : subDirectories){
				File subDirectoryFolder = new File(parentFolderPath+subDirectory);
				File[] listOfFiles = subDirectoryFolder.listFiles();
				for(File mediaFile: listOfFiles){
					File sourceFile = mediaFile;
					File destFile = new File(sourceFile.getName());
					try {
						channelSftp.put(new FileInputStream(destFile), destinationFolderPath+"/"+sourceFile.getName());
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (SftpException e1) {
						e1.printStackTrace();
					}
//	                try {
//	                    Util.copyFileUsingStream(sourceFile, destFile);
//	                } catch (IOException e) {
//	                    e.printStackTrace();
//	                }
				}
			}
		}else{
			String parentFolderPath = "/Users/tapplent/Google Drive/Common Production Images in App/";
			String destinationFolderPath = "/Users/tapplent/Documents/DataUploadSheets/UploadS3Image";
		    /* loop through the parent and copy all the images in destination */
			populateAllLevelHierarchies(parentFolderPath, destinationFolderPath);
		}
	}

	private void populateAllLevelHierarchies(String parentFolderPath, String destinationFolderPath) {
		File parentFolderFile = new File(parentFolderPath);
		File[] files = parentFolderFile.listFiles();
		File destFile = new File(destinationFolderPath);
		for(File file : files){
			if(file.isDirectory()){
				populateAllLevelHierarchies(file.getAbsolutePath(),destFile.getAbsolutePath());
			}else{
				try {
					if(!file.isHidden()) {
						File newDestFile = new File(destinationFolderPath + "/" + file.getName());
						Util.copyFileUsingStream(file, newDestFile);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

//		String[] subDirectories = parentFolderFile.list(new FilenameFilter() {
//			@Override
//			public boolean accept(File current, String name) {
//				return new File(current, name).isDirectory();
//			}
//		});
//		List<String> nonFilesInSubDirectory = new ArrayList<>();
//		for(String subDirectory : subDirectories){
//			File subDirectoryFolder = new File(parentFolderPath+subDirectory);
//			File[] listOfFiles = subDirectoryFolder.listFiles();
//			for(File imageFile: listOfFiles){
//				File destFile = new File(destinationFolderPath+"/"+imageFile.getName());
//				try {
//					LOG.debug(imageFile.getName());
//					if(imageFile.isDirectory()){
//						populateAllLevelHierarchies(imageFile.getAbsolutePath(), destinationFolderPath);
//					}else{
//						Util.copyFileUsingStream(imageFile, destFile);
//					}
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
	}

	private void updateAttachmentToUuidMap() {
		String sql = "SELECT ATTACHMENT_NAME, ATTACHMENT_ID FROM T_ATTACHMENT_DB.TMP_ATTACHMENT_MAP;";
		SearchResult sr = etlDAO.search(sql, null);
		List<Map<String, Object>> rs = sr.getData();
		if(rs != null && !rs.isEmpty()){
			String attachName;
			String attachId;
			for(Map<String, Object> attachEntry : rs){
				attachName = attachEntry.get("ATTACHMENT_NAME").toString();
				attachId = attachEntry.get("ATTACHMENT_ID").toString();
				attachmentToUuidMap.put(attachName, attachId);
			}
		}
	}
	private void updateG11ValueMap() {
		String sql = "SELECT VALUE_CODE, LOCALE_CODE,LOCALE_VALUE FROM T_G11N_VALUE_DB.G11N_VALUE_MAP;";
		SearchResult sr = etlDAO.search(sql, null);
		List<Map<String, Object>> rs = sr.getData();
		if(rs != null && !rs.isEmpty()){
			String valueCode;
			String localeCode;
			String localeValue;
			for(Map<String, Object> attachEntry : rs){
				valueCode = attachEntry.get("VALUE_CODE").toString();
				localeCode = attachEntry.get("LOCALE_CODE").toString();
				localeValue = attachEntry.get("LOCALE_VALUE").toString();
				if(g11ValueToLocaleMap.containsKey(valueCode)){
					Map<String,String> localeCodeToValueMap = g11ValueToLocaleMap.get(valueCode);
					localeCodeToValueMap.put(localeCode, localeValue);
				}else{
					Map<String,String> localeCodeToValueMap = new HashMap<>();
					localeCodeToValueMap.put(localeCode, localeValue);
					g11ValueToLocaleMap.put(valueCode, localeCodeToValueMap);
				}
			}
		}
	}

	/**
	 * This method writes the default values for all the persons. Giving access
	 * to all the records uploaded through this call.
	 */
	@Override
	@Transactional
	public void insertAccessControlDefaultValues() {
		List<ETLMetaHelperVO> accessControlGovernedDOs = etlDAO.getAccessControlGovernedDOs();
//		List<EtlAccessControlHelperVO> accessControlHelperVOs = etlDAO.getAccessControlGovernedDO();
//		// Now that we have list of all access control governed tables we should INSERT INTO SELECT
		for(ETLMetaHelperVO helperVO : accessControlGovernedDOs){
//			etlDAO.populateAccessControlforDO(helperVO);
			populateAccessControlForDO(helperVO);
		}
//		Map<String, String> personNameToIdMap = columnTypeMap.get("PERSON");
//		for (Map.Entry<String, String> entry : personNameToIdMap.entrySet()) {
//			String personId = entry.getValue();
//			for (Map.Entry<String, ETLMetaHelperVO> dbPkEntry : pkIdsToMetaMap.entrySet()) {
//				String value = dbPkEntry.getKey();
//				ETLMetaHelperVO doMeta = dbPkEntry.getValue();
//				/* Now insert into the database */
//				String rowAccessPkId = Common.getUUID();
//				etlDAO.insertRowAccessCondition(rowAccessPkId, personId, doMeta.getDoName(), value, null, true, true,
//						false, "NOW()");
//				etlDAO.insertColumnAccessCondition(rowAccessPkId, doMeta.getDoAttrCodeList(), true, true, false,
//						"NOW()");
//			}
//		}
	}

	private void populateAccessControlForDO(ETLMetaHelperVO helperVO) {
		MasterEntityMetadataVOX masterEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(helperVO.getMtPE());
		Map<String, RowCondition> rowConditionMap = accessManagerService.getRowConditions(helperVO.getMtPE());
		String subjectUserColumnName = masterEntityMetadataVOX.getAttributeByName(helperVO.getSubjectUserPath().replaceAll("[!{|}]", "")).getDbColumnName();
		String primaryKeyColumnName = masterEntityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName();
//		String whatDbPrimaryKey = (String) globalVariables.insertData.getData().get(metadataVOX.getVersionIdAttribute().getDoAttributeCode());
//		List<String> whatDbPrimaryKeyList = etlDAO.getDBPrimaryKeyList(helperVO.getTableName(), primaryKeyColumnName);
//        String whoseSubjectUserPersonId = null;

//		if (masterEntityMetadataVOX.)
		Map<String, Object> records = getAllRecords(subjectUserColumnName, primaryKeyColumnName, helperVO.getTableName());
		if (helperVO.getParentMtPE() == null) {
			try {
				accessManagerService.populateAccessControlForBasePETableByRecords(rowConditionMap, records, helperVO, primaryKeyColumnName);
			} catch (SQLException e) {
				e.printStackTrace();
			}
//			accessManagerService.updatePrimaryKeyForIntermediateTable(primaryKeyColumnName, helperVO);
	//		if (helperVO.getParentMtPE() == null)
	//			accessManagerService.populateRowIntermediateForBasePETable(rowConditionMap, subjectUserColumnName, primaryKeyColumnName, helperVO);
	//		else accessManagerService.populateRowIntermediateForChildPETable(rowConditionMap, subjectUserColumnName, primaryKeyColumnName, helperVO);
		}
		else {
			try {
				accessManagerService.populateRowIntermediateForChildPETableByRecords(rowConditionMap, records, helperVO, primaryKeyColumnName);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private Map<String,Object> getAllRecords(String subjectUserColumnName, String primaryKeyColumnName, String tableName) {
		Map<String, Object> records = etlDAO.getAllRecords(subjectUserColumnName, primaryKeyColumnName, tableName);
		return records;
	}

	/**
	 *
	 */
	private void populateSubjectUser() {
		// get all the dos where subject user is not null;
		List<ETLMetaHelperVO> subjectUserDrivenDO = etlDAO.getSubjectUserDrivenDOs();
		for(ETLMetaHelperVO helperVO : subjectUserDrivenDO){
			String subjectUserPath = helperVO.getSubjectUserPath();
			String subjectUserDOA = helperVO.getSubjectUserPath().substring(2, helperVO.getSubjectUserPath().length()-1);
			StringBuilder SQL = new StringBuilder("UPDATE T_PFM_IEU_AM_ROW_RESOLVED SET WHOSE_SUBJECT_USER_PERSON_FK_ID = (SELECT ");
			StringBuilder FROMSQL = new StringBuilder(" FROM ");
			StringBuilder WHERESQL = new StringBuilder(" WHERE WHAT_DB_PRIMARY_KEY_ID = ");
			// get the meta data of all the doas.

			// Now get the actual column
			String[] doas = subjectUserDOA.split(":");
			DOADetails previousDOADetails = null;
			for(int i = 0 ;i< doas.length; i++){
				DOADetails doaDetails = etlDAO.getDOADetailsByDOAName(doas[i]);
				//This is the last doa.
				if(i == 0){
					// build here the from clause
					FROMSQL.append(doaDetails.getTableName()).append(" ");
					previousDOADetails = doaDetails;
					WHERESQL.append(helperVO.getTableName()+"."+helperVO.getPrimaryKeyColumnName());
				}else{
					FROMSQL.append(" INNER JOIN ").append(doaDetails.getTableName()).append(" ON ").append(previousDOADetails.getTableName()).append(".").append(previousDOADetails.getColumnName());
					// Get the current dos PK.
					ETLMetaHelperVO currentDO = etlDAO.getMetaByDoName(doaDetails.getDoCode());
					FROMSQL.append(" = ").append(doaDetails.getTableName()).append(".").append(currentDO.getPrimaryKeyColumnName());
					// build here the join clause
				}
				if(i == doas.length-1) {
					// build here the select clause
					SQL.append(doaDetails.getTableName() + "." + doaDetails.getColumnName());
				}
			}
			SQL.append(FROMSQL).append(WHERESQL).append(");");
			try {
				etlDAO.executeUpdate(SQL.toString(), new ArrayList<>());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 *
	 */
	public String getSubjectUser(String subjectUserDrivenDO, String pk) {
		String subjectUserId = null;
		ETLMetaHelperVO helperVO = etlDAO.getMetaByDoName(subjectUserDrivenDO);
		String subjectUserDOA = helperVO.getSubjectUserPath().substring(2, helperVO.getSubjectUserPath().length()-1);
		StringBuilder SQL = new StringBuilder("SELECT ");
		StringBuilder FROMSQL = new StringBuilder(" FROM ");
		StringBuilder WHERESQL = new StringBuilder(" WHERE ");
		// get the meta data of all the doas.

		// Now get the actual column
		String[] doas = subjectUserDOA.split(":");
		DOADetails previousDOADetails = null;
		for(int i = 0 ;i< doas.length; i++){
			DOADetails doaDetails = etlDAO.getDOADetailsByDOAName(doas[i]);
			//This is the last doa.
			if(i == 0){
				// build here the from clause
				FROMSQL.append(doaDetails.getTableName()).append(" ");
				previousDOADetails = doaDetails;
				WHERESQL.append(helperVO.getTableName()+"."+helperVO.getPrimaryKeyColumnName()).append("=");
				if(helperVO.getPrimaryKeyDBDataType().equals("T_ID")){
					WHERESQL.append("0x").append(pk);
				}else{
					WHERESQL.append("'"+pk+"'");
				}
			}else{
				FROMSQL.append(" INNER JOIN ").append(doaDetails.getTableName()).append(" ON ").append(previousDOADetails.getTableName()).append(".").append(previousDOADetails.getColumnName());
				// Get the current dos PK.
				ETLMetaHelperVO currentDO = etlDAO.getMetaByDoName(doaDetails.getDoCode());
				FROMSQL.append(" = ").append(doaDetails.getTableName()).append(".").append(currentDO.getPrimaryKeyColumnName());
				// build here the join clause
			}
			if(i == doas.length-1) {
				// build here the select clause
				SQL.append(doaDetails.getTableName() + "." + doaDetails.getColumnName());
			}
		}
		SQL.append(FROMSQL).append(WHERESQL);
		subjectUserId = etlDAO.getSubjectUserId(SQL, helperVO.getPrimaryKeyDBDataType());
		return subjectUserId;
	}
	/**
	 * This method adds the indexes to all the tables in databases 1. It creates
	 * default indexes for functional primary key 2. It creates default indexes
	 * for all the dependency keys 3. It creates full text search for all the
	 * searchable fields which are texts
	 */
	@Transactional
	public void addIndexesToDatabaseTables() {
		/* To create indexes we need table name and column name */
		/*
		 * 1. The syntax is CREATE INDEX 'index name' ON 'table name'
		 * (columnName1, columnName2); 2. We shall loop through the BT-DO
		 * attribute table check the corresponding flags and make appropriate
		 * indexes
		 *
		 */
//       List<EntityAttributeMetadata> btDOAttributeList = metadataService.getBTAttributeList();

		List<DOADetails> doaDetailsList = etlDAO.getBTDOADetails();
		Map<String, DBIndex> indexDOAToIndexDetails = new HashMap<>();
		for (DOADetails btDOA : doaDetailsList) {
			if (btDOA.isFunctionalPrimaryKeyFlag()) {
				if (!indexDOAToIndexDetails.containsKey(btDOA.getDoaName())) {
					indexDOAToIndexDetails.put(btDOA.getDoaName(), new DBIndex(btDOA.getDoaName(),
							btDOA.getColumnName(), btDOA.getTableName(), "FuncPrim"));
					createIndex(indexDOAToIndexDetails.get(btDOA.getDoaName()));
				}
			}
			if (btDOA.isRelationFlag()) {
				if (!indexDOAToIndexDetails.containsKey(btDOA.getDoaName())) {
					indexDOAToIndexDetails.put(btDOA.getDoaName(), new DBIndex(btDOA.getDoaName(),
							btDOA.getColumnName(), btDOA.getTableName(), "Reln"));
					createIndex(indexDOAToIndexDetails.get(btDOA.getDoaName()));
				}
			}
			if (btDOA.isLocalSearchableFlag()) {
				if (btDOA.getDbDataType().equals("TEXT")||btDOA.getDbDataType().equals("T_BIG_TEXT")||btDOA.getDbDataType().equals("LONG_TEXT")) {
					if (!indexDOAToIndexDetails.containsKey(btDOA.getDoaName())) {
						indexDOAToIndexDetails.put(btDOA.getDoaName(), new DBIndex(btDOA.getDoaName(),
								btDOA.getColumnName(), btDOA.getTableName(), "SrchTxt"));
						createIndex(indexDOAToIndexDetails.get(btDOA.getDoaName()));
					}
				}
			}
		}
	}

	@Override
	@Transactional
	public void addIndexesToAccessManagerTables(){
		List<String> amTables = etlDAO.getAccessManagerTables("%_AM_ROW_INT");
		for(String tableName :amTables){
			List<Object> parameters = new ArrayList<>();
			StringBuilder rowPKINTMIndexSQL = new StringBuilder("CREATE INDEX access_Manager_row_PK ON ");
			try {
				rowPKINTMIndexSQL.append(tableName).append(" ")
						.append("(AM_ROW_INTERMEDIATE_PK_ID);");

					etlDAO.executeUpdate(rowPKINTMIndexSQL.toString(), parameters);

				StringBuilder whoPKINTMIndexSQL = new StringBuilder("CREATE INDEX access_Manager_who_Prn ON ");
				whoPKINTMIndexSQL.append(tableName).append(" ")
						.append("(WHO_PERSON_FK_ID);");
				etlDAO.executeUpdate(whoPKINTMIndexSQL.toString(), parameters);
				StringBuilder whatDBPKINTMIndexSQL = new StringBuilder("CREATE INDEX access_Manager_what_db_pk ON ");
				whatDBPKINTMIndexSQL.append(tableName).append(" ")
						.append("(WHAT_DB_PRIMARY_KEY_ID);");
				etlDAO.executeUpdate(whatDBPKINTMIndexSQL.toString(), parameters);
				StringBuilder rowCondDBPKINTMIndexSQL = new StringBuilder("CREATE INDEX access_Manager_row_cond ON ");
				rowCondDBPKINTMIndexSQL.append(tableName).append(" ")
						.append("(AM_ROW_CONDN_FK_ID);");
				etlDAO.executeUpdate(rowCondDBPKINTMIndexSQL.toString(), parameters);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		List<String> amTables1 = etlDAO.getAccessManagerTables("%_AM_ROW_RSV");
		for(String tableName :amTables1){
			List<Object> parameters = new ArrayList<>();
			StringBuilder rowResPKIndexSQL = new StringBuilder("CREATE INDEX access_Manager_row_res_PK ON ");
			try {
				rowResPKIndexSQL.append(tableName).append(" ")
						.append("(AM_ROW_RESOLVED_PK_ID);");
				etlDAO.executeUpdate(rowResPKIndexSQL.toString(), parameters);
				StringBuilder whoPersonIndexSQL = new StringBuilder("CREATE INDEX access_Manager_who_prn_PK ON ");
				whoPersonIndexSQL.append(tableName).append(" ")
					.append("(WHO_PERSON_FK_ID);");
				etlDAO.executeUpdate(whoPersonIndexSQL.toString(), parameters);
				StringBuilder whatDBPrimaryIndexSQL = new StringBuilder("CREATE INDEX access_Manager_what_db_PK ON ");
				whatDBPrimaryIndexSQL.append(tableName).append(" ")
					.append("(WHAT_DB_PRIMARY_KEY_ID);");
				etlDAO.executeUpdate(whatDBPrimaryIndexSQL.toString(), parameters);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		List<String> amTables2 = etlDAO.getAccessManagerTables("%_AM_COL_RSV");
		for(String tableName :amTables2){
			StringBuilder indexSQL = new StringBuilder("CREATE INDEX access_Manager ON ");
			indexSQL.append(tableName).append(" ")
					.append("(AM_ROW_RESOLVED_FK_ID);");
			List<Object> parameters = new ArrayList<>();
			try {
				etlDAO.executeUpdate(indexSQL.toString(), parameters);
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	@Override
	@Transactional
	public void updateRowConditionDefaultValues() {
		etlDAO.updateRowConditionDefaultValues();
	}

	@Override
	@Transactional
	public void updatePersonJobRelationShipSubjectUser() {
		etlDAO.updatePersonJobRelationShipSubjectUser();
	}

	/**
	 * @param dbIndex
	 */
	private void createIndex(DBIndex dbIndex) {
		StringBuilder indexSQL = new StringBuilder("CREATE ");
		if (dbIndex.getIndexReason().equals("SrchTxt")) {
			indexSQL.append(" FULLTEXT INDEX ");
		} else {
			indexSQL.append(" INDEX ");
		}
		indexSQL.append(dbIndex.getIndexReason()).append("_").append(dbIndex.getColumnName()).append(" ON ")
				.append(dbIndex.getTableName()).append(" (").append(dbIndex.getColumnName()).append(");");
		List<Object> parameters = null;
		try {
			etlDAO.executeUpdate(indexSQL.toString(), parameters);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void readJson(){
		String fileNamePath = "/Users/tapplent/Documents/LAYOUT/LAYOUT.csv";
		//JSONParser parser = new JSONParser();
		try {

			//Object obj = parser.parse(new FileReader("/Users/tapplent/Downloads/tabletComination.json"));
			String file = readFile("/Users/tapplent/Downloads/tabletComination.json");

			JSONObject jsonObject = new JSONObject(file);
			Set<String> keys1 = jsonObject.keySet();
			Iterator<String> keys = keys1.iterator();

			List<String> keyMap = new ArrayList<>();

			while(keys.hasNext()) {
				String jsonkey = keys.next();
				if(!keyMap.contains(jsonkey))
					keyMap.add(jsonkey);
			}

			CSVReader csvReader = new CSVReader(new FileReader(fileNamePath));
			csvReader.readNext();
			String[] rowValues = null;
			List<String> rowValue = new ArrayList<>();
			while ((rowValues = csvReader.readNext()) != null) {
				if (!rowValues[0].equals("") && rowValues[0] !=null)
					rowValue.add(rowValues[0]);
			}
			JSONObject JsonFileNotContainsKey = new JSONObject();
			JSONObject LayoutSheetNotContainsKey = new JSONObject();
			for(String value:rowValue){

					if(!keyMap.contains(value)) {
						//JsonFile does not contain key that is there in layout sheet
						JsonFileNotContainsKey.put(value,value);

					}
				for (String entry : keyMap)
				{
					if(!rowValue.contains(entry)){
						//Layout sheet does not contain the key that is there in the jsonFile
						LayoutSheetNotContainsKey.put(entry,entry);

					}
				}
				}
			writeToJSONFile("/Users/tapplent/Documents/LAYOUT/JsonFileNotContainsKey.json", JsonFileNotContainsKey);
			writeToJSONFile("/Users/tapplent/Documents/LAYOUT/LayoutSheetNotContainsKey", LayoutSheetNotContainsKey);
			}
		 catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static String readFile(String filename) {
		String result = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while (line != null) {
				sb.append(line);
				line = br.readLine();
			}
			result = sb.toString();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	public void generateG11NJsonFile(){
		//This function will generate the g11n json files for the client to render hardcoded screens
		updateG11ValueMap();
		Map<String,String> localeTolangCodeMap = new HashMap<>();
		localeTolangCodeMap.put("fr_FR","fr");
		localeTolangCodeMap.put("zh_CN_#Hans","zh");
		localeTolangCodeMap.put("hu_HU","hu");
		localeTolangCodeMap.put("cs_CZ","cs");
		localeTolangCodeMap.put("de_DE","de");
		localeTolangCodeMap.put("in_ID","id");
		localeTolangCodeMap.put("en_US","en");
		localeTolangCodeMap.put("en_IN","en");
		localeTolangCodeMap.put("en_GB","en");
		localeTolangCodeMap.put("vi_VN","vi");
//		for(Map.Entry<String,Map<String,String>> entry:g11ValueToLocaleMap.entrySet()){
//			String value = entry.getKey();
//			Map<String,String>localeCodeToValueMap = entry.getValue();
//			for(Map.Entry<String,String> entry1:localeTolangCodeMap.entrySet()){
//				if(!localeCodeToValueMap.containsKey(entry1.getKey())){
//					String transValue = getSingleG11NValue(entry1.getValue(),value);
//					insertIntoG11NTable(value,entry1.getKey(),transValue);
//
//				}
//			}
//		}



		String fileNamePath = "/Users/tapplent/Documents/G11N/G11N.csv";
		String fileNamePath2="/Users/tapplent/Documents/G11N/LangToCodeMap.csv";
		String fileNamePath3="/Users/tapplent/Documents/G11N/ErrorMsgToCodeMap.csv";


		try{
			//Following code will populate the list with the values to be translated
			CSVReader csvReader = new CSVReader(new FileReader(fileNamePath));
			csvReader.readNext();
			String[] rowValues = null;
			List<String> rowValue = new ArrayList<>();
			while ((rowValues = csvReader.readNext()) != null) {
				if (!rowValues[0].equals(""))
					rowValue.add(rowValues[0]);
			}
			//Following code will populate the langTolocaleAndLangCodeMap which is map of language to its locale and langCode.Eg>English to en|en_US
			CSVReader csvReader1 = new CSVReader(new FileReader(fileNamePath2));
			csvReader1.readNext();
			String[] rowValues1 = null;

			Map<String,String> langTolocaleAndLangCodeMap= new HashMap<>();
			while ((rowValues1 = csvReader1.readNext()) != null) {
				if (!rowValues1[0].equals(""))
					langTolocaleAndLangCodeMap.put(rowValues1[0],rowValues1[1]);

			}
			/*Following code will populate the ErrorMessageToErrorCodeMap which is map of ErrorMessage
			 that client should show when an error code is thrown.
			 Eg>ErrorMsg-Your passwords are not matching. Please retry. to ErrorCode-4015

			*/
			CSVReader csvReader2 = new CSVReader(new FileReader(fileNamePath3));
			csvReader2.readNext();
			String[] rowValues2 = null;
			Map<String,String> ErrorMessageToErrorCodeMap= new HashMap<>();
			while ((rowValues2 = csvReader2.readNext()) != null) {
				if (!rowValues2[0].equals(""))
					ErrorMessageToErrorCodeMap.put(rowValues2[0],rowValues2[1]);

			}
			String b[] = {"en_US","zh_CN_#Hans","cs_CZ","hu_HU","in_ID","fr_FR","de_DE","vi_VN"};
			String filePassss = "/Users/tapplent/Documents/G11N/Translated.csv";
			CSVWriter csvWriter = new CSVWriter(new FileWriter(filePassss));
			csvWriter.writeNext(b);
			for(String oneValue:rowValue){
				if(g11ValueToLocaleMap.containsKey(oneValue)){
					Map<String,String> localeToValue =g11ValueToLocaleMap.get(oneValue);
					String a[] = {localeToValue.get("en_US"),localeToValue.get("zh_CN_#Hans"),localeToValue.get("cs_CZ"),localeToValue.get("hu_HU"),localeToValue.get("in_ID"),localeToValue.get("fr_FR"),localeToValue.get("de_DE"),localeToValue.get("vi_VN")};
					csvWriter.writeNext(a);

				}
			}
			csvWriter.close();
			getMultipleG11NValue(rowValue,langTolocaleAndLangCodeMap,ErrorMessageToErrorCodeMap);

		}catch (Exception e){

		}
	}

	public String getMultipleG11NValue(List<String> hardCodedvalueList,Map<String,String> langTolocaleAndLangCodeMap,Map<String,String> ErrorMessageToErrorCodeMap){

		String key="trnsl.1.1.20170802T094110Z.b56fd051d7e91412.be26c769208bc3a62f8256a6a7a1435df8188be5";
		String transValue =null;
		JSONObject aggregateJSON = new JSONObject();
		JSONObject aggregateJSONForWeb = new JSONObject();
		String filePathForAggJSON = "/Users/tapplent/Documents/i18n/aggregateJSON.json";
		String filePathForAggJSONWeb = "/Users/tapplent/Documents/i18n/aggregateJSONForWeb.json";
		/*
		We have to loop through langTolocaleAndLangCodeMap and loop through all the new values to be translated.
		Every new value should be translated into 70 languages and put into single json file and AggregateJson file
		If the value is already there in the database then its already translated.
		*/
		try{
			for(Map.Entry<String,String> entry:langTolocaleAndLangCodeMap.entrySet()) {

				String language = entry.getKey();
				String langcodeAndLocale[] = entry.getValue().split("\\|");
				String langCode = langcodeAndLocale[0];
				String localeCode = langcodeAndLocale[1];
				StringBuilder url = new StringBuilder("https://translate.yandex.net/api/v1.5/tr.json/translate?lang=").append("en-").append(langCode).append("&key=").append(key);
				List<String> localNewValues = new ArrayList<>();
				//This list-localNewValues is to extract only new values that has been added.For older values we will fetch from the database
				for (String singleValue : hardCodedvalueList) {
					if(!g11ValueToLocaleMap.containsKey(singleValue)) {
						//If value is not there in db then only add to list.
						localNewValues.add(singleValue);
						url.append("&text=").append(URLEncoder.encode(singleValue));
					}
				}
				Iterator<Object> it = null;
				//If the localNewValues is empty then there are no new values to be translated.
				if(!localNewValues.isEmpty()){
					HttpPost post = new HttpPost(url.toString());
					HttpClient client = HttpClientBuilder.create().build();
					HttpResponse response;
					response = client.execute(post);
					JSONObject respJson = new JSONObject(EntityUtils.toString(response.getEntity()));
					JSONArray respJsonJSONArray = respJson.getJSONArray("text");
					it = respJsonJSONArray.iterator();
				}

				/*Two json objects-singleJSONFile object is to create individual file with the locale-Eg>ko_KP.json
				  aggJsonFileHelper object is to create the json file for web-aggregateJSONForWeb.json
				*/

				JSONObject singleJSONFile = new JSONObject();
				JSONObject aggJsonFileHelper = new JSONObject();
				aggJsonFileHelper.put("language",language);
				singleJSONFile.put("language",language);
				if(!g11ValueToLocaleMap.containsKey(language)){
					String convertedValue= getSingleG11NValue(langCode,language);
					aggJsonFileHelper.put("label",convertedValue);
					singleJSONFile.put("label",convertedValue);
					insertIntoG11NTable(language,localeCode,convertedValue);
				}else{
					aggJsonFileHelper.put("label",g11ValueToLocaleMap.get(language).get(localeCode));
					singleJSONFile.put("label",g11ValueToLocaleMap.get(language).get(localeCode));
				}


				aggregateJSONForWeb.put(localeCode,aggJsonFileHelper);
					/*The arrayList-newlyTranslatedValueList will hold the values of the translated text.
					This newlyTranslatedValueList is compared with the localNewValues list to map the value to translated value..
					*/
				List<String> newlyTranslatedValueList = new ArrayList<String>();
				if(it != null){
					while (it.hasNext()) {
						Object arrayValue = it.next();
						transValue = arrayValue.toString();
						newlyTranslatedValueList.add(transValue);
					}
				}
				//Loop through
				for(int i=0;i<hardCodedvalueList.size();i++){
					//ErrorMessageToErrorCodeMap is map which has key as ErrorMessage to be shown and has value as error code
					//If the value is error message then we have to fetch the corresponding code and put into the json file
					if(ErrorMessageToErrorCodeMap.containsKey(hardCodedvalueList.get(i))){
						if(g11ValueToLocaleMap.containsKey(hardCodedvalueList.get(i))){
							String translatedValueFromDb =  g11ValueToLocaleMap.get(hardCodedvalueList.get(i)).get(localeCode);
							singleJSONFile.put(ErrorMessageToErrorCodeMap.get(hardCodedvalueList.get(i)),translatedValueFromDb);
						}else{
							if(!localNewValues.isEmpty() && !newlyTranslatedValueList.isEmpty() ){
								for(int j =0;j<localNewValues.size();j++){
									if(hardCodedvalueList.get(i).equals(localNewValues.get(j))){
										singleJSONFile.put(ErrorMessageToErrorCodeMap.get(localNewValues.get(j)),newlyTranslatedValueList.get(j));
										insertIntoG11NTable(localNewValues.get(j),localeCode,newlyTranslatedValueList.get(j));
									}
								}
							}
						}
					}else{
						if(g11ValueToLocaleMap.containsKey(hardCodedvalueList.get(i))){
							String translatedValueFromDb =  g11ValueToLocaleMap.get(hardCodedvalueList.get(i)).get(localeCode);
							singleJSONFile.put(hardCodedvalueList.get(i),translatedValueFromDb);

						}else{
							if(!localNewValues.isEmpty() && !newlyTranslatedValueList.isEmpty()){
								for(int j =0;j<localNewValues.size();j++){
									if(hardCodedvalueList.get(i).equals(localNewValues.get(j))){
										singleJSONFile.put(localNewValues.get(j),newlyTranslatedValueList.get(j));
										insertIntoG11NTable(localNewValues.get(j),localeCode,newlyTranslatedValueList.get(j));
									}
								}
							}
						}
					}

				}

				aggregateJSON.put(localeCode,singleJSONFile);
				String newString = null;
				if (localeCode.contains("#")){
					newString = localeCode.replaceAll("#","");
					String filePath = "/Users/tapplent/Documents/i18n/"+newString+".json";
					writeToJSONFile(filePath,singleJSONFile);
				}else {
					String filePath = "/Users/tapplent/Documents/i18n/"+localeCode+".json";
					writeToJSONFile(filePath,singleJSONFile);
				}

			}
			writeToJSONFile(filePathForAggJSON,aggregateJSON);
			writeToJSONFile(filePathForAggJSONWeb,aggregateJSONForWeb);

		}catch (Exception e) {

		}
		return transValue;
	}

	public  String getSingleG11NValue(String langCode,String value){
		//String trial_Key= "trnsl.1.1.20170519T083354Z.9dbcfd33d130a6dd.26225d22a113e601a18e897f7251f9268e1aa5be";
		String production_Key="trnsl.1.1.20170822T110047Z.6a020d36623a2196.1dae1b64e94cb5ec52a8fddd722002f44a015445";
		String transValue =null;
		try {

			StringBuilder url = new StringBuilder("https://translate.yandex.net/api/v1.5/tr.json/translate?lang=");
			url.append(langCode);
			url.append("&key=").append(production_Key);
			url.append("&text=").append(URLEncoder.encode(value));

			HttpPost post = new HttpPost(url.toString());
			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response;
			response = client.execute(post);
			JSONObject respJson = new JSONObject(EntityUtils.toString(response.getEntity()));
			transValue = respJson.getJSONArray("text").getString(0);
		}catch (Exception e){

		}
		return transValue;
	}

	public String generateG11NValues(String value){
		JSONObject result = new JSONObject();
		try {
			Map<String, String> localeCodeToValueMap = g11ValueToLocaleMap.get(value);
			for (Map.Entry<String, String> entry : localeCodeToValueMap.entrySet()) {
				if(entry.getKey().equals("en_US")){
					JSONObject en_USG11NObj =new JSONObject();
					en_USG11NObj.put("IM", "I");
					en_USG11NObj.put("TSL", "null");
					String transvalue = entry.getValue().replace("\"","|%");
					StringBuilder resultStr= new StringBuilder();
					resultStr.append(" ");
					resultStr.append(transvalue);
					en_USG11NObj.put("VL", resultStr.toString());
					result.put(entry.getKey(), en_USG11NObj);
				}else{
					JSONObject childG11NObject =new JSONObject();
					childG11NObject.put("IM", "T");
					childG11NObject.put("TSL", "en_US");
					String transvalue = entry.getValue().replace("\"","|%");
					StringBuilder resultStr= new StringBuilder();
					resultStr.append(" ");
					resultStr.append(transvalue);
					childG11NObject.put("VL", resultStr.toString());
					result.put(entry.getKey(), childG11NObject);
				}
			}
		}catch (Exception e){

		}

		return result.toString();
	}


	void g11nForForgotPassword() {
		String fileNamePath = "/Users/tapplent/Documents/G11N/Gmail.csv";


		try {

			CSVReader csvReader = new CSVReader(new FileReader(fileNamePath));
			csvReader.readNext();
			String[] rowValues = null;
			List<String> rowValue = new ArrayList<>();
			while ((rowValues = csvReader.readNext()) != null) {
				if (!rowValues[0].equals(""))
					rowValue.add(rowValues[0]);
			}
			JSONObject english_en_US = new JSONObject();
			english_en_US.put("IM", "I");
			english_en_US.put("TSL", "null");


			JSONObject french_fr_FR = new JSONObject();
			french_fr_FR.put("IM", "T");
			french_fr_FR.put("TSL", "en_US");

			JSONObject Indonesian_in_ID = new JSONObject();
			Indonesian_in_ID.put("IM", "T");
			Indonesian_in_ID.put("TSL", "en_US");

			JSONObject Hungerian_hu_HU = new JSONObject();
			Hungerian_hu_HU.put("IM", "T");
			Hungerian_hu_HU.put("TSL", "en_US");

			JSONObject Chinese_zh_CN = new JSONObject();
			Chinese_zh_CN.put("IM", "T");
			Chinese_zh_CN.put("TSL", "en_US");
			JSONObject German_de_DE = new JSONObject();
			German_de_DE.put("IM", "T");
			German_de_DE.put("TSL", "en_US");
			JSONObject Czech_cs_CZ = new JSONObject();
			Czech_cs_CZ.put("IM", "T");
			Czech_cs_CZ.put("TSL", "en_US");
			JSONObject result=new JSONObject();


			//	"fr","zh","hu","cs","de","id"
			for(String langCode:localeCodes){
				StringBuilder sb = new StringBuilder();
				for (String value:rowValue) {
					String transValue =null;
					if (!langCode.equals("en")){
						transValue = getSingleG11NValue(langCode, value);
					}else{
						transValue=value;
					}

					if (value.equals("Hello")) {
						sb.append(transValue).append(" ").append("!{FullName},").append("\n\n");
					}else if(value.equals("Your verification code is:")){
						sb.append(transValue).append("\n\n").append("!{Code}").append("\n\n");
					}else if(value.equals("Alternatively, you can click on this link directly and reset your password:")){
						sb.append(transValue).append("\n\n").append("!{Link}").append("\n\n");
					}else{
						if (!value.equals("Sincerely,")) {
							sb.append(transValue).append("\n\n");
						}else {
							sb.append(transValue).append("\n");
						}
					}
				}
				if(langCode.equals("fr")){
					french_fr_FR.put("VL", sb.toString());
					result.put("fr_FR", french_fr_FR);
				}else if(langCode.equals("zh")){
					Chinese_zh_CN.put("VL", sb.toString());
					result.put("zh_CN_#Hans", Chinese_zh_CN);
				}else if(langCode.equals("hu")){
					Hungerian_hu_HU.put("VL", sb.toString());
					result.put("hu_HU", Hungerian_hu_HU);
				}else if(langCode.equals("cs")){
					Czech_cs_CZ.put("VL", sb.toString());
					result.put("cs_CZ", Czech_cs_CZ);
				}else if(langCode.equals("de")){
					German_de_DE.put("VL", sb.toString());
					result.put("de_DE", German_de_DE);
				}else if(langCode.equals("id")){
					Indonesian_in_ID.put("VL", sb.toString());
					result.put("in_ID", Indonesian_in_ID);
				}else if(langCode.equals("en")){
					english_en_US.put("VL",sb.toString());
					result.put("en_US",english_en_US);
				}
				System.out.println(sb.toString());

			}
			String filePath = "/Users/tapplent/Documents/G11N/Translated.json";

			System.out.println(result);
			writeToJSONFile(filePath,result);



		}catch (Exception e){

		}
	}
	void g11nForForgotPassword1() {
		String fileNamePath = "/Users/tapplent/Documents/G11N/Gmail.csv";


		try {

			CSVReader csvReader = new CSVReader(new FileReader(fileNamePath));
			csvReader.readNext();
			String[] rowValues = null;
			List<String> rowValue = new ArrayList<>();
			while ((rowValues = csvReader.readNext()) != null) {
				if (!rowValues[0].equals(""))
					rowValue.add(rowValues[0]);
			}
			JSONArray properties = new JSONArray();
			JSONObject english_en_US = new JSONObject();
			english_en_US.put("IM", "I");
			english_en_US.put("TSL", "null");

			JSONObject english_en_GB = new JSONObject();
			english_en_GB.put("IM", "T");
			english_en_GB.put("TSL", "en_US");

			JSONObject english_en_IN = new JSONObject();
			english_en_IN.put("IM", "T");
			english_en_IN.put("TSL", "en_US");

			JSONObject french_fr_FR = new JSONObject();
			french_fr_FR.put("IM", "T");
			french_fr_FR.put("TSL", "en_US");

			JSONObject Indonesian_in_ID = new JSONObject();
			Indonesian_in_ID.put("IM", "T");
			Indonesian_in_ID.put("TSL", "en_US");

			JSONObject Hungerian_hu_HU = new JSONObject();
			Hungerian_hu_HU.put("IM", "T");
			Hungerian_hu_HU.put("TSL", "en_US");

			JSONObject Chinese_zh_CN = new JSONObject();
			Chinese_zh_CN.put("IM", "T");
			Chinese_zh_CN.put("TSL", "en_US");
			JSONObject German_de_DE = new JSONObject();
			German_de_DE.put("IM", "T");
			German_de_DE.put("TSL", "en_US");
			JSONObject Czech_cs_CZ = new JSONObject();
			Czech_cs_CZ.put("IM", "T");
			Czech_cs_CZ.put("TSL", "en_US");
			JSONObject result=new JSONObject();
			JSONObject vietnamese = new JSONObject();
			vietnamese.put("IM", "T");
			vietnamese.put("TSL", "en_US");


			//	"fr","zh","hu","cs","de","id"
			for(String langCode:langCodes){
				StringBuilder sb = new StringBuilder();
				for (String value:rowValue) {
					value=value.trim();
					String transValue =null;
					if (!langCode.equals("en")){
						transValue = getSingleG11NValue(langCode, value);
					}else{
						transValue=value;
					}


					if (value.equals("Hello")) {
						sb.append(transValue).append(" ").append("!{FullName},").append("<p/>");
					}else if(value.equals("Your verification code is:")){
						sb.append(transValue).append("<p/>").append("!{Code}").append("<p/>");
					}else if(value.contains("link")){
						sb.append(transValue).append("<p/>").append("!{Link}").append("<p/>");
					}else{
						if (!value.equals("Sincerely,") && !value.equals("Username") && !value.equals("Password")) {
							if(rowValue.size() ==1){
								sb.append(transValue);
							}else {
								sb.append(transValue).append("<p/>");
							}

						}else {
							if(value.equals("Username")){
								sb.append(transValue).append(": !{Username}").append("<br/>");
							}else if(value.equals("Password")){
								sb.append(transValue).append(": !{Default Password}").append("<p/>");
							}else{
								sb.append(transValue).append("<br/>");
							}
						}
					}
				}
				if(langCode.equals("fr")){
					french_fr_FR.put("VL", sb);
					result.put("fr_FR", french_fr_FR);
					properties.put(french_fr_FR);
				}else if(langCode.equals("zh")){
					Chinese_zh_CN.put("VL", sb);
					result.put("zh_CN_#Hans", Chinese_zh_CN);
				}else if(langCode.equals("hu")){
					Hungerian_hu_HU.put("VL", sb);
					result.put("hu_HU", Hungerian_hu_HU);
				}else if(langCode.equals("cs")){
					Czech_cs_CZ.put("VL", sb);
					result.put("cs_CZ", Czech_cs_CZ);
				}else if(langCode.equals("de")){
					German_de_DE.put("VL", sb);
					result.put("de_DE", German_de_DE);
				}else if(langCode.equals("id")){
					Indonesian_in_ID.put("VL", sb);
					result.put("in_ID", Indonesian_in_ID);
				}else if(langCode.equals("vi")){
					Indonesian_in_ID.put("VL", sb);
					result.put("vi_VN", Indonesian_in_ID);
				} else if(langCode.equals("en")){
					english_en_US.put("VL",sb);
					english_en_GB.put("VL",sb);
					english_en_IN.put("VL",sb);
					result.put("en_US",english_en_US);
					result.put("en_GB",english_en_GB);
					result.put("en_IN",english_en_IN);
				}
				System.out.println(sb.toString());

			}
			String filePath = "/Users/tapplent/Documents/G11N/Translated.json";

			System.out.println(result);
			writeToJSONFile(filePath,result);



		}catch (Exception e){

		}
	}
	String generateNewJSONObject(String value) {
		JSONObject result=new JSONObject();
		try {
			for(Entry<String,String> entry:localeCodeTolangCodeMap.entrySet()){
				if(entry.getKey().equals("en_US")){
					String transValue = value.replace("\"","|%");
					JSONObject english_en_US = new JSONObject();
					english_en_US.put("IM", "I");
					english_en_US.put("TSL", "null");

					JSONObject english_en_GB = new JSONObject();
					english_en_GB.put("IM", "T");
					english_en_GB.put("TSL", "en_US");

					JSONObject english_en_IN = new JSONObject();
					english_en_IN.put("IM", "T");
					english_en_IN.put("TSL", "en_US");
					if(g11ValueToLocaleMap.containsKey(value)){
						Map<String,String> localeCodeToValueMap = g11ValueToLocaleMap.get(value);
						localeCodeToValueMap.put("en_US", transValue);
						localeCodeToValueMap.put("en_GB", transValue);
						localeCodeToValueMap.put("en_IN", transValue);
						insertIntoG11NTable(value,"en_US",transValue);
						insertIntoG11NTable(value,"en_GB",transValue);
						insertIntoG11NTable(value,"en_IN",transValue);
					}else{
						Map<String,String> localecodeToValueMap = new HashMap<>();
						localecodeToValueMap.put("en_US", transValue);
						localecodeToValueMap.put("en_GB", transValue);
						localecodeToValueMap.put("en_IN", transValue);
						g11ValueToLocaleMap.put(value,localecodeToValueMap);
						insertIntoG11NTable(value,"en_US",transValue);
						insertIntoG11NTable(value,"en_GB",transValue);
						insertIntoG11NTable(value,"en_IN",transValue);
					}
					english_en_US.put("VL",transValue);
					english_en_GB.put("VL",transValue);
					english_en_IN.put("VL",transValue);
					result.put("en_US",english_en_US);
					result.put("en_GB",english_en_GB);
					result.put("en_IN",english_en_IN);

				}else if(!entry.getKey().equals("en_IN") && !entry.getKey().equals("en_GB")){
					String transValue =null;
					String partialTransValue = null;
					partialTransValue = getSingleG11NValue(entry.getValue(), value);
					JSONObject g11Nobj = new JSONObject();
					transValue = partialTransValue.replace("\"","|%");
					if(g11ValueToLocaleMap.containsKey(value)){
						Map<String,String> localeCodeToValueMap = g11ValueToLocaleMap.get(value);
						localeCodeToValueMap.put(entry.getKey(), transValue);
						insertIntoG11NTable(value,entry.getKey(),transValue);
					}else{
						Map<String,String> localeCodeToValueMap = new HashMap<>();
						localeCodeToValueMap.put(entry.getKey(), transValue);
						g11ValueToLocaleMap.put(value,localeCodeToValueMap);
						insertIntoG11NTable(value,entry.getKey(),transValue);
					}
					g11Nobj.put("IM","T");
					g11Nobj.put("TSL","en_US");
					g11Nobj.put("VL", transValue);
					result.put(entry.getKey(), g11Nobj);

				}
			}

		}catch (Exception e){

		}
		return result.toString();
	}

	public String getG11NJSONObject(String stringToParse){
		String subArray[] = stringToParse.split(",");
		String extractedString = subArray[0].split(":")[0];
		String sourceLocale =null;
		Pattern p = Pattern.compile("\"([^\"]*)\"");
		Matcher m = p.matcher(extractedString);
		while (m.find()) {
			sourceLocale = m.group(1);
		}

		String sourceValue = null;
		JSONObject inputJson = new JSONObject(stringToParse);
		JSONObject finalJson = new JSONObject();
		String englishValue=null;
		try{
			//Fetch the sourceLocale and SourceValue Of the Parent G11NObject
			JSONObject obj = inputJson.getJSONObject(sourceLocale);
			sourceValue = obj.getString("VL");
			Iterator<String> it = inputJson.keys();
			Iterator<String> iterator = inputJson.keys();
			List<String> localeCods = new ArrayList<>();
			//If the G11N Object already contains some of the input values then fetch them and put into G11N json object..

			while (it.hasNext()){
				String key = it.next();
				JSONObject jsonObject = inputJson.getJSONObject(key);
				String value = jsonObject.getString("VL");
				String transValue = value.replace("\"","|%");
				StringBuilder resultStr= new StringBuilder();
				resultStr.append(" ");
				resultStr.append(transValue);
				//Fetch the englishValue if it is already there inputJSON
				if(key.startsWith("en_")){
					englishValue = transValue;
				}
				JSONObject newJsonObject = new JSONObject();
				newJsonObject.put("VL",resultStr.toString());
				newJsonObject.put("IM","I");
				newJsonObject.put("TSL","null");
				finalJson.put(key,newJsonObject);
				localeCods.add(key);
			}

			if(localeCodeTolangCodeMap.entrySet().size() !=localeCods.size()) {

				if (g11ValueToLocaleMap.containsKey(sourceValue)) {
					Map<String, String> localeCodeToValueMap = g11ValueToLocaleMap.get(sourceValue);
					for (Entry<String, String> entry : localeCodeToValueMap.entrySet()) {
						if (!localeCods.contains(entry.getKey())) {
							JSONObject newJsonObject = new JSONObject();
							String transValue = entry.getValue().replace("\"","|%");
							StringBuilder resultStr= new StringBuilder();
							resultStr.append(" ");
							resultStr.append(transValue);
							newJsonObject.put("VL", resultStr.toString());
							newJsonObject.put("IM", "T");
							newJsonObject.put("TSL", sourceLocale);
							finalJson.put(entry.getKey(), newJsonObject);
						}
					}
				} else {
					//If map does not contain then we should construct the new Object and put into the db..
					Map<String,String> localeCodeToValueMap = new HashMap<>();
					while (iterator.hasNext()){
						String localeCode = iterator.next();
						JSONObject jsonObject = inputJson.getJSONObject(localeCode);
						String value = jsonObject.getString("VL");
						String transValue = value.replace("\"", "|%");
						insertIntoG11NTable(sourceValue,localeCode,transValue);
						localeCodeToValueMap.put(localeCode,transValue);
					}
					for (Entry<String, String> entry : localeCodeTolangCodeMap.entrySet()) {
						if(!localeCods.contains(entry.getKey())){
							if(entry.getKey().startsWith("en_")){

								if(englishValue==null)
									englishValue =getSingleG11NValue("en",sourceValue);
								if(!localeCodeToValueMap.containsKey(entry.getKey()))
									localeCodeToValueMap.put(entry.getKey(),englishValue);
								JSONObject newJsonObject = new JSONObject();
								StringBuilder resultStr= new StringBuilder();
								resultStr.append(" ");
								resultStr.append(englishValue);
								newJsonObject.put("VL",resultStr.toString());
								newJsonObject.put("IM", "T");
								newJsonObject.put("TSL", sourceLocale);
								finalJson.put(entry.getKey(), newJsonObject);
								insertIntoG11NTable(sourceValue,"en_US",englishValue);
								insertIntoG11NTable(sourceValue,"en_IN",englishValue);
								insertIntoG11NTable(sourceValue,"en_GB",englishValue);
							}else{
								String translatedVal = getSingleG11NValue(entry.getValue(),sourceValue);
								String transValue = translatedVal.replace("\"","|%");
								StringBuilder resultStr= new StringBuilder();
								resultStr.append(" ");
								resultStr.append(transValue);
								JSONObject newJsonObject = new JSONObject();
								newJsonObject.put("VL",resultStr.toString());
								newJsonObject.put("IM", "T");
								newJsonObject.put("TSL", sourceLocale);
								finalJson.put(entry.getKey(), newJsonObject);
								if(!localeCodeToValueMap.containsKey(entry.getKey()))
									localeCodeToValueMap.put(entry.getKey(),transValue);

								insertIntoG11NTable(sourceValue,entry.getKey(),transValue);
							}

						}
					}
					g11ValueToLocaleMap.put(sourceValue,localeCodeToValueMap);
				}


			}

		}catch (Exception e){

		}
		return finalJson.toString();
	}

	static void writeToJSONFile(String filePath,JSONObject object){
		File file = new File(filePath);
		try {
			FileUtils.writeStringToFile(file,object.toString());
		}catch (IOException e){


		}
	}

	@Override
	@Transactional
	public void updateHierarchyPersonTable() {

		// Get all the primary key ids where parent is null
		List<String> rootPersonPrimaryIds = hierarchyService.getHierarchialPersonRoots();
//				Map<String, List<String>> parentPersonToChildrenListMap = etlDAO.getChildrenListForParentPersons(rootPersonPrimaryIds);
		for (String rootPrimaryId : rootPersonPrimaryIds) {
//					List<String> childrenList = parentPersonToChildrenListMap.get(rootPrimaryId);
			String hierarchyId = Common.getUUID();
			try {
				hierarchyService.updateLeftRightAndHierachyValuesForPerson(hierarchyId, rootPrimaryId, 1);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
//			}
//		}
	}


	@Transactional
	public void updateHierarchyTables() {
		// Get all hierarc
		List<String> hierarchicalDOs = etlDAO.getHierarchicalDOs();
		for (String hierarchyDO : hierarchicalDOs) {
			MetadataObjectRepository metadata = TenantAwareCache.getMetaDataRepository();
			MasterEntityMetadataVOX doMeta = metadata.getMetadataByDo(hierarchyDO);
			String tableName = doMeta.getDbTableName();
			String primaryKeyColumnName = doMeta.getFunctionalPrimaryKeyAttribute().getDbColumnName();
			String selfJoinParentColumnName = doMeta.getHierarchyKeyAttribute().getDbColumnName();

			// Get all the primary key ids where parent is null
			List<String> rootPrimaryIds = etlDAO.getHierarchialRoots(tableName, primaryKeyColumnName,
					selfJoinParentColumnName);
			for (String rootPrimaryId : rootPrimaryIds) {
				String hierarchyId = Common.getUUID();
				updateLeftRightAndHierachyValues(doMeta, hierarchyId, rootPrimaryId, 1);
			}
		}
	}

	private int updateLeftRightAndHierachyValues(MasterEntityMetadataVOX doMeta, String hierarchyId, String parentId,
												 int leftValue) {
		// the right value of this node is the left value + 1
		int rightValue = leftValue + 1;
		// get all children of this node
		String tableName = doMeta.getDbTableName();
		String primaryKeyColumnName = doMeta.getFunctionalPrimaryKeyAttribute().getDbColumnName();
		String selfJoinParentColumnName = doMeta.getHierarchyKeyAttribute().getDbColumnName();
		List<String> childrenPrimaryKeys = etlDAO.getChildrenListForParent(tableName, primaryKeyColumnName,
				selfJoinParentColumnName, parentId);
		if (childrenPrimaryKeys != null) {
			for (String childrenPrimaryKey : childrenPrimaryKeys) {
				rightValue = updateLeftRightAndHierachyValues(doMeta, hierarchyId, childrenPrimaryKey, rightValue);
			}
		}
		try {
			etlDAO.updateLeftRightAndHierachyValues(tableName, primaryKeyColumnName, selfJoinParentColumnName, parentId,
                    leftValue, rightValue, hierarchyId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rightValue + 1;
	}

	private List<UpdateColumn> uploadETL() {
		Path folderPath = Paths.get("/Users/tapplent/Documents/DataUploadSheets/UploadETL/");
		return uploadTheFolder(folderPath, 3);
	}

	private List<UpdateColumn> uploadTheFolder(Path folderPath, int csvFormatNumber) {
		DBUploadTracker.folderName = folderPath.toString();
		DBUploadTracker.formatNumber = csvFormatNumber;
		List<UpdateColumn> folderUpdateColumns = new ArrayList<>();
		Map<Integer, String> fileNumberToNameMap = new HashMap<>();
		try {
			if (csvFormatNumber != 3) {
				FileInputStream excelFile = new FileInputStream(new File(folderPath.toString()));
				Workbook workbook = new XSSFWorkbook(excelFile);
				int noOfSheets = workbook.getNumberOfSheets();
				for (int i = 0; i < noOfSheets; i++) {
					Sheet sheet = workbook.getSheetAt(i);
					String[] splitedSheetName = sheet.getSheetName().split("\\.");
					if (!sheet.getSheetName().startsWith(".")) {
						try {
						    if(!(folderPath.toString().contains("MTBT") && sheet.getSheetName().contains("CreateTable"))) {
                                Integer fileNumber = Integer.parseInt(splitedSheetName[0]);
                                fileNumberToNameMap.put(fileNumber, sheet.getSheetName());
                            }
						} catch (NumberFormatException e) {
							LOG.debug("Sheet does not start with a number ->", sheet.getSheetName());
						}
					}
				}
//				Files.walk(Paths.get(folderPath.toString())).forEach(filePath -> {
//					if (Files.isRegularFile(filePath)) {
//						String[] splitedFileName = filePath.getFileName().toString().split("\\.");
//						if (!filePath.getFileName().toString().startsWith(".")) {
//							try{
//								if(!(folderPath.toString().contains("MTBT") && filePath.getFileName().toString().contains("CreateTable"))){
//									Integer fileNumber = Integer.parseInt(splitedFileName[0]);
//									fileNumberToNameMap.put(fileNumber, filePath.toString());
//								}
//							}catch(NumberFormatException e){
//								LOG.debug("File does not start with a number ->", filePath.getFileName());
//							}
//						}
//					}
//				});

//			try {
//				Files.walk(Paths.get(folderPath.toString())).forEach(filePath -> {
//					if (Files.isRegularFile(filePath)) {
//						String[] splitedFileName = filePath.getFileName().toString().split("\\.");
//						if (!filePath.getFileName().toString().startsWith(".")) {
//							try{
//								if(!(folderPath.toString().contains("MTBT") && filePath.getFileName().toString().contains("CreateTable"))){
//									Integer fileNumber = Integer.parseInt(splitedFileName[0]);
//									fileNumberToNameMap.put(fileNumber, filePath.toString());
//								}
//							}catch(NumberFormatException e){
//								LOG.debug("File does not start with a number ->", filePath.getFileName());
//							}
//						}
//					}
//				});
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
				LOG.debug("Uploading files from folder {" + folderPath + "}");
				for (int i = 0; i < 5000; i++) {
					if (fileNumberToNameMap.containsKey(i)) {
						String sheetName = fileNumberToNameMap.get(i);
						DBUploadTracker.sheetName = sheetName;
						List<Object> rawData = null;
						LOG.debug("Uploading sheet {" + sheetName + "}");
						if (csvFormatNumber == 1) {
							rawData = extractDataFromCsvFormat1(workbook.getSheet(sheetName));
						} else if (csvFormatNumber == 2) {
							rawData = extractDataFromCsvFormat2(workbook.getSheet(sheetName));
						}
						List<UpdateColumn> fileUpdateColumns = insertIntoTable(rawData, true);
						folderUpdateColumns.addAll(fileUpdateColumns);
					}
				}
			} else{
				LOG.debug("Uploading files from folder {" + folderPath + "}");
				File[] files = new File(folderPath.toString()).listFiles();
				Arrays.sort(files, new Comparator<File>() {
					@Override
					public int compare(File f1, File f2) {
						return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
					}
				});
				for (File file : files) {
					if (!file.getName().startsWith(".")) {
						List<Object> rawData = null;
						rawData = extractDataFromCsvFormat3(file.getAbsolutePath());
						List<UpdateColumn> fileUpdateColumns = insertIntoTable(rawData, false);
						folderUpdateColumns.addAll(fileUpdateColumns);
					}
				}
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return folderUpdateColumns;
	}

	private List<UpdateColumn> insertIntoTable(List<Object> rawData, boolean addStandardFields) {
		List<UpdateColumn> updateColumnsList = new ArrayList<>();
		ETLMetaHelperVO doMeta = (ETLMetaHelperVO) rawData.get(0);
		String tableName = doMeta.getTableName();
		if (tableName.trim().equals("CREATE TABLE")) {
			// Here delete all the entries from the map table SheetIDToDBIDMap and also the static map.
			sheetFKNameToSheetIDToDBIDMap.clear();
			try {
				etlDAO.deleteAllFromSheetIDToDBIDTable();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			@SuppressWarnings("unchecked")
			Map<String, List<ColumnName>> tableNameToColumnsMap = (Map<String, List<ColumnName>>) rawData.get(1);
			createTableCreationFile(tableNameToColumnsMap);
		} else if (tableName.trim().equals("STANDARD FIELDS")) {
			setStandardFieldsValue(rawData);
		} else {
			String foreignKeyName = (String) rawData.get(1);
			if(!foreignKeyNameToCurrentOccurenceCountMap.contains(foreignKeyName) && !foreignKeyName.equals("PERSON")){
				// Go and delete all enteries from this table.

				try {
					etlDAO.deleteAllEntriesFromTable(tableName);
				} catch (SQLException e) {
					e.printStackTrace();
				}

//				etlDAO.deleteAllEntriesFromTable(tableName);
				// Here also delete all the enteries from SheetIDToDBIDMap
				etlDAO.deleteFKEnteriesFromSheetIDToDBIDTable(foreignKeyName);

				columnTypeMap.remove(foreignKeyName);
				foreignKeyNameToCurrentOccurenceCountMap.add(foreignKeyName);
			}
			String[] columnType = (String[]) rawData.get(2);
			String[] columnName = (String[]) rawData.get(3);
			String[] dummyColumnName = (String[]) rawData.get(4);
			String[] dbDataType = (String[]) rawData.get(5);
			String[] jsonName = (String[]) rawData.get(6);
			@SuppressWarnings("unchecked")
			List<String[]> rows = (List<String[]>) rawData.get(7);
			List<UpdateColumn> updateList = insertIntoTable(doMeta, foreignKeyName, columnType, columnName,
					dummyColumnName, dbDataType, jsonName, rows, addStandardFields);
			updateColumnsList.addAll(updateList);
		}
		return updateColumnsList;
	}

	@SuppressWarnings("unchecked")
	private List<UpdateColumn> insertIntoTable(ETLMetaHelperVO doMeta, String foreignKeyName, String[] columnType,
											   String[] columnNames, String[] dummyColumnNames, String[] dbDataTypes, String[] jsonNames,
											   List<String[]> rows, boolean addStandardFields) {
		List<UpdateColumn> toBeUpdatedColumns = new ArrayList<>();
		String tableName = doMeta.getTableName();
		/*
		 * for(int i = 0; i<columnNames.length;i++){
		 * if(!columnNames[i].equals("")){
		 *
		 * String words[] = columnNames[i].split("_"); StringBuilder
		 * voColumnName = new StringBuilder("private "); StringBuilder
		 * dummyVoColumnName = new StringBuilder(); String dataType = "";
		 * if(dbDataTypes[i].equals("T_BLOB")|| dbDataTypes[i].equals("T_CODE")
		 * ||dbDataTypes[i].equals("T_ICON")||dbDataTypes[i].equals("TEXT")||
		 * dbDataTypes[i].equals("T_BIG_TXT") ){ voColumnName.append("String ");
		 * dataType = "String"; }else if (dbDataTypes[i].equals("BOOLEAN")){
		 * voColumnName.append("boolean "); dataType = "boolean"; }else if
		 * (dbDataTypes[i].equals("INTEGER")){ voColumnName.append("int ");
		 * dataType = "int"; } for(String word : words){ if(!word.equals("")){
		 * if(word.equals(words[0])){ voColumnName.append(word.toLowerCase());
		 * dummyVoColumnName.append(word.toLowerCase()); }else{ char a =
		 * word.charAt(0);
		 * voColumnName.append(a+word.substring(1).toLowerCase());
		 * dummyVoColumnName.append(a+word.substring(1).toLowerCase()); } } }
		 * String setter =
		 * generateSetter(dummyVoColumnName.toString(),dataType); String getter
		 * = generateGetter(dummyVoColumnName.toString(),dataType);
		 * System.out.println(voColumnName.toString()+";");
		 * System.out.println(setter); System.out.println(getter); } }
		 */
		// Map<String, GenericLayoutInsertClass> pkNameToGenericLayoutObjects =
		// new HashMap<>();
		Map<String, String> columnNameToColumnTypeMap = new HashMap<>();
		for (int rowNumber = 0; rowNumber < rows.size(); rowNumber++) {
			String[] rowValues = rows.get(rowNumber);
			String primaryKeyName = null;
			String primaryKeyId = null;
			String primaryKeyColumnName = null;
			String primaryKeyDbDataType = null;
			List<UpdateColumn> toBeUpdatedColumnsForARecord = new ArrayList<>();
			Map<String, Object> columnNameValueMap = new HashMap<>();

			Map<String, String> pkMap = null;
			for (int i = 0; i < columnType.length; i++) {
				if (columnType[i] != null && !columnType[i].equals("") && columnNames[i] != null
						&& !columnNames[i].equals("")) {

					Object value = null;

					try {
						if (isUpdateColumnType(columnType[i], dbDataTypes[i], rowValues[i])) {
							if(StringUtil.isDefined(rowValues[i])) {
								if (tableName.equals("T_PRN_EU_PERSON_TXN") && columnType[i].equals("UPDATE_PERSON")) {
									isPersonUpdateRequired=true;
									value = Common.getQS(rowValues[i]);
								} else {
									UpdateColumn updateColumn = new UpdateColumn(tableName, columnNames[i], jsonNames[i],
											rowValues[i], columnType[i], dbDataTypes[i]);

									toBeUpdatedColumnsForARecord.add(updateColumn);
								}
							}

						} else {
							if(columnType[i].equals("PERMISSION")){
									if(rowValues[i-1].equals("APPLICABLE_GROUPS"))
										value = processAccordingToColumnType("PIPESPLIT|GROUP",rowValues[i]);
									if(rowValues[i-1].equals("SUBJECT_USER_RELATIONSHIP_TYPE"))
										value = processAccordingToColumnType("PIPESPLIT|JOB_RELATION_TYPE",rowValues[i]);

							}else {
								value = processAccordingToColumnType(columnType[i], rowValues[i]);
							}

						}
					} catch (ArrayIndexOutOfBoundsException e) {
						System.out.println("ArrayIndex Out Of bounds Exception");
					}
					String columnName = columnNames[i];
					if (columnType[i].equals("PRIMARY_KEY")) {
						primaryKeyName = (String) value;
						primaryKeyColumnName = columnName;

						primaryKeyDbDataType = dbDataTypes[i];
						if (primaryKeyDbDataType.equals("T_ID")) {
							if (primaryKeyName.equals("PRN_SYS")) {
								if (columnTypeMap.containsKey("PERSON")) {
									primaryKeyId = columnTypeMap.get("PERSON").get(primaryKeyName);
								} else {

									primaryKeyId = Common.getUUID();
								}
							} else {
								primaryKeyId = Common.getUUID();
							}

						} else {
							primaryKeyId = (String) value;
						}
						value = primaryKeyId;
						if (tableName.equals("T_PRN_EU_PERSON")) {
							insertIntoPersonToId(primaryKeyName, value);
						}
					}
					if (value != null && !value.toString().isEmpty()) {
						if (columnName.contains("_G11N_") && !columnName.matches("IS_G11N_AUTO_TRANSLATION_REQD|IS_G11N_ORG_LCL_ENTRY_ENFORCED|IS_G11N_GLOBAL_LCL_ENTRY_ENFORCED")) {
							if (!columnName.equals("LEGAL_FULL_NAME_G11N_BIG_TXT")
								&& !columnName.equals("SHORT_NAME_G11N_BIG_TXT")
								&& !columnName.equals("FORMAL_FULL_NAME_G11N_BIG_TXT")
								&& !columnName.equals("FORMAL_SHORT_NAME_G11N_BIG_TXT")
								&& !columnName.equals("RFRD_PRN_FULL_NAME_G11N_BIG_TXT")) {

								//If value starts with "{" then parse the G11nJSONObject and translate it to the locales which are not
								//there G11nJSONObject and put into the db
								if (value.toString().startsWith("{")) {
									if(!tableName.equals("T_SYS_ADM_PASSWORD_POLICY")){
										JSONObject json = new JSONObject(value.toString());
										String obj = getG11NJSONObject(value.toString());
										value = obj;
									}else {
										value = value.toString().replace("'", "''");
									}
								} else if (g11ValueToLocaleMap.containsKey(value.toString())) {
									try {
										value = generateG11NValues(value.toString());
										LOG.debug(value);
									} catch (Exception e) {
										e.printStackTrace();
									}
								} else {
									//If the value is not present in the g11ValueToLocaleMap then we have to translate into
									//all the licenced locales put it as JsonObject
//									if (!value.toString().startsWith("{") && !value.toString().contains("|")) {
//										value = generateNewJSONObject(value.toString());
//									}
									Map<String, Object> g11nMap = new HashMap<>();
									g11nMap.put("en_US", value);
									value = Util.getStringFromG11nMapIntrmdte(g11nMap);
								}
							} else {
							Map<String, Object> g11nMap = new HashMap<>();
							g11nMap.put("en_US", value);
							value = Util.getStringFromG11nMapIntrmdte(g11nMap);
							}
						}
					}
					if (columnType[i].matches("(?i:ICON)")) {
						iconList.add(new EtlIcon(rowValues[i], tableName, dummyColumnNames[i]));
					}
					// Now we have columnName and value.
					if (columnName.endsWith("BLOB")) {
						// if it is a blob
						if (columnNameValueMap.containsKey(columnName)) {
							// if the blob already exists
							Map<String, GenericColumnValue> blobColumnValueMap = (Map<String, GenericColumnValue>) columnNameValueMap
									.get(columnName);
							if (value != null && !value.toString().equals("null")) {
								GenericColumnValue columnValue = new GenericColumnValue(value.toString(),
										dbDataTypes[i], dummyColumnNames[i]);
								blobColumnValueMap.put(jsonNames[i], columnValue);
								if (dummyColumnNames[i].endsWith("G11N_BLOB")) {
									GenericColumnValue blobColumnValue = new GenericColumnValue(value.toString(),
											dbDataTypes[i], null);
									columnValue.getBlobColumnToValueMap().put("en_US", blobColumnValue);
								}
								columnNameToColumnTypeMap.put(jsonNames[i], columnType[i]);
							}
						} else {
							// if the blob dorsn't exists
							Map<String, GenericColumnValue> blobColumnValueMap = new HashMap<>();
							if (value != null && !value.toString().equals("null")) {
								GenericColumnValue columnValueMap = new GenericColumnValue(value.toString(),
										dbDataTypes[i], dummyColumnNames[i]);
								blobColumnValueMap.put(jsonNames[i], columnValueMap);
								columnNameToColumnTypeMap.put(jsonNames[i], columnType[i]);
							}
							columnNameValueMap.put(columnName, blobColumnValueMap);
						}
					} else {
						// if it is not a blob put it directly.
						if (value != null && !value.toString().equals("null")) {
							if(tableName.equals("T_AHP_ADM_INTRO_CONTENT_RULE") && columnName.equals("INTRO_IMAGEID")){
									String imageId =
											uploadAttachmentToS3(value.toString(), false);
									value = imageId;

							}else if (dbDataTypes[i].matches("T_ATTACH|T_IMAGEID")) {
								String imageId = uploadAttachmentToS3(value.toString(), true);
								String thumbnailId = null;
//								if(Arrays.asList(columnNames).contains("ATTACH_THUMBNAIL_IMAGEID")){
								if(doMeta.getIsThumbnailRequired()){
									if(dbDataTypes[i].equals("T_ATTACH")){
										thumbnailId = createThumbnail(value.toString());
									}
									else if(dbDataTypes[i].equals("T_IMAGEID"))
										thumbnailId = imageId;
									if(thumbnailId != null){
										GenericColumnValue columnValueMap = new GenericColumnValue(thumbnailId,
												"T_IMAGEID", "ATTACH_THUMBNAIL_IMAGEID");
										columnNameValueMap.put("ATTACH_THUMBNAIL_IMAGEID", columnValueMap);
										columnNameToColumnTypeMap.put("ATTACH_THUMBNAIL_IMAGEID", "NORMAL");
									}
									doMeta.setIsThumbnailRequired(false);
								}
								value = imageId;
							}
							if(value!=null) {
								GenericColumnValue columnValueMap = new GenericColumnValue(value.toString(),
										dbDataTypes[i], dummyColumnNames[i]);
								columnNameValueMap.put(columnName, columnValueMap);
								columnNameToColumnTypeMap.put(columnName, columnType[i]);
							}
						}
					}

				} else {
					if (!StringUtil.isDefined(columnNames[i])) {
						// try {
						// DBUploadErrorLog.addError("A column name is not
						// present for table {"+tableName+"} at column index
						// {"+i+"}");
						// throw new Exception("A column name is not present for
						// table {"+tableName+"} at column index {"+i+"}");
						// } catch (Exception e) {
						// e.printStackTrace();
						// }
					} else {
						try {
							DBUploadErrorLog.addError("A column type is not present for table {" + tableName
									+ "} at column index {" + i + "}");
							throw new Exception("A column type is not present for table {" + tableName
									+ "} at column index {" + i + "}");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			/*
			 * This primary key map helps to achieve foreignKey to primary key
			 * name to primary key name or primary key name to primary key id
			 * map. (DS: Map<String, Map<String, String>>)
			 */
			pkMap = new HashMap<>();
			if (primaryKeyName != null) {
				// pkNameToPkIdMap.put(primaryKeyName, primaryKeyId);
				pkMap.put(primaryKeyName, primaryKeyId);
				for (UpdateColumn uColumn : toBeUpdatedColumnsForARecord) {
					uColumn.setPrimarykeyColumnName(primaryKeyColumnName);
					uColumn.setPrimaryKeyId(primaryKeyId);
					uColumn.setPrimaryKeyDbDataType(primaryKeyDbDataType);
				}
			}
			if (!(foreignKeyName == null) && !foreignKeyName.equals("") && !foreignKeyName.equals("")) {
				addMapToColumnType(foreignKeyName, pkMap, primaryKeyName, primaryKeyId);
			} else {
				if (DBUploadTracker.formatNumber != 3) {
					DBUploadErrorLog.addError("Foreign Key for table/doCode {" + tableName + "} does not exists.");
					try {
						throw new Exception("Foreign Key for table/doCode {" + tableName + "} does not exists.");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			toBeUpdatedColumns.addAll(toBeUpdatedColumnsForARecord);
			if (addStandardFields) {
				for(Map.Entry<String, Object> entry:stdColumnNameValueMap.entrySet()){
					if(!columnNameValueMap.containsKey(entry.getKey())){
						columnNameValueMap.put(entry.getKey(), entry.getValue());
					}
				}

			}
			// Inserting default values for access control
			LOG.debug("Inserting row number {" + rowNumber + "}");
			DBUploadTracker.rowNumber = rowNumber;
			etlDAO.insert(tableName, columnNameValueMap);
			if (doMeta.getAccessControlApplicable() && StringUtil.isDefined(doMeta.getPrimaryKeyColumnName())) {
				GenericColumnValue value = (GenericColumnValue) columnNameValueMap
						.get(doMeta.getPrimaryKeyColumnName());
				// We have received the primary key to be inserted and we have
				// doMeta with us.
				// with this we should be able to make entries into the system
				pkIdsToMetaMap.put(value.getActualValue(), doMeta);
			}
		}
		return toBeUpdatedColumns;
	}

	private String createThumbnail(String value) {
		String extn = null;
		if(value.split("\\.").length>1) {
			String[] strings = value.split("\\.");
			String fileInitial = strings[0];
			extn = strings[1];
			if(extn.matches("mp4|3gp|flv"))
				try {
					FFmpeg ffmpeg = new FFmpeg("/usr/local/bin/ffmpeg");
					String filePath = "/Users/tapplent/Documents/DataUploadSheets/UploadS3Image/" + value;
					FFmpegBuilder builder =  new FFmpegBuilder().setInput(filePath)
							.setStartOffset(0, TimeUnit.SECONDS) .addOutput("/Users/tapplent/Documents/DataUploadSheets/UploadS3Image/"+fileInitial+".jpg").setFrames(1)
							.done();

					FFmpegExecutor executor;
					executor = new FFmpegExecutor(ffmpeg);
					executor.createJob(builder).run();
					String thumbnailId = uploadAttachmentToS3(fileInitial+".jpg", false);
					return thumbnailId;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}

	private String uploadAttachmentToS3(String value, boolean isCompressionRequired) {

		if(attachmentToUuidMap.containsKey(value))
			return  attachmentToUuidMap.get(value);
		String keyId = null;
		String existingBucketName = "mytapplent";
		String filePath = "/Users/tapplent/Documents/DataUploadSheets/UploadS3Image/" + value;
		String extn = null;
		extn = value.split("\\.")[1];
		if(value.split("\\.").length>1) {
			if(isCompressionRequired && extn.matches("jpeg|png|gif|jpg"))
				filePath = compressAttachment(filePath, value);
		}else{
			DBUploadErrorLog.addError("file {" + value + "} doesn't have extension");
			try {
				throw new Exception("file {" + value + "} doesn't have extension");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String filePathCompressed = "/Users/tapplent/Documents/DataUploadSheets/UploadS3ImageCompressed/";
		//      String amazonFileUploadLocationOriginal = existingBucketName + "/" + TenantContextHolder.getCurrentTenantID();
		String amazonFileUploadLocation1 = existingBucketName + "/interplex";

		String amazonFileUploadLocation2 = existingBucketName + "/interplex-dev";
		BufferedInputStream stream1 = null;
		BufferedInputStream stream2 = null;
		File file = new File(filePath);
		if(file.exists()) {
			try {
				InputStream is1 = new FileInputStream(filePath);
				InputStream is2 = new FileInputStream(filePath);
				stream1 = new BufferedInputStream(is1);
				stream2 = new BufferedInputStream(is2);
				ObjectMetadata objectMetadata = new ObjectMetadata();
				FileNameMap fileNameMap = URLConnection.getFileNameMap();
				String contentType = null;
				if (extn.equals("svg"))
					contentType = "image/svg+xml";
				else contentType = fileNameMap.getContentTypeFor(value);
				//          objectMetadata.setContentDisposition("attachment; filename=" + value);
				objectMetadata.setContentType(contentType);
				Map<String, String> userMetadata = new HashMap<>();
				userMetadata.put("Filename", value);
				objectMetadata.setUserMetadata(userMetadata);
				keyId = Common.getUUID();
				AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
				PutObjectRequest putObjectRequest1 = new PutObjectRequest(amazonFileUploadLocation1, keyId, stream1,objectMetadata);
				PutObjectRequest putObjectRequest2 = new PutObjectRequest(amazonFileUploadLocation2, keyId, stream2,objectMetadata);
				PutObjectResult result1 = s3Client.putObject(putObjectRequest1);
				PutObjectResult result2 = s3Client.putObject(putObjectRequest2);

				stream1.close();
				stream2.close();
				System.out.println("Etag:" + result1.getETag() + "-->" + result1);
				System.out.println("Etag:" + result2.getETag() + "-->" + result2);

				copyObjectFromOneFolderToAnother(existingBucketName, keyId, s3Client);

				attachmentToUuidMap.put(value, keyId);
				insertIntoAttachTable(value, keyId);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else{
			DBUploadErrorLog.addError("file Path {" + filePath + "} is not present.");
			return null;
//            return Common.getUUID();
//                try {
//                    throw new Exception("file Path {" + filePath + "} is not present.");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
		}
		return keyId;

	}

	private void copyObjectFromOneFolderToAnother(String existingBucketName, String keyId, AmazonS3 s3Client) {
		try {
			CopyObjectRequest copyObjRequest = new CopyObjectRequest(existingBucketName, "interplex/"+keyId, existingBucketName, "interplex-test/"+keyId);
			System.out.println("Copying object.");
			s3Client.copyObject(copyObjRequest);

		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, " +
					"which means your request made it " +
					"to Amazon S3, but was rejected with an error " +
					"response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, " +
					"which means the client encountered " +
					"an internal error while trying to " +
					" communicate with S3, " +
					"such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		}
	}

	private String compressAttachment(String filePath, String value) {
		String extn = value.split("\\.")[1];
		String filePathCompressed = "/Users/tapplent/Documents/DataUploadSheets/UploadS3ImageCompressed/" + value;
		File input = new File(filePath);
		if(input.exists()) {
			try {
				if (extn.equals("png")){
					FileUtils.copyFile(new File(filePath), new File(filePathCompressed));
				}
				else {
					LOG.debug("the input file is :" + input);
					BufferedImage image = ImageIO.read(input);

					File compressedImageFile = new File(filePathCompressed);
					OutputStream os = new FileOutputStream(compressedImageFile);

					//String extn = value.split("\\.")[1];
					Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName(extn);
					ImageWriter writer = (ImageWriter) writers.next();

					ImageOutputStream ios = ImageIO.createImageOutputStream(os);
					writer.setOutput(ios);

					ImageWriteParam param = writer.getDefaultWriteParam();

					param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
					if (!extn.equals("png"))
						param.setCompressionQuality(0.2f);
					writer.write(null, new IIOImage(image, null, null), param);

					os.close();
					ios.close();
					writer.dispose();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
//            DBUploadErrorLog.addError("file Path {" + filePath + "} is not present.");
//            try {
//                throw new Exception("file Path {" + filePath + "} is not present.");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
		}
		return filePathCompressed;
	}
	private void deleteAllFromPersonToIdTable() {
		String SQL = "DELETE FROM T_PERSON_DB.T_PERSON_TO_ID;";
		List<Object> param = new ArrayList<>();
		try {
			etlDAO.executeUpdate(SQL,param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void insertIntoAttachTable(String value, String keyId) {
		String SQL = "INSERT INTO T_ATTACHMENT_DB.TMP_ATTACHMENT_MAP VALUES (?,?);";
		List<Object> param = new ArrayList<>();
		param.add(value);
		param.add(keyId);
		try {
			etlDAO.executeUpdate(SQL,param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	private void insertIntoPersonToId(String value, Object keyId) {
		String SQL = "INSERT INTO T_PERSON_DB.T_PERSON_TO_ID VALUES ("+Common.getQS(value)+","+keyId+");";
		List<Object> param = new ArrayList<>();
		try {
			etlDAO.executeUpdate(SQL,param);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private  void insertIntoG11NTable(String valueCode, String localeCode,String localeValue) {
		String selectSql = "SELECT VALUE_CODE, LOCALE_CODE,LOCALE_VALUE FROM T_G11N_VALUE_DB.G11N_VALUE_MAP WHERE VALUE_CODE= ? AND LOCALE_CODE= ?;";
		List<Object> params = new ArrayList<>();
		params.add(valueCode);
		params.add(localeCode);
		ResultSet rs = etlDAO.executeSQL(selectSql,params);
		try {
			if(!rs.next()){
				String SQL = "INSERT INTO T_G11N_VALUE_DB.G11N_VALUE_MAP VALUES (?,?,?);";
				List<Object> param = new ArrayList<>();
				param.add(valueCode);
				param.add(localeCode);
				param.add(localeValue);
				LOG.debug(SQL);
				etlDAO.executeUpdate(SQL, param);
			}else {
				LOG.debug("The valueCode "+valueCode+" and localeCode "+localeCode+" is already present");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private String generateGetter(String columnName, String dataType) {
		StringBuilder getter = new StringBuilder("public ");
		getter.append(dataType).append(" get").append(columnName).append("()").append(" {").append("\n")
				.append("return ").append(columnName).append(";").append("\n").append("}");

		return getter.toString();
	}

	/*
	 * public void setVersionId(String versionId) { this.versionId = versionId;
	 * } public String getEtlHeaderPkId() { return etlHeaderPkId; }
	 */
	private String generateSetter(String columnName, String dataType,String column) {

		StringBuilder setter = new StringBuilder("do.");
		setter.append("set").append(columnName).append("(").append("rs.");
		if (dataType.contains("BINARY") || dataType.contains("VARCHAR")
				|| dataType.equals("MEDIUMTEXT") || dataType.equals("BLOB")
				|| dataType.contains("DATETIME")) {
			dataType = "String";
		} else if (dataType.equals("BIT(1)")) {
			dataType = "boolean";
		} else if (dataType.equals("INT") || dataType.equals("BIGINT")) {

			dataType = "int";
		}
		setter.append("get").append(dataType).append("(\"").append(column).append("\"));");


		return setter.toString();
	}

	@SuppressWarnings("unchecked")
	private void setStandardFieldsValue(List<Object> standardFieldData) {
		stdColumnNameValueMap.clear();
		String[] columnType = (String[]) standardFieldData.get(2);
		String[] columnNames = (String[]) standardFieldData.get(3);
		String[] dummyColumnNames = (String[]) standardFieldData.get(4);
		String[] dbDataTypes = (String[]) standardFieldData.get(5);
		String[] jsonNames = (String[]) standardFieldData.get(6);
		List<String[]> rows = (List<String[]>) standardFieldData.get(7);
		for (String[] rowValues : rows) {
			String personIdCode = null;
			String personId = null;
			for (int i = 0; i < rowValues.length; i++) {
				Object value = null;
				if (!rowValues[i].equals("null")) {
					if (columnType[i].equals("PERSON")) {
						personIdCode = rowValues[i];
						if (columnTypeMap.containsKey("PERSON")) {
							Map<String, String> personIdMap = columnTypeMap.get("PERSON");
							if(personIdMap.containsKey(personIdCode)){
								personId = columnTypeMap.get("PERSON").get(personIdCode);
								value= personId;
							}else {
								personId = Common.getUUID();
								columnTypeMap.get("PERSON").put(personIdCode, personId);
							}
						} else {
							personId = Common.getUUID();
							Map<String, String> personIdMap = new HashMap<>();
							personIdMap.put(personIdCode, personId);
							addMapToColumnType("PERSON", personIdMap, personIdCode, personId);
						}
					}else{
						value = processAccordingToColumnType(columnType[i], rowValues[i]);
					}
				}
				String columnName = columnNames[i];
				// Now we have columnName and value.
				if (columnName.endsWith("BLOB")) {
					// if it is a blob
					if (stdColumnNameValueMap.containsKey(columnName)) {
						// if the blob already exists
						Map<String, GenericColumnValue> blobColumnValueMap = (Map<String, GenericColumnValue>) stdColumnNameValueMap
								.get(columnName);
						if (value != null && !value.toString().equals("null")) {
							blobColumnValueMap.put(jsonNames[i],
									new GenericColumnValue(value.toString(), dbDataTypes[i], dummyColumnNames[i]));
						}
					} else {
						// if the blob dorsn't exists
						Map<String, GenericColumnValue> blobColumnValueMap = new HashMap<>();
						if (value != null && !value.toString().equals("null")) {
							blobColumnValueMap.put(jsonNames[i],
									new GenericColumnValue(value.toString(), dbDataTypes[i], dummyColumnNames[i]));
						}
						stdColumnNameValueMap.put(columnName, blobColumnValueMap);
					}
				}if(columnName.contains("_G11N_")){
					if(value!=null && !value.toString().isEmpty()){
						if(value.toString().startsWith("{")){
//							value = value.toString().substring(1, value.toString().length()-2);
//							value  = value.toString().replace("\"","|%");
						}
						else if(g11ValueToLocaleMap.containsKey(value)){
							try {
								value = generateG11NValues(value.toString());
								LOG.debug(value);
							}  catch (Exception e) {
								e.printStackTrace();
							}
						}else{

//								value = Util.getG11nString("en_US",value);
							Map<String, Object> g11nMap = new HashMap<>();
							g11nMap.put("en_US", value);
							value = Util.getStringFromG11nMapIntrmdte(g11nMap);
						}
						stdColumnNameValueMap.put(columnName,
								new GenericColumnValue(value.toString(), dbDataTypes[i], dummyColumnNames[i]));
					}
				} else {
					// if it is not a blob put it directly.
					if (value != null) {
						stdColumnNameValueMap.put(columnName,
								new GenericColumnValue(value.toString(), dbDataTypes[i], dummyColumnNames[i]));
					}
				}
			}
			// layoutDAO.insertBaseObjectTemplateAndPerson(personId,personName,baseObjectName,baseObjectId,baseTemplateName,baseTemplateId);
		}
	}

	/*
	 * The return object will contain metaData received from files as first
	 * second and third element of the list and rows of the table as fourth
	 * element.
	 */
	private List<Object> extractDataFromCsvFormat1(Sheet sheet) {
		List<Object> resultList = new ArrayList<>();
		BufferedReader br = null;
		List<String[]> rows = new ArrayList<>();
		Iterator<Row> rowIterator = sheet.rowIterator();
		Row tableNameArray = rowIterator.next();
		// Now traverse this row Iterator.
		try {
//			CSVReader csvReader = new CSVReader(new FileReader(file));
//			CSVReader csvReader = new CSVReader(new InputStreamReader(new FileInputStream(file), "UTF-8"),',', '\'', 0);
			// line 1
//			String[] tableNameArray = rowIterator.next().getCell(0);
			String tableName = tableNameArray.getCell(0).getStringCellValue();
			ETLMetaHelperVO doMeta = new ETLMetaHelperVO(null, tableName, null, null);
			resultList.add(doMeta);
			if (!tableName.trim().equals("CREATE TABLE")) {
			    if(tableName.equals("STANDARD FIELDS")){
                    resultList.add(null);
                }else{
                    String foreignKeyName = tableNameArray.getCell(1).getStringCellValue();
                    resultList.add(foreignKeyName);
                }
				// line 2

//				Iterator<Cell> cellIterator = rowIterator.next().cellIterator();
//				ArrayList<String> cells = new ArrayList<>();
//				while(cellIterator.hasNext()){
//					Cell cell = cellIterator.next();
//					cells.add(cell.getStringCellValue());
//				}
//				String[] columnType = new String[cells.size()];
//				cells.toArray(columnType);

//				resultList.add(columnType);
				String[] columnType = getStringArrayFromExcelRow(rowIterator.next());
				resultList.add(columnType);
//				Row columnType = rowIterator.next();

				// line 3
				String[] containerBlob = getStringArrayFromExcelRow(rowIterator.next());
//				Row containerBlob = rowIterator.next();
				resultList.add(containerBlob);
				// line 4
				String[] dummyColumnName = getStringArrayFromExcelRow(rowIterator.next());
//				Row dummyColumnName = rowIterator.next();
				resultList.add(dummyColumnName);
				// line 5
				String[] dbDataType = getStringArrayFromExcelRow(rowIterator.next());
//				Row dbDataType = rowIterator.next();
				resultList.add(dbDataType);
				// line 6
//				csvReader.readNext();
				rowIterator.next();
				// line 7
				String[] jsonName = getStringArrayFromExcelRow(rowIterator.next());
//				Row jsonName = rowIterator.next();
				resultList.add(jsonName);
				String[] rowValues = null;
//				while ((rowValues = csvReader.readNext()) != null) {
//					if (!rowValues[0].equals(""))
//						rows.add(rowValues);
//				}
				while (rowIterator.hasNext()) {
					rowValues = getStringArrayFromExcelRow(rowIterator.next());
					if (!rowValues[0].equals(""))
						rows.add(rowValues);
				}
				resultList.add(rows);
			} else {
				Map<String, List<ColumnName>> tableNameToColumnsMap = getTablesColumnMap(sheet);
				// generateVoObjects(tableNameToColumnsMap);

				generateVoObjects(tableNameToColumnsMap);

				resultList.add(tableNameToColumnsMap);
			}
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return resultList;
	}

	private String[] getStringArrayFromExcelRow(Row row){
		Iterator<Cell> cellIterator = row.cellIterator();
		ArrayList<String> cells = new ArrayList<>();
		while(cellIterator.hasNext()){
			Cell cell = cellIterator.next();
            if(cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
//                System.out.println("Formula is " + cell.getCellFormula());
                switch(cell.getCachedFormulaResultType()) {
                    case Cell.CELL_TYPE_NUMERIC:
//                        System.out.println("Last evaluated as: " + cell.getNumericCellValue());
                        if(HSSFDateUtil.isCellDateFormatted(cell)){
                            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                            try {
                                Date date = sdf.parse(cell.getDateCellValue().toString());
                                DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String cellValue = destDf.format(date);
                                cellValue = Timestamp.valueOf(cellValue).toString();
                                cells.add(cellValue);
                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }
                        }else{
                            cells.add(String.valueOf(cell.getNumericCellValue()));
                        }
                        break;
                    case Cell.CELL_TYPE_STRING:
                        cells.add(String.valueOf(cell.getRichStringCellValue()));
//                        System.out.println("Last evaluated as \"" + cell.getRichStringCellValue() + "\"");
                        break;
                }
            }else {
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
//                        System.out.println("Last evaluated as: " + cell.getNumericCellValue());
                        if (HSSFDateUtil.isCellDateFormatted(cell)) {
                            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                            try {
                                Date date = sdf.parse(cell.getDateCellValue().toString());
                                DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String cellValue = destDf.format(date);
                                cellValue = Timestamp.valueOf(cellValue).toString();
                                cells.add(cellValue);
                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
							cell.setCellType(Cell.CELL_TYPE_STRING);
                            cells.add(String.valueOf(Double.valueOf(cell.getStringCellValue()).longValue()));
                        }
                        break;
                    default:
                        cell.setCellType(Cell.CELL_TYPE_STRING);
                        cells.add(cell.getStringCellValue());
                }
            }

//            cell.setCellType(Cell.CELL_TYPE_STRING);
//			cell.setCellType(CellType.STRING);
//            HSSFDataFormatter hdf = new HSSFDataFormatter();
//            hdf.formatCellValue(cell);
//            cells.add(cell.getStringCellValue());
//			cells.add(hdf.formatCellValue(cell));
		}
		String[] result = new String[cells.size()];
		cells.toArray(result);
		return result;
	}
	private void generateVoObjects(Map<String, List<ColumnName>> tableNameToColumnsMap) {
		List<String> lines = new ArrayList<>();
		for (Map.Entry<String, List<ColumnName>> entry : tableNameToColumnsMap.entrySet()) {
			String tableName = entry.getKey();
			StringBuilder voClass = new StringBuilder("Class ");
			voClass.append(tableName).append("{ ").append("\n");

			List<ColumnName> columnList = entry.getValue();
			for (ColumnName column : columnList) {
				String columnName = column.getColumnName();

				if (!columnName.equals("")) {

					String words[] = columnName.split("_");
					StringBuilder voColumnName = new StringBuilder("private ");
					StringBuilder resultSetColumnName = new StringBuilder("");
					StringBuilder dummyVoColumnName = new StringBuilder();
					String dataType = "";
					if (column.getDbDataType().contains("BINARY") || column.getDbDataType().contains("VARCHAR")
							|| column.getDbDataType().equals("MEDIUMTEXT") || column.getDbDataType().equals("BLOB")
							|| column.getDbDataType().contains("DATETIME")) {
						voColumnName.append("String ");
						dataType = "String";
					} else if (column.getDbDataType().equals("BIT(1)")) {
						voColumnName.append("boolean ");
						dataType = "boolean";
					} else if (column.getDbDataType().equals("INT") || column.getDbDataType().equals("BIGINT")) {
						voColumnName.append("int ");
						dataType = "int";
					}
					for (String word : words) {
						if (!word.equals("")) {
							if (word.equals(words[0])) {
								voColumnName.append(word.toLowerCase());
								resultSetColumnName.append(word.toLowerCase());
								dummyVoColumnName.append(word.toLowerCase());
							} else {
								char a = word.charAt(0);
								voColumnName.append(a + word.substring(1).toLowerCase());
								resultSetColumnName.append(a + word.substring(1).toLowerCase());
								dummyVoColumnName.append(a + word.substring(1).toLowerCase());
							}
						}
					}
					String resultSet = generateSetter(resultSetColumnName.toString(),dataType,columnName);


					// String getter =
					// generateGetter(dummyVoColumnName.toString(),dataType);
					voClass.append(voColumnName.toString() + ";").append("\n");
					//voClass.append(resultSet).append("\n");
					// .append(setter)
					// .append("\n")
					// .append(getter)
					// .append("\n");

				}
			}
			voClass.append("}");
			lines.add(voClass.toString());
			//          LOG.debug(lines);
		}
		Path file1 = Paths.get("/Users/tapplent/Desktop/generateTheValueObjectContent.java");
		try {
			Files.write(file1, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private List<Object> extractDataFromCsvFormat2(Sheet sheet) {
		List<Object> resultList = new ArrayList<>();
		BufferedReader br = null;
		try {
//			CSVReader csvReader = new CSVReader(new FileReader(file));
//			CSVReader csvReader = new CSVReader(new InputStreamReader(new FileInputStream(file), "UTF-8"),',', '\'', 0);
			List<String[]> rows = new ArrayList<>();
			Iterator<Row> rowIterator = sheet.rowIterator();
			Row tableNameArray = rowIterator.next();
			// line 1
//			String[] tableNameArray = csvReader.readNext();
			String doName = tableNameArray.getCell(0).getStringCellValue();
			if (doName.trim().equals("STANDARD FIELDS")) {
				resultList = extractDataFromCsvFormat1(sheet);
			} else if (doName.trim().equals("CREATE TABLE")) {
				Map<String, List<ColumnName>> tableNameToColumnsMap = getTablesColumnMap(sheet);
				generateVoObjects(tableNameToColumnsMap);
				resultList.add(tableNameToColumnsMap);
			} else {
                String foreignKeyName = tableNameArray.getCell(1).getStringCellValue();
				resultList = extractDataFromCsvFormat2And3Helper(sheet, doName, foreignKeyName, 2);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return resultList;
	}

	private List<Object> extractDataFromCsvFormat2And3Helper(Sheet sheet, String doName, String foreignKeyName,
															 int formatNumber) throws IOException {
		Iterator<Row> rowIterator = sheet.rowIterator();
		rowIterator.next();
		List<Object> resultList = new ArrayList<>();
		List<String[]> rows = new ArrayList<>();
		ETLMetaHelperVO doMeta = etlDAO.getMetaByDoName(doName);
		if (doMeta == null || doMeta.getTableName() == null || doMeta.getTableName().equals("")) {
			try {
				DBUploadErrorLog.addError("Table for DO called {" + doName + "} doesn't exists");
				throw new Exception("Table for DO called {" + doName + "} doesn't exists");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Map<String, DOADetails> doaMetaMap = etlDAO.getDoaMetaByDoName(doName);
		resultList.add(doMeta);
		resultList.add(foreignKeyName);
		String[] doaNameArray = null;
		if (formatNumber == 2) {
			// line 2
			String[] columnType = getStringArrayFromExcelRow(rowIterator.next());
			resultList.add(columnType);
			doaNameArray = getStringArrayFromExcelRow(rowIterator.next());
		} else {
			// Add column type as NORMAL in all the fields
			doaNameArray = getStringArrayFromExcelRow(rowIterator.next());
			String[] columnType = new String[doaNameArray.length];
			for (int i = 0; i < columnType.length; i++) {
				columnType[i] = "NORMAL";
			}
			resultList.add(columnType);
		}
		// line 3 is array of doa Names. So once a doa name is read find out its
		// columnName, Container blob name and db DataType.
		int noOfMaxDOAs = doaNameArray.length;
		String[] containerBlob = new String[noOfMaxDOAs];
		String[] dummyColumnName = new String[noOfMaxDOAs];
		String[] dbDataType = new String[noOfMaxDOAs];
		String[] jsonName = new String[noOfMaxDOAs];
		for (int i = 0; i < doaNameArray.length; i++) {
			String doaName = doaNameArray[i];
			if (!doaName.equals("")) {
//				DOADetails doaDetails = etlDAO.getDOADetailsByDOAName(doaName);
				//TODO Add column for attachment.
				DOADetails doaDetails = doaMetaMap.get(doaName);
				try{
					if (doaDetails.getColumnName() != null && !doaDetails.getColumnName().equals("")) {
						LOG.debug(doaDetails.getColumnName());
						dummyColumnName[i] = doaDetails.getColumnName();
						if (doaDetails.getContainerBlobName() == null || doaDetails.getContainerBlobName().equals("")) {

							doaDetails.setContainerBlobName(doaDetails.getColumnName());
							LOG.debug(doaDetails.getContainerBlobName());
						}
						containerBlob[i] = doaDetails.getContainerBlobName();
						LOG.debug(containerBlob[i]);
						dbDataType[i] = doaDetails.getDbDataType();
						jsonName[i] = doaDetails.getDoaName();
					}else {
						try {
							DBUploadErrorLog.addError("Column for doa {" + doaName + "} does not exists");
							throw new Exception("Column for doa {" + doaName + "} does not exists");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}catch(Exception e){
					DBUploadErrorLog.addError("Column for doa {" + doaName + "} does not exists");
					e.printStackTrace();
				}
			}
		}
		// If any one column name = ATTACH_THUMBNAIL_IMAGEID then in meta set the thumbnail flag = true;
		for(Map.Entry<String, DOADetails> entry : doaMetaMap.entrySet()){
			DOADetails details = entry.getValue();
			if(details.getColumnName().equals("ATTACH_THUMBNAIL_IMAGEID")){
				doMeta.setIsThumbnailRequired(true);
			}
		}
		resultList.add(containerBlob);
		resultList.add(dummyColumnName);
		resultList.add(dbDataType);
		resultList.add(jsonName);
		if (formatNumber == 2) {
			// line 4
//			csvReader.readNext();
			rowIterator.next();
		}
		String[] rowValues = null;
//		while ((rowValues = csvReader.readNext()) != null) {
//			if (!rowValues[0].equals("") && !rowValues[0].equals("#REF!"))
//				rows.add(rowValues);
//		}
		while (rowIterator.hasNext()) {
			rowValues = getStringArrayFromExcelRow(rowIterator.next());
			if (!rowValues[0].equals(""))
				rows.add(rowValues);
		}
		resultList.add(rows);
		return resultList;
	}

	private List<Object> extractDataFromCsvFormat3(String filePath) {
		List<Object> resultList = new ArrayList<>();
		File file = new File(filePath);
		String fileName = file.getName();
		String[] splitedName = fileName.split("\\|");
		String headerCode = splitedName[0];
		EtlHeaderVO header = etlDAO.getHeaderByHeaderId(headerCode);
		if (header == null) {
			DBUploadErrorLog.addError("ETL Header for {" + headerCode + "} doesn not exists.");
			try {
				throw new Exception("ETL Header for {" + headerCode + "} doesn not exists.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			DBUploadTracker.dateTimeFormat = header.getDefaultDateTimeFormat();
			// DEFAULT_DATE_TIME_FORMAT_CODE_FK_ID add theis field to format.
			String doName = header.getSourceDoCodeFkId();
			CSVReader csvReader;
			try {
				csvReader = new CSVReader(new FileReader(filePath));
//				resultList = extractDataFromCsvFormat2And3Helper(csvReader, doName, null, 3);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return resultList;
	}


	private boolean isUpdateColumnType(String columnType, String dataType, String rowValue) {
		if(rowValue == null)
			return false;
		if (!columnType.equals("PRIMARY_KEY")) {
			if ( // dataType.equals("T_ID") ||
					columnType.equals("RELATIONSHIP_TYPE") ||
							columnType.equals("SCREEN_SECTION_MASTER") ||
							columnType.equals("UPDATE_DOMAIN_OBJECT_ATTRIBUTE") ||
							columnType.equals("UPDATE_MT_PE") ||
							columnType.startsWith("UPDATE_")) {
				if (columnType.startsWith("UPDATE_")) {
					columnType = columnType.substring(7);
				}
//				if (columnTypeMap.containsKey(columnType)) {
//					if (columnTypeMap.get(columnType).containsKey(rowValue))
//						return false;
//				}
				return true;
			} else {
				if (columnType.startsWith("PIPESPLIT")) {
					String columnTypes[] = columnType.split("\\|");
					StringBuilder sb = new StringBuilder();
					String realColumnType = columnTypes[1];
					if (realColumnType.startsWith("UPDATE_"))
						return true;


				}
				// switch(columnType){
				// case "PAGE":
				// case "CONTROL_MASTER":
				// case "DOMAIN_OBJECT_ATTRIBUTE" :
				// case "RELATIONSHIP_TYPE":
				// return true;
				// default: return false;
				// }
			}
		}

		return false;
	}

	private Object processAccordingToColumnType(String columnType, String value) {
		if (value == null || value.trim().equals("null") || value.trim().equals(""))
			return null;
		if (columnType.endsWith("_FORMULA")) {
			String realColumnType = new String(columnType.substring(0, columnType.indexOf("_FORMULA")));
			try {
				Pattern pattern = Pattern.compile("!\\{(.*?)\\}");
				Matcher matcher = pattern.matcher(value);
				while (matcher.find()) {
					String[] values = matcher.group(1).split(":");
					for (String singleValue : values) {
						processAccordingToColumnType(realColumnType, singleValue);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return value;
		}
		if(columnType.equals("PIPE_WORK_FLOW_ACTION_DFN")){
			columnType =columnType.substring(5);
			Pattern doPattern = Pattern.compile("\\{(.*?)\\}");
			Pattern rePattern = Pattern.compile("(\\*WFAD\\{(.*?)\\})");
			Matcher doMatcher = doPattern.matcher(value);
			Matcher reMatcher = rePattern.matcher(value);
			while (doMatcher.find() && reMatcher.find()) {
				String workflowActionCode = doMatcher.group(1).replace("[\\{|\\}]", "");
				String valueToReplace = (String) processAccordingToColumnType(columnType, workflowActionCode);
				if (valueToReplace != null){
					value = value.replace(reMatcher.group(1), valueToReplace);
				}
			}
			return value;
		}
		if(columnType.equals("UPDATE_DOMAIN_OBJECT_ATTRIBUTE")){
			columnType = "DOMAIN_OBJECT_ATTRIBUTE";
		}
		if(columnType.equals("UPDATE_MT_PE")){
			columnType = "MT_PROCESS_ELEMENT";
		}
		if(columnType.startsWith("UPDATE_")){
			columnType = columnType.substring(7);
		}
		if (columnType.equals("DOA_PATH")) {
			String[] values = value.split(":");
			String realColumnType = "DOMAIN_OBJECT_ATTRIBUTE";
			for (String singleValue : values) {
				processAccordingToColumnType(realColumnType, singleValue);
			}
			return value;
		}
		if(columnType.equals("DYNAMIC_COL_TYPE")){
			String[] values = value.split("\\|");
			String realColumnType = values[0];
			Object actualValue = processAccordingToColumnType(realColumnType, values[1]);
			return actualValue;
		}
		if(columnType.equals("FILTER_MIX_DYNAMIC_COL_TYPE")){
			if(value.contains("Quotes")) {

				String realValue = value.toString().substring(7,value.toString().length()-1);
				if (realValue.contains("|")) {
					String[] values = realValue.split("\\|");
					String realColumnType = values[0];
					Object actualValue = processAccordingToColumnType(realColumnType, values[1]);
					if(actualValue != null) {
						return new StringBuilder()
								.append('\'')
								.append(actualValue.toString())
								.append('\'')
								.toString();
					}else{
						return null;
					}
				}
				 return new StringBuilder()
						.append('\'')
						.append(realValue.toString())
						.append('\'')
						.toString();
			}
			if(value.startsWith("Control")){
				String realValue = value.toString().substring(8,value.toString().length()-1);
				if (realValue.contains("|")) {
					String[] values = realValue.split("\\|");
					String realColumnType = values[0];
					Object actualValue = processAccordingToColumnType(realColumnType, values[1]);
					if(actualValue != null) {
						return new StringBuilder()
								.append("Control|")
								.append(actualValue.toString())
								.toString();
					}else{
						return null;
					}
				}
				return new StringBuilder()
						.append("Control|")
						.append(realValue.toString())
						.toString();
			}
			if(value.contains("|")){
				String[] values = value.split("\\|");
				String realColumnType = values[0];
				Object actualValue = processAccordingToColumnType(realColumnType, values[1]);
				try {
					if(Util.isParsableAsDouble(actualValue.toString()))
						return actualValue;
				}catch (Exception e){
					DBUploadErrorLog
							.addError("Value {" + value + "} in column type {" + columnType + "} is not present.");
					e.printStackTrace();
					return null;
				}


				return actualValue;

			}
			if(Util.isParsableAsDouble(value.toString()))
				return value;

			return value;

		}
		if (columnType.matches("DO_DOA_EXPRESSION|EXPN")) {
			String realColumnType1 = "DOMAIN_OBJECT";
			String realColumnType2 = "DOMAIN_OBJECT_ATTRIBUTE";
			Pattern pattern1 = Pattern.compile("!\\{(.*?)\\}");
			Matcher matcher1 = pattern1.matcher(value);
			Pattern pattern2 = Pattern.compile("_\\{(.*?)\\}");
			Matcher matcher2 = pattern2.matcher(value);
			while (matcher1.find()) {
				String[] values = matcher1.group(1).split(":");
				for (String singleValue : values) {
					processAccordingToColumnType(realColumnType2, singleValue);
				}
			}
			while (matcher2.find()) {
				String[] values = matcher2.group(1).split(":");
				for (String singleValue : values) {
					processAccordingToColumnType(realColumnType1, singleValue);
				}
			}
			return value;
		}
//		if (columnType.matches("(?i:ICON)")) {
//			if(!iconMap.containsKey(value))
//			  iconMap.put(value, value);
//			return value;
//		}
		if (columnType.matches("(?i:SCREEN_SECTION_INSTANCE_ARGS)")) {
			String[] values = value.split("\\|");
			StringBuilder sb = new StringBuilder();
			String realColumnType = "SCREEN_SECTION_INSTANCE";
			int count = 0;
			for (String singleValue : values) {
				Object obj = processAccordingToColumnType(realColumnType, singleValue);
				sb.append(obj);
				if (count != values.length - 1) {
					sb.append("|");
					count++;
				}
			}
			return sb.toString();

		}
		//This code is for the columnType which contains PIPESPLIT
		if(columnType.startsWith("PIPESPLIT")){
			String columnTypes[] = columnType.split("\\|");
			StringBuilder sb = new StringBuilder();
			String realColumnType = columnTypes[1];
			String values[] = value.split("\\|");
			int count = 0;
			for(String realValue:values){
				Object obj = processAccordingToColumnType(realColumnType, realValue);
				sb.append(obj);
				if (count != values.length - 1) {
					sb.append("|");
					count++;
				}
			}
			return sb.toString();
		}
		if (columnType.matches("(?i:NORMAL)|(?i:PRIMARY_KEY)"))
			return value;
		if (columnTypeMap.containsKey(columnType)) {

			if (columnTypeMap.get(columnType).containsKey(value)) {
				return columnTypeMap.get(columnType).get(value);
			} else {
				try {
					if(!columnType.equals("ICON") && !columnType.equals("PERSON_TXN") && !(columnType.equals("PERSON") && value.equals("PRN_SYS")))
					DBUploadErrorLog
							.addError("Value {" + value + "} in column type {" + columnType + "} is not present.");
					if(columnType.matches("(?i:ICON)")){
						if(value !=null) {
							if (!iconMap.containsKey(value))
								iconMap.put(value, value);
						}
					}
//					throw new Exception("Value {" + value + "} in column type {" + columnType + "} is not present.");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			try {
				DBUploadErrorLog.addError("Column Type called {" + columnType + "} is not present.");
				throw new Exception("Column Type called {" + columnType + "} is not present.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private void addMapToColumnType(String columnType, Map<String, String> newMap, String primaryKeyName, String primaryKeyId) {
		if (columnTypeMap.containsKey(columnType)) {
			Map<String, String> existingMap = columnTypeMap.get(columnType);
			for (Entry<String, String> entry : newMap.entrySet()) {
				if (!existingMap.containsKey(entry.getKey())) {
					existingMap.put(entry.getKey(), entry.getValue());
					try {
						etlDAO.addEntryToSheetIDTODBIDMap(columnType, entry.getKey(), entry.getValue());
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} else {
					try {
						DBUploadErrorLog.addError("Primary key value {" + entry.getKey()
								+ "} is already present for column type {" + columnType + "}");
						throw new Exception("Primary key {" + entry.getKey() + "} is already present for column type {"
								+ columnType + "}");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			columnTypeMap.put(columnType, newMap);
			try {
				etlDAO.addEntryToSheetIDTODBIDMap(columnType, primaryKeyName, primaryKeyId);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void generateIconFile() {
		List<String> lines = new ArrayList<>();
		for (EtlIcon icon : iconList) {
			lines.add(new String(icon.getIcon() + "," + icon.getTableName() + "," + icon.getColumnName()));
		}
		Path file = Paths.get("/Users/tapplent/Desktop/generatedIconList.csv");
		try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void generateIconFileWithoutDuplicates() {
		List<String> lines = new ArrayList<>();
		for (Map.Entry<String,String> entry :iconMap.entrySet()) {

			lines.add(entry.getKey());
		}
		Path file = Paths.get("/Users/tapplent/Desktop/generatedIconListWithoutDuplicates.csv");
		try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void createTableCreationFile(Map<String, List<ColumnName>> tableNameToColumnsMap) {
		List<String> lines = new ArrayList<>();
		for (Map.Entry<String, List<ColumnName>> entry : tableNameToColumnsMap.entrySet()) {
			String tableName = entry.getKey();
			List<ColumnName> columnList = entry.getValue();
			String[] tableScript = getTableScript(tableName, columnList);
			lines.add(tableScript[0]);
			lines.add(tableScript[1]);
			try {
				etlDAO.executeUpdate(tableScript[0], null);
				etlDAO.executeUpdate(tableScript[1], null);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		Path file = Paths.get("/Users/tapplent/Desktop/generatedTableTapplentScript.sql");
		try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String[] getTableScript(String tableName, List<ColumnName> columnList) {
		String[] tableCreationArray = new String[2];
		StringBuilder stmt = new StringBuilder("DROP TABLE IF EXISTS ");
		int counter = 0;
		stmt.append(tableName).append(";\n");
		tableCreationArray[0] = stmt.toString();
		stmt = new StringBuilder();
		stmt.append("CREATE TABLE IF NOT EXISTS ").append(tableName).append("(\n");
		for (ColumnName column : columnList) {
			StringBuilder columnScript = new StringBuilder();
			columnScript.append(column.getColumnName()).append(" ").append("\t\t\t\t").append(column.getDbDataType())
					.append(" ").append("\t").append(column.getSpecialRequirements());
			if (!(counter == columnList.size() - 1))
				columnScript.append(",");
			columnScript.append("\n");
			counter++;
			stmt.append(columnScript);
		}
		stmt.append(") ROW_FORMAT=DYNAMIC;");
		tableCreationArray[1] = stmt.toString();
		return tableCreationArray;
	}

	private Map<String, List<ColumnName>> getTablesColumnMap(Sheet createTableFile) {
		Map<String, List<ColumnName>> tableNameToColumnsMap = new HashMap<>();
		try {
			Iterator<Row> rows = createTableFile.rowIterator();

//			CSVReader csvReader = new CSVReader(new FileReader(createTableFile));
//			csvReader.readNext();
//			csvReader.readNext();
			rows.next();
			rows.next();
			String[] columnDetails;
			while (rows.hasNext()) {
				Row row = rows.next();
				String tableName = row.getCell(0).getStringCellValue();
				String columnName = row.getCell(1).getStringCellValue();
				String dbDataType = row.getCell(2).getStringCellValue();
				String specialRequirements = row.getCell(3).getStringCellValue();
				if (!tableName.equals("")) {
					ColumnName column = new ColumnName(tableName, columnName, dbDataType, specialRequirements);
					if (tableNameToColumnsMap.containsKey(tableName)) {
						tableNameToColumnsMap.get(tableName).add(column);
					} else {
						List<ColumnName> columnList = new ArrayList<>();
						columnList.add(column);
						tableNameToColumnsMap.put(tableName, columnList);
					}
				}
			}
//			while ((columnDetails = csvReader.readNext()) != null) {
//				String tableName = columnDetails[0];
//				String columnName = columnDetails[1];
//				String dbDataType = columnDetails[2];
//				String specialRequirements = columnDetails[3];
//				if (!tableName.equals("")) {
//					ColumnName column = new ColumnName(tableName, columnName, dbDataType, specialRequirements);
//					if (tableNameToColumnsMap.containsKey(tableName)) {
//						tableNameToColumnsMap.get(tableName).add(column);
//					} else {
//						List<ColumnName> columnList = new ArrayList<>();
//						columnList.add(column);
//						tableNameToColumnsMap.put(tableName, columnList);
//					}
//				}
//			}
		} finally {

		}
		return tableNameToColumnsMap;
	}

	private void downloadSheets(String userId, List<GoogleSheetsDetails> fileInfoList) {
		Map<String, String> gmailIdToRefreshTokenMap = etlDAO.get_Access_Token(userId);
		if (gmailIdToRefreshTokenMap.containsKey(userId)) {
			String refreshToken = gmailIdToRefreshTokenMap.get(userId);
			String accessToken = GmailTokenUtil.getAccessToken(refreshToken);
			GoogleSheetsUtil.downloadSheetsToFolder(fileInfoList, accessToken);
		} else {
			// Here throw error that given gmail id doesn't exists.
		}
	}

	private void updateValues(List<UpdateColumn> updateColumnList) {
		for (UpdateColumn updateColumn : updateColumnList) {
			if (!updateColumn.getCurrentValue().equals("null")) {
				Object value = processAccordingToColumnType(updateColumn.getColumnType(),
						updateColumn.getCurrentValue());
				etlDAO.updateColumns(updateColumn, (String) value);
			}
		}
	}
	public void updatePersonTxn(){
	String SQL = "UPDATE T_PRN_EU_PERSON_TXN SET PERSON_DEP_FK_ID = (SELECT PERSONID FROM T_PERSON_DB.T_PERSON_TO_ID WHERE PERSON_DEP_FK_ID = personName);";
		try {
			etlDAO.executeUpdate(SQL,null);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	public  String getMultiLingualValue(String value) throws ClientProtocolException, IOException{

		if(g11ValueToLocaleMap.containsKey(value)){
//			Map<String,String> localeCodeToValueMap = g11ValueToLocaleMap.get(value);
//			JSONObject result = new JSONObject();
//			for (Map.Entry<String, String> entry : localeCodeToValueMap.entrySet())
//			{
//				result.put(entry.getKey(), entry.getValue());
//			}
//			result.put("en_US",value);
//			return result.toString();
			Map<String,String> localeCodeToValueMap = g11ValueToLocaleMap.get(value);
			Map<String, Object> result = new HashMap<>();
			for (Map.Entry<String, String> entry : localeCodeToValueMap.entrySet())
			{
				result.put(entry.getKey(), entry.getValue());
			}
			result.put("en_US",value);
			return Util.getStringFromG11nMapIntrmdte(result);

		}else{

			String key="trnsl.1.1.20170519T083354Z.9dbcfd33d130a6dd.26225d22a113e601a18e897f7251f9268e1aa5be";
			Map<String,String> localeCodeToValueMap = new HashMap<>();
			JSONObject result = new JSONObject();
			result.put("en_US", value);
			localeCodeToValueMap.put("en_US",value);
			for(String lang:localeCodes){
				StringBuilder url = new StringBuilder("https://translate.yandex.net/api/v1.5/tr.json/translate?lang=");
				url.append("en-").append(lang);
				url.append("&key=").append(key);
				url.append("&text=").append(URLEncoder.encode(value));
				HttpPost post = new HttpPost(url.toString());
				HttpClient client = HttpClientBuilder.create().build();
				HttpResponse response;
				response = client.execute(post);
				JSONObject respJson= new JSONObject(EntityUtils.toString(response.getEntity()));
				String transValue=respJson.getJSONArray("text").getString(0);

				if(lang.equals("fr")){

					localeCodeToValueMap.put("fr_FR", transValue);


					insertIntoG11NTable(value,"fr_FR",transValue);
					result.put("fr_FR", transValue);

				}

				if(lang.equals("zh")){
					localeCodeToValueMap.put("zh_CN_#Hans", transValue);

						insertIntoG11NTable(value,"zh_CN_#Hans",transValue);
						result.put("zh_CN_#Hans", transValue);


				}

				if(lang.equals("hu")){
					localeCodeToValueMap.put("hu_HU", transValue);


						insertIntoG11NTable(value,"hu_HU",transValue);
						result.put("hu_HU", transValue);


				}

				if(lang.equals("cs")){
					localeCodeToValueMap.put("cs_CZ", transValue);


						insertIntoG11NTable(value,"cs_CZ",transValue);
						result.put("cs_CZ", transValue);


				}

				if(lang.equals("de")){
					localeCodeToValueMap.put("de_DE", transValue);


						insertIntoG11NTable(value,"de_DE",transValue);
						result.put("de_DE", transValue);


				}

				if(lang.equals("id")){
					localeCodeToValueMap.put("in_ID", transValue);

						insertIntoG11NTable(value,"in_ID",transValue);
						result.put("in_ID", transValue);


				}

			}
			g11ValueToLocaleMap.put(value, localeCodeToValueMap);

			return result.toString();
		}

	}
	public String getG11NValue(String localeCode,String value,String [] localeCodes) throws ClientProtocolException, IOException {
		String key = "trnsl.1.1.20170519T083354Z.9dbcfd33d130a6dd.26225d22a113e601a18e897f7251f9268e1aa5be";
		Map<String, String> localeCodeToValueMap = new HashMap<>();
		if (localeCode.contains("fr")) {
			for (String lang : localeCodes) {
				StringBuilder url = new StringBuilder("https://translate.yandex.net/api/v1.5/tr.json/translate?lang=");
				String langcode[] = lang.split(":");
				url.append("fr-").append(langcode[0]);
				url.append("&key=").append(key);
				url.append("&text=").append(URLEncoder.encode(value));
				HttpPost post = new HttpPost(url.toString());
				HttpClient client = HttpClientBuilder.create().build();
				HttpResponse response;
				response = client.execute(post);
//				JSONObject respJson = new JSONObject(EntityUtils.toString(response.getEntity()));
//				String transValue = respJson.getJSONArray("text").getString(0);
			}

		}
		return null;
	}

	public EtlDAO getEtlDAO() {
		return etlDAO;
	}

	public void setEtlDAO(EtlDAO etlDAO) {
		this.etlDAO = etlDAO;
	}

	public MetadataService getMetadataService() {
		return metadataService;
	}

	public void setMetadataService(MetadataService metadataService) {
		this.metadataService = metadataService;
	}

	public MetaDataMapBuilder getMetaDataMapBuilder() {
		return metaDataMapBuilder;
	}

	public void setMetaDataMapBuilder(MetaDataMapBuilder metaDataMapBuilder) {
		this.metaDataMapBuilder = metaDataMapBuilder;
	}

	public ServerRuleManager getServerRuleManager() {
		return serverRuleManager;
	}

	public void setServerRuleManager(ServerRuleManager serverRuleManager) {
		this.serverRuleManager = serverRuleManager;
	}

	public AccessManagerService getAccessManagerService() {
		return accessManagerService;
	}

	public void setAccessManagerService(AccessManagerService accessManagerService) {
		this.accessManagerService = accessManagerService;
	}

	public ValidationError getValidationError() {
		return validationError;
	}

	public void setValidationError(ValidationError validationError) {
		this.validationError = validationError;
	}

	public HierarchyService getHierarchyService() {
		return hierarchyService;
	}

	public void setHierarchyService(HierarchyService hierarchyService) {
		this.hierarchyService = hierarchyService;
	}
}
