package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.common.util.AttachmentObject;
import com.tapplent.platformutility.layout.valueObject.IconVO;

import com.tapplent.platformutility.layout.valueObject.ProcessHomePageVO;
import org.springframework.beans.BeanUtils;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by tapplent on 15/11/16.
 */
public class ProcessHomePage {
    private String prcHomePagePkId;
    private String loggedInPersonFkId;
    private String prcHomePageRuleFkId;
    private String processTypeCodeFkId;
    private String categoryTitleG11nBigTxt;
    private IconVO categoryIconCode;
    private String cardTypeCodeFkId;
    private String cardTitleG11nBigTxt;
    private IconVO cardIconCode;
    private AttachmentObject cardImageid;
    private boolean isFrontCard;
    private boolean isCardFlippable;
    private String flipCardPrcHomePageFkId;
    private int displaySeqNumPosInt;
    private String resolvedTargetScreenInstanceFkId;
    private String resolvedTargetScreenSectionInstanceFkId;
    private int resolvedTargetTabSeqNumPosInt;
    private String dynamicActionFilterExpn;
    private String lastModifiedDatetime;
    private Map<Integer, ProcessHomePageContentGrouper> sectionToContentMap = new HashMap<>();

    public String getPrcHomePagePkId() {
        return prcHomePagePkId;
    }

    public void setPrcHomePagePkId(String prcHomePagePkId) {
        this.prcHomePagePkId = prcHomePagePkId;
    }

    public String getLoggedInPersonFkId() {
        return loggedInPersonFkId;
    }

    public void setLoggedInPersonFkId(String loggedInPersonFkId) {
        this.loggedInPersonFkId = loggedInPersonFkId;
    }

    public String getPrcHomePageRuleFkId() {
        return prcHomePageRuleFkId;
    }

    public void setPrcHomePageRuleFkId(String prcHomePageRuleFkId) {
        this.prcHomePageRuleFkId = prcHomePageRuleFkId;
    }

    public String getProcessTypeCodeFkId() {
        return processTypeCodeFkId;
    }

    public void setProcessTypeCodeFkId(String processTypeCodeFkId) {
        this.processTypeCodeFkId = processTypeCodeFkId;
    }

    public String getCategoryTitleG11nBigTxt() {
        return categoryTitleG11nBigTxt;
    }

    public void setCategoryTitleG11nBigTxt(String categoryTitleG11nBigTxt) {
        this.categoryTitleG11nBigTxt = categoryTitleG11nBigTxt;
    }
    
    public String getCardTypeCodeFkId() {
        return cardTypeCodeFkId;
    }

    public void setCardTypeCodeFkId(String cardTypeCodeFkId) {
        this.cardTypeCodeFkId = cardTypeCodeFkId;
    }

    public String getCardTitleG11nBigTxt() {
        return cardTitleG11nBigTxt;
    }

    public void setCardTitleG11nBigTxt(String cardTitleG11nBigTxt) {
        this.cardTitleG11nBigTxt = cardTitleG11nBigTxt;
    }

    public IconVO getCategoryIconCode() {
		return categoryIconCode;
	}

	public void setCategoryIconCode(IconVO categoryIconCode) {
		this.categoryIconCode = categoryIconCode;
	}

	public IconVO getCardIconCode() {
		return cardIconCode;
	}

	public void setCardIconCode(IconVO cardIconCode) {
		this.cardIconCode = cardIconCode;
	}

    public AttachmentObject getCardImageid() {
        return cardImageid;
    }

    public void setCardImageid(AttachmentObject cardImageid) {
        this.cardImageid = cardImageid;
    }

    public boolean isCardFlippable() {
        return isCardFlippable;
    }

    public void setCardFlippable(boolean cardFlippable) {
        isCardFlippable = cardFlippable;
    }

    public String getFlipCardPrcHomePageFkId() {
        return flipCardPrcHomePageFkId;
    }

    public void setFlipCardPrcHomePageFkId(String flipCardPrcHomePageFkId) {
        this.flipCardPrcHomePageFkId = flipCardPrcHomePageFkId;
    }

    public int getDisplaySeqNumPosInt() {
        return displaySeqNumPosInt;
    }

    public void setDisplaySeqNumPosInt(int displaySeqNumPosInt) {
        this.displaySeqNumPosInt = displaySeqNumPosInt;
    }

    public String getResolvedTargetScreenInstanceFkId() {
        return resolvedTargetScreenInstanceFkId;
    }

    public void setResolvedTargetScreenInstanceFkId(String resolvedTargetScreenInstanceFkId) {
        this.resolvedTargetScreenInstanceFkId = resolvedTargetScreenInstanceFkId;
    }

    public String getResolvedTargetScreenSectionInstanceFkId() {
        return resolvedTargetScreenSectionInstanceFkId;
    }

    public void setResolvedTargetScreenSectionInstanceFkId(String resolvedTargetScreenSectionInstanceFkId) {
        this.resolvedTargetScreenSectionInstanceFkId = resolvedTargetScreenSectionInstanceFkId;
    }

    public int getResolvedTargetTabSeqNumPosInt() {
        return resolvedTargetTabSeqNumPosInt;
    }

    public void setResolvedTargetTabSeqNumPosInt(int resolvedTargetTabSeqNumPosInt) {
        this.resolvedTargetTabSeqNumPosInt = resolvedTargetTabSeqNumPosInt;
    }

    public String getDynamicActionFilterExpn() {
        return dynamicActionFilterExpn;
    }

    public void setDynamicActionFilterExpn(String dynamicActionFilterExpn) {
        this.dynamicActionFilterExpn = dynamicActionFilterExpn;
    }

    public String getLastModifiedDatetime() {
        return lastModifiedDatetime;
    }

    public void setLastModifiedDatetime(String lastModifiedDatetime) {
        this.lastModifiedDatetime = lastModifiedDatetime;
    }

    public Map<Integer, ProcessHomePageContentGrouper> getSectionToContentMap() {
        return sectionToContentMap;
    }

    public void setSectionToContentMap(Map<Integer, ProcessHomePageContentGrouper> sectionToContentMap) {
        this.sectionToContentMap = sectionToContentMap;
    }

    public boolean isFrontCard() {
        return isFrontCard;
    }

    public void setFrontCard(boolean frontCard) {
        isFrontCard = frontCard;
    }

    public static ProcessHomePage fromVO(ProcessHomePageVO processHomePageVO){
        ProcessHomePage processHomePage = new ProcessHomePage();
        BeanUtils.copyProperties(processHomePageVO, processHomePage);
        return processHomePage;
    }
}
