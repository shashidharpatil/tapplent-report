package com.tapplent.apputility.layout.structure;

import java.sql.Timestamp;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.PropertyControlEventPatternMasterVO;

public class PropertyControlEventPatternMaster {
	@JsonIgnore
	private String versionId; 
	private String propertyControlEventPatternPkId; 
	private String propertyControlEventPatternCodeFkId;
	private JsonNode deviceIndependentBlob; 
	private JsonNode deviceDependentBlob;
	@JsonIgnore
	private Timestamp effectiveDatetime;
	@JsonIgnore
	private String saveStateCodeFkId;
	@JsonIgnore
	private String activeStateCodeFkId;
	@JsonIgnore
	private String recordStateCodeFkId;
	@JsonIgnore
	private boolean isDeleted;
	@JsonIgnore
	private JsonNode versionChangesBlob;
	@JsonIgnore
	private JsonNode propogateFutureChangesBlob;
	@JsonIgnore
	private String createdbyPersonFkId;
	@JsonIgnore
	private String createdByPersonFullnameTxt;
	@JsonIgnore
	private Timestamp createdDatetime;
	@JsonIgnore
	private String lastModifiedbyPersonFkId;
	@JsonIgnore
	private String lastModifiedbyPersonFullnameTxt;
	@JsonIgnore
	private Timestamp lastModifiedDatetime;
	@JsonIgnore
	private String dlgtdForCreatedbyPrsnFkId;
	@JsonIgnore
	private String dlgtdForCreatedbyPrsnFullnameTxt;@JsonIgnore 
	private String dlgtdForMdfdbyPrsnFkId; @JsonIgnore
	private String dlgtdForMdfdbyPrsnFullnameTxt; @JsonIgnore
	private boolean isFeatured;@JsonIgnore
	private JsonNode externalReferenceBlob;@JsonIgnore 
	private String refBaseObjectFkId; @JsonIgnore
	private String refBaseTemplateFkId; @JsonIgnore
	private String transitioningBaseTemplateFkId; @JsonIgnore
	private String refWorkflowDefnFkId; @JsonIgnore
	private String refWorkflowTxnFkId; @JsonIgnore
	private String refWorkflowTxnStatusCodeFkId; @JsonIgnore
	private String refWorkflowTxnStepFkId; @JsonIgnore
	private String refWorkflowTxnStepStatusCodeFkId;@JsonIgnore
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getPropertyControlEventPatternPkId() {
		return propertyControlEventPatternPkId;
	}
	public void setPropertyControlEventPatternPkId(String propertyControlEventPatternPkId) {
		this.propertyControlEventPatternPkId = propertyControlEventPatternPkId;
	}
	public String getPropertyControlEventPatternCodeFkId() {
		return propertyControlEventPatternCodeFkId;
	}
	public void setPropertyControlEventPatternCodeFkId(String propertyControlEventPatternCodeFkId) {
		this.propertyControlEventPatternCodeFkId = propertyControlEventPatternCodeFkId;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public Timestamp getEffectiveDatetime() {
		return effectiveDatetime;
	}
	public void setEffectiveDatetime(Timestamp effectiveDatetime) {
		this.effectiveDatetime = effectiveDatetime;
	}
	public String getSaveStateCodeFkId() {
		return saveStateCodeFkId;
	}
	public void setSaveStateCodeFkId(String saveStateCodeFkId) {
		this.saveStateCodeFkId = saveStateCodeFkId;
	}
	public String getActiveStateCodeFkId() {
		return activeStateCodeFkId;
	}
	public void setActiveStateCodeFkId(String activeStateCodeFkId) {
		this.activeStateCodeFkId = activeStateCodeFkId;
	}
	public String getRecordStateCodeFkId() {
		return recordStateCodeFkId;
	}
	public void setRecordStateCodeFkId(String recordStateCodeFkId) {
		this.recordStateCodeFkId = recordStateCodeFkId;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public JsonNode getVersionChangesBlob() {
		return versionChangesBlob;
	}
	public void setVersionChangesBlob(JsonNode versionChangesBlob) {
		this.versionChangesBlob = versionChangesBlob;
	}
	public JsonNode getPropogateFutureChangesBlob() {
		return propogateFutureChangesBlob;
	}
	public void setPropogateFutureChangesBlob(JsonNode propogateFutureChangesBlob) {
		this.propogateFutureChangesBlob = propogateFutureChangesBlob;
	}
	public String getCreatedbyPersonFkId() {
		return createdbyPersonFkId;
	}
	public void setCreatedbyPersonFkId(String createdbyPersonFkId) {
		this.createdbyPersonFkId = createdbyPersonFkId;
	}
	public String getCreatedByPersonFullnameTxt() {
		return createdByPersonFullnameTxt;
	}
	public void setCreatedByPersonFullnameTxt(String createdByPersonFullnameTxt) {
		this.createdByPersonFullnameTxt = createdByPersonFullnameTxt;
	}
	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public String getLastModifiedbyPersonFkId() {
		return lastModifiedbyPersonFkId;
	}
	public void setLastModifiedbyPersonFkId(String lastModifiedbyPersonFkId) {
		this.lastModifiedbyPersonFkId = lastModifiedbyPersonFkId;
	}
	public String getLastModifiedbyPersonFullnameTxt() {
		return lastModifiedbyPersonFullnameTxt;
	}
	public void setLastModifiedbyPersonFullnameTxt(String lastModifiedbyPersonFullnameTxt) {
		this.lastModifiedbyPersonFullnameTxt = lastModifiedbyPersonFullnameTxt;
	}
	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(Timestamp lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
	public String getDlgtdForCreatedbyPrsnFkId() {
		return dlgtdForCreatedbyPrsnFkId;
	}
	public void setDlgtdForCreatedbyPrsnFkId(String dlgtdForCreatedbyPrsnFkId) {
		this.dlgtdForCreatedbyPrsnFkId = dlgtdForCreatedbyPrsnFkId;
	}
	public String getDlgtdForCreatedbyPrsnFullnameTxt() {
		return dlgtdForCreatedbyPrsnFullnameTxt;
	}
	public void setDlgtdForCreatedbyPrsnFullnameTxt(String dlgtdForCreatedbyPrsnFullnameTxt) {
		this.dlgtdForCreatedbyPrsnFullnameTxt = dlgtdForCreatedbyPrsnFullnameTxt;
	}
	public String getDlgtdForMdfdbyPrsnFkId() {
		return dlgtdForMdfdbyPrsnFkId;
	}
	public void setDlgtdForMdfdbyPrsnFkId(String dlgtdForMdfdbyPrsnFkId) {
		this.dlgtdForMdfdbyPrsnFkId = dlgtdForMdfdbyPrsnFkId;
	}
	public String getDlgtdForMdfdbyPrsnFullnameTxt() {
		return dlgtdForMdfdbyPrsnFullnameTxt;
	}
	public void setDlgtdForMdfdbyPrsnFullnameTxt(String dlgtdForMdfdbyPrsnFullnameTxt) {
		this.dlgtdForMdfdbyPrsnFullnameTxt = dlgtdForMdfdbyPrsnFullnameTxt;
	}
	public boolean isFeatured() {
		return isFeatured;
	}
	public void setFeatured(boolean isFeatured) {
		this.isFeatured = isFeatured;
	}
	public JsonNode getExternalReferenceBlob() {
		return externalReferenceBlob;
	}
	public void setExternalReferenceBlob(JsonNode externalReferenceBlob) {
		this.externalReferenceBlob = externalReferenceBlob;
	}
	public String getRefBaseObjectFkId() {
		return refBaseObjectFkId;
	}
	public void setRefBaseObjectFkId(String refBaseObjectFkId) {
		this.refBaseObjectFkId = refBaseObjectFkId;
	}
	public String getRefBaseTemplateFkId() {
		return refBaseTemplateFkId;
	}
	public void setRefBaseTemplateFkId(String refBaseTemplateFkId) {
		this.refBaseTemplateFkId = refBaseTemplateFkId;
	}
	public String getTransitioningBaseTemplateFkId() {
		return transitioningBaseTemplateFkId;
	}
	public void setTransitioningBaseTemplateFkId(String transitioningBaseTemplateFkId) {
		this.transitioningBaseTemplateFkId = transitioningBaseTemplateFkId;
	}
	public String getRefWorkflowDefnFkId() {
		return refWorkflowDefnFkId;
	}
	public void setRefWorkflowDefnFkId(String refWorkflowDefnFkId) {
		this.refWorkflowDefnFkId = refWorkflowDefnFkId;
	}
	public String getRefWorkflowTxnFkId() {
		return refWorkflowTxnFkId;
	}
	public void setRefWorkflowTxnFkId(String refWorkflowTxnFkId) {
		this.refWorkflowTxnFkId = refWorkflowTxnFkId;
	}
	public String getRefWorkflowTxnStatusCodeFkId() {
		return refWorkflowTxnStatusCodeFkId;
	}
	public void setRefWorkflowTxnStatusCodeFkId(String refWorkflowTxnStatusCodeFkId) {
		this.refWorkflowTxnStatusCodeFkId = refWorkflowTxnStatusCodeFkId;
	}
	public String getRefWorkflowTxnStepFkId() {
		return refWorkflowTxnStepFkId;
	}
	public void setRefWorkflowTxnStepFkId(String refWorkflowTxnStepFkId) {
		this.refWorkflowTxnStepFkId = refWorkflowTxnStepFkId;
	}
	public String getRefWorkflowTxnStepStatusCodeFkId() {
		return refWorkflowTxnStepStatusCodeFkId;
	}
	public void setRefWorkflowTxnStepStatusCodeFkId(String refWorkflowTxnStepStatusCodeFkId) {
		this.refWorkflowTxnStepStatusCodeFkId = refWorkflowTxnStepStatusCodeFkId;
	}
	public static PropertyControlEventPatternMaster fromVO(PropertyControlEventPatternMasterVO propertyControlEventPatternMasterVO){
		PropertyControlEventPatternMaster propertyControlEventPatternMaster = new PropertyControlEventPatternMaster();
		BeanUtils.copyProperties(propertyControlEventPatternMasterVO, propertyControlEventPatternMaster);
		return propertyControlEventPatternMaster;
	}
}
