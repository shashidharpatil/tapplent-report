package com.tapplent.apputility.thirdparty.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.apache.http.NameValuePair; 
import com.amazonaws.util.json.JSONException;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.thirdPartyApp.gmail.EverNoteDAO;
import com.tapplent.platformutility.thirdPartyApp.gmail.ThirdPartyUtil;

public class BoxServiceImpl implements BoxService {
	 private EverNoteDAO evernoteDAO;
		
		public EverNoteDAO getEvernoteDAO() {
			return evernoteDAO;
		}
		public void setEvernoteDAO(EverNoteDAO evernoteDAO) {
			this.evernoteDAO = evernoteDAO;
		}
		@Override
	public String uploadTapplentAttachmentToBox(String attachmentId,String personId) throws org.json.JSONException, IOException, ParseException, JSONException{
		String status = "";
		String accessToken ="ypdv8XXGtkeUnQt12eBCwzkB3mf09qwl";
			Map<String,Object> attachmentMap =ThirdPartyUtil.getAttchment(attachmentId);
			 String mimeType = attachmentMap.get("ContentType").toString();
			 InputStream is = (InputStream) attachmentMap.get("InputStream");
//			 Map<String,String> tokens = evernoteDAO.getToken("BOX",personId);
//				String accessToken = tokens.get("accessToken");
//				String refreshToken = tokens.get("refreshToken");
//				boolean flag = ThirdPartyUtil.isTokenValid(accessToken);
//				if(flag==false){
//					accessToken = ThirdPartyUtil.getAccessToken(refreshToken);
//				}
				String attachmentName = "130001.mp3";
			 try {
				status = uploadToBox(is,mimeType,accessToken,attachmentName);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 if(status.equals("failure"))
				 return status;
		
		
		return status;
	}
	public String uploadBoxAttachmentToS3(String fileUrl,String mimeType) throws MalformedURLException, IOException{
		InputStream is = new URL(fileUrl).openStream();
		String attachmentArtifact = Util.uploadAttachmentToS3(mimeType, is);
		return attachmentArtifact;
	}
	private String uploadToBox(InputStream is, String mimeType, String accessToken, String attachmentName) throws Exception {
		
		HttpPost post = new HttpPost("https://upload.box.com/api/2.0/files/content");
		post.addHeader("Authorization", "Bearer "+accessToken);
		
		MultipartEntity builder = new MultipartEntity();
		
		ContentBody fileName = new StringBody("0");
		ContentBody fileBody = new InputStreamBody(is,attachmentName);
		
		builder.addPart("filename", fileBody);
		builder.addPart("folder_id",fileName);
		
		post.setEntity(builder);
		
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(post);
	    String status = String.valueOf(response.getStatusLine().getStatusCode());
	    String message = "failure";
	    if(status.equals("201"))
	    	message = "successfully uploaded to dropBox";
	    
		return message;
	}

}
