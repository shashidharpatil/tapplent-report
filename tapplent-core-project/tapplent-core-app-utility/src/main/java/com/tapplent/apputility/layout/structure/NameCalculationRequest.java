package com.tapplent.apputility.layout.structure;

import java.util.Map;

public class NameCalculationRequest {
    private Map<String, String> nameCalMap;
    private String localeCode;
    private String nameType;

    public Map<String, String> getNameCalMap() {
        return nameCalMap;
    }

    public void setNameCalMap(Map<String, String> nameCalMap) {
        this.nameCalMap = nameCalMap;
    }

    public String getLocaleCode() {
        return localeCode;
    }

    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }
}
