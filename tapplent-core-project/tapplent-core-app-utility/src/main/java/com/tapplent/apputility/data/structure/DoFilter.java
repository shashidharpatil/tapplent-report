package com.tapplent.apputility.data.structure;

import java.util.List;

import com.tapplent.platformutility.search.builder.FilterOperatorValue;
/*
 * Class to take input for external filter 
 */
public class DoFilter {
	private String attribute;
	private List<FilterOperatorValue> opAndValue;
	public DoFilter(){
		
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public List<FilterOperatorValue> getOpAndValue() {
		return opAndValue;
	}
	public void setOpAndValue(List<FilterOperatorValue> opAndValue) {
		this.opAndValue = opAndValue;
	}
}
