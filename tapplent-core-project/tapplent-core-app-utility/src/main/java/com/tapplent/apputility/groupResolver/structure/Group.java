package com.tapplent.apputility.groupResolver.structure;

import com.tapplent.apputility.data.structure.GenericQuery;
import com.tapplent.platformutility.groupresolver.valueobject.GroupVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 12/06/17.
 */
public class Group {
    private String groupID;
    private String genericQueryID;
    private String groupType;
    private GenericQuery genericQuery;

    public String getGenericQueryID() {
        return genericQueryID;
    }

    public void setGenericQueryID(String genericQueryID) {
        this.genericQueryID = genericQueryID;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public GenericQuery getGenericQuery() {
        return genericQuery;
    }

    public void setGenericQuery(GenericQuery genericQuery) {
        this.genericQuery = genericQuery;
    }

    public static Group fromVO(GroupVO groupVO){
        Group group = new Group();
        BeanUtils.copyProperties(groupVO, group);
        return group;
    }
}
