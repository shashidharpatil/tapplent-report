package com.tapplent.apputility.data.structure;

import com.tapplent.apputility.layout.structure.PersonTags;
import com.tapplent.platformutility.accessmanager.structure.Permission;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Manas on 12/29/16.
 */
public class UnitWriteResponseData {
    private Map<String, Object> responseMap;
    private Permission permission;
    private List<UnitWriteResponseData> childrenResponseMap;
    List<PersonTags> personTagList;
    public Map<String, Object> getResponseMap() {
        return responseMap;
    }

    public void setResponseMap(Map<String, Object> responseMap) {
        this.responseMap = responseMap;
    }

    public List<UnitWriteResponseData> getChildrenResponseMap() {
        return childrenResponseMap;
    }

    public void setChildrenResponseMap(List<UnitWriteResponseData> childrenResponseMap) {
        this.childrenResponseMap = childrenResponseMap;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public List<PersonTags> getPersonTagList() {
        return personTagList;
    }

    public void setPersonTagList(List<PersonTags> personTagList) {
        this.personTagList = personTagList;
    }
}
