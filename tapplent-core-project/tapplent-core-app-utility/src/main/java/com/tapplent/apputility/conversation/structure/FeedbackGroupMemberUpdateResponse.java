package com.tapplent.apputility.conversation.structure;

import java.util.List;

/**
 * Created by Tapplent on 6/22/17.
 */
public class FeedbackGroupMemberUpdateResponse {
    List<GroupMember> groupMember;

    public List<GroupMember> getGroupMember() {
        return groupMember;
    }

    public void setGroupMember(List<GroupMember> groupMember) {
        this.groupMember = groupMember;
    }
}
