package com.tapplent.apputility.data.structure;

import com.tapplent.platformutility.uilayout.valueobject.GenericQueryAttributesVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 10/06/17.
 */
public class GenericQueryAttribute {
    private String attributeID;
    private String queryID;
    private String mtPE;
    private String mtPEAlias;
    private String parentMtPEAlias;
    private String fkRelnWithParent;
    private String attributePath;
    private boolean isAggregate;

    public String getAttributeID() {
        return attributeID;
    }

    public void setAttributeID(String attributeID) {
        this.attributeID = attributeID;
    }

    public String getQueryID() {
        return queryID;
    }

    public void setQueryID(String queryID) {
        this.queryID = queryID;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getParentMtPEAlias() {
        return parentMtPEAlias;
    }

    public void setParentMtPEAlias(String parentMtPEAlias) {
        this.parentMtPEAlias = parentMtPEAlias;
    }

    public String getFkRelnWithParent() {
        return fkRelnWithParent;
    }

    public void setFkRelnWithParent(String fkRelnWithParent) {
        this.fkRelnWithParent = fkRelnWithParent;
    }

    public String getAttributePath() {
        return attributePath;
    }

    public void setAttributePath(String attributePath) {
        this.attributePath = attributePath;
    }

    public boolean isAggregate() {
        return isAggregate;
    }

    public void setAggregate(boolean aggregate) {
        isAggregate = aggregate;
    }

    public static GenericQueryAttribute fromVO(GenericQueryAttributesVO genericQueryAttributesVO){
        GenericQueryAttribute genericQueryAttribute = new GenericQueryAttribute();
        BeanUtils.copyProperties(genericQueryAttributesVO, genericQueryAttribute);
        return genericQueryAttribute;
    }
}
