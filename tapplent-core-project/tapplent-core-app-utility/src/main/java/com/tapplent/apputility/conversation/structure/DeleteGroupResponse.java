package com.tapplent.apputility.conversation.structure;

import java.util.List;

/**
 * Created by Tapplent on 6/23/17.
 */
public class DeleteGroupResponse {
    private List<String> feedbackGroupIds;

    public List<String> getFeedbackGroupIds() {
        return feedbackGroupIds;
    }

    public void setFeedbackGroupIds(List<String> feedbackGroupIds) {
        this.feedbackGroupIds = feedbackGroupIds;
    }
}
