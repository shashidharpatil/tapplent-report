package com.tapplent.apputility.data.structure;

import com.tapplent.platformutility.uilayout.valueobject.JobRelationshipsVO;
import org.codehaus.jackson.map.util.BeanUtil;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 06/11/17.
 */
public class JobRelationships {
    private String relationshipType;
    private String relationshipTypeName;
    private String relatedPersonID;
    private String relatedPersonName;

    public String getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(String relationshipType) {
        this.relationshipType = relationshipType;
    }

    public String getRelationshipTypeName() {
        return relationshipTypeName;
    }

    public void setRelationshipTypeName(String relationshipTypeName) {
        this.relationshipTypeName = relationshipTypeName;
    }

    public String getRelatedPersonID() {
        return relatedPersonID;
    }

    public void setRelatedPersonID(String relatedPersonID) {
        this.relatedPersonID = relatedPersonID;
    }

    public String getRelatedPersonName() {
        return relatedPersonName;
    }

    public void setRelatedPersonName(String relatedPersonName) {
        this.relatedPersonName = relatedPersonName;
    }

    public static JobRelationships fromVO(JobRelationshipsVO relationshipsVO) {
        JobRelationships jobRelationships = new JobRelationships();
        BeanUtils.copyProperties(relationshipsVO, jobRelationships);
        return jobRelationships;
    }
}
