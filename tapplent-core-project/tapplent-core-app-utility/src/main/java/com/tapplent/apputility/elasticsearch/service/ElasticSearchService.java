package com.tapplent.apputility.elasticsearch.service;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.suggest.Suggest;

import java.io.IOException;

/**
 * Created by tapplent on 08/05/17.
 */
public interface ElasticSearchService {
    public void populateIndexes() throws IOException;

    Object getGlobalSearchResult(String query);
}
