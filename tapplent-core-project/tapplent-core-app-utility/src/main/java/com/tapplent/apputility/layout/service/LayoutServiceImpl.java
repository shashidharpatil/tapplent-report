package com.tapplent.apputility.layout.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.amazonaws.services.s3.AmazonS3;
import com.tapplent.apputility.data.service.DataRequestService;
import com.tapplent.apputility.data.service.DataService;
import com.tapplent.apputility.data.structure.*;
import com.tapplent.apputility.uilayout.structure.*;
import com.tapplent.platformutility.activityLog.ActivityLog;
import com.tapplent.platformutility.activityLog.ActivityLogRequest;
import com.tapplent.platformutility.common.util.AttachmentObject;
import com.tapplent.platformutility.common.util.valueObject.NameCalculationRuleVO;
import com.tapplent.platformutility.layout.valueObject.SocialStatusVO;
import com.tapplent.apputility.layout.structure.*;
import com.tapplent.apputility.uilayout.service.UILayoutService;
import com.tapplent.platformutility.common.cache.PersonDetailsObjectRepository;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.*;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.person.structure.PersonGroup;
import com.tapplent.platformutility.search.builder.FilterOperatorValue;
import com.tapplent.platformutility.uilayout.valueobject.LoadParameters;
import com.tapplent.platformutility.workflow.structure.WorkflowActorAction;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import com.tapplent.tenantresolver.tenant.TenantInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.etl.dao.EtlDAO;
import com.tapplent.platformutility.layout.dao.LayoutDAO;

public class LayoutServiceImpl implements LayoutService{
	private LayoutDAO layoutDAO;
	private EtlDAO etlDAO;
    private UILayoutService uiLayoutService;
    private DataService dataService;
    private DataRequestService dataRequestService;

    public DataService getDataService() {
        return dataService;
    }

    public void setDataService(DataService dataService) {
        this.dataService = dataService;
    }

	public void setDataRequestService(DataRequestService dataRequestService) {
		this.dataRequestService = dataRequestService;
	}

	private static Map<String,List<String>> doaPathForLayout = new HashMap<>();
	private static final Logger LOG = LogFactory.getLogger(LayoutServiceImpl.class);
	@Transactional
	@Override
	public ResponseLayout getLayout(RequestLayout requestLayout) {
		ResponseLayout response = new ResponseLayout();
		doaPathForLayout.clear();
		MetaProcess bo = new MetaProcess();
		response.setMetaProcess(bo);
		bo.setMetaProcessCode(requestLayout.getMetaProcessCode());
		List<BaseTemplate> baseTemplateList = getBaseTemplateList(requestLayout);
		bo.setBaseTemplateList(baseTemplateList);
		addDObjectAndDOAttributeMeta(bo);
		return response;
	}
	private void addDObjectAndDOAttributeMeta(MetaProcess bo) {
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		for(Map.Entry<String, List<String>> entry : doaPathForLayout.entrySet()){
			String primaryBt = entry.getKey();
			List<String> doaPathList = entry.getValue();
			for(String doaPath : doaPathList){
				String[] doaCodesList = doaPath.split(":");
				for(int i=0; i<doaCodesList.length; i++){
					if(i==0){
						String[] doaSplit = doaCodesList[0].split("\\.");
						String doCode = doaSplit[0];
						LOG.debug("DO code is :: "+ doCode);
						EntityMetadata doMeta = metadataService.getMetadataByBtDo(primaryBt, doCode);
						bo.getDoMetaMap().put(doCode, doMeta);
						List<EntityAttributeMetadata> attributeMetaList = doMeta.getAttributeMeta();
						for(EntityAttributeMetadata attrMeta : attributeMetaList){
							bo.getDoaMetaMap().put(attrMeta.getTemplateDOACode(), attrMeta);
						}
					}else{
						List<EntityAttributeMetadata> btMetaList = metadataService.getBtAttributeMetaListByMtDoa(doaCodesList[i]);
						for(EntityAttributeMetadata meta : btMetaList){
							bo.getDoaMetaMap().put(meta.getTemplateDOACode(), meta);
						}
					}
				}
			}
		}
		for(Map.Entry<String, EntityMetadata> entry : bo.getDoMetaMap().entrySet()){
			bo.getDoMetaList().add(entry.getValue());
		}
		for(Map.Entry<String, EntityAttributeMetadata> entry : bo.getDoaMetaMap().entrySet()){
			bo.getAttrMetaList().add(entry.getValue());
		}
	}

	private List<BaseTemplate> getBaseTemplateList(RequestLayout requestLayout) {
		List<BaseTemplate> btList = new ArrayList<BaseTemplate>();
		String metaProcessCode = requestLayout.getMetaProcessCode();
		String baseTemplateId = requestLayout.getBaseTemplateId();
		Boolean includeArchived = requestLayout.isIncludeArchived();
		if(metaProcessCode == null)
			return null;
		if(baseTemplateId!=null){
			BaseTemplateVO btVO = layoutDAO.getBaseTemplateByBaseTemplate(baseTemplateId);
			BaseTemplate bt = BaseTemplate.fromVO(btVO);
			buildBaseTemplate(bt, requestLayout);
			btList.add(bt);
		}
		else{
			List<BaseTemplateVO> btVOList = layoutDAO.getBaseTemplateByMetaProcess(metaProcessCode, includeArchived);
			for(BaseTemplateVO btVO : btVOList){
				BaseTemplate bt = BaseTemplate.fromVO(btVO);
				buildBaseTemplate(bt, requestLayout);
				btList.add(bt);
			}
		}
		return btList;
	}
	@SuppressWarnings("unchecked")
	private void buildBaseTemplate(BaseTemplate bt, RequestLayout requestLayout) {
		String deviceType = requestLayout.getDeviceType();
		List<PageEquivalence> pageEquivalenceList = getPageEquivalenceList(bt.getBaseTemplateId(), deviceType);
		List<PageEquivalencePlacement> equivalencePlacementList = getPageEquivalencePlacementList(bt.getBaseTemplateId(), deviceType);
		List<ActionRepresetationLaunchTrxn> actionRepresetationLaunchTrxnList = getActionRepresentationLaunchByBT(bt.getBaseTemplateId(), deviceType);
		bt.setPageEquivalenceList(pageEquivalenceList);
		bt.setPageEquivalencePlacements(equivalencePlacementList);
		bt.setArLaunchList(actionRepresetationLaunchTrxnList);
		List<Canvas> canvasList = (List<Canvas>) getCanvas(bt, deviceType);
		bt.setCanvasList(canvasList);
		JsonNode btPropertyBlob = layoutDAO.getBtPropertyBlob(bt.getBaseTemplateId());
		bt.setBtPropertyBlob(getMergedBlob(btPropertyBlob, null));
	}
	private List<PageEquivalence> getPageEquivalenceList(String baseTemplateId, String deviceType) {
		List<PageEquivalence> pageEquivalenceList = new ArrayList<>();
		List<PageEquivalenceVO> pageEquivalenceVOList = layoutDAO.getPageEquivalenceByBT(baseTemplateId);
		for(PageEquivalenceVO equivalenceVO : pageEquivalenceVOList){
			PageEquivalence equivalence = PageEquivalence.fromVO(equivalenceVO);
			equivalence.setDeviceDependentBlob(getMergedBlob(equivalence.getDeviceDependentBlob(), null));
			equivalence.setDeviceIndependentBlob(getMergedBlob(equivalence.getDeviceIndependentBlob(), null));
			getDeviceTypeSpecificData(equivalence.getDeviceDependentBlob(),deviceType);
			pageEquivalenceList.add(equivalence);
		}
		return pageEquivalenceList;
	}
	private List<PageEquivalencePlacement> getPageEquivalencePlacementList(String baseTemplateId, String deviceType) {
		List<PageEquivalencePlacement> pageEquivalencePlacementList = new ArrayList<>();
		List<PageEquivalencePlacementVO> equivalencePlacementVOs = layoutDAO.getPageEquivalencePlacementByBT(baseTemplateId);
		for(PageEquivalencePlacementVO equivalencePlacementVO : equivalencePlacementVOs){
			PageEquivalencePlacement equivalencePlacement = PageEquivalencePlacement.fromVO(equivalencePlacementVO);
			equivalencePlacement.setDeviceDependentBlob(getMergedBlob(equivalencePlacement.getDeviceDependentBlob(), null));
			equivalencePlacement.setDeviceIndependentBlob(getMergedBlob(equivalencePlacement.getDeviceIndependentBlob(), null));
			getDeviceTypeSpecificData(equivalencePlacement.getDeviceDependentBlob(),deviceType);
			pageEquivalencePlacementList.add(equivalencePlacement);
		}
		return pageEquivalencePlacementList;
	}
	private List<ActionRepresetationLaunchTrxn> getActionRepresentationLaunchByBT(String baseTemplateId, String deviceType) {
		List<ActionRepresetationLaunchTrxn> actionRepresetationLaunchTrxnList = new ArrayList<>();
		List<ActionRepresetationLaunchTrxnVO> actionRepresetationLaunchTrxnVOList = layoutDAO.getActionRepresenationLaunchByBaseTemplate(baseTemplateId);
		for(ActionRepresetationLaunchTrxnVO actionRepresetationLaunchTrxnVO : actionRepresetationLaunchTrxnVOList){
			ActionRepresetationLaunchTrxn actionRepresetationLaunchTrxn = ActionRepresetationLaunchTrxn.fromVO(actionRepresetationLaunchTrxnVO);
			actionRepresetationLaunchTrxn.setDeviceDependentBlob(getMergedBlob(actionRepresetationLaunchTrxn.getDeviceDependentBlob(), null));
			actionRepresetationLaunchTrxn.setDeviceIndependentBlob(getMergedBlob(actionRepresetationLaunchTrxn.getDeviceIndependentBlob(), null));
			getDeviceTypeSpecificData(actionRepresetationLaunchTrxn.getDeviceDependentBlob(),deviceType);
			actionRepresetationLaunchTrxnList.add(actionRepresetationLaunchTrxn);
		}
		return actionRepresetationLaunchTrxnList;
	}
	private Object getCanvas(BaseTemplate baseTemplate, String deviceType) {
		String btId = baseTemplate.getBaseTemplateId();
		List<Canvas> canvasList = new ArrayList<>();
		Map<String, Canvas> canvasMap = new HashMap<>();
//		List<Canvas> CanvasHierarchyList = new ArrayList<>();
		List<CanvasVO> canvasVOList = layoutDAO.getCanvasListByBaseTemplate(btId,deviceType);
		for(CanvasVO canvasVO : canvasVOList){
			Canvas canvas = Canvas.fromVO(canvasVO);
			canvas.setDeviceDependentBlob(getMergedBlob(canvas.getMstDeviceDependentBlob(), canvas.getTxnDeviceDependentBlob()));
			canvas.setDeviceIndependentBlob(getMergedBlob(canvas.getMstDeviceIndependentBlob(), canvas.getTxnDeviceIndependentBlob()));
			canvas.setDeviceApplicableBlob(getMergedBlob(canvas.getDeviceApplicableBlob(), null));
			getDeviceTypeSpecificData(canvas.getDeviceDependentBlob(),deviceType);
			if(canvas.getDeviceApplicableBlob().get("applicableTo"+deviceType).asBoolean())
				canvasList.add(canvas);
			canvasMap.put(canvas.getCanvasId(), canvas);
		}
		buildCanvas(canvasMap, baseTemplate, deviceType);
//		if(mapperFormat)
//			return CanvasHierarchyList;
		return canvasList;
	}
	private JsonNode getMergedBlob(JsonNode masterBlob, JsonNode txnBlob) {
			Map<String, Object> mergedBlob = new HashMap<>();
			if(masterBlob == null && txnBlob == null) return null;
			if(masterBlob != null){
			Iterator<Entry<String, JsonNode>> masterIterator = masterBlob.fields();
			while(masterIterator.hasNext()){
				Entry<String, JsonNode> field = masterIterator.next();
				String fieldName = field.getKey();
				MasterEntityAttributeMetadata meta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(fieldName);
				Object fieldValue = field.getValue();
					mergedBlob.put(meta.getJsonName(), fieldValue);
			}
			}
			if(txnBlob != null){
				Iterator<Entry<String, JsonNode>> txnIterator = txnBlob.fields();
				while(txnIterator.hasNext()){
					Entry<String, JsonNode> field = txnIterator.next();
					String fieldName = field.getKey();
					Object fieldValue = field.getValue();
					MasterEntityAttributeMetadata meta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(fieldName);
					mergedBlob.put(meta.getJsonName(), fieldValue);
				}
			}
			ObjectMapper objectMapper = new ObjectMapper();
			JsonNode mergedNode = objectMapper.valueToTree(mergedBlob);
			return mergedNode;
	}

	/**
	 *
	 * @param canvasMap
	 * @param bt
	 * @param deviceType
     */
	private void buildCanvas(Map<String, Canvas> canvasMap, BaseTemplate bt, String deviceType) {
		String basetemplate = bt.getBaseTemplateId();
		Map<String, Control> controlMap = addControlsToCanvas(canvasMap,basetemplate,deviceType);
		addCanvasEventToCanvas(canvasMap, basetemplate, deviceType);
		Map<String, PropertyControl> propertyControlMap = addPropertyControlToCanvas(canvasMap,bt,deviceType);
		List<ActionStandAlone> actionStandaloneList = addActionStandAlone(canvasMap, controlMap, propertyControlMap, basetemplate, deviceType);
		List<CanvasActionForAG> groupActionsList =  addActionGroupToCanvas(canvasMap,basetemplate,deviceType);
		buildActionStepProperties(bt, actionStandaloneList, groupActionsList);
	}
	private void buildActionStepProperties(BaseTemplate bt, List<ActionStandAlone> actionStandaloneList,
			List<CanvasActionForAG> groupActionsList) {
		//Get all action steps for this bt.
		Map<String, List<ActionStep>> actionCodeToStepMap = getActionStepMap();
		Map<String, List<ClientEventRuleToProperty>> ruleToPropertiesMap = getClientEventRuleToPropertyMap();
		Map<String, Map<String, Map<String, GroupActionTxnProperties>>> gATxnIdToRuleToPropertyToValueMap = getGATxnIdToRuleToPropertyToValueMap(bt.getBaseTemplateId());
		Map<String, Map<String, Map<String, ActionStandAloneProperties>>> aSAIdToRuleToPropertyToValueMap = getASAIdToRuleToPropertyToValueMap(bt.getBaseTemplateId());
		for(ActionStandAlone actionStandAlone : actionStandaloneList){
			String actionCode = actionStandAlone.getActionCode();
			List<ActionStep> actionSteps = actionCodeToStepMap.get(actionCode);
			if(actionSteps!=null){
				for(ActionStep step : actionSteps){
					String rule = step.getRuleCode();
					List<ClientEventRuleToProperty> ruleToPropertyList = ruleToPropertiesMap.get(rule);
					if(ruleToPropertyList!=null){
						for(ClientEventRuleToProperty propertyList : ruleToPropertyList){
							String property =  propertyList.getProperty();
							if(aSAIdToRuleToPropertyToValueMap.containsKey(actionStandAlone.getActionStandaloneId())){
								if(aSAIdToRuleToPropertyToValueMap.get(actionStandAlone.getActionStandaloneId()).containsKey(rule)){
									if(aSAIdToRuleToPropertyToValueMap.get(actionStandAlone.getActionStandaloneId()).get(rule).containsKey(property)){
										String propertyValue = aSAIdToRuleToPropertyToValueMap.get(actionStandAlone.getActionStandaloneId()).get(rule).get(property).getPropertyValue();
										ActionStepProperty actionStepProperty = new ActionStepProperty(bt.getBaseTemplateId(), actionStandAlone.getMtPEId(), step.getClientActionStepRuleId(), property, propertyValue);
										bt.getActionStepPropertyList().add(actionStepProperty);
									}
								}
							}
						}
					}
				}
			}
		}
		for(CanvasActionForAG groupAction : groupActionsList){
			String actionCode = groupAction.getActionCode();
			List<ActionStep> actionSteps = actionCodeToStepMap.get(actionCode);
			if(actionSteps!=null){
				for(ActionStep step : actionSteps){
					String rule = step.getRuleCode();
					List<ClientEventRuleToProperty> ruleToPropertyList = ruleToPropertiesMap.get(rule);
					if(ruleToPropertyList!=null){
						for(ClientEventRuleToProperty propertyList : ruleToPropertyList){
							String property =  propertyList.getProperty();
							if(gATxnIdToRuleToPropertyToValueMap.containsKey(groupAction.getCanavsActionsForAgId())){
								if(gATxnIdToRuleToPropertyToValueMap.get(groupAction.getCanavsActionsForAgId()).containsKey(rule)){
									if(gATxnIdToRuleToPropertyToValueMap.get(groupAction.getCanavsActionsForAgId()).get(rule).containsKey(property)){
										String propertyValue = gATxnIdToRuleToPropertyToValueMap.get(groupAction.getCanavsActionsForAgId()).get(rule).get(property).getPropertyValue();
										ActionStepProperty actionStepProperty = new ActionStepProperty(bt.getBaseTemplateId(), groupAction.getMtPE(), step.getClientActionStepRuleId(), property, propertyValue);
										bt.getActionStepPropertyList().add(actionStepProperty);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	private Map<String, Map<String, Map<String, ActionStandAloneProperties>>> getASAIdToRuleToPropertyToValueMap(
			String baseTemplateId) {
		Map<String, Map<String, Map<String, ActionStandAloneProperties>>> aSAIdToRuleToPropertyToValueMap = new HashMap<>();
		List<ActionStandAlonePropertiesVO> actionStandAlonePropertiesVOList = layoutDAO.getStandAloneProperties(baseTemplateId);
		for(ActionStandAlonePropertiesVO actionTxnPropertiesVO : actionStandAlonePropertiesVOList){
			String standAloneTxnId = actionTxnPropertiesVO.getActionStandaloneTxnId();
			String rule = actionTxnPropertiesVO.getRule();
			String property = actionTxnPropertiesVO.getProperty();
			ActionStandAloneProperties standaloneActionTxnProperties = ActionStandAloneProperties.fromVO(actionTxnPropertiesVO);
			if(aSAIdToRuleToPropertyToValueMap.containsKey(standAloneTxnId)){
				Map<String, Map<String, ActionStandAloneProperties>> ruleToPropertyToValueMap = aSAIdToRuleToPropertyToValueMap.get(standAloneTxnId);
				if(ruleToPropertyToValueMap.containsKey(rule)){
					Map<String, ActionStandAloneProperties> propertyToValueMap = ruleToPropertyToValueMap.get(rule);
					propertyToValueMap.put(property, standaloneActionTxnProperties);
				}else{
					Map<String, ActionStandAloneProperties> propertyToValueMap = new HashMap<>();
					propertyToValueMap.put(property, standaloneActionTxnProperties);
					ruleToPropertyToValueMap.put(rule, propertyToValueMap);
				}
			}else{
				Map<String, ActionStandAloneProperties> propertyToValueMap = new HashMap<>();
				propertyToValueMap.put(property, standaloneActionTxnProperties);
				Map<String, Map<String, ActionStandAloneProperties>> ruleToPropertyToValueMap = new HashMap<>();
				ruleToPropertyToValueMap.put(rule, propertyToValueMap);
				aSAIdToRuleToPropertyToValueMap.put(standAloneTxnId, ruleToPropertyToValueMap);
			}
		}
		return aSAIdToRuleToPropertyToValueMap;
	}
	private Map<String, Map<String, Map<String, GroupActionTxnProperties>>> getGATxnIdToRuleToPropertyToValueMap(String baseTemplateId) {
		Map<String, Map<String, Map<String, GroupActionTxnProperties>>> gATxnIdToRuleToPropertyToValueMap = new HashMap<>();
		List<GroupActionTxnPropertiesVO> groupActionTxnPropertiesVOList = layoutDAO.getGroupActionProperties(baseTemplateId);
		for(GroupActionTxnPropertiesVO actionTxnPropertiesVO : groupActionTxnPropertiesVOList){
			String groupActionTxnId = actionTxnPropertiesVO.getGroupActionTxnId();
			String rule = actionTxnPropertiesVO.getRule();
			String property = actionTxnPropertiesVO.getProperty();
			GroupActionTxnProperties groupActionTxnProperties = GroupActionTxnProperties.fromVO(actionTxnPropertiesVO);
			if(gATxnIdToRuleToPropertyToValueMap.containsKey(groupActionTxnId)){
				Map<String, Map<String, GroupActionTxnProperties>> ruleToPropertyToValueMap = gATxnIdToRuleToPropertyToValueMap.get(groupActionTxnId);
				if(ruleToPropertyToValueMap.containsKey(rule)){
					Map<String, GroupActionTxnProperties> propertyToValueMap = ruleToPropertyToValueMap.get(rule);
					propertyToValueMap.put(property, groupActionTxnProperties);
				}else{
					Map<String, GroupActionTxnProperties> propertyToValueMap = new HashMap<>();
					propertyToValueMap.put(property, groupActionTxnProperties);
					ruleToPropertyToValueMap.put(rule, propertyToValueMap);
				}
			}else{
				Map<String, GroupActionTxnProperties> propertyToValueMap = new HashMap<>();
				propertyToValueMap.put(property, groupActionTxnProperties);
				Map<String, Map<String, GroupActionTxnProperties>> ruleToPropertyToValueMap = new HashMap<>();
				ruleToPropertyToValueMap.put(rule, propertyToValueMap);
				gATxnIdToRuleToPropertyToValueMap.put(groupActionTxnId, ruleToPropertyToValueMap);
			}
		}
		return gATxnIdToRuleToPropertyToValueMap;
	}
	private Map<String, List<ClientEventRuleToProperty>> getClientEventRuleToPropertyMap() {
		Map<String, List<ClientEventRuleToProperty>> ruleToPropertiesMap = new HashMap<>();
		List<ClientEventRuleToPropertyVO> clientEventRuleToPropertyVOList = layoutDAO.getClientEventRuleTOProeprtyMap();
		for(ClientEventRuleToPropertyVO clientEventRuleToPropertyVO : clientEventRuleToPropertyVOList){
			ClientEventRuleToProperty clientEventRuleToProperty = ClientEventRuleToProperty.fromVO(clientEventRuleToPropertyVO);
			String rule = clientEventRuleToProperty.getRule();
			if(ruleToPropertiesMap.containsKey(rule)){
				ruleToPropertiesMap.get(rule).add(clientEventRuleToProperty);
			}else{
				List<ClientEventRuleToProperty> properties = new ArrayList<>();
				properties.add(clientEventRuleToProperty);
				ruleToPropertiesMap.put(rule, properties);
			}
		}
		return ruleToPropertiesMap;
	}
	private List<ActionStandAlone> addActionStandAlone(Map<String, Canvas> canvasMap, Map<String, Control> controlMap,
			Map<String, PropertyControl> propertyControlMap, String basetemplateId, String deviceType) {
		List<ActionStandAlone> allStandaloneActions = new ArrayList<>();
		List<ActionStandAloneVO> actionStandAloneVOs = layoutDAO.getActionStandAloneByBaseTemplate(basetemplateId, deviceType);
		for(ActionStandAloneVO actionStandAloneVO : actionStandAloneVOs){
			ActionStandAlone actionStandAlone = new ActionStandAlone();
			BeanUtils.copyProperties(actionStandAloneVO, actionStandAlone);
			actionStandAlone.setDeviceDependentBlob(getMergedBlob(actionStandAlone.getMstDeviceDependentBlob(), actionStandAlone.getTxnDeviceDependentBlob()));
			actionStandAlone.setDeviceIndependentBlob(getMergedBlob(actionStandAlone.getMstDeviceIndependentBlob(), actionStandAlone.getTxnDeviceIndependentBlob()));
			actionStandAlone.setDeviceApplicableBlob(getMergedBlob(actionStandAlone.getDeviceApplicableBlob(), null));
			actionStandAlone.setActionLabel(actionStandAlone.getActionLabel());
			getDeviceTypeSpecificData(actionStandAlone.getDeviceDependentBlob(), deviceType);
			if(actionStandAlone.getDeviceApplicableBlob().get("applicableTo"+deviceType).asBoolean()){
				if(actionStandAlone.getPropertyControlId() != null){
					propertyControlMap.get(actionStandAlone.getPropertyControlId()).getPropertyControlActionList().add(actionStandAlone);
					allStandaloneActions.add(actionStandAlone);
				}else if(actionStandAlone.getControlId() != null){
					controlMap.get(actionStandAlone.getControlId()).getActionStandAloneList().add(actionStandAlone);
					allStandaloneActions.add(actionStandAlone);
				}else if(actionStandAlone.getCanvasId() != null){
					canvasMap.get(actionStandAlone.getCanvasId()).getActionStandAloneList().add(actionStandAlone);
					allStandaloneActions.add(actionStandAlone);
				}
			}
		}
		return allStandaloneActions;
	}
	private void addCanvasEventToCanvas(Map<String, Canvas> canvasMap, String basetemplate, String deviceType) {
		List<CanvasEventVO> canvasEventVOs = layoutDAO.getCanvasEventByBaseTemplate(basetemplate);
		for(CanvasEventVO canvasEventVO : canvasEventVOs){
			CanvasEvent canvasEvent = CanvasEvent.fromVO(canvasEventVO);
			canvasMap.get(canvasEventVO.getCanvasId()).getCanvasEventList().add(canvasEvent);
		}
		
	}
//	private void buildCanvasHierarchy(Map<String, Canvas> canvasMap, List<Canvas> canvasHierarchyList) {
//		for(Entry<String, Canvas> entry : canvasMap.entrySet()){
//			String parentCanvasId = entry.getValue().getParentCanvasId();
//			if(parentCanvasId != null)
//				canvasMap.get(parentCanvasId).getCanvasList().add(entry.getValue());
//			else canvasHierarchyList.add(entry.getValue());
//		}
//	}
	private Map<String, ActionGroup> getActionGroupMap(String baseTemplateId) {
		Map<String, ActionGroup> map = new HashMap<>();
		Map<String, ActionGroupVO> actionGroupVOMap = layoutDAO.getActionGroupMapByBt(baseTemplateId);
		for(Map.Entry<String, ActionGroupVO> entry : actionGroupVOMap.entrySet()){
			ActionGroup actionGroup = new ActionGroup();
			BeanUtils.copyProperties(entry.getValue(), actionGroup);
			actionGroup.setActionGroupLabel(actionGroup.getActionGroupLabel());
			map.put(entry.getKey(), actionGroup);
		}
		return map;
	}
	private List<CanvasActionForAG> addActionGroupToCanvas(Map<String, Canvas> canvasMap, String baseTemplateId, String deviceType) {
		List<CanvasActionForAgVO> canvasActionForAgVOList = layoutDAO.getCanvasActionForAgByBaseTemplate(baseTemplateId, deviceType);
		List<CanvasActionForAG> allGroupActions = new ArrayList<>();
//		Map<String,Map<String,Map<String,CtAcDtArVO>>> ctAcDtArMap = layoutDAO.getCtAcDtArByBT(baseTemplateId);
		Map<String,Map<String,Map<String,CtAcDtArVO>>> ctAcDtArMap = layoutDAO.getCtAcDtArByBT(baseTemplateId);
		Map<String, ActionGroup> actionGroupMap = getActionGroupMap(baseTemplateId); 
		Map<String, Map<String,ActionGroup>> canvasIdToAGMap = new HashMap<>();
		for(CanvasActionForAgVO  canvasActionForAgVO : canvasActionForAgVOList){
			CanvasActionForAG canvasActionForAG = new CanvasActionForAG();
			BeanUtils.copyProperties(canvasActionForAgVO, canvasActionForAG);
			canvasActionForAG.setDeviceDependentBlob(getMergedBlob(canvasActionForAG.getMstDeviceDependentBlob(), canvasActionForAG.getTxnDeviceDependentBlob()));
			canvasActionForAG.setDeviceIndependentBlob(getMergedBlob(canvasActionForAG.getMstDeviceIndependentBlob(), canvasActionForAG.getTxnDeviceIndependentBlob()));
			canvasActionForAG.setActionLabel(canvasActionForAG.getActionLabel());
			canvasActionForAG.setDeviceApplicableBlob(getMergedBlob(canvasActionForAG.getDeviceApplicableBlob(), null));
			if(canvasActionForAG.getDeviceApplicableBlob().get("applicableTo"+deviceType).asBoolean()){
				allGroupActions.add(canvasActionForAG);
				String actionGroupCode = canvasActionForAG.getActionGroupCode();
				String canvasId = canvasActionForAG.getCanvasId();
				ActionGroup actionGroup = actionGroupMap.get(actionGroupCode);
				String canvasTypeCode = canvasMap.get(canvasId).getCanvasTypeCode();
				if(actionGroup!=null){
					String actionCategoryCode = actionGroup.getActionCategoryCode();
					//FIXME this will throw error. Check for it later.
					CtAcDtArVO arCode = ctAcDtArMap.get(canvasTypeCode).get(actionCategoryCode).get(deviceType.toUpperCase());	
					if(canvasIdToAGMap.containsKey(canvasId)){
						Map<String,ActionGroup> actionCodeToAgMap = canvasIdToAGMap.get(canvasId);
						if(actionCodeToAgMap.containsKey(actionGroupCode)){
							ActionGroup group = actionCodeToAgMap.get(actionGroupCode);
							group.getCanvasActionForAGList().add(canvasActionForAG);
						}else{
							// if action group is not present
							ActionGroup newAG = new ActionGroup();
							BeanUtils.copyProperties(actionGroup, newAG);
							newAG.setArCode(arCode.getArCode());
							newAG.setArSeqNumber(arCode.getArSeqNumber());
							List<CanvasActionForAG> actionList = new ArrayList<>();
							actionList.add(canvasActionForAG);
							newAG.setCanvasActionForAGList(actionList);
							newAG.setActionGroupList(new ArrayList<>());
							actionCodeToAgMap.put(actionGroupCode, newAG);
							// check if parent action group is present in the action canvas id to action group map
							if(newAG.getParentActionGroupCode() != null){
								if(actionCodeToAgMap.containsKey(newAG.getParentActionGroupCode())){
									actionCodeToAgMap.get(newAG.getParentActionGroupCode()).getActionGroupList().add(newAG);
								}else{
									ActionGroup newParentAG = new ActionGroup();
									BeanUtils.copyProperties(actionGroupMap.get(newAG.getParentActionGroupCode()), newParentAG);
									CtAcDtArVO parentAr = ctAcDtArMap.get(canvasTypeCode).get(newParentAG.getActionCategoryCode()).get(deviceType);
									newParentAG.setArCode(parentAr.getArCode());
									newParentAG.setArSeqNumber(parentAr.getArSeqNumber());
									List<ActionGroup> actionGroupList = new ArrayList<>();
									actionGroupList.add(newAG);
									newParentAG.setActionGroupList(actionGroupList);
									newParentAG.setCanvasActionForAGList(new ArrayList<>());
									actionCodeToAgMap.put(newAG.getParentActionGroupCode(), newParentAG);
									canvasMap.get(canvasId).getActionGroupList().add(newParentAG);
								}
							}else{
								canvasMap.get(canvasId).getActionGroupList().add(newAG);
							}
						}
					}else{
						Map<String,ActionGroup> actionGroupCodeMap = new HashMap<>();
						ActionGroup newAG = new ActionGroup();
						BeanUtils.copyProperties(actionGroup, newAG);
						newAG.setArCode(arCode.getArCode());
						newAG.setArSeqNumber(arCode.getArSeqNumber());
						List<CanvasActionForAG> actionList = new ArrayList<>();
						actionList.add(canvasActionForAG);
						newAG.setCanvasActionForAGList(actionList);
						newAG.setActionGroupList(new ArrayList<>());
						actionGroupCodeMap.put(actionGroupCode, newAG);
						// check if parent action group is present in the action canvas id to action group map
						if(newAG.getParentActionGroupCode() != null){
								ActionGroup newParentAG = new ActionGroup();
								BeanUtils.copyProperties(actionGroupMap.get(newAG.getParentActionGroupCode()), newParentAG);
								CtAcDtArVO parentAr = ctAcDtArMap.get(canvasTypeCode).get(newParentAG.getActionCategoryCode()).get(deviceType);
								newParentAG.setArCode(parentAr.getArCode());
								newParentAG.setArSeqNumber(parentAr.getArSeqNumber());
								List<ActionGroup> actionGroupList = new ArrayList<>();
								actionGroupList.add(newAG);
								newParentAG.setActionGroupList(actionGroupList);
								newParentAG.setCanvasActionForAGList(new ArrayList<>());
								actionGroupCodeMap.put(newAG.getParentActionGroupCode(), newParentAG);
								canvasMap.get(canvasId).getActionGroupList().add(newParentAG);
						}else{
							canvasMap.get(canvasId).getActionGroupList().add(newAG);
						}
						canvasIdToAGMap.put(canvasId, actionGroupCodeMap);
					}
	//				canvasMap.get(canvasActionForAG.getCanvasId()).getCanvasActionForAGList().add(canvasActionForAG);
				}else{
					try {
						throw new Exception("Action group {"+actionGroupCode+"} is not present.");
					} catch (Exception e) {
						e.printStackTrace();
					} 
				}
			}
		}
		return allGroupActions;
	}
	private Map<String, PropertyControl> addPropertyControlToCanvas(Map<String, Canvas> canvasMap, BaseTemplate bt, String deviceType) {
		String baseTemplate = bt.getBaseTemplateId();
		Map<String, PropertyControl> propertyControlMap = new HashMap<>();
		List<PropertyControlVO> propertyControlVOList = layoutDAO.getPropertyControlListByBaseTemplate(baseTemplate);
		for(PropertyControlVO propertyControlVO : propertyControlVOList){
			PropertyControl propertyControl = PropertyControl.fromVO(propertyControlVO);
			if(propertyControl.getPropertyControlAttribute()!=null){
				addDoaPathToMap(propertyControl.getBaseTemplateId(), propertyControl.getPropertyControlAttribute());
			}
			BeanUtils.copyProperties(propertyControlVO, propertyControl);
			propertyControl.setDeviceDependentBlob(getMergedBlob(propertyControl.getMstDeviceDependentBlob(), propertyControl.getTxnDeviceDependentBlob()));
			propertyControl.setDeviceIndependentBlob(getMergedBlob(propertyControl.getMstDeviceIndependentBlob(), propertyControl.getTxnDeviceIndependentBlob()));
			getDeviceTypeSpecificData(propertyControl.getDeviceDependentBlob(), deviceType);
			propertyControlMap.put(propertyControl.getPropertyControlId(), propertyControl);
			if(canvasMap.get(propertyControl.getCanvasId())!= null)
				canvasMap.get(propertyControl.getCanvasId()).getPropertyControlList().add(propertyControl);
			else System.out.println("--------------------"+ propertyControl.getCanvasId());
		}
//		Map<String,PropertyControlLookup> propertyControlLookup = getPropertyControlLookup();
//		Map<String, PropertyControl> propertyControlMap = new HashMap<>();
//		for(PropertyControlVO propertyControlVO : propertyControlVOList){
//			PropertyControl propertyControl = PropertyControl.fromVO(propertyControlVO);
//			getDeviceTypeSpecificData(propertyControl.getPropertyControlPlacement(), deviceType);
//			fillValue(propertyControl,bt,propertyControlLookup);
//			propertyControlMap.put(propertyControl.getCanvasPropertyControlEuPkId(), propertyControl);
//			String canvasEuFkId = propertyControl.getCanvasEuFkId();
//			canvasMap.get(canvasEuFkId).getPropertyControlList().add(propertyControl);
//		}
//		buildPropertyControl(propertyControlMap, baseTemplate,deviceType);
		return propertyControlMap;
	}
//	private void fillValue(PropertyControl propertyControl, BaseTemplate bt, Map<String, PropertyControlLookup> propertyControlLookup) {
//		String propertyControlCode = propertyControl.getPropertyControlCodeFkId();
//		PropertyControlLookup pcLookup = propertyControlLookup.get(propertyControlCode);
//		System.out.println(propertyControlCode);
//		if(pcLookup!=null){
//			if(pcLookup.getSourceCategory().equals("Layout.BTProperty")){
//				JsonNode btProperty = bt.getBtPropertyBlob();
//				//Add the property control value here.
//				Object value = btProperty.findValue(pcLookup.getLayoutKey());
//				propertyControl.setValue(value);
//			}
//		}
//	}
	private Map<String, Control> addControlsToCanvas(Map<String, Canvas> canvasMap, String baseTemplateId, String deviceType) {
		List<ControlVO> controlVOList = layoutDAO.getControlListByBaseTemplate(baseTemplateId);
		Map<String, Control> controlMap = new HashMap<>();
		for(ControlVO controlVO : controlVOList){
			Control control = Control.fromVO(controlVO);
			addDoaPathToMap(control.getBaseTemplateId(), control.getControlAttribute());
			control.setDeviceDependentBlob(getMergedBlob(control.getMstDeviceDependentBlob(), control.getTxnDeviceDependentBlob()));
			control.setDeviceIndependentBlob(getMergedBlob(control.getMstDeviceIndependentBlob(), control.getTxnDeviceIndependentBlob()));
			getDeviceTypeSpecificData(control.getDeviceDependentBlob(), deviceType);
			controlMap.put(control.getControlId(), control);
			String canvasEuFkId = control.getCanvasId();
			if(canvasMap.get(canvasEuFkId) != null)
				canvasMap.get(canvasEuFkId).getControlList().add(control);
			else System.out.println("----------------"+canvasEuFkId+"-----------------");
		}
		buildControl(controlMap,baseTemplateId,deviceType);
		return controlMap;
	}
	private void addDoaPathToMap(String baseTemplateId, String doaPath) {
		if(doaPath==null)
			return;
		if(doaPathForLayout.containsKey(baseTemplateId)){
			doaPathForLayout.get(baseTemplateId).add(doaPath);
		}else{
			List<String> controlAttrList = new ArrayList<>();
			controlAttrList.add(doaPath);
			doaPathForLayout.put(baseTemplateId,controlAttrList);
		}
	}
	private void buildControl(Map<String, Control> controlMap, String baseTemplateId, String deviceType) {
//		addControlRelationControlToControl(controlMap, baseTemplateId, deviceType);
//		addControlHierarchyControlToControl(controlMap, baseTemplateId);
		addControlGroupByControlToControl(controlMap, baseTemplateId);
		addControlEventToControl(controlMap, baseTemplateId);
	}
	private void addControlEventToControl(Map<String, Control> controlMap, String baseTemplateId) {
		List<ControlEventVO> controlEventVOList = layoutDAO.getControlEventByBaseTemplate(baseTemplateId);
		for(ControlEventVO controlEventEuVO : controlEventVOList){
			ControlEvent controlEventEU = ControlEvent.fromVO(controlEventEuVO);
			controlMap.get(controlEventEU.getControlId()).getControlEventList().add(controlEventEU);
		}
	}
	private void addControlGroupByControlToControl(Map<String, Control> controlMap, String baseTemplateId) {
		List<ControlGroupByControlVO> controlGroupByControlVOList = layoutDAO.getControlGroupByControlByBaseTemplate(baseTemplateId);
		for(ControlGroupByControlVO controlGroupByControlEuVO : controlGroupByControlVOList){
			ControlGroupByControl controlGroupByControl = ControlGroupByControl.fromVO(controlGroupByControlEuVO);
			controlMap.get(controlGroupByControl.getControlId()).getControlGroupByList().add(controlGroupByControl);
		}
	}
	private void addControlHierarchyControlToControl(Map<String, Control> controlMap, String baseTemplateId) {
		List<ControlHierarchyControlVO> controlHierarchyControlVOList = layoutDAO.getControlHierarchyControlByBaseTemplate(baseTemplateId);
		for(ControlHierarchyControlVO controlHierarchyControlEuVO : controlHierarchyControlVOList){
			ControlHierarchyControl controlHierarchyControl = ControlHierarchyControl.fromVO(controlHierarchyControlEuVO);
			controlMap.get(controlHierarchyControl.getControlId()).getControlHCList().add(controlHierarchyControl);
		}
	}

	/**
	 * This method adds
	 * @param controlMap
	 * @param baseTemplateId
	 * @param deviceType
     */
	private void addControlRelationControlToControl(Map<String, Control> controlMap, String baseTemplateId, String deviceType) {
		List<ControlRelationControlVO> controlRelationControlVOList = layoutDAO.getControlRelationControlByBaseTemplate(baseTemplateId);
		Map<String, Control> relationControlMap = new HashMap<>();
		Map<String, List<String>> relationControlCanvasIdToListControlIdMap = new HashMap<>();
		Map<String, String> relationControlCanvasMap = new HashMap<>();
		for(ControlRelationControlVO controlRelationControlEuVO : controlRelationControlVOList){
			ControlRelationControl controlRelationControlEU  = ControlRelationControl.fromVO(controlRelationControlEuVO);
			Control relationControl = controlMap.get(controlRelationControlEU.getControlId());
			relationControl.getControlRCList().add(controlRelationControlEU);
			relationControl.getRcCanvasControlMasterIdToControlRCMap().put(controlRelationControlEU.getRcCanvasControlMasterId(), controlRelationControlEU);
			if(relationControlCanvasIdToListControlIdMap.containsKey(relationControl.getRelationControlCanvasMasterId())){
				relationControlCanvasIdToListControlIdMap.get(relationControl.getRelationControlCanvasMasterId()).add(relationControl.getControlId());
			}else {
				List<String> controlIdList = new ArrayList<>();
				controlIdList.add(relationControl.getControlId());
				relationControlCanvasIdToListControlIdMap.put(relationControl.getRelationControlCanvasMasterId(), controlIdList);
			}
			relationControlMap.put(relationControl.getControlId(), relationControl);
			relationControlCanvasMap.put(relationControl.getRelationControlCanvasMasterId(), relationControl.getRelationControlCanvasMasterId());
		}
		/* now we have list of all the controls that are relation control */
		/* now we need to get to the canvas master table and fetch all the associated master tables */
		/* now fetch the data of canvas master for all the canvas masters which are present */

		List<CanvasMasterVO> canvasMasterVOList = layoutDAO.getCanvasMasterList(relationControlCanvasMap);
		Map<String, CanvasMaster> allCanvasIdToMasterMap = new HashMap<>();
		List<String> allCanvasIdsList = new ArrayList<>();
		for(CanvasMasterVO vo : canvasMasterVOList){
			CanvasMaster canvasMaster = CanvasMaster.fromVO(vo);
			canvasMaster.setDeviceDependentBlob(getMergedBlob(canvasMaster.getDeviceDependentBlob(), null));
			canvasMaster.setDeviceIndependentBlob(getMergedBlob(canvasMaster.getDeviceIndependentBlob(), null));
			canvasMaster.setDeviceApplicableBlob(getMergedBlob(canvasMaster.getDeviceApplicableBlob(), null));
			getDeviceTypeSpecificData(canvasMaster.getDeviceDependentBlob(),deviceType);
			if(canvasMaster.getDeviceApplicableBlob().get("applicableTo"+deviceType).asBoolean()) {
				allCanvasIdsList.add(canvasMaster.getCanvasId());
				allCanvasIdToMasterMap.put(canvasMaster.getCanvasId(), canvasMaster);
				List<String> controlIdList = relationControlCanvasIdToListControlIdMap.get(canvasMaster.getCanvasId());
				/* This is done to avoid assigning same object(canvas in this case) to multiple control (Deep cloning problem)*/
				for(String controlId : controlIdList){
					Control control = controlMap.get(controlId);
					CanvasMaster newCanvasMaster = new CanvasMaster();
					BeanUtils.copyProperties(canvasMaster, newCanvasMaster);
				}
				/* Now we have list of all the canvases where we should add this canvas */
			}

		}
//		buildCanvasMaster(allCanvasIdsList, allCanvasIdToMasterMap, deviceType);
		/* 
		 * We will build the canvases first
		 * This is building of canvas Master
		 * Now that we have all the canvases we need controls, propertyControls, actions
		 * Get the controls  
		 */
		List<ControlMasterVO> controlMasterVOList = layoutDAO.getControlMasterList(allCanvasIdsList);
		List<ControlMaster> controlMasterList = new ArrayList<>();
		Map<String, List<ControlMaster>> canvasMasterIdToControlMasterMap = new HashMap<>();
		/* Now we need to add this control list to the corresponding canvases */
		for(ControlMasterVO vo : controlMasterVOList){
			ControlMaster controlMaster = ControlMaster.fromVO(vo);

//			controlMaster.setDeviceDependentBlob(getMergedBlob(controlMaster.getDeviceDependentBlob(), null));
//			controlMaster.setDeviceIndependentBlob(getMergedBlob(controlMaster.getDeviceIndependentBlob(), null));
//
//			getDeviceTypeSpecificData(controlMaster.getDeviceDependentBlob(), deviceType);
			String canvasMasterId = controlMaster.getCanvasId();
			if(canvasMasterIdToControlMasterMap.containsKey(controlMaster.getCanvasId())){
				canvasMasterIdToControlMasterMap.get(controlMaster.getCanvasId()).add(controlMaster);
			}else{
				List<ControlMaster> newControlMasterList = new ArrayList<>();
				newControlMasterList.add(controlMaster);
				canvasMasterIdToControlMasterMap.put(controlMaster.getCanvasId(), newControlMasterList);
			}
//			controlMasterList.add(controlMaster);
			allCanvasIdToMasterMap.get(canvasMasterId).getControlList().add(controlMaster);
			/*
				Now loop through all the control master
				This control master is required to be attached to a canvas Master
				This control master has a canvasMasterFKID
				We can create a map of canvas master id to control master Map
			 */
			for(Map.Entry<String, Control> entry : relationControlMap.entrySet()){
				Control control = entry.getValue();
				List<CanvasMaster> rcCanvasList = control.getRelationControlCanvasList();
				for(CanvasMaster rcCanvas : rcCanvasList ){
					String canvasId = rcCanvas.getCanvasId();
					List<ControlMaster> controlMasters = canvasMasterIdToControlMasterMap.get(canvasId);
					/* Now all these controls should be added to the canvas master */
					for(ControlMaster master : controlMasters){
						ControlMaster newControlMaster = new ControlMaster();
						BeanUtils.copyProperties(master, newControlMaster);
						/* Now get the values of these two attributes */
						ControlRelationControl relationControl = control.getRcCanvasControlMasterIdToControlRCMap().get(newControlMaster.getControlId());
						if(relationControl!=null) {
							newControlMaster.setControlAttribute(relationControl.getRcCanvasControlAttribute());
							newControlMaster.setAggregateControlFunction(relationControl.getRcCanvasControlAggregateFunction());
							newControlMaster.setDeviceIndependentBlob(getMergedBlob(master.getDeviceIndependentBlob(),relationControl.getDeviceIndependentBlob()));
							newControlMaster.setDeviceDependentBlob(getMergedBlob(master.getDeviceDependentBlob(),relationControl.getDeviceDependentBlob()));
						}
						if(rcCanvas.getControlList()==null){
							List<ControlMaster> newControlMasterList = new ArrayList<>();
							rcCanvas.setControlList(newControlMasterList);
						}
						rcCanvas.getControlList().add(newControlMaster);
					}
				}
			}
		}
		/* get the property controls */
		List<PropertyControlMasterVO> propertyControlMasterVOList = layoutDAO.getPropertyControlMasterList(allCanvasIdsList);
		/* TODO add property control and actionGroup&Actions here */
		//FIXME working here..
	}
	private void getDeviceTypeSpecificData(JsonNode jsonNode, String deviceType) {
		if (jsonNode == null)
			return;
		Iterator<String> it = jsonNode.fieldNames();
		while(it.hasNext()){
			if(!it.next().startsWith(deviceType.toLowerCase())) {
				it.remove();
			}
		}
	}
	public void setLayoutDAO(LayoutDAO layoutDAO) {
		this.layoutDAO = layoutDAO;
	}
	public void setEtlDAO(EtlDAO etlDAO) {
		this.etlDAO = etlDAO;
	}
    public void setUiLayoutService(UILayoutService uiLayoutService) {
        this.uiLayoutService = uiLayoutService;
    }

	@Override
	@Transactional
	public ResponseSysEssentials getSysEssentials(RequestSysEssentials requestSysEssentials) {
		// The logged in person's details
		Person person = TenantAwareCache.getPersonRepository().getPersonDetails(TenantContextHolder.getUserContext().getPersonId());
		// Return Object
		ResponseSysEssentials response = new ResponseSysEssentials();
//        // Person's personal tags
//        List<PersonTags> personTagsList = getPersonTags(person.getPersonPkId());
//        response.setPersonTags(personTagsList);
		response.setGroupIds(person.getPersonGroups().getGroupIds());
//		 // Person's preferences
		PersonPreference personPreference = getPersonPreferences(person.getPersonPkId());
//		personPreference.setPersonTags(personTagsList);
		response.setPersonPreference(personPreference);
		// Add Wish template to person preferences
		Map<String, WishTemplate> wishTemplates = getWishTemplate(person.getPersonPreferences().getPersonPreferencesPkId());
		personPreference.setWishMessage(wishTemplates);
//        // Getting Social Status Master
        List<SocialStatus> socialStatuses = getSocialStatusMaster();
        response.setSocialStatusList(socialStatuses);
        // Getting Tenant's State Icons
		Map<String, Map<String, StateVO>> stateIcons = getStateIcons();
		response.setStateIcons(stateIcons);
		// Getting Job Relationship Type
		Map<String, JobRelationshipType> jobRelationshipTypes = getJobRelationshipTypes();
		response.setJobRelationshipTypes(jobRelationshipTypes);
//		// Getting Licenced Locales
        List<LicencedLocales> licencedLocales = getLicencedLocale();
        response.setLicencedLocales(licencedLocales);
////        // Getting APP Menu
        List<AppMainMenu> appMainMenu = getAppMainMenu(person.getPersonPkId(), requestSysEssentials.getDeviceType());
        response.setAppMainMenuList(appMainMenu);
        // Insert here get work done menu
		List<AppMainMenu> getWorkDoneMenu = getGetWorkDoneMenu(person.getPersonPkId(),requestSysEssentials.getDeviceType());
		response.setGetWorkDoneMenu(getWorkDoneMenu);
		// Insert here Social Menu
		List<AppMainMenu> socialMenu = getSocialMenu(person.getPersonPkId(),requestSysEssentials.getDeviceType());
		response.setSocialMenu(socialMenu);
        List<AppMainMenu> personMenuList = getPersonMainMenu(person.getPersonPkId(),requestSysEssentials.getDeviceType());
        response.setPersonMainMenuList(personMenuList);
        // Insert here change password menu
		List<AppMainMenu> changePasswordList = getChangePasswordMenu(person.getPersonPkId(),requestSysEssentials.getDeviceType());
		response.setChangePasswordList(changePasswordList);
		// Insert here logout menu
		List<AppMainMenu> logoutList = getLogoutMenu(person.getPersonPkId(),requestSysEssentials.getDeviceType());
		response.setLogoutList(logoutList);
		// Insert here conversation ID
		List<AppMainMenu> conversionID = getConversationID(person.getPersonPkId(),requestSysEssentials.getDeviceType());
		response.setHpConvID(conversionID.get(0).getAppMainMenuPkId());
//		// Theme
		List<Theme> themeList = getTheme(null, null);
		response.setThemeList(themeList);
//		// Tenant's UI Settings
////		UiSettings uiSettings = getUiSettings();
////		response.setUiSettings(uiSettings);
//		// Icon's file
		response.setIconSvgURL(setAttachURLfromAttachId("icomoon.svg"));
		response.setIconEotURL(setAttachURLfromAttachId("icomoon.eot"));
		response.setIconTxtURL(setAttachURLfromAttachId("icomoon.ttf"));
		response.setIconWoffURL(setAttachURLfromAttachId("icomoon.woff"));
		response.setStyleCss(setAttachURLfromAttachId("style.css"));
		setAppHomePageAndAppMainMenu(requestSysEssentials, response);
////		UnitRequestData unitRequestData = new UnitRequestData();
////		ScreenRequest screenRequest = new ScreenRequest();
////		unitRequestData.setScreen(screenRequest);
////		screenRequest.setMtPE(personPreference.getDefaultLandingMTPE());
////		screenRequest.setUiDispTypeCodeFkId(personPreference.getDefaulLandingUIDisplayType());
//
//
////		try {
////			UnitResponseData responseData = dataService.getScreen(unitRequestData);
////			response.setAppHomePageInstance(responseData);
////		} catch (IOException e) {
////			e.printStackTrace();
////		} catch (SQLException e) {
////			e.printStackTrace();
////		}
		// App Home Page Body data(cards data)
//		List<AppHomePage> appHomePageList = getAppHomePageList();
//		response.setAppHomePageList(appHomePageList);
		// App Menu Screen layout
//		String targetScreenId = uiLayoutService.getScreenId("APP_MENU_SCRN");
//		ScreenInstance appMainMenuLayout = uiLayoutService.getLayout(targetScreenId,null, requestSysEssentials.getDeviceType());
//		response.setAppMainMenuLayout(appMainMenuLayout);
		return response;
	}

	private Map<String,WishTemplate> getWishTemplate(String personPreferencesPkId) {
		Map<String,WishTemplate> wishTemplateMap = new HashMap<>();
		List<WishTemplateVO> wishTemplateVOS = layoutDAO.getwishTemplatesVO(personPreferencesPkId);
		for(WishTemplateVO wishTemplateVO : wishTemplateVOS){
			WishTemplate wishTemplate = WishTemplate.fromVO(wishTemplateVO);
			wishTemplateMap.put(wishTemplateVO.getWishTemplateType(), wishTemplate);
		}
		return wishTemplateMap;
	}


	public Map<String, JobRelationshipType> getJobRelationshipTypes() {
		Map<String, JobRelationshipType> relationshipTypes = new HashMap<>();
		List<JobRelationshipTypeVO> relationshipTypeVOS = uiLayoutService.getJobRelationshipTypes();
		for(JobRelationshipTypeVO jobRelationshipTypeVO : relationshipTypeVOS){
			JobRelationshipType jobRelationshipType = JobRelationshipType.fromVO(jobRelationshipTypeVO);
			if(StringUtil.isDefined(jobRelationshipTypeVO.getImageID())) {
				AmazonS3 amazonS3 = SystemAwareCache.getSystemRepository().getAmazonS3Client();
				jobRelationshipType.setImageID(Util.getURLforS3(jobRelationshipTypeVO.getImageID(), amazonS3));
			}
			jobRelationshipType.setIcon(Util.getIcon(jobRelationshipTypeVO.getIcon()));
			relationshipTypes.put(jobRelationshipType.getRelationshipTypeCode(),jobRelationshipType);
		}
		return relationshipTypes;
	}

	private Map<String,Map<String,StateVO>> getStateIcons() {
		Map<String,Map<String,StateVO>> iconsMap = new HashMap<>();
		// get Active State Icons
		Map<String, StateVO> activeStateIcons = uiLayoutService.getActiveStateIcons();
		iconsMap.put("ActiveState",activeStateIcons);
		Map<String, StateVO> recordStateIcons = uiLayoutService.getRecordStateIcons();
		iconsMap.put("RecordState",recordStateIcons);
		Map<String, StateVO> savedStateIcons = uiLayoutService.getSavedStateIcons();
		iconsMap.put("SavedState",savedStateIcons);
		return iconsMap;
	}

	@Override
	public void resolveAppHomePageForAllUsers() {
		List<String> personIds = uiLayoutService.getAllPersonIDs();
		// For each person ID make a call to get Screen and store it in the APP HOME PAGE CONTENT TABLE
		for(String personId : personIds){
			TenantInfo currentTenantInfo = TenantContextHolder.popCurrentTenantInfo();
			TenantInfo newTenantInfo = new TenantInfo(currentTenantInfo);
			// push a new Tenant info
		}

	}

	private void setAppHomePageAndAppMainMenu(RequestSysEssentials requestSysEssentials, ResponseSysEssentials response) {
		//TODO Getting App Home Page data and Menu screen layout
		// This request is for getting data of the dummy LOGIN screen actions
		// First we need to get the layout of dummy LOGIN screen
		String dummyLoginScrnId = uiLayoutService.getScreenId("DUMMY_LOGIN_SCRN");
		ScreenInstance dummyLoginLayout = uiLayoutService.getLayout(dummyLoginScrnId,null, requestSysEssentials.getDeviceType(), null);
		// Now we need to loop through dummy login actions and get the response from data service.
		for(ScreenSectionInstance dummyLoginSection : dummyLoginLayout.getSections()){
			for(SectionActionInstance dummyLoginAction : dummyLoginSection.getActions()){
				// Now make the request call on the basis of actions..
				ScreenRequest screenRequest = new ScreenRequest();
				screenRequest.setDeviceType(requestSysEssentials.getDeviceType());
				screenRequest.setLayoutRequired(true);
				screenRequest.setDataRequired(true);
				screenRequest.setMetaRequired(true);
				screenRequest.setSectionActionInstanceId(dummyLoginAction.getActionPkId());
				//TODO We need to set the filters on the basis of the action visual code.

				for(ActionTarget actionTarget : dummyLoginAction.getTargets()){
					screenRequest.setActionTargetPkId(actionTarget.getActionTargetPkId());
					setDummyLoginActionFilters(actionTarget, screenRequest);
				}
				try {
					UnitResponseData responseData = dataRequestService.getScreen(screenRequest);
					if(dummyLoginAction.getActionVisualMasterCode().equals("DUMMY_LOAD_HOME_PAGE")){
						response.setAppHomePageInstance(responseData);
					}else if(dummyLoginAction.getActionVisualMasterCode().equals("DUMMY_LOAD_MAIN_MENU")){
						response.setAppMainMenuInstance(responseData);
					}
				} catch (IOException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void setDummyLoginActionFilters(ActionTarget actionTarget, ScreenRequest screenRequest) {
		if(actionTarget.getTargetSectionFilterParams()!=null)
			for(TargetSectionFilterParams filterParams : actionTarget.getTargetSectionFilterParams()) {
				if (screenRequest.getPeAliasToLoadParamsMap().containsKey(filterParams.getTargetMTPEAlias())) {
					LoadParameters loadParameters = screenRequest.getPeAliasToLoadParamsMap().get(filterParams.getTargetMTPEAlias());
					// Now prepare filters from each of the above.
					if (loadParameters.getFilters().containsKey(filterParams.getAttributePathExpn())) {
						List<FilterOperatorValue> filterOperatorValues = loadParameters.getFilters().get(filterParams.getAttributePathExpn());
						prepareFinalAttributeFilterPath(filterParams, filterOperatorValues);
					} else {
						List<FilterOperatorValue> filterOperatorValues = new ArrayList<>();
						loadParameters.getFilters().put(filterParams.getAttributePathExpn(), filterOperatorValues);
						prepareFinalAttributeFilterPath(filterParams, filterOperatorValues);
					}
				} else {
					LoadParameters loadParameters = new LoadParameters();
					screenRequest.getPeAliasToLoadParamsMap().put(filterParams.getTargetMTPEAlias(), loadParameters);
					List<FilterOperatorValue> filterOperatorValues = new ArrayList<>();
					loadParameters.getFilters().put(filterParams.getAttributePathExpn(), filterOperatorValues);
					prepareFinalAttributeFilterPath(filterParams, filterOperatorValues);
				}
			}
	}

	private void prepareFinalAttributeFilterPath(TargetSectionFilterParams filterParams, List<FilterOperatorValue> filterOperatorValues) {
		String finalAttributeValue = null;
		if(filterParams.getAttributeValue()!=null) {
			if (filterParams.getAttributeValue().equals("!{Person.PrimaryKeyID}") || filterParams.getAttributeValue().equals("!{PersonEventReason.PrimaryKeyID}")) {
				finalAttributeValue = "'" + TenantContextHolder.getUserContext().getPersonId() + "'";
			} else if (filterParams.getAttributeValue().equals("!{GroupMember.Group}")) {
				Person person = TenantAwareCache.getPersonRepository().getPersonDetails(TenantContextHolder.getUserContext().getPersonId());
				PersonGroup personGroup = person.getPersonGroups();
				StringBuilder attributeValue = new StringBuilder();
				for (int i = 0; i < personGroup.getGroupIds().size(); i++) {
					if (i != 0)
						attributeValue.append(",");
					attributeValue.append("'" + personGroup.getGroupIds().get(i) + "'");
				}
				if (StringUtil.isDefined(attributeValue.toString())) {
					finalAttributeValue = attributeValue.toString();
				} else {
					finalAttributeValue = "null";
				}

			} else {
				finalAttributeValue = filterParams.getAttributeValue();
			}
		}
		FilterOperatorValue filterOperatorValue = new FilterOperatorValue(filterParams.getOperator(), finalAttributeValue);
		filterOperatorValues.add(filterOperatorValue);
	}

	private List<AppHomePage> getAppHomePageList() {
        List<AppHomePage> result = new ArrayList<>();
        List<AppHomePageVO> appHomePageVOList = layoutDAO.getAppHomePageList();
        List<AppHomePageSectionVO> appHomePageSectionVOList = layoutDAO.getAppHomePageSectionList();
        List<AppHomePageSecnContentVO> appHomePageSecnContentVOList = layoutDAO.appHomePageSecnContentVOList();
        Map<String, AppHomePage> appHomePageMap = new HashMap<>();
        Map<String, AppHomePageSection> appHomePageSectionMap = new HashMap<>();
        for(AppHomePageVO appHomePageVO : appHomePageVOList){
            AppHomePage appHomePage = AppHomePage.fromVO(appHomePageVO);
            appHomePage.setCategoryIconCodeFkId(Util.getIcon(appHomePageVO.getCategoryIconCodeFkId()));
            appHomePageMap.put(appHomePage.getAppHomePagePkId(), appHomePage);
            result.add(appHomePage);
        }
        for(AppHomePageSectionVO appHomePageSectionVO : appHomePageSectionVOList){
            AppHomePageSection appHomePageSection = AppHomePageSection.fromVO(appHomePageSectionVO);
            appHomePageMap.get(appHomePageSection.getAppHomePageFkId()).getAppHomePageSectionList().add(appHomePageSection);
            appHomePageSectionMap.put(appHomePageSection.getAppHomePageSecnPkId(), appHomePageSection);
        }
        for(AppHomePageSecnContentVO appHomePageSecnContentVO : appHomePageSecnContentVOList){
            AppHomePageSecnContent appHomePageSecnContent = AppHomePageSecnContent.fromVO(appHomePageSecnContentVO);
			if(appHomePageSecnContent.getControlContentTypeCodeFkId()!=null && (appHomePageSecnContent.getControlContentTypeCodeFkId().startsWith("IMAGE") || appHomePageSecnContent.getControlContentTypeCodeFkId().matches("ATTACHMENT|AUDIO|VIDEO"))){
				AmazonS3 amazonS3 = SystemAwareCache.getSystemRepository().getAmazonS3Client();
				appHomePageSecnContent.setControlValueURL(Util.getURLforS3(appHomePageSecnContent.getControlValueG11nBigTxt(), amazonS3));
			}
			if(appHomePageSecnContent.getControlContentTypeCodeFkId()!=null && (appHomePageSecnContent.getControlContentTypeCodeFkId().matches("ICON"))){
				appHomePageSecnContent.setControlValueIcon(Util.getIcon(appHomePageSecnContent.getControlValueG11nBigTxt()));
			}
            appHomePageSectionMap.get(appHomePageSecnContent.getAppHomePageSecnFkId()).getAppHomePageSecnContentList().add(appHomePageSecnContent);
        }
	    return result;
    }

    private Map<String, PropertyControlMaster> getPropertyControlMasterList() {
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		Map<String, PropertyControlMaster> propertyControlMasterMap = metadataService.getPropertyControlMap();
		return propertyControlMasterMap;
	}

	private List<LicencedLocales> getLicencedLocale() {
		List<LicencedLocales> licencedLocales = new ArrayList<>();
		List<LicencedLocalesVO> licencedLocalesVOS = TenantAwareCache.getMetaDataRepository().getLicencedLocaleDirectlyFromDB();
		for(LicencedLocalesVO licencedLocalesVO : licencedLocalesVOS){
			LicencedLocales licencedLocale = LicencedLocales.fromVO(licencedLocalesVO);
			licencedLocale.setLocaleName(Util.getG11nValue(licencedLocalesVO.getLocaleName(),null));
			licencedLocales.add(licencedLocale);
		}
		return licencedLocales;
	}

	private List<SocialStatus> getSocialStatusMaster() {
		List<SocialStatus> result = new ArrayList<>();
		List<SocialStatusVO> socialStatusVOList = layoutDAO.getSocialStatusVOList();
		for(SocialStatusVO statusVO : socialStatusVOList){
			SocialStatus socialStatus = SocialStatus.fromVO(statusVO);
			socialStatus.setSocialStatusIconCodeFKId(Util.getIcon(statusVO.getSocialStatusIconCodeFKId()));
			result.add(socialStatus);
		}
		return result;
	}

	private List<PersonTags> getPersonTags(String personId) {
		List<PersonTags> result = new ArrayList<>();
		List<PersonTagsVO> personTagsVOList = layoutDAO.getPersonTagsVoList(personId);
		for(PersonTagsVO personTagsVO : personTagsVOList){
			PersonTags personTags = PersonTags.fromVO(personTagsVO);
			result.add(personTags);
		}
		return result;
	}

//	private UnitResponseData getAppHomePageResponseData(RequestSysEssentials requestSysEssentials, PersonPreference personPreference) {
//        UnitRequestData unitRequestData = new UnitRequestData();
//        ScreenRequest screenRequest = new ScreenRequest();
//        screenRequest.setTargetScreenInstance(personPreference.getDefaultLandingPageTxt());
//        screenRequest.setDeviceType(requestSysEssentials.getDeviceType());
//        screenRequest.setLayoutRequired(true);
//        unitRequestData.setScreen(screenRequest);
//        UnitResponseData responseData = null;
//        try {
//             responseData = dataService.getScreen(unitRequestData);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return responseData;
//    }

//	private List<Icon> getIconList() {
//		List<Icon> iconList = new ArrayList<>();
//		List<IconVO> iconVOList = layoutDAO.getIconList();
//		for(IconVO iconVO : iconVOList){
//			Icon icon = new Icon();
//			BeanUtils.copyProperties(iconVO, icon);
//			iconList.add(icon);
//		}
//		return iconList;
//	}

	private List<ProcessHomePage> getProcessHomePageData(String personId) {
        List<ProcessHomePage> processHomePageList = new ArrayList<>();
        String processTypeCode = "APP_HOME_PAGE";
        List<ProcessHomePageVO> processHomePageVOList = layoutDAO.getProcessHomePage(processTypeCode, personId);
        Map<String, List<ProcessHomePageContentVO>> processHomePageContentVOMap = layoutDAO.getProcessHomePageContentVO(processTypeCode, personId);
        for(ProcessHomePageVO homePageVO : processHomePageVOList){
            ProcessHomePage homePage = ProcessHomePage.fromVO(homePageVO);
            IconVO cardIconCode = Util.getIcon(homePageVO.getCardIconCodeFkId());
            homePage.setCardIconCode(cardIconCode);
            IconVO categoryIconCode = Util.getIcon(homePageVO.getCategoryIconCodeFkId());
            homePage.setCategoryIconCode(categoryIconCode);
            homePage.setCardImageid(setAttachURLfromAttachId(homePageVO.getCardImageid()));
            List<ProcessHomePageContentVO> contentVOs = processHomePageContentVOMap.get(homePage.getPrcHomePagePkId());
            for(ProcessHomePageContentVO contentVO : contentVOs){
                ProcessHomePageContent content = ProcessHomePageContent.fromVO(contentVO);
                if(content.getControlType()!=null && (content.getControlType().startsWith("IMAGE") || content.getControlType().equals("ATTACHMENT"))){
                    AmazonS3 amazonS3 = SystemAwareCache.getSystemRepository().getAmazonS3Client();
                    content.setControlContentValueURL(Util.getURLforS3(content.getControlContentValueG11nBigTxt(), amazonS3));
                }
                if(homePage.getSectionToContentMap().containsKey(content.getCardSectionInstancePosInt())){
                    ProcessHomePageContentGrouper contentGrouper = homePage.getSectionToContentMap().get(content.getCardSectionInstancePosInt());
                    if(content.getRecordNumberPosInt()>0){
                        if(contentGrouper.getRecordNumToContentMap().containsKey(content.getRecordNumberPosInt())){
                            contentGrouper.getRecordNumToContentMap().get(content.getRecordNumberPosInt()).add(content);
                        }else{
                            List<ProcessHomePageContent> contents = new ArrayList<>();
                            contents.add(content);
                            contentGrouper.getRecordNumToContentMap().put(content.getRecordNumberPosInt(),contents);
                        }
                    }else{
                        //Add it to a default list
                        contentGrouper.getProcessHomePageContentList().add(content);
                    }
                }else{
                    ProcessHomePageContentGrouper contentGrouper = new ProcessHomePageContentGrouper();
                    if(content.getRecordNumberPosInt()>0){
                        if(contentGrouper.getRecordNumToContentMap().containsKey(content.getRecordNumberPosInt())){
                            contentGrouper.getRecordNumToContentMap().get(content.getRecordNumberPosInt()).add(content);
                        }else{
                            List<ProcessHomePageContent> contents = new ArrayList<>();
                            contents.add(content);
                            contentGrouper.getRecordNumToContentMap().put(content.getRecordNumberPosInt(),contents);
                        }
                    }else{
                        //Add it to a default list
                        contentGrouper.getProcessHomePageContentList().add(content);
                    }
                    homePage.getSectionToContentMap().put(content.getCardSectionInstancePosInt(),contentGrouper);
                }
            }
            processHomePageList.add(homePage);
        }

        return processHomePageList;
    }

	private AttachmentObject setAttachURLfromAttachId(String cardImageid) {
		AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
		return Util.getURLforS3(cardImageid, s3Client);
	}

	private List<ProcessHomePageFooter> getProcessHomePageFooter(String personId) {
        List<ProcessHomePageFooter> footerList = new ArrayList<>();
        String processTypeCode = "APP_HOME_PAGE";
        List<ProcessHomePageFooterVO> footerVOList = layoutDAO.getProcessHomePageLayoutFooter(processTypeCode ,personId);
        for(ProcessHomePageFooterVO footerVO : footerVOList){
            ProcessHomePageFooter footer = ProcessHomePageFooter.fromVO(footerVO);
            footerList.add(footer);
        }
        return footerList;
    }

    private List<ProcessHomePageIntroContent> getProcessHomePageIntroContent(String personId) {
        List<ProcessHomePageIntroContent> introContentList = new ArrayList<>();
        String processTypeCode = "APP_HOME_PAGE";
        List<ProcessHomePageIntroContentVO> introContentVOList = layoutDAO.getProcessHomePageIntroContent(processTypeCode, personId);
        for(ProcessHomePageIntroContentVO introContentVO : introContentVOList) {
            ProcessHomePageIntroContent introContent = ProcessHomePageIntroContent.fromVO(introContentVO);
			introContent.setIntroImageURL(setAttachURLfromAttachId(introContentVO.getIntroImageId()));
            introContentList.add(introContent);
        }
        return introContentList;
    }

    @Transactional
    public PersonPreference getPersonPreferences(String personId) {
		TenantAwareCache.getPersonRepository().getPersonIdMap().remove(personId);
		PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
		Person person = personService.getPersonDetails(personId);
		List<PersonTags> personTagsList = getPersonTags(person.getPersonPkId());
		PersonPreference personPreference = PersonPreference.fromVO(person.getPersonPreferences());
		personPreference.setPersonName(Util.getG11nValue(person.getPersonName(), null));
		personPreference.setPersonTags(personTagsList);
		personPreference.setGlobalDefalutLocaleCode(TenantAwareCache.getMetaDataRepository().getTenantDefaultLocale());
        return personPreference;
    }

    private List<AppMainMenu> getAppMainMenu(String personId, String deviceType) {
        String menuType = "MAIN_MENU";
        return  getMainMenu(personId, menuType, deviceType);
    }
    
	private List<AppMainMenu> getPersonMainMenu(String personId, String deviceType) {
	    String menuType = "PERSON_MENU";
        return  getMainMenu(personId, menuType, deviceType);
    }

	private List<AppMainMenu> getGetWorkDoneMenu(String personId, String deviceType) {
		String menuType = "GET_WORK_DONE";
		return  getMainMenu(personId, menuType, deviceType);
	}

	private List<AppMainMenu> getSocialMenu(String personId, String deviceType) {
		String menuType = "SOCIAL";
		return  getMainMenu(personId, menuType, deviceType);
	}

	private List<AppMainMenu> getChangePasswordMenu(String personPkId, String deviceType) {
		String menuType = "PERSON_MENU_CHANGE_PASSWORD";
		return  getMainMenu(personPkId, menuType, deviceType);
	}

	private List<AppMainMenu> getLogoutMenu(String personPkId, String deviceType) {
		String menuType = "PERSON_MENU_LOGOUT";
		return  getMainMenu(personPkId, menuType, deviceType);
	}

	private List<AppMainMenu> getConversationID(String personPkId, String deviceType) {
		String menuType = "HP_CNVSN";
		return  getMainMenu(personPkId, menuType, deviceType);
	}

    private List<AppMainMenu> getMainMenu(String personId, String menuType, String deviceType) {
        List<AppMainMenu> result = new ArrayList<>();
        List<AppMainMenuVO> idToMenuVOList = layoutDAO.getAppMainMenuVOMap(personId, menuType, deviceType);
        Map<String,AppMainMenu> idToMenuMap = new HashMap<>();
        for(AppMainMenuVO appMainMenuVO : idToMenuVOList){
            AppMainMenu appMainMenu = AppMainMenu.fromVO(appMainMenuVO);
            IconVO icon = Util.getIcon(appMainMenuVO.getMenuIconCodeFkId());
            appMainMenu.setMenuIconCode(icon);
            SystemAwareCache.getSystemRepository().getAmazonS3Client();
            appMainMenu.setMenuImage(Util.getURLforS3(appMainMenuVO.getMenuImage(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
            idToMenuMap.put(appMainMenu.getAppMainMenuPkId(),appMainMenu);
            // Here get the count of app main menu.
            String menuExpression = appMainMenu.getMenuExpressionBigTxt();
            if(StringUtil.isDefined(menuExpression)){
                AggregateData countValue = resolveCountValueForMenuExpression(menuExpression);
                appMainMenu.setMenuExpressionCount(countValue);

            }
        }
        for(Map.Entry<String,AppMainMenu> entry : idToMenuMap.entrySet()){
            String parentMenuId = entry.getValue().getParentMenuHcyFkId();
            if(StringUtil.isDefined(parentMenuId)){
                idToMenuMap.get(parentMenuId).getChildrenMenu().add(entry.getValue());
            }else{
                result.add(entry.getValue());
            }
        }
        return result;
    }

    /**
     * this method returns the count associated with menu. It parses an menu expression which has key label kind of listing with keys known well before.
     * @param menuExpression
     * @return
     */
    private AggregateData resolveCountValueForMenuExpression(String menuExpression) {
        String[] keyValuePairsList = menuExpression.split("\\|");
        Map<String, LoadParameters> peToLoadParams = new HashMap<>();
        Map<String,PERequest> mtPEToPERequestMap = new HashMap<>();
        PERequest request = new PERequest();
        request.setExternalDistinct(true);
        Map<String, String> keyValueMap = new HashMap<>();
        for(String keyValuePair : keyValuePairsList){
            String key = keyValuePair.substring(0, keyValuePair.indexOf(":")+1);
            key = key.substring(0, key.length()-1);
            String value = keyValuePair.substring(keyValuePair.indexOf(":")+1);
            keyValueMap.put(key, value);
            switch (key){
                case "function":
                    AttributePathDetails pathDetails = new AttributePathDetails();
                    pathDetails.setAttributePath(value);
                    request.getAttributePaths().getAttributePathToAggDetailsMap().put(value, pathDetails);
                    break;
                case "mtPE":
                    request.setMtPE(value);
                    mtPEToPERequestMap.put(value, request);
                    break;
                case "mtPEwhereClause":
                    LoadParameters loadParam = new LoadParameters();
                    loadParam.setFilterExpn(value);
                    peToLoadParams.put(keyValueMap.get("mtPE"),loadParam);
                    break;
                case "filterMtPE":
                    LoadParameters loadParameters = new LoadParameters();
                    peToLoadParams.put(value, loadParameters);
                    break;
                case "filterMtPEWhereClause":
                    peToLoadParams.get(keyValueMap.get("filterMtPE")).setFilterExpn(value);
                    break;
                case "filterMtPEforeignKey":
                    peToLoadParams.get(keyValueMap.get("filterMtPE")).setFkRelationWithParent(value.replace("=", "|"));
                    break;
            }
        }
////        UnitResponseData response = dataService.getMenuExpressionResponse(mtPEToPERequestMap, peToLoadParams);
//        if(response.getPEData().get(keyValueMap.get("mtPE"))!=null){
//           ProcessElementData peData = response.getPEData().get(keyValueMap.get("mtPE"));
//           AggregateData aggregateData = peData.getAggregateResult().get(keyValueMap.get("function"));
//            return aggregateData;
//        }
        return null;
    }

    /**
	 * @return
	 */
	private List<IconMaster> getIconMasters() {
		List<IconMaster> iconMasters = new ArrayList<>();
		List<IconMasterVO> iconMasterVOs = layoutDAO.getIconMasterList();
		for(IconMasterVO masterVO : iconMasterVOs){
			IconMaster master = IconMaster.fromVO(masterVO);
			iconMasters.add(master);
		}
		return iconMasters;
	}
	private List<ActionRepresentation> getActionRepresentationLookup(String deviceType) {
		List<ActionRepresentation> actionRepresentations = new ArrayList<>();
		List<ActionRepresentationVO> actionRepresenationVOs = layoutDAO.getActionRepresenationByBaseTemplate();
		for(ActionRepresentationVO actionRepresentationVO : actionRepresenationVOs){
			ActionRepresentation actionRepresentation = new ActionRepresentation();
			BeanUtils.copyProperties(actionRepresentationVO, actionRepresentation);
			actionRepresentation.setActionPlacement(getMergedBlob(actionRepresentation.getActionPlacement(), null));
			getDeviceTypeSpecificData(actionRepresentation.getActionPlacement(), deviceType);
			actionRepresentation.setActionRepresentationIconPlacement(getMergedBlob(actionRepresentation.getActionRepresentationIconPlacement(), null));
			getDeviceTypeSpecificData(actionRepresentation.getActionRepresentationIconPlacement(), deviceType);
			actionRepresentation.setActionIconOnlyPlacement(getMergedBlob(actionRepresentation.getActionIconOnlyPlacement(), null));
			getDeviceTypeSpecificData(actionRepresentation.getActionIconOnlyPlacement(), deviceType);
			actionRepresentation.setActionIlIconPlacement(getMergedBlob(actionRepresentation.getActionIlIconPlacement(), null));
			getDeviceTypeSpecificData(actionRepresentation.getActionIlIconPlacement(), deviceType);
			actionRepresentation.setActionIlLabelPlacement(getMergedBlob(actionRepresentation.getActionIlLabelPlacement(), null));
			getDeviceTypeSpecificData(actionRepresentation.getActionIlLabelPlacement(), deviceType);
			actionRepresentation.setActionLabelOnlyPlacement(getMergedBlob(actionRepresentation.getActionLabelOnlyPlacement(), null));
			getDeviceTypeSpecificData(actionRepresentation.getActionLabelOnlyPlacement(), deviceType);
			actionRepresentations.add(actionRepresentation);
		}
		return actionRepresentations;
	}
	private List<PropertyControlEventPatternMaster> getPropertyControlEventPatternMaster(String deviceType) {
		List<PropertyControlEventPatternMaster> propertyControlEventPatternMasters = new ArrayList<>();
		List<PropertyControlEventPatternMasterVO> propertyControlEventPatternMasterVOs = layoutDAO.getPropertyControlEventPatternMaster();
		for(PropertyControlEventPatternMasterVO propertyControlEventPatternMasterVO : propertyControlEventPatternMasterVOs){
			PropertyControlEventPatternMaster eventPatternMaster = PropertyControlEventPatternMaster.fromVO(propertyControlEventPatternMasterVO);
			eventPatternMaster.setDeviceIndependentBlob(getMergedBlob(eventPatternMaster.getDeviceIndependentBlob(),null));
			getDeviceTypeSpecificData(eventPatternMaster.getDeviceDependentBlob(), deviceType);
			propertyControlEventPatternMasters.add(eventPatternMaster);
		}
		return propertyControlEventPatternMasters;
	}
	private List<PropertyControlPatternMaster> getPropertyControlPatternMaster(String deviceType) {
		List<PropertyControlPatternMaster> propertyControlPatternMasters = new ArrayList<>();
		List<PropertyControlPatternMasterVO> propertyControlPatternMasterVOs = layoutDAO.getPropertyControlPatternMaster();
		for(PropertyControlPatternMasterVO propertyControlPatternMasterVO : propertyControlPatternMasterVOs){
			PropertyControlPatternMaster patternMaster = PropertyControlPatternMaster.fromVO(propertyControlPatternMasterVO);
			patternMaster.setDeviceDependentBlob(getMergedBlob(patternMaster.getDeviceDependentBlob(), null));
			patternMaster.setDeviceIndependentBlob(getMergedBlob(patternMaster.getDeviceIndependentBlob(), null));
			getDeviceTypeSpecificData(patternMaster.getDeviceDependentBlob(), deviceType);
			propertyControlPatternMasters.add(patternMaster);
		}
		return propertyControlPatternMasters;
	}
	private List<ActionStep> getActionStep() {
		List<ActionStepVO> actionStepVOList = layoutDAO.getActionStep();
		List<ActionStep> actionStepList = new ArrayList<>();
		for(ActionStepVO actionStepVO : actionStepVOList){
			ActionStep actionStep = ActionStep.fromVO(actionStepVO);
					List<ActionStep> asMapList = new ArrayList<>();
					asMapList.add(actionStep);
					actionStepList.add(actionStep);
			}
		return actionStepList;
	}
	private Map<String, List<ActionStep>> getActionStepMap(){
		List<ActionStep> actionStepList = getActionStep();
		Map<String, List<ActionStep>> actionCodeToActionStepMap = new HashMap<>();
		for(ActionStep actionStep : actionStepList){
			String actionCode = actionStep.getActionCode();
			if(actionCodeToActionStepMap.containsKey(actionCode)){
				actionCodeToActionStepMap.get(actionCode).add(actionStep);
			}else{
				List<ActionStep> actionSteps = new ArrayList<>();
				actionSteps.add(actionStep);
				actionCodeToActionStepMap.put(actionCode, actionSteps);
			}
		}
		return actionCodeToActionStepMap;
	}
	private Map<String,PropertyControlLookup> getPropertyControlLookup() {
		Map<String, PropertyControlLookup> propertyControlLookupMap = new HashMap<>();
		List<PropertyControlLookupVO> propertyControlLookupVOList = layoutDAO.getPropertyControlLookup();
		for(PropertyControlLookupVO propertyControlLookupVO : propertyControlLookupVOList){
			PropertyControlLookup propertyControlLookup = PropertyControlLookup.fromVO(propertyControlLookupVO);
			String sourceCategory = propertyControlLookup.getSourceCategory();
			if(sourceCategory!=null && sourceCategory.endsWith("Lookup")){
				Map<String, Object> lookupMap = getLookupMap(propertyControlLookupVO);
				propertyControlLookup.setLookupMap(lookupMap);
			}
			propertyControlLookupMap.put(propertyControlLookup.getPropertyControlCode(), propertyControlLookup);
		}
		return propertyControlLookupMap;
	}
	private List<PropertyControlLookup> getPropertyControlLookupList(Map<String,PropertyControlLookup> lookupMap){
		List<PropertyControlLookup> lookupList = new ArrayList<>();
		for(Map.Entry<String, PropertyControlLookup> entry : lookupMap.entrySet()){
			lookupList.add(entry.getValue());
		}
		return lookupList;
	}
	private Map<String, Object> getLookupMap(PropertyControlLookupVO propertyControlLookupVO) {
		Map<String, Object> lookupMap = layoutDAO.getLookupMap(propertyControlLookupVO);
		return lookupMap;
	}
	private UiSettings getUiSettings() {
		UiSettingsVO uiSettingsVO = layoutDAO.getUiSettings();
		UiSettings uiSettings = UiSettings.fromVO(uiSettingsVO);
		uiSettings.setSettings(uiSettings.getSettings());
		/* Remove UI Settings */
		return uiSettings;
	}
	private List<Theme> getTheme(String personId, String themeHeaderFkId) {
        List<Theme> themeList = new ArrayList<>();
        List<ThemeVO> themeVOList = layoutDAO.getThemeVOs(personId, themeHeaderFkId);
        for(ThemeVO themeVO : themeVOList){
            Theme theme = Theme.fromVO(themeVO);
            themeList.add(theme);
        }
		return themeList;
	}
	private List<ThemeAlpha> getThemeAlpha() {
		List<ThemeAlpha> themeAlphaList = new ArrayList<>();
		List<ThemeAlphaVO> themeAlphaVOList = layoutDAO.getThemeAlpha();
		for(ThemeAlphaVO themeAlphaVO : themeAlphaVOList){
			ThemeAlpha themeAlpha = ThemeAlpha.fromVO(themeAlphaVO);
			themeAlphaList.add(themeAlpha);
		}
		return themeAlphaList;
	}
	private List<ThemeGroupEntityMap> getThemeGroup() {
		List<ThemeGroupEntityMap> themeGroupList = new ArrayList<>();
		List<ThemeGroupEntityMapVO> themeGroupVOList = layoutDAO.getThemeGroup();
		for(ThemeGroupEntityMapVO themeGroupVO : themeGroupVOList){
			ThemeGroupEntityMap themeGroup = ThemeGroupEntityMap.fromVO(themeGroupVO);
			themeGroupList.add(themeGroup);
		}
		return themeGroupList;
	}
	private List<CanvasElementTheme> getCanvasThemeElement() {
		List<CanvasElementTheme> canvasElementThemeList = new ArrayList<>();
		List<CanvasElementThemeVO> canvasElementThemeVOList = layoutDAO.getCanvasThemeElement();
		for(CanvasElementThemeVO canvasElementThemeVO : canvasElementThemeVOList){
			CanvasElementTheme canvasElementTheme = CanvasElementTheme.fromVO(canvasElementThemeVO);
			canvasElementTheme.setElementThemeProperty(getMergedBlob(canvasElementTheme.getElementThemeProperty(), null));
			canvasElementThemeList.add(canvasElementTheme);
		}
		return canvasElementThemeList;
	}
	private List<CanvasTheme> getCanvasTheme() {
		List<CanvasTheme> canvasThemeList = new ArrayList<>();
		List<CanvasThemeVO> canvasThemeVOList = layoutDAO.getCanvasTheme();
		for(CanvasThemeVO canvasThemeVO : canvasThemeVOList){
			CanvasTheme canvasTheme = CanvasTheme.fromVO(canvasThemeVO);
			canvasTheme.setThemeProperty(getMergedBlob(canvasTheme.getThemeProperty(), null));
			canvasThemeList.add(canvasTheme);
		}
		return canvasThemeList;
	}
	@Override
	@Transactional
	public List<VisualArtefactLibraryVO> getAllTheImages(String libraryType) {
		List<VisualArtefactLibraryVO> visualArtefactLibraryList = layoutDAO.getImagesFromVisualArtefactLibrary(libraryType);
		List<VisualArtefactLibraryVO> visualArtefactLibraryVOList =new ArrayList<>();
		Map<String,Object> imageNameToS3UrlMap = new HashMap<>();

		for(VisualArtefactLibraryVO visualArtefactLibraryVO:visualArtefactLibraryList){
			if(!visualArtefactLibraryVO.isDeleted() && visualArtefactLibraryVO.getActiveStateCodeFkId().equals("ACTIVE") && visualArtefactLibraryVO.getRecordStateCodeFkId().equals("CURRENT") && visualArtefactLibraryVO.getSaveStateCodeFkId().equals("SAVED")) {
				AmazonS3 amazonS3 = SystemAwareCache.getSystemRepository().getAmazonS3Client();
				if(StringUtil.isDefined(visualArtefactLibraryVO.getArtefactImageid())) {
					AttachmentObject attachmentObject = Util.getURLforS3(visualArtefactLibraryVO.getArtefactImageid(), amazonS3);
					String imageUrl = attachmentObject.getGetUrl().toString();
					visualArtefactLibraryVO.setImageUrl(imageUrl);
				}
				visualArtefactLibraryVOList.add(visualArtefactLibraryVO);
			}
		}
		return visualArtefactLibraryVOList;
	}

	@Override
	@Transactional
	public List<Tutorial> getAllTutorials(String personId, String tutorialCatId) {
		List<Tutorial> tutorialList = layoutDAO.getPersonTutorials(personId,tutorialCatId);
		List<Tutorial> personTutorialList =new ArrayList<>();
		for(Tutorial tutorial:tutorialList){
			if(!tutorial.isDeleted() && tutorial.getActiveStateCodeFkId().equals("ACTIVE") && tutorial.getRecordStateCodeFkId().equals("CURRENT") && tutorial.getSaveStateCodeFkId().equals("SAVED")) {
				AmazonS3 amazonS3 = SystemAwareCache.getSystemRepository().getAmazonS3Client();
				if(StringUtil.isDefined(tutorial.getTutorialVideoId())) {
					AttachmentObject attachmentObject = Util.getURLforS3(tutorial.getTutorialVideoId(), amazonS3);
					String videoUrl = attachmentObject.getGetUrl().toString();
					tutorial.setTutorialVideoUrl(videoUrl);
				}
				if(StringUtil.isDefined(tutorial.getTutorialAttachId())) {
					AttachmentObject attachmentObject = Util.getURLforS3(tutorial.getTutorialAttachId(), amazonS3);
					String attachUrl = attachmentObject.getGetUrl().toString();
					tutorial.setTutorialAttachUrl(attachUrl);
				}
				personTutorialList.add(tutorial);
			}
		}
		return personTutorialList;
	}

	@Override
	@Transactional
	public Map<String, ActivityLog> getActivityLogs(ActivityLogRequest activityLogRequest) throws SQLException {
     Map<String, ActivityLog> activityLogs = layoutDAO.getActivityLog(activityLogRequest.getVersionId());
     layoutDAO.populateActivityDetails(activityLogs, activityLogRequest.getMtPE());
     return activityLogs;
	}

	@Override
	@Transactional
	public RatingLevel getRatingLevel(String ratingScaleId, double anchor) {
		RatingLevelVO ratingLevelVO = layoutDAO.getRatingLevel(ratingScaleId, anchor);
		if(ratingLevelVO == null)
			return null;
		RatingLevel ratingLevel = RatingLevel.fromVO(ratingLevelVO);
		return ratingLevel;
	}

	@Override
	@Transactional
	public NameCalculationResponse calculateName(NameCalculationRequest nameCalculationRequest) {
		String localeCode = nameCalculationRequest.getLocaleCode();
		String nameType = nameCalculationRequest.getNameType();
		Map<String, String> doaMap = nameCalculationRequest.getNameCalMap();
		NameCalculationRuleVO nameCalculationRuleVO = TenantAwareCache.getMetaDataRepository().getNameCalculationRule(localeCode, nameType);
		String formula = nameCalculationRuleVO.getFormula();
		Pattern doaPattern = Pattern.compile("\\!\\{(.*?)\\}");
		Pattern rePattern = Pattern.compile("(\\!\\{(.*?)\\})");
		Matcher doaMatcher = doaPattern.matcher(formula);
		Matcher reMatcher = rePattern.matcher(formula);
		while (doaMatcher.find() && reMatcher.find()) {
			String doaName = doaMatcher.group(1);
			String resolvedValue = doaMap.get(doaName);
			if (resolvedValue != null) {
				formula = formula.replace(reMatcher.group(1), "'" + resolvedValue + "'");
			}else {
				formula = formula.replace(reMatcher.group(1), "''");
			}
		}
		formula = "SELECT "+formula+" AS NAME;";
		String name = layoutDAO.calculateName(formula);
		NameCalculationResponse response = new NameCalculationResponse();
		response.setName(name);
		response.setNameType(nameType);
		return response;
	}

	@Override
	@Transactional
	public WorkflowActionResponse getWorkflowActions(WorkflowActionRequest workflowActionRequest) throws SQLException {
		String workflowTxnId = workflowActionRequest.getWorkflowTxnId();
		List<WorkflowActorAction> workflowActorActions = dataService.getWorkflowActionsForLoggedInUser(workflowTxnId);
		WorkflowActionResponse workflowActionResponse = new WorkflowActionResponse();
		workflowActionResponse.setWorkflowActorActions(workflowActorActions);
		return workflowActionResponse;
	}
}