package com.tapplent.apputility.uilayout.structure;

import com.tapplent.platformutility.uilayout.valueobject.ControlActionTargetFilterCriteriaVO;
import com.tapplent.platformutility.uilayout.valueobject.SectionActionTargetFilterCriteriaVO;
import org.bouncycastle.voms.VOMSAttribute;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 25/04/17.
 */
public class TargetFilterCriteria {
    private String filterCriteriaPkId;
    private String filterParameter;
    private String operator;
    private String mtPE;
    private String mtPEAlias;
    private String parentMTPEAlias;
    private String fkRelnWithParent;
    private String valueExpn;

    public String getFilterCriteriaPkId() {
        return filterCriteriaPkId;
    }

    public void setFilterCriteriaPkId(String filterCriteriaPkId) {
        this.filterCriteriaPkId = filterCriteriaPkId;
    }

    public String getFilterParameter() {
        return filterParameter;
    }

    public void setFilterParameter(String filterParameter) {
        this.filterParameter = filterParameter;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getValueExpn() {
        return valueExpn;
    }

    public void setValueExpn(String valueExpn) {
        this.valueExpn = valueExpn;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getParentMTPEAlias() {
        return parentMTPEAlias;
    }

    public void setParentMTPEAlias(String parentMTPEAlias) {
        this.parentMTPEAlias = parentMTPEAlias;
    }

    public String getFkRelnWithParent() {
        return fkRelnWithParent;
    }

    public void setFkRelnWithParent(String fkRelnWithParent) {
        this.fkRelnWithParent = fkRelnWithParent;
    }

    public static TargetFilterCriteria fromVO(ControlActionTargetFilterCriteriaVO filterCriteriaVO) {
        TargetFilterCriteria filterCriteria = new TargetFilterCriteria();
        BeanUtils.copyProperties(filterCriteriaVO, filterCriteria);
        return filterCriteria;
    }

    public static TargetFilterCriteria fromVO(SectionActionTargetFilterCriteriaVO filterCriteriaVO) {
        TargetFilterCriteria filterCriteria = new TargetFilterCriteria();
        BeanUtils.copyProperties(filterCriteriaVO, filterCriteria);
        return filterCriteria;
    }
}
