package com.tapplent.apputility.groupResolver.service;

import com.tapplent.apputility.data.service.DataRequestService;
import com.tapplent.apputility.data.structure.GenericQueryFilters;
import com.tapplent.apputility.data.structure.GenericQueryRequest;
import com.tapplent.apputility.data.structure.UnitResponseData;
import com.tapplent.apputility.groupResolver.structure.Group;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.groupresolver.dao.GroupResolverDAO;
import com.tapplent.platformutility.groupresolver.valueobject.GroupVO;
import com.tapplent.platformutility.search.builder.SqlQuery;
import com.tapplent.platformutility.uilayout.valueobject.GenericQueryFiltersVO;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 12/06/17.
 */
public class GroupResolverServiceImpl implements GroupResolverService {
    private GroupResolverDAO groupResolverDAO;
    private DataRequestService dataRequestService;
    @Transactional
    @Override
    public void resolveAllGroups() throws IOException, SQLException {
        // First we shall delete all the group member resolved entries...
        groupResolverDAO.deleteAllResolvedGroups();
        List<GroupVO> groupVOS = groupResolverDAO.getAllGroups();
        for(GroupVO groupVO : groupVOS){
            Group group = Group.fromVO(groupVO);
            GenericQueryRequest queryRequest = new GenericQueryRequest(group.getGenericQueryID(), true, false);
            UnitResponseData responseData = dataRequestService.getQueryResult(queryRequest);
            SqlQuery sqlQuery = responseData.getPEData().values().iterator().next().getSqlQuery();
            String resolverSql = sqlQuery.getSql();
            if(resolverSql.startsWith("SELECT"))
                resolverSql = resolverSql.substring(6).trim();
            if(resolverSql.startsWith("DISTINCT"))
                resolverSql = resolverSql.substring(8).trim();
            StringBuilder finalSQL = new StringBuilder("INSERT INTO T_PFM_IEU_GROUP_MEMBER (PFM_GROUP_MEMBER_PK_ID, PFM_GROUP_FK_ID,  IS_DELETED, CREATED_DATETIME, GROUP_MEMBER_PERSON_FK_ID)");
            finalSQL.append("SELECT ordered_uuid(UUID()),").append(group.getGroupID()).append(",0,NOW(),");
            if(resolverSql.contains("LIMIT ? OFFSET ?")) {
                resolverSql = resolverSql.substring(0, resolverSql.indexOf("LIMIT ? OFFSET ?"));
                sqlQuery.getParameters().removeLast();
                sqlQuery.getParameters().removeLast();
            }
            finalSQL.append(resolverSql);
            groupResolverDAO.executeUpdate(finalSQL.toString(), sqlQuery.getParameters());
//            StringBuilder finalSQL = new StringBuilder(sqlQuery.getSql());
            // now remove "SELECT DISTINCT"
            //
            // Now modify this SQL string according to our requirements
            // Now for each group resolve the generic query.
            // get the string append the remaining group members parameters

        }
    }

    @Override
    public void resolveGroupByMtPE(String mtPE) throws IOException, SQLException {
        List<GroupVO> groupVOs = TenantAwareCache.getPersonRepository().getGroupList();
        List<GroupVO> groupsByMtPE = getGroupsbyMtPE(groupVOs, mtPE);
        if (!groupsByMtPE.isEmpty()) {
            groupResolverDAO.deleteAllResolvedGroupsByMtPE(groupsByMtPE);
            for (GroupVO groupVO : groupsByMtPE) {
                Group group = Group.fromVO(groupVO);
                GenericQueryRequest queryRequest = new GenericQueryRequest(group.getGenericQueryID(), true, false);
                UnitResponseData responseData = dataRequestService.getQueryResult(queryRequest);
                SqlQuery sqlQuery = responseData.getPEData().values().iterator().next().getSqlQuery();
                String resolverSql = sqlQuery.getSql();
                if (resolverSql.startsWith("SELECT"))
                    resolverSql = resolverSql.substring(6).trim();
                if (resolverSql.startsWith("DISTINCT"))
                    resolverSql = resolverSql.substring(8).trim();
                StringBuilder finalSQL = new StringBuilder("INSERT INTO T_PFM_IEU_GROUP_MEMBER (PFM_GROUP_MEMBER_PK_ID, PFM_GROUP_FK_ID,  IS_DELETED, CREATED_DATETIME, GROUP_MEMBER_PERSON_FK_ID)");
                finalSQL.append("SELECT ordered_uuid(UUID()),").append(group.getGroupID()).append(",0,NOW(),");
                if(resolverSql.contains("LIMIT ? OFFSET ?")) {
                    resolverSql = resolverSql.substring(0, resolverSql.indexOf("LIMIT ? OFFSET ?"));
                    sqlQuery.getParameters().removeLast();
                    sqlQuery.getParameters().removeLast();
                }
                finalSQL.append(resolverSql);
                groupResolverDAO.executeUpdate(finalSQL.toString(), sqlQuery.getParameters());
            }
        }
    }

    private List<GroupVO> getGroupsbyMtPE(List<GroupVO> groupVOs, String mtPE) {
        List<GroupVO> groupsByMtPE = new ArrayList<>();
        for (GroupVO groupVO : groupVOs){
            for (GenericQueryFiltersVO genericQueryFiltersVO : groupVO.getGenericQueryVO().getFilters()){
                if (genericQueryFiltersVO.getTargetMtPE().equals(mtPE)) {
                    groupsByMtPE.add(groupVO);
                    break;
                }
            }
        }
        return groupsByMtPE;
    }

    public void setGroupResolverDAO(GroupResolverDAO groupResolverDAO) {
        this.groupResolverDAO = groupResolverDAO;
    }

    public void setDataRequestService(DataRequestService dataRequestService) {
        this.dataRequestService = dataRequestService;
    }
}
