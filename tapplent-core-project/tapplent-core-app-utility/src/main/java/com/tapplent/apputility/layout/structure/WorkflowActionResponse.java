package com.tapplent.apputility.layout.structure;

import com.tapplent.platformutility.workflow.structure.WorkflowActorAction;

import java.util.List;

public class WorkflowActionResponse {
    private List<WorkflowActorAction> workflowActorActions;

    public List<WorkflowActorAction> getWorkflowActorActions() {
        return workflowActorActions;
    }

    public void setWorkflowActorActions(List<WorkflowActorAction> workflowActorActions) {
        this.workflowActorActions = workflowActorActions;
    }
}
