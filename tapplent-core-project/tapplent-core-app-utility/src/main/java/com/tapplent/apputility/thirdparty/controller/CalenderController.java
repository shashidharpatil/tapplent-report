package com.tapplent.apputility.thirdparty.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tapplent.apputility.thirdparty.service.GoogleCalenderService;
import com.tapplent.apputility.thirdparty.service.GoogleTaskService;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;

@Controller
@RequestMapping("testintegration")
public class CalenderController {
	@SuppressWarnings("unused")
	private static final Logger LOG = LogFactory.getLogger(CalenderController.class);
	private GoogleCalenderService googleCalenderService;
	private GoogleTaskService googleTaskService;

	
	
	@RequestMapping(value={"/v1/t/{tenantId}/u/{personId}/e"}, method=RequestMethod.GET)
	public void testIntegration() throws Exception{
		googleCalenderService.calenderSynch("4B2B269A4F3E1BC19B3B7D103FD06A01");
	}



	public GoogleTaskService getGoogleTaskService() {
		return googleTaskService;
	}



	public void setGoogleTaskService(GoogleTaskService googleTaskService) {
		this.googleTaskService = googleTaskService;
	}



	public GoogleCalenderService getGoogleCalenderService() {
		return googleCalenderService;
	}



	public void setGoogleCalenderService(GoogleCalenderService googleCalenderService) {
		this.googleCalenderService = googleCalenderService;
	}
	
}
