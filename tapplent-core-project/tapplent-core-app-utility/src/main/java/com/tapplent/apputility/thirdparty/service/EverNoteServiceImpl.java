
package com.tapplent.apputility.thirdparty.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import java.io.InputStream;

import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.springframework.transaction.annotation.Transactional;
import com.evernote.auth.EvernoteAuth;
import com.evernote.auth.EvernoteService;
import com.evernote.clients.ClientFactory;
import com.evernote.clients.NoteStoreClient;
import com.evernote.clients.UserStoreClient;
import com.evernote.edam.error.EDAMErrorCode;
import com.evernote.edam.error.EDAMNotFoundException;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.edam.notestore.NoteFilter;
import com.evernote.edam.notestore.NoteMetadata;
import com.evernote.edam.notestore.NotesMetadataList;
import com.evernote.edam.notestore.NotesMetadataResultSpec;
import com.evernote.edam.notestore.SyncState;
import com.evernote.edam.type.Data;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.NoteAttributes;
import com.evernote.edam.type.Notebook;
import com.evernote.edam.type.Resource;
import com.evernote.edam.type.ResourceAttributes;
import com.evernote.thrift.TException;
import com.evernote.thrift.transport.TTransportException;
import com.tapplent.apputility.data.service.ServerRuleManager;
import com.tapplent.apputility.data.structure.PEData;
import com.tapplent.apputility.data.structure.RequestData;
import com.tapplent.apputility.data.structure.UnitRequestData;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.thirdPartyApp.gmail.EventAttchment;
import com.tapplent.platformutility.thirdPartyApp.gmail.EverNoteDAO;
import com.tapplent.platformutility.thirdPartyApp.gmail.Meeting;
import com.tapplent.platformutility.thirdPartyApp.gmail.MeetingIntegration;
import com.tapplent.platformutility.thirdPartyApp.gmail.MeetingInvitee;
import com.tapplent.platformutility.thirdPartyApp.gmail.NoteSubTask;
import com.tapplent.platformutility.thirdPartyApp.gmail.SingleNote;
import com.tapplent.platformutility.thirdPartyApp.gmail.ThirdPartyUtil;

public class EverNoteServiceImpl implements EverNoteService {
	private static UserStoreClient userStore;
	private static NoteStoreClient noteStore;
	private static String newNoteGuid;
	private EverNoteDAO evernoteDAO;
	private ServerRuleManager serverRuleManager;

	public void setServerRuleManager(ServerRuleManager serverRuleManager) {
		this.serverRuleManager = serverRuleManager;

	}

	public void setEvernoteDAO(EverNoteDAO evernoteDAO) {
		this.evernoteDAO = evernoteDAO;
	}

	public static void Init(String token) {
		// Set up the UserStore client and check that we can speak to the server
		EvernoteAuth evernoteAuth = new EvernoteAuth(EvernoteService.PRODUCTION, token);
		ClientFactory factory = new ClientFactory(evernoteAuth);
		try {
			userStore = factory.createUserStoreClient();
		} catch (TTransportException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		boolean versionOk = false;
		try {
			versionOk = userStore.checkVersion("Evernote EDAMDemo (Java)",
					com.evernote.edam.userstore.Constants.EDAM_VERSION_MAJOR,
					com.evernote.edam.userstore.Constants.EDAM_VERSION_MINOR);
		} catch (TException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (!versionOk) {
			System.err.println("Incompatible Evernote client protocol version");
			System.exit(1);
		}

		// Set up the NoteStore client
		try {
			noteStore = factory.createNoteStoreClient();
		} catch (EDAMUserException e) {
			EDAMErrorCode errorCode = e.getErrorCode();
			if(errorCode.equals("AUTH_EXPIRED")){
				//The token is expired ..Now re-authenticate the user to get new accessToken 
			}
			e.printStackTrace();
		} catch (EDAMSystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// this function will create note with simple text in the default notebook
	public String createNote(SingleNote note, String accesstoken, String tapplentNoteId, String personPkId)throws Exception {

		Init(accesstoken);

		String noteTitle = note.getNotetTitleG11NBigTxt();
		String reminderLoc = note.getReminderLoclatlng();
		String address = ThirdPartyUtil.getTheAddressFromCoordinates(reminderLoc);
		String mapUrl = "http://www.google.com/maps/place/" + reminderLoc + "";
		String noteDesc = note.getNoteDescG11NBigTxt();
		Timestamp reminderDateTime = note.getReminderDateTime();

		List<EventAttchment> attachments = evernoteDAO.getNoteAttachments(tapplentNoteId);
		List<NoteSubTask> subTasks = evernoteDAO.getNoteSubTasks(tapplentNoteId, personPkId);
		String nBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		nBody += "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">";
		nBody += "<en-note>";
		if (!noteDesc.contains("#tapplent")) {
			nBody += "<p style=\"color:blue;\">#tapplent</p>";
		}
		nBody += "<div>" + noteDesc + "</div>";
		nBody += "<br/>";

		Note note1 = new Note();
		NoteAttributes noteAtt = new NoteAttributes();
		if (reminderDateTime != null) {
			long unixTime = reminderDateTime.getTime() / 1000L;
			noteAtt.setReminderTime(unixTime);
		}
		note1.setAttributes(noteAtt);

		note1.setTitle(noteTitle);
		// note1.getAttributes().setReminderTime(aa);
		if (subTasks != null && !subTasks.isEmpty()) {
			for (NoteSubTask task : subTasks) {
				if (task.isComplete()) {
					nBody += "<en-todo checked=\"true\"/>" + task.getSubTaskDescG11nBigTxt() + "";
				} else {
					nBody += "<en-todo/>" + task.getSubTaskDescG11nBigTxt() + "";
				}

				nBody += "<br/>";
			}
		}
		nBody += "<br/>";
		nBody += "<div>";
		nBody += "<a href=\"" + mapUrl + "\">" + address + "</a>";
		nBody += "</div>";
		nBody += "<br/>";
		for (EventAttchment attachment : attachments) {

			Map<String, Object> map = ThirdPartyUtil.getAttchment(attachment.getArtefactAttach());
			InputStream inputStream = (InputStream) map.get("InputStream");
			String contentType = (String) map.get("ContentType");
			ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
			byte[] block = new byte[10240];
			int len;
			while ((len = inputStream.read(block)) >= 0) {
				byteOut.write(block, 0, len);
			}
			inputStream.close();
			byte[] body = byteOut.toByteArray();

			// Create a new Data object to contain the file contents
			Data data = new Data();
			data.setSize(body.length);
			data.setBodyHash(MessageDigest.getInstance("MD5").digest(body));
			data.setBody(body);

			Resource resource = new Resource();
			resource.setData(data);
			resource.setMime(contentType);
			resource.setHeight((short) 801);
			resource.setWidth((short) 1982);

			ResourceAttributes attributes = new ResourceAttributes();
			attributes.setAttachment(true);
			attributes.setFileName(attachment.getAttachmentNameTxt());
			resource.setAttributes(attributes);
			note1.addToResources(resource);

			String hashHex = ThirdPartyUtil.bytesToHex(resource.getData().getBodyHash());

			nBody += "<en-media type=\"" + contentType + "\" hash=\"" + hashHex + "\"/>";

		}

		nBody += "</en-note>";

		Notebook parentNotebook = new Notebook();
		note1.setContent(nBody);

		// parentNotebook is optional; if omitted, default notebook is used
		if (parentNotebook != null && parentNotebook.isSetGuid()) {
			note1.setNotebookGuid(parentNotebook.getGuid());
		}

		// Attempt to create note in Evernote account
		Note createdNote = null;
		createdNote = noteStore.createNote(note1);
		newNoteGuid = createdNote.getGuid();
		return newNoteGuid;

	}

	private void updateEverNote(String everNoteId, SingleNote tapplentNote, String personId, String accesstoken)
			throws Exception {
		// First delete the note from the Evernote...
		deleteTheEverNote(everNoteId, accesstoken,tapplentNote.getNotePkId());
		// Then create new Note in the everNote
		String newEverNoteId = createNote(tapplentNote, accesstoken, tapplentNote.getNotePkId(), personId);
		//Now update the note integration table with the newly created evernoteId
		//First get the lastmodifiedDateTime of the integrated note from the noteIntegration table
		Map<String, String> intgInfo = evernoteDAO.getIntegrationInfo(everNoteId, "T_PFM_TAP_NOTE_INTGN",tapplentNote.getNotePkId());
		PEData child2PEData = new PEData();
		child2PEData.setMtPE("NoteIntegration");
		child2PEData.setActionCode("ADD_EDIT");
		Map<String, Object> columnNameToValueMap = new LinkedHashMap<String, Object>();
		columnNameToValueMap.put("NoteIntegration.PrimaryKeyID",intgInfo.get("IntgPkId"));
		columnNameToValueMap.put("NoteIntegration.IntegrationAppReferenceID",newEverNoteId);
		columnNameToValueMap.put("NoteIntegration.LastModifiedDateTime",intgInfo.get("LastModifiedTime"));

		child2PEData.setData(columnNameToValueMap);
		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(child2PEData);
		List<UnitRequestData> requestDataList1 = new ArrayList<>();
		requestDataList1.add(finalRequestData);
		RequestData request = new RequestData();
		request.setActionCodeFkId("ADD_EDIT");
		request.setRequestDataList(requestDataList1);
		try {

			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	@Transactional
	public void everNoteSync(String personPkId) throws Exception {

		// Step1:Fetch the accessToken of EverNoteUser using the PersonPkID

		Map<String, Object> columnNameToValueMap1 = evernoteDAO.isSynchNeeded(personPkId, "T_PFM_TAP_NOTE_SYNCH","NOTE_SYNCH_PK_ID");
		String lastSynchToken = (String) columnNameToValueMap1.get("lastSyncId");
		String integrationAppFkId = (String) columnNameToValueMap1.get("integrationAppName");
		String noteSyncPkId = (String) columnNameToValueMap1.get("syncPkId");
		String lastModifiedDateTime = (String) columnNameToValueMap1.get("lastModifiedDateTime");
		Map<String, String> tokens = evernoteDAO.getToken(integrationAppFkId, personPkId);
		String accesstoken = tokens.get("accessToken");

		Init(accesstoken);

		// suppose str becomes null after some operation(s).
		int latestUpdateCount = 0;
		try {
			if (lastSynchToken != null)
				latestUpdateCount = Integer.parseInt(lastSynchToken);
		} catch (NumberFormatException e) {
			latestUpdateCount = 0;
		}

		// Each time you want to check for new and updated notes...
		SyncState currentState = noteStore.getSyncState();
		int currentUpdateCount = currentState.getUpdateCount();
		
			if (currentUpdateCount > latestUpdateCount) {

				//Something in the account has changed, so search for notes
				NoteFilter filter = new NoteFilter();
				filter.setWords("#tapplent");
				int offset = 0;
				int pageSize = 100;
				NotesMetadataList notes = null;
				NotesMetadataResultSpec spec = new NotesMetadataResultSpec();
				spec.setIncludeUpdated(true);
				spec.setIncludeUpdateSequenceNum(true);
				spec.setIncludeCreated(true);
				notes = noteStore.findNotesMetadata(filter, offset, pageSize, spec);

				for (NoteMetadata evernote : notes.getNotes()) {
					int seqNum = evernote.getUpdateSequenceNum();
					// Retrieve the tapplentNote using the everNoteId
					Note fullEverNote = noteStore.getNote(evernote.getGuid(), true, true, false, false);

					if (seqNum > latestUpdateCount) {

						String tapplentNoteId = evernoteDAO.getEventFkIdFromIntegration(evernote.getGuid(),
								"T_PFM_TAP_NOTE_INTGN", "NOTE_FK_ID");

						if (tapplentNoteId != null && !tapplentNoteId.isEmpty()) {
							String mtPE = "Note";
							EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository()
									.getMetadataByProcessElement(null, mtPE, null);
							Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX
									.getAttributeColumnNameMap();
							SingleNote tapplentNote = evernoteDAO.getNote(tapplentNoteId, attributeMetaMap, personPkId);
							if (evernote.isSetDeleted()) {
								deleteTheTapplentnote(tapplentNoteId, personPkId);

							} else if (tapplentNote.isDeleted()) {
								deleteTheEverNote(evernote.getGuid(), accesstoken,tapplentNoteId);
							} else {

								// Retrieve the lastModifiedtime of note in tapplent and everNote
								Timestamp tapplentNoteLastModifiedTime = tapplentNote.getLastModifiedDateTime();
								Timestamp everNoteLastModifiedTime = new Timestamp(fullEverNote.getUpdated());
								if (tapplentNoteLastModifiedTime.after(everNoteLastModifiedTime)) {
									// Update the tapplent Note record onto the everNote
									updateEverNote(evernote.getGuid(), tapplentNote, personPkId, accesstoken);

								} else if (everNoteLastModifiedTime.after(tapplentNoteLastModifiedTime)) {
									// Update the everNote record onto Tapplent Note record
									updateTheTapplentNote(tapplentNote, fullEverNote,personPkId);

								}
							}

						} else {// Insert the EverNote into Tapplent...So that u
							// have to insert into Note,NoteAttachment and
							// Note Integration
							insertIntoTapplent(personPkId, fullEverNote, integrationAppFkId);

						}

					}
				}
				SyncState currentSyncState = noteStore.getSyncState();
				latestUpdateCount = currentSyncState.getUpdateCount();
				updateLastSyncToken(Integer.toString(latestUpdateCount), noteSyncPkId, lastModifiedDateTime);
			}
				
			// Is there any changes in tapplent

				boolean isSyncNeeded = (boolean) columnNameToValueMap1.get("isSynchNeeded");
				// Use this note Sync id to retrieve list of meeting Ids
				// which have to be fetched

				if (isSyncNeeded) {

					List<String> notePkId1 = evernoteDAO.getEventPkId(noteSyncPkId, "T_PFM_TAP_NOTE_SYNCH_DTLS",
							"NOTE_FK_ID", "NOTE_SYNCH_FK_ID");

					for (String tapplentNoteId : notePkId1) {
						// Check wheather this note has external reference text
						// value as null or not if so new note else update
						// note...
						String mtPE = "Note";
						EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository()
								.getMetadataByProcessElement(null, mtPE, null);
						Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX
								.getAttributeColumnNameMap();
						SingleNote tapplentNote = evernoteDAO.getNote(tapplentNoteId, attributeMetaMap, personPkId);

						String everNoteId = evernoteDAO.getThirdPartyEventId(tapplentNoteId, "T_PFM_TAP_NOTE_INTGN",
								"EVERNOTE", "NOTE_FK_ID");
						if (everNoteId != null && !everNoteId.isEmpty()) {
							Note fullEverNote = noteStore.getNote(everNoteId, true, true, false, false);
							Timestamp tapplentNoteLastModifiedTime = tapplentNote.getLastModifiedDateTime();
							Timestamp everNoteLastModifiedTime = new Timestamp(fullEverNote.getUpdated());
							if (tapplentNoteLastModifiedTime.after(everNoteLastModifiedTime)) {
								// Update the tapplent Note record onto the EverNote
								updateEverNote(everNoteId, tapplentNote, personPkId, accesstoken);
							} else if (everNoteLastModifiedTime.after(tapplentNoteLastModifiedTime)) {
								// Update the ever Note record onto Tapplent Note...
								updateTheTapplentNote(tapplentNote, fullEverNote,personPkId);
							}
						} else {
							// create New everNote and populate Integration
							// table and update synch details table
							String neweverNoteId = createNote(tapplentNote, accesstoken, tapplentNoteId, personPkId);
							PEData child1PEData = new PEData();
							child1PEData.setMtPE("NoteIntegration");

							child1PEData.setActionCode("ADD_EDIT");
							Map<String, Object> columnNameToValueMap2 = new LinkedHashMap<String, Object>();
							columnNameToValueMap2.put("NoteIntegration.NoteID", tapplentNoteId);
							columnNameToValueMap2.put("NoteIntegration.IntegrationApp", "EVERNOTE");
							columnNameToValueMap2.put("NoteIntegration.IntegrationAppReferenceID", neweverNoteId);

							child1PEData.setData(columnNameToValueMap2);
							UnitRequestData finalRequestData = new UnitRequestData();
							finalRequestData.setRootPEData(child1PEData);
							List<UnitRequestData> requestDataList = new ArrayList<>();
							requestDataList.add(finalRequestData);
							RequestData request = new RequestData();
							request.setActionCodeFkId("ADD_EDIT");
							request.setRequestDataList(requestDataList);
							try {

								serverRuleManager.applyServerRulesOnRequest(request);
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}
						// Update Synch details table
						PEData child2PEData = new PEData();
						child2PEData.setMtPE("NoteSynchDetails");
						child2PEData.setActionCode("ADD_EDIT");
						Map<String, Object> columnNameToValueMap3 = new LinkedHashMap<String, Object>();
						Map<String, Object> synchDetailsPkId = evernoteDAO.getSynchDetailsPkId("T_PFM_TAP_NOTE_SYNCH_DTLS", "NOTE_FK_ID", tapplentNoteId, "NOTE_SYNCH_DTLS_PK_ID");
						columnNameToValueMap3.put("NoteSynchDetails.PrimaryKeyID",synchDetailsPkId.get("synchDetailsPkId"));
						columnNameToValueMap3.put("NoteSynchDetails.SynchComplete", true);
						columnNameToValueMap3.put("NoteSynchDetails.LastModifiedDateTime",synchDetailsPkId.get("lastModifiedDateTime").toString());

						child2PEData.setData(columnNameToValueMap3);
						UnitRequestData finalRequestData1 = new UnitRequestData();
						finalRequestData1.setRootPEData(child2PEData);
						List<UnitRequestData> requestDataList1 = new ArrayList<>();
						requestDataList1.add(finalRequestData1);
						RequestData request1 = new RequestData();
						request1.setActionCodeFkId("ADD_EDIT");
						request1.setRequestDataList(requestDataList1);
						try {

							serverRuleManager.applyServerRulesOnRequest(request1);
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
					// Keep track of the new high-water mark
					SyncState currentSyncState = noteStore.getSyncState();
					latestUpdateCount = currentSyncState.getUpdateCount();
					updateLastSyncToken(Integer.toString(latestUpdateCount), noteSyncPkId, lastModifiedDateTime);
					// Store this on to the database
				}

			}


	private void insertIntoTapplent(String personId, Note fullEverNote, String integrationAppFkId) {

		Map<String, Object> columnNameToValueMap = new LinkedHashMap<String, Object>();
		Map<String, String> personInfo = evernoteDAO.getPersonInfo(personId);
		columnNameToValueMap.put("Note.NoteTitle",Util.getG11nString(personInfo.get("locale"), fullEverNote.getTitle()));
		Document doc = Jsoup.parse(fullEverNote.getContent());
		Element noteDescElement = doc.select("en-note").first();
		Element noteDESC = noteDescElement.parent();
		String finalNoteDesc = noteDESC.getElementsByTag("div").first().text();

		Elements noteSubTasks = doc.getElementsByTag("en-todo");

		columnNameToValueMap.put("Note.NoteDescription", Util.getG11nString(personInfo.get("locale"), finalNoteDesc));
		columnNameToValueMap.put("Note.ReminderDateTime",
				new Timestamp(fullEverNote.getAttributes().getReminderTime()).toString());
		// columnNameToValueMap.put("Note.ReminderLocation",
		// fullEverNote.getAttributes().getPlaceName());
		PEData rootPEData = new PEData();
		rootPEData.setMtPE("Note");
		rootPEData.setActionCode("ADD_EDIT");
		rootPEData.setData(columnNameToValueMap);
		List<PEData> childrenPEData = new ArrayList<PEData>();

		List<Resource> attachments = fullEverNote.getResources();
		if (attachments != null && !attachments.isEmpty()) {
			for (Resource attachment : attachments) {

				PEData child1PEData = new PEData();

				child1PEData.setActionCode("ADD_EDIT");
				child1PEData.setMtPE("NoteAttachment");
				Map<String, Object> columnNameToValueMap2 = new LinkedHashMap<String, Object>();
				String attachmentType = attachment.getMime();
				InputStream is1 = new ByteArrayInputStream(attachment.getData().getBody());
				String attachmentId1 = Util.uploadAttachmentToS3(attachmentType, is1);
				columnNameToValueMap2.put("NoteAttachment.AttachmentArtefactID", attachmentId1);
				columnNameToValueMap2.put("NoteAttachment.AttachmentName", attachment.getAttributes().getFileName());

				if (attachment.getAttributes().getFileName().contains(".jpg")
						|| attachment.getAttributes().getFileName().contains(".png"))
					columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode", "IMAGE");
				if (attachment.getAttributes().getFileName().contains(".mp4")
						|| attachment.getAttributes().getFileName().contains(".flv"))
					columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode", "VIDEO");
				else
					columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode", "DOCUMENT");
				child1PEData.setData(columnNameToValueMap2);
				childrenPEData.add(child1PEData);
			}
		}
		if(noteSubTasks !=null && !noteSubTasks.isEmpty()){
			for (Element noteSubTask : noteSubTasks) {

				PEData child1PEData = new PEData();

				Element parent = noteSubTask.parent();
				String desc = "";
				for (Node child : parent.childNodes()) {
					if (child instanceof TextNode) {
						desc = ((TextNode) child).text();
					}
				}

				child1PEData.setActionCode("ADD_EDIT");
				child1PEData.setMtPE("NoteSubTask");

				Map<String, Object> columnNameToValueMap2 = new LinkedHashMap<String, Object>();

				columnNameToValueMap2.put("NoteSubTask.SubTaskDescription",
						Util.getG11nString(personInfo.get("locale"), desc));
				if (noteSubTask.hasAttr("checked"))
					columnNameToValueMap2.put("NoteSubTask.Complete", true);
				else
					columnNameToValueMap2.put("NoteSubTask.Complete", false);

				child1PEData.setData(columnNameToValueMap2);
				childrenPEData.add(child1PEData);
			}
		}
		
		PEData child2PEData = new PEData();
		child2PEData.setActionCode("ADD_EDIT");
		child2PEData.setMtPE("NoteIntegration");
		Map<String, Object> columnNameToValueMap2 = new LinkedHashMap<String, Object>();
		columnNameToValueMap2.put("NoteIntegration.IntegrationApp", integrationAppFkId);
		columnNameToValueMap2.put("NoteIntegration.IntegrationAppReferenceID", fullEverNote.getGuid());
		child2PEData.setData(columnNameToValueMap2);
		childrenPEData.add(child2PEData);

		rootPEData.setChildrenPEData(childrenPEData);

		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(rootPEData);
		List<UnitRequestData> requestDataList = new ArrayList<>();
		requestDataList.add(finalRequestData);
		RequestData request = new RequestData();
		request.setActionCodeFkId("ADD_EDIT");

		request.setRequestDataList(requestDataList);
		try {
			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void deleteTheTapplentnote(String tapplentNoteId, String personId) {
		String mtPE = "Note";
		EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null,
				mtPE, null);
		Map<String, EntityAttributeMetadata> attributeMetaMap = entityMetadataVOX.getAttributeColumnNameMap();
		Meeting tapplentMeeting = evernoteDAO.getMeeting(tapplentNoteId, attributeMetaMap, personId);

		// First delete all the child table entries
		List<EventAttchment> attachments = evernoteDAO.getMeetingAttachments(tapplentNoteId);
		if (attachments != null && !attachments.isEmpty()) {
			for (EventAttchment attachment : attachments) {
				String attachmentId = attachment.getAttachmentPkId();
				PEData child1PEData = new PEData();
				child1PEData.setMtPE("NoteAttachment");
				child1PEData.setActionCode("ADD_EDIT");
				Map<String, Object> columnNameToValueMap1 = new LinkedHashMap<String, Object>();
				columnNameToValueMap1.put("NoteAttachment.PrimaryKeyID", attachmentId);
				columnNameToValueMap1.put("NoteAttachment.Deleted", true);
				columnNameToValueMap1.put("NoteAttachment.LastModifiedDateTime",
						attachment.getLastModifiedDatetime().toString());
				child1PEData.setData(columnNameToValueMap1);
				UnitRequestData finalRequestData = new UnitRequestData();
				finalRequestData.setRootPEData(child1PEData);
				List<UnitRequestData> requestDataList = new ArrayList<>();
				requestDataList.add(finalRequestData);
				RequestData request = new RequestData();
				request.setActionCodeFkId("ADD_EDIT");
				request.setRequestDataList(requestDataList);
				try {

					serverRuleManager.applyServerRulesOnRequest(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		//Delete all the noteSubTasks and update with subtasks which are obtained from the everNote
		List<NoteSubTask> subTasks = evernoteDAO.getNoteSubTasks(tapplentNoteId, personId);
		if (subTasks != null && !subTasks.isEmpty()) {
			for (NoteSubTask subTask : subTasks) {
				String subTaskPkId = subTask.getNoteSubTaskPkId();
				PEData child1PEData = new PEData();
				child1PEData.setMtPE("NoteSubTask");
				child1PEData.setActionCode("ADD_EDIT");
				Map<String, Object> columnNameToValueMap1 = new LinkedHashMap<String, Object>();
				columnNameToValueMap1.put("NoteSubTask.PrimaryKeyID", subTaskPkId);
				columnNameToValueMap1.put("NoteSubTask.Deleted", true);
				columnNameToValueMap1.put("NoteSubTask.LastModifiedDateTime",
						subTask.getLastModifiedDatetime().toString());
				child1PEData.setData(columnNameToValueMap1);
				UnitRequestData finalRequestData = new UnitRequestData();
				finalRequestData.setRootPEData(child1PEData);
				List<UnitRequestData> requestDataList = new ArrayList<>();
				requestDataList.add(finalRequestData);
				RequestData request = new RequestData();
				request.setActionCodeFkId("ADD_EDIT");
				request.setRequestDataList(requestDataList);
				try {

					serverRuleManager.applyServerRulesOnRequest(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		// Delete the Note Integration table entry..
		MeetingIntegration noteIntg = evernoteDAO.getIntegrationPkId("T_PFM_TAP_NOTE_INTGN", "NOTE_FK_ID",
				"NOTE_INTGN_PK_ID", tapplentNoteId);

		if (noteIntg != null && !noteIntg.equals("")) {
			String noteIntgPkId = noteIntg.getMtgIntgnPkId();
			PEData child2PEData = new PEData();
			child2PEData.setMtPE("NoteIntegration");
			child2PEData.setActionCode("ADD_EDIT");
			Map<String, Object> columnNameToValueMap1 = new LinkedHashMap<String, Object>();
			columnNameToValueMap1.put("NoteIntegration.NoteID", noteIntgPkId);
			columnNameToValueMap1.put("NoteIntegration.Deleted", true);
			columnNameToValueMap1.put("NoteIntegration.LastModifiedDateTime",
					noteIntg.getLastModifiedDatetime().toString());
			UnitRequestData finalRequestData = new UnitRequestData();
			finalRequestData.setRootPEData(child2PEData);
			List<UnitRequestData> requestDataList = new ArrayList<>();
			requestDataList.add(finalRequestData);
			RequestData request = new RequestData();
			request.setActionCodeFkId("ADD_EDIT");
			request.setRequestDataList(requestDataList);
			try {
				serverRuleManager.applyServerRulesOnRequest(request);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Delete the parent table Entries...
		Map<String, Object> columnNameToValueMap = new LinkedHashMap<String, Object>();
		columnNameToValueMap.put("Note.PrimaryKeyID", tapplentNoteId);
		columnNameToValueMap.put("Note.Deleted", true);
		columnNameToValueMap.put("Note.LastModifiedDateTime", tapplentMeeting.getLastModifiedDateTime().toString());

		PEData rootPEData = new PEData();
		rootPEData.setMtPE("Note");
		rootPEData.setActionCode("ADD_EDIT");
		rootPEData.setData(columnNameToValueMap);
		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(rootPEData);
		List<UnitRequestData> requestDataList = new ArrayList<>();
		requestDataList.add(finalRequestData);
		RequestData request = new RequestData();
		request.setActionCodeFkId("ADD_EDIT");
		request.setRequestDataList(requestDataList);
		try {
			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void updateTheTapplentNote(SingleNote tapplentNote, Note fullEverNote,String personId)
			throws EDAMUserException, EDAMSystemException, EDAMNotFoundException, TException {
		Map<String, String> personInfo = evernoteDAO.getPersonInfo(personId);
		List<Resource> resources = fullEverNote.getResources();
		// Delete All the attachments and update with the attachments which we
		// get from everNote
		// DELETE FROM T_PFM_EU_NOTE_ATTACHMENT WHERE NOTE_FK_ID = x?
		List<EventAttchment> attachments = evernoteDAO.getMeetingAttachments(tapplentNote.getNotePkId());
		if (attachments != null && !attachments.isEmpty()) {
			for (EventAttchment attachment : attachments) {
				String attachmentId = attachment.getAttachmentPkId();
				PEData child1PEData = new PEData();
				child1PEData.setMtPE("NoteAttachment");
				child1PEData.setActionCode("ADD_EDIT");
				Map<String, Object> columnNameToValueMap1 = new LinkedHashMap<String, Object>();
				columnNameToValueMap1.put("NoteAttachment.PrimaryKeyID", attachmentId);
				columnNameToValueMap1.put("NoteAttachment.Deleted", true);
				columnNameToValueMap1.put("NoteAttachment.LastModifiedDateTime",
						attachment.getLastModifiedDatetime().toString());
				child1PEData.setData(columnNameToValueMap1);
				UnitRequestData finalRequestData = new UnitRequestData();
				finalRequestData.setRootPEData(child1PEData);
				List<UnitRequestData> requestDataList = new ArrayList<>();
				requestDataList.add(finalRequestData);
				RequestData request = new RequestData();
				request.setActionCodeFkId("ADD_EDIT");
				request.setRequestDataList(requestDataList);
				try {

					serverRuleManager.applyServerRulesOnRequest(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		//Delete all the noteSubTasks and update with subtasks which are obtained from the everNote
		List<NoteSubTask> subTasks = evernoteDAO.getNoteSubTasks(tapplentNote.getNotePkId(), personId);
		if (subTasks != null && !subTasks.isEmpty()) {
			for (NoteSubTask subTask : subTasks) {
				String subTaskPkId = subTask.getNoteSubTaskPkId();
				PEData child1PEData = new PEData();
				child1PEData.setMtPE("NoteSubTask");
				child1PEData.setActionCode("ADD_EDIT");
				Map<String, Object> columnNameToValueMap1 = new LinkedHashMap<String, Object>();
				columnNameToValueMap1.put("NoteSubTask.PrimaryKeyID", subTaskPkId);
				columnNameToValueMap1.put("NoteSubTask.Deleted", true);
				columnNameToValueMap1.put("NoteSubTask.LastModifiedDateTime",
						subTask.getLastModifiedDatetime().toString());
				child1PEData.setData(columnNameToValueMap1);
				UnitRequestData finalRequestData = new UnitRequestData();
				finalRequestData.setRootPEData(child1PEData);
				List<UnitRequestData> requestDataList = new ArrayList<>();
				requestDataList.add(finalRequestData);
				RequestData request = new RequestData();
				request.setActionCodeFkId("ADD_EDIT");
				request.setRequestDataList(requestDataList);
				try {

					serverRuleManager.applyServerRulesOnRequest(request);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		Map<String, Object> columnNameToValueMap = new LinkedHashMap<String, Object>();
		Document doc = Jsoup.parse(fullEverNote.getContent());
		Element noteDescElement = doc.select("en-note").first();
		Element noteDESC = noteDescElement.parent();
		String finalNoteDesc = noteDESC.getElementsByTag("div").first().text();
		
		columnNameToValueMap.put("Note.ReminderDateTime",
				new Timestamp(fullEverNote.getAttributes().getReminderTime()).toString());
		columnNameToValueMap.put("Note.PrimaryKeyID", tapplentNote.getNotePkId());
		columnNameToValueMap.put("Note.NoteDescription", Util.getG11nString(personInfo.get("locale"), finalNoteDesc));
		columnNameToValueMap.put("Note.Title", Util.getG11nString(personInfo.get("locale"), fullEverNote.getTitle()));
		columnNameToValueMap.put("Note.LastModifiedDateTime", tapplentNote.getLastModifiedDateTime().toString());

		PEData rootPEData = new PEData();
		rootPEData.setMtPE("Note");

		rootPEData.setActionCode("ADD_EDIT");
		rootPEData.setData(columnNameToValueMap);
		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(rootPEData);
		List<UnitRequestData> requestDataList = new ArrayList<>();
		requestDataList.add(finalRequestData);
		RequestData request = new RequestData();
		request.setActionCodeFkId("ADD_EDIT");
		request.setRequestDataList(requestDataList);
		try {
			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (resources != null && !resources.isEmpty()) {
			for (Resource attachment : resources) {
				PEData child1PEData = new PEData();

				child1PEData.setActionCode("ADD_EDIT");
				child1PEData.setMtPE("NoteAttachment");
				Map<String, Object> columnNameToValueMap2 = new LinkedHashMap<String, Object>();
				String attachmentType = attachment.getMime();
				InputStream is1 = new ByteArrayInputStream(attachment.getData().getBody());
				String attachmentId1 = Util.uploadAttachmentToS3(attachmentType, is1);
				columnNameToValueMap2.put("NoteAttachment.NoteID", tapplentNote.getNotePkId());
				columnNameToValueMap2.put("NoteAttachment.AttachmentArtefactID", attachmentId1);
				columnNameToValueMap2.put("NoteAttachment.AttachmentName", attachment.getAttributes().getFileName());
				// columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode","DOCUMENT");
				if (attachment.getAttributes().getFileName().contains(".jpg")
						|| attachment.getAttributes().getFileName().contains(".png"))
					columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode", "IMAGE");
				if (attachment.getAttributes().getFileName().contains(".mp4")
						|| attachment.getAttributes().getFileName().contains(".flv"))
					columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode", "VIDEO");
				if(attachment.getAttributes().getFileName().contains(".mp3"))
					columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode", "AUDIO");
				else
					columnNameToValueMap2.put("NoteAttachment.AttachmentTypeCode", "DOCUMENT");
				child1PEData.setData(columnNameToValueMap2);
				UnitRequestData finalRequestData1 = new UnitRequestData();
				finalRequestData1.setRootPEData(child1PEData);
				List<UnitRequestData> requestDataList1 = new ArrayList<>();
				requestDataList.add(finalRequestData1);
				RequestData request1 = new RequestData();
				request1.setActionCodeFkId("ADD_EDIT");
				request1.setRequestDataList(requestDataList1);
				try {
					serverRuleManager.applyServerRulesOnRequest(request1);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}
		Elements noteSubTasks = doc.getElementsByTag("en-todo");
		if(noteSubTasks !=null && !noteSubTasks.isEmpty()){
			for (Element noteSubTask : noteSubTasks) {

				PEData child1PEData = new PEData();

				Element parent = noteSubTask.parent();
				String desc = "";
				for (Node child : parent.childNodes()) {
					if (child instanceof TextNode) {
						desc = ((TextNode) child).text();
					}
				}

				child1PEData.setActionCode("ADD_EDIT");
				child1PEData.setMtPE("NoteSubTask");

				Map<String, Object> columnNameToValueMap2 = new LinkedHashMap<String, Object>();
				columnNameToValueMap2.put("NoteSubTask.NoteID", tapplentNote.getNotePkId());
				columnNameToValueMap2.put("NoteSubTask.SubTaskDescription",
						Util.getG11nString(personInfo.get("locale"), desc));
				if (noteSubTask.hasAttr("checked"))
					columnNameToValueMap2.put("NoteSubTask.Complete", true);
				else
					columnNameToValueMap2.put("NoteSubTask.Complete", false);

				child1PEData.setData(columnNameToValueMap2);
				UnitRequestData finalRequestData1 = new UnitRequestData();
				finalRequestData1.setRootPEData(child1PEData);
				List<UnitRequestData> requestDataList1 = new ArrayList<>();
				requestDataList1.add(finalRequestData1);
				RequestData request1 = new RequestData();
				request1.setActionCodeFkId("ADD_EDIT");
				request1.setRequestDataList(requestDataList1);
				try {
					serverRuleManager.applyServerRulesOnRequest(request1);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	private void deleteTheEverNote(String guid, String accesstoken,String tapplentNoteId) throws Exception {
		Init(accesstoken);
		Note note = noteStore.getNote(guid, true, true, false, false);
		note.setDeleted(1);
		// Delete the Note Integration table entry..
				MeetingIntegration noteIntg = evernoteDAO.getIntegrationPkId("T_PFM_TAP_NOTE_INTGN", "NOTE_FK_ID",
						"NOTE_INTGN_PK_ID", tapplentNoteId);

				if (noteIntg != null && !noteIntg.equals("")) {
					String noteIntgPkId = noteIntg.getMtgIntgnPkId();
					PEData child2PEData = new PEData();
					child2PEData.setMtPE("NoteIntegration");
					child2PEData.setActionCode("ADD_EDIT");
					Map<String, Object> columnNameToValueMap1 = new LinkedHashMap<String, Object>();
					columnNameToValueMap1.put("NoteIntegration.NoteID", noteIntgPkId);
					columnNameToValueMap1.put("NoteIntegration.Deleted", true);
					columnNameToValueMap1.put("NoteIntegration.LastModifiedDateTime",
							noteIntg.getLastModifiedDatetime().toString());
					UnitRequestData finalRequestData = new UnitRequestData();
					finalRequestData.setRootPEData(child2PEData);
					List<UnitRequestData> requestDataList = new ArrayList<>();
					requestDataList.add(finalRequestData);
					RequestData request = new RequestData();
					request.setActionCodeFkId("ADD_EDIT");
					request.setRequestDataList(requestDataList);
					try {
						serverRuleManager.applyServerRulesOnRequest(request);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
		
	}

	private void updateLastSyncToken(String lastSynchToken, String noteSyncPkId, String lastModifiedTime) {
		Map<String, Object> columnNameToValueMap = new LinkedHashMap<String, Object>();
		columnNameToValueMap.put("NoteSynch.PrimaryKeyID", noteSyncPkId);
		columnNameToValueMap.put("NoteSynch.SynchNeeded", false);
		columnNameToValueMap.put("NoteSynch.LastModifiedDateTime", lastModifiedTime);
		columnNameToValueMap.put("NoteSynch.LastSynchExternalAppReference", lastSynchToken);
		String lastSynchDateTime = lastModifiedTime;
		columnNameToValueMap.put("NoteSynch.LastSynchDateTime", lastSynchDateTime);

		PEData rootPEData = new PEData();
		rootPEData.setMtPE("NoteSynch");
		rootPEData.setActionCode("ADD_EDIT");
		rootPEData.setData(columnNameToValueMap);
		UnitRequestData finalRequestData = new UnitRequestData();
		finalRequestData.setRootPEData(rootPEData);
		List<UnitRequestData> requestDataList = new ArrayList<>();
		requestDataList.add(finalRequestData);
		RequestData request = new RequestData();
		request.setActionCodeFkId("ADD_EDIT");
		request.setRequestDataList(requestDataList);
		try {
			serverRuleManager.applyServerRulesOnRequest(request);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
