package com.tapplent.apputility.uilayout.structure;

import com.tapplent.apputility.layout.structure.LayoutPermissions;
import com.tapplent.platformutility.common.util.AttachmentObject;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.uilayout.valueobject.ScreenSectionInstanceVO;
import org.apache.xpath.operations.Bool;
import org.springframework.beans.BeanUtils;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 25/04/17.
 */
public class ScreenSectionInstance {
    private String screenSectionInstancePkId;
    private String sectionMasterCodeFkId;
    private String parentSectionInstanceFkId;
    private String screenSectionInstanceName;
    private Boolean isShowAtInitialLoad;
    private String secnUIType;
    private String secnControlPos;
    private String onLoadGenericQuery;
    private boolean isStaticDataSection;
    private int themeSeqNumPosInt;
    private int disSeqNumber;
    private int minMandatoryRecordCount;
    private String noDataMessageG11nBigTxt;
    private IconVO noDataMessageIconCode;
    private AttachmentObject noDataMessageImageId;
    private boolean isPaginated;
    private String sUMtPE;
    private String sUMtPEAlias;
    private String sUParentMTPEAlias;
    private String suFkRelnWithParent;
    private String sUAttributePathExpn;
    private Boolean ignoreAllChildrenFromThisSection;
    private List<ScreenSectionInstance> sections = new ArrayList<>();
    private List<SectionActionInstance> actions = new ArrayList<>();
    private List<SectionControl> controls = new ArrayList<>();
    private List<OrgChartFilterLabel> orgChartFilterLabels = new ArrayList<>();
    private List<LayoutPermissions> layoutPermissions = new ArrayList<>();

    public String getScreenSectionInstancePkId() {
        return screenSectionInstancePkId;
    }

    public void setScreenSectionInstancePkId(String screenSectionInstancePkId) {
        this.screenSectionInstancePkId = screenSectionInstancePkId;
    }

    public String getSectionMasterCodeFkId() {
        return sectionMasterCodeFkId;
    }

    public void setSectionMasterCodeFkId(String sectionMasterCodeFkId) {
        this.sectionMasterCodeFkId = sectionMasterCodeFkId;
    }

    public String getParentSectionInstanceFkId() {
        return parentSectionInstanceFkId;
    }

    public void setParentSectionInstanceFkId(String parentSectionInstanceFkId) {
        this.parentSectionInstanceFkId = parentSectionInstanceFkId;
    }

    public Boolean getShowAtInitialLoad() {
        return isShowAtInitialLoad;
    }

    public void setShowAtInitialLoad(Boolean showAtInitialLoad) {
        isShowAtInitialLoad = showAtInitialLoad;
    }

    public String getSecnUIType() {
        return secnUIType;
    }

    public void setSecnUIType(String secnUIType) {
        this.secnUIType = secnUIType;
    }

    public String getSecnControlPos() {
        return secnControlPos;
    }

    public void setSecnControlPos(String secnControlPos) {
        this.secnControlPos = secnControlPos;
    }

    public boolean isStaticDataSection() {
        return isStaticDataSection;
    }

    public void setStaticDataSection(boolean staticDataSection) {
        isStaticDataSection = staticDataSection;
    }

    public int getThemeSeqNumPosInt() {
        return themeSeqNumPosInt;
    }

    public void setThemeSeqNumPosInt(int themeSeqNumPosInt) {
        this.themeSeqNumPosInt = themeSeqNumPosInt;
    }

    public String getNoDataMessageG11nBigTxt() {
        return noDataMessageG11nBigTxt;
    }

    public void setNoDataMessageG11nBigTxt(String noDataMessageG11nBigTxt) {
        this.noDataMessageG11nBigTxt = noDataMessageG11nBigTxt;
    }

    public IconVO getNoDataMessageIconCode() {
        return noDataMessageIconCode;
    }

    public void setNoDataMessageIconCode(IconVO noDataMessageIconCode) {
        this.noDataMessageIconCode = noDataMessageIconCode;
    }

    public AttachmentObject getNoDataMessageImageId() {
        return noDataMessageImageId;
    }

    public void setNoDataMessageImageId(AttachmentObject noDataMessageImageId) {
        this.noDataMessageImageId = noDataMessageImageId;
    }

    public List<SectionActionInstance> getActions() {
        return actions;
    }

    public void setActions(List<SectionActionInstance> actions) {
        this.actions = actions;
    }

    public List<SectionControl> getControls() {
        return controls;
    }

    public void setControls(List<SectionControl> controls) {
        this.controls = controls;
    }

    public List<ScreenSectionInstance> getSections() {
        return sections;
    }

    public void setSections(List<ScreenSectionInstance> sections) {
        this.sections = sections;
    }

    public String getOnLoadGenericQuery() {
        return onLoadGenericQuery;
    }

    public void setOnLoadGenericQuery(String onLoadGenericQuery) {
        this.onLoadGenericQuery = onLoadGenericQuery;
    }

    public String getScreenSectionInstanceName() {
        return screenSectionInstanceName;
    }

    public void setScreenSectionInstanceName(String screenSectionInstanceName) {
        this.screenSectionInstanceName = screenSectionInstanceName;
    }

    public List<OrgChartFilterLabel> getOrgChartFilterLabels() {
        return orgChartFilterLabels;
    }

    public void setOrgChartFilterLabels(List<OrgChartFilterLabel> orgChartFilterLabels) {
        this.orgChartFilterLabels = orgChartFilterLabels;
    }

    public int getDisSeqNumber() {
        return disSeqNumber;
    }

    public void setDisSeqNumber(int disSeqNumber) {
        this.disSeqNumber = disSeqNumber;
    }

    public boolean isPaginated() {
        return isPaginated;
    }

    public void setPaginated(boolean paginated) {
        isPaginated = paginated;
    }

    public String getsUMtPE() {
        return sUMtPE;
    }

    public void setsUMtPE(String sUMtPE) {
        this.sUMtPE = sUMtPE;
    }

    public String getsUMtPEAlias() {
        return sUMtPEAlias;
    }

    public void setsUMtPEAlias(String sUMtPEAlias) {
        this.sUMtPEAlias = sUMtPEAlias;
    }

    public String getsUParentMTPEAlias() {
        return sUParentMTPEAlias;
    }

    public void setsUParentMTPEAlias(String sUParentMTPEAlias) {
        this.sUParentMTPEAlias = sUParentMTPEAlias;
    }

    public String getSuFkRelnWithParent() {
        return suFkRelnWithParent;
    }

    public void setSuFkRelnWithParent(String suFkRelnWithParent) {
        this.suFkRelnWithParent = suFkRelnWithParent;
    }

    public String getsUAttributePathExpn() {
        return sUAttributePathExpn;
    }

    public void setsUAttributePathExpn(String sUAttributePathExpn) {
        this.sUAttributePathExpn = sUAttributePathExpn;
    }

    public int getMinMandatoryRecordCount() {
        return minMandatoryRecordCount;
    }

    public void setMinMandatoryRecordCount(int minMandatoryRecordCount) {
        this.minMandatoryRecordCount = minMandatoryRecordCount;
    }

    public List<LayoutPermissions> getLayoutPermissions() {
        return layoutPermissions;
    }

    public void setLayoutPermissions(List<LayoutPermissions> layoutPermissions) {
        this.layoutPermissions = layoutPermissions;
    }

    public Boolean getIgnoreAllChildrenFromThisSection() {
        return ignoreAllChildrenFromThisSection;
    }

    public void setIgnoreAllChildrenFromThisSection(Boolean ignoreAllChildrenFromThisSection) {
        this.ignoreAllChildrenFromThisSection = ignoreAllChildrenFromThisSection;
    }

    public static ScreenSectionInstance fromVO(ScreenSectionInstanceVO screenSectionInstanceVO){
        ScreenSectionInstance screenSectionInstance = new ScreenSectionInstance();
        BeanUtils.copyProperties(screenSectionInstanceVO, screenSectionInstance);
        return screenSectionInstance;
    }
}
