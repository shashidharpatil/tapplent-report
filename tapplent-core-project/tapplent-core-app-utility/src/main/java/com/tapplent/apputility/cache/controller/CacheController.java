package com.tapplent.apputility.cache.controller;

import com.tapplent.apputility.cache.service.CacheService;
import com.tapplent.apputility.layout.controller.LayoutController;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.util.Util;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequestMapping("tapp/cache/v1")
public class CacheController {
    private static final Logger LOG = LogFactory.getLogger(LayoutController.class);
    private CacheService cacheService;
    @RequestMapping(value="clearCache/t/{tenantId}/u/{personId}/e/", method= RequestMethod.GET)
    @ResponseBody
    void clearCache(@RequestHeader("access_token") String token,
                          @PathVariable("tenantId") String tenantId,
                          @PathVariable("personId") String userName,
                          @RequestParam("cacheName") String cacheName
    ){
        try {
            if (Util.validateAccessTokenAndSetUserContext(token)) {
                cacheService.clearCache(cacheName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CacheService getCacheService() {
        return cacheService;
    }

    public void setCacheService(CacheService cacheService) {
        this.cacheService = cacheService;
    }
}
