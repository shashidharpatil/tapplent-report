package com.tapplent.apputility.data.structure;

/**
 * Created by tapplent on 28/12/16.
 */
public class G11nResponse {
    private String g11nValues;

    public G11nResponse(String g11nValues) {
        this.g11nValues = g11nValues;
    }

    public String getG11nValues() {
        return g11nValues;
    }

    public void setG11nValues(String g11nValues) {
        this.g11nValues = g11nValues;
    }
}
