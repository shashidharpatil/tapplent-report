package com.tapplent.apputility.layout.structure;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

public class Page {
	String pageId;
	private String startingCanvasId;
	private JsonNode pagePlacement;
	List<Page> pageList;
	public Page(){
		this.pageList = new ArrayList<Page>();
	}
	public Page(String pageId, JsonNode pagePlacement){
		this.pageId = pageId;
		this.pagePlacement = pagePlacement;
		this.pageList = new ArrayList<Page>();
	}
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
	public JsonNode getPagePlacement() {
		return pagePlacement;
	}
	public void setPagePlacement(JsonNode pagePlacement) {
		this.pagePlacement = pagePlacement;
	}
	public List<Page> getPageList() {
		return pageList;
	}
	public void setPageList(List<Page> pageList) {
		this.pageList = pageList;
	}
	public String getStartingCanvasId() {
		return startingCanvasId;
	}
	public void setStartingCanvasId(String startingCanvasId) {
		this.startingCanvasId = startingCanvasId;
	}
}