package com.tapplent.apputility.conversation.structure;

/**
 * Created by Tapplent on 6/9/17.
 */
public class FeedbackGroupRequest {
    String mTPECode;
    String objectId;
    int limit;
    int offset;
    String keyword;

    public String getmTPECode() {
        return mTPECode;
    }

    public void setmTPECode(String mTPECode) {
        this.mTPECode = mTPECode;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
