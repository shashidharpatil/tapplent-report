package com.tapplent.apputility.layout.structure;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.CanvasVO;

public class Canvas {
	private String canvasId;
	private String parentCanvasId;
	@JsonIgnore
	private String canvasDefnMasterFkId;
	@JsonIgnore
	private String canvasTransactionFkId;
	private String baseTemplateId;
	private String mtPEId;
	private String pageFkId;
	private String canvasTypeCode;
	@JsonIgnore
	JsonNode mstDeviceIndependentBlob;
	@JsonIgnore
	JsonNode txnDeviceIndependentBlob;
	JsonNode deviceIndependentBlob;
	@JsonIgnore
	JsonNode mstDeviceDependentBlob;
	@JsonIgnore
	JsonNode txnDeviceDependentBlob;
	JsonNode deviceDependentBlob;
	@JsonIgnore
	JsonNode deviceApplicableBlob;
	private List<Control> controlList;
	private List<PropertyControl> propertyControlList;
	private List<ActionStandAlone> actionStandAloneList;
	private List<CanvasEvent> canvasEventList;
	private List<ActionGroup> actionGroupList;
	public Canvas(){
		this.controlList = new ArrayList<>();
		this.propertyControlList = new ArrayList<>();
		this.actionStandAloneList = new ArrayList<>();
		this.canvasEventList = new ArrayList<>();
		this.actionGroupList = new ArrayList<>();
	}
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public String getParentCanvasId() {
		return parentCanvasId;
	}
	public void setParentCanvasId(String parentCanvasId) {
		this.parentCanvasId = parentCanvasId;
	}
	public String getCanvasDefnMasterFkId() {
		return canvasDefnMasterFkId;
	}
	public void setCanvasDefnMasterFkId(String canvasDefnMasterFkId) {
		this.canvasDefnMasterFkId = canvasDefnMasterFkId;
	}
	public String getCanvasTransactionFkId() {
		return canvasTransactionFkId;
	}
	public void setCanvasTransactionFkId(String canvasTransactionFkId) {
		this.canvasTransactionFkId = canvasTransactionFkId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPEId() {
		return mtPEId;
	}
	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}
	public String getPageFkId() {
		return pageFkId;
	}
	public void setPageFkId(String pageFkId) {
		this.pageFkId = pageFkId;
	}
	public JsonNode getMstDeviceIndependentBlob() {
		return mstDeviceIndependentBlob;
	}
	public void setMstDeviceIndependentBlob(JsonNode mstDeviceIndependentBlob) {
		this.mstDeviceIndependentBlob = mstDeviceIndependentBlob;
	}
	public JsonNode getTxnDeviceIndependentBlob() {
		return txnDeviceIndependentBlob;
	}
	public void setTxnDeviceIndependentBlob(JsonNode txnDeviceIndependentBlob) {
		this.txnDeviceIndependentBlob = txnDeviceIndependentBlob;
	}
	public JsonNode getMstDeviceDependentBlob() {
		return mstDeviceDependentBlob;
	}
	public void setMstDeviceDependentBlob(JsonNode mstDeviceDependentBlob) {
		this.mstDeviceDependentBlob = mstDeviceDependentBlob;
	}
	public JsonNode getTxnDeviceDependentBlob() {
		return txnDeviceDependentBlob;
	}
	public void setTxnDeviceDependentBlob(JsonNode txnDeviceDependentBlob) {
		this.txnDeviceDependentBlob = txnDeviceDependentBlob;
	}
	public JsonNode getDeviceApplicableBlob() {
		return deviceApplicableBlob;
	}
	public void setDeviceApplicableBlob(JsonNode deviceApplicableBlob) {
		this.deviceApplicableBlob = deviceApplicableBlob;
	}
	public List<Control> getControlList() {
		return controlList;
	}
	public void setControlList(List<Control> controlList) {
		this.controlList = controlList;
	}
	public List<PropertyControl> getPropertyControlList() {
		return propertyControlList;
	}
	public void setPropertyControlList(List<PropertyControl> propertyControlList) {
		this.propertyControlList = propertyControlList;
	}
	public List<ActionStandAlone> getActionStandAloneList() {
		return actionStandAloneList;
	}
	public void setActionStandAloneList(List<ActionStandAlone> actionStandAloneList) {
		this.actionStandAloneList = actionStandAloneList;
	}
	public List<CanvasEvent> getCanvasEventList() {
		return canvasEventList;
	}
	public void setCanvasEventList(List<CanvasEvent> canvasEventList) {
		this.canvasEventList = canvasEventList;
	}
	public List<ActionGroup> getActionGroupList() {
		return actionGroupList;
	}
	public void setActionGroupList(List<ActionGroup> actionGroupList) {
		this.actionGroupList = actionGroupList;
	}
	public String getCanvasTypeCode() {
		return canvasTypeCode;
	}
	public void setCanvasTypeCode(String canvasTypeCode) {
		this.canvasTypeCode = canvasTypeCode;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public static Canvas fromVO(CanvasVO canvasVO){
		Canvas canvas = new Canvas();
		BeanUtils.copyProperties(canvasVO, canvas);
		return canvas;
	}
}
