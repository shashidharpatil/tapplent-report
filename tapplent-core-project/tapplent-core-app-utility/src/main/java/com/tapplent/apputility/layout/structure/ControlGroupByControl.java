package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.ControlGroupByControlVO;

public class ControlGroupByControl {
	private String controlGroupByControlId;
	private String baseTemplateId;
	private String mtPE;
	private String controlId;
	private String groupByCanvasId;
	private String groupByControlId;
	private JsonNode deviceIndependentBlob;
	private JsonNode deviceDependentBlob;
	public String getControlGroupByControlId() {
		return controlGroupByControlId;
	}

	public void setControlGroupByControlId(String controlGroupByControlId) {
		this.controlGroupByControlId = controlGroupByControlId;
	}

	public String getBaseTemplateId() {
		return baseTemplateId;
	}

	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getMtPE() {
		return mtPE;
	}

	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}

	public String getControlId() {
		return controlId;
	}

	public void setControlId(String controlId) {
		this.controlId = controlId;
	}

	public String getGroupByCanvasId() {
		return groupByCanvasId;
	}

	public void setGroupByCanvasId(String groupByCanvasId) {
		this.groupByCanvasId = groupByCanvasId;
	}

	public String getGroupByControlId() {
		return groupByControlId;
	}

	public void setGroupByControlId(String groupByControlId) {
		this.groupByControlId = groupByControlId;
	}

	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}

	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}

	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}

	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}

	public static ControlGroupByControl fromVO(ControlGroupByControlVO controlGroupByControlEuVO){
		ControlGroupByControl controlGroupByControlEU = new ControlGroupByControl();
		BeanUtils.copyProperties(controlGroupByControlEuVO, controlGroupByControlEU);
		return controlGroupByControlEU;
	}
}
