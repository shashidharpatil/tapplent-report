package com.tapplent.apputility.layout.structure;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.UiSettingsVO;

public class UiSettings {
	@JsonIgnore
	private String versionId;
	private String uiSettings;
	private JsonNode settings;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getUiSettings() {
		return uiSettings;
	}
	public void setUiSettings(String uiSettings) {
		this.uiSettings = uiSettings;
	}
	public JsonNode getSettings() {
		return settings;
	}
	public void setSettings(JsonNode settings) {
		this.settings = settings;
	}
	public static UiSettings fromVO(UiSettingsVO uiSettingsVO){
		UiSettings uiSettings = new UiSettings();
		BeanUtils.copyProperties(uiSettingsVO, uiSettings);
		return uiSettings;
	}
}
