package com.tapplent.apputility.conversation.structure;

/**
 * Created by Tapplent on 7/10/17.
 */
public class FeedbackMessageReadStatusRequest {
    String feedbackId;

    public String getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(String feedbackId) {
        this.feedbackId = feedbackId;
    }
}
