package com.tapplent.apputility.layout.structure;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.layout.valueObject.PageRelationVO;

public class PageRelation {
	private String versionId;
	private String pageChildEuPkId;
	private String baseTemplateId;
	private String deviceType;
	private String pageChildEquFkId;
	private String fromPageEuFkId;
	private String toPageEuFkId;
	private String startingCanvasId;
	private JsonNode childPagePlacement;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getPageChildEuPkId() {
		return pageChildEuPkId;
	}
	public void setPageChildEuPkId(String pageChildEuPkId) {
		this.pageChildEuPkId = pageChildEuPkId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getPageChildEquFkId() {
		return pageChildEquFkId;
	}
	public void setPageChildEquFkId(String pageChildEquFkId) {
		this.pageChildEquFkId = pageChildEquFkId;
	}
	public String getFromPageEuFkId() {
		return fromPageEuFkId;
	}
	public void setFromPageEuFkId(String fromPageEuFkId) {
		this.fromPageEuFkId = fromPageEuFkId;
	}
	public String getToPageEuFkId() {
		return toPageEuFkId;
	}
	public void setToPageEuFkId(String toPageEuFkId) {
		this.toPageEuFkId = toPageEuFkId;
	}
	public String getStartingCanvasId() {
		return startingCanvasId;
	}
	public void setStartingCanvasId(String startingCanvasId) {
		this.startingCanvasId = startingCanvasId;
	}
	public JsonNode getChildPagePlacement() {
		return childPagePlacement;
	}
	public void setChildPagePlacement(JsonNode childPagePlacement) {
		this.childPagePlacement = childPagePlacement;
	}
	public static PageRelation fromVO(PageRelationVO pageRelVO) {
		PageRelation pageRelation = new PageRelation();
		BeanUtils.copyProperties(pageRelVO, pageRelation);
		//Also convert write the logic to convert the G11n values here.
		return pageRelation;
	}
}
