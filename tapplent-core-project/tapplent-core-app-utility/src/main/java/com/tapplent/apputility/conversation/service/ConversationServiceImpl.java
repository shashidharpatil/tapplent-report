package com.tapplent.apputility.conversation.service;

import com.amazonaws.services.s3.AmazonS3;
import com.google.common.collect.Lists;
import com.tapplent.apputility.conversation.structure.*;
import com.tapplent.platformutility.accessmanager.dao.AccessManagerService;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.conversation.dao.ConversationDAO;
import com.tapplent.platformutility.conversation.valueobject.FeedbackGroupVO;
import com.tapplent.platformutility.conversation.valueobject.FeedbackMessageVO;
import com.tapplent.platformutility.conversation.valueobject.GroupMemberVO;
import com.tapplent.platformutility.conversation.valueobject.PersonDetailsVO;
import com.tapplent.platformutility.layout.valueObject.LayoutPermissionsVO;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Tapplent on 6/9/17.
 */
public class ConversationServiceImpl implements ConversationService{

    private AccessManagerService accessManagerService;
    private ConversationDAO conversationDAO;
    private AmazonS3 s3Client;

    @Override
    @Transactional
    public FeedbackGroupResponse getFeedbackGroupList(String mtPECode, String objectId, int limit, int offset, String keyword) {
        if (limit == 0) limit = 20;//default value
        List<FeedbackGroupVO> feedbackGroupVOList = conversationDAO.getFeedbackGroupList(mtPECode, objectId, limit, offset, keyword);
        Map<String, Integer> groupToUnreadMsgCount = conversationDAO.getUnreadMessageCountByGroup(feedbackGroupVOList);
        List<FeedbackGroup> feedbackGroups = null;
        if (feedbackGroupVOList != null && !feedbackGroupVOList.isEmpty())
            feedbackGroups = new ArrayList<>();//todo return empty list if no groups created?
        for (FeedbackGroupVO feedbackGroupVO : feedbackGroupVOList){
            FeedbackGroup feedbackGroup = FeedbackGroup.fromVO(feedbackGroupVO);
            feedbackGroups.add(feedbackGroup);
            if (feedbackGroup.isBilateralConversation()) {
                String otherPersonId = conversationDAO.getOtherPersonIdByGroup(feedbackGroup.getFeedbackGroupPkId());
                PersonDetailsVO personDetailsVO = conversationDAO.getPersonDetailsByPersonId(otherPersonId);
                if (personDetailsVO != null) {
                    PersonDetail personDetail = new PersonDetail();
                    BeanUtils.copyProperties(personDetailsVO, personDetail);
                    List<PersonDetail> personDetails = new ArrayList<>();
                    personDetails.add(personDetail);
                    feedbackGroup.setPersonDetailList(personDetails);
                }
            }
            if (groupToUnreadMsgCount.get(feedbackGroup.getFeedbackGroupPkId()) != null){
                feedbackGroup.setLoggedinPersonUnreadMessageCount(groupToUnreadMsgCount.get(feedbackGroup.getFeedbackGroupPkId()));
            }
        }

        String loggedinPersonId = TenantContextHolder.getUserContext().getPersonId();
        PersonDetail loggedinPersonDetails = getPersonDetailsByPersonId(loggedinPersonId);
        FeedbackGroupResponse feedbackGroupResponse = new FeedbackGroupResponse();
        feedbackGroupResponse.setLoggedinPersonDetails(loggedinPersonDetails);
        feedbackGroupResponse.setGroupToUnreadMessageCount(groupToUnreadMsgCount);
        feedbackGroupResponse.setFeedbackGroups(feedbackGroups);
        return feedbackGroupResponse;
    }

    @Override
    @Transactional
    public CreateFdbkGrpResponse createFeedbackGroup(CreateFdbkGrpRequest createFdbkGrpRequest) throws SQLException {
        FeedbackGroupVO feedbackGroupVO = new FeedbackGroupVO();
        feedbackGroupVO.setFeedbackGroupPkId(Common.getUUID());
        List<GroupMemberVO> groupMemberVOS = new ArrayList<>();
        PersonDetail personAdminDetail = new PersonDetail();
        String loggedinPersonId = TenantContextHolder.getUserContext().getPersonId();
        personAdminDetail.setPersonId(loggedinPersonId);
        GroupMemberVO groupMemberAdminVO = new GroupMemberVO();
        personAdminDetail.setMemberAdmin(true);
        BeanUtils.copyProperties(personAdminDetail, groupMemberAdminVO);
        groupMemberVOS.add(groupMemberAdminVO);
        CreateFdbkGrpResponse createFdbkGrpResponse = new CreateFdbkGrpResponse();
        for (PersonDetail personDetail : createFdbkGrpRequest.getPersonDetailList()) {
            GroupMemberVO groupMemberVO = new GroupMemberVO();
            BeanUtils.copyProperties(personDetail, groupMemberVO);
            groupMemberVOS.add(groupMemberVO);
        }
        BeanUtils.copyProperties(createFdbkGrpRequest, feedbackGroupVO);
        feedbackGroupVO.setFeedbackGroupPkId(Common.getUUID());
        feedbackGroupVO.setMemberAdmin(true);
        conversationDAO.createFeedbackGroup(feedbackGroupVO);
        FeedbackGroup feedbackGroup = new FeedbackGroup();
        BeanUtils.copyProperties(feedbackGroupVO, feedbackGroup);
        if (feedbackGroup.getFeedbackGroupImageId() != null){
            feedbackGroup.setFeedbackGroupImageUrl(Util.getURLforS3(feedbackGroup.getFeedbackGroupImageId(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
        }
        conversationDAO.createFeedbackGroupMembers(groupMemberVOS, feedbackGroupVO.getFeedbackGroupPkId());
        feedbackGroup.setLoggedinPersonDetail(getPersonDetailsByPersonId(loggedinPersonId));
        feedbackGroup.setLoggedinPersonFeedbackMemberId(feedbackGroup.getLoggedinPersonDetail().getFeedbackGroupMemberId());
        if (createFdbkGrpRequest.isBilateralConversation()) {
            PersonDetailsVO personDetailsVO = conversationDAO.getPersonDetailsByPersonId(createFdbkGrpRequest.getPersonDetailList().get(0).getPersonId());
            if (personDetailsVO != null) {
                PersonDetail personDetail = new PersonDetail();
                BeanUtils.copyProperties(personDetailsVO, personDetail);
                List<PersonDetail> personDetails = new ArrayList<>();
                personDetails.add(personDetail);
                feedbackGroup.setPersonDetailList(personDetails);
            }
        }
        createFdbkGrpResponse.setFeedbackGroup(feedbackGroup);
        return createFdbkGrpResponse;
    }

    private PersonDetail getPersonDetailsByPersonId(String personId) {
        PersonDetailsVO personDetailsVO = conversationDAO.getPersonDetailsByPersonId(personId);
        PersonDetail personDetail = null;
        if (personDetailsVO != null) {
            personDetail = new PersonDetail();
            BeanUtils.copyProperties(personDetailsVO, personDetail);
        }
        return personDetail;
    }

    @Override
    @Transactional
    public FeedbackContactDetails getFeedbackContactDetails(String mTPECode, String objectId, int limit, int offset, String keyword, String feedbackGroupId) throws SQLException {
//        LayoutPermissionsVO layoutPermissionVO = conversationDAO.getLayoutPermissions(actionId);
        List<PersonDetailsVO> personDetailsVOS = conversationDAO.getFeedbackContactDetails(mTPECode, objectId, limit, offset, keyword, feedbackGroupId);
        FeedbackContactDetails feedbackContactDetails = new FeedbackContactDetails();
        List<PersonDetail> personDetails = new ArrayList<>();
        for (PersonDetailsVO personDetailsVO : personDetailsVOS){
            PersonDetail personDetail = PersonDetail.fromVO(personDetailsVO);
            personDetails.add(personDetail);
        }
        feedbackContactDetails.setPersonDetails(personDetails);
        return feedbackContactDetails;
    }

    @Override
    @Transactional
    public WriteFeedbackMessageResponse writeFeedbackMessage(WriteFeedbackMessageRequest writeFeedbackMessageRequest) {
//        String feedbackPkId = writeFeedbackMessageRequest.getFeedbackPkId();
//        String feedbackGroupId = writeFeedbackMessageRequest.getFeedbackGroupId();
//        String message = writeFeedbackMessageRequest.getMessage();
//        String messageType = writeFeedbackMessageRequest.getMessageType();
//        String messageAttach = writeFeedbackMessageRequest.getMessageAttach();
//        String feedbackParentMsgId = writeFeedbackMessageRequest.getFeedbackParentMsgId();
//        boolean messageEdited = writeFeedbackMessageRequest.isMessageEdited();
//        boolean deleted = writeFeedbackMessageRequest.isDeleted();
        List<FeedbackMessage> feedbackMessageList = new ArrayList<>();
        try {
            for (FeedbackMessage feedbackMessage : writeFeedbackMessageRequest.getFeedbackMessageList()) {
                FeedbackMessageVO feedbackMessageVO = new FeedbackMessageVO();
                BeanUtils.copyProperties(feedbackMessage, feedbackMessageVO);
                String feedbackPkId = conversationDAO.writeFeedbackMessage(feedbackMessageVO);
                feedbackMessageVO = conversationDAO.getFeedbackMessage(feedbackPkId);
                BeanUtils.copyProperties(feedbackMessageVO, feedbackMessage);
                feedbackMessage.setFeedbackPkId(feedbackPkId);
                feedbackMessage.setCreatedByPersonId(TenantContextHolder.getUserContext().getPersonId());
                feedbackMessage.setCreatedByPersonName(Util.getG11nValue(TenantContextHolder.getUserContext().getPersonName(), feedbackMessage.getCreatedByPersonId()));
                feedbackMessageList.add(feedbackMessage);
            }
        }catch (SQLException e){
            return null;
        }
        WriteFeedbackMessageResponse writeFeedbackMessageResponse = new WriteFeedbackMessageResponse();
        writeFeedbackMessageResponse.setFeedbackMessageList(feedbackMessageList);
        return writeFeedbackMessageResponse;
    }

    @Override
    @Transactional
    public List<FeedbackMessage> getFeedbackMessage(String feedbackGroupId, int limit, int offset) {
        List<FeedbackMessageVO> feedbackMessageVOs = conversationDAO.getFeedbackMessage(feedbackGroupId, limit, offset);
        List<FeedbackMessageVO> feedbackGroupVOSReversed = Lists.reverse(feedbackMessageVOs);
        List<FeedbackMessage> feedbackMessages = new ArrayList<>();
        for (FeedbackMessageVO feedbackMessageVO : feedbackGroupVOSReversed){
            FeedbackMessage feedbackMessage = new FeedbackMessage();
            BeanUtils.copyProperties(feedbackMessageVO, feedbackMessage);
            if (feedbackMessageVO.getParentFeedbackMessage() != null) {
                FeedbackMessage parentFeedbackMessage =  new FeedbackMessage();
                BeanUtils.copyProperties(feedbackMessageVO.getParentFeedbackMessage(), parentFeedbackMessage);
                feedbackMessage.setParentFeedbackMessage(parentFeedbackMessage);
            }
            if (feedbackMessage.getMessageAttachId() != null)
                feedbackMessage.setMessageAttachUrl(Util.getURLforS3(feedbackMessageVO.getMessageAttachId(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
            feedbackMessages.add(feedbackMessage);
        }
        conversationDAO.populateFeedbackReadStatus(feedbackGroupId);
        return feedbackMessages;
    }

    @Override
    @Transactional
    public List<GroupMember> getFeedbackGroupMemberDetails(String feedbackGroupId) {
        List<PersonDetailsVO> personDetailsVOS = conversationDAO.getFeedbackGroupMemberDetails(feedbackGroupId);
        List<GroupMember> groupMembers = new ArrayList<>();
        for (PersonDetailsVO personDetailsVO : personDetailsVOS){
            GroupMember groupMember =  new GroupMember();
            BeanUtils.copyProperties(personDetailsVO, groupMember);
            groupMember.setPersonPhotoUrl(Util.preSignedGetUrl(personDetailsVO.getPersonPhoto(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
            groupMember.setGroupId(feedbackGroupId);
            groupMembers.add(groupMember);
        }
        return groupMembers;
    }

    @Override
    @Transactional
    public List<GroupMember> updateFeedbackGroupMemberDetails(FeedbackGroupMemberUpdateRequest feedbackGroupMemberUpdateRequest) throws SQLException {
        for (GroupMember groupMember : feedbackGroupMemberUpdateRequest.getGroupMemberList()){
            GroupMemberVO groupMemberVO = new GroupMemberVO();
            BeanUtils.copyProperties(groupMember, groupMemberVO);
            conversationDAO.updateFeedbackGroupMemberDetails(groupMemberVO);
        }
        return feedbackGroupMemberUpdateRequest.getGroupMemberList();
    }

    @Override
    @Transactional
    public List<GroupMember> addFeedbackGroupMemberDetails(FeedbackGroupMemberAddRequest feedbackGroupMemberAddRequest) throws SQLException {
        List<GroupMemberVO> groupMemberVOS = new ArrayList<>();
        for (GroupMember groupMember : feedbackGroupMemberAddRequest.getGroupMembers()){
            groupMember.setFeedbackGroupMemberId(Common.getUUID());
            GroupMemberVO groupMemberVO = new GroupMemberVO();
            groupMemberVO.setGroupId(feedbackGroupMemberAddRequest.getGroupId());
            groupMember.setGroupId(feedbackGroupMemberAddRequest.getGroupId());
            BeanUtils.copyProperties(groupMember, groupMemberVO);
            groupMemberVOS.add(groupMemberVO);
        }
        conversationDAO.addFeedbackGroupMembers(groupMemberVOS);
        for (GroupMember groupMember : feedbackGroupMemberAddRequest.getGroupMembers()){
            PersonDetailsVO personDetail = conversationDAO.getPersonDetailsByPersonId(groupMember.getPersonId());
            if (personDetail != null)
                BeanUtils.copyProperties(personDetail, groupMember);
        }
        return feedbackGroupMemberAddRequest.getGroupMembers();
    }

    @Override
    @Transactional
    public void deleteGroup(String feedbackGroupId) {
        conversationDAO.deleteGroup(feedbackGroupId);
    }

    @Override
    @Transactional
    public CreateFdbkGrpResponse updateFeedbackGroup(CreateFdbkGrpRequest feedbackgroupRequest) throws SQLException {
        FeedbackGroupVO feedbackGroupVO = new FeedbackGroupVO();
        BeanUtils.copyProperties(feedbackgroupRequest, feedbackGroupVO);
        conversationDAO.updateFeedbackGroup(feedbackGroupVO);
        CreateFdbkGrpResponse createFdbkGrpResponse = new CreateFdbkGrpResponse();
        FeedbackGroup feedbackGroup = new FeedbackGroup();
        BeanUtils.copyProperties(feedbackgroupRequest, feedbackGroup);
        feedbackGroup.setFeedbackGroupImageUrl(Util.getURLforS3(feedbackgroupRequest.getFeedbackGroupImageId(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
        createFdbkGrpResponse.setFeedbackGroup(feedbackGroup);
        return createFdbkGrpResponse;
    }

    @Override
    @Transactional
    public List<GroupMember> getFeedbackGroupMemberReadStatus(FeedbackMessageReadStatusRequest feedbackMessageReadStatusRequest) {
        List<GroupMemberVO> groupMemberVOs = conversationDAO.getFeedGroupMemberReadStatus(feedbackMessageReadStatusRequest.getFeedbackId());
        List<GroupMember> groupMembers = new ArrayList<>();
        for (GroupMemberVO groupMemberVO : groupMemberVOs){
            GroupMember groupMember = new GroupMember();
            BeanUtils.copyProperties(groupMemberVO, groupMember);
            groupMember.setPersonPhotoUrl(Util.preSignedGetUrl(groupMemberVO.getPersonPhoto(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
            groupMembers.add(groupMember);
        }
        return groupMembers;
    }

    @Override
    @Transactional
    public List<FeedbackMessage> getBookmarkedFeedbackMessage(String feedbackGroupId, int limit, int offset) {
        List<FeedbackMessageVO> feedbackMessageVOs = conversationDAO.getBookmarkedFeedbackMessage(feedbackGroupId, limit, offset);
        List<FeedbackMessageVO> feedbackGroupVOSReversed = Lists.reverse(feedbackMessageVOs);
        List<FeedbackMessage> feedbackMessages = new ArrayList<>();
        for (FeedbackMessageVO feedbackMessageVO : feedbackGroupVOSReversed){
            FeedbackMessage feedbackMessage = new FeedbackMessage();
            BeanUtils.copyProperties(feedbackMessageVO, feedbackMessage);
            if (feedbackMessageVO.getParentFeedbackMessage() != null) {
                FeedbackMessage parentFeedbackMessage =  new FeedbackMessage();
                BeanUtils.copyProperties(feedbackMessageVO.getParentFeedbackMessage(), parentFeedbackMessage);
                feedbackMessage.setParentFeedbackMessage(parentFeedbackMessage);
            }
            if (feedbackMessage.getMessageAttachId() != null)
                feedbackMessage.setMessageAttachUrl(Util.getURLforS3(feedbackMessageVO.getMessageAttachId(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
            feedbackMessages.add(feedbackMessage);
        }
        return feedbackMessages;
    }

    public AccessManagerService getAccessManagerService() {
        return accessManagerService;
    }

    public void setAccessManagerService(AccessManagerService accessManagerService) {
        this.accessManagerService = accessManagerService;
    }

    public ConversationDAO getConversationDAO() {
        return conversationDAO;
    }

    public void setConversationDAO(ConversationDAO conversationDAO) {
        this.conversationDAO = conversationDAO;
    }

    public AmazonS3 getS3Client() {
        return s3Client;
    }

    public void setS3Client(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }
}
