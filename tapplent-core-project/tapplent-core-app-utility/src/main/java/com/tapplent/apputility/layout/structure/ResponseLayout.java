package com.tapplent.apputility.layout.structure;

import java.io.Serializable;

public class ResponseLayout implements Serializable{
	private static final long serialVersionUID = 1L;
	private MetaProcess metaProcess;
	public MetaProcess getMetaProcess() {
		return metaProcess;
	}
	public void setMetaProcess(MetaProcess metaProcess) {
		this.metaProcess = metaProcess;
	}
	
}
