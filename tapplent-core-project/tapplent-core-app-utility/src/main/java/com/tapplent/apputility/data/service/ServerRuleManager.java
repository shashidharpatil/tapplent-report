package com.tapplent.apputility.data.service;

import com.tapplent.apputility.data.controller.FeaturedResponse;
import com.tapplent.apputility.data.structure.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface ServerRuleManager {
	ResponseData applyServerRulesOnRequest(RequestData requestData) throws Exception;
//	void indexData(String doName);
	ResponseData applyServerRulesOnETLWriteRequest(RequestData requestData) throws Exception;

	RelationResponse getRelationControlData(String controlId);

    G11nResponse getG11nData(String dbPkId, String doaExpn);

	UnitResponseData getRelationDownwardEffect(String mtPE, String pk);

	ResponseData getQueryData(GenericQueryRequest queryRequest) throws IOException, SQLException;

	List<ColleaguesAtWorkResponse> getColleaguesData(String contextID);

    FeaturedResponse getFeaturingPersonDetail(String contextID, String mtPE);

    Object getBreakedUpData(String contextType, String contextID);

    List<JobRelationships> getDefaultJobRelations(String contextID1, String contextID2);

    String populateReportData(String groupID, String timeZone) throws IOException, SQLException;
}
