package com.tapplent.apputility.etl.structure;

import com.fasterxml.jackson.databind.JsonNode;

public class EtlTaargetDataMode {
//	VERSION ID            
//	DATA MODE CODE PK ID  
//	NAME G11N BLOB        
//	ICON CODE FK ID    
	private String versionId;
	private String dataModeCodePkId ;
	private JsonNode nameG11nBlob; 
	private String iconCodeFkId;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getDataModeCodePkId() {
		return dataModeCodePkId;
	}
	public void setDataModeCodePkId(String dataModeCodePkId) {
		this.dataModeCodePkId = dataModeCodePkId;
	}
	public JsonNode getNameG11nBlob() {
		return nameG11nBlob;
	}
	public void setNameG11nBlob(JsonNode nameG11nBlob) {
		this.nameG11nBlob = nameG11nBlob;
	}
	public String getIconCodeFkId() {
		return iconCodeFkId;
	}
	public void setIconCodeFkId(String iconCodeFkId) {
		this.iconCodeFkId = iconCodeFkId;
	}
}
