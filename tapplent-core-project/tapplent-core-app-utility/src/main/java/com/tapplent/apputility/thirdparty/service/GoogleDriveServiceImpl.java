package com.tapplent.apputility.thirdparty.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.tapplent.apputility.data.service.ServerRuleManager;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.thirdPartyApp.gmail.EverNoteDAO;
import com.tapplent.platformutility.thirdPartyApp.gmail.ThirdPartyUtil;

public class GoogleDriveServiceImpl implements GoogleDriveService {
	 private EverNoteDAO evernoteDAO;
	
	public EverNoteDAO getEvernoteDAO() {
		return evernoteDAO;
	}
	public void setEvernoteDAO(EverNoteDAO evernoteDAO) {
		this.evernoteDAO = evernoteDAO;
	}
	public String uploadDriveAttachmentToS3(String fileUrl,String mimeType) throws MalformedURLException, IOException{
		InputStream is = new URL(fileUrl).openStream();
		String attachmentArtifact = Util.uploadAttachmentToS3(mimeType, is);
		return attachmentArtifact;
	}
	
	public String uploadTapplentAttachmentToDrive(String attachmentId[],String personId) throws org.json.JSONException, IOException, ParseException, JSONException{
		String status = "";
		for(String s3Id:attachmentId){
			Map<String,Object> attachmentMap =ThirdPartyUtil.getAttchment(s3Id);
			 String mimeType = attachmentMap.get("ContentType").toString();
			 InputStream is = (InputStream) attachmentMap.get("InputStream");
			 Map<String,String> tokens = evernoteDAO.getToken("GOOGLE_DRIVE",personId);
				String accessToken = tokens.get("accessToken");
				String refreshToken = tokens.get("refreshToken");
				boolean flag = ThirdPartyUtil.isTokenValid(accessToken);
				if(flag==false){
					accessToken = ThirdPartyUtil.getAccessToken(refreshToken);
				}
				String attachmentName = "";
			 status = uploadToDrive(is,mimeType,accessToken,attachmentName);
			 if(status.equals("failure"))
				 return status;
		}
		
		return status;
	}
private String uploadToDrive(InputStream is, String mimeType,String accesstoken,String attachmentName) throws IOException, ParseException, JSONException, org.json.JSONException {
		
		StringBuilder url = new StringBuilder("https://www.googleapis.com/drive/v2/files");
		String title = "";
		if(attachmentName.contains("Audio")){
			title = "tapplent.mp3";
		}else if(attachmentName.contains("Video")){
			title = "tapplent.mp4";
		}else if (attachmentName.contains("Image")){
			title = "tapplent.jpeg";
			
		}else if(attachmentName.contains("Document")){
			title = "tapplent.pdf";
		}
		HttpPost post = new HttpPost(url.toString());
		post.addHeader("Authorization", "Bearer "+accesstoken);
		post.addHeader("Content-Type", "application/json");
		JSONObject request = new JSONObject();
		request.put("title", title);
		request.put("mimeType", mimeType);
		StringEntity entity1 = new StringEntity(request.toString());
		post.setEntity(entity1);
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(post);
	    JSONObject res = new JSONObject(EntityUtils.toString(response.getEntity()));
	    String fileId = res.getString("id");
	    HttpPut put = new HttpPut("https://www.googleapis.com/upload/drive/v2/files/"+fileId+"?uploadType=media");
	    put.addHeader("Authorization", "Bearer "+accesstoken);
	    put.addHeader("Content-Type", mimeType);
	    byte [] bytes = IOUtils.toByteArray(is);
        ByteArrayEntity requestEntity = new ByteArrayEntity(bytes); 
        put.setEntity(requestEntity); 
		HttpClient client1 = HttpClientBuilder.create().build();
	    HttpResponse response1;
	    response1 = client1.execute(put);
	    String status= String.valueOf(response1.getStatusLine().getStatusCode());
	    String message = "failure";
	    if(status.matches("201") ||status.matches("200") ){
	    	message = "successfully uploaded attachment to google drive";
	    }
		
		return message;
	}
}
