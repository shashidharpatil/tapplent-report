package com.tapplent.apputility.layout.structure;

public class ColumnName {
	private String tableName;
	private String columnName;
	private String dbDataType;
	private String specialRequirements;
	public ColumnName(String tableName, String columnName, String dbDataType, String specialRequirements) {
		super();
		this.tableName = tableName;
		this.columnName = columnName;
		this.dbDataType = dbDataType;
		this.specialRequirements = specialRequirements;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getDbDataType() {
		return dbDataType;
	}
	public void setDbDataType(String dbDataType) {
		this.dbDataType = dbDataType;
	}
	public String getSpecialRequirements() {
		return specialRequirements;
	}
	public void setSpecialRequirements(String specialRequirements) {
		this.specialRequirements = specialRequirements;
	}
}
