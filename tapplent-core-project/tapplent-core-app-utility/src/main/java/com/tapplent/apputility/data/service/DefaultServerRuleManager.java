package com.tapplent.apputility.data.service;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import com.tapplent.apputility.data.controller.FeaturedResponse;
import com.tapplent.apputility.data.structure.*;
import com.tapplent.apputility.entity.structure.PersonExitRule;
import com.tapplent.apputility.layout.structure.PersonTags;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.PersonTagsVO;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.workflow.structure.WorkflowDetails;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.apputility.entity.provider.EntityProvider;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.insert.impl.DefaultInsertData;
import com.tapplent.platformutility.insert.impl.ValidationError;
import com.tapplent.platformutility.metadata.MetadataService;
import com.tapplent.platformutility.search.impl.SearchService;

public class DefaultServerRuleManager implements ServerRuleManager {

	private static final Logger LOG = LogFactory.getLogger(DefaultServerRuleManager.class);

	private MetadataService metadataService;
	private SearchService searchService;
	private EntityProvider entityProvider;
//	private MetaDataMapBuilder metaDataMapBuilder;
	private DataService dataService;
	private DataRequestService dataRequestService;
	private ValidationError validationError;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ResponseData applyServerRulesOnRequest(RequestData requestData) throws Exception {
		ResponseData response = new ResponseData();
		String actionCode;
//		if(requestData.getPreviousActionCodeFkId() != null)
//			actionCode = requestData.getPreviousActionCodeFkId() + "_" + requestData.getActionCodeFkId();
		actionCode = requestData.getActionCodeFkId();
//		ServerActionStep parentActionStep = null;
//		ServerRuleManager accountService = (ServerRuleManager) TapplentApplicationContext.getApplicationContext().getBean(ServerRuleManager.class);
//		String accountServiceClassName = accountService.getClass().getName();
//		System.out.println(accountServiceClassName);
		if (actionCode.contains("NEXT_STEP")) {
			for (UnitRequestData unitRequestData : requestData.getRequestDataList()) {
				UnitResponseData unitResponseData = dataRequestService.getScreen(unitRequestData.getScreen());
				response.getResponseDataList().add(unitResponseData);
			}
		} else {
			Object writeResponse = applyServerRulesOnWriteRequest(requestData, response);
		}

		return response;
	}

	private Object applyServerRulesOnWriteRequest(RequestData requestData, ResponseData response) throws Exception {
		List<UnitRequestData> unitRequestDataList = requestData.getRequestDataList();
		List<UnitRequestData> unitRequestHierarchyDataList = getUnitRequestHierarchyData(requestData);
		RootVariables rootVariables = setRootVariables(requestData);
		for (UnitRequestData unitRequestData : unitRequestHierarchyDataList) {
			PEData rootPEData = unitRequestData.getRootPEData();
			rootVariables.setActionCode(unitRequestData.getRootPEData().getActionCode());//enables ADD_EDIT and DELETE in a single call!
			GlobalVariables globalVariables = initializeGlobalVariables(rootPEData, null, rootVariables);
			Object unitWriteResponseData = processWriteRequestAndGetResponse(rootPEData, globalVariables);
//			response.getResponseDataList().add(unitWriteResponseData);
			globalVariables.insertData.getData().put(globalVariables.entityMetadataVOX.getAttributeByColumnName("LAST_MODIFIED_DATETIME").getDoAttributeCode(), globalVariables.currentTimestampString);
//			response.getWriteResponse().add(globalVariables.insertData.getData());
			response.getWriteResponse().add(unitWriteResponseData);
//			response.getWriteResponse().add(globalVariables.underlyingRecord);
		}
		return response;
	}

	private RootVariables setRootVariables(RequestData requestData) {
		RootVariables rootVariables = new RootVariables();
		rootVariables.setPrimaryLocale(requestData.getPrimaryLocale());
		rootVariables.setUsernameGenKey(requestData.getUsernameGenKey());
		rootVariables.setEmployeeCodeGenKey(requestData.getEmployeeCodeGenKey());
		ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
		String currentTimestampString = utc.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
		rootVariables.setCurrentTimestampString(currentTimestampString);
		return rootVariables;
	}

	private List<UnitRequestData> getUnitRequestHierarchyData(RequestData requestData) {
		List<UnitRequestData> unitRequestDataList = requestData.getRequestDataList();
		List<UnitRequestData> unitRequestHierarchyDataList = new ArrayList<>();
		Map<String, Map<String, UnitRequestData>> mtPEtoPKtoURDataMap = new HashMap<>();
		Map<String, Set<String>> parentMtPEtoFkRelationDOAtoValueMap = new HashMap<>();
		for (UnitRequestData unitRequestData : unitRequestDataList){
			PEData peData = unitRequestData.getRootPEData();
			String parentMtPEAlias = peData.getParentMTPEAlias();
			String fkRelation = peData.getFkRelationWithParent();
			if (fkRelation != null) {
				String[] splits = fkRelation.split("\\|");
				String currentRelationDOA = splits[1];
				if (parentMtPEtoFkRelationDOAtoValueMap.containsKey(parentMtPEAlias)) {
					if (!parentMtPEtoFkRelationDOAtoValueMap.get(parentMtPEAlias).contains(currentRelationDOA)) {
						parentMtPEtoFkRelationDOAtoValueMap.get(parentMtPEAlias).add(currentRelationDOA);
					}
				}
				else {
					Set<String> fkRelationDOASet = new HashSet<>();
					fkRelationDOASet.add(currentRelationDOA);
					parentMtPEtoFkRelationDOAtoValueMap.put(parentMtPEAlias, fkRelationDOASet);
				}
			}
		}
		for (UnitRequestData unitRequestData : unitRequestDataList){
			PEData peData = unitRequestData.getRootPEData();
			String mtPE = peData.getMtPE();
			String mtPEAlias = peData.getMtPEAlias();
			Map<String, Object> data = peData.getData();
			if (parentMtPEtoFkRelationDOAtoValueMap.containsKey(mtPEAlias)){
				for (String relationDOA : parentMtPEtoFkRelationDOAtoValueMap.get(mtPEAlias)){
					String fkValue = (String) data.get(relationDOA);
					if (mtPEtoPKtoURDataMap.containsKey(mtPEAlias)){
						mtPEtoPKtoURDataMap.get(mtPEAlias).put(fkValue, unitRequestData);
					}
					else {
						Map<String, UnitRequestData> dataMap = new HashMap<>();
						dataMap.put(fkValue, unitRequestData);
						mtPEtoPKtoURDataMap.put(mtPEAlias, dataMap);
					}
				}
			}
		}

//		for (UnitRequestData unitRequestData : unitRequestDataList){
//			PEData peData = unitRequestData.getRootPEData();
//			String mtPE = peData.getMtPE();
//			String mtPEAlias = peData.getMtPEAlias();
//			Map<String, Object> data = peData.getData();
//			MasterEntityMetadataVOX masterEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(mtPE);
//			String dOName = masterEntityMetadataVOX.getDomainObjectCode();
//			String pkValue = (String) data.get(dOName+".PrimaryKeyID");
//			if (pkValue != null) {
//				if (mtPEtoPKtoURDataMap.containsKey(mtPEAlias)){
//					mtPEtoPKtoURDataMap.get(mtPEAlias).put(pkValue, unitRequestData);
//				}
//				else {
//					Map<String, UnitRequestData> dataMap = new HashMap<>();
//					dataMap.put(pkValue, unitRequestData);
//					mtPEtoPKtoURDataMap.put(mtPEAlias, dataMap);
//				}
//			}
//		}
		for (UnitRequestData unitRequestData : unitRequestDataList){
			PEData peData = unitRequestData.getRootPEData();
			String parentMTPEAlias = peData.getParentMTPEAlias();
			Map<String, Object> data = peData.getData();
			String fkRelationWithParent = peData.getFkRelationWithParent();
			String parentPrimaryDOAValue = null;
			if (fkRelationWithParent != null && mtPEtoPKtoURDataMap.containsKey(parentMTPEAlias)) {
				String[] splits = fkRelationWithParent.split("\\|");
				String currentRelationDOA = splits[0];
				parentPrimaryDOAValue = (String) data.get(currentRelationDOA);
			}
			if (parentPrimaryDOAValue != null){
				mtPEtoPKtoURDataMap.get(parentMTPEAlias).get(parentPrimaryDOAValue).getRootPEData().getChildrenPEData().add(peData);
			}
			else {
				unitRequestHierarchyDataList.add(unitRequestData);
			}
		}
		return unitRequestHierarchyDataList;
	}

	private UnitWriteResponseData processWriteRequestAndGetResponse(PEData rootPEData, GlobalVariables globalVariables) throws Exception {
		String actionCode = globalVariables.actionCode;
		switch (actionCode){
			case "ADD_EDIT":
				if(!(globalVariables.entityMetadataVOX.getSaveStateAttribute() == null && !globalVariables.isSubmit) || globalVariables.mtPE.equals("WizardDraftValue")) {//AUTOSAVE not allowed for tables having no SAVE_STATE!
					if (globalVariables.isSubmit /*&& !globalVariables.isMakePrimary*/)
						entityProvider.validate(globalVariables);
					entityProvider.operateOnUnderlyingRecord(globalVariables);
				}
				break;
			case "MAKE_PRIMARY":
				entityProvider.operateOnUnderlyingRecord(globalVariables);
				break;
			case "DUPLICATE":
				entityProvider.setRelatedRecordToDuplicate(globalVariables);
				entityProvider.operateOnUnderlyingRecord(globalVariables);
				break;
			case "DELETE":
				UnitWriteResponseData unitWriteResponseData = deleteDataInHierarchy(globalVariables);
				return unitWriteResponseData;
			default:
				entityProvider.validate(globalVariables);
				entityProvider.operateOnUnderlyingRecord(globalVariables);
		}
		List<PEData> childPEList = rootPEData.getChildrenPEData();
		UnitWriteResponseData unitWriteResponseData = new UnitWriteResponseData();
		if(childPEList != null && !childPEList.isEmpty()) {
			List<UnitWriteResponseData> childrenResponseDataList = new ArrayList<>();
			for (PEData childPEData : childPEList) {
				RootVariables childRootVariables = updateRootVariable(globalVariables, childPEData.getActionCode());
//				String childActionCode = childPEData.getActionCode();
				GlobalVariables childGlobalVariables = initializeGlobalVariables(childPEData, globalVariables, childRootVariables);
				UnitWriteResponseData childUnitWriteResponseData = processWriteRequestAndGetResponse(childPEData, childGlobalVariables);
				childrenResponseDataList.add(childUnitWriteResponseData);
			}
			unitWriteResponseData.setChildrenResponseMap(childrenResponseDataList);
		}
		unitWriteResponseData.setResponseMap(globalVariables.insertData.getData());
		unitWriteResponseData.getResponseMap().put("mtPEAlias",globalVariables.mtPEAlias);
		unitWriteResponseData.setPermission(globalVariables.permission);
		if(globalVariables.personTagsList != null){
			unitWriteResponseData.setPersonTagList(getPersonTagList(globalVariables.personTagsList));
		}
		executeEventSpecificActions(globalVariables);
//		unitWriteResponseData.setResponseMap(globalVariables.valueobject.getDoaValueMap());
		return unitWriteResponseData;
	}

	private void executeEventSpecificActions(GlobalVariables globalVariables) throws Exception {
		if (globalVariables.userCreationFlag){
			entityProvider.createUser(globalVariables);
		}
		if (globalVariables.isSubmit && globalVariables.mtPE.equals("PersonJobAssignment")){
			entityProvider.executeEventsForJobAssignment(globalVariables);
		}
		if (globalVariables.isSubmit && globalVariables.mtPE.equals("PersonExit")){
//			Map<String, Object> data = globalVariables.insertData.getData();
//			if (data.get("PersonTxn.EventReason").toString().matches("SEPARATION_VOLUNTARY|SEPARATION_INVOLUNTARY")) {
				entityProvider.inactivateUser(globalVariables);//(not required if inactivaate all related records is called from here) fixme try to do it generically : make a dummy request data
//				String replacementPersonId =
				updateExistingRelationships(globalVariables);
				inactivateAllRelatedRecords(globalVariables);
//			}
		}
	}

	private void inactivateAllRelatedRecords(GlobalVariables globalVariables) throws Exception {
		RequestData requestData = new RequestData();
		requestData.setActionCodeFkId("DELETE");
		PEData rootPEData = new PEData();
		rootPEData.setActionCode("DELETE");
		rootPEData.setDeleteActionType("INACTIVATE");
		rootPEData.setMtPE("Person");
		rootPEData.setSubmit(true);
		rootPEData.setUniqueMerge(true);
		MasterEntityMetadataVOX masterEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE("Person");
		UnitRequestData unitRequestData = new UnitRequestData();
		unitRequestData.setRootPEData(rootPEData);
		List<UnitRequestData> unitRequestDataList = new ArrayList<>();
		unitRequestDataList.add(unitRequestData);
		requestData.setRequestDataList(unitRequestDataList);
		String doName = masterEntityMetadataVOX.getDomainObjectCode();
		Map<String, Object> data = globalVariables.parentInsertData.getData();
		String effectiveDatetime = (String) data.get("PersonTxn.EffectiveDateTime");
		String personId = (String) data.get("PersonTxn.Person");
		Map<String, Object> underlyingPersonRecord = entityProvider.getUnderlyingPersonRecord(personId, masterEntityMetadataVOX);
		String lastModifiedDatetime = (String) underlyingPersonRecord.get("Person.LastModifiedDateTime");
		String versionId = (String) underlyingPersonRecord.get("Person.VersionID");
		String recordStateClient = globalVariables.recordStateClient;
		Map<String, Object> record = new HashMap<>();
		record.put(doName+".EffectiveDateTime", effectiveDatetime);
		record.put(doName+".PrimaryKeyID", personId);
		record.put(doName+".lastModifiedDatetime", lastModifiedDatetime);
		record.put(doName+".VersionID", versionId);
		if (recordStateClient != null && recordStateClient.equals("CURRENT"))
			record.put(doName+".RecordState", recordStateClient);
		rootPEData.setData(record);
		applyServerRulesOnRequest(requestData);
	}

//	private String getReplacementPerson(List<PEData> childPEList) {
//		Optional<PEData> personExitPEData = childPEList.stream().filter(p -> p.getMtPE().equals("PersonExit")).findFirst();
//		if (personExitPEData.isPresent()){
//			personExitPEData.get().getData().
//		}
//	}

	private void updateExistingRelationships(GlobalVariables globalVariables) throws Exception {
		Map<String, Object> personTxnData = globalVariables.parentInsertData.getData();
		String personId = (String) personTxnData.get("PersonTxn.Person");
		String effectiveDatetime = (String) personTxnData.get("PersonTxn.EffectiveDateTime");
		Map<String, Object> exitData = globalVariables.insertData.getData();
		String replacementPersonId = (String) exitData.get("PersonExit.ReplacementPerson");
		String recordStateClient = globalVariables.recordStateClient;
		Map<String, List<PersonExitRule>> criteriaToExitRuleMap = entityProvider.getCriteriaToExitRuleMap();
		Map<String, Map<Integer, List<String>>> allRelationForPerson = entityProvider.getAllRelationsForPerson(personId);
		for (Map.Entry<String, List<PersonExitRule>> entry : criteriaToExitRuleMap.entrySet()){
			for (PersonExitRule personExitRule : entry.getValue()){
				List<Map<String, Object>> recordsToUpdate = entityProvider.getAllRecordsByExitRule(personExitRule, personId);
				if (recordsToUpdate != null && !recordsToUpdate.isEmpty()) {
					if (replacementPersonId == null) {
//						Map<String, Map<Integer, List<String>>> allRelationForPerson = entityProvider.getAllRelationsForPerson(personId);//fixme called from here when cached
						String replacementRelationshipType = personExitRule.getReplaceJobRelationTypeCode();
						Integer replacementRelationshipHierarchy = Integer.valueOf(personExitRule.getReplaceJobRelationHierarchyLevel());
						if (allRelationForPerson.containsKey(replacementRelationshipType) &&
								allRelationForPerson.get(replacementRelationshipType).containsKey(replacementRelationshipHierarchy)) {
							replacementPersonId = allRelationForPerson.get(replacementRelationshipType).get(replacementRelationshipHierarchy).get(0);
						}
					}
					if (replacementPersonId == null)
						throw new Exception("Replacement Person not found!");
					buildRequestAndUpdateExistingRelationships(recordsToUpdate, replacementPersonId, effectiveDatetime, recordStateClient);
				}
				//replace replacing person's direct manager
				if (personExitRule.getExitRuleCodeId().equals("REPL_DIRECT_MGR")){
					if (allRelationForPerson.containsKey("DIRECT_MANAGER")) {
						String replacedPersonDirectManager = allRelationForPerson.get("DIRECT_MANAGER").get(1).get(0);
						if (!replacedPersonDirectManager.equals(replacementPersonId)){
							Map<String, Object> recordToUpdate = entityProvider.getDirectManagerRecord(replacementPersonId);
							List<Map<String, Object>> recordToUpdateList = new ArrayList<>();
							recordToUpdateList.add(recordToUpdate);
							buildRequestAndUpdateExistingRelationships(recordToUpdateList, replacedPersonDirectManager, effectiveDatetime, recordStateClient);
						}
					}
				}
			}
		}
		//Inactivate replaced person's Direct Manager's record
		Map<String, Object> recordToUpdate = entityProvider.getDirectManagerRecord(personId);
		inactivateReplacedPersonRecord(recordToUpdate, effectiveDatetime, recordStateClient);
	}

	private void inactivateReplacedPersonRecord(Map<String, Object> recordToUpdate, String effectiveDatetime, String recordStateClient) throws Exception {
		RequestData requestData = new RequestData();
		requestData.setActionCodeFkId("ADD_EDIT");
		PEData rootPEData = new PEData();
		rootPEData.setActionCode("ADD_EDIT");
		rootPEData.setMtPE("PersonJobRelationship");
		rootPEData.setSubmit(true);
		rootPEData.setUniqueMerge(true);
		MasterEntityMetadataVOX masterEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE("PersonJobRelationship");
		UnitRequestData unitRequestData = new UnitRequestData();
		unitRequestData.setRootPEData(rootPEData);
		List<UnitRequestData> unitRequestDataList = new ArrayList<>();
		unitRequestDataList.add(unitRequestData);
		requestData.setRequestDataList(unitRequestDataList);
		String doName = masterEntityMetadataVOX.getDomainObjectCode();
		recordToUpdate.put(doName+".EffectiveDateTime", effectiveDatetime);
		recordToUpdate.put(doName+".ActiveState", "INACTIVE");
		if (recordStateClient != null && recordStateClient.equalsIgnoreCase("CURRENT"))
			recordToUpdate.put(doName+".RecordState", recordStateClient);
		rootPEData.setData(recordToUpdate);
		applyServerRulesOnRequest(requestData);
	}

	private void buildRequestAndUpdateExistingRelationships(List<Map<String, Object>> recordsToUpdate, String replacementPersonId, String effectiveDatetime, String recordStateClient) throws Exception {
		RequestData requestData = new RequestData();
		requestData.setActionCodeFkId("ADD_EDIT");
		PEData rootPEData = new PEData();
		rootPEData.setActionCode("ADD_EDIT");
		rootPEData.setMtPE("PersonJobRelationship");
		rootPEData.setSubmit(true);
		rootPEData.setUniqueMerge(true);
		MasterEntityMetadataVOX masterEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE("PersonJobRelationship");
		UnitRequestData unitRequestData = new UnitRequestData();
		unitRequestData.setRootPEData(rootPEData);
		List<UnitRequestData> unitRequestDataList = new ArrayList<>();
		unitRequestDataList.add(unitRequestData);
		requestData.setRequestDataList(unitRequestDataList);
		String doName = masterEntityMetadataVOX.getDomainObjectCode();
		for (Map<String, Object> record : recordsToUpdate){
			record.put(doName+".EffectiveDateTime", effectiveDatetime);
			record.put(doName+".RelatedPerson", replacementPersonId);
			if (recordStateClient != null && recordStateClient.equalsIgnoreCase("CURRENT"))
				record.put(doName+".RecordState", recordStateClient);
			rootPEData.setData(record);
			applyServerRulesOnRequest(requestData);
		}
	}

	private RootVariables updateRootVariable(GlobalVariables globalVariables, String actionCode) {
		RootVariables rootVariables = new RootVariables();
		rootVariables.setActionCode(actionCode);
		rootVariables.setPrimaryLocale(globalVariables.primaryLocale);
		rootVariables.setUsernameGenKey(globalVariables.usernameGenKey);
		rootVariables.setEmployeeCodeGenKey(globalVariables.employeeCodeGenKey);
		rootVariables.setCurrentTimestampString(globalVariables.currentTimestampString);
		return rootVariables;
	}

	private List<PersonTags> getPersonTagList(List<PersonTagsVO> personTagsList) {
		List<PersonTags> personTags = new ArrayList<>();
		for (PersonTagsVO personTagsVO : personTagsList){
			PersonTags personTag = new PersonTags();
			BeanUtils.copyProperties(personTagsVO, personTag);
			personTags.add(personTag);
		}
		return personTags;
	}

	private PEData setRequiredDataToDelete(GlobalVariables globalVariables) throws Exception {
		Map<String, Object> data = globalVariables.insertData.getData();
		EntityAttributeMetadata lastModifiedDateTimeMeta = globalVariables.entityMetadataVOX.getAttributeByName(globalVariables.entityMetadataVOX.getDomainObjectCode()+".LastModifiedDateTime");
		EntityAttributeMetadata pkMeta = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
		EntityAttributeMetadata versionMeta = globalVariables.entityMetadataVOX.getVersoinIdAttribute();
		EntityAttributeMetadata effectiveDatetimeMeta = globalVariables.entityMetadataVOX.getEffectiveDatetimeAttribute();
		EntityAttributeMetadata saveStateMeta = globalVariables.entityMetadataVOX.getSaveStateAttribute();
		EntityAttributeMetadata recordStateMeta = globalVariables.entityMetadataVOX.getRecordStateAttribute();
		String pk = (String) data.get(pkMeta.getDoAttributeCode());
		UnitResponseData unitResponseData = null;
		if (globalVariables.setContext)
			unitResponseData = dataService.getRelationDownwardEffect(globalVariables.context, pk, false);
		else unitResponseData = dataService.getRelationDownwardEffect(globalVariables.mtPE, pk, false);
		PEData pEData = new PEData();
		pEData.initialize(pEData, globalVariables);
		for (Map.Entry entry : unitResponseData.getPEData().entrySet()){
			ProcessElementData processElementData = (ProcessElementData) entry.getValue();
			Record record = ((List<Record>) processElementData.getRecordSet()).get(0);
			Map<String, EntityAttributeMetadata> attributeMetaMap = globalVariables.entityMetadataVOX.getAttributeMap();
			Map<String, AttributeData> dataMap = record.getAttributeMap();
			Map<String, DoaMeta> labelAliasMap = unitResponseData.getLabelToAliasMap();
			DoaMeta pkDoaMeta = labelAliasMap.get(pkMeta.getDoAttributeCode());
			DoaMeta lmdDoaMeta = labelAliasMap.get(lastModifiedDateTimeMeta.getDoAttributeCode());
			pEData.setMtPE(processElementData.getMtPE());
//			pEData.getData().putAll(labelAliasMap.entrySet().stream().filter(p ->  p.getKey().startsWith("!{")
//					&& attributeMetaMap.containsKey(p.getKey().replaceAll("[!{|}]","")))
//					.collect(Collectors.toMap(p -> p.getKey().replaceAll("[!{|}]",""), p -> dataMap.get(p.getValue().getAlias()).getcV())));// can't handle null value in
			for (Map.Entry<String, DoaMeta> deleteRecordEntry : labelAliasMap.entrySet()){
				if (deleteRecordEntry.getKey().startsWith("!{") &&
						attributeMetaMap.containsKey(deleteRecordEntry.getKey().replaceAll("[!{|}]",""))){
					pEData.getData().put(deleteRecordEntry.getKey().replaceAll("[!{|}]",""), dataMap.get(deleteRecordEntry.getValue().getAlias()).getcV());
				}
			}
			/*replace all G11n values */
			pEData.getData().putAll(labelAliasMap.entrySet().stream().filter(p ->  p.getKey().endsWith("}G11n")
					&& attributeMetaMap.containsKey(p.getKey().replace("!{","").replace("}G11n", "")))
					.collect(Collectors.toMap(p -> p.getKey().replace("!{","").replace("}G11n", ""), p -> dataMap.get(p.getValue().getAlias()).getcV())));
			pEData.getData().put(pkMeta.getDoAttributeCode(), dataMap.get(pkDoaMeta.getAlias()).getcV());
			pEData.getData().put(lastModifiedDateTimeMeta.getDoAttributeCode(), dataMap.get(lmdDoaMeta.getAlias()).getcV());
			if (globalVariables.entityMetadataVOX.isVersioned()) {
				DoaMeta versionDoaMeta = labelAliasMap.get(versionMeta.getDoAttributeCode());
				pEData.getData().put(versionMeta.getDoAttributeCode(), dataMap.get(versionDoaMeta.getAlias()).getcV());
			}
			if (effectiveDatetimeMeta != null){
				pEData.getData().put(effectiveDatetimeMeta.getDoAttributeCode(), (String) data.get(effectiveDatetimeMeta.getDoAttributeCode()));
			}
			if (saveStateMeta != null){
				DoaMeta saveStateDoaMeta = labelAliasMap.get(saveStateMeta.getDoAttributeCode());
				pEData.getData().put(saveStateMeta.getDoAttributeCode(), dataMap.get(saveStateDoaMeta.getAlias()).getcV());
				String saveState = (String) dataMap.get(saveStateDoaMeta.getAlias()).getcV();
				pEData.setSubmit(isNotDraftDelete(saveState));
			}
			if (globalVariables.recordStateClient != null && globalVariables.recordStateClient.equals("CURRENT") && recordStateMeta != null){
				pEData.getData().put(recordStateMeta.getDoAttributeCode(), globalVariables.recordStateClient);
			}
			setDeleteActionToPEData(globalVariables.entityMetadataVOX, pEData);
			buildDeleteDataInHierarchy(pEData, processElementData, labelAliasMap, globalVariables.entityMetadataVOX);
		}
		return pEData;
	}

	private boolean isNotDraftDelete(String saveState) {
		if (saveState == null || saveState.equalsIgnoreCase("SAVED"))
			return true;
		return false;
	}

	private void setDeleteActionToPEData(EntityMetadataVOX entityMetadataVOX, PEData pEData) throws Exception {
		switch (pEData.getDeleteActionType()){
			case "delete":
			case "DELETE":
				pEData.getData().put(entityMetadataVOX.getDomainObjectCode()+".Deleted", true);
				break;
			case "inactivate":
			case "INACTIVATE":
				pEData.getData().put(entityMetadataVOX.getDomainObjectCode()+".ActiveState", "INACTIVE");
				break;
			case "activate":
			case "ACTIVATE":
				pEData.getData().put(entityMetadataVOX.getDomainObjectCode()+".ActiveState", "ACTIVE");
				break;
			case "restore":
			case "RESTORE":
				pEData.getData().put(entityMetadataVOX.getDomainObjectCode()+".Deleted", false);
				break;
			default: throw new Exception("Delete Action Type not Present! ");
		}
	}

	private void buildDeleteDataInHierarchy(PEData pEData, ProcessElementData processElementData, Map<String, DoaMeta> labelAliasMap, EntityMetadataVOX parentEntityMetadataVOX) throws Exception {
		Record record = ((List<Record>) processElementData.getRecordSet()).get(0);
		List<ProcessElementData> dataList = record.getChildrenPEData();
		String mtPE = null;
		if(dataList != null && !dataList.isEmpty()) {
			List<PEData> childrenPEData = new ArrayList<>();
			for (ProcessElementData data : dataList) {
				if (data.getRecordSet() != null && !((List<Record>) data.getRecordSet()).isEmpty()) {
					mtPE = data.getMtPE();
//				EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(null, mtPE, null);
					String baseTemplateId = null;
					if (baseTemplateId == null) {
						baseTemplateId = TenantAwareCache.getPersonRepository().getDefaultBTForGroup(mtPE, TenantContextHolder.getUserContext().getPersonId());
					}
					EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(baseTemplateId, mtPE, null);
					EntityAttributeMetadata pkMeta = entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
					EntityAttributeMetadata versionMeta = entityMetadataVOX.getVersoinIdAttribute();
					EntityAttributeMetadata lmdMeta = entityMetadataVOX.getAttributeByName(entityMetadataVOX.getDomainObjectCode() + ".LastModifiedDateTime");
					EntityAttributeMetadata effectiveDatetimeMeta = entityMetadataVOX.getEffectiveDatetimeAttribute();
					EntityAttributeMetadata recordStateMeta = entityMetadataVOX.getRecordStateAttribute();
					Record recordInternal = ((List<Record>) data.getRecordSet()).get(0);
					PEData childPEData = new PEData();
					BeanUtils.copyProperties(pEData, childPEData, "data");// see for other properties
					childPEData.setMtPE(mtPE);
					Map<String, AttributeData> dataMap = recordInternal.getAttributeMap();
					DoaMeta pkDoaMeta = labelAliasMap.get(pkMeta.getDoAttributeCode());
					DoaMeta lmdDoaMeta = labelAliasMap.get(lmdMeta.getDoAttributeCode());
					childPEData.getData().put(pkMeta.getDoAttributeCode(), dataMap.get(pkDoaMeta.getAlias()).getcV());
					childPEData.getData().put(lmdMeta.getDoAttributeCode(), dataMap.get(lmdDoaMeta.getAlias()).getcV());
					if (entityMetadataVOX.isVersioned()) {
						DoaMeta versionDoaMeta = labelAliasMap.get(versionMeta.getDoAttributeCode());
						childPEData.getData().put(versionMeta.getDoAttributeCode(), dataMap.get(versionDoaMeta.getAlias()).getcV());
					}
					if (entityMetadataVOX.getEffectiveDatetimeAttribute() != null) {
//					DoaMeta effectivDatetimeDoaMeta = labelAliasMap.get(effectiveDatetimeMeta.getDoAttributeCode());
						childPEData.getData().put(effectiveDatetimeMeta.getDoAttributeCode(), pEData.getData().get(parentEntityMetadataVOX.getDomainObjectCode() + ".EffectiveDateTime"));
					}
					if (entityMetadataVOX.getRecordStateAttribute() != null && pEData.getData().get(pEData.getMtPE()+".RecordState") != null){
						childPEData.getData().put(recordStateMeta.getDoAttributeCode(), pEData.getData().get(pEData.getMtPE()+".RecordState"));
					}
					setDeleteActionToPEData(entityMetadataVOX, childPEData);
					childrenPEData.add(childPEData);
					buildDeleteDataInHierarchy(childPEData, data, labelAliasMap, entityMetadataVOX);
				}
			}
			pEData.setChildrenPEData(childrenPEData);
		}
	}

	private UnitWriteResponseData deleteDataInHierarchy(GlobalVariables globalVariables) throws Exception {
		PEData peData = setRequiredDataToDelete(globalVariables);
		RootVariables rootVariables = updateRootVariable(globalVariables, globalVariables.actionCode);
		globalVariables = initializeGlobalVariables(peData, null, rootVariables);
		UnitWriteResponseData unitDeleteResponseData = processDeleteRequestAndGetResponse(peData, globalVariables);
		return unitDeleteResponseData;
	}

	private UnitWriteResponseData processDeleteRequestAndGetResponse(PEData peData, GlobalVariables globalVariables) throws Exception {
		List<PEData> childPEList = peData.getChildrenPEData();
		UnitWriteResponseData unitWriteResponseData = new UnitWriteResponseData();
		if(childPEList != null && !childPEList.isEmpty()) {
			List<UnitWriteResponseData> childrenResponseDataList = new ArrayList<>();
			for (PEData childPEData : childPEList) {
				if (!childPEData.getMtPE().equals("TaskSynchDetails")) {
					String childActionCode = childPEData.getActionCode();
					RootVariables rootVariables = updateRootVariable(globalVariables, childPEData.getActionCode());
//					GlobalVariables parentGlobalVariables = globalVariables;
					GlobalVariables childGlobalVariables = initializeGlobalVariables(childPEData, null, rootVariables);
					UnitWriteResponseData childUnitWriteResponseData = processDeleteRequestAndGetResponse(childPEData, childGlobalVariables);
					childrenResponseDataList.add(childUnitWriteResponseData);
				}
			}
			unitWriteResponseData.setChildrenResponseMap(childrenResponseDataList);
		}
		entityProvider.operateOnUnderlyingRecord(globalVariables);
		globalVariables.insertData.getData().put(globalVariables.domainObject+".LastModifiedDateTime", globalVariables.currentTimestampString);
		unitWriteResponseData.setResponseMap(globalVariables.insertData.getData());
		return unitWriteResponseData;
	}


	private GlobalVariables initializeGlobalVariables(PEData rootPEData, GlobalVariables parentGlobalVariables, RootVariables rootVariables) throws Exception {
		GlobalVariables globalVariables = new GlobalVariables();
		globalVariables.setContext = rootPEData.isSetContext();
		globalVariables.masterEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(rootPEData.getMtPE());
		globalVariables.currentTimestamp = Timestamp.valueOf(rootVariables.getCurrentTimestampString());
//		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		globalVariables.currentTimestampString = rootVariables.getCurrentTimestampString();
		globalVariables.primaryLocale = rootVariables.getPrimaryLocale();
		globalVariables.actionCode = rootVariables.getActionCode();
		globalVariables.employeeCodeGenKey = rootVariables.getEmployeeCodeGenKey();
		globalVariables.usernameGenKey = rootVariables.getUsernameGenKey();
		if (globalVariables.setContext) {
				globalVariables.context = rootPEData.getContext();
				if (globalVariables.context.equalsIgnoreCase("Tag")){
					String tagPE = globalVariables.masterEntityMetadataVOX.getTagPE();
					String tagBT = TenantAwareCache.getPersonRepository().getDefaultBTForGroup(tagPE, TenantContextHolder.getUserContext().getPersonId());
					globalVariables.entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(tagBT, tagPE, null);
					rootPEData.getData().put("Tag.LoggedInPerson", TenantContextHolder.getUserContext().getPersonId());
				}
				else if (globalVariables.context.equalsIgnoreCase("Bookmark")){
					String bookmarkPE = globalVariables.masterEntityMetadataVOX.getBookmarkPE();
					String bookmarkBT = TenantAwareCache.getPersonRepository().getDefaultBTForGroup(bookmarkPE, TenantContextHolder.getUserContext().getPersonId());
					globalVariables.entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(bookmarkBT, bookmarkPE, null);
					rootPEData.getData().put("Bookmark.LoggedInPerson", TenantContextHolder.getUserContext().getPersonId());
				}
				else if (globalVariables.context.equalsIgnoreCase("Featured")){
					String featuredPE = globalVariables.masterEntityMetadataVOX.getFeaturedPE();
					String bookmarkBT = TenantAwareCache.getPersonRepository().getDefaultBTForGroup(featuredPE, TenantContextHolder.getUserContext().getPersonId());
					globalVariables.entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(bookmarkBT, featuredPE, null);
					rootPEData.getData().put("Featured.FeaturingPerson", TenantContextHolder.getUserContext().getPersonId());
				}
		}
		else {
			if (rootPEData.getBaseTemplateId() != null && rootPEData.getDomainObject() != null)
				globalVariables.entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByBtDo(rootPEData.getBaseTemplateId(), rootPEData.getDomainObject());
			else if (rootPEData.getMtPE() != null || rootPEData.getBtPE() != null)
				if (rootPEData.getBaseTemplateId() == null) {
					String baseTemplateId = TenantAwareCache.getPersonRepository().getDefaultBTForGroup(rootPEData.getMtPE(), TenantContextHolder.getUserContext().getPersonId());
					rootPEData.setBaseTemplateId(baseTemplateId);
				}
				globalVariables.entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(rootPEData.getBaseTemplateId(), rootPEData.getMtPE(), rootPEData.getBtPE());
		}
		globalVariables.insertData = createInsertData(rootPEData, globalVariables);
		globalVariables.btPE = globalVariables.entityMetadataVOX.getBtPE(); //FIXME beware for context!
		globalVariables.mtPE = rootPEData.getMtPE(); //globalVariables.entityMetadataVOX.getMtPE(); entityMetadataVOX changes based on context!
		globalVariables.mtPEAlias = rootPEData.getMtPEAlias();
		globalVariables.actionVisualMasterCode = rootPEData.getActionVisualMasterCode();
		globalVariables.domainObject = globalVariables.entityMetadataVOX.getDomainObjectCode();
		globalVariables.baseTemplateId = globalVariables.entityMetadataVOX.getBaseTemplateId();
		globalVariables.businessRule = rootPEData.getBusinessRule();
		globalVariables.isInline = globalVariables.entityMetadataVOX.isUpdateInline();//rootPEData.isInline();
		globalVariables.compareKeyAttribute = globalVariables.entityMetadataVOX.getCompareKeyAttribute();
		globalVariables.uniqueMerge = rootPEData.isUniqueMerge();
		globalVariables.propogate = rootPEData.isPropagate();
		if (!globalVariables.mtPE.equals("WizardDraftValue"))
			globalVariables.logActivity = rootPEData.isLogActivity();
		globalVariables.parentMetaProcessElement = rootPEData.getParentMetaProcessElement();
		globalVariables.parentDOPrimaryKey = (String) Util.remove0x(rootPEData.getParentDOPrimaryKey());
		globalVariables.parentDOVersionID = (String) Util.remove0x(rootPEData.getParentDOVersionID());
		globalVariables.fkRelationWithParent = rootPEData.getFkRelationWithParent();
		globalVariables.returnMasterTags = rootPEData.isReturnMasterTags();
		globalVariables.duplicateOfAttribute = rootPEData.getDuplicateOfAttribute();
		globalVariables.deleteActionType = rootPEData.getDeleteActionType();
		globalVariables.deleteActionCategory = rootPEData.getDeleteActionCategory();
		globalVariables.model = null;
		globalVariables.relatedUnderlyingRecord = new HashMap<>();
		globalVariables.underlyingRecord = new HashMap<>();
		globalVariables.isUpdate = false;
		globalVariables.validationRequired = false;

		if (globalVariables.entityMetadataVOX.isVersioned()){
			globalVariables.recordStateClient = (String) rootPEData.getData().get(globalVariables.domainObject+".RecordState");
		}

		if (rootPEData.getPersonTag() != null){
			PersonTagsVO personTagsVO = new PersonTagsVO();
			BeanUtils.copyProperties(rootPEData.getPersonTag(), personTagsVO);
			globalVariables.personTag = personTagsVO;
		}
		if(parentGlobalVariables != null)
			globalVariables.parentInsertData = parentGlobalVariables.insertData;
		if(globalVariables.parentInsertData!=null) {
			updateInsertData(globalVariables.insertData, globalVariables.parentInsertData, globalVariables.entityMetadataVOX, globalVariables.fkRelationWithParent);
			globalVariables.parentDOPrimaryKey = parentGlobalVariables.model.getInsertEntityPK().getValue();
			if(parentGlobalVariables.entityMetadataVOX.isVersioned())//fixme discuss condition for updates across versions.
				globalVariables.parentDOVersionID = parentGlobalVariables.model.getInsertEntityVersion().getValue();
		}
		/*
		this should happen only after parent insert data has been set...?
		 */
		entityProvider.setUnderlyingRelatedDataFromDb(globalVariables);
		/*
		for deleting draft record
		 */
		globalVariables.isSubmit = rootPEData.isSubmit() && isNotDraftDelete(globalVariables);
		/*
		update workflow details
		 */
		updateWorkflowDetails(globalVariables);
		if (rootPEData.getWorkflowDetails() != null) {
			globalVariables.workflowDetails.workflowActionId = rootPEData.getWorkflowDetails().workflowActionId;
			globalVariables.workflowDetails.workflowActionComment = rootPEData.getWorkflowDetails().workflowActionComment;
		}
		/*
		For setting User Creation FLAG and generate Employee ID
		*/
		if (globalVariables.mtPE.equals("Person") && globalVariables.isSubmit && !globalVariables.setContext) {
			setFlagForUserCreation(globalVariables, rootPEData);
			if (globalVariables.employeeCodeGenKey != null) {//otherwise person photo tries to generate Employee code.
				String employeeCode = entityProvider.generateEmployeeCode(globalVariables.employeeCodeGenKey, globalVariables.insertData.getData());
				globalVariables.insertData.getData().put("Person.EmployeeCode", employeeCode);
			}
		}
		if (globalVariables.underlyingRecord != null && !globalVariables.underlyingRecord.isEmpty()
				/*&& !globalVariables.actionCode.equals("DELETE")*/){ //to be deleted record set comes with absolute Value(g11n), not JsonObject. so while updating gives format error.
			EntityMetadataVOX metadataVOX = globalVariables.entityMetadataVOX;
			Map<String, EntityAttributeMetadata> standardFieldMap = metadataVOX.getStandardDoaFields();
			Map<String, EntityAttributeMetadata> attributeNameMap = metadataVOX.getAttributeNameMap();
			globalVariables.changedFields = Util.getChangedCurrentFields(globalVariables.underlyingRecord, globalVariables.insertData.getData(), globalVariables.entityMetadataVOX);
			for (Map.Entry<String, EntityAttributeMetadata> attrEntry : attributeNameMap.entrySet()){
				if(!standardFieldMap.containsKey(attrEntry.getKey()) && !globalVariables.insertData.getData().containsKey(attrEntry.getKey())){
					Object underlyingValue = globalVariables.underlyingRecord.get(attrEntry.getKey());
					globalVariables.insertData.getData().putIfAbsent(attrEntry.getKey(), underlyingValue);
				}
			}
		}
		/*
		Set Display Context Value
		 */
		if (globalVariables.entityMetadataVOX.getIsContextObjectField() != null){
			globalVariables.displayContextValue = (String) globalVariables.underlyingRecord.get(globalVariables.entityMetadataVOX.getIsContextObjectField().getDoAttributeCode());
		}
		/*
		maintains uniqueness in isPrimary field for a particular MTPE and person.
		 */
		if (globalVariables.isSubmit && globalVariables.entityMetadataVOX.getPrimary() //&& globalVariables.insertData.getData().get(globalVariables.domainObject+".Primary") != null
				/*&& (boolean) globalVariables.insertData.getData().get(globalVariables.domainObject+".Primary")*/)
			globalVariables.makePrimaryUnique = true;
		return globalVariables;
	}

	private Boolean isNotDraftDelete(GlobalVariables globalVariables) {
		if (globalVariables.entityMetadataVOX.getSaveStateAttribute() == null)
			return true;
		Map<String, Object> underlyingRecord = globalVariables.underlyingRecord;
		Map<String, Object> data = globalVariables.insertData.getData();
		if (underlyingRecord != null && !underlyingRecord.isEmpty() && underlyingRecord.get(globalVariables.domainObject+".SaveState").equals("DRAFT")
				&& data.get(globalVariables.domainObject+".Deleted") != null
				&& (boolean) data.get(globalVariables.domainObject+".Deleted"))
			return false;
		else return true;
	}

	private void updateWorkflowDetails(GlobalVariables globalVariables) {
		globalVariables.workflowDetails = WorkflowDetails.fromUnderlyingRecord(globalVariables.underlyingRecord, globalVariables.domainObject);
	}

	private void setFlagForUserCreation(GlobalVariables globalVariables, PEData rootPEData) {
		if (globalVariables.mtPE.equals("Person") && globalVariables.isSubmit){
			Map<String, Object> data = globalVariables.insertData.getData();
			if (data.containsKey("Person.UserRequired") && (boolean) data.get("Person.UserRequired") &&
				globalVariables.underlyingRecord != null && globalVariables.underlyingRecord.get("Person.SaveState").equals("DRAFT")){
					LOG.debug("############################ userCreationFlag = true ##########################");
					globalVariables.userCreationFlag = true;
//					globalVariables.employeeCodeGenKey = rootPEData.getEmployeeCodeGenKey();
//					globalVariables.usernameGenKey = rootPEData.getUsernameGenKey();
			}
		}
	}

	private void updateInsertData(DefaultInsertData insertData, DefaultInsertData parentInsertData, EntityMetadataVOX entityMetadataVOX, String fkRelationWithParent) {
		Map<String, EntityAttributeMetadata> attributeMetadataMap = entityMetadataVOX.getAttributeNameMap();
		for(Map.Entry<String, EntityAttributeMetadata> attribute : attributeMetadataMap.entrySet()){
			if(attribute.getValue().isRelationFlag()){//fixme is standard required or not?
				Object parentValue = parentInsertData.getData().get(attribute.getValue().getToDoAttributeCode());
				if(parentValue != null)
					insertData.getData().put(attribute.getKey(), parentValue);
			}
		}
		String[] splits = fkRelationWithParent.split("\\|");
		String relationDOAName = splits[0];
		String parentRelationDOAName = splits[1];
		Object parentRelationDOAValue = parentInsertData.getData().get(parentRelationDOAName);
		insertData.getData().put(relationDOAName, parentRelationDOAValue);
	}

	private MetadataObjectRepository getMetadataObjectRepository() {
		return TenantAwareCache.getMetaDataRepository();
	}

	@Override
	@Transactional
	public ResponseData getQueryData(GenericQueryRequest requestData) throws IOException, SQLException {
		ResponseData response = new ResponseData();
		UnitResponseData unitResponseData = dataRequestService.getQueryResult(requestData);
		response.getResponseDataList().add(unitResponseData);
		return response;
	}

	@Override
	@Transactional
	public List<ColleaguesAtWorkResponse> getColleaguesData(String contextID) {
		return dataRequestService.getColleaguesAtWork(contextID);
	}

	@Override
	@Transactional
	public FeaturedResponse getFeaturingPersonDetail(String contextID, String mtPE) {
		MasterEntityMetadataVOX masterEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(mtPE);
		String featuredPE = masterEntityMetadataVOX.getFeaturedPE();
		MasterEntityMetadataVOX featuredEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(featuredPE);
		String featuredTableName = featuredEntityMetadataVOX.getDbTableName();
		return dataRequestService.getFeaturingPersonDetail(contextID, featuredTableName);
	}

	@Override
	public Object getBreakedUpData(String contextType, String contextID) {
		return dataService.getBreakedUpData(contextType, contextID);
	}

	@Override
	@Transactional
	public List<JobRelationships> getDefaultJobRelations(String contextID, String contextID2) {
		return dataService.getDefaultJobRelations(contextID, contextID2);
	}

	@Override
	public String populateReportData(String groupID, String timeZone) throws IOException, SQLException {
		dataRequestService.populateReportData(groupID, timeZone);
		return null;
	}

	@SuppressWarnings("unchecked")
	private DefaultInsertData createInsertData(PEData rootPEData, GlobalVariables globalVariables) throws Exception {
		DefaultInsertData insertData = new DefaultInsertData();
//		Map<String, Object> propogateTrueMap = (Map<String, Object>) rootPEData.getData().get("propogateTrue");
//		Map<String, Object> propogateFalseMap = (Map<String, Object>) rootPEData.getData().get("propogateFalse");
//		if(propogateTrueMap != null || propogateFalseMap != null){
//			Map<String, Object> allInserDataMap = new LinkedHashMap<>();
//			if(propogateTrueMap != null){
//				allInserDataMap.putAll(propogateTrueMap);
//				insertData.setPropogateData(propogateTrueMap);
//			}
//			if(propogateFalseMap != null)
//				allInserDataMap.putAll(propogateFalseMap);
//			insertData.setData(allInserDataMap);
//		}
//		insertData.setData(rootPEData.getData().entrySet().stream().filter(p -> !p.getKey().contains(":")).collect(Collectors.toMap(p-> p.getKey(), p->p.getValue())));
		EntityMetadataVOX entityMetadataVOX = globalVariables.entityMetadataVOX;
		String doName = entityMetadataVOX.getDomainObjectCode();
		String currentTimestampString = globalVariables.currentTimestampString;
		for (Map.Entry<String, Object> entry : rootPEData.getData().entrySet()){
			if (!entry.getKey().contains(":")){
				if (entityMetadataVOX.getAttributeByName(entry.getKey()).getDbDataTypeCode().equals("BOOLEAN")){
					if (entry.getValue() instanceof String){
						if (entry.getValue().equals("0") || ((String) entry.getValue()).equalsIgnoreCase("false"))
							insertData.getData().put(entry.getKey(), false);
						else if (entry.getValue().equals("1") || ((String) entry.getValue()).equalsIgnoreCase("true"))
							insertData.getData().put(entry.getKey(), true);
						else throw new Exception();
					}
					else if (entry.getValue() == null)
						insertData.getData().put(entry.getKey(), false);
					else insertData.getData().put(entry.getKey(), entry.getValue());
				}
//				else if(entry.getKey().equals(doName+".EffectiveDateTime") && ((String)entry.getValue()).equalsIgnoreCase("NOW()")){
//					insertData.getData().put(entry.getKey(), currentTimestampString);
//				}
				else if (entityMetadataVOX.getAttributeByName(entry.getKey()).getDbDataTypeCode().equals("DATE_TIME") && entry.getValue() != null){
					if (entry.getValue().toString().equals(""))
						insertData.getData().put(entry.getKey(), null);
					else insertData.getData().put(entry.getKey(), Util.formatDatetimeString(entry.getValue()));
				}
				else insertData.getData().put(entry.getKey(), entry.getValue());
			}

		}
		insertData.setProcessElementId(rootPEData.getBtPE());
		return insertData;
	}

	private Map<String,Object> removeOxFromIDs(Map<String, Object> data) {
		for (Map.Entry<String, Object> entry : data.entrySet()){
			if(entry.getValue() instanceof String)
				entry.setValue(Util.remove0x(entry.getValue().toString()));
		}
		return data;
	}

	public MetadataService getMetadataService() {
		return metadataService;
	}

	public void setMetadataService(MetadataService metadataService) {
		this.metadataService = metadataService;
	}

	public SearchService getSearchService() {
		return searchService;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

	public EntityProvider getEntityProvider() {
		return entityProvider;
	}

	public void setEntityProvider(EntityProvider entityProvider) {
		this.entityProvider = entityProvider;
	}

	public DataService getDataService() {
		return dataService;
	}
	public void setDataService(DataService dataService) {
		this.dataService = dataService;
	}

	public ValidationError getValidationError() {
		return validationError;
	}

	public void setValidationError(ValidationError validationError) {
		this.validationError = validationError;
	}


	@Override
	public ResponseData applyServerRulesOnETLWriteRequest(RequestData requestData) throws Exception {
		ResponseData response = new ResponseData();
		PEData rootPEData = requestData.getRequestDataList().get(0).getRootPEData();
		RootVariables rootVariables = setRootVariables(requestData);
		ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
		String currentTimestampString = utc.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
//		Timestamp currentTimestamp = Timestamp.valueOf(currentTimestampString);
		GlobalVariables globalVariables = initializeGlobalVariables(rootPEData, null, rootVariables);
		for(UnitRequestData unitRequestData : requestData.getRequestDataList()){
//			entityProvider.validate();
			entityProvider.validate(globalVariables);
			if (validationError.hasError()) {
				validationError.logValidationErrors();
				validationError.clear();
				continue;
//				throw new DataIntegrityViolationException("Validation Error!");
			}
			entityProvider.operateOnUnderlyingRecord(globalVariables);
		}
		return response;
	}

	@Override
	public RelationResponse getRelationControlData(String controlId) {
		return dataService.getRelationControlData(controlId, null);
	}

	@Override
	public G11nResponse getG11nData(String dbPkId, String doaExpn) {
		return dataService.getG11nData(dbPkId, doaExpn);
	}

	@Override
	public UnitResponseData getRelationDownwardEffect(String mtPE, String pk) {
		return dataService.getRelationDownwardEffect(mtPE, pk, false);
	}

	public DataRequestService getDataRequestService() {
		return dataRequestService;
	}

	public void setDataRequestService(DataRequestService dataRequestService) {
		this.dataRequestService = dataRequestService;
	}
}
