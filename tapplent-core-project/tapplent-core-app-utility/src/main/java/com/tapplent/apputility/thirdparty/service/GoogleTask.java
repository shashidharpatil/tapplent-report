package com.tapplent.apputility.thirdparty.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleTask {
	
public static String getDefaultListId(String access_token) throws ClientProtocolException, IOException, JSONException{
	
	HttpGet get = new HttpGet("https://www.googleapis.com/tasks/v1/users/@me/lists");
	get.addHeader("Content-Type", "application/json");
	get.addHeader("Authorization", "Bearer "+access_token);
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(get);
    String json = EntityUtils.toString(response.getEntity());
    JSONObject responseObject = new JSONObject(json);
   JSONArray taskListArray = responseObject.getJSONArray("items");
   JSONObject taskList = taskListArray.getJSONObject(0);
   String defaultListId = taskList.getString("id");
   
    //int status_code = response.getStatusLine().getStatusCode();
	return defaultListId;
}
public static void createNewTask() throws ClientProtocolException, IOException, JSONException{
	String access_token = "ya29.Ci-vA0moKJ4M8uIxwfNjHqeW79rXDsnctoG30_Ug7ydf20XyNpZ9XYiKSpc78DwWXA";
	String defaultTaskList = getDefaultListId(access_token);
	StringBuilder url = new StringBuilder("https://www.googleapis.com/tasks/v1/lists/");
	url.append(defaultTaskList).append("/tasks");
	HttpPost post = new HttpPost(url.toString());
	post.addHeader("Content-Type", "application/json");
	post.addHeader("Authorization", "Bearer "+access_token);
	JSONObject jsonRequestBody = new JSONObject();
	String title = "TAPPLENT_NEW_TASK";
	String notes = "Tapplent's new Task For You";
	jsonRequestBody.put("title", title);
	jsonRequestBody.put("notes", notes);
	StringEntity entity = new StringEntity(jsonRequestBody.toString());
	post.setEntity(entity);
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(post);
    int status_code = response.getStatusLine().getStatusCode();
}
public static void updateTheTask() throws ClientProtocolException, IOException, JSONException{
	//https://www.googleapis.com/tasks/v1/lists/tasklist/tasks/task
String access_token = "ya29.Ci-vA_NWJH7--7aPHku2XS3Jv71TVVIpmwT218_izmQX8enpCYv1z-LzlUc174NWrg";
String taskList = "MDQ4OTkzMDY1MDA5Nzc4Mjc0Mjk6MDow";
String task = "MDQ4OTkzMDY1MDA5Nzc4Mjc0Mjk6MDo3NDA0ODQxNTk";
StringBuilder url = new StringBuilder("https://www.googleapis.com/tasks/v1/lists/");
url.append(taskList);
HttpPut put = new HttpPut(url.toString());
put.addHeader("Content-Type", "application/json");
put.addHeader("Authorization", "Bearer "+access_token);
JSONObject jsonRequestBody = new JSONObject();
String title = "TAPPLENT_TASK";
String notes = "New Task For You";
jsonRequestBody.put("title", title);
jsonRequestBody.put("notes", notes);
StringEntity entity = new StringEntity(jsonRequestBody.toString());
put.setEntity(entity);
HttpClient client = HttpClientBuilder.create().build();
HttpResponse response;
response = client.execute(put);
int status_code = response.getStatusLine().getStatusCode();	
}
public static void deleteTheTask() throws ClientProtocolException, IOException{
	//https://www.googleapis.com/tasks/v1/lists/tasklist/tasks/task
String access_token = "ya29.Ci-vA_NWJH7--7aPHku2XS3Jv71TVVIpmwT218_izmQX8enpCYv1z-LzlUc174NWrg";
String taskId = "MDQ4OTkzMDY1MDA5Nzc4Mjc0Mjk6MDo3NDA0ODQxNTk";
String taskList = "MDQ4OTkzMDY1MDA5Nzc4Mjc0Mjk6MDow";
StringBuilder url = new StringBuilder("https://www.googleapis.com/tasks/v1/lists/");
url.append(taskList).append("/tasks/").append(taskId);
HttpDelete delete = new HttpDelete(url.toString());
delete.addHeader("Content-Type", "application/json");
delete.addHeader("Authorization", "Bearer "+access_token);
HttpClient client = HttpClientBuilder.create().build();
HttpResponse response;
response = client.execute(delete);
int status_code = response.getStatusLine().getStatusCode();	
}
}
