package com.tapplent.apputility.data.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.common.util.AttachmentObject;

import java.net.URL;

public class AttributeData {
	//G11n
	//object map of locale and value
	private Object oV;
	private Object cV;
	private Boolean isRP;
	private Boolean isUP;
	private AttachmentObject URL;
	private String sUTZ;
	@JsonIgnore
	private String rowAccessPkId;
	@JsonIgnore
	private String columnLabel;
	@JsonIgnore
	private boolean internal;
	public Object getoV() {
		return oV;
	}
	public void setoV(Object oV) {
		this.oV = oV;
	}
	public Object getcV() {
		return cV;
	}
	public void setcV(Object cV) {
		this.cV = cV;
	}

	public Boolean getRP() {
		return isRP;
	}

	public void setRP(Boolean RP) {
		isRP = RP;
	}

	public Boolean getUP() {
		return isUP;
	}

	public void setUP(Boolean UP) {
		isUP = UP;
	}

	public AttachmentObject getURL() {
		return URL;
	}

	public void setURL(AttachmentObject URL) {
		this.URL = URL;
	}

	public String getsUTZ() {
		return sUTZ;
	}

	public void setsUTZ(String sUTZ) {
		this.sUTZ = sUTZ;
	}

	public String getRowAccessPkId() {
		return rowAccessPkId;
	}

	public void setRowAccessPkId(String rowAccessPkId) {
		this.rowAccessPkId = rowAccessPkId;
	}

	public String getColumnLabel() {
		return columnLabel;
	}

	public void setColumnLabel(String columnLabel) {
		this.columnLabel = columnLabel;
	}

	public boolean isInternal() {
		return internal;
	}

	public void setInternal(boolean internal) {
		this.internal = internal;
	}
}
