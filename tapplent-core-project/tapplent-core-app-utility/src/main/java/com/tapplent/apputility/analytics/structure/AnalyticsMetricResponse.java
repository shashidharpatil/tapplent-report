package com.tapplent.apputility.analytics.structure;

import com.tapplent.platformutility.uilayout.valueobject.ScreenInstanceVO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 09/12/16.
 */
public class AnalyticsMetricResponse {
    private ScreenInstanceVO screenInstance;
    private List<AnalyticsMetricDetailsResponse> detailsResponseList = new ArrayList<>();

    public List<AnalyticsMetricDetailsResponse> getDetailsResponseList() {
        return detailsResponseList;
    }

    public void setDetailsResponseList(List<AnalyticsMetricDetailsResponse> detailsResponseList) {
        this.detailsResponseList = detailsResponseList;
    }

    public ScreenInstanceVO getScreenInstance() {
        return screenInstance;
    }

    public void setScreenInstance(ScreenInstanceVO screenInstance) {
        this.screenInstance = screenInstance;
    }
}
