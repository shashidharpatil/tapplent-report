package com.tapplent.apputility.uilayout.service;


import com.tapplent.apputility.data.structure.GenericQuery;
import com.tapplent.apputility.uilayout.structure.LocationBreakUpData;
import com.tapplent.apputility.data.structure.JobRelationships;
import com.tapplent.apputility.uilayout.structure.ActionTarget;
import com.tapplent.apputility.uilayout.structure.OrganizationBreakUpData;
import com.tapplent.apputility.uilayout.structure.ScreenInstance;
import com.tapplent.apputility.uilayout.structure.*;
import com.tapplent.platformutility.accessmanager.structure.ResolvedColumnCondition;
import com.tapplent.platformutility.conversation.valueobject.PersonDetailsVO;
import com.tapplent.platformutility.layout.valueObject.JobRelationshipTypeVO;
import com.tapplent.platformutility.layout.valueObject.StateVO;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by sripat on 01/11/16.
 */
public interface UILayoutService {

    public ScreenInstance getLayout(List<String> targetSectionInstanceList, String deviceType, String subjectUserID);

    public ScreenInstance getLayout(String targetScreenInstance, List<String> targetSectionInstanceList, String deviceType, String subjectUserID);
//    public List<LoadParameters> getLoadParameters(String callFor, String fkId, PersonGroup group);
//
////    String getDefaultBTForGroup(String mtPECode,PersonGroup group);
//
//    String getSectionMasterCode(String targetSectionInstance);
//
//    SectionControlVO getControlData(String controlId);
//
//    Map<Integer, SectionLayoutMapper> getSectionLayoutMapper(String mtPE, String uiDispTypeCodeFkId, String tabSeqNumPosInt);
//
    String getScreenInstanceFromSection(String s);

    Map<String,Map<String,ResolvedColumnCondition>> getColumnConditions(String personId, Set<String> strings);

    String getScreenId(String app_menu_scrn);

    ActionTarget getActionTarget(String actionTargetPkId, String whatFor);

    GenericQuery getGenericQuery(String queryID);

    List<String> getAllPersonIDs();

    Map<String,StateVO> getActiveStateIcons();

    Map<String,StateVO> getRecordStateIcons();

    Map<String,StateVO> getSavedStateIcons();

    List<JobRelationshipTypeVO> getJobRelationshipTypes();

    List<PersonDetailsVO> getFeaturingPersonDetail(String contextID, String featuredTableName);

    List<OrganizationBreakUpData> getOrganizationBreakUpData(String contextID);

    List<LocationBreakUpData> getLocationBreakUpData(String contextID);

    List<JobRelationships> getDefaultJobRelations(String contextID1, String contextID2);

    List<CostCentreBreakUpData> getCostCentreBreakUpData(String contextID);

    List<JobFamilyBreakUpData> getJobFamilyBreakUpData(String contextID);

    String getReportingSectionID();
}
