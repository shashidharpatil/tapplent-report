package com.tapplent.apputility.data.controller;

import java.io.IOException;

import java.sql.SQLException;
import java.util.*;

import com.tapplent.apputility.data.structure.*;
import com.tapplent.platformutility.Exception.TapplentException;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.insert.impl.ValidationError;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tapplent.apputility.data.service.ServerRuleManager;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;

@Controller
@RequestMapping("tapp/data/v1")
public class DataController {
	private static final Logger LOG = LogFactory.getLogger(DataController.class);
	private ServerRuleManager serverRuleManager;
	private ValidationError validationError;
	@RequestMapping(value="data/t/{tenantId}/u/{personId}/e/", method=RequestMethod.POST)
	@ResponseBody
	ResponseData getData(@RequestHeader("access_token") String token,
			@PathVariable("tenantId") String tenantId,
			@PathVariable("personId") String userName,
			@RequestParam("personName") String personName,
			@RequestParam("timeZone") String timeZone,
			@RequestBody RequestData requestData) throws IOException, SQLException{
		try {
			if(!Util.isValidTenant()){
				throw new TapplentException("Not Valid Tenant!");
			}
			else if (!Util.validateAccessTokenAndSetUserContext(token)){
				throw new TapplentException("Token not valid!");
			}
			else return serverRuleManager.applyServerRulesOnRequest(requestData);
		}catch (TapplentException e){
			e.printStackTrace();
			ResponseData error = getErrorLogResponseData();
			List<String> errorList = new ArrayList<>();
			errorList.add(TenantContextHolder.getCurrentTenantID());
			error.getErrorLog().put("Not Valid Tenant", errorList);
			return error;
		}
		catch (Exception e) {
			e.printStackTrace();
			ResponseData error = getErrorLogResponseData();
			if (error.getErrorLog().isEmpty()) {
				List<String> unhandledExceptionList = new ArrayList<>();
				unhandledExceptionList.add(ExceptionUtils.getStackTrace(e));
				error.getErrorLog().put("UnhandledException", unhandledExceptionList);
			}
			return error;
		}
//		return dataService.operateOnData(requestData);
	}
	@RequestMapping(value="genQuery/t/{tenantId}/u/{personId}/e/", method=RequestMethod.POST)
	@ResponseBody
	ResponseData getQueryData(@RequestHeader("access_token") String token,
						 @PathVariable("tenantId") String tenantId,
						 @PathVariable("personId") String userName,
						 @RequestParam("personName") String personName,
						 @RequestParam("timeZone") String timeZone,
						 @RequestBody GenericQueryRequest queryRequest) throws IOException, SQLException{
		try {
			return serverRuleManager.getQueryData(queryRequest);
		} catch (Exception e) {
			e.printStackTrace();
			return getErrorLogResponseData();
		}
//		return dataService.operateOnData(requestData);
//		return null;
	}

	@RequestMapping(value="colleagues/t/{tenantId}/u/{personId}/e/", method=RequestMethod.GET)
	@ResponseBody
	List<ColleaguesAtWorkResponse> getColleaguesData(@RequestHeader("access_token") String token,
											   @PathVariable("tenantId") String tenantId,
											   @PathVariable("personId") String userName,
											   @RequestParam("contextID") String contextID) throws IOException, SQLException{
		try {
			return serverRuleManager.getColleaguesData(contextID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value="defaultJobRelations/t/{tenantId}/u/{personId}/e/", method=RequestMethod.GET)
	@ResponseBody
	List<JobRelationships> getDefaultJobRelations(@RequestHeader("access_token") String token,
													 @PathVariable("tenantId") String tenantId,
													 @PathVariable("personId") String userName,
													 @RequestParam("contextID1") String contextID1,
												  	@RequestParam("contextID2") String contextID2) throws IOException, SQLException{
		try {
			return serverRuleManager.getDefaultJobRelations(contextID1, contextID2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value="breakup/t/{tenantId}/u/{personId}/e/", method=RequestMethod.GET)
	@ResponseBody
	Object getColleaguesData(@RequestHeader("access_token") String token,
								        			 @RequestParam("controlType") String contextType,
													 @RequestParam("contextID") String contextID) throws IOException, SQLException{
		try {
			return serverRuleManager.getBreakedUpData(contextType,contextID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="report/t/{tenantId}/u/{personId}/e/", method=RequestMethod.GET)
	@ResponseBody
	String populateReportData(@RequestHeader("access_token") String token,
							  @RequestParam("groupID") String groupID,
							  @RequestParam("timeZone") String timeZone) throws IOException, SQLException{
		try {
			return serverRuleManager.populateReportData(groupID, timeZone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Report data has been successfully populated.";
	}

	@RequestMapping(value="relation/t/{tenantId}/u/{personId}/e/", method=RequestMethod.GET)
	@ResponseBody
	RelationResponse getRelationControlData(@RequestHeader("access_token") String token,
											@PathVariable("tenantId") String tenantId,
											@PathVariable("personId") String userName,
											@RequestParam("personName") String personName,
											@RequestParam("timeZone") String timeZone,
											@RequestParam("controlId") String controlId) throws IOException, SQLException{
		try {
			return serverRuleManager.getRelationControlData(controlId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value="g11n/t/{tenantId}/u/{personId}/e/", method=RequestMethod.GET)
	@ResponseBody
	G11nResponse getG11nData(@RequestHeader("access_token") String token,
							 @PathVariable("tenantId") String tenantId,
							 @PathVariable("personId") String userName,
							 @RequestParam("dbPkId") String dbPkId,
							 @RequestParam("doaExpn") String doaExpn) throws IOException, SQLException{
		try {
			return serverRuleManager.getG11nData(dbPkId, doaExpn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping(value="relnEffect/t/{tenantId}/u/{personId}/e/", method=RequestMethod.GET)
	@ResponseBody
	UnitResponseData getRelationDownwardEffect(@RequestHeader("access_token") String token,
											@PathVariable("tenantId") String tenantId,
											@PathVariable("personId") String userName,
											@RequestParam("mtPE") String mtPE,
											@RequestParam("pk") String pk) throws IOException, SQLException{
		try {
			return serverRuleManager.getRelationDownwardEffect(mtPE, pk);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="featured/t/{tenantId}/u/{personId}/e/", method=RequestMethod.GET)
	@ResponseBody
	FeaturedResponse getFeaturingPersonDetail(@RequestHeader("access_token") String token,
													 @PathVariable("tenantId") String tenantId,
													 @PathVariable("personId") String userName,
													 @RequestParam("contextID") String contextID,
										   			 @RequestParam("mtPE") String mtPE) throws IOException, SQLException{
		try {
			return serverRuleManager.getFeaturingPersonDetail(contextID, mtPE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ServerRuleManager getServerRuleManager() {
		return serverRuleManager;
	}
	public void setServerRuleManager(ServerRuleManager serverRuleManager) {
		this.serverRuleManager = serverRuleManager;
	}

	public ResponseData getErrorLogResponseData() {
		ResponseData responseData = new ResponseData();
		Map<String, List<String>> errorsMap = new HashMap<>();
		errorsMap.putAll(validationError.getErrors());
		responseData.setErrorLog(errorsMap);
		validationError.clear();
		return responseData;
	}

	public ValidationError getValidationError() {
		return validationError;
	}

	public void setValidationError(ValidationError validationError) {
		this.validationError = validationError;
	}

	//	@RequestMapping(value="data/t/{tenantId}/u/{personId}/d/{doName}/e/", method=RequestMethod.POST)
//	void indexData(@PathVariable("tenantId") String tenantId,
//			@PathVariable("personId") String userName,
//			@PathVariable("doName") String doName){
//		serverRuleManager.indexData(doName);
//	}
	
}
