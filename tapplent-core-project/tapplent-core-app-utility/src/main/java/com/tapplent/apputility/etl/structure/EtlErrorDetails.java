package com.tapplent.apputility.etl.structure;

public class EtlErrorDetails {
//	VERSION ID                         
//	ETL ERROR DETAILS PK ID            
//	ETL SUMMARY LOG FK ID              
//	SOURCE BT DO FK ID                 
//	SOURCE RECORD ID                   
//	ERROR DESCRIPTION TXT       
	private String versionId;
	private String etlErrorDetailsPkId;
	private String etlSummaryLogFkId;
	private String sourceBtDoFkId;
	private String sourceRecordId;
	private String errorDescriptionTxt;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getEtlErrorDetailsPkId() {
		return etlErrorDetailsPkId;
	}
	public void setEtlErrorDetailsPkId(String etlErrorDetailsPkId) {
		this.etlErrorDetailsPkId = etlErrorDetailsPkId;
	}
	public String getEtlSummaryLogFkId() {
		return etlSummaryLogFkId;
	}
	public void setEtlSummaryLogFkId(String etlSummaryLogFkId) {
		this.etlSummaryLogFkId = etlSummaryLogFkId;
	}
	public String getSourceBtDoFkId() {
		return sourceBtDoFkId;
	}
	public void setSourceBtDoFkId(String sourceBtDoFkId) {
		this.sourceBtDoFkId = sourceBtDoFkId;
	}
	public String getSourceRecordId() {
		return sourceRecordId;
	}
	public void setSourceRecordId(String sourceRecordId) {
		this.sourceRecordId = sourceRecordId;
	}
	public String getErrorDescriptionTxt() {
		return errorDescriptionTxt;
	}
	public void setErrorDescriptionTxt(String errorDescriptionTxt) {
		this.errorDescriptionTxt = errorDescriptionTxt;
	}
}
