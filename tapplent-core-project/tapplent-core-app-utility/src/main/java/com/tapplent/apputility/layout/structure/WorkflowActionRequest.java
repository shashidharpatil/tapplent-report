package com.tapplent.apputility.layout.structure;

public class WorkflowActionRequest {
    private String workflowTxnId;

    public String getWorkflowTxnId() {
        return workflowTxnId;
    }

    public void setWorkflowTxnId(String workflowTxnId) {
        this.workflowTxnId = workflowTxnId;
    }
}
