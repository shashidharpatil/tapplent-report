package com.tapplent.apputility.uilayout.structure;

import com.tapplent.platformutility.uilayout.valueobject.OrganizationBreakUpDataVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 27/09/17.
 */
public class OrganizationBreakUpData {
    private String orgPkId;
    private String orgName;
    private String orgType;
    private String orgTypeName;

    public String getOrgPkId() {
        return orgPkId;
    }

    public void setOrgPkId(String orgPkId) {
        this.orgPkId = orgPkId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getOrgTypeName() {
        return orgTypeName;
    }

    public void setOrgTypeName(String orgTypeName) {
        this.orgTypeName = orgTypeName;
    }

    public static OrganizationBreakUpData fromVO(OrganizationBreakUpDataVO organizationBreakUpDataVO) {
        OrganizationBreakUpData organizationBreakUpData = new OrganizationBreakUpData();
        BeanUtils.copyProperties(organizationBreakUpDataVO, organizationBreakUpData);
        return organizationBreakUpData;
    }
}
