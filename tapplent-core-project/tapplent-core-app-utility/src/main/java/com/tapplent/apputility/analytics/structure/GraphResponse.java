package com.tapplent.apputility.analytics.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.apputility.data.structure.UnitResponseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 23/08/17.
 */
public class GraphResponse {
    private String metricInfo;
    private String metricFormula;
    private String bestPracticeRecommendation;
    private String chartTitle;
    private String timeXAxisTitle;
    private String xAxisTitle;
    private String yAxis1Title;
    private String yAxis2Title;
    private String isLegendVisible;
    private String legendDisplayPosition;
    private boolean isDrillDownable;
    private boolean isTableAvailable;
    private String parentAnalyticsMetricDetailID;
    @JsonIgnore
    private String parentDrilldownAnalyticsMetricDetailFkId;
    @JsonIgnore
    private AnalyticsMetricDetailsResponse minMetricDetails;
    @JsonIgnore
    private AnalyticsMetricDetailsResponse midMetricDetails;
    @JsonIgnore
    private AnalyticsMetricDetailsResponse maxMetricDetails;
    @JsonIgnore
    private String relatedTabularAnalyticsMetricDetailFkId;
    @JsonIgnore
    private String relatedFilterReceipientDoaCodeFkId;
    @JsonIgnore
    private UnitResponseData yAxisData;
    @JsonIgnore
    private List<XAxisData> xAxisData;
    @JsonIgnore
    private List<XAxisData> subXAxisData;
    @JsonIgnore
    private UnitResponseData minYAxisData;
    @JsonIgnore
    private UnitResponseData avgYAxisData;
    @JsonIgnore
    private UnitResponseData maxYAxisData;
    @JsonIgnore
    private UnitResponseData tableData;
    @JsonIgnore
    private Map<String, Object> dataSeriesMap = new HashMap<>();
    private List<Object> dataSeries = new ArrayList<>();
    public String getMetricInfo() {
        return metricInfo;
    }

    public void setMetricInfo(String metricInfo) {
        this.metricInfo = metricInfo;
    }

    public String getMetricFormula() {
        return metricFormula;
    }

    public void setMetricFormula(String metricFormula) {
        this.metricFormula = metricFormula;
    }

    public String getBestPracticeRecommendation() {
        return bestPracticeRecommendation;
    }

    public void setBestPracticeRecommendation(String bestPracticeRecommendation) {
        this.bestPracticeRecommendation = bestPracticeRecommendation;
    }

    public String getChartTitle() {
        return chartTitle;
    }

    public void setChartTitle(String chartTitle) {
        this.chartTitle = chartTitle;
    }

    public String getTimeXAxisTitle() {
        return timeXAxisTitle;
    }

    public void setTimeXAxisTitle(String timeXAxisTitle) {
        this.timeXAxisTitle = timeXAxisTitle;
    }

    public String getxAxisTitle() {
        return xAxisTitle;
    }

    public void setxAxisTitle(String xAxisTitle) {
        this.xAxisTitle = xAxisTitle;
    }

    public String getyAxis1Title() {
        return yAxis1Title;
    }

    public void setyAxis1Title(String yAxis1Title) {
        this.yAxis1Title = yAxis1Title;
    }

    public String getyAxis2Title() {
        return yAxis2Title;
    }

    public void setyAxis2Title(String yAxis2Title) {
        this.yAxis2Title = yAxis2Title;
    }

    public String getIsLegendVisible() {
        return isLegendVisible;
    }

    public void setIsLegendVisible(String isLegendVisible) {
        this.isLegendVisible = isLegendVisible;
    }

    public String getLegendDisplayPosition() {
        return legendDisplayPosition;
    }

    public void setLegendDisplayPosition(String legendDisplayPosition) {
        this.legendDisplayPosition = legendDisplayPosition;
    }

    public boolean isDrillDownable() {
        return isDrillDownable;
    }

    public void setDrillDownable(boolean drillDownable) {
        isDrillDownable = drillDownable;
    }

    public boolean isTableAvailable() {
        return isTableAvailable;
    }

    public void setTableAvailable(boolean tableAvailable) {
        isTableAvailable = tableAvailable;
    }

    public String getParentAnalyticsMetricDetailID() {
        return parentAnalyticsMetricDetailID;
    }

    public void setParentAnalyticsMetricDetailID(String parentAnalyticsMetricDetailID) {
        this.parentAnalyticsMetricDetailID = parentAnalyticsMetricDetailID;
    }

    public String getParentDrilldownAnalyticsMetricDetailFkId() {
        return parentDrilldownAnalyticsMetricDetailFkId;
    }

    public void setParentDrilldownAnalyticsMetricDetailFkId(String parentDrilldownAnalyticsMetricDetailFkId) {
        this.parentDrilldownAnalyticsMetricDetailFkId = parentDrilldownAnalyticsMetricDetailFkId;
    }

    public AnalyticsMetricDetailsResponse getMinMetricDetails() {
        return minMetricDetails;
    }

    public void setMinMetricDetails(AnalyticsMetricDetailsResponse minMetricDetails) {
        this.minMetricDetails = minMetricDetails;
    }

    public AnalyticsMetricDetailsResponse getMidMetricDetails() {
        return midMetricDetails;
    }

    public void setMidMetricDetails(AnalyticsMetricDetailsResponse midMetricDetails) {
        this.midMetricDetails = midMetricDetails;
    }

    public AnalyticsMetricDetailsResponse getMaxMetricDetails() {
        return maxMetricDetails;
    }

    public void setMaxMetricDetails(AnalyticsMetricDetailsResponse maxMetricDetails) {
        this.maxMetricDetails = maxMetricDetails;
    }

    public String getRelatedTabularAnalyticsMetricDetailFkId() {
        return relatedTabularAnalyticsMetricDetailFkId;
    }

    public void setRelatedTabularAnalyticsMetricDetailFkId(String relatedTabularAnalyticsMetricDetailFkId) {
        this.relatedTabularAnalyticsMetricDetailFkId = relatedTabularAnalyticsMetricDetailFkId;
    }

    public String getRelatedFilterReceipientDoaCodeFkId() {
        return relatedFilterReceipientDoaCodeFkId;
    }

    public void setRelatedFilterReceipientDoaCodeFkId(String relatedFilterReceipientDoaCodeFkId) {
        this.relatedFilterReceipientDoaCodeFkId = relatedFilterReceipientDoaCodeFkId;
    }

    public UnitResponseData getyAxisData() {
        return yAxisData;
    }

    public void setyAxisData(UnitResponseData yAxisData) {
        this.yAxisData = yAxisData;
    }

    public List<XAxisData> getxAxisData() {
        return xAxisData;
    }

    public void setxAxisData(List<XAxisData> xAxisData) {
        this.xAxisData = xAxisData;
    }

    public List<XAxisData> getSubXAxisData() {
        return subXAxisData;
    }

    public void setSubXAxisData(List<XAxisData> subXAxisData) {
        this.subXAxisData = subXAxisData;
    }

    public UnitResponseData getMinYAxisData() {
        return minYAxisData;
    }

    public void setMinYAxisData(UnitResponseData minYAxisData) {
        this.minYAxisData = minYAxisData;
    }

    public UnitResponseData getAvgYAxisData() {
        return avgYAxisData;
    }

    public void setAvgYAxisData(UnitResponseData avgYAxisData) {
        this.avgYAxisData = avgYAxisData;
    }

    public UnitResponseData getMaxYAxisData() {
        return maxYAxisData;
    }

    public void setMaxYAxisData(UnitResponseData maxYAxisData) {
        this.maxYAxisData = maxYAxisData;
    }

    public UnitResponseData getTableData() {
        return tableData;
    }

    public void setTableData(UnitResponseData tableData) {
        this.tableData = tableData;
    }

    public Map<String, Object> getDataSeriesMap() {
        return dataSeriesMap;
    }

    public void setDataSeriesMap(Map<String, Object> dataSeriesMap) {
        this.dataSeriesMap = dataSeriesMap;
    }

    public List<Object> getDataSeries() {
        return dataSeries;
    }

    public void setDataSeries(List<Object> dataSeries) {
        this.dataSeries = dataSeries;
    }
}
