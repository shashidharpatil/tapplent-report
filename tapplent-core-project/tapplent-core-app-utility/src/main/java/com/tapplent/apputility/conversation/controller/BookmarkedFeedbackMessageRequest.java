package com.tapplent.apputility.conversation.controller;

public class BookmarkedFeedbackMessageRequest {
    private String feedbackGroupId;
    private int limit;
    private int offset;

    public String getFeedbackGroupId() {
        return feedbackGroupId;
    }

    public void setFeedbackGroupId(String feedbackGroupId) {
        this.feedbackGroupId = feedbackGroupId;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
