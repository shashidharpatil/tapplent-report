package com.tapplent.apputility.analytics.structure;

import com.tapplent.platformutility.analytics.structure.AnalyticsMetricChartVO;
import com.tapplent.platformutility.common.util.Util;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 23/08/17.
 */
public class AnalyticsMetricChart {
    private String chartPkID;
    private String metricInfo;
    private String metricFormula;
    private String bestPracticeRecommendation;
    private String chartTitle;
    private String timeXAxisTitle;
    private String xAxisTitle;
    private String yAxis1Title;
    private String yAxis2Title;
    private String isLegendVisible;
    private String legendDisplayPosition;
    private boolean isDrillDownable;
    private boolean isTableAvailable;
    private String parentAnalyticsMetricDetailID;
    private List<AnalyticsMetricChartQuery> analyticsMetricChartQueries = new ArrayList<>();
    private List<AnalyticsMetricChartFilters> analyticsMetricChartFilters = new ArrayList<>();

    public String getChartPkID() {
        return chartPkID;
    }

    public void setChartPkID(String chartPkID) {
        this.chartPkID = chartPkID;
    }

    public String getMetricInfo() {
        return Util.getG11nValue(metricInfo, null);
    }

    public void setMetricInfo(String metricInfo) {
        this.metricInfo = metricInfo;
    }

    public String getMetricFormula() {
        return Util.getG11nValue(metricFormula, null);
    }

    public void setMetricFormula(String metricFormula) {
        this.metricFormula = metricFormula;
    }

    public String getBestPracticeRecommendation() {
        return Util.getG11nValue(bestPracticeRecommendation, null);
    }

    public void setBestPracticeRecommendation(String bestPracticeRecommendation) {
        this.bestPracticeRecommendation = bestPracticeRecommendation;
    }

    public String getChartTitle() {
        return Util.getG11nValue(chartTitle, null);
    }

    public void setChartTitle(String chartTitle) {
        this.chartTitle = chartTitle;
    }

    public String getTimeXAxisTitle() {
        return Util.getG11nValue(timeXAxisTitle, null);
    }

    public void setTimeXAxisTitle(String timeXAxisTitle) {
        this.timeXAxisTitle = timeXAxisTitle;
    }

    public String getxAxisTitle() {
        return Util.getG11nValue(xAxisTitle, null);
    }

    public void setxAxisTitle(String xAxisTitle) {
        this.xAxisTitle = xAxisTitle;
    }

    public String getyAxis1Title() {
        return Util.getG11nValue(yAxis1Title, null);
    }

    public void setyAxis1Title(String yAxis1Title) {
        this.yAxis1Title = yAxis1Title;
    }

    public String getyAxis2Title() {
        return Util.getG11nValue(yAxis2Title, null);
    }

    public void setyAxis2Title(String yAxis2Title) {
        this.yAxis2Title = yAxis2Title;
    }

    public String getIsLegendVisible() {
        return isLegendVisible;
    }

    public void setIsLegendVisible(String isLegendVisible) {
        this.isLegendVisible = isLegendVisible;
    }

    public String getLegendDisplayPosition() {
        return legendDisplayPosition;
    }

    public void setLegendDisplayPosition(String legendDisplayPosition) {
        this.legendDisplayPosition = legendDisplayPosition;
    }

    public boolean isDrillDownable() {
        return isDrillDownable;
    }

    public void setDrillDownable(boolean drillDownable) {
        isDrillDownable = drillDownable;
    }

    public boolean isTableAvailable() {
        return isTableAvailable;
    }

    public void setTableAvailable(boolean tableAvailable) {
        isTableAvailable = tableAvailable;
    }

    public String getParentAnalyticsMetricDetailID() {
        return parentAnalyticsMetricDetailID;
    }

    public void setParentAnalyticsMetricDetailID(String parentAnalyticsMetricDetailID) {
        this.parentAnalyticsMetricDetailID = parentAnalyticsMetricDetailID;
    }

    public List<AnalyticsMetricChartQuery> getAnalyticsMetricChartQueries() {
        return analyticsMetricChartQueries;
    }

    public void setAnalyticsMetricChartQueries(List<AnalyticsMetricChartQuery> analyticsMetricChartQueries) {
        this.analyticsMetricChartQueries = analyticsMetricChartQueries;
    }

    public List<AnalyticsMetricChartFilters> getAnalyticsMetricChartFilters() {
        return analyticsMetricChartFilters;
    }

    public void setAnalyticsMetricChartFilters(List<AnalyticsMetricChartFilters> analyticsMetricChartFilters) {
        this.analyticsMetricChartFilters = analyticsMetricChartFilters;
    }

    public static AnalyticsMetricChart fromVO(AnalyticsMetricChartVO analyticsMetricChartVO) {
        AnalyticsMetricChart analyticsMetricChart = new AnalyticsMetricChart();
        BeanUtils.copyProperties(analyticsMetricChartVO, analyticsMetricChart);
        return analyticsMetricChart;
    }
}
