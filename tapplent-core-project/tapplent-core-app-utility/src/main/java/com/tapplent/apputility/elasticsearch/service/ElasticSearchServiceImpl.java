package com.tapplent.apputility.elasticsearch.service;

import com.amazonaws.services.cloudsearchdomain.model.SuggestResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tapplent.apputility.elasticsearch.structure.ElasticsearchAutoCompleteResponse;
import com.tapplent.apputility.elasticsearch.structure.ElasticsearchPersonTest;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.elasticsearch.dao.ElasticSearchDAO;
import com.tapplent.platformutility.elasticsearch.valueObject.ElasticsearchPersonTestVO;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;
import org.elasticsearch.action.ActionFuture;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.admin.indices.exists.types.TypesExistsResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequestBuilder;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.Requests;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
//import org.elasticsearch.transport.client.PreBuiltTransportClient;
//import org.elasticsearch.xpack.client.PreBuiltXPackTransportClient;
import org.elasticsearch.xpack.client.PreBuiltXPackTransportClient;
import org.springframework.transaction.annotation.Transactional;
import com.tdunning.math.stats.TDigest;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.elasticsearch.search.suggest.completion.CompletionSuggestion.*;

/**
 * Created by tapplent on 08/05/17.
 */
public class ElasticSearchServiceImpl implements ElasticSearchService {
    private ElasticSearchDAO elasticSearchDAO;
    private static final String ID_NOT_FOUND = "<ID NOT FOUND>";
    private boolean ip6Enabled = true;
    private boolean ip4Enabled = true;
    private static final Logger LOG = LogFactory.getLogger(ElasticSearchServiceImpl.class);
    TransportClient client;

    public ElasticSearchServiceImpl() throws IOException {
    }

    //We actually don't need to create an index I guess.
    //
    private void createPersonIndex(){

    }
    // Get client
    private Client getClient() {
        if(client!=null)
            return client;
//         Get all the properties from the command line or from a particular file.
//         String host = System.getProperty("host"); 68760ddd146ca73330d258dc88aa5661.us-east-1.aws.found.io
//        String host = "68760ddd146ca73330d258dc88aa5661.us-east-1.aws.found.io";
//        int port = 9343;
//        String xpackSecurityuser =  "elastic:wXsbxPQO7ASsWmAFRuzb6kwb";
//        String clusterName = "68760ddd146ca73330d258dc88aa5661";
//        boolean enableSsl = true;
//        ip6Enabled = Boolean.parseBoolean(System.getProperty("ip6", "true"));
//        ip4Enabled = Boolean.parseBoolean(System.getProperty("ip4", "true"));
//
//        LOG.info("Connecting to cluster: [{}] via [{}:{}] using ssl:[{}]", clusterName, host, port, enableSsl);
//
//        // Build the settings for our client.
//        Settings settings = Settings.builder()
//                .put("client.transport.nodes_sampler_interval", "5s")
//                .put("client.transport.sniff", false)
//                .put("transport.tcp.compress", true)
//                .put("cluster.name", clusterName)
//                .put("xpack.security.transport.ssl.enabled", enableSsl)
//                .put("request.headers.X-Found-Cluster", "${cluster.name}")
//                .put("xpack.security.user", xpackSecurityuser)
//                .build();
//        TransportClient client=new PreBuiltXPackTransportClient(settings);
//        try {
//            for (InetAddress address : InetAddress.getAllByName(host)) {
//                if ((ip6Enabled && address instanceof Inet6Address)
//                        || (ip4Enabled && address instanceof Inet4Address)) {
//                    client.addTransportAddress(new InetSocketTransportAddress(address, port));
//                }
//            }
//        } catch (UnknownHostException e) {
//            LOG.error("Unable to get the host", e.getMessage());
//        }
        String host = System.getProperty("host");
        int port = Integer.parseInt(System.getProperty("port", "9343"));

        String hostBasedClusterName = host.split("\\.", 2)[0];
        String clusterName = System.getProperty("cluster", hostBasedClusterName);

        boolean enableSsl = Boolean.parseBoolean(System.getProperty("ssl", "true"));
        // Note: If enabling IPv6, then you should ensure that your host and network can route it to the Cloud endpoint.
        // (eg Docker disables IPv6 routing by default) - see also the initialization code at the top of this file.
        ip6Enabled = Boolean.parseBoolean(System.getProperty("ip6", "false"));
        ip4Enabled = Boolean.parseBoolean(System.getProperty("ip4", "true"));

        LOG.info("Connecting to cluster: [{}] via [{}:{}] using ssl:[{}]", clusterName, host, port, enableSsl);

        // Build the settings for our client.
        Settings settings = Settings.builder()
                .put("client.transport.nodes_sampler_interval", "5s")
                .put("client.transport.sniff", false)
                .put("transport.tcp.compress", true)
                .put("cluster.name", clusterName)
                .put("xpack.security.transport.ssl.enabled", enableSsl)
                .put("request.headers.X-Found-Cluster", "${cluster.name}")
                .put("xpack.security.user", System.getProperty("xpack.security.user"))
                .build();

        // Instantiate a TransportClient and add the cluster to the list of addresses to connect to.
        // Only port 9343 (SSL-encrypted) is currently supported. The use of X-Pack security features (formerly Shield) is required.
        client = new PreBuiltXPackTransportClient(settings);
//        TransportClient client = new PreBuiltXPackTransportClient(settings);
        try {
            for (InetAddress address : InetAddress.getAllByName(host)) {
                if ((ip6Enabled && address instanceof Inet6Address)
                        || (ip4Enabled && address instanceof Inet4Address)) {
                    client.addTransportAddress(new InetSocketTransportAddress(address, port));
                }
            }
        } catch (UnknownHostException e) {
            LOG.error("Unable to get the host", e.getMessage());
        }

//        while(true) {
//            try {
//                LOG.info("Getting cluster health... ");
//                ActionFuture<ClusterHealthResponse> healthFuture = client.admin().cluster().health(Requests.clusterHealthRequest());
//                ClusterHealthResponse healthResponse = healthFuture.get(5, TimeUnit.SECONDS);
//                LOG.info("Got cluster health response: [{}]", healthResponse.getStatus());
//            } catch(Throwable t) {
//                LOG.error("Unable to get cluster health response: [{}]", t.getMessage());
//            }
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException ie) { ie.printStackTrace(); }
//        }
        // int port = Integer.parseInt(System.getProperty("port", "9343"));
        // xpack.security.user='elastic:wXsbxPQO7ASsWmAFRuzb6kwb'
//        TransportClient client = null;
//        try {
//            client = new PreBuiltTransportClient(Settings.EMPTY)
//                    .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));
//
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }
        return client;
//        return client;
//        return null;
    }
    // create a mapping
    @Transactional
    public void populateIndexes() throws IOException {
        final Client client = getClient();
        final String indexName = "person";
        final String documentType = "Person";
//         Our document id is going to be exactly equivalent of Person ID.

        final IndicesExistsResponse res = client.admin().indices().prepareExists(indexName).execute().actionGet();
        if (res.isExists()) {
            final DeleteIndexRequestBuilder delIdx = client.admin().indices().prepareDelete(indexName);
            delIdx.execute().actionGet();
        }
        final CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate(indexName);
        // We need to build the mapping

        final XContentBuilder mappingBuilder = jsonBuilder().startObject();

        // Now adding fields to the person
        mappingBuilder.startObject("properties");

        addFieldToMapping(mappingBuilder, "PrimaryKeyID");
        addFieldToMapping(mappingBuilder, "FirstName");
        addFieldToMapping(mappingBuilder, "MiddleName");
        addFieldToMapping(mappingBuilder, "LastName");
        addFieldToMapping(mappingBuilder, "BirthFullName");
        addFieldToMapping(mappingBuilder, "PreferredName");
        addFieldToMapping(mappingBuilder, "FormalFullName");

        mappingBuilder.endObject();
        mappingBuilder.endObject();
        System.out.println(mappingBuilder.string());
        final PutMappingRequestBuilder putMappingRequestBuilder = client.admin().indices().preparePutMapping(indexName).setType(documentType).setSource(mappingBuilder);
        createIndexRequestBuilder.execute().actionGet();
        putMappingRequestBuilder.execute().actionGet();

//         Now that we have got mapping done we need to start indexing data.

        MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
        MasterEntityMetadataVOX personMeta = metadataService.getMetadataByDo("Person");
        List<Map<String, String>> personTestList = elasticSearchDAO.getPersons();
        // Now we need to prepare the documents for each record.
        BulkRequestBuilder bulkRequest = client.prepareBulk();
        for(Map<String, String> personMap : personTestList){
            bulkRequest.add(client.prepareIndex(indexName, documentType, personMap.get("PrimaryKeyID")).setSource(personMap));
        }
        BulkResponse bulkResponse = bulkRequest.get();
        if (bulkResponse.hasFailures()) {
            System.out.println("Process ran with errors");
        }
    }

    @Override
    public Object getGlobalSearchResult(String query) {
        List<ElasticsearchAutoCompleteResponse> responses = new ArrayList<>();
        final Client client = getClient();
        final String indexName = "person";
        final String documentType = "Person";

        SearchResponse searchResponse = client.prepareSearch(indexName).setTypes(documentType).setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setFetchSource(new String[]{"PrimaryKeyID","FormalFullName","FirstName"},null).suggest(new SuggestBuilder()
                        .addSuggestion("person-suggest", SuggestBuilders.completionSuggestion("FormalFullName").text(query)) ).get();
        CompletionSuggestion personSuggestion =  searchResponse.getSuggest().getSuggestion("person-suggest");
        for(CompletionSuggestion.Entry entry : personSuggestion.getEntries()){
            System.out.println("person suggestions for: " + entry.getText());
            for (CompletionSuggestion.Entry.Option option: ((CompletionSuggestion.Entry)entry).getOptions()) {
                ElasticsearchAutoCompleteResponse response = new ElasticsearchAutoCompleteResponse();
                response.setDoc_id(option.getHit().id());
                response.setType(option.getHit().type());
                response.setIndex(option.getHit().index());
                response.setSource(option.getHit().getSource());
                response.setText(option.getText().string());
                response.setSuggestionName("person-suggest");
//                System.out.println(option.getText());
//                System.out.println(option.getDoc());
//                System.out.println(option.getHit().getSource());
//                System.out.println(option.getScore());
                responses.add(response);
            }
        }
        return responses;
    }



    public void edgeNgramIndexSettings() throws IOException {
        final Client client = getClient();
        final XContentBuilder mappingBuilder = jsonBuilder().startObject();
        mappingBuilder.startObject("analyzer").startObject("t_edge_n_gram_analyzer").field("tokenizer").value("my_tokenizer").endObject();
        mappingBuilder.startObject("tokenizer").startObject("t_edge_n_gram_analyzer")
                .field("type","edge_ngram")
                .field("min_gram", 2)
                .field("max_gram", 10)
                .field("token_chars", new String[]{"letter","digit"})
                .endObject().endObject().endObject();
        System.out.print(mappingBuilder.string());

//        client.admin().indices().prepareUpdateSettings("person").setSettings(Settings.builder().put("analysis",mappingBuilder)).get();

    }

    public static void main(String args[]){
        final XContentBuilder mappingBuilder;
        try {
            mappingBuilder = jsonBuilder().startObject();
        mappingBuilder.startObject("analyzer").startObject("t_edge_n_gram_analyzer").field("tokenizer").value("my_tokenizer").endObject();
        mappingBuilder.startObject("tokenizer").startObject("t_edge_n_gram_analyzer")
                .field("type","edge_ngram")
                .field("min_gram", 2)
                .field("max_gram", 10)
                .field("token_chars", new String[]{"letter","digit"})
                .endObject().endObject().endObject().endObject();
        System.out.print(mappingBuilder.string());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static void addFieldToMapping(XContentBuilder mappingBuilder, String fieldName) throws IOException {
        mappingBuilder.startObject(fieldName);
        mappingBuilder.field("type").value("completion").endObject();
    }

    public ElasticSearchDAO getElasticSearchDAO() {
        return elasticSearchDAO;
    }

    public void setElasticSearchDAO(ElasticSearchDAO elasticSearchDAO) {
        this.elasticSearchDAO = elasticSearchDAO;
    }
}