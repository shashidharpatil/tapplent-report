package com.tapplent.platformutility.common.util;

import java.net.URL;

/**
 * Created by tapplent on 20/07/17.
 */
public class AttachmentObject {
    private URL getUrl;
    private URL headUrl;

    public URL getGetUrl() {
        return getUrl;
    }

    public void setGetUrl(URL getUrl) {
        this.getUrl = getUrl;
    }

    public URL getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(URL headUrl) {
        this.headUrl = headUrl;
    }
}
