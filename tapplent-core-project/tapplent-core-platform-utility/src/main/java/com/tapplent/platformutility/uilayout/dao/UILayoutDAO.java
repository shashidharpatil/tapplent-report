package com.tapplent.platformutility.uilayout.dao;


import com.tapplent.platformutility.accessmanager.structure.ResolvedColumnCondition;
import com.tapplent.platformutility.conversation.valueobject.PersonDetailsVO;
import com.tapplent.platformutility.layout.valueObject.JobRelationshipTypeVO;
import com.tapplent.platformutility.layout.valueObject.LayoutPermissionsVO;
import com.tapplent.platformutility.layout.valueObject.StateVO;
import com.tapplent.platformutility.uilayout.valueobject.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by sripat on 01/11/16.
 */
public interface UILayoutDAO {
//    public ScreenInstanceVO getLayout(String targetScreenInstance, List<String> targetSectionInstanceList, String deviceType, PersonGroup group);
//    public Map<String, List<LoadParameters>> getLoadParameters(String callFor, Set<String> fkIdList, PersonGroup group);
//
//    List<String> getDefaultBTForGroup(String mtPECode, PersonGroup group);
//
//    String getSectionMasterCode(String targetSectionInstance);
//
//    SectionControlVO getControlData(String controlId);
//
//    Map<Integer, SectionLayoutMapper> getSectionLayoutMapper(String mtPE, String uiDispTypeCodeFkId, String tabSeqNumPosInt);
//
    String getScreenInstanceFromSection(String sectionId);

    Map<String,Map<String,ResolvedColumnCondition>> getColumnConditions(String personId, Set<String> amRowPkIds);

    String getScreenId(String screenMasterCode);

    ScreenInstanceVO getScreenInstance(String targetScreenInstance, String deviceType);

    List<ScreenSectionInstanceVO> getSectionInstances(String screenInstancePkId, String deviceType);

    List<SectionControlVO> getControlList(String screenInstancePkId, Set<String> relevantSectionIdList);

    List<ControlStaticValuesVO> getStaticControlValues(String screenInstancePkId, Set<String> relevantSectionIdList);

    List<ControlInstanceFiltersVO> getControlInstanceFilters(String screenInstancePkId, Set<String> relevantSectionIdList);

    List<ControlActionInstanceVO> getControlActionInstances(String screenInstancePkId, Set<String> relevantSectionIdList);

    List<ControlActionArgumentVO> getControlActionArguments(String screenInstancePkId, Set<String> relevantSectionIdList);

    List<ControlActionTargetsVO> getControlActionTargets(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType);

    List<ControlActionTargetSectionSortParamsVO> getControlActionTargetSortParams(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType);

    List<ControlActionTargetSectionFilterParamsVO> getControlActionTargetFilterParams(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType);

    List<ControlActionTargetFilterCriteriaVO> getControlActionTargetFilterCriteria(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType);

    List<SectionActionInstanceVO> getSectionActionInstance(String screenInstancePkId, Set<String> relevantSectionIdList);

    List<SectionActionArgumentVO> getSectionActionArguments(String screenInstancePkId, Set<String> relevantSectionIdList);

    List<SectionActionTargetsVO> getSectionActionTargets(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType);

    List<SectionActionTargetFilterCriteriaVO> getSectionActionTargetFilterCriteria(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType);

    List<SectionActionTargetSectionSortParamsVO> getSectionActionTargetSortParams(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType);

    List<SectionActionTargetSectionFilterParamsVO> getSectionActionTargetFilterParams(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType);

    ControlActionTargetsVO getControlActionTargets(String actionTargetPkId);

    SectionActionTargetsVO getSectionActionTargets(String actionTargetPkId);

    List<GenericQueryAttributesVO> getGenericQueryAttributes(String queryID);

    List<GenericQueryFiltersVO> getGenericQueryFilters(String queryID);

    List<String> getAllPersonIDs();

    List<OrgChartFilterLabelVO> getOrgChartFilterLabels(Set<String> sectionIDs);

//    List<OrgChartFilterAttrPathVO> getOrgChartFilterAttrPaths(Set<String> sectionIDs);

    List<OrgChartFilterRangeVO> getOrgChartFilterRanges(Set<String> sectionIDs);

    Map<String,StateVO> getActiveStateIcons();

    Map<String,StateVO> getRecordStateIcons();

    Map<String,StateVO> getSavedStateIcons();

    List<JobRelationshipTypeVO> getJobRelationshipTypes();

    List<PersonDetailsVO> getFeaturingPersonDetail(String contextID, String mtPE);

    List<OrganizationBreakUpDataVO> getOrganizationBreakUpData(String contextID);

    List<ControlEventInstanceVO> getControlEventVO(String screenInstancePkId, Set<String> relevantSectionIdList);

    Map<String,RatingScaleVO> getRatingScaleVOs(Set<String> ratingScaleIDs);

    List<ControlEventCriteriaVO> getControlEventCriteriaVOs(Set<String> strings);

    List<FormulaInputParametersDetailsVO> getInputParametersDetails(Set<String> strings);

    List<LocationBreakUpDataVO> getLocationBreakUpData(String contextID);

    List<JobRelationshipsVO> getDefaultJobRelations(String contextID1, String contextID2);

    List<CostCentreBreakUpDataVO> getCostCentreBreakUpData(String contextID);

    List<JobFamilyBreakUpDataVO> getJobFamilyBreakUpData(String contextID);

    List<LayoutPermissionsVO> getLayoutPermissionsForControls(Set<String> sectionIDs, String deviceType);

    List<LayoutPermissionsVO> getLayoutPermissionsForSectionActions(Set<String> sectionIDs, String deviceType);

    List<LayoutPermissionsVO> getLayoutPermissionsForControlActions(Set<String> relevantSectionIdList, String deviceType);

    List<LayoutPermissionsVO> getLayoutPermissionsForSections(Set<String> relevantSectionIdList, String deviceType);

    String getReportingSectionID();
}
