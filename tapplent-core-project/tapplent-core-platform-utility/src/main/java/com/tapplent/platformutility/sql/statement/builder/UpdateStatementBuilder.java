package com.tapplent.platformutility.sql.statement.builder;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadatamap.MetaDataMap;
import com.tapplent.platformutility.sql.context.model.InsertContextModel;
import com.tapplent.platformutility.sql.context.model.SQLContextModel;

public class UpdateStatementBuilder implements SQLStatementBuilder {

	private final InsertContextModel contextModel;
	private EntityMetadataVOX entityMetadataVOX;

	public UpdateStatementBuilder(SQLContextModel contextModel, EntityMetadataVOX entityMetadataVOX) {
		this.entityMetadataVOX = entityMetadataVOX;
		this.contextModel = (InsertContextModel) contextModel;
	}

	@Override
	public String build() {
		SQLMyBatisQueryBuilder queryBuilder = new SQLMyBatisQueryBuilder();
		queryBuilder.addUpdate(contextModel.getEntityName());
		Map<String, Object> insertValueMap = contextModel.getInsertValueMap();
		if(null!=insertValueMap){
			Set<Entry<String, Object>> entrySet = insertValueMap.entrySet();
			for (Entry<String, Object> entry : entrySet) {
				Object object = entry.getValue();
				EntityAttributeMetadata metaByColumnName = entityMetadataVOX.getAttributeByColumnName(entry.getKey());
				if(metaByColumnName.getDbDataTypeCode().equals("T_BLOB") && !metaByColumnName.isGlocalizedFlag() && object != null && !(object instanceof String)){
					StringBuilder blobString = new StringBuilder();
					blobString.append("COLUMN_ADD("+entry.getKey()+", ");

					if (object instanceof ObjectNode) {
						Iterator<Entry<String, JsonNode>> nodeIterator = ((ObjectNode) object).fields();
						while (nodeIterator.hasNext()) {
							Entry<String, JsonNode> innerEntry = nodeIterator.next();
							blobString.append("'" + innerEntry.getKey() + "', ");
							EntityAttributeMetadata attributeMetadata = entityMetadataVOX.getAttributeByName(innerEntry.getKey());
							if (attributeMetadata != null)
								blobString.append(Util.getStringTobeInserted(attributeMetadata, innerEntry.getValue()) + ", ");//fixme not right, please correct!
							else
								blobString.append("'" + (innerEntry.getValue().asText()).replace("'", "''") + "', ");
						}
					} else {
						Map<String, String> blobMap = (Map<String, String>) object;
						for (Entry<String, String> innerEntry : blobMap.entrySet()) {
							blobString.append("'" + innerEntry.getKey() + "', ");
							EntityAttributeMetadata attributeMetadata = entityMetadataVOX.getAttributeByName(innerEntry.getKey());
//							blobString.append(Util.getStringTobeInserted(attributeMetadata, innerEntry.getValue()) + ", ");
							blobString.append("'"+innerEntry.getValue() + "', ");
						}
					}
					blobString.replace(blobString.length()-2, blobString.length(), ")");
					queryBuilder.addSet(entry.getKey() + " = " + blobString.toString());
				}
				else queryBuilder.addSet(entry.getKey()+" = "+ Util.getStringTobeInserted(metaByColumnName,entry.getValue()));
			}
		}
		if(entityMetadataVOX.isVersioned())
			queryBuilder.addWhere(contextModel.getInsertEntityPK().getColumn()+"="+contextModel.getInsertEntityPK().getValue()+" AND "
					+ contextModel.getInsertEntityVersion().getColumn() + "=" + contextModel.getInsertEntityVersion().getValue());
		else queryBuilder.addWhere(contextModel.getInsertEntityPK().getColumn()+"="+contextModel.getInsertEntityPK().getValue());
		return queryBuilder.getQueryString();
	}

//	public String buildDefault() {
//		SQLMyBatisQueryBuilder queryBuilder = new SQLMyBatisQueryBuilder();
//		queryBuilder.addUpdate(contextModel.getEntityName());
//		Map<String, Object> insertValueMap = contextModel.getInsertValueMap();
//		if(null!=insertValueMap){
//			Set<Entry<String, Object>> entrySet = insertValueMap.entrySet();
//			for (Entry<String, Object> entry : entrySet) {
//				EntityAttributeMetadata metaByColumnName = entityMetadataVOX.getAttributeByColumnName(entry.getKey());
//				if(metaByColumnName.getDbDataTypeCode().equals("T_BLOB") && !metaByColumnName.isGlocalizedFlag()){
//					StringBuilder blobString = new StringBuilder();
//					blobString.append("COLUMN_ADD("+entry.getKey()+", ");
//					Object object = entry.getValue();
//					if(object instanceof ObjectNode){
//						Iterator<Entry<String, JsonNode>> nodeIterator = ((ObjectNode) object).fields();
//						while (nodeIterator.hasNext()){
//							Entry<String, JsonNode> innerEntry = nodeIterator.next();
//							blobString.append("'" + innerEntry.getKey() + "', ");
//							EntityAttributeMetadata attributeMetadata = entityMetadataVOX.getAttributeByName(innerEntry.getKey());
//							if(attributeMetadata != null)
//								blobString.append(Util.getStringTobeInserted(attributeMetadata, innerEntry.getValue()) + ", ");//util T_BLOB will take care blob inside blob with inner blob being G11n only!
//							else blobString.append("'" + (innerEntry.getValue().asText()).replace("'", "''") + "', ");
//						}
//					}
//					else{
//						Map<String, String> blobMap = (Map<String, String>) object;
//						for(Entry<String, String> innerEntry : blobMap.entrySet()){
//							blobString.append("'" + innerEntry.getKey() + "', ");
//							EntityAttributeMetadata attributeMetadata = entityMetadataVOX.getAttributeByName(innerEntry.getKey());
//							blobString.append(Util.getStringTobeInserted(attributeMetadata, innerEntry.getValue())+ ", ");
//						}
//					}
//					blobString.replace(blobString.length()-2, blobString.length(), ")");
//					queryBuilder.addSet(entry.getKey() + " = " + blobString.toString());
//				}
//				else queryBuilder.addSet(entry.getKey()+" = "+ Util.getStringTobeInserted(metaByColumnName,entry.getValue()));
//			}
//		}
//		queryBuilder.addWhere(contextModel.getInsertEntityPK().getColumn()+"="+"x'"+contextModel.getInsertEntityPK().getValue()+"'");
//		return queryBuilder.getQueryString();
//	}
}