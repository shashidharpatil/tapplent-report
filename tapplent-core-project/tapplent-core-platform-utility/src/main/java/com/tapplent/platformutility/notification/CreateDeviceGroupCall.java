package com.tapplent.platformutility.notification;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tapplent.platformutility.notification.helper.AppConstants;
import com.tapplent.platformutility.notification.helper.DGCreateModel;

public class CreateDeviceGroupCall {

	public static String post(String apiKey, String projectId, DGCreateModel group) {
		String notkey = "";
		try {

			// 1. URL
			URL url = new URL(AppConstants.gcmGroupUrl);

			// 2. Open connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			// 3. Specify POST method
			conn.setRequestMethod("POST");

			// 4. Set the headers
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "key=" + apiKey);
			conn.setRequestProperty("project_id", projectId);

			conn.setDoOutput(true);

			// 5. Add JSON data into POST request body

			// `5.1 Use Jackson object mapper to convert Contnet object into
			// JSON
			ObjectMapper mapper = new ObjectMapper();

			// 5.2 Get connection output stream
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

			// 5.3 Copy Content "JSON" into
			mapper.writeValue(wr, group);
			System.out.println("mapper" + mapper.toString());

			// 5.4 Send the request
			wr.flush();

			// 5.5 close
			wr.close();

			// 6. Get the response
			int responseCode = conn.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// 7. Print result

			org.json.JSONObject object;
			object = new org.json.JSONObject(response.toString());

			System.out.println(response.toString());
			notkey = object.get(AppConstants.notification_key).toString();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return notkey;
	}

}
