package com.tapplent.platformutility.uilayout.valueobject;

import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 25/04/17.
 */
public class LayoutThemeMapper {
    private String themeMapperPkId;
    private String screenMasterCode;
    private String sectionMasterCode;
    private String actionVisualMasterCode;
    private String actionGroupCode;
    private int themeSeqNumber;

    public String getThemeMapperPkId() {
        return themeMapperPkId;
    }

    public void setThemeMapperPkId(String themeMapperPkId) {
        this.themeMapperPkId = themeMapperPkId;
    }

    public String getScreenMasterCode() {
        return screenMasterCode;
    }

    public void setScreenMasterCode(String screenMasterCode) {
        this.screenMasterCode = screenMasterCode;
    }

    public String getSectionMasterCode() {
        return sectionMasterCode;
    }

    public void setSectionMasterCode(String sectionMasterCode) {
        this.sectionMasterCode = sectionMasterCode;
    }

    public String getActionVisualMasterCode() {
        return actionVisualMasterCode;
    }

    public void setActionVisualMasterCode(String actionVisualMasterCode) {
        this.actionVisualMasterCode = actionVisualMasterCode;
    }

    public String getActionGroupCode() {
        return actionGroupCode;
    }

    public void setActionGroupCode(String actionGroupCode) {
        this.actionGroupCode = actionGroupCode;
    }

    public int getThemeSeqNumber() {
        return themeSeqNumber;
    }

    public void setThemeSeqNumber(int themeSeqNumber) {
        this.themeSeqNumber = themeSeqNumber;
    }

    public static LayoutThemeMapper fromVO(LayoutThemeMapperVO layoutThemeMapperVO){
        LayoutThemeMapper layoutThemeMapper = new LayoutThemeMapper();
        BeanUtils.copyProperties(layoutThemeMapperVO, layoutThemeMapper);
        return layoutThemeMapper;
    }
}
