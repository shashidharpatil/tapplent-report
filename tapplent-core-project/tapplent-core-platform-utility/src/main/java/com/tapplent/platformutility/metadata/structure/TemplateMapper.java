package com.tapplent.platformutility.metadata.structure;

/**
 * Created by Tapplent on 1/8/17.
 */
public class TemplateMapper {

    private String versionId;
    private String TemplateMapperPkId;
    private String PfmGroupFkId;
    private String MtPeCodeFkId;
    private String BaseTemplateCodeFkId;
    private boolean IsMetaDefault;
    private boolean IsReportDefault;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getTemplateMapperPkId() {
        return TemplateMapperPkId;
    }

    public void setTemplateMapperPkId(String templateMapperPkId) {
        TemplateMapperPkId = templateMapperPkId;
    }

    public String getPfmGroupFkId() {
        return PfmGroupFkId;
    }

    public void setPfmGroupFkId(String pfmGroupFkId) {
        PfmGroupFkId = pfmGroupFkId;
    }

    public String getMtPeCodeFkId() {
        return MtPeCodeFkId;
    }

    public void setMtPeCodeFkId(String mtPeCodeFkId) {
        MtPeCodeFkId = mtPeCodeFkId;
    }

    public String getBaseTemplateCodeFkId() {
        return BaseTemplateCodeFkId;
    }

    public void setBaseTemplateCodeFkId(String baseTemplateCodeFkId) {
        BaseTemplateCodeFkId = baseTemplateCodeFkId;
    }

    public boolean isMetaDefault() {
        return IsMetaDefault;
    }

    public void setMetaDefault(boolean metaDefault) {
        IsMetaDefault = metaDefault;
    }

    public boolean isReportDefault() {
        return IsReportDefault;
    }

    public void setReportDefault(boolean reportDefault) {
        IsReportDefault = reportDefault;
    }
}
