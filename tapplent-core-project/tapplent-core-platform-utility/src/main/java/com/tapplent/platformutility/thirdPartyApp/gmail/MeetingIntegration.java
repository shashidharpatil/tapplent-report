package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.sql.Timestamp;

public class MeetingIntegration {

	private String mtgIntgnPkId;
	private String mtgFkId;
	private String externalIntegrationAppCodeFkId;
	private String externalReferenceTxt;
	private boolean isDeleted;
	private String createdDatetime;
	private Timestamp lastModifiedDatetime;
	public String getMtgIntgnPkId() {
		return mtgIntgnPkId;
	}
	public void setMtgIntgnPkId(String mtgIntgnPkId) {
		this.mtgIntgnPkId = mtgIntgnPkId;
	}
	public String getMtgFkId() {
		return mtgFkId;
	}
	public void setMtgFkId(String mtgFkId) {
		this.mtgFkId = mtgFkId;
	}
	public String getExternalIntegrationAppCodeFkId() {
		return externalIntegrationAppCodeFkId;
	}
	public void setExternalIntegrationAppCodeFkId(String externalIntegrationAppCodeFkId) {
		this.externalIntegrationAppCodeFkId = externalIntegrationAppCodeFkId;
	}
	public String getExternalReferenceTxt() {
		return externalReferenceTxt;
	}
	public void setExternalReferenceTxt(String externalReferenceTxt) {
		this.externalReferenceTxt = externalReferenceTxt;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(String createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(Timestamp timestamp) {
		this.lastModifiedDatetime = timestamp;
	}
	
}
