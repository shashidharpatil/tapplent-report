package com.tapplent.platformutility.common.util;

import java.io.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.FileNameMap;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.tapplent.platformutility.Exception.TapplentException;
import com.tapplent.platformutility.activityLog.ActivityLog;
import com.tapplent.platformutility.activityLog.AuditMsgFormat;
import com.tapplent.platformutility.common.cache.PersonDetailsObjectRepository;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.util.valueObject.FilterExpn;
import com.tapplent.platformutility.common.util.valueObject.G11n;
import com.tapplent.platformutility.g11n.structure.OEMObject;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.search.builder.FilterOperatorValue;
import com.tapplent.platformutility.uilayout.valueobject.ControlType;
import com.tapplent.platformutility.uilayout.valueobject.LoadParameters;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import com.tapplent.tenantresolver.user.UserContext;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadata.structure.MasterEntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;
import com.tapplent.platformutility.search.builder.expression.Operator;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;

public class Util {

	final static Logger LOG = LogFactory.getLogger(Util.class);

	public static String getStringTobeInserted(EntityAttributeMetadata attribute, Object object) {
		Object valueToReturn = object;
		if(object == null || object.equals(""))
			return "NULL" ;
		if(attribute != null){
			EntityDataTypeEnum eDt = EntityDataTypeEnum.valueOf(attribute.getDbDataTypeCode());
			switch (eDt) {
//			case DATE_TIME:
//				Timestamp timestamp = (Timestamp) object;
////				valueToReturn = Common.getQS(timestamp);
//				Timestamp.valueOf((String) object);
//				break;
				case BOOLEAN:
					valueToReturn =  String.valueOf(object);
					break;
				case POSITIVE_INTEGER:
				case NEGATIVE_INTEGER:
				case BIG_INTEGER:
				case POSITIVE_DECIMAL:
				case NEGATIVE_DECIMAL:
				case CURRENCY:
				case LOCATION_LATITUDE:
				case LOCATION_LONGITUDE:
					valueToReturn = (String)object;
					break;
				case DATE_TIME:
//				SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
////				valueToReturn = (Timestamp)object;
//				valueToReturn =  "'" +fmt.format(object).toString()+ "'";
					java.util.Date date = null;
					try {
						String dateTimeFormat="yyyy-MM-dd HH:mm:ss.SSS";
						SimpleDateFormat sdf = new SimpleDateFormat(dateTimeFormat);
						date = sdf.parse(object.toString());
						DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
						valueToReturn = destDf.format(date);
//				    if (!value.equals(sdf.format(date))) {
//				        date = null;
//				    }

				} catch (ParseException ex) {
				    ex.printStackTrace();
				}
				valueToReturn = "'"+valueToReturn.toString()+"'";
				break;
			case T_CODE:
			case T_ICON:
			case TEXT:
			case T_BIG_TEXT:
			case LONG_TEXT:
				object = ((String)object).replaceAll("\\r\\n|\\r|\\n", " ");
				if (attribute.isGlocalizedFlag())
					object = getG11nStringToInsert((String) object, false);
				valueToReturn = "'" + ((String)object).replace("'", "''") + "'";
				break;
			case ATTACHMENT:
			case AUDIO:
			case VIDEO:
			case IMAGE:
			case T_ID:
			case T_IMAGEID:
			case T_ATTACH:
				valueToReturn = "0x" + remove0x((String)object);
				break;
			case T_BLOB:
				if(object.equals("")) return "";

//				boolean imageFlag = attribute.getDbDataTypeCode().equals("T_IMAGE");
					StringBuilder blobString = new StringBuilder();
					blobString.append("COLUMN_CREATE(");
					if(object instanceof ObjectNode){
						Iterator<Entry<String, JsonNode>> nodeIterator = ((ObjectNode) object).fields();
						while (nodeIterator.hasNext()){
							Entry<String, JsonNode> entry = nodeIterator.next();
							blobString.append("'" + entry.getKey() + "'");
//						if(imageFlag)
//							blobString.append(", x'" + entry.getValue().asText() + "', ");
							blobString.append(", '" + (entry.getValue().asText()).replace("'", "''") + "', ");
						}
					}
					else{
						Map<String, String> blobMap = (Map<String, String>) object;
						for(Entry<String, String> entry : blobMap.entrySet()){
							blobString.append("'" + entry.getKey() + "'");
//						if(imageFlag)
//							blobString.append(", x'" + entry.getValue() + "', ");
							blobString.append(", '" + (entry.getValue()).replace("'", "''") + "', ");
						}
					}
					blobString.replace(blobString.length()-2, blobString.length(), ")");
					valueToReturn = blobString.toString();
					break;
				default:
					valueToReturn = "'" + ((String)object).replace("'", "''") + "'";
					break;
			}
		} else {
			LOG.debug("Attribute is NULL for column name " + " and value " + object);

//				valueToReturn = "'" + (String)object + "'";//FIXME check for null.
			 valueToReturn = "NULL";
		}
		return (String) valueToReturn;
	}

	public static String getStringTobeInserted(String dbDataType, Object object) {

		Object valueToReturn = object;
		if(object == null || object.equals(""))
			return "NULL" ;
		if(dbDataType != null){
			EntityDataTypeEnum eDt = EntityDataTypeEnum.valueOf(dbDataType);
			switch (eDt) {
//			case DATE_TIME:
//				Timestamp timestamp = (Timestamp) object;
////				valueToReturn = Common.getQS(timestamp);
//				Timestamp.valueOf((String) object);
//				break;
				case BOOLEAN:
					valueToReturn =  String.valueOf(object);
					break;
				case POSITIVE_INTEGER:
				case NEGATIVE_INTEGER:
				case BIG_INTEGER:
				case POSITIVE_DECIMAL:
				case NEGATIVE_DECIMAL:
				case CURRENCY:
				case LOCATION_LATITUDE:
				case LOCATION_LONGITUDE:
					valueToReturn = (String)object;
					break;
				case DATE_TIME:
//				SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
////				valueToReturn = (Timestamp)object;
//				valueToReturn =  "'" +fmt.format(object).toString()+ "'";
					java.util.Date date = null;
					try {
						String dateTimeFormat="yyyy-MM-dd HH:mm:ss.SSS";
						SimpleDateFormat sdf = new SimpleDateFormat(dateTimeFormat);
						date = sdf.parse(object.toString());
						DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
						valueToReturn = destDf.format(date);
//				    if (!value.equals(sdf.format(date))) {
//				        date = null;
//				    }

					} catch (ParseException ex) {
						ex.printStackTrace();
					}
					valueToReturn = "'"+Timestamp.valueOf(valueToReturn.toString()).toString()+"'";
					break;
				case T_CODE:
				case T_ICON:
				case TEXT:
				case T_BIG_TEXT:
					object = ((String)object).replaceAll("\\r\\n|\\r|\\n", " ");
					valueToReturn = "'" + ((String)object).replace("'", "''") + "'";
					break;
				case ATTACHMENT:
				case AUDIO:
				case VIDEO:
				case IMAGE:
				case T_ID:
				case T_IMAGEID:
				case T_ATTACH:
					valueToReturn = "0x" + remove0x((String)object);
					break;
				case T_BLOB:
					if(object.equals("")) return "";

//				boolean imageFlag = attribute.getDbDataTypeCode().equals("T_IMAGE");
					StringBuilder blobString = new StringBuilder();
					blobString.append("COLUMN_CREATE(");
					if(object instanceof ObjectNode){
						Iterator<Entry<String, JsonNode>> nodeIterator = ((ObjectNode) object).fields();
						while (nodeIterator.hasNext()){
							Entry<String, JsonNode> entry = nodeIterator.next();
							blobString.append("'" + entry.getKey() + "'");
//						if(imageFlag)
//							blobString.append(", x'" + entry.getValue().asText() + "', ");
							blobString.append(", '" + (entry.getValue().asText()).replace("'", "''") + "', ");
						}
					}
					else{
						Map<String, String> blobMap = (Map<String, String>) object;
						for(Entry<String, String> entry : blobMap.entrySet()){
							blobString.append("'" + entry.getKey() + "'");
//						if(imageFlag)
//							blobString.append(", x'" + entry.getValue() + "', ");
							blobString.append(", '" + (entry.getValue()).replace("'", "''") + "', ");
						}
					}
					blobString.replace(blobString.length()-2, blobString.length(), ")");
					valueToReturn = blobString.toString();
					break;
				default:
					valueToReturn = "'" + ((String)object).replace("'", "''") + "'";
					break;
			}
		} else {
			LOG.debug("Attribute is NULL for column name " + " and value " + object);
			if(object != null)
				valueToReturn = "'" + (String)object + "'";//FIXME check for null.
			else valueToReturn = "NULL";
		}
		return (String) valueToReturn;
	}

	public static Date getDateFromString(String dateInput){
		java.util.Date date = null;
		if(DBUploadTracker.dateTimeFormat ==null ){
			DBUploadTracker.dateTimeFormat="yyyy-MM-dd HH:mm:ss";
		};
		SimpleDateFormat sdf = new SimpleDateFormat(DBUploadTracker.dateTimeFormat);
		try{
			date = sdf.parse(dateInput);
		}catch(ParseException p){
			SimpleDateFormat newSdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				date = newSdf.parse(dateInput);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	public static String getStringTobeInserted(String dbDataType, Object value, boolean isBlobColumn) {
		if(value == null)
			return null;
		String sValue = value.toString();
		if(!dbDataType.equals("T_ID"))
			sValue = (value.toString()).replace("'", "''");
		final Logger LOG = LogFactory.getLogger(Util.class);
		Object valueToReturn = sValue;
		EntityDataTypeEnum eDt = EntityDataTypeEnum.valueOf(dbDataType);
		switch (eDt) {
//			case DATE_TIME:
//				Timestamp timestamp = (Timestamp) object;
////				valueToReturn = Common.getQS(timestamp);
//				Timestamp.valueOf((String) object);
//				break;
			case BOOLEAN:
			case POSITIVE_INTEGER:
			case NEGATIVE_INTEGER:
			case BIG_INTEGER:
			case POSITIVE_DECIMAL:
			case NEGATIVE_DECIMAL:
			case CURRENCY:
			case LOCATION_LATITUDE:
			case LOCATION_LONGITUDE:
				valueToReturn = (String)sValue;
				break;
			case DATE_TIME:
				if(sValue.equals("NOW()")){
					ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
					sValue = utc.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
				}else{
					java.util.Date date = null;
					if(DBUploadTracker.dateTimeFormat ==null ){
						DBUploadTracker.dateTimeFormat="yyyy-MM-dd HH:mm:ss";
					};
					SimpleDateFormat sdf = new SimpleDateFormat(DBUploadTracker.dateTimeFormat);
					try{
						date = sdf.parse(sValue);
					}catch(ParseException p){
						SimpleDateFormat newSdf = new SimpleDateFormat("yyyy-MM-dd");
						try {
							date = newSdf.parse(sValue);
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}

					DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					sValue = destDf.format(date);
//					    if (!value.equals(sdf.format(date))) {
//					        date = null;
//					    }
					sValue = Timestamp.valueOf(sValue).toString();
				}
				valueToReturn = "'" + (String)sValue + "'";
				break;
			case T_CODE:
			case T_ICON:
			case TEXT:
			case T_BIG_TEXT:
			case LOCATION:
			case LONG_TEXT:
			case T_EXPRESSION:
				valueToReturn = "'" + org.apache.commons.lang3.StringEscapeUtils.unescapeJava(sValue) + "'";
				break;
			case ATTACHMENT:
			case AUDIO:
			case VIDEO:
			case IMAGE:
			case T_ID:
			case T_ATTACH:
			case T_IMAGEID:
				if(sValue.equals("UUID()")){
					sValue = Common.getUUID();
				}
				if(isBlobColumn){
					valueToReturn = "'" + (String)sValue + "'";
				}else{
					if(!sValue.startsWith("x"))
						valueToReturn = (String)sValue ;
				}
				break;
			default:
				valueToReturn = "'" + (String)sValue + "'";
				break;

		}
		return (String) valueToReturn;
	}

	public static Object convertStringToTypedValue(String dbDataTypeCodeFkId, Entry<String, Object> entry) {
		final Logger LOG = LogFactory.getLogger(Util.class);
		Object attributeValue = entry.getValue();
		if(attributeValue == null || isObjectEmptyString(attributeValue)) return null;
		String attributeValueString = attributeValue.toString();
		try{
			switch(dbDataTypeCodeFkId){
				case "T_ID":
				case "T_CODE":
				case "T_ICON":
				case "TEXT":
				case "ATTACHMENT":
				case "AUDIO":
				case "VIDEO":
				case "IMAGE":
				case "CURRENCY":
					return attributeValue.toString();
				case "T_BLOB":
//				ObjectMapper mapper =  new ObjectMapper();
//				JsonNode blob = mapper.readTree((JsonParser) entry.getValue());
//				return blob;
					if(entry.getValue() instanceof Map){
						return (Map<String, Object>) entry.getValue();
					}
					else if (entry.getValue() instanceof JsonNode){
						return (JsonNode) entry.getValue();
					}
					else throw new Exception();
				case "DATE_TIME":
//					if (attributeValueString.equals("")) return null;
					return Timestamp.valueOf(attributeValueString);
				case "POSITIVE_INTEGER":
				case "NEGATIVE_INTEGER":
					return Integer.parseInt(attributeValueString);
				case "BIG_INTEGER":
					return Long.parseLong(attributeValueString);
				case "POSITIVE_DECIMAL":
				case "NEGATIVE_DECIMAL":
				case "LOCATION_LATITUDE":
				case "LOCATION_LONGITUDE":
					return Double.parseDouble(attributeValueString);
				case "BOOLEAN":
					return Boolean.parseBoolean(attributeValueString);
			}
		} catch(Exception e){
			e.printStackTrace();
			LOG.error(attributeValue + " is not a appropriate value for " + dbDataTypeCodeFkId);
		}
		return null;
	}
	public static String getBlobDataTypeFromEntityDataType(String dataType){
		String result = null;
		if(StringUtil.isDefined(dataType)){
			EntityDataTypeEnum eDataType = EntityDataTypeEnum.valueOf(dataType);
			switch(eDataType){
				case T_ID:
				case T_BLOB:
				case BOOLEAN:
				case T_IMAGEID:
				case T_ATTACH :
					return "BINARY";
				case CURRENCY:
				case T_CODE:
				case T_ICON:
				case TEXT:
				case LONG_TEXT:
				case T_EXPRESSION:
				case T_BIG_TEXT:
					return "CHAR";
				case DATE:
					return "DATE";
				case TIME:
					return "TIME";
				case DATE_TIME:
					return "DATETIME";
				case POSITIVE_INTEGER:
				case NEGATIVE_INTEGER:
				case BIG_INTEGER:
					return "INTEGER";
				case POSITIVE_DECIMAL:
				case NEGATIVE_DECIMAL:
				case LOCATION_LATITUDE:
				case LOCATION_LONGITUDE:
				case LOCATION:
					return "DECIMAL";
				default: return "CHAR";
			}
		}
		return result;
	}
	public static Object getStringTobuildWhereClauseAsParameter(EntityAttributeMetadata attribute, Object object, String columnName, String primaryLocale ) {
		Object valueToReturn = object;
		if(object==null || isObjectEmptyString(object)){
			return null;
		}
		if(attribute != null){
//			if(attribute.getContainerBlobDbColumnName() != null){
//				StringBuilder blobString = new StringBuilder();
//				blobString.append("COLUMN_GET(");
//				blobString.append(attribute.getContainerBlobDbColumnName() + ", '");
//				blobString.append(attribute.getDoAttributeCode() + "' as "+Util.getBlobDataTypeFromEntityDataType(attribute.getDbDataTypeCode())+") = ");
//				valueToReturn = blobString;
//				return valueToReturn;
//			}
			EntityDataTypeEnum eDt = EntityDataTypeEnum.valueOf(attribute.getDbDataTypeCode());
			switch (eDt) {
			case DATE_TIME:
				if (object instanceof Timestamp){
					SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.");
					java.sql.Timestamp ts = (Timestamp) object;
					LOG.debug(ts.toString());

				valueToReturn = "'"+ts.toString()+"'";
				}
				else {
					try {
						String dateTimeFormat="yyyy-MM-dd HH:mm:ss.SSS";
						SimpleDateFormat sdf = new SimpleDateFormat(dateTimeFormat);
						java.util.Date date = sdf.parse(object.toString());
						DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
						valueToReturn = destDf.format(date);
					}catch (ParseException ex) {
						ex.printStackTrace();
					}
					valueToReturn = "'"+valueToReturn+"'";
				}
				break;
			case BOOLEAN:
				valueToReturn = object;
				break;
			case POSITIVE_INTEGER:
			case NEGATIVE_INTEGER:
			case BIG_INTEGER:
			case POSITIVE_DECIMAL:
			case NEGATIVE_DECIMAL:
			case CURRENCY:
			case LOCATION_LATITUDE:
			case LOCATION_LONGITUDE:
				valueToReturn = (String)object;
				break;
			case T_CODE:
			case T_ICON:
			case TEXT:
			case T_BIG_TEXT:
				valueToReturn = (String)object; 
				break;
			case T_ATTACH:
			case AUDIO:
			case VIDEO:
			case IMAGE:
			case T_ID:
				valueToReturn = convertStringIdToByteId((String)object) ;
				break;
////			case T_BLOB:
//				Map<String, String> blobMap = (Map<String, String>) object;
//				StringBuilder blobString = new StringBuilder();
//				blobString.append("COLUMN_GET(");
//				for(Entry<String, String> entry : blobMap.entrySet()){
//					//WHERE COLUMN_GET(dynamic_cols, 'color' as char)='black'
//					blobString.append(columnName + ", '" + entry.getKey() + "' as char) = " + entry.getValue());
//				}
//				valueToReturn = blobString;
//				break;
				case T_BLOB:
					ObjectNode node = (ObjectNode) object;
					valueToReturn = node.findValue(primaryLocale).asText();
					break;
				default:
					valueToReturn = "'" + (String)object + "'";
					break;
			}
		} else {
			LOG.debug("Attribute or value is NULL for column name "+ columnName + " and value " + object);
			if(object != null)
				valueToReturn = "'" + (String)object + "'";
			else return null;
		}
		return valueToReturn;
	}

	private static boolean isObjectEmptyString(Object object) {
		if (object instanceof String)
			return object.toString().equals("");
		return false;
	}

	public static Object dataFormatter(Object inputValue, String dbDataType) throws IOException, SQLException{
		Object valueToReturn = null;
		if(inputValue==null){
			return null;
		}
		if(dbDataType != null){
			EntityDataTypeEnum dataType = EntityDataTypeEnum.valueOf(dbDataType);
			switch(dataType){
				case DATE_TIME:
				case BOOLEAN:
				case POSITIVE_INTEGER:
				case NEGATIVE_INTEGER:
				case BIG_INTEGER:
				case POSITIVE_DECIMAL:
				case NEGATIVE_DECIMAL:
				case CURRENCY:
				case LOCATION_LATITUDE:
				case LOCATION_LONGITUDE:
				case T_CODE:
				case T_ICON:
				case TEXT:
					valueToReturn = inputValue;
					break;
				case ATTACHMENT:
				case AUDIO:
				case VIDEO:
				case IMAGE:
				case T_ID:
					byte[] byteInput = inputValue.toString().getBytes();
					valueToReturn = convertByteToString(byteInput);
					break;
				case T_BLOB:
					Blob blobInput = (Blob) inputValue;
					valueToReturn = getBlobJson(blobInput);
					break;
				default:
					valueToReturn=inputValue;
					valueToReturn = valueToReturn.toString();
					break;
			}
		}
		return valueToReturn;
	}

	public static String getSqlStringForTable(EntityMetadataVOX entityMetadataVOX) {
//		List<EntityAttributeMetadata> filteredBlobData = GlobalVariables.entityMetadataVOX.getAttributeMeta().stream()
//												.filter(p -> p.getDbDataTypeCode().equals("T_BLOB"))
//												.collect(Collectors.toList());
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		for(EntityAttributeMetadata attribute : entityMetadataVOX.getAttributeMeta()){
			if(attribute.getDbDataTypeCode().equals("T_BLOB") && attribute.isGlocalizedFlag() && attribute.getContainerBlobDbColumnName() == null){
				sql.append("COLUMN_JSON(" + attribute.getDbColumnName() + ") AS '" + attribute.getDoAttributeCode() + "', ");
			}
			else if(attribute.getContainerBlobDbColumnName() != null) {
				if(attribute.getDbDataTypeCode().equals("T_BLOB") && attribute.isGlocalizedFlag()){
					//FIXME Add code for COLUMN_GET(attribute.getContainerBlobDbColumnName(), COLUMN_JSON(attribute.getDoAttributeCode()) as BINARY?)) //for G11n inside Blob
					sql.append("");
				}
				else sql.append("COLUMN_GET(" + attribute.getContainerBlobDbColumnName() + ", '" + attribute.getDoAttributeCode() + "' " +
						" AS " + Util.getBlobDataTypeFromEntityDataType(attribute.getDbDataTypeCode()) +") AS '" + attribute.getDoAttributeCode() + "', ");
			}
			else if(!attribute.getDbDataTypeCode().equals("T_BLOB")){
				sql.append(attribute.getDbColumnName()  + " as '" + attribute.getDoAttributeCode() + "', ");
			}
		}
		sql.replace(sql.length()-2, sql.length(), "");
		sql.append(" FROM " + entityMetadataVOX.getDbTableName() + " ");
//		for(EntityAttributeMetadata attribute : filteredBlobData){
//
//		}
//		if(filteredData != null && !filteredData.isEmpty())
//			sql.replace(sql.length()-2, sql.length()-1, " ");
		return sql.toString();
	}

	public static String getSqlStringForMasterTable(MasterEntityMetadataVOX entityMetadataVOX) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		for(MasterEntityAttributeMetadata attribute : entityMetadataVOX.getAttributeMeta()){
			if(attribute.getDbDataTypeCode().equals("T_BLOB") && attribute.isGlocalized() && attribute.getContainerBlobDbColumnName() == null){
				sql.append("COLUMN_JSON(" + attribute.getDbColumnName() + ") AS '" + attribute.getDoAttributeCode() + "', ");
			}
			else if(attribute.getContainerBlobDbColumnName() != null) {
				if(attribute.getDbDataTypeCode().equals("T_BLOB") && attribute.isGlocalized()){
					//FIXME Add code for COLUMN_GET(attribute.getContainerBlobDbColumnName(), COLUMN_JSON(attribute.getDoAttributeCode()) as BINARY?)) //for G11n inside Blob
					sql.append("");
				}
				else sql.append("COLUMN_GET(" + attribute.getContainerBlobDbColumnName() + ", '" + attribute.getDoAttributeCode() + "' " +
						" AS " + Util.getBlobDataTypeFromEntityDataType(attribute.getDbDataTypeCode()) +") AS '" + attribute.getDoAttributeCode() + "', ");
			}
			else if(!attribute.getDbDataTypeCode().equals("T_BLOB")){
				sql.append(attribute.getDbColumnName()  + " as '" + attribute.getDoAttributeCode() + "', ");
			}
		}
		sql.replace(sql.length()-2, sql.length(), "");
		sql.append(" FROM " + entityMetadataVOX.getDbTableName() + " ");
		return sql.toString();
	}

	public static List<Map<String, Object>> convertRawDataIntoTypedData(List<Map<String, Object>> data, EntityMetadataVOX entityMetadataVOX) {
		for(Map<String, Object> record : data){
			Map<String, Object> newColumnValueMap = new HashMap<>();
			for(Entry<String, Object> column : record.entrySet()){
				if(column.getKey() == null || column.getKey().isEmpty()) continue;
				EntityAttributeMetadata attributeMeta = entityMetadataVOX.getAttributeByName(column.getKey());
				EntityDataTypeEnum eDt = EntityDataTypeEnum.valueOf(attributeMeta.getDbDataTypeCode());
				switch (eDt) {
					case BOOLEAN:
					case POSITIVE_INTEGER:
					case NEGATIVE_INTEGER:
					case BIG_INTEGER:
					case POSITIVE_DECIMAL:
					case NEGATIVE_DECIMAL:
					case CURRENCY:
					case LOCATION_LATITUDE:
					case LOCATION_LONGITUDE:
//					valueToReturn = (String)object;
						newColumnValueMap.put(attributeMeta.getDbColumnName(), column.getValue());
						break;
					case DATE_TIME:
					case T_CODE:
					case T_ICON:
					case TEXT:
//					valueToReturn = "'" + (String)object + "'";
						newColumnValueMap.put(attributeMeta.getDbColumnName(), column.getValue());
						break;
					case ATTACHMENT:
					case AUDIO:
					case VIDEO:
					case IMAGE:
					case T_ID:
					case T_IMAGEID:
					case T_ATTACH:
						if(column.getValue() != null){
							record.put(column.getKey(), "0x"+Hex.encodeHexString((byte[]) column.getValue()).toUpperCase());
							newColumnValueMap.put(attributeMeta.getDbColumnName(), "0x"+column.getValue());
						}
						break;
					case T_BLOB:
						if(column.getValue() != null){
							ObjectMapper mapper = new ObjectMapper();
							try {
								record.put(column.getKey(), mapper.readTree((byte[]) column.getValue()));
								newColumnValueMap.put(attributeMeta.getDbColumnName(), column.getValue());
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						break;
					default:
//					valueToReturn = "'" + (String)object + "'";
						break;
				}
			}
//			record.putAll(newColumnValueMap);
		}
		return data;
	}

	public static Map<String, Object> getChangedPreviousFields(Map<String, Object> previousRecord,
															   Map<String, Object> nextRecord, EntityMetadataVOX entityMetadataVOX) throws ParseException {
		Map<String, Object> changedFields = new HashMap<>();
		Object previousValue;
		Object nextValue;
		for(Entry<String, Object> previousField : previousRecord.entrySet()){
			if(previousField.getValue() != null && nextRecord.get(previousField.getKey()) != null){
				EntityAttributeMetadata attributeMeta = entityMetadataVOX.getAttribute(previousField.getKey());
				EntityDataTypeEnum eDt = EntityDataTypeEnum.valueOf(attributeMeta.getDbDataTypeCode());
				switch (eDt) {
					case BOOLEAN:
						previousValue = (boolean) previousField.getValue();
						nextValue = (boolean) nextRecord.get(previousField.getKey());
						if((!previousValue.equals(nextValue)) || (previousValue == null ^ nextValue == null))
							changedFields.put(previousField.getKey(), previousValue);
						break;
					case POSITIVE_INTEGER:
					case NEGATIVE_INTEGER:
						if (nextRecord.get(previousField.getKey()) instanceof String){
							if (!((String) nextRecord.get(previousField.getKey())).isEmpty())
								nextValue = Integer.valueOf((String) nextRecord.get(previousField.getKey()));
							else nextValue = null;
						}
						else nextValue = nextRecord.get(previousField.getKey());
						if(previousField.getValue()==null){
							changedFields.put(previousField.getKey(), previousField.getValue());
						}else{
							previousValue = (Integer) previousField.getValue();
							if (previousValue == null) changedFields.put(previousField.getKey(), previousValue);
							if((previousValue == null ^ nextValue == null) || (!previousValue.equals(nextValue)))
								changedFields.put(previousField.getKey(), previousValue);
						}
						break;
					case BIG_INTEGER:
						if (nextRecord.get(previousField.getKey()) instanceof String){
							if (!((String) nextRecord.get(previousField.getKey())).isEmpty())
								nextValue = BigInteger.valueOf((long) nextRecord.get(previousField.getKey()));
							else nextValue = null;
						}
						else nextValue = nextRecord.get(previousField.getKey());
						if(previousField.getValue()==null){
							changedFields.put(previousField.getKey(), nextValue);
						}else{
							previousValue = (BigInteger) previousField.getValue();
							if (previousValue == null) changedFields.put(previousField.getKey(), nextValue);
							if((previousValue == null ^ nextValue == null) || (((BigInteger)previousValue).compareTo((BigInteger) nextValue)) != 0)
								changedFields.put(previousField.getKey(), nextValue);
						}
						break;
					case POSITIVE_DECIMAL:
					case NEGATIVE_DECIMAL:
					case CURRENCY:
					case LOCATION_LATITUDE:
					case LOCATION_LONGITUDE:
						if (nextRecord.get(previousField.getKey()) instanceof String){
							if (!((String) nextRecord.get(previousField.getKey())).isEmpty())
								nextValue = new BigDecimal((String) nextRecord.get(previousField.getKey()));
							else nextValue = null;
						}
						else nextValue = nextRecord.get(previousField.getKey());
						if(previousField.getValue()==null){
							changedFields.put(previousField.getKey(), previousField.getValue());
						}else{
							previousValue = (BigDecimal) previousField.getValue();
							if (previousValue == null) changedFields.put(previousField.getKey(), previousValue);
							if((previousValue == null ^ nextValue == null) || (((BigDecimal)previousValue).compareTo((BigDecimal)(nextValue))) != 0)
								changedFields.put(previousField.getKey(), previousValue);
						}
						break;
//						previousValue = (long) previousField.getValue();
//						nextValue = (long) nextRecord.get(previousField.getKey());
//						if((!previousValue.equals(nextValue)) || (previousValue == null ^ nextValue == null))
//							changedFields.put(previousField.getKey(), previousValue);
//						break;
					case DATE_TIME:
					case DATE:
						previousValue = (Timestamp) previousField.getValue();
						nextValue = (Timestamp) nextRecord.get(previousField.getKey());
						if((!previousValue.equals(nextValue)) || (previousValue == null ^ nextValue == null))
							changedFields.put(previousField.getKey(), previousValue);
						break;
					case TIME:
						if (nextRecord.get(previousField.getKey())== null || nextRecord.get(previousField.getKey()).toString().equals("")){
							if (previousField.getValue() != null)
								changedFields.put(previousField.getKey(), null);
						}
						else {
							DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
							nextValue = nextRecord.get(previousField.getKey()).toString();
							Object nextValueToCompare = new Time(formatter.parse(nextRecord.get(previousField.getKey()).toString()).getTime());
							if (previousField.getValue() == null) {
								changedFields.put(previousField.getKey(), nextValue);
							}else {
								previousValue = previousField.getValue();
								if (!compareTimes((Time)previousValue, (Time) nextValueToCompare))
									changedFields.put(previousField.getKey(), nextValueToCompare);
							}
						}
						break;
					case T_CODE:
					case T_ICON:
					case TEXT:
					case T_BIG_TEXT:
					case LOCATION:
					case LONG_TEXT:
						previousValue = (String) previousField.getValue();
						nextValue = (String) nextRecord.get(previousField.getKey());
						if (attributeMeta.isGlocalizedFlag()){
							if (!compareG11nStrings((String) previousValue, (String) nextValue))
								changedFields.put(previousField.getKey(), nextValue);
						}
						else if((!previousValue.equals(nextValue)) || (previousValue == null ^ nextValue == null))
							changedFields.put(previousField.getKey(), previousValue);
						break;
					case T_ID:
					case T_ATTACH:
					case T_IMAGEID:
					case AUDIO:
					case VIDEO:
					case IMAGE:
						previousValue = ((String) previousField.getValue()).toUpperCase();
						nextValue = ((String) nextRecord.get(previousField.getKey())).toUpperCase();
						if((!previousValue.equals(nextValue)) || (previousValue == null ^ nextValue == null))
							changedFields.put(previousField.getKey(), previousValue);
						break;
					case T_BLOB:
						if(!attributeMeta.isGlocalizedFlag()){
							//code for comparing individual elements of two blobs provided.
						}
						else{
							previousValue = (JsonNode) previousField.getValue();
							nextValue = (JsonNode) nextRecord.get(previousField.getKey());
							if((!previousValue.equals(nextValue)) || (previousValue == null ^ nextValue == null))
								changedFields.put(previousField.getKey(), previousValue);
						}
						break;
					default:
						break;
				}
			}
//			else if(nextRecord.get(previousField.getKey()) != null){//FIXME see if required!
//				changedFields.put(previousField.getKey(), null);
//			}
		}
		return changedFields;
	}


	public static String convertByteToString(byte[] input){
		if(input == null)
			return null;
		return "0x"+(Hex.encodeHexString(input)).toUpperCase();
	}
	public static byte[] convertStringIdToByteId(String id){
		id = (String)remove0x(id);
		if(!StringUtil.isDefined(id))
			return null;
		try {
			return (Hex.decodeHex(id.toCharArray()));
		} catch (DecoderException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static JsonNode getBlobJson(Blob input) throws IOException, SQLException{
		if(input == null)
			return null;
		ObjectMapper mapper = new ObjectMapper();
		JsonNode result = mapper.readTree(input.getBinaryStream());
		return result;
	}
	public static byte[] serialize(Object obj) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream os = new ObjectOutputStream(out);
		os.writeObject(obj);
		return out.toByteArray();
	}
	public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		ObjectInputStream is = new ObjectInputStream(in);
		return is.readObject();
	}
	public static JsonNode getG11NBlob(JsonNode input){
		JsonNode output = null;
		if(input==null)
			return null;
		try{
			output = input.fields().next().getValue();
		}catch (NoSuchElementException e){
			return null;
		}
		return output;
	}

	public static List<String> getAttributePathFromExpression(String attrExpression) {
		List<String> result = new ArrayList<String>();
		Pattern pattern = Pattern.compile("!\\{(.*?)\\}");
		Matcher matcher = pattern.matcher(attrExpression);
		while (matcher.find()) {
			result.add(matcher.group(1));
		}
		return result;
	}
	public static String getDOAFromAttributePath(String attributePath){
		if(attributePath.contains(":")){
			return attributePath.substring(attributePath.lastIndexOf(":")+1);
		}else{
			return attributePath;
		}
	}

	public static String getBaseDOAFromAtrributePath(String attributePath) {
		if(attributePath.contains(":")){
			return attributePath.substring(0, attributePath.indexOf(":"));
		}else{
			return attributePath;
		}
	}
	/**
	 * @param operator
	 * @return
	 */
	public static boolean isOperatorAFunction(Operator operator) {
		switch(operator){
			case DBCOUNT:
			case DBAVERAGE:
			case DBMAX:
			case DBMIN:
			case DBSTDDEV:
			case DBSUM:
			case DBVAR:
			case CONCAT:
			case DAY:
			case MONTH:
			case YEAR:
			case WEEKDAY:
			case LASTDAY:
			case MATCH:
			case AGAINST_IN_BOOLEAN_MODE:
			case DATESUB:
			case MOD:
			case INTCAST:
			case G11N_PLURAL:
			case G11N_STATIC:
			case INTERVAL_YR:
			case INTERVAL_MONTH:
			case INTERVAL_DAY:
			case INTERVAL_WEEK:
			case EXCHANGERATE:
			case DATE_FORMAT:
			case COALESCE:
			case LEFT:
			case FIELD:
			case SUBSTRING:
			return true;
			default: return false;
		}
	}
	public static AttachmentObject getURLforS3(Object currentValue, AmazonS3 s3Client) {
		if(currentValue == null)
			return null;
		String id = (String) currentValue;
		AttachmentObject attachmentObject = new AttachmentObject();
		attachmentObject.setGetUrl(preSignedGetUrl(id, s3Client));
		attachmentObject.setHeadUrl(preSignedHeadUrl(id, s3Client));
		return attachmentObject;
	}

	private static URL preSignedHeadUrl(String id, AmazonS3 s3Client) {
		java.util.Date expiration = new java.util.Date();
		long msec = expiration.getTime();
		msec = msec+ 1000*60 *60 * 24 * 30l;
//		msec += 1000 * 60 * 60 * 24 * 30; // Add 1 month.
		expiration.setTime(msec);
		String bucketName = "mytapplent"+"/"+ TenantContextHolder.getCurrentTenantID();
		GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, id);
		generatePresignedUrlRequest.setMethod(HttpMethod.HEAD);
		generatePresignedUrlRequest.setExpiration(expiration);
		generatePresignedUrlRequest.getCustomRequestHeaders();
		URL s = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
		return s;
	}

	public static URL preSignedGetUrl(String id, AmazonS3 s3Client){
		if (id == null)
			return null;
		if (s3Client == null)
			s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
		java.util.Date expiration = new java.util.Date();
		long msec = expiration.getTime();
		msec = msec+ 1000*60 *60 * 24 * 30l;
//		msec += 1000 * 60 * 60 * 24 * 30; // Add 1 month.
		expiration.setTime(msec);
		String bucketName = "mytapplent"+"/"+ TenantContextHolder.getCurrentTenantID();
		GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, id);
		generatePresignedUrlRequest.setMethod(HttpMethod.GET);
		generatePresignedUrlRequest.setExpiration(expiration);
		generatePresignedUrlRequest.getCustomRequestHeaders();
		URL s = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
		return s;
	}

	public static void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} catch (IOException e){
			e.printStackTrace();
		}
		finally {
			is.close();
			os.close();
		}
	}

	public static IconVO getIcon(String iconCode) {
		IconVO icon = null;
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		icon = metadataService.getIconDetailsByIconCode(iconCode);
		return icon;
	}

	public static String uploadAttachmentToS3(String contentType, InputStream dataStream) {//FIXME for svg files if content-type is wrong.

		String keyId = null;
		String existingBucketName = "mytapplent";

		String amazonFileUploadLocation = existingBucketName + "/" + TenantContextHolder.getCurrentTenantID();
		BufferedInputStream stream = null;
		if(dataStream != null && contentType != null) {
			try {
				stream = new BufferedInputStream(dataStream);
				ObjectMetadata objectMetadata = new ObjectMetadata();
				FileNameMap fileNameMap = URLConnection.getFileNameMap();
				objectMetadata.setContentType(contentType);
				keyId = Common.getUUID();
				AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
				PutObjectRequest putObjectRequest1 = new PutObjectRequest(amazonFileUploadLocation, keyId, stream, objectMetadata);
				PutObjectResult result = s3Client.putObject(putObjectRequest1);
				stream.close();
				System.out.println("Etag:" + result.getETag() + "-->" + result);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else{
			DBUploadErrorLog.addError("file Path { } is not present.");
			return "4B76E2243DB7696EB207617C0EF91585";
		}
		return keyId;

	}

	public static String getG11nString(String localeCode, Object value) {
		if(value!=null){
			String stringValue = value.toString();
			if(stringValue.contains("\"")){
				// |% is the escape character
				stringValue = stringValue.replace("\"","|%");
			}
			return "\""+localeCode+"\":\""+stringValue+"\"";
		}
		return null;
	}
	public static String getG11nValue(String value, String personId){
		if(StringUtil.isDefined(value)) {
			if(!StringUtil.isDefined(personId)) {
				UserContext user = TenantContextHolder.getUserContext();
				personId = user.getPersonId();
			}
			PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
			Person person = personService.getPersonDetails(personId);
			String localeCode = person.getPersonPreferences().getLocaleCodeFkId();
            /* Now we have locale code we need to fetch the correct value */
			int startLocaleIndex = value.indexOf("\""+localeCode+"\":");
			if(startLocaleIndex== -1)
				startLocaleIndex = value.indexOf("\""+person.getOrgDefaultLocale()+"\":");
			if(startLocaleIndex== -1)
				startLocaleIndex = value.indexOf("\"" + TenantAwareCache.getMetaDataRepository().getTenantDefaultLocale() + "\":");
			if(startLocaleIndex!=-1){
				int midLocaleIndex = value.indexOf("VL\":\"",startLocaleIndex);
				int endLocaleIndex = value.indexOf("\"", midLocaleIndex+5);
				String subFinalString = value.substring(midLocaleIndex+5,endLocaleIndex);
				subFinalString = subFinalString.substring(1);
				return subFinalString.replaceAll("\\|%","\"").replaceAll("\\r","\n");
			}
		}
		return null;
	}

	public static String getG11nValueByLocaleCode(String value, String localeCode){
		if(StringUtil.isDefined(value)) {
            /* Now we have locale code we need to fetch the correct value */
            String personId = TenantContextHolder.getUserContext().getPersonId();
			PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
			Person person = personService.getPersonDetails(personId);
			int startLocaleIndex = value.indexOf("\""+localeCode+"\":");
			if(startLocaleIndex== -1)
				startLocaleIndex = value.indexOf("\""+person.getOrgDefaultLocale()+"\":");
			if(startLocaleIndex== -1)
				startLocaleIndex = value.indexOf("\"" + TenantAwareCache.getMetaDataRepository().getTenantDefaultLocale() + "\":");
			if(startLocaleIndex!=-1){
				int midLocaleIndex = value.indexOf("VL\":\"",startLocaleIndex);
				int endLocaleIndex = value.indexOf("\"", midLocaleIndex+5);
				String subFinalString = value.substring(midLocaleIndex+5,endLocaleIndex);
				subFinalString = subFinalString.substring(1);
				return subFinalString.replaceAll("\\|%","\"").replaceAll("\\r","\n");
			}
		}
		return null;
	}
	/**
	 * This function pareses the given expn accroding to the pattern defined for Control types such as ICON_TEXT, ICON_LABEL_TEXT etc.
	 * @param controlPathExpn
	 * @return
	 */
	public static List<String> getPathExpnForHardcodedControlType(String controlPathExpn){
		List<String> result = new ArrayList<>();
//        String line = "getDOIcon(!{Task.DOName})|!{Task.DOName}+\" | \"+!{Task.Disp+\"kduhsgh\"+layContextValue}";
		String[] quotesSplittedStrings = controlPathExpn.split("(\".*?\")");
		for (String s : quotesSplittedStrings){
			String[] strings = s.split("[\\|\\+]");
			for (String st : strings){
				result.add(st);
			}
		}
		return result;
	}

	public static boolean isPropertyControlExpn(String expn) {
		if(expn.contains("getAttributeIcon") ||expn.contains("getAttributeName")||expn.contains("getBTPEIcon")||expn.contains("getBTPESummaryTitle")||expn.contains("getTotalRecordCount")||expn.contains("getBTPEHelpText"))
			return true;
		return false;
	}
	public static Map<String,Object> getChangedCurrentFields(Map<String, Object> previousRecord, Map<String, Object> currentRecord, EntityMetadataVOX entityMetadataVOX) throws ParseException {
		Map<String, Object> changedFields = new HashMap<>();
		Object previousValue;
		Object currentValue;
		for(Entry<String, Object> previousField : previousRecord.entrySet()){
			if(currentRecord.get(previousField.getKey()) != null){
				EntityAttributeMetadata attributeMeta = entityMetadataVOX.getAttribute(previousField.getKey());
				EntityDataTypeEnum eDt = EntityDataTypeEnum.valueOf(attributeMeta.getDbDataTypeCode());
				switch (eDt) {
					case BOOLEAN:
						if (currentRecord.get(previousField.getKey()) instanceof String) {// FOR HIDDEN CONTROLS
							currentValue = null;
							previousValue = previousField.getValue();
							if (currentRecord.get(previousField.getKey()).equals("0"))
								currentValue = false;
							else if (currentRecord.get(previousField.getKey()).equals("1"))
								currentValue = true;
							if (previousValue != null) {
								if ((boolean)previousValue ^ (boolean)currentValue) {
									changedFields.put(previousField.getKey(), currentValue);
								}
							}
							else changedFields.put(previousField.getKey(), currentValue);
						}//not required now as it is handled in server rule manager itself!
						else {
							currentValue = (boolean) currentRecord.get(previousField.getKey());
							if (previousField.getValue() == null) {
								changedFields.put(previousField.getKey(), currentValue);
							} else {
								previousValue = previousField.getValue();
								if (previousValue == null) changedFields.put(previousField.getKey(), currentValue);
								else if ((previousValue == null ^ currentValue == null) || (!previousValue.equals(currentValue)))
									changedFields.put(previousField.getKey(), currentValue);
							}
						}
						break;
					case POSITIVE_INTEGER:
					case NEGATIVE_INTEGER:
						if (currentRecord.get(previousField.getKey()) instanceof String){
							if (!((String) currentRecord.get(previousField.getKey())).isEmpty())
								currentValue = Integer.valueOf((String) currentRecord.get(previousField.getKey()));
							else currentValue = null;
						}
						else currentValue = currentRecord.get(previousField.getKey());
						if(previousField.getValue()==null){
							changedFields.put(previousField.getKey(), currentValue);
						}else{
							previousValue = (Integer) previousField.getValue();
							if (previousValue == null) changedFields.put(previousField.getKey(), currentValue);
							if((previousValue == null ^ currentValue == null) || (((Integer)previousValue).compareTo((Integer)currentValue)) != 0)
								changedFields.put(previousField.getKey(), currentValue);
						}
						break;
					case BIG_INTEGER:
						if (currentRecord.get(previousField.getKey()) instanceof String){
							if (!((String) currentRecord.get(previousField.getKey())).isEmpty())
								currentValue = BigInteger.valueOf((long) currentRecord.get(previousField.getKey()));
							else currentValue = null;
						}
						else currentValue = currentRecord.get(previousField.getKey());
						if(previousField.getValue()==null){
							changedFields.put(previousField.getKey(), currentValue);
						}else{
							previousValue = (BigInteger) previousField.getValue();
							if (previousValue == null) changedFields.put(previousField.getKey(), currentValue);
							if((previousValue == null ^ currentValue == null) || (((BigInteger)previousValue).compareTo((BigInteger) currentValue)) != 0)
								changedFields.put(previousField.getKey(), currentValue);
						}
						break;
					case POSITIVE_DECIMAL:
					case NEGATIVE_DECIMAL:
					case CURRENCY:
					case LOCATION_LATITUDE:
					case LOCATION_LONGITUDE:
						if (currentRecord.get(previousField.getKey()) instanceof String){
							if (!((String) currentRecord.get(previousField.getKey())).isEmpty())
								currentValue = new BigDecimal((String) currentRecord.get(previousField.getKey()));
							else currentValue = null;
						}
						else currentValue = currentRecord.get(previousField.getKey());
						if(previousField.getValue()==null){
							changedFields.put(previousField.getKey(), currentValue);
						}else{
							previousValue = previousField.getValue();
							if (previousValue == null) changedFields.put(previousField.getKey(), currentValue);
							if((previousValue == null ^ currentValue == null) || (((BigDecimal) previousValue).compareTo((BigDecimal) currentValue) != 0))
								changedFields.put(previousField.getKey(), currentValue);
						}
						break;
					case DATE_TIME:
					case DATE:
						if (currentRecord.get(previousField.getKey())== null || currentRecord.get(previousField.getKey()).toString().equals("")){
							if (previousField.getValue() != null)
								changedFields.put(previousField.getKey(), null);
						}
						else {
							currentValue = Timestamp.valueOf(currentRecord.get(previousField.getKey()).toString());
							if (previousField.getValue() == null) {
								changedFields.put(previousField.getKey(), currentValue);
							} else {
								previousValue = previousField.getValue();
								if (previousValue == null) changedFields.put(previousField.getKey(), currentValue);
								if (/*(previousValue == null ^ currentValue == null) ||*/ (!previousValue.equals(currentValue)))
									changedFields.put(previousField.getKey(), currentValue);
							}
						}
						break;
					case TIME:
						if (currentRecord.get(previousField.getKey())== null || currentRecord.get(previousField.getKey()).toString().equals("")){
							if (previousField.getValue() != null)
								changedFields.put(previousField.getKey(), null);
						}
						else {
							DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
							currentValue = currentRecord.get(previousField.getKey()).toString();
							Object currentValueToCompare = new Time(formatter.parse(currentRecord.get(previousField.getKey()).toString()).getTime());
							if (previousField.getValue() == null) {
								changedFields.put(previousField.getKey(), currentValue);
							}else {
								previousValue = previousField.getValue();
								if (!compareTimes((Time)previousValue, (Time) currentValueToCompare))
									changedFields.put(previousField.getKey(), currentValueToCompare);
							}
						}
						break;
					case T_CODE:
					case T_ICON:
					case TEXT:
					case T_BIG_TEXT:
					case LOCATION:
					case LONG_TEXT:
						currentValue = (String) currentRecord.get(previousField.getKey());
						if(previousField.getValue()==null){
							changedFields.put(previousField.getKey(), currentValue);
						}else {
							previousValue = (String) previousField.getValue();
							if (previousValue == null) changedFields.put(previousField.getKey(), currentValue);
							if (attributeMeta.isGlocalizedFlag()){
								if (!compareG11nStrings((String) previousValue, (String) currentValue))
									changedFields.put(previousField.getKey(), currentValue);
							}
							else if ((previousValue == null ^ currentValue == null) || (!previousValue.equals(currentValue)))
								changedFields.put(previousField.getKey(), currentValue);
						}
						break;
					case T_ID:
					case T_ATTACH:
					case T_IMAGEID:
					case AUDIO:
					case VIDEO:
					case IMAGE:
						currentValue = ((String) currentRecord.get(previousField.getKey()));
						if(previousField.getValue()==null){
							changedFields.put(previousField.getKey(), currentValue);
						}else {
							previousValue = ((String) previousField.getValue());
							if (previousValue == null) changedFields.put(previousField.getKey(), currentValue);
							if ((previousValue == null ^ currentValue == null) || !((String) previousValue).equalsIgnoreCase((String) currentValue))
								changedFields.put(previousField.getKey(), currentValue);
						}
						break;
					case T_BLOB:
						if(!attributeMeta.isGlocalizedFlag()){
							//code for comparing individual elements of two blobs provided.
						}
						else{
							currentValue = (JsonNode) currentRecord.get(previousField.getKey());
							if(previousField.getValue()==null){
								changedFields.put(previousField.getKey(), currentValue);
							}else{
								previousValue = (JsonNode) previousField.getValue();
								if (previousValue == null) changedFields.put(previousField.getKey(), currentValue);
								if((previousValue == null ^ currentValue == null) || (!previousValue.equals(currentValue)))
									changedFields.put(previousField.getKey(), currentValue);
							}
						}
						break;
					default:
						break;
				}
			}
		}
		return changedFields;
	}

	private static boolean compareTimes(Date previousTime, Date currentTime) {
		Calendar c1 = Calendar.getInstance();
		c1.setTime(previousTime);
		Calendar c2 = Calendar.getInstance();
		c2.setTime(currentTime);
		if (c1.get(Calendar.HOUR_OF_DAY) == c2.get(Calendar.HOUR_OF_DAY) &&
				c1.get(Calendar.MINUTE) == c2.get(Calendar.MINUTE) &&
				c1.get(Calendar.SECOND) == c2.get(Calendar.SECOND))
			return true;
		return false;
	}

	public static Map<String,G11n> getG11nMapFromString(String g11nString) {
    	if (g11nString == null || g11nString.isEmpty())
    		return null;
    	Map<String, G11n> g11nMap = new HashMap<>();
//		int startLocaleIndex = 0;//value.indexOf("\""+localeCode+"\"");
//		while(startLocaleIndex+2 < g11nString.length()){
//			int midLocaleIndex = g11nString.indexOf("\":\"",startLocaleIndex);
//			int endLocaleIndex = g11nString.indexOf("\"", midLocaleIndex+3);
//			String localeCode = g11nString.substring(startLocaleIndex+1,midLocaleIndex);
//			String subFinalString = g11nString.substring(midLocaleIndex+3,endLocaleIndex);
//			subFinalString = subFinalString.replaceAll("\\|%","\"");
//			startLocaleIndex = endLocaleIndex+1;
//			g11nMap.put(localeCode, subFinalString);
//		}
		JSONObject g11nJsonObject = new JSONObject(g11nString);
		Iterator<String> g11nIterator = g11nJsonObject.keys();
		String localeCode;
		G11n g11n;
		while (g11nIterator.hasNext()){
			localeCode = g11nIterator.next();
			JSONObject g11nJsonValue = g11nJsonObject.getJSONObject(localeCode);
			g11n = new G11n();
			g11n.setVL(g11nJsonValue.getString("VL"));
			g11n.setIM(g11nJsonValue.getString("IM"));
			g11n.setTSL(g11nJsonValue.getString("TSL"));
			g11nMap.put(localeCode, g11n);
		}
		return g11nMap;
	}

	public static void buildActivityLogMsg(ActivityLog activityLog, AuditMsgFormat auditMsgFormat, Map<String, String> expressionToValueMap) {
		if(auditMsgFormat != null /*&& auditMsgFormat.getMsgSyntaxInContextG11nBigTxt() != null*/) {
			String msgYouInContext = auditMsgFormat.getMsgSyntaxYouInContextG11nBigTxt();
			String msgYouOutContext = auditMsgFormat.getMsgSyntaxYouOutOfContextG11nBigTxt();
			String msgSyntaxThirdPersonInContext = auditMsgFormat.getMsgSyntaxThirdPersonInContextG11nBigTxt();
			String msgSyntaxThirdPersonOutOfContext = auditMsgFormat.getMsgSyntaxThirdPersonOutOfContextG11nBigTxt();

			activityLog.setMsgYouInContextG11nBigTxt(getMsgWithActualValues(msgYouInContext, expressionToValueMap));
			activityLog.setMsgYouOutOfContextG11nBigTxt(getMsgWithActualValues(msgYouOutContext, expressionToValueMap));
			activityLog.setMsgThirdPersonInContextG11nBigTxt(getMsgWithActualValues(msgSyntaxThirdPersonInContext, expressionToValueMap));
			activityLog.setMsgThirdPersonOutOfContextG11nBigTxt(getMsgWithActualValues(msgSyntaxThirdPersonOutOfContext, expressionToValueMap));

//			activityLog.setActionMsgInContextG11nBigText(getMsgWithActualValues(msgInContext, expressionToValueMap));
//			if(auditMsgFormat.getMsgSynGenCondtnExpn() != null && statifiesMsgExpnCondition(auditMsgFormat.getMsgSynGenCondtnExpn(), expressionToValueMap))//FIXME check condition
//				activityLog.setActionMsgGenericG11nBigText(getMsgWithActualValues(msgSynGenWoCompTextG11nBigTxt, expressionToValueMap));
//			else activityLog.setActionMsgGenericG11nBigText(getMsgWithActualValues(msgSynGenWCompTextG11nBigTxt, expressionToValueMap));
		}
	}

//	private static boolean statifiesMsgExpnCondition(String msgSynGenCondtnExpn, Map<String, String> expressionToValueMap) {
//		String actualExpn = getActualValue(msgSynGenCondtnExpn, "en_US", expressionToValueMap);
//		String[] strings = actualExpn.split("=");
//		if(strings[0].trim().equals(strings[1].trim()))
//			return true;
//		return false;
//	}

	private static String getMsgWithActualValues(String message, Map<String, String> expressionToValueMap) {
		Map<String, G11n> g11nMap = Util.getG11nMapFromString(message);
		for(Map.Entry<String, G11n> entry : g11nMap.entrySet()) {
			String localeCode = entry.getKey();
			String localeMsg = entry.getValue().getVL();
			localeMsg = getActualValue(localeMsg, localeCode, expressionToValueMap);
			entry.getValue().setVL(localeMsg);
		}
		return Util.getG11nStringFromMap(g11nMap);
	}

	private static String getG11nStringFromMap(Map<String, G11n> g11nMap) {
		JSONObject finalJsonObject = new JSONObject();
		for (Entry<String, G11n> entry : g11nMap.entrySet()) {
			JSONObject newJsonObject = new JSONObject();
			if (!entry.getValue().getVL().isEmpty()) {
				String value = entry.getValue().getVL().trim();
				String transValue = value.replace("\"", "|%");
				String resultStr = " " + transValue;
				newJsonObject.put("VL", resultStr);
			} else {
				newJsonObject.put("VL", entry.getValue().getVL());
			}
			newJsonObject.put("IM", entry.getValue().getIM());
			newJsonObject.put("TSL", entry.getValue().getTSL());
			finalJsonObject.put(entry.getKey(), newJsonObject);
		}
		return finalJsonObject.toString();
	}

	private static String getActualValue(String expression, String localeCode, Map<String, String> expressionToValueMap) {
		Pattern doPattern = Pattern.compile("\\!\\{(.*?)\\}");
		Pattern rePattern = Pattern.compile("(\\!\\{(.*?)\\})");
//		Pattern youPattern = Pattern.compile("(\\!\\{(.*?)\\}\\~\\{[Y|y]ou\\})");
		Matcher doMatcher = doPattern.matcher(expression);
		Matcher reMatcher = rePattern.matcher(expression);
//		Matcher youMatcher = youPattern.matcher(expression);
		while (doMatcher.find() && reMatcher.find()) {
			String doaName = doMatcher.group(1);
			String resolvedValue = null;
			MasterEntityAttributeMetadata attributeMeta = null;
			if (doaName.contains(":")) {
				String[] strings = doaName.split(":",2);
				attributeMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(strings[1]);
			}else{
				attributeMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(doaName);
			}
			if (attributeMeta.isGlocalized()) {
				resolvedValue = Util.getG11nValueByLocaleCode(expressionToValueMap.get(doaName), localeCode);
			}
			else
				resolvedValue = expressionToValueMap.get(doaName);
			if (resolvedValue != null) {
//				if (youMatcher.find())
//					expression = expression.replace(reMatcher.group(1), (CharSequence) "!{" + resolvedValue + "~" + TenantContextHolder.getUserContext().getPersonId() + "}");
//				else
					expression = expression.replace(reMatcher.group(1), (CharSequence) resolvedValue);
			}
		}
		expression = replaceAllStaticValues(expression, expressionToValueMap);
		return expression;
	}

	private static String replaceAllStaticValues(String expression, Map<String, String> expressionToValueMap) {
		Pattern staticPattern = Pattern.compile("(\\$\\{(.*?)\\})");
		Matcher staticMatcher = staticPattern.matcher(expression);
		while (staticMatcher.find()) {
			String resolvedValue = expressionToValueMap.get(staticMatcher.group(1));
			if (resolvedValue != null)
				expression = expression.replace(staticMatcher.group(1), (CharSequence) expressionToValueMap.get(staticMatcher.group(1)));
		}
		return expression;
	}

	public static Map<String,Object> buildContextDoaValueMap(Map<String, Object> doaValueMap, ActivityLog activityLog, EntityMetadataVOX activityMetadataVOX, String currentTimestampString) {
		Map<String, Object> contextualMap = new HashMap<>();
		contextualMap.putAll(doaValueMap);
		String DOCode = activityMetadataVOX.getDomainObjectCode();
		contextualMap.put(DOCode+".PrimaryKeyID", activityLog.getActivityLogPkId());
		contextualMap.put(DOCode+".ActivityAuthorPerson", activityLog.getActivityAuthorPerson());
		contextualMap.put(DOCode+".ProcessTypeCode", activityLog.getProcessTypeCodeFkId());
		contextualMap.put(DOCode+".MetaProcessElement", activityLog.getMtPeCodeFkId());
		contextualMap.put(DOCode+".DOPrimaryKey", activityLog.getDoRowPrimaryKeyFkId());
		contextualMap.put(DOCode+".DOVersionID", activityLog.getDoRowVersionFkId());
		contextualMap.put(DOCode+".DisplayContextValue", activityLog.getDisplayContextValueBigTxt());
		contextualMap.put(DOCode+".DOIcon", activityLog.getDoIconCodeFkId());
		contextualMap.put(DOCode+".DOName", activityLog.getDoNameG11nBigTxt());
		contextualMap.put(DOCode+".ActivityLogActionCode", activityLog.getActionVisualMasterCode());
		contextualMap.put(DOCode+".MsgYouInContextTxt", activityLog.getMsgYouInContextG11nBigTxt());
		contextualMap.put(DOCode+".MsgYouOutofContextTxt", activityLog.getMsgYouOutOfContextG11nBigTxt());
		contextualMap.put(DOCode+".MsgThirdPersonInContextTxt", activityLog.getMsgThirdPersonInContextG11nBigTxt());
		contextualMap.put(DOCode+".MsgThirdPersonOutofContextTxt", activityLog.getMsgThirdPersonOutOfContextG11nBigTxt());
		contextualMap.put(DOCode+".ActivityDateTime", currentTimestampString);
		contextualMap.put(DOCode+".DetailLaunchTargetMTPE", activityLog.getActivityLogPkId());
		return contextualMap;
	}

	public static String getStringFromG11nMap(Map<String, Object> localeValueMap) {
		if (localeValueMap == null)
			return null;
		StringBuilder g11nString = new StringBuilder();
		String g11nLocaleString = null;
		for (Entry<String, Object> entry : localeValueMap.entrySet()){
			g11nLocaleString = getG11nString(entry.getKey(), entry.getValue());
    		g11nString.append(g11nLocaleString);
		}
		return g11nString.toString();
	}

	public static Map<String,Object> getG11nMapFromJsonNode(JsonNode duplicateStringBlob) {
		Iterator<Entry<String, JsonNode>> nodeIterator = duplicateStringBlob.fields();
		Map<String, Object> g11nMap = new HashMap<>();
		while (nodeIterator.hasNext()){
			Entry<String, JsonNode> entry = nodeIterator.next();
			g11nMap.put(entry.getKey(), entry.getValue().asText());
		}
		return g11nMap;
	}

	public static Object remove0x(String id) {
		if (id == null) return null;
		if (id.startsWith("0x")) {
			return id.substring(2);
		}
		return id;
	}

	public static String setStringValuesForExpression(String expression, Map dataVariables, char quoteCharacter, Map<String, EntityAttributeMetadata> attributeMap) {
		Pattern doPattern = Pattern.compile("\\!\\{(.*?)\\}");
		Pattern rePattern = Pattern.compile("(\\!\\{(.*?)\\})");
		Matcher doMatcher = doPattern.matcher(expression);
		Matcher reMatcher = rePattern.matcher(expression);
		while (doMatcher.find() && reMatcher.find()) {
			String doaName = doMatcher.group(1);
			String doaDbDataType = attributeMap.get(doaName).getDbDataTypeCode();
			if (doaDbDataType.matches("T_ID|T_CODE|T_BIG_TEXT|TEXT|LONG_TEXT|T_IMAGEID|T_ATTACH")) { //Fixme DATE_TIME, DATE, TIME?
				String doaValue = (String) dataVariables.get(doaName);
				if (doaValue != null) {
					expression = expression.replace(reMatcher.group(1), quoteCharacter + doaValue + quoteCharacter);
				}
			}
		}
		return expression;
	}


	public static FilterExpn getFilterExpnFromFilters(Map<String,List<FilterOperatorValue>> filters) {
		FilterExpn filterExpnObject = new FilterExpn();
    	if(filters== null)
    		return null;
		StringBuilder filterExpn = new StringBuilder();
		for(Map.Entry<String, List<FilterOperatorValue>> entry : filters.entrySet()){
			boolean isWhereClause = false;
			for(FilterOperatorValue filterOperatorValue : entry.getValue()){
				if(!filterOperatorValue.isHavingClause()){
					isWhereClause = true;
				}
			}
			if(isWhereClause){
				if(filterExpn.length()!=0){
					filterExpn.append(" AND ");
				}
				filterExpn.append("(");
				String attrExpn = entry.getKey();
				if(attrExpn.contains("..Deleted")){
					filterExpnObject.setDeleteFilterAdded(true);
				}
				int i=0;
				for(FilterOperatorValue filterOperatorValue : entry.getValue()){
					if(!filterOperatorValue.isHavingClause()) {
						if (i != 0) {
							filterExpn.append(" OR ");
						}
						filterExpn.append("(");
						filterExpn.append(attrExpn);
						filterExpn.append(getSubFilterExpn(filterOperatorValue));
						filterExpn.append(")");
						i++;
					}
                /* Now on the basis of operator make the expression */
				}
				filterExpn.append(")");
			}
		}
		filterExpnObject.setFilterExpn(filterExpn.toString());
		return filterExpnObject;
	}

	public static String getHavingExpnFromFilters(Map<String,List<FilterOperatorValue>> filters) {
		if(filters== null)
			return null;
		StringBuilder filterExpn = new StringBuilder();
		for(Map.Entry<String, List<FilterOperatorValue>> entry : filters.entrySet()){
			boolean isHavingClause = false;
			for(FilterOperatorValue filterOperatorValue : entry.getValue()){
				if(filterOperatorValue.isHavingClause()){
					isHavingClause = true;
				}
			}
			if(isHavingClause){
				if(filterExpn.length()!=0){
					filterExpn.append(" AND ");
				}
				filterExpn.append("(");
				String attrExpn = entry.getKey();
				int i=0;
				for(FilterOperatorValue filterOperatorValue : entry.getValue()){
					if(filterOperatorValue.isHavingClause()) {
						if (i != 0) {
							filterExpn.append(" OR ");
						}
						filterExpn.append("(");
						filterExpn.append(attrExpn);
						filterExpn.append(getSubFilterExpn(filterOperatorValue));
						filterExpn.append(")");
						i++;
					}
                /* Now on the basis of operator make the expression */
				}
				filterExpn.append(")");
			}
		}
		return filterExpn.toString();
	}

	private static String getSubFilterExpn(FilterOperatorValue operatorValue) {
		String operator = operatorValue.getOperator();
		StringBuilder subFilterExpn = new StringBuilder();
		switch (operator){
			case "IN":
				subFilterExpn.append("IN")
						.append("(")
						.append(operatorValue.getValue())
						.append(")");
				break;
			default:
				subFilterExpn.append(" ")
						.append(operator)
						.append(" ");
				if(operatorValue.getValue()!=null)
					subFilterExpn.append(operatorValue.getValue());
		}
		return subFilterExpn.toString();
	}
	public static boolean isParsableAsInteger( String s) {
		try {
			Integer.valueOf(s);
			return true;
		} catch (NumberFormatException numberFormatException) {
			return false;
		}
	}

	public static boolean isParsableAsDouble(String s) {
		try {
			Double.valueOf(s);
			return true;
		} catch (NumberFormatException numberFormatException) {
			return false;
		}
	}

	public static String getStringFromG11nMaps(Map<String, Object> map) {
		if (map == null)
			return null;
		StringBuilder g11nString = new StringBuilder();
		String g11nLocaleString = null;
		for (Entry<String, Object> entry : map.entrySet()){
			G11n g11n = (G11n) entry.getValue();
			g11nLocaleString = getG11nStringNew(entry.getKey(),(G11n) entry.getValue());
			g11nString.append(g11nLocaleString);
		}
		return g11nString.toString();
	}

	private static String getG11nStringNew(String key, G11n value) {
		if(value!=null){
//			String stringValue = value.toString();
			String vl = value.getVL();
			String vlString = getG11nPropertyValueString("VL", vl);
			String im = value.getIM();
			String imString = getG11nPropertyValueString("IM", im);
			String tsl = value.getTSL();
			String tslString = getG11nPropertyValueString("TSL", tsl);
			return "{\""+key+"\""+":"+"{ "+vlString+","+imString+","+tslString+"}}";
		}
		return null;
	}

	private static String getG11nPropertyValueString(String g11nProperty, String g11nPropertyValue) {
		if(g11nPropertyValue!=null){
			if(g11nPropertyValue.contains("\"")){
				// |% is the escape character
				g11nPropertyValue = g11nPropertyValue.replace("\"","|%");
			}
			return "\""+g11nProperty+"\":\""+g11nPropertyValue+"\"";
		}
		return "\""+g11nProperty+"\":\""+"null"+"\"";
	}


	public static Map<String,Object> getG11nMapFromStringNew(String g11nStringValue) {//TODO take care of { & }
		if (g11nStringValue == null || g11nStringValue.isEmpty())
			return null;
		Map<String, Object> g11nMap = new HashMap<>();
		int startLocaleIndex = 0;//value.indexOf("\""+localeCode+"\"");
		while(startLocaleIndex+2 < g11nStringValue.length()){
			int midLocaleIndex = g11nStringValue.indexOf("\":{\"",startLocaleIndex);
			int vlMidIndex = g11nStringValue.indexOf("\":\"",midLocaleIndex);
			int vlEndIndex = g11nStringValue.indexOf("\",\"",vlMidIndex);
			int imMidIndex = g11nStringValue.indexOf("\":\"",vlEndIndex);
			int imEndIndex = g11nStringValue.indexOf("\",\"",imMidIndex);
			int tslMidIndex = g11nStringValue.indexOf("\":\"",imEndIndex);
			int tslEndIndex = g11nStringValue.indexOf("\"}",tslMidIndex);
			G11n g11n = new G11n();
			String localeCode = g11nStringValue.substring(startLocaleIndex+1,midLocaleIndex);
			String vlSubString = g11nStringValue.substring(midLocaleIndex+4, vlMidIndex);
			String vlValueSubString = g11nStringValue.substring(vlMidIndex+3, vlEndIndex).replaceAll("\\|%","\"");
			g11n.setVL(vlValueSubString);
			String imSubString = g11nStringValue.substring(vlEndIndex+3, imMidIndex);
			String imValueSubString = g11nStringValue.substring(imMidIndex+3, imEndIndex);
			g11n.setIM(imValueSubString);
			String tslSubString = g11nStringValue.substring(imEndIndex+3, tslMidIndex);
			String tslValueSubString = g11nStringValue.substring(tslMidIndex+3, tslEndIndex);
			g11n.setTSL(tslValueSubString);
			startLocaleIndex = tslEndIndex+2;
			g11nMap.put(localeCode, g11n);
		}
		return g11nMap;
	}

	public static String getStringFromG11nMapIntrmdte(Map<String, Object> localeValueMap) {
		if (localeValueMap == null)
			return null;
		StringBuilder g11nString = new StringBuilder();
		String g11nLocaleString = null;
		for (Entry<String, Object> entry : localeValueMap.entrySet()){
			G11n g11n = new G11n();
			g11n.setIM("I");
			g11n.setVL(" "+entry.getValue().toString());
			g11nLocaleString = getG11nStringNew(entry.getKey(), g11n);
			g11nString.append(g11nLocaleString);
		}
		return g11nString.toString();
	}

	public static Map<String, Object> getG11nMapFromStringIntrmdte(String g11nString){
		Map<String,Object> objectMap = getG11nMapFromStringNew(g11nString);
		Map<String, Object> intrmdteMap = new HashMap<>();
		for (Entry<String, Object> entry : objectMap.entrySet()){
			intrmdteMap.put(entry.getKey(), ((G11n)entry.getValue()).getVL());
		}
		return intrmdteMap;
	}

	public static boolean isValidTenant() throws TapplentException {
		String tenantId = TenantContextHolder.getCurrentTenantID();
		if(tenantId.equals("ERROR")){
			return false;
		}
		return true;
	}

	public static boolean validateAccessTokenAndSetUserContext(String accessToken) throws IOException{
			String personId = TenantContextHolder.getUserContext().getPersonId();
			if (accessToken.equals("abc")) {
				setUserContext(personId);
				return true;
			}
			else {
				String tenantId = TenantContextHolder.getCurrentTenantID();
				String serverName = TenantContextHolder.getCurrentTenantInfo().getServerName();
				String url;
				if (serverName.equals("localhost"))
					url = "https://www.mytapplentdev.com/tapplent-security-app/user/v1/validateToken?tenantId="+tenantId;
				else url = "https://"+serverName+"/tapplent-security-app/user/v1/validateToken?tenantId="+tenantId;
				URL obj = new URL(url);
				HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

				con.setRequestMethod("GET");
				con.setRequestProperty("access_token", accessToken);

				con.setDoOutput(true);
				if (con.getResponseCode() == 200) {
					String content = getStringFromInputStream(con.getInputStream());
					ObjectMapper objectMapper = new ObjectMapper();
					JsonNode jsonNode = objectMapper.readTree(content);
					personId = jsonNode.get("personId").asText();
					setUserContext(personId);
					return true;
				}
				else return false;
			}
	}

	private static void setUserContext(String personId) {
			String personName = TenantAwareCache.getPersonRepository().getPersonDetails(personId).getPersonName();
			TenantContextHolder.getUserContext().setPersonName(personName);
			TenantContextHolder.getUserContext().setPersonId(personId);
	}

	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

	public static boolean isHardcodedControlType(String controlType) {
		switch (ControlType.valueOfControlType(controlType)) {
			case COLLEG_COUNT:
			case COLLEG_TEXT:
			case COLLEG_IMAGE:
			case TAG_MASTER:
				return true;
		}
		return false;
	}

	public static String getDatetimeUntilSeconds(Timestamp lastModifiedDatetime) throws ParseException {
		java.util.Date date = null;
		String dateTimeFormat="yyyy-MM-dd HH:mm:ss.SSS";
		SimpleDateFormat sdf = new SimpleDateFormat(dateTimeFormat);
		date = sdf.parse(lastModifiedDatetime.toString());
		DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String valueToReturn = destDf.format(date);
		return valueToReturn;
	}

    public static boolean isOnlyFilterControl(String screenSectionMasterCode, Map<String, LoadParameters> peAliasToLoadParamsMap, String mtPEAlias) {
		if(screenSectionMasterCode.equals("FILTER_BOOLEAN_ATTR_SECN") || screenSectionMasterCode.equals("FILTER_OTHER_ATTR_SECN")){
			if(peAliasToLoadParamsMap.containsKey(mtPEAlias)){
				LoadParameters loadParameters = peAliasToLoadParamsMap.get(mtPEAlias);
				if(loadParameters.getFilters() != null && !loadParameters.getFilters().isEmpty()){
					return false;
				}
			}
			return true;
		}else {
			return false;
		}
    }

	public static boolean isBaseStandardField(String columnLabel) {
		if(columnLabel.endsWith(".VersionID") || columnLabel.endsWith(".PrimaryKeyID") || columnLabel.endsWith(".EffectiveDateTime")
				|| columnLabel.endsWith(".SaveState") || columnLabel.endsWith(".ActiveState")
				|| columnLabel.endsWith(".RecordState") || columnLabel.endsWith(".Deleted")
				|| columnLabel.endsWith(".LastModifiedDateTime") || columnLabel.endsWith(".Primary") || columnLabel.endsWith(".Featured") || columnLabel.endsWith(".RefWorkflowDefinition")){
			return true;
		}
		return false;

	}

	public static boolean isRepetitiveSection(String sectionMasterCode){
		switch (sectionMasterCode){
			case "REPETITIVE_SECN": return true;
			default: return false;
		}
	}

	public static String getG11nStringToInsert(String attributeValue,boolean isSubmit) {
		if (attributeValue == null) return null;
		Map<String, OEMObject> oemObjectMap = SystemAwareCache.getSystemRepository().getOemObjectMap();
		Map<String ,Map<String,String>> oemToLocaleLangMap =SystemAwareCache.getSystemRepository().getOemToLocaleLangMap();
		Person person = TenantAwareCache.getPersonRepository().getPersonDetails(TenantContextHolder.getUserContext().getPersonId());
		String personBaseLocale = person.getPersonPreferences().getLocaleCodeFkId();
		String orgDefaultLocale = person.getOrgDefaultLocale();
		String instanceDefaultLocale = TenantAwareCache.getMetaDataRepository().getTenantDefaultLocale();
		//Fetch the Yandex related localeCodeTolangMap
		Map<String,String> localeToLangMap = oemToLocaleLangMap.get("Yandex");
		//Fetch the production key relates to Yandex
		OEMObject keyDetails = oemObjectMap.get("YANDEX_TRANSLATE");
		String productionKey = keyDetails.getOemKey();
		JSONObject inputJson = new JSONObject(attributeValue);
		JSONObject finalJson = new JSONObject();
		List<String> localeCodes = new ArrayList<>();
		//Fetch the localeCodeTolangCodeMap from the database
		//Fetch the Production key from the database
		String defaultSourceLocale = null;
		Iterator<String> it = inputJson.keys();
		while (it.hasNext()){
			String key =it.next();
			defaultSourceLocale = key;
			JSONObject jsonObject = inputJson.getJSONObject(key);
			JSONObject newJsonObject = new JSONObject();
			if(!jsonObject.getString("VL").isEmpty()) {
				String value = jsonObject.getString("VL").trim();
				String transValue = value.replace("\"", "|%");
				StringBuilder resultStr = new StringBuilder();
				resultStr.append(" ");
				resultStr.append(transValue);
				newJsonObject.put("VL",resultStr.toString());
			}else{
				newJsonObject.put("VL",jsonObject.getString("VL"));
			}
			newJsonObject.put("IM",jsonObject.getString("IM"));
			newJsonObject.put("TSL",jsonObject.getString("TSL"));
			finalJson.put(key,newJsonObject);
			localeCodes.add(key);
		}
		if(isSubmit) {
			if (localeCodes.contains(personBaseLocale)) {
				JSONObject jsonG11NObject = inputJson.getJSONObject(personBaseLocale);
				if (jsonG11NObject.getString("VL").isEmpty()) {
					if (!localeCodes.contains(orgDefaultLocale))
						finalJson.put(orgDefaultLocale, getEmptyJSONObject(personBaseLocale, jsonG11NObject));
					if (!localeCodes.contains(instanceDefaultLocale))
						finalJson.put(instanceDefaultLocale, getEmptyJSONObject(personBaseLocale, jsonG11NObject));
				} else {
					if (!localeCodes.contains(orgDefaultLocale))
						finalJson.put(orgDefaultLocale, getSingleG11NJSONObject(localeToLangMap, personBaseLocale, orgDefaultLocale, inputJson, productionKey));
					if (!localeCodes.contains(instanceDefaultLocale))
						finalJson.put(instanceDefaultLocale, getSingleG11NJSONObject(localeToLangMap, personBaseLocale, instanceDefaultLocale, inputJson, productionKey));
				}
			} else if (inputJson.has(orgDefaultLocale)) {
				JSONObject jsonG11NObject = inputJson.getJSONObject(orgDefaultLocale);
				if (jsonG11NObject.getString("VL").isEmpty()) {
					if (!localeCodes.contains(instanceDefaultLocale))
						finalJson.put(instanceDefaultLocale, getEmptyJSONObject(orgDefaultLocale, jsonG11NObject));
				} else {
					if (!localeCodes.contains(instanceDefaultLocale))
						finalJson.put(instanceDefaultLocale, getSingleG11NJSONObject(localeToLangMap, personBaseLocale, instanceDefaultLocale, inputJson, productionKey));
				}

			} else if (inputJson.has(instanceDefaultLocale)) {
				JSONObject jsonG11NObject = inputJson.getJSONObject(instanceDefaultLocale);
				if (jsonG11NObject.getString("VL").isEmpty()) {
					if (!localeCodes.contains(orgDefaultLocale))
						finalJson.put(orgDefaultLocale, getEmptyJSONObject(instanceDefaultLocale, jsonG11NObject));
				} else {
					if (!localeCodes.contains(orgDefaultLocale))
						finalJson.put(orgDefaultLocale, getSingleG11NJSONObject(localeToLangMap, instanceDefaultLocale, orgDefaultLocale, inputJson, productionKey));
				}
			} else {
				JSONObject jsonG11NObject = inputJson.getJSONObject(defaultSourceLocale);
				if (jsonG11NObject.getString("VL").isEmpty()) {
					if (!localeCodes.contains(orgDefaultLocale))
						finalJson.put(orgDefaultLocale, getEmptyJSONObject(defaultSourceLocale, jsonG11NObject));
					if (!localeCodes.contains(instanceDefaultLocale))
						finalJson.put(instanceDefaultLocale, getEmptyJSONObject(defaultSourceLocale, jsonG11NObject));
				} else {
					if (!localeCodes.contains(orgDefaultLocale))
						finalJson.put(orgDefaultLocale, getSingleG11NJSONObject(localeToLangMap, defaultSourceLocale, orgDefaultLocale, inputJson, productionKey));
					if (!localeCodes.contains(instanceDefaultLocale))
						finalJson.put(instanceDefaultLocale, getSingleG11NJSONObject(localeToLangMap, defaultSourceLocale, instanceDefaultLocale, inputJson, productionKey));
				}
			}
			return finalJson.toString();
		}
		return finalJson.toString();
	}
	public static String getFormattedG11nString(String attributeValue) {
		if (attributeValue == null) return null;
		JSONObject inputJson = new JSONObject(attributeValue);
		JSONObject finalJson = new JSONObject();
		Iterator<String> it = inputJson.keys();
		while (it.hasNext()){
			String key =it.next();
			JSONObject jsonObject = inputJson.getJSONObject(key);
			JSONObject newJsonObject = new JSONObject();
			if(!jsonObject.getString("VL").isEmpty()) {
				String value = jsonObject.getString("VL").trim();
				String transValue = value.replace("\"", "|%");
				StringBuilder resultStr = new StringBuilder();
				resultStr.append(" ");
				resultStr.append(transValue);
				newJsonObject.put("VL",resultStr.toString());
			}else{
				newJsonObject.put("VL",jsonObject.getString("VL"));
			}
			newJsonObject.put("IM",jsonObject.getString("IM"));
			newJsonObject.put("TSL",jsonObject.getString("TSL"));
			finalJson.put(key,newJsonObject);
		}

		return finalJson.toString();
	}

	public static String getSingleG11NValue(String sourceLang,String destLang,String sourceVal,String production_Key){

		String transValue =null;
		try {

			StringBuilder url = new StringBuilder("https://translate.yandex.net/api/v1.5/tr.json/translate?lang=");
			url.append(sourceLang);
			url.append("-");
			url.append(destLang);
			url.append("&key=").append(production_Key);
			url.append("&text=").append(URLEncoder.encode(sourceVal,"UTF-8"));

			HttpPost post = new HttpPost(url.toString());
			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response;
			response = client.execute(post);
			JSONObject respJson = new JSONObject(EntityUtils.toString(response.getEntity()));
			transValue = respJson.getJSONArray("text").getString(0);
		}catch (Exception e){
			e.printStackTrace();
		}
		return transValue;
	}

	public static JSONObject getSingleG11NJSONObject(Map<String,String> localeToLangMap, String sourceLoc,String destLoc, JSONObject inputJSON,String production_Key){
		JSONObject jsonObject = inputJSON.getJSONObject(sourceLoc);
		String value = getSingleG11NValue(localeToLangMap.get(sourceLoc),localeToLangMap.get(destLoc),jsonObject.getString("VL"),production_Key);
		String transValue = value.replace("\"","|%");
		StringBuilder resultStr= new StringBuilder();
		resultStr.append(" ");
		resultStr.append(transValue);
		JSONObject newJsonObject = new JSONObject();
		newJsonObject.put("VL",resultStr.toString());
		newJsonObject.put("IM","T");
		newJsonObject.put("TSL",sourceLoc);
		return newJsonObject;
	}
	public static JSONObject getEmptyJSONObject(String sourceLoc,JSONObject jsonG11NObject){
		JSONObject newG11NJsonObject = new JSONObject();
		newG11NJsonObject.put("VL",jsonG11NObject.getString("VL"));
		newG11NJsonObject.put("IM","T");
		newG11NJsonObject.put("TSL",sourceLoc);
		return newG11NJsonObject;
	}

	public static String replaceDoaWithValue(String formula, Map<String, Object> data) {
		Pattern doaPattern = Pattern.compile("\\!\\{(.*?)\\}");
		Pattern rePattern = Pattern.compile("(\\!\\{(.*?)\\})");
		Matcher doaMatcher = doaPattern.matcher(formula);
		Matcher reMatcher = rePattern.matcher(formula);
		while (doaMatcher.find() && reMatcher.find()) {
			String doaName = doaMatcher.group(1);
			String resolvedValue = (String) data.get(doaName);
			if (resolvedValue != null) {
				formula = formula.replace(reMatcher.group(1), "'" + resolvedValue + "'");
			}else {
				formula = formula.replace(reMatcher.group(1), "''");
			}
		}
		return formula;
	}

	public static String getStandardColumns(){
		return " IS_DELETED, CREATEDBY_PERSON_FK_ID, CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT, CREATED_DATETIME, LAST_MODIFIEDBY_PERSON_FK_ID, LAST_MODIFIEDBY_PERSON_FULLNAME_G11N_BIG_TXT, LAST_MODIFIED_DATETIME ";
	}

	public static String getStandardColumnValues(String currentTimestampString){
		String personId = TenantContextHolder.getUserContext().getPersonId();
		String personName = TenantContextHolder.getUserContext().getPersonName();
		return " FALSE, "+personId+", '"+personName+"', '"+currentTimestampString+"', "+personId+", '"+personName+"', '"+currentTimestampString+"' ";
	}

	public static Timestamp getTimestampFromString(String timestampString) throws ParseException {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
			Date parsedDate = dateFormat.parse(timestampString);
			Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
			return timestamp;
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static String formatDatetimeString(Object value) throws ParseException {
		java.util.Date date = null;
		String dateTimeFormat="yyyy-MM-dd HH:mm:ss.SSS";
		SimpleDateFormat sdf = new SimpleDateFormat(dateTimeFormat);
		date = sdf.parse(value.toString());
		DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String valueToReturn = destDf.format(date);
		return valueToReturn;
	}

	public static String getDummyG11nStringForInstanceLocal(Object value) {
		String globalLocale = TenantAwareCache.getMetaDataRepository().getTenantDefaultLocale();
		String valueToInsert = value.toString().replace("\"","|%");
		String dummyString = "{\""+globalLocale+"\":{ \"VL\":\" "+valueToInsert+"\",\"IM\":\"I\",\"TSL\":\"null\"}}";
		return dummyString;
	}

	public static boolean compare(String str1, String str2) {
		return (str1 == null ? str2 == null : str1.equals(str2));
	}

	public static Map<String,Object> getNonStandardNotNullFields(Map<String, Object> record, EntityMetadataVOX entityMetadataVOX) {
		Map<String, Object> nonStandardRecord = new HashMap<>();
		for(Entry<String, Object> entry : record.entrySet()){
			if(entityMetadataVOX.getAttribute(entry.getKey()) != null &&
					!entityMetadataVOX.getAttribute(entry.getKey()).isNonVersionTrackableFlag() &&
					!entityMetadataVOX.getStandardDoaFields().containsKey(entry.getKey()) &&
					entry.getValue() != null)
				nonStandardRecord.put(entry.getKey(), entry.getValue());
		}
		return nonStandardRecord;
	}
	public static boolean compareG11nStrings(String g11n1,String g11n2){
		if ((g11n1 == null ^ g11n2 == null))
			return false;
		Map<String,String> localeToValueMapForG11n1 = populateLocaleToValueMap(g11n1);
		Map<String,String> localeToValueMapForG11n2 = populateLocaleToValueMap(g11n2);
		if(localeToValueMapForG11n1.keySet().size() !=localeToValueMapForG11n2.keySet().size())
			return false;
		for(Entry<String, String> entry:localeToValueMapForG11n1.entrySet()){
			if(!localeToValueMapForG11n2.containsKey(entry.getKey()))
				return false;
			if(!entry.getValue().equals(localeToValueMapForG11n2.get(entry.getKey()))){
				return false;
			}
		}
		return true;
	}
	public static  Map<String,String> populateLocaleToValueMap(String g11n){
		JSONObject g11nJsonObject = new JSONObject(g11n);
		Map<String,String> localeToValueMap =new HashMap<>();
		Iterator<String> iterator = g11nJsonObject.keys();
		//Populate the localeToValueMap for both strings
		while (iterator.hasNext()){
			String  localeCode =iterator.next();
			JSONObject jsonObject = g11nJsonObject.getJSONObject(localeCode);
			String localeValue = jsonObject.getString("VL").trim();
			if(!localeToValueMap.containsKey(localeCode)){
				localeToValueMap.put(localeCode,localeValue);
			}
		}
		return localeToValueMap;
	}

	public static String getStringTobeInsertedForMasterMeta(MasterEntityAttributeMetadata attributeMeta, Object object) {
		Object valueToReturn = object;
		if(object == null || object.equals(""))
			return "NULL" ;
		if(attributeMeta != null){
			EntityDataTypeEnum eDt = EntityDataTypeEnum.valueOf(attributeMeta.getDbDataTypeCode());
			switch (eDt) {
				case BOOLEAN:
					valueToReturn =  String.valueOf(object);
					break;
				case POSITIVE_INTEGER:
				case NEGATIVE_INTEGER:
				case BIG_INTEGER:
				case POSITIVE_DECIMAL:
				case NEGATIVE_DECIMAL:
				case CURRENCY:
				case LOCATION_LATITUDE:
				case LOCATION_LONGITUDE:
					valueToReturn = (String)object;
					break;
				case DATE_TIME:
					Date date = null;
					try {
						String dateTimeFormat="yyyy-MM-dd HH:mm:ss.SSS";
						SimpleDateFormat sdf = new SimpleDateFormat(dateTimeFormat);
						date = sdf.parse(object.toString());
						DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
						valueToReturn = destDf.format(date);
					} catch (ParseException ex) {
						ex.printStackTrace();
					}
					valueToReturn = "'"+valueToReturn.toString()+"'";
					break;
				case T_CODE:
				case T_ICON:
				case TEXT:
				case T_BIG_TEXT:
				case LONG_TEXT:
					object = ((String)object).replaceAll("\\r\\n|\\r|\\n", " ");
					if (attributeMeta.isGlocalized())
						object = getG11nStringToInsert((String) object, false);
					valueToReturn = "'" + ((String)object).replace("'", "''") + "'";
					break;
				case ATTACHMENT:
				case AUDIO:
				case VIDEO:
				case IMAGE:
				case T_ID:
				case T_IMAGEID:
				case T_ATTACH:
					valueToReturn = "0x" + remove0x((String)object);
					break;
				case T_BLOB:
					if(object.equals("")) return "";

					StringBuilder blobString = new StringBuilder();
					blobString.append("COLUMN_CREATE(");
					if(object instanceof ObjectNode){
						Iterator<Entry<String, JsonNode>> nodeIterator = ((ObjectNode) object).fields();
						while (nodeIterator.hasNext()){
							Entry<String, JsonNode> entry = nodeIterator.next();
							blobString.append("'" + entry.getKey() + "'");
							blobString.append(", '" + (entry.getValue().asText()).replace("'", "''") + "', ");
						}
					}
					else{
						Map<String, String> blobMap = (Map<String, String>) object;
						for(Entry<String, String> entry : blobMap.entrySet()){
							blobString.append("'" + entry.getKey() + "'");
							blobString.append(", '" + (entry.getValue()).replace("'", "''") + "', ");
						}
					}
					blobString.replace(blobString.length()-2, blobString.length(), ")");
					valueToReturn = blobString.toString();
					break;
				default:
					valueToReturn = "'" + ((String)object).replace("'", "''") + "'";
					break;
			}
		} else {
			LOG.debug("Attribute is NULL for column name " + " and value " + object);
			valueToReturn = "NULL";
		}
		return (String) valueToReturn;
	}
}