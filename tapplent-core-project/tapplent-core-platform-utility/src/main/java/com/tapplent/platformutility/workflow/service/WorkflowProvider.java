package com.tapplent.platformutility.workflow.service;

import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.tenantresolver.server.property.ServerActionStep;

import java.sql.SQLException;
import java.text.ParseException;

public interface WorkflowProvider {

	public void CreateLaunchWorkflowIfApplicable(GlobalVariables globalVariables) throws SQLException, ParseException;

//	public List<WorkflowActor> resloveActors(GlobalVariables globalVariables, WorkflowStepDefn workflowDefnStep) throws SQLException;

//	public List<WorkflowActor> resolveActors(GlobalVariables globalVariables, WorkflowStepDefn workflowStepDefn, WorkflowStepTxn workflowStepTxn);

	public void generateWorkflowNotifaction(ServerActionStep currentActionStep, GlobalVariables globalVariables);
}
