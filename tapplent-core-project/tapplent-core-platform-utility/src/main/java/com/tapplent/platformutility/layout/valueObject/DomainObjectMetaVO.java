package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class DomainObjectMetaVO {
	private String domainObjectMetaEuPkId;
	private String grouperBaseObjectFkId; 
	private String btDoSpecFkId; 
	private String baseTemplateDepFkId; 
	private String baseObjectDepFkId; 
	private String domainObjectFkId;
	private String mtPE;
	private String btProcessElementFkId; 
	private JsonNode btDoSpecNameG11nBlob; 
	private String btDoSpecNameG11nText; 
	private String dbTableName; 
	private JsonNode doSummaryTitleTextG11nBlob; 
	private String doSummaryTitleIconFkId; 
	private JsonNode doDetailsTitleTextG11nBlob; 
	private String doHelpTextG11nBlob;
	private boolean isReadToBeLogged;
	private JsonNode noDataMessageG11nBlob; 
	private String noDataMessageIconId;
	private JsonNode countOfRecordsTitleG11nBlob;
	public String getDomainObjectMetaEuPkId() {
		return domainObjectMetaEuPkId;
	}
	public void setDomainObjectMetaEuPkId(String domainObjectMetaEuPkId) {
		this.domainObjectMetaEuPkId = domainObjectMetaEuPkId;
	}
	public String getGrouperBaseObjectFkId() {
		return grouperBaseObjectFkId;
	}
	public void setGrouperBaseObjectFkId(String grouperBaseObjectFkId) {
		this.grouperBaseObjectFkId = grouperBaseObjectFkId;
	}
	public String getBtDoSpecFkId() {
		return btDoSpecFkId;
	}
	public void setBtDoSpecFkId(String btDoSpecFkId) {
		this.btDoSpecFkId = btDoSpecFkId;
	}
	public String getBaseTemplateDepFkId() {
		return baseTemplateDepFkId;
	}
	public void setBaseTemplateDepFkId(String baseTemplateDepFkId) {
		this.baseTemplateDepFkId = baseTemplateDepFkId;
	}
	public String getBaseObjectDepFkId() {
		return baseObjectDepFkId;
	}
	public void setBaseObjectDepFkId(String baseObjectDepFkId) {
		this.baseObjectDepFkId = baseObjectDepFkId;
	}
	public String getDomainObjectFkId() {
		return domainObjectFkId;
	}
	public void setDomainObjectFkId(String domainObjectFkId) {
		this.domainObjectFkId = domainObjectFkId;
	}
	public String getBtProcessElementFkId() {
		return btProcessElementFkId;
	}
	public void setBtProcessElementFkId(String btProcessElementFkId) {
		this.btProcessElementFkId = btProcessElementFkId;
	}
	public JsonNode getBtDoSpecNameG11nBlob() {
		return btDoSpecNameG11nBlob;
	}
	public void setBtDoSpecNameG11nBlob(JsonNode btDoSpecNameG11nBlob) {
		this.btDoSpecNameG11nBlob = btDoSpecNameG11nBlob;
	}
	public String getBtDoSpecNameG11nText() {
		return btDoSpecNameG11nText;
	}
	public void setBtDoSpecNameG11nText(String btDoSpecNameG11nText) {
		this.btDoSpecNameG11nText = btDoSpecNameG11nText;
	}
	public String getDbTableName() {
		return dbTableName;
	}
	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}
	public JsonNode getDoSummaryTitleTextG11nBlob() {
		return doSummaryTitleTextG11nBlob;
	}
	public void setDoSummaryTitleTextG11nBlob(JsonNode doSummaryTitleTextG11nBlob) {
		this.doSummaryTitleTextG11nBlob = doSummaryTitleTextG11nBlob;
	}
	public String getDoSummaryTitleIconFkId() {
		return doSummaryTitleIconFkId;
	}
	public void setDoSummaryTitleIconFkId(String doSummaryTitleIconFkId) {
		this.doSummaryTitleIconFkId = doSummaryTitleIconFkId;
	}
	public JsonNode getDoDetailsTitleTextG11nBlob() {
		return doDetailsTitleTextG11nBlob;
	}
	public void setDoDetailsTitleTextG11nBlob(JsonNode doDetailsTitleTextG11nBlob) {
		this.doDetailsTitleTextG11nBlob = doDetailsTitleTextG11nBlob;
	}
	public String getDoHelpTextG11nBlob() {
		return doHelpTextG11nBlob;
	}
	public void setDoHelpTextG11nBlob(String doHelpTextG11nBlob) {
		this.doHelpTextG11nBlob = doHelpTextG11nBlob;
	}
	public JsonNode getNoDataMessageG11nBlob() {
		return noDataMessageG11nBlob;
	}
	public void setNoDataMessageG11nBlob(JsonNode noDataMessageG11nBlob) {
		this.noDataMessageG11nBlob = noDataMessageG11nBlob;
	}
	public String getNoDataMessageIconId() {
		return noDataMessageIconId;
	}
	public void setNoDataMessageIconId(String noDataMessageIconId) {
		this.noDataMessageIconId = noDataMessageIconId;
	}
	public boolean isReadToBeLogged() {
		return isReadToBeLogged;
	}
	public void setReadToBeLogged(boolean isReadToBeLogged) {
		this.isReadToBeLogged = isReadToBeLogged;
	}
	public JsonNode getCountOfRecordsTitleG11nBlob() {
		return countOfRecordsTitleG11nBlob;
	}
	public void setCountOfRecordsTitleG11nBlob(JsonNode countOfRecordsTitleG11nBlob) {
		this.countOfRecordsTitleG11nBlob = countOfRecordsTitleG11nBlob;
	}
	public String getMtPE() {
		return mtPE;
	}
	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}
}
