package com.tapplent.platformutility.layout.dao;

import java.io.IOException;





import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;


import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.activityLog.ActivityLog;
import com.tapplent.platformutility.activityLog.ActivityLogDtl;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.*;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.MasterEntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.platformutility.person.structure.PersonGroup;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

//import com.tapplent.platformutility.search.builder.SearchFields;

public class LayoutDAOImpl extends TapplentBaseDAO implements LayoutDAO{


	private static final Logger LOG = LogFactory.getLogger(LayoutDAOImpl.class);

	/**
     * This method retrieves data from table T_BT_ADM_BASE_TEMPLATE
	 * @param metaProcessCode
	 * @param isIncludeArchived if archived base templates are required or not
     * @return list of base template value object
     */
	public List<BaseTemplateVO> getBaseTemplateByMetaProcess(String metaProcessCode, boolean isIncludeArchived) {
		List<BaseTemplateVO> baseTemplateList = null;
		String SQL = "SELECT\n" +
				"  *,\n" +
				"  BASE_TEMPLATE_NAME_G11N_BIG_TXT \n" +
				"FROM T_BT_ADM_BASE_TEMPLATE\n" +
				"WHERE PROCESS_DEP_CODE_FK_ID = ? AND IS_ARCHIVED IN (?) AND RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		parameters.add(metaProcessCode);
		if(isIncludeArchived){
			parameters.add("true,false");
		}
		else{
			parameters.add("false");
		}
		ResultSet rs = executeSQL(SQL, parameters);
		baseTemplateList = getBaseTemplateListFromResultSet(rs);
		return baseTemplateList;
	}

	/**
	 *	This method retrieves data from table T_BT_ADM_BASE_TEMPLATE
	 * @param baseTemplateId for which ID the base template details are required
	 * @return list of base template value object
     */
	public BaseTemplateVO getBaseTemplateByBaseTemplate(String baseTemplateId){
		BaseTemplateVO bt = null;
		String SQL = "SELECT\n" +
				"  *,\n" +
				"  COLUMN_JSON(BASE_TEMPLATE_NAME_G11N_BIG_TXT) AS baseTemplateName\n" +
				"FROM T_BT_ADM_BASE_TEMPLATE\n" +
				"WHERE BASE_TEMPLATE_CODE_PK_ID = ? AND RECORD_STATE_CODE_FK_ID = 'CURRENT';\n";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		List<BaseTemplateVO> baseTemplateList = getBaseTemplateListFromResultSet(rs);
		if(!baseTemplateList.isEmpty()) {
			bt = baseTemplateList.get(0);
		}
		return bt;
	}

	/**
	 *
	 * @param rs ResultSet object having data from T_BT_ADM_BASE_TEMPLATE table
	 * @return
	 */
	private List<BaseTemplateVO> getBaseTemplateListFromResultSet(ResultSet rs) {
		List<BaseTemplateVO> baseTemplateList = new ArrayList<BaseTemplateVO>();
		try{
			while (rs.next()){
				BaseTemplateVO bt = new BaseTemplateVO();
				bt.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
				bt.setBaseTemplateId(rs.getString("BASE_TEMPLATE_CODE_PK_ID"));
				bt.setBaseTemplateName(getG11nValue(rs.getString("BASE_TEMPLATE_NAME_G11N_BIG_TXT")));
				bt.setMetaProcessCode(rs.getString("PROCESS_DEP_CODE_FK_ID"));
				bt.setBaseTemplateIcon(rs.getString("BASE_TEMPLATE_ICON_CODE_FK_ID"));
				bt.setArchived(rs.getBoolean("IS_ARCHIVED"));
				bt.setShowNonPermissionedActions(rs.getBoolean("IS_NON_PERMISSIONED_ACTIONS_SHOWN"));
				bt.setBtStandard(rs.getBoolean("IS_BT_STANDARD"));
				baseTemplateList.add(bt);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return baseTemplateList;
	}

	/**
	 * Method to retrieve data from table T_UI_ADM_PAGE_EQUIVALENCE
	 * @param baseTemplateId
	 * @return
     */
	@Override
	public List<PageEquivalenceVO> getPageEquivalenceByBT(String baseTemplateId) {
		List<PageEquivalenceVO> pageEquivalenceVOList = new ArrayList<>();
		String SQL = "SELECT\n" +
				"  *,\n" +
				"  COLUMN_JSON(DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob,\n" +
				"  COLUMN_JSON(DEVICE_DEPENDENT_BLOB)   AS deviceDependentBlob\n" +
				"FROM T_UI_ADM_PAGE_EQUIVALENCE\n" +
				"WHERE BASE_TEMPLATE_CODE_FK_ID = ? AND RECORD_STATE_CODE_FK_ID = 'CURRENT' ;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				PageEquivalenceVO pageEquivalenceVO = new PageEquivalenceVO();
				pageEquivalenceVO.setBaseTemplate(rs.getString("BASE_TEMPLATE_CODE_FK_ID"));
				pageEquivalenceVO.setMtPE(rs.getString("MT_PROCESS_ELEMENT_CODE_FK_ID"));
				pageEquivalenceVO.setViewType(rs.getString("VIEW_TYPE_CODE_FK_ID"));
				pageEquivalenceVO.setPageEquivalenceTransactionId(convertByteToString(rs.getBytes("PAGE_EQUIVALENCE_TXN_PK_ID")));
				pageEquivalenceVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
				pageEquivalenceVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
				pageEquivalenceVOList.add(pageEquivalenceVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return pageEquivalenceVOList;
	}
	@Override
	public List<PageEquivalencePlacementVO> getPageEquivalencePlacementByBT(String baseTemplateId) {
		List<PageEquivalencePlacementVO> equivalencePlacementVOs = new ArrayList<>();
		String SQL = "SELECT\n" +
				"  *,\n" +
				"  CONTAINER_PAGE_FK_ID,\n" +
				"  PROVIDER_PAGE_FK_ID,\n" +
				"  COLUMN_JSON(DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob,\n" +
				"  COLUMN_JSON(DEVICE_DEPENDENT_BLOB)   AS deviceDependentBlob\n" +
				"FROM T_UI_ADM_PAGE_EQUIVALENCE_PLACEMENT_TXN\n" +
				"WHERE BASE_TEMPLATE_CODE_FK_ID = ? AND RECORD_STATE_CODE_FK_ID = 'CURRENT' ;\n";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				PageEquivalencePlacementVO equivalencePlacementVO = new PageEquivalencePlacementVO();
				equivalencePlacementVO.setPageEquivalencePlacementId(convertByteToString(rs.getBytes("PAGE_EQUIVALENCE_PLACEMENT_TXN_PK_ID")));
				equivalencePlacementVO.setBaseTemplate(rs.getString("BASE_TEMPLATE_CODE_FK_ID"));
				equivalencePlacementVO.setContainerPage(convertByteToString(rs.getBytes("CONTAINER_PAGE_FK_ID")));
				equivalencePlacementVO.setProviderPage(convertByteToString(rs.getBytes("PROVIDER_PAGE_FK_ID")));
				equivalencePlacementVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
				equivalencePlacementVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
				equivalencePlacementVOs.add(equivalencePlacementVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return equivalencePlacementVOs;
	}
	@Override
	public List<ActionRepresetationLaunchTrxnVO> getActionRepresenationLaunchByBaseTemplate(String baseTemplateId) {
		List<ActionRepresetationLaunchTrxnVO> actionRepresetationLaunchTrxnVOs = new ArrayList<>();
		String SQL ="SELECT\n" +
				"  *,\n" +
				"  COLUMN_JSON(DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob,\n" +
				"  COLUMN_JSON(DEVICE_DEPENDENT_BLOB)   AS deviceDependentBlob\n" +
				"FROM T_UI_ADM_ACTION_REPRESENTATION_LAUNCH_TXN\n" +
				"WHERE BASE_TEMPLATE_CODE_FK_ID = ? AND RECORD_STATE_CODE_FK_ID = 'CURRENT' ;\n";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				ActionRepresetationLaunchTrxnVO actionRepresetationLaunchTrxnVO = new ActionRepresetationLaunchTrxnVO();
				actionRepresetationLaunchTrxnVO.setArsLaunchId(convertByteToString(rs.getBytes("ACTION_REPRESENTATION_LAUNCH_TXN_PK_ID")));
				actionRepresetationLaunchTrxnVO.setBaseTemplateId(rs.getString("BASE_TEMPLATE_CODE_FK_ID"));
				actionRepresetationLaunchTrxnVO.setActionRepresentationCode(rs.getString("ACTION_REPRESENTATION_CODE_FK_ID"));
				actionRepresetationLaunchTrxnVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
				actionRepresetationLaunchTrxnVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
				actionRepresetationLaunchTrxnVOs.add(actionRepresetationLaunchTrxnVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return actionRepresetationLaunchTrxnVOs;
	}
	@Override
	public List<CanvasVO> getCanvasListByBaseTemplate(String baseTemplateId,String deviceType) {
		List<CanvasVO> canvasVOList = new ArrayList<>();
		String SQL = "SELECT TXN.CANVAS_TXN_PK_ID,TXN.PARENT_CANVAS_TXN_FK_ID,MST.CANVAS_MASTER_PK_ID,TXN.CANVAS_TXN_PK_ID,TXN.BASE_TEMPLATE_CODE_FK_ID,TXN.MT_PROCESS_ELEMENT_CODE_FK_ID,TXN.PAGE_FK_ID,MST.CANVAS_TYPE_CODE_FK_ID, "
				+ "COLUMN_JSON(MST.DEVICE_INDEPENDENT_BLOB) AS mstDeviceIndependentBlob, "
				+ "COLUMN_JSON(TXN.DEVICE_INDEPENDENT_BLOB) AS txnDeviceIndependentBlob, "
				+ "COLUMN_JSON(MST.DEVICE_DEPENDENT_BLOB) AS mstDeviceDependentBlob, "
				+ "COLUMN_JSON(TXN.DEVICE_DEPENDENT_BLOB) AS txnDeviceDependentBlob, "
				+ "COLUMN_JSON(MST.DEVICE_APPLICABLE_BLOB) AS deviceApplicableBlob "
				+ "FROM T_UI_ADM_CANVAS_TXN TXN LEFT JOIN T_UI_ADM_CANVAS_MASTER MST ON TXN.CANVAS_MASTER_FK_ID = MST.CANVAS_MASTER_PK_ID AND MST.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
				+ "WHERE TXN.BASE_TEMPLATE_CODE_FK_ID= ? AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' ;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
//		parameters.add("applicableTo"+deviceType);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()) {
				CanvasVO canvasVO = new CanvasVO();
				canvasVO.setCanvasId(convertByteToString(rs.getBytes("TXN.CANVAS_TXN_PK_ID")));
				canvasVO.setParentCanvasId(convertByteToString(rs.getBytes("TXN.PARENT_CANVAS_TXN_FK_ID")));
				canvasVO.setBaseTemplateId(convertByteToString(rs.getBytes("TXN.BASE_TEMPLATE_CODE_FK_ID")));
				canvasVO.setMtPEId(rs.getString("TXN.MT_PROCESS_ELEMENT_CODE_FK_ID"));
				canvasVO.setCanvasDefnMasterFkId(convertByteToString(rs.getBytes("MST.CANVAS_MASTER_PK_ID")));
				canvasVO.setPageFkId(convertByteToString(rs.getBytes("TXN.PAGE_FK_ID")));
				canvasVO.setCanvasTypeCode(rs.getString("MST.CANVAS_TYPE_CODE_FK_ID"));
				canvasVO.setMstDeviceDependentBlob(getBlobJson(rs.getBlob("mstDeviceDependentBlob")));
				canvasVO.setTxnDeviceDependentBlob(getBlobJson(rs.getBlob("txnDeviceDependentBlob")));
				canvasVO.setMstDeviceIndependentBlob(getBlobJson(rs.getBlob("mstDeviceIndependentBlob")));
				canvasVO.setTxnDeviceIndependentBlob(getBlobJson(rs.getBlob("TxnDeviceIndependentBlob")));
				canvasVO.setDeviceApplicableBlob(getBlobJson(rs.getBlob("deviceApplicableBlob")));
//				canvasVO.setCanvasDefnFkId(convertByteToString(rs.getBytes("CANVAS_DEFN_FK_ID")));
//				canvasVO.setCanvasTypeCodeFkId(rs.getString("CANVAS_TYPE_CODE_FK_ID"));
//				canvasVO.setResizable(rs.getBoolean("IS_RESIZEABLE"));
//				canvasVO.setBtProcessElementDepFkId(convertByteToString(rs.getBytes("BT_PROCESS_ELEMENT_DEP_FK_ID")));
//				canvasVO.setViewTypeCodeFkId(rs.getString("VIEW_TYPE_CODE_FK_ID"));
//				canvasVO.setParentCanvasDefnFkId(convertByteToString(rs.getBytes("PARENT_CANVAS_DEFN_FK_ID")));
//				canvasVO.setTemplateTransition(rs.getBoolean("IS_TEMPLATE_TRANSITION"));
//				canvasVO.setAssociatedCanvasFkId(convertByteToString(rs.getBytes("ASSOCIATED_CANVAS_FK_ID")));
//				canvasVO.setAssociatedCanvasEuFkId(convertByteToString(rs.getBytes("ASSOCIATED_CANVAS_EU_FK_ID")));
//				canvasVO.setFixedPosition(rs.getBoolean("IS_FIXED_POSTION"));
//				canvasVO.setShowAtInitialLoad(rs.getBoolean("SHOW_AT_INITIAL_LOAD"));
//				canvasVO.setAutoClickIfOneItem(rs.getBoolean("AUTO_CLICK_IF_ONE_ITEM"));
//				canvasVO.setAutoClickOnFirstItem(rs.getBoolean("AUTO_CLICK_ON_FIRST_ITEM"));
//				canvasVO.setLayoutSendAlongLogin(rs.getBoolean("IS_LAYOUT_SEND_ALONG_LOGIN"));
//				canvasVO.setAutoRefresh(rs.getBoolean("IS_AUTO_REFRESH"));
//				canvasVO.setShowInWizard(rs.getBoolean("IS_SHOW_IN_WIZARD"));
//				canvasVO.setWizardStep(rs.getBoolean("IS_WIZARD_STEP"));
//				canvasVO.setWizardSeqNo(rs.getInt("WIZARD_SEQ_NO"));
//				canvasVO.setWizardSubmitStep(rs.getBoolean("IS_WIZARD_SUBMIT_STEP"));
//				canvasVO.setCanvasPlacement(getBlobJson(rs.getBlob("canvasPlacement")));
//				canvasVO.setCanvasGesture(getBlobJson(rs.getBlob("canvasGesture")));
//				canvasVO.setCanvasTheme(getBlobJson(rs.getBlob("canvasTheme")));
				canvasVOList.add(canvasVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return canvasVOList;
	}
	@Override
	public List<ControlVO> getControlListByBaseTemplate(String baseTemplateId) {
		List<ControlVO> controlVOList = new ArrayList<>();
		String SQL = "SELECT TXN.CONTROL_TXN_PK_ID,TXN.CANVAS_TXN_FK_ID,TXN.CONTROL_TXN_PK_ID,MST.CONTROL_MASTER_PK_ID,CTXN.BASE_TEMPLATE_CODE_FK_ID, CTXN.MT_PROCESS_ELEMENT_CODE_FK_ID, TXN.CONTROL_ATTR_PATH_EXPN,TXN.CONTROL_ATTRIBUTE_AGGREGATE_FUNCTION_CODE_FK_ID, "
                +"COLUMN_JSON(MST.DEVICE_INDEPENDENT_BLOB) AS mstDeviceIndependentBlob, "
                +"COLUMN_JSON(TXN.DEVICE_INDEPENDENT_BLOB) AS txnDeviceIndependentBlob, "
                +"COLUMN_JSON(MST.DEVICE_DEPENDENT_BLOB) AS mstDeviceDependentBlob, "
                +"COLUMN_JSON(TXN.DEVICE_DEPENDENT_BLOB) AS txnDeviceDependentBlob "
            +"FROM T_UI_ADM_CONTROL_TXN TXN LEFT JOIN T_UI_ADM_CONTROL_MASTER MST ON TXN.CONTROL_MASTER_FK_ID = MST.CONTROL_MASTER_PK_ID AND MST.RECORD_STATE_CODE_FK_ID = 'CURRENT' LEFT JOIN T_UI_ADM_CANVAS_TXN CTXN ON CTXN.CANVAS_TXN_PK_ID = TXN.CANVAS_TXN_FK_ID AND CTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
            +"WHERE CTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' ;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				ControlVO controlVO = new ControlVO();
				 controlVO.setControlId(convertByteToString(rs.getBytes("TXN.CONTROL_TXN_PK_ID")));
	                controlVO.setCanvasId(convertByteToString(rs.getBytes("TXN.CANVAS_TXN_FK_ID")));
	                controlVO.setControlTransactionFkId(convertByteToString(rs.getBytes("TXN.CONTROL_TXN_PK_ID")));
	                controlVO.setControlinstanceFkId(convertByteToString(rs.getBytes("MST.CONTROL_MASTER_PK_ID")));
	                controlVO.setBaseTemplateId(rs.getString("CTXN.BASE_TEMPLATE_CODE_FK_ID"));
	                controlVO.setMtPEId(rs.getString("CTXN.MT_PROCESS_ELEMENT_CODE_FK_ID"));
	                controlVO.setControlAttribute(rs.getString("TXN.CONTROL_ATTR_PATH_EXPN"));
	                controlVO.setAggregateControlFunction(rs.getString("TXN.CONTROL_ATTRIBUTE_AGGREGATE_FUNCTION_CODE_FK_ID"));
	                controlVO.setMstDeviceIndependentBlob(getBlobJson(rs.getBlob("mstDeviceIndependentBlob")));
	                controlVO.setTxnDeviceIndependentBlob(getBlobJson(rs.getBlob("txnDeviceIndependentBlob")));
	                controlVO.setMstDeviceDependentBlob(getBlobJson(rs.getBlob("mstDeviceDependentBlob")));
	                controlVO.setTxnDeviceDependentBlob(getBlobJson(rs.getBlob("txnDeviceDependentBlob")));
				controlVOList.add(controlVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return controlVOList;
	}
	@Override
	public List<PropertyControlVO> getPropertyControlListByBaseTemplate(String baseTemplateId) {
		List<PropertyControlVO> propertyControlVOList = new ArrayList<>();
		String SQL = "SELECT TXN.PROPERTY_CONTROL_TXN_PK_ID,TXN.CANVAS_TXN_FK_ID,TXN.PROPERTY_CONTROL_MASTER_FK_ID, MST.PROPERTY_CONTROL_CODE_FK_ID,PCL.PROPERTY_CONTROL_TYPE_CODE_FK_ID,CNTXN.BASE_TEMPLATE_CODE_FK_ID,CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID,TXN.PROP_CONTROL_ATTR_PATH_EXPN, "
                +"COLUMN_JSON(MST.DEVICE_INDEPENDENT_BLOB) AS mstDeviceIndependentBlob, "
                +"COLUMN_JSON(TXN.DEVICE_INDEPENDENT_BLOB) AS txnDeviceIndependentBlob, "
                +"COLUMN_JSON(MST.DEVICE_DEPENDENT_BLOB) AS mstDeviceDependentBlob, "
                +"COLUMN_JSON(TXN.DEVICE_DEPENDENT_BLOB) AS txnDeviceDependentBlob "
	      +"FROM T_UI_ADM_PROPERTY_CONTROL_TXN TXN LEFT JOIN T_UI_ADM_PROPERTY_CONTROL_MASTER MST ON TXN.PROPERTY_CONTROL_MASTER_FK_ID = MST.PROPERTY_CONTROL_MASTER_PK_ID AND MST.RECORD_STATE_CODE_FK_ID = 'CURRENT' LEFT JOIN T_UI_LKP_PROPERTY_CONTROL PCL ON PCL.PROPERTY_CONTROL_CODE_PK_ID = MST.PROPERTY_CONTROL_CODE_FK_ID AND PCL.RECORD_STATE_CODE_FK_ID = 'CURRENT' LEFT JOIN T_UI_ADM_CANVAS_TXN CNTXN ON CNTXN.CANVAS_TXN_PK_ID = TXN.CANVAS_TXN_FK_ID AND CNTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
	      +"WHERE CNTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' ;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
                PropertyControlVO propertyControlVO = new PropertyControlVO();
                propertyControlVO.setPropertyControlId(convertByteToString(rs.getBytes("TXN.PROPERTY_CONTROL_TXN_PK_ID")));
                propertyControlVO.setPropertyControlTransactionFkId(convertByteToString(rs.getBytes("TXN.PROPERTY_CONTROL_MASTER_FK_ID")));
                propertyControlVO.setBaseTemplateId(convertByteToString(rs.getBytes("CNTXN.BASE_TEMPLATE_CODE_FK_ID")));
                propertyControlVO.setPropertyControlInstanceFkId(rs.getString("CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID"));
                propertyControlVO.setCanvasId(convertByteToString(rs.getBytes("TXN.CANVAS_TXN_FK_ID")));
                propertyControlVO.setPropertyControlCode(rs.getString("MST.PROPERTY_CONTROL_CODE_FK_ID"));
                propertyControlVO.setControlTypeCode(rs.getString("PCL.PROPERTY_CONTROL_TYPE_CODE_FK_ID"));
                propertyControlVO.setPropertyControlAttribute(rs.getString("TXN.PROP_CONTROL_ATTR_PATH_EXPN"));
                propertyControlVO.setMstDeviceIndependentBlob(getBlobJson(rs.getBlob("mstDeviceIndependentBlob")));
                propertyControlVO.setMstDeviceDependentBlob(getBlobJson(rs.getBlob("mstDeviceDependentBlob")));
                propertyControlVO.setTxnDeviceIndependentBlob(getBlobJson(rs.getBlob("txnDeviceIndependentBlob")));
                propertyControlVO.setTxnDeviceDependentBlob(getBlobJson(rs.getBlob("txnDeviceDependentBlob")));
                propertyControlVOList.add(propertyControlVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return propertyControlVOList;
	}
	@Override
	public List<ActionRepresentationVO> getActionRepresenationByBaseTemplate() {
		List<ActionRepresentationVO> actionRepresentationVOList = new ArrayList<>();
		String SQL = "SELECT\n" +
				"  *,\n" +
				"  COLUMN_JSON(ACTION_PLACEMENT_BLOB)                     AS actionPlacement,\n" +
				"  COLUMN_JSON(ACTION_REPRESENTATION_ICON_PLACEMENT_BLOB) AS actionRepresentationIconPlacement,\n" +
				"  COLUMN_JSON(ACTION_IL_ICON_PLACEMENT_BLOB)             AS actionIlIconPlacement,\n" +
				"  COLUMN_JSON(ACTION_IL_LABEL_PLACEMENT_BLOB)            AS actionIlLabelPlacement,\n" +
				"  COLUMN_JSON(ACTION_ICON_ONLY_PLACEMENT_BLOB)           AS actionIconOnlyPlacement,\n" +
				"  COLUMN_JSON(ACTION_LABEL_ONLY_PLACEMENT_BLOB)          AS actionLabelOnlyPlacement\n" +
				"FROM T_UI_LKP_ACTION_REPRESENTATION " +
				"WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				ActionRepresentationVO actionRepresentationVO = new ActionRepresentationVO();
				actionRepresentationVO.setActionRepresentationCode(rs.getString("ACTION_REPRESENTATION_CODE_PK_ID"));
				actionRepresentationVO.setActionRepresentationName(getG11nValue(rs.getString("ACTION_REPRESENTATION_NAME_G11N_BIG_TXT")));
				actionRepresentationVO.setActionRepresentationIconId(rs.getString("ACTION_REPRESENTATION_ICON_CODE_FK_ID"));
				actionRepresentationVO.setMinNoMenuItems(rs.getInt("MIN_NO_OF_MENU_ITEMS_POS_INT"));
				actionRepresentationVO.setAssociatedActionGestureCode(rs.getString("ASSOCIATED_ACTION_GESTURE_CODE_FK_ID"));
				actionRepresentationVO.setAssociatedActionGestureAnimationCode(rs.getString("ASSOCIATED_ACTION_GESTURE_ANIMATION_CODE_FK_ID"));
				actionRepresentationVO.setActionPlacement(getBlobJson(rs.getBlob("actionPlacement")));
				actionRepresentationVO.setActionRepresentationIconPlacement(getBlobJson(rs.getBlob("actionRepresentationIconPlacement")));
				actionRepresentationVO.setActionIlIconPlacement(getBlobJson(rs.getBlob("actionIlIconPlacement")));
				actionRepresentationVO.setActionIlLabelPlacement(getBlobJson(rs.getBlob("actionIlLabelPlacement")));
				actionRepresentationVO.setActionIconOnlyPlacement(getBlobJson(rs.getBlob("actionIconOnlyPlacement")));
				actionRepresentationVO.setActionLabelOnlyPlacement(getBlobJson(rs.getBlob("actionLabelOnlyPlacement")));
				actionRepresentationVOList.add(actionRepresentationVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return actionRepresentationVOList;
	}
	@Override
	public List<CanvasThemeVO> getCanvasTheme() {
		List<CanvasThemeVO> canvasThemeVOList = new ArrayList<>();
		String SQL = "SELECT\n" +
				"  *,\n" +
				"  COLUMN_JSON(THEME_PROPERTY_BLOB) AS 'themeProperty'\n" +
				"FROM T_UI_ADM_CANVAS_THEME WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				CanvasThemeVO canvasThemeVO = new CanvasThemeVO();
				canvasThemeVO.setCanvasThemeSelecPkId(convertByteToString(rs.getBytes("CANVAS_THEME_PK_ID")));
				canvasThemeVO.setCanvasType(rs.getString("CANVAS_TYPE_CODE_FK_ID"));
				canvasThemeVO.setThemeTemplateCode(rs.getString("THEME_TEMPLATE_CODE_DEP_FK_ID"));
				canvasThemeVO.setThemeProperty(getBlobJson(rs.getBlob("themeProperty")));
				canvasThemeVOList.add(canvasThemeVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return canvasThemeVOList;
	}
	@Override
	public List<CanvasElementThemeVO> getCanvasThemeElement() {
		List<CanvasElementThemeVO> canvasElementThemeVOList = new ArrayList<>();
		String SQL = "SELECT\n" +
				"  *,\n" +
				"  COLUMN_JSON(ELEMENT_THEME_PROPERTY_BLOB) AS 'elementThemeProperty'\n" +
				"FROM T_UI_ADM_CANVAS_ELEMENT_THEME\n" +
				"WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				CanvasElementThemeVO canvasElementThemeVO = new CanvasElementThemeVO();
				canvasElementThemeVO.setCanvasElementThemeSelecPkId(convertByteToString(rs.getBytes("CANVAS_ELEMENT_THEME_PK_ID")));
				canvasElementThemeVO.setCanvasTypeCodeFkId(rs.getString("CANVAS_TYPE_CODE_FK_ID"));
				canvasElementThemeVO.setThemeTemplateCodeFkId(rs.getString("THEME_TEMPLATE_CODE_DEP_FK_ID"));
				canvasElementThemeVO.setGroupId(rs.getString("THEME_GROUP_CODE_FK_ID"));
				canvasElementThemeVO.setThemeSizeCode(rs.getString("THEME_SIZE_CODE_FK_ID"));
				canvasElementThemeVO.setElementThemeProperty(getBlobJson(rs.getBlob("elementThemeProperty")));
				canvasElementThemeVOList.add(canvasElementThemeVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return canvasElementThemeVOList;
	}
	@Override
	public List<ThemeAlphaVO> getThemeAlpha() {
		List<ThemeAlphaVO> alphaVOList = new ArrayList<>();
		String SQL = "SELECT *\n" +
				"FROM T_UI_ADM_THEME_ALPHA\n" +
				"WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				ThemeAlphaVO alphaVO = new ThemeAlphaVO();
				alphaVO.setThemeAlpha(convertByteToString(rs.getBytes("THEME_ALPHA_PK_ID")));
				alphaVO.setThemeTemplateCode(rs.getString("THEME_TEMPLATE_CODE_FK_ID"));
				alphaVO.setThemeStateCode(rs.getString("THEME_STATE_CODE_FK_ID"));
				alphaVO.setThemeAlphaPosInt(rs.getInt("THEME_ALPHA_POS_INT"));
				alphaVOList.add(alphaVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return alphaVOList;
	}
	@Override
	public List<ThemeGroupEntityMapVO> getThemeGroup() {
		List<ThemeGroupEntityMapVO> groupVOList = new ArrayList<>();
		String SQL = "SELECT *\n" +
				"FROM T_UI_ADM_THEME_GROUP_ENTITY_MAP\n" +
				"WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				ThemeGroupEntityMapVO groupVO = new ThemeGroupEntityMapVO();
				groupVO.setThemeGroupEntityMapId(convertByteToString(rs.getBytes("THEME_GROUP_ENTITY_MAP_PK_ID")));
				groupVO.setThemeGroupCode(rs.getString("THEME_GROUP_CODE_FK_ID"));
				groupVO.setThemeTemplateCode(rs.getString("THEME_TEMPLATE_CODE_FK_ID"));
				groupVO.setThemeEntityTypeCode(rs.getString("THEME_ENTITY_TYPE_CODE_FK_ID"));
				groupVO.setThemeEntityCode(rs.getString("THEME_ENTITY_CODE_FK_ID"));
				groupVOList.add(groupVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return groupVOList;
	}
	@Override
	public UiSettingsVO getUiSettings() {
		UiSettingsVO uiSettingsVO = new UiSettingsVO();
		String SQL = "SELECT\n" +
				"  *,\n" +
				"  COLUMN_JSON(UI_PROPERTY_BLOB) AS settings\n" +
				"FROM T_UI_ADM_SETTING\n" +
				"WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		int counter = 0;
		try {
			while(rs.next()){
				if(counter == 0){
					uiSettingsVO.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
					uiSettingsVO.setUiSettings(convertByteToString(rs.getBytes("UI_SETTING_PK_ID")));
					uiSettingsVO.setSettings(getBlobJson(rs.getBlob("settings")));
					counter++;
				}
				else{
					throw new Exception();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return uiSettingsVO;
	}
	@Override
	public List<PropertyControlLookupVO> getPropertyControlLookup() {
		List<PropertyControlLookupVO> propertyControlLookupVOList = new ArrayList<>();
		String SQL = "SELECT\n" +
				"  *\n" +
				"FROM T_UI_LKP_PROPERTY_CONTROL\n" +
				"WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				PropertyControlLookupVO propertyControlLookupVO = new PropertyControlLookupVO();
				propertyControlLookupVO.setPropertyControlCode(rs.getString("PROPERTY_CONTROL_CODE_PK_ID"));
				propertyControlLookupVO.setPropertyControlName(getG11nValue(rs.getString("PROPERTY_CONTROL_NAME_G11N_BIG_TXT")));
				propertyControlLookupVO.setControlTypeCode(rs.getString("PROPERTY_CONTROL_TYPE_CODE_FK_ID"));
				propertyControlLookupVO.setPropertyControlIcon(rs.getString("PROPERTY_CONTROL_ICON_CODE_FK_ID"));
				propertyControlLookupVO.setGlocalized(rs.getBoolean("IS_GLOCALIZED"));
				propertyControlLookupVO.setBooleanData(rs.getBoolean("IS_BOOLEAN"));
				propertyControlLookupVO.setSourceCategory(rs.getString("SOURCE_CATEGORY_TXT"));
				propertyControlLookupVO.setLayoutKey(rs.getString("LAYOUT_KEY_TXT"));
				propertyControlLookupVO.setSourceDomainObject(rs.getString("DOMAIN_OBJECT_CODE_FK_ID"));
				propertyControlLookupVO.setSourceDOA(rs.getString("DOMAIN_OBJECT_ATTRIBUTE_CODE_FK_ID"));
				propertyControlLookupVO.setReturnDOA(rs.getString("RETURN_DOMAIN_OBJECT_ATTRIBUTE_CODE_FK_ID"));
				propertyControlLookupVO.setBooleanTrueStateLayoutKey(rs.getString("BOOLEAN_TRUE_STATE_LAYOUT_KEY_TXT"));
				propertyControlLookupVO.setBooleanFalseStateLayoutKey(rs.getString("BOOLEAN_FALSE_STATE_LAYOUT_KEY_TXT"));				
				propertyControlLookupVOList.add(propertyControlLookupVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return propertyControlLookupVOList;
	}
	@Override
	public List<ActionStepVO> getActionStep() {
		List<ActionStepVO> actionStepVOList = new ArrayList<>();
		String SQL = "SELECT\n" +
				"  *,\n" +
				"  COLUMN_JSON(ACTION_STEP_NAME_G11N_BIG_TXT) AS actionStepNameBlob\n" +
				"FROM T_UI_ADM_CLIENT_ACTION_STEP_RULES\n" +
				"WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				ActionStepVO actionStepVO = new ActionStepVO();
				actionStepVO.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
				actionStepVO.setClientActionStepRuleId(convertByteToString(rs.getBytes("CLIENT_ACTION_STEP_RULES_PK_ID")));
				actionStepVO.setActionCode(rs.getString("ACTION_CODE_DEP_FK_ID"));
				actionStepVO.setActionStepName(getG11nValue(rs.getString("ACTION_STEP_NAME_G11N_BIG_TXT")));
				actionStepVO.setActionStepidText(rs.getString("ACTION_STEPID_TXT"));
				actionStepVO.setParentActionStepidText(rs.getString("PARENT_ACTION_STEPID_TXT"));
				actionStepVO.setActionControlCode(rs.getString("ACTION_CONTROL_CODE_FK_ID"));
				actionStepVO.setRuleCode(rs.getString("ACTION_STEP_RULE_CODE_FK_ID"));
				actionStepVOList.add(actionStepVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return actionStepVOList;
	}

	/**
	 * TODO PROPERTY CONTROL MAPPER TO BE COMPLETED
	 * @param propertyControlLookupVO
	 * @return
     */
	@Override
	public Map<String,Object> getLookupMap(PropertyControlLookupVO propertyControlLookupVO) {
//		String table = propertyControlLookupVO.getDbTaleName();
////		String pkColumn = propertyControlLookupVO.getDbTablePkColumn();
////		String returnColumn = propertyControlLookupVO.getDbTableReturnColumn();
//		List<SearchFields> columns = new ArrayList<>();
//		SearchFields pkColumn = new SearchFields(null);
//		SearchFields returnColumn = new SearchFields(null);
//		pkColumn.setColumnAlias("'"+propertyControlLookupVO.getDbTablePkColumn()+"'");
//		pkColumn.setColumnName(propertyControlLookupVO.getDbTablePkColumn());
//		pkColumn.setDataType("DEFAULT");
//		columns.add(pkColumn);
//		returnColumn.setColumnAlias("'"+propertyControlLookupVO.getDbTableReturnColumn()+"'");
//		returnColumn.setColumnName(propertyControlLookupVO.getDbTableReturnColumn());
//		if(propertyControlLookupVO.isGlocalized()){
//			returnColumn.setDataType("T_BLOB");
//		}else{
//			returnColumn.setDataType("DEFAULT");
//		}
//		columns.add(returnColumn);
//		ResultSet rs = getResult(columns,table);
//		Map<String, Object> lookupMap = new HashMap<>();
//		try {
//			while(rs.next()){
//				if(propertyControlLookupVO.isGlocalized()){
//					lookupMap.put(rs.getString(propertyControlLookupVO.getDbTablePkColumn()), getBlobJson(rs.getBlob(propertyControlLookupVO.getDbTableReturnColumn())));
//				}else{
//					lookupMap.put(rs.getString(propertyControlLookupVO.getDbTablePkColumn()), rs.getObject(propertyControlLookupVO.getDbTableReturnColumn()));
//				}
//			}
//		} catch (SQLException | IOException e) {
//			e.printStackTrace();
//		}
//		return lookupMap;
		return null;
	}
//	private ResultSet getResult(List<SearchFields> columns, String table) {
//		StringBuffer SQL = new StringBuffer("SELECT ");
//		int selectCounter = 0;
//		for(SearchFields column : columns){
//			if(selectCounter != 0)
//				SQL.append(", ");
//			if(column.getDataType().equals("T_BLOB")){
//				SQL.append(" COLUMN_JSON(").append(column.getColumnName()).append(") as ").append(column.getColumnAlias());
//			}else{
//				SQL.append(column.getColumnName()).append(" as ").append(column.getColumnAlias());
//			}
//			selectCounter++;
//		}
//		SQL.append(" FROM ").append(table);
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL.toString(), parameters);
//		return rs;
//	}
	@Override
	public List<ControlRelationControlVO> getControlRelationControlByBaseTemplate(String baseTemplateId) {
		List<ControlRelationControlVO> controlRelationControlEuVOs = new ArrayList<>();
		String SQL= "SELECT TXN.CONTROL_RC_TXN_PK_ID,TXN.CONTROL_TXN_FK_ID,CNTXN.BASE_TEMPLATE_CODE_FK_ID,CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID,CNTXN.CANVAS_TXN_PK_ID,CRTXN.CONTROL_TXN_PK_ID,TXN.RCCANVAS_CONTROL_MASTER_FK_ID,TXN.RCCANVAS_CONTROL_ATTR_PATH_EXPN,TXN.RCCANVAS_CONTROL_ATTRIBUTE_AGGREGATE_FUNCTION_CODE_FK_ID, "
                +"COLUMN_JSON(TXN.DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob, "
                +"COLUMN_JSON(TXN.DEVICE_DEPENDENT_BLOB) AS deviceDependentBlob "
          +"FROM T_UI_ADM_CONTROL_RC_TXN TXN " +
                "LEFT JOIN T_UI_ADM_CONTROL_TXN CRTXN ON CRTXN.CONTROL_TXN_PK_ID = TXN.CONTROL_TXN_FK_ID AND CRTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' \n" +
                "LEFT JOIN T_UI_ADM_CANVAS_TXN CNTXN ON CNTXN.CANVAS_TXN_PK_ID = CRTXN.CANVAS_TXN_FK_ID AND CNTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n"
          +"WHERE CNTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				ControlRelationControlVO controlRelationControlVO = new ControlRelationControlVO();
				controlRelationControlVO.setControlRCId(convertByteToString(rs.getBytes("TXN.CONTROL_RC_TXN_PK_ID")));
				controlRelationControlVO.setBaseTemplateId(rs.getString("CNTXN.BASE_TEMPLATE_CODE_FK_ID"));
				controlRelationControlVO.setMtPE(rs.getString("CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID"));
				controlRelationControlVO.setCanvasId(convertByteToString(rs.getBytes("CNTXN.CANVAS_TXN_PK_ID")));
				controlRelationControlVO.setControlId(convertByteToString(rs.getBytes("TXN.CONTROL_TXN_FK_ID")));
				controlRelationControlVO.setRcCanvasControlMasterId(convertByteToString(rs.getBytes("TXN.RCCANVAS_CONTROL_MASTER_FK_ID")));
				controlRelationControlVO.setRcCanvasControlAttribute(rs.getString("TXN.RCCANVAS_CONTROL_ATTR_PATH_EXPN"));
				controlRelationControlVO.setRcCanvasControlAggregateFunction(rs.getString("TXN.RCCANVAS_CONTROL_ATTRIBUTE_AGGREGATE_FUNCTION_CODE_FK_ID"));
				controlRelationControlVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
				controlRelationControlVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
                controlRelationControlEuVOs.add(controlRelationControlVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return controlRelationControlEuVOs;
	}
	@Override
	public List<ControlHierarchyControlVO> getControlHierarchyControlByBaseTemplate(String baseTemplateId) {
		List<ControlHierarchyControlVO> controlHierarchyControlEuVOs = new ArrayList<>();
		String SQL = "SELECT TXN.CONTROL_HC_TXN_ID,TXN.CONTROL_TXN_FK_ID,CNTXN.BASE_TEMPLATE_CODE_FK_ID,CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID,CNTXN.CANVAS_TXN_PK_ID,CRTXN.CONTROL_TXN_PK_ID, TXN.HC_CANVAS_CONTROL_MASTER_FK_ID, TXN.HCCANVAS_CONTROL_ATTR_PATH_EXPN, TXN.HCCANVAS_CONTROL_ATTRIBUTE_AGGREGATE_FUNCTION_CODE_FK_ID, "
                          +"COLUMN_JSON(TXN.DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob, "
                          +"COLUMN_JSON(TXN.DEVICE_DEPENDENT_BLOB) AS deviceDependentBlob "
                +"FROM T_UI_ADM_CONTROL_HC_TXN TXN " +
                "LEFT JOIN T_UI_ADM_CONTROL_TXN CRTXN ON CRTXN.CONTROL_TXN_PK_ID = TXN.CONTROL_TXN_FK_ID AND CRTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' " +
                "LEFT JOIN T_UI_ADM_CANVAS_TXN CNTXN ON CNTXN.CANVAS_TXN_PK_ID = CRTXN.CANVAS_TXN_FK_ID AND CNTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
                +"WHERE CNTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				ControlHierarchyControlVO controlHierarchyControlVO = new ControlHierarchyControlVO();
				controlHierarchyControlVO.setControlHCId(convertByteToString(rs.getBytes("TXN.CONTROL_HC_TXN_ID")));
				controlHierarchyControlVO.setBaseTemplateId(rs.getString("CNTXN.BASE_TEMPLATE_CODE_FK_ID"));
				controlHierarchyControlVO.setMtPE(rs.getString("CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID"));
				controlHierarchyControlVO.setCanvasId(convertByteToString(rs.getBytes("CNTXN.CANVAS_TXN_PK_ID")));
				controlHierarchyControlVO.setControlId(convertByteToString(rs.getBytes("TXN.CONTROL_TXN_FK_ID")));
				controlHierarchyControlVO.setHcCanvasControlMasterId(convertByteToString(rs.getBytes("TXN.HC_CANVAS_CONTROL_MASTER_FK_ID")));
				controlHierarchyControlVO.setHcCanvasControlAttribute(rs.getString("TXN.HCCANVAS_CONTROL_ATTR_PATH_EXPN"));
				controlHierarchyControlVO.setHcCanvasControlAggregateFunction(rs.getString("TXN.HCCANVAS_CONTROL_ATTRIBUTE_AGGREGATE_FUNCTION_CODE_FK_ID"));
				controlHierarchyControlVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
				controlHierarchyControlVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
                controlHierarchyControlEuVOs.add(controlHierarchyControlVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return controlHierarchyControlEuVOs;
	}
	@Override
	public List<ControlGroupByControlVO> getControlGroupByControlByBaseTemplate(String baseTemplateId) {
		List<ControlGroupByControlVO> controlGroupByControlEuVOs = new ArrayList<>();
		String SQL = "SELECT TXN.CONTROL_GROUP_BY_CONTROL_TXN_ID,TXN.CONTROL_GROUP_BY_CONTROL_TXN_ID,CNTXN.BASE_TEMPLATE_CODE_FK_ID,CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID,CNTXN.CANVAS_TXN_PK_ID,CRTXN.CONTROL_TXN_PK_ID,TXN.GROUP_BY_CONTROL_TXN_FK_ID,TXN.SEQUENCE_NUMBER_POS_INT, "
                +"COLUMN_JSON(TXN.DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob, "
                +"COLUMN_JSON(TXN.DEVICE_DEPENDENT_BLOB) AS deviceDependentBlob "
        +"FROM T_UI_ADM_CONTROL_GROUP_BY_CONTROL_TXN TXN " +
                "LEFT JOIN T_UI_ADM_CONTROL_TXN CRTXN ON CRTXN.CONTROL_TXN_PK_ID = TXN.CONTROL_TXN_FK_ID AND CRTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT'" +
                "LEFT JOIN T_UI_ADM_CANVAS_TXN CNTXN ON CNTXN.CANVAS_TXN_PK_ID = CRTXN.CANVAS_TXN_FK_ID AND CNTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT'"
        +"WHERE CNTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){             
                ControlGroupByControlVO controlGroupByControlVO = new ControlGroupByControlVO();
                controlGroupByControlVO.setControlGroupByControlId(convertByteToString(rs.getBytes("TXN.CONTROL_GROUP_BY_CONTROL_TXN_ID")));
                controlGroupByControlVO.setBaseTemplateId(rs.getString("CNTXN.BASE_TEMPLATE_CODE_FK_ID"));
                controlGroupByControlVO.setMtPE(rs.getString("CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID"));
                controlGroupByControlVO.setCanvasId(convertByteToString(rs.getBytes("CNTXN.CANVAS_TXN_PK_ID")));
                controlGroupByControlVO.setControlId(convertByteToString(rs.getBytes("TXN.CONTROL_TXN_FK_ID")));
                controlGroupByControlVO.setGroupByControlId(convertByteToString(rs.getBytes("TXN.GROUP_BY_CONTROL_TXN_FK_ID")));
                controlGroupByControlVO.setSeqNumber(rs.getInt("TXN.SEQUENCE_NUMBER_POS_INT"));
                controlGroupByControlVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
                controlGroupByControlVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
                controlGroupByControlEuVOs.add(controlGroupByControlVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return controlGroupByControlEuVOs;
	}
	@Override
	public List<ControlEventVO> getControlEventByBaseTemplate(String baseTemplateId) {
		List<ControlEventVO> controlEventEuVOs = new ArrayList<>();
		String SQL = "SELECT TXN.CONTROL_EVENT_TXN_PK_ID,TXN.CONTROL_EVENT_TXN_PK_ID,CNTXN.BASE_TEMPLATE_CODE_FK_ID,CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID,CNTXN.CANVAS_TXN_PK_ID,CRTXN.CONTROL_TXN_PK_ID, "
                +"COLUMN_JSON(TXN.DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob, "
                +"COLUMN_JSON(TXN.DEVICE_DEPENDENT_BLOB) AS deviceDependentBlob "
	        +"FROM T_UI_ADM_CONTROL_EVENT_TXN TXN " +
                "LEFT JOIN T_UI_ADM_CONTROL_TXN CRTXN ON CRTXN.CONTROL_TXN_PK_ID = TXN.CONTROL_TXN_FK_ID AND CRTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' " +
                "LEFT JOIN T_UI_ADM_CANVAS_TXN CNTXN ON CNTXN.CANVAS_TXN_PK_ID = CRTXN.CANVAS_TXN_FK_ID AND CNTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
	        +"WHERE CNTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){                        
				ControlEventVO controlEventVO = new ControlEventVO();
				controlEventVO.setControlEventId(convertByteToString(rs.getBytes("TXN.CONTROL_EVENT_TXN_PK_ID")));
				controlEventVO.setBaseTemplateId(rs.getString("CNTXN.BASE_TEMPLATE_CODE_FK_ID"));
				controlEventVO.setMtPE(rs.getString("CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID"));
				controlEventVO.setControlId(convertByteToString(rs.getBytes("CRTXN.CONTROL_TXN_PK_ID")));
				controlEventVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
                controlEventVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
				controlEventEuVOs.add(controlEventVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return controlEventEuVOs;
	}
	@Override
	public List<CanvasEventVO> getCanvasEventByBaseTemplate(String basetemplate) {
		List<CanvasEventVO> canvasEventVOs = new ArrayList<>();
		String SQL = "SELECT\n" +
                "  *,\n" +
                "  COLUMN_JSON(CETXN.DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob,\n" +
                "  COLUMN_JSON(CETXN.DEVICE_DEPENDENT_BLOB)   AS deviceDependentBlob\n" +
                "FROM T_UI_ADM_CANVAS_EVENT_TXN CETXN\n" +
                "    LEFT JOIN T_UI_ADM_CANVAS_TXN CTXN ON CETXN.CANVAS_TXN_FK_ID = CTXN.CANVAS_TXN_PK_ID AND CTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n" +
                "    WHERE CTXN.BASE_TEMPLATE_CODE_FK_ID=? AND CETXN.RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		parameters.add(basetemplate);
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while(rs.next()){
				CanvasEventVO canvasEventVO = new CanvasEventVO();
				canvasEventVO.setCanvasEventId(convertByteToString(rs.getBytes("CETXN.CANVAS_EVENT_TXN_ID ")));
                canvasEventVO.setCanvasEventTransactionFkId(convertByteToString(rs.getBytes("CETXN.CANVAS_TXN_FK_ID")));
                canvasEventVO.setBaseTemplateId(rs.getString("CTXN.BASE_TEMPLATE_CODE_FK_ID"));
                canvasEventVO.setMtPEId(rs.getString("CTXN.MT_PROCESS_ELEMENT_CODE_FK_ID"));
                canvasEventVO.setCanvasId(convertByteToString(rs.getBytes("CTXN.CANVAS_TXN_PK_ID")));
                canvasEventVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
                canvasEventVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
				canvasEventVOs.add(canvasEventVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return canvasEventVOs;
	}
	@Override
	public List<PropertyControlPatternMasterVO> getPropertyControlPatternMaster() {
		List<PropertyControlPatternMasterVO> propertyControlPatternMasterVOs = new ArrayList<>();
		String SQL = "SELECT\n" +
                "  *,\n" +
                "  COLUMN_JSON(PCPM.DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob,\n" +
                "  COLUMN_JSON(PCPM.DEVICE_DEPENDENT_BLOB)   AS deviceDependentBlob\n" +
                "FROM T_UI_ADM_PROPERTY_CONTROL_PATTERN_MASTER PCPM\n" +
                "  LEFT JOIN T_UI_LKP_PROPERTY_CONTROL PCL\n" +
                "    ON PCL.PROPERTY_CONTROL_CODE_PK_ID = PROPERTY_CONTROL_CODE_FK_ID AND PCL.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n" +
                "WHERE PCPM.RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while(rs.next()){
				PropertyControlPatternMasterVO propertyControlPatternMasterVO = new PropertyControlPatternMasterVO();
				propertyControlPatternMasterVO.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
				propertyControlPatternMasterVO.setPropertyControlPatternPkId(convertByteToString(rs.getBytes("PROPERTY_CONTROL_PATTERN_MASTER_PK_ID")));
				propertyControlPatternMasterVO.setPropertyControlPatternCodeFkId(rs.getString("PROPERTY_CONTROL_PATTERN_CODE_FK_ID"));
				propertyControlPatternMasterVO.setPropertyControlCode(rs.getString("PROPERTY_CONTROL_CODE_FK_ID"));
//				propertyControlPatternMasterVO.setControlTypeCode(rs.getString("CONTROL_TYPE_CODE_FK_ID"));
				propertyControlPatternMasterVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
				propertyControlPatternMasterVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
//				propertyControlPatternMasterVO.setEffectiveDatetime(rs.getTimestamp("EFFECTIVE_DATETIME"));
//				propertyControlPatternMasterVO.setSaveStateCodeFkId(rs.getString("SAVE_STATE_CODE_FK_ID"));
//				propertyControlPatternMasterVO.setActiveStateCodeFkId(rs.getString("ACTIVE_STATE_CODE_FK_ID"));
//				propertyControlPatternMasterVO.setRecordStateCodeFkId(rs.getString("RECORD_STATE_CODE_FK_ID"));
//				propertyControlPatternMasterVO.setIsDeleted(rs.getBoolean("IS_DELETED"));
//				propertyControlPatternMasterVO.setVersionChangesBlob(getBlobJson(rs.getBlob("versionChangesBlob")));
//				propertyControlPatternMasterVO.setPropogateFutureChangesBlob(getBlobJson(rs.getBlob("propogateFutureChangesBlob")));
//				propertyControlPatternMasterVO.setCreatedbyPersonFkId(convertByteToString(rs.getBytes("CREATEDBY_PERSON_FK_ID")));
//				propertyControlPatternMasterVO.setCreatedByPersonFullnameTxt(rs.getString("CREATED_BY_PERSON_FULLNAME_TXT"));
//				propertyControlPatternMasterVO.setCreatedDatetime(rs.getTimestamp("CREATED_DATETIME"));
//				propertyControlPatternMasterVO.setLastModifiedbyPersonFkId(convertByteToString(rs.getBytes("LAST_MODIFIEDBY_PERSON_FK_ID")));
//				propertyControlPatternMasterVO.setLastModifiedbyPersonFullnameTxt(rs.getString("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT"));
//				propertyControlPatternMasterVO.setLastModifiedDatetime(rs.getTimestamp("LAST_MODIFIED_DATETIME"));
//				propertyControlPatternMasterVO.setDlgtdForCreatedbyPrsnFkId(convertByteToString(rs.getBytes("DLGTD_FOR_CREATEDBY_PRSN_FK_ID")));
//				propertyControlPatternMasterVO.setDlgtdForCreatedbyPrsnFullnameTxt(convertByteToString(rs.getBytes("DLGTD_FOR_CREATEDBY_PRSN_FULLNAME_TXT")));
//				propertyControlPatternMasterVO.setDlgtdForMdfdbyPrsnFkId(convertByteToString(rs.getBytes("DLGTD_FOR_MDFDBY_PRSN_FK_ID")));
//				propertyControlPatternMasterVO.setDlgtdForMdfdbyPrsnFullnameTxt(convertByteToString(rs.getBytes("DLGTD_FOR_MDFDBY_PRSN_FULLNAME_TXT")));
//				propertyControlPatternMasterVO.setLastModifiedbyPersonFullnameTxt(convertByteToString(rs.getBytes("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT")));
//				propertyControlPatternMasterVO.setFeatured(rs.getBoolean("IS_FEATURED"));
//				propertyControlPatternMasterVO.setExternalReferenceBlob(getBlobJson(rs.getBlob("externalReferenceBlob")));
//				propertyControlPatternMasterVO.setRefBaseObjectFkId(convertByteToString(rs.getBytes("REF_BASE_OBJECT_FK_ID")));
//				propertyControlPatternMasterVO.setRefBaseTemplateFkId(convertByteToString(rs.getBytes("REF_BASE_TEMPLATE_FK_ID")));
//				propertyControlPatternMasterVO.setTransitioningBaseTemplateFkId(convertByteToString(rs.getBytes("TRANSITIONING_BASE_TEMPLATE_FK_ID")));
//				propertyControlPatternMasterVO.setRefWorkflowDefnFkId(convertByteToString(rs.getBytes("REF_WORKFLOW_DEFN_FK_ID")));
//				propertyControlPatternMasterVO.setRefWorkflowTxnFkId(convertByteToString(rs.getBytes("REF_WORKFLOW_TXN_FK_ID")));
//				propertyControlPatternMasterVO.setRefWorkflowTxnStatusCodeFkId(rs.getString("REF_WORKFLOW_TXN_STATUS_CODE_FK_ID"));
//				propertyControlPatternMasterVO.setRefWorkflowTxnStepFkId(convertByteToString(rs.getBytes("REF_WORKFLOW_TXN_STEP_FK_ID")));
//				propertyControlPatternMasterVO.setRefWorkflowTxnStepStatusCodeFkId(rs.getString("REF_WORKFLOW_TXN_STEP_STATUS_CODE_FK_ID"));
				propertyControlPatternMasterVOs.add(propertyControlPatternMasterVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return propertyControlPatternMasterVOs;
	}
	@Override
	public List<PropertyControlEventPatternMasterVO> getPropertyControlEventPatternMaster() {
		List<PropertyControlEventPatternMasterVO> propertyControlEventPatternMasterVOs = new ArrayList<>();
		String SQL = "SELECT\n" +
                "  *,\n" +
                "  COLUMN_JSON(DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob\n" +
                "FROM T_UI_ADM_PROPERTY_CONTROL_EVENT_PATTERN_MASTER\n" +
                "WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while(rs.next()){
				PropertyControlEventPatternMasterVO propertyControlEventPatternMasterVO = new PropertyControlEventPatternMasterVO();
				propertyControlEventPatternMasterVO.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
				propertyControlEventPatternMasterVO.setPropertyControlEventPatternPkId(convertByteToString(rs.getBytes("PROPERTY_CONTROL_EVENT_PATTERN_MASTER_PK_ID")));
				propertyControlEventPatternMasterVO.setPropertyControlEventPatternCodeFkId(rs.getString("PROPERTY_CONTROL_EVENT_PATTERN_CODE_FK_ID"));
//				propertyControlEventPatternMasterVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
				propertyControlEventPatternMasterVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
//				propertyControlEventPatternMasterVO.setEffectiveDatetime(rs.getTimestamp("EFFECTIVE_DATETIME"));
//				propertyControlEventPatternMasterVO.setSaveStateCodeFkId(rs.getString("SAVE_STATE_CODE_FK_ID"));
//				propertyControlEventPatternMasterVO.setActiveStateCodeFkId(rs.getString("ACTIVE_STATE_CODE_FK_ID"));
//				propertyControlEventPatternMasterVO.setRecordStateCodeFkId(rs.getString("RECORD_STATE_CODE_FK_ID"));
//				propertyControlEventPatternMasterVO.setIsDeleted(rs.getBoolean("IS_DELETED"));
//				propertyControlEventPatternMasterVO.setVersionChangesBlob(getBlobJson(rs.getBlob("versionChangesBlob")));
//				propertyControlEventPatternMasterVO.setPropogateFutureChangesBlob(getBlobJson(rs.getBlob("propogateFutureChangesBlob")));
//				propertyControlEventPatternMasterVO.setCreatedbyPersonFkId(convertByteToString(rs.getBytes("CREATEDBY_PERSON_FK_ID")));
//				propertyControlEventPatternMasterVO.setCreatedByPersonFullnameTxt(rs.getString("CREATED_BY_PERSON_FULLNAME_TXT"));
//				propertyControlEventPatternMasterVO.setCreatedDatetime(rs.getTimestamp("CREATED_DATETIME"));
//				propertyControlEventPatternMasterVO.setLastModifiedbyPersonFkId(convertByteToString(rs.getBytes("LAST_MODIFIEDBY_PERSON_FK_ID")));
//				propertyControlEventPatternMasterVO.setLastModifiedbyPersonFullnameTxt(rs.getString("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT"));
//				propertyControlEventPatternMasterVO.setLastModifiedDatetime(rs.getTimestamp("LAST_MODIFIED_DATETIME"));
//				propertyControlEventPatternMasterVO.setDlgtdForCreatedbyPrsnFkId(convertByteToString(rs.getBytes("DLGTD_FOR_CREATEDBY_PRSN_FK_ID")));
//				propertyControlEventPatternMasterVO.setDlgtdForCreatedbyPrsnFullnameTxt(convertByteToString(rs.getBytes("DLGTD_FOR_CREATEDBY_PRSN_FULLNAME_TXT")));
//				propertyControlEventPatternMasterVO.setDlgtdForMdfdbyPrsnFkId(convertByteToString(rs.getBytes("DLGTD_FOR_MDFDBY_PRSN_FK_ID")));
//				propertyControlEventPatternMasterVO.setDlgtdForMdfdbyPrsnFullnameTxt(convertByteToString(rs.getBytes("DLGTD_FOR_MDFDBY_PRSN_FULLNAME_TXT")));
//				propertyControlEventPatternMasterVO.setLastModifiedbyPersonFullnameTxt(convertByteToString(rs.getBytes("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT")));
//				propertyControlEventPatternMasterVO.setFeatured(rs.getBoolean("IS_FEATURED"));
//				propertyControlEventPatternMasterVO.setExternalReferenceBlob(getBlobJson(rs.getBlob("externalReferenceBlob")));
//				propertyControlEventPatternMasterVO.setRefBaseObjectFkId(convertByteToString(rs.getBytes("REF_BASE_OBJECT_FK_ID")));
//				propertyControlEventPatternMasterVO.setRefBaseTemplateFkId(convertByteToString(rs.getBytes("REF_BASE_TEMPLATE_FK_ID")));
//				propertyControlEventPatternMasterVO.setTransitioningBaseTemplateFkId(convertByteToString(rs.getBytes("TRANSITIONING_BASE_TEMPLATE_FK_ID")));
//				propertyControlEventPatternMasterVO.setRefWorkflowDefnFkId(convertByteToString(rs.getBytes("REF_WORKFLOW_DEFN_FK_ID")));
//				propertyControlEventPatternMasterVO.setRefWorkflowTxnFkId(convertByteToString(rs.getBytes("REF_WORKFLOW_TXN_FK_ID")));
//				propertyControlEventPatternMasterVO.setRefWorkflowTxnStatusCodeFkId(rs.getString("REF_WORKFLOW_TXN_STATUS_CODE_FK_ID"));
//				propertyControlEventPatternMasterVO.setRefWorkflowTxnStepFkId(convertByteToString(rs.getBytes("REF_WORKFLOW_TXN_STEP_FK_ID")));
//				propertyControlEventPatternMasterVO.setRefWorkflowTxnStepStatusCodeFkId(rs.getString("REF_WORKFLOW_TXN_STEP_STATUS_CODE_FK_ID"));
				propertyControlEventPatternMasterVOs.add(propertyControlEventPatternMasterVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return propertyControlEventPatternMasterVOs;
	}
	@Override
	public List<ClientEventRuleToPropertyVO> getClientEventRuleTOProeprtyMap() {
		List<ClientEventRuleToPropertyVO> clientEventRuleToPropertyVOList = new ArrayList<>();
		String SQL = "SELECT *\n" +
                "FROM T_UI_LKP_CLIENT_EVENT_RULE_PROPERTY_MAP\n" +
                "WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';\n";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while(rs.next()){
				ClientEventRuleToPropertyVO clientEventRuleToPropertyVO = new ClientEventRuleToPropertyVO();
				clientEventRuleToPropertyVO.setClientEventRulePropertyMapId(convertByteToString(rs.getBytes("CLIENT_EVENT_RULE_PROPERTY_MAP_PK_ID")));
				clientEventRuleToPropertyVO.setRule(rs.getString("CLIENT_EVENT_RULE_CODE_FK_ID"));
				clientEventRuleToPropertyVO.setProperty(rs.getString("CLIENT_EVENT_PROPERTY_CODE_FK_ID"));
				clientEventRuleToPropertyVOList.add(clientEventRuleToPropertyVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientEventRuleToPropertyVOList;
	}
	@Override
	public List<ActionStandAlonePropertiesVO> getStandAloneProperties(String baseTemplateId) {
		List<ActionStandAlonePropertiesVO> actionStandAloneTxnPropertiesVOList = new ArrayList<>();
		String SQL = "SELECT TXN.STANDALONE_ACTION_TXN_PROPS_PK_ID, SATXN.STANDALONE_ACTION_TXN_PK_ID, TXN.CLIENT_EVENT_RULE_CODE_FK_ID, TXN.CLIENT_EVENT_PROPERTY_CODE_FK_ID, TXN.CLIENT_EVENT_PROPERTY_LNG_TXT "
				+"FROM T_UI_ADM_ACTION_STANDALONE_TXN_PROPS TXN "
				+"  LEFT JOIN T_UI_ADM_ACTION_STANDALONE_TXN SATXN ON TXN.STANDALONE_ACTION_TXN_FK_ID= SATXN.STANDALONE_ACTION_TXN_PK_ID AND SATXN.RECORD_STATE_CODE_FK_ID = 'CURRENT'"
				+"  LEFT JOIN T_UI_ADM_CANVAS_TXN CNTXN ON CNTXN.CANVAS_TXN_PK_ID = SATXN.CANVAS_TXN_FK_ID AND CNTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT'"
				+"WHERE CNTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while(rs.next()){
				ActionStandAlonePropertiesVO actionTxnPropertiesVO = new ActionStandAlonePropertiesVO();
				actionTxnPropertiesVO.setActionStandalonePropertyId(convertByteToString(rs.getBytes("TXN.STANDALONE_ACTION_TXN_PROPS_PK_ID")));
				actionTxnPropertiesVO.setActionStandaloneTxnId(convertByteToString(rs.getBytes("SATXN.STANDALONE_ACTION_TXN_PK_ID")));
				actionTxnPropertiesVO.setRule(rs.getString("TXN.CLIENT_EVENT_RULE_CODE_FK_ID"));
				actionTxnPropertiesVO.setProperty(rs.getString("TXN.CLIENT_EVENT_PROPERTY_CODE_FK_ID"));
				actionTxnPropertiesVO.setPropertyValue(rs.getString("TXN.CLIENT_EVENT_PROPERTY_LNG_TXT"));
				actionStandAloneTxnPropertiesVOList.add(actionTxnPropertiesVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return actionStandAloneTxnPropertiesVOList;
	}
	@Override
	public List<GroupActionTxnPropertiesVO> getGroupActionProperties(String baseTemplateId) {
		List<GroupActionTxnPropertiesVO> groupActionTxnPropertiesVOList = new ArrayList<>();
		String SQL = "SELECT TXN.GROUP_ACTION_TXN_PROPS_PK_ID, GATXN.GROUP_ACTION_TXN_PK_ID, TXN.CLIENT_EVENT_RULE_CODE_FK_ID, TXN.CLIENT_EVENT_PROPERTY_CODE_FK_ID, TXN.CLIENT_EVENT_PROPERTY_LNG_TXT "
				+"FROM T_UI_ADM_GROUP_ACTION_TXN_PROPS TXN "
				+"  LEFT JOIN T_UI_ADM_GROUP_ACTION_TXN GATXN ON TXN.GROUP_ACTION_TXN_FK_ID= GATXN.GROUP_ACTION_TXN_PK_ID AND GATXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
				+"  LEFT JOIN T_UI_ADM_CANVAS_TXN CNTXN ON CNTXN.CANVAS_TXN_PK_ID = GATXN.CANVAS_TXN_FK_ID AND CNTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
				+"WHERE CNTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' ;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while(rs.next()){
				GroupActionTxnPropertiesVO actionTxnPropertiesVO = new GroupActionTxnPropertiesVO();
				actionTxnPropertiesVO.setGroupActionTxnPropertiesId(convertByteToString(rs.getBytes("TXN.GROUP_ACTION_TXN_PROPS_PK_ID")));
				actionTxnPropertiesVO.setGroupActionTxnId(convertByteToString(rs.getBytes("GATXN.GROUP_ACTION_TXN_PK_ID")));
				actionTxnPropertiesVO.setRule(rs.getString("TXN.CLIENT_EVENT_RULE_CODE_FK_ID"));
				actionTxnPropertiesVO.setProperty(rs.getString("TXN.CLIENT_EVENT_PROPERTY_CODE_FK_ID"));
				actionTxnPropertiesVO.setPropertyValue(rs.getString("TXN.CLIENT_EVENT_PROPERTY_LNG_TXT"));
				groupActionTxnPropertiesVOList.add(actionTxnPropertiesVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return groupActionTxnPropertiesVOList;
	}
	@Override
	public List<ActionStandAloneVO> getActionStandAloneByBaseTemplate(String basetemplateId, String deviceType) {
		List<ActionStandAloneVO> actionStandAloneVOs = new ArrayList<>();
		String SQL = "SELECT TXN.STANDALONE_ACTION_TXN_PK_ID, TXN.CANVAS_TXN_FK_ID, CRTXN.CONTROL_TXN_PK_ID, PCRTXN.PROPERTY_CONTROL_TXN_PK_ID, CNTXN.BASE_TEMPLATE_CODE_FK_ID, CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID, MST.ACTION_CODE_FK_ID, AL.ACTION_CONTROL_TYPE_CODE_FK_ID, AL.IS_LICENSE,AL.IS_CREATE_ACCESS_CONTROL_EQUIVALENT, AL.IS_READ_ACCESS_CONTROL_EQUIVALENT, AL.IS_UPDATE_ACCESS_CONTROL_EQUIVALENT, AL.IS_DELETE_ACCESS_CONTROL_EQUIVALENT,BTADO.ACTION_LABEL_G11N_BIG_TXT, BTADO.ACTION_ICON_CODE_FK_ID, BTADO.IS_SHOW_ACTION_LABEL, BTADO.IS_SHOW_ACTION_ICON, BTADO.ACTION_ON_STATE_ICON_FK_ID, BTADO.ACTION_OFF_STATE_ICON_FK_ID, BTADO.ACTION_SEQ_NUMBER_POS_INT,\n" +
                "                COLUMN_JSON(MST.DEVICE_INDEPENDENT_BLOB) AS mstDeviceIndependentBlob,\n" +
                "                COLUMN_JSON(TXN.DEVICE_INDEPENDENT_BLOB) AS txnDeviceIndependentBlob,\n" +
                "                COLUMN_JSON(MST.DEVICE_DEPENDENT_BLOB) AS mstDeviceDependentBlob,\n" +
                "                COLUMN_JSON(MST.DEVICE_APPLICABLE_BLOB) AS deviceApplicableBlob\n" +
                "        FROM T_UI_ADM_ACTION_STANDALONE_TXN TXN\n" +
                "          LEFT JOIN T_UI_ADM_ACTION_STANDALONE_MASTER MST ON TXN.STANDALONE_ACTION_MASTER_FK_ID = MST.STANDALONE_ACTION_MASTER_PK_ID AND MST.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n" +
                "          LEFT JOIN T_UI_ADM_CANVAS_TXN CNTXN ON CNTXN.CANVAS_TXN_PK_ID = TXN.CANVAS_TXN_FK_ID AND CNTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n" +
                "          LEFT JOIN T_UI_ADM_CONTROL_TXN CRTXN ON CRTXN.CANVAS_TXN_FK_ID = TXN.CANVAS_TXN_FK_ID AND CRTXN.CONTROL_MASTER_FK_ID = MST.CONTROL_MASTER_FK_ID AND CRTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n" +
                "          LEFT JOIN T_UI_ADM_PROPERTY_CONTROL_TXN PCRTXN ON PCRTXN.CANVAS_TXN_FK_ID = TXN.CANVAS_TXN_FK_ID AND PCRTXN.PROPERTY_CONTROL_MASTER_FK_ID = MST.PROPERTY_CONTROL_MASTER_FK_ID AND PCRTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n" +
                "          LEFT JOIN T_UI_LKP_ACTION AL ON AL.ACTION_CODE_PK_ID = MST.ACTION_CODE_FK_ID AND AL.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n" +
                "         LEFT JOIN T_UI_ADM_BT_ACTION_DISPLAY_OVERRIDE BTADO ON BTADO.ACTION_CODE_FK_ID = MST.ACTION_CODE_FK_ID AND BTADO.BASE_TEMPLATE_DEP_CODE_FK_ID = CNTXN.BASE_TEMPLATE_CODE_FK_ID AND BTADO.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n" +
                "        WHERE CNTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND AL.IS_LICENSE = true AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' ;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(basetemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while(rs.next()){
				ActionStandAloneVO actionStandAloneVO = new ActionStandAloneVO();
                actionStandAloneVO.setActionStandaloneId(convertByteToString(rs.getBytes("TXN.STANDALONE_ACTION_TXN_PK_ID")));
                actionStandAloneVO.setCanvasId(convertByteToString(rs.getBytes("TXN.CANVAS_TXN_FK_ID")));
                actionStandAloneVO.setControlId(convertByteToString(rs.getBytes("CRTXN.CONTROL_TXN_PK_ID")));
                actionStandAloneVO.setPropertyControlId(convertByteToString(rs.getBytes("PCRTXN.PROPERTY_CONTROL_TXN_PK_ID")));
                actionStandAloneVO.setBaseTemplateId(rs.getString("CNTXN.BASE_TEMPLATE_CODE_FK_ID"));
                actionStandAloneVO.setMtPEId(rs.getString("CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID"));
                actionStandAloneVO.setActionCode(rs.getString("MST.ACTION_CODE_FK_ID"));
                actionStandAloneVO.setControlTypeCode(rs.getString("AL.ACTION_CONTROL_TYPE_CODE_FK_ID"));
                actionStandAloneVO.setActionLabel(getG11nValue(rs.getString("BTADO.ACTION_LABEL_G11N_BIG_TXT")));
                actionStandAloneVO.setActionIcon(convertByteToString(rs.getBytes("BTADO.ACTION_ICON_CODE_FK_ID")));
                actionStandAloneVO.setShowActionLabel(rs.getBoolean("BTADO.IS_SHOW_ACTION_LABEL"));
                actionStandAloneVO.setShowActionIcon(rs.getBoolean("BTADO.IS_SHOW_ACTION_ICON"));
                actionStandAloneVO.setActionOnStateIcon(convertByteToString(rs.getBytes("BTADO.ACTION_ON_STATE_ICON_FK_ID")));
                actionStandAloneVO.setActionOffStateIcon(convertByteToString(rs.getBytes("BTADO.ACTION_OFF_STATE_ICON_FK_ID")));
                actionStandAloneVO.setLicenced(rs.getBoolean("AL.IS_LICENSE"));
                actionStandAloneVO.setCreateAccessControlEquivalent(rs.getBoolean("AL.IS_CREATE_ACCESS_CONTROL_EQUIVALENT"));
                actionStandAloneVO.setReadAccessControlEquivalent(rs.getBoolean("AL.IS_READ_ACCESS_CONTROL_EQUIVALENT"));
                actionStandAloneVO.setUpdateAccessControlEquivalent(rs.getBoolean("AL.IS_UPDATE_ACCESS_CONTROL_EQUIVALENT"));
                actionStandAloneVO.setDeleteAccessControlEquivalent(rs.getBoolean("AL.IS_DELETE_ACCESS_CONTROL_EQUIVALENT"));
                actionStandAloneVO.setActionSeqNo(rs.getInt("BTADO.ACTION_SEQ_NUMBER_POS_INT"));
                actionStandAloneVO.setMstDeviceDependentBlob(getBlobJson(rs.getBlob("mstDeviceDependentBlob")));
                actionStandAloneVO.setMstDeviceIndependentBlob(getBlobJson(rs.getBlob("mstDeviceIndependentBlob")));
                actionStandAloneVO.setTxnDeviceIndependentBlob(getBlobJson(rs.getBlob("txnDeviceIndependentBlob")));
                actionStandAloneVO.setDeviceApplicableBlob(getBlobJson(rs.getBlob("deviceApplicableBlob")));
                actionStandAloneVOs.add(actionStandAloneVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return actionStandAloneVOs;
	}
	@Override
	public Map<String, ActionGroupVO> getActionGroupMapByBt(String baseTemplate) {
		Map<String, ActionGroupVO> actionGroupMap = new HashMap<>();
		String SQL = "SELECT BTAGDO.BASE_TEMPLATE_CODE_FK_ID, BTAGDO.ACTION_GROUP_CODE_FK_ID, BTAGDO.IS_AUTO_EXPAND_ALLOWED, BTAGDO.ACTION_GROUP_ICON_CODE_FK_ID, BTAGDO.IS_ACTION_GROUP_LABEL_SHOWN, BTAGDO.IS_ACTION_GROUP_ICON_SHOWN, BTAGDO.ACTION_GROUP_SEQ_NUMBER_POS_INT, BTAGDO.ACTION_GROUP_CODE_FK_ID, AG.PARENT_ACTION_GROUP_CODE_FK_ID, AG.ACTION_CATEGORY_CODE_FK_ID "
					+"FROM T_UI_ADM_BT_ACTION_GROUP_DISPLAY_OVERRIDE BTAGDO "
					+"LEFT JOIN T_UI_ADM_ACTION_GROUP AG ON BTAGDO.ACTION_GROUP_CODE_FK_ID = AG.ACTION_GROUP_CODE_PK_ID AND AG.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
					+ "WHERE BTAGDO.BASE_TEMPLATE_CODE_FK_ID = ? AND BTAGDO.RECORD_STATE_CODE_FK_ID = 'CURRENT' ";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplate);
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while(rs.next()){
				ActionGroupVO actionGroupVO = new ActionGroupVO();
                actionGroupVO.setBaseTemplateId(rs.getString("BTAGDO.BASE_TEMPLATE_CODE_FK_ID"));
                actionGroupVO.setActionGroupCode(rs.getString("BTAGDO.ACTION_GROUP_CODE_FK_ID"));
                actionGroupVO.setAllowAutoExpand(rs.getBoolean("BTAGDO.IS_AUTO_EXPAND_ALLOWED"));
                actionGroupVO.setActionGroupLabel(getG11nValue(rs.getString("BTAGDO.ACTION_GROUP_LABEL_G11N_BIG_TXT")));
                actionGroupVO.setActionGroupIcon(rs.getString("BTAGDO.ACTION_GROUP_ICON_CODE_FK_ID"));
                actionGroupVO.setShowActionGroupLabel(rs.getBoolean("BTAGDO.IS_ACTION_GROUP_LABEL_SHOWN"));
                actionGroupVO.setShowActionGroupIconId(rs.getBoolean("BTAGDO.IS_ACTION_GROUP_ICON_SHOWN"));
                actionGroupVO.setActionGroupSeqNum(rs.getInt("BTAGDO.ACTION_GROUP_SEQ_NUMBER_POS_INT"));
                actionGroupVO.setParentActionGroupCode(rs.getString("AG.PARENT_ACTION_GROUP_CODE_FK_ID"));
                actionGroupVO.setActionCategoryCode(rs.getString("AG.ACTION_CATEGORY_CODE_FK_ID"));
                actionGroupMap.put(actionGroupVO.getActionGroupCode(),actionGroupVO);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return actionGroupMap;
	}
	@Override
	public List<CanvasActionForAgVO> getCanvasActionForAgByBaseTemplate(String baseTemplateId, String deviceType) {
		List<CanvasActionForAgVO> canvasActionForAgVOList = new ArrayList<>();
		String SQL = "SELECT TXN.GROUP_ACTION_TXN_PK_ID, CNTXN.BASE_TEMPLATE_CODE_FK_ID, CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID,CNTXN.CANVAS_TXN_PK_ID, MST.ACTION_CODE_FK_ID, MST.ACTION_GROUP_CODE_FK_ID, MST.GROUP_ACTION_MASTER_PK_ID, AL.ACTION_CONTROL_TYPE_CODE_FK_ID,AL.IS_LICENSE, AL.IS_CREATE_ACCESS_CONTROL_EQUIVALENT, AL.IS_READ_ACCESS_CONTROL_EQUIVALENT, AL.IS_UPDATE_ACCESS_CONTROL_EQUIVALENT, AL.IS_DELETE_ACCESS_CONTROL_EQUIVALENT,BTADO.ACTION_LABEL_G11N_BIG_TXT, BTADO.ACTION_ICON_CODE_FK_ID, BTADO.IS_SHOW_ACTION_LABEL, BTADO.IS_SHOW_ACTION_ICON, BTADO.ACTION_ON_STATE_ICON_FK_ID, BTADO.ACTION_OFF_STATE_ICON_FK_ID, BTADO.ACTION_SEQ_NUMBER_POS_INT, "
                +"COLUMN_JSON(MST.DEVICE_INDEPENDENT_BLOB) AS mstDeviceIndependentBlob, "
                +"COLUMN_JSON(TXN.DEVICE_INDEPENDENT_BLOB) AS txnDeviceIndependentBlob, "
                +"COLUMN_JSON(MST.DEVICE_APPLICABLE_BLOB) AS deviceApplicableBlob "
          +"FROM T_UI_ADM_GROUP_ACTION_TXN TXN "
          +"LEFT JOIN T_UI_ADM_ACTION_GROUP_MASTER MST ON TXN.GROUP_ACTION_MASTER_FK_ID = MST.GROUP_ACTION_MASTER_PK_ID AND MST.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
          +"LEFT JOIN T_UI_ADM_CANVAS_TXN CNTXN ON CNTXN.CANVAS_TXN_PK_ID = TXN.CANVAS_TXN_FK_ID AND CNTXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
          +"LEFT JOIN T_UI_LKP_ACTION AL ON MST.ACTION_CODE_FK_ID = AL.ACTION_CODE_PK_ID AND AL.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
          +"LEFT JOIN T_UI_ADM_ACTION_GROUP AGL ON MST.ACTION_GROUP_CODE_FK_ID = AGL.ACTION_GROUP_CODE_PK_ID AND AGL.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
          +"LEFT JOIN T_UI_ADM_BT_ACTION_DISPLAY_OVERRIDE BTADO ON BTADO.ACTION_CODE_FK_ID = MST.ACTION_CODE_FK_ID AND BTADO.BASE_TEMPLATE_DEP_CODE_FK_ID = CNTXN.BASE_TEMPLATE_CODE_FK_ID AND BTADO.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
          +"LEFT JOIN T_UI_ADM_BT_ACTION_GROUP_DISPLAY_OVERRIDE BTAGDO ON BTAGDO.ACTION_GROUP_CODE_FK_ID = MST.ACTION_GROUP_CODE_FK_ID AND BTAGDO.BASE_TEMPLATE_CODE_FK_ID = CNTXN.BASE_TEMPLATE_CODE_FK_ID AND BTAGDO.RECORD_STATE_CODE_FK_ID = 'CURRENT' "
          +"WHERE CNTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND AL.IS_LICENSE = true AND TXN.RECORD_STATE_CODE_FK_ID = 'CURRENT' ";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				CanvasActionForAgVO actionForAgVO = new CanvasActionForAgVO();
				actionForAgVO.setCanavsActionsForAgId(convertByteToString(rs.getBytes("TXN.GROUP_ACTION_TXN_PK_ID")));
				actionForAgVO.setBaseTemplateId(rs.getString("CNTXN.BASE_TEMPLATE_CODE_FK_ID"));
				actionForAgVO.setMtPE(rs.getString("CNTXN.MT_PROCESS_ELEMENT_CODE_FK_ID"));
				actionForAgVO.setCanvasId(convertByteToString(rs.getBytes("CNTXN.CANVAS_TXN_PK_ID")));
				actionForAgVO.setActionCode(rs.getString("MST.ACTION_CODE_FK_ID"));
				actionForAgVO.setActionGroupCode(rs.getString("MST.ACTION_GROUP_CODE_FK_ID"));
				actionForAgVO.setGroupActionMasterId(convertByteToString(rs.getBytes("MST.GROUP_ACTION_MASTER_PK_ID")));
				actionForAgVO.setControlTypeCode(rs.getString("AL.ACTION_CONTROL_TYPE_CODE_FK_ID"));
				actionForAgVO.setActionLabel(getG11nValue(rs.getString("BTADO.ACTION_LABEL_G11N_BIG_TXT")));
				actionForAgVO.setActionIcon(convertByteToString(rs.getBytes("BTADO.ACTION_ICON_CODE_FK_ID")));
				actionForAgVO.setShowActionLabel(rs.getBoolean("BTADO.IS_SHOW_ACTION_LABEL"));
				actionForAgVO.setShowActionIcon(rs.getBoolean("BTADO.IS_SHOW_ACTION_ICON"));
				actionForAgVO.setActionOnStateIcon(convertByteToString(rs.getBytes("BTADO.ACTION_ON_STATE_ICON_FK_ID")));
				actionForAgVO.setActionOffStateIcon(convertByteToString(rs.getBytes("BTADO.ACTION_OFF_STATE_ICON_FK_ID")));
				actionForAgVO.setLicenced(rs.getBoolean("AL.IS_LICENSE"));
				actionForAgVO.setCreateAccessControlEquivalent(rs.getBoolean("AL.IS_CREATE_ACCESS_CONTROL_EQUIVALENT"));
				actionForAgVO.setReadAccessControlEquivalent(rs.getBoolean("IS_READ_ACCESS_CONTROL_EQUIVALENT"));
				actionForAgVO.setUpdateAccessControlEquivalent(rs.getBoolean("IS_UPDATE_ACCESS_CONTROL_EQUIVALENT"));
				actionForAgVO.setDeleteAccessControlEquivalent(rs.getBoolean("IS_DELETE_ACCESS_CONTROL_EQUIVALENT"));
				actionForAgVO.setActionSeqNo(rs.getInt("BTADO.ACTION_SEQ_NUMBER_POS_INT"));
				actionForAgVO.setDeviceApplicableBlob(getBlobJson(rs.getBlob("deviceApplicableBlob")));
//				actionForAgVO.setMstDeviceDependentBlob(getBlobJson(rs.getBlob("mstDeviceDependentBlob")));
//				actionForAgVO.setTxnDeviceDependentBlob(getBlobJson(rs.getBlob("txnDeviceDependentBlob")));
				actionForAgVO.setMstDeviceIndependentBlob(getBlobJson(rs.getBlob("mstDeviceIndependentBlob")));
				actionForAgVO.setTxnDeviceIndependentBlob(getBlobJson(rs.getBlob("txnDeviceIndependentBlob")));				
				canvasActionForAgVOList.add(actionForAgVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return canvasActionForAgVOList;
	}
	@Override
	public Map<String, Map<String, Map<String, CtAcDtArVO>>> getCtAcDtArByBT(String baseTemplateId) {
		Map<String, Map<String, Map<String, CtAcDtArVO>>> map = new HashMap<>();
		String SQL = "SELECT *\n" +
                "FROM T_UI_ADM_CT_AC_DT_ACTION_REPRESENTATION_TXN\n" +
                "WHERE BASE_TEMPLATE_CODE_FK_ID = ? AND RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while(rs.next()){
				CtAcDtArVO VO = new CtAcDtArVO();
				VO.setBaseTemplateId(convertByteToString(rs.getBytes("BASE_TEMPLATE_CODE_FK_ID")));
				VO.setCanvasTypeCode(rs.getString("CANVAS_TYPE_CODE_FK_ID"));
				VO.setActionCategory(rs.getString("ACTION_CATEGORY_CODE_FK_ID"));
				VO.setDeviceTypeCode(rs.getString("DEVICE_TYPE_CODE_FK_ID"));
				VO.setArCode(rs.getString("ACTION_REPRESENTATION_CODE_FK_ID"));
				VO.setArSeqNumber(rs.getInt("ACTION_CATEGORY_SEQUENCE_NUMBER_POS_INT"));
				if(map.containsKey(VO.getCanvasTypeCode())){
					Map<String,Map<String,CtAcDtArVO>> acDtArMap = map.get(VO.getCanvasTypeCode());
					if(acDtArMap.containsKey(VO.getActionCategory())){
						Map<String,CtAcDtArVO> dtArMap = acDtArMap.get(VO.getActionCategory());
						if(dtArMap.containsKey(VO.getDeviceTypeCode())){
							//here throw error because this entry should already be present
						}else{
							//That device type is not present in map
							dtArMap.put(VO.getDeviceTypeCode(), VO);
						}
					}else{
						//That action category is not present in map.
						Map<String,CtAcDtArVO> dtArMap = new HashMap<>();
						dtArMap.put(VO.getDeviceTypeCode(), VO);
						acDtArMap.put(VO.getActionCategory(), dtArMap);
					}
				}else{
					Map<String,Map<String,CtAcDtArVO>> acDtArMap = new HashMap<>();
					Map<String,CtAcDtArVO> dtArMap = new HashMap<>();
					dtArMap.put(VO.getDeviceTypeCode(), VO);
					acDtArMap.put(VO.getActionCategory(), dtArMap);
					map.put(VO.getCanvasTypeCode(), acDtArMap);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}
	@Override
	public JsonNode getBtPropertyBlob(String baseTemplateId) {
		String SQL =  "SELECT COLUMN_JSON(PROPERTY_BLOB) AS btPropertyBlob\n" +
                "FROM T_BT_ADM_BT_SPECIFIC_PROPERTY\n" +
                "WHERE BASE_TEMPLATE_CODE_FK_ID = ? AND RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		JsonNode result = null;
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			if(rs.next()){
				result = getBlobJson(rs.getBlob("btPropertyBlob"));
			}
		}catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.tapplent.platformutility.layout.dao.LayoutDAO#getIconMasterList()
	 */
	@Override
	public List<IconMasterVO> getIconMasterList() {
		List<IconMasterVO> icons = new ArrayList<>(); 
		String SQL =  "SELECT *\n" +
                "FROM T_UI_LKP_ICON_MASTER\n" +
                "WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				IconMasterVO iconMasterVO = new IconMasterVO();
				iconMasterVO.setIconCode(rs.getString("ICON_CODE_PK_ID"));
				iconMasterVO.setIconClass(rs.getString("ICON_CLASS_NAME_TXT"));
				iconMasterVO.setIconUnicode(rs.getString("ICON_UNICODE_TXT"));
				icons.add(iconMasterVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return icons;
	}
	/* (non-Javadoc)
	 * @see com.tapplent.platformutility.layout.dao.LayoutDAO#getCanvasMasterList(java.util.Map)
	 */
	@Override
	public List<CanvasMasterVO> getCanvasMasterList(Map<String, String> canvasMasterMap) {
		List<CanvasMasterVO> result = new ArrayList<>();
		/*
		 * SELECT *
			FROM T_UI_ADM_CANVAS_MASTER AS canvas,
			  T_UI_ADM_CANVAS_MASTER AS parentCanvas
			WHERE canvas.INTRNL_LEFT_POS_INT BETWEEN parentCanvas.INTRNL_LEFT_POS_INT AND parentCanvas.INTRNL_RIGHT_POS_INT
			      AND  canvas.INTRNL_HIERARCHY_ID = parentCanvas.INTRNL_HIERARCHY_ID
			      AND (parentCanvas.CANVAS_MASTER_PK_ID IN ( 0x41B261934B53A1398CE22DCC355CB4B2, 0x467FB0EF37ACDAA6869D329E736D15BB));
		 */
		StringBuilder SQL = new StringBuilder("SELECT\n" +
                "  *,\n" +
                "  COLUMN_JSON(canvas.DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob,\n" +
                "  COLUMN_JSON(canvas.DEVICE_DEPENDENT_BLOB)   AS deviceDependentBlob,\n" +
                "  COLUMN_JSON(canvas.DEVICE_APPLICABLE_BLOB)  AS deviceApplicableBlob\n" +
                "FROM T_UI_ADM_CANVAS_MASTER AS canvas,\n" +
                "  T_UI_ADM_CANVAS_MASTER AS parentCanvas\n" +
                "WHERE canvas.INTRNL_LEFT_POS_INT BETWEEN parentCanvas.INTRNL_LEFT_POS_INT AND parentCanvas.INTRNL_RIGHT_POS_INT\n" +
                "      AND canvas.INTRNL_HIERARCHY_ID = parentCanvas.INTRNL_HIERARCHY_ID\n" +
                "      AND canvas.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND parentCanvas.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n");
		List<Object> parameters = new ArrayList<>();
		if(!canvasMasterMap.isEmpty()){
			SQL.append("AND");
			SQL.append("parentCanvas.CANVAS_MASTER_PK_ID IN (");
			int count = 0;
			for(Map.Entry<String, String> entry : canvasMasterMap.entrySet()){
				String canvasMasterId = entry.getKey();
				if(count!=0)
					SQL.append(",");
				SQL.append("?");
				byte[] id = Util.convertStringIdToByteId(canvasMasterId);
				parameters.add(id);
                count++;
			}
			SQL.append(");");
		}
		ResultSet rs = executeSQL(SQL.toString(), parameters);
		try {
			while(rs.next()){
				CanvasMasterVO canvasMasterVO = new CanvasMasterVO();
				canvasMasterVO.setCanvasId(convertByteToString(rs.getBytes("canvas.CANVAS_MASTER_PK_ID")));
				canvasMasterVO.setParentCanvasId(convertByteToString(rs.getBytes("canvas.PARENT_CANVAS_MASTER_FK_ID")));
				canvasMasterVO.setCanvasTypeCode(rs.getString("canvas.CANVAS_TYPE_CODE_FK_ID"));
				canvasMasterVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
				canvasMasterVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
				canvasMasterVO.setDeviceApplicableBlob(getBlobJson(rs.getBlob("deviceApplicableBlob")));
				result.add(canvasMasterVO);
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.tapplent.platformutility.layout.dao.LayoutDAO#getControlMasterList(java.util.List)
	 */
	@Override
	public List<ControlMasterVO> getControlMasterList(List<String> allCanvasIdsList) {
		List<ControlMasterVO> result = new ArrayList<>();
		if(allCanvasIdsList!=null && !allCanvasIdsList.isEmpty()){
            StringBuilder SQL = new StringBuilder("SELECT\n" +
                    "  *,\n" +
                    "  COLUMN_JSON(DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob,\n" +
                    "  COLUMN_JSON(DEVICE_DEPENDENT_BLOB)   AS deviceDependentBlob\n" +
                    "FROM T_UI_ADM_CONTROL_MASTER\n" +
                    "WHERE CANVAS_MASTER_DEP_FK_ID IN (\n");
			List<Object> parameters = new ArrayList<>();
			int count = 0;
			for(String canvasId : allCanvasIdsList){
				if(count!= 0)
					SQL.append(",");
				SQL.append("?");
				byte[] id = Util.convertStringIdToByteId(canvasId);
				parameters.add(id);
                count++;
			}
			SQL.append(") AND T_UI_ADM_CONTROL_MASTER.RECORD_STATE_CODE_FK_ID = 'CURRENT'; ");
			ResultSet rs = executeSQL(SQL.toString(), parameters);
			try {
				while(rs.next()){
					ControlMasterVO controlMasterVO = new ControlMasterVO();
					controlMasterVO.setControlId(convertByteToString(rs.getBytes("CONTROL_MASTER_PK_ID")));
					controlMasterVO.setCanvasId(convertByteToString(rs.getBytes("CANVAS_MASTER_DEP_FK_ID")));
//					controlMasterVO.setControlAttribute(rs.getString(""));
//					controlMasterVO.setAggregateControlFunction(aggregateControlFunction);
					controlMasterVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
					controlMasterVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
					result.add(controlMasterVO);
				}
			} catch (SQLException | IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<PropertyControlMasterVO> getPropertyControlMasterList(List<String> allCanvasIdsList) {
		List<PropertyControlMasterVO> result = new ArrayList<>();
		if(allCanvasIdsList!= null && !allCanvasIdsList.isEmpty()){
			StringBuilder SQL = new StringBuilder("SELECT\n" +
					"  *,\n" +
					"  COLUMN_JSON(DEVICE_INDEPENDENT_BLOB) AS deviceIndependentBlob,\n" +
					"  COLUMN_JSON(DEVICE_DEPENDENT_BLOB)   AS deviceDependentBlob\n" +
					"FROM T_UI_ADM_PROPERTY_CONTROL_MASTER\n" +
					"WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT' AND\n" +
					"CANVAS_MASTER_FK_ID IN (");
			List<Object> parameters = new ArrayList<>();
			int count = 0;
			for(String canvasId : allCanvasIdsList){
				if(count!= 0)
					SQL.append(",");
				SQL.append("?");
				byte[] id = Util.convertStringIdToByteId(canvasId);
				parameters.add(id);
				count++;
			}
			SQL.append(");");
			ResultSet rs = executeSQL(SQL.toString(), parameters);
			try {
				while(rs.next()){
					PropertyControlMasterVO propertyControlMasterVO = new PropertyControlMasterVO();
					propertyControlMasterVO.setPropertyControlId(convertByteToString(rs.getBytes("PROPERTY_CONTROL_MASTER_PK_ID")));
					propertyControlMasterVO.setPropertyControlCode(rs.getString("PROPERTY_CONTROL_CODE_FK_ID"));
					propertyControlMasterVO.setCanvasId(convertByteToString(rs.getBytes("CANVAS_MASTER_FK_ID")));
					propertyControlMasterVO.setDeviceIndependentBlob(getBlobJson(rs.getBlob("deviceIndependentBlob")));
					propertyControlMasterVO.setDeviceDependentBlob(getBlobJson(rs.getBlob("deviceDependentBlob")));
					result.add(propertyControlMasterVO);
                }
			} catch (SQLException | IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<AppMainMenuVO> getAppMainMenuVOMap(String personId, String menuType, String deviceType) {
		List<AppMainMenuVO> AppMainMenuVOs = new ArrayList<>();
		StringBuilder SQL = new StringBuilder("SELECT *\n" +
				"FROM T_AHP_ADM_MENU\n" +
				"  INNER JOIN T_AHP_ADM_MENU_PERMISSION ON MENU_FK_ID = MENU_PK_ID AND PERMISSION_TYPE_CODE_FK_ID = 'APPLICABLE_GROUPS'\n" +
				"WHERE T_AHP_ADM_MENU.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND T_AHP_ADM_MENU.IS_DELETED = FALSE AND\n" +
				"      T_AHP_ADM_MENU.SAVE_STATE_CODE_FK_ID = 'SAVED' AND T_AHP_ADM_MENU.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND MENU_TYPE_CODE_FK_ID = ?");
		PersonGroup group = TenantAwareCache.getPersonRepository().getPersonGroups(TenantContextHolder.getUserContext().getPersonId());
		if(group.getGroupIds() != null && !group.getGroupIds().isEmpty()){
			SQL.append(" AND ");
			SQL.append("(");
			for(int i =0 ; i< group.getGroupIds().size(); i++){
				if(i!=0)
					SQL.append(" OR ");
				SQL.append("PERMISSION_VALUE_LNG_TXT like ");
				SQL.append("'%").append(group.getGroupIds().get(i)).append("%'");
			}
			SQL.append(")");
		}
		List<Object> parameters = new ArrayList<>();
        parameters.add(menuType);
		ResultSet rs = executeSQL(SQL.toString(), parameters);
		AppMainMenuVO appMainMenuVO =  null;
		try{
			while (rs.next()){
				/*
				MENU_PK_ID
MENU_ICON_CODE_FK_ID
MENU_NAME_G11N_BIG_TXT
PARENT_MENU_HCY_FK_ID
MENU_SEQUENCE_NUMBER_POS_INT
MENU_TYPE_CODE_FK_ID
IS_SHOWN_IN_EXPND_FORM
ICON_COLOUR_TXT
NOTES_G11N_BIG_TXT
INTRNL_HIERARCHY_ID
INTRNL_LEFT_POS_INT
INTRNL_RIGHT_POS_INT
				 */
				appMainMenuVO = new AppMainMenuVO();
				appMainMenuVO.setAppMainMenuPkId(Util.convertByteToString(rs.getBytes("MENU_PK_ID")));
//				appMainMenuVO.setLoggedInPersonFkId(Util.convertByteToString(rs.getBytes("LOGGED_IN_PERSON_FK_ID")));
//				appMainMenuVO.setAppMainMenuRuleFkId(Util.convertByteToString(rs.getBytes("APP_MAIN_MENU_RULE_FK_ID")));
				appMainMenuVO.setMenuIconCodeFkId(rs.getString("MENU_ICON_CODE_FK_ID"));
				appMainMenuVO.setMenuImage(Util.convertByteToString(rs.getBytes("MENU_IMAGEID")));
				appMainMenuVO.setMenuNameG11nBigTxt(getG11nValue(rs.getString("MENU_NAME_G11N_BIG_TXT")));
				try{
					appMainMenuVO.setParentMenuHcyFkId(Util.convertByteToString(rs.getBytes("SLF_PRNT_MENU_HCY_FK_ID")));
				}catch (Exception e){

				}
				try{
					appMainMenuVO.setParentMenuHcyFkId(Util.convertByteToString(rs.getBytes("PARENT_MENU_HCY_FK_ID")));
				}catch (Exception e){

				}
				appMainMenuVO.setShowInExpndForm(rs.getBoolean("IS_SHOWN_IN_EXPND_FORM"));
				appMainMenuVO.setMenuSequenceNumberPosInt(rs.getInt("MENU_SEQUENCE_NUMBER_POS_INT"));
				appMainMenuVO.setIconColourTxt(rs.getString("ICON_COLOUR_TXT"));
//				appMainMenuVO.setMenuExpressionBigTxt(rs.getString("MENU_EXPRESSION_BIG_TXT"));
//				appMainMenuVO.setDynamicActionArgBigTxt(rs.getString("DYNAMIC_ACTION_ARG_BIG_TXT"));
//				appMainMenuVO.setNotesG11nBigTxt(getG11nValue(rs.getString("NOTES_G11N_BIG_TXT")));
				appMainMenuVO.setIntrnlHierarchyId(Util.convertByteToString(rs.getBytes("INTRNL_HIERARCHY_ID")));
				appMainMenuVO.setIntrnlLeftPosInt(rs.getInt("INTRNL_LEFT_POS_INT"));
				appMainMenuVO.setIntrnlRightPosInt(rs.getInt("INTRNL_RIGHT_POS_INT"));
				appMainMenuVO.setAllowQuickAdd(rs.getBoolean("ALLOW_GWD_QUICK_ADD"));
//				appMainMenuVO.setIsDeleted(rs.getBoolean("IS_DELETED"));
				AppMainMenuVOs.add(appMainMenuVO);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return AppMainMenuVOs;
	}

//	@Override
//	public PersonPreferenceVO getPersonPreference(String personId) {
//		PersonPreferenceVO preferenceVO = new PersonPreferenceVO();
//		String SQL = "SELECT * FROM T_PRN_EU_PERSON_PREFERENCES WHERE PERSON_FK_ID = x?;";
//		List<Object> parameters = new ArrayList<>();
//		parameters.add(Util.remove0x(personId));
//		ResultSet rs = executeSQL(SQL, parameters);
//		try {
//			while(rs.next()){
//                preferenceVO.setPersonPreferencesPkId(Util.convertByteToString(rs.getBytes("PERSON_PREFERENCES_PK_ID")));
//                preferenceVO.setPersonFkId(Util.convertByteToString(rs.getBytes("PERSON_FK_ID")));
//				preferenceVO.setLocaleCodeFkId(rs.getString("LOCALE_CODE_FK_ID"));
//				preferenceVO.setLanguageDirectionCodeFkId(rs.getString("LANGUAGE_DIRECTION_CODE_FK_ID"));
//				preferenceVO.setLanguageFamilyCodeFkId(rs.getString("LANGUAGE_FAMILY_CODE_FK_ID"));
//				preferenceVO.setNumberFormatCodeFkId(rs.getString("NUMBER_FORMAT_CODE_FK_ID"));
//				preferenceVO.setDateFormatCodeFkId(rs.getString("DATE_FORMAT_CODE_FK_ID"));
//				preferenceVO.setTimeFormatCodeFkId(rs.getString("TIME_FORMAT_CODE_FK_ID"));
//				preferenceVO.setTimezoneCodeFkId(rs.getString("TIMEZONE_CODE_FK_ID"));
//				preferenceVO.setLocaleCurrencyCodeFkId(rs.getString("LOCALE_CURRENCY_CODE_FK_ID"));
//				preferenceVO.setLocaleRadixPointTxt(rs.getString("LOCALE_RADIX_POINT_TXT"));
//				preferenceVO.setLocaleThousandSeperatorTxt(rs.getString("LOCALE_THOUSAND_SEPARATOR_TXT"));
//				preferenceVO.setDefaultLandingPageTxt(Util.convertByteToString(rs.getBytes("DEFAULT_LANDING_SCREEN_INSTANCE_FK_ID")));
//            }
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return preferenceVO;
//	}

	@Override
	public List<ProcessHomePageFooterVO> getProcessHomePageLayoutFooter(String processTypeCode, String personId) {
        List<ProcessHomePageFooterVO> footerContentVOList = new ArrayList<>();
        String SQL = "SELECT * FROM T_PRC_IEU_FOOTER_CONTENT WHERE LOGGED_IN_PERSON_FK_ID = x? AND PROCESS_TYPE_CODE_FK_ID = ? AND IS_DELETED = 0;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(personId));
        parameters.add(processTypeCode);
        ResultSet rs = executeSQL(SQL, parameters);

        try{
            while (rs.next()){
                ProcessHomePageFooterVO footerContentVO =  new ProcessHomePageFooterVO();
                footerContentVO.setPrcFooterContentPkId(Util.convertByteToString(rs.getBytes("PRC_FOOTER_CONTENT_PK_ID")));
                footerContentVO.setLoggedInPersonPkId(Util.convertByteToString(rs.getBytes("LOGGED_IN_PERSON_FK_ID")));
                footerContentVO.setPrcFooterContentRuleFkId(Util.convertByteToString(rs.getBytes("PRC_FOOTER_CONTENT_RULE_FK_ID")));
                footerContentVO.setProcessTypeCodeFkId(rs.getString("PROCESS_TYPE_CODE_FK_ID"));
                footerContentVO.setFooterTextG11NBigTxt(getG11nValue(rs.getString("FOOTER_TEXT_G11N_BIG_TXT")));
                footerContentVO.setDeleted(rs.getBoolean("IS_DELETED"));
                footerContentVO.setLastModifiedDatetime(rs.getString("LAST_MODIFIED_DATETIME"));
                footerContentVOList.add(footerContentVO);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return footerContentVOList;
	}

	@Override
	public List<ProcessHomePageIntroContentVO> getProcessHomePageIntroContent(String processTypeCode, String personId) {
        List<ProcessHomePageIntroContentVO> introContentVOList = new ArrayList<>();
        String SQL = "SELECT * FROM T_PRC_IEU_INTRO_CONTENT WHERE LOGGED_IN_PERSON_FK_ID = x? AND PROCESS_TYPE_CODE_FK_ID = ? AND IS_DELETED = 0;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(personId));
        parameters.add(processTypeCode);
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while (rs.next()){
                /*
                PRC_INTRO_CONTENT_PK_ID
                LOGGED_IN_PERSON_PK_ID
                PRC_INTRO_CONTENT_RULE_FK_ID
                PROCESS_TYPE_CODE_FK_ID
                INTRO_IMAGEID
                INTRO_TITLE_G11N_BIG_TXT
                INTRO_SUB_TEXT_G11N_BIG_TXT
                LINKED_STATIC_EXT_URL_TXT
                LINKED_RESOLVED_TARGET_SCREEN_INSTANCE_FK_ID
                LINKED_RESOLVED_TARGET_SCREEN_SECTION_INSTANCE_FK_ID
                LINKED_RESOLVED_TARGET_TAB_SEQ_NUM_POS_INT
                LINKED_DYNAMIC_ACTION_FILTER_EXPN
                IS_DELETED
                LAST_MODIFIED_DATETIME
                 */
                ProcessHomePageIntroContentVO introContentVO = new ProcessHomePageIntroContentVO();
                introContentVO.setPrcIntroContentPkId(Util.convertByteToString(rs.getBytes("PRC_INTRO_CONTENT_PK_ID")));
                introContentVO.setLoggedInPersonPkId(Util.convertByteToString(rs.getBytes("LOGGED_IN_PERSON_FK_ID")));
                introContentVO.setPrcIntroContentRuleFkId(Util.convertByteToString(rs.getBytes("PRC_INTRO_CONTENT_RULE_FK_ID")));
                introContentVO.setProcessTypeCodeFkId(rs.getString("PROCESS_TYPE_CODE_FK_ID"));
                introContentVO.setIntroImageId(Util.convertByteToString(rs.getBytes("INTRO_IMAGEID")));
                introContentVO.setIntroTitleG11nBigTxt(getG11nValue(rs.getString("INTRO_TITLE_G11N_BIG_TXT")));
                introContentVO.setIntroSubTextG11nBigTxt(getG11nValue(rs.getString("INTRO_SUB_TEXT_G11N_BIG_TXT")));
                introContentVO.setLinkedStaticExtUrlTxt(rs.getString("LINKED_STATIC_EXT_URL_TXT"));
                introContentVO.setLinkedResolvedTargetScreenInstanceFkId(Util.convertByteToString(rs.getBytes("LINKED_RESOLVED_TARGET_SCREEN_INSTANCE_FK_ID")));
                introContentVO.setLinkedResolvedTargetScreenSectionInstanceFkId(Util.convertByteToString(rs.getBytes("LINKED_RESOLVED_TARGET_SCREEN_SECTION_INSTANCE_FK_ID")));
                introContentVO.setLinkedResolvedTargetTabSeqNumPosInt(rs.getInt("LINKED_RESOLVED_TARGET_TAB_SEQ_NUM_POS_INT"));
                introContentVO.setLinkedDynamicActionFilterExpn(rs.getString("LINKED_DYNAMIC_ACTION_FILTER_EXPN"));
                introContentVO.setDeleted(rs.getBoolean("IS_DELETED"));
                introContentVO.setLastModifiedDatetime(rs.getDate("LAST_MODIFIED_DATETIME"));
                introContentVOList.add(introContentVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return introContentVOList;
	}

    @Override
    public List<ProcessHomePageVO> getProcessHomePage(String processTypeCode, String personId) {
        List<ProcessHomePageVO> ProcessHomePageVOs = new ArrayList<>();
        String SQL = "SELECT * FROM T_PRC_IEU_PRC_HOME_PAGE WHERE LOGGED_IN_PERSON_FK_ID = x? AND PROCESS_TYPE_CODE_FK_ID = ? AND IS_DELETED = 0;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(personId));
        parameters.add(processTypeCode);
        ResultSet rs = executeSQL(SQL, parameters);
        try{
            while (rs.next()){
                ProcessHomePageVO processHomePageVO =  new ProcessHomePageVO();
                processHomePageVO.setPrcHomePagePkId(Util.convertByteToString(rs.getBytes("PRC_HOME_PAGE_PK_ID")));
                processHomePageVO.setLoggedInPersonFkId(Util.convertByteToString(rs.getBytes("LOGGED_IN_PERSON_FK_ID")));
                processHomePageVO.setPrcHomePageRuleFkId(Util.convertByteToString(rs.getBytes("PRC_HOME_PAGE_RULE_FK_ID")));
                processHomePageVO.setProcessTypeCodeFkId(rs.getString("PROCESS_TYPE_CODE_FK_ID"));
                processHomePageVO.setCategoryTitleG11nBigTxt(getG11nValue(rs.getString("CATEGORY_TITLE_G11N_BIG_TXT")));
                processHomePageVO.setCategoryIconCodeFkId(rs.getString("CATEGORY_ICON_CODE_FK_ID"));
                processHomePageVO.setCardTypeCodeFkId(rs.getString("CARD_TYPE_CODE_FK_ID"));
                processHomePageVO.setCardIconCodeFkId(rs.getString("CARD_ICON_CODE_FK_ID"));
                processHomePageVO.setCardTitleG11nBigTxt(getG11nValue(rs.getString("CARD_TITLE_G11N_BIG_TXT")));
                processHomePageVO.setCardImageid(Util.convertByteToString(rs.getBytes("CARD_IMAGEID")));
                processHomePageVO.setFrontCard(rs.getBoolean("IS_FRONT_CARD"));
                processHomePageVO.setCardFlippable(rs.getBoolean("IS_CARD_FLIPPABLE"));
                processHomePageVO.setFlipCardPrcHomePageFkId(Util.convertByteToString(rs.getBytes("FLIP_CARD_PRC_HOME_PAGE_FK_ID")));
                processHomePageVO.setDisplaySeqNumPosInt(rs.getInt("DISPLAY_SEQ_NUM_POS_INT"));
                processHomePageVO.setResolvedTargetScreenInstanceFkId(Util.convertByteToString(rs.getBytes("RESOLVED_TARGET_SCREEN_INSTANCE_FK_ID")));
                processHomePageVO.setResolvedTargetScreenSectionInstanceFkId(Util.convertByteToString(rs.getBytes("RESOLVED_TARGET_SCREEN_SECTION_INSTANCE_FK_ID")));
                processHomePageVO.setResolvedTargetTabSeqNumPosInt(rs.getInt("RESOLVED_TARGET_TAB_SEQ_NUM_POS_INT"));
                processHomePageVO.setDynamicActionFilterExpn(rs.getString("DYNAMIC_ACTION_FILTER_EXPN"));
                processHomePageVO.setDeleted(rs.getBoolean("IS_DELETED"));
                ProcessHomePageVOs.add(processHomePageVO);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return ProcessHomePageVOs;
    }

    @Override
    public Map<String, List<ProcessHomePageContentVO>> getProcessHomePageContentVO(String processTypeCode, String personId) {
        Map<String, List<ProcessHomePageContentVO>> homePageToContentListMap = new HashMap<>();
        String SQL = "SELECT * FROM T_PRC_IEU_PRC_HOME_PAGE_CONTENT INNER JOIN T_PRC_IEU_PRC_HOME_PAGE ON PRC_HOME_PAGE_FK_ID = PRC_HOME_PAGE_PK_ID WHERE LOGGED_IN_PERSON_FK_ID = x? AND PROCESS_TYPE_CODE_FK_ID = ? AND T_PRC_IEU_PRC_HOME_PAGE.IS_DELETED = 0 AND T_PRC_IEU_PRC_HOME_PAGE_CONTENT.IS_DELETED = 0;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(personId));
        parameters.add(processTypeCode);
        ResultSet rs = executeSQL(SQL, parameters);
        try{
            while (rs.next()){
                ProcessHomePageContentVO processHomePageContentVO =  new ProcessHomePageContentVO();
                processHomePageContentVO.setPrcHomePageContentPkId(Util.convertByteToString(rs.getBytes("PRC_HOME_PAGE_CONTENT_PK_ID")));
                processHomePageContentVO.setPrcHomePageFkId(Util.convertByteToString(rs.getBytes("PRC_HOME_PAGE_FK_ID")));
                processHomePageContentVO.setCardSectionTypeCodeFkId(rs.getString("CARD_SECTION_TYPE_CODE_FK_ID"));
                processHomePageContentVO.setCardSectionInstancePosInt(rs.getInt("CARD_SECTION_INSTANCE_POS_INT"));
                processHomePageContentVO.setRecordNumberPosInt(rs.getInt("RECORD_NUMBER_POS_INT"));
                processHomePageContentVO.setCardSectionControlTypeCodeFkId(rs.getString("CARD_SECTION_CONTROL_TYPE_CODE_FK_ID"));
				processHomePageContentVO.setControlType(rs.getString("CONTROL_CONTENT_TYPE_CODE_FK_ID"));
                processHomePageContentVO.setControlContentValueG11nBigTxt(getG11nValue(rs.getString("CONTROL_CONTENT_VALUE_G11N_BIG_TXT")));
                processHomePageContentVO.setContentReferencePrimaryKeyId(Util.convertByteToString(rs.getBytes("CONTENT_REFERENCE_PRIMARY_KEY_ID")));
                processHomePageContentVO.setParentRecordNumberPosInt(rs.getInt("PARENT_RECORD_NUMBER_POS_INT"));
                processHomePageContentVO.setParentCardSectionInstancePosInt(rs.getInt("PARENT_CARD_SECTION_INSTANCE_POS_INT"));
                processHomePageContentVO.setDeleted(rs.getBoolean("IS_DELETED"));
                if(homePageToContentListMap.containsKey(processHomePageContentVO.getPrcHomePageFkId())){
                    homePageToContentListMap.get(processHomePageContentVO.getPrcHomePageFkId()).add(processHomePageContentVO);
                }else{
                    List<ProcessHomePageContentVO> processHomePageContentVOList = new ArrayList<>();
                    processHomePageContentVOList.add(processHomePageContentVO);
                    homePageToContentListMap.put(processHomePageContentVO.getPrcHomePageFkId(),processHomePageContentVOList);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return homePageToContentListMap;
    }

    @Override
    public List<ThemeVO> getThemeVOs(String personId, String themeHeaderFkId) {
        List<ThemeVO> themeVOList = new ArrayList<>();
        String SQL = "SELECT * FROM T_UI_ADM_THEME;";
        List<Object> parameters = new ArrayList<>();
//        parameters.add(Util.remove0x(personId));
//		parameters.add(Util.remove0x(themeHeaderFkId));
        ResultSet rs = executeSQL(SQL, parameters);
        try{
            while (rs.next()){
                ThemeVO themeVO = new ThemeVO();
                themeVO.setThemeId(Util.convertByteToString(rs.getBytes("THEME_PK_ID")));
                themeVO.setThemeHeaderFkId(Util.convertByteToString(rs.getBytes("THEME_HEADER_FK_ID")));
                themeVO.setThemeGroupSeqNumPosInt(rs.getInt("THEME_GROUP_SEQ_NUM_POS_INT"));
                themeVO.setThemeStateCodeFkId(rs.getString("THEME_STATE_CODE_FK_ID"));
                themeVO.setBackgroundColour1Txt(rs.getString("BACKGROUND_COLOUR_1_TXT"));
                themeVO.setBackgroundColour2Txt(rs.getString("BACKGROUND_COLOUR_2_TXT"));
                themeVO.setBackgroundColour3Txt(rs.getString("BACKGROUND_COLOUR_3_TXT"));
                themeVO.setBackgroundColour4Txt(rs.getString("BACKGROUND_COLOUR_4_TXT"));
                themeVO.setBackgroundColour5Txt(rs.getString("BACKGROUND_COLOUR_5_TXT"));
                themeVO.setBackgroundColourGradientWidth1PosDec(rs.getFloat("BACKGROUND_COLOUR_GRADIENT_WIDTH_1_POS_DEC"));
                themeVO.setBackgroundColourGradientWidth2PosDec(rs.getFloat("BACKGROUND_COLOUR_GRADIENT_WIDTH_2_POS_DEC"));
                themeVO.setBackgroundColourGradientWidth3PosDec(rs.getFloat("BACKGROUND_COLOUR_GRADIENT_WIDTH_3_POS_DEC"));
                themeVO.setBackgroundColourGradientWidth4PosDec(rs.getFloat("BACKGROUND_COLOUR_GRADIENT_WIDTH_4_POS_DEC"));
                themeVO.setBackgroundColourGradientWidth5PosDec(rs.getFloat("BACKGROUND_COLOUR_GRADIENT_WIDTH_5_POS_DEC"));
                themeVO.setBackgroundImageId(rs.getString("BACK_GROUND_IMAGEID"));
                themeVO.setBackgroundImageGradientAnglePosDec(rs.getFloat("BACK_GROUND_IMAGE_GRADIENT_ANGLE_POS_DEC"));
                themeVO.setFontFamilyCodeFkId(rs.getString("FONT_FAMILY_CODE_FK_ID"));
                themeVO.setFontColourPrimaryTxt(rs.getString("FONT_COLOUR_PRIMARY_TXT"));
				themeVO.setFontColourSecondaryTxt(rs.getString("FONT_COLOUR_SECONDARY_TXT"));
				themeVO.setFontColourTertiaryTxt(rs.getString("FONT_COLOUR_TERTIARY_TXT"));
                themeVOList.add(themeVO);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return themeVOList;
    }

	@Override
	public List<PersonTagsVO> getPersonTagsVoList(String personId) {
		List<PersonTagsVO> personTagsVOList = new ArrayList<>();
		String SQL = "SELECT *\n" +
				"FROM T_PFM_IEU_PERSON_TAG_MASTER WHERE LOGGED_IN_USER_FK_ID = x? AND IS_DELETED = 0;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(Util.remove0x(personId));
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while (rs.next()){
				PersonTagsVO personTagsVO = new PersonTagsVO();
				personTagsVO.setPersonTagsPkId(Util.convertByteToString(rs.getBytes("PERSON_TAG_MASTER_PK_ID")));
				personTagsVO.setBackgroundColourTxt(rs.getString("BACKGROUND_COLOUR_TXT"));
				personTagsVO.setFontColourTxt(rs.getString("FONT_COLOUR_TXT"));
				personTagsVO.setLoggedInUserFkId(Util.convertByteToString(rs.getBytes("LOGGED_IN_USER_FK_ID")));
				personTagsVO.setLastModifiedDateTime(Util.getStringTobeInserted("DATE_TIME", rs.getTimestamp("LAST_MODIFIED_DATETIME")).replace("'",""));
				personTagsVO.setTagTxt(rs.getString("TAG_TXT"));
				personTagsVOList.add(personTagsVO);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return personTagsVOList;
	}

	@Override
	public List<SocialStatusVO> getSocialStatusVOList() {
		List<SocialStatusVO> socialStatusVOList = new ArrayList<>();
		String SQL = "SELECT * FROM T_PRN_LKP_SOCIAL_STATUS WHERE IS_DELETED = 0 AND RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while (rs.next()){
				SocialStatusVO socialStatusVO = new SocialStatusVO();
				socialStatusVO.setSocialStatusCodePkId(rs.getString("SOCIAL_STATUS_CODE_PK_ID"));
				socialStatusVO.setSocialStatusNameG11nBigTxt(getG11nValue(rs.getString("SOCIAL_STATUS_NAME_G11N_BIG_TXT")));
				socialStatusVO.setSocialStatusIconCodeFKId(rs.getString("SOCIAL_STATUS_ICON_CODE_FK_ID"));
				socialStatusVO.setOverrideFontColourTxt(rs.getString("OVERRIDE_FONT_COLOUR_TXT"));
				socialStatusVOList.add(socialStatusVO);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return socialStatusVOList;
	}

	@Override
	public List<LicencedLocalesVO> getLicencedLocale() {
		List<LicencedLocalesVO> result = new ArrayList<>();
		String SQL = "SELECT LOCALE_CODE_PK_ID, LANGUAGE_FAMILY_CODE_FK_ID, LOCALE_NAME_G11N_BIG_TXT, IS_GLOBAL_DFLT FROM T_BT_TAP_LICENSED_LOCALE INNER  JOIn T_SYS_LKP_LOCALE ON LOCALE_CODE_FK_ID = LOCALE_CODE_PK_ID WHERE T_BT_TAP_LICENSED_LOCALE.IS_DELETED = 0;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while (rs.next()){
				LicencedLocalesVO licencedLocalesVO = new LicencedLocalesVO();
				licencedLocalesVO.setLocale(rs.getString("LOCALE_CODE_PK_ID"));
				licencedLocalesVO.setLocaleName(getG11nValue(rs.getString("LOCALE_NAME_G11N_BIG_TXT")));
				licencedLocalesVO.setLanguageCode(rs.getString("LANGUAGE_FAMILY_CODE_FK_ID"));
				licencedLocalesVO.setGlobalDefault(rs.getBoolean("IS_GLOBAL_DFLT"));
				result.add(licencedLocalesVO);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<AppHomePageVO> getAppHomePageList() {
		List<AppHomePageVO> result = new ArrayList<>();
		String SQL = "SELECT * FROM T_AHP_IEU_APP_HOME_PAGE WHERE LOGGED_IN_PERSON_FK_ID = x?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(Util.remove0x(TenantContextHolder.getUserContext().getPersonId()));
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()){
				AppHomePageVO appHomePageVO = new AppHomePageVO();
				appHomePageVO.setAppHomePagePkId(Util.convertByteToString(rs.getBytes("APP_HOME_PAGE_PK_ID")));
				appHomePageVO.setLoggedInPersonFkId(Util.convertByteToString(rs.getBytes("LOGGED_IN_PERSON_FK_ID")));
				appHomePageVO.setAppHomePageRuleFkId(Util.convertByteToString(rs.getBytes("APP_HOME_PAGE_RULE_FK_ID")));
				appHomePageVO.setCategoryTitleG11nBigTxt(getG11nValue(rs.getString("CATEGORY_TITLE_G11N_BIG_TXT")));
				appHomePageVO.setCategoryIconCodeFkId(rs.getString("CATEGORY_ICON_CODE_FK_ID"));
				appHomePageVO.setGridItemNameTxt(rs.getString("GRID_ITEM_NAME_TXT"));
				appHomePageVO.setCardFlippable(rs.getBoolean("IS_CARD_FLIPPABLE"));
				appHomePageVO.setDisplaySeqNumPosInt(rs.getInt("DISPLAY_SEQ_NUM_POS_INT"));
				appHomePageVO.setDynamicActionArgBigTxt(rs.getString("DYNAMIC_ACTION_ARG_BIG_TXT"));
				appHomePageVO.setDynamicActionFilterExpn(rs.getString("DYNAMIC_ACTION_FILTER_EXPN"));
				result.add(appHomePageVO);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<AppHomePageSectionVO> getAppHomePageSectionList() {
		List<AppHomePageSectionVO> result = new ArrayList<>();
		String SQL = "SELECT * FROM T_AHP_IEU_APP_HOME_PAGE_SECN INNER JOIN T_AHP_IEU_APP_HOME_PAGE ON APP_HOME_PAGE_FK_ID = APP_HOME_PAGE_PK_ID WHERE LOGGED_IN_PERSON_FK_ID = x?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(Util.remove0x(TenantContextHolder.getUserContext().getPersonId()));
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()){
				AppHomePageSectionVO appHomePageSectionVO = new AppHomePageSectionVO();
				appHomePageSectionVO.setAppHomePageSecnPkId(Util.convertByteToString(rs.getBytes("APP_HOME_PAGE_SECN_PK_ID")));
				appHomePageSectionVO.setAppHomePageFkId(Util.convertByteToString(rs.getBytes("APP_HOME_PAGE_FK_ID")));
				appHomePageSectionVO.setSecnMasterCodeFkId(rs.getString("SECN_MASTER_CODE_FK_ID"));
				appHomePageSectionVO.setParentSectionMasterCodeFkId(rs.getString("PARENT_SECTION_MASTER_CODE_FK_ID"));
				appHomePageSectionVO.setSectionUiTypePosInt(rs.getInt("SECTION_UI_TYPE_POS_INT"));
				appHomePageSectionVO.setThemeSeqNumPosInt(rs.getInt("THEME_SEQ_NUM_POS_INT"));
				result.add(appHomePageSectionVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<AppHomePageSecnContentVO> appHomePageSecnContentVOList() {
		List<AppHomePageSecnContentVO> result = new ArrayList<>();
		String SQL = "SELECT * FROM T_AHP_IEU_APP_HOME_PAGE_SECN_CONTENT INNER JOIN T_AHP_IEU_APP_HOME_PAGE_SECN ON APP_HOME_PAGE_SECN_FK_ID = APP_HOME_PAGE_SECN_PK_ID INNER JOIN T_AHP_IEU_APP_HOME_PAGE ON APP_HOME_PAGE_FK_ID = APP_HOME_PAGE_PK_ID WHERE LOGGED_IN_PERSON_FK_ID = x?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(Util.remove0x(TenantContextHolder.getUserContext().getPersonId()));
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()){
				AppHomePageSecnContentVO appHomePageSecnContentVO = new AppHomePageSecnContentVO();
				appHomePageSecnContentVO.setAppHomepageSecnContentPkId(Util.convertByteToString(rs.getBytes("APP_HOME_PAGE_SECN_CONTENT_PK_ID")));
				appHomePageSecnContentVO.setAppHomePageSecnFkId(Util.convertByteToString(rs.getBytes("APP_HOME_PAGE_SECN_FK_ID")));
				appHomePageSecnContentVO.setControlContentTypeCodeFkId(rs.getString("CONTROL_CONTENT_TYPE_CODE_FK_ID"));
				appHomePageSecnContentVO.setControlSeqNumPosInt(rs.getInt("CONTROL_SEQ_NUM_POS_INT"));
				appHomePageSecnContentVO.setControlValueG11nBigTxt(getG11nValue(rs.getString("CONTROL_VALUE_G11N_BIG_TXT")));
				result.add(appHomePageSecnContentVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<WishTemplateVO> getwishTemplatesVO(String personPreferencesPkId) {
		List<WishTemplateVO> result = new ArrayList<>();
		String SQL = "SELECT * FROM T_PRN_EU_WISH_TMPLT_PREF WHERE PERSON_DEP_FK_ID=x?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(Util.remove0x(personPreferencesPkId));
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()){
				WishTemplateVO wishTemplateVO = new WishTemplateVO();
				wishTemplateVO.setWishTemplateID(Util.convertByteToString(rs.getBytes("PERSON_WSH_TMPLT_PREF_PK_ID")));
				wishTemplateVO.setPreferencesID(Util.convertByteToString(rs.getBytes("PERSON_DEP_FK_ID")));
				wishTemplateVO.setWishTemplateType(Util.convertByteToString(rs.getBytes("WSH_TMPLT_FK_ID")));
				wishTemplateVO.setMessageBody(rs.getString("WISH_MSG_BDY_BIG_TXT"));
				wishTemplateVO.setMessageHeader(rs.getString("WISH_MSG_HDR_BIG_TXT"));
				wishTemplateVO.setMessageFooter(rs.getString("WISH_MSG_FTR_BIG_TXT"));
				result.add(wishTemplateVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 *
	 * @param input
	 * @return
	 */
	private String convertByteToString(byte[] input){
		if(input == null)
			return null;
		return Util.convertByteToString(input);
	}
	private JsonNode getBlobJson(Blob input) throws IOException, SQLException{
		return Util.getBlobJson(input);
	}
	private String getG11nValue(String input){
		String result = Util.getG11nValue(input, null);
		if(StringUtil.isDefined(input) && !StringUtil.isDefined(result)){
			return input;
		}else{
			return result;
		}
	}
	/*This method retrieve the all the active images from the table-'T_UI_ADM_VIZ_ARTEFACTS_LIB'

	 */
	@Override
	public List<VisualArtefactLibraryVO> getImagesFromVisualArtefactLibrary(String libType) {
		List<VisualArtefactLibraryVO> visualArtefactLibraryVOList =new ArrayList<>();
		String SQL = "SELECT * FROM T_UI_ADM_VIZ_ARTEFACTS_LIB WHERE VIZ_ARTEFACT_LIB_TYPE_FK_ID = x?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(Util.remove0x(libType));
		ResultSet rs = executeSQL(SQL,parameters);
		try {
			while (rs.next()){
					VisualArtefactLibraryVO visualArtefactLibraryVO = new VisualArtefactLibraryVO();
					visualArtefactLibraryVO.setArtefactImageid(Util.convertByteToString(rs.getBytes("ARTEFACT_IMAGEID")));
					visualArtefactLibraryVO.setVizArtefactNameG11nBigTxt(Util.getG11nValue(rs.getString("VIZ_ARTEFACT_NAME_G11N_BIG_TXT"),null));
					visualArtefactLibraryVO.setDeleted(rs.getBoolean("IS_DELETED"));
					visualArtefactLibraryVO.setActiveStateCodeFkId(rs.getString("ACTIVE_STATE_CODE_FK_ID"));
					visualArtefactLibraryVO.setRecordStateCodeFkId(rs.getString("RECORD_STATE_CODE_FK_ID"));
					visualArtefactLibraryVO.setSaveStateCodeFkId(rs.getString("SAVE_STATE_CODE_FK_ID"));
					visualArtefactLibraryVOList.add(visualArtefactLibraryVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return visualArtefactLibraryVOList;
	}

	@Override
	public RatingLevelVO getRatingLevel(String ratingScaleId, double anchor) {
		String SQL = "SELECT * FROM T_PFM_ADM_RATING_LEVEL WHERE RATING_SCALE_FK_ID = x? AND RATING_LEVEL_ANCHOR_SCORE_NEG_DEC = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(Util.remove0x(ratingScaleId));
		parameters.add(anchor);
		ResultSet rs = executeSQL(SQL, parameters);
		RatingLevelVO ratingLevelVO = null;
		try {
			if (rs.next()) {
				ratingLevelVO = new RatingLevelVO();
				ratingLevelVO.setRatingLevelPkId(Util.convertByteToString(rs.getBytes("RATING_LEVEL_PK_ID")));
				ratingLevelVO.setRatingLevelName(Util.getG11nValue(rs.getString("RATING_LEVEL_NAME_G11N_BIG_TXT"), null));
				ratingLevelVO.setRatingLevelAnchorScore(rs.getDouble("RATING_LEVEL_ANCHOR_SCORE_NEG_DEC"));
				ratingLevelVO.setRatingLevelAverageScore(rs.getDouble("RATING_LEVEL_AVERAGE_SCORE_NEG_DEC"));
				ratingLevelVO.setRatingLevelMinimumScore(rs.getDouble("RATING_LEVEL_MINIMUM_SCORE_NEG_DEC"));
				ratingLevelVO.setRatingLevelMaximumScore(rs.getDouble("RATING_LEVEL_MAXIMUM_SCORE_NEG_DEC"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ratingLevelVO;
	}

	@Override
	public String calculateName(String formula) {
		List<Object> params = new ArrayList<>();
		ResultSet rs = executeSQL(formula, params);
		String name = null;
		try {
			if (rs.next()) {
				name = rs.getString("NAME");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return name;
	}

	@Override
	public List<Tutorial> getPersonTutorials(String personId, String tutorialCatId) {
		List<Tutorial> personTutorialList =new ArrayList<>();
		String SQL = "SELECT C.ICON_CODE_PK_ID,C.ICON_CLASS_NAME_TXT,C.ICON_UNICODE_TXT,A.TUTORIAL_CAT_FK_ID,A.TUTORIAL_VIDEO,A.TUTORIAL_ATTACH,A.TUTORIAL_TITLE_G11N_BIG_TXT,A.TUTORIAL_DESC_G11N_BIG_TXT,A.TUTORIAL_AUDIENCE_G11N_BIG_TXT,A.SEQ_NUM_POS_INT,A.IS_DELETED,A.ACTIVE_STATE_CODE_FK_ID,A.RECORD_STATE_CODE_FK_ID,A.SAVE_STATE_CODE_FK_ID FROM T_AHP_ADM_TUTORIAL AS A INNER JOIN T_AHP_ADM_TUTORIAL_CAT AS B ON A.TUTORIAL_CAT_FK_ID =B.TUTORIAL_CAT_PK_ID INNER JOIN T_UI_LKP_ICON_MASTER AS C ON A.TUTORIAL_DEVICE_ICON_CODE_FK_ID =C.ICON_CODE_PK_ID WHERE B.TUTORIAL_CAT_PK_ID = x? AND A.WHO_GROUP_FK_ID IN (SELECT DISTINCT PFM_GROUP_FK_ID FROM T_PFM_IEU_GROUP_MEMBER WHERE GROUP_MEMBER_PERSON_FK_ID = x?);";
		List<Object> parameters = new ArrayList<>();
		parameters.add(Util.remove0x(tutorialCatId));
		parameters.add(Util.remove0x(personId));
		ResultSet rs = executeSQL(SQL,parameters);
		try {
			while (rs.next()){
				Tutorial tutorial = new Tutorial();
				tutorial.setCategory(Util.convertByteToString(rs.getBytes("A.TUTORIAL_CAT_FK_ID")));
				tutorial.setTutorialVideoId(Util.convertByteToString(rs.getBytes("A.TUTORIAL_VIDEO")));
				tutorial.setTutorialAttachId(Util.convertByteToString(rs.getBytes("A.TUTORIAL_ATTACH")));
				tutorial.setTutorialTitleG11NTxt(Util.getG11nValue(rs.getString("A.TUTORIAL_TITLE_G11N_BIG_TXT"),null));
				tutorial.setTutotialDescriptionG11NTxt(Util.getG11nValue(rs.getString("A.TUTORIAL_DESC_G11N_BIG_TXT"),null));
				tutorial.setTutorialAudienceG11nTxt(Util.getG11nValue(rs.getString("A.TUTORIAL_AUDIENCE_G11N_BIG_TXT"),null));
				IconMasterVO iconDetails = new IconMasterVO();
				iconDetails.setIconCode(rs.getString("C.ICON_CODE_PK_ID"));
				iconDetails.setIconClass(rs.getString("C.ICON_CLASS_NAME_TXT"));
				iconDetails.setIconUnicode(rs.getString("C.ICON_UNICODE_TXT"));
				tutorial.setDeviceIcon(iconDetails);
				tutorial.setSeqNumber(rs.getInt("A.SEQ_NUM_POS_INT"));
				tutorial.setDeleted(rs.getBoolean("A.IS_DELETED"));
				tutorial.setActiveStateCodeFkId(rs.getString("A.ACTIVE_STATE_CODE_FK_ID"));
				tutorial.setRecordStateCodeFkId(rs.getString("A.RECORD_STATE_CODE_FK_ID"));
				tutorial.setSaveStateCodeFkId(rs.getString("A.SAVE_STATE_CODE_FK_ID"));
				personTutorialList.add(tutorial);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return personTutorialList;
	}
	@Override
	public Map<String, ActivityLog> getActivityLog(String versionId) {
		Map<String, ActivityLog> activityLogs =new LinkedHashMap<>();
		String SQL = "SELECT * FROM T_PFM_IEU_ACTIVITY_LOG JOIN T_PRN_EU_PERSON ON PERSON_PK_ID = ACT_AUTH_PERSON_FK_ID WHERE DO_ROW_VERSION_FK_ID = x? ORDER BY ACTIVITY_DATETIME DESC";
		List<Object> parameters = new ArrayList<>();
		parameters.add(Util.remove0x(versionId));
		ResultSet rs = executeSQL(SQL,parameters);
		try {
			while (rs.next()){
				ActivityLog activityLog = new ActivityLog();
				activityLog.setActivityLogPkId(Util.convertByteToString(rs.getBytes("ACTIVITY_LOG_PK_ID")));
				activityLog.setActivityAuthorPerson(Util.convertByteToString(rs.getBytes("ACT_AUTH_PERSON_FK_ID")));
				activityLog.setMtPeCodeFkId(rs.getString("MT_PE_CODE_FK_ID"));
				activityLog.setDoRowPrimaryKeyFkId(Util.convertByteToString(rs.getBytes("DO_ROW_PRIMARY_KEY_FK_ID")));
				activityLog.setDoRowVersionFkId(Util.convertByteToString(rs.getBytes("DO_ROW_VERSION_FK_ID")));
				activityLog.setDisplayContextValueBigTxt(Util.getG11nValue(rs.getString("DISPLAY_CONTEXT_VALUE_G11N_BIG_TXT"),null));
				activityLog.setDeleted(rs.getBoolean("IS_DELETED"));
				activityLog.setDoNameG11nBigTxt(Util.getG11nValue(rs.getString("BT_DO_NAME_G11N_BIG_TXT"),null));
				activityLog.setActionVisualMasterCode(rs.getString("ACTION_VISUAL_MASTER_CODE_FK_ID"));
				activityLog.setMsgYouInContextG11nBigTxt(Util.getG11nValue(rs.getString("MSG_SYNTAX_YOU_IN_CONTEXT_G11N_BIG_TXT"),null));
				activityLog.setMsgYouOutOfContextG11nBigTxt(Util.getG11nValue(rs.getString("MSG_SYN_YOU_OUT_OF_CONTEXT_G11N_BIG_TXT"),null));
				activityLog.setMsgThirdPersonInContextG11nBigTxt(Util.getG11nValue(rs.getString("MSG_SYNTAX_THIRD_PRSN_IN_CONTEXT_G11N_BIG_TXT"),null));
				activityLog.setMsgThirdPersonOutOfContextG11nBigTxt(Util.getG11nValue(rs.getString("MSG_SYNTAX_THIRD_PRSN_OUT_OF_CONTEXT_G11N_BIG_TXT"),null));
				activityLog.setDoIconCodeFkId(rs.getString("BT_DO_ICON_CODE_FK_ID"));
				activityLog.setActivityDatetime(rs.getTimestamp("ACTIVITY_DATETIME"));
				if (activityLog.getActivityDatetime() != null)
					try {
						activityLog.setActivityDatetimeString(Util.formatDatetimeString(activityLog.getActivityDatetime()));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				if (activityLog.getDoIconCodeFkId() != null)
					activityLog.setDoIconCode(Util.getIcon(activityLog.getDoIconCodeFkId()));
				activityLog.setPersonName(Util.getG11nValue(rs.getString("FORMAL_FULL_NAME_G11N_BIG_TXT"), null));
				activityLog.setPersonPhoto(Util.preSignedGetUrl(Util.convertByteToString(rs.getBytes("PHOTO_IMAGEID")), null));
				activityLog.setPersonInitials(rs.getString("NAME_INITIALS_TXT"));
				activityLogs.put(activityLog.getActivityLogPkId(), activityLog);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return activityLogs;
	}

	@Override
	public void populateActivityDetails(Map<String, ActivityLog> activityLogs, String mtPE) throws SQLException {
		if (activityLogs == null || activityLogs.isEmpty())
			return;
		String baseTemplate = TenantAwareCache.getPersonRepository().getDefaultBTForGroup(mtPE, TenantContextHolder.getUserContext().getPersonId());
		StringBuilder SQL = new StringBuilder("SELECT * FROM T_PFM_IEU_ACTIVITY_LOG_DTL WHERE ACTIVITY_LOG_FK_ID IN (");
		List<Object> parameters = new ArrayList<>();
		for (Map.Entry<String, ActivityLog> activityLogEntry : activityLogs.entrySet()){
			SQL.append("x?, ");
			parameters.add(Util.remove0x(activityLogEntry.getKey()));
		}
		SQL.replace(SQL.length()-2, SQL.length(), ");");
		ResultSet rs = executeSQL(SQL.toString(),parameters);

		while (rs.next()){
			ActivityLogDtl activityLogDtl = new ActivityLogDtl();
			activityLogDtl.setActivityLogDtlPkId(Util.convertByteToString(rs.getBytes("ACTIVITY_LOG_DTL_PK_ID")));
			activityLogDtl.setActivityLogFkId(Util.convertByteToString(rs.getBytes("ACTIVITY_LOG_FK_ID")));
			activityLogDtl.setMtDoaCodeFkId(rs.getString("MT_DOA_CODE_FK_ID"));
//			activityLogDtl.setAttrIconCodeFkId(rs.getString("ATTR_ICON_CODE_FK_ID"));
//			activityLogDtl.setAttrNameG11nBigTxt(Util.getG11nValue(rs.getString("ATTR_NAME_G11N_BIG_TXT"), null));
//			activityLogDtl.setAttrG11n(rs.getBoolean("IS_ATTR_G11N"));
//			activityLogDtl.setBtCodeFkId(rs.getString("BT_CODE_FK_ID"));
			activityLogDtl.setNearLabel(rs.getBoolean("IS_NEAR_LABEL"));
			activityLogDtl.setNewValueG11nBigTxt(Util.getG11nValue(rs.getString("NEW_VALUE_G11N_BIG_TXT"), null));
			activityLogDtl.setBtCodeFkId(baseTemplate);

			EntityAttributeMetadata preAttributeMeta;
			MasterEntityAttributeMetadata postAttributeMeta;

			if (activityLogDtl.getMtDoaCodeFkId().contains(":")){
				String[] splits = activityLogDtl.getMtDoaCodeFkId().split(":");
				String preDOA = splits[0];
				String postDOA = splits[1];
				preAttributeMeta = TenantAwareCache.getMetaDataRepository().getMetadataByBtDoa(baseTemplate, preDOA);
				postAttributeMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(postDOA);// not possible as PersonAddress.Country:Country.Name, here Country.Name meta not available!
			}
			else{
				preAttributeMeta = TenantAwareCache.getMetaDataRepository().getMetadataByBtDoa(baseTemplate, activityLogDtl.getMtDoaCodeFkId());
				postAttributeMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDoa(activityLogDtl.getMtDoaCodeFkId());
			}

			activityLogDtl.setAttributeIcon(preAttributeMeta.getAttrIcon());
			if (activityLogDtl.isNearLabel())
				activityLogDtl.setAttributeName(preAttributeMeta.getAttrDisplayName(false));
			else activityLogDtl.setAttributeName(preAttributeMeta.getAttrRelnDisplayName());
			if (preAttributeMeta.isVisibleInAudit()){
				if (postAttributeMeta.getDbDataTypeCode().equals("BOOLEAN"))
					activityLogDtl.setBooleanValue(true);
				if (postAttributeMeta.getDbDataTypeCode().matches("DATE|TIME|DATE_TIME"))
					activityLogDtl.setDatetimeValue(true);
				activityLogDtl.setDataType(postAttributeMeta.getDbDataTypeCode());
				if (activityLogDtl.getDataType().matches("T_ATTACH|T_IMAGEID")){
					activityLogDtl.setNewValueG11nBigTxt(Util.preSignedGetUrl(activityLogDtl.getNewValueG11nBigTxt(), null).toString());
				}
				if (activityLogs.get(activityLogDtl.getActivityLogFkId()).getActivityLogDtlMap() == null) {
					LinkedHashMap<String, ActivityLogDtl> activityLogDtlMap = new LinkedHashMap<>();
					activityLogDtlMap.put(activityLogDtl.getActivityLogDtlPkId(), activityLogDtl);
					activityLogs.get(activityLogDtl.getActivityLogFkId()).setActivityLogDtlMap(activityLogDtlMap);
				} else
					activityLogs.get(activityLogDtl.getActivityLogFkId()).getActivityLogDtlMap().put(activityLogDtl.getActivityLogDtlPkId(), activityLogDtl);
			}
		}
		for (Map.Entry<String, ActivityLog> entry : activityLogs.entrySet()){
			Map<String, ActivityLogDtl> activityLogDtlMap = entry.getValue().getActivityLogDtlMap();
			if (activityLogDtlMap != null && !activityLogDtlMap.isEmpty()) {
				List<ActivityLogDtl> activityLogDtls = new ArrayList<>(activityLogDtlMap.values());
				activityLogDtls.sort(new Comparator<ActivityLogDtl>() {
					public int compare(ActivityLogDtl s1, ActivityLogDtl s2) {
						return s1.getMtDoaCodeFkId().compareToIgnoreCase(s2.getMtDoaCodeFkId());
					}
				});
				activityLogDtlMap.clear();
				for (ActivityLogDtl activityLogDtl : activityLogDtls) {
					activityLogDtlMap.put(activityLogDtl.getActivityLogDtlPkId(), activityLogDtl);
				}
			}
		}
	}
//	/**
//	 * Deprecated
//	 * @param baseTemplateId
//	 * @param deviceType
//	 * @return
//	 */
//	@Deprecated
//	@Override
//	public List<PageRelationVO> getPageRelationByBaseTemplate(String baseTemplateId, String deviceType) {
//		List<PageRelationVO> pageRelationVOList = new ArrayList<>();
//		String SQL = null;
//		if(deviceType.equals("MOBILE")){
//			SQL = "SELECT *, COLUMN_JSON(CHILD_PAGE_PLACEMENT) as childPagePlacement FROM T_UI_AUT_PAGE_CHILD_EU WHERE BASE_TEMPLATE_DEP_FK_ID = x? AND DEVICE_TYPE_CODE_FK_ID = 'MOBILE' ;";
//		}
//		else if (deviceType.equals("PHABLET")) {
//			SQL = "SELECT *, COLUMN_JSON(CHILD_PAGE_PLACEMENT) as childPagePlacement FROM T_UI_AUT_PAGE_CHILD_EU WHERE BASE_TEMPLATE_DEP_FK_ID = x? AND DEVICE_TYPE_CODE_FK_ID IN ('MOBILE', 'PHABLET');";
//		}
//		else if (deviceType.equals("MINI_TABLET")) {
//			SQL = "SELECT *, COLUMN_JSON(CHILD_PAGE_PLACEMENT) as childPagePlacement FROM T_UI_AUT_PAGE_CHILD_EU WHERE BASE_TEMPLATE_DEP_FK_ID = x? AND DEVICE_TYPE_CODE_FK_ID IN ('MOBILE', 'PHABLET', 'MINI_TABLET');";
//		}
//		else if (deviceType.equals("TABLET")) {
//			SQL = "SELECT *, COLUMN_JSON(CHILD_PAGE_PLACEMENT) as childPagePlacement FROM T_UI_AUT_PAGE_CHILD_EU WHERE BASE_TEMPLATE_DEP_FK_ID = x? AND DEVICE_TYPE_CODE_FK_ID IN ('MOBILE', 'PHABLET', 'MINI_TABLET', 'TABLET');";
//		}
//		else if (deviceType.equals("WEB"))
//			SQL = "SELECT *, COLUMN_JSON(CHILD_PAGE_PLACEMENT) as childPagePlacement FROM T_UI_AUT_PAGE_CHILD_EU WHERE BASE_TEMPLATE_DEP_FK_ID = x? AND DEVICE_TYPE_CODE_FK_ID IN ('MOBILE', 'PHABLET', 'MINI_TABLET', 'TABLET', 'WEB');";
//		List<Object> parameters = new ArrayList<>();
//		parameters.add(baseTemplateId);
//		ResultSet rs = executeSQL(SQL, parameters);
//		try {
//			while(rs.next()){
//				PageRelationVO pageRelationVO = new PageRelationVO();
//				pageRelationVO.setPageChildEuPkId(convertByteToString(rs.getBytes("PAGE_CHILD_EU_PK_ID")));
//				pageRelationVO.setBaseTemplateId(convertByteToString(rs.getBytes("BASE_TEMPLATE_DEP_FK_ID")));
//				pageRelationVO.setDeviceType(rs.getString("DEVICE_TYPE_CODE_FK_ID"));
//				pageRelationVO.setPageChildEquFkId(convertByteToString(rs.getBytes("PAGE_CHILD_EQU_FK_ID")));
//				pageRelationVO.setFromPageEuFkId(convertByteToString(rs.getBytes("FROM_PAGE_EU_FK_ID")));
//				pageRelationVO.setToPageEuFkId(convertByteToString(rs.getBytes("TO_PAGE_EU_FK_ID")));
//				pageRelationVO.setStartingCanvasId(convertByteToString(rs.getBytes("STARTING_CANVAS_EU_FK_ID")));
//				pageRelationVO.setChildPagePlacement(getBlobJson(rs.getBlob("childPagePlacement")));
//				pageRelationVOList.add(pageRelationVO) ;
//			}
//		} catch (SQLException | IOException e) {
//			e.printStackTrace();
//		}
//		return pageRelationVOList;
//	}
//	@Override
//	@Deprecated
//	public List<ActionStepPropertyVO> getActionStepPropertyListByBaseTemplate(String baseTemplateId) {
//		List<ActionStepPropertyVO> actionStepPropertyVOList = new ArrayList<>();
//		String SQL = "SELECT * FROM T_UI_ACTION_STEP_PROPERTY_EU WHERE BASE_TEMPLATE_FK_ID = x?";
//		List<Object> parameters = new ArrayList<>();
//		parameters.add(baseTemplateId);
//		ResultSet rs = executeSQL(SQL, parameters);
//		try {
//			while(rs.next()){
//				ActionStepPropertyVO actionStepPropertyVO = new ActionStepPropertyVO();
//				actionStepPropertyVO.setActionStepPropertyId(convertByteToString(rs.getBytes("ACTION_STEP_PROPERTY_EU_PK_ID")));
//				actionStepPropertyVO.setBaseTemplateId(convertByteToString(rs.getBytes("BASE_TEMPLATE_FK_ID")));
//				actionStepPropertyVO.setMtPEId(convertByteToString(rs.getBytes("MT_PROCESS_ELEMENT_DEP_FK_ID")));
//				actionStepPropertyVO.setActionStepLookupId(convertByteToString(rs.getBytes("ACTION_STEP_LOOKUP_FK_ID")));
//				actionStepPropertyVO.setPropertyCode(rs.getString("ACTION_STEP_PROPERTY_CODE_FK_ID"));
//				actionStepPropertyVO.setPropertyValue(rs.getString("PROPERTY_VALUE"));
//				actionStepPropertyVOList.add(actionStepPropertyVO);
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return actionStepPropertyVOList;
//	}
//	@Override
//	@Deprecated
//	public List<ControlActionVO> getControlActionByBaseTemplate(String baseTemplateId) {
//		List<ControlActionVO> controlActionVOList = new ArrayList<>();
//		String SQL = "SELECT * FROM T_UI_AUT_CONTROL_ACTION_EU WHERE BASE_TEMPLATE_FK_ID = x?;";
//		List<Object> parameters = new ArrayList<>();
//		parameters.add(baseTemplateId);
//		ResultSet rs = executeSQL(SQL, parameters);
//		try {
//			while(rs.next()){
//				ControlActionVO controlActionVO = new ControlActionVO();
//				controlActionVO.setControlActionEuPkId(convertByteToString(rs.getBytes("CONTROL_ACTION_EU_PK_ID")));
//				controlActionVO.setControlEuFkId(convertByteToString(rs.getBytes("CONTROL_EU_FK_ID")));
//				controlActionVO.setBaseTemplateFkId(convertByteToString(rs.getBytes("BASE_TEMPLATE_FK_ID")));
//				controlActionVO.setControlActionDefnFkId(convertByteToString(rs.getBytes("CONTROL_ACTION_DEFN_FK_ID")));
//				controlActionVO.setControlDefnDepFkId(convertByteToString(rs.getBytes("CONTROL_DEFN_DEP_FK_ID")));
//				controlActionVO.setCanvasEuFkId(convertByteToString(rs.getBytes("CANVAS_EU_FK_ID")));
//				controlActionVO.setActionCodeFkId(rs.getString("ACTION_CODE_FK_ID"));
//				controlActionVO.setActionGestureCodeFkId(rs.getString("ACTION_GESTURE_CODE_FK_ID"));
//				controlActionVO.setActionGestureAnimationCodeFkId(rs.getString("ACTION_GESTURE_ANIMATION_CODE_FK_ID"));
//				controlActionVOList.add(controlActionVO);
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return controlActionVOList;
//	}
//	@Deprecated
//	@Override
//	public String insertIntoCanvasMaster(String DEVICE_INDEPENDENT_BLOB,
//										 String DEVICE_DEPENDENT_BLOB, String DEVICE_APPLICABLE_BLOB) {
//		String canvasMasterPkId = null;
//		String SQL = "call insertCanvasDefnMaster(?,?,?,?);";
//		LOG.debug(SQL);
//		Connection con = null;
//		try {
//			con = getConnection();
//			CallableStatement cstmt = con.prepareCall(SQL);
//			cstmt.setString("DEVICE_INDEPENDENT_BLOB",DEVICE_INDEPENDENT_BLOB);
//			cstmt.setString("DEVICE_DEPENDENT_BLOB", DEVICE_DEPENDENT_BLOB);
//			cstmt.setString("DEVICE_APPLICABLE_BLOB", DEVICE_APPLICABLE_BLOB);
//			cstmt.registerOutParameter("CANVAS_DEFN_MASTER_PK_ID", Types.VARCHAR);
//			cstmt.execute();
//			canvasMasterPkId = cstmt.getString("CANVAS_DEFN_MASTER_PK_ID");
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return canvasMasterPkId;
//	}
//	@Override
//	public void registerPrimaryKeyName(String primaryKeyName, String canvasMasterPkId) {
//		String SQL = "INSERT INTO T_TAP_PK_NAME_TO_ID_MAPPING VALUES (?,x?)";
//		List<Object> parameters = new ArrayList<>();
//		parameters.add(primaryKeyName);
//		parameters.add(canvasMasterPkId);
//		try{
//			Connection con = null;
//			con = getConnection();
//			PreparedStatement ps = con.prepareStatement(SQL);
//			LOG.debug(SQL);
//			for (int parameterIndex =0; parameterIndex<parameters.size(); parameterIndex++){
//				ps.setObject(parameterIndex+1, parameters.get(parameterIndex));
//			}
//			ps.executeUpdate();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//	@Override
//	@Deprecated
//	public Map<String, String> getBaseTemplateMap() {
//		Map<String, String> baseTemplateMap = new HashMap<>();
//		String SQL = "SELECT BASE_TEMPLATE_NAME_G11N_TEXT,BASE_TEMPLATE_PK_ID FROM T_BT_ADM_BASE_TEMPLATE;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try {
//			while(rs.next()){
//				String baseTemplateName = rs.getString("BASE_TEMPLATE_NAME_G11N_TEXT");
//				String baseTemplateId = convertByteToString(rs.getBytes("BASE_TEMPLATE_PK_ID"));
//				baseTemplateMap.put(baseTemplateName, baseTemplateId);
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return baseTemplateMap;
//	}
//	@Override
//	public String insertIntoCanvasTransaction(String bASE_TEMPLATE_FK_ID, String mT_PROCESS_ELEMENT_FK_ID, String CANVAS_MASTER_DEFN_FK_ID,
//											  String dEVICE_INDEPENDENT_BLOB, String dEVICE_DEPENDENT_BLOB) {
//		String canvasTransactionPkId = null;
//		String SQL = "{call insertCanvasTransaction(?,?,?,?,?)}";
//		LOG.debug(SQL);
//		Connection con = null;
//		try {
//			con = getConnection();
//			CallableStatement cstmt = con.prepareCall(SQL);
//			cstmt.setObject("BASE_TEMPLATE_FK_ID", bASE_TEMPLATE_FK_ID);
//			LOG.debug(bASE_TEMPLATE_FK_ID);
//			cstmt.setObject("MT_PROCESS_ELEMENT_FK_ID", mT_PROCESS_ELEMENT_FK_ID);
//			LOG.debug(mT_PROCESS_ELEMENT_FK_ID);
//			cstmt.setString("CANVAS_MASTER_DEFN_FK_ID", CANVAS_MASTER_DEFN_FK_ID);
//			LOG.debug(CANVAS_MASTER_DEFN_FK_ID);
//			cstmt.setString("DEVICE_INDEPENDENT_BLOB",dEVICE_INDEPENDENT_BLOB);
//			LOG.debug(dEVICE_INDEPENDENT_BLOB);
//			cstmt.setString("DEVICE_DEPENDENT_BLOB", dEVICE_DEPENDENT_BLOB);
//			LOG.debug(dEVICE_DEPENDENT_BLOB);
//			cstmt.registerOutParameter("CANVAS_TRANSACTION_PK_ID", Types.VARCHAR);
//			cstmt.execute();
//			canvasTransactionPkId = cstmt.getString("CANVAS_TRANSACTION_PK_ID");
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return canvasTransactionPkId;
//	}
//	@Override
//	public Map<String, String> getProcessElementMap() {
//		Map<String, String> processElementMap = new HashMap<>();
//		String SQL = "SELECT MT_PROCESS_ELEMENT_NAME_G11N_TEXT, MT_PROCESS_ELEMENT_PK_ID FROM T_MT_ADM_PROCESS_ELEMENT;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try{
//			while(rs.next()){
//				String processElementName = rs.getString("MT_PROCESS_ELEMENT_NAME_G11N_TEXT");
//				String processElementId = convertByteToString(rs.getBytes("MT_PROCESS_ELEMENT_PK_ID"));
//				processElementMap.put(processElementName, processElementId);
//			}
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return processElementMap;
//	}
//	@Override
//	public Map<String, String> getViewTypeMap() {
//		Map<String, String> viewTypeMap = new HashMap<>();
//		String SQL = "SELECT VIEW_TYPE_CODE_PK_ID,VIEW_TYPE_CODE_PK_ID FROM T_UI_ADM_VIEW_TYPE_LOOKUP;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try{
//			while(rs.next()){
//				String viewTypeName = rs.getString("VIEW_TYPE_CODE_PK_ID");
//				String viewTypeCodePkId = rs.getString("VIEW_TYPE_CODE_PK_ID");
//				viewTypeMap.put(viewTypeName, viewTypeCodePkId);
//			}
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return viewTypeMap;
//	}
//
//	@Override
//	public void insertJsonNameToColumnType(Map<String, String> jsonNameToColumnTypeMap) {
//		for(Map.Entry<String, String> entry : jsonNameToColumnTypeMap.entrySet()){
//			insertJsonToColumnType(entry.getKey(), entry.getValue());
//		}
//	}
//	private void insertJsonToColumnType(String jsonName, String columnType){
//		String SQL = "INSERT INTO JSON_NAME_TO_COLUMN_TYPE_MAPPING (JSON_NAME, COLUMN_TYPE) VALUES (?, ?);";
//		try{
//			Connection con = null;
//			con = getConnection();
//			PreparedStatement ps = con.prepareStatement(SQL);
//			LOG.debug(SQL);
//			ps.setString(1,jsonName);
//			ps.setString(2,columnType);
//			ps.executeUpdate();
//		} catch (SQLException e) {
//
//		}
//	}
//	@Override
//	public Map<String, String> getCanvasTypeMap() {
//		Map<String, String> canvasTypeMap = new HashMap<>();
//		String SQL = "SELECT CANVAS_TYPE_CODE_PK_ID,CANVAS_TYPE_CODE_PK_ID FROM T_UI_ADM_CANVAS_TYPE_LOOKUP;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try{
//			while(rs.next()){
//				String canvasTypeName = rs.getString("CANVAS_TYPE_CODE_PK_ID");
//				String canvasTypeCodePkId = rs.getString("CANVAS_TYPE_CODE_PK_ID");
//				canvasTypeMap.put(canvasTypeName, canvasTypeCodePkId);
//			}
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return canvasTypeMap;
//	}
//
//	@Override
//	public void insertBaseObjectTemplateAndPerson(String personId, String personName, String baseObjectName,
//												  String metaProcessCode, String baseTemplateName, String baseTemplateId) {
//		StringBuilder personInsertSQL = new StringBuilder("{Call insertStdPerson("+"'"+personId+"','"+personName+"',"+"'"+metaProcessCode+"','"+baseTemplateId+"')}");
//		StringBuilder baseObjectSQL = new StringBuilder("{Call insertStdbaseobject("+"x'"+metaProcessCode+"','"+baseObjectName+"',"+"x'"+baseTemplateId+"',x'"+personId+"','"+personName+"')}");
//		StringBuilder baseTemplateSQL = new StringBuilder("{Call insertStdbasetemplate("+"x'"+baseTemplateId+"','"+baseTemplateName+"',"+"x'"+metaProcessCode+"',x'"+personId+"','"+personName+"')}");
//		LOG.debug(personInsertSQL.toString());
//		LOG.debug(baseObjectSQL.toString());
//		LOG.debug(baseTemplateSQL.toString());
//		Connection con = null;
//		try {
//			con = getConnection();
//			CallableStatement personCstmt = con.prepareCall(personInsertSQL.toString());
//			CallableStatement boCstmt = con.prepareCall(baseObjectSQL.toString());
//			CallableStatement btCstmt = con.prepareCall(baseTemplateSQL.toString());
//			personCstmt.execute();
//			boCstmt.execute();
//			btCstmt.execute();
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//	@Override
//    @Deprecated
//	public Map<String, String> getBaseObjectMap() {
//		Map<String, String> baseObjectmapMap = new HashMap<>();
//		String SQL = "SELECT BASE_OBJECT_NAME_G11N_TEXT,BASE_OBJECT_PK_ID FROM T_MT_ADM_BASE_OBJECT;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try{
//			while(rs.next()){
//				String baseObjectName = rs.getString("BASE_OBJECT_NAME_G11N_TEXT");
//				String baseObjectPkId = convertByteToString(rs.getBytes("BASE_OBJECT_PK_ID"));;
//				baseObjectmapMap.put(baseObjectName, baseObjectPkId);
//			}
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return baseObjectmapMap;
//	}
//	@Override
//	public Map<String, String> getGestureMap() {
//		Map<String, String> gestureMap = new HashMap<>();
//		String SQL = "SELECT GESTURE_CODE_PK_ID,GESTURE_CODE_PK_ID FROM T_UI_ADM_GESTURE_LOOKUP;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try{
//			while(rs.next()){
//				String gestureName = rs.getString("GESTURE_CODE_PK_ID");
//				String gesturePkId = rs.getString("GESTURE_CODE_PK_ID");
//				gestureMap.put(gestureName, gesturePkId);
//			}
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return gestureMap;
//	}
//	@Override
//	public Map<String, String> getAnimationCodeMap() {
//		Map<String, String> animationMap = new HashMap<>();
//		String SQL = "SELECT ANIMATION_CODE_PK_ID,ANIMATION_CODE_PK_ID FROM T_UI_ADM_ANIMATION_LOOKUP;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try{
//			while(rs.next()){
//				String animationName = rs.getString("ANIMATION_CODE_PK_ID");
//				String animtionPkId = rs.getString("ANIMATION_CODE_PK_ID");
//				animationMap.put(animationName, animtionPkId);
//			}
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return animationMap;
//	}
//	@Override
//	public Map<String, String> getEntityCodeMap() {
//		Map<String, String> entityCodeMap = new HashMap<>();
//		String SQL = "SELECT ENTITY_CODE_PK_ID,ENTITY_CODE_PK_ID FROM T_UI_ADM_ENTITY_LOOOKUP;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try{
//			while(rs.next()){
//				String entityName = rs.getString("ENTITY_CODE_PK_ID");
//				String entityPkId = rs.getString("ENTITY_CODE_PK_ID");
//				entityCodeMap.put(entityName, entityPkId);
//			}
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return entityCodeMap;
//	}
//	@Override
//	public Map<String, String> getThemeSizeMap() {
//		Map<String, String> themeMap = new HashMap<>();
//		String SQL = "SELECT THEME_SIZE_CODE_PK_ID,THEME_SIZE_CODE_PK_ID FROM T_SYS_ADM_THEME_SIZE_LOOKUP;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try{
//			while(rs.next()){
//				String themeName = rs.getString("THEME_SIZE_CODE_PK_ID");
//				String themePkId = rs.getString("THEME_SIZE_CODE_PK_ID");
//				themeMap.put(themeName, themePkId);
//			}
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return themeMap;
//	}
//	@Override
//	public Map<String, String> getDoaMap() {
//		Map<String, String> map = new HashMap<>();
//		String SQL = "SELECT DO_ATTRIBUTE_NAME_G11N_TEXT,DO_ATTRIBUTE_PK_ID FROM T_MT_ADM_DO_ATTRIBUTE;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try{
//			while(rs.next()){
//				String Name = rs.getString("DO_ATTRIBUTE_NAME_G11N_TEXT");
//				String PkId = convertByteToString(rs.getBytes("DO_ATTRIBUTE_PK_ID"));
//				map.put(Name, PkId);
//			}
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return map;
//	}
//	@Override
//	public Map<String, String> getDoMap() {
//		Map<String, String> map = new HashMap<>();
//		String SQL = "SELECT DOMAIN_OBJECT_NAME_G11N_TEXT,DOMAIN_OBJECT_PK_ID FROM T_MT_ADM_DOMAIN_OBJECT;";
//		List<Object> parameters = new ArrayList<>();
//		ResultSet rs = executeSQL(SQL, parameters);
//		try{
//			while(rs.next()){
//				String Name = rs.getString("DOMAIN_OBJECT_NAME_G11N_TEXT");
//				String PkId = convertByteToString(rs.getBytes("DOMAIN_OBJECT_PK_ID"));
//				map.put(Name, PkId);
//			}
//		}catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return map;
//	}
}