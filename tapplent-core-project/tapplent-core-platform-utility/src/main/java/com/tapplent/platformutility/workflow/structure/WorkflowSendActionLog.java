package com.tapplent.platformutility.workflow.structure;

public class WorkflowSendActionLog {
    private String actorSendActionLogPkId;
    private String actorReceiptActionLogId;
    private String sendToWorkflowStepActorId;
    private String sendToPersonId;
    private String sendToOnBehalfOfPersonId;
    private String actionVisualMasterCodeId;
    private String actionSentDateTime;
    private String sentActionComment;
    private boolean isDeleted;

    public String getActorSendActionLogPkId() {
        return actorSendActionLogPkId;
    }

    public void setActorSendActionLogPkId(String actorSendActionLogPkId) {
        this.actorSendActionLogPkId = actorSendActionLogPkId;
    }

    public String getActorReceiptActionLogId() {
        return actorReceiptActionLogId;
    }

    public void setActorReceiptActionLogId(String actorReceiptActionLogId) {
        this.actorReceiptActionLogId = actorReceiptActionLogId;
    }

    public String getSendToWorkflowStepActorId() {
        return sendToWorkflowStepActorId;
    }

    public void setSendToWorkflowStepActorId(String sendToWorkflowStepActorId) {
        this.sendToWorkflowStepActorId = sendToWorkflowStepActorId;
    }

    public String getSendToPersonId() {
        return sendToPersonId;
    }

    public void setSendToPersonId(String sendToPersonId) {
        this.sendToPersonId = sendToPersonId;
    }

    public String getSendToOnBehalfOfPersonId() {
        return sendToOnBehalfOfPersonId;
    }

    public void setSendToOnBehalfOfPersonId(String sendToOnBehalfOfPersonId) {
        this.sendToOnBehalfOfPersonId = sendToOnBehalfOfPersonId;
    }

    public String getActionVisualMasterCodeId() {
        return actionVisualMasterCodeId;
    }

    public void setActionVisualMasterCodeId(String actionVisualMasterCodeId) {
        this.actionVisualMasterCodeId = actionVisualMasterCodeId;
    }

    public String getActionSentDateTime() {
        return actionSentDateTime;
    }

    public void setActionSentDateTime(String actionSentDateTime) {
        this.actionSentDateTime = actionSentDateTime;
    }

    public String getSentActionComment() {
        return sentActionComment;
    }

    public void setSentActionComment(String sentActionComment) {
        this.sentActionComment = sentActionComment;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
