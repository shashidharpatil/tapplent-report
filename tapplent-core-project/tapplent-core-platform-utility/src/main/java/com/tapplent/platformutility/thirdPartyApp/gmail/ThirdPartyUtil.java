package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONException;
import org.json.JSONObject;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.tapplent.platformutility.common.cache.SystemAwareCache;

public class ThirdPartyUtil {
	public static String getAccessToken(String refreshToken){
		
		List<BasicNameValuePair> para = new ArrayList<BasicNameValuePair>();
		String CLIENT_ID="3438275722-khh8q6fqlbuva9nnolriitijo5e89m11.apps.googleusercontent.com";
		String CLIENT_SECRET="JLcugPJf28iA41CaIHXRP98H";
		para.add(new BasicNameValuePair("client_id",CLIENT_ID));
		para.add(new BasicNameValuePair("client_secret", CLIENT_SECRET));
		para.add(new BasicNameValuePair("refresh_token",refreshToken));
		para.add(new BasicNameValuePair("grant_type","refresh_token"));			      
		HttpPost post = new HttpPost("https://www.googleapis.com/oauth2/v4/token");
		//post.addHeader("Content-Type", "application/x-www-form-urlencoded");
		HttpClient client = HttpClientBuilder.create().build();
		String accessToken = "";
		HttpResponse response = null;
		try {
			post.setEntity(new UrlEncodedFormEntity(para, HTTP.UTF_8));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			response = client.execute(post);
			String json = EntityUtils.toString(response.getEntity());
		    JSONObject responseObject = new JSONObject(json);
		    accessToken = responseObject.getString("access_token");
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return accessToken;		
	}
	

	public static String getTheAddressFromCoordinates(String geoCode) throws Exception{
		StringBuilder url = new StringBuilder("https://maps.googleapis.com/maps/api/geocode/json?latlng=");
		url.append(geoCode).append("&key=");
		url.append("AIzaSyCfj3tDEn63cAFo9hBjCQTSA7Kj5GlsQxQ");
		HttpGet get = new HttpGet(url.toString());
		get.addHeader("Content-Type", "application/json");
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(get);
	    JSONObject responseObject = new JSONObject(EntityUtils.toString(response.getEntity()));
	    org.json.JSONArray results = responseObject.getJSONArray("results");
	    
	    	JSONObject entry = results.getJSONObject(0);
	    	String formatted_address = entry.getString("formatted_address");
	   
		return formatted_address;
	}
	/**
	 * Reads fully from the specified input stream and returns a byte array containing the contents read
	 * @param stream The input stream to read from
	 * @return A byte array containing data read from the specified input stream
	 * @throws IOException An error encountered when reading from the input stream
	 */
	public static byte[] readFromStreamToByteArray(InputStream stream) throws IOException
	{
		byte[] buffer = new byte[1024];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int bytesRead;
		while ((bytesRead = stream.read(buffer, 0, buffer.length)) != -1)
		{
			baos.write(buffer, 0, bytesRead);
		}
		return baos.toByteArray();
	}
	public static boolean isTokenValid(String accessToken) throws ClientProtocolException, IOException {
		StringBuilder url = new StringBuilder("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=");
		url.append(accessToken);
		HttpGet get = new HttpGet(url.toString());
		get.addHeader("Content-Type", "application/json");
		HttpClient client = HttpClientBuilder.create().build();
	    HttpResponse response;
	    response = client.execute(get);
		JSONObject responseObject = null;
		try {
			responseObject = new JSONObject(EntityUtils.toString(response.getEntity()));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if(responseObject.has("expires_in"))
	     return true;
	   return false;
	}
	public static Timestamp isoToSqlTimestamp(String lastModifiedTime){
		DateTime dateTime = new DateTime(lastModifiedTime, DateTimeZone.forOffsetHoursMinutes(5, 30));
		DateTimeFormatter dateFormatter1 = ISODateTimeFormat.hourMinuteSecondFraction();
		DateTimeFormatter dateFormatter2 = ISODateTimeFormat.date();
		String dateString = dateFormatter2.print(dateTime);
		String timeString = dateFormatter1.print(dateTime);
		String timeStamp = dateString+" "+timeString;
		Timestamp oneNoteLastModifiedTime = Timestamp.valueOf(timeStamp);
		return oneNoteLastModifiedTime;
	}
	
	public static Map<String,Object> getAttchment(String id){
		AmazonS3 amazonS3 = SystemAwareCache.getSystemRepository().getAmazonS3Client();
		S3Object attachment = amazonS3.getObject("testingtapplent/22", id);
		S3ObjectInputStream is = attachment.getObjectContent();
		ObjectMetadata metadata	= attachment.getObjectMetadata();
		Map<String,Object> attachmentMap = new HashMap<String,Object>();
		attachmentMap.put("contentDeposition", metadata.getContentDisposition());		
		attachmentMap.put("contentLength", metadata.getContentLength());
		attachmentMap.put("ContentType", metadata.getContentType());
		attachmentMap.put("InputStream", is);
		return attachmentMap;
		
	}
	public static String bytesToHex(byte[] bytes) {
	    StringBuilder sb = new StringBuilder();
	    for (byte hashByte : bytes) {
	      int intVal = 0xff & hashByte;
	      if (intVal < 0x10) {
	        sb.append('0');
	      }
	      sb.append(Integer.toHexString(intVal));
	    }
	    return sb.toString();
	  }
}
