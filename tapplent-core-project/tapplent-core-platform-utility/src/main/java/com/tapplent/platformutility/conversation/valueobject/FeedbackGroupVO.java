package com.tapplent.platformutility.conversation.valueobject;

import com.tapplent.platformutility.common.util.AttachmentObject;

import java.util.Map;

/**
 * Created by Tapplent on 6/9/17.
 */
public class FeedbackGroupVO {
    private String feedbackGroupPkId;
    private String feedbackGroupName;
    private String feedbackGroupImageId;
    private AttachmentObject feedbackGroupImageUrl;
    private String mTPECode;
    private String objectId;
    private boolean bilateralConversation;
    private boolean memNtfnMuted;
    private boolean memberAdmin;
    private String loggedinPersonFeedbackMemberId;

    public String getFeedbackGroupPkId() {
        return feedbackGroupPkId;
    }

    public void setFeedbackGroupPkId(String feedbackGroupPkId) {
        this.feedbackGroupPkId = feedbackGroupPkId;
    }

    public String getFeedbackGroupName() {
        return feedbackGroupName;
    }

    public void setFeedbackGroupName(String feedbackGroupName) {
        this.feedbackGroupName = feedbackGroupName;
    }

    public String getFeedbackGroupImageId() {
        return feedbackGroupImageId;
    }

    public void setFeedbackGroupImageId(String feedbackGroupImageId) {
        this.feedbackGroupImageId = feedbackGroupImageId;
    }

    public AttachmentObject getFeedbackGroupImageUrl() {
        return feedbackGroupImageUrl;
    }

    public void setFeedbackGroupImageUrl(AttachmentObject feedbackGroupImageUrl) {
        this.feedbackGroupImageUrl = feedbackGroupImageUrl;
    }

    public String getmTPECode() {
        return mTPECode;
    }

    public void setmTPECode(String mTPECode) {
        this.mTPECode = mTPECode;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public boolean isBilateralConversation() {
        return bilateralConversation;
    }

    public void setBilateralConversation(boolean bilateralConversation) {
        this.bilateralConversation = bilateralConversation;
    }

    public boolean isMemNtfnMuted() {
        return memNtfnMuted;
    }

    public void setMemNtfnMuted(boolean memNtfnMuted) {
        this.memNtfnMuted = memNtfnMuted;
    }

    public boolean isMemberAdmin() {
        return memberAdmin;
    }

    public void setMemberAdmin(boolean memberAdmin) {
        this.memberAdmin = memberAdmin;
    }

    public String getLoggedinPersonFeedbackMemberId() {
        return loggedinPersonFeedbackMemberId;
    }

    public void setLoggedinPersonFeedbackMemberId(String loggedinPersonFeedbackMemberId) {
        this.loggedinPersonFeedbackMemberId = loggedinPersonFeedbackMemberId;
    }
}
