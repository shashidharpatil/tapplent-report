package com.tapplent.platformutility.person.service;

import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.person.structure.PersonGroup;

/**
 * Created by tapplent on 07/11/16.
 */
public interface PersonService {
    Person getPerson(String personId);

    String getDefaultBTForGroup(String mtPE, Person person);

    Person getSystemDummyPerson(String personName);
}
