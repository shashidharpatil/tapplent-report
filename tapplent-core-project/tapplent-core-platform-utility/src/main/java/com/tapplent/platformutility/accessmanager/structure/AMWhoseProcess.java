package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by tapplent on 04/11/16.
 */
public class AMWhoseProcess {
    private String amWhosePkId;
    private String amAccessProcessFkId;
    private String relationshipTypeCodeFkId;
    private String elHierarchyLevelPosInt;

    public String getAmWhosePkId() {
        return amWhosePkId;
    }

    public void setAmWhosePkId(String amWhosePkId) {
        this.amWhosePkId = amWhosePkId;
    }

    public String getAmAccessProcessFkId() {
        return amAccessProcessFkId;
    }

    public void setAmAccessProcessFkId(String amAccessProcessFkId) {
        this.amAccessProcessFkId = amAccessProcessFkId;
    }

    public String getRelationshipTypeCodeFkId() {
        return relationshipTypeCodeFkId;
    }

    public void setRelationshipTypeCodeFkId(String relationshipTypeCodeFkId) {
        this.relationshipTypeCodeFkId = relationshipTypeCodeFkId;
    }

    public String getElHierarchyLevelPosInt() {
        return elHierarchyLevelPosInt;
    }

    public void setElHierarchyLevelPosInt(String elHierarchyLevelPosInt) {
        this.elHierarchyLevelPosInt = elHierarchyLevelPosInt;
    }
}
