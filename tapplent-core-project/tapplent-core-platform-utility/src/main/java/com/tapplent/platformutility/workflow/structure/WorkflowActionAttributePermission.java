package com.tapplent.platformutility.workflow.structure;

import java.util.Map;

public class WorkflowActionAttributePermission {
    private Map<String, Map<String, ActorActionPermission>> mtPEToActorActionPermissionMap;
    private Map<String, Map<String, ActorAttributePermission>> mtPEToActorAttributePermissionMap;
    private Map<String, Map<String, NonActorActionPermission>> nonActorActionPermissionMap;
    private Map<String, Map<String, NonActorAttributePermission>> nonActorAttributePermissionMap;
    private boolean actorExists;

    public Map<String, Map<String, ActorActionPermission>> getMtPEToActorActionPermissionMap() {
        return mtPEToActorActionPermissionMap;
    }

    public void setMtPEToActorActionPermissionMap(Map<String, Map<String, ActorActionPermission>> mtPEToActorActionPermissionMap) {
        this.mtPEToActorActionPermissionMap = mtPEToActorActionPermissionMap;
    }

    public Map<String, Map<String, ActorAttributePermission>> getMtPEToActorAttributePermissionMap() {
        return mtPEToActorAttributePermissionMap;
    }

    public void setMtPEToActorAttributePermissionMap(Map<String, Map<String, ActorAttributePermission>> mtPEToActorAttributePermissionMap) {
        this.mtPEToActorAttributePermissionMap = mtPEToActorAttributePermissionMap;
    }

    public Map<String, Map<String, NonActorActionPermission>> getNonActorActionPermissionMap() {
        return nonActorActionPermissionMap;
    }

    public void setNonActorActionPermissionMap(Map<String, Map<String, NonActorActionPermission>> nonActorActionPermissionMap) {
        this.nonActorActionPermissionMap = nonActorActionPermissionMap;
    }

    public Map<String, Map<String, NonActorAttributePermission>> getNonActorAttributePermissionMap() {
        return nonActorAttributePermissionMap;
    }

    public void setNonActorAttributePermissionMap(Map<String, Map<String, NonActorAttributePermission>> nonActorAttributePermissionMap) {
        this.nonActorAttributePermissionMap = nonActorAttributePermissionMap;
    }

    public boolean isActorExists() {
        return actorExists;
    }

    public void setActorExists(boolean actorExists) {
        this.actorExists = actorExists;
    }
}
