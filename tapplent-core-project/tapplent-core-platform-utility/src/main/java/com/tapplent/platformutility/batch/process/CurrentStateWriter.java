package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import com.tapplent.platformutility.batch.model.RCRD_STATE_MtDOData;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class CurrentStateWriter extends JdbcBatchItemWriter<RCRD_STATE_MtDOData> {

    public CurrentStateWriter() {
        super();
        DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
        setDataSource(ds);
        setSql(getSQL());
        setItemPreparedStatementSetter(new ItemPreparedStatementSetter<RCRD_STATE_MtDOData>() {
            @Override
            public void setValues(RCRD_STATE_MtDOData item, PreparedStatement ps) throws SQLException {
               /* String sql = "UPDATE\n" +
                        "   " + item.getDbTableName() + " \n" +
                        "INNER JOIN\n" +
                        "    (\n" +
                        "      SELECT MAX(EFFECTIVE_DATETIME) MAXDT, VERSION_ID, PERSON_PHONE_PK_ID\n" +
                        "      FROM T_PRN_EU_PERSON_PHONE\n" +
                        "      WHERE EFFECTIVE_DATETIME < SYSDATE()\n" +
                        "      GROUP BY PERSON_PHONE_PK_ID\n" +
                        "      ) ET1\n" +
                        "ON ET.EFFECTIVE_DATETIME = MAXDT AND ET1.PERSON_PHONE_PK_ID = ET.PERSON_PHONE_PK_ID\n" +
                        "    SET ET.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n" +
                        "WHERE ET.EFFECTIVE_DATETIME < SYSDATE() ;";
                setSql(sql);*/
            }
        });
        setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<RCRD_STATE_MtDOData>());
        setAssertUpdates(false);
        afterPropertiesSet();
    }


    public String getSQL() {
        String sql = "INSERT INTO T_SCH_IEU_SCHEDULER_INSTANCE  (SCHEDULER_INSTANCE_PK_ID,SCHED_DEFN_FK_ID," +
                "EXECUTION_VERSION_ID,JOB_TYPE_CODE_FK_ID,SCHEDULED_DATETIME,NOTES_G11N_BIG_TXT) values (" + Common.getUUID() + "," + Common.getUUID() + "," +
                Common.getUUID() + ",RECORD_STATE_CALCULATION,2017-09-09 01:01:01.000,insert test);";


       /* String sql = "UPDATE\n" +
                "   T_PRN_EU_PERSON_PHONE ET\n" +
                "INNER JOIN\n" +
                "    (\n" +
                "      SELECT MAX(EFFECTIVE_DATETIME) MAXDT, VERSION_ID, PERSON_PHONE_PK_ID\n" +
                "      FROM T_PRN_EU_PERSON_PHONE\n" +
                "      WHERE EFFECTIVE_DATETIME < SYSDATE()\n" +
                "      GROUP BY PERSON_PHONE_PK_ID\n" +
                "      ) ET1\n" +
                "ON ET.EFFECTIVE_DATETIME = MAXDT AND ET1.PERSON_PHONE_PK_ID = ET.PERSON_PHONE_PK_ID\n" +
                "    SET ET.RECORD_STATE_CODE_FK_ID = 'CURRENT'\n" +
                "WHERE ET.EFFECTIVE_DATETIME < SYSDATE() ;";*/
        return sql;

    }

    public void write(List<? extends RCRD_STATE_MtDOData> items) throws Exception {
        for (RCRD_STATE_MtDOData rcrd_state_mtDOData : items) {
           /* String sql = "UPDATE\n" +
                    "   " + rcrd_state_mtDOData.getDbTableName() + " ET\n" +
                    "INNER JOIN\n" +
                    "    (\n" +
                    "      SELECT MAX(EFFECTIVE_DATETIME) MAXDT, " + rcrd_state_mtDOData.getDbPkColumnName() + "\n" +
                    "      FROM " + rcrd_state_mtDOData.getDbTableName() + "\n" +
                    "      WHERE EFFECTIVE_DATETIME < SYSDATE()\n" +
                    "      GROUP BY " + rcrd_state_mtDOData.getDbPkColumnName() + "\n" +
                    "      ) ET1\n" +
                    "ON ET.EFFECTIVE_DATETIME > MAXDT AND ET1." + rcrd_state_mtDOData.getDbPkColumnName() + " = ET." + rcrd_state_mtDOData.getDbPkColumnName() + "\n" +
                    "    SET ET.RECORD_STATE_CODE_FK_ID = 'FUTURE'\n" +
                    "WHERE ET.EFFECTIVE_DATETIME < SYSDATE() ;";*/
            String sql = "INSERT INTO T_SCH_IEU_SCHEDULER_INSTANCE  (SCHEDULER_INSTANCE_PK_ID,SCHED_DEFN_FK_ID," +
                    "EXECUTION_VERSION_ID,JOB_TYPE_CODE_FK_ID,SCHEDULED_DATETIME,NOTES_G11N_BIG_TXT) values (" + Common.getUUID() + "," + Common.getUUID() + "," +
                    Common.getUUID() + ",RECORD_STATE_CALCULATION,2017-09-09 01:01:01.000,insert test" + rcrd_state_mtDOData.getDbPkColumnName() + ");";
            setSql(sql);

        }
        super.write(items);

    }
}
