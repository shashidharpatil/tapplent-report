package com.tapplent.platformutility.analytics.structure;

/**
 * Created by tapplent on 23/08/17.
 */
public class GraphTypeVO {
    private String graphTypeCode;
    private String graphTypeName;

    public String getGraphTypeCode() {
        return graphTypeCode;
    }

    public void setGraphTypeCode(String graphTypeCode) {
        this.graphTypeCode = graphTypeCode;
    }

    public String getGraphTypeName() {
        return graphTypeName;
    }

    public void setGraphTypeName(String graphTypeName) {
        this.graphTypeName = graphTypeName;
    }
}
