package com.tapplent.platformutility.sql.statement.builder;

public interface SQLStatementBuilder {
	String build();
}
