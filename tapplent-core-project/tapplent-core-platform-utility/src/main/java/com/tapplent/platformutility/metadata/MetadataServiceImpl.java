package com.tapplent.platformutility.metadata;

import java.sql.SQLException;
import java.util.*;

import com.tapplent.platformutility.accessmanager.structure.AMColCondition;
import com.tapplent.platformutility.accessmanager.structure.RowCondition;
import com.tapplent.platformutility.activityLog.AuditMsgFormat;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.valueObject.AnalyticsDOAControlMap;
import com.tapplent.platformutility.common.util.valueObject.NameCalculationRuleVO;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.layout.valueObject.LicencedLocalesVO;
import com.tapplent.platformutility.metadata.dao.MetadataDAO;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.uilayout.valueobject.LayoutThemeMapper;
import com.tapplent.platformutility.uilayout.valueobject.LayoutThemeMapperVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.util.RequestSelectType;
import com.tapplent.platformutility.search.builder.SortData;
import com.tapplent.platformutility.common.util.StringUtil;
@Service
public class MetadataServiceImpl implements MetadataService{
	
	@SuppressWarnings("unused")
	private static final Logger LOG = LogFactory.getLogger(MetadataServiceImpl.class);
	private MetadataDAO metadataDAO;
	public MetadataDAO getMetadataDAO() {
		return metadataDAO;
	}
	public void setMetadataDAO(MetadataDAO metadataDAO) {
		this.metadataDAO = metadataDAO;
	}
	@Override
	@Transactional
	public EntityMetadata getMetadataByProcessElement(String baseTemplate,String mtPE,String processElementId){
		if(processElementId ==null && (baseTemplate==null&&processElementId==null)){
			return null;
		}
//		Map<String, MetadataVO> pkIdToMetadataMap = metadataDAO.getMetadataByProcessElement(baseTemplate,mtPE,processElementId);
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		Map<String, BtDoSpecVO> pkIdBtDoSpecMap = metadataDAO.getBtDoSpec(baseTemplate, mtPE, processElementId);
		/*Map<String, BtDoAttributeSpecVO>*/List<BtDoAttributeSpecVO> pkIdBtDoAttributeSpecMap = metadataDAO.getBtDoAttributeSpec(pkIdBtDoSpecMap.keySet().iterator().next());
//		List<MetadataRelationVO> metadataRelationVOlist = metadataDAO.getMetadataRelationByProcessElement(baseTemplate,mtPE,processElementId);
		EntityMetadata entitymetadata = new EntityMetadata();
		//Helper map to add relation table to the EntityAttributeMetadata
//		Map<String, EntityAttributeMetadata> pkIdMap = new HashMap<String, EntityAttributeMetadata>();
		//loop through the Map
//		for (Map.Entry<String, MetadataVO> entry : pkIdToMetadataMap.entrySet())
//		{	
//			EntityAttributeMetadata attributeMetadata = new EntityAttributeMetadata();
//			MetadataVO metadataVO = entry.getValue();
//			if(entitymetadata.getDomainObjectFkId()==null){
//				BeanUtils.copyProperties(metadataVO, entitymetadata);
//			}
//			else
//			{ 
//				if(!entitymetadata.getDomainObjectFkId().equals(metadataVO.getDomainObjectFkId())){
//					LOG.debug("Two different domain object Ids found in process element with ID "+processElementId);
//				}
//			}
//			//Set hierarchy flag = true for the Entitymetadata if Hierarchy flag of a DOA is true.
//			if(metadataVO.getHierarchyFlag())
//				entitymetadata.setHierarchyFlag(true);
//			BeanUtils.copyProperties(metadataVO, attributeMetadata);
//			entitymetadata.getAttributeMeta().add(attributeMetadata);
//			pkIdMap.put(attributeMetadata.getDataApiInternalTablePkId(), attributeMetadata);
//		}
		BtDoSpecVO btDoSpecVO = pkIdBtDoSpecMap.values().iterator().next();
		BeanUtils.copyProperties(btDoSpecVO, entitymetadata);
		entitymetadata.setDoSummaryTitleIcon(metadataService.getIconDetailsByIconCode(btDoSpecVO.getDoSummaryTitleIcon()));
		for(BtDoAttributeSpecVO entry : pkIdBtDoAttributeSpecMap){
			EntityAttributeMetadata attributeMetadata = new EntityAttributeMetadata();
			BeanUtils.copyProperties(entry, attributeMetadata);
			attributeMetadata.setAttrIcon(metadataService.getIconDetailsByIconCode(entry.getAttrIcon()));
			attributeMetadata.setBooleanOnStateIcon(metadataService.getIconDetailsByIconCode(entry.getBooleanOnStateIcon()));
			attributeMetadata.setBooleanOffStateIcon(metadataService.getIconDetailsByIconCode(entry.getBooleanOffStateIcon()));
			if(entry.getCountrySpecificDOAMetaVOList()!=null) {
				for (CountrySpecificDOAMetaVO countrySpecificDOAMetaVO :entry.getCountrySpecificDOAMetaVOList()){
					CountrySpecificDOAMeta countrySpecificDOAMeta = new CountrySpecificDOAMeta();
					BeanUtils.copyProperties(countrySpecificDOAMetaVO, countrySpecificDOAMeta);
					attributeMetadata.getCountryCodeToMetaMap().put(countrySpecificDOAMeta.getCountryCodeFkId(), countrySpecificDOAMeta);
				}
			}
			entitymetadata.getAttributeMeta().add(attributeMetadata);
		}
//		for(MetadataRelationVO metadatRelationVO : metadataRelationVOlist){
//			EntityAttributeRelationMetadata entityAttributeRealtionMetadata = new EntityAttributeRelationMetadata();
//			BeanUtils.copyProperties(metadatRelationVO, entityAttributeRealtionMetadata);
//			if(pkIdMap.containsKey(entityAttributeRealtionMetadata.getDataApiInternalTableFkId())){
//				pkIdMap.get(entityAttributeRealtionMetadata.getDataApiInternalTableFkId()).getRelationMeta().add(entityAttributeRealtionMetadata);
//			}
//			else
//			{
//				LOG.debug("getDataApiInternalTableFkId "+ entityAttributeRealtionMetadata.getDataApiInternalTableFkId()+" is not found for entityAttributeMetadataPkId "+entityAttributeRealtionMetadata.getDataApiRelationTablePkId() );
//			}
//			//Check if the EntityAttributeMetadataFK exists in the map or not.
//			//If yes Add it to the corresponding EntityAttributeMetadata from the map.
//		}
		return entitymetadata;
	}
	
	@Override
	public MasterEntityMetadata getMetadataByDomainObject(String doName) {
		MtDomainObjectVO mtDomainObjectVO = metadataDAO.getMtDomainObjectByDo(doName);
		List<MtDoAttributeVO> pkIdMtDoAttributeMap = metadataDAO.getMtDoAttributeByDo(mtDomainObjectVO.getDomainObjectCode());
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		MasterEntityMetadata mstEntityMetadata = new MasterEntityMetadata();
		BeanUtils.copyProperties(mtDomainObjectVO, mstEntityMetadata);
		mstEntityMetadata.setDomainObjectIcon(metadataService.getIconDetailsByIconCode(mtDomainObjectVO.getDomainObjectIcon()));
		for(MtDoAttributeVO entry : pkIdMtDoAttributeMap){
			MasterEntityAttributeMetadata attributeMetadata = new MasterEntityAttributeMetadata();
			BeanUtils.copyProperties(entry, attributeMetadata);
			attributeMetadata.setDoAttributeIcon(metadataService.getIconDetailsByIconCode(entry.getDoAttributeIcon()));
			mstEntityMetadata.getAttributeMeta().add(attributeMetadata);
		}
		return mstEntityMetadata;
	}
	/* (non-Javadoc)
	 * @see com.tapplent.platformutility.metadata.MetadataService#getMetadataByMTPE(java.lang.String)
	 */
	@Override
	public MasterEntityMetadata getMetadataByMTPE(String mtPE) {
		String doCode = metadataDAO.getDOCodeByMTPE(mtPE);
		return getMetadataByDomainObject(doCode);
	}

	@Override
	@Transactional
	public EntityMetadata getMetadataByBtDo(String bt, String doName) {
		if(bt == null && doName == null)
			return null;
		BtDoSpecVO btDoSpecVO = metadataDAO.getBtDoSpec(bt, doName);
		if(btDoSpecVO==null){
			try {
				throw new Exception("This domainObject doesn't exist : " + doName+ " for base template "+bt);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		List<BtDoAttributeSpecVO> attributeSpecVOs = metadataDAO.getBtDoAttributeSpec(btDoSpecVO.getTemplateDOCode());
		EntityMetadata entityMetadata = new EntityMetadata();
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		BeanUtils.copyProperties(btDoSpecVO, entityMetadata);
		entityMetadata.setDoSummaryTitleIcon(metadataService.getIconDetailsByIconCode(btDoSpecVO.getDoSummaryTitleIcon()));
		for(BtDoAttributeSpecVO attributeSpecVO : attributeSpecVOs){
			EntityAttributeMetadata entityAttributeMetadata = new EntityAttributeMetadata();
			BeanUtils.copyProperties(attributeSpecVO, entityAttributeMetadata);
			entityAttributeMetadata.setAttrIcon(metadataService.getIconDetailsByIconCode(attributeSpecVO.getAttrIcon()));
			entityAttributeMetadata.setBooleanOnStateIcon(metadataService.getIconDetailsByIconCode(attributeSpecVO.getBooleanOnStateIcon()));
			entityAttributeMetadata.setBooleanOffStateIcon(metadataService.getIconDetailsByIconCode(attributeSpecVO.getBooleanOffStateIcon()));
			if(attributeSpecVO.getCountrySpecificDOAMetaVOList()!=null) {
				for (CountrySpecificDOAMetaVO countrySpecificDOAMetaVO :attributeSpecVO.getCountrySpecificDOAMetaVOList()){
					CountrySpecificDOAMeta countrySpecificDOAMeta = new CountrySpecificDOAMeta();
					BeanUtils.copyProperties(countrySpecificDOAMetaVO, countrySpecificDOAMeta);
					entityAttributeMetadata.getCountryCodeToMetaMap().put(countrySpecificDOAMeta.getCountryCodeFkId(), countrySpecificDOAMeta);
				}
			}
			entityMetadata.getAttributeMeta().add(entityAttributeMetadata);
		}
		return entityMetadata;
	}
	
	@Override
	public List<EntityAttributeMetadata> getBtAttributeMetaListByMtDoa(String doaCodeFkId) {
		List<BtDoAttributeSpecVO> attributeSpecVOs = metadataDAO.getBtDoAttributeSpecListByMtDoa(doaCodeFkId);
		List<EntityAttributeMetadata> entityAttributeMetadatas = new ArrayList<>();
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		for(BtDoAttributeSpecVO attributeSpecVO : attributeSpecVOs){
			EntityAttributeMetadata attributeMetadata = new EntityAttributeMetadata();
			BeanUtils.copyProperties(attributeSpecVO,attributeMetadata);
			attributeMetadata.setAttrIcon(metadataService.getIconDetailsByIconCode(attributeSpecVO.getAttrIcon()));
			attributeMetadata.setBooleanOnStateIcon(metadataService.getIconDetailsByIconCode(attributeSpecVO.getBooleanOnStateIcon()));
			attributeMetadata.setBooleanOffStateIcon(metadataService.getIconDetailsByIconCode(attributeSpecVO.getBooleanOffStateIcon()));
			if(attributeSpecVO.getCountrySpecificDOAMetaVOList()!=null) {
				for (CountrySpecificDOAMetaVO countrySpecificDOAMetaVO :attributeSpecVO.getCountrySpecificDOAMetaVOList()){
					CountrySpecificDOAMeta countrySpecificDOAMeta = new CountrySpecificDOAMeta();
					BeanUtils.copyProperties(countrySpecificDOAMetaVO, countrySpecificDOAMeta);
					attributeMetadata.getCountryCodeToMetaMap().put(countrySpecificDOAMeta.getCountryCodeFkId(), countrySpecificDOAMeta);
				}
			}
			entityAttributeMetadatas.add(attributeMetadata);
		}
		return entityAttributeMetadatas;
	}
	@Override
	public AttributePaths getAttributePath(String baseTemplateId, String mtPE, RequestSelectType selectType) {
		AttributePaths result = new AttributePaths();
		List<AttributePathDetails> returnAttributePaths = new ArrayList<>();
		switch(selectType){
			case LAYOUT:
				returnAttributePaths = metadataDAO.getControlAttributePaths(baseTemplateId, mtPE);
				break;
			case RELATION:
				returnAttributePaths = metadataDAO.getRelationControlAttributePaths(baseTemplateId, mtPE);
				break;
			case HIERARCHY:
				returnAttributePaths = metadataDAO.getHierarchyControlAttributePaths(baseTemplateId, mtPE);
				break;
			case ExternalAPI:
				returnAttributePaths = metadataDAO.getExternalAPIAttributePaths(baseTemplateId, mtPE);
				break;
			default: return null;
		}
		addAttributePaths(result, returnAttributePaths);
		return result;
	}
	/**
	 * @param result
	 * @param attributePathsList
	 */
	private void addAttributePaths(AttributePaths result, List<AttributePathDetails> attributePathsList) {
		/* First add the normal attribute path */
		for(AttributePathDetails pathDetails : attributePathsList){
//			if(StringUtil.isDefined(pathDetails.getAggregateFunction())){
			if(1==0){
//				/* It is a aggregate attribute path */
//				Map<String, Map<String, AttributePathDetails>> attributePathToAggFunctToDetailsMap = result.getAttributePathToAggDetailsMap();
//				String aggregateFunction = pathDetails.getAggregateFunction();
//				String aggregateAttributePath = pathDetails.getAttributePath();
//				if(attributePathToAggFunctToDetailsMap.containsKey(aggregateAttributePath)){
//					attributePathToAggFunctToDetailsMap.get(aggregateAttributePath).put(aggregateFunction, pathDetails);
//				}else{
//					Map<String, AttributePathDetails> functionToDetailsMap = new HashMap<>();
//					functionToDetailsMap.put(aggregateFunction, pathDetails);
//					attributePathToAggFunctToDetailsMap.put(aggregateAttributePath, functionToDetailsMap);
			}else if(pathDetails.isStickyHeaderSort()){
				SortData sortData = new SortData(pathDetails.getAttributePath(),true, pathDetails.getStickyHeaderSortSeq(),pathDetails.getOrderBYValues());
			}else{
				/* Non aggregate attribute path */
				Set<String> nonAggregateAttributePath = result.getAttributePaths();
				if(StringUtil.isDefined(pathDetails.getAttributePath()))
					nonAggregateAttributePath.add(pathDetails.getAttributePath());
			}
		}
	}
	@Override
	public List<DataActions> getDataActionsList() {
		List<DataActions> result = new ArrayList<>();
		List<DataActionsVO> dataActionsVO = metadataDAO.getDataActions();
		for(DataActionsVO actionsVO : dataActionsVO){
			DataActions dataActions = DataActions.fromVO(actionsVO);
			result.add(dataActions);
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.tapplent.platformutility.metadata.MetadataService#getBTAttributeList()
	 */
	@Override
	public List<EntityAttributeMetadata> getBTAttributeList() {
		List<EntityAttributeMetadata> result = new ArrayList<>();
		List<BtDoAttributeSpecVO> btDOAs = metadataDAO.getBTDOAtrributeList();
		MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
		for(BtDoAttributeSpecVO btDOAVO : btDOAs){
			EntityAttributeMetadata btDOA = new EntityAttributeMetadata();
			BeanUtils.copyProperties(btDOAVO, btDOA);
			btDOA.setAttrIcon(metadataService.getIconDetailsByIconCode(btDOAVO.getAttrIcon()));
			btDOA.setBooleanOnStateIcon(metadataService.getIconDetailsByIconCode(btDOAVO.getBooleanOnStateIcon()));
			btDOA.setBooleanOffStateIcon(metadataService.getIconDetailsByIconCode(btDOAVO.getBooleanOffStateIcon()));
			result.add(btDOA);
		}
		return result;
	}

	@Override
	public Map<String, IconVO> getIconMap() {
		return metadataDAO.getIconMap();
	}

	@Override
	public Map<String, MTPE> getMtPEMap() {
		Map<String, MTPE> resultMap = new HashMap<>();
		List<MTPEVO> mtpevoList = metadataDAO.getMtPEMap();
		for(MTPEVO mtpevo : mtpevoList){
			MTPE mtpe = MTPE.fromVO(mtpevo);
			resultMap.put(mtpe.getMetaProcessElementCodePkId(), mtpe);
		}
		return resultMap;
	}

	@Override
	public List<String> getDependencyKeyRelatedPEs(String mtPE) {
		return metadataDAO.getDependencyKeyRelatedPEs(mtPE);
	}

	@Override
	public List<String> getForeignKeyRelatedPEs(String mtPE) {
		return metadataDAO.getForeignKeyRelatedPEs(mtPE);
	}

	public TemplateMapper getTemaplateMapperByMtPE(String mtPE) {
		TemplateMapperVO templateMapperVO = metadataDAO.getTemplateMapperByMtPE(mtPE);
		TemplateMapper templateMapper = new TemplateMapper();
		BeanUtils.copyProperties(templateMapperVO, templateMapper);
		return templateMapper;
	}

	@Override
	public Map<String, PropertyControlMaster> getPropertyControlMap() {
		return metadataDAO.getPropertyControlMap();
	}

	@Override
	@Transactional
	public CountrySpecificDOAMeta getCountrySpecificDOAMeta(String btDoSpecId, String baseCountryCodeFkId) {
		CountrySpecificDOAMetaVO countrySpecificDOAMetaVO = metadataDAO.getCountrySpecificDOAMeta(btDoSpecId, baseCountryCodeFkId);
		if(countrySpecificDOAMetaVO!=null){
			CountrySpecificDOAMeta countrySpecificDOAMeta = CountrySpecificDOAMeta.fromVO(countrySpecificDOAMetaVO);
			MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
			countrySpecificDOAMeta.setDoaDisplayIconCodeFkId(metadataService.getIconDetailsByIconCode(countrySpecificDOAMetaVO.getDoaDisplayIconCodeFkId()));
			return countrySpecificDOAMeta;
		}else{
			return null;
		}
	}

	@Override
	public AuditMsgFormat getAuditMsgFormatByMtPEAndAction(String activityLogActionCode, String mtPE) {
		AuditMsgFormat auditMsgFormat = metadataDAO.getAuditMsgFormatByMtPEAndAction(activityLogActionCode, mtPE);
		return auditMsgFormat;
	}

	@Override
	public String getSubjectUserId(StringBuilder sql, String dbDataTypeCode) {
		return metadataDAO.getSubjectUserId(sql, dbDataTypeCode);
	}

	@Override
	public UiSetting getUiSetting() {
		UiSettingVO uiSettingVO = metadataDAO.getUiSetting();
		UiSetting uiSetting = UiSetting.fromVO(uiSettingVO);
		return uiSetting;
	}

	@Override
	public List<LayoutThemeMapper> getLayoutThemeMapperList() {
		List<LayoutThemeMapper> result = new ArrayList<>();
		List<LayoutThemeMapperVO> layoutThemeMapperVOs = metadataDAO.getLayoutThemeMapperVOs();
		for(LayoutThemeMapperVO layoutThemeMapperVO : layoutThemeMapperVOs){
			LayoutThemeMapper layoutThemeMapper = LayoutThemeMapper.fromVO(layoutThemeMapperVO);
			result.add(layoutThemeMapper);
		}
		return result;
	}

	@Override
	@Transactional
	public List<LicencedLocalesVO> getLicencedLocale() {
		return metadataDAO.getLicencedLocale();
	}

	@Override
	public Map<String, List<RowCondition>> getRowConditionsForAllMTPEs() {
		Map<String, List<RowCondition>> result = new HashMap<>();
		Map<String, RowCondition> mtPEToAllButSelfConditions = new HashMap<>();
		List<RowCondition> rowConditions = metadataDAO.getRowConditionsForAllMTPEs();
		for(RowCondition rowCondition : rowConditions){
			if(result.containsKey(rowCondition.getWhatMtPECodeFkId())){
				result.get(rowCondition.getWhatMtPECodeFkId()).add(rowCondition);
			}else{
				List<RowCondition> newRowConditions = new ArrayList<>();
				newRowConditions.add(rowCondition);
				result.put(rowCondition.getWhatMtPECodeFkId(), newRowConditions);
			}
			if(rowCondition.isAllButSelfPermissioned()){
				mtPEToAllButSelfConditions.put(rowCondition.getRowCondnPkId(), rowCondition);
			}
		}
		// For all All but self permissioned get the Column conditions
		populateColumnConditions(mtPEToAllButSelfConditions);
		return result;
	}

	@Override
	public void insertLabelAlias(String label, Integer currentMaxAlias) throws SQLException {
		metadataDAO.insertLabelAlias(label, currentMaxAlias);
	}

	@Override
	public Map<String, Integer> getLabelToAliasMap() {
		return metadataDAO.getLabelAlias();
	}

	@Override
	@Transactional
	public NameCalculationRuleVO getNameCalculationRule(String localeCode, String nameType) {
		return metadataDAO.getNameCalculationRule(localeCode, nameType);
	}

	@Override
	public List<AnalyticsDOAControlMap> getAnalyticsDoaToControlMap() {
		return metadataDAO.getAnalyticsDoaToControlMap();
	}

	private void populateColumnConditions(Map<String, RowCondition> mtPEToAllButSelfConditions) {
		Map<String, List<AMColCondition>> rowPkToColConditionMap = metadataDAO.getColumnConditions(mtPEToAllButSelfConditions.keySet());
		for(Map.Entry<String, RowCondition> entry : mtPEToAllButSelfConditions.entrySet()){
			String rowConditionPkID = entry.getKey();
			entry.getValue().getColumnConditionList().addAll(rowPkToColConditionMap.get(rowConditionPkID));
		}
	}
}
