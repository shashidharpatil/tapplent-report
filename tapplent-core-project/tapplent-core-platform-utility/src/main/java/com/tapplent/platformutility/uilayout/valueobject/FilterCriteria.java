package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 27/12/16.
 */
public class FilterCriteria {

    private String filterCriteriaPkId;
    private String filterFkId;
    private String controlAttrExpn;
    private String operFkId;
    private String valueExpn;
    private int seqNumPosInt;

    public String getFilterCriteriaPkId() {
        return filterCriteriaPkId;
    }

    public void setFilterCriteriaPkId(String filterCriteriaPkId) {
        this.filterCriteriaPkId = filterCriteriaPkId;
    }

    public String getFilterFkId() {
        return filterFkId;
    }

    public void setFilterFkId(String filterFkId) {
        this.filterFkId = filterFkId;
    }

    public String getControlAttrExpn() {
        return controlAttrExpn;
    }

    public void setControlAttrExpn(String controlAttrExpn) {
        this.controlAttrExpn = controlAttrExpn;
    }

    public String getOperFkId() {
        return operFkId;
    }

    public void setOperFkId(String operFkId) {
        this.operFkId = operFkId;
    }

    public String getValueExpn() {
        return valueExpn;
    }

    public void setValueExpn(String valueExpn) {
        this.valueExpn = valueExpn;
    }

    public int getSeqNumPosInt() {
        return seqNumPosInt;
    }

    public void setSeqNumPosInt(int seqNumPosInt) {
        this.seqNumPosInt = seqNumPosInt;
    }
}
