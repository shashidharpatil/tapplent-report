package com.tapplent.platformutility.hierarchy.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by tapplent on 03/02/17.
 *
 * Read relation as subjectPersonId's
 */
public class Relation {
    private String relationshipType;
    private int level;
    @JsonIgnore
    private String subjectPersonId;
    private String relatedPersonId;

    public String getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(String relationshipType) {
        this.relationshipType = relationshipType;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getSubjectPersonId() {
        return subjectPersonId;
    }

    public void setSubjectPersonId(String subjectPersonId) {
        this.subjectPersonId = subjectPersonId;
    }
}
