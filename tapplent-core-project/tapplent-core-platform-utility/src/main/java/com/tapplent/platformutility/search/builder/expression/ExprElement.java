package com.tapplent.platformutility.search.builder.expression;

import java.util.List;

public interface ExprElement {
	public void addChildElement(ExprElement element);
	public List<ExprElement> getChildELements();
}
