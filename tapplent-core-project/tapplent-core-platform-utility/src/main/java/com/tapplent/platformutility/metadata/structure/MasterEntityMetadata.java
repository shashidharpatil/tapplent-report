package com.tapplent.platformutility.metadata.structure;

import java.util.ArrayList;
import java.util.List;

import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.metadata.MtDomainObjectVO;

public class MasterEntityMetadata {
	private String versionId;
	private String domainObjectCode;
	private String domainObjectName;
	private IconVO domainObjectIcon;
	private String baseObjectId;
	private String dbTableName;
	private String doTypeCode;
	private boolean isCustomizable;
	private boolean isCommon;
	private boolean isAccessControlGoverned;
	private String subjectPersonDoaFullPath;
	private String activityPE;
	private String activityDetailsPE;
	private String tagPE;
	private String bookmarkPE;
	private	String featuredPE;
	private String rowIntermediateMtPE;
	private String rowResolvedMtPE;
	private String colResolvedMtPE;
	private String endorseMtPE;

	private List<MasterEntityAttributeMetadata> attributeMeta;
	public MasterEntityMetadata(){
		this.attributeMeta = new ArrayList<>();
	}
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public boolean isCustomizable() {
		return isCustomizable;
	}
	public void setCustomizable(boolean isCustomizable) {
		this.isCustomizable = isCustomizable;
	}
	public boolean isCommon() {
		return isCommon;
	}
	public void setCommon(boolean isCommon) {
		this.isCommon = isCommon;
	}
	public boolean isAccessControlGoverned() {
		return isAccessControlGoverned;
	}
	public void setAccessControlGoverned(boolean isAccessControlGoverned) {
		this.isAccessControlGoverned = isAccessControlGoverned;
	}
	public String getDomainObjectCode() {
		return domainObjectCode;
	}
	public void setDomainObjectCode(String domainObjectCode) {
		this.domainObjectCode = domainObjectCode;
	}
	public String getDomainObjectName() {
		return Util.getG11nValue(domainObjectName, null);
	}
	public void setDomainObjectName(String domainObjectName) {
		this.domainObjectName = domainObjectName;
	}

	public IconVO getDomainObjectIcon() {
		return domainObjectIcon;
	}

	public void setDomainObjectIcon(IconVO domainObjectIcon) {
		this.domainObjectIcon = domainObjectIcon;
	}

	public String getBaseObjectId() {
		return baseObjectId;
	}
	public void setBaseObjectId(String baseObjectId) {
		this.baseObjectId = baseObjectId;
	}
	public String getDbTableName() {
		return dbTableName;
	}
	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}
	public String getDoTypeCode() {
		return doTypeCode;
	}
	public void setDoTypeCode(String doTypeCode) {
		this.doTypeCode = doTypeCode;
	}
	public String getSubjectPersonDoaFullPath() {
		return subjectPersonDoaFullPath;
	}
	public void setSubjectPersonDoaFullPath(String subjectPersonDoaFullPath) {
		this.subjectPersonDoaFullPath = subjectPersonDoaFullPath;
	}
	public List<MasterEntityAttributeMetadata> getAttributeMeta() {
		return attributeMeta;
	}
	public void setAttributeMeta(List<MasterEntityAttributeMetadata> attributeMeta) {
		this.attributeMeta = attributeMeta;
	}

	public String getActivityPE() {
		return activityPE;
	}

	public void setActivityPE(String activityPE) {
		this.activityPE = activityPE;
	}

	public String getActivityDetailsPE() {
		return activityDetailsPE;
	}

	public void setActivityDetailsPE(String activityDetailsPE) {
		this.activityDetailsPE = activityDetailsPE;
	}

	public String getTagPE() {
		return tagPE;
	}

	public void setTagPE(String tagPE) {
		this.tagPE = tagPE;
	}

	public String getBookmarkPE() {
		return bookmarkPE;
	}

	public void setBookmarkPE(String bookmarkPE) {
		this.bookmarkPE = bookmarkPE;
	}

	public String getFeaturedPE() {
		return featuredPE;
	}

	public void setFeaturedPE(String featuredPE) {
		this.featuredPE = featuredPE;
	}

	public String getRowIntermediateMtPE() {
		return rowIntermediateMtPE;
	}

	public void setRowIntermediateMtPE(String rowIntermediateMtPE) {
		this.rowIntermediateMtPE = rowIntermediateMtPE;
	}

	public String getRowResolvedMtPE() {
		return rowResolvedMtPE;
	}

	public void setRowResolvedMtPE(String rowResolvedMtPE) {
		this.rowResolvedMtPE = rowResolvedMtPE;
	}

	public String getColResolvedMtPE() {
		return colResolvedMtPE;
	}

	public void setColResolvedMtPE(String colResolvedMtPE) {
		this.colResolvedMtPE = colResolvedMtPE;
	}

	public String getEndorseMtPE() {
		return endorseMtPE;
	}

	public void setEndorseMtPE(String endorseMtPE) {
		this.endorseMtPE = endorseMtPE;
	}

	public static MasterEntityMetadata fromVO(MtDomainObjectVO mtDomainObjectVO){
		MasterEntityMetadata masterEntityMetadata = new MasterEntityMetadata();
		BeanUtils.copyProperties(mtDomainObjectVO, masterEntityMetadata);
		return masterEntityMetadata;
	}
}
