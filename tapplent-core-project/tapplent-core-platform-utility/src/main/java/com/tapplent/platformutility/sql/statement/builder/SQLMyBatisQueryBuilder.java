package com.tapplent.platformutility.sql.statement.builder;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.jdbc.SQL;

public class SQLMyBatisQueryBuilder {
	
	SQL sql = new SQL();

	public SQLMyBatisQueryBuilder addFrom(String fromClause) {
		sql.FROM(fromClause);
		return this;
	}

	public void addWhere(String whereExpression) {
		if (StringUtils.isNotEmpty(whereExpression)) {
			sql.WHERE(whereExpression);
		}
	}

	public void addOrderBy(String key) {
		if (StringUtils.isNotEmpty(key)) {
			sql.ORDER_BY(key);
		}

	}

	public String getQueryString() {
		String query = sql.toString();
		return query;
	}

	public void addSelect(String select) {
		if (StringUtils.isNotEmpty(select)) {
			sql.SELECT(select);
		}

	}

	public void addInsert(String tableName) {
		sql.INSERT_INTO(tableName);
	}

	public void addValues(String columns, String values) {
		sql.VALUES(columns, values);
	}

	public void addInnerJoin(String innerJoin) {
		sql.INNER_JOIN(innerJoin);
	}

	public void addDelete(String tableName) {
		sql.DELETE_FROM(tableName);
	}

	public void addUpdate(String entityName) {
		sql.UPDATE(entityName);
	}

	public void addSet(String set) {
		sql.SET(set);
	}
}
