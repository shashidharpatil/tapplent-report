package com.tapplent.platformutility.thirdPartyApp.facebook;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.POST_PostData;
import com.tapplent.platformutility.batch.model.POST_SocialDoaMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;


public class FacebookNotification {
	
	//This function will retrieve the basic profile of the facebook user
	public void getUserProfileDetails(POST_PostData obj) {
	
          
          try{	
			    JSONObject object = (JSONObject) obj.getPersonCredential();
				String access_token = object.getString("access_token");
				StringBuilder url=new StringBuilder(Constants.facebookProfileUrl);
				url.append(access_token);
				
			        HttpGet get=new HttpGet(url.toString());
					HttpClient client = HttpClientBuilder.create().build();
				    HttpResponse response;
					response=client.execute(get);
					 
				} catch (ClientProtocolException e2) {
					e2.printStackTrace();
				} catch (IOException e2) {
					e2.printStackTrace();
				}  catch (JSONException e) {
					e.printStackTrace();
				}
	    return ;
	   
	}
	
	//This function will fetch the value for the parameter
	public static String getData(String Key , List<POST_SocialDoaMap> doaMapList, ResultSet resultset) throws SQLException{
		
		String data = "";
		TreeMap<Integer,String> map = new TreeMap<Integer,String>();
		for(POST_SocialDoaMap doaMap: doaMapList){
			if(doaMap.getSocailAccountElementCodeFkId().equals(Key)){
			String value = resultset.getString(doaMap.getMapDoaExpn());
			map.put(doaMap.getSequenceNumberPosInt(), value);
			}
		}
		for (Entry<Integer, String> entry : map.entrySet())
		{
		    data = data+ entry.getValue();
		}
		
		return data;
	}
	//This function will check the response
	public static boolean responseCheck(int status_code){
		int status_codes[] = {200,201,202,203,204,205,206,207,208,226};
		boolean flag=false;
		for(int code:status_codes){
			if(status_code==code){
				flag=true;
				break;
			}else{
				flag= false;
			}
		}
		return flag;
	}
	
//This function will POST  on the User Wall
	public void postOnUserWall(POST_PostData obj) {

		
		try {
			JSONObject object = (JSONObject) obj.getPersonCredential();
			String access_token = object.getString("access_token");
			List<POST_SocialDoaMap> list = obj.getDoaMap();
			ResultSet rs = obj.getResultSet();
			String Key = "link";
			String link = getData(Key, list, rs);

			if (!link.equals("")) {

				String key[] = { "message", "link", "picture", "name", "caption", "description" };
				Map<String, String> elementMap = new HashMap<String, String>();
				for (int i = 0; i < key.length; i++) {
					String value = getData(key[i], list, rs);
					elementMap.put(key[i], value);
				}
				if (!elementMap.get("message").equals("")) {

					StringBuilder url = new StringBuilder(Constants.facebookPostImageUrl);
					url.append("?message=").append(elementMap.get("message"));
					if (!elementMap.get("link").equals("")) {

						url.append("&link=").append(elementMap.get("link"));
						if (!elementMap.get("picture").equals("")) {
							url.append(elementMap.get("picture"));
						}
						if (!elementMap.get("name").equals("")) {
							url.append("&name=").append(elementMap.get("name"));
						}
						if (!elementMap.get("caption").equals("")) {
							url.append("&caption=").append(elementMap.get("caption"));
						}
						if (!elementMap.get("description").equals("")) {
							url.append("&description=").append(elementMap.get("description"));
						}
						HttpPost post = new HttpPost(url.toString());
						post.addHeader("Authorization", "OAuth "+access_token);
						HttpClient client = HttpClientBuilder.create().build();
						HttpResponse response;
						response = client.execute(post);
						int status_code = response.getStatusLine().getStatusCode();
						boolean flag = responseCheck(status_code);
						if(flag){
							obj.setSuccess(flag);
						} else{
							obj.setError(response);
						}
						
					} else {
						HttpPost post = new HttpPost(url.toString());
						post.addHeader("Authorization", "OAuth "+access_token);
						HttpClient client = HttpClientBuilder.create().build();
						HttpResponse response;
						response = client.execute(post);
						response = client.execute(post);
						int status_code = response.getStatusLine().getStatusCode();
						boolean flag = responseCheck(status_code);
						if(flag){
							obj.setSuccess(flag);
						} else{
							obj.setError(response);
						}
					    

					}
				} else {
					// Error will be thrown "Invalid Content";
				}
				return;
			} else {
				
				String key = "url";
				String image_url = getData(key, list, rs);
				key = "caption";
				String caption = getData(key, list, rs);
				StringBuilder URL = new StringBuilder(Constants.facebookPostImageUrl);
				if (!image_url.equals("")) {
					URL.append("?url=").append(image_url);
					if (!caption.equals("")) {
						URL.append("&caption=").append(caption);
					}
					HttpPost post = new HttpPost(URL.toString());
					post.addHeader("Authorization", "OAuth " + access_token);
					HttpClient client = HttpClientBuilder.create().build();
					HttpResponse response;
					response = client.execute(post);
					response = client.execute(post);
					int status_code = response.getStatusLine().getStatusCode();
					boolean flag = responseCheck(status_code);
					if(flag){
						obj.setSuccess(flag);
					} else{
						obj.setError(response);
					}
				} else {
					// Error will be thrown"Please Enter the required fields"
				}

				return ;

			}
		} catch (ClientProtocolException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ;

	}
		
	//This function will POST on the Company Page
	public void postOnCompanyPage(POST_PostData obj){
		
		try {
			JSONObject object = (JSONObject) obj.getPersonCredential();
			String page_token = object.getString("access_token");
			String page_id = object.getString("page_id");
			List<POST_SocialDoaMap> list = obj.getDoaMap();
			ResultSet rs = obj.getResultSet();
			String Key = "link";
			String link = getData(Key, list, rs);

			if (!link.equals("")) {

				
				String key[] = { "message", "link", "picture", "name", "caption", "description" };
				Map<String, String> elementMap = new HashMap<String, String>();
				for (int i = 0; i < key.length; i++) {
					String value = getData(key[i], list, rs);
					elementMap.put(key[i], value);
				}
				if (!elementMap.get("message").equals("")) {

					StringBuilder url = new StringBuilder(Constants.facebookCompanyPostUrl);
					url.append(page_id).append("/feed");
					url.append("?message=").append(elementMap.get("message"));
					if (!elementMap.get("link").equals("")) {

						url.append("&link=").append(elementMap.get("link"));
						if (!elementMap.get("picture").equals("")) {
							url.append(elementMap.get("picture"));
						}
						if (!elementMap.get("name").equals("")) {
							url.append("&name=").append(elementMap.get("name"));
						}
						if (!elementMap.get("caption").equals("")) {
							url.append("&caption=").append(elementMap.get("caption"));
						}
						if (!elementMap.get("description").equals("")) {
							url.append("&description=").append(elementMap.get("description"));
						}
						HttpPost post = new HttpPost(url.toString());
						post.addHeader("Authorization", "OAuth "+page_token);
						HttpClient client = HttpClientBuilder.create().build();
						HttpResponse response;
						response = client.execute(post);
						response = client.execute(post);
						int status_code = response.getStatusLine().getStatusCode();
						boolean flag = responseCheck(status_code);
						if(flag){
							obj.setSuccess(flag);
						} else{
							obj.setError(response);
						}

					} else {
						HttpPost post = new HttpPost(url.toString());
						post.addHeader("Authorization", "OAuth "+page_token);
						HttpClient client = HttpClientBuilder.create().build();
						HttpResponse response;
						response = client.execute(post);
						int status_code = response.getStatusLine().getStatusCode();
						boolean flag = responseCheck(status_code);
						if(flag){
							obj.setSuccess(flag);
						} else{
							obj.setError(response);
						}

					}
				} else {
					// Error will be thrown "Invalid Content"
				}
				return ;
			} else {
				
				String key = "url";
				String image_url = getData(key, list, rs);
				key = "caption";
				String caption = getData(key, list, rs);
				StringBuilder URL = new StringBuilder(Constants.facebookCompanyPostUrl);
				URL.append(page_id).append("/photos");
				if (!image_url.equals("")) {
					URL.append("?url=").append(image_url);
					if (!caption.equals("")) {
						URL.append("&caption=").append(caption);
					}
					HttpPost post = new HttpPost(URL.toString());
					post.addHeader("Authorization", "OAuth " + page_token);
					HttpClient client = HttpClientBuilder.create().build();
					HttpResponse response;
					response = client.execute(post);
					response = client.execute(post);
					int status_code = response.getStatusLine().getStatusCode();
					boolean flag = responseCheck(status_code);
					if(flag){
						obj.setSuccess(flag);
					} else{
						obj.setError(response);
					}
				} else {
					// Error will be thrown"Please Enter the url"
				}

				return ;

			}
		} catch (ClientProtocolException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ;

		
	}
		
	//This function will GET the long_lived_token from the short_lived_token
	public JSONObject getLongLivedAccessToken(POST_PostData obj) throws JSONException {
		JSONObject jsonresponse = new JSONObject();
		JSONObject object = (JSONObject) obj.getPersonCredential();
		String short_lived_token = object.getString("access_token");
		String client_id = object.getString("client_id");
		String client_secret = object.getString("client_secret");
		StringBuilder url=new StringBuilder(Constants.facebookExchangeTokenUrl);
		url.append("&client_id").append(client_id).append("&client_secret").append(client_secret).append("&fb_exchange_token").append(short_lived_token);
	try {	
		HttpGet get=new HttpGet(url.toString());
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response;
		response = client.execute(get);
		HttpEntity entity = response.getEntity();
		String responseString = EntityUtils.toString(entity);
		jsonresponse = new JSONObject(responseString);
	  } catch (ClientProtocolException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}  catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonresponse;
		}
	//This function will refresh the long_lived_token
	public JSONObject refreshLongLivedToken(POST_PostData obj) throws JSONException {
		JSONObject jsonresponse = new JSONObject();
		JSONObject object = (JSONObject) obj.getPersonCredential();
		String long_lived_token = object.getString("access_token");
		String client_id = object.getString("client_id");
		String client_secret = object.getString("client_secret");
		String redirect_uri = object.getString("redirect_uri");
		StringBuilder url = new StringBuilder(Constants.facebookRefreshTokenUrl);
		url.append(long_lived_token).append("&client_secret=").append(client_secret).append("&redirect_uri=").append(redirect_uri);
		url.append("&client_id=").append(client_id);
	try{	
		HttpGet get=new HttpGet(url.toString());
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response;
		response=client.execute(get);
		//This response will contain Authorization code,Which should be exchanged to get long_lived_token
		HttpEntity entity=response.getEntity();
		String responseString=EntityUtils.toString(entity);
		jsonresponse = new JSONObject(responseString);
		String code = jsonresponse.getString("code");
		StringBuilder URL = new StringBuilder("https://graph.facebook.com/v2.6/oauth/access_token?");
		URL.append("client_id=").append(client_id);
		URL.append("&redirect_uri=").append(redirect_uri);
		URL.append("&client_secret=").append(client_secret);
		URL.append("&code=").append(code);
		HttpGet get1=new HttpGet(URL.toString());
		HttpClient client1 = HttpClientBuilder.create().build();
		HttpResponse response1;
		response1=client1.execute(get1);
		HttpEntity entity1=response1.getEntity();
		String responseString1=EntityUtils.toString(entity1);
		JSONObject jsonresponse1 = new JSONObject(responseString1);
		jsonresponse = new JSONObject(jsonresponse1);
	  } catch (ClientProtocolException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}  catch (JSONException e) {
			e.printStackTrace();
		} 
		
	return jsonresponse;
	}
	
	
	
	//This function will delete the published post
	public JSONObject deleteThePost(POST_PostData obj) throws JSONException{
		
		JSONObject jsonresponse = new JSONObject();
		JSONObject object = (JSONObject) obj.getPersonCredential();
		String access_token = object.getString("access_token");
		String POST_ID = object.getString("post_id");
		StringBuilder url = new StringBuilder(Constants.facebookDelete_updateUrl);
		url.append(POST_ID);
try {	HttpDelete delete = new HttpDelete(url.toString());
		delete.addHeader("Authorization", "OAuth "+access_token);
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response;
		response = client.execute(delete);
		HttpEntity entity = response.getEntity();
		String stringResponse = EntityUtils.toString(entity);
		jsonresponse = new JSONObject(stringResponse);
	 } catch (ClientProtocolException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}  catch (JSONException e) {
			e.printStackTrace();
		}
  return jsonresponse;
	}
 //This function will update the content of the POST
	public JSONObject updateThePost(POST_PostData obj) throws JSONException{
		
		
		String message = "HI.tapplent";
		JSONObject jsonresponse = new JSONObject();
		JSONObject object = (JSONObject) obj.getPersonCredential();
		String access_token = object.getString("access_token");
		String POST_ID = object.getString("post_id");
		StringBuilder url = new StringBuilder(Constants.facebookDelete_updateUrl);
		url.append(POST_ID);
		url.append("?message=").append(message);
	try{
		HttpPost post = new HttpPost(url.toString());
		post.addHeader("Authorization","OAuth "+access_token);
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response;
		response = client.execute(post);
		HttpEntity entity=response.getEntity();
		String responseString=EntityUtils.toString(entity);
		jsonresponse = new JSONObject(responseString);
		
        } catch (ClientProtocolException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}  catch (JSONException e) {
			e.printStackTrace();
		}
	return jsonresponse;
		
	}
//This function will check the validity of the access_token,If token is valid then it returns true else return false
	public boolean isTokenValid(POST_PostData obj) throws JSONException{
		
	JSONObject jsonresponse = new JSONObject();
	JSONObject object = (JSONObject) obj.getPersonCredential();
	String access_token = object.getString("access_token");
	String app_access_token = "";
	boolean flag = false;
try {
	StringBuilder url = new StringBuilder("https://graph.facebook.com/debug_token?input_token=");
	url.append(access_token);
	url.append("&access_token=").append(app_access_token);
	HttpGet get = new HttpGet(url.toString());
	HttpClient client = HttpClientBuilder.create().build();
	HttpResponse response;
	response = client.execute(get);
	HttpEntity entity=response.getEntity();
	String responseString=EntityUtils.toString(entity);
	jsonresponse = new JSONObject(responseString);
	if(jsonresponse.getString("is_valid").equals("true")){
		flag = true;//Token is valid
	}else{
		flag = false;//Token is invalid
	}
	} catch (ClientProtocolException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} catch (JSONException e) {
		e.printStackTrace();
	}
	
	return flag;
	}
}
