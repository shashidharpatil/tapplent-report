package com.tapplent.platformutility.layout.valueObject;

import java.util.Date;

/**
 * Created by tapplent on 15/11/16.
 */
public class ProcessHomePageIntroContentVO {
    private String prcIntroContentPkId;
    private String loggedInPersonPkId;
    private String prcIntroContentRuleFkId;
    private String processTypeCodeFkId;
    private String introImageId;
    private String introTitleG11nBigTxt;
    private String introSubTextG11nBigTxt;
    private String linkedStaticExtUrlTxt;
    private String linkedResolvedTargetScreenInstanceFkId;
    private String linkedResolvedTargetScreenSectionInstanceFkId;
    private int linkedResolvedTargetTabSeqNumPosInt;
    private String linkedDynamicActionFilterExpn;
    private Boolean isDeleted;
    private Date lastModifiedDatetime;

    public String getPrcIntroContentPkId() {
        return prcIntroContentPkId;
    }

    public void setPrcIntroContentPkId(String prcIntroContentPkId) {
        this.prcIntroContentPkId = prcIntroContentPkId;
    }

    public String getLoggedInPersonPkId() {
        return loggedInPersonPkId;
    }

    public void setLoggedInPersonPkId(String loggedInPersonPkId) {
        this.loggedInPersonPkId = loggedInPersonPkId;
    }

    public String getPrcIntroContentRuleFkId() {
        return prcIntroContentRuleFkId;
    }

    public void setPrcIntroContentRuleFkId(String prcIntroContentRuleFkId) {
        this.prcIntroContentRuleFkId = prcIntroContentRuleFkId;
    }

    public String getProcessTypeCodeFkId() {
        return processTypeCodeFkId;
    }

    public void setProcessTypeCodeFkId(String processTypeCodeFkId) {
        this.processTypeCodeFkId = processTypeCodeFkId;
    }

    public String getIntroImageId() {
        return introImageId;
    }

    public void setIntroImageId(String introImageId) {
        this.introImageId = introImageId;
    }

    public String getIntroTitleG11nBigTxt() {
        return introTitleG11nBigTxt;
    }

    public void setIntroTitleG11nBigTxt(String introTitleG11nBigTxt) {
        this.introTitleG11nBigTxt = introTitleG11nBigTxt;
    }

    public String getIntroSubTextG11nBigTxt() {
        return introSubTextG11nBigTxt;
    }

    public void setIntroSubTextG11nBigTxt(String introSubTextG11nBigTxt) {
        this.introSubTextG11nBigTxt = introSubTextG11nBigTxt;
    }

    public String getLinkedStaticExtUrlTxt() {
        return linkedStaticExtUrlTxt;
    }

    public void setLinkedStaticExtUrlTxt(String linkedStaticExtUrlTxt) {
        this.linkedStaticExtUrlTxt = linkedStaticExtUrlTxt;
    }

    public String getLinkedResolvedTargetScreenInstanceFkId() {
        return linkedResolvedTargetScreenInstanceFkId;
    }

    public void setLinkedResolvedTargetScreenInstanceFkId(String linkedResolvedTargetScreenInstanceFkId) {
        this.linkedResolvedTargetScreenInstanceFkId = linkedResolvedTargetScreenInstanceFkId;
    }

    public String getLinkedResolvedTargetScreenSectionInstanceFkId() {
        return linkedResolvedTargetScreenSectionInstanceFkId;
    }

    public void setLinkedResolvedTargetScreenSectionInstanceFkId(String linkedResolvedTargetScreenSectionInstanceFkId) {
        this.linkedResolvedTargetScreenSectionInstanceFkId = linkedResolvedTargetScreenSectionInstanceFkId;
    }

    public int getLinkedResolvedTargetTabSeqNumPosInt() {
        return linkedResolvedTargetTabSeqNumPosInt;
    }

    public void setLinkedResolvedTargetTabSeqNumPosInt(int linkedResolvedTargetTabSeqNumPosInt) {
        this.linkedResolvedTargetTabSeqNumPosInt = linkedResolvedTargetTabSeqNumPosInt;
    }

    public String getLinkedDynamicActionFilterExpn() {
        return linkedDynamicActionFilterExpn;
    }

    public void setLinkedDynamicActionFilterExpn(String linkedDynamicActionFilterExpn) {
        this.linkedDynamicActionFilterExpn = linkedDynamicActionFilterExpn;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Date getLastModifiedDatetime() {
        return lastModifiedDatetime;
    }

    public void setLastModifiedDatetime(Date lastModifiedDatetime) {
        this.lastModifiedDatetime = lastModifiedDatetime;
    }
}
