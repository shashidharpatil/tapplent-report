package com.tapplent.platformutility.notification;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tapplent.platformutility.notification.helper.AppConstants;
import com.tapplent.platformutility.notification.helper.ContentforID;
import com.tapplent.platformutility.notification.helper.ContentforNotKey;
import com.tapplent.platformutility.notification.helper.DGCreateModel;
import com.tapplent.platformutility.notification.helper.DGOperationModel;
import com.tapplent.platformutility.notification.helper.Notification;

//Get the Api key for GCM by creating and registering the project in google console
//Create a notification Key for a single user but using multiple devices using DeviceGroup.java class
//Get Registration Id of each client or user.
//Every client on app install sends the registration ID to the server.
//Also get the project key for the project created in Google console.

public class GCMHandler {

	public String createGroup(String apiKey, String notKeyName, String project_id, ArrayList<String> ids) {
		DGCreateModel creategroup = new DGCreateModel();
		creategroup.setOperation(AppConstants.create);
		creategroup.setNotification_key_name(notKeyName);
		creategroup.setRegistration_ids(ids);
		String notificatonKey = CreateDeviceGroupCall.post(apiKey, project_id, creategroup);
		return notificatonKey;
	}

	public String addGroupMember(String apiKey, String notKeyName, String project_id, String notification_key,
			ArrayList<String> ids) {
		DGOperationModel addGroupMember = new DGOperationModel();
		addGroupMember.setOperation(AppConstants.add);
		addGroupMember.setNotification_key_name(notKeyName);
		addGroupMember.setRegistration_ids(ids);
		addGroupMember.setNotification_key(notification_key);
		String notificatonKey = ModifyDeviceGroupCall.post(apiKey, project_id, addGroupMember);
		return notificatonKey;
	}

	public String removeGroupMember(String apiKey, String notKeyName, String project_id, String notification_key,
			ArrayList<String> ids) {
		DGOperationModel addGroupMember = new DGOperationModel();
		addGroupMember.setOperation(AppConstants.remove);
		addGroupMember.setNotification_key_name(notKeyName);
		addGroupMember.setRegistration_ids(ids);
		addGroupMember.setNotification_key(notification_key);
		String notificatonKey = ModifyDeviceGroupCall.post(apiKey, project_id, addGroupMember);
		return notificatonKey;
	}

	public void sendGCMusingRegId(Notification notification, String regId, String apiKey) {
		ContentforID c = new ContentforID();
		c.addRegId(regId);
		c.createData(notification);
		POST2GCM.post(apiKey, c, null);

	}

	public void sendGCMusingNotKey(Notification notification, String notKey, String apiKey) {
		ContentforNotKey c = new ContentforNotKey();
		c.addNotKey(notKey);
		c.createData(notification);
		POST2GCM.post(apiKey, null, c);
	}

	
}
