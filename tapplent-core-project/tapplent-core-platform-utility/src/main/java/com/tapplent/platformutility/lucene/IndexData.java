package com.tapplent.platformutility.lucene;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

//import org.apache.lucene.analysis.Analyzer;
//import org.apache.lucene.analysis.standard.StandardAnalyzer;
//import org.apache.lucene.document.Document;
//import org.apache.lucene.document.Field;
//import org.apache.lucene.document.TextField;
//import org.apache.lucene.index.IndexWriter;
//import org.apache.lucene.index.IndexWriterConfig;
//import org.apache.lucene.store.Directory;
//import org.apache.lucene.store.RAMDirectory;

import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.metadata.structure.MasterEntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadata;
import com.tapplent.platformutility.search.impl.SearchDAO;

public class IndexData {
	
	SearchDAO searchDAO;
	
	public SearchDAO getSearchDAO() {
		return searchDAO;
	}

	public void setSearchDAO(SearchDAO searchDAO) {
		this.searchDAO = searchDAO;
	}

	public void indexFiles(String doName){
//		Analyzer analyzer = new StandardAnalyzer();
//
//		Directory directory = new RAMDirectory();
//		IndexWriterConfig config = new IndexWriterConfig(analyzer);
//		IndexWriter iwriter = null;
//	    try {
//			 iwriter = new IndexWriter(directory, config);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	    List<Map<String, Object>> dataList = searchDAO.search("SELECT * FROM "+doName, null).getData();
//	    MasterEntityMetadata doMeta = TenantAwareCache.getMetaDataRepository().getMetadataByDo(doName);
//	    Map<String, MasterEntityAttributeMetadata> attrMetaMap = doMeta.getAttributeMeta().stream()
//	    															.collect(Collectors.toMap(MasterEntityAttributeMetadata::getDoAttributeCode, p->p));
//	    for(Map<String, Object> row : dataList){
//	    	Document doc = new Document();
//	    	for(Entry<String, Object> entry : row.entrySet()){
//			    MasterEntityAttributeMetadata attrMeta = attrMetaMap.get(entry.getKey());
//		    	if(attrMeta.isLocalSearchable()){
//		    		doc.add(new Field(attrMeta.getDoAttributeCode(), entry.getValue().toString(), TextField.TYPE_STORED));
//		    	}
//	    	}
//	    	try{
//	    	iwriter.addDocument(doc);
//	    	}catch(IOException e){
//	    		e.printStackTrace();
//	    	}
//	    }
//	    try {
//	    	iwriter.close();
//	    } catch (IOException e) {
//	    	e.printStackTrace();
//	    }
	}
}
