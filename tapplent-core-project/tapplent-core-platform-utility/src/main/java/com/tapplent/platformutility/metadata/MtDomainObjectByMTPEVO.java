/**
 * 
 */
package com.tapplent.platformutility.metadata;

/**
 * @author Shubham Patodi
 *
 */
public class MtDomainObjectByMTPEVO extends MtDomainObjectVO{
	private String mtPE;

	public String getMtPE() {
		return mtPE;
	}

	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}
}
