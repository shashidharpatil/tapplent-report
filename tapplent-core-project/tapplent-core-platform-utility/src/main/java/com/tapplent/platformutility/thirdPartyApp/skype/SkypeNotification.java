package com.tapplent.platformutility.thirdPartyApp.skype;

import com.skype.ChatMessage;
import com.skype.ChatMessageAdapter;
import com.skype.Skype;
import com.skype.SkypeException;
import com.skype.User;

public class SkypeNotification {
	
	public static void sendSkypeMessage() throws SkypeException{
		 try {
	           String message = "Hello, This is test message";
	 
	           Skype.chat("sripad.raghavendra").send(message);
	 
	           System.out.println("Message sent!");
	 
	       } catch (SkypeException e) {
	          e.printStackTrace();
	       }
	       System.out.println("Start Auto Answering ...");
	         
	        // To prevent exiting from this program
	        Skype.setDaemon(false);
	         
	         
	        Skype.addChatMessageListener(new ChatMessageAdapter() {
	            public void chatMessageReceived(ChatMessage received)
	                    throws SkypeException {
	                if (received.getType().equals(ChatMessage.Type.SAID)) {
	 
	                    // Sender
	                    User sender =received.getSender();    
	                     
	                    System.out.println(sender.getId() +" say:");
	                    System.out.println(" "+received.getContent() );
	                     
	                    if (received.getContent().equals("1"))
	                    {  
	                    received.getSender().send(received.getContent()+"Hi..U have typed 1.CLICK HERE");
	                     
	                    System.out.println(" - Auto answered!");
	                    }
	                    else if (received.getContent().equals("2"))
	                    {
	                    	received.getSender().send(received.getContent()+"Hi..U have typed 2.CLICK HERE");
	                    	System.out.println(" - Auto answered!");
	                    }
	                    else if (received.getContent().equals("3"))
	                    {
	                    	received.getSender().send(received.getContent()+"Hi..U have typed 3.CLICK HERE");
	                    	System.out.println(" - Auto answered!");
	                    }
	                    else
	                    { received.getSender().send(received.getContent()+"You have entered the wrong option.try again with the right option");
	                    	
	                    
	                    }	
	                }
	            }
	        });
	         
	         
	        System.out.println("Auto Answering started!");
		
	}
	
	

}
