/*
 * Copyright 2002-2007 Robert Breidecker.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tapplent.platformutility.expression.jeval.function.math;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.expression.jeval.EvaluationConstants;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.expression.jeval.function.Function;
import com.tapplent.platformutility.expression.jeval.function.FunctionConstants;
import com.tapplent.platformutility.expression.jeval.function.FunctionException;
import com.tapplent.platformutility.expression.jeval.function.FunctionHelper;
import com.tapplent.platformutility.expression.jeval.function.FunctionResult;
import com.tapplent.platformutility.expression.jeval.function.date.DateAdd;

import java.util.ArrayList;


/**
 * This class is a function that executes within Evaluator. The function returns
 * the greater of two double values. See the Math.max(double) method in the JDK
 * for a complete description of how this function works.
 */
public class Distance implements Function {
    
	private static final Logger Log = LogFactory.getLogger(Distance.class);

	/**
     * Returns the name of the function - "max".
     *
     * @return The name of this function class.
     */
    public String getName() {
        return "distance";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     *
     * @param evaluator An instance of Evaluator.
     * @param arguments A string argument that will be converted into two double
     *                  values and evaluated.
     * @return The greater of two values.
     * @throws FunctionException Thrown if the argument(s) are not valid for this function.
     */
    public FunctionResult execute(final Evaluator evaluator, final String arguments)
            throws FunctionException {
        Double result = null;

        ArrayList numbers = FunctionHelper.getDoubles(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);


        try {
            Log.debug("lat1",String.valueOf(numbers.get(0)));
            Log.debug("lat2",String.valueOf(numbers.get(1)));
            Log.debug("lon1",String.valueOf(numbers.get(2)));
            Log.debug("lon2",String.valueOf(numbers.get(3)));
            final int R = 6371; // Radius of the earth

            Double latDistance = Math.toRadians((Double)numbers.get(1) - (Double)numbers.get(0));
            Double lonDistance = Math.toRadians((Double)numbers.get(3) - (Double)numbers.get(2));
            Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                    + Math.cos(Math.toRadians((Double)numbers.get(0))) * Math.cos(Math.toRadians((Double)numbers.get(1)))
                    * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
            Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            double distance = R * c * 1000; // convert to meters

            double height = 0 - 0;

            distance = Math.pow(distance, 2) + Math.pow(height, 2);

            result= (Math.sqrt(distance));

        } catch (Exception e) {
            throw new FunctionException("Two numeric arguments are required.", e);
        }

        return new FunctionResult(result.toString(),
                FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC);
    }


}