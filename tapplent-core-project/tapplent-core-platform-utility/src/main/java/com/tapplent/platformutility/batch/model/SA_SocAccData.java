package com.tapplent.platformutility.batch.model;

public class SA_SocAccData {
	
	private String personSocialAccountPkId;
	private String personDepFkId;				
	private String socialAccountTypeCodeFkId;				
	private Object	socialAccountCaracteristicsBlob;
	private boolean isValid;

	private String accountHandleLngTxt;

	private Object deviceIndependentBlob;

	

	public String getAccountHandleLngTxt() {
		return accountHandleLngTxt;
	}

	public void setAccountHandleLngTxt(String accountHandleLngTxt) {
		this.accountHandleLngTxt = accountHandleLngTxt;
	}

	public Object getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}

	public void setDeviceIndependentBlob(Object deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	
	public String getPersonSocialAccountPkId() {
		return personSocialAccountPkId;
	}
	public void setPersonSocialAccountPkId(String personSocialAccountPkId) {
		this.personSocialAccountPkId = personSocialAccountPkId;
	}
	public String getPersonDepFkId() {
		return personDepFkId;
	}
	public void setPersonDepFkId(String personDepFkId) {
		this.personDepFkId = personDepFkId;
	}
	public String getSocialAccountTypeCodeFkId() {
		return socialAccountTypeCodeFkId;
	}
	public void setSocialAccountTypeCodeFkId(String socialAccountTypeCodeFkId) {
		this.socialAccountTypeCodeFkId = socialAccountTypeCodeFkId;
	}
	public Object getSocialAccountCaracteristicsBlob() {
		return socialAccountCaracteristicsBlob;
	}
	public void setSocialAccountCaracteristicsBlob(Object socialAccountCaracteristicsBlob) {
		this.socialAccountCaracteristicsBlob = socialAccountCaracteristicsBlob;
	}
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	
 

}
