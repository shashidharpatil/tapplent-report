package com.tapplent.platformutility.search.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Deque;
import java.util.List;
import java.util.Map;

import com.tapplent.platformutility.search.api.SearchData;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.builder.SqlQuery;

public interface SearchService {

	SearchResult doSearch(SearchData searchData) throws IOException;

    String getG11nData(String tableName, String columnName, String pkColumnName, String dbPkId, String dbDataTypeCode);

    SqlQuery getSqlQuery(DefaultSearchData searchData);

    List<Map<String,Object>> search(String sql, Deque<Object> parameter);
}
