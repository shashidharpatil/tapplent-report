package com.tapplent.platformutility.batch.scheduler;

import com.tapplent.platformutility.batch.model.SchedulerInstanceModel;
import com.tapplent.platformutility.batch.model.SchedulerModel;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SchedulerResolver {
    public List<SchedulerInstanceModel> sInstance = new ArrayList<SchedulerInstanceModel>();
    Calendar defCal = Calendar.getInstance();

    public void fetchScheduler() {
        defCal.add(Calendar.MONTH, 1);

	/*	String sql = "select T_SCH_ADM_SCHED_DEFN.SCHED_DEFN_PK_ID," + "T_SCH_ADM_SCHED_DEFN.JOB_TYPE_CODE_FK_ID,"
                + "T_SCH_ADM_SCHED_DEFN.IS_ONE_TIME_SCHED," + "T_SCH_ADM_SCHED_DEFN.IS_RECURRING_SCHED,"
				+ "T_SCH_ADM_SCHED_DEFN.ONE_TIME_DATETIME," + "T_SCH_ADM_SCHED_GROUPER.SCHED_GROUPER_PK_ID,"
				+ "T_SCH_ADM_SCHED_GROUPER.SCHED_DRIVING_FREQ_CODE_FK_ID,"
				+ "T_SCH_ADM_SCHED_GROUPER.SCHED_DRIVING_INTERVAL_POS_INT,"
				+ "T_SCH_ADM_SCHED_GROUPER.SCHED_EXEC_START_DATETIME,"
				+ "T_SCH_ADM_SCHED_GROUPER.SCHED_EXEC_END_DATETIME,"
				+ "T_SCH_ADM_SCHED_GROUPER.IS_EXECUTION_NEVER_ENDS," + "T_SCH_ADM_SCHED_RCR_FREQ.SCHED_RCR_FREQ_PK_ID,"
				+ "T_SCH_ADM_SCHED_RCR_FREQ.FREQUENCY_CODE_FK_ID," + "T_SCH_ADM_SCHED_RCR_FREQ.INTERVAL_POS_INT,"
				+ "T_SCH_ADM_SCHED_RCR_FREQ.ABSOLUTE_DATETIME "
				+ "from T_SCH_ADM_SCHED_DEFN inner join T_SCH_ADM_SCHED_GROUPER "
				+ "on T_SCH_ADM_SCHED_DEFN.SCHED_DEFN_PK_ID = T_SCH_ADM_SCHED_GROUPER.SCHED_DEFN_FK_ID "
				+ "inner join T_SCH_ADM_SCHED_RCR_FREQ on T_SCH_ADM_SCHED_GROUPER.SCHED_GROUPER_PK_ID = "
				+ "T_SCH_ADM_SCHED_RCR_FREQ.SCHED_GROUPER_FK_ID";

		String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		String DB_URL = "jdbc:mysql://localhost/Server";

		String USER = "root";
		String PASS = "Bond0020";

		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// STEP 3: Open a connection
			System.out.println("Connecting to database...");
			conn = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);

			// STEP 4: Execute a query
			System.out.println("Creating statement...");
			stmt = (Statement) conn.createStatement();

			ResultSet rs = stmt.executeQuery(sql);
			Map<String, List<SchedulerModel>> map = new HashMap<>();
			while (rs.next()) {
				// Retrieve by column name
				SchedulerModel s = new SchedulerModel();
				s.setSchedDefnPkId(rs.getString(Constants.schedDefnPkId));
				s.setJobTypeCodeFkId(rs.getString(Constants.jobTypeCodeFkId));
				s.setOneTimeSched(rs.getBoolean(Constants.isOneTimeSched));
				s.setRecurringSched(rs.getBoolean(Constants.isRecurringSched));
				s.setOneTimeDatetime(rs.getString(Constants.oneTimeDatetime));
				s.setSchedGrouperPkId(rs.getString(Constants.schedGrouperPkId));
				s.setSchedDrivingFreqCodeFkId(rs.getString(Constants.schedDrivingFreqCodeFkId));
				s.setSchedDrivingIntervalPosInt(rs.getInt(Constants.schedDrivingIntervalPosInt));
				s.setSchedExecStartDatetime(rs.getString(Constants.schedExecStartDatetime));
				s.setSchedExecEndDatetime(rs.getString(Constants.schedExecEndDatetime));
				s.setExecutionNeverEnds(rs.getBoolean(Constants.isExecutionNeverEnds));
				s.setSchedRcrFreqPkId(rs.getString(Constants.schedRcrFreqPkId));
				s.setFrequencyCodeFkId(rs.getString(Constants.frequencyCodeFkId));
				s.setIntervalPosInt(rs.getString(Constants.intervalPosInt));
				s.setAbsoluteDatetime(rs.getString(Constants.absoluteDatetime));
				if (map.containsKey(s.getSchedDefnPkId() + s.getSchedGrouperPkId())) {
					map.get(s.getSchedDefnPkId() + s.getSchedGrouperPkId()).add(s);
				} else {
					List<SchedulerModel> list = new ArrayList<SchedulerModel>();
					list.add(s);
					map.put(s.getSchedDefnPkId() + s.getSchedGrouperPkId(), list);
				}
				
			}
			// STEP 6: Clean-up environment
			rs.close();

			stmt.close();
			conn.close();

			Iterator it = map.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				resolve((List<SchedulerModel>) pair.getValue());
				it.remove(); // avoids a ConcurrentModificationException
			}
			insertSchedulerInstance();

		}

		catch (Exception e1) {
			e1.printStackTrace();
		}*/

        //(TDWI) resolve from scheduler defn table
    }

    public void resolve(List<SchedulerModel> list) {

        if (list.get(0).isOneTimeSched() && convertTimeStamp(list.get(0).getOneTimeDatetime()).compareTo(defCal) < 0) {
            SchedulerInstanceModel s = new SchedulerInstanceModel();
            s.setJobTypeCodeFkId(list.get(0).getJobTypeCodeFkId());
            s.setSchedDefnFkId(list.get(0).getSchedDefnPkId());
            s.setScheduledDatetime(list.get(0).getOneTimeDatetime());
            sInstance.add(s);

        }

        if (list.get(0).getSchedDrivingFreqCodeFkId().equals("YEAR")) {
            System.out.println(list.get(0).getSchedDrivingFreqCodeFkId() + " " + list.size());
            if (checkKey("WEEK", list)) {
                resolveYear(list);
            } else {
                resolveYearWithSpecificDate(list);
            }
        }
        if (list.get(0).getSchedDrivingFreqCodeFkId().equals("MONTH")) {
            if (checkKey("WEEK", list)) {
                resolveMonth(list);
            } else {
                resolveMonthSpecificDate(list);
            }
        }
        if (list.get(0).getSchedDrivingFreqCodeFkId().equals("WEEK")) {
            resolveWeek(list);
        }
        if (list.get(0).getSchedDrivingFreqCodeFkId().equals("DAY")) {
            resolveDay(list);
        }
        if (list.get(0).getSchedDrivingFreqCodeFkId().equals("HOUR")) {
            System.out.println(list.get(0).getSchedDrivingFreqCodeFkId());
            resolveHour(list);
        }
    }

    public void resolveYear(List<SchedulerModel> list) {
        Calendar sc = convertTimeStamp(list.get(0).getSchedExecStartDatetime());


        Calendar ec = convertTimeStamp(list.get(0).getSchedExecEndDatetime());

        for (int i = 0; i < 100; i++) {

            if ((sc.get(Calendar.YEAR) + (list.get(0).getSchedDrivingIntervalPosInt() * i)) <= ec.get(Calendar.YEAR)) {
                Calendar c = Calendar.getInstance();

                c.set(Calendar.YEAR, sc.get(Calendar.YEAR) + (list.get(0).getSchedDrivingIntervalPosInt() * i));
                System.out.println("year " + c.get(Calendar.YEAR));
                for (int l = 0; l < list.size(); l++) {
                    if (list.get(l).getFrequencyCodeFkId().equals("MONTH")) {
                        c.set(Calendar.MONTH, Integer.valueOf(list.get(l).getIntervalPosInt()) - 1);
                        for (int m = 0; m < list.size(); m++) {
                            if (list.get(m).getFrequencyCodeFkId().equals("WEEK")) {
                                for (int j = 0; j < list.size(); j++) {
                                    if (list.get(j).getFrequencyCodeFkId().equals("DAY")) {
                                        Calendar c1 = calculateday(c, Integer.valueOf(list.get(m).getIntervalPosInt()),
                                                Integer.valueOf(list.get(j).getIntervalPosInt()));

                                        for (int k = 0; k < list.size(); k++) {
                                            if (list.get(k).getFrequencyCodeFkId().equals("TIME")) {
                                                String[] time = list.get(k).getIntervalPosInt().split(":");
                                                c1.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
                                                c1.set(Calendar.MINUTE, Integer.valueOf(time[1]));
                                                if (c1.compareTo(sc) >= 0 && c1.compareTo(ec) <= 0)
                                                    System.out.println("time" + c1.getTime());
                                                addSchedulerInstance(c1, list.get(k));

                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    public void resolveYearWithSpecificDate(List<SchedulerModel> list) {
        Calendar sc = convertTimeStamp(list.get(0).getSchedExecStartDatetime());
        System.out.println(sc.getTime());
        Calendar ec = convertTimeStamp(list.get(0).getSchedExecEndDatetime());
        System.out.println(ec.getTime());
        for (int i = 0; i < 100; i++) {

            if ((sc.get(Calendar.YEAR) + (list.get(0).getSchedDrivingIntervalPosInt() * i)) <= ec.get(Calendar.YEAR)) {
                Calendar c1 = Calendar.getInstance();

                c1.set(Calendar.YEAR, sc.get(Calendar.YEAR) + (list.get(0).getSchedDrivingIntervalPosInt() * i));
                System.out.println("year " + c1.get(Calendar.YEAR));
                for (int l = 0; l < list.size(); l++) {
                    if (list.get(l).getFrequencyCodeFkId().equals("MONTH")) {
                        c1.set(Calendar.MONTH, Integer.valueOf(list.get(l).getIntervalPosInt()) - 1);

                        for (int j = 0; j < list.size(); j++) {
                            if (list.get(j).getFrequencyCodeFkId().equals("DAY")) {
                                c1.set(Calendar.DAY_OF_MONTH, Integer.valueOf(list.get(j).getIntervalPosInt()));

                                for (int k = 0; k < list.size(); k++) {
                                    if (list.get(k).getFrequencyCodeFkId().equals("TIME")) {
                                        String[] time = list.get(k).getIntervalPosInt().split(":");
                                        c1.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
                                        c1.set(Calendar.MINUTE, Integer.valueOf(time[1]));
                                        if (c1.compareTo(sc) >= 0 && c1.compareTo(ec) <= 0)
                                            System.out.println("time" + c1.getTime());
                                        addSchedulerInstance(c1, list.get(k));
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    public void resolveMonth(List<SchedulerModel> list) {
        Calendar sc = convertTimeStamp(list.get(0).getSchedExecStartDatetime());

        Calendar ec = convertTimeStamp(list.get(0).getSchedExecEndDatetime());
        int period = list.get(0).getSchedDrivingIntervalPosInt();
        while (sc.compareTo(ec) <= 0) {
            for (int m = 0; m < list.size(); m++) {
                if (list.get(m).getFrequencyCodeFkId().equals("WEEK")) {
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).getFrequencyCodeFkId().equals("DAY")) {
                            Calendar c1 = calculateday(sc, Integer.valueOf(list.get(m).getIntervalPosInt()),
                                    Integer.valueOf(list.get(j).getIntervalPosInt()));

                            for (int k = 0; k < list.size(); k++) {
                                if (list.get(k).getFrequencyCodeFkId().equals("TIME")) {
                                    String[] time = list.get(k).getIntervalPosInt().split(":");
                                    c1.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
                                    c1.set(Calendar.MINUTE, Integer.valueOf(time[1]));

                                    System.out.println("time" + c1.getTime());
                                    addSchedulerInstance(c1, list.get(k));
                                }
                            }
                        }
                    }
                }

            }
            sc.add(Calendar.MONTH, period);

        }

    }

    public void resolveMonthSpecificDate(List<SchedulerModel> list) {

        Calendar sc = convertTimeStamp(list.get(0).getSchedExecStartDatetime());

        Calendar ec = convertTimeStamp(list.get(0).getSchedExecEndDatetime());
        int period = list.get(0).getSchedDrivingIntervalPosInt();
        while (sc.compareTo(ec) <= 0) {
            for (int m = 0; m < list.size(); m++) {

                if (list.get(m).getFrequencyCodeFkId().equals("DAY")) {
                    sc.set(Calendar.DAY_OF_MONTH, Integer.valueOf(list.get(m).getIntervalPosInt()));

                    for (int k = 0; k < list.size(); k++) {
                        if (list.get(k).getFrequencyCodeFkId().equals("TIME")) {
                            String[] time = list.get(k).getIntervalPosInt().split(":");
                            sc.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
                            sc.set(Calendar.MINUTE, Integer.valueOf(time[1]));

                            System.out.println("time" + sc.getTime());
                            addSchedulerInstance(sc, list.get(k));
                        }
                    }
                }

            }
            sc.add(Calendar.MONTH, period);

        }

    }

    public void resolveWeek(List<SchedulerModel> list) {
        Calendar sc = convertTimeStamp(list.get(0).getSchedExecStartDatetime());

        Calendar ec = convertTimeStamp(list.get(0).getSchedExecEndDatetime());

        for (int o = sc.get(Calendar.YEAR); o <= ec.get(Calendar.YEAR); o++) {
            for (int i = 0; i < 12; i++) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.YEAR, o);
                c.set(Calendar.MONTH, i);
                for (int m = 0; m < list.size(); m++) {
                    if (list.get(m).getFrequencyCodeFkId().equals("WEEK")) {
                        for (int j = 0; j < list.size(); j++) {
                            if (list.get(j).getFrequencyCodeFkId().equals("DAY")) {
                                Calendar c1 = calculateday(c, Integer.valueOf(list.get(m).getIntervalPosInt()),
                                        Integer.valueOf(list.get(j).getIntervalPosInt()));

                                for (int k = 0; k < list.size(); k++) {
                                    if (list.get(k).getFrequencyCodeFkId().equals("TIME")) {
                                        String[] time = list.get(k).getIntervalPosInt().split(":");
                                        c1.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
                                        c1.set(Calendar.MINUTE, Integer.valueOf(time[1]));
                                        if (c1.compareTo(sc) >= 0 && c1.compareTo(ec) <= 0)
                                            System.out.println("time" + c1.getTime());
                                        addSchedulerInstance(c1, list.get(k));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void resolveDay(List<SchedulerModel> list) {
        Calendar sc = convertTimeStamp(list.get(0).getSchedExecStartDatetime());

        Calendar ec = convertTimeStamp(list.get(0).getSchedExecEndDatetime());
        int period = list.get(0).getSchedDrivingIntervalPosInt();
        while (sc.compareTo(ec) <= 0) {
            sc.add(Calendar.DAY_OF_MONTH, period);
            for (int k = 0; k < list.size(); k++) {
                if (list.get(k).getFrequencyCodeFkId().equals("TIME")) {
                    String[] time = list.get(k).getIntervalPosInt().split(":");
                    sc.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
                    sc.set(Calendar.MINUTE, Integer.valueOf(time[1]));
                    if (sc.compareTo(sc) >= 0 && sc.compareTo(ec) <= 0)
                        System.out.println("time" + sc.getTime());
                    addSchedulerInstance(sc, list.get(k));
                }
            }
        }
    }

    public void resolveHour(List<SchedulerModel> list) {
        Calendar sc = convertTimeStamp(list.get(0).getSchedExecStartDatetime());
        System.out.println(sc.getTime());
        Calendar ec = convertTimeStamp(list.get(0).getSchedExecEndDatetime());
        System.out.println(ec.getTime());
        int period = list.get(0).getSchedDrivingIntervalPosInt();
        while (sc.compareTo(ec) <= 0) {

            for (int k = 0; k < list.size(); k++) {
                if (list.get(k).getFrequencyCodeFkId().equals("MINUTE")) {

                    sc.set(Calendar.MINUTE, Integer.valueOf(list.get(k).getIntervalPosInt()));

                    System.out.println("time" + sc.getTime());
                    addSchedulerInstance(sc, list.get(k));
                }
            }
            sc.add(Calendar.HOUR_OF_DAY, period);
        }
    }

    public Calendar calculateday(Calendar d, Integer week, Integer Day) {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM/dd/YYYY");
        calendar.set(Calendar.YEAR, d.get(Calendar.YEAR));
        calendar.set(Calendar.MONTH, d.get(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_WEEK, Day);
        calendar.set(Calendar.DAY_OF_WEEK_IN_MONTH, week);

        calendar.add(Calendar.DAY_OF_MONTH, -(calendar.get(Calendar.DAY_OF_WEEK) - Day));

        return calendar;
    }

    public boolean checkKey(String key, List<SchedulerModel> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getFrequencyCodeFkId().equals(key)) {
                return true;
            }
        }
        return false;

    }

    public Calendar convertTimeStamp(String timeStamp) {
        Calendar c = Calendar.getInstance();
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            Date parsedDate = dateFormat.parse(timeStamp);
            Timestamp timestamp = new Timestamp(parsedDate.getTime());
            long timestampLong = timestamp.getTime();
            Date d = new Date(timestampLong);
            System.out.println(d.getTime());
            c.setTime(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return c;
    }

    private void addSchedulerInstance(Calendar c, SchedulerModel s) {
        SchedulerInstanceModel s1 = new SchedulerInstanceModel();
        s1.setJobTypeCodeFkId(s.getJobTypeCodeFkId());
        s1.setSchedDefnFkId(s.getSchedDefnPkId());
        java.sql.Timestamp d = new java.sql.Timestamp(c.getTimeInMillis());
        s1.setScheduledDatetime(String.valueOf(d));
        sInstance.add(s1);

    }


    private void insertSchedulerInstance()

    {
	

	

	/*String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	String DB_URL = "jdbc:mysql://localhost/Server";

	String USER = "root";
	String PASS = "Bond0020";

	Connection conn = null;
	Statement stmt = null;
	try {
		// STEP 2: Register JDBC driver
		Class.forName("com.mysql.jdbc.Driver");

		// STEP 3: Open a connection
		System.out.println("Connecting to database...");
		conn = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);

		// STEP 4: Execute a query
		System.out.println("Creating statement...");
		stmt = (Statement) conn.createStatement();
		for(int i=0;i<sInstance.size();i++){
			SchedulerInstanceModel s1 = sInstance.get(i);
		String sql = "insert into T_SCH_IEU_SCHEDULER_INSTANCE ("
				+ "SCHEDULER_INSTANCE_PK_ID,"
				+ "SCHED_DEFN_FK_ID,"
				+ "JOB_TYPE_CODE_FK_ID,"
				+ "SCHEDULED_DATETIME) values ('"+i+"','"+s1.getSchedDefnFkId()+"','"+s1.getJobTypeCodeFkId()+"','"+s1.getScheduledDatetime()+"');";
		boolean result = stmt.execute(sql);
		}
		// STEP 6: Clean-up environment
		

		stmt.close();
		conn.close();
}
	catch (Exception e){
		e.printStackTrace();
		}*/

        //(TDWI) insert into scheduler instance table
    }
}
