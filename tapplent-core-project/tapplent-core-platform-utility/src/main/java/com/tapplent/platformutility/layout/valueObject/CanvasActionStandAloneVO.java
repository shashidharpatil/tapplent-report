package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class CanvasActionStandAloneVO {
	private String canvasActionStandAloneEuPkId;
	private String canvasEuFkId;
	private String canvassActionDefnFkId;
	private String baseTemplateDepFkId;
	private String btProcessElementDepFkId;
	private String viewTypeCodeDepFkId;
	private String canvasDefnDepFkId;
	private String actionCodeFkId;
	private boolean showAtInitialLoad;
	private String actionGestureCodeFkId;
	private String actionGestureAnimationCodeFkId;
	private String controlTypeCodeFkId;
	private JsonNode actionLabelG11nBlob;
	private String actionIconFkId;
	private boolean isShowActionLabel;
	private boolean isShowActionIcon;
	private String actionOnStateIconFkId;
	private String actionOffStateIconFkId;
	private boolean isLicenced;
	private boolean isCreateAccessControlEquivalent;
	private boolean isReadAccessControlEquivalent;
	private boolean isUpdateAccessControlEquivalent;
	private boolean isDeleteAccessControlEquivalent;
	private int actionSeqNo;
	private JsonNode canvasActionPlacement;
	private JsonNode canvasActionIconPlacement;
	private JsonNode canvasActionLabelPlacement;
	private JsonNode canvasActionStandAloneTheme;
	private JsonNode isApplicable;
	public String getCanvasActionStandAloneEuPkId() {
		return canvasActionStandAloneEuPkId;
	}
	public void setCanvasActionStandAloneEuPkId(String canvasActionStandAloneEuPkId) {
		this.canvasActionStandAloneEuPkId = canvasActionStandAloneEuPkId;
	}
	public String getCanvasEuFkId() {
		return canvasEuFkId;
	}
	public void setCanvasEuFkId(String canvasEuFkId) {
		this.canvasEuFkId = canvasEuFkId;
	}
	public String getBaseTemplateDepFkId() {
		return baseTemplateDepFkId;
	}
	public void setBaseTemplateDepFkId(String baseTemplateDepFkId) {
		this.baseTemplateDepFkId = baseTemplateDepFkId;
	}
	public String getBtProcessElementDepFkId() {
		return btProcessElementDepFkId;
	}
	public void setBtProcessElementDepFkId(String btProcessElementDepFkId) {
		this.btProcessElementDepFkId = btProcessElementDepFkId;
	}
	public String getViewTypeCodeDepFkId() {
		return viewTypeCodeDepFkId;
	}
	public void setViewTypeCodeDepFkId(String viewTypeCodeDepFkId) {
		this.viewTypeCodeDepFkId = viewTypeCodeDepFkId;
	}
	public String getCanvasDefnDepFkId() {
		return canvasDefnDepFkId;
	}
	public void setCanvasDefnDepFkId(String canvasDefnDepFkId) {
		this.canvasDefnDepFkId = canvasDefnDepFkId;
	}
	public String getActionCodeFkId() {
		return actionCodeFkId;
	}
	public void setActionCodeFkId(String actionCodeFkId) {
		this.actionCodeFkId = actionCodeFkId;
	}
	public boolean isShowAtInitialLoad() {
		return showAtInitialLoad;
	}
	public void setShowAtInitialLoad(boolean showAtInitialLoad) {
		this.showAtInitialLoad = showAtInitialLoad;
	}
	public String getActionGestureCodeFkId() {
		return actionGestureCodeFkId;
	}
	public void setActionGestureCodeFkId(String actionGestureCodeFkId) {
		this.actionGestureCodeFkId = actionGestureCodeFkId;
	}
	public String getActionGestureAnimationCodeFkId() {
		return actionGestureAnimationCodeFkId;
	}
	public void setActionGestureAnimationCodeFkId(String actionGestureAnimationCodeFkId) {
		this.actionGestureAnimationCodeFkId = actionGestureAnimationCodeFkId;
	}
	public String getControlTypeCodeFkId() {
		return controlTypeCodeFkId;
	}
	public void setControlTypeCodeFkId(String controlTypeCodeFkId) {
		this.controlTypeCodeFkId = controlTypeCodeFkId;
	}
	public JsonNode getActionLabelG11nBlob() {
		return actionLabelG11nBlob;
	}
	public void setActionLabelG11nBlob(JsonNode actionLabelG11nBlob) {
		this.actionLabelG11nBlob = actionLabelG11nBlob;
	}
	public String getActionIconFkId() {
		return actionIconFkId;
	}
	public void setActionIconFkId(String actionIconFkId) {
		this.actionIconFkId = actionIconFkId;
	}
	public boolean isShowActionLabel() {
		return isShowActionLabel;
	}
	public void setShowActionLabel(boolean isShowActionLabel) {
		this.isShowActionLabel = isShowActionLabel;
	}
	public boolean isShowActionIcon() {
		return isShowActionIcon;
	}
	public void setShowActionIcon(boolean isShowActionIcon) {
		this.isShowActionIcon = isShowActionIcon;
	}
	public String getActionOnStateIconFkId() {
		return actionOnStateIconFkId;
	}
	public void setActionOnStateIconFkId(String actionOnStateIconFkId) {
		this.actionOnStateIconFkId = actionOnStateIconFkId;
	}
	public String getActionOffStateIconFkId() {
		return actionOffStateIconFkId;
	}
	public void setActionOffStateIconFkId(String actionOffStateIconFkId) {
		this.actionOffStateIconFkId = actionOffStateIconFkId;
	}
	public boolean isLicenced() {
		return isLicenced;
	}
	public void setLicenced(boolean isLicenced) {
		this.isLicenced = isLicenced;
	}
	public boolean isCreateAccessControlEquivalent() {
		return isCreateAccessControlEquivalent;
	}
	public void setCreateAccessControlEquivalent(boolean isCreateAccessControlEquivalent) {
		this.isCreateAccessControlEquivalent = isCreateAccessControlEquivalent;
	}
	public boolean isReadAccessControlEquivalent() {
		return isReadAccessControlEquivalent;
	}
	public void setReadAccessControlEquivalent(boolean isReadAccessControlEquivalent) {
		this.isReadAccessControlEquivalent = isReadAccessControlEquivalent;
	}
	public boolean isUpdateAccessControlEquivalent() {
		return isUpdateAccessControlEquivalent;
	}
	public void setUpdateAccessControlEquivalent(boolean isUpdateAccessControlEquivalent) {
		this.isUpdateAccessControlEquivalent = isUpdateAccessControlEquivalent;
	}
	public boolean isDeleteAccessControlEquivalent() {
		return isDeleteAccessControlEquivalent;
	}
	public void setDeleteAccessControlEquivalent(boolean isDeleteAccessControlEquivalent) {
		this.isDeleteAccessControlEquivalent = isDeleteAccessControlEquivalent;
	}
	public int getActionSeqNo() {
		return actionSeqNo;
	}
	public void setActionSeqNo(int actionSeqNo) {
		this.actionSeqNo = actionSeqNo;
	}
	public JsonNode getCanvasActionPlacement() {
		return canvasActionPlacement;
	}
	public void setCanvasActionPlacement(JsonNode canvasActionPlacement) {
		this.canvasActionPlacement = canvasActionPlacement;
	}
	public JsonNode getCanvasActionIconPlacement() {
		return canvasActionIconPlacement;
	}
	public void setCanvasActionIconPlacement(JsonNode canvasActionIconPlacement) {
		this.canvasActionIconPlacement = canvasActionIconPlacement;
	}
	public JsonNode getCanvasActionLabelPlacement() {
		return canvasActionLabelPlacement;
	}
	public void setCanvasActionLabelPlacement(JsonNode canvasActionLabelPlacement) {
		this.canvasActionLabelPlacement = canvasActionLabelPlacement;
	}
	public JsonNode getCanvasActionStandAloneTheme() {
		return canvasActionStandAloneTheme;
	}
	public void setCanvasActionStandAloneTheme(JsonNode canvasActionStandAloneTheme) {
		this.canvasActionStandAloneTheme = canvasActionStandAloneTheme;
	}
	public JsonNode getIsApplicable() {
		return isApplicable;
	}
	public void setIsApplicable(JsonNode isApplicable) {
		this.isApplicable = isApplicable;
	}
	public String getCanvassActionDefnFkId() {
		return canvassActionDefnFkId;
	}
	public void setCanvassActionDefnFkId(String canvassActionDefnFkId) {
		this.canvassActionDefnFkId = canvassActionDefnFkId;
	}
}
