package com.tapplent.platformutility.layout.valueObject;

public class CtAcDtArVO {
	private String baseTemplateId;
	private String canvasTypeCode;
	private String actionCategory;
	private String deviceTypeCode;
	private String arCode;
	private int arSeqNumber;
	public String getCanvasTypeCode() {
		return canvasTypeCode;
	}
	public void setCanvasTypeCode(String canvasTypeCode) {
		this.canvasTypeCode = canvasTypeCode;
	}
	public String getActionCategory() {
		return actionCategory;
	}
	public void setActionCategory(String actionCategory) {
		this.actionCategory = actionCategory;
	}
	public String getDeviceTypeCode() {
		return deviceTypeCode;
	}
	public void setDeviceTypeCode(String deviceTypeCode) {
		this.deviceTypeCode = deviceTypeCode;
	}
	public String getArCode() {
		return arCode;
	}
	public void setArCode(String arCode) {
		this.arCode = arCode;
	}
	public int getArSeqNumber() {
		return arSeqNumber;
	}
	public void setArSeqNumber(int arSeqNumber) {
		this.arSeqNumber = arSeqNumber;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	
}
