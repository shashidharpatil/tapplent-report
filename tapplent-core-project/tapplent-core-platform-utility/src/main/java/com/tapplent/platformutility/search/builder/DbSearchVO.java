package com.tapplent.platformutility.search.builder;

import java.util.ArrayList;
import java.util.List;

public class DbSearchVO {
    //private DbSearchTemplate[] tables;  // template[0] as primary object
    private List<DbSearchTemplate> tables;
    private List<DbSearchRelation> relations;// for N templates, N-1 relations
    private Integer page;
    private Integer pageSize;
    public DbSearchVO(){
    	this.tables = new ArrayList<DbSearchTemplate>();
    	this.relations = new ArrayList<DbSearchRelation>();
    }
	public List<DbSearchTemplate> getTables() {
		return tables;
	}

	public void setTables(List<DbSearchTemplate> tables) {
		this.tables = tables;
	}

	public List<DbSearchRelation> getRelations() {
		return relations;
	}
	public void setRelations(List<DbSearchRelation> relations) {
		this.relations = relations;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
    
}
