package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class ActionStandAloneVO {
	/*ACTION STANDALONE EU FK ID
	CANVAS EU FK ID           
	CONTROL EU FK ID          
	PROPERTY CONTROL EU FK ID 
	BASE TEMPLATE FK ID       
	MT PROCESS ELEMENT FK ID  
	ACTION CODE FK ID         
	DEVICE INDEPENDENT BLOB   
	DEVICE DEPENDENT BLOB     
	DEVICE APPLICABLE BLOB    
	ACTION STEP PROPERTY BLOB 
	*/
	private String actionStandaloneId;
	private String canvasId;
	private String controlId;
	private String propertyControlId;
	private String baseTemplateId;
	private String mtPEId; 
	private String actionCode;
	private String controlTypeCode;
	private String actionLabel;
	private String actionIcon;
	private boolean isShowActionLabel;
	private boolean isShowActionIcon;
	private String actionOnStateIcon;
	private String actionOffStateIcon;
	private boolean isLicenced;
	private boolean isCreateAccessControlEquivalent;
	private boolean isReadAccessControlEquivalent;
	private boolean isUpdateAccessControlEquivalent;
	private boolean isDeleteAccessControlEquivalent;
	private int actionSeqNo;
	private JsonNode mstDeviceIndependentBlob;
	private JsonNode txnDeviceIndependentBlob;
	private JsonNode mstDeviceDependentBlob;
	private JsonNode txnDeviceDependentBlob;
	private JsonNode deviceApplicableBlob;
	private JsonNode mstActionStepPropertyBlob;
	private JsonNode txnActionStepPropertyBlob;
	public String getActionStandaloneId() {
		return actionStandaloneId;
	}
	public void setActionStandaloneId(String actionStandaloneId) {
		this.actionStandaloneId = actionStandaloneId;
	}
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public String getControlId() {
		return controlId;
	}
	public void setControlId(String controlId) {
		this.controlId = controlId;
	}
	public String getPropertyControlId() {
		return propertyControlId;
	}
	public void setPropertyControlId(String propertyControlId) {
		this.propertyControlId = propertyControlId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPEId() {
		return mtPEId;
	}
	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}
	public String getActionCode() {
		return actionCode;
	}
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public JsonNode getDeviceApplicableBlob() {
		return deviceApplicableBlob;
	}
	public void setDeviceApplicableBlob(JsonNode deviceApplicableBlob) {
		this.deviceApplicableBlob = deviceApplicableBlob;
	}
	public boolean isShowActionLabel() {
		return isShowActionLabel;
	}
	public void setShowActionLabel(boolean isShowActionLabel) {
		this.isShowActionLabel = isShowActionLabel;
	}
	public boolean isShowActionIcon() {
		return isShowActionIcon;
	}
	public void setShowActionIcon(boolean isShowActionIcon) {
		this.isShowActionIcon = isShowActionIcon;
	}
	public boolean isLicenced() {
		return isLicenced;
	}
	public void setLicenced(boolean isLicenced) {
		this.isLicenced = isLicenced;
	}
	public boolean isCreateAccessControlEquivalent() {
		return isCreateAccessControlEquivalent;
	}
	public void setCreateAccessControlEquivalent(boolean isCreateAccessControlEquivalent) {
		this.isCreateAccessControlEquivalent = isCreateAccessControlEquivalent;
	}
	public boolean isReadAccessControlEquivalent() {
		return isReadAccessControlEquivalent;
	}
	public void setReadAccessControlEquivalent(boolean isReadAccessControlEquivalent) {
		this.isReadAccessControlEquivalent = isReadAccessControlEquivalent;
	}
	public boolean isUpdateAccessControlEquivalent() {
		return isUpdateAccessControlEquivalent;
	}
	public void setUpdateAccessControlEquivalent(boolean isUpdateAccessControlEquivalent) {
		this.isUpdateAccessControlEquivalent = isUpdateAccessControlEquivalent;
	}
	public boolean isDeleteAccessControlEquivalent() {
		return isDeleteAccessControlEquivalent;
	}
	public void setDeleteAccessControlEquivalent(boolean isDeleteAccessControlEquivalent) {
		this.isDeleteAccessControlEquivalent = isDeleteAccessControlEquivalent;
	}
	public int getActionSeqNo() {
		return actionSeqNo;
	}
	public void setActionSeqNo(int actionSeqNo) {
		this.actionSeqNo = actionSeqNo;
	}
	public String getControlTypeCode() {
		return controlTypeCode;
	}
	public void setControlTypeCode(String controlTypeCode) {
		this.controlTypeCode = controlTypeCode;
	}

	public String getActionLabel() {
		return actionLabel;
	}

	public void setActionLabel(String actionLabel) {
		this.actionLabel = actionLabel;
	}

	public String getActionIcon() {
		return actionIcon;
	}
	public void setActionIcon(String actionIcon) {
		this.actionIcon = actionIcon;
	}
	public String getActionOnStateIcon() {
		return actionOnStateIcon;
	}
	public void setActionOnStateIcon(String actionOnStateIcon) {
		this.actionOnStateIcon = actionOnStateIcon;
	}
	public String getActionOffStateIcon() {
		return actionOffStateIcon;
	}
	public void setActionOffStateIcon(String actionOffStateIcon) {
		this.actionOffStateIcon = actionOffStateIcon;
	}
	public JsonNode getMstDeviceIndependentBlob() {
		return mstDeviceIndependentBlob;
	}
	public void setMstDeviceIndependentBlob(JsonNode mstDeviceIndependentBlob) {
		this.mstDeviceIndependentBlob = mstDeviceIndependentBlob;
	}
	public JsonNode getTxnDeviceIndependentBlob() {
		return txnDeviceIndependentBlob;
	}
	public void setTxnDeviceIndependentBlob(JsonNode txnDeviceIndependentBlob) {
		this.txnDeviceIndependentBlob = txnDeviceIndependentBlob;
	}
	public JsonNode getMstDeviceDependentBlob() {
		return mstDeviceDependentBlob;
	}
	public void setMstDeviceDependentBlob(JsonNode mstDeviceDependentBlob) {
		this.mstDeviceDependentBlob = mstDeviceDependentBlob;
	}
	public JsonNode getTxnDeviceDependentBlob() {
		return txnDeviceDependentBlob;
	}
	public void setTxnDeviceDependentBlob(JsonNode txnDeviceDependentBlob) {
		this.txnDeviceDependentBlob = txnDeviceDependentBlob;
	}
	public JsonNode getMstActionStepPropertyBlob() {
		return mstActionStepPropertyBlob;
	}
	public void setMstActionStepPropertyBlob(JsonNode mstActionStepPropertyBlob) {
		this.mstActionStepPropertyBlob = mstActionStepPropertyBlob;
	}
	public JsonNode getTxnActionStepPropertyBlob() {
		return txnActionStepPropertyBlob;
	}
	public void setTxnActionStepPropertyBlob(JsonNode txnActionStepPropertyBlob) {
		this.txnActionStepPropertyBlob = txnActionStepPropertyBlob;
	}
	
	
//	private String propertyControlActionEuPkId;
//	private String basetemplateFkId;
//	private String propertyControlActionDefnFkId;
//	private String propertyControlDefnFkId;
//	private String propertyControlEuFkId;
//	private String actionCodeFkId;
//	private String actionGestureCodeFkId;
//	private String actionGestureAnimationCodeFkId;
//	public String getPropertyControlActionEuPkId() {
//		return propertyControlActionEuPkId;
//	}
//	public void setPropertyControlActionEuPkId(String propertyControlActionEuPkId) {
//		this.propertyControlActionEuPkId = propertyControlActionEuPkId;
//	}
//	public String getBasetemplateFkId() {
//		return basetemplateFkId;
//	}
//	public void setBasetemplateFkId(String basetemplateFkId) {
//		this.basetemplateFkId = basetemplateFkId;
//	}
//	public String getPropertyControlActionDefnFkId() {
//		return propertyControlActionDefnFkId;
//	}
//	public void setPropertyControlActionDefnFkId(String propertyControlActionDefnFkId) {
//		this.propertyControlActionDefnFkId = propertyControlActionDefnFkId;
//	}
//	public String getPropertyControlDefnFkId() {
//		return propertyControlDefnFkId;
//	}
//	public void setPropertyControlDefnFkId(String propertyControlDefnFkId) {
//		this.propertyControlDefnFkId = propertyControlDefnFkId;
//	}
//	public String getPropertyControlEuFkId() {
//		return propertyControlEuFkId;
//	}
//	public void setPropertyControlEuFkId(String propertyControlEuFkId) {
//		this.propertyControlEuFkId = propertyControlEuFkId;
//	}
//	public String getActionCodeFkId() {
//		return actionCodeFkId;
//	}
//	public void setActionCodeFkId(String actionCodeFkId) {
//		this.actionCodeFkId = actionCodeFkId;
//	}
//	public String getActionGestureCodeFkId() {
//		return actionGestureCodeFkId;
//	}
//	public void setActionGestureCodeFkId(String actionGestureCodeFkId) {
//		this.actionGestureCodeFkId = actionGestureCodeFkId;
//	}
//	public String getActionGestureAnimationCodeFkId() {
//		return actionGestureAnimationCodeFkId;
//	}
//	public void setActionGestureAnimationCodeFkId(String actionGestureAnimationCodeFkId) {
//		this.actionGestureAnimationCodeFkId = actionGestureAnimationCodeFkId;
//	}
}
