package com.tapplent.platformutility.layout.valueObject;

public class BaseObjectVO {
	private String baseObjectId;

	public String getBaseObjectId() {
		return baseObjectId;
	}

	public void setBaseObjectId(String baseObjectId) {
		this.baseObjectId = baseObjectId;
	}
}
