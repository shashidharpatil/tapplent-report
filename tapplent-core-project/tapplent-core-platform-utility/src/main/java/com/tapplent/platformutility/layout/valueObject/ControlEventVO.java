package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class ControlEventVO {
	private String controlEventId; 
	private String baseTemplateFkId;
	private String baseTemplateId;
	private String mtPE;
	private String controlId;	
	private JsonNode deviceIndependentBlob;
	private JsonNode deviceDependentBlob;
	public String getControlEventId() {
		return controlEventId;
	}
	public void setControlEventId(String controlEventId) {
		this.controlEventId = controlEventId;
	}
	public String getBaseTemplateFkId() {
		return baseTemplateFkId;
	}
	public void setBaseTemplateFkId(String baseTemplateFkId) {
		this.baseTemplateFkId = baseTemplateFkId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPE() {
		return mtPE;
	}
	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}
	public String getControlId() {
		return controlId;
	}
	public void setControlId(String controlId) {
		this.controlId = controlId;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
}
