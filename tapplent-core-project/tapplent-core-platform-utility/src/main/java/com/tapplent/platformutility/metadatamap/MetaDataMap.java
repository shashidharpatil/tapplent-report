package com.tapplent.platformutility.metadatamap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityAttributeRelationMetadata;

public class MetaDataMap {
	
	private Map<String, EntityAttributeMetadata> attributeNameMap = new HashMap<String, EntityAttributeMetadata>();
	private Map<String, EntityAttributeMetadata> attributeColumnNameMap = new HashMap<String, EntityAttributeMetadata>();
	private Map<String, EntityAttributeMetadata> attributeBtDoaSpecMap = new HashMap<>();
	private Map<String,Map<String, EntityAttributeRelationMetadata>> relatedAttributeNameMap = new HashMap<>();
	private Map<String,Map<String, EntityAttributeRelationMetadata>> relatedAttributeColumnNameMap = new HashMap<>();
//	private List<SelectItem> selectedList = new ArrayList<SelectItem>();
	private String tableName;
	private EntityAttributeMetadata primaryKeyAttribute;
	private EntityAttributeMetadata versoinIdAttribute;
	private String pfmProcessElementFkId;
	private String baseTemplateFkId;
	private String baseObjectFkId;
	private String domainObjectFkId;
	private String domainObjectNameG11nText;
	
	

	public void setAttributeByName(String attributeName, EntityAttributeMetadata attribute) {
		this.attributeNameMap.put(attributeName, attribute);
	}

	public EntityAttributeMetadata getAttributeByName(String attributeName) {
		return this.attributeNameMap.get(attributeName);
	}

	public void setAttributeByColumnName(String columnName, EntityAttributeMetadata attribute) {
		this.attributeColumnNameMap.put(columnName, attribute);
	}

	public EntityAttributeMetadata getAttributeByColumnName(String columnName) {
		return this.attributeColumnNameMap.get(columnName);
	}

	public void setAttributeByBtDoaSpec(String btDoaSpecId, EntityAttributeMetadata attribute) {
		this.attributeBtDoaSpecMap.put(btDoaSpecId, attribute);
	}

	public EntityAttributeMetadata getAttributeByBtDoaSpec(String btDoaSpecId) {
		return this.attributeBtDoaSpecMap.get(btDoaSpecId);
	}
	
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableName() {
		return tableName;
	}
	
	public void setPrimaryKeyAttribute(EntityAttributeMetadata attributeEntity) {
		this.primaryKeyAttribute=attributeEntity;
	}

	public EntityAttributeMetadata getPrimaryKeyAttribute() {
		return primaryKeyAttribute;
	}

	public EntityAttributeMetadata getVersoinIdAttribute() {
		return versoinIdAttribute;
	}

	public void setVersoinIdAttribute(EntityAttributeMetadata versoinIdAttribute) {
		this.versoinIdAttribute = versoinIdAttribute;
	}

	public String getPfmProcessElementFkId() {
		return pfmProcessElementFkId;
	}

	public void setPfmProcessElementFkId(String pfmProcessElementFkId) {
		this.pfmProcessElementFkId = pfmProcessElementFkId;
	}

	public String getBaseTemplateFkId() {
		return baseTemplateFkId;
	}

	public void setBaseTemplateFkId(String baseTemplateFkId) {
		this.baseTemplateFkId = baseTemplateFkId;
	}

	public String getBaseObjectFkId() {
		return baseObjectFkId;
	}

	public void setBaseObjectFkId(String baseObjectFkId) {
		this.baseObjectFkId = baseObjectFkId;
	}

	public String getDomainObjectFkId() {
		return domainObjectFkId;
	}

	public void setDomainObjectFkId(String domainObjectFkId) {
		this.domainObjectFkId = domainObjectFkId;
	}

	public String getDomainObjectNameG11nText() {
		return domainObjectNameG11nText;
	}

	public void setDomainObjectNameG11nText(String domainObjectNameG11nText) {
		this.domainObjectNameG11nText = domainObjectNameG11nText;
	}

	public Map<String, Map<String, EntityAttributeRelationMetadata>> getRelatedAttributeNameMap() {
		return relatedAttributeNameMap;
	}

	public void setRelatedAttributeNameMap(
			Map<String, Map<String, EntityAttributeRelationMetadata>> relatedAttributeNameMap) {
		this.relatedAttributeNameMap = relatedAttributeNameMap;
	}

	public Map<String, Map<String, EntityAttributeRelationMetadata>> getRelatedAttributeColumnNameMap() {
		return relatedAttributeColumnNameMap;
	}

	public void setRelatedAttributeColumnNameMap(
			Map<String, Map<String, EntityAttributeRelationMetadata>> relatedAttributeColumnNameMap) {
		this.relatedAttributeColumnNameMap = relatedAttributeColumnNameMap;
	}

	public Map<String, EntityAttributeMetadata> getAttributeNameMap() {
		return attributeNameMap;
	}

	public Map<String, EntityAttributeMetadata> getAttributeColumnNameMap() {
		return attributeColumnNameMap;
	}

	public void setAttributeColumnNameMap(Map<String, EntityAttributeMetadata> attributeColumnNameMap) {
		this.attributeColumnNameMap = attributeColumnNameMap;
	}
	
//	public void setRelatedAttributeByName(String relatedAttributeName, EntityAttributeRelationMetadata attribute) {
//		this.relatedAttributeNameMap.put(relatedAttributeName, attribute);
//	}
//
//	public EntityAttributeRelationMetadata getRelatedAttributeByName(String relatedAttributeName) {
//		return this.relatedAttributeNameMap.get(relatedAttributeName);
//	}
//
//	public void setRelatedAttributeByColumnName(String columnName, EntityAttributeRelationMetadata attribute) {
//		this.relatedAttributeColumnNameMap.put(columnName, attribute);
//	}
//
//	public EntityAttributeRelationMetadata getRelatedAttributeByColumnName(String columnName) {
//		return this.relatedAttributeColumnNameMap.get(columnName);
//	}

	
}
