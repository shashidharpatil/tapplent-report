package com.tapplent.platformutility.uilayout.valueobject;

import com.tapplent.platformutility.layout.valueObject.IconVO;

/**
 * Created by tapplent on 25/04/17.
 */
public class ControlStaticValuesVO {
    private String staticPkId;
    private String controlFkId;
    private String staticText;
    private String staticIcon;
    private String staticImage;
    private int valueItemNumber;

    public String getStaticPkId() {
        return staticPkId;
    }

    public void setStaticPkId(String staticPkId) {
        this.staticPkId = staticPkId;
    }

    public String getStaticText() {
        return staticText;
    }

    public void setStaticText(String staticText) {
        this.staticText = staticText;
    }

    public String getStaticIcon() {
        return staticIcon;
    }

    public void setStaticIcon(String staticIcon) {
        this.staticIcon = staticIcon;
    }

    public int getValueItemNumber() {
        return valueItemNumber;
    }

    public void setValueItemNumber(int valueItemNumber) {
        this.valueItemNumber = valueItemNumber;
    }

    public String getControlFkId() {
        return controlFkId;
    }

    public void setControlFkId(String controlFkId) {
        this.controlFkId = controlFkId;
    }

    public String getStaticImage() {
        return staticImage;
    }

    public void setStaticImage(String staticImage) {
        this.staticImage = staticImage;
    }
}
