package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class PageRelationVO {
	private String pageChildEuPkId;
	private String baseTemplateId;
	private String deviceType;
	private String pageChildEquFkId;
	private String fromPageEuFkId;
	private String toPageEuFkId;
	private String startingCanvasId;
	private JsonNode childPagePlacement;
	public String getPageChildEuPkId() {
		return pageChildEuPkId;
	}
	public void setPageChildEuPkId(String pageChildEuPkId) {
		this.pageChildEuPkId = pageChildEuPkId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getPageChildEquFkId() {
		return pageChildEquFkId;
	}
	public void setPageChildEquFkId(String pageChildEquFkId) {
		this.pageChildEquFkId = pageChildEquFkId;
	}
	public String getFromPageEuFkId() {
		return fromPageEuFkId;
	}
	public void setFromPageEuFkId(String fromPageEuFkId) {
		this.fromPageEuFkId = fromPageEuFkId;
	}
	public String getToPageEuFkId() {
		return toPageEuFkId;
	}
	public void setToPageEuFkId(String toPageEuFkId) {
		this.toPageEuFkId = toPageEuFkId;
	}
	public String getStartingCanvasId() {
		return startingCanvasId;
	}
	public void setStartingCanvasId(String startingCanvasId) {
		this.startingCanvasId = startingCanvasId;
	}
	public JsonNode getChildPagePlacement() {
		return childPagePlacement;
	}
	public void setChildPagePlacement(JsonNode childPagePlacement) {
		this.childPagePlacement = childPagePlacement;
	}
}
