package com.tapplent.platformutility.search.builder;

public final class SearchConstants {
	public static final String ID_DOMAIN_TEMPLATE_MAP = "idDomainTemplateMap";
    public static final String CURRENT_DOMAIN_OBJECT = "currentDomainObject";
    public static final String ENTITY_TABLE_DEFAULT_PRIMARY_COLUMN_NAME = "ENTITY_INSTANCE_ID";
    public static final String DOMAIN_OBJECT_PSEUDO_SEARCH_ID_COLUMN = "id";
    public static final String LEFT_OUTER_JOIN = "left_outer_join";

    public static final String JOIN = "join";
    public static final String MAPPING_SOURCE_COLUMN = "MAPPING_ID";
    public static final String MAPPING_TARGET_COLUMN = "RELATED_ENTITY_INSTANCE_ID";
    public static final String RIGHT_OUTER_JOIN = "right_outer_join";
    public static final String INNER_JOIN = "inner_join";
    public static final String TEMPLATE_ID = "template_id";
    public static final String TEMPLATE_ID_ALIAS = "####template#id#####";
    public static final Integer DEFAULT_PAGE_SIZE = 25;
    public static final String NAMED_PARAMETER_REGEX_PATTERN = "\\A#\\{\\w+}\\z";
}