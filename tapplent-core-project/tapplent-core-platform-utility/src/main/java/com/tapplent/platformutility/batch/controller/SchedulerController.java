package com.tapplent.platformutility.batch.controller;

import com.tapplent.platformutility.batch.scheduler.SchedulerInitOnStartUp;
import com.tapplent.tenantresolver.tenant.TenantRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.DriverManager;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by sripat on 29/08/16.
 */

@Controller
@RequestMapping("/tapp/batch/v1")
public class SchedulerController {

    Runnable schlCallable;
    Runnable schInit;

    public Runnable getSchlCallable() {
        return schlCallable;
    }

    public void setSchlCallable(Runnable schCallable) {
        this.schlCallable = schCallable;
    }

    public Runnable getSchInit() {
        return schInit;
    }

    public void setSchInit(Runnable schInit) {
        this.schInit = schInit;
    }

    @RequestMapping(value = "/batch/t/{tenantId}/u/{personId}/e/", method = RequestMethod.POST)
    @ResponseBody
    ResponseEntity<String> startBatch(@PathVariable String tenantId,
                                      @PathVariable String personId) {
        ScheduledExecutorService executor = new ScheduledThreadPoolExecutor(20);
        executor.scheduleWithFixedDelay(schlCallable, 0,5, TimeUnit.SECONDS);
        schInit.run();
        return new ResponseEntity<String>("" + TenantRegistry.getInstance().getTenantIdTenantMap().size(), HttpStatus.OK);
    }
}
