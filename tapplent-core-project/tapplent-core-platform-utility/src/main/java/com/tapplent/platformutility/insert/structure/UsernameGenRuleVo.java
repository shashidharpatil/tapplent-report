package com.tapplent.platformutility.insert.structure;

public class UsernameGenRuleVo {
    private String usernameGenRulePkId;
    private String formula;

    public String getUsernameGenRulePkId() {
        return usernameGenRulePkId;
    }

    public void setUsernameGenRulePkId(String usernameGenRulePkId) {
        this.usernameGenRulePkId = usernameGenRulePkId;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
}
