package com.tapplent.platformutility.accessmanager.structure;

import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 02/11/16.
 */
public class ResolvedRowCondition {
    private String resolvedRowPkId;
    private String whoPersonId;
    private String domainObjectId;
    private String dbPrimaryKeyId;
    private String functionalPrimaryKeyid;
    private String subjectUserId;
    private boolean isCreateCurrentPermitted;
    private boolean isCreateHistoryPermitted;
    private boolean isCreateFuturePermitted;
    private boolean isReadCurrentPermitted;
    private boolean isReadHistoryPermitted;
    private boolean isReadFuturePermitted;
    private boolean isUpdateCurrentPermitted;
    private boolean isUpdateHistoryPermitted;
    private boolean isUpdateFuturePermitted;
    private boolean isDeleteCurrentPermitted;
    private boolean isDeleteHistoryPermitted;
    private boolean isDeleteFuturePermitted;
    private Map<String, ResolvedColumnCondition> doaToColumnConditionMap;


    public String getResolvedRowPkId() {
        return resolvedRowPkId;
    }

    public void setResolvedRowPkId(String resolvedRowPkId) {
        this.resolvedRowPkId = resolvedRowPkId;
    }

    public String getWhoPersonId() {
        return whoPersonId;
    }

    public void setWhoPersonId(String whoPersonId) {
        this.whoPersonId = whoPersonId;
    }

    public String getDomainObjectId() {
        return domainObjectId;
    }

    public void setDomainObjectId(String domainObjectId) {
        this.domainObjectId = domainObjectId;
    }

    public String getDbPrimaryKeyId() {
        return dbPrimaryKeyId;
    }

    public void setDbPrimaryKeyId(String dbPrimaryKeyId) {
        this.dbPrimaryKeyId = dbPrimaryKeyId;
    }

    public String getFunctionalPrimaryKeyid() {
        return functionalPrimaryKeyid;
    }

    public void setFunctionalPrimaryKeyid(String functionalPrimaryKeyid) {
        this.functionalPrimaryKeyid = functionalPrimaryKeyid;
    }

    public String getSubjectUserId() {
        return subjectUserId;
    }

    public void setSubjectUserId(String subjectUserId) {
        this.subjectUserId = subjectUserId;
    }

    public boolean isCreateCurrentPermitted() {
        return isCreateCurrentPermitted;
    }

    public void setCreateCurrentPermitted(boolean createCurrentPermitted) {
        isCreateCurrentPermitted = createCurrentPermitted;
    }

    public boolean isCreateHistoryPermitted() {
        return isCreateHistoryPermitted;
    }

    public void setCreateHistoryPermitted(boolean createHistoryPermitted) {
        isCreateHistoryPermitted = createHistoryPermitted;
    }

    public boolean isCreateFuturePermitted() {
        return isCreateFuturePermitted;
    }

    public void setCreateFuturePermitted(boolean createFuturePermitted) {
        isCreateFuturePermitted = createFuturePermitted;
    }

    public boolean isReadCurrentPermitted() {
        return isReadCurrentPermitted;
    }

    public void setReadCurrentPermitted(boolean readCurrentPermitted) {
        isReadCurrentPermitted = readCurrentPermitted;
    }

    public boolean isReadHistoryPermitted() {
        return isReadHistoryPermitted;
    }

    public void setReadHistoryPermitted(boolean readHistoryPermitted) {
        isReadHistoryPermitted = readHistoryPermitted;
    }

    public boolean isReadFuturePermitted() {
        return isReadFuturePermitted;
    }

    public void setReadFuturePermitted(boolean readFuturePermitted) {
        isReadFuturePermitted = readFuturePermitted;
    }

    public boolean isUpdateCurrentPermitted() {
        return isUpdateCurrentPermitted;
    }

    public void setUpdateCurrentPermitted(boolean updateCurrentPermitted) {
        isUpdateCurrentPermitted = updateCurrentPermitted;
    }

    public boolean isUpdateHistoryPermitted() {
        return isUpdateHistoryPermitted;
    }

    public void setUpdateHistoryPermitted(boolean updateHistoryPermitted) {
        isUpdateHistoryPermitted = updateHistoryPermitted;
    }

    public boolean isUpdateFuturePermitted() {
        return isUpdateFuturePermitted;
    }

    public void setUpdateFuturePermitted(boolean updateFuturePermitted) {
        isUpdateFuturePermitted = updateFuturePermitted;
    }

    public boolean isDeleteCurrentPermitted() {
        return isDeleteCurrentPermitted;
    }

    public void setDeleteCurrentPermitted(boolean deleteCurrentPermitted) {
        isDeleteCurrentPermitted = deleteCurrentPermitted;
    }

    public boolean isDeleteHistoryPermitted() {
        return isDeleteHistoryPermitted;
    }

    public void setDeleteHistoryPermitted(boolean deleteHistoryPermitted) {
        isDeleteHistoryPermitted = deleteHistoryPermitted;
    }

    public boolean isDeleteFuturePermitted() {
        return isDeleteFuturePermitted;
    }

    public void setDeleteFuturePermitted(boolean deleteFuturePermitted) {
        isDeleteFuturePermitted = deleteFuturePermitted;
    }

    public Map<String, ResolvedColumnCondition> getDoaToColumnConditionMap() {
        return doaToColumnConditionMap;
    }

    public void setDoaToColumnConditionMap(Map<String, ResolvedColumnCondition> doaToColumnConditionMap) {
        this.doaToColumnConditionMap = doaToColumnConditionMap;
    }
}
