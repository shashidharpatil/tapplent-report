package com.tapplent.platformutility.accessmanager.structure;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by tapplent on 04/11/16.
 */
public class AMRowCondition {
    private String rowCondnPkId;
    private String accessProcessFkId;
    private String whatMtPeCodeFkId;
    private String whatDoCodeFkId;
    private String whatMtPeFilterExpn;
    private String parentRowCondnFkId;
    private Date startDate;
    private Date endDate;
    private boolean isPermissionNeverEnds;
    private String subjectUserId;
    private boolean isCreateCurrentPermitted;
    private boolean isCreateHistoryPermitted;
    private boolean isCreateFuturePermitted;
    private boolean isReadCurrentPermitted;
    private boolean isReadHistoryPermitted;
    private boolean isReadFuturePermitted;
    private boolean isUpdateCurrentPermitted;
    private boolean isUpdateHistoryPermitted;
    private boolean isUpdateFuturePermitted;
    private boolean isDeleteCurrentPermitted;
    private boolean isDeleteHistoryPermitted;
    private boolean isDeleteFuturePermitted;
    List<AMColCondition> colConditions = new ArrayList<>();

    public String getRowCondnPkId() {
        return rowCondnPkId;
    }

    public void setRowCondnPkId(String rowCondnPkId) {
        this.rowCondnPkId = rowCondnPkId;
    }

    public String getAccessProcessFkId() {
        return accessProcessFkId;
    }

    public void setAccessProcessFkId(String accessProcessFkId) {
        this.accessProcessFkId = accessProcessFkId;
    }

    public String getWhatMtPeCodeFkId() {
        return whatMtPeCodeFkId;
    }

    public void setWhatMtPeCodeFkId(String whatMtPeCodeFkId) {
        this.whatMtPeCodeFkId = whatMtPeCodeFkId;
    }

    public String getWhatDoCodeFkId() {
        return whatDoCodeFkId;
    }

    public void setWhatDoCodeFkId(String whatDoCodeFkId) {
        this.whatDoCodeFkId = whatDoCodeFkId;
    }

    public String getWhatMtPeFilterExpn() {
        return whatMtPeFilterExpn;
    }

    public void setWhatMtPeFilterExpn(String whatMtPeFilterExpn) {
        this.whatMtPeFilterExpn = whatMtPeFilterExpn;
    }

    public String getParentRowCondnFkId() {
        return parentRowCondnFkId;
    }

    public void setParentRowCondnFkId(String parentRowCondnFkId) {
        this.parentRowCondnFkId = parentRowCondnFkId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isPermissionNeverEnds() {
        return isPermissionNeverEnds;
    }

    public void setPermissionNeverEnds(boolean permissionNeverEnds) {
        isPermissionNeverEnds = permissionNeverEnds;
    }

    public String getSubjectUserId() {
        return subjectUserId;
    }

    public void setSubjectUserId(String subjectUserId) {
        this.subjectUserId = subjectUserId;
    }

    public boolean isCreateCurrentPermitted() {
        return isCreateCurrentPermitted;
    }

    public void setCreateCurrentPermitted(boolean createCurrentPermitted) {
        isCreateCurrentPermitted = createCurrentPermitted;
    }

    public boolean isCreateHistoryPermitted() {
        return isCreateHistoryPermitted;
    }

    public void setCreateHistoryPermitted(boolean createHistoryPermitted) {
        isCreateHistoryPermitted = createHistoryPermitted;
    }

    public boolean isCreateFuturePermitted() {
        return isCreateFuturePermitted;
    }

    public void setCreateFuturePermitted(boolean createFuturePermitted) {
        isCreateFuturePermitted = createFuturePermitted;
    }

    public boolean isReadCurrentPermitted() {
        return isReadCurrentPermitted;
    }

    public void setReadCurrentPermitted(boolean readCurrentPermitted) {
        isReadCurrentPermitted = readCurrentPermitted;
    }

    public boolean isReadHistoryPermitted() {
        return isReadHistoryPermitted;
    }

    public void setReadHistoryPermitted(boolean readHistoryPermitted) {
        isReadHistoryPermitted = readHistoryPermitted;
    }

    public boolean isReadFuturePermitted() {
        return isReadFuturePermitted;
    }

    public void setReadFuturePermitted(boolean readFuturePermitted) {
        isReadFuturePermitted = readFuturePermitted;
    }

    public boolean isUpdateCurrentPermitted() {
        return isUpdateCurrentPermitted;
    }

    public void setUpdateCurrentPermitted(boolean updateCurrentPermitted) {
        isUpdateCurrentPermitted = updateCurrentPermitted;
    }

    public boolean isUpdateHistoryPermitted() {
        return isUpdateHistoryPermitted;
    }

    public void setUpdateHistoryPermitted(boolean updateHistoryPermitted) {
        isUpdateHistoryPermitted = updateHistoryPermitted;
    }

    public boolean isUpdateFuturePermitted() {
        return isUpdateFuturePermitted;
    }

    public void setUpdateFuturePermitted(boolean updateFuturePermitted) {
        isUpdateFuturePermitted = updateFuturePermitted;
    }

    public boolean isDeleteCurrentPermitted() {
        return isDeleteCurrentPermitted;
    }

    public void setDeleteCurrentPermitted(boolean deleteCurrentPermitted) {
        isDeleteCurrentPermitted = deleteCurrentPermitted;
    }

    public boolean isDeleteHistoryPermitted() {
        return isDeleteHistoryPermitted;
    }

    public void setDeleteHistoryPermitted(boolean deleteHistoryPermitted) {
        isDeleteHistoryPermitted = deleteHistoryPermitted;
    }

    public boolean isDeleteFuturePermitted() {
        return isDeleteFuturePermitted;
    }

    public void setDeleteFuturePermitted(boolean deleteFuturePermitted) {
        isDeleteFuturePermitted = deleteFuturePermitted;
    }

    public List<AMColCondition> getColConditions() {
        return colConditions;
    }

    public void setColConditions(List<AMColCondition> colConditions) {
        this.colConditions = colConditions;
    }
}
