package com.tapplent.platformutility.metadatamap;

import java.util.HashMap;
import java.util.Map;

import com.tapplent.platformutility.expression.jeval.function.math.Log;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityAttributeRelationMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadata;

public class MetaDataMapBuilderImpl implements MetaDataMapBuilder {

	@Override
	public MetaDataMap getMetaDataMap(EntityMetadata entityMetadata) {
		MetaDataMap metaDataMap = constructMetaDataMap(entityMetadata);
		populateAttributeMetaData(metaDataMap, entityMetadata);
		return metaDataMap;
	}

	private void populateAttributeMetaData(MetaDataMap metaDataMap, EntityMetadata entityMetadata) {
//		EntityAttributeMetadata primaryAttribute = null;
		for (EntityAttributeMetadata entityAttributeMetadata : entityMetadata.getAttributeMeta()) {
//			templateEntity.addSelectedItem(new SelectItem(attributeEntity.getAttributeName(), attributeEntity
//					.getColumnName()));
			metaDataMap.setAttributeByName(entityAttributeMetadata.getDoAttributeCode().trim(), entityAttributeMetadata);
			metaDataMap.setAttributeByColumnName(entityAttributeMetadata.getDbColumnName().trim(), entityAttributeMetadata);
//			metaDataMap.setAttributeByBtDoaSpec(entityAttributeMetadata.getBtDoAttributeSpecFkId().trim(), entityAttributeMetadata);
			Map<String, EntityAttributeRelationMetadata> relatedAttributeByName = new HashMap<>();
			Map<String, EntityAttributeRelationMetadata> relatedAttributeByColumnName = new HashMap<>();
			for(EntityAttributeRelationMetadata attributeRelationMetadata : entityAttributeMetadata.getRelationMeta()){
				relatedAttributeByName.put(attributeRelationMetadata.getMtDoAttributeSpecG11nText().trim(), attributeRelationMetadata);
				relatedAttributeByColumnName.put(attributeRelationMetadata.getDbColumnName().trim(), attributeRelationMetadata);
//				metaDataMap.setRelatedAttributeByColumnName(attributeRelationMetadata.getMtDoAttributeSpecG11nText(), attributeRelationMetadata);
			}
			Map<String, Map<String, EntityAttributeRelationMetadata>>  relatedAttributeNameMap = new HashMap<>();
			relatedAttributeNameMap.put(entityAttributeMetadata.getDoAttributeCode(), relatedAttributeByName);
			Map<String, Map<String, EntityAttributeRelationMetadata>>  relatedAttributeColumnNameMap = new HashMap<>();
			relatedAttributeColumnNameMap.put(entityAttributeMetadata.getDoAttributeCode(), relatedAttributeByColumnName);
			metaDataMap.setRelatedAttributeNameMap(relatedAttributeNameMap);
			metaDataMap.setRelatedAttributeColumnNameMap(relatedAttributeColumnNameMap);
			
			if(entityAttributeMetadata.isFunctionalPrimaryKeyFlag()){
				metaDataMap.setPrimaryKeyAttribute(entityAttributeMetadata);
//				primaryEntity=attributeEntity;
			}
			if(entityAttributeMetadata.isDbPrimaryKeyFlag()){
				metaDataMap.setVersoinIdAttribute(entityAttributeMetadata);
			}
		}
//		if(null!=primaryAttribute){
//			metaDataMap.setPrimaryKeyAttribute(primaryAttribute);
//			templateEntity.addSelectedItem(new SelectItem(primaryEntity.getAttributeName(), primaryEntity
//					.getColumnName()));
//			
//			templateEntity.setAttributeByName(primaryEntity.getAttributeName(), primaryEntity);
//			templateEntity.setAttributeByColumnName(primaryEntity.getColumnName(), primaryEntity);
//		}
	}

	private MetaDataMap constructMetaDataMap(EntityMetadata entityMetadata) {
		MetaDataMap metaDataMap = new MetaDataMap();
		metaDataMap.setTableName(entityMetadata.getDbTableName());
		metaDataMap.setPfmProcessElementFkId(entityMetadata.getBtPE());
//		metaDataMap.setBaseObjectFkId(entityMetadata.getMetaProcessCode());
		metaDataMap.setBaseTemplateFkId(entityMetadata.getBaseTemplateId());
		metaDataMap.setDomainObjectFkId(entityMetadata.getDomainObjectCode());
//		metaDataMap.setDomainObjectNameG11nText(entityMetadata.getDomainObjectNameG11nText());
		return metaDataMap;
	}
	
}
