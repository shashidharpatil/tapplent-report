package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by sripat on 02/11/16.
 */
public class LayoutRequest {
    private String targetScreenInstance;
    private  String deviceType;

    public String getTargetScreenInstance() {
        return targetScreenInstance;
    }

    public void setTargetScreenInstance(String targetScreenInstance) {
        this.targetScreenInstance = targetScreenInstance;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
