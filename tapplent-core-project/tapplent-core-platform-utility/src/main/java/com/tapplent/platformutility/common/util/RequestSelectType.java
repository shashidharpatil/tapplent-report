/**
 * 
 */
package com.tapplent.platformutility.common.util;

/**
 * @author Shubham Patodi
 *
 */
public enum RequestSelectType {
	LAYOUT("layout"),
	RELATION("relation"),
	HIERARCHY("hierarchy"),
	ExternalAPI("externalAPI");
	private String selectType;
	RequestSelectType(String selectType){
		this.selectType = selectType;
	}
	public String getSelectType(){
		return selectType;
	}
}
