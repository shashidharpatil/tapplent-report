package com.tapplent.platformutility.batch.model;

public class RCRD_STATE_MtDOData {
    private String versionId;
    private String domainObjectCode;
    private String dbTableName;
    private String doTypeCode;
    private String effectiveDatetime;
    private boolean isCustomizable;
    private String dbPkColumnName;


    public String getDbPkColumnName() {
        return dbPkColumnName;
    }

    public void setDbPkColumnName(String dbPkColumnName) {
        this.dbPkColumnName = dbPkColumnName;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getDomainObjectCode() {
        return domainObjectCode;
    }

    public void setDomainObjectCode(String domainObjectCode) {
        this.domainObjectCode = domainObjectCode;
    }

    public String getDbTableName() {
        return dbTableName;
    }

    public void setDbTableName(String dbTableName) {
        this.dbTableName = dbTableName;
    }

    public String getDoTypeCode() {
        return doTypeCode;
    }

    public void setDoTypeCode(String doTypeCode) {
        this.doTypeCode = doTypeCode;
    }

    public String getEffectiveDatetime() {
        return effectiveDatetime;
    }

    public void setEffectiveDatetime(String effectiveDatetime) {
        this.effectiveDatetime = effectiveDatetime;
    }

    public boolean isCustomizable() {
        return isCustomizable;
    }

    public void setCustomizable(boolean customizable) {
        isCustomizable = customizable;
    }
}
