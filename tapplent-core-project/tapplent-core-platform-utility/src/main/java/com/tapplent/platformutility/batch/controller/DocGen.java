package com.tapplent.platformutility.batch.controller;

import java.util.concurrent.Callable;

import com.tapplent.platformutility.batch.model.SchedulerInstanceModel;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DocGen implements Callable {
    SchedulerInstanceModel schedulerInstanceModel;

    public DocGen(SchedulerInstanceModel schedulerInstanceModel) {
        this.schedulerInstanceModel = schedulerInstanceModel;
    }

    @Override
    public Object call() throws Exception {
        String[] springConfig = {"jobs/DocGen.xml"};
        ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
        JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("schedulerInstancePkId", schedulerInstanceModel.getSchedulerInstancePkId());
        Job job = (Job) context.getBean("DocumentGeneration");
        JobExecution execution = null;
        try {
            execution = jobLauncher.run(job, jobParametersBuilder.toJobParameters());
            System.out.println("ststus" + execution.getExitStatus());
        } catch (Exception e) {
            System.out.println("ststus" + execution.getExitStatus());
            e.printStackTrace();
        }
        return null;
    }

}
