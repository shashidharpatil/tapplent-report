package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.util.List;
import java.util.Map;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;

public interface EverNoteDAO {
	
	Map<String, Object> isSynchNeeded(String personPkId, String string,String eventpk);

	List<Meeting> getAllMeetings(String personPkId);

	Meeting getMeeting(String tapplentMeetingPkId,Map<String, EntityAttributeMetadata> attributeMetaMap,String personId);

	List<String> getAllNotes(String thirdPartyAppName);

	SingleNote getNote(String tapplentNotePkId, Map<String, EntityAttributeMetadata> attributeMetaMap, String personPkId);

	Map<String,String> getToken(String socialAccType, String personFkId);

	List<EventAttchment> getMeetingAttachments(String meetingId);

	List<EventAttchment> getNoteAttachments(String noteId);

	String getTheEventId(String thirdPartyEventId, String tableName, String integrationAppCode, String eventId);
	
	Task getTask(String tapplentTaskPkId, Map<String, EntityAttributeMetadata> attributeMetaMap, String personPkId);

	String getThirdPartyEventId(String tapplentEventPkId, String tableName, String integrationAppCode,String tapplentEventFkId);

	List<String> getEventPkId(String noteSyncFkId,String tableName,String eventFkId,String noteSynkId);

	String getEventFkIdFromIntegration(String evernoteId,String tableName,String eventFkId);

	List<MeetingInvitee> getAllMeetingInvitee(String meetingId);

	MeetingIntegration getIntegrationPkId(String tableName, String eventFkId,String intgPkId,String tapplentMeetingId);

	Map<String, Object> getSynchDetailsPkId(String tableName,String eventFkId ,String tapplentEventId,String synchDetailsPkID);

	List<MeetingInvitee> getMeetingInvitee(String meetingId);

	Map<String,String> getPersonInfo(String personId);

	String getAttendeeInfo(String attendeeEmail);

	List<NoteSubTask> getNoteSubTasks(String tapplentNoteId,String personId);

	Map<String, String> getIntegrationInfo(String everNoteId, String tableName, String notePkId);

}
