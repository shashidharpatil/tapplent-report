package com.tapplent.platformutility.groupresolver.dao;

import com.tapplent.platformutility.groupresolver.valueobject.GroupVO;
import com.tapplent.platformutility.uilayout.valueobject.GenericQueryVO;

import java.sql.SQLException;
import java.util.Deque;
import java.util.List;

/**
 * Created by tapplent on 12/06/17.
 */
public interface GroupResolverDAO {
    List<GroupVO> getAllGroups();

    void deleteAllResolvedGroups() throws SQLException;

    void executeUpdate(String s, Deque<Object> parameters) throws SQLException;

    GenericQueryVO getGenericQuery(String genericQueryID);

    void deleteAllResolvedGroupsByMtPE(List<GroupVO> groupsByMtPE) throws SQLException;
}
