/**
 * 
 */
package com.tapplent.platformutility.metadata.structure;

import java.util.*;

import com.tapplent.platformutility.search.builder.SortData;

/**
 * @author Shubham Patodi
 * @since 05/07/16
 */
public class AttributePaths {
	private Set<String> attributePaths = new HashSet<>();
	private Map<String, AttributePathDetails> attributePathToAggDetailsMap;
//	private Map<String, String> attributePathsMap;
	private List<SortData> stickyHeaderOrderByList;
	private Set<String> searchableAttributePaths = new HashSet<>();
	public AttributePaths(){
//		this.attributePathsMap = new HashMap<>();
		this.attributePathToAggDetailsMap = new HashMap<>();
		this.stickyHeaderOrderByList = new ArrayList<>();
	}
//	public Map<String, String> getAttributePathsMap() {
//		return attributePathsMap;
//	}
//	public void setAttributePathsMap(Map<String, String> attributePathsMap) {
//		this.attributePathsMap = attributePathsMap;
//	}
	public Map<String, AttributePathDetails> getAttributePathToAggDetailsMap() {
		return attributePathToAggDetailsMap;
	}
	public void setAttributePathToAggDetailsMap(
			Map<String, AttributePathDetails> attributePathToAggDetailsMap) {
		this.attributePathToAggDetailsMap = attributePathToAggDetailsMap;
	}
	public List<SortData> getStickyHeaderOrderByList() {
		return stickyHeaderOrderByList;
	}
	public void setStickyHeaderOrderByList(List<SortData> stickyHeaderOrderByList) {
		this.stickyHeaderOrderByList = stickyHeaderOrderByList;
	}

	public Set<String> getAttributePaths() {
		return attributePaths;
	}

	public void setAttributePaths(Set<String> attributePaths) {
		this.attributePaths = attributePaths;
	}

	public Set<String> getSearchableAttributePaths() {
		return searchableAttributePaths;
	}

	public void setSearchableAttributePaths(Set<String> searchableAttributePaths) {
		this.searchableAttributePaths = searchableAttributePaths;
	}
}
