package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import com.tapplent.platformutility.batch.model.RCRD_STATE_MtDOData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;

import javax.sql.DataSource;
import java.util.List;

public class FutureStateWriter extends JdbcBatchItemWriter<RCRD_STATE_MtDOData>{

    public FutureStateWriter(){
        super();
        DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
        setDataSource(ds);
        setSql(getSQL());
        setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<RCRD_STATE_MtDOData>());
      //  afterPropertiesSet();
    }


    public String getSQL(){
        String sql = "UPDATE\n" +
                "   T_PRN_EU_PERSON_PHONE ET\n" +
                "INNER JOIN\n" +
                "    (\n" +
                "      SELECT MAX(EFFECTIVE_DATETIME) MAXDT, VERSION_ID, PERSON_PHONE_PK_ID\n" +
                "      FROM T_PRN_EU_PERSON_PHONE\n" +
                "      WHERE EFFECTIVE_DATETIME < SYSDATE()\n" +
                "      GROUP BY PERSON_PHONE_PK_ID\n" +
                "      ) ET1\n" +
                "ON ET.EFFECTIVE_DATETIME > MAXDT AND ET1.PERSON_PHONE_PK_ID = ET.PERSON_PHONE_PK_ID\n" +
                "    SET ET.RECORD_STATE_CODE_FK_ID = 'FUTURE'\n" +
                "WHERE ET.EFFECTIVE_DATETIME < SYSDATE() ;";
        return sql;

    }

    public void write(List<? extends RCRD_STATE_MtDOData> items) throws Exception
    {

    }
}
