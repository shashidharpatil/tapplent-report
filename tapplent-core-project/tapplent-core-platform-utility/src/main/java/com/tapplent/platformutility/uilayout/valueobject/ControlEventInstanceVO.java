package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 01/10/17.
 */
public class ControlEventInstanceVO {
    private String controlEventPkId;
    private String controlId;
    private String event;
    private String rule;
    private String targetControlInstance;
    private String targetFormatQuery;
    private String targetFormatText;

    public String getControlEventPkId() {
        return controlEventPkId;
    }

    public void setControlEventPkId(String controlEventPkId) {
        this.controlEventPkId = controlEventPkId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getTargetControlInstance() {
        return targetControlInstance;
    }

    public void setTargetControlInstance(String targetControlInstance) {
        this.targetControlInstance = targetControlInstance;
    }

    public String getTargetFormatQuery() {
        return targetFormatQuery;
    }

    public void setTargetFormatQuery(String targetFormatQuery) {
        this.targetFormatQuery = targetFormatQuery;
    }

    public String getTargetFormatText() {
        return targetFormatText;
    }

    public void setTargetFormatText(String targetFormatText) {
        this.targetFormatText = targetFormatText;
    }

    public String getControlId() {
        return controlId;
    }

    public void setControlId(String controlId) {
        this.controlId = controlId;
    }
}
