package com.tapplent.platformutility.accessmanager.dao;

import com.tapplent.platformutility.accessmanager.structure.Permission;
import com.tapplent.platformutility.accessmanager.structure.RowCondition;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.etl.valueObject.ETLMetaHelperVO;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;

import java.sql.SQLException;
import java.util.Map;

/**
 * Created by tapplent on 02/02/17.
 */
public interface AccessManagerService {
    Permission getPermissionsForData(Map<String, Object> data, String mtPE, EntityMetadataVOX entityMetadataVOX);

    void updatePermissionsForData(GlobalVariables globalVariables) throws SQLException;

    Map<String, RowCondition> getRowConditions(String mtPE);

    void populateRowIntermediateForBasePETable(Map<String, RowCondition> rowConditionMap, String subjectUserColumnName, String primaryKeyColumnName, ETLMetaHelperVO etlMetaHelperVO) throws SQLException;

    void populateRowIntermediateForChildPETable(Map<String, RowCondition> rowConditionMap, String subjectUserColumnName, String primaryKeyColumnName, ETLMetaHelperVO etlMetaHelperVO) throws SQLException;

    void updatePrimaryKeyForIntermediateTable(String primaryKeyColumnName, ETLMetaHelperVO helperVO) throws SQLException;
    
    void populateRowIntermediateForChildPETableByRecords(Map<String, RowCondition> rowConditionMap, Map<String, Object> records, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException;

    void populateAccessControlForBasePETableByRecords(Map<String, RowCondition> rowConditionMap, Map<String, Object> records, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException;

    Permission getEffectiveDatedPermissionForPerson(String mtPE);
}
