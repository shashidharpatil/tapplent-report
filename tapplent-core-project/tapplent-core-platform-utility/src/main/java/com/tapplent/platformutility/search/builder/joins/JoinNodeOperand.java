package com.tapplent.platformutility.search.builder.joins;

import com.tapplent.platformutility.search.builder.expression.ExpressionBuilder;

public class JoinNodeOperand extends JoinOperand{
	JoinType joinType;
	ExpressionBuilder onExpression;
	JoinOperand left;
	JoinOperand right;
	public JoinNodeOperand(JoinType joinType,ExpressionBuilder onExpression){
		this.joinType = joinType;
		this.onExpression = onExpression;
	}
	public JoinOperand getLeft() {
		return left;
	}
	public void setLeft(JoinOperand left) {
		this.left = left;
	}
	public JoinOperand getRight() {
		return right;
	}
	public void setRight(JoinOperand right) {
		this.right = right;
	}
	public ExpressionBuilder getOnExpression() {
		return onExpression;
	}
	public JoinType getJoinType() {
		return joinType;
	}
}
