package com.tapplent.platformutility.common.util;

public class StringUtil {
	/**
	 * Determines if the value stored in a string "is defined". If the string is
	 * null, its length is zero, or it has the value "null" or "false", it is
	 * considered undefined.
	 */
	public static boolean isDefined(final String value) {
		return ((value != null) && (value.length() > 0) && (!value.equalsIgnoreCase("null")) && (!value.equalsIgnoreCase("false")));
	}
}
