package com.tapplent.platformutility.uilayout.valueobject;

public class LocationBreakUpDataVO {
    private String personLocationBrkUpId;
    private String locationName;
    private String personId;
    private String locationId;
    private String locationType;
    private String locationTypeName;

    public String getPersonLocationBrkUpId() {
        return personLocationBrkUpId;
    }

    public void setPersonLocationBrkUpId(String personLocationBrkUpId) {
        this.personLocationBrkUpId = personLocationBrkUpId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getLocationTypeName() {
        return locationTypeName;
    }

    public void setLocationTypeName(String locationTypeName) {
        this.locationTypeName = locationTypeName;
    }
}
