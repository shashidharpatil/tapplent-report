package com.tapplent.platformutility.elasticsearch.valueObject;

/**
 * Created by tapplent on 08/05/17.
 */
public class ElasticsearchPersonTestVO {
    private String PrimaryKeyID;
    private String FirstName;
    private String MiddleName;
    private String LastName;
    private String Suffix;
    private String PreferredName;
    private String BirthFullName;
    private String FormalFullName;

    public String getPrimaryKeyID() {
        return PrimaryKeyID;
    }

    public void setPrimaryKeyID(String primaryKeyID) {
        PrimaryKeyID = primaryKeyID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getSuffix() {
        return Suffix;
    }

    public void setSuffix(String suffix) {
        Suffix = suffix;
    }

    public String getPreferredName() {
        return PreferredName;
    }

    public void setPreferredName(String preferredName) {
        PreferredName = preferredName;
    }

    public String getBirthFullName() {
        return BirthFullName;
    }

    public void setBirthFullName(String birthFullName) {
        BirthFullName = birthFullName;
    }

    public String getFormalFullName() {
        return FormalFullName;
    }

    public void setFormalFullName(String formalFullName) {
        FormalFullName = formalFullName;
    }
}
