package com.tapplent.platformutility.conversation.valueobject;

import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.layout.valueObject.SocialStatusVO;

import java.util.List;

/**
 * Created by Tapplent on 6/12/17.
 */
public class PersonDetailsVO {
    private String personId;
    private String feedbackGroupMemberId;
    private String personPhoto;
    private String personFullName;
    private String personDesignation;
    private String location;
    private String nameInitials;
    private boolean memberAdmin;
    private boolean memberNtfnMuted;
    private String lastModifiedDatetime;
    private String featuredPkId;
    private SocialStatusVO socialStatus;
    private List<Phone> phoneList;
    private List<Email> emailList;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getFeedbackGroupMemberId() {
        return feedbackGroupMemberId;
    }

    public void setFeedbackGroupMemberId(String feedbackGroupMemberId) {
        this.feedbackGroupMemberId = feedbackGroupMemberId;
    }

    public String getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(String personPhoto) {
        this.personPhoto = personPhoto;
    }

    public String getPersonFullName() {
        return personFullName;
    }

    public void setPersonFullName(String personFullName) {
        this.personFullName = personFullName;
    }

    public String getPersonDesignation() {
        return personDesignation;
    }

    public void setPersonDesignation(String personDesignation) {
        this.personDesignation = personDesignation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isMemberAdmin() {
        return memberAdmin;
    }

    public void setMemberAdmin(boolean memberAdmin) {
        this.memberAdmin = memberAdmin;
    }

    public boolean isMemberNtfnMuted() {
        return memberNtfnMuted;
    }

    public void setMemberNtfnMuted(boolean memberNtfnMuted) {
        this.memberNtfnMuted = memberNtfnMuted;
    }

    public String getNameInitials() {
        return nameInitials;
    }

    public void setNameInitials(String nameInitials) {
        this.nameInitials = nameInitials;
    }

    public SocialStatusVO getSocialStatus() {
        return socialStatus;
    }

    public void setSocialStatus(SocialStatusVO socialStatus) {
        this.socialStatus = socialStatus;
    }

    public List<Phone> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<Phone> phoneList) {
        this.phoneList = phoneList;
    }

    public List<Email> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<Email> emailList) {
        this.emailList = emailList;
    }

    public String getLastModifiedDatetime() {
        return lastModifiedDatetime;
    }

    public void setLastModifiedDatetime(String lastModifiedDatetime) {
        this.lastModifiedDatetime = lastModifiedDatetime;
    }

    public String getFeaturedPkId() {
        return featuredPkId;
    }

    public void setFeaturedPkId(String featuredPkId) {
        this.featuredPkId = featuredPkId;
    }
}
