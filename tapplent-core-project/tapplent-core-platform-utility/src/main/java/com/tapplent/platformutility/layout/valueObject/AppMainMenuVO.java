package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by tapplent on 15/11/16.
 */
public class AppMainMenuVO {
    private String appMainMenuPkId;
    private String loggedInPersonFkId;
    private String appMainMenuRuleFkId;
    private String menuImage;
    private String menuIconCodeFkId;
    private String menuNameG11nBigTxt;
    private String parentMenuHcyFkId;
    private boolean isShowInExpndForm;
    private int menuSequenceNumberPosInt;
    private String iconColourTxt;
    private String menuExpressionBigTxt;
    private String dynamicActionArgBigTxt;
    private boolean isAllowQuickAdd;
    @JsonIgnore
    private String notesG11nBigTxt;
    @JsonIgnore
    private String intrnlHierarchyId;
    @JsonIgnore
    private int  intrnlLeftPosInt;
    @JsonIgnore
    private int  intrnlRightPosInt;
    @JsonIgnore
    private boolean isDeleted;

    public String getAppMainMenuPkId() {
        return appMainMenuPkId;
    }

    public void setAppMainMenuPkId(String appMainMenuPkId) {
        this.appMainMenuPkId = appMainMenuPkId;
    }

    public String getLoggedInPersonFkId() {
        return loggedInPersonFkId;
    }

    public void setLoggedInPersonFkId(String loggedInPersonFkId) {
        this.loggedInPersonFkId = loggedInPersonFkId;
    }

    public String getAppMainMenuRuleFkId() {
        return appMainMenuRuleFkId;
    }

    public void setAppMainMenuRuleFkId(String appMainMenuRuleFkId) {
        this.appMainMenuRuleFkId = appMainMenuRuleFkId;
    }

    public String getMenuIconCodeFkId() {
		return menuIconCodeFkId;
	}

	public void setMenuIconCodeFkId(String menuIconCodeFkId) {
		this.menuIconCodeFkId = menuIconCodeFkId;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getMenuNameG11nBigTxt() {
        return menuNameG11nBigTxt;
    }

    public void setMenuNameG11nBigTxt(String menuNameG11nBigTxt) {
        this.menuNameG11nBigTxt = menuNameG11nBigTxt;
    }

    public String getParentMenuHcyFkId() {
        return parentMenuHcyFkId;
    }

    public void setParentMenuHcyFkId(String parentMenuHcyFkId) {
        this.parentMenuHcyFkId = parentMenuHcyFkId;
    }

    public int getMenuSequenceNumberPosInt() {
        return menuSequenceNumberPosInt;
    }

    public void setMenuSequenceNumberPosInt(int menuSequenceNumberPosInt) {
        this.menuSequenceNumberPosInt = menuSequenceNumberPosInt;
    }

    public String getNotesG11nBigTxt() {
        return notesG11nBigTxt;
    }

    public void setNotesG11nBigTxt(String notesG11nBigTxt) {
        this.notesG11nBigTxt = notesG11nBigTxt;
    }

    public String getIntrnlHierarchyId() {
        return intrnlHierarchyId;
    }

    public void setIntrnlHierarchyId(String intrnlHierarchyId) {
        this.intrnlHierarchyId = intrnlHierarchyId;
    }

    public int getIntrnlLeftPosInt() {
        return intrnlLeftPosInt;
    }

    public void setIntrnlLeftPosInt(int intrnlLeftPosInt) {
        this.intrnlLeftPosInt = intrnlLeftPosInt;
    }

    public int getIntrnlRightPosInt() {
        return intrnlRightPosInt;
    }

    public void setIntrnlRightPosInt(int intrnlRightPosInt) {
        this.intrnlRightPosInt = intrnlRightPosInt;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public boolean isShowInExpndForm() {
        return isShowInExpndForm;
    }

    public void setShowInExpndForm(boolean showInExpndForm) {
        isShowInExpndForm = showInExpndForm;
    }

    public String getIconColourTxt() {
        return iconColourTxt;
    }

    public void setIconColourTxt(String iconColourTxt) {
        this.iconColourTxt = iconColourTxt;
    }

    public String getMenuExpressionBigTxt() {
        return menuExpressionBigTxt;
    }

    public void setMenuExpressionBigTxt(String menuExpressionBigTxt) {
        this.menuExpressionBigTxt = menuExpressionBigTxt;
    }

    public String getDynamicActionArgBigTxt() {
        return dynamicActionArgBigTxt;
    }

    public void setDynamicActionArgBigTxt(String dynamicActionArgBigTxt) {
        this.dynamicActionArgBigTxt = dynamicActionArgBigTxt;
    }

    public boolean isAllowQuickAdd() {
        return isAllowQuickAdd;
    }

    public void setAllowQuickAdd(boolean allowQuickAdd) {
        isAllowQuickAdd = allowQuickAdd;
    }

    public String getMenuImage() {
        return menuImage;
    }

    public void setMenuImage(String menuImage) {
        this.menuImage = menuImage;
    }
}
