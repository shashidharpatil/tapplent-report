package com.tapplent.platformutility.analytics.structure;

/**
 * Created by tapplent on 24/08/17.
 */
public class MetricAxisVO {
    private String pkID;
    private String name;
    private String metricAxisSourceType;
    private String mtPE;
    private String mtPEAlias;
    private String parentMTPEAlias;
    private String fkRelnWithParent;
    private String controlAttributePath;
    private boolean isClientDisplay;
    private boolean isClientMatch;
    private String rangeName;
    private String rangeIcon;
    private double rangeMinValue;
    private String rangeMinName;
    private String rangeMinIcon;
    private double rangeMaxValue;
    private String rangeMaxName;
    private String rangeMaxIcon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMetricAxisSourceType() {
        return metricAxisSourceType;
    }

    public void setMetricAxisSourceType(String metricAxisSourceType) {
        this.metricAxisSourceType = metricAxisSourceType;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getParentMTPEAlias() {
        return parentMTPEAlias;
    }

    public void setParentMTPEAlias(String parentMTPEAlias) {
        this.parentMTPEAlias = parentMTPEAlias;
    }

    public String getFkRelnWithParent() {
        return fkRelnWithParent;
    }

    public void setFkRelnWithParent(String fkRelnWithParent) {
        this.fkRelnWithParent = fkRelnWithParent;
    }

    public String getControlAttributePath() {
        return controlAttributePath;
    }

    public void setControlAttributePath(String controlAttributePath) {
        this.controlAttributePath = controlAttributePath;
    }

    public boolean isClientDisplay() {
        return isClientDisplay;
    }

    public void setClientDisplay(boolean clientDisplay) {
        isClientDisplay = clientDisplay;
    }

    public boolean isClientMatch() {
        return isClientMatch;
    }

    public void setClientMatch(boolean clientMatch) {
        isClientMatch = clientMatch;
    }

    public String getRangeName() {
        return rangeName;
    }

    public void setRangeName(String rangeName) {
        this.rangeName = rangeName;
    }

    public String getRangeIcon() {
        return rangeIcon;
    }

    public void setRangeIcon(String rangeIcon) {
        this.rangeIcon = rangeIcon;
    }

    public double getRangeMinValue() {
        return rangeMinValue;
    }

    public void setRangeMinValue(double rangeMinValue) {
        this.rangeMinValue = rangeMinValue;
    }

    public double getRangeMaxValue() {
        return rangeMaxValue;
    }

    public void setRangeMaxValue(double rangeMaxValue) {
        this.rangeMaxValue = rangeMaxValue;
    }

    public String getRangeMinName() {
        return rangeMinName;
    }

    public void setRangeMinName(String rangeMinName) {
        this.rangeMinName = rangeMinName;
    }

    public String getRangeMinIcon() {
        return rangeMinIcon;
    }

    public void setRangeMinIcon(String rangeMinIcon) {
        this.rangeMinIcon = rangeMinIcon;
    }

    public String getRangeMaxName() {
        return rangeMaxName;
    }

    public void setRangeMaxName(String rangeMaxName) {
        this.rangeMaxName = rangeMaxName;
    }

    public String getRangeMaxIcon() {
        return rangeMaxIcon;
    }

    public void setRangeMaxIcon(String rangeMaxIcon) {
        this.rangeMaxIcon = rangeMaxIcon;
    }

    public String getPkID() {
        return pkID;
    }

    public void setPkID(String pkID) {
        this.pkID = pkID;
    }
}
