package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.helper.DG_DocGenHelper;
import com.tapplent.platformutility.batch.model.DG_DocGenData;
import org.springframework.batch.core.ItemProcessListener;



public class DocGenItemProcessListener implements ItemProcessListener<DG_DocGenData, DG_DocGenData> {
	
	  
	    public void beforeProcess(DG_DocGenData dgData){
	        if(dgData == null){
	            //...
	        }
	        //...
	    }

	  
	    public void afterProcess(DG_DocGenData dgData, DG_DocGenData dgData1){
	        //..
	    }

	   
	    public void onProcessError(DG_DocGenData dgData, Exception e){
			// insert into skipped elements table
			DG_DocGenHelper dg_docGenHelper = new DG_DocGenHelper();
			dg_docGenHelper.insertSkippedElements(dgData);

	    }
	
}
