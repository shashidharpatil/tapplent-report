package com.tapplent.platformutility.search.builder;

public class FilterOperatorValue {
	private boolean isValueAColumn;
	private String operator;
	private Object value;
	private boolean eliminateResultFromRoot;
	private boolean havingClause;
	public FilterOperatorValue(){
		this.isValueAColumn = false;
	}
	public FilterOperatorValue(String operator, String value) {
		this.operator = operator;
		this.value = value;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public boolean isValueAColumn() {
		return isValueAColumn;
	}
	public void setValueAColumn(boolean isValueAColumn) {
		this.isValueAColumn = isValueAColumn;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public boolean isEliminateResultFromRoot() {
		return eliminateResultFromRoot;
	}

	public void setEliminateResultFromRoot(boolean eliminateResultFromRoot) {
		this.eliminateResultFromRoot = eliminateResultFromRoot;
	}

	public boolean isHavingClause() {
		return havingClause;
	}

	public void setHavingClause(boolean havingClause) {
		this.havingClause = havingClause;
	}
}
