package com.tapplent.platformutility.batch.model;

public class NTFN_NotfnDoaMap {

	private String ntfnChnlDoaMapPkId;
	private String channelElementCodeFkId;
	private String mapDoaExpn;
	private Integer sequenceNumberPosInt;
	public String getNtfnChnlDoaMapPkId() {
		return ntfnChnlDoaMapPkId;
	}
	public void setNtfnChnlDoaMapPkId(String ntfnChnlDoaMapPkId) {
		this.ntfnChnlDoaMapPkId = ntfnChnlDoaMapPkId;
	}
	public String getChannelElementCodeFkId() {
		return channelElementCodeFkId;
	}
	public void setChannelElementCodeFkId(String channelElementCodeFkId) {
		this.channelElementCodeFkId = channelElementCodeFkId;
	}
	public String getMapDoaExpn() {
		return mapDoaExpn;
	}
	public void setMapDoaExpn(String mapDoaExpn) {
		this.mapDoaExpn = mapDoaExpn;
	}
	public Integer getSequenceNumberPosInt() {
		return sequenceNumberPosInt;
	}
	public void setSequenceNumberPosInt(Integer sequenceNumberPosInt) {
		this.sequenceNumberPosInt = sequenceNumberPosInt;
	}
	
	
	
}
