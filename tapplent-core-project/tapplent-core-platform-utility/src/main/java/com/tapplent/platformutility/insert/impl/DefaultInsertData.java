package com.tapplent.platformutility.insert.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.tapplent.platformutility.search.builder.FilterOperatorValue;

public class DefaultInsertData implements InsertData {

	private String processElementId;
	private Map<String, Object> data = new LinkedHashMap<String, Object>();//this data is different from RequestData's data.
	private Map<String, Object> propogateData = new LinkedHashMap<>();
//	private boolean propogate;
//	private String actionCode;
//	private Map<String,List<FilterOperatorValue>> filters;
//	private Map<String, Object> underlyingRecord;
	
	@Override
	public Map<String, Object> getData() {
		return data;
	}

	@Override
	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public Map<String, Object> getPropogateData() {
		return propogateData;
	}

	public void setPropogateData(Map<String, Object> propogateData) {
		this.propogateData = propogateData;
	}

	public String getProcessElementId() {
		return processElementId;
	}

	public void setProcessElementId(String processElementId) {
		this.processElementId = processElementId;
	}

//	public Map<String,List<FilterOperatorValue>> getFilters() {
//		return filters;
//	}
//
//	public void setFilters(Map<String,List<FilterOperatorValue>> filters) {
//		this.filters = filters;
//	}
	
//	public String getActionCode() {
//		return actionCode;
//	}
//
//	public void setActionCode(String actionCode) {
//		this.actionCode = actionCode;
//	}

}
