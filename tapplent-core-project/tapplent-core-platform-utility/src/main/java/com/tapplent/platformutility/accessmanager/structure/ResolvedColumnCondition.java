package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by tapplent on 02/11/16.
 */
public class ResolvedColumnCondition {
    private String pk;
    private String fk;
    private String doa;
    private boolean isCRp;
    private boolean isCUp;
    private boolean isHRp;
    private boolean isHUp;
    private boolean isFRp;
    private boolean isFUp;

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getFk() {
        return fk;
    }

    public void setFk(String fk) {
        this.fk = fk;
    }

    public String getDoa() {
        return doa;
    }

    public void setDoa(String doa) {
        this.doa = doa;
    }

    public boolean isCRp() {
        return isCRp;
    }

    public void setCRp(boolean CRp) {
        isCRp = CRp;
    }

    public boolean isCUp() {
        return isCUp;
    }

    public void setCUp(boolean CUp) {
        isCUp = CUp;
    }

    public boolean isHRp() {
        return isHRp;
    }

    public void setHRp(boolean HRp) {
        isHRp = HRp;
    }

    public boolean isHUp() {
        return isHUp;
    }

    public void setHUp(boolean HUp) {
        isHUp = HUp;
    }

    public boolean isFRp() {
        return isFRp;
    }

    public void setFRp(boolean FRp) {
        isFRp = FRp;
    }

    public boolean isFUp() {
        return isFUp;
    }

    public void setFUp(boolean FUp) {
        isFUp = FUp;
    }
}
