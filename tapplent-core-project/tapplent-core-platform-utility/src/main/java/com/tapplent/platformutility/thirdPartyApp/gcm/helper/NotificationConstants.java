package com.tapplent.platformutility.thirdPartyApp.gcm.helper;

public enum NotificationConstants {
	
	title,
	notificationText,
	subText,
	smallIcon,
	actions,
	notificationKey,
	notificationId

}
