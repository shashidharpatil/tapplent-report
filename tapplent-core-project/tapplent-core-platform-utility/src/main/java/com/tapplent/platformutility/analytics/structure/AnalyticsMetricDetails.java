package com.tapplent.platformutility.analytics.structure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 05/12/16.
 */
public class AnalyticsMetricDetails {
    private String analyticsMetricDetailPkId;
    private String analyticsMetricFkId;
    private String analysisTypeCodeFkId;
    private int seqNumPosInt;
    private String helpExplanationG11nBigTxt;
    private String chartTitleG11nBigTxt;
    private String chartTypeCodeFkId;
    private String xAxizTitleG11nBigTxt;
    private String yAxizTitle1G11nBigTxt;
    private String yAxizTitle2G11nBigTxt;
    private boolean isLegendVisible;
    private boolean isDrillDownEnabled;
    private boolean isTableAvailable;
    private String legendPositionCodeFkId;
    private String parentAnalyticsMetricDetailFkId;
    private String parentDrilldownAnalyticsMetricDetailFkId;
    private List<AnalyticsMetricDetailsQuery> analyticsMetricDetailsQueryList = new ArrayList<>();
    private List<AnalyticsMetricDetailsFilter> analyticsMetricDetailsFilterList = new ArrayList<>();
    public List<AnalyticsMetricDetailsQuery> getAnalyticsMetricDetailsQueryList() {
        return analyticsMetricDetailsQueryList;
    }

    public void setAnalyticsMetricDetailsQueryList(List<AnalyticsMetricDetailsQuery> analyticsMetricDetailsQueryList) {
        this.analyticsMetricDetailsQueryList = analyticsMetricDetailsQueryList;
    }

    public List<AnalyticsMetricDetailsFilter> getAnalyticsMetricDetailsFilterList() {
        return analyticsMetricDetailsFilterList;
    }

    public void setAnalyticsMetricDetailsFilterList(List<AnalyticsMetricDetailsFilter> analyticsMetricDetailsFilterList) {
        this.analyticsMetricDetailsFilterList = analyticsMetricDetailsFilterList;
    }

    public String getAnalyticsMetricDetailPkId() {
        return analyticsMetricDetailPkId;
    }

    public void setAnalyticsMetricDetailPkId(String analyticsMetricDetailPkId) {
        this.analyticsMetricDetailPkId = analyticsMetricDetailPkId;
    }

    public String getAnalyticsMetricFkId() {
        return analyticsMetricFkId;
    }

    public void setAnalyticsMetricFkId(String analyticsMetricFkId) {
        this.analyticsMetricFkId = analyticsMetricFkId;
    }

    public String getAnalysisTypeCodeFkId() {
        return analysisTypeCodeFkId;
    }

    public void setAnalysisTypeCodeFkId(String analysisTypeCodeFkId) {
        this.analysisTypeCodeFkId = analysisTypeCodeFkId;
    }

    public int getSeqNumPosInt() {
        return seqNumPosInt;
    }

    public void setSeqNumPosInt(int seqNumPosInt) {
        this.seqNumPosInt = seqNumPosInt;
    }

    public String getHelpExplanationG11nBigTxt() {
        return helpExplanationG11nBigTxt;
    }

    public void setHelpExplanationG11nBigTxt(String helpExplanationG11nBigTxt) {
        this.helpExplanationG11nBigTxt = helpExplanationG11nBigTxt;
    }

    public String getChartTitleG11nBigTxt() {
        return chartTitleG11nBigTxt;
    }

    public void setChartTitleG11nBigTxt(String chartTitleG11nBigTxt) {
        this.chartTitleG11nBigTxt = chartTitleG11nBigTxt;
    }

    public String getChartTypeCodeFkId() {
        return chartTypeCodeFkId;
    }

    public void setChartTypeCodeFkId(String chartTypeCodeFkId) {
        this.chartTypeCodeFkId = chartTypeCodeFkId;
    }

    public String getxAxizTitleG11nBigTxt() {
        return xAxizTitleG11nBigTxt;
    }

    public void setxAxizTitleG11nBigTxt(String xAxizTitleG11nBigTxt) {
        this.xAxizTitleG11nBigTxt = xAxizTitleG11nBigTxt;
    }

    public String getyAxizTitle1G11nBigTxt() {
        return yAxizTitle1G11nBigTxt;
    }

    public void setyAxizTitle1G11nBigTxt(String yAxizTitle1G11nBigTxt) {
        this.yAxizTitle1G11nBigTxt = yAxizTitle1G11nBigTxt;
    }

    public String getyAxizTitle2G11nBigTxt() {
        return yAxizTitle2G11nBigTxt;
    }

    public void setyAxizTitle2G11nBigTxt(String yAxizTitle2G11nBigTxt) {
        this.yAxizTitle2G11nBigTxt = yAxizTitle2G11nBigTxt;
    }

    public boolean isLegendVisible() {
        return isLegendVisible;
    }

    public void setLegendVisible(boolean legendVisible) {
        isLegendVisible = legendVisible;
    }

    public String getLegendPositionCodeFkId() {
        return legendPositionCodeFkId;
    }

    public void setLegendPositionCodeFkId(String legendPositionCodeFkId) {
        this.legendPositionCodeFkId = legendPositionCodeFkId;
    }

    public String getParentAnalyticsMetricDetailFkId() {
        return parentAnalyticsMetricDetailFkId;
    }

    public void setParentAnalyticsMetricDetailFkId(String parentAnalyticsMetricDetailFkId) {
        this.parentAnalyticsMetricDetailFkId = parentAnalyticsMetricDetailFkId;
    }

    public String getParentDrilldownAnalyticsMetricDetailFkId() {
        return parentDrilldownAnalyticsMetricDetailFkId;
    }

    public void setParentDrilldownAnalyticsMetricDetailFkId(String parentDrilldownAnalyticsMetricDetailFkId) {
        this.parentDrilldownAnalyticsMetricDetailFkId = parentDrilldownAnalyticsMetricDetailFkId;
    }

    public boolean isDrillDownEnabled() {
        return isDrillDownEnabled;
    }

    public void setDrillDownEnabled(boolean drillDownEnabled) {
        isDrillDownEnabled = drillDownEnabled;
    }

    public boolean isTableAvailable() {
        return isTableAvailable;
    }

    public void setTableAvailable(boolean tableAvailable) {
        isTableAvailable = tableAvailable;
    }
}
