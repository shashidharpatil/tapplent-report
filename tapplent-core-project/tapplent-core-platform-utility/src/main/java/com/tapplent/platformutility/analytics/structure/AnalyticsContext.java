package com.tapplent.platformutility.analytics.structure;

/**
 * Created by tapplent on 05/12/16.
 */
public class AnalyticsContext {
    private String analyticsContextPkId;
    private String processTypeCodeFkId;
    private String mtPeCodeFkId;
    private String doPrimaryKeyFkId;

    public String getAnalyticsContextPkId() {
        return analyticsContextPkId;
    }

    public void setAnalyticsContextPkId(String analyticsContextPkId) {
        this.analyticsContextPkId = analyticsContextPkId;
    }

    public String getProcessTypeCodeFkId() {
        return processTypeCodeFkId;
    }

    public void setProcessTypeCodeFkId(String processTypeCodeFkId) {
        this.processTypeCodeFkId = processTypeCodeFkId;
    }

    public String getMtPeCodeFkId() {
        return mtPeCodeFkId;
    }

    public void setMtPeCodeFkId(String mtPeCodeFkId) {
        this.mtPeCodeFkId = mtPeCodeFkId;
    }

    public String getDoPrimaryKeyFkId() {
        return doPrimaryKeyFkId;
    }

    public void setDoPrimaryKeyFkId(String doPrimaryKeyFkId) {
        this.doPrimaryKeyFkId = doPrimaryKeyFkId;
    }
}
