package com.tapplent.platformutility.batch.helper;

import java.sql.SQLException;
import java.util.Map;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import com.tapplent.platformutility.thirdPartyApp.gcm.controller.GCMHandler;
import com.tapplent.platformutility.thirdPartyApp.gcm.helper.Notification;
import org.json.JSONException;



public class NTFN_SyncNtfnCaller {

	public Object sendNtfn(String personNotificationId) throws SQLException, JSONException {
		NTFN_NotificationData item = new NTFN_NotificationData();
		// (TDWI) query from T_Person_Notification and create
		// NTFN_NotificationData Object;
		NTFN_NotificationHelper.setUpNotfn(item);
		for (int i = 0; i < item.getNotChannels().size(); i++) {
			if (item.getNotChannels().get(i).getChannelTypeCodeFkId().equals(Constants.pushNotification)) {

				Map<String, Object> mapModel = (Map<String, Object>) NTFN_NotificationHelper
						.buildPushNotificationModel(item, Constants.google);
				GCMHandler gcmHandler = new GCMHandler();
				if (((Notification) mapModel.get("not")).getNotificationId() != null) {
					gcmHandler.sendGCMusingRegId((Notification) mapModel.get("not"),
							((Notification) mapModel.get("not")).getNotificationId(), (String) mapModel.get("apiKey"),
							item);
				} else if (((Notification) mapModel.get("not")).getNotificationKey() != null) {
					gcmHandler.sendGCMusingNotKey((Notification) mapModel.get("not"),
							((Notification) mapModel.get("not")).getNotificationKey(), (String) mapModel.get("apiKey"),
							item);
				}

			}

			else if (item.getNotChannels().get(i).getChannelTypeCodeFkId().equals(Constants.gmail)) {
				if (item.getNtfnBtCodeFkId() != null) {
					String url = NTFN_NotificationHelper.fetchURL(item.getNtfnBtCodeFkId(), item.getNtfnMtPeCodeFkId(),
							item.getNtfnVtCodeFkId(), item.getNtfnCanvasTxnFkId(), item.getDependencyKeyExpn());
				}
				String doc = NTFN_NotificationHelper.fetchAttachmentifexists(item);

				// (TDWI) call gmail Notification and pass body url also
				// attachment if exists

			}

			else if (item.getNotChannels().get(i).getChannelTypeCodeFkId().equals(Constants.skype)) {

				// (TDWI) call skype Notification

			}

			else if (item.getNotChannels().get(i).getChannelTypeCodeFkId().equals(Constants.telegram)) {

				// (TDWI) call telegram Notification

			}

			else if (item.getNotChannels().get(i).getChannelTypeCodeFkId().equals(Constants.faceBookMessenger)) {

				// (TDWI) call faceBookMessenger Notification
			}

		}
		if (item.isSuccess() == true) {
			return Constants.success;
		} else {
			return item.getError();
		}

	}

}
