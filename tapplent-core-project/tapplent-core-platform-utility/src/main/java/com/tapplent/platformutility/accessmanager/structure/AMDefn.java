package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by tapplent on 04/11/16.
 */
public class AMDefn {
    private String amDefnPkId;
    private String amNameG11nBigText;
    private String accessIconCodeFkId;

    public String getAmDefnPkId() {
        return amDefnPkId;
    }

    public void setAmDefnPkId(String amDefnPkId) {
        this.amDefnPkId = amDefnPkId;
    }

    public String getAmNameG11nBigText() {
        return amNameG11nBigText;
    }

    public void setAmNameG11nBigText(String amNameG11nBigText) {
        this.amNameG11nBigText = amNameG11nBigText;
    }

    public String getAccessIconCodeFkId() {
        return accessIconCodeFkId;
    }

    public void setAccessIconCodeFkId(String accessIconCodeFkId) {
        this.accessIconCodeFkId = accessIconCodeFkId;
    }
}
