package com.tapplent.platformutility.etl.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class EtlHeaderVO {
	private String versionId;
	private String etlHeaderPkId; 
	private String name;
	private String description;
	private String sourceDoCodeFkId;
	private String baseTemplateFkId; 
	private String createUpdatePersonFkId; 
	private String primaryLocaleCodeFkId; 
	private String defaultSaveStateCodeFkId;
	private String defaultDateTimeFormat;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getEtlHeaderPkId() {
		return etlHeaderPkId;
	}
	public void setEtlHeaderPkId(String etlHeaderPkId) {
		this.etlHeaderPkId = etlHeaderPkId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSourceDoCodeFkId() {
		return sourceDoCodeFkId;
	}
	public void setSourceDoCodeFkId(String sourceDoCodeFkId) {
		this.sourceDoCodeFkId = sourceDoCodeFkId;
	}
	public String getBaseTemplateFkId() {
		return baseTemplateFkId;
	}
	public void setBaseTemplateFkId(String baseTemplateFkId) {
		this.baseTemplateFkId = baseTemplateFkId;
	}
	public String getCreateUpdatePersonFkId() {
		return createUpdatePersonFkId;
	}
	public void setCreateUpdatePersonFkId(String createUpdatePersonFkId) {
		this.createUpdatePersonFkId = createUpdatePersonFkId;
	}
	public String getPrimaryLocaleCodeFkId() {
		return primaryLocaleCodeFkId;
	}
	public void setPrimaryLocaleCodeFkId(String primaryLocaleCodeFkId) {
		this.primaryLocaleCodeFkId = primaryLocaleCodeFkId;
	}
	public String getDefaultSaveStateCodeFkId() {
		return defaultSaveStateCodeFkId;
	}
	public void setDefaultSaveStateCodeFkId(String defaultSaveStateCodeFkId) {
		this.defaultSaveStateCodeFkId = defaultSaveStateCodeFkId;
	}
	public String getDefaultDateTimeFormat() {
		return defaultDateTimeFormat;
	}
	public void setDefaultDateTimeFormat(String defaultDateTimeFormat) {
		this.defaultDateTimeFormat = defaultDateTimeFormat;
	}
}
