package com.tapplent.platformutility.workflow.structure;

public class WorkflowSelection {
    private String workflowSelectionId;
    private String workflowDefinitionId;
    private String businessRuleCode;
    private String baseTemplate;
    private String conditionExpression;
    private String processTypeCode;
    private String mtPE;

    public String getWorkflowSelectionId() {
        return workflowSelectionId;
    }

    public void setWorkflowSelectionId(String workflowSelectionId) {
        this.workflowSelectionId = workflowSelectionId;
    }

    public String getWorkflowDefinitionId() {
        return workflowDefinitionId;
    }

    public void setWorkflowDefinitionId(String workflowDefinitionId) {
        this.workflowDefinitionId = workflowDefinitionId;
    }

    public String getBusinessRuleCode() {
        return businessRuleCode;
    }

    public void setBusinessRuleCode(String businessRuleCode) {
        this.businessRuleCode = businessRuleCode;
    }

    public String getBaseTemplate() {
        return baseTemplate;
    }

    public void setBaseTemplate(String baseTemplate) {
        this.baseTemplate = baseTemplate;
    }

    public String getConditionExpression() {
        return conditionExpression;
    }

    public void setConditionExpression(String conditionExpression) {
        this.conditionExpression = conditionExpression;
    }

    public String getProcessTypeCode() {
        return processTypeCode;
    }

    public void setProcessTypeCode(String processTypeCode) {
        this.processTypeCode = processTypeCode;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }
}
