package com.tapplent.platformutility.expression.jeval.function.string;

import com.tapplent.platformutility.expression.jeval.EvaluationConstants;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.expression.jeval.function.Function;
import com.tapplent.platformutility.expression.jeval.function.FunctionConstants;
import com.tapplent.platformutility.expression.jeval.function.FunctionException;
import com.tapplent.platformutility.expression.jeval.function.FunctionHelper;
import com.tapplent.platformutility.expression.jeval.function.FunctionResult;

import java.util.ArrayList;

/**
 * Created by sripad on 13/01/16.
 */
public class Right implements Function {


    /**
     * Returns the name of the function - "right".
     *
     * @return The name of this function class.
     */
    public String getName() {
        return "right";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     *
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A string argument that will be converted into one string
     *            argument and one integer arguments. The first argument is the
     *            source string, the second argument is the length from right to be fetched
     *            . The string argument(s)
     *            HAS to be enclosed in quotes. White space that is not enclosed
     *            within quotes will be trimmed. Quote characters in the first
     *            and last positions of any string argument (after being
     *            trimmed) will be removed also. The quote characters used must
     *            be the same as the quote characters used by the current
     *            instance of Evaluator. If there are multiple arguments, they
     *            must be separated by a comma (",").
     *
     * @return Returns the specified substring.
     *
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    public FunctionResult execute(final Evaluator evaluator, final String arguments)
            throws FunctionException {
        String result = null;
        String exceptionMessage = "One string argument and one integer "
                + "arguments are required.";

        ArrayList values = FunctionHelper.getOneStringAndOneInteger(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

        if (values.size() != 2) {
            throw new FunctionException(exceptionMessage);
        }

        try {
            String argumentOne = FunctionHelper.trimAndRemoveQuoteChars(
                    (String) values.get(0), evaluator.getQuoteCharacter());
            int length = ((Integer) values.get(1)).intValue();
            int lengthofstring = argumentOne.length();
            result = argumentOne.substring(lengthofstring-length, lengthofstring);
        } catch (FunctionException fe) {
            throw new FunctionException(fe.getMessage(), fe);
        } catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result,
                FunctionConstants.FUNCTION_RESULT_TYPE_STRING);
    }
}
