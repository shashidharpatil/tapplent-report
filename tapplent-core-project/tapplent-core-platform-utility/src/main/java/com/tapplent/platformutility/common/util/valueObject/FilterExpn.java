package com.tapplent.platformutility.common.util.valueObject;

/**
 * Created by tapplent on 02/11/17.
 */
public class FilterExpn {
    private String filterExpn;
    private boolean isDeleteFilterAdded = false;

    public String getFilterExpn() {
        return filterExpn;
    }

    public void setFilterExpn(String filterExpn) {
        this.filterExpn = filterExpn;
    }

    public boolean isDeleteFilterAdded() {
        return isDeleteFilterAdded;
    }

    public void setDeleteFilterAdded(boolean deleteFilterAdded) {
        isDeleteFilterAdded = deleteFilterAdded;
    }
}
