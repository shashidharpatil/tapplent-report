package com.tapplent.platformutility.server.property;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.tenantresolver.server.property.ServerActionStep;
import com.tapplent.tenantresolver.server.property.ServerActionStepProperty;

public class ServerPropertyDAOImpl extends TapplentBaseDAO implements ServerPropertyDAO {
	
	private static final Logger LOG = LogFactory.getLogger(ServerPropertyDAOImpl.class);
	
	private static final String ALL_SERVER_ACTION_STEP = "SELECT VERSION_ID, SERVER_ACTION_STEP_RULES_PK_ID, SERVER_ACTION_CODE_DEP_FK_ID, SERVER_ACTION_STEPID_TXT, COLUMN_JSON(SERVER_ACTION_STEP_NAME_G11N_BLOB) as actionStepName, "
			+ "PARENT_SERVER_ACTION_STEPID_TXT, SERVER_ACTION_STEP_RULE_CODE_FK_ID FROM T_UI_LKP_SERVER_ACTION_STEP_RULES;";
	
	private static final String ALL_SERVER_ACTION_PROPERTIES = "SELECT VERSION_ID, SERVER_ACTION_STEP_PROPERTY_TRANSACTION_PK_ID, SERVER_ACTION_STEP_RULES_FK_ID, SERVER_ACTION_STEP_PROPERTY_CODE_FK_ID, SERVER_PROPERTY_VALUE_TXT "
			+ "FROM T_UI_LKP_SERVER_ACTION_STEP_PROP_TXN;";
	
	public Map<String, ServerActionStep> getAllServerActionStep() {
		
		PreparedStatement ps;
		Map<String, ServerActionStep> actionCodeActionStepMap = new HashMap<String, ServerActionStep>();
		
		try {
			Connection con = null;
			con = getConnection();
			ps = con.prepareStatement(ALL_SERVER_ACTION_STEP);
			LOG.debug(ALL_SERVER_ACTION_STEP);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
//				VERSION_ID` binary(16) NOT NULL,
//				  `SERVER_ACTION_STEP_RULES_PK_ID` binary(16) NOT NULL,
//				  `SERVER_ACTION_CODE_DEP_FK_ID` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
//				  `SERVER_ACTION_STEPID_TXT` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
//				  `SERVER_ACTION_STEP_NAME_G11N_BLOB` blob,
//				  `PARENT_SERVER_ACTION_STEPID_TXT` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
//				  `SERVER_ACTION_STEP_RULE_CODE_FK_ID` var
				ServerActionStep serverActionStep = new ServerActionStep();
				serverActionStep.setVersionId(Hex.encodeHexString(rs.getBytes("VERSION_ID")));
				serverActionStep.setActionStepLookupPkId(Hex.encodeHexString(rs.getBytes("SERVER_ACTION_STEP_RULES_PK_ID")));
				serverActionStep.setActionCodeFkId(rs.getString("SERVER_ACTION_CODE_DEP_FK_ID"));
				serverActionStep.setParentActionStepId(rs.getString("PARENT_SERVER_ACTION_STEPID_TXT"));
				serverActionStep.setActionStepId(rs.getString("SERVER_ACTION_STEPID_TXT"));				
//				serverActionStep.setActionStepNameG11nText(rs.getString("ACTION_STEP_NAME_G11N_TEXT"));
				
				serverActionStep.setServerRuleCodeFkId(rs.getString("SERVER_ACTION_STEP_RULE_CODE_FK_ID"));				
				actionCodeActionStepMap.put(serverActionStep.getActionStepLookupPkId(), serverActionStep);
			}
		}
		catch (SQLException e) {
		e.printStackTrace();
		}
		return actionCodeActionStepMap;
	}
	
	public Map<String, List<ServerActionStepProperty>> getAllServerActionStepProperty() {
		
		PreparedStatement ps;
		Map<String, List<ServerActionStepProperty>> actionStepPropertyMap = new HashMap<String, List<ServerActionStepProperty>>();
				
		try {
			Connection con = null;
			con = getConnection();
			ps = con.prepareStatement(ALL_SERVER_ACTION_PROPERTIES);
			LOG.debug(ALL_SERVER_ACTION_PROPERTIES);

			ResultSet rs = ps.executeQuery();
			while(rs.next()){
//				VERSION_ID` binary(16) NOT NULL,
//				  `SERVER_ACTION_STEP_PROPERTY_TRANSACTION_PK_ID` binary(16) NOT NULL,
//				  `SERVER_ACTION_STEP_RULES_FK_ID` binary(16) DEFAULT NULL,
//				  `SERVER_ACTION_STEP_PROPERTY_CODE_FK_ID` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
//				  `SERVER_PROPERTY_VALUE_TXT` varchar(100)
				ServerActionStepProperty serverActionStepProperty = new ServerActionStepProperty();
				serverActionStepProperty.setVersionId(Hex.encodeHexString(rs.getBytes("VERSION_ID")));
				serverActionStepProperty.setActionStepPropertyDefnPkId(Hex.encodeHexString(rs.getBytes("SERVER_ACTION_STEP_PROPERTY_TRANSACTION_PK_ID")));
				serverActionStepProperty.setActionStepLookupFkId(Hex.encodeHexString(rs.getBytes("SERVER_ACTION_STEP_RULES_FK_ID")));
				serverActionStepProperty.setActionStepPropertyCodeFkId(rs.getString("SERVER_ACTION_STEP_PROPERTY_CODE_FK_ID"));
				serverActionStepProperty.setPropertyValue(rs.getString("SERVER_PROPERTY_VALUE_TXT"));
				if(actionStepPropertyMap.get(serverActionStepProperty.getActionStepLookupFkId()) == null){
					List<ServerActionStepProperty> serverActionStepPropertyList = new ArrayList<>();
					serverActionStepPropertyList.add(serverActionStepProperty);
					actionStepPropertyMap.put(serverActionStepProperty.getActionStepLookupFkId(), serverActionStepPropertyList);
				}
				else{
					actionStepPropertyMap.get(serverActionStepProperty.getActionStepLookupFkId()).add(serverActionStepProperty);
				}
			}
		}
		catch (SQLException e) {
		e.printStackTrace();
		}
		return actionStepPropertyMap;
	}
}
