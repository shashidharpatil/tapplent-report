package com.tapplent.platformutility.lexer;

import java.util.ArrayList;
import java.util.List;

import com.tapplent.platformutility.search.builder.expression.Operator;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;


public class Lexer {
	
	private int lookahead = 0;
	private int line = 1;
	private Token previousToken;
	public LexerResult getTokens(String expression){
		LexerResult result = new LexerResult();
		//TODO update this DISTINCT flag if a token says DISTINCT
		//TODO if it is an aggregate attribute path expression we need to add the DISTINCT keyword or else the current distinct will work.
		expression = expression.trim();
		if(expression.toUpperCase().startsWith("DISTINCT")){
			expression = expression.substring(8);
			result.setDistinct(true);
		}
		List<Object> tokens = new ArrayList<>();
		char[] array = expression.toCharArray();
		Token token = null;
		while(lookahead < array.length){
			previousToken = token;
			token = getNextToken(array);
			if(token==null)
				return new LexerResult();
			if (token.operator != null && token.operator.toString().equalsIgnoreCase("B_SUB")) {
				if (previousToken == null || previousToken.operator != null)
					token.operator = Operator.U_SUB;
			}
			if(token.operator!=null){
				tokens.add(token.operator.toString());
			}else{
				tokens.add(token.leafOperand);
			}
		}
		result.setTokens(tokens);
		return result;
	}
	private Token getNextToken(char[] array) {
		Token token = new Token(); 
		StringBuilder pToken = new StringBuilder();
		pToken.append("                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 ");
		pToken.setCharAt(0, array[lookahead]);
		int result = 0;
		int j = 0;      //variable for temp array index
		
		increaseLineNumberIfRequired(array, pToken.charAt(0));
		pToken.setCharAt(0, array[lookahead]);
		if(pToken.charAt(0) == ' ' || pToken.charAt(0) == '\t'){		//Condition for white spaces and tab.
			while(array[lookahead]=='\t' || array[lookahead] == ' ')
			{
				lookahead++;
			}
		}
		pToken.setCharAt(0, array[lookahead]);
		
		if(pToken.charAt(0) >= 'a' && pToken.charAt(0) <= 'z' || pToken.charAt(0) >= 'A' && pToken.charAt(0) <= 'Z'){ //if the first letter is alphabet.
			while(lookahead < array.length && (array[lookahead] >= 'a' && array[lookahead] <= 'z' || array[lookahead] >= 'A' && array[lookahead] <= 'Z' || array[lookahead] >= '0' && array[lookahead] <= '9')){
				pToken.setCharAt(j, array[lookahead]);
				j++;
				lookahead++;
			}
			result = 1;
		}
		
		if(pToken.charAt(0) >= '0' && pToken.charAt(0) <= '9' ){//if the first character is a digit.
			result = 2;
		}
		
		if(pToken.charAt(0) == '!'){// if the first character signifies a DOA
			result = 3;
		}
		switch(result){
			case 0:
				switch(pToken.charAt(0)){
					case '*': lookahead++; token.operator = Operator.MULTIPLY; return token;
					case '^': lookahead++; token.operator = Operator.BITWISE_XOR; return token;
					case '/': lookahead++; token.operator = Operator.DIVIDE; return token;
					case '%': lookahead++; token.operator = Operator.MODULO_OP; return token;
					case '-': lookahead++; token.operator = Operator.B_SUB; return token;
					case '+': lookahead++; token.operator = Operator.ADD; return token;
					case '=': lookahead++; token.operator = Operator.EQ; return token;
					case ',': lookahead++; token.operator = Operator.COMMA; return token;
					case '&': lookahead++; token.operator = Operator.BITWISE_AND; return token;
					case '|': lookahead++; token.operator = Operator.OR; return token;
					case '(': lookahead++; token.operator = Operator.OPEN_PARENTHESIS; return token;
					case ')': lookahead++; token.operator = Operator.CLOSE_PARENTHESIS; return token;
					case '>': lookahead++;
						if(array[lookahead] == '='){
							lookahead++;
							token.operator = Operator.GTE;
							return token;
						}
						else if(array[lookahead] == '>'){
							lookahead++;
							token.operator = Operator.RIGHT_SHIFT;
							return token;
						}
						token.operator = Operator.GT;
						return token;
					case '<': lookahead++;
						if(array[lookahead] == '='){
							lookahead++;
							if(array[lookahead] == '>'){
								token.operator = Operator.NULL_SAFE_EQ;
								return token;
							}
							token.operator = Operator.LE;
							return token;
						}
						else if(array[lookahead] == '>'){
							lookahead++;
							token.operator = Operator.NE;
							return token;
						}
						else if(array[lookahead] == '<'){
							lookahead++;
							token.operator = Operator.LEFT_SHIFT;
							return token;
						}
						token.operator = Operator.LT;
						return token;
					case ':': lookahead++; 
						if(array[lookahead] == '='){
							lookahead++;
							token.operator = Operator.ASSIGNMENT;
							return token;
						}
					case '\'': lookahead++;
						StringBuilder tempValue = new StringBuilder();
						while(lookahead < array.length && array[lookahead] != '\''){
							tempValue.append(array[lookahead]);
							lookahead++;
						}
						lookahead++;
						if(tempValue.toString().startsWith("0x")){
							token.leafOperand.setDataType("T_ID");
							token.leafOperand.setValue(tempValue.toString());
						}else {
							token.leafOperand.setValue(tempValue.toString());
							token.leafOperand.setDataType("TEXT");
						}
						return token;
				}
			case 1: 
//				switch(Character.toLowerCase(pToken.charAt(0))){
//					case 'a': 
//						switch(Character.toLowerCase(pToken.charAt(1))){
//							case 'n':
//								switch(Character.toLowerCase(pToken.charAt(2))){
//									case 'd': token.operator = Operator.AND;
//									return token;
//								}
//						}
//					case 'b':
//						switch(Character.toLowerCase(pToken.charAt(1))){
//							case 'e':
//								switch(Character.toLowerCase(pToken.charAt(2))){
//								case 't':
//									switch(Character.toLowerCase(pToken.charAt(3))){
//									case 'w':
//										switch(Character.toLowerCase(pToken.charAt(4))){
//										case 'e':
//											switch(Character.toLowerCase(pToken.charAt(5))){
//											case 'e':
//												switch(Character.toLowerCase(pToken.charAt(6))){
//												case 'n':token.operator = Operator.BW;
//												return token;
//												}
//											}
//										}
//									}
//								}
//							}
//					case 'd': 
//						switch(Character.toLowerCase(pToken.charAt(1))){
//							case 'i':
//								switch(Character.toLowerCase(pToken.charAt(2))){
//									case 'v': token.operator = Operator.INT_DIV;
//									return token;
//								}
//						}
//					case 'l': 
//						switch(Character.toLowerCase(pToken.charAt(1))){
//							case 'i':
//								switch(Character.toLowerCase(pToken.charAt(2))){
//									case 'k':
//										switch(Character.toLowerCase(pToken.charAt(3))){
//											case 'e':token.operator = Operator.LIKE;
//											return token;
//								}
//						}
//					}
//				}
				if(pToken.toString().trim().equalsIgnoreCase("and")){
					token.operator = Operator.AND;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("between")){
					token.operator = Operator.BW;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("div")){
					token.operator = Operator.INT_DIV;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("like")){
					token.operator = Operator.LIKE;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("in")){
					token.operator = Operator.IN;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("null")){
					token.operator = Operator.NULL;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("not")){
					Token nextToken = getNextToken(array);
					if(nextToken.operator.toString().equalsIgnoreCase("in")){
						token.operator = Operator.NOT_IN;
						return token;
					}
					else if(nextToken.operator.equals(Operator.NULL)){
						token.operator = Operator.NOT_NULL;
						return token;
					}
					else
						token.operator = Operator.ERROR;//FIXME
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("xor")){
					token.operator = Operator.XOR;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("or")){
					token.operator = Operator.OR;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("is")){
					Token nextToken = getNextToken(array);
					if(nextToken.operator.equals(Operator.NOT_NULL)) {
						token.operator = Operator.IS_NOT_NULL;
						return token;
					}
					else if(nextToken.operator.equals(Operator.NULL)){
						token.operator = Operator.IS_NULL;
						return token;
					}
				}else if(pToken.toString().trim().equalsIgnoreCase("concat")){
					token.operator = Operator.CONCAT;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("dbcount")){
					token.operator = Operator.DBCOUNT;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("day")){
					token.operator = Operator.DAY;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("month")){
					token.operator = Operator.MONTH;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("year")){
					token.operator = Operator.YEAR;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("weekday")){
					token.operator = Operator.WEEKDAY;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("lastday")){
					token.operator = Operator.LASTDAY;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("dbaverage")){
					token.operator = Operator.DBAVERAGE;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("dbmax")){
					token.operator = Operator.DBMAX;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("dbmin")){
					token.operator = Operator.DBMIN;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("dbstddev")){
					token.operator = Operator.DBSTDDEV;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("dbsum")){
					token.operator = Operator.DBSUM;
					return token;
				}
				else if(pToken.toString().trim().equalsIgnoreCase("dbvar")){
					token.operator = Operator.DBVAR;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("mod")){
					token.operator = Operator.MOD;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("exchangeRate")){
					token.operator = Operator.EXCHANGERATE;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("COALESCE")){
					token.operator = Operator.COALESCE;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("left")){
					token.operator = Operator.LEFT;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("intcast")){
					token.operator = Operator.INTCAST;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("g11nPlural")){
					token.operator = Operator.G11N_PLURAL;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("g11nStatic")){
					token.operator = Operator.G11N_STATIC;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("substring")){
					token.operator = Operator.SUBSTRING;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("if")){
					token.operator = Operator.IF;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("field")){
					token.operator = Operator.FIELD;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("distinct")){
					token.operator = Operator.DISTINCT;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("today")){
					token.leafOperand.setValue("UTC_DATE");
					token.leafOperand.setDataType("T_KEYWORD");
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("asc")){
					token.leafOperand.setValue("ASC");
					token.leafOperand.setDataType("T_KEYWORD");
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("desc")){
					token.leafOperand.setValue("DESC");
					token.leafOperand.setDataType("T_KEYWORD");
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("yr")){
					token.leafOperand.setValue("YEAR");
					token.leafOperand.setDataType("T_KEYWORD");
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("mnt")){
					token.leafOperand.setValue("MONTH");
					token.leafOperand.setDataType("T_KEYWORD");
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("dy")){
					token.leafOperand.setValue("DAY");
					token.leafOperand.setDataType("T_KEYWORD");
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("QUAR")){
					token.leafOperand.setValue("QUARTER");
					token.leafOperand.setDataType("T_KEYWORD");
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("datediff")){
					token.operator = Operator.DATEDIFF;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("datesub")){
					token.operator = Operator.DATESUB;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("dateadd")){
					token.operator = Operator.DATEADD;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("dateFormat")){
					token.operator = Operator.DATE_FORMAT;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("appendIsNull")){
					token.operator = Operator.APPEND_IS_NULL;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("asint")){
					token.leafOperand.setValue("AS INT");
					token.leafOperand.setDataType("T_KEYWORD");
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("YearInterval")){
					token.operator = Operator.INTERVAL_YR;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("MonthInterval")){
					token.operator = Operator.INTERVAL_MONTH;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("DayInterval")){
					token.operator = Operator.INTERVAL_DAY;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("WeekInterval")){
					token.operator = Operator.INTERVAL_WEEK;
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("interval")){
					Token valueToken = getNextToken(array);
					String diffValue = valueToken.leafOperand.getValue();
					Token unitToken = getNextToken(array);
					token.leafOperand.setDataType("T_KEYWORD");
					token.leafOperand.setValue("INTERVAL "+diffValue+" "+unitToken.leafOperand.getValue());
					return token;
				}else if(pToken.toString().trim().equalsIgnoreCase("loggedInUser")||pToken.toString().trim().equalsIgnoreCase("loggedInPerson")){
					token.leafOperand.setDataType("T_ID");
					token.leafOperand.setValue(TenantContextHolder.getUserContext().getPersonId());
					return token;
				}
				else {
					token.leafOperand.setValue(pToken.toString().trim());
					token.leafOperand.setDataType("TEXT");
					return null;
				}
			case 2:
//				if(array[lookahead] == '0' && ++lookahead < array.length && array[lookahead] == 'x'){
//					pToken.setCharAt(j++, '0');
//					pToken.setCharAt(j++, array[lookahead]);
//					lookahead++;
//					while(lookahead < array.length && ((array[lookahead] >= '0' && array[lookahead] <= '9') || (array[lookahead] >= 'A' && array[lookahead] <= 'F'))){
//						pToken.setCharAt(j, array[lookahead]);
//						j++;
//						lookahead++;
//					}
//					token.leafOperand.setDataType("T_ID");
//					token.leafOperand.setValue(pToken.toString().trim().replace("0x",""));
//				}
//				else {
				while(lookahead < array.length && ((array[lookahead] >= '0' && array[lookahead] <= '9') || array[lookahead] == '.')){
					if(array[lookahead] == '.')
						token.leafOperand.setDataType("DECIMAL");
					pToken.setCharAt(j, array[lookahead]);
					j++;
					lookahead++;
				}
				if(token.leafOperand.getDataType() == null)
					token.leafOperand.setDataType("INTEGER");
				token.leafOperand.setValue(pToken.toString().trim());
//				}

				return token;
			case 3:
				if(array[++lookahead] == '{'){
					while(array[++lookahead] != '}'){
						pToken.setCharAt(j, array[lookahead]);
						j++;
					}
					lookahead++;
					/*
					 * only the value of doa is getting set.
					 * Make the complete column the way you want where you are calling the list of tokens. 
					 */
					token.getLeafOperand().setIsColumnName(true);
					token.leafOperand.setValue(pToken.toString().trim());
					return token;
				}
		}
		return null;
	}
	
	private void increaseLineNumberIfRequired(char[] array, char c) {
		if(c == '\n'){
			line++;
			lookahead++;
			while(array[lookahead] == '\t' || array[lookahead]  == ' '){
				lookahead++;
			}
			c = array[lookahead];
			if(c == '\n'){
				increaseLineNumberIfRequired(array, c);
			}
			else return;
		}
		else return;
	}
}
