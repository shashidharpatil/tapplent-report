package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 28/12/16.
 */
public class SocialStatusVO {
    private String socialStatusCodePkId;
    private String socialStatusNameG11nBigTxt;
    private String socialStatusIconCodeFKId;
    private String overrideFontColourTxt;

    public String getSocialStatusCodePkId() {
        return socialStatusCodePkId;
    }

    public void setSocialStatusCodePkId(String socialStatusCodePkId) {
        this.socialStatusCodePkId = socialStatusCodePkId;
    }

    public String getSocialStatusNameG11nBigTxt() {
        return socialStatusNameG11nBigTxt;
    }

    public void setSocialStatusNameG11nBigTxt(String socialStatusNameG11nBigTxt) {
        this.socialStatusNameG11nBigTxt = socialStatusNameG11nBigTxt;
    }

    public String getSocialStatusIconCodeFKId() {
        return socialStatusIconCodeFKId;
    }

    public void setSocialStatusIconCodeFKId(String socialStatusIconCodeFKId) {
        this.socialStatusIconCodeFKId = socialStatusIconCodeFKId;
    }

    public String getOverrideFontColourTxt() {
        return overrideFontColourTxt;
    }

    public void setOverrideFontColourTxt(String overrideFontColourTxt) {
        this.overrideFontColourTxt = overrideFontColourTxt;
    }
}
