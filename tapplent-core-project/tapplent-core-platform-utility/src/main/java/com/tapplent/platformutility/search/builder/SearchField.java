package com.tapplent.platformutility.search.builder;

public class SearchField {
    private String doAtributeCode;
    private String columnName;
    private String tableAlias;
    private String columnAlias;
    private String columnLabel;
    private boolean distinct;
    private String attributeAlias;
    private boolean aggFunction;
    private String dataType;
    private boolean dependencyFlag;
    private boolean isGlocalized;
    private String containerBlobName;
	private String dbPrimaryKeyColumnLabel;
    private String recordStateColumnLabel;
    private Boolean isInternal = false;
    public SearchField(String attributeName){
    	this.doAtributeCode = attributeName;
    }
	public SearchField(String columnLabel, String dataType){
		this.columnLabel = columnLabel;
		this.dataType = dataType;
	}
	public String getDoAtributeCode() {
		return doAtributeCode;
	}
	public void setDoAtributeCode(String doAtributeCode) {
		this.doAtributeCode = doAtributeCode;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		
		this.columnName = columnName;
	}
	public String getColumnAlias() {
		return columnAlias;
	}
	public void setColumnAlias(String columnAlias) {
		this.columnAlias = columnAlias;
	}
	public boolean isDistinct() {
		return distinct;
	}
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}
	public String getAttributeAlias() {
		return attributeAlias;
	}
	public void setAttributeAlias(String attributeAlias) {
		this.attributeAlias = attributeAlias;
	}
	public String getColumnLabel() {
		return columnLabel;
	}
	public void setColumnLabel(String columnLabel) {
		this.columnLabel = columnLabel;
	}
	public boolean isAggFunction() {
		return aggFunction;
	}
	public void setAggFunction(boolean aggFunction) {
		this.aggFunction = aggFunction;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public boolean isDependencyFlag() {
		return dependencyFlag;
	}
	public void setDependencyFlag(boolean dependencyFlag) {
		this.dependencyFlag = dependencyFlag;
	}
	public String getTableAlias() {
		return tableAlias;
	}
	public void setTableAlias(String tableAlias) {
		this.tableAlias = tableAlias;
	}
	public boolean isGlocalized() {
		return isGlocalized;
	}
	public void setGlocalized(boolean isGlocalized) {
		this.isGlocalized = isGlocalized;
	}
	public String getContainerBlobName() {
		return containerBlobName;
	}
	public void setContainerBlobName(String containerBlobName) {
		this.containerBlobName = containerBlobName;
	}

	public String getDbPrimaryKeyColumnLabel() {
		return dbPrimaryKeyColumnLabel;
	}

	public void setDbPrimaryKeyColumnLabel(String dbPrimaryKeyColumnLabel) {
		this.dbPrimaryKeyColumnLabel = dbPrimaryKeyColumnLabel;
	}

    public String getRecordStateColumnLabel() {
        return recordStateColumnLabel;
    }

    public void setRecordStateColumnLabel(String recordStateColumnLabel) {
        this.recordStateColumnLabel = recordStateColumnLabel;
    }

	public Boolean getInternal() {
		return isInternal;
	}

	public void setInternal(Boolean internal) {
		isInternal = internal;
	}
}
