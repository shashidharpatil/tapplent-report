package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.POST_PostData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;

import javax.sql.DataSource;
import java.util.List;



public class PostItemWriter extends JdbcBatchItemWriter<POST_PostData> {

	public PostItemWriter(){
		super();
		DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
		setDataSource(ds);
		setSql(getSQL());	
		setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<POST_PostData>());
		afterPropertiesSet();
	}
	
	
	public String getSQL(){
		String sql = "UPDATE T_PRN_IEU_SOC_ACC_PERSON_POST SET RETURN_STATUS_CODE_FK_ID = 'COMPLETED' WHERE SOCIAL_ACCOUNT_PERSON_POST_PK_ID = :socialAccountPersonPostPkId";
		return sql;
		
	}
	@Override
	public void write(List<? extends POST_PostData> arg0) throws Exception {
		
		super.write(arg0);
		
		for(POST_PostData bdg : arg0){
			
			System.out.println("inside arg0" + arg0.size());
			if(bdg.getNotificationDefinitionFkId()!=null){
				
				//(TDWI) insert into Notification Transaction
				
			}
			
		}
		
		
	}

}
