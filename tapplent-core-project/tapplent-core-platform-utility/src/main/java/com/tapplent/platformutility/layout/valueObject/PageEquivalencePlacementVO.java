package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class PageEquivalencePlacementVO {
	String pageEquivalencePlacementId;
	String baseTemplate;
	String containerPage;
	String providerPage;
	JsonNode deviceIndependentBlob;
	JsonNode deviceDependentBlob;
	public String getContainerPage() {
		return containerPage;
	}
	public void setContainerPage(String containerPage) {
		this.containerPage = containerPage;
	}
	public String getBaseTemplate() {
		return baseTemplate;
	}
	public void setBaseTemplate(String baseTemplate) {
		this.baseTemplate = baseTemplate;
	}
	public String getProviderPage() {
		return providerPage;
	}
	public void setProviderPage(String providerPage) {
		this.providerPage = providerPage;
	}
	public String getPageEquivalencePlacementId() {
		return pageEquivalencePlacementId;
	}
	public void setPageEquivalencePlacementId(String pageEquivalencePlacementId) {
		this.pageEquivalencePlacementId = pageEquivalencePlacementId;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
}
