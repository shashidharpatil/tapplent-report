package com.tapplent.platformutility.workflow.structure;

import java.sql.Timestamp;

public class WorkflowActorDefn {
	private String versionId;
	private String workflowActorDefnPkId;
	private String workflowStepDefnFkId;
	private int actorDispSeqNumPosInt;
	private String actorNameG11nBigTxt;
	private String actorIconCodeFkId;
	private String actorPersonGroupFkId;
	private boolean isRelativeToLoggedInUser;
	private boolean isRelativeToSubjectUser;
	private String relativeToActorId;
	private String relativeRelationshipCodeFkId;
	private int relativeRelationshipHierarchyLevelPosInt;
	private Timestamp resolveActorAsOfDatetime;
	private boolean isAdhocUserActor;
	private boolean allowExternalUser;
	private String noActorsActionCodeFkId;
	private String notesG11nBigTxt;
	private String createdDatetime;
	private String lastModifiedDatetime;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getWorkflowActorDefnPkId() {
		return workflowActorDefnPkId;
	}
	public void setWorkflowActorDefnPkId(String workflowActorDefnPkId) {
		this.workflowActorDefnPkId = workflowActorDefnPkId;
	}
	public String getWorkflowStepDefnFkId() {
		return workflowStepDefnFkId;
	}
	public void setWorkflowStepDefnFkId(String workflowStepDefnFkId) {
		this.workflowStepDefnFkId = workflowStepDefnFkId;
	}
	public int getActorDispSeqNumPosInt() {
		return actorDispSeqNumPosInt;
	}
	public void setActorDispSeqNumPosInt(int actorDispSeqNumPosInt) {
		this.actorDispSeqNumPosInt = actorDispSeqNumPosInt;
	}
	public String getActorNameG11nBigTxt() {
		return actorNameG11nBigTxt;
	}
	public void setActorNameG11nBigTxt(String actorNameG11nBigTxt) {
		this.actorNameG11nBigTxt = actorNameG11nBigTxt;
	}
	public String getActorIconCodeFkId() {
		return actorIconCodeFkId;
	}
	public void setActorIconCodeFkId(String actorIconCodeFkId) {
		this.actorIconCodeFkId = actorIconCodeFkId;
	}
	public String getActorPersonGroupFkId() {
		return actorPersonGroupFkId;
	}
	public void setActorPersonGroupFkId(String actorPersonGroupFkId) {
		this.actorPersonGroupFkId = actorPersonGroupFkId;
	}
	public boolean isRelativeToLoggedInUser() {
		return isRelativeToLoggedInUser;
	}
	public void setRelativeToLoggedInUser(boolean isRelativeToLoggedInUser) {
		this.isRelativeToLoggedInUser = isRelativeToLoggedInUser;
	}
	public boolean isRelativeToSubjectUser() {
		return isRelativeToSubjectUser;
	}
	public void setRelativeToSubjectUser(boolean isRelativeToSubjectUser) {
		this.isRelativeToSubjectUser = isRelativeToSubjectUser;
	}
	public String getRelativeToActorId() {
		return relativeToActorId;
	}
	public void setRelativeToActorId(String relativeToActorId) {
		this.relativeToActorId = relativeToActorId;
	}
	public String getRelativeRelationshipCodeFkId() {
		return relativeRelationshipCodeFkId;
	}
	public void setRelativeRelationshipCodeFkId(String relativeRelationshipCodeFkId) {
		this.relativeRelationshipCodeFkId = relativeRelationshipCodeFkId;
	}
	public int getRelativeRelationshipHierarchyLevelPosInt() {
		return relativeRelationshipHierarchyLevelPosInt;
	}
	public void setRelativeRelationshipHierarchyLevelPosInt(int relativeRelationshipHierarchyLevelPosInt) {
		this.relativeRelationshipHierarchyLevelPosInt = relativeRelationshipHierarchyLevelPosInt;
	}
	public Timestamp getResolveActorAsOfDatetime() {
		return resolveActorAsOfDatetime;
	}
	public void setResolveActorAsOfDatetime(Timestamp resolveActorAsOfDatetime) {
		this.resolveActorAsOfDatetime = resolveActorAsOfDatetime;
	}
	public boolean isAdhocUserActor() {
		return isAdhocUserActor;
	}
	public void setAdhocUserActor(boolean isAdhocUserActor) {
		this.isAdhocUserActor = isAdhocUserActor;
	}
	public boolean isAllowExternalUser() {
		return allowExternalUser;
	}
	public void setAllowExternalUser(boolean allowExternalUser) {
		this.allowExternalUser = allowExternalUser;
	}
	public String getNoActorsActionCodeFkId() {
		return noActorsActionCodeFkId;
	}
	public void setNoActorsActionCodeFkId(String noActorsActionCodeFkId) {
		this.noActorsActionCodeFkId = noActorsActionCodeFkId;
	}
	public String getNotesG11nBigTxt() {
		return notesG11nBigTxt;
	}
	public void setNotesG11nBigTxt(String notesG11nBigTxt) {
		this.notesG11nBigTxt = notesG11nBigTxt;
	}
	public String getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(String createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public String getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(String lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}

}
