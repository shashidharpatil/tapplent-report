package com.tapplent.platformutility.person.structure;

import java.util.List;

/**
 * Created by tapplent on 07/11/16.
 */
public class PersonGroup {
    private String personId;
    private List<String> groupIds;

    public PersonGroup(String personId, List<String> groupIds) {
        this.personId = personId;
        this.groupIds = groupIds;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public List<String> getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(List<String> groupIds) {
        this.groupIds = groupIds;
    }
}
