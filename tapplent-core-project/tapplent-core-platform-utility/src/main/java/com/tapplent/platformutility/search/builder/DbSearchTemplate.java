package com.tapplent.platformutility.search.builder;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class DbSearchTemplate {
    private String templateId;
//    private String domainObjectId;
    private String table;
    private String domainObjectCode;
    private String tableAlias;
    /*
     * Uniquely identifies a table
     */
    private String tableId;
    /*
     * Set this flag to true once all the required columns are added
     */
    private boolean isColumnsAdded;
    private List<SearchField> columns;
    private Stack<SearchField> dependencyKeys;
    private Map<String, SearchField> columnAliasToColumnMap;
    private Map<String, SearchField> columnLabelToColumnMap;
    private Map<String, SearchField> doAttributeCodeToColumnMap;
    private List<DbSearchFilter> filters;
    private List<SortData> sortData;
    private Map<Integer, SortData> seqNumberToSortMap;
    private boolean isPrimaryDO;
    private boolean isDependencyToBeApplied;
	private SearchField functionPrimaryKey;
	private SearchField databasePrimaryKey;
	public DbSearchTemplate() {
		this.filters = new ArrayList<DbSearchFilter>();
		this.columnLabelToColumnMap = new HashMap<String, SearchField>();
		this.columnAliasToColumnMap = new HashMap<String, SearchField>();
		this.doAttributeCodeToColumnMap = new HashMap<String, SearchField>();
		this.seqNumberToSortMap = new HashMap<Integer, SortData>();
		this.columns = new ArrayList<SearchField>();
	}
	public void addColumn(SearchField column){
		this.columns.add(column);
		columnAliasToColumnMap.put(column.getColumnAlias(), column);
		columnLabelToColumnMap.put(column.getColumnLabel(), column);
		doAttributeCodeToColumnMap.put(column.getDoAtributeCode(), column);
		
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
//	public String getDomainObjectId() {
//		return domainObjectId;
//	}
//	public void setDomainObjectId(String domainObjectId) {
//		this.domainObjectId = domainObjectId;
//	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public String getDomainObjectCode() {
		return domainObjectCode;
	}
	public void setDomainObjectCode(String domainObjectCode) {
		this.domainObjectCode = domainObjectCode;
	}
	public String getTableAlias() {
		return tableAlias;
	}
	public void setTableAlias(String tableAlias) {
		this.tableAlias = tableAlias;
	}
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public List<SearchField> getColumns() {
		return columns;
	}
	public void setColumns(List<SearchField> columns) {
		this.columns = columns;
	}
	public Stack<SearchField> getDependencyKeys() {
		return dependencyKeys;
	}
	public void setDependencyKeys(Stack<SearchField> dependencyKeys) {
		this.dependencyKeys = dependencyKeys;
	}
	public boolean isPrimaryDO() {
		return isPrimaryDO;
	}
	public void setPrimaryDO(boolean isPrimaryDO) {
		this.isPrimaryDO = isPrimaryDO;
	}
	public Map<String, SearchField> getColumnAliasToColumnMap() {
		return columnAliasToColumnMap;
	}
	public void setColumnAliasToColumnMap(Map<String, SearchField> columnAliasToColumnMap) {
		this.columnAliasToColumnMap = columnAliasToColumnMap;
	}
	public Map<String, SearchField> getColumnLabelToColumnMap() {
		return columnLabelToColumnMap;
	}
	public void setColumnLabelToColumnMap(Map<String, SearchField> columnLabelToColumnMap) {
		this.columnLabelToColumnMap = columnLabelToColumnMap;
	}
	public boolean isColumnsAdded() {
		return isColumnsAdded;
	}
	public void setColumnsAdded(boolean isColumnsAdded) {
		this.isColumnsAdded = isColumnsAdded;
	}
	public List<DbSearchFilter> getFilters() {
		return filters;
	}
	public void setFilters(List<DbSearchFilter> filters) {
		this.filters = filters;
	}
	public List<SortData> getSortData() {
		return sortData;
	}
	public void setSortData(List<SortData> sortData) {
		this.sortData = sortData;
	}
	public Map<Integer, SortData> getSeqNumberToSortMap() {
		return seqNumberToSortMap;
	}
	public void setSeqNumberToSortMap(Map<Integer, SortData> seqNumberToSortMap) {
		this.seqNumberToSortMap = seqNumberToSortMap;
	}
	public boolean isDependencyToBeApplied() {
		return isDependencyToBeApplied;
	}
	public void setDependencyToBeApplied(boolean isDependencyToBeApplied) {
		this.isDependencyToBeApplied = isDependencyToBeApplied;
	}
	public Map<String, SearchField> getDoAttributeCodeToColumnMap() {
		return doAttributeCodeToColumnMap;
	}
	public void setDoAttributeCodeToColumnMap(Map<String, SearchField> doAttributeCodeToColumnMap) {
		this.doAttributeCodeToColumnMap = doAttributeCodeToColumnMap;
	}

	public SearchField getFunctionPrimaryKey() {
		return functionPrimaryKey;
	}

	public void setFunctionPrimaryKey(SearchField functionPrimaryKey) {
		this.functionPrimaryKey = functionPrimaryKey;
	}

	public SearchField getDatabasePrimaryKey() {
		return databasePrimaryKey;
	}

	public void setDatabasePrimaryKey(SearchField databasePrimaryKey) {
		this.databasePrimaryKey = databasePrimaryKey;
	}
}
