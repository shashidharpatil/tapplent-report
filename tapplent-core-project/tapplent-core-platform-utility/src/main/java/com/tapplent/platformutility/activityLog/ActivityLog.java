package com.tapplent.platformutility.activityLog;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.layout.valueObject.IconVO;

import java.net.URL;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Manas on 1/6/17.
 */
public class ActivityLog
{
    private String activityLogPkId;
    private String activityAuthorPerson;
    private String processTypeCodeFkId;
    private String mtPeCodeFkId;
    private IconVO doIconCode;
    private String doIconCodeFkId;
    private String doRowPrimaryKeyFkId;
    private String doRowVersionFkId;
    private String displayContextValueBigTxt;
    private String doNameG11nBigTxt;
    private String actionVisualMasterCode;
    private String msgYouInContextG11nBigTxt;
    private String msgYouOutOfContextG11nBigTxt;
    private String msgThirdPersonInContextG11nBigTxt;
    private String msgThirdPersonOutOfContextG11nBigTxt;
    @JsonIgnore
    private Timestamp activityDatetime;
    private String activityDatetimeString;
    private String dtlLaunchTrgtMtPeCodeFkId;
    private boolean isDeleted;
    private String personName;
    private URL personPhoto;
    private String personInitials;
    Map<String, ActivityLogDtl> activityLogDtlMap;

    public String getActivityLogPkId() {
        return activityLogPkId;
    }

    public void setActivityLogPkId(String activityLogPkId) {
        this.activityLogPkId = activityLogPkId;
    }

    public String getActivityAuthorPerson() {
        return activityAuthorPerson;
    }

    public void setActivityAuthorPerson(String activityAuthorPerson) {
        this.activityAuthorPerson = activityAuthorPerson;
    }

    public String getProcessTypeCodeFkId() {
        return processTypeCodeFkId;
    }

    public void setProcessTypeCodeFkId(String processTypeCodeFkId) {
        this.processTypeCodeFkId = processTypeCodeFkId;
    }

    public String getMtPeCodeFkId() {
        return mtPeCodeFkId;
    }

    public void setMtPeCodeFkId(String mtPeCodeFkId) {
        this.mtPeCodeFkId = mtPeCodeFkId;
    }

    public IconVO getDoIconCode() {
        return doIconCode;
    }

    public void setDoIconCode(IconVO doIconCode) {
        this.doIconCode = doIconCode;
    }

    public String getDoIconCodeFkId() {
        return doIconCodeFkId;
    }

    public void setDoIconCodeFkId(String doIconCodeFkId) {
        this.doIconCodeFkId = doIconCodeFkId;
    }

    public String getDoRowPrimaryKeyFkId() {
        return doRowPrimaryKeyFkId;
    }

    public void setDoRowPrimaryKeyFkId(String doRowPrimaryKeyFkId) {
        this.doRowPrimaryKeyFkId = doRowPrimaryKeyFkId;
    }

    public String getDoRowVersionFkId() {
        return doRowVersionFkId;
    }

    public void setDoRowVersionFkId(String doRowVersionFkId) {
        this.doRowVersionFkId = doRowVersionFkId;
    }

    public String getDisplayContextValueBigTxt() {
        return displayContextValueBigTxt;
    }

    public void setDisplayContextValueBigTxt(String displayContextValueBigTxt) {
        this.displayContextValueBigTxt = displayContextValueBigTxt;
    }

    public String getDoNameG11nBigTxt() {
        return doNameG11nBigTxt;
    }

    public void setDoNameG11nBigTxt(String doNameG11nBigTxt) {
        this.doNameG11nBigTxt = doNameG11nBigTxt;
    }

    public String getActionVisualMasterCode() {
        return actionVisualMasterCode;
    }

    public void setActionVisualMasterCode(String actionVisualMasterCode) {
        this.actionVisualMasterCode = actionVisualMasterCode;
    }

    public String getMsgYouInContextG11nBigTxt() {
        return msgYouInContextG11nBigTxt;
    }

    public void setMsgYouInContextG11nBigTxt(String msgYouInContextG11nBigTxt) {
        this.msgYouInContextG11nBigTxt = msgYouInContextG11nBigTxt;
    }

    public String getMsgYouOutOfContextG11nBigTxt() {
        return msgYouOutOfContextG11nBigTxt;
    }

    public void setMsgYouOutOfContextG11nBigTxt(String msgYouOutOfContextG11nBigTxt) {
        this.msgYouOutOfContextG11nBigTxt = msgYouOutOfContextG11nBigTxt;
    }

    public String getMsgThirdPersonInContextG11nBigTxt() {
        return msgThirdPersonInContextG11nBigTxt;
    }

    public void setMsgThirdPersonInContextG11nBigTxt(String msgThirdPersonInContextG11nBigTxt) {
        this.msgThirdPersonInContextG11nBigTxt = msgThirdPersonInContextG11nBigTxt;
    }

    public String getMsgThirdPersonOutOfContextG11nBigTxt() {
        return msgThirdPersonOutOfContextG11nBigTxt;
    }

    public void setMsgThirdPersonOutOfContextG11nBigTxt(String msgThirdPersonOutOfContextG11nBigTxt) {
        this.msgThirdPersonOutOfContextG11nBigTxt = msgThirdPersonOutOfContextG11nBigTxt;
    }

    public Timestamp getActivityDatetime() {
        return activityDatetime;
    }

    public void setActivityDatetime(Timestamp activityDatetime) {
        this.activityDatetime = activityDatetime;
    }

    public String getDtlLaunchTrgtMtPeCodeFkId() {
        return dtlLaunchTrgtMtPeCodeFkId;
    }

    public void setDtlLaunchTrgtMtPeCodeFkId(String dtlLaunchTrgtMtPeCodeFkId) {
        this.dtlLaunchTrgtMtPeCodeFkId = dtlLaunchTrgtMtPeCodeFkId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public URL getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(URL personPhoto) {
        this.personPhoto = personPhoto;
    }

    public String getPersonInitials() {
        return personInitials;
    }

    public void setPersonInitials(String personInitials) {
        this.personInitials = personInitials;
    }

    public String getActivityDatetimeString() {
        return activityDatetimeString;
    }

    public void setActivityDatetimeString(String activityDatetimeString) {
        this.activityDatetimeString = activityDatetimeString;
    }

    public Map<String, ActivityLogDtl> getActivityLogDtlMap() {
        return activityLogDtlMap;
    }

    public void setActivityLogDtlMap(Map<String, ActivityLogDtl> activityLogDtlMap) {
        this.activityLogDtlMap = activityLogDtlMap;
    }
}
