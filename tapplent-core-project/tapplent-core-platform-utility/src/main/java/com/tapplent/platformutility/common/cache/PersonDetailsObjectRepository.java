package com.tapplent.platformutility.common.cache;

import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.groupresolver.dao.GroupResolverDAO;
import com.tapplent.platformutility.groupresolver.valueobject.GroupVO;
import com.tapplent.platformutility.hierarchy.service.HierarchyService;
import com.tapplent.platformutility.hierarchy.structure.Relation;
import com.tapplent.platformutility.person.service.PersonService;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.person.structure.PersonGroup;
import com.tapplent.platformutility.uilayout.valueobject.GenericQueryVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Shubham Patodi on 09/08/16.
 */
public class PersonDetailsObjectRepository {
    private PersonService personService;
    private HierarchyService hierarchyService;
    private GroupResolverDAO groupResolverDAO;
    private final Map<String, Person> systemDummyPersonMap = new ConcurrentHashMap<>();
    private final Map<String, Person> personIdMap = new ConcurrentHashMap<>();
    private final Map<String, Map<String, List<Relation>>> personRelationMap = new ConcurrentHashMap<>();
    private final List<GroupVO> groupList = new ArrayList<>();

    public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(PersonService personService) {
        this.personService = personService;
    }

    public Map<String, Person> getPersonIdMap() {
        return personIdMap;
    }

    public Person getPersonDetails(String personId){
        if(personIdMap.containsKey(personId)){
            return personIdMap.get(personId);
        }else{
            Person person = personService.getPerson(personId);
            if(person!=null)
                personIdMap.put(personId, person);
            return person;
        }
    }

    public PersonGroup getPersonGroups(String personId) {
        if(personIdMap.containsKey(personId)){
            return personIdMap.get(personId).getPersonGroups();
        }else{
            Person person = getPersonDetails(personId);
            if(person!=null) {
                personIdMap.put(person.getPersonPkId(), person);
                return personIdMap.get(personId).getPersonGroups();
            }
        }
        return null;
    }

    public Person getSystemDummyPersonDetails(String personName){
        if(systemDummyPersonMap.containsKey(personName)){
            return systemDummyPersonMap.get(personName);
        }else{
            Person person = personService.getSystemDummyPerson(personName);
            if(person!=null)
                systemDummyPersonMap.put(personName, person);
            return person;
        }
    }

    public String getDefaultBTForGroup(String mtPE, String personId) {
        Person person = getPersonDetails(personId);
        String baseTemplate = person.getMtPEToDefaultBTPEMap().get(mtPE);
        if(!StringUtil.isDefined(baseTemplate)){
            baseTemplate = personService.getDefaultBTForGroup(mtPE, person);
            person.getMtPEToDefaultBTPEMap().put(mtPE, baseTemplate);
        }
        return baseTemplate;
    }

    public Map<String, Map<String, List<Relation>>> getPersonRelationMap(String personOneId, String personTwoId){
        Map<String, Map<String, List<Relation>>> relationMap = new ConcurrentHashMap<>();
        if (personRelationMap.containsKey(personOneId) && personRelationMap.get(personOneId).containsKey(personTwoId)){
                Map<String, List<Relation>> internalRelationMapEntry = new HashMap<>();
                internalRelationMapEntry.put(personTwoId, personRelationMap.get(personOneId).get(personTwoId));
                relationMap.put(personOneId, internalRelationMapEntry);
        }
        else {
            relationMap = getRelationAndPopulatePersonRelationMap(personOneId, personTwoId, personRelationMap);
        }
        if (personRelationMap.containsKey(personTwoId) && personRelationMap.get(personTwoId).containsKey(personOneId)){
                Map<String, List<Relation>> internalRelationMapEntry = new HashMap<>();
                internalRelationMapEntry.put(personOneId, personRelationMap.get(personTwoId).get(personOneId));
                relationMap.put(personTwoId, internalRelationMapEntry);
        }
        else {
            relationMap = getRelationAndPopulatePersonRelationMap(personOneId, personTwoId, personRelationMap);
        }
        return relationMap;
    }

    public List<GroupVO> getGroupList(){
        if (groupList.isEmpty()){
            List<GroupVO> groupVOList = groupResolverDAO.getAllGroups();
            for (GroupVO groupVO : groupVOList){
                GenericQueryVO genericQueryVO = groupResolverDAO.getGenericQuery(groupVO.getGenericQueryID());
                groupVO.setGenericQueryVO(genericQueryVO);
            }
            groupList.addAll(groupVOList);
        }
        return groupList;
    }

    private Map<String,Map<String,List<Relation>>> getRelationAndPopulatePersonRelationMap(String personOneId, String personTwoId, Map<String, Map<String, List<Relation>>> personRelationMap) {
        Map<String, Map<String, List<Relation>>> relationMap = hierarchyService.getAllRelationBetweenTwoPerson(personOneId, personTwoId);
        if (personRelationMap.containsKey(personOneId)) {
            personRelationMap.get(personOneId).putAll(relationMap.get(personOneId));
        }
        else personRelationMap.put(personOneId, relationMap.get(personOneId));
        if (personRelationMap.containsKey(personTwoId)){
            personRelationMap.get(personTwoId).putAll(relationMap.get(personTwoId));
        }
        else personRelationMap.put(personTwoId, relationMap.get(personTwoId));
        return relationMap;
    }

    public void resetPersonDetailsObjectRepository(){
        personIdMap.clear();
        personRelationMap.clear();
        groupList.clear();
    }

    public Map<String, Map<String, List<Relation>>> getPersonRelationMap() {
        return personRelationMap;
    }

    public HierarchyService getHierarchyService() {
        return hierarchyService;
    }

    public void setHierarchyService(HierarchyService hierarchyService) {
        this.hierarchyService = hierarchyService;
    }

    public GroupResolverDAO getGroupResolverDAO() {
        return groupResolverDAO;
    }

    public void setGroupResolverDAO(GroupResolverDAO groupResolverDAO) {
        this.groupResolverDAO = groupResolverDAO;
    }
}
