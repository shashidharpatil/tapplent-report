package com.tapplent.platformutility.batch.controller;

import com.tapplent.platformutility.batch.model.SchedulerInstanceModel;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.Callable;

public class AccountValidation implements Callable {
	SchedulerInstanceModel schedulerInstanceModel;

	public AccountValidation(SchedulerInstanceModel schedulerInstanceModel) {
		this.schedulerInstanceModel = schedulerInstanceModel;
	}

	@Override
	public Object call() throws Exception {
		String[] springConfig = { "jobs/SocAcc.xml" };
		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
		jobParametersBuilder.addString("schedulerInstancePkId", schedulerInstanceModel.getSchedulerInstancePkId());
		Job job = (Job) context.getBean("SocAccValidator");
		try {
			JobExecution execution = jobLauncher.run(job, jobParametersBuilder.toJobParameters());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
