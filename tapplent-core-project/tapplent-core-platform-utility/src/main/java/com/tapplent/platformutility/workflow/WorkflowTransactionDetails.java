package com.tapplent.platformutility.workflow;

import java.sql.Timestamp;

public class WorkflowTransactionDetails {
	private String workflowTransactionPkId;
	private String actorPersonGroupFkId;
	private String relativeToActorId;
	private String relativeRelationshipCodeFkId;
	private String relativeRelationshipHierarchyLevelPosInt;
	private String workflowActorFkId;
	private String actorActionFkId;
	private String actionTypeCodeFkId;
	private String stepFkId;
	private Timestamp resolveActorAsOfDatetime;
	private boolean isRelativeToLoggedInUser;
	private boolean isRelativeToSubjectUser;
	public String getWorkflowTransactionPkId() {
		return workflowTransactionPkId;
	}
	public void setWorkflowTransactionPkId(String workflowTransactionPkId) {
		this.workflowTransactionPkId = workflowTransactionPkId;
	}
	public String getActorPersonGroupFkId() {
		return actorPersonGroupFkId;
	}
	public void setActorPersonGroupFkId(String actorPersonGroupFkId) {
		this.actorPersonGroupFkId = actorPersonGroupFkId;
	}
	public String getRelativeToActorId() {
		return relativeToActorId;
	}
	public void setRelativeToActorId(String relativeToActorId) {
		this.relativeToActorId = relativeToActorId;
	}
	public String getRelativeRelationshipCodeFkId() {
		return relativeRelationshipCodeFkId;
	}
	public void setRelativeRelationshipCodeFkId(String relativeRelationshipCodeFkId) {
		this.relativeRelationshipCodeFkId = relativeRelationshipCodeFkId;
	}
	public String getRelativeRelationshipHierarchyLevelPosInt() {
		return relativeRelationshipHierarchyLevelPosInt;
	}
	public void setRelativeRelationshipHierarchyLevelPosInt(String relativeRelationshipHierarchyLevelPosInt) {
		this.relativeRelationshipHierarchyLevelPosInt = relativeRelationshipHierarchyLevelPosInt;
	}
	public String getWorkflowActorFkId() {
		return workflowActorFkId;
	}
	public void setWorkflowActorFkId(String workflowActorFkId) {
		this.workflowActorFkId = workflowActorFkId;
	}
	public String getActorActionFkId() {
		return actorActionFkId;
	}
	public void setActorActionFkId(String actorActionFkId) {
		this.actorActionFkId = actorActionFkId;
	}
	public String getActionTypeCodeFkId() {
		return actionTypeCodeFkId;
	}
	public void setActionTypeCodeFkId(String actionTypeCodeFkId) {
		this.actionTypeCodeFkId = actionTypeCodeFkId;
	}
	public String getStepFkId() {
		return stepFkId;
	}
	public void setStepFkId(String stepFkId) {
		this.stepFkId = stepFkId;
	}
	public Timestamp getResolveActorAsOfDatetime() {
		return resolveActorAsOfDatetime;
	}
	public void setResolveActorAsOfDatetime(Timestamp resolveActorAsOfDatetime) {
		this.resolveActorAsOfDatetime = resolveActorAsOfDatetime;
	}
	public boolean isRelativeToLoggedInUser() {
		return isRelativeToLoggedInUser;
	}
	public void setRelativeToLoggedInUser(boolean isRelativeToLoggedInUser) {
		this.isRelativeToLoggedInUser = isRelativeToLoggedInUser;
	}
	public boolean isRelativeToSubjectUser() {
		return isRelativeToSubjectUser;
	}
	public void setRelativeToSubjectUser(boolean isRelativeToSubjectUser) {
		this.isRelativeToSubjectUser = isRelativeToSubjectUser;
	}
}
