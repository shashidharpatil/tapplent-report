package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 04/01/17.
 */
public enum ControlType {
    ICON_TEXT,
    ICON_LABEL_TEXT,
    LABEL_TEXT,
    ICON_TEXT_ICON,
    LABEL_DATE,
    LABEL_TIME,
    LABEL_DATETIME,
    DATETIME_RANGE_ICON,
    IMAGE_CIRCLE_LABEL,
    LABEL,
    TAG,
    GRID,
    FEATURED,
    BOOKMARKED,
    MULTI_SELECT,
    SINGLE_SELECT,
    TEAM,
    ATTACHMENT,
    WRITE_TEXT_CONTROL,
    REACTION,
    RATING,
    VERTICAL_CHECK_BOX,
    SAVED_ACTIVE_STATE,
    COLLEG_IMAGE,
    COLLEG_TEXT,
    COLLEG_COUNT,
    TAG_MASTER,
    LABEL_PHONE_NUMBER,
    LOGGED_IN_PERSON_NAME,
    DEFAULT;
    public static ControlType valueOfControlType(String value){
        if(value!=null) {
            try {
                return ControlType.valueOf(value);
            } catch (IllegalArgumentException e) {
                return ControlType.DEFAULT;
            }
        }else{
            return ControlType.DEFAULT;
        }
    }
}
