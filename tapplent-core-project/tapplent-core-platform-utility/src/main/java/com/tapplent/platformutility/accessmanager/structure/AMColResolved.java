package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by tapplent on 04/11/16.
 */
public class AMColResolved {
    private String resolvedColumnPkId;
    private String resolvedRowFkId;
    private String domainObjectAttributeId;
    private boolean isCreatePermitted;
    private boolean isReadPermitted;
    private boolean isUpdatePermitted;
    private boolean isDeletePermitted;

    public String getResolvedColumnPkId() {
        return resolvedColumnPkId;
    }

    public void setResolvedColumnPkId(String resolvedColumnPkId) {
        this.resolvedColumnPkId = resolvedColumnPkId;
    }

    public String getResolvedRowFkId() {
        return resolvedRowFkId;
    }

    public void setResolvedRowFkId(String resolvedRowFkId) {
        this.resolvedRowFkId = resolvedRowFkId;
    }

    public String getDomainObjectAttributeId() {
        return domainObjectAttributeId;
    }

    public void setDomainObjectAttributeId(String domainObjectAttributeId) {
        this.domainObjectAttributeId = domainObjectAttributeId;
    }

    public boolean isCreatePermitted() {
        return isCreatePermitted;
    }

    public void setCreatePermitted(boolean createPermitted) {
        isCreatePermitted = createPermitted;
    }

    public boolean isReadPermitted() {
        return isReadPermitted;
    }

    public void setReadPermitted(boolean readPermitted) {
        isReadPermitted = readPermitted;
    }

    public boolean isUpdatePermitted() {
        return isUpdatePermitted;
    }

    public void setUpdatePermitted(boolean updatePermitted) {
        isUpdatePermitted = updatePermitted;
    }

    public boolean isDeletePermitted() {
        return isDeletePermitted;
    }

    public void setDeletePermitted(boolean deletePermitted) {
        isDeletePermitted = deletePermitted;
    }
}
