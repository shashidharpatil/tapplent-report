package com.tapplent.platformutility.expression.jeval.function.string;

import com.tapplent.platformutility.expression.jeval.EvaluationConstants;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.expression.jeval.function.Function;
import com.tapplent.platformutility.expression.jeval.function.FunctionConstants;
import com.tapplent.platformutility.expression.jeval.function.FunctionException;
import com.tapplent.platformutility.expression.jeval.function.FunctionHelper;
import com.tapplent.platformutility.expression.jeval.function.FunctionResult;

import java.util.ArrayList;

/**
 * Created by sripad on 13/01/16.
 */
public class Lpad implements Function {


    /**
     * Returns the name of the function - "lpad".
     *
     * @return The name of this function class.
     */
    public String getName() {
        return "lpad";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     *
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A String input is the first argument and second argument is
     *            an integer to tell the number of spaces to be added
     *            to the left of the input string. The string
     *            argument(s) HAS to be enclosed in quotes. White space that is
     *            not enclosed within quotes will be trimmed. Quote characters
     *            in the first and last positions of any string argument (after
     *            being trimmed) will be removed also. The quote characters used
     *            must be the same as the quote characters used by the current
     *            instance of Evaluator. If there are multiple arguments, they
     *            must be separated by a comma (",").
     *
     * @return Returns a string with every occurence of the old character
     *         replaced with the new character.
     *
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    public FunctionResult execute(final Evaluator evaluator, final String arguments)
            throws FunctionException {
        String result = null;
        String exceptionMessage = "One string argument and one integer "
                + "arguments are required.";

        ArrayList values = FunctionHelper.getOneStringAndOneInteger(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

        if (values.size() != 2) {
            throw new FunctionException(exceptionMessage);
        }

        try {
            String argumentOne = FunctionHelper.trimAndRemoveQuoteChars(
                    (String) values.get(0), evaluator.getQuoteCharacter());

            int pad = ((Integer) values.get(1)).intValue();
            for(int i=0; i<pad;i++){
                argumentOne=" "+argumentOne;
            }



            result = String.valueOf(argumentOne);

        } catch (FunctionException fe) {
            throw new FunctionException(fe.getMessage(), fe);
        } catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result,
                FunctionConstants.FUNCTION_RESULT_TYPE_STRING);
    }
}
