package com.tapplent.platformutility.uilayout.valueobject;

import com.tapplent.platformutility.layout.valueObject.IconVO;

/**
 * Created by tapplent on 06/07/17.
 */
public class OrgChartFilterRangeVO {
    private String orgChartFilterRangePKID;
    private String orgChartLabel;
    private String rangeName;
    private String rangeIcon;
    private double rangeMin;
    private String rangeMinName;
    private String rangeMinIcon;
    private double rangeMax;
    private String rangeMaxName;
    private String rangeMaxIcon;

    public String getOrgChartFilterRangePKID() {
        return orgChartFilterRangePKID;
    }

    public void setOrgChartFilterRangePKID(String orgChartFilterRangePKID) {
        this.orgChartFilterRangePKID = orgChartFilterRangePKID;
    }

    public String getOrgChartLabel() {
        return orgChartLabel;
    }

    public void setOrgChartLabel(String orgChartLabel) {
        this.orgChartLabel = orgChartLabel;
    }

    public String getRangeName() {
        return rangeName;
    }

    public void setRangeName(String rangeName) {
        this.rangeName = rangeName;
    }

    public String getRangeIcon() {
        return rangeIcon;
    }

    public void setRangeIcon(String rangeIcon) {
        this.rangeIcon = rangeIcon;
    }

    public double getRangeMin() {
        return rangeMin;
    }

    public void setRangeMin(double rangeMin) {
        this.rangeMin = rangeMin;
    }

    public String getRangeMinName() {
        return rangeMinName;
    }

    public void setRangeMinName(String rangeMinName) {
        this.rangeMinName = rangeMinName;
    }

    public String getRangeMinIcon() {
        return rangeMinIcon;
    }

    public void setRangeMinIcon(String rangeMinIcon) {
        this.rangeMinIcon = rangeMinIcon;
    }

    public double getRangeMax() {
        return rangeMax;
    }

    public void setRangeMax(double rangeMax) {
        this.rangeMax = rangeMax;
    }

    public String getRangeMaxName() {
        return rangeMaxName;
    }

    public void setRangeMaxName(String rangeMaxName) {
        this.rangeMaxName = rangeMaxName;
    }

    public String getRangeMaxIcon() {
        return rangeMaxIcon;
    }

    public void setRangeMaxIcon(String rangeMaxIcon) {
        this.rangeMaxIcon = rangeMaxIcon;
    }
}
