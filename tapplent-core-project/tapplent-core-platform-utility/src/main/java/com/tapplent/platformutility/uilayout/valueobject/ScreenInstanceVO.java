package com.tapplent.platformutility.uilayout.valueobject;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sripat on 01/11/16.
 */
public class ScreenInstanceVO implements Serializable {

    private String screenInstancePkId;
    private int themeSeqNumPosInt;
    private String screenMasterCodeFkId;
    private List<ScreenSectionInstanceVO> screenSectionInstances;
    @JsonIgnore
    private List<SectionControlVO> sectionControls = new ArrayList<>();
    public String getScreenMasterCodeFkId() {
        return screenMasterCodeFkId;
    }

    public void setScreenMasterCodeFkId(String screenMasterCodeFkId) {
        this.screenMasterCodeFkId = screenMasterCodeFkId;
    }

    public String getScreenInstancePkId() {
        return screenInstancePkId;
    }

    public void setScreenInstancePkId(String screenInstancePkId) {
        this.screenInstancePkId = screenInstancePkId;
    }

    public int getThemeSeqNumPosInt() {
        return themeSeqNumPosInt;
    }

    public void setThemeSeqNumPosInt(int themeSeqNumPosInt) {
        this.themeSeqNumPosInt = themeSeqNumPosInt;
    }

    public List<ScreenSectionInstanceVO> getScreenSectionInstances() {
        return screenSectionInstances;
    }

    public void setScreenSectionInstances(List<ScreenSectionInstanceVO> screenSectionInstances) {
        this.screenSectionInstances = screenSectionInstances;
    }

    public List<SectionControlVO> getSectionControls() {
        return sectionControls;
    }

    public void setSectionControls(List<SectionControlVO> sectionControls) {
        this.sectionControls = sectionControls;
    }
}
