package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class AttributeMetaVO {
	private String attributeMetaEuPkId; 
	private String grouperMetaProcessCode;
	private String btDoAttributeSpecFkId; 
	private String baseTemplateDepFkId; 
	private String metaProcessCode;
	private String btDoSpecDepFkId; 
	private String domainObjectFkId; 
	private String dbTableName ;
	private String doAttributeFkId; 
	private String mtDoAttributeSepcNameFkG11nText;
	private String dbColumnName;
	private String btDataTypeCodeFkId; 
	private String btDoAttrSpecNameG11nText;
	private JsonNode attrDisplayNameG11nBlob;
	private String attrIconFkId;
	private boolean functionalPrimaryKeyFlag;
	private boolean dbPrimaryKeyFlag;
	private boolean dependencyKeyFlag; 
	private boolean relationFlag; 
	private boolean hierarchyFlag; 
	private boolean selfJoinParentFlag; 
	private boolean reportableFlag;
	private boolean isSubjectCountry;
	private boolean isGlocalized;
	private boolean isCountrySpecific;
	private boolean isMandatory;
	private boolean isEncrypted;
	private boolean isAnonymized; 
	private boolean isDisplayMasked;
	private String  displayMaskCharacter;
	private boolean isReportMasked;
	private String reportMaskCharcter;
	private boolean isUniqueAttribute;
	private JsonNode attrHelpTextG11nBlob; 
	private JsonNode attrHintTextG11nBlob; 
	private boolean isUseSubjectPersonTimezone;
	private boolean isSortConfig;
	private int sortConfigSeq;
	private String sortOrderCodeFkId;
	private boolean isFilterConfig;
	private int filterConfigSeq;
	private int charMaxLength;
	private JsonNode attributeUnitOfMeasureG11nBlob; 
	private String booleanOnStateIconFkId;
	private String booleanOffStateIconFkId; 
	private boolean isEnableEmoticon;
	private boolean isContextObjectField;
	private int contextSeqNumber;
	private boolean isFilterOverride;
	private String toDomainObjectFkId; 
	private String toDbTableName;
	private String toDoAttributeFkId; 
	private String toDoAttributeNameG11nText;
	private String toDbColumnName;
	private boolean cardinalityManyFlag;
	private boolean cardinalityManyToManyFlag;
	private boolean isCardinalityMany;
	private boolean isCardinalityManyToMany;
	private JsonNode filterBlob;
	public String getAttributeMetaEuPkId() {
		return attributeMetaEuPkId;
	}
	public void setAttributeMetaEuPkId(String attributeMetaEuPkId) {
		this.attributeMetaEuPkId = attributeMetaEuPkId;
	}
	public String getBtDoAttributeSpecFkId() {
		return btDoAttributeSpecFkId;
	}
	public void setBtDoAttributeSpecFkId(String btDoAttributeSpecFkId) {
		this.btDoAttributeSpecFkId = btDoAttributeSpecFkId;
	}
	public String getBaseTemplateDepFkId() {
		return baseTemplateDepFkId;
	}
	public void setBaseTemplateDepFkId(String baseTemplateDepFkId) {
		this.baseTemplateDepFkId = baseTemplateDepFkId;
	}
	public String getGrouperMetaProcessCode() {
		return grouperMetaProcessCode;
	}
	public void setGrouperMetaProcessCode(String grouperMetaProcessCode) {
		this.grouperMetaProcessCode = grouperMetaProcessCode;
	}
	public String getMetaProcessCode() {
		return metaProcessCode;
	}
	public void setMetaProcessCode(String metaProcessCode) {
		this.metaProcessCode = metaProcessCode;
	}
	public String getBtDoSpecDepFkId() {
		return btDoSpecDepFkId;
	}
	public void setBtDoSpecDepFkId(String btDoSpecDepFkId) {
		this.btDoSpecDepFkId = btDoSpecDepFkId;
	}
	public String getDomainObjectFkId() {
		return domainObjectFkId;
	}
	public void setDomainObjectFkId(String domainObjectFkId) {
		this.domainObjectFkId = domainObjectFkId;
	}
	public String getDbTableName() {
		return dbTableName;
	}
	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}
	public String getDoAttributeFkId() {
		return doAttributeFkId;
	}
	public void setDoAttributeFkId(String doAttributeFkId) {
		this.doAttributeFkId = doAttributeFkId;
	}
	public String getMtDoAttributeSepcNameFkG11nText() {
		return mtDoAttributeSepcNameFkG11nText;
	}
	public void setMtDoAttributeSepcNameFkG11nText(String mtDoAttributeSepcNameFkG11nText) {
		this.mtDoAttributeSepcNameFkG11nText = mtDoAttributeSepcNameFkG11nText;
	}
	public String getDbColumnName() {
		return dbColumnName;
	}
	public void setDbColumnName(String dbColumnName) {
		this.dbColumnName = dbColumnName;
	}
	public String getBtDataTypeCodeFkId() {
		return btDataTypeCodeFkId;
	}
	public void setBtDataTypeCodeFkId(String btDataTypeCodeFkId) {
		this.btDataTypeCodeFkId = btDataTypeCodeFkId;
	}
	public String getBtDoAttrSpecNameG11nText() {
		return btDoAttrSpecNameG11nText;
	}
	public void setBtDoAttrSpecNameG11nText(String btDoAttrSpecNameG11nText) {
		this.btDoAttrSpecNameG11nText = btDoAttrSpecNameG11nText;
	}
	public JsonNode getAttrDisplayNameG11nBlob() {
		return attrDisplayNameG11nBlob;
	}
	public void setAttrDisplayNameG11nBlob(JsonNode attrDisplayNameG11nBlob) {
		this.attrDisplayNameG11nBlob = attrDisplayNameG11nBlob;
	}
	public String getAttrIconFkId() {
		return attrIconFkId;
	}
	public void setAttrIconFkId(String attrIconFkId) {
		this.attrIconFkId = attrIconFkId;
	}
	public boolean isFunctionalPrimaryKeyFlag() {
		return functionalPrimaryKeyFlag;
	}
	public void setFunctionalPrimaryKeyFlag(boolean functionalPrimaryKeyFlag) {
		this.functionalPrimaryKeyFlag = functionalPrimaryKeyFlag;
	}
	public boolean isDbPrimaryKeyFlag() {
		return dbPrimaryKeyFlag;
	}
	public void setDbPrimaryKeyFlag(boolean dbPrimaryKeyFlag) {
		this.dbPrimaryKeyFlag = dbPrimaryKeyFlag;
	}
	public boolean isDependencyKeyFlag() {
		return dependencyKeyFlag;
	}
	public void setDependencyKeyFlag(boolean dependencyKeyFlag) {
		this.dependencyKeyFlag = dependencyKeyFlag;
	}
	public boolean isRelationFlag() {
		return relationFlag;
	}
	public void setRelationFlag(boolean relationFlag) {
		this.relationFlag = relationFlag;
	}
	public boolean isHierarchyFlag() {
		return hierarchyFlag;
	}
	public void setHierarchyFlag(boolean hierarchyFlag) {
		this.hierarchyFlag = hierarchyFlag;
	}
	public boolean isSelfJoinParentFlag() {
		return selfJoinParentFlag;
	}
	public void setSelfJoinParentFlag(boolean selfJoinParentFlag) {
		this.selfJoinParentFlag = selfJoinParentFlag;
	}
	public boolean isReportableFlag() {
		return reportableFlag;
	}
	public void setReportableFlag(boolean reportableFlag) {
		this.reportableFlag = reportableFlag;
	}
	public boolean isSubjectCountry() {
		return isSubjectCountry;
	}
	public void setSubjectCountry(boolean isSubjectCountry) {
		this.isSubjectCountry = isSubjectCountry;
	}
	public boolean isGlocalized() {
		return isGlocalized;
	}
	public void setGlocalized(boolean isGlocalized) {
		this.isGlocalized = isGlocalized;
	}
	public boolean isCountrySpecific() {
		return isCountrySpecific;
	}
	public void setCountrySpecific(boolean isCountrySpecific) {
		this.isCountrySpecific = isCountrySpecific;
	}
	public boolean isMandatory() {
		return isMandatory;
	}
	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}
	public boolean isEncrypted() {
		return isEncrypted;
	}
	public void setEncrypted(boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}
	public boolean isAnonymized() {
		return isAnonymized;
	}
	public void setAnonymized(boolean isAnonymized) {
		this.isAnonymized = isAnonymized;
	}
	public boolean isDisplayMasked() {
		return isDisplayMasked;
	}
	public void setDisplayMasked(boolean isDisplayMasked) {
		this.isDisplayMasked = isDisplayMasked;
	}
	public String getDisplayMaskCharacter() {
		return displayMaskCharacter;
	}
	public void setDisplayMaskCharacter(String displayMaskCharacter) {
		this.displayMaskCharacter = displayMaskCharacter;
	}
	public boolean isReportMasked() {
		return isReportMasked;
	}
	public void setReportMasked(boolean isReportMasked) {
		this.isReportMasked = isReportMasked;
	}
	public String getReportMaskCharcter() {
		return reportMaskCharcter;
	}
	public void setReportMaskCharcter(String reportMaskCharcter) {
		this.reportMaskCharcter = reportMaskCharcter;
	}
	public boolean isUniqueAttribute() {
		return isUniqueAttribute;
	}
	public void setUniqueAttribute(boolean isUniqueAttribute) {
		this.isUniqueAttribute = isUniqueAttribute;
	}
	public JsonNode getAttrHelpTextG11nBlob() {
		return attrHelpTextG11nBlob;
	}
	public void setAttrHelpTextG11nBlob(JsonNode attrHelpTextG11nBlob) {
		this.attrHelpTextG11nBlob = attrHelpTextG11nBlob;
	}
	public JsonNode getAttrHintTextG11nBlob() {
		return attrHintTextG11nBlob;
	}
	public void setAttrHintTextG11nBlob(JsonNode attrHintTextG11nBlob) {
		this.attrHintTextG11nBlob = attrHintTextG11nBlob;
	}
	public boolean isUseSubjectPersonTimezone() {
		return isUseSubjectPersonTimezone;
	}
	public void setUseSubjectPersonTimezone(boolean isUseSubjectPersonTimezone) {
		this.isUseSubjectPersonTimezone = isUseSubjectPersonTimezone;
	}
	public boolean isSortConfig() {
		return isSortConfig;
	}
	public void setSortConfig(boolean isSortConfig) {
		this.isSortConfig = isSortConfig;
	}
	public int getSortConfigSeq() {
		return sortConfigSeq;
	}
	public void setSortConfigSeq(int sortConfigSeq) {
		this.sortConfigSeq = sortConfigSeq;
	}
	public String getSortOrderCodeFkId() {
		return sortOrderCodeFkId;
	}
	public void setSortOrderCodeFkId(String sortOrderCodeFkId) {
		this.sortOrderCodeFkId = sortOrderCodeFkId;
	}
	public boolean isFilterConfig() {
		return isFilterConfig;
	}
	public void setFilterConfig(boolean isFilterConfig) {
		this.isFilterConfig = isFilterConfig;
	}
	public int getFilterConfigSeq() {
		return filterConfigSeq;
	}
	public void setFilterConfigSeq(int filterConfigSeq) {
		this.filterConfigSeq = filterConfigSeq;
	}
	public int getCharMaxLength() {
		return charMaxLength;
	}
	public void setCharMaxLength(int charMaxLength) {
		this.charMaxLength = charMaxLength;
	}
	public JsonNode getAttributeUnitOfMeasureG11nBlob() {
		return attributeUnitOfMeasureG11nBlob;
	}
	public void setAttributeUnitOfMeasureG11nBlob(JsonNode attributeUnitOfMeasureG11nBlob) {
		this.attributeUnitOfMeasureG11nBlob = attributeUnitOfMeasureG11nBlob;
	}
	public String getBooleanOnStateIconFkId() {
		return booleanOnStateIconFkId;
	}
	public void setBooleanOnStateIconFkId(String booleanOnStateIconFkId) {
		this.booleanOnStateIconFkId = booleanOnStateIconFkId;
	}
	public String getBooleanOffStateIconFkId() {
		return booleanOffStateIconFkId;
	}
	public void setBooleanOffStateIconFkId(String booleanOffStateIconFkId) {
		this.booleanOffStateIconFkId = booleanOffStateIconFkId;
	}
	public boolean isEnableEmoticon() {
		return isEnableEmoticon;
	}
	public void setEnableEmoticon(boolean isEnableEmoticon) {
		this.isEnableEmoticon = isEnableEmoticon;
	}
	public boolean isContextObjectField() {
		return isContextObjectField;
	}
	public void setContextObjectField(boolean isContextObjectField) {
		this.isContextObjectField = isContextObjectField;
	}
	public int getContextSeqNumber() {
		return contextSeqNumber;
	}
	public void setContextSeqNumber(int contextSeqNumber) {
		this.contextSeqNumber = contextSeqNumber;
	}
	public boolean isFilterOverride() {
		return isFilterOverride;
	}
	public void setFilterOverride(boolean isFilterOverride) {
		this.isFilterOverride = isFilterOverride;
	}
	public String getToDomainObjectFkId() {
		return toDomainObjectFkId;
	}
	public void setToDomainObjectFkId(String toDomainObjectFkId) {
		this.toDomainObjectFkId = toDomainObjectFkId;
	}
	public String getToDbTableName() {
		return toDbTableName;
	}
	public void setToDbTableName(String toDbTableName) {
		this.toDbTableName = toDbTableName;
	}
	public String getToDoAttributeFkId() {
		return toDoAttributeFkId;
	}
	public void setToDoAttributeFkId(String toDoAttributeFkId) {
		this.toDoAttributeFkId = toDoAttributeFkId;
	}
	public String getToDoAttributeNameG11nText() {
		return toDoAttributeNameG11nText;
	}
	public void setToDoAttributeNameG11nText(String toDoAttributeNameG11nText) {
		this.toDoAttributeNameG11nText = toDoAttributeNameG11nText;
	}
	public String getToDbColumnName() {
		return toDbColumnName;
	}
	public void setToDbColumnName(String toDbColumnName) {
		this.toDbColumnName = toDbColumnName;
	}
	public boolean isCardinalityMany() {
		return isCardinalityMany;
	}
	public void setCardinalityMany(boolean isCardinalityMany) {
		this.isCardinalityMany = isCardinalityMany;
	}
	public boolean isCardinalityManyToMany() {
		return isCardinalityManyToMany;
	}
	public void setCardinalityManyToMany(boolean isCardinalityManyToMany) {
		this.isCardinalityManyToMany = isCardinalityManyToMany;
	}
	public boolean isCardinalityManyFlag() {
		return cardinalityManyFlag;
	}
	public void setCardinalityManyFlag(boolean cardinalityManyFlag) {
		this.cardinalityManyFlag = cardinalityManyFlag;
	}
	public boolean isCardinalityManyToManyFlag() {
		return cardinalityManyToManyFlag;
	}
	public void setCardinalityManyToManyFlag(boolean cardinalityManyToManyFlag) {
		this.cardinalityManyToManyFlag = cardinalityManyToManyFlag;
	}
	public JsonNode getFilterBlob() {
		return filterBlob;
	}
	public void setFilterBlob(JsonNode filterBlob) {
		this.filterBlob = filterBlob;
	}
}
