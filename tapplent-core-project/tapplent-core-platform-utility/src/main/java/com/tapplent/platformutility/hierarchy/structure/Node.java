package com.tapplent.platformutility.hierarchy.structure;

import java.util.Map;
import java.util.Objects;

/**
 * Created by Manas on 8/11/16.
 */
public class Node {
    private Map<String, Object> row;
    private int right;
    private int left;
    private int level;

    public Node(Map<String, Object> row, int level){
        this.row = row;
        this.level = level;
    }

    public Map<String, Object> getRow() {
        return row;
    }

    public void setRow(Map<String, Object> row) {
        this.row = row;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }
}
