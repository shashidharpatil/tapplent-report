package com.tapplent.platformutility.layout.valueObject;

public class ActionStandAlonePropertiesVO {
	private String actionStandalonePropertyId;
	private String actionStandaloneTxnId;
	private String rule;
	private String property;
	private String propertyValue;
	public String getActionStandalonePropertyId() {
		return actionStandalonePropertyId;
	}
	public void setActionStandalonePropertyId(String actionStandalonePropertyId) {
		this.actionStandalonePropertyId = actionStandalonePropertyId;
	}
	public String getActionStandaloneTxnId() {
		return actionStandaloneTxnId;
	}
	public void setActionStandaloneTxnId(String actionStandaloneTxnId) {
		this.actionStandaloneTxnId = actionStandaloneTxnId;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	public String getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
}
