package com.tapplent.platformutility.batch.model;

public class NTFN_channelconfig {
	
	private String channelTypeCodePkId;

	private Object accountHandleLngTxt;

	private String deviceIndependentBlob;

	public String getChannelTypeCodePkId() {
		return channelTypeCodePkId;
	}

	public void setChannelTypeCodePkId(String channelTypeCodePkId) {
		this.channelTypeCodePkId = channelTypeCodePkId;
	}

	public Object getAccountHandleLngTxt() {
		return accountHandleLngTxt;
	}

	public void setAccountHandleLngTxt(Object accountHandleLngTxt) {
		this.accountHandleLngTxt = accountHandleLngTxt;
	}

	public String getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}

	public void setDeviceIndependentBlob(String deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	
	
	
	
}
