package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by tapplent on 15/11/16.
 */
public class ProcessHomePageVO {
    private String prcHomePagePkId;
    private String loggedInPersonFkId;
    private String prcHomePageRuleFkId;
    private String processTypeCodeFkId;
    private String categoryTitleG11nBigTxt;
    private String categoryIconCodeFkId;
    private String cardTypeCodeFkId;
    private String cardTitleG11nBigTxt;
    private String cardIconCodeFkId;
    private String cardImageid;
    private boolean isFrontCard;
    private boolean isCardFlippable;
    private String flipCardPrcHomePageFkId;
    private int displaySeqNumPosInt;
    private String resolvedTargetScreenInstanceFkId;
    private String resolvedTargetScreenSectionInstanceFkId;
    private int resolvedTargetTabSeqNumPosInt;
    private String dynamicActionFilterExpn;
    @JsonIgnore
    private boolean isDeleted;
    @JsonIgnore
    private String lastModifiedDatetime;

//    private String sectionTitleG11NBigTxt;
//    private String sectionIconCodeFkId;
//    private String cardControlTypeCodeFkId;
//    private String controlDisplayAttributePathExpn;
//    private String contentValueDirectSingularG11NBigTxt;
//    private String contentValueDirectPluralG11NBigTxt;


    public String getPrcHomePagePkId() {
        return prcHomePagePkId;
    }

    public void setPrcHomePagePkId(String prcHomePagePkId) {
        this.prcHomePagePkId = prcHomePagePkId;
    }

    public String getLoggedInPersonFkId() {
        return loggedInPersonFkId;
    }

    public void setLoggedInPersonFkId(String loggedInPersonFkId) {
        this.loggedInPersonFkId = loggedInPersonFkId;
    }

    public String getPrcHomePageRuleFkId() {
        return prcHomePageRuleFkId;
    }

    public void setPrcHomePageRuleFkId(String prcHomePageRuleFkId) {
        this.prcHomePageRuleFkId = prcHomePageRuleFkId;
    }

    public String getProcessTypeCodeFkId() {
        return processTypeCodeFkId;
    }

    public void setProcessTypeCodeFkId(String processTypeCodeFkId) {
        this.processTypeCodeFkId = processTypeCodeFkId;
    }

    public String getCategoryTitleG11nBigTxt() {
        return categoryTitleG11nBigTxt;
    }

    public void setCategoryTitleG11nBigTxt(String categoryTitleG11nBigTxt) {
        this.categoryTitleG11nBigTxt = categoryTitleG11nBigTxt;
    }

    public String getCategoryIconCodeFkId() {
        return categoryIconCodeFkId;
    }

    public void setCategoryIconCodeFkId(String categoryIconCodeFkId) {
        this.categoryIconCodeFkId = categoryIconCodeFkId;
    }

    public String getCardTypeCodeFkId() {
        return cardTypeCodeFkId;
    }

    public void setCardTypeCodeFkId(String cardTypeCodeFkId) {
        this.cardTypeCodeFkId = cardTypeCodeFkId;
    }

    public String getCardTitleG11nBigTxt() {
        return cardTitleG11nBigTxt;
    }

    public void setCardTitleG11nBigTxt(String cardTitleG11nBigTxt) {
        this.cardTitleG11nBigTxt = cardTitleG11nBigTxt;
    }

    public String getCardIconCodeFkId() {
        return cardIconCodeFkId;
    }

    public void setCardIconCodeFkId(String cardIconCodeFkId) {
        this.cardIconCodeFkId = cardIconCodeFkId;
    }

    public String getCardImageid() {
        return cardImageid;
    }

    public void setCardImageid(String cardImageid) {
        this.cardImageid = cardImageid;
    }

    public boolean isCardFlippable() {
        return isCardFlippable;
    }

    public void setCardFlippable(boolean cardFlippable) {
        isCardFlippable = cardFlippable;
    }

    public String getFlipCardPrcHomePageFkId() {
        return flipCardPrcHomePageFkId;
    }

    public void setFlipCardPrcHomePageFkId(String flipCardPrcHomePageFkId) {
        this.flipCardPrcHomePageFkId = flipCardPrcHomePageFkId;
    }

    public int getDisplaySeqNumPosInt() {
        return displaySeqNumPosInt;
    }

    public void setDisplaySeqNumPosInt(int displaySeqNumPosInt) {
        this.displaySeqNumPosInt = displaySeqNumPosInt;
    }

    public String getResolvedTargetScreenInstanceFkId() {
        return resolvedTargetScreenInstanceFkId;
    }

    public void setResolvedTargetScreenInstanceFkId(String resolvedTargetScreenInstanceFkId) {
        this.resolvedTargetScreenInstanceFkId = resolvedTargetScreenInstanceFkId;
    }

    public String getResolvedTargetScreenSectionInstanceFkId() {
        return resolvedTargetScreenSectionInstanceFkId;
    }

    public void setResolvedTargetScreenSectionInstanceFkId(String resolvedTargetScreenSectionInstanceFkId) {
        this.resolvedTargetScreenSectionInstanceFkId = resolvedTargetScreenSectionInstanceFkId;
    }

    public int getResolvedTargetTabSeqNumPosInt() {
        return resolvedTargetTabSeqNumPosInt;
    }

    public void setResolvedTargetTabSeqNumPosInt(int resolvedTargetTabSeqNumPosInt) {
        this.resolvedTargetTabSeqNumPosInt = resolvedTargetTabSeqNumPosInt;
    }

    public String getDynamicActionFilterExpn() {
        return dynamicActionFilterExpn;
    }

    public void setDynamicActionFilterExpn(String dynamicActionFilterExpn) {
        this.dynamicActionFilterExpn = dynamicActionFilterExpn;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getLastModifiedDatetime() {
        return lastModifiedDatetime;
    }

    public void setLastModifiedDatetime(String lastModifiedDatetime) {
        this.lastModifiedDatetime = lastModifiedDatetime;
    }

    public boolean isFrontCard() {
        return isFrontCard;
    }

    public void setFrontCard(boolean frontCard) {
        isFrontCard = frontCard;
    }
}
