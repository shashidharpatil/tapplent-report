package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 15/11/16.
 */
public class ProcessHomePageFooterVO {
    private String PrcFooterContentPkId;
    private String LoggedInPersonPkId;
    private String PrcFooterContentRuleFkId;
    private String ProcessTypeCodeFkId;
    private String FooterTextG11NBigTxt;
    private boolean IsDeleted;
    private String LastModifiedDatetime;

    public String getPrcFooterContentPkId() {
        return PrcFooterContentPkId;
    }

    public void setPrcFooterContentPkId(String prcFooterContentPkId) {
        PrcFooterContentPkId = prcFooterContentPkId;
    }

    public String getLoggedInPersonPkId() {
        return LoggedInPersonPkId;
    }

    public void setLoggedInPersonPkId(String loggedInPersonPkId) {
        LoggedInPersonPkId = loggedInPersonPkId;
    }

    public String getPrcFooterContentRuleFkId() {
        return PrcFooterContentRuleFkId;
    }

    public void setPrcFooterContentRuleFkId(String prcFooterContentRuleFkId) {
        PrcFooterContentRuleFkId = prcFooterContentRuleFkId;
    }

    public String getProcessTypeCodeFkId() {
        return ProcessTypeCodeFkId;
    }

    public void setProcessTypeCodeFkId(String processTypeCodeFkId) {
        ProcessTypeCodeFkId = processTypeCodeFkId;
    }

    public String getFooterTextG11NBigTxt() {
        return FooterTextG11NBigTxt;
    }

    public void setFooterTextG11NBigTxt(String footerTextG11NBigTxt) {
        FooterTextG11NBigTxt = footerTextG11NBigTxt;
    }

    public boolean isDeleted() {
        return IsDeleted;
    }

    public void setDeleted(boolean deleted) {
        IsDeleted = deleted;
    }

    public String getLastModifiedDatetime() {
        return LastModifiedDatetime;
    }

    public void setLastModifiedDatetime(String lastModifiedDatetime) {
        LastModifiedDatetime = lastModifiedDatetime;
    }
}
