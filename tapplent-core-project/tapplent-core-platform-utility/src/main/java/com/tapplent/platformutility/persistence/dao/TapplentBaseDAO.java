package com.tapplent.platformutility.persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.tapplent.platformutility.common.util.Util;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

public class TapplentBaseDAO extends SqlSessionDaoSupport{
	
	private static final Logger LOG = LogFactory.getLogger(TapplentBaseDAO.class);
//	DataSourceTransactionManager transactionManager;
	
	public Connection getConnection() throws SQLException{
		final Logger LOG = LogFactory.getLogger(TapplentBaseDAO.class);
		Connection conn = getSqlSession().getConnection();//DataSourceUtils.getConnection(transactionManager.getDataSource());//this.getSqlSession().getConnection();//
		conn.setCatalog(TenantContextHolder.getCurrentTenantSchema());
		LOG.debug("fetching data from schema {} " + TenantContextHolder.getCurrentTenantSchema());
		return conn;
	}
	
	protected ResultSet executeSQL(String SQL, List<Object> parameters){
		ResultSet result = null;
		try {
			Connection con = null;
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(SQL);
			LOG.debug(SQL);
			for (int parameterIndex =0; parameterIndex<parameters.size(); parameterIndex++){
				ps.setObject(parameterIndex+1, parameters.get(parameterIndex));
				if(parameters.get(parameterIndex) instanceof byte[]){
					LOG.debug("parameter at index {"+parameterIndex+1+"} => {"+ Util.convertByteToString((byte[])parameters.get(parameterIndex))+"}");
				}else{
					LOG.debug("parameter at index {"+parameterIndex+1+"} => {"+parameters.get(parameterIndex)+"}");
				}

			}
			result = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public int executeUpdate(String SQL, List<Object> parameters) throws SQLException {
		try {
			Connection con = null;
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(SQL);
			LOG.debug(SQL);
			if(parameters!=null){
				for (int parameterIndex =0; parameterIndex<parameters.size(); parameterIndex++){
					LOG.debug("parameter at index "+parameterIndex+1 +" is "+parameters.get(parameterIndex));
					ps.setObject(parameterIndex+1, parameters.get(parameterIndex));
				}
			}
			int count = ps.executeUpdate();
			LOG.debug("$$$    "+ count +"   $$$ number of records inserted or updated");
			return count;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
	}
//	public DataSourceTransactionManager getTransactionManager() {
//		return transactionManager;
//	}
//
//	public void setTransactionManager(DataSourceTransactionManager transactionManager) {
//		this.transactionManager = transactionManager;
//	}
}
