package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.helper.DG_ConvertURLtoDocType;
import com.tapplent.platformutility.batch.helper.DG_DocGenHelper;
import com.tapplent.platformutility.batch.helper.DG_PPTGen;
import com.tapplent.platformutility.batch.model.DG_DocGenData;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterStep;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;



public class DocGenProcessor implements ItemProcessor<DG_DocGenData, DG_DocGenData> {
	private String schedulerInstancePkId;

	
	@BeforeStep
	public void beforeStep(StepExecution stepExecution) {
		JobExecution jobExecution = stepExecution.getJobExecution();
		schedulerInstancePkId = jobExecution.getJobParameters().getString(Constants.schedulerInstancePkId);

	}

	public DG_DocGenData process(DG_DocGenData item) throws Exception {
		item.setSchedulerInstanceId(schedulerInstancePkId);
		// TODO Auto-generated method stub
		DG_DocGenHelper dg_docGenHelper = new DG_DocGenHelper();
		if (item.getDocTypeCodeFkId().equals(Constants.pdf)) {

			String s3link = DG_ConvertURLtoDocType.generatePDF(item, dg_docGenHelper.getPassword(item));
			item.setFileAttach(s3link);
		} else if (item.getDocTypeCodeFkId().equals(Constants.ppt)) {

			DG_PPTGen pptGen = new DG_PPTGen(dg_docGenHelper.getPPTemplate(item), dg_docGenHelper.getPPTData(item),
					dg_docGenHelper.getPPTDoaMap(item));
			item.setFileAttach(pptGen.s3Link);

		} else if (item.getDocTypeCodeFkId().equals(Constants.word)) {

			String s3link = DG_ConvertURLtoDocType.generateWord(item);
			item.setFileAttach(s3link);
		} else if (item.getDocTypeCodeFkId().equals(Constants.jpeg)) {

			String s3link = DG_ConvertURLtoDocType.generateJPG(item);
			item.setFileAttach(s3link);
		} else if (item.getDocTypeCodeFkId().equals(Constants.csv)) {

			// (TDWI) same as analytics

		}

	
		
		 if(item.isSuccess()==true){
	

		return item;
		
		  } else{  throw new
		  BatchException();}
		 
	}

}
