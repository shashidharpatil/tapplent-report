package com.tapplent.platformutility.system;

import java.util.Map;
import java.util.Properties;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.tapplent.platformutility.g11n.structure.OEMObject;

public class SystemServiceImpl implements SystemService {

	SystemDAO systemDAO;

	@Override
	public ChannelSftp getSftpServerConnection() {
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try{
			JSch jSch = new JSch();
			jSch.addIdentity("/Users/tapplent/Downloads/phase1.pem");
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
//			session = jSch.getSession("bitnami", "ec2-54-172-15-165.compute-1.amazonaws.com", 22); //old address
			session = jSch.getSession("ubuntu", "ec2-52-90-57-62.compute-1.amazonaws.com", 22);
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp)channel;
		}
		catch(JSchException e){
			e.printStackTrace();
		}
		return channelSftp;
	}

	@Override
	public AmazonS3 getAmazonS3Client() {
		ClientConfiguration config = new ClientConfiguration();
		config.setSocketTimeout(0);//FIXME infinite timeout is not recommended.
		AmazonS3 s3Client = new AmazonS3Client(new AmazonS3CredentialsProvider(), config);
		return s3Client;
	}

	@Override
	public Map<String, OEMObject> getOemObjectMap() {
		Map<String, OEMObject> oemObjectMap = systemDAO.getOemObjectMap();
		return oemObjectMap;
	}

	@Override
	public Map<String, Map<String, String>> getOemToLocaleLangMap() {
		return systemDAO.getOemToLocaleLangMap();
	}

	public SystemDAO getSystemDAO() {
		return systemDAO;
	}

	public void setSystemDAO(SystemDAO systemDAO) {
		this.systemDAO = systemDAO;
	}
}
