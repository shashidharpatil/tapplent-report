package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class ControlGroupByControlVO {

	private String controlGroupByControlId;
	private String baseTemplateId;
	private String mtPE;
	private String canvasId;
	private String controlId;
	private String groupByControlId;
	private int seqNumber;
	private JsonNode deviceIndependentBlob;
	private JsonNode deviceDependentBlob;
	public String getControlGroupByControlId() {
		return controlGroupByControlId;
	}
	public void setControlGroupByControlId(String controlGroupByControlId) {
		this.controlGroupByControlId = controlGroupByControlId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPE() {
		return mtPE;
	}
	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public String getControlId() {
		return controlId;
	}
	public void setControlId(String controlId) {
		this.controlId = controlId;
	}
	public String getGroupByControlId() {
		return groupByControlId;
	}
	public void setGroupByControlId(String groupByControlId) {
		this.groupByControlId = groupByControlId;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public int getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}
	
}
