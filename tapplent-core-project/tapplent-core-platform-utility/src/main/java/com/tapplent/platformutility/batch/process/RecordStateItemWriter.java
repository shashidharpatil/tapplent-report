package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.POST_PostData;
import com.tapplent.platformutility.batch.model.RCRD_STATE_MtDOData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class RecordStateItemWriter extends JdbcBatchItemWriter<RCRD_STATE_MtDOData> {

    private ItemWriter<RCRD_STATE_MtDOData> currentStateWriter;
    /*private ItemWriter<RCRD_STATE_MtDOData> futureStateWriter;
    private ItemWriter<RCRD_STATE_MtDOData> historyStateWriter;*/

    public ItemWriter<RCRD_STATE_MtDOData> getCurrentStateWriter() {
        return currentStateWriter;
    }

    public void setCurrentStateWriter(ItemWriter<RCRD_STATE_MtDOData> currentStateWriter) {
        this.currentStateWriter = currentStateWriter;
    }

  /*  public ItemWriter<RCRD_STATE_MtDOData> getFutureStateWriter() {
        return futureStateWriter;
    }

    public void setFutureStateWriter(ItemWriter<RCRD_STATE_MtDOData> futureStateWriter) {
        this.futureStateWriter = futureStateWriter;
    }

    public ItemWriter<RCRD_STATE_MtDOData> getHistoryStateWriter() {
        return historyStateWriter;
    }

    public void setHistoryStateWriter(ItemWriter<RCRD_STATE_MtDOData> historyStateWriter) {
        this.historyStateWriter = historyStateWriter;
    }*/

    public RecordStateItemWriter(){
        super();
        DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
        setDataSource(ds);
        setSql("select 10");
        setItemPreparedStatementSetter(new ItemPreparedStatementSetter<RCRD_STATE_MtDOData>() {
            @Override
            public void setValues(RCRD_STATE_MtDOData item, PreparedStatement ps) throws SQLException {

            }
        });
        setAssertUpdates(false);

        setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<RCRD_STATE_MtDOData>());
//        afterPropertiesSet();
    }

    @Transactional(readOnly = false, propagation = Propagation.NOT_SUPPORTED)
    public void write(List<? extends RCRD_STATE_MtDOData> items) throws Exception
    {
        currentStateWriter.write(items);
       /* futureStateWriter.write(items);
        historyStateWriter.write(items);*/
    }
}
