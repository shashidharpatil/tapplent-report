package com.tapplent.platformutility.common.cache;

import java.sql.SQLException;
import java.util.*;

import java.util.concurrent.ConcurrentHashMap;

import com.tapplent.platformutility.accessmanager.structure.RowCondition;
import com.tapplent.platformutility.activityLog.AuditMsgFormat;
import com.tapplent.platformutility.common.util.RequestSelectType;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.common.util.valueObject.AnalyticsDOAControlMap;
import com.tapplent.platformutility.common.util.valueObject.NameCalculationRuleVO;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.layout.valueObject.LicencedLocalesVO;
import com.tapplent.platformutility.metadata.MetadataService;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.uilayout.valueobject.LayoutThemeMapper;
import org.springframework.beans.BeanUtils;

public class MetadataObjectRepository {
	/* TODO a DO to <MTPE> Map */
	private final Map<String, EntityMetadataVOX> btPEToEntityMetaMap = new ConcurrentHashMap<>();
	private final Map<String, Map<String, EntityMetadataVOX>> btMtPEToEntityMetaMap = new ConcurrentHashMap<>();
	private final Map<String, Map<String, EntityMetadataVOX>> btDoToEntityMetaMap = new ConcurrentHashMap<>();
	private final Map<String, Map<String, AuditMsgFormat>> mtPEActionToAuditMsgFormatMap = new ConcurrentHashMap<>();
	private final Map<String, MasterEntityMetadataVOX> doToMasterEntityMetaMap = new ConcurrentHashMap<>();
	private final Map<String, MasterEntityMetadataVOX> mtPEToMasterEntityMetaMap = new ConcurrentHashMap<>();
	private final Map<String, Map<String, Map<Boolean,Map<String,List<DataActions>>>>> asSsIsdelWstatToActionList = new HashMap<>();
	private final Map<String, IconVO> iconMap = new HashMap<>();
    private final Map<String, MTPE> mtPEMap = new HashMap<>();
    private final Map<String, PropertyControlMaster> propertyControlMap = new HashMap<>();
    private final Map<String, Map<String, Map<String, Map<String, Integer>>>> scrSecActActgroupThemeMapper= new HashMap<>();
    private final List<LicencedLocalesVO> licencedLocales = new ArrayList<>();
    private final Map<String, List<RowCondition>> mtPEToRowConditionsMap = new ConcurrentHashMap<>();
    private final Map<String, Integer> labelToAliasMap = new ConcurrentHashMap<>();
    private final Map<String, Map<String, NameCalculationRuleVO>> localeNameTypeToNameCalMap = new HashMap<>();
    private final List<AnalyticsDOAControlMap> analyticsDOAControlMaps = new ArrayList<>();
    public static Integer currentMaxAlias;
    private UiSetting uiSetting = new UiSetting();
	private MetadataService metadataService;

	public MetadataService getMetadataService() {
		return metadataService;
	}

	public void setMetadataService(MetadataService metadataService) {
		this.metadataService = metadataService;
	}
	//TODO remove this. licenced locale is getting updated multiple times.
	public List<LicencedLocalesVO> getLicencedLocaleDirectlyFromDB(){
		return metadataService.getLicencedLocale();
	}
	public List<LicencedLocalesVO> getLicencedLocales() {
		if(licencedLocales.isEmpty()){
			List<LicencedLocalesVO> licencedLocalesVOS = metadataService.getLicencedLocale();
			licencedLocales.addAll(licencedLocalesVOS);
		}
		return licencedLocales;
	}

	public String getTenantDefaultLocale(){
		if(licencedLocales.isEmpty()){
			List<LicencedLocalesVO> licencedLocalesVOS = metadataService.getLicencedLocale();
			licencedLocales.addAll(licencedLocalesVOS);
		}
		for(LicencedLocalesVO licencedLocalesVO : licencedLocales){
			if(licencedLocalesVO.isGlobalDefault())
				return licencedLocalesVO.getLocale();
		}
		return null;
	}

	public EntityMetadataVOX getMetadataByProcessElement(String btPE){
		return getMetadataByProcessElement(null, null, btPE);
	}
	
	public EntityMetadataVOX getMetadataByProcessElement(String baseTemplateId, String mtPE) {
		return getMetadataByProcessElement(baseTemplateId, mtPE, null);
	}

	public IconVO getIconDetailsByIconCode(String iconCode){
		if(!iconMap.isEmpty()){
			return iconMap.get(iconCode);
		}else{
            iconMap.putAll(metadataService.getIconMap());
			return iconMap.get(iconCode);
		}
	}

    public MTPE getMTPE(String mtPE) {
        if(!mtPEMap.isEmpty()){
            return mtPEMap.get(mtPE);
        }else{
            mtPEMap.putAll(metadataService.getMtPEMap());
            return mtPEMap.get(mtPE);
        }
    }
	
	public EntityMetadataVOX getMetadataByProcessElement(String baseTemplateId, String mtPE, String btPE) {
		if(btPE!=null && !btPE.isEmpty()){
			/* If the call is made by BT-PE */
			EntityMetadataVOX meta = btPEToEntityMetaMap.get(btPE);
			/* If this BT-PE meta is not present */
			if(meta == null){
				EntityMetadataVOX m = populateMetaForDomainObject(null, null, btPE);
				btPEToEntityMetaMap.putIfAbsent(btPE, m);
				updateBtMtPEToEntityMetaMap(m);
				updateBtDoToEntityMetaMap(m);
				return m;
			}else{
				return meta;
			}
		}
		else if(mtPE != null ){
			if (baseTemplateId == null){
				String defalutBt = getDefaultBtForMtPE(mtPE);
				baseTemplateId = defalutBt;
			}
			/* If the call is by base template and MT-PE */
			if(baseTemplateId != null) {
				if (btMtPEToEntityMetaMap.get(baseTemplateId) != null) {
					EntityMetadataVOX meta = btMtPEToEntityMetaMap.get(baseTemplateId).get(mtPE);
					if (meta == null) {
						EntityMetadataVOX m = populateMetaForDomainObject(baseTemplateId, mtPE, null);
						btPE = m.getBtPE();
						btPEToEntityMetaMap.putIfAbsent(btPE, m);
						updateBtMtPEToEntityMetaMap(m);
						updateBtDoToEntityMetaMap(m);
						return m;
					} else {
						return meta;
					}
				} else {
					EntityMetadataVOX m = populateMetaForDomainObject(baseTemplateId, mtPE, null);
					Map<String, EntityMetadataVOX> mtPEToMetaMap = new HashMap<>();
					mtPEToMetaMap.put(mtPE, m);
					btMtPEToEntityMetaMap.put(baseTemplateId, mtPEToMetaMap);
					btPEToEntityMetaMap.putIfAbsent(m.getBtPE(), m);
					return m;
				}
			}
		}
		return null;
	}

	private String getDefaultBtForMtPE(String mtPE) {
		TemplateMapper templateMapper = metadataService.getTemaplateMapperByMtPE(mtPE);
		return  templateMapper.getBaseTemplateCodeFkId();
	}

	public MasterEntityMetadataVOX getMetadataByMtPE(String mtPE){
		MasterEntityMetadataVOX meta = mtPEToMasterEntityMetaMap.get(mtPE);
		if(meta == null){
			MasterEntityMetadataVOX m = populateMetaFromMTPE(mtPE);
			String doCode = m.getDomainObjectCode();
			doToMasterEntityMetaMap.putIfAbsent(doCode, m);
			mtPEToMasterEntityMetaMap.putIfAbsent(mtPE, m);
			return m;
		}
		return meta;
	}

	public MasterEntityAttributeMetadata getMetadataByDoa(String doaName){
		String[] splits = doaName.split("\\.");
		String doName = splits[0];
		MasterEntityMetadataVOX meta = doToMasterEntityMetaMap.get(doName);
		if(meta == null){
			MasterEntityMetadataVOX m = populateMetaForDomainObject(doName);
			doToMasterEntityMetaMap.putIfAbsent(doName, m);
			return m.getAttributeByName(doaName);
		}else {
			return meta.getAttributeNameMap().get(doaName);
		}
	}
	
	public MasterEntityMetadataVOX getMetadataByDo(String doName){
		MasterEntityMetadataVOX meta = doToMasterEntityMetaMap.get(doName);
		if(meta == null){
			MasterEntityMetadataVOX m = populateMetaForDomainObject(doName);
			doToMasterEntityMetaMap.putIfAbsent(doName, m);
			return m;
		}
		return meta;
	}
	
	public EntityMetadataVOX getMetadataByBtDo(String bt, String doName){
		if(btDoToEntityMetaMap.get(bt) != null){
			EntityMetadataVOX meta = btDoToEntityMetaMap.get(bt).get(doName);
			if(meta == null){
				EntityMetadataVOX m = populateMetaForDomainObject(bt, doName);
				btDoToEntityMetaMap.get(bt).putIfAbsent(doName, m);
				updateBtMtPEToEntityMetaMap(m);
				return m;
			}else{
				return meta;
			}
		}
		else{
			EntityMetadataVOX m = populateMetaForDomainObject(bt, doName);
			Map<String, EntityMetadataVOX> doToMetaMap = new HashMap<>();
			doToMetaMap.put(doName, m);
			btDoToEntityMetaMap.put(bt, doToMetaMap);
			updateBtMtPEToEntityMetaMap(m);
			return m;
		}
	}
	
	public EntityAttributeMetadata getMetadataByBtDoa(String bt, String doaName){
		String[] splits = doaName.split("\\.");
		String doName = splits[0];
		EntityAttributeMetadata entityAttributeMetadata = getMetadataByBtDo(bt, doName).getAttributeByName(doaName);
		return entityAttributeMetadata;
	}

	public EntityAttributeMetadata getMetadataByBtDoa(String btDoaName){
		String[] splits = btDoaName.split("\\.");
		String bt = splits[0];
		String doName = splits[1];
		String doaName = splits[1]+"."+splits[2];
		EntityAttributeMetadata entityAttributeMetadata = getMetadataByBtDo(bt, doName).getAttributeByName(doaName);
		return entityAttributeMetadata;
	}
	
	public List<EntityAttributeMetadata> getBtAttributeMetaListByMtDoa(String doaCodeFkId){
		List<EntityAttributeMetadata> metaList = metadataService.getBtAttributeMetaListByMtDoa(doaCodeFkId);
		return metaList;
	}
	
	private EntityMetadataVOX populateMetaForDomainObject(String baseTemplateId, String mtPE, String btPE) {
		EntityMetadata meta = metadataService.getMetadataByProcessElement(baseTemplateId, mtPE, btPE);
		return EntityMetadataVOX.create(meta);
	}
	
	private MasterEntityMetadataVOX populateMetaForDomainObject(String doName){
		MasterEntityMetadata meta = metadataService.getMetadataByDomainObject(doName);
		return MasterEntityMetadataVOX.create(meta);
	}
	
	private EntityMetadataVOX populateMetaForDomainObject(String bt, String doName){
		EntityMetadata meta = metadataService.getMetadataByBtDo(bt, doName);
		return EntityMetadataVOX.create(meta);
	}
	
	/**
	 * @param mtPE
	 * @return
	 */
	private MasterEntityMetadataVOX populateMetaFromMTPE(String mtPE) {
		MasterEntityMetadata meta = metadataService.getMetadataByMTPE(mtPE);
		return MasterEntityMetadataVOX.create(meta);
	}
	
	private void updateBtDoToEntityMetaMap(EntityMetadataVOX m) {
		if(btDoToEntityMetaMap.get(m.getBaseTemplateId()) != null)
			btDoToEntityMetaMap.get(m.getBaseTemplateId()).putIfAbsent(m.getDomainObjectCode(), m);
		else{
			Map<String, EntityMetadataVOX> doToMetaMap = new HashMap<>();
			btDoToEntityMetaMap.put(m.getBaseTemplateId(), doToMetaMap);
		}
	}

	private void updateBtMtPEToEntityMetaMap(EntityMetadataVOX m) {
		if(btMtPEToEntityMetaMap.get(m.getBaseTemplateId()) != null)
			btMtPEToEntityMetaMap.get(m.getBaseTemplateId()).putIfAbsent(m.getMtPE(), m);
		else{
			Map<String, EntityMetadataVOX> mtPEToMetaMap = new HashMap<>();
			btMtPEToEntityMetaMap.put(m.getBaseTemplateId(), mtPEToMetaMap);
		}
	}
	
	public AttributePaths getAttributePathsForPE(String baseTemplateId, String mtPE, RequestSelectType selectType){
		EntityMetadataVOX metadata = getMetadataByProcessElement(baseTemplateId, mtPE);
		AttributePaths attributePaths = metadata.getAttributePaths(selectType);
		if(attributePaths == null){
			attributePaths = metadataService.getAttributePath(baseTemplateId, mtPE, selectType);
			metadata.setAttributePaths(selectType, attributePaths);
		}
		return attributePaths;
	}

	/**
	 * @param activeStateValue
	 * @param saveStateValue
	 * @param isDeleted
	 * @param workFlowTransactionValue
	 * @return
	 */
	public List<DataActions> getDataActions(String activeStateValue, String saveStateValue,
			Boolean isDeleted, String workFlowTransactionValue) {
		if(asSsIsdelWstatToActionList.isEmpty()){
			populateAsIsdelWstatToActionList();
		}
		if(asSsIsdelWstatToActionList.containsKey(activeStateValue)){
			Map<String, Map<Boolean, Map<String, List<DataActions>>>> ssIsdelWstatToActionList = asSsIsdelWstatToActionList.get(activeStateValue);
			if(ssIsdelWstatToActionList.containsKey(saveStateValue)){
				Map<Boolean, Map<String, List<DataActions>>> isdelWstatToActionList = ssIsdelWstatToActionList.get(saveStateValue);
				if(isdelWstatToActionList.containsKey(isDeleted)){
					Map<String,List<DataActions>> wStatActionsList = isdelWstatToActionList.get(isDeleted);
					if(wStatActionsList.containsKey(workFlowTransactionValue)){
						List<DataActions> resultActionsList = wStatActionsList.get(workFlowTransactionValue);
						return resultActionsList;
					}
				}
			}
		}
		return null;
	}

	/**
	 * 
	 */
	private void populateAsIsdelWstatToActionList() {
		List<DataActions> dataActionsList = metadataService.getDataActionsList();
		for(DataActions dataAction : dataActionsList){
			String activeState = dataAction.getActiveState();
			if(asSsIsdelWstatToActionList.containsKey(activeState)){
				Map<String, Map<Boolean, Map<String, List<DataActions>>>> ssIsdelWstatToActionList = asSsIsdelWstatToActionList.get(activeState);
				String saveState = dataAction.getSaveState();
				if(ssIsdelWstatToActionList.containsKey(saveState)){
					Map<Boolean, Map<String, List<DataActions>>> isdelWstatToActionList = ssIsdelWstatToActionList.get(saveState);
					Boolean isDeleted = dataAction.isDeleted();
					if(isdelWstatToActionList.containsKey(isDeleted)){
						String wStatCode = dataAction.getWorkFlowTransactionStatusCode();
						Map<String,List<DataActions>> wStatActionsList = isdelWstatToActionList.get(isDeleted);
						if(wStatActionsList.containsKey(wStatCode)){
							List<DataActions> resultActionsList = wStatActionsList.get(wStatCode);
							resultActionsList.add(dataAction);
						}else{
							List<DataActions> resultActionsList = new ArrayList<>();
							resultActionsList.add(dataAction);
							wStatActionsList.put(wStatCode, resultActionsList);
						}
					}else{
						List<DataActions> resultActionsList = new ArrayList<>();
						resultActionsList.add(dataAction);
						Map<String, List<DataActions>> wStatActionsList = new HashMap<>();
						String wStatCode = dataAction.getWorkFlowTransactionStatusCode();
						wStatActionsList.put(wStatCode, resultActionsList);
						isdelWstatToActionList.put(isDeleted, wStatActionsList);
					}
				}else{
					List<DataActions> resultActionsList = new ArrayList<>();
					resultActionsList.add(dataAction);
					Map<String, List<DataActions>> wStatActionsList = new HashMap<>();
					String wStatCode = dataAction.getWorkFlowTransactionStatusCode();
					wStatActionsList.put(wStatCode, resultActionsList);
					Map<Boolean, Map<String, List<DataActions>>> isdelWstatToActionList = new HashMap<>();
					Boolean isDeleted = dataAction.isDeleted();
					isdelWstatToActionList.put(isDeleted, wStatActionsList);
					ssIsdelWstatToActionList.put(saveState, isdelWstatToActionList);
				}
			}else{
				List<DataActions> resultActionsList = new ArrayList<>();
				resultActionsList.add(dataAction);
				Map<String, List<DataActions>> wStatActionsList = new HashMap<>();
				String wStatCode = dataAction.getWorkFlowTransactionStatusCode();
				wStatActionsList.put(wStatCode, resultActionsList);
				Map<Boolean, Map<String, List<DataActions>>> isdelWstatToActionList = new HashMap<>();
				Boolean isDeleted = dataAction.isDeleted();
				isdelWstatToActionList.put(isDeleted, wStatActionsList);
				String saveState = dataAction.getSaveState();
				Map<String, Map<Boolean, Map<String, List<DataActions>>>> ssIsdelWstatToActionList = new HashMap<>();
				ssIsdelWstatToActionList.put(saveState, isdelWstatToActionList);
				asSsIsdelWstatToActionList.put(activeState, ssIsdelWstatToActionList);
			}
		}
	}

	public List<String> getDependencyKeyRelatedPEs(String mtPE) {
		return metadataService.getDependencyKeyRelatedPEs(mtPE);
	}

	public List<String> getForeignKeyRelatedPEs(String mtPE) {
		return metadataService.getForeignKeyRelatedPEs(mtPE);
	}

	public PropertyControlMaster getPropertyControlByCode(String propertyControlCode) {
		if(propertyControlMap.containsKey(propertyControlCode)){
			return propertyControlMap.get(propertyControlCode);
		}else{
			Map<String, PropertyControlMaster> masterMap = metadataService.getPropertyControlMap();
			return masterMap.get(propertyControlCode);
		}
	}

	public Map<String, PropertyControlMaster> getPropertyControlMap() {
		if(propertyControlMap.isEmpty()){
			return metadataService.getPropertyControlMap();
		}else{
			return propertyControlMap;
		}
	}

	public CountrySpecificDOAMeta getCountrySpecificDOAMeta(String baseTemplate, String doa, String baseCountryCodeFkId) {
		EntityAttributeMetadata entityAttributeMetadata = getMetadataByBtDoa(baseTemplate, doa);
		return metadataService.getCountrySpecificDOAMeta(entityAttributeMetadata.getBtDoSpecId(), baseCountryCodeFkId);
	}

    public AuditMsgFormat getAuditMsgFormat(String activityLogActionCode, String mtPE) {
		if(mtPEActionToAuditMsgFormatMap.get(mtPE) != null){
			AuditMsgFormat auditMsgFormat = mtPEActionToAuditMsgFormatMap.get(mtPE).get(activityLogActionCode);
			if(auditMsgFormat == null){
				auditMsgFormat = metadataService.getAuditMsgFormatByMtPEAndAction(activityLogActionCode, mtPE);
			}
			mtPEActionToAuditMsgFormatMap.get(mtPE).put(activityLogActionCode, auditMsgFormat);
			return auditMsgFormat;
		}
		else {
			AuditMsgFormat auditMsgFormat = metadataService.getAuditMsgFormatByMtPEAndAction(activityLogActionCode, mtPE);
			Map<String, AuditMsgFormat> actionToAuditMsgFormatMap = new HashMap<>();
			actionToAuditMsgFormatMap.put(activityLogActionCode, auditMsgFormat);
			mtPEActionToAuditMsgFormatMap.put(mtPE, actionToAuditMsgFormatMap);
			return auditMsgFormat;
		}
    }

	public String getSubjectUser(String subjectUserDrivenDO, String pk) {
		pk = (String) Util.remove0x(pk);
		String subjectUserId = null;
		MasterEntityMetadataVOX helperVO = getMetadataByDo(subjectUserDrivenDO);
		String subjectUserDOA = helperVO.getSubjectPersonDoaFullPath().substring(2, helperVO.getSubjectPersonDoaFullPath().length()-1);
		StringBuilder SQL = new StringBuilder("SELECT ");
		StringBuilder FROMSQL = new StringBuilder(" FROM ");
		StringBuilder WHERESQL = new StringBuilder(" WHERE ");
		// get the meta data of all the doas.

		// Now get the actual column
		String[] doas = subjectUserDOA.split(":");
		MasterEntityAttributeMetadata previousDOADetails = null;
		for(int i = 0 ;i< doas.length; i++){
			MasterEntityAttributeMetadata doaDetails =  getMetadataByDoa(doas[i]);
			//This is the last doa.
			if(i == 0){
				// build here the from clause
				FROMSQL.append(doaDetails.getDbTableName()).append(" ");
				previousDOADetails = doaDetails;
				WHERESQL.append(helperVO.getDbTableName()+"."+helperVO.getFunctionalPrimaryKeyAttribute().getDbColumnName()).append("=");
				if(helperVO.getFunctionalPrimaryKeyAttribute().getDbDataTypeCode().equals("T_ID")){
					WHERESQL.append("0x").append(pk);
				}else{
					WHERESQL.append("'"+pk+"'");
				}
			}else{
				FROMSQL.append(" INNER JOIN ").append(doaDetails.getDbTableName()).append(" ON ").append(previousDOADetails.getDbTableName()).append(".").append(previousDOADetails.getDbColumnName());
				MasterEntityMetadataVOX currentDO = getMetadataByDo(doaDetails.getDomainObjectCode());
				FROMSQL.append(" = ").append(doaDetails.getDbTableName()).append(".").append(currentDO.getFunctionalPrimaryKeyAttribute().getDbColumnName());
			}
			if(i == doas.length-1) {
				SQL.append(doaDetails.getDbTableName() + "." + doaDetails.getDbColumnName());
			}
		}
		SQL.append(FROMSQL).append(WHERESQL);
		subjectUserId = metadataService.getSubjectUserId(SQL, helperVO.getFunctionalPrimaryKeyAttribute().getDbDataTypeCode());
//		subjectUserId = etlDAO.getSubjectUserId(SQL, helperVO.getPrimaryKeyDBDataType());
		return subjectUserId;
	}

    public UiSetting getUiStetting() {
		if(uiSetting.getUiSettingPkId() == null) {
			UiSetting uiSetting= metadataService.getUiSetting();
			BeanUtils.copyProperties(uiSetting, this.uiSetting);
		}
		return uiSetting;
    }
	public Integer getThemeSeqNumber(String screenCode){
		return getThemeSeqNumber(screenCode, "null", "null", "null");
	}

	public Integer getThemeSeqNumber(String screenCode, String sectionCode){
		return getThemeSeqNumber(screenCode, sectionCode, "null", "null");
	}

	public Integer getThemeSeqNumber(String screenCode, String sectionCode, String actionVisualCode){
		return getThemeSeqNumber(screenCode, sectionCode,  actionVisualCode, "null");
	}
    public Integer getThemeSeqNumber(String screenCode, String sectionCode, String actionVisualCode, String actionGroupCode){
		if(scrSecActActgroupThemeMapper.isEmpty()){
			populateThemeMap();
		}
		if(scrSecActActgroupThemeMapper.containsKey(screenCode)){
			Map<String, Map<String, Map<String, Integer>>> secActActgroupThemeMapper = scrSecActActgroupThemeMapper.get(screenCode);
			if(secActActgroupThemeMapper.containsKey(sectionCode)){
				Map<String, Map<String, Integer>> actActgroupThemeMapper = secActActgroupThemeMapper.get(sectionCode);
				if(actActgroupThemeMapper.containsKey(actionVisualCode)){
					Map<String, Integer> actgroupThemeMapper = actActgroupThemeMapper.get(actionVisualCode);
					if(actgroupThemeMapper.containsKey(actionGroupCode)){
						return actgroupThemeMapper.get(actionGroupCode);
					}
				}
			}
		}
		return null;
	}

	private void populateThemeMap() {
    	List<LayoutThemeMapper> themeMappers = metadataService.getLayoutThemeMapperList();
    	for(LayoutThemeMapper layoutThemeMapper : themeMappers){
    		String screenCode = layoutThemeMapper.getScreenMasterCode();
    		if(scrSecActActgroupThemeMapper.containsKey(screenCode)){
				Map<String, Map<String, Map<String, Integer>>> secActActgroupThemeMapper = scrSecActActgroupThemeMapper.get(screenCode);
				String sectionCode = layoutThemeMapper.getSectionMasterCode();
				if(secActActgroupThemeMapper.containsKey(sectionCode)){
					Map<String, Map<String, Integer>> actActgroupThemeMapper = secActActgroupThemeMapper.get(sectionCode);
					String actionVisualCode = layoutThemeMapper.getActionVisualMasterCode();
					if(actActgroupThemeMapper.containsKey(actionVisualCode)){
						Map<String, Integer> actgroupThemeMapper = actActgroupThemeMapper.get(actionVisualCode);
						String actiongroup = layoutThemeMapper.getActionGroupCode();
						if(!actgroupThemeMapper.containsKey(actiongroup)){
							actgroupThemeMapper.put(actiongroup, layoutThemeMapper.getThemeSeqNumber());
						}
					}else{
						Map<String, Integer> actgroupThemeMapper = new HashMap<>();
						String actiongroup = layoutThemeMapper.getActionGroupCode();
						actgroupThemeMapper.put(actiongroup, layoutThemeMapper.getThemeSeqNumber());
						actActgroupThemeMapper.put(actionVisualCode, actgroupThemeMapper);
					}
				}else{
					Map<String, Map<String, Integer>> actActgroupThemeMapper = new HashMap<>();
					String actionVisualCode = layoutThemeMapper.getActionVisualMasterCode();
					Map<String, Integer> actgroupThemeMapper = new HashMap<>();
					String actiongroup = layoutThemeMapper.getActionGroupCode();
					actgroupThemeMapper.put(actiongroup, layoutThemeMapper.getThemeSeqNumber());
					actActgroupThemeMapper.put(actionVisualCode, actgroupThemeMapper);
					secActActgroupThemeMapper.put(sectionCode, actActgroupThemeMapper);
				}
			}else{
				Map<String, Map<String, Map<String, Integer>>> secActActgroupThemeMapper = new HashMap<>();
				String sectionCode = layoutThemeMapper.getSectionMasterCode();
				Map<String, Map<String, Integer>> actActgroupThemeMapper = new HashMap<>();
				String actionVisualCode = layoutThemeMapper.getActionVisualMasterCode();
				Map<String, Integer> actgroupThemeMapper = new HashMap<>();
				String actiongroup = layoutThemeMapper.getActionGroupCode();
				actgroupThemeMapper.put(actiongroup, layoutThemeMapper.getThemeSeqNumber());
				actActgroupThemeMapper.put(actionVisualCode, actgroupThemeMapper);
				secActActgroupThemeMapper.put(sectionCode, actActgroupThemeMapper);
				scrSecActActgroupThemeMapper.put(screenCode, secActActgroupThemeMapper);
			}
		}
	}

	/*
	What all functions do we need in order to complete our requirements
	1. Does this MTPE has ALL_BUT_SELF relation permission
	2. What are the column conditions for ALL_BUT_SELF
	 */
	public boolean isMtPEAllButSelfPermissioned(String mtPE){
		if(mtPEToRowConditionsMap.isEmpty()){
			populateRowConditionMap();
		}else{
			if(mtPEToRowConditionsMap.containsKey(mtPE)){
				List<RowCondition> rowConditions = mtPEToRowConditionsMap.get(mtPE);
				for(RowCondition rowCondition : rowConditions){
					if(rowCondition.isAllButSelfPermissioned()){
						return true;
					}
				}
			}
		}
		return false;
	}

	public RowCondition getAllButSelfRowCondition(String mtPE){
		if(mtPEToRowConditionsMap.isEmpty()){
			populateRowConditionMap();
		}else{
			if(mtPEToRowConditionsMap.containsKey(mtPE)){
				List<RowCondition> rowConditions = mtPEToRowConditionsMap.get(mtPE);
				for(RowCondition rowCondition : rowConditions){
					if(rowCondition.isAllButSelfPermissioned()){
						return rowCondition;
					}
				}
			}
		}
		return null;
	}

	private void populateRowConditionMap() {
		Map<String, List<RowCondition>> result = metadataService.getRowConditionsForAllMTPEs();
		mtPEToRowConditionsMap.putAll(result);
	}

	public void resetMetadataObjectRepository(){
		btPEToEntityMetaMap.clear();
		btMtPEToEntityMetaMap.clear();
		btDoToEntityMetaMap.clear();
		mtPEActionToAuditMsgFormatMap.clear();
		doToMasterEntityMetaMap.clear();
		mtPEToMasterEntityMetaMap.clear();
		asSsIsdelWstatToActionList.clear();
		iconMap.clear();
		mtPEMap.clear();
		propertyControlMap.clear();
		scrSecActActgroupThemeMapper.clear();
		licencedLocales.clear();
		mtPEToRowConditionsMap.clear();
	}

	public int getAlias(String label) throws SQLException {
		if(labelToAliasMap.isEmpty()){
			populateLabelToAliasMap();
			List<Integer> aliasList = new ArrayList<>();
			aliasList.addAll(labelToAliasMap.values());
			Collections.sort(aliasList);
			if(aliasList.size()!=0)
				currentMaxAlias = aliasList.get(aliasList.size()-1);
			else
				currentMaxAlias = 0;
		}
		if(labelToAliasMap.containsKey(label)){
			return labelToAliasMap.get(label);
		}else{
			metadataService.insertLabelAlias(label, currentMaxAlias+1);
			labelToAliasMap.put(label, currentMaxAlias+1);
			currentMaxAlias = currentMaxAlias + 1;
			return currentMaxAlias;

		}
	}

	private void populateLabelToAliasMap() {
		Map<String, Integer> result = metadataService.getLabelToAliasMap();
		labelToAliasMap.putAll(result);
	}

	public NameCalculationRuleVO getNameCalculationRule(String localeCode, String nameType) {
		NameCalculationRuleVO ruleVO = null;
		if (localeNameTypeToNameCalMap.containsKey(localeCode)){
			if (localeNameTypeToNameCalMap.get(localeCode).containsKey(nameType)){
				return localeNameTypeToNameCalMap.get(localeCode).get(nameType);
			}
			else {
				ruleVO = metadataService.getNameCalculationRule(localeCode, nameType);
				if (ruleVO != null) {
					localeNameTypeToNameCalMap.get(localeCode).put(nameType, ruleVO);
				}
			}
		}
		else {
			ruleVO = metadataService.getNameCalculationRule(localeCode, nameType);
			if (ruleVO != null) {
				Map<String, NameCalculationRuleVO> nameTypeToNameCalculationMap = new HashMap<>();
				nameTypeToNameCalculationMap.put(ruleVO.getNameType(), ruleVO);
				localeNameTypeToNameCalMap.put(ruleVO.getLocaleCode(), nameTypeToNameCalculationMap);
			}
		}
		return ruleVO;
	}

	public List<AnalyticsDOAControlMap> getAnalyticsDoaToControlMap() {
		if(analyticsDOAControlMaps.isEmpty()){
			List<AnalyticsDOAControlMap> result = metadataService.getAnalyticsDoaToControlMap();
			analyticsDOAControlMaps.addAll(result);
		}
		return analyticsDOAControlMaps;
	}
}
