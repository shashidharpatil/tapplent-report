package com.tapplent.platformutility.batch.model;

public class DG_PPTData {

	
	private String exportPptTemplatePkId;
	private String exportPptTemplateNameG11NBlob;
	private String mtPeCodeFkId;
	private Integer sequenceNumberPosInt;
	private String pptTemplateFileNameTxt;
	private String pptTemplateDocId;
	private boolean isExportPdf;
	public String getExportPptTemplatePkId() {
		return exportPptTemplatePkId;
	}
	public void setExportPptTemplatePkId(String exportPptTemplatePkId) {
		this.exportPptTemplatePkId = exportPptTemplatePkId;
	}
	public String getExportPptTemplateNameG11NBlob() {
		return exportPptTemplateNameG11NBlob;
	}
	public void setExportPptTemplateNameG11NBlob(String exportPptTemplateNameG11NBlob) {
		this.exportPptTemplateNameG11NBlob = exportPptTemplateNameG11NBlob;
	}
	public String getMtPeCodeFkId() {
		return mtPeCodeFkId;
	}
	public void setMtPeCodeFkId(String mtPeCodeFkId) {
		this.mtPeCodeFkId = mtPeCodeFkId;
	}
	public Integer getSequenceNumberPosInt() {
		return sequenceNumberPosInt;
	}
	public void setSequenceNumberPosInt(Integer sequenceNumberPosInt) {
		this.sequenceNumberPosInt = sequenceNumberPosInt;
	}
	public String getPptTemplateFileNameTxt() {
		return pptTemplateFileNameTxt;
	}
	public void setPptTemplateFileNameTxt(String pptTemplateFileNameTxt) {
		this.pptTemplateFileNameTxt = pptTemplateFileNameTxt;
	}
	public String getPptTemplateDocId() {
		return pptTemplateDocId;
	}
	public void setPptTemplateDocId(String pptTemplateDocId) {
		this.pptTemplateDocId = pptTemplateDocId;
	}
	public boolean isExportPdf() {
		return isExportPdf;
	}
	public void setExportPdf(boolean isExportPdf) {
		this.isExportPdf = isExportPdf;
	}
	
	
}
