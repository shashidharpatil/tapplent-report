package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.sql.Timestamp;

public class EventAttchment {
	private String attachmentPkId;
	private String attachmentFkId;
	private String artefactAttach;
	private String attachThumnailImageid;
	private String attachmentNameTxt;
	private String attachmentTypeCodeFkId;
	private Timestamp lastModifiedDatetime;
	private boolean isDeleted;
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(Timestamp lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
	public String getAttachmentPkId() {
		return attachmentPkId;
	}
	public void setAttachmentPkId(String attachmentPkId) {
		this.attachmentPkId = attachmentPkId;
	}
	public String getAttachmentFkId() {
		return attachmentFkId;
	}
	public void setAttachmentFkId(String attachmentFkId) {
		this.attachmentFkId = attachmentFkId;
	}
	public String getArtefactAttach() {
		return artefactAttach;
	}
	public void setArtefactAttach(String artefactAttach) {
		this.artefactAttach = artefactAttach;
	}
	public String getAttachThumnailImageid() {
		return attachThumnailImageid;
	}
	public void setAttachThumnailImageid(String attachThumnailImageid) {
		this.attachThumnailImageid = attachThumnailImageid;
	}
	public String getAttachmentNameTxt() {
		return attachmentNameTxt;
	}
	public void setAttachmentNameTxt(String attachmentNameG11nBigTxt) {
		this.attachmentNameTxt = attachmentNameG11nBigTxt;
	}
	public String getAttachmentTypeCodeFkId() {
		return attachmentTypeCodeFkId;
	}
	public void setAttachmentTypeCodeFkId(String attachmentTypeCodeFkId) {
		this.attachmentTypeCodeFkId = attachmentTypeCodeFkId;
	}
	
	

}
