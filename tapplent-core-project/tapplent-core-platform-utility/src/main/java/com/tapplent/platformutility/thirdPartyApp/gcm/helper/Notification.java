package com.tapplent.platformutility.thirdPartyApp.gcm.helper;

public class Notification {
	
	private String notificationKey; 
	private String notificationId;
	private String title;
	private String smallIcon;
	private String notificationText;
	private String subText;
	private String actions;
	
	
	public String getNotificationKey() {
		return notificationKey;
	}
	public void setNotificationKey(String notificationKey) {
		this.notificationKey = notificationKey;
	}
	public String getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSmallIcon() {
		return smallIcon;
	}
	public void setSmallIcon(String smallIcon) {
		this.smallIcon = smallIcon;
	}
	public String getNotificationText() {
		return notificationText;
	}
	public void setNotificationText(String notificationText) {
		this.notificationText = notificationText;
	}
	public String getSubText() {
		if(subText.equals(null))
		return subText;
		return null;
	}
	public void setSubText(String subText) {
		this.subText = subText;
	}
	public String getActions() {
		if(actions==null)
		return actions;
		return null;
	}
	public void setActions(String actions) {
		this.actions = actions;
	}
	
	

}
