package com.tapplent.platformutility.metadata;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.tapplent.platformutility.accessmanager.structure.RowCondition;
import com.tapplent.platformutility.activityLog.AuditMsgFormat;
import com.tapplent.platformutility.common.util.RequestSelectType;
import com.tapplent.platformutility.common.util.valueObject.AnalyticsDOAControlMap;
import com.tapplent.platformutility.common.util.valueObject.NameCalculationRuleVO;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.layout.valueObject.LicencedLocalesVO;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.uilayout.valueobject.LayoutThemeMapper;

public interface MetadataService {

	EntityMetadata getMetadataByProcessElement(String baseTemplate,String mtPE,String processElementId);

	MasterEntityMetadata getMetadataByDomainObject(String doName);
	
	MasterEntityMetadata getMetadataByMTPE(String mtPE);

	EntityMetadata getMetadataByBtDo(String bt, String doName);

	List<EntityAttributeMetadata> getBtAttributeMetaListByMtDoa(String doaCodeFkId);

	AttributePaths getAttributePath(String baseTemplateId, String mtPE, RequestSelectType selectType);

	/**
	 * @return
	 */
	List<DataActions> getDataActionsList();

	/**
	 * @return the complete list of BT attributes
	 */
	List<EntityAttributeMetadata> getBTAttributeList();

	Map<String, IconVO> getIconMap();

    Map<String,MTPE> getMtPEMap();

    List<String> getDependencyKeyRelatedPEs(String mtPE);
	List<String> getForeignKeyRelatedPEs(String mtPE);
	TemplateMapper getTemaplateMapperByMtPE(String mtPE);

    Map<String,PropertyControlMaster> getPropertyControlMap();

	CountrySpecificDOAMeta getCountrySpecificDOAMeta(String btDoSpecId, String baseCountryCodeFkId);

    AuditMsgFormat getAuditMsgFormatByMtPEAndAction(String activityLogActionCode, String mtPE);

    String getSubjectUserId(StringBuilder sql, String dbDataTypeCode);

	UiSetting getUiSetting();

    List<LayoutThemeMapper> getLayoutThemeMapperList();

    List<LicencedLocalesVO> getLicencedLocale();

    Map<String,List<RowCondition>> getRowConditionsForAllMTPEs();

    void insertLabelAlias(String label, Integer currentMaxAlias) throws SQLException;

	Map<String,Integer> getLabelToAliasMap();

    NameCalculationRuleVO getNameCalculationRule(String localeCode, String nameType);

    List<AnalyticsDOAControlMap> getAnalyticsDoaToControlMap();
}
