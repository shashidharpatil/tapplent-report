package com.tapplent.platformutility.batch.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class POST_PostData {

	private String socialAccountPostTxnDepFkId;
	private String personFkId;
	private boolean returnStatusCodeFkId;
	private String socialAccountPersonPostPkId;
	private String socialAccountPostTxnPkId;
	private String socialAccountPostDefnFkId;
	private String mtPeCodeFkId;
	private String doRowPrimaryKeyTxt;
	private String doRowVersionFkId;
	private String socialAccountTypeCodeFkId;
	private String notificationDefinitionFkId;
	private Object personCredential;
	private List<POST_SocialDoaMap> doaMap;
	private ResultSet resultSet;
	private boolean isSuccess;
	private Object error;
	private String returnId;

	public String getReturnId() {
		return returnId;
	}

	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}

	public Object getError() {
		return error;
	}

	public void setError(Object error) {
		this.error = error;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getSocialAccountPostTxnDepFkId() {
		return socialAccountPostTxnDepFkId;
	}

	public void setSocialAccountPostTxnDepFkId(String socialAccountPostTxnDepFkId) {
		this.socialAccountPostTxnDepFkId = socialAccountPostTxnDepFkId;
	}

	public String getPersonFkId() {
		return personFkId;
	}

	public void setPersonFkId(String personFkId) {
		this.personFkId = personFkId;
	}

	public boolean getReturnStatusCodeFkId() {
		return returnStatusCodeFkId;
	}

	public void setReturnStatusCodeFkId(boolean returnStatusCodeFkId) {
		this.returnStatusCodeFkId = returnStatusCodeFkId;
	}

	public String getSocialAccountPersonPostPkId() {
		return socialAccountPersonPostPkId;
	}

	public void setSocialAccountPersonPostPkId(String socialAccountPersonPostPkId) {
		this.socialAccountPersonPostPkId = socialAccountPersonPostPkId;
	}

	public String getSocialAccountPostTxnPkId() {
		return socialAccountPostTxnPkId;
	}

	public void setSocialAccountPostTxnPkId(String socialAccountPostTxnPkId) {
		this.socialAccountPostTxnPkId = socialAccountPostTxnPkId;
	}

	public String getSocialAccountPostDefnFkId() {
		return socialAccountPostDefnFkId;
	}

	public void setSocialAccountPostDefnFkId(String socialAccountPostDefnFkId) {
		this.socialAccountPostDefnFkId = socialAccountPostDefnFkId;
	}

	public String getMtPeCodeFkId() {
		return mtPeCodeFkId;
	}

	public void setMtPeCodeFkId(String mtPeCodeFkId) {
		this.mtPeCodeFkId = mtPeCodeFkId;
	}

	public String getDoRowPrimaryKeyTxt() {
		return doRowPrimaryKeyTxt;
	}

	public void setDoRowPrimaryKeyTxt(String doRowPrimaryKeyTxt) {
		this.doRowPrimaryKeyTxt = doRowPrimaryKeyTxt;
	}

	public String getDoRowVersionFkId() {
		return doRowVersionFkId;
	}

	public void setDoRowVersionFkId(String doRowVersionFkId) {
		this.doRowVersionFkId = doRowVersionFkId;
	}

	public String getSocialAccountTypeCodeFkId() {
		return socialAccountTypeCodeFkId;
	}

	public void setSocialAccountTypeCodeFkId(String socialAccountTypeCodeFkId) {
		this.socialAccountTypeCodeFkId = socialAccountTypeCodeFkId;
	}

	public String getNotificationDefinitionFkId() {
		return notificationDefinitionFkId;
	}

	public void setNotificationDefinitionFkId(String notificationDefinitionFkId) {
		this.notificationDefinitionFkId = notificationDefinitionFkId;
	}

	public Object getPersonCredential() {
		return personCredential;
	}

	public void setPersonCredential(Object personCredential) {
		this.personCredential = personCredential;
	}

	public List<POST_SocialDoaMap> getDoaMap() {
		return doaMap;
	}

	public void setDoaMap(List<POST_SocialDoaMap> doaMap) {
		this.doaMap = doaMap;
	}

	public ResultSet getResultSet() {
		return resultSet;
	}

	public void setResultSet(ResultSet resultSet) {
		this.resultSet = resultSet;
	}

}
