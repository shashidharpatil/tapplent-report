package com.tapplent.platformutility.sql.context.model;

public abstract class SQLContextModel {
	
	String entityName;
	String alias;
	private String selectExpression;
	private String whereExpression;

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public void setSelectExpression(String parseSelectExpression) {
		 this.selectExpression=parseSelectExpression;
	}

	public String getSelectExpression() {
		return selectExpression;
	}
	public String getWhereExpression() {
		return whereExpression;
	}

	public void setWhereExpression(String whereExpression) {
		this.whereExpression = whereExpression;
	}
}
