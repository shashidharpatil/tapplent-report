package com.tapplent.platformutility.notification.helper;

public class AppConstants {

	public static String notification_key = "notification_key";
	
	public static String create = "create";
	
	public static String add = "add";
	
	public static String remove = "remove";
	
	public static String gcmGroupUrl = "https://android.googleapis.com/gcm/notification";
	
	public static String gcmSendUrl = "https://android.googleapis.com/gcm/send";

	public static String fcmGroupUrl = "https://android.googleapis.com/fcm/notification";

	public static String fcmSendUrl = "https://android.googleapis.com/fcm/send";
	
}
