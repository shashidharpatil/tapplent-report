package com.tapplent.platformutility.search.builder;

public enum FilterOperator {
    EQ("="),
    GT(">"),
    GTE(">="),
    IN(" IN "),
    LT("<"),
    LE("<="),
    NE("<>"),
    BW("BETWEEN"), OR("OR"), AND("AND"), IS_NOT_NULL(" IS NOT NULL "), IS_NULL(" IS NULL "), LIKE(" like ");
    private final String sqlCondition;
    FilterOperator(String sqlCondition) {
        this.sqlCondition = sqlCondition;
    }
    public String getSqlCondition() {
        return sqlCondition;
    }
}
