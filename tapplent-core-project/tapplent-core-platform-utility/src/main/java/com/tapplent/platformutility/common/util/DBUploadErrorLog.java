package com.tapplent.platformutility.common.util;

import java.util.ArrayList;
import java.util.List;

public class DBUploadErrorLog {
	public static List<String> errorList = new ArrayList<String>();
	public static void addError(String error){
		StringBuilder errorWrapper = new StringBuilder(error);
		errorWrapper.append(System.lineSeparator())
		.append("Folder Name => ").append(DBUploadTracker.folderName).append(System.lineSeparator())
		.append("Sheet Name => ").append(DBUploadTracker.sheetName).append(System.lineSeparator())
		.append("Doa Name => ").append(DBUploadTracker.doaName).append(System.lineSeparator());
		int rowNumber = DBUploadTracker.rowNumber;
		if(DBUploadTracker.formatNumber == 1){
			rowNumber = rowNumber+7;
		}else if(DBUploadTracker.formatNumber == 2){
			rowNumber = rowNumber+4;
		}
		errorWrapper.append("Row Number => ").append(rowNumber);
		errorList.add(errorWrapper.toString());
	}
}
