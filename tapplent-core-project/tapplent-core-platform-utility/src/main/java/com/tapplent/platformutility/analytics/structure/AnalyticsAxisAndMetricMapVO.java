package com.tapplent.platformutility.analytics.structure;

/**
 * Created by tapplent on 23/08/17.
 */
public class AnalyticsAxisAndMetricMapVO {
    private String pkID;
    private String metricCategory;
    private String metricCategoryMenuType;
    private String metricAxisID;
    private String metricAxisName;
    private String ParentAnalyticsMetricAxisMap;
    private String parentAnalyticsMetricAxisID;

    public String getPkID() {
        return pkID;
    }

    public void setPkID(String pkID) {
        this.pkID = pkID;
    }

    public String getMetricCategory() {
        return metricCategory;
    }

    public void setMetricCategory(String metricCategory) {
        this.metricCategory = metricCategory;
    }

    public String getMetricAxisID() {
        return metricAxisID;
    }

    public void setMetricAxisID(String metricAxisID) {
        this.metricAxisID = metricAxisID;
    }

    public String getMetricAxisName() {
        return metricAxisName;
    }

    public void setMetricAxisName(String metricAxisName) {
        this.metricAxisName = metricAxisName;
    }

    public String getParentAnalyticsMetricAxisMap() {
        return ParentAnalyticsMetricAxisMap;
    }

    public void setParentAnalyticsMetricAxisMap(String parentAnalyticsMetricAxisMap) {
        ParentAnalyticsMetricAxisMap = parentAnalyticsMetricAxisMap;
    }

    public String getMetricCategoryMenuType() {
        return metricCategoryMenuType;
    }

    public void setMetricCategoryMenuType(String metricCategoryMenuType) {
        this.metricCategoryMenuType = metricCategoryMenuType;
    }

    public String getParentAnalyticsMetricAxisID() {
        return parentAnalyticsMetricAxisID;
    }

    public void setParentAnalyticsMetricAxisID(String parentAnalyticsMetricAxisID) {
        this.parentAnalyticsMetricAxisID = parentAnalyticsMetricAxisID;
    }
}
