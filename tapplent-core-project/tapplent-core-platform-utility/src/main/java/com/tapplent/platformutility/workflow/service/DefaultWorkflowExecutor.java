package com.tapplent.platformutility.workflow.service;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.hierarchy.service.HierarchyService;
import com.tapplent.platformutility.insert.impl.InsertDAO;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.impl.SearchDAO;
import com.tapplent.platformutility.workflow.dao.WorkflowDAO;
import com.tapplent.platformutility.workflow.structure.*;

import org.springframework.beans.BeanUtils;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * Created by Manas on 8/30/16.
 */
public class DefaultWorkflowExecutor implements WorkflowExecutor {

    private static final Logger LOG = LogFactory.getLogger(DefaultWorkflowExecutor.class);

    SearchDAO searchDAO;

    InsertDAO insertDAO;

    HierarchyService hierarchyService;

    WorkflowUtility workflowUtility;

    WorkflowDAO workflowDAO;

    public void executeWorkflowBatch() throws SQLException {
        Map<String, Workflow> inProgressWorkflows = workflowDAO.getInProgressWorkflows();
        Map<String, Map<String, WorkflowStep>> remainingWorkflowSteps = workflowDAO.getRemainingWorkflowSteps(inProgressWorkflows.keySet());//wfTxnId->wfStepTxnId->wfStep
        //FIXME filter steps for the current and immediate subsequent step.
        Map<String, Map<String, WorkflowActor>> associatedActors = workflowDAO.getAssociatedActors(remainingWorkflowSteps.values());//wfStepId->ActorId->wfActor
        Map<String, Map<String, WorkflowActorAction>> associatedActorActions = workflowDAO.getAssociatedActorActions(remainingWorkflowSteps, associatedActors);//stepTxnId->actionId->Action
        populateWorkflowHierarchy(inProgressWorkflows, remainingWorkflowSteps, associatedActors, associatedActorActions);
        Map<String, Map<String, WorkflowActorAction>> autoMovableActions = getAutoMovableActions(remainingWorkflowSteps, associatedActorActions);
        Map<String, Map<Integer, WorkflowStep>> inprogressWorkflowSteps = getInProgressAndNextWorkflowSteps(remainingWorkflowSteps, autoMovableActions);//wftTxnId->StepNo.->wfStep
        List<String> stepUpdateSqls = new ArrayList<>();
        for(Entry<String, Map<Integer, WorkflowStep>> stepEntry : inprogressWorkflowSteps.entrySet()){
            if(stepEntry.getValue().size() == 2) {
                Iterator iterator = stepEntry.getValue().entrySet().iterator();
                WorkflowStep currentWorkflowStep =(WorkflowStep)((Entry) iterator.next()).getValue();
                WorkflowStep nextWorkflowStep =(WorkflowStep)((Entry) iterator.next()).getValue();
                WorkflowStepDefn workflowStepDefn = new WorkflowStepDefn();
                BeanUtils.copyProperties(currentWorkflowStep, workflowStepDefn);
                Timestamp currentTimestamp = new Timestamp((new  Date().getTime()));
                if (currentWorkflowStep.getStepActualEndDatetime().before(new Timestamp(new Date().getTime())) && currentWorkflowStep.isDueDateEnforced()) {
                    GlobalVariables globalVariables = new GlobalVariables();
                    globalVariables.workflowDetails.workflowTxnId = stepEntry.getKey();
                    globalVariables.workflowDetails.workflowDefId = inProgressWorkflows.get(nextWorkflowStep.getWorkflowTxnStepPkId()).getWorkflowDefinitionFkId();
                    String currentStepId = nextWorkflowStep.getWorkflowTxnStepPkId();
                    Map<String, WorkflowActorAction> autoMoveActions = autoMovableActions.get(currentStepId);
                    if (nextWorkflowStep.getStepActorResolutionCodeFkId().equals("AT_STEP")) {
//                        workflowProvider.resolveActors(globalVariables, workflowStepDefn);
                        //get the actions from these actors
                    } else {
                        //FIXme 'AT_LAUNCH' what should happen?
                        //get actionList for target actors
                    }
                    List<String> actionUpdateSqls = getSqlListForAllAMActions(autoMoveActions, currentTimestamp);
                    //update target actions for receive date and the person Id from which the action was received
                    stepUpdateSqls.addAll(updateStepUpdateList(currentStepId, nextWorkflowStep.getWorkflowTxnStepPkId()));
                    //update underlying record for step and if the step is final then for wf status too!
                }
            }
            else{
                // code for starting 1st step!
            }
            //add all sqls to a batch!
        }
    }

    private void populateWorkflowHierarchy(Map<String, Workflow> inProgressWorkflows, Map<String, Map<String, WorkflowStep>> remainingWorkflowSteps,
                                           Map<String, Map<String, WorkflowActor>> associatedActors, Map<String, Map<String, WorkflowActorAction>> associatedActorActions) {
        for(Entry<String, Map<String, WorkflowStep>> steps : remainingWorkflowSteps.entrySet()){
            Map<Integer, WorkflowStep> stepMap = new HashMap<>();
            for(Entry<String, WorkflowStep> step : steps.getValue().entrySet()){
                //stepIntoWorflow(stepMapCreation)
                stepMap.put(step.getValue().getStepSeqNumPosInt(), step.getValue());

                //actorIntoStep
                String workflowStepDefnId = step.getValue().getWorkflowStepDefnFkId();
                Map<String, WorkflowActor> associatedActorMap = associatedActors.get(workflowStepDefnId);
                step.getValue().setAssociatedActorMap(associatedActorMap);

                //actorActionIntoStep
                Map<String, WorkflowActorAction> associatedActorActionMap = associatedActorActions.get(step.getKey());
                step.getValue().setAssociatedActorActionMap(associatedActorActionMap);
            }
            //stepIntoWorflow(actual)
            inProgressWorkflows.get(steps.getKey()).setWorkflowStepMap(stepMap);
        }
    }

    private List<String> updateStepUpdateList(String currentStepId, String nextStepId) throws SQLException {
        String cSql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_STEP SET STEP_STATUS_CODE_FK_ID = 'AUTO_MOVED' WHERE WORKFLOW_TXN_STEP_PK_ID = "+currentStepId+";";
        String nSql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_STEP SET STEP_STATUS_CODE_FK_ID = 'IN_PROGRESS' WHERE WORKFLOW_TXN_STEP_PK_ID = "+nextStepId+";";
        List<String> stepUpdateSqls = new ArrayList<>();
        stepUpdateSqls.add(cSql);
        insertDAO.insertRow(cSql);
        stepUpdateSqls.add(nSql);
        insertDAO.insertRow(nSql);
        return stepUpdateSqls;
    }

    private List<String> getSqlListForAllAMActions(Map<String, WorkflowActorAction> autoMoveActions, Timestamp currentTimestamp) {
        List<String> sqls = new ArrayList<>();
        StringBuilder template = new StringBuilder("UPDATE T_PFM_IEU_WORKFLOW_TXN_ACTION SET SRC_ACTR_ACTION_STATUS_CODE_FK_ID = 'AUTO_MOVED', SRC_ACTR_ACTL_CMPLT_DATETIME = ");
        template.append(currentTimestamp+ " WORKFLOW_TXN_ACTION_PK_ID = ");
        for(Entry<String, WorkflowActorAction> action : autoMoveActions.entrySet()){
            String sql = template + action.getKey() + ";";
            sqls.add(sql);
        }
        return sqls;
    }

    private Map<String,Map<String, WorkflowActorAction>> getAutoMovableActions(Map<String, Map<String, WorkflowStep>> remainingWorkflowSteps,
                                                                               Map<String, Map<String, WorkflowActorAction>> associatedActorActions) {
        Map<String, Map<String, WorkflowActorAction>> autoMovableActions = new HashMap<>();
        for(Entry<String, Map<String, WorkflowStep>> remainingWorkflowStepEntry : remainingWorkflowSteps.entrySet()){
            String remainingWorkflowStepId = remainingWorkflowStepEntry.getValue().entrySet().iterator().next().getValue().getWorkflowTxnStepPkId();
            Map<String, WorkflowActorAction> relatedActions = associatedActorActions.get(remainingWorkflowStepId);
            Map<String, WorkflowActorAction> relatedAutoMovableActions = relatedActions.entrySet().stream()
                                                .filter(p->p.getValue().isActionToDlftExecOnForcedMove())
                                                .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
            autoMovableActions.put(remainingWorkflowStepId, relatedAutoMovableActions);
        }
        return autoMovableActions;
    }

    private Map<String,Map<Integer,WorkflowStep>> getInProgressAndNextWorkflowSteps(Map<String, Map<String, WorkflowStep>> remainingWorkflowSteps, Map<String, Map<String, WorkflowActorAction>> autoMovableActions) {
        Map<String, Map<Integer, WorkflowStep>> inProgressWorkflowSteps = new LinkedHashMap<>();
        for(Entry<String, Map<String, WorkflowStep>> stepEntry : remainingWorkflowSteps.entrySet()){//notstarted bhi aayega!
            Map<String, WorkflowStep> steps = stepEntry.getValue();
            Map<Integer, WorkflowStep> inprogressStep = steps.entrySet().stream()
                                                                .filter(p->p.getValue().getStepStatusCodeFkId().equals("IN_PROGRESS"))
                                                                .collect(Collectors.toMap(p -> p.getValue().getStepSeqNumPosInt(), p -> p.getValue()));
            if(inprogressStep != null){
                inprogressStep = steps.entrySet().stream()
                                .filter(p->p.getValue().getStepSeqNumPosInt()==1)
                                .collect(Collectors.toMap(p -> p.getValue().getStepSeqNumPosInt(), p -> p.getValue()));
            }else {
                Integer currentStepInt = inprogressStep.entrySet().iterator().next().getKey();
                Map<Integer, WorkflowStep> nextStep = steps.entrySet().stream()
                                                            .filter(p->p.getValue().getStepSeqNumPosInt() == currentStepInt+1)//&& check if last step marker is required
                                                            .collect(Collectors.toMap(p -> p.getValue().getStepSeqNumPosInt(), p -> p.getValue()));
                inprogressStep.put(currentStepInt+1, nextStep.get(currentStepInt+1));
            }
            inProgressWorkflowSteps.put(stepEntry.getKey(), inprogressStep);
        }
        return inProgressWorkflowSteps;
    }

    private Map<String,Map<String,WorkflowActorAction>> getAssociatedActorActions(Map<String, Map<String, WorkflowStep>> remainingWorkflowSteps, Map<String, Map<String, WorkflowActor>> associatedActors) {
       return null;
    }


    public void executeWorkflowBatchByRecord() throws Exception {
        Workflow workflow = workflowDAO.getInProgressWorkflow();
        EntityMetadataVOX entityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(workflow.getBtCodeFkId(), workflow.getMtPE());
        Map<String, Object> underlyingRecord = getUnderlyingRecord(entityMetadataVOX, workflow.getDoRowPrimaryKeyTxt(), workflow.getDoRowVersionFkId());
        String stepTxnStatus = (String) underlyingRecord.get(entityMetadataVOX.getDomainObjectCode()+".RefWorkflowTxnStepStatus");
        String stepTxnId = (String) underlyingRecord.get(entityMetadataVOX.getDomainObjectCode()+".RefWorkflowTxnStepID");
        GlobalVariables globalVariables = initializeGlobalVariableForDefaultActions(underlyingRecord, entityMetadataVOX);
        globalVariables.businessRule = workflow.getBusinessRule();
        if (stepTxnStatus.equals("IN_PROGRESS")){
            WorkflowStep currentWorkflowStep = workflowDAO.getWorkflowStep(stepTxnId);
            if (currentWorkflowStep.isDueDateEnforced() && Timestamp.valueOf(globalVariables.currentTimestampString).after(currentWorkflowStep.getStepActualEndDatetime())){
                // get all actors who haven't completed their task
                // do default actions (implicitly all transactions)
                List<WorkflowActor> nonCompletedWorkflowActors = workflowDAO.getNonCompletedWorkflowActors(currentWorkflowStep.getWorkflowTxnStepPkId());
                List<WorkflowActorAction> defaultActionForNCWActors = workflowDAO.getWorkflowDefaultActionForActorList(nonCompletedWorkflowActors);
                for (WorkflowActorAction defaultAction : defaultActionForNCWActors){
//                    executeDefaultAction(defaultAction);
                    String currentStepTxnId = globalVariables.workflowDetails.workflowTxnStepId;
                    String currentStepStatus = globalVariables.workflowDetails.workflowTxnStepStatus;
                    executeWorkflow(globalVariables, defaultAction.getWorkflowTxnActorPossibleActionPkId());
                    if (!globalVariables.workflowDetails.workflowTxnStepId.equals(currentStepTxnId) ||
                            !globalVariables.workflowDetails.workflowTxnStepStatus.equals(currentStepStatus))
                        break;
                }

            }
            if (!currentWorkflowStep.isEndStep()){
                //This is only for the next step start and actor resolution(if required)! not for actions.
                WorkflowStep nextWorkflowStep = workflowDAO.getWorkflowStep(globalVariables.workflowDetails.workflowTxnStepId);
                resolveNextStepAndUpdateSendToLog(globalVariables, currentWorkflowStep, nextWorkflowStep, workflow);
            }
        }
        else if (stepTxnStatus.matches("COMPLETED|AUTO_MOVED|SKIPPED")){
            WorkflowStep currentWorkflowStep = workflowDAO.getWorkflowStep(stepTxnId);
            //FIXME is it the right for getting next not started step for non-trivial workflows
            WorkflowStep nextWorkflowStep = getNextWorkflowStep(currentWorkflowStep.getStepSeqNumPosInt(), workflow.getWorkflowTxnPkId());
            resolveNextStepAndUpdateSendToLog(globalVariables, currentWorkflowStep, nextWorkflowStep, workflow);
        }
    }

    private WorkflowStep getNextWorkflowStep(int stepSeqNumPosInt, String workflowTxnPkId) throws SQLException {
        return workflowDAO.getNextWorkflowStep(stepSeqNumPosInt, workflowTxnPkId);
    }

    private void resolveNextStepAndUpdateSendToLog(GlobalVariables globalVariables, WorkflowStep currentWorkflowStep, WorkflowStep nextWorkflowStep, Workflow workflow) throws SQLException, ParseException {
        if (nextWorkflowStep.isStartDateEnforced() && Timestamp.valueOf(globalVariables.currentTimestampString).after(nextWorkflowStep.getStepActualEndDatetime())){
            // resolve actors if AT_STEP
            // change the send to Log if AT_STEP :
            //          a. Take all send to logs with SEND_TO_PERSON_FK_ID as null and SEND_TO_WFLOW_TXN_STEP_ACTOR_FK_ID is part of currentWorkflowStep.
            //          b. Get the resolve actors and put it in the same log with as many records as the the actors.
            if (nextWorkflowStep.getStepActorResolutionCodeFkId().equals("AT_STEP")){
                Map<String, WorkflowStep> workflowStepMap = workflowDAO.getAllWorkflowSteps(workflow.getWorkflowTxnPkId());
                List<WorkflowActor> nextStepResolvedActors = workflowUtility.resolveActors(globalVariables, nextWorkflowStep, workflowStepMap);
                Map<String, WorkflowActor> nextStepResolvedActorsMap = nextStepResolvedActors.stream().collect(Collectors.toMap(WorkflowActor :: getWorkflowTxnStepActorPkId, workflowActor -> workflowActor));
                Map<String, WorkflowActor> currentStepResolvedActorMap = workflowUtility.getResolvedPersonActorByStep(currentWorkflowStep.getWorkflowTxnStepPkId());
                Map<String, WorkflowReceiptActionLog> receiptActionLogMap = workflowUtility.getReceiptActionLogMap(currentStepResolvedActorMap);
                Map<String, WorkflowSendActionLog> sendActionLogMap = workflowUtility.getWorkflowSendActionLog(receiptActionLogMap);
                updateSendActionLogTableWithResolvedNextStepActors(sendActionLogMap, nextStepResolvedActors);
            }
        }
    }

    private void updateSendActionLogTableWithResolvedNextStepActors(Map<String, WorkflowSendActionLog> sendActionLogMap, List<WorkflowActor> nextStepResolvedActors) throws SQLException {
        workflowUtility.updateSendActionLogTableWithResolvedNextStepActors(sendActionLogMap, nextStepResolvedActors);
    }

    private GlobalVariables initializeGlobalVariableForDefaultActions(Map<String, Object> underlyingRecord, EntityMetadataVOX entityMetadataVOX) {
        WorkflowDetails workflowDetails = new WorkflowDetails();
        workflowDetails.workflowTxnId = (String) underlyingRecord.get(entityMetadataVOX.getDomainObjectCode() + ".RefWorkflowTxn");
        workflowDetails.workflowDefId = (String) underlyingRecord.get(entityMetadataVOX.getDomainObjectCode() + ".RefWorkflowDefinition");
        workflowDetails.workflowStatus = (String) underlyingRecord.get(entityMetadataVOX.getDomainObjectCode() + ".RefWorkflowTxnStatus");
        workflowDetails.workflowTxnStepId = (String) underlyingRecord.get(entityMetadataVOX.getDomainObjectCode() + ".RefWorkflowTxnStepID");
        workflowDetails.workflowTxnStepStatus = (String) underlyingRecord.get(entityMetadataVOX.getDomainObjectCode() + ".RefWorkflowTxnStepStatus");
        GlobalVariables globalVariables = new GlobalVariables();
        globalVariables.underlyingRecord = underlyingRecord;
        globalVariables.insertData.setData(underlyingRecord);
        globalVariables.workflowDetails = workflowDetails;
        ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
        String currentTimestampString = utc.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
        globalVariables.currentTimestampString = currentTimestampString;
        return globalVariables;
    }

    private void executeDefaultAction(WorkflowActorAction defaultAction) {

    }

    private Map<String,Object> getUnderlyingRecord(EntityMetadataVOX entityMetadataVOX, String doRowPrimaryKey, String doRowVersionId) {
        Map<String,Object> underlyingRecord = new HashMap<>();
        String doName = entityMetadataVOX.getDomainObjectCode();
        EntityAttributeMetadata pkMeta = entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
        String pkValue = doRowPrimaryKey;
        String pkColumnName = pkMeta.getDbColumnName().trim();
        String versionId = null;
        String versionColumnName = null;
        EntityAttributeMetadata vMeta = null;
        if(entityMetadataVOX.isVersioned()) {
            vMeta = entityMetadataVOX.getVersoinIdAttribute();
            versionId = doRowVersionId;
            versionColumnName = vMeta.getDbColumnName().trim();
        }
//        if(globalVariables.model != null){
//            pkValue = pkValue != null ? pkValue : (String) globalVariables.model.getInsertValueMap().get(pkMeta.getDbColumnName());
//            if (globalVariables.entityMetadataVOX.isVersioned())
//                versionId = versionId != null ? versionId : (String) globalVariables.model.getInsertValueMap().get(vMeta.getDbColumnName());
//        }
        Deque<Object> arguments  =  new ArrayDeque<>();
        StringBuilder sql = new StringBuilder();
        String selectString =  Util.getSqlStringForTable(entityMetadataVOX);
        if(pkValue != null && entityMetadataVOX.isVersioned() && versionId != null) {
            sql.append("(" + selectString + " WHERE " + pkColumnName +
                    " = " + Util.getStringTobeInserted(pkMeta, pkValue) + " AND " + versionColumnName + " = ?);");
//				arguments.add(pkValue);
            arguments.add(Util.convertStringIdToByteId(versionId));
            LOG.debug("----------UNDERLYING_RECORD_QUERY----------");
            SearchResult currentSr = searchDAO.search(sql.toString(), arguments);
            if (currentSr.getData() != null && !currentSr.getData().isEmpty()) {
                List<Map<String, Object>> typedData = Util.convertRawDataIntoTypedData(currentSr.getData(), entityMetadataVOX);
                underlyingRecord = typedData.get(0);
            }
        }
        return underlyingRecord;
    }


    @Override
    public void executeWorkflow(GlobalVariables globalVariables, String workflowActionId) throws Exception {
    	/*
    	 * get action from join of action txn and defn.
    	 * if target_actor_id not null --> get all person of target actor and insert in log tables, then re-evaluate exit expression for step containing source actor
    	 *   |
    	 * mutually exclusive (either target-step would be null or target-actor! Both can be null only in case of last step.)
    	 *   |
    	 * if target_step_id !null --> resolve target_step if not resolved
    	 * 			insert into send and receipt tables
    	 * 			re-evaluate all exit criteria of the (current step and next step)'s actors and if true update all step_specific actions for corresponding actor.
    	 * 																						/->if is controlling step exit == 1 move to next step and disable all actors of that step
    	 * if exit condition true logic-->(null then move to next step) else if !=null and true
    	 * 																						\->
    	 * 
    	 */
    	String workflowDefnId = globalVariables.workflowDetails.workflowDefId;
    	if (workflowActionId != null && workflowDefnId != null) {
    	    workflowUtility.validateWorkflowRelatedData(globalVariables.businessRule, globalVariables.underlyingRecord);
            WorkflowActorAction workflowActorAction = workflowDAO.getWorkflowActorActionDetails(workflowActionId, workflowDefnId);
            WorkflowActor sourcePersonActor = workflowDAO.getResolvedPersonActor(workflowActorAction.getWorkflowTxnStepActorFkId());
            //fixme we need steps here to resolve actors if required
//            List<WorkflowStep> workflowSteps = workflowDAO.getRemainingWorkflowSteps();
            //required for using the consistent function
            List<WorkflowActor> sourcePersonActorDummyList = new ArrayList<>();
            sourcePersonActorDummyList.add(sourcePersonActor);
            if (workflowActorAction.getTargetActorFkId() != null) {
                executeWorkflowForTargetActor(globalVariables, workflowActorAction, sourcePersonActor, sourcePersonActorDummyList);
            } else if (workflowActorAction.getTargetStepFkId() != null) {
                executeWorkflowForTargetStep(globalVariables, workflowActorAction, sourcePersonActor, sourcePersonActorDummyList);
            }else {
                executeWorkflowForEndStep(globalVariables, workflowActorAction, sourcePersonActor, sourcePersonActorDummyList);
            }
        }
    }

    private void executeWorkflowForTargetActor(GlobalVariables globalVariables, WorkflowActorAction workflowActorAction, WorkflowActor sourcePersonActor, List<WorkflowActor> sourcePersonActorDummyList) throws Exception {
        //get step txn and defn and pass it to function below
//                WorkflowStep currentWorkflow = workflowDAO.getWorkflowTargetStepByAction(workflowActorAction);//fixme
        List<WorkflowActor> targetPersonActorList = workflowDAO.getResolvedTargetPersonActorByAction(workflowActorAction, globalVariables.workflowDetails.workflowTxnId);//only actor Txn
        InsertIntoSendAndReceiptActionLog(globalVariables, workflowActorAction, sourcePersonActorDummyList, targetPersonActorList, false);
        List<WorkflowActor> sourceActorForWholeStep = workflowDAO.getResolvedSourcePersonActorFromWholeStepByActorTxn(workflowActorAction.getWorkflowTxnStepActorFkId()); //T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION JOIN T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR
        workflowUtility.updateActorActionStatus(workflowActorAction.getWorkflowTxnActorPossibleActionPkId());
        workflowUtility.updatePersonActorStatus(sourcePersonActor.getWorkflowTxnStepActorPkId(), sourcePersonActor.getResolvedPersonFkId(), false);
        List<WorkflowActorAction> allActionsForSourceStep = workflowDAO.getWorkflowActorActionForActorList(sourceActorForWholeStep);
        reEvaluateExitExpressionsForWorkflowStep(allActionsForSourceStep);
        List<WorkflowActorAction> allActionForTargetStep = workflowDAO.getWorkflowActorActionForActorList(targetPersonActorList);
        reEvaluateExitExpressionsForWorkflowStep(allActionForTargetStep);
        //activate all actor-specific-action for target actor. fixme NOT RIGHT! remove this when the above function is established well!
//        activateAllTargetActorSpecificActions(targetPersonActorList);
        workflowUtility.updateTargetActorsActionsStatus(targetPersonActorList);
        updateTargetActorsDefaultActionsIfRequired(globalVariables, targetPersonActorList);
    }

    private void executeWorkflowForTargetStep(GlobalVariables globalVariables, WorkflowActorAction workflowActorAction, WorkflowActor sourcePersonActor, List<WorkflowActor> sourcePersonActorDummyList) throws Exception {
        Map<String, WorkflowStep> sourceAndTargetSteps = getSourceAndTargetSteps(workflowActorAction);
        WorkflowStep targetWorkflowStep = sourceAndTargetSteps.get("TARGET_STEP");
        List<WorkflowActor> sourcePersonActorList = workflowUtility.getResolvedPersonActorByStep(sourceAndTargetSteps.get("SOURCE_STEP"), globalVariables, sourceAndTargetSteps);
        List<WorkflowActor> targetPersonActorList = workflowUtility.getResolvedPersonActorByStep(sourceAndTargetSteps.get("TARGET_STEP"), globalVariables, sourceAndTargetSteps);
        InsertIntoSendAndReceiptActionLog(globalVariables, workflowActorAction, sourcePersonActorDummyList, targetPersonActorList, false);//FIXME apply date-time based gates
        workflowUtility.updateActorActionStatus(workflowActorAction.getWorkflowTxnActorPossibleActionPkId());
        workflowUtility.updatePersonActorStatus(sourcePersonActor.getWorkflowTxnStepActorPkId(), sourcePersonActor.getResolvedPersonFkId(), true);
        /*deactivate all actions of the current step if isCritCntrlgSrcStepExit == true.*/
//        String exitCountCriteria = workflowActorAction.getExitCountCriteriaExpn();
        List<WorkflowActorAction> allSourceStepPossibleActions = workflowDAO.getWorkflowActorActionForActorList(sourcePersonActorList);
        if (workflowUtility.isExitCriteriaFulfilledForStepTransition(workflowActorAction.getExitCountCriteriaForStepTrnsnExpn())) {
            workflowDAO.deActivateActions(allSourceStepPossibleActions);//fixme should be done by reEvaluateExitExpressionsForWorkflowStep ...?
            String sourceStepStatus = "COMPLETED";
            globalVariables.workflowDetails.workflowTxnStepStatus = sourceStepStatus;
            String targetStepStatus = null;
            if (!targetWorkflowStep.isStartDateEnforced() || targetWorkflowStep.getStepActualStartDatetime().before(globalVariables.currentTimestamp)) {//!(nextStep.isStartDateEnforced() && nextStep.getStepActualStartDatetime().after(currentTimestamp))
                targetStepStatus = "IN_PROGRESS";
                globalVariables.workflowDetails.workflowTxnStepId = workflowActorAction.getTargetWflowTxnStepFkId();
                globalVariables.workflowDetails.workflowTxnStepStatus = targetStepStatus;
            }
            workflowUtility.updateStepTXNStatus(sourcePersonActor.getWorkflowTxnStepFkId(), sourceStepStatus, targetWorkflowStep.getWorkflowTxnStepPkId(), targetStepStatus);
            workflowUtility.updateWorkflowDetailsInUnderlyingRecord(globalVariables);
        }
//        else {
//            WorkflowUtility.isExitCriteriaFulfilledForStepTransition(exitCountCriteria);//fixme not required
//        }
        List<WorkflowActorAction> allActionsForTargetStep = workflowDAO.getWorkflowActorActionForActorList(targetPersonActorList);//not required for source as the step_exit_action itself is called that means u need to exit the step deactivating all source actor's actions.
        reEvaluateExitExpressionsForWorkflowStep(allActionsForTargetStep);//required for target step
        workflowUtility.updateTargetActorsActionsStatus(targetPersonActorList);
        updateTargetActorsDefaultActionsIfRequired(globalVariables, targetPersonActorList);//if target actor is resolved with no actor (dummy)!
    }

    private void updateTargetActorsDefaultActionsIfRequired(GlobalVariables globalVariables, List<WorkflowActor> targetPersonActorList) throws Exception {
        Person systemDummyPerson = TenantAwareCache.getPersonRepository().getSystemDummyPersonDetails("WorkflowAdministrator");
        String dummyPersonId = systemDummyPerson.getPersonPkId();
        WorkflowActorAction defaultWorkflowActorAction = null;
        for (WorkflowActor targetWorkflowActor : targetPersonActorList){
            if (targetWorkflowActor.getResolvedPersonFkId() != null) {// if the target step is not resolved yet!
                if (targetWorkflowActor.getResolvedPersonFkId().equals(dummyPersonId)
                        && targetWorkflowActor.getNoActorsActionCodeFkId().equalsIgnoreCase("SKIP")) {
                    defaultWorkflowActorAction = workflowDAO.getWorkflowDefaultActionForActor(targetWorkflowActor.getWorkflowTxnStepActorPkId());
                    //fixme if null the only change the status to to skip ?
                    if (defaultWorkflowActorAction != null)
                        executeWorkflow(globalVariables, defaultWorkflowActorAction.getWorkflowTxnActorPossibleActionPkId());
                }
            }
        }
    }

    private void executeWorkflowForEndStep(GlobalVariables globalVariables, WorkflowActorAction workflowActorAction, WorkflowActor sourcePersonActor, List<WorkflowActor> sourcePersonActorDummyList) throws SQLException {
        WorkflowStep workflowSourceStep = workflowDAO.getWorkflowSourceStepByAction(workflowActorAction);
//        if (workflowSourceStep.isEndStep()) {
        List<WorkflowActor> sourcePersonActorList = workflowUtility.getResolvedPersonActorByStep(workflowSourceStep, globalVariables, null);
        InsertIntoSendAndReceiptActionLog(globalVariables, workflowActorAction, sourcePersonActorDummyList, null, workflowSourceStep.isEndStep());//FIXME apply date-time based gates
        workflowUtility.updateActorActionStatus(workflowActorAction.getWorkflowTxnActorPossibleActionPkId());
        workflowUtility.updatePersonActorStatus(sourcePersonActor.getWorkflowTxnStepActorPkId(), sourcePersonActor.getResolvedPersonFkId(), true);
//        String exitCountCriteria = workflowActorAction.getExitCountCriteriaExpn();
        if (WorkflowUtility.isExitCriteriaFulfilledForStepTransition(workflowActorAction.getExitCountCriteriaForStepTrnsnExpn())) {// it should not be required! as always be true ..?
            List<WorkflowActorAction> allSourceStepPossibleActions = workflowDAO.getWorkflowActorActionForActorList(sourcePersonActorList);
            workflowDAO.deActivateActions(allSourceStepPossibleActions);//fixme should be done by reEvaluateExitExpressionsForWorkflowStep ...?
            String sourceStepStatus = "COMPLETED";
            workflowUtility.updateStepTXNStatus(workflowSourceStep.getWorkflowTxnStepPkId(), sourceStepStatus, null, null);
//              globalVariables.workflowDetails.workflowTxnStepId = workflowActorAction.getTargetWflowTxnStepFkId();
            globalVariables.workflowDetails.workflowTxnStepStatus = sourceStepStatus;
            globalVariables.workflowDetails.workflowStatus = "COMPLETED";
            workflowUtility.updateWorkflowTXNStatus(globalVariables.workflowDetails.workflowStatus, globalVariables.workflowDetails.workflowTxnId);
            workflowUtility.updateWorkflowDetailsInUnderlyingRecord(globalVariables);
        }
//        }
    }

    private void activateAllTargetActorSpecificActions(List<WorkflowActor> targetPersonActorList) throws SQLException {
        String sql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION SET IS_ACTION_VISIBLE_THRU_EXIT_CRIT_EVLN = TRUE WHERE WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID = x?";
        for(WorkflowActor workflowActor : targetPersonActorList){
            Deque<Object> parameter = new ArrayDeque<>();
            parameter.add(Util.remove0x(workflowActor.getWorkflowTxnStepActorPkId()));
            insertDAO.insertRowWithParameter(sql, parameter);
        }
    }

    private Map<String, WorkflowStep> getSourceAndTargetSteps(WorkflowActorAction workflowActorAction) throws SQLException {
		Map<String, WorkflowStep> sourceAndTargetSteps = new HashMap<>();
		WorkflowStep workflowSourceStep = workflowDAO.getWorkflowSourceStepByAction(workflowActorAction);
    	sourceAndTargetSteps.put("SOURCE_STEP", workflowSourceStep);
        WorkflowStep workflowTargetStep = workflowDAO.getWorkflowTargetStepByAction(workflowActorAction);
        sourceAndTargetSteps.put("TARGET_STEP", workflowTargetStep);
//    	if (workflowActorAction.getTargetWflowTxnStepFkId() != null) {
//            WorkflowStep workflowTargetStep = workflowDAO.getWorkflowTargetStepByAction(workflowActorAction);
//            sourceAndTargetSteps.put("TARGET_STEP", workflowTargetStep);
//        }
//        else sourceAndTargetSteps.put("TARGET_STEP", null);
        return sourceAndTargetSteps;
	}

	private void reEvaluateExitExpressionsForWorkflowStep(List<WorkflowActorAction> allActions) throws SQLException {
    	String sql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION SET IS_ACTION_VISIBLE_THRU_EXIT_CRIT_EVLN = TRUE WHERE WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID = x?";// FIXME TRUE - this only controls the visibility of the aciton
    	Deque<Object> parameter;
		for(WorkflowActorAction actionTxn : allActions){
			parameter = new ArrayDeque<>();
			parameter.add(Util.remove0x(actionTxn.getWorkflowTxnActorPossibleActionPkId()));
			if(WorkflowUtility.isExitCriteriaFulfilledForActionVisibility(actionTxn.getExitCountCriteriaForActionVisibilityExpn())){
                insertDAO.insertRowWithParameter(sql, parameter);//fixme deactivating only exit criteria applicable check action, should deactivate all actions of that actor person?
			} else {
				sql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION SET IS_ACTION_VISIBLE_THRU_EXIT_CRIT_EVLN = FALSE WHERE WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID = x?";//earlier false
				insertDAO.insertRowWithParameter(sql, parameter);
			}
		}
	}

	private void InsertIntoSendAndReceiptActionLog(GlobalVariables globalVariables,
                                                   WorkflowActorAction workflowActorAction,
                                                   List<WorkflowActor> sourcePersonActor,
                                                   List<WorkflowActor> targetPersonActors, boolean endStep) throws SQLException {
		List<String> receiptActionLogPkList = insertIntoReceiptActionLog(globalVariables, workflowActorAction, sourcePersonActor, targetPersonActors);
		if (!endStep)
            insertIntoSendActionLog(globalVariables, workflowActorAction, targetPersonActors, receiptActionLogPkList);
	}

	private void insertIntoSendActionLog(GlobalVariables globalVariables,
			WorkflowActorAction workflowActorAction, List<WorkflowActor> targetActorTxns,
			List<String> receiptActionLogPkList) throws SQLException {
		StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_WORKFLOW_TXN_ACTOR_SEND_ACTION_LOG (WORKFLOW_TXN_ACTOR_SEND_ACTION_LOG_PK_ID, WORKFLOW_TXN_ACTOR_RECEIPT_ACTION_LOG_FK_ID, SEND_TO_WFLOW_TXN_STEP_ACTOR_FK_ID,"
				+ "SEND_TO_PERSON_FK_ID, SEND_TO_ON_BEHALF_OF_PERSON_FK_ID, ACTN_VISUAL_MASTER_CODE_FK_ID, ACTION_SENT_DATETIME, SENT_ACTION_COMMENT_G11N_BIG_TXT, IS_DELETED) VALUES");
		for (String receiptActionLogPk : receiptActionLogPkList) {
		    if (targetActorTxns != null && !targetActorTxns.isEmpty()) {
                for (WorkflowActor targetActorTxn : targetActorTxns) {
                    String sendActionLogPkId = Common.getUUID();
                    sql.append("(" + sendActionLogPkId + ", ");
                    sql.append(receiptActionLogPk + ", ");
                    sql.append(targetActorTxn.getWorkflowTxnStepActorPkId() + ", ");
                    sql.append(targetActorTxn.getResolvedPersonFkId() + ", ");
                    sql.append("null, ");// on behalf
                    sql.append("'" + workflowActorAction.getActionVisualMasterCodeFkId() + "', ");
                    sql.append("'" + globalVariables.currentTimestamp + "', ");
                    if (globalVariables.workflowDetails.workflowActionComment != null)
                        sql.append("'"+globalVariables.workflowDetails.workflowActionComment+"', ");
                    else sql.append("null, ");
                    sql.append(" false ), ");
                }
            }else{//for dummy receiver only in case of resolving AT_STEP
                String sendActionLogPkId = Common.getUUID();
                sql.append("(" + sendActionLogPkId + ", ");
                sql.append(receiptActionLogPk + ", ");
                sql.append("null, null, ");
                sql.append("null, ");
                sql.append("'" + workflowActorAction.getActionVisualMasterCodeFkId() + "', ");
                sql.append("'" + globalVariables.currentTimestamp + "', ");
                if (globalVariables.workflowDetails.workflowActionComment != null)
                    sql.append("'"+globalVariables.workflowDetails.workflowActionComment+"', ");
                else sql.append("null, ");
                sql.append(" false ), ");
            }
        }
		sql.replace(sql.length() - 2, sql.length(), ";");
        insertDAO.insertRow(sql.toString());
	}

	private List<String> insertIntoReceiptActionLog(GlobalVariables globalVariables, WorkflowActorAction workflowActorAction, List<WorkflowActor> sourcePersonActorList, List<WorkflowActor> targetPersonActors) throws SQLException {
		List<String> receiptActionLogPkList = new ArrayList<>();
		StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_WORKFLOW_TXN_ACTOR_RECEIPT_ACTION_LOG (WORKFLOW_TXN_ACTOR_RECEIPT_ACTION_LOG_PK_ID, WORKFLOW_TXN_STEP_ACTOR_FK_ID, RCVD_WFLOW_TXN_STEP_ACTOR_FK_ID,"
				+ "RCVD_FROM_PERSON_FK_ID, RCVD_FROM_ON_BEHALF_OF_PERSON_FK_ID, ACTION_MASTER_CODE_FK_ID, ACTION_RCV_DATETIME, RCV_ACTION_COMMENT_G11N_BIG_TXT, IS_DELETED) VALUES ");
        //fixme this would not be true if the action is auto moved!
        for (WorkflowActor sourcePersonActor : sourcePersonActorList) {
            String receiptActionLogPkId = Common.getUUID();
            receiptActionLogPkList.add(receiptActionLogPkId);
            sql.append("(" + receiptActionLogPkId + ", ");
            sql.append(workflowActorAction.getWorkflowTxnStepActorFkId() + ", ");
            sql.append(sourcePersonActor.getWorkflowTxnStepActorPkId() + ", ");
            sql.append(sourcePersonActor.getResolvedPersonFkId() + ", ");
            sql.append("null, ");// on behalf
            sql.append("'" + workflowActorAction.getActionVisualMasterCodeFkId() + "', ");
            sql.append("'" + globalVariables.currentTimestamp + "', ");
            if (globalVariables.workflowDetails.workflowActionComment != null)
                sql.append("'"+globalVariables.workflowDetails.workflowActionComment+"', ");
            else sql.append("null, ");
            sql.append(" false ), ");
        }
		sql.replace(sql.length() - 2, sql.length(), ";");
        insertDAO.insertRow(sql.toString());
		return receiptActionLogPkList;
	}


    public WorkflowDAO getWorkflowDAO() {
        return workflowDAO;
    }

    public void setWorkflowDAO(WorkflowDAO workflowDAO) {
        this.workflowDAO = workflowDAO;
    }

    public SearchDAO getSearchDAO() {
        return searchDAO;
    }

    public void setSearchDAO(SearchDAO searchDAO) {
        this.searchDAO = searchDAO;
    }

    public InsertDAO getInsertDAO() {
        return insertDAO;
    }

    public void setInsertDAO(InsertDAO insertDAO) {
        this.insertDAO = insertDAO;
    }

    public WorkflowUtility getWorkflowUtility() {
        return workflowUtility;
    }

    public void setWorkflowUtility(WorkflowUtility workflowUtility) {
        this.workflowUtility = workflowUtility;
    }

    public HierarchyService getHierarchyService() {
        return hierarchyService;
    }

    public void setHierarchyService(HierarchyService hierarchyService) {
        this.hierarchyService = hierarchyService;
    }
}
