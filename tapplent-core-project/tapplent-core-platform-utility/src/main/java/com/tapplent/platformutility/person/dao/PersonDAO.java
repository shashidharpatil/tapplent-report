package com.tapplent.platformutility.person.dao;

import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.person.structure.PersonGroup;

import java.util.List;

/**
 * Created by tapplent on 07/11/16.
 */
public interface PersonDAO {
    List<String> getPersonGroups(String personId);

    Person getPerson(String personId);

    String getDefaultBTForGroup(String mtPECode, PersonGroup group);

    Person getSystemDummyPerson(String personName);
}
