package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.sql.Timestamp;

public class MeetingInvitee {
	private String meetingInviteePkId;
	private String meetingFkId;
	private String invitedPersonFkId;
	private String attendeeStatusCodeFkId;
	private boolean isDeleted;
	private Timestamp lastModifiedDatetime;

	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(Timestamp lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
	public String getMeetingInviteePkId() {
		return meetingInviteePkId;
	}
	public void setMeetingInviteePkId(String meetingInviteePkId) {
		this.meetingInviteePkId = meetingInviteePkId;
	}
	public String getMeetingFkId() {
		return meetingFkId;
	}
	public void setMeetingFkId(String meetingFkId) {
		this.meetingFkId = meetingFkId;
	}
	public String getInvitedPersonFkId() {
		return invitedPersonFkId;
	}
	public void setInvitedPersonFkId(String invitedPersonFkId) {
		this.invitedPersonFkId = invitedPersonFkId;
	}
	public String getAttendeeStatusCodeFkId() {
		return attendeeStatusCodeFkId;
	}
	public void setAttendeeStatusCodeFkId(String attendeeStatusCodeFkId) {
		this.attendeeStatusCodeFkId = attendeeStatusCodeFkId;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
