package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 26/04/17.
 */
public class ControlActionInstanceVO {

    private String actionPkId;
    private String controlFkId;
    private String actionMasterCode;
    private String actionGroupCode;
    private String cnfMsgTxt;
    private String successMessage;
    private String failureMessage;
    private String applicableToControlState;
    private String mtPEAlias;

    public String getActionPkId() {
        return actionPkId;
    }

    public void setActionPkId(String actionPkId) {
        this.actionPkId = actionPkId;
    }

    public String getActionMasterCode() {
        return actionMasterCode;
    }

    public void setActionMasterCode(String actionMasterCode) {
        this.actionMasterCode = actionMasterCode;
    }

    public String getActionGroupCode() {
        return actionGroupCode;
    }

    public void setActionGroupCode(String actionGroupCode) {
        this.actionGroupCode = actionGroupCode;
    }

    public String getCnfMsgTxt() {
        return cnfMsgTxt;
    }

    public void setCnfMsgTxt(String cnfMsgTxt) {
        this.cnfMsgTxt = cnfMsgTxt;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    public String getApplicableToControlState() {
        return applicableToControlState;
    }

    public void setApplicableToControlState(String applicableToControlState) {
        this.applicableToControlState = applicableToControlState;
    }

    public String getControlFkId() {
        return controlFkId;
    }

    public void setControlFkId(String controlFkId) {
        this.controlFkId = controlFkId;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }
}
