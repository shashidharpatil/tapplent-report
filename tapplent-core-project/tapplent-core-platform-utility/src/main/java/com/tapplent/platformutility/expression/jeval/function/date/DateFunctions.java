/*
 * Copyright 2002-2007 Robert Breidecker.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tapplent.platformutility.expression.jeval.function.date;

import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.expression.jeval.function.Function;
import com.tapplent.platformutility.expression.jeval.function.FunctionGroup;
import com.tapplent.platformutility.expression.jeval.function.string.CharAt;
import com.tapplent.platformutility.expression.jeval.function.string.CompareTo;
import com.tapplent.platformutility.expression.jeval.function.string.CompareToIgnoreCase;
import com.tapplent.platformutility.expression.jeval.function.string.Concat;
import com.tapplent.platformutility.expression.jeval.function.string.Contains;
import com.tapplent.platformutility.expression.jeval.function.string.EndsWith;
import com.tapplent.platformutility.expression.jeval.function.string.EqualsIgnoreCase;
import com.tapplent.platformutility.expression.jeval.function.string.Eval;
import com.tapplent.platformutility.expression.jeval.function.string.Exact;
import com.tapplent.platformutility.expression.jeval.function.string.Find;
import com.tapplent.platformutility.expression.jeval.function.string.IndexOf;
import com.tapplent.platformutility.expression.jeval.function.string.LastIndexOf;
import com.tapplent.platformutility.expression.jeval.function.string.Left;
import com.tapplent.platformutility.expression.jeval.function.string.Length;
import com.tapplent.platformutility.expression.jeval.function.string.Lpad;
import com.tapplent.platformutility.expression.jeval.function.string.Mid;
import com.tapplent.platformutility.expression.jeval.function.string.Replace;
import com.tapplent.platformutility.expression.jeval.function.string.Right;
import com.tapplent.platformutility.expression.jeval.function.string.Rpad;
import com.tapplent.platformutility.expression.jeval.function.string.StartsWith;
import com.tapplent.platformutility.expression.jeval.function.string.Substitute;
import com.tapplent.platformutility.expression.jeval.function.string.ToLowerCase;
import com.tapplent.platformutility.expression.jeval.function.string.ToProperCase;
import com.tapplent.platformutility.expression.jeval.function.string.ToUpperCase;
import com.tapplent.platformutility.expression.jeval.function.string.Trim;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



/**
 * A groups of functions that can loaded at one time into an instance of
 * Evaluator. This group contains all of the functions located in the
 * net.sourceforge.jeval.function.string package.
 */
public class DateFunctions implements FunctionGroup {
    /**
     * Used to store instances of all of the functions loaded by this class.
     */
    private List functions = new ArrayList();

    /**
     * Default contructor for this class. The functions loaded by this class are
     * instantiated in this constructor.
     */
    public DateFunctions() {

                functions.add(new DateAdd());
        functions.add(new DateDiff());
        functions.add(new TimeDiff());
        functions.add(new TimeAdd());
        functions.add(new Day());
        functions.add(new Month());
        functions.add(new Year());
        functions.add(new Now());
        functions.add(new Today());
        functions.add(new GetDate());
        functions.add(new DateValue());

    }

    /**
     * Returns the name of the function group - "dateFunctions".
     *
     * @return The name of this function group class.
     */
    public String getName() {
        return "dateFunctions";
    }

    /**
     * Returns a list of the functions that are loaded by this class.
     *
     * @return A list of the functions loaded by this class.
     */
    public List getFunctions() {
        return functions;
    }

    /**
     * Loads the functions in this function group into an instance of Evaluator.
     *
     * @param evaluator
     *            An instance of Evaluator to load the functions into.
     */
    public void load(final Evaluator evaluator) {
        Iterator functionIterator = functions.iterator();

        while (functionIterator.hasNext()) {
            evaluator.putFunction((Function) functionIterator.next());
        }
    }

    /**
     * Unloads the functions in this function group from an instance of
     * Evaluator.
     *
     * @param evaluator
     *            An instance of Evaluator to unload the functions from.
     */
    public void unload(final Evaluator evaluator) {
        Iterator functionIterator = functions.iterator();

        while (functionIterator.hasNext()) {
            evaluator.removeFunction(((Function) functionIterator.next())
                    .getName());
        }
    }
}
