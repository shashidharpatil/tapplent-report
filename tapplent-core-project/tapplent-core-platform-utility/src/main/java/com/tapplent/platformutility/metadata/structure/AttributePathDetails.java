/**
 * 
 */
package com.tapplent.platformutility.metadata.structure;

import com.tapplent.platformutility.search.builder.FilterOperatorValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * To get attribute path of the which helps in making SELECT clause
 * 
 * @author Shubham Patodi
 * @since 05/07/16
 */
public class AttributePathDetails {
	private String id;
	private String attributePath;
	private boolean isStickyHeaderSort;
	private String orderBYValues;
	private int stickyHeaderSortSeq;
//	private String aggregateFunction;
	private String aggregateFunctionHavingClause;
	private String aggregateFunctionWhereClause;
	private List<GroupByAttributeDetails> groupByAttributes;
	public AttributePathDetails(){
		this.groupByAttributes = new ArrayList<>();
	}
	public AttributePathDetails(String attributePath){
		this.attributePath = attributePath;
		this.groupByAttributes = new ArrayList<>();
	}
	public String getAttributePath() {
		return attributePath;
	}
	public void setAttributePath(String attributePath) {
		this.attributePath = attributePath;
	}
	public String getAggregateFunctionHavingClause() {
		return aggregateFunctionHavingClause;
	}
	public void setAggregateFunctionHavingClause(String aggregateFunctionHavingClause) {
		this.aggregateFunctionHavingClause = aggregateFunctionHavingClause;
	}
	public String getAggregateFunctionWhereClause() {
		return aggregateFunctionWhereClause;
	}
	public void setAggregateFunctionWhereClause(String aggregateFunctionWhereClause) {
		this.aggregateFunctionWhereClause = aggregateFunctionWhereClause;
	}
	public String getOrderBYValues() {
		return orderBYValues;
	}
	public void setOrderBYValues(String orderBYValues) {
		this.orderBYValues = orderBYValues;
	}
	public int getStickyHeaderSortSeq() {
		return stickyHeaderSortSeq;
	}
	public void setStickyHeaderSortSeq(int stickyHeaderSortSeq) {
		this.stickyHeaderSortSeq = stickyHeaderSortSeq;
	}
	public boolean isStickyHeaderSort() {
		return isStickyHeaderSort;
	}
	public void setStickyHeaderSort(boolean isStickyHeaderSort) {
		this.isStickyHeaderSort = isStickyHeaderSort;
	}
	public List<GroupByAttributeDetails> getGroupByAttributes() {
		return groupByAttributes;
	}
	public void setGroupByAttributes(List<GroupByAttributeDetails> groupByAttributes) {
		this.groupByAttributes = groupByAttributes;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
