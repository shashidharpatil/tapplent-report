package com.tapplent.platformutility.metadata.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.IconVO;

public class EntityAttributeMetadata {
	@JsonIgnore
	private String versionId;
	private String templateDOACode;
	private String baseTemplateId;
	@JsonIgnore
	private String processCode;
	@JsonIgnore
	private String btDoSpecId;
	private String mtPE;
	@JsonIgnore
	private String domainObjectCode;
	@JsonIgnore
	private String dbTableName;
	private String doAttributeCode;
	@JsonIgnore
	private String dbColumnName;
	private String btDataTypeCode;
	private String attrDisplayName;
	private String attrRelnDisplayName;
	private IconVO attrIcon;
	private String dbDataTypeCode;
	private boolean isFunctionalPrimaryKeyFlag;
	private boolean isDbPrimaryKeyFlag;
	private boolean isRelationFlag;
	private boolean isHierarchyFlag;
	private boolean isSelfJoinParentFlag;
	private boolean isReportableFlag;
	private boolean isNonVersionTrackableFlag;
	private boolean isGlocalizedFlag;
	private boolean isLocalSearchableFlag;
	private boolean isMandatory;
	private boolean isEncrypted;
	private boolean isAnonymized;
	private boolean isDisplayMasked;
	private boolean isUseSubjectPersonTimezone;
	private String displayMaskCharacter;
	private boolean isReportMasked;
	private boolean isCountrySpecific;
	private String countryAttrDisplayName;
	private IconVO countryAttrIcon;
	@JsonIgnore
	private Map<String, CountrySpecificDOAMeta> countryCodeToMetaMap;
	private String reportMaskCharcter;
	private boolean isUniqueAttributeForRecord;
	private boolean isUniqueAttribute;
	private String attrHelpText;
	private String attrHintText;
	private int textCharMaxLength;
	private String attributeUnitOfMeasure;
	private IconVO booleanOnStateIcon;
	private IconVO booleanOffStateIcon;
	private boolean isEnableEmoticon;
	private boolean isContextObjectField;
	private int contextSeqNumber;
	@JsonIgnore
	private String containerBlobDbColumnName;
	@JsonIgnore
	private String toDomainObjectCode;
	@JsonIgnore
	private String toDbTableName;
	@JsonIgnore
	private String toDoAttributeCode;
	@JsonIgnore
	private String toDbColumnName;
	private boolean isCardinalityManyFlag;
	private boolean isWritingHelpRequired;
	private boolean isPropogatable;
	@JsonIgnore
	private boolean isDependencyKeyFlag;//FIXME remove
	private String addEditWizardStepName;
	private String addEditWizardSeqNumber;
	private String viewControlTypeCode;
	private String editControlTypeCode;
	@JsonIgnore
	private String dependencyDoAttributeCode;//FIXME remove
	private boolean isCompareKeyForCUD;
	private boolean autoTranslationRequired;
	private boolean enforceOrgLocaleEntry;
	private boolean enforceGlobalLocaleEntry;
	private boolean visibleInAudit;
	private String auditAttributePathExpn;
	@JsonIgnore
	private List<EntityAttributeRelationMetadata> relationMeta;
	public EntityAttributeMetadata(){
		this.relationMeta = new ArrayList<>();
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getTemplateDOACode() {
		return templateDOACode;
	}

	public void setTemplateDOACode(String templateDOACode) {
		this.templateDOACode = templateDOACode;
	}

	public String getBaseTemplateId() {
		return baseTemplateId;
	}

	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getBtDoSpecId() {
		return btDoSpecId;
	}

	public void setBtDoSpecId(String btDoSpecId) {
		this.btDoSpecId = btDoSpecId;
	}

	public String getMtPE() {
		return mtPE;
	}

	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}

	public String getDomainObjectCode() {
		return domainObjectCode;
	}

	public void setDomainObjectCode(String domainObjectCode) {
		this.domainObjectCode = domainObjectCode;
	}

	public String getDbTableName() {
		return dbTableName;
	}

	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}

	public String getDoAttributeCode() {
		return doAttributeCode;
	}

	public void setDoAttributeCode(String doAttributeCode) {
		this.doAttributeCode = doAttributeCode;
	}

	public String getDbColumnName() {
		return dbColumnName;
	}

	public void setDbColumnName(String dbColumnName) {
		this.dbColumnName = dbColumnName;
	}

	public String getBtDataTypeCode() {
		return btDataTypeCode;
	}

	public void setBtDataTypeCode(String btDataTypeCode) {
		this.btDataTypeCode = btDataTypeCode;
	}

	public String getAttrDisplayName(boolean wholeStringRequired) {
		if (wholeStringRequired)
			return attrDisplayName;
		else return Util.getG11nValue(attrDisplayName, null);
	}

	public void setAttrDisplayName(String attrDisplayName) {
		this.attrDisplayName = attrDisplayName;
	}

	public String getAttrRelnDisplayName() {
		return Util.getG11nValue(attrRelnDisplayName,null);
	}

	public void setAttrRelnDisplayName(String attrRelnDisplayName) {
		this.attrRelnDisplayName = attrRelnDisplayName;
	}

	public String getDbDataTypeCode() {
		return dbDataTypeCode;
	}

	public void setDbDataTypeCode(String dbDataTypeCode) {
		this.dbDataTypeCode = dbDataTypeCode;
	}

	public boolean isFunctionalPrimaryKeyFlag() {
		return isFunctionalPrimaryKeyFlag;
	}

	public void setFunctionalPrimaryKeyFlag(boolean functionalPrimaryKeyFlag) {
		isFunctionalPrimaryKeyFlag = functionalPrimaryKeyFlag;
	}

	public boolean isDbPrimaryKeyFlag() {
		return isDbPrimaryKeyFlag;
	}

	public void setDbPrimaryKeyFlag(boolean dbPrimaryKeyFlag) {
		isDbPrimaryKeyFlag = dbPrimaryKeyFlag;
	}

	public boolean isRelationFlag() {
		return isRelationFlag;
	}

	public void setRelationFlag(boolean relationFlag) {
		isRelationFlag = relationFlag;
	}

	public boolean isHierarchyFlag() {
		return isHierarchyFlag;
	}

	public void setHierarchyFlag(boolean hierarchyFlag) {
		isHierarchyFlag = hierarchyFlag;
	}

	public boolean isSelfJoinParentFlag() {
		return isSelfJoinParentFlag;
	}

	public void setSelfJoinParentFlag(boolean selfJoinParentFlag) {
		isSelfJoinParentFlag = selfJoinParentFlag;
	}

	public boolean isReportableFlag() {
		return isReportableFlag;
	}

	public void setReportableFlag(boolean reportableFlag) {
		isReportableFlag = reportableFlag;
	}

	public boolean isNonVersionTrackableFlag() {
		return isNonVersionTrackableFlag;
	}

	public void setNonVersionTrackableFlag(boolean nonVersionTrackableFlag) {
		isNonVersionTrackableFlag = nonVersionTrackableFlag;
	}

	public boolean isGlocalizedFlag() {
		return isGlocalizedFlag;
	}

	public void setGlocalizedFlag(boolean glocalizedFlag) {
		isGlocalizedFlag = glocalizedFlag;
	}

	public boolean isLocalSearchableFlag() {
		return isLocalSearchableFlag;
	}

	public void setLocalSearchableFlag(boolean localSearchableFlag) {
		isLocalSearchableFlag = localSearchableFlag;
	}

	public boolean isCountrySpecific() {
		return isCountrySpecific;
	}

	public void setCountrySpecific(boolean countrySpecific) {
		isCountrySpecific = countrySpecific;
	}

	public boolean isMandatory() {
		return isMandatory;
	}

	public void setMandatory(boolean mandatory) {
		isMandatory = mandatory;
	}

	public boolean isEncrypted() {
		return isEncrypted;
	}

	public void setEncrypted(boolean encrypted) {
		isEncrypted = encrypted;
	}

	public boolean isAnonymized() {
		return isAnonymized;
	}

	public void setAnonymized(boolean anonymized) {
		isAnonymized = anonymized;
	}

	public boolean isDisplayMasked() {
		return isDisplayMasked;
	}

	public void setDisplayMasked(boolean displayMasked) {
		isDisplayMasked = displayMasked;
	}

	public String getDisplayMaskCharacter() {
		return displayMaskCharacter;
	}

	public void setDisplayMaskCharacter(String displayMaskCharacter) {
		this.displayMaskCharacter = displayMaskCharacter;
	}

	public boolean isReportMasked() {
		return isReportMasked;
	}

	public void setReportMasked(boolean reportMasked) {
		isReportMasked = reportMasked;
	}

	public String getReportMaskCharcter() {
		return reportMaskCharcter;
	}

	public void setReportMaskCharcter(String reportMaskCharcter) {
		this.reportMaskCharcter = reportMaskCharcter;
	}

	public boolean isUniqueAttributeForRecord() {
		return isUniqueAttributeForRecord;
	}

	public void setUniqueAttributeForRecord(boolean uniqueAttributeForRecord) {
		isUniqueAttributeForRecord = uniqueAttributeForRecord;
	}

	public boolean isUniqueAttribute() {
		return isUniqueAttribute;
	}

	public void setUniqueAttribute(boolean uniqueAttribute) {
		isUniqueAttribute = uniqueAttribute;
	}

	public String getAttrHelpText() {
		return Util.getG11nValue(attrHelpText, null);
	}

	public void setAttrHelpText(String attrHelpText) {
		this.attrHelpText = attrHelpText;
	}

	public String getAttrHintText() {
		return Util.getG11nValue(attrHintText, null);
	}

	public void setAttrHintText(String attrHintText) {
		this.attrHintText = attrHintText;
	}

	public boolean isUseSubjectPersonTimezone() {
		return isUseSubjectPersonTimezone;
	}

	public void setUseSubjectPersonTimezone(boolean useSubjectPersonTimezone) {
		isUseSubjectPersonTimezone = useSubjectPersonTimezone;
	}

	public int getTextCharMaxLength() {
		return textCharMaxLength;
	}

	public void setTextCharMaxLength(int textCharMaxLength) {
		this.textCharMaxLength = textCharMaxLength;
	}

	public String getAttributeUnitOfMeasure() {
		return Util.getG11nValue(attributeUnitOfMeasure, null);
	}

	public void setAttributeUnitOfMeasure(String attributeUnitOfMeasure) {
		this.attributeUnitOfMeasure = attributeUnitOfMeasure;
	}

	public boolean isContextObjectField() {
		return isContextObjectField;
	}

	public void setContextObjectField(boolean contextObjectField) {
		isContextObjectField = contextObjectField;
	}

	public int getContextSeqNumber() {
		return contextSeqNumber;
	}

	public void setContextSeqNumber(int contextSeqNumber) {
		this.contextSeqNumber = contextSeqNumber;
	}

	public String getContainerBlobDbColumnName() {
		return containerBlobDbColumnName;
	}

	public void setContainerBlobDbColumnName(String containerBlobDbColumnName) {
		this.containerBlobDbColumnName = containerBlobDbColumnName;
	}

	public String getToDomainObjectCode() {
		return toDomainObjectCode;
	}

	public void setToDomainObjectCode(String toDomainObjectCode) {
		this.toDomainObjectCode = toDomainObjectCode;
	}

	public String getToDbTableName() {
		return toDbTableName;
	}

	public void setToDbTableName(String toDbTableName) {
		this.toDbTableName = toDbTableName;
	}

	public String getToDoAttributeCode() {
		return toDoAttributeCode;
	}

	public void setToDoAttributeCode(String toDoAttributeCode) {
		this.toDoAttributeCode = toDoAttributeCode;
	}

	public String getToDbColumnName() {
		return toDbColumnName;
	}

	public void setToDbColumnName(String toDbColumnName) {
		this.toDbColumnName = toDbColumnName;
	}

	public boolean isCardinalityManyFlag() {
		return isCardinalityManyFlag;
	}

	public void setCardinalityManyFlag(boolean cardinalityManyFlag) {
		isCardinalityManyFlag = cardinalityManyFlag;
	}

	public boolean isWritingHelpRequired() {
		return isWritingHelpRequired;
	}

	public void setWritingHelpRequired(boolean writingHelpRequired) {
		isWritingHelpRequired = writingHelpRequired;
	}

	public boolean isPropogatable() {
		return isPropogatable;
	}

	public void setPropogatable(boolean propogatable) {
		isPropogatable = propogatable;
	}

	public boolean isDependencyKeyFlag() {
		return isDependencyKeyFlag;
	}

	public void setDependencyKeyFlag(boolean dependencyKeyFlag) {
		isDependencyKeyFlag = dependencyKeyFlag;
	}

	public String getAddEditWizardStepName() {
		return addEditWizardStepName;
	}

	public void setAddEditWizardStepName(String addEditWizardStepName) {
		this.addEditWizardStepName = addEditWizardStepName;
	}

	public String getAddEditWizardSeqNumber() {
		return addEditWizardSeqNumber;
	}

	public void setAddEditWizardSeqNumber(String addEditWizardSeqNumber) {
		this.addEditWizardSeqNumber = addEditWizardSeqNumber;
	}

	public String getViewControlTypeCode() {
		return viewControlTypeCode;
	}

	public void setViewControlTypeCode(String viewControlTypeCode) {
		this.viewControlTypeCode = viewControlTypeCode;
	}

	public String getEditControlTypeCode() {
		return editControlTypeCode;
	}

	public void setEditControlTypeCode(String editControlTypeCode) {
		this.editControlTypeCode = editControlTypeCode;
	}

	public String getDependencyDoAttributeCode() {
		return dependencyDoAttributeCode;
	}

	public void setDependencyDoAttributeCode(String dependencyDoAttributeCode) {
		this.dependencyDoAttributeCode = dependencyDoAttributeCode;
	}

	public List<EntityAttributeRelationMetadata> getRelationMeta() {
		return relationMeta;
	}

	public void setRelationMeta(List<EntityAttributeRelationMetadata> relationMeta) {
		this.relationMeta = relationMeta;
	}

	public IconVO getAttrIcon() {
		return attrIcon;
	}

	public void setAttrIcon(IconVO attrIcon) {
		this.attrIcon = attrIcon;
	}

	public IconVO getBooleanOnStateIcon() {
		return booleanOnStateIcon;
	}

	public void setBooleanOnStateIcon(IconVO booleanOnStateIcon) {
		this.booleanOnStateIcon = booleanOnStateIcon;
	}

	public IconVO getBooleanOffStateIcon() {
		return booleanOffStateIcon;
	}

	public void setBooleanOffStateIcon(IconVO booleanOffStateIcon) {
		this.booleanOffStateIcon = booleanOffStateIcon;
	}

	public boolean isEnableEmoticon() {
		return isEnableEmoticon;
	}

	public void setEnableEmoticon(boolean enableEmoticon) {
		isEnableEmoticon = enableEmoticon;
	}

	public String getCountryAttrDisplayName() {
		return countryAttrDisplayName;
	}

	public void setCountryAttrDisplayName(String countryAttrDisplayName) {
		this.countryAttrDisplayName = countryAttrDisplayName;
	}

	public boolean isAutoTranslationRequired() {
		return autoTranslationRequired;
	}

	public void setAutoTranslationRequired(boolean autoTranslationRequired) {
		this.autoTranslationRequired = autoTranslationRequired;
	}

	public boolean isEnforceOrgLocaleEntry() {
		return enforceOrgLocaleEntry;
	}

	public void setEnforceOrgLocaleEntry(boolean enforceOrgLocaleEntry) {
		this.enforceOrgLocaleEntry = enforceOrgLocaleEntry;
	}

	public boolean isEnforceGlobalLocaleEntry() {
		return enforceGlobalLocaleEntry;
	}

	public void setEnforceGlobalLocaleEntry(boolean enforceGlobalLocaleEntry) {
		this.enforceGlobalLocaleEntry = enforceGlobalLocaleEntry;
	}

	public IconVO getCountryAttrIcon() {
		return countryAttrIcon;
	}

	public void setCountryAttrIcon(IconVO countryAttrIcon) {
		this.countryAttrIcon = countryAttrIcon;
	}

	public Map<String, CountrySpecificDOAMeta> getCountryCodeToMetaMap() {
		if(countryCodeToMetaMap == null)
			return new HashMap<>();
		return countryCodeToMetaMap;
	}

	public void setCountryCodeToMetaMap(Map<String, CountrySpecificDOAMeta> countryCodeToMetaMap) {
		this.countryCodeToMetaMap = countryCodeToMetaMap;
	}

	public boolean isCompareKeyForCUD() {
		return isCompareKeyForCUD;
	}

	public void setCompareKeyForCUD(boolean compareKeyForCUD) {
		isCompareKeyForCUD = compareKeyForCUD;
	}

	public String getAttrDisplayName() {
		return attrDisplayName;
	}

	public boolean isVisibleInAudit() {
		return visibleInAudit;
	}

	public void setVisibleInAudit(boolean visibleInAudit) {
		this.visibleInAudit = visibleInAudit;
	}

	public String getAuditAttributePathExpn() {
		return auditAttributePathExpn;
	}

	public void setAuditAttributePathExpn(String auditAttributePathExpn) {
		this.auditAttributePathExpn = auditAttributePathExpn;
	}
}
