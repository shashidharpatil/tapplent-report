package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by tapplent on 04/11/16.
 */
public class AMColIntermediate {
    private String amColIntermediatePkId;
    private String amRowIntermediateFkId;
    private String doaCodeFkId;
    private boolean isCreatePermitted;
    private boolean isReadPermitted;
    private boolean isUpdatePermitted;
    private boolean isDeletePermitted;

    public String getAmColIntermediatePkId() {
        return amColIntermediatePkId;
    }

    public void setAmColIntermediatePkId(String amColIntermediatePkId) {
        this.amColIntermediatePkId = amColIntermediatePkId;
    }

    public String getAmRowIntermediateFkId() {
        return amRowIntermediateFkId;
    }

    public void setAmRowIntermediateFkId(String amRowIntermediateFkId) {
        this.amRowIntermediateFkId = amRowIntermediateFkId;
    }

    public String getDoaCodeFkId() {
        return doaCodeFkId;
    }

    public void setDoaCodeFkId(String doaCodeFkId) {
        this.doaCodeFkId = doaCodeFkId;
    }

    public boolean isCreatePermitted() {
        return isCreatePermitted;
    }

    public void setCreatePermitted(boolean createPermitted) {
        isCreatePermitted = createPermitted;
    }

    public boolean isReadPermitted() {
        return isReadPermitted;
    }

    public void setReadPermitted(boolean readPermitted) {
        isReadPermitted = readPermitted;
    }

    public boolean isUpdatePermitted() {
        return isUpdatePermitted;
    }

    public void setUpdatePermitted(boolean updatePermitted) {
        isUpdatePermitted = updatePermitted;
    }

    public boolean isDeletePermitted() {
        return isDeletePermitted;
    }

    public void setDeletePermitted(boolean deletePermitted) {
        isDeletePermitted = deletePermitted;
    }
}
