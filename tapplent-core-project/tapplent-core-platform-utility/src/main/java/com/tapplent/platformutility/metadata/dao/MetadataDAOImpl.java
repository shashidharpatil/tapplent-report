package com.tapplent.platformutility.metadata.dao;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import com.tapplent.platformutility.accessmanager.structure.AMColCondition;
import com.tapplent.platformutility.accessmanager.structure.RowCondition;
import com.tapplent.platformutility.activityLog.AuditMsgFormat;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.common.util.valueObject.AnalyticsDOAControlMap;
import com.tapplent.platformutility.common.util.valueObject.NameCalculationRuleVO;
import com.tapplent.platformutility.layout.valueObject.LicencedLocalesVO;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.uilayout.valueobject.LayoutThemeMapperVO;
import org.apache.commons.codec.binary.Hex;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.metadata.BtDoAttributeSpecVO;
import com.tapplent.platformutility.metadata.BtDoSpecVO;
import com.tapplent.platformutility.metadata.DataActionsVO;
import com.tapplent.platformutility.metadata.MTPEVO;
import com.tapplent.platformutility.metadata.MtDoAttributeVO;
import com.tapplent.platformutility.metadata.MtDomainObjectVO;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;

public class MetadataDAOImpl extends TapplentBaseDAO implements MetadataDAO{
	private static final Logger LOG = LogFactory.getLogger(MetadataDAOImpl.class);
	private String convertByteToString(byte[] input){
		if(input == null)
			return null;
		return Hex.encodeHexString(input);
	}
	private JsonNode getBlobJson(Blob input) throws IOException, SQLException{
		if(input == null)
			return null;
		ObjectMapper mapper = new ObjectMapper();
		JsonNode result = mapper.readTree(input.getBinaryStream());
		return result;
	}
	@SuppressWarnings("JpaQueryApiInspection")
	@Override
	public Map<String, BtDoSpecVO> getBtDoSpec(String baseTemplate, String mtPE, String btPE) {
		PreparedStatement ps;
		Map<String, BtDoSpecVO> pkIdToBtDoSpecMap = new HashMap<String, BtDoSpecVO>();
		String sql = null;
		if(btPE!=null){
			sql = "SELECT * " +
					"FROM T_BT_ADM_DO_SPEC A JOIN T_BT_ADM_PROCESS_ELEMENT B ON A.BT_PROCESS_ELEMENT_CODE_FK_ID = B.TEMPLATE_PROCESS_ELEMENT_CODE_PK_ID JOIN T_MT_ADM_PROCESS_ELEMENT M ON M.META_PROCESS_ELEMENT_CODE_PK_ID = B.MT_PROCESS_ELEMENT_CODE_FK_ID JOIN T_MT_ADM_DOMAIN_OBJECT D ON A.DOMAIN_OBJECT_CODE_FK_ID = D.DOMAIN_OBJECT_CODE_PK_ID WHERE A.BT_PROCESS_ELEMENT_CODE_FK_ID = ?;";
		}else{
			sql = "SELECT * "
					+ "FROM T_BT_ADM_DO_SPEC A JOIN T_BT_ADM_PROCESS_ELEMENT B ON A.BT_PROCESS_ELEMENT_CODE_FK_ID = B.TEMPLATE_PROCESS_ELEMENT_CODE_PK_ID JOIN T_MT_ADM_PROCESS_ELEMENT M ON M.META_PROCESS_ELEMENT_CODE_PK_ID = B.MT_PROCESS_ELEMENT_CODE_FK_ID JOIN T_MT_ADM_DOMAIN_OBJECT D ON A.DOMAIN_OBJECT_CODE_FK_ID = D.DOMAIN_OBJECT_CODE_PK_ID WHERE B.BASE_TEMPLATE_CODE_FK_ID = ? AND B.MT_PROCESS_ELEMENT_CODE_FK_ID = ?;";
		}
		try{
			Connection conn = getConnection();
			ps = conn.prepareStatement(sql);
			LOG.debug(sql);
			if(btPE!=null){
				ps.setString(1, btPE);
			}else{
				ps.setString(1, baseTemplate);
				ps.setString(2, mtPE);
			}
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				BtDoSpecVO btDoSpecVO = new BtDoSpecVO();
				btDoSpecVO.setVersionId(convertByteToString(rs.getBytes("A.VERSION_ID")));
				btDoSpecVO.setTemplateDOCode((rs.getString("A.TEMPLATE_DOMAIN_OBJECT_CODE_PK_ID")));
				btDoSpecVO.setBaseTemplateId(rs.getString("A.BASE_TEMPLATE_CODE_FK_ID"));
				btDoSpecVO.setProcessCode(rs.getString("A.PROCESS_CODE_FK_ID"));
				btDoSpecVO.setDomainObjectCode(rs.getString("A.DOMAIN_OBJECT_CODE_FK_ID"));
				btDoSpecVO.setBtPE(rs.getString("A.BT_PROCESS_ELEMENT_CODE_FK_ID"));
				btDoSpecVO.setMtPE(rs.getString("MT_PROCESS_ELEMENT_CODE_FK_ID"));
			//	btDoSpecVO.setParentMtPE(rs.getString("M.SLF_PRNT_MT_PROCESS_ELEMENT_CODE_FK_ID"));
				btDoSpecVO.setBtDOName(rs.getString("BT_DO_SPEC_NAME_G11N_BIG_TXT"));
				btDoSpecVO.setDoTypeCode(rs.getString("A.DO_TYPE_CODE_FK_ID"));
				btDoSpecVO.setDbTableName(rs.getString("A.DB_TABLE_NAME_TXT"));
				btDoSpecVO.setDoSummaryTitle(rs.getString("A.DO_SUMMARY_TITLE_TEXT_G11N_BIG_TXT"));
				btDoSpecVO.setDoSummaryTitleIcon(rs.getString("A.DO_SUMMARY_TITLE_ICON_CODE_FK_ID"));
				btDoSpecVO.setDoDetailsTitle(rs.getString("DO_DETAILS_TITLE_TEXT_G11N_BIG_TXT"));
				btDoSpecVO.setDoHelpText(rs.getString("DO_HELP_TEXT_G11N_BIG_TXT"));
				btDoSpecVO.setCountOfRecordsSingularTitle(rs.getString("COUNT_OF_RECORDS_SINGULAR_TITLE_G11N_BIG_TXT"));
				btDoSpecVO.setCountOfRecordsPluralTitle(rs.getString("COUNT_OF_RECORDS_PLURAL_TITLE_G11N_BIG_TXT"));
				btDoSpecVO.setDoCustomizableFlag(rs.getBoolean("A.IS_DO_CUSTOMIZABLE_FLAG"));
			//	btDoSpecVO.setDoAccessControlGovernedFlag(rs.getBoolean("D.IS_ACCESS_CONTROL_GOVERNED"));
				btDoSpecVO.setUpdateInline(rs.getBoolean("A.IS_UPDATE_INLINE"));
//				btDoSpecVO.setSubjectPersonDoaFullPath(rs.getString("A.SUBJECT_PERSON_DOA_FULL_PATH_TXT"));
				pkIdToBtDoSpecMap.put(btDoSpecVO.getTemplateDOCode(), btDoSpecVO);
			}
		}catch(Exception e){
			e.printStackTrace();
			LOG.error(sql);
			throw new RuntimeException(e);
		}
		return pkIdToBtDoSpecMap;
	}
	@Override
	public List<BtDoAttributeSpecVO>/*Map<String, BtDoAttributeSpecVO>*/ getBtDoAttributeSpec(String btDoSpecFkId) {
		PreparedStatement ps;
		List<BtDoAttributeSpecVO> attributeSpecVOs = null;
		String sql = "SELECT *, C.MT_PROCESS_ELEMENT_CODE_FK_ID "
				+" FROM T_BT_ADM_DO_ATTRIBUTE_SPEC A LEFT JOIN T_BT_ADM_DO_SPEC B ON A.BT_DO_SPEC_CODE_FK_ID = B.TEMPLATE_DOMAIN_OBJECT_CODE_PK_ID LEFT JOIN T_BT_ADM_PROCESS_ELEMENT C ON B.BT_PROCESS_ELEMENT_CODE_FK_ID = C.TEMPLATE_PROCESS_ELEMENT_CODE_PK_ID WHERE BT_DO_SPEC_CODE_FK_ID = ?;";
		try{
			Connection conn = getConnection();
			ps = conn.prepareStatement(sql);
			LOG.debug(sql);
			ps.setString(1, btDoSpecFkId);
			ResultSet rs = ps.executeQuery();
			attributeSpecVOs = getAttributeSpecVOListFromResultSet(rs);
		}catch(Exception e){
			e.printStackTrace();
			LOG.error(sql);
			throw new RuntimeException(e);
		}
		return attributeSpecVOs;
	}
	
	
	/**
	 * @param rs result set having rows of BT-DO Attribute Specs
	 * @return
	 */
	private List<BtDoAttributeSpecVO> getAttributeSpecVOListFromResultSet(ResultSet rs) {
		if(rs == null)
			return null;
		Map<String, BtDoAttributeSpecVO> idToAttrMap = new HashMap<>();
		List<BtDoAttributeSpecVO> attributeSpecVOs = new ArrayList<>();
		try {
			while(rs.next()){
				BtDoAttributeSpecVO btDoAttributeSpecVO = new BtDoAttributeSpecVO();
				btDoAttributeSpecVO.setVersionId(convertByteToString(rs.getBytes("A.VERSION_ID")));
				btDoAttributeSpecVO.setTemplateDOACode(rs.getString("A.TEMPLATE_DOMAIN_OBJECT_ATTRIBUTE_CODE_PK_ID"));
				btDoAttributeSpecVO.setBaseTemplateId(rs.getString("A.BASE_TEMPLATE_CODE_FK_ID"));
				btDoAttributeSpecVO.setProcessCode(rs.getString("A.PROCESS_CODE_FK_ID"));
				btDoAttributeSpecVO.setBtDoSpecId(rs.getString("A.BT_DO_SPEC_CODE_FK_ID"));
				btDoAttributeSpecVO.setMtPE(rs.getString("C.MT_PROCESS_ELEMENT_CODE_FK_ID"));
				btDoAttributeSpecVO.setDomainObjectCode(rs.getString("A.DOMAIN_OBJECT_CODE_FK_ID"));             
				btDoAttributeSpecVO.setDbTableName(rs.getString("A.DB_TABLE_NAME_TXT"));
				btDoAttributeSpecVO.setDoAttributeCode(rs.getString("A.DO_ATTRIBUTE_CODE_FK_ID"));
				btDoAttributeSpecVO.setDbColumnName(rs.getString("A.DB_COLUMN_NAME_TXT"));
//				btDoAttributeSpecVO.setBtDataTypeCode(rs.getString("A.BT_DATA_TYPE_CODE_FK_ID"));
				btDoAttributeSpecVO.setAttrDisplayName(rs.getString("ATTR_DISPLAY_NAME_G11N_BIG_TXT"));
				btDoAttributeSpecVO.setAttrRelnDisplayName(rs.getString("ATTR_RELN_DISPLAY_NAME_G11N_BIG_TXT"));
				btDoAttributeSpecVO.setAttrIcon(rs.getString("A.ATTR_ICON_CODE_FK_ID"));
				btDoAttributeSpecVO.setDbDataTypeCode(rs.getString("A.DB_DATA_TYPE_CODE_FK_ID"));
				btDoAttributeSpecVO.setFunctionalPrimaryKeyFlag(rs.getBoolean("A.IS_FUNCTIONAL_PRIMARY_KEY_FLAG"));
				btDoAttributeSpecVO.setDbPrimaryKeyFlag(rs.getBoolean("A.IS_DB_PRIMARY_KEY_FLAG"));
				btDoAttributeSpecVO.setRelationFlag(rs.getBoolean("A.IS_RELATION_FLAG"));
				btDoAttributeSpecVO.setHierarchyFlag(rs.getBoolean("A.IS_HIERARCHY_FLAG"));
				btDoAttributeSpecVO.setSelfJoinParentFlag(rs.getBoolean("A.IS_SELF_JOIN_PARENT_FLAG"));
				btDoAttributeSpecVO.setReportableFlag(rs.getBoolean("A.IS_REPORTABLE_FLAG"));
				btDoAttributeSpecVO.setNonVersionTrackableFlag(rs.getBoolean("A.IS_NON_VERSION_TRACKABLE_FLAG"));
				btDoAttributeSpecVO.setGlocalizedFlag(rs.getBoolean("A.IS_GLOCALIZED_FLAG"));
				btDoAttributeSpecVO.setLocalSearchableFlag(rs.getBoolean("A.IS_LOCAL_SEARCHABLE_FLAG"));
//				btDoAttributeSpecVO.setCountrySpecific(rs.getBoolean("A.IS_COUNTRY_SPECIFIC"));
				btDoAttributeSpecVO.setMandatory(rs.getBoolean("A.IS_MANDATORY"));
				btDoAttributeSpecVO.setEncrypted(rs.getBoolean("A.IS_ENCRYPTED"));
				btDoAttributeSpecVO.setAnonymized(rs.getBoolean("A.IS_ANONYMIZED"));
				btDoAttributeSpecVO.setDisplayMasked(rs.getBoolean("A.IS_DISPLAY_MASKED"));
				btDoAttributeSpecVO.setDisplayMaskCharacter(rs.getString("A.DISPLAY_MASK_CHARACTER_TXT"));
				btDoAttributeSpecVO.setReportMasked(rs.getBoolean("A.IS_REPORT_MASKED"));
				btDoAttributeSpecVO.setReportMaskCharcter(rs.getString("A.REPORT_MASK_CHARCTER_TXT"));
				btDoAttributeSpecVO.setUniqueAttributeForRecord(rs.getBoolean("A.IS_UNIQUE_ATTRIBUTE_FOR_RECORD"));
				btDoAttributeSpecVO.setUniqueAttribute(rs.getBoolean("A.IS_UNIQUE_ATTRIBUTE"));
				btDoAttributeSpecVO.setAttrHelpText(rs.getString("ATTR_HELP_TEXT_G11N_BIG_TXT"));
				btDoAttributeSpecVO.setAttrHintText(rs.getString("ATTR_HINT_TEXT_G11N_BIG_TXT"));
				btDoAttributeSpecVO.setUseSubjectPersonTimezone(rs.getBoolean("A.IS_USE_SUBJECT_PERSON_TIMEZONE"));
				btDoAttributeSpecVO.setTextCharMaxLength(rs.getInt("A.TEXT_CHAR_MAX_LENGTH_POS_INT"));
				btDoAttributeSpecVO.setAttributeUnitOfMeasure(rs.getString("ATTRIBUTE_UNIT_OF_MEASURE_G11N_BIG_TXT"));
				btDoAttributeSpecVO.setBooleanOnStateIcon(rs.getString("A.BOOLEAN_ON_STATE_ICON_CODE_FK_ID"));
				btDoAttributeSpecVO.setBooleanOffStateIcon(rs.getString("A.BOOLEAN_OFF_STATE_ICON_CODE_FK_ID"));
				btDoAttributeSpecVO.setEnableEmoticon(rs.getBoolean("A.IS_ENABLE_EMOTICON"));
				btDoAttributeSpecVO.setContextObjectField(rs.getBoolean("A.IS_CONTEXT_OBJECT_FIELD"));
//				btDoAttributeSpecVO.setContextSeqNumber(rs.getInt("A.CONTEXT_SEQ_NUMBER_POS_INT"));
				btDoAttributeSpecVO.setContainerBlobDbColumnName(rs.getString("A.CONTAINER_BLOB_DB_COLUMN_NAME_TXT"));
				btDoAttributeSpecVO.setToDomainObjectCode(convertByteToString(rs.getBytes("A.TO_DOMAIN_OBJECT_CODE_FK_ID")));
				btDoAttributeSpecVO.setToDbTableName(rs.getString("A.TO_DB_TABLE_NAME_TXT"));
				btDoAttributeSpecVO.setToDoAttributeCode(rs.getString("A.TO_DO_ATTRIBUTE_CODE_FK_ID"));
				btDoAttributeSpecVO.setToDbColumnName(rs.getString("A.TO_DB_COLUMN_NAME_TXT"));
				btDoAttributeSpecVO.setCardinalityManyFlag(rs.getBoolean("A.IS_CARDINALITY_MANY_FLAG"));
				btDoAttributeSpecVO.setWritingHelpRequired(rs.getBoolean("A.IS_WRITING_HELP_REQUIRED"));
				btDoAttributeSpecVO.setPropogatable(rs.getBoolean("A.IS_PROPOGATABLE"));
				btDoAttributeSpecVO.setCompareKeyForCUD(rs.getBoolean("A.IS_COMPARE_KEY_FOR_CUD"));
				btDoAttributeSpecVO.setAutoTranslationRequired(rs.getBoolean("A.IS_COMPARE_KEY_FOR_CUD"));
				btDoAttributeSpecVO.setEnforceOrgLocaleEntry(rs.getBoolean("A.IS_COMPARE_KEY_FOR_CUD"));
				btDoAttributeSpecVO.setEnforceGlobalLocaleEntry(rs.getBoolean("A.IS_COMPARE_KEY_FOR_CUD"));
				btDoAttributeSpecVO.setVisibleInAudit(rs.getBoolean("IS_VISIBLE_IN_AUDIT"));
				btDoAttributeSpecVO.setAuditAttributePathExpn(rs.getString("AUDIT_ATTR_PATH_EXPN"));
				btDoAttributeSpecVO.setAutoTranslationRequired(rs.getBoolean("IS_G11N_AUTO_TRANSLATION_REQD"));
				btDoAttributeSpecVO.setEnforceOrgLocaleEntry(rs.getBoolean("IS_G11N_ORG_LCL_ENTRY_ENFORCED"));
				btDoAttributeSpecVO.setEnforceGlobalLocaleEntry(rs.getBoolean("IS_G11N_GLOBAL_LCL_ENTRY_ENFORCED"));
				attributeSpecVOs.add(btDoAttributeSpecVO);
				idToAttrMap.put(btDoAttributeSpecVO.getTemplateDOACode(), btDoAttributeSpecVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// For all the ids get the country specific metadata
//		populateCountrySpecificDOAMeta(idToAttrMap.keySet(), idToAttrMap);
		return attributeSpecVOs;
	}

	public BtDoSpecVO getBtDoSpec(String baseTemplateId, String doName) {
		PreparedStatement ps;
		BtDoSpecVO btDoSpecVO = null;
		String sql = "SELECT "
				  +" *, "
				  +" B.MT_PROCESS_ELEMENT_CODE_FK_ID "
					  +" FROM T_BT_ADM_DO_SPEC A "
					  +" JOIN T_BT_ADM_PROCESS_ELEMENT B ON A.BT_PROCESS_ELEMENT_CODE_FK_ID = B.TEMPLATE_PROCESS_ELEMENT_CODE_PK_ID "
					  +" JOIN T_MT_ADM_PROCESS_ELEMENT M ON M.META_PROCESS_ELEMENT_CODE_PK_ID = B.MT_PROCESS_ELEMENT_CODE_FK_ID "
					  +" WHERE A.BASE_TEMPLATE_CODE_FK_ID = ? AND "
					  +"  A.DOMAIN_OBJECT_CODE_FK_ID = ?; ";
		try{
			Connection conn = getConnection();
			ps = conn.prepareStatement(sql);
			LOG.debug(sql);
			ps.setString(1, baseTemplateId);
			ps.setString(2, doName);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				btDoSpecVO = new BtDoSpecVO();
				btDoSpecVO.setVersionId(convertByteToString(rs.getBytes("A.VERSION_ID")));
				btDoSpecVO.setTemplateDOCode((rs.getString("A.TEMPLATE_DOMAIN_OBJECT_CODE_PK_ID")));
				btDoSpecVO.setBaseTemplateId(rs.getString("A.BASE_TEMPLATE_CODE_FK_ID"));
				btDoSpecVO.setProcessCode(rs.getString("A.PROCESS_CODE_FK_ID"));
				btDoSpecVO.setDomainObjectCode(rs.getString("A.DOMAIN_OBJECT_CODE_FK_ID"));
				btDoSpecVO.setBtPE(rs.getString("A.BT_PROCESS_ELEMENT_CODE_FK_ID"));
				btDoSpecVO.setMtPE(rs.getString("B.MT_PROCESS_ELEMENT_CODE_FK_ID"));
				try{
					//btDoSpecVO.setParentMtPE(rs.getString("M.SLF_PRNT_MT_PROCESS_ELEMENT_CODE_FK_ID"));
				}catch (Exception e){

				}
				try{
					//btDoSpecVO.setParentMtPE(rs.getString("M.SLF_PRNT_MT_PROCESS_ELEMENT_CODE_FK_ID"));
				}catch (Exception e){

				}
				btDoSpecVO.setBtDOName(rs.getString("BT_DO_SPEC_NAME_G11N_BIG_TXT"));
				btDoSpecVO.setDoTypeCode(rs.getString("A.DO_TYPE_CODE_FK_ID"));
				btDoSpecVO.setDbTableName(rs.getString("A.DB_TABLE_NAME_TXT"));
				btDoSpecVO.setDoSummaryTitle(rs.getString("A.DO_SUMMARY_TITLE_TEXT_G11N_BIG_TXT"));
				btDoSpecVO.setDoSummaryTitleIcon(rs.getString("A.DO_SUMMARY_TITLE_ICON_CODE_FK_ID"));
				btDoSpecVO.setDoDetailsTitle(rs.getString("DO_DETAILS_TITLE_TEXT_G11N_BIG_TXT"));
				btDoSpecVO.setDoHelpText(rs.getString("DO_HELP_TEXT_G11N_BIG_TXT"));
				btDoSpecVO.setCountOfRecordsSingularTitle(rs.getString("COUNT_OF_RECORDS_SINGULAR_TITLE_G11N_BIG_TXT"));
				btDoSpecVO.setCountOfRecordsPluralTitle(rs.getString("COUNT_OF_RECORDS_PLURAL_TITLE_G11N_BIG_TXT"));
				btDoSpecVO.setDoCustomizableFlag(rs.getBoolean("A.IS_DO_CUSTOMIZABLE_FLAG"));
				btDoSpecVO.setUpdateInline(rs.getBoolean("A.IS_UPDATE_INLINE"));
//				btDoSpecVO.setDoAccessControlGovernedFlag(rs.getBoolean("A.IS_DO_ACCESS_CONTROL_GOVERNED_FLAG"));
//				btDoSpecVO.setSubjectPersonDoaFullPath(rs.getString("A.SUBJECT_PERSON_DOA_FULL_PATH_TXT"));
			}
		}catch(Exception e){
			e.printStackTrace();
			LOG.error(sql);
			throw new RuntimeException(e);
		}
		return btDoSpecVO;
	}
	@Override
	public MtDomainObjectVO getMtDomainObjectByDo(String doName) {
		PreparedStatement ps;
		MtDomainObjectVO mtDomainObjectVO = null;
		String sql = "SELECT *, DOMAIN_OBJECT_NAME_G11N_BIG_TXT FROM T_MT_ADM_DOMAIN_OBJECT WHERE DOMAIN_OBJECT_CODE_PK_ID = ?;";
		try{
			Connection conn = getConnection();
			ps = conn.prepareStatement(sql);
			LOG.debug(sql);
			ps.setString(1, doName);
			ResultSet rs = ps.executeQuery();  
			while(rs.next()){
				mtDomainObjectVO = new MtDomainObjectVO();
				mtDomainObjectVO.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
				mtDomainObjectVO.setDomainObjectCode((rs.getString("DOMAIN_OBJECT_CODE_PK_ID")));
				mtDomainObjectVO.setDomainObjectName(rs.getString("DOMAIN_OBJECT_NAME_G11N_BIG_TXT"));
				mtDomainObjectVO.setDomainObjectIcon(rs.getString("DOMAIN_OBJECT_ICON_CODE_FK_ID"));
//				mtDomainObjectVO.setBaseObjectId(rs.getString("REF_PROCESS_CODE_FK_ID"));
				mtDomainObjectVO.setDbTableName(rs.getString("DB_TABLE_NAME_TXT"));
				mtDomainObjectVO.setDoTypeCode(rs.getString("DO_TYPE_CODE_FK_ID"));
				mtDomainObjectVO.setCustomizable(rs.getBoolean("IS_CUSTOMIZABLE"));
//				mtDomainObjectVO.setCommon(rs.getBoolean("IS_COMMON"));
//				mtDomainObjectVO.setAccessControlGoverned(rs.getBoolean("IS_ACCESS_CONTROL_GOVERNED"));
				mtDomainObjectVO.setSubjectPersonDoaFullPath(rs.getString("SUBJECT_PERSON_DOA_FULL_PATH_EXPN"));
				mtDomainObjectVO.setActivityPE(rs.getString("ACTIVITY_LOG_META_PROCESS_ELEMENT_CODE_FK_ID"));
				mtDomainObjectVO.setActivityDetailsPE(rs.getString("ACTIVITY_LOG_DETAILS_META_PROCESS_ELEMENT_CODE_FK_ID"));
				mtDomainObjectVO.setTagPE(rs.getString("TAG_META_PROCESS_ELEMENT_CODE_FK_ID"));
				mtDomainObjectVO.setBookmarkPE(rs.getString("BOOKMARK_META_PROCESS_ELEMENT_CODE_FK_ID"));
				mtDomainObjectVO.setFeaturedPE(rs.getString("FEATURED_META_PROCESS_ELEMENT_CODE_FK_ID"));
//				mtDomainObjectVO.setRowIntermediateMtPE(rs.getString("AM_ROW_INT_MTPE_CODE_FK_ID"));
//				mtDomainObjectVO.setRowResolvedMtPE(rs.getString("AM_ROW_RSLVD_MTPE_CODE_FK_ID"));
//				mtDomainObjectVO.setColResolvedMtPE(rs.getString("AM_COL_RSLVD_MTPE_CODE_FK_ID"));
				mtDomainObjectVO.setEndorseMtPE(rs.getString("ENDORSEMENT_META_PROCESS_ELEMENT_CODE_FK_ID"));
			}
		}catch(Exception e){
			e.printStackTrace();
			LOG.error(sql);
			throw new RuntimeException(e);
		}
		return mtDomainObjectVO;
	}
	
	@Override
	public List<MtDoAttributeVO> getMtDoAttributeByDo(String domainObjectCodePkId) {
		PreparedStatement ps;
		MtDoAttributeVO mtDoAttributeVO = null;
		List<MtDoAttributeVO> mtDoAttributeVOs = new ArrayList<>();
		String sql = "SELECT * FROM T_MT_ADM_DO_ATTRIBUTE WHERE DOMAIN_OBJECT_CODE_FK_ID = ?;";
		try{
			Connection conn = getConnection();
			ps = conn.prepareStatement(sql);
			LOG.debug(sql);
			ps.setString(1, domainObjectCodePkId);
			LOG.debug(domainObjectCodePkId);
			ResultSet rs = ps.executeQuery();  
			while(rs.next()){
				mtDoAttributeVO = new MtDoAttributeVO();
				mtDoAttributeVO.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
				mtDoAttributeVO.setDoAttributeCode(rs.getString("DOMAIN_OBJECT_ATTRIBUTE_CODE_PK_ID"));
				mtDoAttributeVO.setDoAttributeName(rs.getString("DO_ATTRIBUTE_NAME_G11N_BIG_TXT"));
				mtDoAttributeVO.setDoAttributeIcon(rs.getString("DO_ATTRIBUTE_ICON_CODE_FK_ID"));
				mtDoAttributeVO.setDomainObjectCode(rs.getString("DOMAIN_OBJECT_CODE_FK_ID"));
//				mtDoAttributeVO.setBaseObjectId(rs.getString("REF_PROCESS_CODE_FK_ID"));
				mtDoAttributeVO.setDbTableName(rs.getString("DB_TABLE_NAME_TXT"));
				mtDoAttributeVO.setDbColumnName(rs.getString("DB_COLUMN_NAME_TXT"));
				mtDoAttributeVO.setDbDataTypeCode(rs.getString("DB_DATA_TYPE_CODE_FK_ID"));
				mtDoAttributeVO.setFunctionalPrimaryKey(rs.getBoolean("IS_FUNCTIONAL_PRIMARY_KEY"));
				mtDoAttributeVO.setDbPrimaryKey(rs.getBoolean("IS_DB_PRIMARY_KEY"));
				mtDoAttributeVO.setDependencyKey(rs.getBoolean("IS_DEPENDENCY_KEY"));
				mtDoAttributeVO.setMandatory(rs.getBoolean("IS_MANDATORY"));
				mtDoAttributeVO.setCustomizable(rs.getBoolean("IS_CUSTOMIZABLE"));
				mtDoAttributeVO.setGlocalized(rs.getBoolean("IS_GLOCALIZED"));
				mtDoAttributeVO.setSelfJoinParent(rs.getBoolean("IS_SELF_JOIN_PARENT"));
				mtDoAttributeVO.setRelation(rs.getBoolean("IS_RELATION"));
				mtDoAttributeVO.setHierarchy(rs.getBoolean("IS_HIERARCHY"));
				mtDoAttributeVO.setReportable(rs.getBoolean("IS_REPORTABLE"));
				mtDoAttributeVO.setNonVersionTrackable(rs.getBoolean("IS_NON_VERSION_TRACKABLE"));
				mtDoAttributeVO.setLocalSearchable(rs.getBoolean("IS_LOCAL_SEARCHABLE"));
				mtDoAttributeVO.setContainerBlobDbColumnName(rs.getString("CONTAINER_BLOB_DB_COLUMN_NAME_TXT"));
				mtDoAttributeVO.setToDomainObjectCode(rs.getString("TO_DOMAIN_OBJECT_CODE_FK_ID"));
				mtDoAttributeVO.setToDbTableName(rs.getString("TO_DB_TABLE_NAME_TXT"));
				mtDoAttributeVO.setToDoAttributeCode(rs.getString("TO_DO_ATTRIBUTE_CODE_FK_ID"));
				mtDoAttributeVO.setToDbColumnName(rs.getString("TO_DB_COLUMN_NAME_TXT"));
				mtDoAttributeVO.setCardinalityManyAllowed(rs.getBoolean("IS_CARDINALITY_MANY_ALLOWED"));
//				mtDoAttributeVO.setJsonName(rs.getString("JSON_NAME_TXT"));
				mtDoAttributeVOs.add(mtDoAttributeVO);
			}
		}catch(Exception e){
			e.printStackTrace();
			LOG.error(sql);
			throw new RuntimeException(e);
			}
		return mtDoAttributeVOs;
	}
	@Override
	public List<BtDoAttributeSpecVO> getBtDoAttributeSpecListByMtDoa(String doaCodeFkId) {
		List<BtDoAttributeSpecVO> attributeSpecVOs = new ArrayList<>();
		String sql = "SELECT *, C.MT_PROCESS_ELEMENT_CODE_FK_ID  FROM T_BT_ADM_DO_ATTRIBUTE_SPEC A LEFT JOIN T_BT_ADM_DO_SPEC B ON A.BT_DO_SPEC_CODE_FK_ID = B.TEMPLATE_DOMAIN_OBJECT_CODE_PK_ID LEFT JOIN T_BT_ADM_PROCESS_ELEMENT C ON B.BT_PROCESS_ELEMENT_CODE_FK_ID = C.TEMPLATE_PROCESS_ELEMENT_CODE_PK_ID WHERE A.DO_ATTRIBUTE_CODE_FK_ID = ?;";
		try{
			PreparedStatement ps;
			Connection conn = getConnection();
			ps = conn.prepareStatement(sql);
			LOG.debug(sql);
			ps.setString(1, doaCodeFkId);
			ResultSet rs = ps.executeQuery();
			attributeSpecVOs = getAttributeSpecVOListFromResultSet(rs);
		}catch(Exception e){
			e.printStackTrace();
			LOG.error(sql);
			throw new RuntimeException(e);
		}
		return attributeSpecVOs;
	}
	@Override
	public List<AttributePathDetails> getControlAttributePaths(String baseTemplateId, String mtPE) {
		String SQL = "SELECT "
				  +"TXN.CONTROL_TXN_PK_ID as 'id', "
				  +"TXN.CONTROL_ATTRIBUTE_PATH_LNG_TXT as 'attributePath', "
				  +"TXN.CONTROL_ATTRIBUTE_AGGREGATE_FUNCTION_CODE_FK_ID as 'aggregateFunction', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlTxn.AggregateHavingExpression' AS CHAR) AS 'AggregateHavingExpression', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlTxn.AggregateWhereExpression' AS CHAR) AS 'AggregateWhereExpression', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlTxn.MobileControlAttributePath' AS CHAR) AS 'mobileAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlTxn.ControlTxn.PhabletControlAttributePath' AS CHAR) AS 'phablateAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlTxn.ControlTxn.MiniTabletControlAttributePath' AS CHAR) AS 'miniTabletAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlTxn.ControlTxn.TabletControlAttributePath' AS CHAR) AS 'tabletAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlTxn.ControlTxn.WebControlAttributePath' AS CHAR) AS 'webAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlTxn.StickyHeaderSort' AS BINARY)         AS 'isStickyHeaderSort', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlTxn.OrderByDOAValues' AS CHAR)            AS 'orderBYValues', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlTxn.StickyHeaderSortSequence' AS INTEGER) AS 'stickyHeaderSortSeq' "
						+"FROM T_UI_ADM_CONTROL_TXN TXN "
						  +"LEFT JOIN T_UI_ADM_CANVAS_TXN CTXN ON CTXN.CANVAS_TXN_PK_ID = TXN.CANVAS_TXN_FK_ID "
						+"WHERE CTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND CTXN.MT_PROCESS_ELEMENT_CODE_FK_ID = ?; ";
		return getAttributePathSelect(baseTemplateId, mtPE, SQL, true, false);
	}
	@Override
	public List<AttributePathDetails> getRelationControlAttributePaths(String baseTemplateId, String mtPE) {
		String SQL = "SELECT "
				+"TXN.CONTROL_RC_TXN_ID as 'id', "
				+"TXN.RCCANVAS_CONTROL_ATTRIBUTE_PATH_LNG_TXT as 'attributePath', "
//			  +"TXN.RCCANVAS_CONTROL_ATTRIBUTE_AGGREGATE_FUNCTION_CODE_FK_ID as 'aggregateFunction', "
//			  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlRCTxn.AggregateHavingExpression' AS CHAR) AS 'AggregateHavingExpression', "
//			  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlRCTxn.AggregateWhereExpression' AS CHAR) AS 'AggregateWhereExpression', "
			  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlRCTxn.MobileControlAttributePath' AS CHAR) AS 'mobileAttributePath', "
			  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlRCTxn.PhabletControlAttributePath' AS CHAR) AS 'phablateAttributePath', "
			  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlRCTxn.MiniTabletControlAttributePath' AS CHAR) AS 'miniTabletAttributePath', "
			  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlRCTxn.TabletControlAttributePath' AS CHAR) AS 'tabletAttributePath', "
			  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlRCTxn.WebControlAttributePath' AS CHAR) AS 'webAttributePath', "
			  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlRCTxn.StickyHeaderSort' AS BINARY)         AS 'isStickyHeaderSort', "
			  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlRCTxn.OrderByDOAValues' AS CHAR)            AS 'orderBYValues', "
			  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlRCTxn.StickyHeaderSortSequence' AS INTEGER) AS 'stickyHeaderSortSeq' "
						+"FROM T_UI_ADM_CONTROL_RC_TXN TXN "
							+"LEFT JOIN T_UI_ADM_CONTROL_TXN CLTXN ON TXN.CONTROL_TXN_FK_ID = CLTXN.CONTROL_TXN_PK_ID "
							+"LEFT JOIN T_UI_ADM_CANVAS_TXN CTXN ON CTXN.CANVAS_TXN_PK_ID = CLTXN.CANVAS_TXN_FK_ID "
						+"WHERE CTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND CTXN.MT_PROCESS_ELEMENT_CODE_FK_ID = ?;";
		return getAttributePathSelect(baseTemplateId, mtPE, SQL, false, false);
	}
	@Override
	public List<AttributePathDetails> getHierarchyControlAttributePaths(String baseTemplateId, String mtPE) {
		String SQL = "SELECT "
				  +"TXN.CONTROL_HC_TXN_ID as 'id', "
				  +"TXN.HCCANVAS_CONTROL_ATTRIBUTE_PATH_LNG_TXT as 'attributePath', "
//				  +"TXN.HCCANVAS_CONTROL_ATTRIBUTE_AGGREGATE_FUNCTION_CODE_FK_ID as 'aggregateFunction', "
//				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlHCTxn.AggregateHavingExpression' AS CHAR) AS 'AggregateHavingExpression', "
//				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlHCTxn.AggregateWhereExpression' AS CHAR) AS 'AggregateWhereExpression', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlHCTxn.MobileControlAttributePath' AS CHAR) AS 'mobileAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlHCTxn.PhabletControlAttributePath' AS CHAR) AS 'phablateAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlHCTxn.MiniTabletControlAttributePath' AS CHAR) AS 'miniTabletAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlHCTxn.TabletControlAttributePath' AS CHAR) AS 'tabletAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'ControlHCTxn.WebControlAttributePath' AS CHAR) AS 'webAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlHCTxn.StickyHeaderSort' AS BINARY)         AS 'isStickyHeaderSort', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlHCTxn.OrderByDOAValues' AS CHAR)            AS 'orderBYValues', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'ControlHCTxn.StickyHeaderSortSequence' AS INTEGER) AS 'stickyHeaderSortSeq' "
							+"FROM T_UI_ADM_CONTROL_HC_TXN TXN "
								+"LEFT JOIN T_UI_ADM_CONTROL_TXN CLTXN ON TXN.CONTROL_TXN_FK_ID = CLTXN.CONTROL_TXN_PK_ID "
								+"LEFT JOIN T_UI_ADM_CANVAS_TXN CTXN ON CTXN.CANVAS_TXN_PK_ID = CLTXN.CANVAS_TXN_FK_ID "
							+"WHERE CTXN.BASE_TEMPLATE_CODE_FK_ID = ? AND CTXN.MT_PROCESS_ELEMENT_CODE_FK_ID = ?;";
		return getAttributePathSelect(baseTemplateId, mtPE, SQL, false, false);
	}
	@Override
	public List<AttributePathDetails> getExternalAPIAttributePaths(String baseTemplateId, String mtPE) {
		String SQL = "SELECT "
				+"TXN.BT_EXT_API_DOA_PK_ID as 'id', "
				  +"TXN.ATTRIBUTE_PATH_LNG_TXT as 'attributePath', "
				  +"TXN.AGGREGATE_FUNCTION_CODE_FK_ID as 'aggregateFunction', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'TemplateExternalAPIDOA.AggregateHavingExpression' AS CHAR) AS 'AggregateHavingExpression', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'TemplateExternalAPIDOA.AggregateWhereExpression' AS CHAR) AS 'AggregateWhereExpression', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'TemplateExternalAPIDOA.MobileControlAttributePath' AS CHAR) AS 'mobileAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'TemplateExternalAPIDOA.PhabletControlAttributePath' AS CHAR) AS 'phablateAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'TemplateExternalAPIDOA.MiniTabletControlAttributePath' AS CHAR) AS 'miniTabletAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'TemplateExternalAPIDOA.TabletControlAttributePath' AS CHAR) AS 'tabletAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_DEPENDENT_BLOB, 'TemplateExternalAPIDOA.WebControlAttributePath' AS CHAR) AS 'webAttributePath', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'TemplateExternalAPIDOA.StickyHeaderSort' AS BINARY)         AS 'isStickyHeaderSort', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'TemplateExternalAPIDOA.OrderByDOAValues' AS CHAR)            AS 'orderBYValues', "
				  +"COLUMN_GET(TXN.DEVICE_INDEPENDENT_BLOB, 'TemplateExternalAPIDOA.StickyHeaderSortSequence' AS INTEGER) AS 'stickyHeaderSortSeq' "
							+"FROM T_BT_ADM_EXT_API_DOA TXN "
							+"WHERE TXN.BASE_TEMPLATE_DEP_CODE_FK_ID = ? AND TXN.MT_PROCESS_ELEMENT_CODE_FK_ID = ?;";
		return getAttributePathSelect(baseTemplateId, mtPE, SQL, false, true);
	}
	/**
	 * @param baseTemplateId
	 * @param mtPE
	 * @param SQL
	 * @return
	 */
	private List<AttributePathDetails> getAttributePathSelect(String baseTemplateId, String mtPE, String SQL, boolean isControlAttribute, boolean isExtrnalAPIAttribute) {
		List<AttributePathDetails> result = new ArrayList<>();
		List<Object> parameters = new ArrayList<>();
		parameters.add(baseTemplateId);
		parameters.add(mtPE);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				AttributePathDetails attributePathSelect = new AttributePathDetails();
				attributePathSelect.setId(Util.convertByteToString(rs.getBytes("id")));
				attributePathSelect.setAttributePath(rs.getString("attributePath"));
				if(isControlAttribute || isExtrnalAPIAttribute){
//					attributePathSelect.setAggregateFunction(rs.getString("aggregateFunction"));
					attributePathSelect.setAggregateFunctionHavingClause(rs.getString("AggregateHavingExpression"));
					attributePathSelect.setAggregateFunctionWhereClause(rs.getString("AggregateWhereExpression"));
//					if(StringUtil.isDefined(attributePathSelect.getAggregateFunction())){
//						/* make a select statement */
//						List<GroupByAttributeDetails> groupByAttributeDetails = getGroupByAttributeDetails(attributePathSelect.getId(), isControlAttribute, isExtrnalAPIAttribute);
//						attributePathSelect.setGroupByAttributes(groupByAttributeDetails);
//					}
				}
				attributePathSelect.setStickyHeaderSort(rs.getBoolean("isStickyHeaderSort"));
				attributePathSelect.setOrderBYValues(rs.getString("orderBYValues"));
				attributePathSelect.setStickyHeaderSortSeq(rs.getInt("stickyHeaderSortSeq"));
				result.add(attributePathSelect);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * @param id
	 * @param isControlAttribute
	 * @param isExtrnalAPIAttribute
	 * @return
	 */
	private List<GroupByAttributeDetails> getGroupByAttributeDetails(String id, boolean isControlAttribute,
			boolean isExtrnalAPIAttribute) {
		List<GroupByAttributeDetails> gbAttrDetails = new ArrayList<>();
		String SQL = null;
		if(isControlAttribute){
			SQL ="SELECT "
					  +"TUACT.CONTROL_ATTRIBUTE_PATH_LNG_TXT as 'groupByAttributePath', "
					  +"TUACGBCT.SEQUENCE_NUMBER_POS_INT as 'seqNumber' "
					  +"FROM T_UI_ADM_CONTROL_GROUP_BY_CONTROL_TXN TUACGBCT LEFT JOIN T_UI_ADM_CONTROL_TXN TUACT "
					  +"    ON TUACGBCT.GROUP_BY_CONTROL_TXN_FK_ID = TUACT.CONTROL_TXN_PK_ID  "
					  +"WHERE TUACGBCT.CONTROL_TXN_FK_ID = x?;";
		}else if(isExtrnalAPIAttribute){
			/* TODO add the SELECT statement corresponding to exteranlAPI here */ 
			SQL = "SELECT "
					+"TBAEAGBDT.GROUP_BY_ATTRIBUTE_PATH_LNG_TXT AS 'groupByAttributePath', "
					+"TBAEAGBDT.SEQUENCE_NUMBER_POS_INT     AS 'seqNumber' "
		  +"FROM T_BT_ADM_EXTERNAL_API_GROUP_BY_DOA_TXN TBAEAGBDT "
		  +"WHERE TBAEAGBDT.BT_EXT_API_DOA_FK_ID = x?;"; 
		}
		List<Object> parameters = new ArrayList<>();
		parameters.add(id);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				GroupByAttributeDetails attributeDetails = new GroupByAttributeDetails();
				attributeDetails.setGroupByAttributePath(rs.getString("groupByAttributePath"));
				attributeDetails.setGroupByAttributeSeqNumber(rs.getInt("seqNumber"));
				gbAttrDetails.add(attributeDetails);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return gbAttrDetails;
	}
	@Override
	public String getDOCodeByMTPE(String mtPE) {
		String result = null;
		String SQL = "SELECT MT_DOMAIN_OBJECT_CODE_FK_ID as doCode "
						+"FROM T_MT_ADM_PROCESS_ELEMENT "
						+"WHERE META_PROCESS_ELEMENT_CODE_PK_ID = ? AND RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		parameters.add(mtPE);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			if(rs.next()){
				result = rs.getString("doCode");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.tapplent.platformutility.metadata.dao.MetadataDAO#getDataActions()
	 */
	@Override
	public List<DataActionsVO> getDataActions() {
		List<DataActionsVO> result = new ArrayList<>();
		String SQL = "SELECT * FROM T_PFM_ADM_DATA_DRIVEN_ACTION;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				DataActionsVO actionsVO = new DataActionsVO();
				actionsVO.setDataDrivenActionsId(Util.convertByteToString(rs.getBytes("DATA_DRIVEN_ACTION_PK_ID")));
				actionsVO.setActiveState(rs.getString("DA_ACTIVE_STATE_CODE_FK_ID"));
				actionsVO.setSaveState(rs.getString("DA_SAVE_STATE_CODE_FK_ID"));
				actionsVO.setDeleted(rs.getBoolean("IS_DA_DELETED"));
				actionsVO.setWorkFlowTransactionStatusCode(rs.getString("DA_WORKFLOW_TRANSACTION_STATUS_CODE_FK_ID"));
				actionsVO.setActionCode(rs.getString("ACTION_MASTER_CODE_FK_ID"));
				actionsVO.setApplicable(rs.getBoolean("IS_APPLICABLE"));
				result.add(actionsVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.tapplent.platformutility.metadata.dao.MetadataDAO#getBTDOAtrributeList()
	 */
	@Override
	public List<BtDoAttributeSpecVO> getBTDOAtrributeList() {
		List<BtDoAttributeSpecVO> attributeSpecVOs = new ArrayList<>();
		String SQL = "SELECT *, C.MT_PROCESS_ELEMENT_CODE_FK_ID "
				+" FROM T_BT_ADM_DO_ATTRIBUTE_SPEC A LEFT JOIN T_BT_ADM_DO_SPEC B ON A.BT_DO_SPEC_CODE_FK_ID = B.TEMPLATE_DOMAIN_OBJECT_CODE_PK_ID LEFT JOIN T_BT_ADM_PROCESS_ELEMENT C ON B.BT_PROCESS_ELEMENT_CODE_FK_ID = C.TEMPLATE_PROCESS_ELEMENT_CODE_PK_ID ;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		attributeSpecVOs = getAttributeSpecVOListFromResultSet(rs);
		return attributeSpecVOs;
	}

	@Override
	public Map<String, IconVO> getIconMap() {
		Map<String, IconVO> result = new HashMap<>();
		String SQL = "SELECT * FROM T_UI_LKP_ICON_MASTER;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()){
				IconVO iconVO = new IconVO();
				iconVO.setCode(rs.getString("ICON_CODE_PK_ID"));
				iconVO.setIconNameG11nBigTxt(Util.getG11nValue(rs.getString("ICON_NAME_G11N_BIG_TXT"), null));
				iconVO.setIconPurposeTxt(rs.getString("ICON_PURPOSE_TXT"));
				iconVO.setClassName(rs.getString("ICON_CLASS_NAME_TXT"));
				iconVO.setUnicode(rs.getString("ICON_UNICODE_TXT"));
				result.put(iconVO.getCode(),iconVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

    @Override
    public List<MTPEVO> getMtPEMap() {
        List<MTPEVO> result = new ArrayList<>();
        String SQL = "SELECT * FROM T_MT_ADM_PROCESS_ELEMENT;";
        List<Object> parameters = new ArrayList<>();
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while (rs.next()){
                MTPEVO mtPE = new MTPEVO();
                mtPE.setMetaProcessElementCodePkId(rs.getString("META_PROCESS_ELEMENT_CODE_PK_ID"));
                mtPE.setMtProcessElementNameG11nBigTxt(rs.getString("MT_PROCESS_ELEMENT_NAME_G11N_BIG_TXT"));
                mtPE.setMtProcessElementIconCodeFkId(rs.getString("MT_PROCESS_ELEMENT_ICON_CODE_FK_ID"));
                mtPE.setMtProcessCodeFkId(rs.getString("MT_PROCESS_CODE_FK_ID"));
                mtPE.setMtDomainObjectCodeFkId(rs.getString("MT_DOMAIN_OBJECT_CODE_FK_ID"));
               // mtPE.setParentMtProcessElementCodeFkId(rs.getString("SLF_PRNT_MT_PROCESS_ELEMENT_CODE_FK_ID"));
                result.add(mtPE);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

	@Override
	public List<String> getDependencyKeyRelatedPEs(String mtPE) {
		List<String> fkPEs = new ArrayList<>();
		String SQL = "SELECT DISTINCT META_PROCESS_ELEMENT_CODE_PK_ID FROM T_MT_ADM_PROCESS_ELEMENT PE INNER JOIN T_MT_ADM_DO_ATTRIBUTE DOA ON PE.MT_DOMAIN_OBJECT_CODE_FK_ID = DOA.DOMAIN_OBJECT_CODE_FK_ID WHERE TO_DO_ATTRIBUTE_CODE_FK_ID = ? AND IS_DEPENDENCY_KEY;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(mtPE);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()){
            	fkPEs.add(rs.getString(1));
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return fkPEs;
	}

	@Override
	public List<String> getForeignKeyRelatedPEs(String mtPE) {
		List<String> fkPEs = new ArrayList<>();
		String SQL = "SELECT DISTINCT META_PROCESS_ELEMENT_CODE_PK_ID FROM T_MT_ADM_PROCESS_ELEMENT PE INNER JOIN T_MT_ADM_DO_ATTRIBUTE DOA ON PE.MT_DOMAIN_OBJECT_CODE_FK_ID = DOA.DOMAIN_OBJECT_CODE_FK_ID WHERE TO_DO_ATTRIBUTE_CODE_FK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(mtPE);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()){
				fkPEs.add(rs.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return fkPEs;
	}

	@Override
	public List<AnalyticsDOAControlMap> getAnalyticsDoaToControlMap() {
		List<AnalyticsDOAControlMap> result = new ArrayList<>();
		String SQL = "";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()){
				AnalyticsDOAControlMap analyticsDOAControlMap = new AnalyticsDOAControlMap();
				analyticsDOAControlMap.setPkID(Util.convertByteToString(rs.getBytes("")));
				analyticsDOAControlMap.setAnalyticsDOA(rs.getString(""));
				analyticsDOAControlMap.setControlInstanceID(Util.convertByteToString(rs.getBytes("")));
				result.add(analyticsDOAControlMap);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public TemplateMapperVO getTemplateMapperByMtPE(String mtPE) {
		String SQL = "SELECT * FROM T_PFM_ADM_TEMPLATE_MAPPER WHERE MT_PE_CODE_FK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(mtPE);
		ResultSet rs = executeSQL(SQL, parameters);
		TemplateMapperVO templateMapperVO = null;
		try {
			while (rs.next()){
				templateMapperVO = new TemplateMapperVO();
				templateMapperVO.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
				templateMapperVO.setTemplateMapperPkId(convertByteToString(rs.getBytes("TEMPLATE_MAPPER_PK_ID")));
				templateMapperVO.setPfmGroupFkId(convertByteToString(rs.getBytes("PFM_GROUP_FK_ID")));
				templateMapperVO.setMtPeCodeFkId(rs.getString("MT_PE_CODE_FK_ID"));
				templateMapperVO.setBaseTemplateCodeFkId(rs.getString("BASE_TEMPLATE_CODE_FK_ID"));
				templateMapperVO.setMetaDefault(rs.getBoolean("IS_META_DEFAULT"));
				templateMapperVO.setReportDefault(rs.getBoolean("IS_REPORT_DEFAULT"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return templateMapperVO;
	}

	@Override
	public Map<String, PropertyControlMaster> getPropertyControlMap() {
		Map<String, PropertyControlMaster> result = new HashMap<>();
		String SQL = "SELECT * FROM T_UI_TAP_PROPERTY_CONTROL_MASTER;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				PropertyControlMaster master = new PropertyControlMaster();
				master.setPropertyControlMasterCodePkId(rs.getString("PROPERTY_CONTROL_MASTER_CODE_PK_ID"));
				master.setPropertyControlMasterNameG11nBigTxt(Util.getG11nValue(rs.getString("PROPERTY_CONTROL_MASTER_NAME_G11N_BIG_TXT"), null));
				master.setPropertyControlMasterIconCodeFkId(rs.getString("PROPERTY_CONTROL_MASTER_ICON_CODE_FK_ID"));
				master.setAppSpecific(rs.getBoolean("IS_APP_SPECIFIC"));
				master.setMtPESpecific(rs.getBoolean("IS_MT_PE_SPECIFIC"));
				master.setAttributeSpecific(rs.getBoolean("IS_ATTRIBUTE_SPECIFIC"));
				master.setRecordSpecific(rs.getBoolean("IS_RECORD_SPECIFIC"));
				result.put(master.getPropertyControlMasterCodePkId(), master);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public CountrySpecificDOAMetaVO getCountrySpecificDOAMeta(String btDoSpecId, String baseCountryCodeFkId) {
		CountrySpecificDOAMetaVO metaVO = null;
		String SQL = "SELECT * FROM T_BT_ADM_COUNTRY_SPECIFIC_DO_ATTR_OVERRIDE WHERE BT_DOA_CODE_FK_ID = ? AND COUNTRY_CODE_FK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(btDoSpecId);
		parameters.add(baseCountryCodeFkId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()) {
                metaVO = new CountrySpecificDOAMetaVO();
                metaVO.setBtDoaCountrySpecificDisplayOverridePkId(Util.convertByteToString(rs.getBytes("BT_DOA_COUNTRY_SPECIFIC_DISPLAY_OVERRIDE_PK_ID")));
                metaVO.setBtDoaCodeFkId(rs.getString("BT_DOA_CODE_FK_ID"));
                metaVO.setCountryCodeFkId(rs.getString("COUNTRY_CODE_FK_ID"));
                metaVO.setDoaDisplayNameOverrideG11nBigTxt(rs.getString("DOA_DISPLAY_NAME_OVERRIDE_G11N_BIG_TXT"));
                metaVO.setDoaDisplayIconCodeFkId(rs.getString("DOA_DISPLAY_ICON_CODE_FK_ID"));
            }
            if (metaVO == null){
				metaVO = new CountrySpecificDOAMetaVO();
				metaVO.setDataNull(true);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return metaVO;
	}


	private void populateCountrySpecificDOAMeta(Set<String> doaList, Map<String, BtDoAttributeSpecVO> idToAttrMap) {
		if(doaList== null)
			return;
		StringBuilder SQL = new StringBuilder("SELECT * FROM T_BT_ADM_COUNTRY_SPECIFIC_DO_ATTR_OVERRIDE WHERE BT_DOA_CODE_FK_ID IN(");
		for(int i =0; i<doaList.size();i++){
			if(i!=0){
				SQL.append(",");
			}
			SQL.append("?");
		}
		if(doaList.size() == 0){
			SQL.append("null");
		}
		SQL.append(")");
		List<Object> parameters = new ArrayList<>();
		for(String doa : doaList){
			parameters.add(doa);
		}
		ResultSet rs = executeSQL(SQL.toString(), parameters);
		try {
			while (rs.next()) {
				CountrySpecificDOAMetaVO metaVO = new CountrySpecificDOAMetaVO();
				metaVO.setBtDoaCountrySpecificDisplayOverridePkId(Util.convertByteToString(rs.getBytes("BT_DOA_COUNTRY_SPECIFIC_DISPLAY_OVERRIDE_PK_ID")));
				metaVO.setBtDoaCodeFkId(rs.getString("BT_DOA_CODE_FK_ID"));
				metaVO.setCountryCodeFkId(rs.getString("COUNTRY_CODE_FK_ID"));
				metaVO.setDoaDisplayNameOverrideG11nBigTxt(rs.getString("DOA_DISPLAY_NAME_OVERRIDE_G11N_BIG_TXT"));
				metaVO.setDoaDisplayIconCodeFkId(rs.getString("DOA_DISPLAY_ICON_CODE_FK_ID"));
				metaVO.setVisible(rs.getBoolean("IS_VISIBLE"));
				metaVO.setEditable(rs.getBoolean("IS_EDITABLE"));
				metaVO.setSensitive(rs.getBoolean("IS_SENSITIVE"));
				idToAttrMap.get(metaVO.getBtDoaCodeFkId()).getCountrySpecificDOAMetaVOList().add(metaVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public AuditMsgFormat getAuditMsgFormatByMtPEAndAction(String activityLogActionCode, String mtPE) {
		AuditMsgFormat auditMsgFormat = new AuditMsgFormat();
		String sql = "SELECT * FROM T_PFM_ADM_AUDIT_MSG_FORMAT WHERE MT_PE_CODE_FK_ID = ? AND ACTION_VISUAL_MASTER_CODE_FK_ID = ?";
		List<Object> parameters = new ArrayList<>();
		parameters.add(mtPE);
		parameters.add(activityLogActionCode);
		ResultSet rs = executeSQL(sql, parameters);
		try {
			if (rs.next()) {
				auditMsgFormat = new AuditMsgFormat();
				auditMsgFormat.setVersionId(Util.convertByteToString(rs.getBytes("VERSION_ID")));
				auditMsgFormat.setAdtMsgFmtPkId(Util.convertByteToString(rs.getBytes("ADT_MSG_FMT_PK_ID")));
				auditMsgFormat.setMtPeCodeFkId(rs.getString("MT_PE_CODE_FK_ID"));
				auditMsgFormat.setMtPeCodeFkId(rs.getString("BT_CODE_FK_ID"));
				auditMsgFormat.setActivityLogActionCodeFkId(rs.getString("ACTION_VISUAL_MASTER_CODE_FK_ID"));
				auditMsgFormat.setMsgSyntaxYouInContextG11nBigTxt(rs.getString("MSG_SYNTAX_YOU_IN_CONTEXT_G11N_BIG_TXT"));
				auditMsgFormat.setMsgSyntaxYouOutOfContextG11nBigTxt(rs.getString("MSG_SYN_YOU_OUT_OF_CONTEXT_G11N_BIG_TXT"));
				auditMsgFormat.setMsgSyntaxThirdPersonInContextG11nBigTxt(rs.getString("MSG_SYNTAX_THIRD_PRSN_IN_CONTEXT_G11N_BIG_TXT"));
				auditMsgFormat.setMsgSyntaxThirdPersonOutOfContextG11nBigTxt(rs.getString("MSG_SYNTAX_THIRD_PRSN_OUT_OF_CONTEXT_G11N_BIG_TXT"));
				auditMsgFormat.setCreateDetails(rs.getBoolean("IS_CREATE_DETAILS"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return auditMsgFormat;	}

	@Override
	public String getSubjectUserId(StringBuilder sql, String dbDataTypeCode) {
		String subjectUserId = null;
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = super.executeSQL(sql.toString(), parameters);
		try {
			if (rs.next()) {
				if (dbDataTypeCode.equals("T_ID")) {
					subjectUserId = Util.convertByteToString(rs.getBytes(1));
				} else {
					subjectUserId = rs.getString(1);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return subjectUserId;
	}

	@Override
	public UiSettingVO getUiSetting() {
		UiSettingVO uiSettingVO = new UiSettingVO();
		String SQL = "SELECT\n" +
				"  *,\n" +
				"  COLUMN_JSON(UI_PROPERTY_BLOB) AS settings\n" +
				"FROM T_UI_ADM_SETTING\n" +
				"WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		int counter = 0;
		try {
			while(rs.next()){
				if(counter == 0){
					uiSettingVO.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
					uiSettingVO.setUiSettingPkId(convertByteToString(rs.getBytes("UI_SETTING_PK_ID")));
					uiSettingVO.setUiPropertyBlob(getBlobJson(rs.getBlob("settings")));
					counter++;
				}
				else{
					throw new Exception();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return uiSettingVO;

	}

	@Override
	public List<LayoutThemeMapperVO> getLayoutThemeMapperVOs() {
		List<LayoutThemeMapperVO> result = new ArrayList<>();
		String SQL = "SELECT * FROM T_UI_TAP_LAYOUT_THEME_MAPPER WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()) {
				LayoutThemeMapperVO layoutThemeMapperVO = new LayoutThemeMapperVO();
				layoutThemeMapperVO.setThemeMapperPkId(convertByteToString(rs.getBytes("LAYOUT_THEME_MAPPER_PK_ID")));
				layoutThemeMapperVO.setScreenMasterCode(rs.getString("SCREEN_MASTER_CODE_FK_ID"));
				layoutThemeMapperVO.setSectionMasterCode(rs.getString("SECTION_MASTER_CODE_FK_ID"));
				layoutThemeMapperVO.setActionVisualMasterCode(rs.getString("ACTION_VISUAL_MASTER_CODE_FK_ID"));
				layoutThemeMapperVO.setActionGroupCode(rs.getString("ACTION_GROUP_CODE_FK_ID"));
				layoutThemeMapperVO.setThemeSeqNumber(rs.getInt("THEME_SEQ_NUM_POS_INT"));
				result.add(layoutThemeMapperVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<LicencedLocalesVO> getLicencedLocale() {
		List<LicencedLocalesVO> result = new ArrayList<>();
		String SQL = "SELECT LOCALE_CODE_PK_ID, LANGUAGE_FAMILY_CODE_FK_ID, LOCALE_NAME_G11N_BIG_TXT, IS_GLOBAL_DFLT, LANG_MAP_TXT, OEM_NAME_TXT FROM T_BT_TAP_LICENSED_LOCALE INNER JOIN T_SYS_LKP_LOCALE ON LOCALE_CODE_FK_ID = LOCALE_CODE_PK_ID" +
				" INNER JOIN T_SYS_TAP_LCL_LANG_TRNSLT_MAP ON T_SYS_TAP_LCL_LANG_TRNSLT_MAP.LOCALE_CODE_FK_ID = LOCALE_CODE_PK_ID WHERE T_BT_TAP_LICENSED_LOCALE.IS_DELETED = 0;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while (rs.next()){
				LicencedLocalesVO licencedLocalesVO = new LicencedLocalesVO();
				licencedLocalesVO.setLocale(rs.getString("LOCALE_CODE_PK_ID"));
				licencedLocalesVO.setLocaleName(rs.getString("LOCALE_NAME_G11N_BIG_TXT"));
				licencedLocalesVO.setLanguageCode(rs.getString("LANG_MAP_TXT"));//LANGUAGE_FAMILY_CODE_FK_ID
				licencedLocalesVO.setLangMapTxt(rs.getString("LANG_MAP_TXT"));
				licencedLocalesVO.setOemName(rs.getString("OEM_NAME_TXT"));
				licencedLocalesVO.setGlobalDefault(rs.getBoolean("IS_GLOBAL_DFLT"));
				result.add(licencedLocalesVO);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public List<RowCondition> getRowConditionsForAllMTPEs() {
		List<RowCondition> result = new ArrayList<>();
		String SQL = "SELECT *,T_PFM_ADM_AM_WHOSE_PROCESS.AM_WHOSE_PK_ID FROM T_PFM_ADM_AM_ROW_CONDN LEFT JOIN T_PFM_ADM_AM_WHOSE_PROCESS on T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID=T_PFM_ADM_AM_ROW_CONDN.AM_ACCESS_PROCESS_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID ='ALL_BUT_SELF';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				RowCondition rowCondition = new RowCondition();
				rowCondition.setRowCondnPkId(convertByteToString(rs.getBytes("AM_ROW_CONDN_PK_ID")));
				rowCondition.setAccessProcessFkId(convertByteToString(rs.getBytes("AM_ACCESS_PROCESS_FK_ID")));
				rowCondition.setWhatMtPECodeFkId(convertByteToString(rs.getBytes("WHAT_MT_PE_CODE_FK_ID")));
				rowCondition.setWhatDOCode(convertByteToString(rs.getBytes("WHAT_DO_CODE_FK_ID")));
				rowCondition.setParentRowCondnFkId(convertByteToString(rs.getBytes("SLF_PRNT_ROW_CONDN_FK_ID")));
				rowCondition.setStartDateTime(rs.getTimestamp("START_DATETIME"));
				rowCondition.setEndDataTime(rs.getTimestamp("END_DATETIME"));
				rowCondition.setCreateCurrentPermitted(rs.getBoolean("IS_CREATE_CURRENT_PERMITTED"));
				rowCondition.setCreateHistoryPermitted(rs.getBoolean("IS_CREATE_HISTORY_PERMITTED"));
				rowCondition.setCreateFuturePermitted(rs.getBoolean("IS_CREATE_FUTURE_PERMITTED"));
				rowCondition.setReadCurrentPermitted(rs.getBoolean("IS_READ_CURRENT_PERMITTED"));
				rowCondition.setReadHistoryPermitted(rs.getBoolean("IS_READ_HISTORY_PERMITTED"));
				rowCondition.setReadFuturePermitted(rs.getBoolean("IS_READ_FUTURE_PERMITTED"));
				rowCondition.setUpdateCurrentPermitted(rs.getBoolean("IS_UPDATE_CURRENT_PERMITTED"));
				rowCondition.setUpdateHistoryPermitted(rs.getBoolean("IS_UPDATE_HISTORY_PERMITTED"));
				rowCondition.setUpdateFuturePermitted(rs.getBoolean("IS_UPDATE_FUTURE_PERMITTED"));
				rowCondition.setDeleteCurrentPermitted(rs.getBoolean("IS_DELETE_CURRENT_PERMITTED"));
				rowCondition.setDeleteHistoryPermitted(rs.getBoolean("IS_DELETE_HISTORY_PERMITTED"));
				rowCondition.setDeleteFuturePermitted(rs.getBoolean("IS_DELETE_FUTURE_PERMITTED"));
				if(StringUtil.isDefined(Util.convertByteToString(rs.getBytes("T_PFM_ADM_AM_WHOSE_PROCESS.AM_WHOSE_PK_ID")))){
					rowCondition.setAllButSelfPermissioned(true);
				}
				result.add(rowCondition);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Map<String, List<AMColCondition>> getColumnConditions(Set<String> rowPks) {
		Map<String, List<AMColCondition>> result = new HashMap<>();
		if(rowPks== null || rowPks.size()==0)
			return result;
		StringBuilder SQL = new StringBuilder("SELECT * FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID IN (");
		List<Object> parameters = new ArrayList<>();
		int i =0;
		for(String rowPkID : rowPks){
			if(i!=0)
				SQL.append(",");
			parameters.add(rowPkID);
			SQL.append("?");
			i++;
		}
		SQL.append(")");
		ResultSet rs = executeSQL(SQL.toString(), parameters);
		try {
			while(rs.next()){
				AMColCondition amColCondition = new AMColCondition();
				amColCondition.setAmColPkId(convertByteToString(rs.getBytes("AM_COL_PK_ID")));
				amColCondition.setAmRowCondnFkId(convertByteToString(rs.getBytes("AM_ROW_CONDN_FK_ID")));
				amColCondition.setWhatDoaFkId(rs.getString("WHAT_DOA_CODE_FK_ID"));
				amColCondition.setStartDate(rs.getTimestamp("START_DATETIME"));
				amColCondition.setEndDate(rs.getTimestamp("END_DATETIME"));
				amColCondition.setPermissionEnds(rs.getBoolean("IS_PERM_NEVER_ENDS"));
				amColCondition.setCreateCurrentPermitted(rs.getBoolean("IS_CREATE_CURRENT_PERMITTED"));
				amColCondition.setCreateHistoryPermitted(rs.getBoolean("IS_CREATE_HISTORY_PERMITTED"));
				amColCondition.setCreateFuturePermitted(rs.getBoolean("IS_CREATE_FUTURE_PERMITTED"));
				amColCondition.setReadCurrentPermitted(rs.getBoolean("IS_READ_CURRENT_PERMITTED"));
				amColCondition.setReadHistoryPermitted(rs.getBoolean("IS_READ_HISTORY_PERMITTED"));
				amColCondition.setReadFuturePermitted(rs.getBoolean("IS_READ_FUTURE_PERMITTED"));
				amColCondition.setUpdateCurrentPermitted(rs.getBoolean("IS_UPDATE_CURRENT_PERMITTED"));
				amColCondition.setUpdateHistoryPermitted(rs.getBoolean("IS_UPDATE_HISTORY_PERMITTED"));
				amColCondition.setUpdateFuturePermitted(rs.getBoolean("IS_UPDATE_FUTURE_PERMITTED"));
				if(result.containsKey(amColCondition.getAmRowCondnFkId())){
					result.get(amColCondition.getAmRowCondnFkId()).add(amColCondition);
				}else{
					List<AMColCondition> amColConditions = new ArrayList<>();
					amColConditions.add(amColCondition);
					result.put(amColCondition.getAmRowCondnFkId(), amColConditions);
				}
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void insertLabelAlias(String label, Integer currentMaxAlias) throws SQLException {
		String SQL = "INSERT INTO LABEL_TO_ALIAS_MAP (LABEL, ALIAS_NUMBER) VALUE (?, ?);";
		List<Object> parameters = new ArrayList<>();
		parameters.add(label);
		parameters.add(currentMaxAlias);
		executeUpdate(SQL, parameters);
	}

	@Override
	public Map<String, Integer> getLabelAlias() {
		Map<String, Integer> result = new HashMap<>();
		String SQL = "SELECT LABEL, ALIAS_NUMBER FROM LABEL_TO_ALIAS_MAP;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()){
                result.put(rs.getString(1), rs.getInt(2));
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public NameCalculationRuleVO getNameCalculationRule(String localeCode, String nameType) {
		NameCalculationRuleVO ruleVO = null;
		String SQL = "SELECT * FROM T_PRN_ADM_NAME_CALC_RULE WHERE LOCALE_CODE_FK_ID = ? AND NAME_TYPE_CODE_FK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(localeCode);
		parameters.add(nameType);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			if (rs.next()){
				ruleVO = new NameCalculationRuleVO();
				ruleVO.setNameCalculationPkId(convertByteToString(rs.getBytes("PRSN_NAME_CALC_RULE_PK_ID")));
				ruleVO.setLocaleCode(rs.getString("LOCALE_CODE_FK_ID"));
				ruleVO.setNameType(rs.getString("NAME_TYPE_CODE_FK_ID"));
				ruleVO.setFormula(rs.getString("FORMULA_LNG_TXT"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ruleVO;
	}

}