package com.tapplent.platformutility.batch.helper;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

import java.io.*;

/**
 * Created by sripat on 23/08/16.
 */
public class S3Helper {

    public static InputStream fetchObject(String id) {
        AmazonS3 amazonS3 = SystemAwareCache.getSystemRepository().getAmazonS3Client();
        java.util.Date expiration = new java.util.Date();
        long msec = expiration.getTime();
        msec += 1000 * 60 * 60; // Add 1 hour.
        expiration.setTime(msec);
        String bucketName = "mytapplent" + "/" + TenantContextHolder.getCurrentTenantID();
        S3Object object = amazonS3.getObject(
                new GetObjectRequest(bucketName, id));
        InputStream objectData = object.getObjectContent();
        // Process the objectData stream.
        try {
            objectData.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return objectData;
    }

    public static String insertIntoS3(InputStream input, String fileName) {

        String amazonFileUploadLocationOriginal = "mytapplent" + "/" + TenantContextHolder.getCurrentTenantID();
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentDisposition("attachment; filename=" + fileName);
        try {
            objectMetadata.setContentLength(getBytes(input).length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String keyName = Common.getUUID();
        AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
        PutObjectRequest putObjectRequest = new PutObjectRequest(amazonFileUploadLocationOriginal, keyName, input, objectMetadata);
        PutObjectResult result = s3Client.putObject(putObjectRequest);
        return keyName;
    }

    public static byte[] getBytes(InputStream is) throws IOException {

        int len;
        int size = 1024;
        byte[] buf;

        if (is instanceof ByteArrayInputStream) {
            size = is.available();
            buf = new byte[size];
            len = is.read(buf, 0, size);
        } else {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            buf = new byte[size];
            while ((len = is.read(buf, 0, size)) != -1)
                bos.write(buf, 0, len);
            buf = bos.toByteArray();
        }
        return buf;
    }

}
