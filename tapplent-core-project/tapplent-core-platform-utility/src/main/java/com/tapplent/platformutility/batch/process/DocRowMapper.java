package com.tapplent.platformutility.batch.process;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.DG_DocGenData;
import org.springframework.jdbc.core.RowMapper;




public class DocRowMapper implements RowMapper<DG_DocGenData> {

	//@Override
	public DG_DocGenData mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		DG_DocGenData bdGen = new DG_DocGenData();
		
		bdGen.setPersonDocumentPkId(rs.getString(Constants.personDocumentPkId));
		bdGen.setPersonFkId(rs.getString(Constants.personFkId));
		bdGen.setMtPeCodeFkId(rs.getString(Constants.mtPeCodeFkId));
		bdGen.setDoRowPrimaryKeyTxt(rs.getString(Constants.doRowPrimaryKeyTxt));
		bdGen.setDoRowVersionFkId(rs.getString(Constants.doRowVersionFkId));
		bdGen.setDependencyKeyExpn(rs.getString(Constants.dependencyKeyExpn));
		bdGen.setSrcMtPeCodeFkId(rs.getString(Constants.srcMtPeCodeFkId));
		bdGen.setSrcBtCodeFkId(rs.getString(Constants.srcBtCodeFkId));
		bdGen.setSrcViewTypeCodeFkId(rs.getString(Constants.srcViewTypeCodeFkId));
		bdGen.setFileAttach(rs.getString(Constants.fileAttach));
		bdGen.setFileNameLngTxt(rs.getString(Constants.fileNameLngTxt));
		bdGen.setReturnStatusCodeFkId(rs.getString(Constants.returnStatusCodeFkId));
		bdGen.setDocGenDefinitionPkId(rs.getString(Constants.docGenDefinitionPkId));
		bdGen.setDocBtCodeFkId(rs.getString(Constants.docBtCodeFkId));
		bdGen.setDocMtPeCodeFkId(rs.getString(Constants.docMtPeCodeFkId));
		bdGen.setDocVtCodeFkId(rs.getString(Constants.docVtCodeFkId));
		bdGen.setDocCanvasTxnFkId(rs.getString(Constants.docCanvasTxnFkId));
		bdGen.setExportPptTemplateFkId(rs.getString(Constants.exportPptTemplateFkId));
		bdGen.setDocTypeCodeFkId(rs.getString(Constants.docTypeCodeFkId));
		bdGen.setPasswordEnabled(rs.getBoolean(Constants.isPasswordEnabled));
		bdGen.setNotificationDefinitionFkId(rs.getString(Constants.notificationDefinitionFkId));
		bdGen.setDocNameExpn(rs.getString(Constants.docNameExpn));
		bdGen.setDocPwdExpn(rs.getString(Constants.docPwdExpn));
		
		return bdGen;
	}

}




















