package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 23/01/17.
 */
public class AppHomePageSecnContentVO {
    /*
    APP_HOME_PAGE_SECN_CONTENT_PK_ID
APP_HOME_PAGE_SECN_FK_ID
CONTROL_CONTENT_TYPE_CODE_FK_ID
CONTROL_SEQ_NUM_POS_INT
CONTROL_VALUE_G11N_BIG_TXT
     */
    private String appHomepageSecnContentPkId;
    private String appHomePageSecnFkId;
    private String controlContentTypeCodeFkId;
    private int controlSeqNumPosInt;
    private String controlValueG11nBigTxt;

    public String getAppHomepageSecnContentPkId() {
        return appHomepageSecnContentPkId;
    }

    public void setAppHomepageSecnContentPkId(String appHomepageSecnContentPkId) {
        this.appHomepageSecnContentPkId = appHomepageSecnContentPkId;
    }

    public String getAppHomePageSecnFkId() {
        return appHomePageSecnFkId;
    }

    public void setAppHomePageSecnFkId(String appHomePageSecnFkId) {
        this.appHomePageSecnFkId = appHomePageSecnFkId;
    }

    public String getControlContentTypeCodeFkId() {
        return controlContentTypeCodeFkId;
    }

    public void setControlContentTypeCodeFkId(String controlContentTypeCodeFkId) {
        this.controlContentTypeCodeFkId = controlContentTypeCodeFkId;
    }

    public int getControlSeqNumPosInt() {
        return controlSeqNumPosInt;
    }

    public void setControlSeqNumPosInt(int controlSeqNumPosInt) {
        this.controlSeqNumPosInt = controlSeqNumPosInt;
    }

    public String getControlValueG11nBigTxt() {
        return controlValueG11nBigTxt;
    }

    public void setControlValueG11nBigTxt(String controlValueG11nBigTxt) {
        this.controlValueG11nBigTxt = controlValueG11nBigTxt;
    }
}
