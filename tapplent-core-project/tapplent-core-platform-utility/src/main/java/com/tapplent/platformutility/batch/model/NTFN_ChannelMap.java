package com.tapplent.platformutility.batch.model;

public class NTFN_ChannelMap {
	private String notificationDefnChannelMapPkId;
	private String channelTypeCodeFkId;
	private boolean isUseContactTimePreferences;
	public String getNotificationDefinitionFkId() {
		return notificationDefnChannelMapPkId;
	}
	public void setNotificationDefinitionFkId(String notificationDefinitionFkId) {
		this.notificationDefnChannelMapPkId = notificationDefinitionFkId;
	}
	public String getChannelTypeCodeFkId() {
		return channelTypeCodeFkId;
	}
	public void setChannelTypeCodeFkId(String channelTypeCodeFkId) {
		this.channelTypeCodeFkId = channelTypeCodeFkId;
	}
	public boolean isUseContactTimePreferences() {
		return isUseContactTimePreferences;
	}
	public void setUseContactTimePreferences(boolean isUseContactTimePreferences) {
		this.isUseContactTimePreferences = isUseContactTimePreferences;
	}
	
	
}
