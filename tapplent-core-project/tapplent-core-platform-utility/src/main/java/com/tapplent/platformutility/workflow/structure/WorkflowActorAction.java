package com.tapplent.platformutility.workflow.structure;

/**
 * Created by Manas on 9/1/16.
 */
public class WorkflowActorAction {
	
    private String workflowTxnActorPossibleActionPkId;
    private String WorkflowTxnStepActorFkId;
    private String actionVisualMasterCodeFkId;
    private String targetWflowTxnStepFkId;
    private String targetStepActorDefnFkId;
    private boolean isActionVisibleThruExitCritEvln;
    private String versionId;
    private String workflowActionDefnFkId;
    private String workflowDefinitionFkId;
    private int actionDispSeqNumPosInt;
    private String sourceActorFkId;
    private String targetActorFkId;
    private String targetStepFkId;
    private String IfActionConditionExpn;
    private boolean isActionToDlftExecOnForcedMove;
    private boolean isActionDelegateable;
    private boolean isAutoExecIfActorEqualsSubject;
    private boolean isAutoExecuteIfActorEqualToInitiator;
    private boolean isAutoExecuteIfDuplicate;
    private boolean isActionCommentMandatory;
//    private String exitCountCriteriaExpn;
	private String exitCountCriteriaForActionVisibilityExpn;
	private String exitCountCriteriaForStepTrnsnExpn;
//    private boolean isCrtriaCntrlgSrcStepExit;
//    private boolean isCrtriaCntrlgTgtStepEntryVsblty;
    private String actionExitSrcActNtfnId;
    private String actionExitTrgtActNtfnId;
    
	public String getWorkflowTxnActorPossibleActionPkId() {
		return workflowTxnActorPossibleActionPkId;
	}
	public void setWorkflowTxnActorPossibleActionPkId(String workflowTxnActorPossibleActionPkId) {
		this.workflowTxnActorPossibleActionPkId = workflowTxnActorPossibleActionPkId;
	}
	public String getWorkflowTxnStepActorFkId() {
		return WorkflowTxnStepActorFkId;
	}
	public void setWorkflowTxnStepActorFkId(String workflowTxnStepActorFkId) {
		WorkflowTxnStepActorFkId = workflowTxnStepActorFkId;
	}

	public String getActionVisualMasterCodeFkId() {
		return actionVisualMasterCodeFkId;
	}

	public void setActionVisualMasterCodeFkId(String actionVisualMasterCodeFkId) {
		this.actionVisualMasterCodeFkId = actionVisualMasterCodeFkId;
	}

	public String getTargetWflowTxnStepFkId() {
		return targetWflowTxnStepFkId;
	}
	public void setTargetWflowTxnStepFkId(String targetWflowTxnStepFkId) {
		this.targetWflowTxnStepFkId = targetWflowTxnStepFkId;
	}
	public String getTargetStepActorDefnFkId() {
		return targetStepActorDefnFkId;
	}
	public void setTargetStepActorDefnFkId(String targetStepActorDefnFkId) {
		this.targetStepActorDefnFkId = targetStepActorDefnFkId;
	}

	public boolean isActionVisibleThruExitCritEvln() {
		return isActionVisibleThruExitCritEvln;
	}

	public void setActionVisibleThruExitCritEvln(boolean actionVisibleThruExitCritEvln) {
		isActionVisibleThruExitCritEvln = actionVisibleThruExitCritEvln;
	}

	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getWorkflowActionDefnFkId() {
		return workflowActionDefnFkId;
	}
	public void setWorkflowActionDefnFkId(String workflowActionDefnFkId) {
		this.workflowActionDefnFkId = workflowActionDefnFkId;
	}
	public String getWorkflowDefinitionFkId() {
		return workflowDefinitionFkId;
	}
	public void setWorkflowDefinitionFkId(String workflowDefinitionFkId) {
		this.workflowDefinitionFkId = workflowDefinitionFkId;
	}

	public int getActionDispSeqNumPosInt() {
		return actionDispSeqNumPosInt;
	}

	public void setActionDispSeqNumPosInt(int actionDispSeqNumPosInt) {
		this.actionDispSeqNumPosInt = actionDispSeqNumPosInt;
	}

	public String getSourceActorFkId() {
		return sourceActorFkId;
	}
	public void setSourceActorFkId(String sourceActorFkId) {
		this.sourceActorFkId = sourceActorFkId;
	}
	public String getTargetActorFkId() {
		return targetActorFkId;
	}
	public void setTargetActorFkId(String targetActorFkId) {
		this.targetActorFkId = targetActorFkId;
	}
	public String getTargetStepFkId() {
		return targetStepFkId;
	}
	public void setTargetStepFkId(String targetStepFkId) {
		this.targetStepFkId = targetStepFkId;
	}
	public String getIfActionConditionExpn() {
		return IfActionConditionExpn;
	}
	public void setIfActionConditionExpn(String ifActionConditionExpn) {
		IfActionConditionExpn = ifActionConditionExpn;
	}
	public boolean isActionToDlftExecOnForcedMove() {
		return isActionToDlftExecOnForcedMove;
	}
	public void setActionToDlftExecOnForcedMove(boolean isActionToDlftExecOnForcedMove) {
		this.isActionToDlftExecOnForcedMove = isActionToDlftExecOnForcedMove;
	}
	public boolean isActionDelegateable() {
		return isActionDelegateable;
	}
	public void setActionDelegateable(boolean isActionDelegateable) {
		this.isActionDelegateable = isActionDelegateable;
	}
	public boolean isAutoExecIfActorEqualsSubject() {
		return isAutoExecIfActorEqualsSubject;
	}
	public void setAutoExecIfActorEqualsSubject(boolean isAutoExecIfActorEqualsSubject) {
		this.isAutoExecIfActorEqualsSubject = isAutoExecIfActorEqualsSubject;
	}
	public boolean isAutoExecuteIfActorEqualToInitiator() {
		return isAutoExecuteIfActorEqualToInitiator;
	}
	public void setAutoExecuteIfActorEqualToInitiator(boolean isAutoExecuteIfActorEqualToInitiator) {
		this.isAutoExecuteIfActorEqualToInitiator = isAutoExecuteIfActorEqualToInitiator;
	}
	public boolean isAutoExecuteIfDuplicate() {
		return isAutoExecuteIfDuplicate;
	}
	public void setAutoExecuteIfDuplicate(boolean isAutoExecuteIfDuplicate) {
		this.isAutoExecuteIfDuplicate = isAutoExecuteIfDuplicate;
	}
	public boolean isActionCommentMandatory() {
		return isActionCommentMandatory;
	}
	public void setActionCommentMandatory(boolean isActionCommentMandatory) {
		this.isActionCommentMandatory = isActionCommentMandatory;
	}

	public String getExitCountCriteriaForActionVisibilityExpn() {
		return exitCountCriteriaForActionVisibilityExpn;
	}

	public void setExitCountCriteriaForActionVisibilityExpn(String exitCountCriteriaForActionVisibilityExpn) {
		this.exitCountCriteriaForActionVisibilityExpn = exitCountCriteriaForActionVisibilityExpn;
	}

	public String getExitCountCriteriaForStepTrnsnExpn() {
		return exitCountCriteriaForStepTrnsnExpn;
	}

	public void setExitCountCriteriaForStepTrnsnExpn(String exitCountCriteriaForStepTrnsnExpn) {
		this.exitCountCriteriaForStepTrnsnExpn = exitCountCriteriaForStepTrnsnExpn;
	}
	public String getActionExitSrcActNtfnId() {
		return actionExitSrcActNtfnId;
	}
	public void setActionExitSrcActNtfnId(String actionExitSrcActNtfnId) {
		this.actionExitSrcActNtfnId = actionExitSrcActNtfnId;
	}
	public String getActionExitTrgtActNtfnId() {
		return actionExitTrgtActNtfnId;
	}
	public void setActionExitTrgtActNtfnId(String actionExitTrgtActNtfnId) {
		this.actionExitTrgtActNtfnId = actionExitTrgtActNtfnId;
	}
}
