package com.tapplent.platformutility.hierarchy.service;

import com.tapplent.platformutility.hierarchy.structure.HierarchyInfo;
import com.tapplent.platformutility.hierarchy.structure.Node;
import com.tapplent.platformutility.hierarchy.structure.Relation;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Manas on 8/12/16.
 */
public interface HierarchyService {
    public Map<String, Node> getNthLevelParent(String primaryKey, EntityMetadataVOX doMeta, int level);
    public Map<String, Node> getAllChildrenUptoNlevel(String primaryKey, EntityMetadataVOX doMeta, int level);

    List<HierarchyInfo> getAllChildren(MasterEntityMetadataVOX rootMtPEMeta, String pk);

    Map<String, Map<String, List<Relation>>> getAllRelationBetweenBaseAndRelatedPerson(String basePersonId, String relatedPerson);

    Map<String, Node> getAllParents(String primaryKey, EntityMetadataVOX doMeta);

    Map<String, Map<String, List<Relation>>> getAllRelationBetweenTwoPerson(String personOneId, String personTwoId);

    public void updateLeftRightForNodeInsert(String hierarchyId, String parentNodeId, EntityMetadataVOX entityMetadataVOX, String primaryNodeId) throws SQLException;

    public void updateLeftRightForNodeDelete(String hierarchyId, EntityMetadataVOX entityMetadataVOX, int nodeLeft, int nodeRight, String PrimaryKey) throws SQLException;

    public void updateLeftRightForNodeParentUpdate(String hierarchyId, EntityMetadataVOX entityMetadataVOX, Node rootNode, Node parentNode) throws SQLException;

    String getPersonIdForRootPerson(String hierarchyId);

    List<String> getHierarchialPersonRoots();

    public int updateLeftRightAndHierachyValuesForPerson(String hierarchyId, String rootPrimaryId, int level) throws SQLException;

    Map<String,Map<Integer,List<String>>> getAllRelationsForPerson(String key);
}
