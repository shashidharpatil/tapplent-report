package com.tapplent.platformutility.common.util;

public enum EntityDataTypeEnum {
	T_ID("T_ID"), 		//Binary
	T_CODE("T_CODE"),	//String
	T_ICON("T_ICON"),	//String
	SELF("SELF"), // Refers to the primary key
	TEXT("TEXT"), // Refers all character type data		//String
	T_BIG_TEXT("T_BIG_TEXT"),
	LOCATION("LOCATION"),
	T_EXPRESSION("T_EXPRESSION"), //It is to check if this expression contains DOAs
	LONG_TEXT("LONG_TEXT"),
	DATE("DATE"),	//String
	TIME("TIME"),	//String
	DATE_TIME("DATE_TIME"), // Refers to a Datetime value	//String
	POSITIVE_INTEGER("POSITIVE_INTEGER"),	//Integer
	NEGATIVE_INTEGER("NEGATIVE_INTEGER"),	//Negative Integer	
	BIG_INTEGER("BIG_INTEGER"),				//Integer
	POSITIVE_DECIMAL("POSITIVE_DECIMAL"),	//Decimal	
	NEGATIVE_DECIMAL("NEGATIVE_DECIMAL"),	//Negative decimal
	T_BLOB("T_BLOB"),						//Ignore
	CURRENCY("CURRENCY"),					//String
	/**
	 * Refers to a Boolean value
	 */
	BOOLEAN("BOOLEAN"),						//Integer
	/**
	 * Refers to a attachement id
	 */
	ATTACHMENT("ATTACHMENT"),				//Binary
	T_ATTACH("ATTACH"),
	/**
	 * Refers to a attachement id of type audio
	 */
	AUDIO("AUDIO"),							//Binary
	/**
	 * Refers to a video attachement id
	 */
	VIDEO("VIDEO"),							//Binary
	/**
	 * Refers to a image attachement id
	 */
	IMAGE("IMAGE"),							//Binary
	LOCATION_LATITUDE("LOCATION_LATITUDE"), //Decimal
	LOCATION_LONGITUDE("LOCATION_LONGITUDE"),	//Negative Decimal
	T_IMAGEID("T_IMAGEID"),	//Blob
	T_IMAGE("T_IMAGE"),	//Binary
	T_KEYWORD("T_KEYWORD"),
	DEFAULT("DEFAULT");
	private String type;

	EntityDataTypeEnum(String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}
}
