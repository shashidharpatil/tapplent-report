package com.tapplent.platformutility.analytics.structure;

/**
 * Created by tapplent on 25/08/17.
 */
public class CalendarPeriodUnitVO {
    private String periodUnit;
    private String periodUnitName;

    public String getPeriodUnit() {
        return periodUnit;
    }

    public void setPeriodUnit(String periodUnit) {
        this.periodUnit = periodUnit;
    }

    public String getPeriodUnitName() {
        return periodUnitName;
    }

    public void setPeriodUnitName(String periodUnitName) {
        this.periodUnitName = periodUnitName;
    }
}
