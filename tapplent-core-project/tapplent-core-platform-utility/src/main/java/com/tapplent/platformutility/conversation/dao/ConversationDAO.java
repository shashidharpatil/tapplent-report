package com.tapplent.platformutility.conversation.dao;

import com.tapplent.platformutility.conversation.valueobject.FeedbackGroupVO;
import com.tapplent.platformutility.conversation.valueobject.FeedbackMessageVO;
import com.tapplent.platformutility.conversation.valueobject.GroupMemberVO;
import com.tapplent.platformutility.conversation.valueobject.PersonDetailsVO;
import com.tapplent.platformutility.layout.valueObject.LayoutPermissionsVO;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Tapplent on 6/9/17.
 */
public interface ConversationDAO {
    public List<FeedbackGroupVO> getFeedbackGroupList(String mtPECode, String objectId, int limit, int offset, String keyword);

    void createFeedbackGroup(FeedbackGroupVO feedbackGroupVO) throws SQLException;

    void createFeedbackGroupMembers(List<GroupMemberVO> groupMemberVOS, String feedbackGroupPkId);

    List<PersonDetailsVO> getFeedbackContactDetails(String mTPECode, String objectId, int limit, int offset, String keyword, String feedbackGroupId);

    String writeFeedbackMessage(FeedbackMessageVO feedbackMessageVO) throws SQLException;

    List<FeedbackMessageVO> getFeedbackMessage(String feedbackGroupId, int limit, int offset);

    void populateFeedbackReadStatus(String feedbackGroupId);

    Map<String,Integer> getUnreadMessageCountByGroup(List<FeedbackGroupVO> feedbackGroupVOList);

    List<PersonDetailsVO> getFeedbackGroupMemberDetails(String feedbackGroupId);

    PersonDetailsVO getPersonDetailsByPersonId(String personId);

    void updateFeedbackGroupMemberDetails(GroupMemberVO groupMemberVO) throws SQLException;

    void addFeedbackGroupMembers(List<GroupMemberVO> groupMemberVOS) throws SQLException;

    void deleteGroup(String feedbackGroupId);

    String getOtherPersonIdByGroup(String feedbackGroupPkId);

    void updateFeedbackGroup(FeedbackGroupVO feedbackGroupVO) throws SQLException;

    List<GroupMemberVO> getFeedGroupMemberReadStatus(String feedbackId);

    FeedbackMessageVO getFeedbackMessageById(String feedbackPkId);

    List<FeedbackMessageVO> getBookmarkedFeedbackMessage(String feedbackGroupId, int limit, int offset);

    FeedbackMessageVO getFeedbackMessage(String feedbackPkId);

    LayoutPermissionsVO getLayoutPermissions(String actionId) throws SQLException;
}
