package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by Tapplent on 4/8/17.
 */
public class AMWhatFilterCriteria {

//    WHAT_FILTER_CRITERIA_PK_ID
//            AM_ROW_CONDN_FK_ID
//    CONTROL_ATTRIBUTE_PATH_EXPN
//            OPER_FK_ID
//    VALUE_EXPN
    private String whatFilterCriteriaPkId;
    private String amRowCondnFkId;
    private String controlAttributePathExpn;
    private String operFkId;
    private String valueExpn;

    public String getWhatFilterCriteriaPkId() {
        return whatFilterCriteriaPkId;
    }

    public void setWhatFilterCriteriaPkId(String whatFilterCriteriaPkId) {
        this.whatFilterCriteriaPkId = whatFilterCriteriaPkId;
    }

    public String getAmRowCondnFkId() {
        return amRowCondnFkId;
    }

    public void setAmRowCondnFkId(String amRowCondnFkId) {
        this.amRowCondnFkId = amRowCondnFkId;
    }

    public String getControlAttributePathExpn() {
        return controlAttributePathExpn;
    }

    public void setControlAttributePathExpn(String controlAttributePathExpn) {
        this.controlAttributePathExpn = controlAttributePathExpn;
    }

    public String getOperFkId() {
        return operFkId;
    }

    public void setOperFkId(String operFkId) {
        this.operFkId = operFkId;
    }

    public String getValueExpn() {
        return valueExpn;
    }

    public void setValueExpn(String valueExpn) {
        this.valueExpn = valueExpn;
    }
}
