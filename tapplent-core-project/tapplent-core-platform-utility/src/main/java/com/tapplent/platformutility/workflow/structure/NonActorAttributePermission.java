package com.tapplent.platformutility.workflow.structure;

public class NonActorAttributePermission {
    private String attributePermissionPkId;
    private String actionPermissionDefnId;
    private String permissionTypeCodeId;
    private String permissionValue;
    private String AdditionalSubjectUserApplicableGroup;
    private String recordStatePermission;
    private String mtPE;
    private String attributePathExpn;
    private boolean isAttrVisible;
    private boolean isAttrEditable;

    public String getAttributePermissionPkId() {
        return attributePermissionPkId;
    }

    public void setAttributePermissionPkId(String attributePermissionPkId) {
        this.attributePermissionPkId = attributePermissionPkId;
    }

    public String getActionPermissionDefnId() {
        return actionPermissionDefnId;
    }

    public void setActionPermissionDefnId(String actionPermissionDefnId) {
        this.actionPermissionDefnId = actionPermissionDefnId;
    }

    public String getPermissionTypeCodeId() {
        return permissionTypeCodeId;
    }

    public void setPermissionTypeCodeId(String permissionTypeCodeId) {
        this.permissionTypeCodeId = permissionTypeCodeId;
    }

    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }

    public String getAdditionalSubjectUserApplicableGroup() {
        return AdditionalSubjectUserApplicableGroup;
    }

    public void setAdditionalSubjectUserApplicableGroup(String additionalSubjectUserApplicableGroup) {
        AdditionalSubjectUserApplicableGroup = additionalSubjectUserApplicableGroup;
    }

    public String getRecordStatePermission() {
        return recordStatePermission;
    }

    public void setRecordStatePermission(String recordStatePermission) {
        this.recordStatePermission = recordStatePermission;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getAttributePathExpn() {
        return attributePathExpn;
    }

    public void setAttributePathExpn(String attributePathExpn) {
        this.attributePathExpn = attributePathExpn;
    }

    public boolean isAttrVisible() {
        return isAttrVisible;
    }

    public void setAttrVisible(boolean attrVisible) {
        isAttrVisible = attrVisible;
    }

    public boolean isAttrEditable() {
        return isAttrEditable;
    }

    public void setAttrEditable(boolean attrEditable) {
        isAttrEditable = attrEditable;
    }
}
