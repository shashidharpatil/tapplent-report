package com.tapplent.platformutility.workflow.structure;

import java.sql.Timestamp;

/**
 * Created by Manas on 8/10/16.
 */
public class WorkflowStepTxn {
    private String workflowTxnStepPkId;
    private String workflowTxnFkId;
    private String workflowStepDefnFkId;
    private Timestamp stepActualStartDatetime;
    private Timestamp stepActualEndDatetime;
    private String stepStatusCodeFkId;
    private Timestamp createdDatetime;
    private Timestamp lastModifiedDatetime;

    public String getWorkflowTxnStepPkId() {
        return workflowTxnStepPkId;
    }

    public void setWorkflowTxnStepPkId(String workflowTxnStepPkId) {
        this.workflowTxnStepPkId = workflowTxnStepPkId;
    }

   public String getWorkflowStepDefnFkId() {
        return workflowStepDefnFkId;
    }

    public String getWorkflowTxnFkId() {
	return workflowTxnFkId;
}

public void setWorkflowTxnFkId(String workflowTxnFkId) {
	this.workflowTxnFkId = workflowTxnFkId;
}

	public void setWorkflowStepDefnFkId(String workflowStepDefnFkId) {
        this.workflowStepDefnFkId = workflowStepDefnFkId;
    }

    public Timestamp getStepActualStartDatetime() {
        return stepActualStartDatetime;
    }

    public void setStepActualStartDatetime(Timestamp stepActualStartDatetime) {
        this.stepActualStartDatetime = stepActualStartDatetime;
    }

    public Timestamp getStepActualEndDatetime() {
        return stepActualEndDatetime;
    }

    public void setStepActualEndDatetime(Timestamp stepActualEndDatetime) {
        this.stepActualEndDatetime = stepActualEndDatetime;
    }

    public String getStepStatusCodeFkId() {
        return stepStatusCodeFkId;
    }

    public void setStepStatusCodeFkId(String stepStatusCodeFkId) {
        this.stepStatusCodeFkId = stepStatusCodeFkId;
    }

	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}

	public void setLastModifiedDatetime(Timestamp lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
}
