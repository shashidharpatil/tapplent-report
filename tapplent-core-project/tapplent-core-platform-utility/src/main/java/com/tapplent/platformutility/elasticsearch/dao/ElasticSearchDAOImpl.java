package com.tapplent.platformutility.elasticsearch.dao;

import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.elasticsearch.valueObject.ElasticsearchPersonTestVO;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 08/05/17.
 */
public class ElasticSearchDAOImpl extends TapplentBaseDAO implements ElasticSearchDAO{
    @Override
    public List<Map<String, String>> getPersons() {

        List<Map<String, String>> personTestVOMapList = new ArrayList<>();
        String sql = "SELECT PERSON_PK_ID, FIRST_NAME_TXT, MIDDLE_NAME_TXT, LAST_NAME_TXT, NAME_SUFFIX_CODE_FK_ID, PREFERRED_NAME_LNG_TXT, BIRTH_FULL_NAME_LNG_TXT, FORMAL_FULL_NAME_LNG_TXT FROM T_PRN_EU_PERSON;";
        List<Object> params = new ArrayList<>();
        ResultSet rs = executeSQL(sql, params);
        try {
            while (rs.next()){
                Map<String, String> personTestVOMap = new HashMap<>();
                if(rs.getBytes("PERSON_PK_ID")!=null)
                personTestVOMap.put("PrimaryKeyID",Util.convertByteToString(rs.getBytes("PERSON_PK_ID")));
                if(rs.getString("FIRST_NAME_TXT")!=null)
                personTestVOMap.put("FirstName",rs.getString("FIRST_NAME_TXT"));
                if(rs.getString("MIDDLE_NAME_TXT")!=null)
                personTestVOMap.put("MiddleName",rs.getString("MIDDLE_NAME_TXT"));
                if(rs.getString("LAST_NAME_TXT")!=null)
                personTestVOMap.put("LastName",rs.getString("LAST_NAME_TXT"));
                if(rs.getString("NAME_SUFFIX_CODE_FK_ID")!=null)
                personTestVOMap.put("Suffix",rs.getString("NAME_SUFFIX_CODE_FK_ID"));
                if(rs.getString("PREFERRED_NAME_LNG_TXT")!=null)
                personTestVOMap.put("PreferredName",rs.getString("PREFERRED_NAME_LNG_TXT"));
                if(rs.getString("BIRTH_FULL_NAME_LNG_TXT")!=null)
                personTestVOMap.put("BirthFullName",rs.getString("BIRTH_FULL_NAME_LNG_TXT"));
                if(rs.getString("FORMAL_FULL_NAME_LNG_TXT")!=null)
                personTestVOMap.put("FormalFullName",rs.getString("FORMAL_FULL_NAME_LNG_TXT"));
                personTestVOMapList.add(personTestVOMap);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personTestVOMapList;
    }
}
