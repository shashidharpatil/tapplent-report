package com.tapplent.platformutility.search.builder;

public class DbSearchRelation {
    private String tableFrom;
    private String tableFromKey;
    private String tableTo;
    private String tableToKey;
    private String connectType;
	public String getTableFrom() {
		return tableFrom;
	}
	public void setTableFrom(String tableFrom) {
		this.tableFrom = tableFrom;
	}
	public String getTableFromKey() {
		return tableFromKey;
	}
	public void setTableFromKey(String tableFromKey) {
		this.tableFromKey = tableFromKey;
	}
	public String getTableTo() {
		return tableTo;
	}
	public void setTableTo(String tableTo) {
		this.tableTo = tableTo;
	}
	public String getTableToKey() {
		return tableToKey;
	}
	public void setTableToKey(String tableToKey) {
		this.tableToKey = tableToKey;
	}
	public String getConnectType() {
		return connectType;
	}
	public void setConnectType(String connectType) {
		this.connectType = connectType;
	}
    
}
