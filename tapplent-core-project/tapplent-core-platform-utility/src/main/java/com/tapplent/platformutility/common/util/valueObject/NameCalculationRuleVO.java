package com.tapplent.platformutility.common.util.valueObject;

public class NameCalculationRuleVO {
    private String nameCalculationPkId;
    private String localeCode;
    private String nameType;
    private String formula;

    public String getNameCalculationPkId() {
        return nameCalculationPkId;
    }

    public void setNameCalculationPkId(String nameCalculationPkId) {
        this.nameCalculationPkId = nameCalculationPkId;
    }

    public String getLocaleCode() {
        return localeCode;
    }

    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
}
