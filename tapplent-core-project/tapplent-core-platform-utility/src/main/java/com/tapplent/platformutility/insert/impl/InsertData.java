package com.tapplent.platformutility.insert.impl;

import java.util.Map;

public interface InsertData {
	Map<String, Object> getData();
	void setData(Map<String, Object> data);
}
