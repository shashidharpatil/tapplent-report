package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class ControlVO {

	private String controlId; 
	private String canvasId;
	private String controlTransactionFkId;
	private String controlinstanceFkId;
	private String baseObjectFkId;
	private String baseTemplateId;
	private String btProcessElementFkId;
	private String domainObjectFkId;
	private String mtPEId;
	private String controlAttribute;
	private String aggregateControlFunction;
	JsonNode deviceIndependentBlob;
	JsonNode deviceDependentBlob;
	JsonNode mstDeviceIndependentBlob;
	JsonNode txnDeviceIndependentBlob;
	JsonNode mstDeviceDependentBlob;
	JsonNode txnDeviceDependentBlob;
	public String getControlId() {
		return controlId;
	}
	public void setControlId(String controlId) {
		this.controlId = controlId;
	}
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public String getControlTransactionFkId() {
		return controlTransactionFkId;
	}
	public void setControlTransactionFkId(String controlTransactionFkId) {
		this.controlTransactionFkId = controlTransactionFkId;
	}
	public String getControlinstanceFkId() {
		return controlinstanceFkId;
	}
	public void setControlinstanceFkId(String controlinstanceFkId) {
		this.controlinstanceFkId = controlinstanceFkId;
	}
	public String getBaseObjectFkId() {
		return baseObjectFkId;
	}
	public void setBaseObjectFkId(String baseObjectFkId) {
		this.baseObjectFkId = baseObjectFkId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getBtProcessElementFkId() {
		return btProcessElementFkId;
	}
	public void setBtProcessElementFkId(String btProcessElementFkId) {
		this.btProcessElementFkId = btProcessElementFkId;
	}
	public String getDomainObjectFkId() {
		return domainObjectFkId;
	}
	public void setDomainObjectFkId(String domainObjectFkId) {
		this.domainObjectFkId = domainObjectFkId;
	}
	public String getMtPEId() {
		return mtPEId;
	}
	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}
	public String getControlAttribute() {
		return controlAttribute;
	}
	public void setControlAttribute(String controlAttribute) {
		this.controlAttribute = controlAttribute;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public String getAggregateControlFunction() {
		return aggregateControlFunction;
	}
	public void setAggregateControlFunction(String aggregateControlFunction) {
		this.aggregateControlFunction = aggregateControlFunction;
	}
	public JsonNode getMstDeviceIndependentBlob() {
		return mstDeviceIndependentBlob;
	}
	public void setMstDeviceIndependentBlob(JsonNode mstDeviceIndependentBlob) {
		this.mstDeviceIndependentBlob = mstDeviceIndependentBlob;
	}
	public JsonNode getTxnDeviceIndependentBlob() {
		return txnDeviceIndependentBlob;
	}
	public void setTxnDeviceIndependentBlob(JsonNode txnDeviceIndependentBlob) {
		this.txnDeviceIndependentBlob = txnDeviceIndependentBlob;
	}
	public JsonNode getMstDeviceDependentBlob() {
		return mstDeviceDependentBlob;
	}
	public void setMstDeviceDependentBlob(JsonNode mstDeviceDependentBlob) {
		this.mstDeviceDependentBlob = mstDeviceDependentBlob;
	}
	public JsonNode getTxnDeviceDependentBlob() {
		return txnDeviceDependentBlob;
	}
	public void setTxnDeviceDependentBlob(JsonNode txnDeviceDependentBlob) {
		this.txnDeviceDependentBlob = txnDeviceDependentBlob;
	}
}
