package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class CanvasThemeVO {
	private String versionId;
	private String canvasThemeSelecPkId;
	private String canvasType;
	private String themeTemplateCode;
	private JsonNode themeProperty;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getCanvasType() {
		return canvasType;
	}
	public void setCanvasType(String canvasType) {
		this.canvasType = canvasType;
	}
	public String getThemeTemplateCode() {
		return themeTemplateCode;
	}
	public void setThemeTemplateCode(String themeTemplateCode) {
		this.themeTemplateCode = themeTemplateCode;
	}
	public JsonNode getThemeProperty() {
		return themeProperty;
	}
	public void setThemeProperty(JsonNode themeProperty) {
		this.themeProperty = themeProperty;
	}
	public String getCanvasThemeSelecPkId() {
		return canvasThemeSelecPkId;
	}
	public void setCanvasThemeSelecPkId(String canvasThemeSelecPkId) {
		this.canvasThemeSelecPkId = canvasThemeSelecPkId;
	}
}
