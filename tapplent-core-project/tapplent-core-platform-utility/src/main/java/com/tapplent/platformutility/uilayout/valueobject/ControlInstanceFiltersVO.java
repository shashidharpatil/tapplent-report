package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 25/04/17.
 */
public class ControlInstanceFiltersVO {
    private String filterPkId;
    private String controlFkId;
    private String attributePathExpn;
    private String operator;
    private String attributeValue;
    private boolean isAlwaysApplied;
    private boolean isHavingClause;

    public String getFilterPkId() {
        return filterPkId;
    }

    public void setFilterPkId(String filterPkId) {
        this.filterPkId = filterPkId;
    }

    public String getControlFkId() {
        return controlFkId;
    }

    public void setControlFkId(String controlFkId) {
        this.controlFkId = controlFkId;
    }

    public String getAttributePathExpn() {
        return attributePathExpn;
    }

    public void setAttributePathExpn(String attributePathExpn) {
        this.attributePathExpn = attributePathExpn;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public boolean isAlwaysApplied() {
        return isAlwaysApplied;
    }

    public void setAlwaysApplied(boolean alwaysApplied) {
        isAlwaysApplied = alwaysApplied;
    }

    public boolean isHavingClause() {
        return isHavingClause;
    }

    public void setHavingClause(boolean havingClause) {
        isHavingClause = havingClause;
    }
}
