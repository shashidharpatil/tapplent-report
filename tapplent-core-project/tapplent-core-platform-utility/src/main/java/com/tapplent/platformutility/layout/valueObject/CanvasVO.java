package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class CanvasVO {
	private String canvasId;
	private String parentCanvasId;
	private String canvasDefnMasterFkId;
	private String canvasTransactionFkId;
	private String baseTemplateId;
	private String mtPEId;
	private String pageFkId;
	private String canvasTypeCode;
	JsonNode mstDeviceIndependentBlob;
	JsonNode txnDeviceIndependentBlob;
	JsonNode mstDeviceDependentBlob;
	JsonNode txnDeviceDependentBlob;
	JsonNode deviceApplicableBlob;
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public String getParentCanvasId() {
		return parentCanvasId;
	}
	public void setParentCanvasId(String parentCanvasId) {
		this.parentCanvasId = parentCanvasId;
	}
	public String getCanvasDefnMasterFkId() {
		return canvasDefnMasterFkId;
	}
	public void setCanvasDefnMasterFkId(String canvasDefnMasterFkId) {
		this.canvasDefnMasterFkId = canvasDefnMasterFkId;
	}
	public String getCanvasTransactionFkId() {
		return canvasTransactionFkId;
	}
	public void setCanvasTransactionFkId(String canvasTransactionFkId) {
		this.canvasTransactionFkId = canvasTransactionFkId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPEId() {
		return mtPEId;
	}
	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}
	public String getPageFkId() {
		return pageFkId;
	}
	public void setPageFkId(String pageFkId) {
		this.pageFkId = pageFkId;
	}
	public JsonNode getMstDeviceIndependentBlob() {
		return mstDeviceIndependentBlob;
	}
	public void setMstDeviceIndependentBlob(JsonNode mstDeviceIndependentBlob) {
		this.mstDeviceIndependentBlob = mstDeviceIndependentBlob;
	}
	public JsonNode getTxnDeviceIndependentBlob() {
		return txnDeviceIndependentBlob;
	}
	public void setTxnDeviceIndependentBlob(JsonNode txnDeviceIndependentBlob) {
		this.txnDeviceIndependentBlob = txnDeviceIndependentBlob;
	}
	public JsonNode getMstDeviceDependentBlob() {
		return mstDeviceDependentBlob;
	}
	public void setMstDeviceDependentBlob(JsonNode mstDeviceDependentBlob) {
		this.mstDeviceDependentBlob = mstDeviceDependentBlob;
	}
	public JsonNode getTxnDeviceDependentBlob() {
		return txnDeviceDependentBlob;
	}
	public void setTxnDeviceDependentBlob(JsonNode txnDeviceDependentBlob) {
		this.txnDeviceDependentBlob = txnDeviceDependentBlob;
	}
	public JsonNode getDeviceApplicableBlob() {
		return deviceApplicableBlob;
	}
	public void setDeviceApplicableBlob(JsonNode deviceApplicableBlob) {
		this.deviceApplicableBlob = deviceApplicableBlob;
	}
	public String getCanvasTypeCode() {
		return canvasTypeCode;
	}
	public void setCanvasTypeCode(String canvasTypeCode) {
		this.canvasTypeCode = canvasTypeCode;
	}
}
