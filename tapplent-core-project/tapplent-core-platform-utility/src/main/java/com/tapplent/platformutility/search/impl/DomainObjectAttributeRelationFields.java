package com.tapplent.platformutility.search.impl;
/*
 * This class tells the columns of the relation table to be fetched
 */
public class DomainObjectAttributeRelationFields {
	private String domainObjectFkId;
	private String dbTableName;
	private String doAttributeFkId;
	private String dbColumnName;
	// Where is it getting build?
	private String mtDoAttributeSpecG11nText;
	private String relationDbDataTypeCodeFkId;
	public DomainObjectAttributeRelationFields(){
		
	}
	public String getDomainObjectFkId() {
		return domainObjectFkId;
	}
	public void setDomainObjectFkId(String domainObjectFkId) {
		this.domainObjectFkId = domainObjectFkId;
	}
	public String getDbTableName() {
		return dbTableName;
	}
	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}
	public String getDoAttributeFkId() {
		return doAttributeFkId;
	}
	public void setDoAttributeFkId(String doAttributeFkId) {
		this.doAttributeFkId = doAttributeFkId;
	}
	public String getDbColumnName() {
		return dbColumnName;
	}
	public void setDbColumnName(String dbColumnName) {
		this.dbColumnName = dbColumnName;
	}
	public String getMtDoAttributeSpecG11nText() {
		return mtDoAttributeSpecG11nText;
	}
	public void setMtDoAttributeSpecG11nText(String mtDoAttributeSpecG11nText) {
		this.mtDoAttributeSpecG11nText = mtDoAttributeSpecG11nText;
	}
	public String getRelationDbDataTypeCodeFkId() {
		return relationDbDataTypeCodeFkId;
	}
	public void setRelationDbDataTypeCodeFkId(String relationDbDataTypeCodeFkId) {
		this.relationDbDataTypeCodeFkId = relationDbDataTypeCodeFkId;
	}
	
}
