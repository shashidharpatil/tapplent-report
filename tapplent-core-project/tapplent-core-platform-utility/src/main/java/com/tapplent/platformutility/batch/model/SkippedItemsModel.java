package com.tapplent.platformutility.batch.model;

public class SkippedItemsModel {

	private String skippedElmntPkId;
	private String schedulerInstanceFkId;
	private String errorMessageBigTxt;
	private String linkedMtPeCodeFkId;
	private String linkedDoRowPrimaryKeyTxt;
	private String linkedDoRowVersionFkId;

	public String getSkippedElmntPkId() {
		return skippedElmntPkId;
	}

	public void setSkippedElmntPkId(String skippedElmntPkId) {
		this.skippedElmntPkId = skippedElmntPkId;
	}

	public String getSchedulerInstanceFkId() {
		return schedulerInstanceFkId;
	}

	public void setSchedulerInstanceFkId(String schedulerInstanceFkId) {
		this.schedulerInstanceFkId = schedulerInstanceFkId;
	}

	public String getErrorMessageBigTxt() {
		return errorMessageBigTxt;
	}

	public void setErrorMessageBigTxt(String errorMessageBigTxt) {
		this.errorMessageBigTxt = errorMessageBigTxt;
	}

	public String getLinkedMtPeCodeFkId() {
		return linkedMtPeCodeFkId;
	}

	public void setLinkedMtPeCodeFkId(String linkedMtPeCodeFkId) {
		this.linkedMtPeCodeFkId = linkedMtPeCodeFkId;
	}

	public String getLinkedDoRowPrimaryKeyTxt() {
		return linkedDoRowPrimaryKeyTxt;
	}

	public void setLinkedDoRowPrimaryKeyTxt(String linkedDoRowPrimaryKeyTxt) {
		this.linkedDoRowPrimaryKeyTxt = linkedDoRowPrimaryKeyTxt;
	}

	public String getLinkedDoRowVersionFkId() {
		return linkedDoRowVersionFkId;
	}

	public void setLinkedDoRowVersionFkId(String linkedDoRowVersionFkId) {
		this.linkedDoRowVersionFkId = linkedDoRowVersionFkId;
	}
}
