package com.tapplent.platformutility.metadata.structure;

/**
 * Created by tapplent on 23/11/16.
 */
public class DependencyKeysHelper {
    private String currentMTPEAlias;
    private String currentDOACode;
    private String parentMTPEAlias;
    private String parentDOACode;

    public DependencyKeysHelper(String currentDOACode, String parentDOACode, String currentMTPEAlias, String parentMTPEAlias) {
        this.currentDOACode = currentDOACode;
        this.parentDOACode = parentDOACode;
        this.currentMTPEAlias = currentMTPEAlias;
        this.parentMTPEAlias = parentMTPEAlias;
    }

    public String getCurrentDOACode() {
        return currentDOACode;
    }

    public void setCurrentDOACode(String currentDOACode) {
        this.currentDOACode = currentDOACode;
    }

    public String getParentDOACode() {
        return parentDOACode;
    }

    public void setParentDOACode(String parentDOACode) {
        this.parentDOACode = parentDOACode;
    }

    public String getCurrentMTPEAlias() {
        return currentMTPEAlias;
    }

    public void setCurrentMTPEAlias(String currentMTPEAlias) {
        this.currentMTPEAlias = currentMTPEAlias;
    }

    public String getParentMTPEAlias() {
        return parentMTPEAlias;
    }

    public void setParentMTPEAlias(String parentMTPEAlias) {
        this.parentMTPEAlias = parentMTPEAlias;
    }
}
