package com.tapplent.platformutility.workflow.structure;

/**
 * Created by Manas on 8/30/16.
 */
public class WorkflowTxn {
    private String workflowTxnPkId;
    private String workflowDefinitionFkId;
    private String doRowPrimaryKeyTxt;
    private String doRowVersionFkId;
    private String underlyingSrvrActionCodeFkId;
    private String workflowStatusCodeFkId;
    private String srcMtPeCodeFkId;
    private String srcBtCodeFkId;
    private boolean isPropogateFlag;

    public String getWorkflowTxnPkId() {
        return workflowTxnPkId;
    }

    public void setWorkflowTxnPkId(String workflowTxnPkId) {
        this.workflowTxnPkId = workflowTxnPkId;
    }

    public String getWorkflowDefinitionFkId() {
        return workflowDefinitionFkId;
    }

    public void setWorkflowDefinitionFkId(String workflowDefinitionFkId) {
        this.workflowDefinitionFkId = workflowDefinitionFkId;
    }

    public String getDoRowPrimaryKeyTxt() {
        return doRowPrimaryKeyTxt;
    }

    public void setDoRowPrimaryKeyTxt(String doRowPrimaryKeyTxt) {
        this.doRowPrimaryKeyTxt = doRowPrimaryKeyTxt;
    }

    public String getDoRowVersionFkId() {
        return doRowVersionFkId;
    }

    public void setDoRowVersionFkId(String doRowVersionFkId) {
        this.doRowVersionFkId = doRowVersionFkId;
    }

    public String getUnderlyingSrvrActionCodeFkId() {
        return underlyingSrvrActionCodeFkId;
    }

    public void setUnderlyingSrvrActionCodeFkId(String underlyingSrvrActionCodeFkId) {
        this.underlyingSrvrActionCodeFkId = underlyingSrvrActionCodeFkId;
    }

    public String getWorkflowStatusCodeFkId() {
        return workflowStatusCodeFkId;
    }

    public void setWorkflowStatusCodeFkId(String workflowStatusCodeFkId) {
        this.workflowStatusCodeFkId = workflowStatusCodeFkId;
    }

    public String getSrcMtPeCodeFkId() {
        return srcMtPeCodeFkId;
    }

    public void setSrcMtPeCodeFkId(String srcMtPeCodeFkId) {
        this.srcMtPeCodeFkId = srcMtPeCodeFkId;
    }

    public String getSrcBtCodeFkId() {
        return srcBtCodeFkId;
    }

    public void setSrcBtCodeFkId(String srcBtCodeFkId) {
        this.srcBtCodeFkId = srcBtCodeFkId;
    }

    public boolean isPropogateFlag() {
        return isPropogateFlag;
    }

    public void setPropogateFlag(boolean propogateFlag) {
        isPropogateFlag = propogateFlag;
    }
}
