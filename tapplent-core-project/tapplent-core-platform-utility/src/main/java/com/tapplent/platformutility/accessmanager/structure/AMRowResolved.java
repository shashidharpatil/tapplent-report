package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by tapplent on 04/11/16.
 */
public class AMRowResolved {
    private String resolvedRowPkId;
    private String whoPersonId;
    private String domainObjectId;
    private String whatDbPrimaryKeyId;
    private String whoseSubjectUserId;
    private boolean isCreatePermitted;
    private boolean isReadCurrentPermitted;
    private boolean isReadHistoryPermitted;
    private boolean isReadFuturePermitted;
    private boolean isUpdateCurrentPermitted;
    private boolean isUpdateHistoryPermitted;
    private boolean isUpdateFuturePermitted;
    private boolean isDeleteCurrentPermitted;
    private boolean isDeleteHistoryPermitted;
    private boolean isDeleteFuturePermitted;

    public String getResolvedRowPkId() {
        return resolvedRowPkId;
    }

    public void setResolvedRowPkId(String resolvedRowPkId) {
        this.resolvedRowPkId = resolvedRowPkId;
    }

    public String getWhoPersonId() {
        return whoPersonId;
    }

    public void setWhoPersonId(String whoPersonId) {
        this.whoPersonId = whoPersonId;
    }

    public String getDomainObjectId() {
        return domainObjectId;
    }

    public void setDomainObjectId(String domainObjectId) {
        this.domainObjectId = domainObjectId;
    }

    public String getWhatDbPrimaryKeyId() {
        return whatDbPrimaryKeyId;
    }

    public void setWhatDbPrimaryKeyId(String whatDbPrimaryKeyId) {
        this.whatDbPrimaryKeyId = whatDbPrimaryKeyId;
    }

    public String getWhoseSubjectUserId() {
        return whoseSubjectUserId;
    }

    public void setWhoseSubjectUserId(String whoseSubjectUserId) {
        this.whoseSubjectUserId = whoseSubjectUserId;
    }

    public boolean isCreatePermitted() {
        return isCreatePermitted;
    }

    public void setCreatePermitted(boolean createPermitted) {
        isCreatePermitted = createPermitted;
    }

    public boolean isReadCurrentPermitted() {
        return isReadCurrentPermitted;
    }

    public void setReadCurrentPermitted(boolean readCurrentPermitted) {
        isReadCurrentPermitted = readCurrentPermitted;
    }

    public boolean isReadHistoryPermitted() {
        return isReadHistoryPermitted;
    }

    public void setReadHistoryPermitted(boolean readHistoryPermitted) {
        isReadHistoryPermitted = readHistoryPermitted;
    }

    public boolean isReadFuturePermitted() {
        return isReadFuturePermitted;
    }

    public void setReadFuturePermitted(boolean readFuturePermitted) {
        isReadFuturePermitted = readFuturePermitted;
    }

    public boolean isUpdateCurrentPermitted() {
        return isUpdateCurrentPermitted;
    }

    public void setUpdateCurrentPermitted(boolean updateCurrentPermitted) {
        isUpdateCurrentPermitted = updateCurrentPermitted;
    }

    public boolean isUpdateHistoryPermitted() {
        return isUpdateHistoryPermitted;
    }

    public void setUpdateHistoryPermitted(boolean updateHistoryPermitted) {
        isUpdateHistoryPermitted = updateHistoryPermitted;
    }

    public boolean isUpdateFuturePermitted() {
        return isUpdateFuturePermitted;
    }

    public void setUpdateFuturePermitted(boolean updateFuturePermitted) {
        isUpdateFuturePermitted = updateFuturePermitted;
    }

    public boolean isDeleteCurrentPermitted() {
        return isDeleteCurrentPermitted;
    }

    public void setDeleteCurrentPermitted(boolean deleteCurrentPermitted) {
        isDeleteCurrentPermitted = deleteCurrentPermitted;
    }

    public boolean isDeleteHistoryPermitted() {
        return isDeleteHistoryPermitted;
    }

    public void setDeleteHistoryPermitted(boolean deleteHistoryPermitted) {
        isDeleteHistoryPermitted = deleteHistoryPermitted;
    }

    public boolean isDeleteFuturePermitted() {
        return isDeleteFuturePermitted;
    }

    public void setDeleteFuturePermitted(boolean deleteFuturePermitted) {
        isDeleteFuturePermitted = deleteFuturePermitted;
    }
}
