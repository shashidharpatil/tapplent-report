package com.tapplent.platformutility.metadata.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;

public class MasterEntityMetadataVOX extends MasterEntityMetadata {
	private Map<String, MasterEntityAttributeMetadata> attributeNameMap = new HashMap<>();
	private Map<String, MasterEntityAttributeMetadata> attributeColumnNameMap = new HashMap<>();
	private Map<String,Map<String, EntityAttributeRelationMetadata>> relatedAttributeNameMap = new HashMap<>();
	private Map<String,Map<String, EntityAttributeRelationMetadata>> relatedAttributeColumnNameMap = new HashMap<>();
	private List<MasterEntityAttributeMetadata> localSearchableFields = new ArrayList<>();
	private MasterEntityAttributeMetadata functionalPrimaryKeyAttribute;
	private MasterEntityAttributeMetadata hierarchyKeyAttribute = null;
	private boolean isHierarchyTable = false;
	/* It is db Primary key attribute */
	private MasterEntityAttributeMetadata versionIdAttribute;
	private MasterEntityAttributeMetadata recordStateAttribute;
	private MasterEntityAttributeMetadata saveStateAttribute;
	private MasterEntityAttributeMetadata activeStateAttribute;
	private MasterEntityAttributeMetadata isDeletedAttribute;
	private MasterEntityAttributeMetadata versionChangesBlobAttribute;
	private MasterEntityAttributeMetadata isFeaturedAttribute;
	private MasterEntityAttributeMetadata referenceBaseTemplateAttribute;
	private MasterEntityAttributeMetadata transitioningBaseTemplateAttribute;
	private MasterEntityAttributeMetadata workFlowTransactionStatusCodeAttribute;
	
	public static MasterEntityMetadataVOX create(MasterEntityMetadata meta) {
		if (meta == null) return null;
		MasterEntityMetadataVOX vox = new MasterEntityMetadataVOX();
        BeanUtils.copyProperties(meta, vox);
        vox.initialize();
        return vox;
	}

	private void initialize() {
		
		for (MasterEntityAttributeMetadata mstEntityAttributeMetadata : getAttributeMeta()) {
			attributeNameMap.put(mstEntityAttributeMetadata.getDoAttributeCode().trim(), mstEntityAttributeMetadata);
			attributeColumnNameMap.put(mstEntityAttributeMetadata.getDbColumnName().trim(), mstEntityAttributeMetadata);
//			attributeBtDoaSpecMap.put(mstEntityAttributeMetadata.getDoAttributeCodeFkId().trim(), mstEntityAttributeMetadata);
//			Map<String, EntityAttributeRelationMetadata> relatedAttributeByName = new HashMap<>();//FIXME change it for master
//			Map<String, EntityAttributeRelationMetadata> relatedAttributeByColumnName = new HashMap<>();
//			for(EntityAttributeRelationMetadata attributeRelationMetadata : mstEntityAttributeMetadata.getRelationMeta()){
//				relatedAttributeByName.put(attributeRelationMetadata.getMtDoAttributeSpecG11nText().trim(), attributeRelationMetadata);
//				relatedAttributeByColumnName.put(attributeRelationMetadata.getDbColumnName().trim(), attributeRelationMetadata);
//			}
//			relatedAttributeNameMap.put(mstEntityAttributeMetadata.getDoAttributeCodeFkId(), relatedAttributeByName);
//			relatedAttributeColumnNameMap.put(mstEntityAttributeMetadata.getDoAttributeCodeFkId(), relatedAttributeByColumnName);
			if(mstEntityAttributeMetadata.isLocalSearchable()){
				localSearchableFields.add(mstEntityAttributeMetadata);
			}
			if(mstEntityAttributeMetadata.isFunctionalPrimaryKey()){
				functionalPrimaryKeyAttribute = mstEntityAttributeMetadata;
			}
			if(mstEntityAttributeMetadata.isDbPrimaryKey()){
				versionIdAttribute = mstEntityAttributeMetadata;
			}
			if(mstEntityAttributeMetadata.isSelfJoinParent()){
				hierarchyKeyAttribute = mstEntityAttributeMetadata;
				isHierarchyTable = true;
			}
			if(mstEntityAttributeMetadata.getDoAttributeCode().endsWith(".RecordState")){
				recordStateAttribute = mstEntityAttributeMetadata;
			}
			if(mstEntityAttributeMetadata.getDoAttributeCode().endsWith(".ActiveState")){
				activeStateAttribute = mstEntityAttributeMetadata;
			}
			if(mstEntityAttributeMetadata.getDoAttributeCode().endsWith(".SaveState")){
				saveStateAttribute = mstEntityAttributeMetadata;
			}
			if(mstEntityAttributeMetadata.getDoAttributeCode().endsWith(".Deleted")){
				isDeletedAttribute = mstEntityAttributeMetadata;
			}
			if(mstEntityAttributeMetadata.getDoAttributeCode().endsWith(".VersionChanges")){
				versionChangesBlobAttribute = mstEntityAttributeMetadata;
			}
			if(mstEntityAttributeMetadata.getDoAttributeCode().endsWith(".Featured")){
				isFeaturedAttribute = mstEntityAttributeMetadata;
			}
			if(mstEntityAttributeMetadata.getDoAttributeCode().endsWith(".ReferenceBaseTemplate")){
				referenceBaseTemplateAttribute = mstEntityAttributeMetadata;
			}
			if(mstEntityAttributeMetadata.getDoAttributeCode().endsWith(".TransitioningBaseTemplate")){
				transitioningBaseTemplateAttribute = mstEntityAttributeMetadata;
			}
			if(mstEntityAttributeMetadata.getDoAttributeCode().endsWith(".ReferenceWorkflowTransactionStatus")){
				workFlowTransactionStatusCodeAttribute = mstEntityAttributeMetadata;
			}
		}
	}
	
	public void setAttributeByName(String attributeName, MasterEntityAttributeMetadata attribute) {
		this.attributeNameMap.put(attributeName, attribute);
	}

	public MasterEntityAttributeMetadata getAttributeByName(String attributeName) {
		return this.attributeNameMap.get(attributeName);
	}

	public void setAttributeByColumnName(String columnName, MasterEntityAttributeMetadata attribute) {
		this.attributeColumnNameMap.put(columnName, attribute);
	}

	public MasterEntityAttributeMetadata getAttributeByColumnName(String columnName) {
		return this.attributeColumnNameMap.get(columnName);
	}

	public Map<String, MasterEntityAttributeMetadata> getAttributeNameMap() {
		return attributeNameMap;
	}

	public void setAttributeNameMap(Map<String, MasterEntityAttributeMetadata> attributeNameMap) {
		this.attributeNameMap = attributeNameMap;
	}

	public Map<String, MasterEntityAttributeMetadata> getAttributeColumnNameMap() {
		return attributeColumnNameMap;
	}

	public void setAttributeColumnNameMap(Map<String, MasterEntityAttributeMetadata> attributeColumnNameMap) {
		this.attributeColumnNameMap = attributeColumnNameMap;
	}

	public Map<String, Map<String, EntityAttributeRelationMetadata>> getRelatedAttributeNameMap() {
		return relatedAttributeNameMap;
	}

	public void setRelatedAttributeNameMap(
			Map<String, Map<String, EntityAttributeRelationMetadata>> relatedAttributeNameMap) {
		this.relatedAttributeNameMap = relatedAttributeNameMap;
	}

	public Map<String, Map<String, EntityAttributeRelationMetadata>> getRelatedAttributeColumnNameMap() {
		return relatedAttributeColumnNameMap;
	}

	public void setRelatedAttributeColumnNameMap(
			Map<String, Map<String, EntityAttributeRelationMetadata>> relatedAttributeColumnNameMap) {
		this.relatedAttributeColumnNameMap = relatedAttributeColumnNameMap;
	}

	public MasterEntityAttributeMetadata getPrimaryKeyAttribute() {
		return functionalPrimaryKeyAttribute;
	}

	public void setPrimaryKeyAttribute(MasterEntityAttributeMetadata primaryKeyAttribute) {
		this.functionalPrimaryKeyAttribute = primaryKeyAttribute;
	}

	public MasterEntityAttributeMetadata getVersionIdAttribute() {
		return versionIdAttribute;
	}

	public void setVersionIdAttribute(MasterEntityAttributeMetadata versionIdAttribute) {
		this.versionIdAttribute = versionIdAttribute;
	}

	public MasterEntityAttributeMetadata getHierarchyKeyAttribute() {
		return hierarchyKeyAttribute;
	}

	public void setHierarchyKeyAttribute(MasterEntityAttributeMetadata hierarchyKeyAttribute) {
		this.hierarchyKeyAttribute = hierarchyKeyAttribute;
	}

	public MasterEntityAttributeMetadata getRecordStateAttribute() {
		return recordStateAttribute;
	}

	public void setRecordStateAttribute(MasterEntityAttributeMetadata recordStateAttribute) {
		this.recordStateAttribute = recordStateAttribute;
	}

	public MasterEntityAttributeMetadata getFunctionalPrimaryKeyAttribute() {
		return functionalPrimaryKeyAttribute;
	}

	public void setFunctionalPrimaryKeyAttribute(MasterEntityAttributeMetadata functionalPrimaryKeyAttribute) {
		this.functionalPrimaryKeyAttribute = functionalPrimaryKeyAttribute;
	}

	public MasterEntityAttributeMetadata getSaveStateAttribute() {
		return saveStateAttribute;
	}

	public void setSaveStateAttribute(MasterEntityAttributeMetadata saveStateAttribute) {
		this.saveStateAttribute = saveStateAttribute;
	}

	public MasterEntityAttributeMetadata getActiveStateAttribute() {
		return activeStateAttribute;
	}

	public void setActiveStateAttribute(MasterEntityAttributeMetadata activeStateAttribute) {
		this.activeStateAttribute = activeStateAttribute;
	}

	public MasterEntityAttributeMetadata getIsDeletedAttribute() {
		return isDeletedAttribute;
	}

	public void setIsDeletedAttribute(MasterEntityAttributeMetadata isDeletedAttribute) {
		this.isDeletedAttribute = isDeletedAttribute;
	}

	public MasterEntityAttributeMetadata getVersionChangesBlobAttribute() {
		return versionChangesBlobAttribute;
	}

	public void setVersionChangesBlobAttribute(MasterEntityAttributeMetadata versionChangesBlobAttribute) {
		this.versionChangesBlobAttribute = versionChangesBlobAttribute;
	}

	public MasterEntityAttributeMetadata getIsFeaturedAttribute() {
		return isFeaturedAttribute;
	}

	public void setIsFeaturedAttribute(MasterEntityAttributeMetadata isFeaturedAttribute) {
		this.isFeaturedAttribute = isFeaturedAttribute;
	}

	public MasterEntityAttributeMetadata getReferenceBaseTemplateAttribute() {
		return referenceBaseTemplateAttribute;
	}

	public void setReferenceBaseTemplateAttribute(MasterEntityAttributeMetadata referenceBaseTemplateAttribute) {
		this.referenceBaseTemplateAttribute = referenceBaseTemplateAttribute;
	}

	public MasterEntityAttributeMetadata getTransitioningBaseTemplateAttribute() {
		return transitioningBaseTemplateAttribute;
	}

	public void setTransitioningBaseTemplateAttribute(MasterEntityAttributeMetadata transitioningBaseTemplateAttribute) {
		this.transitioningBaseTemplateAttribute = transitioningBaseTemplateAttribute;
	}

	public MasterEntityAttributeMetadata getWorkFlowTransactionStatusCodeAttribute() {
		return workFlowTransactionStatusCodeAttribute;
	}

	public void setWorkFlowTransactionStatusCodeAttribute(
			MasterEntityAttributeMetadata workFlowTransactionStatusCodeAttribute) {
		this.workFlowTransactionStatusCodeAttribute = workFlowTransactionStatusCodeAttribute;
	}

	public List<MasterEntityAttributeMetadata> getLocalSearchableFields() {
		return localSearchableFields;
	}

	public void setLocalSearchableFields(List<MasterEntityAttributeMetadata> localSearchableFields) {
		this.localSearchableFields = localSearchableFields;
	}

	public boolean isHierarchyTable() {
		return isHierarchyTable;
	}

	public void setHierarchyTable(boolean hierarchyTable) {
		isHierarchyTable = hierarchyTable;
	}
}
