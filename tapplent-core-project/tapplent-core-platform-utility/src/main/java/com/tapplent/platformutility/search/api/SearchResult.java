package com.tapplent.platformutility.search.api;

import java.util.*;

public class SearchResult {
	private List<Map<String, Object>> data = new LinkedList<>();
	private int numberOfRecords;
	public int getNumberOfRecords() {
		return numberOfRecords;
	}

	public void setNumberOfRecords(int numberOfRecords) {
		this.numberOfRecords = numberOfRecords;
	}

	public List<Map<String, Object>> getData() {
		return data;
	}

	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}
}
