package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 12/09/17.
 */
public class WishTemplateVO {
    private String wishTemplateID;
    private String preferencesID;
    private String wishTemplateType;
    private String messageHeader;
    private String messageBody;
    private String messageFooter;

    public String getWishTemplateID() {
        return wishTemplateID;
    }

    public void setWishTemplateID(String wishTemplateID) {
        this.wishTemplateID = wishTemplateID;
    }

    public String getPreferencesID() {
        return preferencesID;
    }

    public void setPreferencesID(String preferencesID) {
        this.preferencesID = preferencesID;
    }

    public String getWishTemplateType() {
        return wishTemplateType;
    }

    public void setWishTemplateType(String wishTemplateType) {
        this.wishTemplateType = wishTemplateType;
    }

    public String getMessageHeader() {
        return messageHeader;
    }

    public void setMessageHeader(String messageHeader) {
        this.messageHeader = messageHeader;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageFooter() {
        return messageFooter;
    }

    public void setMessageFooter(String messageFooter) {
        this.messageFooter = messageFooter;
    }
}
