package com.tapplent.platformutility.insert.impl;

import com.tapplent.platformutility.insert.structure.EmployeeCodeGenRuleVO;
import com.tapplent.platformutility.insert.structure.UsernameGenRuleVo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface InsertDAO {
	int insertRow(String insertQuery) throws SQLException;
	
	void insertRowWithParameter(String sql,Deque<Object> parameter) throws SQLException;

	void insertG11Values(Map<String, Object> labels, String g11nTableName) throws Exception;

	void insertRowWithParameterMap(String SQL, LinkedHashMap<String, String> parameterMap);

	Timestamp findCurrentRecord(String tableName, String primaryKey, String dataType, Object primaryKeyValue, String currentTimestampString) throws SQLException;

	void updateRecordState(String tableName, String primaryKey, Object primaryKeyValue, String dataType, String currentTimestamp) throws SQLException;

    EmployeeCodeGenRuleVO getEmployeeCodeGenRule(String employeeCodeGenKey) throws SQLException;

	String calculateFormula(String formula) throws SQLException;

	UsernameGenRuleVo getUsernameGenRule(String usernameGenKey) throws SQLException;

	void insertUser(Map<String, Object> data, String username, String encryptedPassword, String currentTimestampString, String userEffectiveDatetime) throws SQLException;

	void updateEmployeeCodeSequence(String employeeCodeGenKey) throws SQLException;

	ResultSet search(String sql, List<Object> params) throws SQLException;
}
