package com.tapplent.platformutility.analytics.structure;

/**
 * Created by tapplent on 25/08/17.
 */
public enum CalendarPeriodUnitEnum {
    YEAR,
    HALF_YEAR,
    QUARTER,
    MONTH,
    WEEK,
    DAY,
    HOUR
}
/*
    If it is hour: It is not applicable right now
    If it is DAY: Just give all the dates between the start data and the end date.
    If it is WEEK: Apply offset logic and give all the saturdays.
    If it is MONTH: Just give all the last dates of all the months.
    If it is Quarter: Apply offset logic and get all the last dates of the Quarters applicable.
    If it is Half year: Apply offset and get all the last dates of the Half year applicable.
    If it is Year: Apply offset and get all the last dates of the year applicable.
 */