package com.tapplent.platformutility.etl.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Deque;
import java.util.List;
import java.util.Map;

import com.tapplent.platformutility.common.util.UpdateColumn;
import com.tapplent.platformutility.etl.valueObject.*;
import com.tapplent.platformutility.layout.valueObject.DOADetails;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;
import com.tapplent.platformutility.search.api.SearchResult;

public interface EtlDAO {

	List<EtlHeaderVO> getAllHeaders();

	EtlHeaderVO getHeaderByHeaderId(String headerId);

	List<EtlMapVO> getEtlMapByHeaderId(String headerId);

	List<EtlMapDetailsVO> getEtlMapDetailsByHeaderId(String headerId);

	String getprocessElementIdByBtDo(String btDoFkId, String bt);

	List<Map<String, Object>> getDataListFromTable(String string, EntityMetadataVOX sourceEntityMetadataVOX);
	
	public void insert(String tableName, Map<String, Object> columnNameValueMap);
	
	int executeUpdate(String SQL, List<Object> parameters) throws SQLException;
	
	public Map<String, String> get_Access_Token(String userId);
	
	public void updateColumns(UpdateColumn updateColumn, String binaryValue);
	
	public DOADetails getDOADetailsByDOAName(String doaName);
	
	public Map<String, String> getDOADetailByDOAName(String fieldName);


	void updateParentCanvasTxn() throws SQLException;

	List<String> getHierarchialRoots(String tableName, String primaryKeyColumnName, String selfJoinParentColumnName);

	List<String> getChildrenListForParent(String tableName, String primaryKeyColumnName, String selfJoinParentColumnName,
			String parentId);

	void updateLeftRightAndHierachyValues(String tableName, String primaryKeyColumnName,
			String selfJoinParentColumnName, String parentId, int leftValue, int rightValue, String hierarchyId) throws SQLException;

	Map<String, Object> getUnderlyingExistingRecord(String string, EntityMetadataVOX targetEntityMetadataVOX);

	void deleteAllFromTargetTable(String dbTableName) throws SQLException;

	List<Map<String, Object>> getDataListFromTableMaster(String sql, MasterEntityMetadataVOX entityMetadataVOX);

	SearchResult search(String string, Deque<Object> arguments);

	ETLMetaHelperVO getMetaByDoName(String doName);

	void insertRowAccessCondition(String rowAccessPkId, String personId, String doName, Object value, Object subjectUserId, boolean isUpdatePermissioned, boolean isDeletePermissioned, boolean isDeleted, String createdDateTime) throws SQLException;

	void insertColumnAccessCondition(String rowAccessPkId, List<String> doAttrCodeList, boolean isReadPermissioned, boolean isUpdatePermissioned, boolean isDeleted, String createdDateTime) throws SQLException;

    void addG11nStoredProcedure() throws SQLException;

	Map<String, DOADetails> getDoaMetaByDoName(String doName);

    List<DOADetails> getBTDOADetails();

    List<ETLMetaHelperVO> getAccessControlGovernedDOs();

	void populateAccessControlforDO(ETLMetaHelperVO helperVO) throws SQLException;

    List<ETLMetaHelperVO> getSubjectUserDrivenDOs();

    List<String> getHierarchicalDOs();

	ResultSet executeSQL(String SQL, List<Object> parameters);

	String getSubjectUserId(StringBuilder sql, String primaryKeyDBDataType);

	Map<String, List<String>> getChildrenListForParentPersons(List<String> rootPersonPrimaryIds);

	void updateLeftRightAndHierachyValuesForPerson(String parentId, int leftValue, int rightValue, String hierarchyId) throws SQLException;

    List<String> getDBPrimaryKeyList(String tableName, String primaryKeyColumnName);


    Map<String,Object> getAllRecords(String subjectUserColumnName, String primaryKeyColumnName, String tableName);

	List<String> getAccessManagerTables(String pattern);


    void deleteAllFromSheetIDToDBIDTable() throws SQLException;

	List<SheetIDToDBIDHelper> getAllColumnTypeToIDValues();

	void addEntryToSheetIDTODBIDMap(String columnType, String primaryKeyName, String primaryKeyId) throws SQLException;


	void deleteAllEntriesFromTable(String tableName) throws SQLException;

    void deleteFKEnteriesFromSheetIDToDBIDTable(String foreignKeyName);

    void updateRowConditionDefaultValues();

    void updatePersonJobRelationShipSubjectUser();
}
