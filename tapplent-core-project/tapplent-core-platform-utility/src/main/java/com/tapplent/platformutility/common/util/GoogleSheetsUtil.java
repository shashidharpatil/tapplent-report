package com.tapplent.platformutility.common.util;

import java.io.BufferedWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class GoogleSheetsUtil extends Thread {
	/*
	 *  @author : Shashidhar Patil
	 *  @since : 27/05/2016 
	 *  This method downloads all google sheets into the folder specified by using access token.
	 */
	private String accessToken;
	private String fileId;
	private String folderpath;
	private String formatNumber;
	static CountDownLatch count;
	
	GoogleSheetsUtil(String fileid,String accessToken, String formatNumber){
		this.fileId=fileid;
		this.accessToken=accessToken;
		this.formatNumber = formatNumber;
	}
	public static void downloadSheetsToFolder(List<GoogleSheetsDetails> fileInfoList, String accessToken) {
		count = new CountDownLatch(fileInfoList.size());
		GoogleSheetsUtil[] threads = new GoogleSheetsUtil[fileInfoList.size()];
		for(int i=0;i<threads.length;i++){
			threads[i] = new GoogleSheetsUtil(fileInfoList.get(i).getFileId(),accessToken, fileInfoList.get(i).getFormatNumber());
			threads[i].start();	
		}
	    try {
	    	count.await();
        	for(int j=0;j<threads.length;j++){
        		fileInfoList.get(j).setFolderPath(threads[j].folderpath);
        	}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void run(){
		StringBuilder folderPath = new StringBuilder("/Users/tapplent/Documents/DataUploadSheets/");
		String fileID=this.fileId;
	    StringBuilder getRequest = new StringBuilder("https://spreadsheets.google.com/feeds/worksheets/");
	    getRequest.append(fileID).append("/private/full");
	    HttpGet get = new HttpGet(getRequest.toString());
	    get.addHeader("Authorization", "Bearer " + this.accessToken);
	    get.addHeader("Gdata-version", "3.0");
	    HttpClient client1 = HttpClientBuilder.create().build();
	    HttpResponse res2;
	    try {
	        res2 = client1.execute(get);
	        HttpEntity resEntity = res2.getEntity();
	        String xmlResponse = EntityUtils.toString(resEntity);
//	        System.out.println(xmlResponse);
	        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder;
	        InputSource is;
	        builder = factory.newDocumentBuilder();
	        is = new InputSource(new StringReader(xmlResponse));
	        Document doc = builder.parse(is);
	        doc.getDocumentElement().normalize();
	        Element rootElement = doc.getDocumentElement();
	        System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
	        //getting the root title
	        NodeList rootTitleList = rootElement.getElementsByTagName("title");
	        Node rootTitleNode = rootTitleList.item(0);
	        String rootTitle = rootTitleNode.getFirstChild().getNodeValue();
	        if(!formatNumber.equals("3")){
	        	folderPath.append(rootTitle).append("/");
	        }else{
	        	folderPath.append("UploadETL").append("/");
	        }
	        if(folderCreated(folderPath.toString())){
		        NodeList nList = doc.getElementsByTagName("entry");
		        if(this.formatNumber.equals("3") && (nList.getLength()!=1)){
		        	try {
						throw new Exception("Worksheet inside ETL Upload sheet with fileId {"+this.fileId+"} does not equals => 1.");
					} catch (Exception e) {
						e.printStackTrace();
					}
		        }
		        for (int temp = 0; temp < nList.getLength(); temp++) {
		            Node nNode = nList.item(temp);
		            System.out.println("\nCurrent Element :" + nNode.getNodeName());
		            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		                Element eElement = (Element) nNode;
		                NodeList linkList = eElement.getElementsByTagName("link");
		                NodeList titleList = eElement.getElementsByTagName("title");
		                Node titleNode = titleList.item(0);
		                String title = titleNode.getFirstChild().getNodeValue();
		                for (int i = 0; i < linkList.getLength(); i++) {
		                    Node linkNode = linkList.item(i);
		                    if (linkNode.getNodeType() == Node.ELEMENT_NODE) {
		                        Element linkElement = (Element) linkNode;
		                        if (linkElement.getAttribute("type").equals("text/csv")) {
		                            System.out.println("\nCurrent Element :" + linkNode.getNodeName());
		                            System.out.println("\n" + linkElement.getAttribute("href") + "\n");
		                            String URL = linkElement.getAttribute("href");
		                            HttpGet get1 = new HttpGet(URL);
		                            get1.addHeader("Authorization", "Bearer " +this.accessToken);
		                            get1.addHeader("Gdata-version", "3.0");
		                            HttpClient client3 = HttpClientBuilder.create().build();
		                            String fileResponse = getCorrectResponse(client3,get1);
		                            StringBuilder path = new StringBuilder(folderPath);
		                            if(this.formatNumber.equals("3")){
		                            	path.append(rootTitle).append(".csv");
		                            }else{
		                            	path.append(title).append(".csv");
		                            }
	                            	File file = new File(path.toString());
	                            	file.createNewFile();
	                            	FileWriter fw2 = new FileWriter(file.getAbsolutePath());
	                            	BufferedWriter bw2=new BufferedWriter(fw2);
	                            	bw2.write(fileResponse);
	                            	bw2.close();
		                        }
		                    }
		                }
		            }
		        }
	        }else{
	        	System.out.println("Folder creation failed");
	        }
	    } catch (ParserConfigurationException e) {} catch (SAXException e) {} catch (ParseException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    this.folderpath=folderPath.toString();
	    //return folderPath.toString();
	    count.countDown();
	    return;
	}
	private static String getCorrectResponse(HttpClient client3, HttpGet get1) throws ClientProtocolException, IOException {
		HttpResponse res3 = client3.execute(get1);
        HttpEntity en = res3.getEntity();
        String fileResponse = EntityUtils.toString(en);
        if(fileResponse.contains("Wow, this file is really popular! It might be unavailable until the crowd clears.")){
            try {
				   sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        	fileResponse = getCorrectResponse(client3,get1);
        }
        return fileResponse;
	}

	private static boolean folderCreated(String folderPath) {
		// and if it doesn't we will create
		boolean result = false;
		try {
			File folder = new File(folderPath);
			if(folder.exists()){
				result = true;
				Files.walk(Paths.get(folderPath)).forEach(filePath -> {
				    if (Files.isRegularFile(filePath)) {
				    	File file = new File(filePath.toAbsolutePath().toString());
				    	if(file.exists())
				    		file.delete();
				    }
				});	
			}else{
	            if (result = folder.mkdir()) {
	            	//FIXME import here looger.
	                System.out.println("Folder called {"+folder.getName()+"} is created!");
	            } else {
	                System.out.println("Failed to create directory => {"+folder.getName()+"}");
	            }
	        }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}


