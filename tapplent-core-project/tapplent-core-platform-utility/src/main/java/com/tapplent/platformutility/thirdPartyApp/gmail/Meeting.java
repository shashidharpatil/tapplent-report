package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.sql.Timestamp;
import java.util.List;

public class Meeting {
	
	private String meetingPkId;
	private String meetingTitleG11NBigTxt;
	private String meetingDescG11NBigTxt;
	private String meetingCategoryCodeFkId;
	private String meetingEventBackgroundImageid;
	private Timestamp meetingStartDatetime;
	private Timestamp meetingEndDatetime;
	private String locationTxt;
	private String geoLoclatlng;
	private String meetingTypeCodeFkId;
	private String meetingCallInfoDetailsG11NBigTxt;
	private List<EventAttchment> attachmentIds;
	private Timestamp lastModifiedDateTime;
	private boolean isPrivate;
	public boolean isPrivate() {
		return isPrivate;
	}
	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	private boolean isDeleted;
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Timestamp getLastModifiedDateTime() {
		return lastModifiedDateTime;
	}
	public void setLastModifiedDateTime(Timestamp lastModifiedDateTime) {
		this.lastModifiedDateTime = lastModifiedDateTime;
	}
	public String getMeetingPkId() {
		return meetingPkId;
	}
	public void setMeetingPkId(String meetingPkId) {
		this.meetingPkId = meetingPkId;
	}
	public String getMeetingTitleG11NBigTxt() {
		return meetingTitleG11NBigTxt;
	}
	public void setMeetingTitleG11NBigTxt(String meetingTitleG11NBigTxt) {
		this.meetingTitleG11NBigTxt = meetingTitleG11NBigTxt;
	}
	public String getMeetingDescG11NBigTxt() {
		return meetingDescG11NBigTxt;
	}
	public void setMeetingDescG11NBigTxt(String meetingDescG11NBigTxt) {
		this.meetingDescG11NBigTxt = meetingDescG11NBigTxt;
	}
	public String getMeetingCategoryCodeFkId() {
		return meetingCategoryCodeFkId;
	}
	public void setMeetingCategoryCodeFkId(String meetingCategoryCodeFkId) {
		this.meetingCategoryCodeFkId = meetingCategoryCodeFkId;
	}
	public String getMeetingEventBackgroundImageid() {
		return meetingEventBackgroundImageid;
	}
	public void setMeetingEventBackgroundImageid(String meetingEventBackgroundImageid) {
		this.meetingEventBackgroundImageid = meetingEventBackgroundImageid;
	}
	public Timestamp getMeetingStartDatetime() {
		return meetingStartDatetime;
	}
	public void setMeetingStartDatetime(Timestamp meetingStartDatetime) {
		this.meetingStartDatetime = meetingStartDatetime;
	}
	public Timestamp getMeetingEndDatetime() {
		return meetingEndDatetime;
	}
	public void setMeetingEndDatetime(Timestamp meetingEndDatetime) {
		this.meetingEndDatetime = meetingEndDatetime;
	}
	public String getLocationTxt() {
		return locationTxt;
	}
	public void setLocationTxt(String locationTxt) {
		this.locationTxt = locationTxt;
	}
	public String getGeoLoclatlng() {
		return geoLoclatlng;
	}
	public void setGeoLoclatlng(String geoLoclatlng) {
		this.geoLoclatlng = geoLoclatlng;
	}
	public String getMeetingTypeCodeFkId() {
		return meetingTypeCodeFkId;
	}
	public void setMeetingTypeCodeFkId(String meetingTypeCodeFkId) {
		this.meetingTypeCodeFkId = meetingTypeCodeFkId;
	}
	public String getMeetingCallInfoDetailsG11NBigTxt() {
		return meetingCallInfoDetailsG11NBigTxt;
	}
	public void setMeetingCallInfoDetailsG11NBigTxt(String meetingCallInfoDetailsG11NBigTxt) {
		this.meetingCallInfoDetailsG11NBigTxt = meetingCallInfoDetailsG11NBigTxt;
	}
	public List<EventAttchment> getAttachmentIds() {
		return attachmentIds;
	}
	public void setAttachmentIds(List<EventAttchment> attachmentIds) {
		this.attachmentIds = attachmentIds;
	}
}
