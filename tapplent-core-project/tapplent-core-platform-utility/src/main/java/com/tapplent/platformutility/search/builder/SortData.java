package com.tapplent.platformutility.search.builder;

public class SortData {
	private String attributePathExpn;
	private boolean isAscending = true;
	private int seqNumber;
	private String orderByValues;
	private boolean applySortToRoot;
	private String mtPEAlias;
	public SortData(){

	}
	public SortData(String attributePathExpn, boolean isAscending, int seqNumber, String orderByValues) {
		this.attributePathExpn = attributePathExpn;
		this.isAscending = isAscending;
		this.seqNumber = seqNumber;
		this.orderByValues = orderByValues;
	}



	public String getAttributePathExpn() {
		return attributePathExpn;
	}
	public void setAttributePathExpn(String attributePathExpn) {
		this.attributePathExpn = attributePathExpn;
	}
	public int getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}
	public boolean isAscending() {
		return isAscending;
	}
	public void setIsAscending(boolean isAscending) {
		this.isAscending = isAscending;
	}
	public String getOrderByValues() {
		return orderByValues;
	}
	public void setOrderByValues(String orderByValues) {
		this.orderByValues = orderByValues;
	}
	public void setAscending(boolean isAscending) {
		this.isAscending = isAscending;
	}

	public boolean isApplySortToRoot() {
		return applySortToRoot;
	}

	public void setApplySortToRoot(boolean applySortToRoot) {
		this.applySortToRoot = applySortToRoot;
	}

	public String getMtPEAlias() {
		return mtPEAlias;
	}

	public void setMtPEAlias(String mtPEAlias) {
		this.mtPEAlias = mtPEAlias;
	}
}
