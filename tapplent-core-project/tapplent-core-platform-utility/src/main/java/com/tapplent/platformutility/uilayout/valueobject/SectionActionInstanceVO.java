package com.tapplent.platformutility.uilayout.valueobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.layout.valueObject.IconVO;

/**
 * Created by sripat on 01/11/16.
 */
public class SectionActionInstanceVO implements Serializable {
    private String actionPkId;
    private String sectionInstanceFkId;
    private String actionMasterCode;
    private String overrideActionLabel;
    private String overrideActionIcon;
    private String mtPEAlias;
    private String actionGroupCode;
    private String actionGroupIcon;
    private String actionGroupName;
    private String actionVisualMasterCode;
    private String actionVisualMasterActionLabel;
    private String actionVisualMasterActionIcon;
    private int seqNumber;
    private String controlPositionTxt;
    private String cnfMsgTxt;
    private String successMessage;
    private String failureMessage;
    private String themeSize;
    private String fontStyle;
    private String iconColour;
    private String fontCase;
    private String fontColour;
    private String backgroundColour;
    private String baseTemplateID;
    private boolean isActivityLogged;

    public String getActionPkId() {
        return actionPkId;
    }

    public void setActionPkId(String actionPkId) {
        this.actionPkId = actionPkId;
    }

    public String getActionMasterCode() {
        return actionMasterCode;
    }

    public void setActionMasterCode(String actionMasterCode) {
        this.actionMasterCode = actionMasterCode;
    }

    public String getOverrideActionLabel() {
        return overrideActionLabel;
    }

    public void setOverrideActionLabel(String overrideActionLabel) {
        this.overrideActionLabel = overrideActionLabel;
    }

    public String getOverrideActionIcon() {
        return overrideActionIcon;
    }

    public void setOverrideActionIcon(String overrideActionIcon) {
        this.overrideActionIcon = overrideActionIcon;
    }

    public String getActionVisualMasterActionLabel() {
        return actionVisualMasterActionLabel;
    }

    public void setActionVisualMasterActionLabel(String actionVisualMasterActionLabel) {
        this.actionVisualMasterActionLabel = actionVisualMasterActionLabel;
    }

    public String getActionVisualMasterActionIcon() {
        return actionVisualMasterActionIcon;
    }

    public void setActionVisualMasterActionIcon(String actionVisualMasterActionIcon) {
        this.actionVisualMasterActionIcon = actionVisualMasterActionIcon;
    }

    public String getActionGroupCode() {
        return actionGroupCode;
    }

    public void setActionGroupCode(String actionGroupCode) {
        this.actionGroupCode = actionGroupCode;
    }

    public String getActionVisualMasterCode() {
        return actionVisualMasterCode;
    }

    public void setActionVisualMasterCode(String actionVisualMasterCode) {
        this.actionVisualMasterCode = actionVisualMasterCode;
    }

    public int getSeqNumber() {
        return seqNumber;
    }

    public void setSeqNumber(int seqNumber) {
        this.seqNumber = seqNumber;
    }

    public String getControlPositionTxt() {
        return controlPositionTxt;
    }

    public void setControlPositionTxt(String controlPositionTxt) {
        this.controlPositionTxt = controlPositionTxt;
    }

    public String getCnfMsgTxt() {
        return cnfMsgTxt;
    }

    public void setCnfMsgTxt(String cnfMsgTxt) {
        this.cnfMsgTxt = cnfMsgTxt;
    }

    public String getSuccessMessage() {
        return successMessage;
    }

    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }

    public String getThemeSize() {
        return themeSize;
    }

    public void setThemeSize(String themeSize) {
        this.themeSize = themeSize;
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    public String getIconColour() {
        return iconColour;
    }

    public void setIconColour(String iconColour) {
        this.iconColour = iconColour;
    }

    public String getFontCase() {
        return fontCase;
    }

    public void setFontCase(String fontCase) {
        this.fontCase = fontCase;
    }

    public String getFontColour() {
        return fontColour;
    }

    public void setFontColour(String fontColour) {
        this.fontColour = fontColour;
    }

    public String getBackgroundColour() {
        return backgroundColour;
    }

    public void setBackgroundColour(String backgroundColour) {
        this.backgroundColour = backgroundColour;
    }

    public String getSectionInstanceFkId() {
        return sectionInstanceFkId;
    }

    public void setSectionInstanceFkId(String sectionInstanceFkId) {
        this.sectionInstanceFkId = sectionInstanceFkId;
    }

    public String getActionGroupIcon() {
        return actionGroupIcon;
    }

    public void setActionGroupIcon(String actionGroupIcon) {
        this.actionGroupIcon = actionGroupIcon;
    }

    public String getActionGroupName() {
        return actionGroupName;
    }

    public void setActionGroupName(String actionGroupName) {
        this.actionGroupName = actionGroupName;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public boolean isActivityLogged() {
        return isActivityLogged;
    }

    public void setActivityLogged(boolean activityLogged) {
        isActivityLogged = activityLogged;
    }

    public String getBaseTemplateID() {
        return baseTemplateID;
    }

    public void setBaseTemplateID(String baseTemplateID) {
        this.baseTemplateID = baseTemplateID;
    }
}
