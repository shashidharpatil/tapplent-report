package com.tapplent.platformutility.activityLog;

/**
 * Created by Tapplent on 1/10/17.
 */
public class AuditMsgFormat {
    private String versionId;
    private String AdtMsgFmtPkId;
    private String MtPeCodeFkId;
    private String btCodeFkId;
    private String ActivityLogActionCodeFkId;
    private String msgSyntaxYouInContextG11nBigTxt;
    private String msgSyntaxYouOutOfContextG11nBigTxt;
    private String msgSyntaxThirdPersonInContextG11nBigTxt;
    private String msgSyntaxThirdPersonOutOfContextG11nBigTxt;
    private boolean IsCreateDetails;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getAdtMsgFmtPkId() {
        return AdtMsgFmtPkId;
    }

    public void setAdtMsgFmtPkId(String adtMsgFmtPkId) {
        AdtMsgFmtPkId = adtMsgFmtPkId;
    }

    public String getMtPeCodeFkId() {
        return MtPeCodeFkId;
    }

    public void setMtPeCodeFkId(String mtPeCodeFkId) {
        MtPeCodeFkId = mtPeCodeFkId;
    }

    public String getActivityLogActionCodeFkId() {
        return ActivityLogActionCodeFkId;
    }

    public void setActivityLogActionCodeFkId(String activityLogActionCodeFkId) {
        ActivityLogActionCodeFkId = activityLogActionCodeFkId;
    }

    public String getBtCodeFkId() {
        return btCodeFkId;
    }

    public void setBtCodeFkId(String btCodeFkId) {
        this.btCodeFkId = btCodeFkId;
    }

    public String getMsgSyntaxYouInContextG11nBigTxt() {
        return msgSyntaxYouInContextG11nBigTxt;
    }

    public void setMsgSyntaxYouInContextG11nBigTxt(String msgSyntaxYouInContextG11nBigTxt) {
        this.msgSyntaxYouInContextG11nBigTxt = msgSyntaxYouInContextG11nBigTxt;
    }

    public String getMsgSyntaxYouOutOfContextG11nBigTxt() {
        return msgSyntaxYouOutOfContextG11nBigTxt;
    }

    public void setMsgSyntaxYouOutOfContextG11nBigTxt(String msgSyntaxYouOutOfContextG11nBigTxt) {
        this.msgSyntaxYouOutOfContextG11nBigTxt = msgSyntaxYouOutOfContextG11nBigTxt;
    }

    public String getMsgSyntaxThirdPersonInContextG11nBigTxt() {
        return msgSyntaxThirdPersonInContextG11nBigTxt;
    }

    public void setMsgSyntaxThirdPersonInContextG11nBigTxt(String msgSyntaxThirdPersonInContextG11nBigTxt) {
        this.msgSyntaxThirdPersonInContextG11nBigTxt = msgSyntaxThirdPersonInContextG11nBigTxt;
    }

    public String getMsgSyntaxThirdPersonOutOfContextG11nBigTxt() {
        return msgSyntaxThirdPersonOutOfContextG11nBigTxt;
    }

    public void setMsgSyntaxThirdPersonOutOfContextG11nBigTxt(String msgSyntaxThirdPersonOutOfContextG11nBigTxt) {
        this.msgSyntaxThirdPersonOutOfContextG11nBigTxt = msgSyntaxThirdPersonOutOfContextG11nBigTxt;
    }

    public boolean isCreateDetails() {
        return IsCreateDetails;
    }

    public void setCreateDetails(boolean createDetails) {
        IsCreateDetails = createDetails;
    }
}
