package com.tapplent.platformutility.server.property;

import java.util.List;
import java.util.Map;

import com.tapplent.tenantresolver.server.property.ServerActionStep;
import com.tapplent.tenantresolver.server.property.ServerActionStepProperty;

public interface ServerPropertyDAO {
	public Map<String, ServerActionStep> getAllServerActionStep();
	public Map<String, List<ServerActionStepProperty>> getAllServerActionStepProperty();
}
