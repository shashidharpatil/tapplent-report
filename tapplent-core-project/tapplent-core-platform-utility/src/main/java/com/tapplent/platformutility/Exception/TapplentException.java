package com.tapplent.platformutility.Exception;

/**
 * Created by Tapplent on 7/18/17.
 */
public class TapplentException extends Exception{

    public TapplentException(String message)
    {
        super(message);
    }

//    public TapplentException()
//    {
//        super();
//    }
}
