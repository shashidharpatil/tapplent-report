package com.tapplent.platformutility.search.builder.expression;

public class NodeOperand extends ExprOperand{
	Operator operator;
	final ArgumentCount ac;
	ExprOperand left;
	ExprOperand right;
	public NodeOperand(Operator operator, ArgumentCount ac) {
		this.ac=ac;
		this.operator=operator;
	}
	public ExprOperand getLeft() {
		return left;
	}
	public void setLeft(ExprOperand left) {
		this.left = left;
	}
	public ExprOperand getRight() {
		return right;
	}
	public void setRight(ExprOperand right) {
		this.right = right;
	}
	public Operator getOperator(){
		return operator;
	}
}
