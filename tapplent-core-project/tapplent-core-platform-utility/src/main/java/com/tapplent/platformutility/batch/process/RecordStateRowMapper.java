package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.RCRD_STATE_MtDOData;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RecordStateRowMapper implements RowMapper<RCRD_STATE_MtDOData> {
    @Override
    public RCRD_STATE_MtDOData mapRow(ResultSet rs, int rowNum) throws SQLException {

        RCRD_STATE_MtDOData recordStateData = new RCRD_STATE_MtDOData();
        recordStateData.setVersionId(rs.getString("VERSION_ID"));
        recordStateData.setDomainObjectCode(rs.getString("DOMAIN_OBJECT_CODE_PK_ID"));
        recordStateData.setDbTableName(rs.getString("DB_TABLE_NAME_TXT"));
        recordStateData.setDoTypeCode(rs.getString("DO_TYPE_CODE_FK_ID"));
        recordStateData.setDoTypeCode(rs.getString("EFFECTIVE_DATETIME"));
        recordStateData.setCustomizable(rs.getBoolean("IS_CUSTOMIZABLE"));
        recordStateData.setDbPkColumnName(rs.getString("DB_COLUMN_NAME_TXT"));
        return recordStateData;
    }
}
