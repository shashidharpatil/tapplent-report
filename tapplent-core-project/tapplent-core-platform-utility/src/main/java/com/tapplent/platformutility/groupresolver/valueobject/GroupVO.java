package com.tapplent.platformutility.groupresolver.valueobject;

import com.tapplent.platformutility.uilayout.valueobject.GenericQueryVO;

/**
 * Created by tapplent on 12/06/17.
 */
public class GroupVO {
    private String groupID;
    private String genericQueryID;
    private String groupType;
    private GenericQueryVO genericQueryVO;

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public String getGenericQueryID() {
        return genericQueryID;
    }

    public void setGenericQueryID(String genericQueryID) {
        this.genericQueryID = genericQueryID;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public GenericQueryVO getGenericQueryVO() {
        return genericQueryVO;
    }

    public void setGenericQueryVO(GenericQueryVO genericQueryVO) {
        this.genericQueryVO = genericQueryVO;
    }
}
