package com.tapplent.platformutility.workflow.structure;

import org.springframework.beans.BeanUtils;

import java.sql.Timestamp;

/**
 * Created by Tapplent on 8/10/16.
 */
public class WorkflowActor {
	private String workflowTxnStepActorPkId;
	private String workflowTxnStepFkId;
	private String stepActorDefnFkId;
	private String resolvedPersonFkId;
	private String stepActorStatusCodeFkId;
	private Timestamp createdDatetime;
	private Timestamp lastModifiedDatetime;
    private String workflowStepDefnFkId;
    private int actorDispSeqNumPosInt;
    private String actorNameG11nBigTxt;
    private String actorIconCodeFkId;
    private String actorPersonGroupFkId;
    private boolean isRelativeToLoggedInUser;
    private boolean isRelativeToSubjectUser;
    private String relativeToActorId;
    private String relativeRelationshipCodeFkId;
    private int relativeRelationshipHierarchyLevelPosInt;
    private Timestamp resolveActorAsOfDatetime;
    private boolean isAdhocUserActor;
    private boolean allowExternalUser;
    private String noActorsActionCodeFkId;
	
	public String getWorkflowTxnStepActorPkId() {
		return workflowTxnStepActorPkId;
	}
	public void setWorkflowTxnStepActorPkId(String workflowTxnStepActorPkId) {
		this.workflowTxnStepActorPkId = workflowTxnStepActorPkId;
	}
	public String getWorkflowTxnStepFkId() {
		return workflowTxnStepFkId;
	}
	public void setWorkflowTxnStepFkId(String workflowTxnStepFkId) {
		this.workflowTxnStepFkId = workflowTxnStepFkId;
	}
	public String getStepActorDefnFkId() {
		return stepActorDefnFkId;
	}
	public void setStepActorDefnFkId(String stepActorDefnFkId) {
		this.stepActorDefnFkId = stepActorDefnFkId;
	}
	public String getResolvedPersonFkId() {
		return resolvedPersonFkId;
	}
	public void setResolvedPersonFkId(String resolvedPersonFkId) {
		this.resolvedPersonFkId = resolvedPersonFkId;
	}
	public String getStepActorStatusCodeFkId() {
		return stepActorStatusCodeFkId;
	}
	public void setStepActorStatusCodeFkId(String stepActorStatusCodeFkId) {
		this.stepActorStatusCodeFkId = stepActorStatusCodeFkId;
	}
	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(Timestamp lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
	public String getWorkflowStepDefnFkId() {
		return workflowStepDefnFkId;
	}
	public void setWorkflowStepDefnFkId(String workflowStepDefnFkId) {
		this.workflowStepDefnFkId = workflowStepDefnFkId;
	}
	public int getActorDispSeqNumPosInt() {
		return actorDispSeqNumPosInt;
	}
	public void setActorDispSeqNumPosInt(int actorDispSeqNumPosInt) {
		this.actorDispSeqNumPosInt = actorDispSeqNumPosInt;
	}
	public String getActorNameG11nBigTxt() {
		return actorNameG11nBigTxt;
	}
	public void setActorNameG11nBigTxt(String actorNameG11nBigTxt) {
		this.actorNameG11nBigTxt = actorNameG11nBigTxt;
	}
	public String getActorIconCodeFkId() {
		return actorIconCodeFkId;
	}
	public void setActorIconCodeFkId(String actorIconCodeFkId) {
		this.actorIconCodeFkId = actorIconCodeFkId;
	}
	public String getActorPersonGroupFkId() {
		return actorPersonGroupFkId;
	}
	public void setActorPersonGroupFkId(String actorPersonGroupFkId) {
		this.actorPersonGroupFkId = actorPersonGroupFkId;
	}
	public boolean isRelativeToLoggedInUser() {
		return isRelativeToLoggedInUser;
	}
	public void setRelativeToLoggedInUser(boolean isRelativeToLoggedInUser) {
		this.isRelativeToLoggedInUser = isRelativeToLoggedInUser;
	}
	public boolean isRelativeToSubjectUser() {
		return isRelativeToSubjectUser;
	}
	public void setRelativeToSubjectUser(boolean isRelativeToSubjectUser) {
		this.isRelativeToSubjectUser = isRelativeToSubjectUser;
	}
	public String getRelativeToActorId() {
		return relativeToActorId;
	}
	public void setRelativeToActorId(String relativeToActorId) {
		this.relativeToActorId = relativeToActorId;
	}
	public String getRelativeRelationshipCodeFkId() {
		return relativeRelationshipCodeFkId;
	}
	public void setRelativeRelationshipCodeFkId(String relativeRelationshipCodeFkId) {
		this.relativeRelationshipCodeFkId = relativeRelationshipCodeFkId;
	}
	public int getRelativeRelationshipHierarchyLevelPosInt() {
		return relativeRelationshipHierarchyLevelPosInt;
	}
	public void setRelativeRelationshipHierarchyLevelPosInt(int relativeRelationshipHierarchyLevelPosInt) {
		this.relativeRelationshipHierarchyLevelPosInt = relativeRelationshipHierarchyLevelPosInt;
	}
	public Timestamp getResolveActorAsOfDatetime() {
		return resolveActorAsOfDatetime;
	}
	public void setResolveActorAsOfDatetime(Timestamp resolveActorAsOfDatetime) {
		this.resolveActorAsOfDatetime = resolveActorAsOfDatetime;
	}
	public boolean isAdhocUserActor() {
		return isAdhocUserActor;
	}
	public void setAdhocUserActor(boolean isAdhocUserActor) {
		this.isAdhocUserActor = isAdhocUserActor;
	}
	public boolean isAllowExternalUser() {
		return allowExternalUser;
	}
	public void setAllowExternalUser(boolean allowExternalUser) {
		this.allowExternalUser = allowExternalUser;
	}
	public String getNoActorsActionCodeFkId() {
		return noActorsActionCodeFkId;
	}
	public void setNoActorsActionCodeFkId(String noActorsActionCodeFkId) {
		this.noActorsActionCodeFkId = noActorsActionCodeFkId;
	}

	public static WorkflowActor fromDefinition(WorkflowActorDefn workflowActorDefn) {
		WorkflowActor workflowActor = new WorkflowActor();
		BeanUtils.copyProperties(workflowActorDefn, workflowActor);
		workflowActor.setStepActorDefnFkId(workflowActorDefn.getWorkflowActorDefnPkId());
		return workflowActor;
	}
}
