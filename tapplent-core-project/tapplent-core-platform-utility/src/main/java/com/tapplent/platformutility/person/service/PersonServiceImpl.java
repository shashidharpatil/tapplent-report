package com.tapplent.platformutility.person.service;

import com.tapplent.platformutility.insert.impl.ValidationError;
import com.tapplent.platformutility.person.dao.PersonDAO;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.person.structure.PersonGroup;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tapplent on 07/11/16.
 */
public class PersonServiceImpl implements PersonService{
    private PersonDAO personDAO;
    private ValidationError validationError;
    private PersonGroup getPersonGroups(String personId) {
        List<String> personGroupIds = personDAO.getPersonGroups(personId);
        PersonGroup result = new PersonGroup(personId, personGroupIds);
        return result;
    }

    @Override
    @Transactional
    public Person getPerson(String personId) {
        Person person = personDAO.getPerson(personId);
        if (person == null)
            validationError.addError("Invalid Person", "Person Id : "+personId+" is not valid.");
        if (validationError.hasError()){
            validationError.logValidationErrors();
//            validationError.clear();
        }
        return person;
    }

    @Override
    public String getDefaultBTForGroup(String mtPE, Person person) {
        String baseTemplate = personDAO.getDefaultBTForGroup(mtPE, person.getPersonGroups());
        return baseTemplate;
    }

    @Override
    public Person getSystemDummyPerson(String personName) {
        Person person = personDAO.getSystemDummyPerson(personName);
        if (person == null)
            validationError.addError("Invalid Person", "Person Name : "+personName+" is not valid.");
        if (validationError.hasError()){
            validationError.logValidationErrors();
        }
        return person;
    }

    public PersonDAO getPersonDAO() {
        return personDAO;
    }

    public void setPersonDAO(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    public ValidationError getValidationError() {
        return validationError;
    }

    public void setValidationError(ValidationError validationError) {
        this.validationError = validationError;
    }
}
