package com.tapplent.platformutility.layout.valueObject;

import java.net.URL;

/**
 * Created by tapplent on 15/11/16.
 */
public class PersonPreferenceVO {
    String versionId;
    private String personPreferencesPkId;
    private String personFkId;
    private String dateFormatCodeFkId;
    private String timeFormatCodeFkId;
    private String currencyCodeFkId;
    private String timezoneCodeFkId;
    private String localeCodeFkId;
    private String orgDefaultLocaleCodeFkId;
    private boolean isShowTicker;
    private String socialStatus;
    private String moodMessage;
    private URL backGroundImage;
    private URL conversationBackGroundImage;
    private URL introductionVideo;
    private URL namePronounciationAudio;
    private String namePronounciation;
    private URL personPhoto;
    private String effectiveDateTime;
    private String lastModifiedDateTime;
    private String utcOffset;

    public String getPersonPreferencesPkId() {
        return personPreferencesPkId;
    }

    public void setPersonPreferencesPkId(String personPreferencesPkId) {
        this.personPreferencesPkId = personPreferencesPkId;
    }

    public String getPersonFkId() {
        return personFkId;
    }

    public void setPersonFkId(String personFkId) {
        this.personFkId = personFkId;
    }

    public String getDateFormatCodeFkId() {
        return dateFormatCodeFkId;
    }

    public void setDateFormatCodeFkId(String dateFormatCodeFkId) {
        this.dateFormatCodeFkId = dateFormatCodeFkId;
    }

    public String getTimeFormatCodeFkId() {
        return timeFormatCodeFkId;
    }

    public void setTimeFormatCodeFkId(String timeFormatCodeFkId) {
        this.timeFormatCodeFkId = timeFormatCodeFkId;
    }

    public String getCurrencyCodeFkId() {
        return currencyCodeFkId;
    }

    public void setCurrencyCodeFkId(String currencyCodeFkId) {
        this.currencyCodeFkId = currencyCodeFkId;
    }

    public String getTimezoneCodeFkId() {
        return timezoneCodeFkId;
    }

    public void setTimezoneCodeFkId(String timezoneCodeFkId) {
        this.timezoneCodeFkId = timezoneCodeFkId;
    }

    public String getLocaleCodeFkId() {
        return localeCodeFkId;
    }

    public void setLocaleCodeFkId(String localeCodeFkId) {
        this.localeCodeFkId = localeCodeFkId;
    }

    public boolean isShowTicker() {
        return isShowTicker;
    }

    public void setShowTicker(boolean showTicker) {
        isShowTicker = showTicker;
    }

    public String getSocialStatus() {
        return socialStatus;
    }

    public void setSocialStatus(String socialStatus) {
        this.socialStatus = socialStatus;
    }

    public String getMoodMessage() {
        return moodMessage;
    }

    public void setMoodMessage(String moodMessage) {
        this.moodMessage = moodMessage;
    }

    public URL getBackGroundImage() {
        return backGroundImage;
    }

    public void setBackGroundImage(URL backGroundImage) {
        this.backGroundImage = backGroundImage;
    }

    public URL getConversationBackGroundImage() {
        return conversationBackGroundImage;
    }

    public void setConversationBackGroundImage(URL conversationBackGroundImage) {
        this.conversationBackGroundImage = conversationBackGroundImage;
    }

    public URL getIntroductionVideo() {
        return introductionVideo;
    }

    public void setIntroductionVideo(URL introductionVideo) {
        this.introductionVideo = introductionVideo;
    }

    public URL getNamePronounciationAudio() {
        return namePronounciationAudio;
    }

    public void setNamePronounciationAudio(URL namePronounciationAudio) {
        this.namePronounciationAudio = namePronounciationAudio;
    }

    public String getNamePronounciation() {
        return namePronounciation;
    }

    public void setNamePronounciation(String namePronounciation) {
        this.namePronounciation = namePronounciation;
    }

    public URL getPersonPhoto() {
        return personPhoto;
    }

    public void setPersonPhoto(URL personPhoto) {
        this.personPhoto = personPhoto;
    }

    public String getOrgDefaultLocaleCodeFkId() {
        return orgDefaultLocaleCodeFkId;
    }

    public void setOrgDefaultLocaleCodeFkId(String orgDefaultLocaleCodeFkId) {
        this.orgDefaultLocaleCodeFkId = orgDefaultLocaleCodeFkId;

    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getEffectiveDateTime() {
        return effectiveDateTime;
    }

    public void setEffectiveDateTime(String effectiveDateTime) {
        this.effectiveDateTime = effectiveDateTime;
    }

    public String getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(String lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }

    public String getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(String utcOffset) {
        this.utcOffset = utcOffset;
    }
}
