package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class UiSettingsVO {
	private String versionId;
	private String uiSettings;
	private JsonNode settings;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getUiSettings() {
		return uiSettings;
	}
	public void setUiSettings(String uiSettings) {
		this.uiSettings = uiSettings;
	}
	public JsonNode getSettings() {
		return settings;
	}
	public void setSettings(JsonNode settings) {
		this.settings = settings;
	}
}