package com.tapplent.platformutility.search.builder.joins;

public class JoinLeafOperand extends JoinOperand{
	private String tableName;
	private String tableAlias;
	public JoinLeafOperand (String tableName,String tableAlias){
		this.tableName = tableName;
		this.tableAlias = tableAlias;
	}
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableAlias() {
		return tableAlias;
	}

	public void setTableAlias(String tableAlias) {
		this.tableAlias = tableAlias;
	}
}
