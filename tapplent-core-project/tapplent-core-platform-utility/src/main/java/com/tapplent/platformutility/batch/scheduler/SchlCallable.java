package com.tapplent.platformutility.batch.scheduler;

import com.tapplent.platformutility.batch.controller.BatchDAO;

import java.util.TimerTask;

/**
 * Created by sripat on 29/08/16.
 */

public class SchlCallable extends TimerTask  {
    BatchDAO batchDAO;
    public void setBatchDAO(BatchDAO batchDAO){this.batchDAO=batchDAO;}
    @Override
    public void run()  {
        try {
            batchDAO.initializeBatch();
        }
        catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
