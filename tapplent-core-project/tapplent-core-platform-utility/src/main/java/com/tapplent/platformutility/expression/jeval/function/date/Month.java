package com.tapplent.platformutility.expression.jeval.function.date;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.expression.jeval.function.Function;
import com.tapplent.platformutility.expression.jeval.function.FunctionConstants;
import com.tapplent.platformutility.expression.jeval.function.FunctionException;
import com.tapplent.platformutility.expression.jeval.function.FunctionResult;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by sripad on 14/01/16.
 */
public class Month implements Function {

	private static final Logger Log = LogFactory.getLogger(DateAdd.class);

    /**
     * Returns the name of the function - "dateadd".
     *
     * @return The name of this function class.
     */
    public String getName() {
        return "month";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     *
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A string argument that will be converted into two string
     *            arguments and one integer argument. The first argument is the
     *            string to test, the second argument is the prefix and the
     *            third argument is the index to start at. The string
     *            argument(s) HAS to be enclosed in quotes. White space that is
     *            not enclosed within quotes will be trimmed. Quote characters
     *            in the first and last positions of any string argument (after
     *            being trimmed) will be removed also. The quote characters used
     *            must be the same as the quote characters used by the current
     *            instance of Evaluator. If there are multiple arguments, they
     *            must be separated by a comma (",").
     *
     * @return Returns "1.0" (true) if the string ends with the suffix,
     *         otherwise it returns "0.0" (false). The return value respresents
     *         a Boolean value that is compatible with the Boolean operators
     *         used by Evaluator.
     *
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    public FunctionResult execute(final Evaluator evaluator, final String arguments)
            throws FunctionException {
        String result = null;
        String exceptionMessage = "No"
                + "argument are required.";



        try {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            //getTime() returns the current date in default time zone
            Date date = calendar.getTime();
            int dayOfMonth = calendar.get(Calendar.MONTH);
            result=String.valueOf(dayOfMonth+1);
            Log.debug("day111",String.valueOf(dayOfMonth));
        }  catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result,
                FunctionConstants.FUNCTION_RESULT_TYPE_NUMERIC);
    }
}
