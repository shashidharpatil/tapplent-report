package com.tapplent.platformutility.search.builder.expression;

public class LeafOperand extends ExprOperand{
	private String value;
	private String dataType;
	private boolean isColumnName = false;
	private boolean isColumnAlias = false;
	private boolean isGlocalized = false;
	private String containerBlobName;
	private String doAttributeCode;
	private String columnName;
	private String columnAlias;
	private String tableAlias;
	public LeafOperand(){
		
	}
	public LeafOperand(String value, String dataType){
		this.value=value;
		this.dataType=dataType;
	}
	public LeafOperand(boolean isColumnName, String columnName, String doAttributeCode, String columnAlias,String dataType, String tableAlias, boolean isGlocalized, String containerBlobName){
		this.columnAlias = columnAlias;
		this.columnName = columnName;
		this.tableAlias = tableAlias;
		this.isColumnName = isColumnName;
		this.dataType = dataType;
		this.isGlocalized = isGlocalized;
		this.doAttributeCode = doAttributeCode;
		this.containerBlobName = containerBlobName;
	}
	public LeafOperand(boolean isColumnName, String columnName, String doAttributeCode, String dataType, String tableAlias){
		this.columnName = columnName;
		this.tableAlias = tableAlias;
		this.isColumnName = isColumnName;
		this.dataType = dataType;
		this.doAttributeCode = doAttributeCode;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public Boolean getIsColumnName() {
		return isColumnName;
	}
	public void setIsColumnName(Boolean isColumnName) {
		this.isColumnName = isColumnName;
	}
	public String getColumnAlias() {
		return columnAlias;
	}
	public void setColumnAlias(String columnAlias) {
		this.columnAlias = columnAlias;
	}
	public String getTableAlias() {
		return tableAlias;
	}
	public void setTableAlias(String tableAlias) {
		this.tableAlias = tableAlias;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public Boolean getIsGlocalized() {
		return isGlocalized;
	}
	public void setIsGlocalized(Boolean isGlocalized) {
		this.isGlocalized = isGlocalized;
	}
	public String getContainerBlobName() {
		return containerBlobName;
	}
	public void setContainerBlobName(String containerBlobName) {
		this.containerBlobName = containerBlobName;
	}
	public void setColumnName(boolean isColumnName) {
		this.isColumnName = isColumnName;
	}
	public void setGlocalized(boolean isGlocalized) {
		this.isGlocalized = isGlocalized;
	}
	public String getDoAttributeCode() {
		return doAttributeCode;
	}
	public void setDoAttributeCode(String doAttributeCode) {
		this.doAttributeCode = doAttributeCode;
	}

	public boolean isColumnAlias() {
		return isColumnAlias;
	}

	public void setColumnAlias(boolean columnAlias) {
		isColumnAlias = columnAlias;
	}
}
