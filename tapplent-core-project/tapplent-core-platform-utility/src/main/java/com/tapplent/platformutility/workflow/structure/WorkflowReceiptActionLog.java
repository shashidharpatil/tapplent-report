package com.tapplent.platformutility.workflow.structure;

public class WorkflowReceiptActionLog {
    private String actorReceiptActionLogPkId;
    private String stepActorId;
    private String receivedStepActorId;
    private String receivedFromPersonId;
    private String receivedFromOnBehalfOfPersonId;
    private String actionMasterCodeId;
    private String actionReceivedDatetime;
    private String receivedActionComment;
    private boolean isDeleted;

    public String getActorReceiptActionLogPkId() {
        return actorReceiptActionLogPkId;
    }

    public void setActorReceiptActionLogPkId(String actorReceiptActionLogPkId) {
        this.actorReceiptActionLogPkId = actorReceiptActionLogPkId;
    }

    public String getStepActorId() {
        return stepActorId;
    }

    public void setStepActorId(String stepActorId) {
        this.stepActorId = stepActorId;
    }

    public String getReceivedStepActorId() {
        return receivedStepActorId;
    }

    public void setReceivedStepActorId(String receivedStepActorId) {
        this.receivedStepActorId = receivedStepActorId;
    }

    public String getReceivedFromPersonId() {
        return receivedFromPersonId;
    }

    public void setReceivedFromPersonId(String receivedFromPersonId) {
        this.receivedFromPersonId = receivedFromPersonId;
    }

    public String getReceivedFromOnBehalfOfPersonId() {
        return receivedFromOnBehalfOfPersonId;
    }

    public void setReceivedFromOnBehalfOfPersonId(String receivedFromOnBehalfOfPersonId) {
        this.receivedFromOnBehalfOfPersonId = receivedFromOnBehalfOfPersonId;
    }

    public String getActionMasterCodeId() {
        return actionMasterCodeId;
    }

    public void setActionMasterCodeId(String actionMasterCodeId) {
        this.actionMasterCodeId = actionMasterCodeId;
    }

    public String getActionReceivedDatetime() {
        return actionReceivedDatetime;
    }

    public void setActionReceivedDatetime(String actionReceivedDatetime) {
        this.actionReceivedDatetime = actionReceivedDatetime;
    }

    public String getReceivedActionComment() {
        return receivedActionComment;
    }

    public void setReceivedActionComment(String receivedActionComment) {
        this.receivedActionComment = receivedActionComment;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
