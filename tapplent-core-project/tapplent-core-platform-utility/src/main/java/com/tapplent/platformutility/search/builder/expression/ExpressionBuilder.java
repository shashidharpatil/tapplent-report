package com.tapplent.platformutility.search.builder.expression;

import java.sql.Timestamp;
import java.util.Stack;

public class ExpressionBuilder {
	private Stack<String> operatorStack;
	private Stack<ExprOperand> operandStack;
	public ExpressionBuilder(){
		this.operatorStack=new Stack<>();
		this.operandStack= new Stack<>();
	}
	public ExprOperand getExpressionTree(){
		emptyOperatorStack();
		if(!operandStack.isEmpty())
			return operandStack.peek();
		return null;
	}
	public boolean isEmpty(){
		return operandStack.isEmpty();
	}
	public void addExprTokenToTree(Object token){
		if(token==null)
			return;
		//If it is an open parenthesis
		// 1. push the open parenthesis on the operator stack
		if(token.toString().equals("OPEN_PARENTHESIS")){
			operatorStack.push(token.toString());
		}
		//If it is a close parenthesis
		// 1. create a while loop till the time an open parenthesis is received.
		// 2. Based on the operator keep on making the node=operands and push them to the stack.
		else if(token.toString().equals("CLOSE_PARENTHESIS")){
			while(!operatorStack.isEmpty() && !operatorStack.peek().equals("OPEN_PARENTHESIS")){
				Operator op = Operator.valueOf(operatorStack.peek());
				buildOperandNode(op);
			}
			operatorStack.pop();
		}
		//If it is an operator. call a method named operate on operator.
		else if(isOperator(token)){
			operateOnOperator(token.toString());
			operatorStack.push(token.toString());
		}
		//Else consider it as an operand.
		// 1. Check if the input is an instance of Leaf-Operand node or not.
		// If it is push that element to the on top of operand-stack.
		else{
			if(token instanceof LeafOperand){
				operandStack.push((LeafOperand)token);
			}
		}
	}
	public void emptyOperatorStack(){
		while(!operatorStack.isEmpty()){
			Operator op = Operator.valueOf(operatorStack.peek());
			buildOperandNode(op);
		}
	}
	private void buildOperandNode(Operator op){
		ArgumentCount ac = getArgumentCount(operatorStack.peek().toString());
		NodeOperand node = new NodeOperand(op, ac);
		//get a node-operand
		switch(ac){
		case UNARY:
			ExprOperand operandPeek = operandStack.peek();
			operandStack.pop();
			node.setLeft(operandPeek);
			operandStack.push(node);
			operatorStack.pop();
			break;
		case BINARY:
		case N_ARY:	
			ExprOperand rightElement = operandStack.peek();
			operandStack.pop();
			ExprOperand leftElement = operandStack.peek();
			operandStack.pop();
			node.setLeft(leftElement);
			node.setRight(rightElement);
			operandStack.push(node);
			operatorStack.pop();
			break;
//		case N_ARY:
//			ExprOperand operandTop = operandStack.peek();
//			Stack<ExprOperand> parameters = new Stack<>();
//			if(operandTop instanceof LeafOperand){
//				LeafOperand oTop = (LeafOperand) operandTop;
//				if(oTop.getValue().equals(")")){
//					operandStack.pop();
//					while(!operandStack.isEmpty()){
//						if(operandStack.peek() instanceof LeafOperand){
//							LeafOperand loprand = (LeafOperand) operandStack.peek();
//							if(loprand.getValue().equals("(")){
//								operandStack.pop();
//								break;
//							}
//						}
//						parameters.push(operandTop);
//						//Here check if the top most element in the stack is an instance of Leaf operand
//						//If it is check the value of operand, if it is equivalent to "(" stop there. 
//						// Else keep poping the operand stack and add it to the child list of current node.
//					}
//				}
//			while(!parameters.isEmpty()){
//				node.addChildElement(parameters.peek());
//				parameters.pop();
//			}
//			break;
			//For Functions or N_ARY operators use this logic
			//So the open bracket for these function or N_ARY operators will come as operand.
			//Here pop till the open parenthesis is received. Make each element the child.
			default:
		}
	}
	private void operateOnOperator(String token) {
		//Operate on operator method
		// 1. Get the precedence of operator.
		// 2. Create a while loop to check if the operator stack is not empty and compare the precedence of top operator in operator stack with current operator
		// 2a. if the precedence of the top operator is greater than the current operator pop that operator.
					// Build a new node-Operand object. Based on the operation(unary, binary, n-ary) pop that many elements from the stack and make them children of node-operand.
					// push this new element on top of operand-stack.
		// 3. push the current operator on the operator stack.
		//Here the higher the precedence lower is its precedence index. We need to pop the elements with higher precedence.
		int precedence = precedence(token);
		while(!operatorStack.isEmpty() && precedence >= precedence(operatorStack.peek())){
			Operator opStackPeek = Operator.valueOf(operatorStack.peek());
			buildOperandNode(opStackPeek);
		}
	}
	Object getData(String value, String dataType) {
        Object data = null;
        switch (dataType) {
            case "CHAR":
            case "VARCHAR":
                data = value;
                break;
            case "DATETIME":
                data = new Timestamp(Long.parseLong(value));
                break;
            case "BOOLEAN":
                data = Boolean.getBoolean(value);
                break;
            case "INTEGER":
            case "NUMERIC":
                data = Long.valueOf(value);
                break;
            default:
                data = value;
        }
        return data;
    }
	private ArgumentCount getArgumentCount(String operator) {
		Operator op = Operator.valueOf(operator);
		switch(op){
		case ASC:
		case DESC:
		case IS_NOT_NULL:
		case DBCOUNT:
		case DBAVERAGE:
		case DATESUB:
		case DATEADD:
		case DBMAX:
		case DBMIN:
		case DBSTDDEV:
		case DBSUM:
		case DBVAR:
		case YEAR:
		case DAY:
		case WEEKDAY:
		case LASTDAY:
		case CONCAT:
		case MONTH:
		case DISTINCT:
		case TODAY:
		case DATEDIFF:
		case MATCH:
		case MOD:
		case INTCAST:
		case G11N_PLURAL:
		case G11N_STATIC:
		case COALESCE:
		case INTERVAL_YR:
		case INTERVAL_MONTH:
		case INTERVAL_DAY:
		case INTERVAL_WEEK:
		case EXCHANGERATE:
		case DATE_FORMAT:
		case LEFT:
		case APPEND_IS_NULL:
		case FIELD:
		case IF:
		case SUBSTRING:
		case IS_NULL: return ArgumentCount.UNARY;
		case LT:
		case GT:
		case GTE:
		case LE:
		case NE:
		case BW:
		case OR:
		case AND:
		case COMMA:
		case AS:
		case B_SUB:
		case AGAINST_IN_BOOLEAN_MODE:
		case DIVIDE:
		case MULTIPLY:
		case ADD:
		case U_SUB:
		case LIKE:
		case EQ: return ArgumentCount.BINARY;
		case IN: return ArgumentCount.N_ARY;
		default: return null;
	}
	}
	private boolean isOperator(Object token) {
		if(token==null)
			return false;
		try{
			Operator op = Operator.valueOf(token.toString());
			if(op!=null)
				return true;
		}catch(IllegalArgumentException e){
			return false;
		}
		return false;
	}
	/*
	 * Function to assign precedence to the operators
	 */
	private int precedence(String opString){
		Operator op = Operator.valueOf(opString);
		switch(op){
		//If it is a function it has highest precedence.
			case DBCOUNT:
			case DBAVERAGE:
			case DBMAX:
			case DBMIN:
			case DBSTDDEV:
			case DBSUM:
			case DBVAR:
			case FIELD:
			case ASC:
			case DESC:
			case YEAR:
			case WEEKDAY:
			case LASTDAY:
			case CONCAT:
			case DAY:
			case MONTH:
			case DISTINCT:
			case TODAY:
			case DATEDIFF:
			case MATCH:
			case AGAINST_IN_BOOLEAN_MODE:
			case DATESUB:
			case DATEADD:
			case MOD:
			case INTCAST:
			case G11N_PLURAL:
			case G11N_STATIC:
			case INTERVAL_YR:
			case INTERVAL_MONTH:
			case INTERVAL_DAY:
			case INTERVAL_WEEK:
			case EXCHANGERATE:
			case DATE_FORMAT:
			case COALESCE:
			case LEFT:
			case APPEND_IS_NULL:
			case IF:
			case SUBSTRING:
				return 0;	
			case INTERVAL:
				return 1;
			case BINARY:
			case COLLATE:
				return 2;
			case U_SUB:
				return 3;
			case BITWISE_XOR:
				return 4;
			case MULTIPLY:
			case DIVIDE:
			case INT_DIV:
			case MODULO_OP:
				return 5;
			case B_SUB:
			case ADD:
				return 6;
			case LEFT_SHIFT:
			case RIGHT_SHIFT:
				return 7;
			case BITWISE_AND:
				return 8;
			case BITWISE_OR:
				return 9;
			case EQ:
			case NULL_SAFE_EQ:
			case GTE:
			case GT:
			case LE:
			case LT:
			case NE:
			case LIKE:
			case IN:
			case IS_NOT_NULL:
			case IS_NULL:
				return 10;
			case BW:
				return 14;
//			case NOT:
//				return 12;
			case AND:
				return 13;
			case XOR:
				return 14;
			case OR:
				return 15;
			case ASSIGNMENT:
			case AS:
				return 16;
			case COMMA:
				return 17;
			case OPEN_PARENTHESIS:
			case CLOSE_PARENTHESIS:
				return 18;
			default:
				return 20;
		}
	}
}