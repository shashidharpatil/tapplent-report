package com.tapplent.platformutility.data.insertEntity;

public class InsertEntityVersion {
	private final String column;
	private final String doaName;
	private final String value;

	public InsertEntityVersion(String column, String doaName, String value) {
		this.column = column;
		this.doaName = doaName;
		this.value = value;
	}

	public String getColumn() {
		return column;
	}

	public String getValue() {
		return value;
	}

	public String getDoaName() {
		return doaName;
	}
}
