package com.tapplent.platformutility.search.impl;


import java.util.*;

import com.tapplent.platformutility.metadata.structure.AttributePathDetails;
import com.tapplent.platformutility.metadata.structure.DependencyKeysHelper;
import com.tapplent.platformutility.search.api.SearchData;
import com.tapplent.platformutility.search.builder.DbSearchTemplate;
import com.tapplent.platformutility.search.builder.SearchField;
import com.tapplent.platformutility.search.builder.SortData;
import com.tapplent.platformutility.search.builder.expression.ExpressionBuilder;
import com.tapplent.platformutility.search.builder.joins.JoinExpressionBuilder;
/**
 * This class contains all the fields and methods required to make SELECT statement.
 * @author Shubham Patodi
 * @since 30/06/2016
 *
 */
public class DefaultSearchData implements SearchData{
//	private String baseTemplateId;
	private String mtPEAlias;
	private String mtPE;
	private String btPE;
	private Map<String, DbSearchTemplate> tables;
	private final Map<String, Integer> tableUsage = new HashMap<>();
	private final Map<String, String> tableIdAndAliasMap = new HashMap<>();
    private final Map<String, String> tableIdAndNameMap = new HashMap<>();
    private Map<String, SearchField> columnAliasToColumnMap = new HashMap<>();
    private Map<String, SearchField> columnLabelToColumnMap = new HashMap<>();
    private SearchField functionPrimaryKey;
    private SearchField databasePrimaryKey;
	private SearchField recordStateColumn;
    private int selectClauseCounter;
    private boolean isInternalDistinct;
    private boolean isExternalDistinct;
    private boolean isNoSelectToBeMade;
    private boolean isResolveG11n;
	private ExpressionBuilder selectClause = new ExpressionBuilder();
	private JoinExpressionBuilder joinClause = new JoinExpressionBuilder();
	private ExpressionBuilder whereClause = new ExpressionBuilder();
	private ExpressionBuilder groupByClause = new ExpressionBuilder();
	private ExpressionBuilder havingClause = new ExpressionBuilder();
	private ExpressionBuilder orderByClause = new ExpressionBuilder();
	private Deque<Object> parametersQue;
	private Deque<Object> countParameterQue;
	private Integer page;
    private Integer pageSize;
    private boolean isApplyPagination;
    private int defaultPaginationSize;
    private boolean returnOnlySqlQuery;
    private AttributePathDetails aggregateAttributePathDetails;
    private List<DefaultSearchData> aggregateStmts;
    private boolean isFunctionStmt;
	private List<DependencyKeysHelper> dependencyKeyList = new ArrayList<>();
	private DomainObjectTemplate domainObjectTemplate;
    private Map<String, Object> dependencyKeys;
    private Map<String, List<DomainObjectFilter>> externalFilters;
    private String filterExpression;
    private List<SortData> stickyHdrSortData;
    private Map<String, Object> dependencyMappedParentData;
	private Set<String> attributePaths;
	private boolean logActivity;
	private String logActivityMTPE;
	private String logActivityDtlMTPE;
	private String processTypeCode;
	private String subjectUserPath;
    public DefaultSearchData() {
		this.domainObjectTemplate = new DomainObjectTemplate();
		this.externalFilters = new HashMap<String, List<DomainObjectFilter>>();
		this.aggregateStmts = new ArrayList<>();
		this.tables = new HashMap<>();
		this.attributePaths = new HashSet<>();
	}
	@Override
	public Integer getPageSize() {
		return pageSize;
	}
	@Override
	public void setPageSize(Integer pageSize) {
		this.pageSize=pageSize;
	}
	@Override
	public Integer getPage() {
		return page;
	}
	@Override
	public void setPage(Integer page) {
		this.page=page;
	}
	public SearchField getFunctionPrimaryKey() {
		return functionPrimaryKey;
	}
	public void setFunctionPrimaryKey(SearchField functionPrimaryKey) {
		this.functionPrimaryKey = functionPrimaryKey;
	}
	public SearchField getDatabasePrimaryKey() {
		return databasePrimaryKey;
	}
	public void setDatabasePrimaryKey(SearchField databasePrimaryKey) {
		this.databasePrimaryKey = databasePrimaryKey;
	}
	public Map<String, SearchField> getColumnAliasToColumnMap() {
		return columnAliasToColumnMap;
	}
	public void setColumnAliasToColumnMap(Map<String, SearchField> columnAliasToColumnMap) {
		this.columnAliasToColumnMap = columnAliasToColumnMap;
	}
	public Map<String, SearchField> getColumnLabelToColumnMap() {
		return columnLabelToColumnMap;
	}
	public void setColumnLabelToColumnMap(Map<String, SearchField> columnLabelToColumnMap) {
		this.columnLabelToColumnMap = columnLabelToColumnMap;
	}
	public DomainObjectTemplate getDomainObjectTemplate() {
		return domainObjectTemplate;
	}
	public void setDomainObjectTemplate(DomainObjectTemplate domainObjectTemplate) {
		this.domainObjectTemplate = domainObjectTemplate;
	}
	public Map<String, Object> getDependencyKeys() {
		return dependencyKeys;
	}
	public void setDependencyKeys(Map<String, Object> dependencyKeys) {
		this.dependencyKeys = dependencyKeys;
	}
	public Map<String, List<DomainObjectFilter>> getExternalFilters() {
		return externalFilters;
	}
	public void setExternalFilters(Map<String, List<DomainObjectFilter>> externalFilters) {
		this.externalFilters = externalFilters;
	}
	public List<SortData> getStickyHdrSortData() {
		return stickyHdrSortData;
	}
	public void setStickyHdrSortData(List<SortData> stickyHdrSortData) {
		this.stickyHdrSortData = stickyHdrSortData;
	}
	public String getFilterExpression() {
		return filterExpression;
	}
	public void setFilterExpression(String filterExpression) {
		this.filterExpression = filterExpression;
	}
	public ExpressionBuilder getSelectClause() {
		return selectClause;
	}
	public void setSelectClause(ExpressionBuilder selectClause) {
		this.selectClause = selectClause;
	}
	public JoinExpressionBuilder getJoinClause() {
		return joinClause;
	}
	public void setJoinClause(JoinExpressionBuilder joinClause) {
		this.joinClause = joinClause;
	}
	public ExpressionBuilder getWhereClause() {
		return whereClause;
	}
	public void setWhereClause(ExpressionBuilder whereClause) {
		this.whereClause = whereClause;
	}
	public ExpressionBuilder getGroupByClause() {
		return groupByClause;
	}
	public void setGroupByClause(ExpressionBuilder groupByClause) {
		this.groupByClause = groupByClause;
	}
	public ExpressionBuilder getHavingClause() {
		return havingClause;
	}
	public void setHavingClause(ExpressionBuilder havingClause) {
		this.havingClause = havingClause;
	}
	public ExpressionBuilder getOrderByClause() {
		return orderByClause;
	}
	public void setOrderByClause(ExpressionBuilder orderByClause) {
		this.orderByClause = orderByClause;
	}
	public List<DefaultSearchData> getAggregateStmts() {
		return aggregateStmts;
	}
	public void setAggregateStmts(List<DefaultSearchData> aggregateStmts) {
		this.aggregateStmts = aggregateStmts;
	}
	public boolean getIsFunctionStmt() {
		return isFunctionStmt;
	}
	public void setIsFunctionStmt(boolean b) {
		this.isFunctionStmt = b;
	}
	public Map<String, DbSearchTemplate> getTables() {
		return tables;
	}
	public void setTables(Map<String, DbSearchTemplate> tables) {
		this.tables = tables;
	}
	public int getSelectClauseCounter() {
		return selectClauseCounter;
	}
	public void setSelectClauseCounter(int selectClauseCounter) {
		this.selectClauseCounter = selectClauseCounter;
	}
	public Map<String, Integer> getTableUsage() {
		return tableUsage;
	}
	public Map<String, String> getTableIdAndAliasMap() {
		return tableIdAndAliasMap;
	}
	public boolean isInternalDistinct() {
		return isInternalDistinct;
	}
	public void setInternalDistinct(boolean isDistinct) {
		this.isInternalDistinct = isDistinct;
	}

	public String getMtPEAlias() {
		return mtPEAlias;
	}

	public void setMtPEAlias(String mtPEAlias) {
		this.mtPEAlias = mtPEAlias;
	}

	public boolean isExternalDistinct() {
		return isExternalDistinct;
	}

	public void setExternalDistinct(boolean externalDistinct) {
		isExternalDistinct = externalDistinct;
	}

	public boolean isNoSelectToBeMade() {
		return isNoSelectToBeMade;
	}

	public void setNoSelectToBeMade(boolean noSelectToBeMade) {
		isNoSelectToBeMade = noSelectToBeMade;
	}

	//	public String getBaseTemplateId() {
//		return baseTemplateId;
//	}
//	public void setBaseTemplateId(String baseTemplateId) {
//		this.baseTemplateId = baseTemplateId;
//	}
	public String getMtPE() {
		return mtPE;
	}
	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}
	public String getBtPE() {
		return btPE;
	}
	public void setBtPE(String btPE) {
		this.btPE = btPE;
	}
	public Deque<Object> getParametersQue() {
		return parametersQue;
	}
	public void setParametersQue(Deque<Object> parametersQue) {
		this.parametersQue = parametersQue;
	}
	public Deque<Object> getCountParameterQue() {
		return countParameterQue;
	}
	public void setCountParameterQue(Deque<Object> countParameterQue) {
		this.countParameterQue = countParameterQue;
	}
	public Map<String, String> getTableIdAndNameMap() {
		return tableIdAndNameMap;
	}
	public void increaseSelectCluaseCounter(){
		selectClauseCounter++;
	}
	public AttributePathDetails getAggregateAttributePathDetails() {
		return aggregateAttributePathDetails;
	}
	public void setAggregateAttributePathDetails(AttributePathDetails aggregateAttributePathDetails) {
		this.aggregateAttributePathDetails = aggregateAttributePathDetails;
	}

	public boolean isResolveG11n() {
		return isResolveG11n;
	}

	public void setResolveG11n(boolean resolveG11n) {
		isResolveG11n = resolveG11n;
	}

	public Set<String> getAttributePaths() {
		return attributePaths;
	}

	public void setAttributePaths(Set<String> attributePaths) {
		this.attributePaths = attributePaths;
	}

	public List<DependencyKeysHelper> getDependencyKeyList() {
		return dependencyKeyList;
	}

	public void setDependencyKeyList(List<DependencyKeysHelper> dependencyKeyList) {
		this.dependencyKeyList = dependencyKeyList;
	}

	public boolean isApplyPagination() {
		return isApplyPagination;
	}

	public void setApplyPagination(boolean applyPagination) {
		isApplyPagination = applyPagination;
	}

	public boolean isLogActivity() {
		return logActivity;
	}

	public void setLogActivity(boolean logActivity) {
		this.logActivity = logActivity;
	}

	public String getLogActivityMTPE() {
		return logActivityMTPE;
	}

	public void setLogActivityMTPE(String logActivityMTPE) {
		this.logActivityMTPE = logActivityMTPE;
	}

	public String getLogActivityDtlMTPE() {
		return logActivityDtlMTPE;
	}

	public void setLogActivityDtlMTPE(String logActivityDtlMTPE) {
		this.logActivityDtlMTPE = logActivityDtlMTPE;
	}

	public SearchField getRecordStateColumn() {
		return recordStateColumn;
	}

	public void setRecordStateColumn(SearchField recordStateColumn) {
		this.recordStateColumn = recordStateColumn;
	}

	public boolean isReturnOnlySqlQuery() {
		return returnOnlySqlQuery;
	}

	public void setReturnOnlySqlQuery(boolean returnOnlySqlQuery) {
		this.returnOnlySqlQuery = returnOnlySqlQuery;
	}

	public int getDefaultPaginationSize() {
		return defaultPaginationSize;
	}

	public void setDefaultPaginationSize(int defaultPaginationSize) {
		this.defaultPaginationSize = defaultPaginationSize;
	}

	public Map<String, Object> getDependencyMappedParentData() {
		return dependencyMappedParentData;
	}
	public void setDependencyMappedParentData(Map<String, Object> dependencyMappedParentData) {
		this.dependencyMappedParentData = dependencyMappedParentData;
	}

	public String getProcessTypeCode() {
		return processTypeCode;
	}

	public String getSubjectUserPath() {
		return subjectUserPath;
	}

	public void setSubjectUserPath(String subjectUserPath) {
		this.subjectUserPath = subjectUserPath;
	}

	public void setProcessTypeCode(String processTypeCode) {
		this.processTypeCode = processTypeCode;
	}

	public String getTableAlias(String id, String tableName) {
        String alias;
        Integer suffix = Optional.ofNullable(tableUsage.get(tableName)).orElse(0);
        alias = tableName + (++suffix);
        tableUsage.put(tableName, suffix);
        tableIdAndAliasMap.put(id, alias);
        tableIdAndNameMap.put(id, tableName);
        return alias;
    }
	public void addColumn(String tableId, SearchField column){
		columnLabelToColumnMap.put(column.getColumnLabel(), column);
		columnAliasToColumnMap.put(column.getColumnAlias(), column);
		DbSearchTemplate table = tables.get(tableId);
		table.addColumn(column);
	}

}