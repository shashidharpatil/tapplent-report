package com.tapplent.platformutility.uilayout.valueobject;

import com.tapplent.platformutility.layout.valueObject.IconVO;

/**
 * Created by tapplent on 01/10/17.
 */
public class RatingScaleVO {
    private String ratingScalePkID;
    private String ratingName;
    private double minRatingLevel;
    private double maxRatingLevel;
    private double intervalRatingLevel;
    private String ratingScaleIcon;

    public String getRatingScalePkID() {
        return ratingScalePkID;
    }

    public void setRatingScalePkID(String ratingScalePkID) {
        this.ratingScalePkID = ratingScalePkID;
    }

    public String getRatingName() {
        return ratingName;
    }

    public void setRatingName(String ratingName) {
        this.ratingName = ratingName;
    }

    public double getMinRatingLevel() {
        return minRatingLevel;
    }

    public void setMinRatingLevel(double minRatingLevel) {
        this.minRatingLevel = minRatingLevel;
    }

    public double getMaxRatingLevel() {
        return maxRatingLevel;
    }

    public void setMaxRatingLevel(double maxRatingLevel) {
        this.maxRatingLevel = maxRatingLevel;
    }

    public double getIntervalRatingLevel() {
        return intervalRatingLevel;
    }

    public void setIntervalRatingLevel(double intervalRatingLevel) {
        this.intervalRatingLevel = intervalRatingLevel;
    }

    public String getRatingScaleIcon() {
        return ratingScaleIcon;
    }

    public void setRatingScaleIcon(String ratingScaleIcon) {
        this.ratingScaleIcon = ratingScaleIcon;
    }
}
