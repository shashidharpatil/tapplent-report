package com.tapplent.platformutility.accessmanager.dao;

/**
 * Created by tapplent on 02/11/16.
 */
public class ConditionValueObject {
    private String resolvedRowPkId;
    private String whoPersonId;
    private String domainObjectId;
    private String dbPrimaryKeyId;
    private String functionalPrimaryKeyid;
    private String subjectUserId;
    private boolean isCreateCurrentPermittedRow;
    private boolean isCreateHistoryPermittedRow;
    private boolean isCreateFuturePermittedRow;
    private boolean isReadCurrentPermittedRow;
    private boolean isReadHistoryPermittedRow;
    private boolean isReadFuturePermittedRow;
    private boolean isUpdateCurrentPermittedRow;
    private boolean isUpdateHistoryPermittedRow;
    private boolean isUpdateFuturePermittedRow;
    private boolean isDeleteCurrentPermittedRow;
    private boolean isDeleteHistoryPermittedRow;
    private boolean isDeleteFuturePermittedRow;

    private String resolvedColumnPkId;
    private String resolvedRowFkId;
    private String domainObjectAttributeId;
    private boolean isCreateCurrentPermittedColumn;
    private boolean isCreateHistoryPermittedColumn;
    private boolean isCreateFuturePermittedColumn;
    private boolean isReadCurrentPermittedColumn;
    private boolean isReadHistoryPermittedColumn;
    private boolean isReadFuturePermittedColumn;
    private boolean isUpdateCurrentPermittedColumn;
    private boolean isUpdateHistoryPermittedColumn;
    private boolean isUpdateFuturePermittedColumn;
    private boolean isDeleteCurrentPermittedColumn;
    private boolean isDeleteHistoryPermittedColumn;
    private boolean isDeleteFuturePermittedColumn;

    public String getResolvedRowPkId() {
        return resolvedRowPkId;
    }

    public void setResolvedRowPkId(String resolvedRowPkId) {
        this.resolvedRowPkId = resolvedRowPkId;
    }

    public String getWhoPersonId() {
        return whoPersonId;
    }

    public void setWhoPersonId(String whoPersonId) {
        this.whoPersonId = whoPersonId;
    }

    public String getDomainObjectId() {
        return domainObjectId;
    }

    public void setDomainObjectId(String domainObjectId) {
        this.domainObjectId = domainObjectId;
    }

    public String getDbPrimaryKeyId() {
        return dbPrimaryKeyId;
    }

    public void setDbPrimaryKeyId(String dbPrimaryKeyId) {
        this.dbPrimaryKeyId = dbPrimaryKeyId;
    }

    public String getFunctionalPrimaryKeyid() {
        return functionalPrimaryKeyid;
    }

    public void setFunctionalPrimaryKeyid(String functionalPrimaryKeyid) {
        this.functionalPrimaryKeyid = functionalPrimaryKeyid;
    }

    public String getSubjectUserId() {
        return subjectUserId;
    }

    public void setSubjectUserId(String subjectUserId) {
        this.subjectUserId = subjectUserId;
    }

    public boolean isCreateCurrentPermittedRow() {
        return isCreateCurrentPermittedRow;
    }

    public void setCreateCurrentPermittedRow(boolean createCurrentPermittedRow) {
        isCreateCurrentPermittedRow = createCurrentPermittedRow;
    }

    public boolean isCreateHistoryPermittedRow() {
        return isCreateHistoryPermittedRow;
    }

    public void setCreateHistoryPermittedRow(boolean createHistoryPermittedRow) {
        isCreateHistoryPermittedRow = createHistoryPermittedRow;
    }

    public boolean isCreateFuturePermittedRow() {
        return isCreateFuturePermittedRow;
    }

    public void setCreateFuturePermittedRow(boolean createFuturePermittedRow) {
        isCreateFuturePermittedRow = createFuturePermittedRow;
    }

    public boolean isReadCurrentPermittedRow() {
        return isReadCurrentPermittedRow;
    }

    public void setReadCurrentPermittedRow(boolean readCurrentPermittedRow) {
        isReadCurrentPermittedRow = readCurrentPermittedRow;
    }

    public boolean isReadHistoryPermittedRow() {
        return isReadHistoryPermittedRow;
    }

    public void setReadHistoryPermittedRow(boolean readHistoryPermittedRow) {
        isReadHistoryPermittedRow = readHistoryPermittedRow;
    }

    public boolean isReadFuturePermittedRow() {
        return isReadFuturePermittedRow;
    }

    public void setReadFuturePermittedRow(boolean readFuturePermittedRow) {
        isReadFuturePermittedRow = readFuturePermittedRow;
    }

    public boolean isUpdateCurrentPermittedRow() {
        return isUpdateCurrentPermittedRow;
    }

    public void setUpdateCurrentPermittedRow(boolean updateCurrentPermittedRow) {
        isUpdateCurrentPermittedRow = updateCurrentPermittedRow;
    }

    public boolean isUpdateHistoryPermittedRow() {
        return isUpdateHistoryPermittedRow;
    }

    public void setUpdateHistoryPermittedRow(boolean updateHistoryPermittedRow) {
        isUpdateHistoryPermittedRow = updateHistoryPermittedRow;
    }

    public boolean isUpdateFuturePermittedRow() {
        return isUpdateFuturePermittedRow;
    }

    public void setUpdateFuturePermittedRow(boolean updateFuturePermittedRow) {
        isUpdateFuturePermittedRow = updateFuturePermittedRow;
    }

    public boolean isDeleteCurrentPermittedRow() {
        return isDeleteCurrentPermittedRow;
    }

    public void setDeleteCurrentPermittedRow(boolean deleteCurrentPermittedRow) {
        isDeleteCurrentPermittedRow = deleteCurrentPermittedRow;
    }

    public boolean isDeleteHistoryPermittedRow() {
        return isDeleteHistoryPermittedRow;
    }

    public void setDeleteHistoryPermittedRow(boolean deleteHistoryPermittedRow) {
        isDeleteHistoryPermittedRow = deleteHistoryPermittedRow;
    }

    public boolean isDeleteFuturePermittedRow() {
        return isDeleteFuturePermittedRow;
    }

    public void setDeleteFuturePermittedRow(boolean deleteFuturePermittedRow) {
        isDeleteFuturePermittedRow = deleteFuturePermittedRow;
    }

    public String getResolvedColumnPkId() {
        return resolvedColumnPkId;
    }

    public void setResolvedColumnPkId(String resolvedColumnPkId) {
        this.resolvedColumnPkId = resolvedColumnPkId;
    }

    public String getResolvedRowFkId() {
        return resolvedRowFkId;
    }

    public void setResolvedRowFkId(String resolvedRowFkId) {
        this.resolvedRowFkId = resolvedRowFkId;
    }

    public String getDomainObjectAttributeId() {
        return domainObjectAttributeId;
    }

    public void setDomainObjectAttributeId(String domainObjectAttributeId) {
        this.domainObjectAttributeId = domainObjectAttributeId;
    }

    public boolean isCreateCurrentPermittedColumn() {
        return isCreateCurrentPermittedColumn;
    }

    public void setCreateCurrentPermittedColumn(boolean createCurrentPermittedColumn) {
        isCreateCurrentPermittedColumn = createCurrentPermittedColumn;
    }

    public boolean isCreateHistoryPermittedColumn() {
        return isCreateHistoryPermittedColumn;
    }

    public void setCreateHistoryPermittedColumn(boolean createHistoryPermittedColumn) {
        isCreateHistoryPermittedColumn = createHistoryPermittedColumn;
    }

    public boolean isCreateFuturePermittedColumn() {
        return isCreateFuturePermittedColumn;
    }

    public void setCreateFuturePermittedColumn(boolean createFuturePermittedColumn) {
        isCreateFuturePermittedColumn = createFuturePermittedColumn;
    }

    public boolean isReadCurrentPermittedColumn() {
        return isReadCurrentPermittedColumn;
    }

    public void setReadCurrentPermittedColumn(boolean readCurrentPermittedColumn) {
        isReadCurrentPermittedColumn = readCurrentPermittedColumn;
    }

    public boolean isReadHistoryPermittedColumn() {
        return isReadHistoryPermittedColumn;
    }

    public void setReadHistoryPermittedColumn(boolean readHistoryPermittedColumn) {
        isReadHistoryPermittedColumn = readHistoryPermittedColumn;
    }

    public boolean isReadFuturePermittedColumn() {
        return isReadFuturePermittedColumn;
    }

    public void setReadFuturePermittedColumn(boolean readFuturePermittedColumn) {
        isReadFuturePermittedColumn = readFuturePermittedColumn;
    }

    public boolean isUpdateCurrentPermittedColumn() {
        return isUpdateCurrentPermittedColumn;
    }

    public void setUpdateCurrentPermittedColumn(boolean updateCurrentPermittedColumn) {
        isUpdateCurrentPermittedColumn = updateCurrentPermittedColumn;
    }

    public boolean isUpdateHistoryPermittedColumn() {
        return isUpdateHistoryPermittedColumn;
    }

    public void setUpdateHistoryPermittedColumn(boolean updateHistoryPermittedColumn) {
        isUpdateHistoryPermittedColumn = updateHistoryPermittedColumn;
    }

    public boolean isUpdateFuturePermittedColumn() {
        return isUpdateFuturePermittedColumn;
    }

    public void setUpdateFuturePermittedColumn(boolean updateFuturePermittedColumn) {
        isUpdateFuturePermittedColumn = updateFuturePermittedColumn;
    }

    public boolean isDeleteCurrentPermittedColumn() {
        return isDeleteCurrentPermittedColumn;
    }

    public void setDeleteCurrentPermittedColumn(boolean deleteCurrentPermittedColumn) {
        isDeleteCurrentPermittedColumn = deleteCurrentPermittedColumn;
    }

    public boolean isDeleteHistoryPermittedColumn() {
        return isDeleteHistoryPermittedColumn;
    }

    public void setDeleteHistoryPermittedColumn(boolean deleteHistoryPermittedColumn) {
        isDeleteHistoryPermittedColumn = deleteHistoryPermittedColumn;
    }

    public boolean isDeleteFuturePermittedColumn() {
        return isDeleteFuturePermittedColumn;
    }

    public void setDeleteFuturePermittedColumn(boolean deleteFuturePermittedColumn) {
        isDeleteFuturePermittedColumn = deleteFuturePermittedColumn;
    }
}
