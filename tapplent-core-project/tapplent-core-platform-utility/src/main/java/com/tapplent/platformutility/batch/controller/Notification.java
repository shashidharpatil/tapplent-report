package com.tapplent.platformutility.batch.controller;

import java.util.concurrent.Callable;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.helper.NTFN_NotificationHelper;
import com.tapplent.platformutility.batch.model.SchedulerInstanceModel;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Notification implements Callable {

	SchedulerInstanceModel schedulerInstanceModel;

	public Notification(SchedulerInstanceModel schedulerInstanceModel) {
		this.schedulerInstanceModel = schedulerInstanceModel;
	}

	@Override
	public Object call() throws Exception {
		String[] springConfig = { "jobs/Notification.xml" };
		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
		jobParametersBuilder.addString("googleconfig", NTFN_NotificationHelper.getOrgConfig(Constants.google));
		jobParametersBuilder.addString("facebookconfig", NTFN_NotificationHelper.getOrgConfig(Constants.faceBook));
		jobParametersBuilder.addString("skypeconfig", NTFN_NotificationHelper.getOrgConfig(Constants.skype));
		jobParametersBuilder.addString("telegramconfig", NTFN_NotificationHelper.getOrgConfig(Constants.telegram));
		jobParametersBuilder.addString("schedulerInstancePkId", schedulerInstanceModel.getSchedulerInstancePkId());
		Job job = (Job) context.getBean("Notification");
		try {
			JobExecution execution = jobLauncher.run(job, jobParametersBuilder.toJobParameters());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
