package com.tapplent.platformutility.uilayout.valueobject;

import java.util.List;

/**
 * Created by tapplent on 27/12/16.
 */
public class Filter {
    private String filterPkId;
    private String loggedInPersonFkId;
    private String filterNameG11nBigTxt;
    private String sourceFilterScreenSectionInstanceFkId;
    private List<FilterCriteria> filterCriteriaList;
    public String getFilterPkId() {
        return filterPkId;
    }

    public void setFilterPkId(String filterPkId) {
        this.filterPkId = filterPkId;
    }

    public String getLoggedInPersonFkId() {
        return loggedInPersonFkId;
    }

    public void setLoggedInPersonFkId(String loggedInPersonFkId) {
        this.loggedInPersonFkId = loggedInPersonFkId;
    }

    public String getFilterNameG11nBigTxt() {
        return filterNameG11nBigTxt;
    }

    public void setFilterNameG11nBigTxt(String filterNameG11nBigTxt) {
        this.filterNameG11nBigTxt = filterNameG11nBigTxt;
    }

    public String getSourceFilterScreenSectionInstanceFkId() {
        return sourceFilterScreenSectionInstanceFkId;
    }

    public void setSourceFilterScreenSectionInstanceFkId(String sourceFilterScreenSectionInstanceFkId) {
        this.sourceFilterScreenSectionInstanceFkId = sourceFilterScreenSectionInstanceFkId;
    }

    public List<FilterCriteria> getFilterCriteriaList() {
        return filterCriteriaList;
    }

    public void setFilterCriteriaList(List<FilterCriteria> filterCriteriaList) {
        this.filterCriteriaList = filterCriteriaList;
    }
}
