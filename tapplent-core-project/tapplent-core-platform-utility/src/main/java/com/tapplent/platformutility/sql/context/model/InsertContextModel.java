package com.tapplent.platformutility.sql.context.model;

import java.util.Map;

import com.tapplent.platformutility.data.insertEntity.InsertEntityPK;
import com.tapplent.platformutility.data.insertEntity.InsertEntityVersion;

public class InsertContextModel extends SQLContextModel {
	
	private Map<String, String> orderByCollection;
	private String selectExpression;
	private String whereExpression;
	private Map<String, Object> insertValueMap;
	private Map<String, Object> doaValueMap;
	private InsertEntityPK insertEntityPK;
	private InsertEntityVersion insertEntityVersion;

	public Map<String, String> getOrderByCollection() {
		return orderByCollection;
	}

	public void setOrderByCollection(Map<String, String> orderByCollection) {
		this.orderByCollection = orderByCollection;
	}

	public String getSelectExpression() {
		return selectExpression;
	}

	public void setSelectExpression(String selectExpression) {
		this.selectExpression = selectExpression;
	}

	public String getWhereExpression() {
		return whereExpression;
	}

	public void setWhereExpression(String whereExpression) {
		this.whereExpression = whereExpression;
	}

	public void setInsertValueMap(Map<String, Object> columnNameValueMap) {
		this.insertValueMap=columnNameValueMap;
	}

	public Map<String, Object> getInsertValueMap() {
		return insertValueMap;
	}

	public Map<String, Object> getDoaValueMap() {
		return doaValueMap;
	}

	public void setDoaValueMap(Map<String, Object> doaValueMap) {
		this.doaValueMap = doaValueMap;
	}

	public InsertEntityPK getInsertEntityPK() {
		return insertEntityPK;
	}

	public void setInsertEntityPK(InsertEntityPK insertEntityPK) {
		this.insertEntityPK = insertEntityPK;
	}

	public InsertEntityVersion getInsertEntityVersion() {
		return insertEntityVersion;
	}

	public void setInsertEntityVersion(InsertEntityVersion insertEntityVersion) {
		this.insertEntityVersion = insertEntityVersion;
	}

}
