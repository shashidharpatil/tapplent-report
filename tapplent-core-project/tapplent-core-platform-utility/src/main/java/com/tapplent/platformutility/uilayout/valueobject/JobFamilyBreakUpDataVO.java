package com.tapplent.platformutility.uilayout.valueobject;

public class JobFamilyBreakUpDataVO {
    private String jobFamilyBrkUpId;
    private String personId;
    private String jobFamilyId;
    private String jobFamilyName;
    private String jobFamilyType;
    private String jobFamilyTypeName;

    public String getJobFamilyBrkUpId() {
        return jobFamilyBrkUpId;
    }

    public void setJobFamilyBrkUpId(String jobFamilyBrkUpId) {
        this.jobFamilyBrkUpId = jobFamilyBrkUpId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getJobFamilyId() {
        return jobFamilyId;
    }

    public void setJobFamilyId(String jobFamilyId) {
        this.jobFamilyId = jobFamilyId;
    }

    public String getJobFamilyName() {
        return jobFamilyName;
    }

    public void setJobFamilyName(String jobFamilyName) {
        this.jobFamilyName = jobFamilyName;
    }

    public String getJobFamilyType() {
        return jobFamilyType;
    }

    public void setJobFamilyType(String jobFamilyType) {
        this.jobFamilyType = jobFamilyType;
    }

    public String getJobFamilyTypeName() {
        return jobFamilyTypeName;
    }

    public void setJobFamilyTypeName(String jobFamilyTypeName) {
        this.jobFamilyTypeName = jobFamilyTypeName;
    }
}
