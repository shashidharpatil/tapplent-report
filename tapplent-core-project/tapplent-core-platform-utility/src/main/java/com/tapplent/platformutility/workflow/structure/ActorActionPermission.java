package com.tapplent.platformutility.workflow.structure;

public class ActorActionPermission {
    private String actionPermissionPkId;
    private String actionPermissionDefnId;
    private String mtPE;
    private String actionVisualMasterCode;
    private boolean isActionApplicable;
    private String recordStatePermission;

    public String getActionPermissionPkId() {
        return actionPermissionPkId;
    }

    public void setActionPermissionPkId(String actionPermissionPkId) {
        this.actionPermissionPkId = actionPermissionPkId;
    }

    public String getActionPermissionDefnId() {
        return actionPermissionDefnId;
    }

    public void setActionPermissionDefnId(String actionPermissionDefnId) {
        this.actionPermissionDefnId = actionPermissionDefnId;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getActionVisualMasterCode() {
        return actionVisualMasterCode;
    }

    public void setActionVisualMasterCode(String actionVisualMasterCode) {
        this.actionVisualMasterCode = actionVisualMasterCode;
    }

    public boolean isActionApplicable() {
        return isActionApplicable;
    }

    public void setActionApplicable(boolean actionApplicable) {
        isActionApplicable = actionApplicable;
    }

    public String getRecordStatePermission() {
        return recordStatePermission;
    }

    public void setRecordStatePermission(String recordStatePermission) {
        this.recordStatePermission = recordStatePermission;
    }
}
