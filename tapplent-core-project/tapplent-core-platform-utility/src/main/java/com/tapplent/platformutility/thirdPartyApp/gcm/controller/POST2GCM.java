package com.tapplent.platformutility.thirdPartyApp.gcm.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import com.tapplent.platformutility.thirdPartyApp.gcm.helper.ContentforID;
import com.tapplent.platformutility.thirdPartyApp.gcm.helper.ContentforNotKey;

public class POST2GCM {

	public static NTFN_NotificationData post(String apiKey, ContentforID content, ContentforNotKey content1,
											 NTFN_NotificationData item) {

		try {

			// 1. URL
			URL url = new URL(Constants.gcmSendUrl);

			// 2. Open connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			// 3. Specify POST method
			conn.setRequestMethod("POST");

			// 4. Set the headers
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "key=" + apiKey);

			conn.setDoOutput(true);

			// 5. Add JSON data into POST request body

			// `5.1 Use Jackson object mapper to convert Contnet object into
			// JSON
			ObjectMapper mapper = new ObjectMapper();

			// 5.2 Get connection output stream
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

			// 5.3 Copy Content "JSON" into
			if (content != null)
				mapper.writeValue(wr, content);
			else if (content1 != null)
				mapper.writeValue(wr, content1);
			System.out.println("mapper" + mapper.toString());

			// 5.4 Send the request
			wr.flush();

			// 5.5 close
			wr.close();

			// 6. Get the response
			int responseCode = conn.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			if (responseCode != 200) {
				item.setSuccess(false);
				item.setError(response);
			} else {
				item.setSuccess(true);
			}
			// 7. Print result
			System.out.println(response.toString());

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return item;
	}
}
