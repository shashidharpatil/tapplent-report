package com.tapplent.platformutility.batch.helper;
//import com.tapplent.platformutility.batch.valueobject.DG_DocGenData;
//import org.apache.commons.io.FileUtils;
//import org.apache.log4j.BasicConfigurator;
//import org.docx4j.convert.in.xhtml.XHTMLImporterImpl;
//import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
//import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart;
//import org.w3c.tidy.Tidy;
//import org.xhtmlrenderer.pdf.ITextRenderer;
//import org.xhtmlrenderer.pdf.PDFEncryption;
//
//import javax.imageio.ImageIO;
//import java.awt.*;
//import java.awt.image.BufferedImage;
//import java.io.*;
//import java.net.URL;
//import java.nio.ByteBuffer;
//import java.nio.channels.FileChannel;

import java.io.File;

import com.tapplent.platformutility.batch.model.DG_DocGenData;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.BasicConfigurator;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.pdf.PDFEncryption;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;



public class DG_ConvertURLtoDocType {
//
    public static String generateWord(DG_DocGenData item) {

////
////        try {
////            DG_DocGenHelper dg_docGenHelper = new DG_DocGenHelper();
////            String urlStr = dg_docGenHelper.fetchURL(item);
////            BasicConfigurator.configure();
////            // Must tidy first :-(
////            URL url = new URL(urlStr);
////
////            ByteArrayOutputStream baos = new ByteArrayOutputStream();
////        /* Transforms to well-formed XHTML */
////            Tidy t = new Tidy();
////            t.setXHTML(true);
////            t.setQuiet(true);
////            t.setShowWarnings(false);
////            t.parse(url.openStream(), baos);
////            File temphtml = File.createTempFile("temp-file-html", ".html");
////            OutputStream outputStream = new FileOutputStream(temphtml);
////            baos.writeTo(outputStream);
////
////            String stringFromFile = FileUtils.readFileToString(temphtml,
////                    "UTF-8");
////
////            String unescaped = stringFromFile.replace("&", "&amp;");
////
////
////            // Create an empty docx package
////            WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
////
////            NumberingDefinitionsPart ndp = new NumberingDefinitionsPart();
////            wordMLPackage.getMainDocumentPart().addTargetPart(ndp);
////            ndp.unmarshalDefaultNumbering();
////
////            // Convert the XHTML, and add it into the empty docx we made
////            XHTMLImporterImpl XHTMLImporter = new XHTMLImporterImpl(wordMLPackage);
////            XHTMLImporter.setHyperlinkStyle("Hyperlink");
////            wordMLPackage.getMainDocumentPart().getContent().addAll(XHTMLImporter.convert(unescaped, "hi"));
////            File temp = File.createTempFile("temp-file-docx", ".docx");
////            wordMLPackage.save(temp);
////            String id = S3Helper.insertIntoS3(new FileInputStream(temp), item.getFileNameLngTxt());
////            temp.delete();
////            temphtml.delete();
////            return id;
////
////        } catch (Exception e) {
////            e.printStackTrace();
////            item.setSuccess(false);
////            item.setError(e);
////        }


//        try {
//            DG_DocGenHelper dg_docGenHelper = new DG_DocGenHelper();
//            String urlStr = dg_docGenHelper.fetchURL(item);
//            BasicConfigurator.configure();
//            // Must tidy first :-(
//            URL url = new URL(urlStr);
//
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        /* Transforms to well-formed XHTML */
//            Tidy t = new Tidy();
//            t.setXHTML(true);
//            t.setQuiet(true);
//            t.setShowWarnings(false);
//            t.parse(url.openStream(), baos);
//            File temphtml = File.createTempFile("temp-file-html", ".html");
//            OutputStream outputStream = new FileOutputStream(temphtml);
//            baos.writeTo(outputStream);
//
//            String stringFromFile = FileUtils.readFileToString(temphtml,
//                    "UTF-8");
//
//            String unescaped = stringFromFile.replace("&", "&amp;");
//
//
//            // Create an empty docx package
//            WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
//
//            NumberingDefinitionsPart ndp = new NumberingDefinitionsPart();
//            wordMLPackage.getMainDocumentPart().addTargetPart(ndp);
//            ndp.unmarshalDefaultNumbering();
//
//            // Convert the XHTML, and add it into the empty docx we made
//            XHTMLImporterImpl XHTMLImporter = new XHTMLImporterImpl(wordMLPackage);
//            XHTMLImporter.setHyperlinkStyle("Hyperlink");
//            wordMLPackage.getMainDocumentPart().getContent().addAll(XHTMLImporter.convert(unescaped, "hi"));
//            File temp = File.createTempFile("temp-file-docx", ".docx");
//            wordMLPackage.save(temp);
//            String id = S3Helper.insertIntoS3(new FileInputStream(temp), item.getFileNameLngTxt());
//            temp.delete();
//            temphtml.delete();
//            return id;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            item.setSuccess(false);
//            item.setError(e);
//        }

        return null;
//
//
    }
//
//
    public static String generateJPG(DG_DocGenData item) {

////        DG_DocGenHelper dg_docGenHelper = new DG_DocGenHelper();
////        String urlStr = dg_docGenHelper.fetchURL(item);
////        try {
////            BasicConfigurator.configure();
////            // Must tidy first :-(
////            URL url = new URL(urlStr);
////            ByteArrayOutputStream baos = new ByteArrayOutputStream();
////	    		/* Transforms to well-formed XHTML */
////            Tidy t = new Tidy();
////            t.setXHTML(true);
////            t.setQuiet(true);
////            t.setShowWarnings(false);
////            t.parse(url.openStream(), baos);
////            File temphtml = File.createTempFile("temp-file-html", ".html");
////            OutputStream outputStream = new FileOutputStream(temphtml);
////            baos.writeTo(outputStream);
////            final ITextRenderer iTextRenderer = new ITextRenderer();
////
////            iTextRenderer.setDocument(temphtml);
////            iTextRenderer.layout();
////            File pdf1 = File.createTempFile("temp-file-pdf", ".pdf");
////            final FileOutputStream fileOutputStream = new FileOutputStream(pdf1);
////            iTextRenderer.createPDF(fileOutputStream);
////            fileOutputStream.close();
////            String sourceDir = pdf1.getAbsolutePath();
////            File sourceFile = new File(sourceDir);
////            File jpg = File.createTempFile("temp-file-jpg", ".jpg");
////            if (sourceFile.exists()) {
////                RandomAccessFile raf = new RandomAccessFile(sourceFile, "r");
////                FileChannel channel = raf.getChannel();
////                ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
////                PDFFile pdf = new PDFFile(buf);
////                int pageNumber = 1;
////                for (int i = 0; i < pdf.getNumPages(); i++) {
////                    PDFPage page = pdf.getPage(i);
////                    // create the image
////                    Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(), (int) page.getBBox().getHeight());
////                    BufferedImage bufferedImage = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);
////                    // image width, // image height, // clip rect, // null for the ImageObserver, // fill background with white, // block until drawing is done
////                    Image image = page.getImage(rect.width, rect.height, rect, null, true, true);
////                    Graphics2D bufImageGraphics = bufferedImage.createGraphics();
////                    bufImageGraphics.drawImage(image, 0, 0, null);
////
////                    ImageIO.write(bufferedImage, "jpg", jpg);
////                    pageNumber++;
////                }
////                String id =S3Helper.insertIntoS3(new FileInputStream(jpg), item.getFileNameLngTxt());
////                temphtml.delete();
////                pdf1.delete();
////                jpg.delete();
////                return id;
////            } else {
////                System.err.println(sourceFile.getName() + " File not exists");
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////            item.setSuccess(false);
////            item.setError(e);
////        }
////
////
////

//        DG_DocGenHelper dg_docGenHelper = new DG_DocGenHelper();
//        String urlStr = dg_docGenHelper.fetchURL(item);
//        try {
//            BasicConfigurator.configure();
//            // Must tidy first :-(
//            URL url = new URL(urlStr);
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//	    		/* Transforms to well-formed XHTML */
//            Tidy t = new Tidy();
//            t.setXHTML(true);
//            t.setQuiet(true);
//            t.setShowWarnings(false);
//            t.parse(url.openStream(), baos);
//            File temphtml = File.createTempFile("temp-file-html", ".html");
//            OutputStream outputStream = new FileOutputStream(temphtml);
//            baos.writeTo(outputStream);
//            final ITextRenderer iTextRenderer = new ITextRenderer();
//
//            iTextRenderer.setDocument(temphtml);
//            iTextRenderer.layout();
//            File pdf1 = File.createTempFile("temp-file-pdf", ".pdf");
//            final FileOutputStream fileOutputStream = new FileOutputStream(pdf1);
//            iTextRenderer.createPDF(fileOutputStream);
//            fileOutputStream.close();
//            String sourceDir = pdf1.getAbsolutePath();
//            File sourceFile = new File(sourceDir);
//            File jpg = File.createTempFile("temp-file-jpg", ".jpg");
//            if (sourceFile.exists()) {
//                RandomAccessFile raf = new RandomAccessFile(sourceFile, "r");
//                FileChannel channel = raf.getChannel();
//                ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
//                PDFFile pdf = new PDFFile(buf);
//                int pageNumber = 1;
//                for (int i = 0; i < pdf.getNumPages(); i++) {
//                    PDFPage page = pdf.getPage(i);
//                    // create the image
//                    Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(), (int) page.getBBox().getHeight());
//                    BufferedImage bufferedImage = new BufferedImage(rect.width, rect.height, BufferedImage.TYPE_INT_RGB);
//                    // image width, // image height, // clip rect, // null for the ImageObserver, // fill background with white, // block until drawing is done
//                    Image image = page.getImage(rect.width, rect.height, rect, null, true, true);
//                    Graphics2D bufImageGraphics = bufferedImage.createGraphics();
//                    bufImageGraphics.drawImage(image, 0, 0, null);
//
//                    ImageIO.write(bufferedImage, "jpg", jpg);
//                    pageNumber++;
//                }
//                String id =S3Helper.insertIntoS3(new FileInputStream(jpg), item.getFileNameLngTxt());
//                temphtml.delete();
//                pdf1.delete();
//                jpg.delete();
//                return id;
//            } else {
//                System.err.println(sourceFile.getName() + " File not exists");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            item.setSuccess(false);
//            item.setError(e);
//        }
//
//
//

        return null;
    }
//
    public static String generatePDF(DG_DocGenData item, byte[] pass) {

////        try {
////            DG_DocGenHelper dg_docGenHelper = new DG_DocGenHelper();
////            String urlStr = dg_docGenHelper.fetchURL(item);
////            BasicConfigurator.configure();
////            // Must tidy first :-(
////            URL url = new URL(urlStr);
////
////            ByteArrayOutputStream baos = new ByteArrayOutputStream();
////			/* Transforms to well-formed XHTML */
////            Tidy t = new Tidy();
////            t.setXHTML(true);
////            t.setQuiet(true);
////            t.setShowWarnings(false);
////            t.parse(url.openStream(), baos);
////            File temphtml = File.createTempFile("temp-file-html", ".html");
////            OutputStream outputStream = new FileOutputStream(temphtml);
////            baos.writeTo(outputStream);
////
////            File pdf = create(temphtml, pass);
////            String id = S3Helper.insertIntoS3(new FileInputStream(pdf), item.getFileNameLngTxt());
////            pdf.delete();
////            item.setSuccess(true);
////            return id;
////        } catch (Exception e) {
////            item.setSuccess(false);
////            item.setError(e);
////        }

//        try {
//            DG_DocGenHelper dg_docGenHelper = new DG_DocGenHelper();
//            String urlStr = dg_docGenHelper.fetchURL(item);
//            BasicConfigurator.configure();
//            // Must tidy first :-(
//            URL url = new URL(urlStr);
//
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			/* Transforms to well-formed XHTML */
//            Tidy t = new Tidy();
//            t.setXHTML(true);
//            t.setQuiet(true);
//            t.setShowWarnings(false);
//            t.parse(url.openStream(), baos);
//            File temphtml = File.createTempFile("temp-file-html", ".html");
//            OutputStream outputStream = new FileOutputStream(temphtml);
//            baos.writeTo(outputStream);
//
//            File pdf = create(temphtml, pass);
//            String id = S3Helper.insertIntoS3(new FileInputStream(pdf), item.getFileNameLngTxt());
//            pdf.delete();
//            item.setSuccess(true);
//            return id;
//        } catch (Exception e) {
//            item.setSuccess(false);
//            item.setError(e);
//        }

        return null;
//
    }
//
    public static File create(File html, byte[] pass) {

////        try {
////
////            /**
////             * Creating an instance of iText renderer which will be used to
////             * generate the pdf from the html document.
////             */
////            final ITextRenderer iTextRenderer = new ITextRenderer();
////
////            /**
////             * Setting the document as the url value passed. This means that the
////             * iText renderer has to parse this html document to generate the
////             * pdf.
////             */
////            iTextRenderer.setDocument(html);
////            iTextRenderer.layout();
////
////            //setting password
////            if (pass != null) {
////                PDFEncryption pdfEn = new PDFEncryption();
////                pdfEn.setUserPassword(pass);
////                iTextRenderer.setPDFEncryption(pdfEn);
////            }
////
////            /**
////             * The generated pdf will be written to the invoice.pdf file.
////             */
////            File pdf = File.createTempFile("temp-file-pdf", ".pdf");
////            final FileOutputStream fileOutputStream = new FileOutputStream(
////                    pdf);
////
////            /**
////             * Creating the pdf and writing it in invoice.pdf file.
////             */
////            iTextRenderer.createPDF(fileOutputStream);
////            fileOutputStream.close();
////           return pdf;
////
////        } catch (final DocumentException e) {
////            e.printStackTrace();
////        } catch (FileNotFoundException e) {
////            e.printStackTrace();
////        } catch (IOException e) {
////            e.printStackTrace();
////        }

//        try {
//
//            /**
//             * Creating an instance of iText renderer which will be used to
//             * generate the pdf from the html document.
//             */
//            final ITextRenderer iTextRenderer = new ITextRenderer();
//
//            /**
//             * Setting the document as the url value passed. This means that the
//             * iText renderer has to parse this html document to generate the
//             * pdf.
//             */
//            iTextRenderer.setDocument(html);
//            iTextRenderer.layout();
//
//            //setting password
//            if (pass != null) {
//                PDFEncryption pdfEn = new PDFEncryption();
//                pdfEn.setUserPassword(pass);
//                iTextRenderer.setPDFEncryption(pdfEn);
//            }
//
//            /**
//             * The generated pdf will be written to the invoice.pdf file.
//             */
//            File pdf = File.createTempFile("temp-file-pdf", ".pdf");
//            final FileOutputStream fileOutputStream = new FileOutputStream(
//                    pdf);
//
//            /**
//             * Creating the pdf and writing it in invoice.pdf file.
//             */
//            iTextRenderer.createPDF(fileOutputStream);
//            fileOutputStream.close();
//           return pdf;
//
//        } catch (final DocumentException e) {
//            e.printStackTrace();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        return null;
    }


}
