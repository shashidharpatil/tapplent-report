package com.tapplent.platformutility.uilayout.valueobject;

import com.tapplent.platformutility.layout.valueObject.IconVO;

/**
 * Created by tapplent on 06/07/17.
 */
public class OrgChartFilterLabelVO {
    private String orgChartFilterPKID;
    private String filterSectionID;
    private String labelName;
    private String labelIcon;
    private String controlType;
    private String controlTypeOnStateIcon;
    private String controlTypeOffStateIcon;
    private String baseTemplate;
    private String labelDOA;
    private boolean isFarLabel;


    public String getOrgChartFilterPKID() {
        return orgChartFilterPKID;
    }

    public void setOrgChartFilterPKID(String orgChartFilterPKID) {
        this.orgChartFilterPKID = orgChartFilterPKID;
    }

    public String getFilterSectionID() {
        return filterSectionID;
    }

    public void setFilterSectionID(String filterSectionID) {
        this.filterSectionID = filterSectionID;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getLabelIcon() {
        return labelIcon;
    }

    public void setLabelIcon(String labelIcon) {
        this.labelIcon = labelIcon;
    }

    public String getControlType() {
        return controlType;
    }

    public void setControlType(String controlType) {
        this.controlType = controlType;
    }

    public String getControlTypeOnStateIcon() {
        return controlTypeOnStateIcon;
    }

    public void setControlTypeOnStateIcon(String controlTypeOnStateIcon) {
        this.controlTypeOnStateIcon = controlTypeOnStateIcon;
    }

    public String getControlTypeOffStateIcon() {
        return controlTypeOffStateIcon;
    }

    public void setControlTypeOffStateIcon(String controlTypeOffStateIcon) {
        this.controlTypeOffStateIcon = controlTypeOffStateIcon;
    }

    public String getBaseTemplate() {
        return baseTemplate;
    }

    public void setBaseTemplate(String baseTemplate) {
        this.baseTemplate = baseTemplate;
    }

    public String getLabelDOA() {
        return labelDOA;
    }

    public void setLabelDOA(String labelDOA) {
        this.labelDOA = labelDOA;
    }

    public boolean isFarLabel() {
        return isFarLabel;
    }

    public void setFarLabel(boolean farLabel) {
        isFarLabel = farLabel;
    }
}
