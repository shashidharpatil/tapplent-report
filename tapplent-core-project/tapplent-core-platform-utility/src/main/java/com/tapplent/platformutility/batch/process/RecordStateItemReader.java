package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.RCRD_STATE_MtDOData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class RecordStateItemReader extends JdbcPagingItemReader<RCRD_STATE_MtDOData> {

    public RecordStateItemReader() {
        super();
        DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
        setDataSource(ds);
        setPageSize(20);
        setQueryProvider(queryProvider(ds));
        setRowMapper(rowMapper());
    }

    private RowMapper<RCRD_STATE_MtDOData> rowMapper() {
        return new RecordStateRowMapper();
    }

    private PagingQueryProvider queryProvider(DataSource ds) {
        MySqlPagingQueryProvider provider = new MySqlPagingQueryProvider();
        /*provider.setSelectClause("T_MT_ADM_DOMAIN_OBJECT.VERSION_ID, " +
                "T_MT_ADM_DOMAIN_OBJECT.DOMAIN_OBJECT_CODE_PK_ID, " +
                "T_MT_ADM_DOMAIN_OBJECT.DB_TABLE_NAME_TXT, " +
                "T_MT_ADM_DOMAIN_OBJECT.DO_TYPE_CODE_FK_ID, " +
                "T_MT_ADM_DOMAIN_OBJECT.IS_CUSTOMIZABLE, " +
                "T_MT_ADM_DOMAIN_OBJECT.EFFECTIVE_DATETIME," +
                "T_MT_ADM_DO_ATTRIBUTE.DB_COLUMN_NAME_TXT ");
        provider.setFromClause("FROM"
                + " T_MT_ADM_DOMAIN_OBJECT INNER JOIN T_MT_ADM_DO_ATTRIBUTE  ON " +
                "T_MT_ADM_DOMAIN_OBJECT.DOMAIN_OBJECT_CODE_PK_ID =\n" +
                "                T_MT_ADM_DO_ATTRIBUTE.DOMAIN_OBJECT_CODE_FK_ID\n" +
                "        INNER JOIN T_BT_ADM_DO_SPEC  ON " +
                "T_MT_ADM_DOMAIN_OBJECT.DOMAIN_OBJECT_CODE_PK_ID = T_BT_ADM_DO_SPEC.DOMAIN_OBJECT_CODE_FK_ID");
        provider.setWhereClause("T_MT_ADM_DOMAIN_OBJECT.SAVE_STATE_CODE_FK_ID = 'SAVED' AND T_MT_ADM_DOMAIN_OBJECT.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' " +
                "AND T_MT_ADM_DOMAIN_OBJECT.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND T_MT_ADM_DOMAIN_OBJECT.IS_DELETED = FALSE AND IS_UPDATE_INLINE = FALSE AND " +
                "IS_FUNCTIONAL_PRIMARY_KEY ");*/

        provider.setSelectClause("T_SCH_IEU_SCHEDULER_INSTANCE.SCHEDULER_INSTANCE_PK_ID");
        provider.setFromClause("FROM T_SCH_IEU_SCHEDULER_INSTANCE");
        Map abc = new HashMap<String, Order>();
        //abc.put("DOMAIN_OBJECT_CODE_PK_ID", Order.ASCENDING);
        abc.put("SCHEDULER_INSTANCE_PK_ID", Order.ASCENDING);
        provider.setSortKeys(abc);



        try {
            return provider;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
