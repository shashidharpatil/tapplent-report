package com.tapplent.platformutility.metadata.structure;

/**
 * Created by tapplent on 09/01/17.
 */
public class PropertyControlMaster {
    private String propertyControlMasterCodePkId;
    private String propertyControlMasterNameG11nBigTxt;
    private String propertyControlMasterIconCodeFkId;
    private boolean isMtPESpecific;
    private boolean isAppSpecific;
    private boolean isRecordSpecific;
    private boolean isAttributeSpecific;

    public String getPropertyControlMasterCodePkId() {
        return propertyControlMasterCodePkId;
    }

    public void setPropertyControlMasterCodePkId(String propertyControlMasterCodePkId) {
        this.propertyControlMasterCodePkId = propertyControlMasterCodePkId;
    }

    public String getPropertyControlMasterNameG11nBigTxt() {
        return propertyControlMasterNameG11nBigTxt;
    }

    public void setPropertyControlMasterNameG11nBigTxt(String propertyControlMasterNameG11nBigTxt) {
        this.propertyControlMasterNameG11nBigTxt = propertyControlMasterNameG11nBigTxt;
    }

    public String getPropertyControlMasterIconCodeFkId() {
        return propertyControlMasterIconCodeFkId;
    }

    public void setPropertyControlMasterIconCodeFkId(String propertyControlMasterIconCodeFkId) {
        this.propertyControlMasterIconCodeFkId = propertyControlMasterIconCodeFkId;
    }

    public boolean isMtPESpecific() {
        return isMtPESpecific;
    }

    public void setMtPESpecific(boolean mtPESpecific) {
        isMtPESpecific = mtPESpecific;
    }

    public boolean isAppSpecific() {
        return isAppSpecific;
    }

    public void setAppSpecific(boolean appSpecific) {
        isAppSpecific = appSpecific;
    }

    public boolean isRecordSpecific() {
        return isRecordSpecific;
    }

    public void setRecordSpecific(boolean recordSpecific) {
        isRecordSpecific = recordSpecific;
    }

    public boolean isAttributeSpecific() {
        return isAttributeSpecific;
    }

    public void setAttributeSpecific(boolean attributeSpecific) {
        isAttributeSpecific = attributeSpecific;
    }
}
