package com.tapplent.platformutility.common.util;

/**
 * Created by tapplent on 12/09/17.
 */
public class G11nDetails {
    private String localeCode;
    private String langCode;
    public String getLocaleCode() {
        return localeCode;
    }
    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }
    public String getLangCode() {
        return langCode;
    }
    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }
}
