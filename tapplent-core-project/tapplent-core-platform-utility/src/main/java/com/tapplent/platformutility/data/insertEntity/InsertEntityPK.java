package com.tapplent.platformutility.data.insertEntity;

public class InsertEntityPK {
	
	private final String column;
	private final String value;
	private final String doaName;

	public InsertEntityPK(String column, String doaName, String value) {
		this.column = column;
		this.doaName = doaName;
		this.value = value;
	}

	public String getColumn() {
		return column;
	}

	public String getValue() {
		return value;
	}

	public String getDoaName() {
		return doaName;
	}
}
