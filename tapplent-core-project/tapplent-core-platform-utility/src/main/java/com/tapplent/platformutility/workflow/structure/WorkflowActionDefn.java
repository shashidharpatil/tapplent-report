package com.tapplent.platformutility.workflow.structure;

/**
 * Created by Manas on 8/22/16.
 */
public class WorkflowActionDefn {
    private String versionId;
    private String workflowActionDefnPkId;
    private String workflowDefinitionFkId;
    private int actorActionDispSeqNumPosInt;
    private String actionMasterCodeFkId;
    private String sourceActorFkId;
    private String targetActorFkId;
    private String targetStepFkId;
    private boolean isExitCriteriaCheckApplicable;
    private String ifActionConditionExpn;
    private boolean isActionToDlftExecOnForcedMove;
    private boolean isActionDelegateable;
    private boolean isAutoExecIfActorEqualsSubject;
    private boolean isAutoExecuteIfActorEqualToInitiator;
    private boolean isAutoExecuteIfDuplicate;
    private boolean isActionCommentMandatory;
    private String exitCountCriteriaExpn;
    private boolean isCrtriaCntrlgSrcStepExit;
    private boolean isCrtriaCntrlgTgtStepEntryVsblty;
    private String actionExitSrcActNtfnId;
    private String actionExitTrgtActNtfnId;
    
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getWorkflowActionDefnPkId() {
		return workflowActionDefnPkId;
	}
	public void setWorkflowActionDefnPkId(String workflowActionDefnPkId) {
		this.workflowActionDefnPkId = workflowActionDefnPkId;
	}
	public String getWorkflowDefinitionFkId() {
		return workflowDefinitionFkId;
	}
	public void setWorkflowDefinitionFkId(String workflowDefinitionFkId) {
		this.workflowDefinitionFkId = workflowDefinitionFkId;
	}
	public int getActorActionDispSeqNumPosInt() {
		return actorActionDispSeqNumPosInt;
	}
	public void setActorActionDispSeqNumPosInt(int actorActionDispSeqNumPosInt) {
		this.actorActionDispSeqNumPosInt = actorActionDispSeqNumPosInt;
	}

	public String getActionMasterCodeFkId() {
		return actionMasterCodeFkId;
	}

	public void setActionMasterCodeFkId(String actionMasterCodeFkId) {
		this.actionMasterCodeFkId = actionMasterCodeFkId;
	}

	public String getSourceActorFkId() {
		return sourceActorFkId;
	}
	public void setSourceActorFkId(String sourceActorFkId) {
		this.sourceActorFkId = sourceActorFkId;
	}
	public String getTargetActorFkId() {
		return targetActorFkId;
	}
	public void setTargetActorFkId(String targetActorFkId) {
		this.targetActorFkId = targetActorFkId;
	}
	public String getTargetStepFkId() {
		return targetStepFkId;
	}
	public void setTargetStepFkId(String targetStepFkId) {
		this.targetStepFkId = targetStepFkId;
	}
	public boolean isExitCriteriaCheckApplicable() {
		return isExitCriteriaCheckApplicable;
	}
	public void setExitCriteriaCheckApplicable(boolean isExitCriteriaCheckApplicable) {
		this.isExitCriteriaCheckApplicable = isExitCriteriaCheckApplicable;
	}
	public String getIfActionConditionExpn() {
		return ifActionConditionExpn;
	}
	public void setIfActionConditionExpn(String ifActionConditionExpn) {
		this.ifActionConditionExpn = ifActionConditionExpn;
	}
	public boolean isActionToDlftExecOnForcedMove() {
		return isActionToDlftExecOnForcedMove;
	}
	public void setActionToDlftExecOnForcedMove(boolean isActionToDlftExecOnForcedMove) {
		this.isActionToDlftExecOnForcedMove = isActionToDlftExecOnForcedMove;
	}
	public boolean isActionDelegateable() {
		return isActionDelegateable;
	}
	public void setActionDelegateable(boolean isActionDelegateable) {
		this.isActionDelegateable = isActionDelegateable;
	}
	public boolean isAutoExecIfActorEqualsSubject() {
		return isAutoExecIfActorEqualsSubject;
	}
	public void setAutoExecIfActorEqualsSubject(boolean isAutoExecIfActorEqualsSubject) {
		this.isAutoExecIfActorEqualsSubject = isAutoExecIfActorEqualsSubject;
	}
	public boolean isAutoExecuteIfActorEqualToInitiator() {
		return isAutoExecuteIfActorEqualToInitiator;
	}
	public void setAutoExecuteIfActorEqualToInitiator(boolean isAutoExecuteIfActorEqualToInitiator) {
		this.isAutoExecuteIfActorEqualToInitiator = isAutoExecuteIfActorEqualToInitiator;
	}
	public boolean isAutoExecuteIfDuplicate() {
		return isAutoExecuteIfDuplicate;
	}
	public void setAutoExecuteIfDuplicate(boolean isAutoExecuteIfDuplicate) {
		this.isAutoExecuteIfDuplicate = isAutoExecuteIfDuplicate;
	}
	public boolean isActionCommentMandatory() {
		return isActionCommentMandatory;
	}
	public void setActionCommentMandatory(boolean isActionCommentMandatory) {
		this.isActionCommentMandatory = isActionCommentMandatory;
	}
	public String getExitCountCriteriaExpn() {
		return exitCountCriteriaExpn;
	}
	public void setExitCountCriteriaExpn(String exitCountCriteriaExpn) {
		this.exitCountCriteriaExpn = exitCountCriteriaExpn;
	}
	public boolean isCrtriaCntrlgSrcStepExit() {
		return isCrtriaCntrlgSrcStepExit;
	}
	public void setCrtriaCntrlgSrcStepExit(boolean isCrtriaCntrlgSrcStepExit) {
		this.isCrtriaCntrlgSrcStepExit = isCrtriaCntrlgSrcStepExit;
	}
	public boolean isCrtriaCntrlgTgtStepEntryVsblty() {
		return isCrtriaCntrlgTgtStepEntryVsblty;
	}
	public void setCrtriaCntrlgTgtStepEntryVsblty(boolean isCrtriaCntrlgTgtStepEntryVsblty) {
		this.isCrtriaCntrlgTgtStepEntryVsblty = isCrtriaCntrlgTgtStepEntryVsblty;
	}
	public String getActionExitSrcActNtfnId() {
		return actionExitSrcActNtfnId;
	}
	public void setActionExitSrcActNtfnId(String actionExitSrcActNtfnId) {
		this.actionExitSrcActNtfnId = actionExitSrcActNtfnId;
	}
	public String getActionExitTrgtActNtfnId() {
		return actionExitTrgtActNtfnId;
	}
	public void setActionExitTrgtActNtfnId(String actionExitTrgtActNtfnId) {
		this.actionExitTrgtActNtfnId = actionExitTrgtActNtfnId;
	}

}
