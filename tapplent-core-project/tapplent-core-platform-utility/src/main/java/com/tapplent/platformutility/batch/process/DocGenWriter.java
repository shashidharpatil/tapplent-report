package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.DG_DocGenData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;

import javax.sql.DataSource;
import java.util.List;




public class DocGenWriter extends JdbcBatchItemWriter<DG_DocGenData> {

	
	
	public DocGenWriter(){
		super();
		DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
		setDataSource(ds);
		setSql(getSQL());	
		setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<DG_DocGenData>());
		afterPropertiesSet();
	}
	
	
	public String getSQL(){
		String sql = "UPDATE T_PRN_IEU_DOCUMENT SET RETURN_STATUS_CODE_FK_ID = 'COMPLETED', FILE_ATTACH = :fileAttach WHERE PERSON_DOCUMENT_PK_ID = :personDocumentPkId";
		return sql;
		
	}
	@Override
	public void write(List<? extends DG_DocGenData> arg0) throws Exception {
		
		super.write(arg0);
		
		for(DG_DocGenData bdg : arg0){
			
			System.out.println("inside arg0" + arg0.size());
			if(bdg.getNotificationDefinitionFkId()!=null){
				
				//(TDWI) insert into Notification Transaction call same code as insert during direct flow
				
			}
			
		}
		
		
	}
	
	

}
