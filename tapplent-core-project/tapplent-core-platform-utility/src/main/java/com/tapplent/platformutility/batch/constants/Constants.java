package com.tapplent.platformutility.batch.constants;

public class Constants {
    public static String personDocumentPkId = "PERSON_DOCUMENT_PK_ID";

    public static String personFkId = "PERSON_FK_ID";

    public static String mtPeCodeFkId = "MT_PE_CODE_FK_ID";

    public static String doRowPrimaryKeyTxt = "DO_ROW_PRIMARY_KEY_TXT";

    public static String doRowVersionFkId = "DO_ROW_VERSION_FK_ID";

    public static String dependencyKeyExpn = "DEPENDENCY_KEY_EXPN";

    public static String srcMtPeCodeFkId = "SRC_MT_PE_CODE_FK_ID";

    public static String srcBtCodeFkId = "SRC_BT_CODE_FK_ID";

    public static String srcViewTypeCodeFkId = "SRC_VIEW_TYPE_CODE_FK_ID";

    public static String fileAttach = "FILE_ATTACH";

    public static String fileNameLngTxt = "FILE_NAME_LNG_TXT";

    public static String returnStatusCodeFkId = "RETURN_STATUS_CODE_FK_ID";

    public static String docGenDefinitionPkId = "DOC_GEN_DEFINITION_PK_ID";

    public static String docBtCodeFkId = "DOC_BT_CODE_FK_ID";

    public static String docMtPeCodeFkId = "DOC_MT_PE_CODE_FK_ID";

    public static String docVtCodeFkId = "DOC_VT_CODE_FK_ID";

    public static String docCanvasTxnFkId = "DOC_CANVAS_TXN_FK_ID";

    public static String exportPptTemplateFkId = "EXPORT_PPT_TEMPLATE_FK_ID";

    public static String docTypeCodeFkId = "DOC_TYPE_CODE_FK_ID";

    public static String isPasswordEnabled = "IS_PASSWORD_ENABLED";

    public static String notificationDefinitionFkId = "NOTIFICATION_DEFINITION_FK_ID";

    public static String docNameExpn = "DOC_NAME_EXPN";

    public static String docPwdExpn = "DOC_PWD_EXPN";

    public static String pdf = "PDF";

    public static String ppt = "PPT";

    public static String word = "WORD";

    public static String jpeg = "JPEG";

    public static String csv = "CSV";

    public static String exportPptTemplatePkId = "EXPORT_PPT_TEMPLATE_PK_ID";

    public static String exportPptTemplateNameG11NBlob = "EXPORT_PPT_TEMPLATE_NAME_G11N_BLOB";

    public static String sequenceNumberPosInt = "SEQUENCE_NUMBER_POS_INT";

    public static String pptTemplateFileNameTxt = "PPT_TEMPLATE_FILE_NAME_TXT";

    public static String pptTemplateDocId = "PPT_TEMPLATE_DOC_ID";

    public static String isExportPdf = "IS_EXPORT_PDF";

    public static String exportPptTemplateDoaPkId = "EXPORT_PPT_TEMPLATE_DOA_PK_ID";

    public static String pptSlideidTxt = "PPT_SLIDEID_TXT";

    public static String pptShapeidTxt = "PPT_SHAPEID_TXT";

    public static String pptBtCodeFkId = "PPT_BT_CODE_FK_ID";

    public static String pptMtDoaPathLngTxt = "PPT_MT_DOA_PATH_LNG_TXT";

    public static String socialAccountPostTxnDepFkId = "SOCIAL_ACCOUNT_POST_TXN_DEP_FK_ID";

    public static String socialAccountPersonPostPkId = "SOCIAL_ACCOUNT_PERSON_POST_PK_ID";

    public static String socialAccountPostTxnPkId = "SOCIAL_ACCOUNT_POST_TXN_PK_ID";

    public static String socialAccountPostDefnFkId = "SOCIAL_ACCOUNT_POST_DEFN_FK_ID";

    public static String socialAccountTypeCodeFkId = "SOCIAL_ACCOUNT_TYPE_CODE_FK_ID";

    public static String faceBook = "FACEBOOK";

    public static String linkedIn = "LINKEDIN";

    public static String twitter = "TWITTER";

    public static String google = "GOOGLE";

    public static String pushNotification = "PUSH_NOTIFICATION";

    public static String skype = "SKYPE";

    public static String gmail = "GMAIL";

    public static String telegram = "TELEGRAM";

    public static String faceBookMessenger = "FACEBOOK_MESSENGER";

    public static String personNotificationPkId = "PERSON_NOTIFICATION_PK_ID";

    public static String generateDatetime = "GENERATE_DATETIME";

    public static String isSync = "IS_SYNC";

    public static String ntfnBtCodeFkId = "NTFN_BT_CODE_FK_ID";

    public static String ntfnMtPeCodeFkId = "NTFN_MT_PE_CODE_FK_ID";

    public static String ntfnVtCodeFkId = "NTFN_VT_CODE_FK_ID";

    public static String ntfnCanvasTxnFkId = "NTFN_CANVAS_TXN_FK_ID";

    public static String forDocGenDefnFkId = "FOR_DOC_GEN_DEFN_FK_ID";

    public static String notification_key = "notification_key";

    public static String create = "create";

    public static String add = "add";

    public static String remove = "remove";

    public static String gcmGroupUrl = "https://android.googleapis.com/gcm/notification";

    public static String gcmSendUrl = "https://android.googleapis.com/gcm/send";

    public static String title = "title";

    public static String notificationText = "notificationText";

    public static String subText = "subText";

    public static String smallIcon = "smallIcon";

    public static String actions = "actions";

    public static String notificationKey = "notificationKey";

    public static String notificationId = "notificationId";

    public static String apikey = "AppSecretKey";

    public static String projectId = "AppId";

    public static String success = "SUCCESS";

    public static String personDepFkId = "PERSON_DEP_FK_ID";

    public static String personSocialAccountPkId = "PERSON_SOCIAL_ACCOUNT_PK_ID";

    public static String socialAccountCaracteristicsBlob = "SOCIAL_ACCOUNT_CARACTERISTICS_BLOB";

    public static String isValid = "SUCCESS";

    public static String deviceIndependentBlob = "DEVICE_INDEPENDENT_BLOB";

    public static String accountHandleLngTxt = "ACCOUNT_HANDLE_LNG_TXT";




    //for Scheduler
    public static String schedDefnPkId = "SCHED_DEFN_PK_ID";
    public static String jobTypeCodeFkId = "JOB_TYPE_CODE_FK_ID";
    public static String isOneTimeSched = "IS_ONE_TIME_SCHED";
    public static String isRecurringSched = "IS_RECURRING_SCHED";
    public static String oneTimeDatetime = "ONE_TIME_DATETIME";
    public static String schedGrouperPkId = "SCHED_GROUPER_PK_ID";
    public static String schedDrivingFreqCodeFkId = "SCHED_DRIVING_FREQ_CODE_FK_ID";
    public static String schedDrivingIntervalPosInt = "SCHED_DRIVING_INTERVAL_POS_INT";
    public static String schedExecStartDatetime = "SCHED_EXEC_START_DATETIME";
    public static String schedExecEndDatetime = "SCHED_EXEC_END_DATETIME";
    public static String isExecutionNeverEnds = "IS_EXECUTION_NEVER_ENDS";
    public static String schedRcrFreqPkId = "SCHED_RCR_FREQ_PK_ID";
    public static String frequencyCodeFkId = "FREQUENCY_CODE_FK_ID";
    public static String intervalPosInt = "INTERVAL_POS_INT";
    public static String absoluteDatetime = "ABSOLUTE_DATETIME";



    public static String schedulerInstancePkId = "SCHEDULER_INSTANCE_PK_ID";
    public static String schedDefnFkId = "SCHED_DEFN_FK_ID";
    public static String executionVersionId = "EXECUTION_VERSION_ID";

    public static String scheduledDatetime = "SCHEDULED_DATETIME";
    public static String executionStartDatetime = "EXECUTION_START_DATETIME";
    public static String executionEndDatetime = "EXECUTION_END_DATETIME";
    public static String execStatusCodeFkId = "EXEC_STATUS_CODE_FK_ID";
    public static String exitCodeFkId = "EXIT_CODE_FK_ID";
    public static String exitMessageBigTxt = "EXIT_MESSAGE_BIG_TXT";

    //Facebook Constants
    public static String facebookProfileUrl = "https://graph.facebook.com/me?fields=id,name,birthday,email,work,gender,age_range,languages,hometown,education&access_token=";
    public static String facebookPostImageUrl = "https://graph.facebook.com/me/photos";
    public static String facebookPostLinkUrl = "https://graph.facebook.com/me/feed";
    public static String facebookCompanyPostUrl = "https://graph.facebook.com/";
    public static String facebookExchangeTokenUrl = "https://graph.facebook.com/oauth/access_token? grant_type=fb_exchange_token";
    public static String facebookRefreshTokenUrl = "https://graph.facebook.com/oauth/client_code?access_token=";
    public static String facebookDelete_updateUrl = "https://graph.facebook.com/";
    public static String facebookRedirectUri = "http://tapplent-social-site-permission.azurewebsites.net/";

    //LinkedIn Constants
    public static String linkedInProfileUrl = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,picture-url,industry,summary,specialties,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)),educations:(id,school-name,field-of-study,start-date,end-date,degree,activities,notes),associations,interests,num-recommenders,date-of-birth,publications:(id,title,publisher:(name),authors:(id,name),date,url,summary),patents:(id,title,summary,number,status:(id,name),office:(name),inventors:(id,name),date,url),languages:(id,language:(name),proficiency:(level,name)),skills:(id,skill:(name)),certifications:(id,name,authority:(name),number,start-date,end-date),courses:(id,name,number),recommendations-received:(id,recommendation-type,recommendation-text,recommender),honors-awards,three-current-positions,three-past-positions,volunteer";
    public static String linkedInPostUrl = "https://api.linkedin.com/v1/people/~/shares";
    public static String linkedInCompanyPostUrl = "https://api.linkedin.com/v1/companies/";

    //Twitter Constants
    public static String TwitterProfileUrl = "";
    public static String TwitterPostTextUrl = "https://api.twitter.com/1.1/statuses/update.json";
    public static String TwitterPostImageUrl = "https://api.twitter.com/1.1/statuses/update_with_media.json";
    public static String TwitterDeletePostUrl = "https://api.twitter.com/1.1/statuses/destroy/";

    //Pintrest Constants
    public static String PintrestProfileUrl = "https://api.pinterest.com/v1/users/me/";
    public static String PintrestGetBoardsUrl = "https://api.pinterest.com/v1/users/me/boards/";
    public static String PintrestGetPinsUrl = "https://api.pinterest.com/v1/users/me/pins/";
    public static String PintrestCreate_DeleteBoardUrl = "https://api.pinterest.com/v1/boards/";
    public static String PintrestCreate_DeletePinUrl = "https://api.pinterest.com/v1/pins/";

    //Google Constants
    public static String GoogleTextMailUrl = "https://www.googleapis.com/upload/gmail/v1/users/shashidharpatil1234@gmail.com/messages/send";
    public static String GoogleAttachmentUrl = "https://www.googleapis.com/upload/gmail/v1/users/userId/messages/send?uploadType=media";
}
