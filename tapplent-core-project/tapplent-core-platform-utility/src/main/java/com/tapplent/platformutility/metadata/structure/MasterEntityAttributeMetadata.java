package com.tapplent.platformutility.metadata.structure;

import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.metadata.MtDoAttributeVO;

public class MasterEntityAttributeMetadata {
	private String versionId;
	private String doAttributeCode;
	private String doAttributeName;
	private IconVO doAttributeIcon;
	private String domainObjectCode;
	private String baseObjectId;
	private String dbTableName;
	private String dbColumnName;
	private String dbDataTypeCode;
	private boolean isFunctionalPrimaryKey;
	private boolean isDbPrimaryKey;
	private boolean isDependencyKey;
	private boolean isMandatory;
	private boolean isCustomizable;
	private boolean isGlocalized;
	private boolean isSelfJoinParent;
	private boolean isRelation;
	private boolean isHierarchy;
	private boolean isReportable;
	private boolean isNonVersionTrackable;
	private boolean isLocalSearchable;
	private String containerBlobDbColumnName;
	private String toDomainObjectCode;
	private String toDbTableName;
	private String toDoAttributeCode;
	private String toDbColumnName;
	private boolean isCardinalityManyAllowed;
	private String jsonName;

	public String getVersionId() {
		return versionId;
	}


	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}


	public String getDoAttributeCode() {
		return doAttributeCode;
	}


	public void setDoAttributeCode(String doAttributeCode) {
		this.doAttributeCode = doAttributeCode;
	}


	public String getDoAttributeName(boolean wholeStringRequired) {
		if (wholeStringRequired)
			return doAttributeName;
		else return Util.getG11nValue(doAttributeName, null);
	}


	public void setDoAttributeName(String doAttributeName) {
		this.doAttributeName = doAttributeName;
	}


	public IconVO getDoAttributeIcon() {
		return doAttributeIcon;
	}

	public void setDoAttributeIcon(IconVO doAttributeIcon) {
		this.doAttributeIcon = doAttributeIcon;
	}

	public String getDomainObjectCode() {
		return domainObjectCode;
	}


	public void setDomainObjectCode(String domainObjectCode) {
		this.domainObjectCode = domainObjectCode;
	}


	public String getBaseObjectId() {
		return baseObjectId;
	}


	public void setBaseObjectId(String baseObjectId) {
		this.baseObjectId = baseObjectId;
	}


	public String getDbTableName() {
		return dbTableName;
	}


	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}


	public String getDbColumnName() {
		return dbColumnName;
	}


	public void setDbColumnName(String dbColumnName) {
		this.dbColumnName = dbColumnName;
	}


	public String getDbDataTypeCode() {
		return dbDataTypeCode;
	}


	public void setDbDataTypeCode(String dbDataTypeCode) {
		this.dbDataTypeCode = dbDataTypeCode;
	}


	public boolean isFunctionalPrimaryKey() {
		return isFunctionalPrimaryKey;
	}


	public void setFunctionalPrimaryKey(boolean isFunctionalPrimaryKey) {
		this.isFunctionalPrimaryKey = isFunctionalPrimaryKey;
	}


	public boolean isDbPrimaryKey() {
		return isDbPrimaryKey;
	}


	public void setDbPrimaryKey(boolean isDbPrimaryKey) {
		this.isDbPrimaryKey = isDbPrimaryKey;
	}


	public boolean isDependencyKey() {
		return isDependencyKey;
	}


	public void setDependencyKey(boolean isDependencyKey) {
		this.isDependencyKey = isDependencyKey;
	}


	public boolean isMandatory() {
		return isMandatory;
	}


	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}


	public boolean isCustomizable() {
		return isCustomizable;
	}


	public void setCustomizable(boolean isCustomizable) {
		this.isCustomizable = isCustomizable;
	}


	public boolean isGlocalized() {
		return isGlocalized;
	}


	public void setGlocalized(boolean isGlocalized) {
		this.isGlocalized = isGlocalized;
	}


	public boolean isSelfJoinParent() {
		return isSelfJoinParent;
	}


	public void setSelfJoinParent(boolean isSelfJoinParent) {
		this.isSelfJoinParent = isSelfJoinParent;
	}


	public boolean isRelation() {
		return isRelation;
	}


	public void setRelation(boolean isRelation) {
		this.isRelation = isRelation;
	}


	public boolean isHierarchy() {
		return isHierarchy;
	}


	public void setHierarchy(boolean isHierarchy) {
		this.isHierarchy = isHierarchy;
	}


	public boolean isReportable() {
		return isReportable;
	}


	public void setReportable(boolean isReportable) {
		this.isReportable = isReportable;
	}


	public boolean isNonVersionTrackable() {
		return isNonVersionTrackable;
	}


	public void setNonVersionTrackable(boolean isNonVersionTrackable) {
		this.isNonVersionTrackable = isNonVersionTrackable;
	}


	public boolean isLocalSearchable() {
		return isLocalSearchable;
	}


	public void setLocalSearchable(boolean isLocalSearchable) {
		this.isLocalSearchable = isLocalSearchable;
	}


	public String getContainerBlobDbColumnName() {
		return containerBlobDbColumnName;
	}


	public void setContainerBlobDbColumnName(String containerBlobDbColumnName) {
		this.containerBlobDbColumnName = containerBlobDbColumnName;
	}


	public String getToDomainObjectCode() {
		return toDomainObjectCode;
	}


	public void setToDomainObjectCode(String toDomainObjectCode) {
		this.toDomainObjectCode = toDomainObjectCode;
	}


	public String getToDbTableName() {
		return toDbTableName;
	}


	public void setToDbTableName(String toDbTableName) {
		this.toDbTableName = toDbTableName;
	}


	public String getToDoAttributeCode() {
		return toDoAttributeCode;
	}


	public void setToDoAttributeCode(String toDoAttributeCode) {
		this.toDoAttributeCode = toDoAttributeCode;
	}


	public String getToDbColumnName() {
		return toDbColumnName;
	}


	public void setToDbColumnName(String toDbColumnName) {
		this.toDbColumnName = toDbColumnName;
	}


	public boolean isCardinalityManyAllowed() {
		return isCardinalityManyAllowed;
	}


	public void setCardinalityManyAllowed(boolean isCardinalityManyAllowed) {
		this.isCardinalityManyAllowed = isCardinalityManyAllowed;
	}


	public String getJsonName() {
		return jsonName;
	}


	public void setJsonName(String jsonName) {
		this.jsonName = jsonName;
	}


	public static MasterEntityAttributeMetadata fromVO(MtDoAttributeVO mtDoAttributeVO){
		MasterEntityAttributeMetadata masterEntityAttributeMetadata = new MasterEntityAttributeMetadata();
		BeanUtils.copyProperties(mtDoAttributeVO, masterEntityAttributeMetadata);
		return masterEntityAttributeMetadata;
	}
}
