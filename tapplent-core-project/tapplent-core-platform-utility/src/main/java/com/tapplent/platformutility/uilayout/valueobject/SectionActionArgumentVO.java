package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 26/04/17.
 */
public class SectionActionArgumentVO {
    private String actionArgumentPkId;
    private String sectionActionFkId;
    private String argumentParameter;
    private String mtPE;
    private String mtPEAlias;
    private String parentMTPEAlias;
    private String fkRelnWithParent;
    private String operator;
    private String attributeValue;

    public String getActionArgumentPkId() {
        return actionArgumentPkId;
    }

    public void setActionArgumentPkId(String actionArgumentPkId) {
        this.actionArgumentPkId = actionArgumentPkId;
    }

    public String getSectionActionFkId() {
        return sectionActionFkId;
    }

    public void setSectionActionFkId(String sectionActionFkId) {
        this.sectionActionFkId = sectionActionFkId;
    }

    public String getArgumentParameter() {
        return argumentParameter;
    }

    public void setArgumentParameter(String argumentParameter) {
        this.argumentParameter = argumentParameter;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getParentMTPEAlias() {
        return parentMTPEAlias;
    }

    public void setParentMTPEAlias(String parentMTPEAlias) {
        this.parentMTPEAlias = parentMTPEAlias;
    }

    public String getFkRelnWithParent() {
        return fkRelnWithParent;
    }

    public void setFkRelnWithParent(String fkRelnWithParent) {
        this.fkRelnWithParent = fkRelnWithParent;
    }
}
