package com.tapplent.platformutility.workflow.structure;

import java.sql.Timestamp;

/**
 * Created by Manas on 8/30/16.
 */
public class WorkflowActorPossibleActionTxn {
    private String workflowTxnActorPossibleActionId;
    private String WorkflowTxnStepActorId;
    private String actionMasterCodeFkId;
    private String WorkflowActorActionDefnId;
    private String SrcActrPersonFkId;
    private String SrcActorFkId;
    private Timestamp SrcActrActlRcvDatetime;
    private Timestamp SrcActrActlCmpltDatetime;
    private String SrcActrActionStatusCodeFkId;
    private String SrcActionCommentLngTxt;
    private boolean IsCommentMandatoryFlag;
    private boolean IsActionEnabled;
    private String IfActionConditionExpn;

    public String getWorkflowTxnActorPossibleActionId() {
        return workflowTxnActorPossibleActionId;
    }

    public void setWorkflowTxnActorPossibleActionId(String workflowTxnActorPossibleActionId) {
        this.workflowTxnActorPossibleActionId = workflowTxnActorPossibleActionId;
    }

    public String getWorkflowTxnStepActorId() {
        return WorkflowTxnStepActorId;
    }

    public void setWorkflowTxnStepActorId(String workflowTxnStepActorId) {
        WorkflowTxnStepActorId = workflowTxnStepActorId;
    }

    public String getActionMasterCodeFkId() {
        return actionMasterCodeFkId;
    }

    public void setActionMasterCodeFkId(String actionMasterCodeFkId) {
        this.actionMasterCodeFkId = actionMasterCodeFkId;
    }

    public String getWorkflowActorActionDefnId() {
        return WorkflowActorActionDefnId;
    }

    public void setWorkflowActorActionDefnId(String workflowActorActionDefnId) {
        WorkflowActorActionDefnId = workflowActorActionDefnId;
    }

    public String getSrcActrPersonFkId() {
        return SrcActrPersonFkId;
    }

    public void setSrcActrPersonFkId(String srcActrPersonFkId) {
        SrcActrPersonFkId = srcActrPersonFkId;
    }

    public String getSrcActorFkId() {
        return SrcActorFkId;
    }

    public void setSrcActorFkId(String srcActorFkId) {
        SrcActorFkId = srcActorFkId;
    }

    public Timestamp getSrcActrActlRcvDatetime() {
        return SrcActrActlRcvDatetime;
    }

    public void setSrcActrActlRcvDatetime(Timestamp srcActrActlRcvDatetime) {
        SrcActrActlRcvDatetime = srcActrActlRcvDatetime;
    }

    public Timestamp getSrcActrActlCmpltDatetime() {
        return SrcActrActlCmpltDatetime;
    }

    public void setSrcActrActlCmpltDatetime(Timestamp srcActrActlCmpltDatetime) {
        SrcActrActlCmpltDatetime = srcActrActlCmpltDatetime;
    }

    public String getSrcActrActionStatusCodeFkId() {
        return SrcActrActionStatusCodeFkId;
    }

    public void setSrcActrActionStatusCodeFkId(String srcActrActionStatusCodeFkId) {
        SrcActrActionStatusCodeFkId = srcActrActionStatusCodeFkId;
    }

    public String getSrcActionCommentLngTxt() {
        return SrcActionCommentLngTxt;
    }

    public void setSrcActionCommentLngTxt(String srcActionCommentLngTxt) {
        SrcActionCommentLngTxt = srcActionCommentLngTxt;
    }

    public boolean isCommentMandatoryFlag() {
        return IsCommentMandatoryFlag;
    }

    public void setCommentMandatoryFlag(boolean commentMandatoryFlag) {
        IsCommentMandatoryFlag = commentMandatoryFlag;
    }

    public boolean isActionEnabled() {
        return IsActionEnabled;
    }

    public void setActionEnabled(boolean actionEnabled) {
        IsActionEnabled = actionEnabled;
    }

    public String getIfActionConditionExpn() {
        return IfActionConditionExpn;
    }

    public void setIfActionConditionExpn(String ifActionConditionExpn) {
        IfActionConditionExpn = ifActionConditionExpn;
    }
}
