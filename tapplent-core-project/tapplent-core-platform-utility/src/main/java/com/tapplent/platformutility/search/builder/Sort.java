package com.tapplent.platformutility.search.builder;

public enum Sort {
	ASC("ASC"), 
	DESC("DESC");
    private String order;
    Sort(String order) {
        this.order = order;
    }
    public String getOrder() {
        return order;
    }
}

