package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.codec.binary.Hex;
import org.springframework.transaction.annotation.Transactional;

import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;

public class EverNoteDAOImpl extends TapplentBaseDAO implements EverNoteDAO {
	@Override
	@Transactional
	public List<String> getAllNotes(String thirdPartyAppName) {
		
		List<String> noteIds = new ArrayList<>();
		String SQL = "SELECT * FROM T_PFM_TAP_NOTE_INTGN WHERE EXTERNAL_INTEGRATION_APP_CODE_FK_ID = ? AND EXTERNAL_REFERENCE_TXT =NULL;"; 
		List<Object> parameters = new ArrayList<>();
		parameters.add(thirdPartyAppName);

		ResultSet rs = executeSQL(SQL, parameters);
		String noteId = "";
		try{
			while(rs.next()){
				noteId = convertByteToString(rs.getBytes("NOTE_FK_ID"));
				noteIds.add(noteId);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return noteIds;
		
		
	}
	@Override
	public List<Meeting> getAllMeetings(String personPkId) {
		String SQL = "SELECT MEETING_PK_ID,MEETING_TITLE_G11N_BIG_TXT,MEETING_DESC_G11N_BIG_TXT,MEETING_CATEGORY_CODE_FK_ID,MEETING_EVENT_BACKGROUND_IMAGEID,MEETING_START_DATETIME,MEETING_END_DATETIME,LOCATION_TXT,MEETING_TYPE_CODE_FK_ID FROM T_PFM_EU_MEETING LEFT JOIN T_PFM_TAP_MTG_SYNCH ON CREATEDBY_PERSON_FK_ID = PERSON_FK_ID WHERE CREATEDBY_PERSON_FK_ID=? AND EXTERNAL_INTEGRATION_APP_FK_ID = ?;";
		List<Meeting> meetings = new ArrayList<>();
		List<Object> parameters = new ArrayList<>();
		parameters.add(personPkId);
		parameters.add("GOOGLE_CALENDAR");
		ResultSet rs = executeSQL(SQL, parameters);
		Meeting meeting = null;
		try{
			while(rs.next()){
				meeting = new Meeting();
				meeting.setMeetingPkId(convertByteToString(rs.getBytes("MEETING_PK_ID")));
				meeting.setMeetingTitleG11NBigTxt(rs.getString("MEETING_TITLE_G11N_BIG_TXT"));
				meeting.setMeetingDescG11NBigTxt(rs.getString("MEETING_DESC_G11N_BIG_TXT"));
				meeting.setMeetingCategoryCodeFkId(rs.getString("MEETING_CATEGORY_CODE_FK_ID"));
				meeting.setMeetingEventBackgroundImageid(rs.getString("MEETING_EVENT_BACKGROUND_IMAGEID"));
				meeting.setMeetingStartDatetime(Timestamp.valueOf(rs.getString("MEETING_START_DATETIME")));
				meeting.setMeetingEndDatetime(Timestamp.valueOf(rs.getString("MEETING_END_DATETIME")));
				meeting.setLocationTxt(rs.getString("LOCATION_TXT"));
				meeting.setMeetingTypeCodeFkId(rs.getString("MEETING_TYPE_CODE_FK_ID"));
				meeting.setMeetingCallInfoDetailsG11NBigTxt(rs.getString("MEETING_CALL_INFO_DETAILS_G11N_BIG_TXT"));			
				meetings.add(meeting);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return meetings;
	}
	@Override
	public Meeting getMeeting(String tapplentMeetingPkId,Map<String, EntityAttributeMetadata> attributeMetaMap,String personId) {
		
		String SQL = "SELECT * FROM T_PFM_EU_MEETING WHERE MEETING_PK_ID = x?;"; 
		List<Object> parameters = new ArrayList<>();
		parameters.add(tapplentMeetingPkId);
		ResultSet rs = executeSQL(SQL, parameters);
		Meeting meeting = null;
		try{
			if(rs.next()){
				meeting = new Meeting();
				meeting.setMeetingPkId(convertByteToString(rs.getBytes("MEETING_PK_ID")));
				
				if(attributeMetaMap.get("MEETING_TITLE_G11N_BIG_TXT").isGlocalizedFlag())
					meeting.setMeetingTitleG11NBigTxt(Util.getG11nValue(rs.getString("MEETING_TITLE_G11N_BIG_TXT"), personId));
				else 
				 meeting.setMeetingTitleG11NBigTxt(rs.getString("MEETING_TITLE_G11N_BIG_TXT"));
				if(attributeMetaMap.get("MEETING_DESC_G11N_BIG_TXT").isGlocalizedFlag())
					meeting.setMeetingDescG11NBigTxt(Util.getG11nValue(rs.getString("MEETING_DESC_G11N_BIG_TXT"), personId));
				else 
					meeting.setMeetingDescG11NBigTxt(rs.getString("MEETING_DESC_G11N_BIG_TXT"));
				
				meeting.setMeetingCategoryCodeFkId(convertByteToString(rs.getBytes("MEETING_CATEGORY_CODE_FK_ID")));
				meeting.setMeetingEventBackgroundImageid(convertByteToString(rs.getBytes("MEETING_EVENT_BACKGROUND_IMAGEID")));
				meeting.setMeetingStartDatetime(Timestamp.valueOf(rs.getString("MEETING_START_DATETIME")));
				meeting.setMeetingEndDatetime(Timestamp.valueOf(rs.getString("MEETING_END_DATETIME")));
				meeting.setLocationTxt(rs.getString("LOCATION_TXT"));
				meeting.setGeoLoclatlng(rs.getString("GEO_LOCLATLNG"));
				meeting.setMeetingTypeCodeFkId(rs.getString("MEETING_TYPE_CODE_FK_ID"));
				if(attributeMetaMap.get("MEETING_CALL_INFO_DETAILS_G11N_BIG_TXT").isGlocalizedFlag())
					meeting.setMeetingCallInfoDetailsG11NBigTxt(Util.getG11nValue(rs.getString("MEETING_CALL_INFO_DETAILS_G11N_BIG_TXT"), personId));
				else 
					meeting.setMeetingCallInfoDetailsG11NBigTxt(rs.getString("MEETING_CALL_INFO_DETAILS_G11N_BIG_TXT"));
				meeting.setLastModifiedDateTime(Timestamp.valueOf(rs.getString("LAST_MODIFIED_DATETIME")));
				meeting.setDeleted(rs.getBoolean("IS_DELETED"));
				meeting.setPrivate(rs.getBoolean("IS_PRIVATE"));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return meeting;
		
	}
	@Override
public SingleNote getNote(String tapplentNotePkId,Map<String, EntityAttributeMetadata> attributeMetaMap, String personId) {
		
	String SQL = "SELECT * FROM T_PFM_EU_NOTE WHERE NOTE_PK_ID = x?;";
	
	List<Object> parameters = new ArrayList<>();
	parameters.add(tapplentNotePkId);
	ResultSet rs = executeSQL(SQL, parameters);
	SingleNote note = null;
	try{
		if(rs.next()){
			note = new SingleNote();
			if(attributeMetaMap.get("NOTE_TITLE_G11N_BIG_TXT").isGlocalizedFlag())
				note.setNotetTitleG11NBigTxt(Util.getG11nValue(rs.getString("NOTE_TITLE_G11N_BIG_TXT"), personId));
			else 
				note.setNotetTitleG11NBigTxt(rs.getString("NOTE_TITLE_G11N_BIG_TXT"));
			if(attributeMetaMap.get("NOTE_DESC_G11N_BIG_TXT").isGlocalizedFlag())
				note.setNoteDescG11NBigTxt(Util.getG11nValue(rs.getString("NOTE_DESC_G11N_BIG_TXT"), personId));
			else 
				note.setNoteDescG11NBigTxt(rs.getString("NOTE_DESC_G11N_BIG_TXT"));
			note.setNotePkId(convertByteToString(rs.getBytes("NOTE_PK_ID")));
			
			note.setAboutPersonFkId(convertByteToString(rs.getBytes("ABOUT_PERSON_FK_ID")));
			//note.setReminderDateTime(Timestamp.valueOf(rs.getString("REMINDER_DATETIME")));
			note.setReminderLoclatlng(rs.getString("REMINDER_LOCLATLNG"));
			note.setLastModifiedDateTime(Timestamp.valueOf(rs.getString("LAST_MODIFIED_DATETIME")));
			
		}
	}catch(SQLException e){
		e.printStackTrace();
	}
	return note;
		
	}
	@Override
	public Map<String,Object> isSynchNeeded(String personPkId,String tableName,String eventPkId) {
		Map<String,Object> columnNameValueMap = new HashMap<String,Object>();
		String SQL = "SELECT * FROM "+tableName+" WHERE PERSON_FK_ID= x? AND IS_DELETED = FALSE;"; 
		List<Object> parameters = new ArrayList<>();
		parameters.add(personPkId);
		boolean isSynch =false;
		String lastSyncId = "";
		String integrationAppName = "";
		String syncPkId = "";
		Timestamp lastModifiedDateTime = null;
		
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			if(rs.next()){
				
				syncPkId = convertByteToString(rs.getBytes(eventPkId));
				columnNameValueMap.put("syncPkId", syncPkId);
				isSynch = rs.getBoolean("IS_SYNCH_NEEDED");
				columnNameValueMap.put("isSynchNeeded", isSynch);
				lastSyncId = rs.getString("LAST_SYNCH_EXT_APP_REF_TXT");
				columnNameValueMap.put("lastSyncId", lastSyncId);
				integrationAppName = rs.getString("EXTERNAL_INTEGRATION_APP_CODE_FK_ID");
				columnNameValueMap.put("integrationAppName",integrationAppName);
				lastModifiedDateTime = Timestamp.valueOf(rs.getString("LAST_MODIFIED_DATETIME"));
				columnNameValueMap.put("lastModifiedDateTime", lastModifiedDateTime);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return columnNameValueMap;
	}
	@Override
	 public Map<String,String> getToken(String socialAccType,String personFkId){
		 String SQL = "SELECT INTGN_ACCT_REFRESH_TOKEN_LNG_TXT,INTGN_ACCT_ACCESS_TOKEN_LNG_TXT FROM T_PRN_EU_INTGN_ACCT LEFT JOIN T_PRN_EU_INTGN_PREF  ON T_PRN_EU_INTGN_ACCT.PERSON_FK_ID = T_PRN_EU_INTGN_PREF.PERSON_FK_ID WHERE T_PRN_EU_INTGN_PREF.IS_INTGN_ENABLED =TRUE AND T_PRN_EU_INTGN_ACCT.EXTERNAL_INTEGRATION_APP_CODE_FK_ID = ?;"; 
		 List<Object> parameters = new ArrayList<>();
		 Map<String,String> tokens = new HashMap<String,String>();
			parameters.add(socialAccType);
			ResultSet rs = executeSQL(SQL, parameters);
			try{
				if(rs.next()){
					tokens.put("accessToken", rs.getString("INTGN_ACCT_ACCESS_TOKEN_LNG_TXT"));
					tokens.put("refreshToken", rs.getString("INTGN_ACCT_REFRESH_TOKEN_LNG_TXT"));
					
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			return tokens;
			
	 }
	@Override
	public List<EventAttchment> getMeetingAttachments(String meetingId) {
		List<EventAttchment> attachments = new ArrayList<>();
		String SQL = "SELECT * FROM T_PFM_EU_MEETING_ATTACHMENT WHERE MEETING_FK_ID = x?;";
		 List<Object> parameters = new ArrayList<>();
			parameters.add(meetingId);
			ResultSet rs = executeSQL(SQL, parameters);
			EventAttchment attachment = null;
			try{
				while(rs.next()){
					attachment = new EventAttchment();
					attachment.setAttachmentPkId(convertByteToString(rs.getBytes("MEETING_ATTACHMENT_PK_ID")));
					attachment.setArtefactAttach(convertByteToString(rs.getBytes("ARTEFACT_ATTACH")));
					attachment.setAttachThumnailImageid(convertByteToString(rs.getBytes("ATTACH_THUMBNAIL_IMAGEID")));
					attachment.setAttachmentNameTxt(rs.getString("ATTACHMENT_NAME_TXT"));
					attachment.setAttachmentTypeCodeFkId(rs.getString("ATTACHMENT_TYPE_CODE_FK_ID"));
					attachment.setDeleted(rs.getBoolean("IS_DELETED"));
					attachment.setLastModifiedDatetime(Timestamp.valueOf(rs.getString("LAST_MODIFIED_DATETIME")));
					attachments.add(attachment);
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			return attachments;
	}
	@Override
	public List<MeetingInvitee> getAllMeetingInvitee(String meetingId) {
		List<MeetingInvitee> invitees = new ArrayList<>();
		String SQL = "SELECT * FROM T_PFM_EU_MEETING_ATTACHMENT WHERE MEETING_FK_ID = ?;";
		 List<Object> parameters = new ArrayList<>();
			parameters.add(meetingId);
			ResultSet rs = executeSQL(SQL, parameters);
			MeetingInvitee invitee = null;
			try{
				while(rs.next()){
					invitee = new MeetingInvitee();
					invitee.setMeetingInviteePkId(convertByteToString(rs.getBytes("MEETING_INVITEE_PK_ID")));
					invitee.setInvitedPersonFkId(convertByteToString(rs.getBytes("INVITED_PERSON_FK_ID")));
					invitee.setAttendeeStatusCodeFkId(rs.getString("ATTENDEE_STATUS_CODE_FK_ID"));
					invitee.setDeleted(rs.getBoolean("IS_DELETED"));
					invitees.add(invitee);
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			return invitees;
	}
	@Override
	public List<EventAttchment> getNoteAttachments(String noteId) {
		List<EventAttchment> attachments = new ArrayList<>();
		String SQL = "SELECT * FROM T_PFM_EU_NOTE_ATTACHMENT WHERE NOTE_FK_ID = x?;";
		 List<Object> parameters = new ArrayList<>();
			parameters.add(noteId);
			ResultSet rs = executeSQL(SQL, parameters);
			EventAttchment attachment = null;
			try{
				while(rs.next()){
					attachment = new EventAttchment();
					attachment.setAttachmentPkId(convertByteToString(rs.getBytes("NOTE_ATTACHMENT_PK_ID")));
					attachment.setArtefactAttach(convertByteToString(rs.getBytes("ARTEFACT_ATTACH")));
					attachment.setAttachThumnailImageid(convertByteToString(rs.getBytes("ATTACH_THUMBNAIL_IMAGEID")));
					attachment.setAttachmentNameTxt(rs.getString("ATTACHMENT_NAME_TXT"));
					attachment.setAttachmentTypeCodeFkId(convertByteToString(rs.getBytes("ATTACHMENT_TYPE_CODE_FK_ID")));
					attachment.setLastModifiedDatetime(Timestamp.valueOf(rs.getString("LAST_MODIFIED_DATETIME")));
					attachments.add(attachment);
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			return attachments;
	}
	
	@Override
	public String getTheEventId(String thirdPartyEventId,String tableName,String integrationAppCode,String eventId){
		String SQL = "SELECT "+eventId+" FROM "+tableName+" WHERE EXTERNAL_INTEGRATION_APP_FK_ID = x? AND EXTERNAL_REFERENCE_TXT = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(integrationAppCode);
		parameters.add(thirdPartyEventId);
		ResultSet rs = executeSQL(SQL, parameters);
		String eventFkId = "";
		try{
			if(rs.next()){
				eventFkId = convertByteToString(rs.getBytes(eventId));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return eventFkId;
	}
	@Override
	public Task getTask(String tapplentTaskPkId,Map<String, EntityAttributeMetadata> attributeMetaMap, String personPkId) {
		
		String SQL = "SELECT * FROM T_PFM_EU_TASK WHERE TASK_PK_ID = x?;"; 
		List<Object> parameters = new ArrayList<>();
		parameters.add(tapplentTaskPkId);
		ResultSet rs = executeSQL(SQL, parameters);
		Task task = null;
		try{
			if(rs.next()){
				task = new Task();
				if(attributeMetaMap.get("TASK_TITLE_G11N_BIG_TXT").isGlocalizedFlag())
					task.setTaskTitleG11nBigTxt(Util.getG11nValue(rs.getString("TASK_TITLE_G11N_BIG_TXT"), personPkId));
				else 
					task.setTaskTitleG11nBigTxt(rs.getString("TASK_TITLE_G11N_BIG_TXT"));
				if(attributeMetaMap.get("TASK_DESC_COMMENT_G11N_BIG_TXT").isGlocalizedFlag())
					task.setTaskDescCommentG11nBigTxt(Util.getG11nValue(rs.getString("TASK_DESC_COMMENT_G11N_BIG_TXT"), personPkId));
				else 
					task.setTaskDescCommentG11nBigTxt(rs.getString("TASK_TITLE_G11N_BIG_TXT"));
				
				task.setTaskEndDatetime(Timestamp.valueOf(rs.getString("TASK_END_DATETIME")));
				task.setTaskStatusCodeFkId(rs.getString("TASK_STATUS_CODE_FK_ID"));
				task.setLastModifiedDatetime(Timestamp.valueOf(rs.getString("LAST_MODIFIED_DATETIME")));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return task;
		
	}
	@Override
	public String getThirdPartyEventId(String tapplenteventPkId, String tableName, String integrationAppCode,String eventFkId) {
	
	String SQL = "SELECT EXTERNAL_REFERENCE_TXT FROM "+tableName+" WHERE EXTERNAL_INTEGRATION_APP_CODE_FK_ID = ? AND "+eventFkId+" = x?;";
	List<Object> parameters = new ArrayList<>();
	parameters.add(integrationAppCode);
	parameters.add(tapplenteventPkId);
	ResultSet rs = executeSQL(SQL, parameters);
	String thirdPartyeventId = "";
	try{
		if(rs.next()){
			thirdPartyeventId = rs.getString("EXTERNAL_REFERENCE_TXT");
		}
	}catch(SQLException e){
		e.printStackTrace();
	}
	return thirdPartyeventId;
}
	@Override
	public List<String> getEventPkId(String noteSyncFkId,String tableName,String eventFkId,String noteSynkId) {
		String SQL = "SELECT "+eventFkId+" FROM "+tableName+" WHERE "+noteSynkId+"= x?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(noteSyncFkId);
		ResultSet rs = executeSQL(SQL, parameters);
		List<String> notePkIds = new ArrayList<String>();
		String notePkID = "";
		try {
			while(rs.next()){
				notePkID = convertByteToString(rs.getBytes(eventFkId));
				notePkIds.add(notePkID);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return notePkIds;
	}

	@Override
	public String getEventFkIdFromIntegration(String evernoteId,String tableName,String eventFkId) {
		String SQL = "SELECT "+eventFkId+" FROM "+tableName+" WHERE EXTERNAL_REFERENCE_TXT = ? AND IS_DELETED = FALSE;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(evernoteId);
		ResultSet rs = executeSQL(SQL, parameters);
		String tapplentEventId = "";
		try {
			if(rs.next()){
				tapplentEventId = convertByteToString(rs.getBytes(eventFkId));
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return tapplentEventId;
		
	}
	private String convertByteToString(byte[] input){
		if(input == null)
			return null;
		return Hex.encodeHexString(input).toUpperCase();
	}
	@Override
	public MeetingIntegration getIntegrationPkId(String tableName, String eventFkId,String intgPkId,String tapplentMeetingId) {
		String SQL = "SELECT * FROM "+tableName+" WHERE "+eventFkId+" = x?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(tapplentMeetingId);
		ResultSet rs = executeSQL(SQL, parameters);
		MeetingIntegration mtgIntg = null;
		try {
			if(rs.next()){
				mtgIntg = new MeetingIntegration();
				mtgIntg.setMtgIntgnPkId(convertByteToString(rs.getBytes(intgPkId)));
				mtgIntg.setDeleted(rs.getBoolean("IS_DELETED"));
				mtgIntg.setExternalIntegrationAppCodeFkId(rs.getString("EXTERNAL_INTEGRATION_APP_CODE_FK_ID"));
				mtgIntg.setLastModifiedDatetime(Timestamp.valueOf(rs.getString("LAST_MODIFIED_DATETIME")));
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return mtgIntg;
	}
	@Override
	public Map<String, Object> getSynchDetailsPkId(String tableName,String eventFkId ,String tapplentEventId,String synchDetailsPkID) {
		String SQL = "SELECT * FROM "+tableName+" WHERE "+eventFkId+" = x?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(tapplentEventId);
		ResultSet rs = executeSQL(SQL, parameters);
		String synchDetailsPkId = "";
		
		Map<String,Object> detailsMap = new HashMap<String,Object>();
		try{
			if(rs.next()){
				synchDetailsPkId =convertByteToString(rs.getBytes(synchDetailsPkID));
				Timestamp a = Timestamp.valueOf(rs.getString("LAST_MODIFIED_DATETIME"));
				detailsMap.put("synchDetailsPkId",synchDetailsPkId);
				detailsMap.put("lastModifiedDateTime", a);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return detailsMap;
		
	}
	@Override
	public List<MeetingInvitee> getMeetingInvitee(String meetingId) {
		List<MeetingInvitee> invitees = new ArrayList<>();
		
		
		String SQL = "SELECT * FROM T_PFM_EU_MEETING_INVITEE WHERE MEETING_FK_ID = x?;";
		 List<Object> parameters = new ArrayList<>();
			parameters.add(meetingId);
			ResultSet rs = executeSQL(SQL, parameters);
			MeetingInvitee invitee = null;
			try{
				while(rs.next()){
					invitee = new MeetingInvitee();
					invitee.setMeetingInviteePkId(convertByteToString(rs.getBytes("MEETING_INVITEE_PK_ID")));
					invitee.setInvitedPersonFkId(convertByteToString(rs.getBytes("INVITED_PERSON_FK_ID")));
					invitee.setAttendeeStatusCodeFkId(rs.getString("ATTENDEE_STATUS_CODE_FK_ID"));
					invitee.setDeleted(rs.getBoolean("IS_DELETED"));
					invitee.setLastModifiedDatetime(Timestamp.valueOf(rs.getString("LAST_MODIFIED_DATETIME")));
					invitees.add(invitee);
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			return invitees;
		
	}
	@Override
	public Map<String,String> getPersonInfo(String personId) {
		String SQL = "SELECT * FROM T_PRN_EU_PERSON WHERE PERSON_PK_ID = x?;";
		 List<Object> parameters = new ArrayList<>();
			parameters.add(personId);
			ResultSet rs = executeSQL(SQL, parameters);
			String personEmail = "";
			String personLocale = "";
			Map<String,String> perdonInfo = new HashMap<String,String>();
			try{
				if(rs.next()){
					personEmail = rs.getString("WORK_EMAIL_ID_TXT");
					perdonInfo.put("workEmail", personEmail);
					personLocale = rs.getString("LOCALE_CODE_FK_ID");
					perdonInfo.put("locale", personLocale);
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
		return perdonInfo;
	}
	@Override
	public String getAttendeeInfo(String attendeeEmail) {
		String SQL = "SELECT * FROM T_PRN_EU_PERSON WHERE WORK_EMAIL_ID_TXT = ?;";
		 List<Object> parameters = new ArrayList<>();
			parameters.add(attendeeEmail);
			ResultSet rs = executeSQL(SQL, parameters);
			String personId = "";
			try{
				if(rs.next()){
					personId =convertByteToString(rs.getBytes("PERSON_PK_ID"));
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			
		return personId;
	}
	@Override
	public List<NoteSubTask> getNoteSubTasks(String tapplentNoteId,String personId) {
		List<NoteSubTask> subTasks = new ArrayList<>();
		String SQL = "SELECT * FROM T_PFM_EU_NOTE_SUB_TASK WHERE NOTE_FK_ID = x?;";
		 List<Object> parameters = new ArrayList<>();
			parameters.add(tapplentNoteId);
			ResultSet rs = executeSQL(SQL, parameters);
			NoteSubTask subTask = null;
			try{
				while(rs.next()){
					subTask = new NoteSubTask();
					subTask.setNoteSubTaskPkId(convertByteToString(rs.getBytes("NOTE_SUB_TASK_PK_ID")));
					subTask.setNoteFkId(convertByteToString(rs.getBytes("NOTE_FK_ID")));
					subTask.setSubTaskDescG11nBigTxt(Util.getG11nValue(rs.getString("SUB_TASK_DESC_G11N_BIG_TXT"), personId));
					subTask.setComplete(rs.getBoolean("IS_COMPLETE"));
					subTask.setDeleted(rs.getBoolean("IS_DELETED"));
					subTask.setLastModifiedDatetime(Timestamp.valueOf(rs.getString("LAST_MODIFIED_DATETIME")));
					subTasks.add(subTask);
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			
			return subTasks;
		
	}
	@Override
	public Map<String, String> getIntegrationInfo(String everNoteId, String tableName, String notePkId) {
		String SQL = "SELECT * FROM "+tableName+" WHERE NOTE_FK_ID = x? AND EXTERNAL_REFERENCE_TXT = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(notePkId);
		parameters.add(everNoteId);
		ResultSet rs = executeSQL(SQL, parameters);
		Map<String,String> intgInfo = new HashMap<>();
		String lastModifiedTime = "";
		String intgPkId = "";
		try {
			if(rs.next()){
				try {
					lastModifiedTime = rs.getString("LAST_MODIFIED_DATETIME");
					intgPkId = rs.getString("NOTE_INTGN_PK_ID");
					intgInfo.put("IntgPkId",intgPkId);
					intgInfo.put("LastModifiedTime", lastModifiedTime);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return intgInfo;
	}
		
	}

