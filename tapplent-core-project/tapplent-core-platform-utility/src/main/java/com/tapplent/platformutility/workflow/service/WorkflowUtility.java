package com.tapplent.platformutility.workflow.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.insert.impl.ValidationError;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.workflow.dao.WorkflowDAO;
import com.tapplent.platformutility.workflow.structure.*;
import org.springframework.beans.BeanUtils;

import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.hierarchy.service.HierarchyService;
import com.tapplent.platformutility.insert.impl.InsertDAO;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.impl.SearchDAO;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

public class WorkflowUtility {
	
	private SearchDAO searchDAO;

    private InsertDAO insertDAO;

    private WorkflowDAO workflowDAO;

    private HierarchyService hierarchyService;

    private ValidationError validationError;

	public static boolean isExitCriteriaFulfilledForStepTransition(String exitCountCriteriaExpn) {
		if(exitCountCriteriaExpn == null)
			return true;
		//evaluate Exit criteria
		return false;
	}
	
	public List<WorkflowActor> resolveActors(GlobalVariables globalVariables, WorkflowStep currentWorkflowStep, Map<String, WorkflowStep> workflowSteps) throws SQLException {
        Map<String, WorkflowActorDefn> workflowActorDefnList = getWorkflowDefActors(globalVariables, currentWorkflowStep);//workflowActorId->workflowActorDefn
        Map<String, WorkflowActor> resolvedPersonIdActorIdActorMap = getResolvedPersonsFromActors(globalVariables, workflowActorDefnList);//personId_ActorId->workflowActor //not unique--> actor-person combination make it unique.
        Map<String, Map<String, WorkflowActionDefn>> resolvedPersonIdActorIdActionMap = getResolvedActionsFromActorPersons(globalVariables, resolvedPersonIdActorIdActorMap);//personId_actorId->actionPk->action FIXME resolve logic for uniqueness!
        List<WorkflowActor> resolvedPersonActors = updateStepActorTxn(globalVariables, resolvedPersonIdActorIdActorMap, resolvedPersonIdActorIdActionMap, currentWorkflowStep, workflowSteps);
        return resolvedPersonActors;
    }

	private Map<String, WorkflowActorDefn> getWorkflowDefActors(GlobalVariables globalVariables, WorkflowStep workflowStep) {
        String sql = "SELECT VERSION_ID, WORKFLOW_ACTOR_DEFN_PK_ID, WORKFLOW_STEP_DEFN_FK_ID, ACTOR_DISP_SEQ_NUM_POS_INT, ACTOR_NAME_G11N_BIG_TXT, " +
                "ACTOR_ICON_CODE_FK_ID, ACTOR_PERSON_GROUP_FK_ID, IS_RELATIVE_TO_LOGGED_IN_USER, IS_RELATIVE_TO_SUBJECT_USER, RELATIVE_TO_ACTOR_FK_ID, " +
                "RELATIVE_RELATIONSHIP_CODE_FK_ID, RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT, RESOLVE_ACTOR_AS_OF_DATETIME, IS_ADHOC_USER_ACTOR, " +
                "ALLOW_EXTERNAL_USER, NO_ACTORS_ACTION_CODE_FK_ID FROM T_PFM_ADM_WORKFLOW_ACTOR_DEFN WHERE WORKFLOW_STEP_DEFN_FK_ID = "+
                workflowStep.getWorkflowStepDefnFkId()+";";
        SearchResult sr = searchDAO.search(sql, new ArrayDeque<>());//FIXME use util's selectString
        Map<String, WorkflowActorDefn> actors = new HashMap<>();
        if(sr.getData() != null) {
            for(Map<String, Object> row : sr.getData()) {
                WorkflowActorDefn workflowActorDefn = new WorkflowActorDefn();
                workflowActorDefn.setWorkflowActorDefnPkId(Util.convertByteToString((byte[]) row.get("WORKFLOW_ACTOR_DEFN_PK_ID")));
                workflowActorDefn.setWorkflowStepDefnFkId(Util.convertByteToString((byte[]) row.get("WORKFLOW_STEP_DEFN_FK_ID")));
                workflowActorDefn.setActorDispSeqNumPosInt((int)row.get("ACTOR_DISP_SEQ_NUM_POS_INT"));
                workflowActorDefn.setActorNameG11nBigTxt(row.get("ACTOR_NAME_G11N_BIG_TXT").toString());
                workflowActorDefn.setActorIconCodeFkId((String) row.get("ACTOR_ICON_CODE_FK_ID"));
                workflowActorDefn.setActorPersonGroupFkId((String) row.get("ACTOR_PERSON_GROUP_FK_ID"));
                workflowActorDefn.setRelativeToLoggedInUser((boolean)row.get("IS_RELATIVE_TO_LOGGED_IN_USER"));
                workflowActorDefn.setRelativeToSubjectUser((boolean)row.get("IS_RELATIVE_TO_SUBJECT_USER"));
                workflowActorDefn.setRelativeToActorId((String) row.get("RELATIVE_TO_ACTOR_FK_ID"));
                workflowActorDefn.setRelativeRelationshipCodeFkId((String) row.get("RELATIVE_RELATIONSHIP_CODE_FK_ID"));
                workflowActorDefn.setRelativeRelationshipHierarchyLevelPosInt(row.get("RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT") == null ? 0 : (int) row.get("RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT"));
                workflowActorDefn.setResolveActorAsOfDatetime((Timestamp)row.get("RESOLVE_ACTOR_AS_OF_DATETIME"));
                workflowActorDefn.setAdhocUserActor(row.get("IS_ADHOC_USER_ACTOR") == null ? false : (boolean)row.get("IS_ADHOC_USER_ACTOR"));
                workflowActorDefn.setAllowExternalUser(row.get("ALLOW_EXTERNAL_USER") == null ? false : (boolean)row.get("ALLOW_EXTERNAL_USER"));
                workflowActorDefn.setNoActorsActionCodeFkId((String) row.get("NO_ACTORS_ACTION_CODE_FK_ID"));
                actors.put(workflowActorDefn.getWorkflowActorDefnPkId(), workflowActorDefn);
            }
        }
        return actors;
    }
	
	private  Map<String, WorkflowActor> getResolvedPersonsFromActors(GlobalVariables globalVariables, Map<String, WorkflowActorDefn> workflowActorDefnList) {
        Map<String, WorkflowActorDefn> contextualPersonIds = new HashMap<>();
        Map<String, WorkflowActor> resolvedPersonIdActorDefnIdActorMap = new HashMap<>();
        for(WorkflowActorDefn workflowActorDefn : workflowActorDefnList.values()){
            if(workflowActorDefn.isRelativeToLoggedInUser()){
                String loogedInPersonId = TenantContextHolder.getUserContext().getPersonId();
                contextualPersonIds.put(loogedInPersonId, workflowActorDefn);
//                resolvedPersonIdActorIdActorMap = getResolvedPersonsFromHierarchy(globalVariables, contextualPersonIds);
                getResolvedPersonsFromHierarchy(globalVariables, contextualPersonIds, resolvedPersonIdActorDefnIdActorMap);
            }
            else if (workflowActorDefn.isRelativeToSubjectUser()){
                String subjectUserId = TenantAwareCache.getMetaDataRepository().getSubjectUser(globalVariables.domainObject, globalVariables.model.getInsertEntityPK().getValue());
				contextualPersonIds.put(subjectUserId, workflowActorDefn);
				getResolvedPersonsFromHierarchy(globalVariables, contextualPersonIds, resolvedPersonIdActorDefnIdActorMap);
			}
            else if (workflowActorDefn.getActorPersonGroupFkId() != null){
                String personGroupId = workflowActorDefn.getActorPersonGroupFkId();
                contextualPersonIds = getpersonIdsFromGroupId(personGroupId, workflowActorDefn);
                getResolvedPersonsFromHierarchy(globalVariables, contextualPersonIds, resolvedPersonIdActorDefnIdActorMap);
            }
            else if (workflowActorDefn.getRelativeToActorId() != null){
                String relativeToActorId = workflowActorDefn.getRelativeToActorId();
                Map<String, WorkflowActorDefn> relativeWorkflowActorDefn = getWorkflowDefActorFromActorId(relativeToActorId);
//                if(relativeWorkflowActor.containsKey(relativeToActorId)){
//                    relativeWorkflowActor.put(relativeToActorId, relativeWorkflowActor.get(relativeToActorId));
//                }
//                else{
//                    relativeWorkflowActor = getWorkflowDefActorFromActorId(relativeToActorId);
//                }
//                Map<String, WorkflowActor> relativeContextualPersonIds = getResolvedPersonsFromActors(globalVariables, relativeWorkflowActorDefn);//this is WRONG!
                Map<String, WorkflowActor> relativeResolvedPersonIdActorIdActorMap = new HashMap<>();
                getResolvedPersonsFromHierarchy(globalVariables, relativeWorkflowActorDefn, relativeResolvedPersonIdActorIdActorMap);
                //change the value to absolute actors
                for(Entry<String, WorkflowActor> entry: relativeResolvedPersonIdActorIdActorMap.entrySet()){
//                    String[] entryKeys = entry.getKey().split("_");
                    entry.getKey().replace(relativeToActorId, workflowActorDefn.getWorkflowActorDefnPkId()); //replaces key's value from A_B to A_C where 'A' is PersonId, 'B' is relativeActorDefnId and 'C' is absoluteActorDefnId
                	BeanUtils.copyProperties(workflowActorDefn, entry.getValue()); //replaces value from relativeActorDefn to absoluteActorDefn.
//                    entry.setValue(workflowActorDefn);
                }
                resolvedPersonIdActorDefnIdActorMap.putAll(relativeResolvedPersonIdActorIdActorMap);
            }
            else if (workflowActorDefn.isAdhocUserActor()){
                //formulate & code for ad-hoc
            }
        }
        return resolvedPersonIdActorDefnIdActorMap;
    }
	
	 private Map<String,Map<String, WorkflowActionDefn>> getResolvedActionsFromActorPersons(GlobalVariables globalVariables, Map<String, WorkflowActor> resolvedPersonIdActorIdActorMap) {
	        Map<String, Map<String, WorkflowActionDefn>> actorToActorActionMap = new HashMap<>();//actorId->actionPk->action
	        Map<String, Map<String, WorkflowActionDefn>> resolvedPersonToActionMap =  new HashMap<>();//personId->actionPk->action
	        String sql = "SELECT * FROM T_PFM_ADM_WORKFLOW_ACTION_DEFN WHERE WORKFLOW_DEFINITION_FK_ID = "+
	                globalVariables.workflowDetails.workflowDefId+" AND " +
	                "SOURCE_ACTOR_FK_ID = ? ;";
	        for(Entry<String, WorkflowActor> entry : resolvedPersonIdActorIdActorMap.entrySet()) {
	            String actorId = entry.getValue().getStepActorDefnFkId();
	            if (actorToActorActionMap.get(actorId) == null) {
	                Deque<Object> parameter = new ArrayDeque<>();
	                parameter.add(Util.convertStringIdToByteId(actorId));
	                SearchResult sr = searchDAO.search(sql, parameter);
	                if (sr.getData() != null) {
	                    List<Map<String, Object>> rows = sr.getData();
	                    Map<String, WorkflowActionDefn> workflowActorActionMap = new HashMap<>();//actionId->action
	                    for (Map<String, Object> row : rows) {
	                        WorkflowActionDefn workflowActionDefn = new WorkflowActionDefn();
	                        workflowActionDefn.setVersionId(Util.convertByteToString((byte[])row.get("VERSION_ID")));
	                        workflowActionDefn.setWorkflowActionDefnPkId(Util.convertByteToString((byte[])row.get("WORKFLOW_ACTION_DEFN_PK_ID")));
	                        workflowActionDefn.setWorkflowDefinitionFkId(Util.convertByteToString((byte[])row.get("WORKFLOW_DEFINITION_FK_ID")));
	                        workflowActionDefn.setActorActionDispSeqNumPosInt((int) row.get("ACTION_DISP_SEQ_NUM_POS_INT"));
	                        workflowActionDefn.setActionMasterCodeFkId((String)row.get("ACTN_VISUAL_MASTER_CODE_FK_ID"));
	                        workflowActionDefn.setSourceActorFkId(Util.convertByteToString((byte[])row.get("SOURCE_ACTOR_FK_ID")));
	                        workflowActionDefn.setTargetActorFkId(Util.convertByteToString((byte[])row.get("TARGET_ACTOR_FK_ID")));
	                        workflowActionDefn.setTargetStepFkId(Util.convertByteToString((byte[]) row.get("TARGET_STEP_FK_ID")));
	                        workflowActionDefn.setIfActionConditionExpn((String) row.get("IF_ACTION_CONDITION_EXPN"));
	                        workflowActionDefn.setActionToDlftExecOnForcedMove((boolean) row.get("IS_ACTION_TO_DFLT_EXEC_ON_FORCED_MOVE"));
	                        workflowActionDefn.setActionDelegateable((boolean) row.get("IS_ACTION_DELEGATEABLE"));
	                        workflowActionDefn.setAutoExecIfActorEqualsSubject((boolean) row.get("IS_AUTO_EXEC_IF_ACTOR_EQUALS_SUBJECT"));
	                        workflowActionDefn.setAutoExecuteIfActorEqualToInitiator((boolean) row.get("IS_AUTO_EXECUTE_IF_ACTOR_EQUAL_TO_INITIATOR"));
	                        workflowActionDefn.setAutoExecuteIfDuplicate((boolean) row.get("IS_AUTO_EXECUTE_IF_DUPLICATE"));
	                        workflowActionDefn.setActionCommentMandatory((boolean) row.get("IS_ACTION_COMMENT_MANDATORY"));
	                        workflowActionDefn.setExitCountCriteriaExpn((String) row.get("EXIT_COUNT_CRITERIA_EXPN"));
	                        workflowActionDefn.setCrtriaCntrlgSrcStepExit(row.get("IS_CRTRIA_CNTRLG_SRC_STEP_EXIT") == null ? false : (boolean) row.get("IS_CRTRIA_CNTRLG_SRC_STEP_EXIT"));
	                        workflowActionDefn.setCrtriaCntrlgTgtStepEntryVsblty(row.get("IS_CRTRIA_CNTRLG_TGT_STEP_ENTRY_VSBLTY") == null ? false : (boolean) row.get("IS_CRTRIA_CNTRLG_TGT_STEP_ENTRY_VSBLTY"));
	                        workflowActionDefn.setActionExitSrcActNtfnId((String)row.get("ACTION_EXIT_SRC_ACT_NTFN_ID"));
	                        workflowActionDefn.setActionExitTrgtActNtfnId((String)(row.get("ACTION_EXIT_TRGT_ACT_NTFN_ID")));
	                        workflowActorActionMap.put(workflowActionDefn.getWorkflowActionDefnPkId(), workflowActionDefn);
	                    }
	                    resolvedPersonToActionMap.put(entry.getKey(), workflowActorActionMap);
	                    actorToActorActionMap.put(actorId, workflowActorActionMap);
	                }
	            }
	            else{
	                resolvedPersonToActionMap.put(entry.getKey(), actorToActorActionMap.get(actorId));
	            }
	        }
	        return  resolvedPersonToActionMap;
	    }
	 
	 private List<WorkflowActor> updateStepActorTxn(GlobalVariables globalVariables,
													Map<String, WorkflowActor> resolvedPersonIdActorIdActorMap,
													Map<String, Map<String, WorkflowActionDefn>> resolvedPersonIdActorIdActionMap,
													WorkflowStep currentWorkflowStep,
													Map<String, WorkflowStep> workflowSteps) throws SQLException {
		List<WorkflowActor> personActors = new ArrayList<>();
		StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR (WORKFLOW_TXN_STEP_ACTOR_PK_ID, WORKFLOW_TXN_STEP_FK_ID, STEP_ACTOR_DEFN_FK_ID, RESOLVED_PERSON_FK_ID, "
					+ "STEP_ACTOR_STATUS_CODE_FK_ID, IS_DELETED, CREATED_DATETIME, LAST_MODIFIED_DATETIME) VALUES ");

		for (Entry<String, WorkflowActor> resolvedPersonIdActorIdActor : resolvedPersonIdActorIdActorMap.entrySet()) {
			String[] keys = resolvedPersonIdActorIdActor.getKey().split("_");
			String personId = keys[0];
			WorkflowActor actor = resolvedPersonIdActorIdActor.getValue();
			actor.setWorkflowTxnStepActorPkId(Common.getUUID());
			sql.append("("+actor.getWorkflowTxnStepActorPkId() + ", ");
			sql.append(currentWorkflowStep.getWorkflowTxnStepPkId() + ", ");
			sql.append(actor.getStepActorDefnFkId() + ", ");
			sql.append(personId + ", ");
			sql.append("'NOT_ACTED', false, '" + globalVariables.currentTimestampString + "', '" + globalVariables.currentTimestampString + "'), ");
			updateActionTxn(globalVariables, resolvedPersonIdActorIdActionMap.get(resolvedPersonIdActorIdActor.getKey()), currentWorkflowStep, workflowSteps, actor);//fixme
			personActors.add(actor);
		}
		sql.replace(sql.length() - 2, sql.length(), ";");
		insertDAO.insertRow(sql.toString());
		return personActors;
		}
	 
	 private void updateActionTxn(GlobalVariables globalVariables,
								  Map<String, WorkflowActionDefn> resolvedPersonIdActionMap,
								  WorkflowStep workflowStep,
								  Map<String, WorkflowStep> workflowSteps,
								  WorkflowActor actor) throws SQLException {
	    	StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION (WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID, WORKFLOW_TXN_STEP_ACTOR_FK_ID, ACTN_VISUAL_MASTER_CODE_FK_ID, " +
	                "WORKFLOW_ACTION_DEFN_FK_ID, TARGET_WFLOW_TXN_STEP_FK_ID, TARGET_STEP_ACTOR_DEFN_FK_ID, IF_ACTION_CONDITION_EXPN, " +
	                "IS_COMMENT_MANDATORY_FLAG, IS_ACTION_VISIBLE_THRU_EXIT_CRIT_EVLN, IS_DELETED, CREATED_DATETIME, LAST_MODIFIED_DATETIME) VALUES ");
	    	boolean defaultStepActionVisibilty;
//	    	for (Entry<String, Map<String, WorkflowActionDefn>> actionEntryMap : resolvedPersonIdActionMap.entrySet()) {
			for(Entry<String, WorkflowActionDefn> actionEntry :resolvedPersonIdActionMap.entrySet()){
				WorkflowActionDefn action = actionEntry.getValue();
				String targetStepDefnId = action.getTargetStepFkId();
				String targetStepTxnId = null;
				if (targetStepDefnId != null)
					targetStepTxnId = workflowSteps.get(targetStepDefnId).getWorkflowTxnStepPkId();
				defaultStepActionVisibilty = isActionVisibleByDefalut(action);//defn should ensure that actor-specific-actions don't have exit_criteria_applicable.
				sql.append("("+Common.getUUID() + ", ");
				sql.append(actor.getWorkflowTxnStepActorPkId() + ", ");
				sql.append("'"+action.getActionMasterCodeFkId() + "', ");
				sql.append(action.getWorkflowActionDefnPkId() + ", ");
				sql.append( targetStepTxnId + ", ");
				sql.append( action.getTargetActorFkId() + ", ");
				sql.append(action.getIfActionConditionExpn() == null ? "NULL, " : "'" + action.getIfActionConditionExpn() + "', ");
				sql.append(action.isActionCommentMandatory() + ", ");
				sql.append(defaultStepActionVisibilty + ", ");
				sql.append("false, now(), now() ");
				sql.append("), ");
			}
//	        }
	        sql.replace(sql.length() - 2, sql.length(), ";");
		 	insertDAO.insertRow(sql.toString());
		}
	 
	 private static boolean isActionVisibleByDefalut(WorkflowActionDefn action) {
			if(action.isExitCriteriaCheckApplicable()){
				if(WorkflowUtility.isExitCriteriaFulfilledForStepTransition(action.getExitCountCriteriaExpn())){
					return true;
//					if(action.isCrtriaCntrlgSrcStepExit())
//						return false;//exit_criteria should be evaluated here? 
//					else if(action.isCrtriaCntrlgTgtStepEntryVsblty())
//						return true;
				}
				else return false;
			}
			return true;
		}
	 
	 private Map<String,WorkflowActor> getResolvedPersonsFromHierarchy(GlobalVariables globalVariables, Map<String, WorkflowActorDefn> contextualPersonIds, Map<String, WorkflowActor> resolvedPersonIdActorDefnIdActorMap) {
			for(Entry<String, WorkflowActorDefn> contextualPerson : contextualPersonIds.entrySet()){
	            WorkflowActorDefn actor = contextualPerson.getValue();
	            String relationshipCode = actor.getRelativeRelationshipCodeFkId();
				int level = actor.getRelativeRelationshipHierarchyLevelPosInt();
				if (relationshipCode.equals("SELF")){
					String relatedPersonId = contextualPerson.getKey();
					WorkflowActor workflowActor = WorkflowActor.fromDefinition(contextualPerson.getValue());
	                workflowActor.setResolvedPersonFkId(relatedPersonId);
	                resolvedPersonIdActorDefnIdActorMap.put(relatedPersonId+"_"+actor.getWorkflowActorDefnPkId(), workflowActor);
	                continue;
				}
				Map<String, Map<Integer, List<String>>> personAllRelations = hierarchyService.getAllRelationsForPerson(contextualPerson.getKey());
				Map<Integer, List<String>> relationshipCodeMap = personAllRelations.get(relationshipCode);
				if (relationshipCodeMap != null) {
					List<String> relatedPersonIdList = relationshipCodeMap.get(level);
					for (String relatedPersonId : relatedPersonIdList){
						WorkflowActor workflowActor = new WorkflowActor();
						workflowActor.setResolvedPersonFkId(relatedPersonId);
						workflowActor.setStepActorDefnFkId(contextualPerson.getValue().getWorkflowActorDefnPkId());
						BeanUtils.copyProperties(contextualPerson.getValue(), workflowActor);
						resolvedPersonIdActorDefnIdActorMap.put(relatedPersonId+"_"+actor.getWorkflowActorDefnPkId(), workflowActor);
					}
				}else {//fixme optimize
					WorkflowActor workflowActor = new WorkflowActor();
					workflowActor.setStepActorDefnFkId(contextualPerson.getValue().getWorkflowActorDefnPkId());
					BeanUtils.copyProperties(contextualPerson.getValue(), workflowActor);
					Person systemDummyPerson = TenantAwareCache.getPersonRepository().getSystemDummyPersonDetails("WorkflowAdministrator");
					String personId = systemDummyPerson.getPersonPkId();
					resolvedPersonIdActorDefnIdActorMap.put(personId+"_"+actor.getWorkflowActorDefnPkId(), workflowActor);
				}
//				Map<String, Node> resolvedNodes = null;
//	            EntityMetadataVOX doMeta = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(globalVariables.baseTemplateId,
//	                    globalVariables.mtPE, globalVariables.btPE);
//	            if(actor.getRelativeRelationshipCodeFkId().equals("DIRECT_MANAGER") && actor.getResolveActorAsOfDatetime() == null)
//	                resolvedNodes = hierarchyService.getNthLevelParent(contextualPerson.getKey(), doMeta, level);
//	            else if(actor.getRelativeRelationshipCodeFkId().equals("DIRECT_REPORT") && actor.getResolveActorAsOfDatetime() == null)
//	                resolvedNodes = hierarchyService.getAllChildrenUptoNlevel(contextualPerson.getKey(), doMeta, level);
//	            else{
////	                 get 1st level relationship from another relationship table.
////	                 get the remaining nodes/node from the direct approach.
////	                 resolveActorAsOfDatetime.
//	            }
//	            for(Entry<String, Node> entry : resolvedNodes.entrySet()){
//	                String personId = entry.getValue().getRow().get("PersonWorkRelationship.FromPerson").toString();//FIXME for indirect relations. different DO!
//	                WorkflowActor workflowActor = new WorkflowActor();
//	                workflowActor.setResolvedPersonFkId(personId);
//	                BeanUtils.copyProperties(contextualPerson.getValue(), workflowActor);
//	                resolvedPersonIdActorDefnIdActorMap.put(personId+"_"+actor.getWorkflowActorDefnPkId(), workflowActor);
//	            }
	        }
	        return resolvedPersonIdActorDefnIdActorMap;
	    }
	 private Map<String,WorkflowActorDefn> getWorkflowDefActorFromActorId(String relativeToActorId) {
	        String sql = "SELECT VERSION_ID, WORKFLOW_ACTOR_DEFN_PK_ID, WORKFLOW_STEP_DEFN_FK_ID, ACTOR_DISP_SEQ_NUM_POS_INT, ACTOR_NAME_G11N_Big_TXT, " +
	                "ACTOR_ICON_CODE_FK_ID, ACTOR_PERSON_GROUP_FK_ID, IS_RELATIVE_TO_LOGGED_IN_USER, IS_RELATIVE_TO_SUBJECT_USER, RELATIVE_TO_ACTOR_ID, " +
	                "RELATIVE_RELATIONSHIP_CODE_FK_ID, RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT, RESOLVE_ACTOR_AS_OF_DATETIME, IS_ADHOC_USER_ACTOR, " +
	                "ALLOW_EXTERNAL_USER, NO_ACTORS_ACTION_CODE_FK_ID FROM T_PFM_ADM_WORKFLOW_ACTOR WHERE WORKFLOW_ACTOR_PK_ID = "+
	                relativeToActorId+";";
	        SearchResult sr = searchDAO.search(sql, null);//FIXME use util's selectString
	        Map<String, WorkflowActorDefn> actorDefn = new HashMap<>();
	        if(sr.getData() != null) {
	            Map<String, Object> row = sr.getData().get(0);
	            WorkflowActorDefn workflowActorDefn = new WorkflowActorDefn();
	            workflowActorDefn.setWorkflowActorDefnPkId(row.get("WORKFLOW_ACTOR_PK_ID").toString());
	            workflowActorDefn.setWorkflowStepDefnFkId(row.get("WORKFLOW_STEP_DEP_FK_ID").toString());
	            workflowActorDefn.setActorDispSeqNumPosInt((int)row.get("ACTOR_DISP_SEQ_NUM_POS_INT"));
	            workflowActorDefn.setActorNameG11nBigTxt(row.get("ACTOR_NAME_G11N_BLOB").toString());
	            workflowActorDefn.setActorIconCodeFkId(row.get("ACTOR_ICON_CODE_FK_ID").toString());
	            workflowActorDefn.setActorPersonGroupFkId(row.get("ACTOR_PERSON_GROUP_FK_ID").toString());
	            workflowActorDefn.setRelativeToLoggedInUser((boolean)row.get("IS_RELATIVE_TO_LOGGED_IN_USER"));
	            workflowActorDefn.setRelativeToSubjectUser((boolean)row.get("IS_RELATIVE_TO_SUBJECT_USER"));
	            workflowActorDefn.setRelativeToActorId(row.get("RELATIVE_TO_ACTOR_ID").toString());
	            workflowActorDefn.setRelativeRelationshipCodeFkId(row.get("RELATIVE_RELATIONSHIP_CODE_FK_ID").toString());
	            workflowActorDefn.setRelativeRelationshipHierarchyLevelPosInt((int)row.get("RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT"));
	            workflowActorDefn.setResolveActorAsOfDatetime((Timestamp)row.get("RESOLVE_ACTOR_AS_OF_DATETIME"));
	            workflowActorDefn.setAdhocUserActor((boolean)row.get("IS_ADHOC_USER_ACTOR"));
	            workflowActorDefn.setAllowExternalUser((boolean)row.get("ALLOW_EXTERNAL_USER"));
	            workflowActorDefn.setNoActorsActionCodeFkId(row.get("NO_ACTORS_ACTION_CODE_FK_ID").toString());
	            actorDefn.put(workflowActorDefn.getWorkflowActorDefnPkId(), workflowActorDefn);
	        }
	        return actorDefn;
	    }
	 
	 private Map<String, WorkflowActorDefn> getpersonIdsFromGroupId(String personGroupId, WorkflowActorDefn workflowActorDefn) {
	        Map<String, WorkflowActorDefn> contextualPersonIds = null;
	        String sql = "SELECT GROUP_MEMBER_PERSON_FK_ID FROM T_PFM_IEU_GROUP_MEMBER WHERE PFM_GROUP_DEP_FK_ID = "+personGroupId+";";
	        SearchResult sr = searchDAO.search(sql, null);
	        if(sr.getData() != null) {
	            contextualPersonIds = new HashMap<>();
	            for (Map<String, Object> row : sr.getData()) {
	                contextualPersonIds.put(row.get("GROUP_MEMBER_PERSON_FK_ID").toString(), workflowActorDefn);
	            }
	        }
	        return contextualPersonIds;
	    }

	public void updateWorkflowDetailsInUnderlyingRecord(GlobalVariables globalVariables) throws SQLException {
		WorkflowDetails workflowDetails = globalVariables.workflowDetails;
		EntityMetadataVOX entityMetadataVOX = globalVariables.entityMetadataVOX;
		Map<String, Object> data = globalVariables.insertData.getData();
		String tableName = entityMetadataVOX.getDbTableName();
		String pkColumnName = entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName();
		String versionColumnName = entityMetadataVOX.getVersoinIdAttribute().getDbColumnName();
		String sql = "UPDATE "+tableName+" SET REF_WORKFLOW_DEFN_FK_ID = "+workflowDetails.workflowDefId+", " +
				"REF_WORKFLOW_TXN_FK_ID = "+workflowDetails.workflowTxnId+", " +
				"REF_WORKFLOW_TXN_STATUS_CODE_FK_ID = '"+workflowDetails.workflowStatus+"', " +
				"REF_WORKFLOW_TXN_STEP_FK_ID = "+workflowDetails.workflowTxnStepId+", " +
				"REF_WORKFLOW_TXN_STEP_STATUS_CODE_FK_ID = '"+workflowDetails.workflowTxnStepStatus +"' "+
				"WHERE "+pkColumnName+" = "+data.get(entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode())+" AND " +
				versionColumnName+" = "+data.get(entityMetadataVOX.getVersoinIdAttribute().getDoAttributeCode())+";";
		insertDAO.insertRow(sql);
	}

	public SearchDAO getSearchDAO() {
		return searchDAO;
	}

	public void setSearchDAO(SearchDAO searchDAO) {
		this.searchDAO = searchDAO;
	}

	public HierarchyService getHierarchyService() {
		return hierarchyService;
	}

	public void setHierarchyService(HierarchyService hierarchyService) {
		this.hierarchyService = hierarchyService;
	}

	public InsertDAO getInsertDAO() {
		return insertDAO;
	}

	public void setInsertDAO(InsertDAO insertDAO) {
		this.insertDAO = insertDAO;
	}

	public ValidationError getValidationError() {
		return validationError;
	}

	public void setValidationError(ValidationError validationError) {
		this.validationError = validationError;
	}

	public void updateActorActionStatus(String workflowTxnActorPossibleActionPkId) throws SQLException {
		String sql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION SET IS_ACTED = TRUE WHERE WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID = x?";
		Deque<Object> parameter = new ArrayDeque<>();
		parameter.add(Util.remove0x(workflowTxnActorPossibleActionPkId));
		insertDAO.insertRowWithParameter(sql, parameter);
	}

	public void updatePersonActorStatus(String workflowTxnStepActorFkId, String resolvedPersonFkId, boolean isCompleted) throws SQLException {
		Person systemDummyPerson = TenantAwareCache.getPersonRepository().getSystemDummyPersonDetails("WorkflowAdministrator");
		String systemDummyPersonId = systemDummyPerson.getPersonPkId();
		String sql = null;
		if (systemDummyPersonId.equals(resolvedPersonFkId))
			sql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR SET STEP_ACTOR_STATUS_CODE_FK_ID = 'SKIPPED' WHERE WORKFLOW_TXN_STEP_ACTOR_PK_ID = x?";
		else if (isCompleted) sql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR SET STEP_ACTOR_STATUS_CODE_FK_ID = 'COMPLETED' WHERE WORKFLOW_TXN_STEP_ACTOR_PK_ID = x?";
		else sql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR SET STEP_ACTOR_STATUS_CODE_FK_ID = 'ACTED' WHERE WORKFLOW_TXN_STEP_ACTOR_PK_ID = x?";
		Deque<Object> parameter = new ArrayDeque<>();
		parameter.add(Util.remove0x(workflowTxnStepActorFkId));
		insertDAO.insertRowWithParameter(sql, parameter);
	}

	public void updateWorkflowTXNStatus(String workflowStatus, String workflowTxnId) throws SQLException {
		String sql = "UPDATE T_PFM_IEU_WORKFLOW_TXN SET WORKFLOW_STATUS_CODE_FK_ID = ? WHERE WORKFLOW_TXN_PK_ID = x?";
		Deque<Object> parameter = new ArrayDeque<>();
		parameter.add(workflowStatus);
		parameter.add(Util.remove0x(workflowTxnId));
		insertDAO.insertRowWithParameter(sql, parameter);
	}

	public void updateStepTXNStatus(String sourceStepId, String sourceStepStatus, String targetStepId, String targetStepStatus) throws SQLException {
		String cSql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_STEP SET STEP_STATUS_CODE_FK_ID = '"+sourceStepStatus+"' WHERE WORKFLOW_TXN_STEP_PK_ID = x?;";
		Deque<Object> parameter = new ArrayDeque<>();
		parameter.add(Util.remove0x(sourceStepId));
		insertDAO.insertRowWithParameter(cSql, parameter);
		if (targetStepId != null && targetStepStatus != null) {
			parameter.clear();
			parameter.add(Util.remove0x(targetStepId));
			String nSql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_STEP SET STEP_STATUS_CODE_FK_ID = '"+targetStepStatus+"' WHERE WORKFLOW_TXN_STEP_PK_ID = x?;";
			insertDAO.insertRowWithParameter(nSql, parameter);
		}
	}

	public List<WorkflowActor> getResolvedPersonActorByStep(WorkflowStep sourceWorkflowStep, GlobalVariables globalVariables, Map<String, WorkflowStep> sourceAndTargetSteps) throws SQLException {
		WorkflowStep targetWorkflowStep = sourceAndTargetSteps.get("TARGET_STEP");
		List<WorkflowActor> actorList = null;
		String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR A JOIN T_PFM_ADM_WORKFLOW_ACTOR_DEFN B ON A.STEP_ACTOR_DEFN_FK_ID = B.WORKFLOW_ACTOR_DEFN_PK_ID WHERE WORKFLOW_TXN_STEP_FK_ID = x? ;";
		List<Object> parameter = new ArrayList<>();
		parameter.add(Util.remove0x(sourceWorkflowStep.getWorkflowTxnStepPkId()));
		ResultSet rs = workflowDAO.searchResultSet(sql, parameter);
		if (rs.next()){
			actorList = new ArrayList<>();
			do {
				WorkflowActor actor = workflowDAO.getresolvedPersonActorFromResultSet(rs);
				actorList.add(actor);
			}while (rs.next());
		}else if(!targetWorkflowStep.isStartDateEnforced() || targetWorkflowStep.getStepActualStartDatetime().before(globalVariables.currentTimestamp)){
			Map<String, WorkflowStep> targetWorkflowStepMap = new HashMap<>();
			targetWorkflowStepMap.put(targetWorkflowStep.getWorkflowStepDefnFkId(), targetWorkflowStep);
			actorList = resolveActors(globalVariables, sourceWorkflowStep, targetWorkflowStepMap);//needed as next step is not resolved then how the action log can be maintained.
		}//else returning null meaning actor resolution date is enforced and has not yet come.
		return actorList;
	}

	public WorkflowDAO getWorkflowDAO() {
		return workflowDAO;
	}

	public void setWorkflowDAO(WorkflowDAO workflowDAO) {
		this.workflowDAO = workflowDAO;
	}

	public static boolean isExitCriteriaFulfilledForActionVisibility(String exitCountCriteriaForActionVisibilityExpn) {
		if(exitCountCriteriaForActionVisibilityExpn == null)
			return true;
		//evaluate Exit criteria
		return false;
	}

    public Map<String, Map<Integer,WorkflowActionPermissionDefinition>> getPermissionDefinitionForActorPerson(String workflowTxnId, String loggedInPersonId) throws SQLException {
		Map<String, Map<Integer,WorkflowActionPermissionDefinition>> permissionForActorPerson = getPermissionDefinitionForExistingActorPerson(workflowTxnId, loggedInPersonId);
		if (permissionForActorPerson == null || permissionForActorPerson.isEmpty()){
			permissionForActorPerson = getPermissionDefinitionForNonExistingActorPerson(workflowTxnId);
		}
		return permissionForActorPerson;
    }

	private Map<String, Map<Integer,WorkflowActionPermissionDefinition>> getPermissionDefinitionForNonExistingActorPerson(String workflowTxnId) throws SQLException {
		Map<String, Map<Integer,WorkflowActionPermissionDefinition>> permissionMap = null;
		String sql = "SELECT * FROM T_PFM_ADM_WORKFLOW_ACTION_PERMN_DEFN P INNER JOIN T_PFM_IEU_WORKFLOW_TXN T ON P.WORKFLOW_DEFINITION_FK_ID = T.WORKFLOW_DEFINITION_FK_ID\n" +
					"WHERE T.WORKFLOW_TXN_PK_ID = x? AND P.WORKFLOW_ACTOR_DEFINITION_FK_ID IS NULL;";
		List<Object> params = new ArrayList<>();
		params.add(workflowTxnId);
		ResultSet rs = workflowDAO.searchResultSet(sql, params);
		if (rs != null && rs.next()){
			permissionMap = new HashMap<>();
			Map<Integer, WorkflowActionPermissionDefinition> priorityToPermissionMap = new HashMap<>();
			do {
				WorkflowActionPermissionDefinition permDef = getWorkflowActionPermissionDefinition(rs);
				priorityToPermissionMap.put(permDef.getPermissionPriorityPosition(), permDef);
			}while (rs.next());
			permissionMap.put("NO_ACTOR", priorityToPermissionMap);
		}
		return permissionMap;
	}

	private WorkflowActionPermissionDefinition getWorkflowActionPermissionDefinition(ResultSet rs) throws SQLException {
		WorkflowActionPermissionDefinition permDef = new WorkflowActionPermissionDefinition();
		permDef.setWorkflowActionPermDefPkId(Util.convertByteToString(rs.getBytes("P.WORKFLOW_ACTION_PERMN_DEFN_PK_ID")));
		permDef.setWorkflowDefinitionId(Util.convertByteToString(rs.getBytes("P.WORKFLOW_DEFINITION_FK_ID")));
		permDef.setWorkflowActorDefinitionId(Util.convertByteToString(rs.getBytes("P.WORKFLOW_ACTOR_DEFINITION_FK_ID")));
		permDef.setWorkflowActionedExpn(rs.getString("P.WORKFLOW_ACTIONED_EXPN"));
		permDef.setPermissionPriorityPosition(rs.getInt("P.PERMISSION_PRIORITY_POS_INT"));
		return permDef;
	}

	private Map<String, Map<Integer,WorkflowActionPermissionDefinition>> getPermissionDefinitionForExistingActorPerson(String workflowTxnId, String loggedInPersonId) throws SQLException {
		Map<String, Map<Integer,WorkflowActionPermissionDefinition>> permissionMap = null;
		String sql = "SELECT * FROM T_PFM_ADM_WORKFLOW_ACTION_PERMN_DEFN P INNER JOIN T_PFM_IEU_WORKFLOW_TXN T ON P.WORKFLOW_DEFINITION_FK_ID = T.WORKFLOW_DEFINITION_FK_ID\n" +
				"INNER JOIN T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR A ON A.STEP_ACTOR_DEFN_FK_ID = P.WORKFLOW_ACTOR_DEFINITION_FK_ID\n" +
				"WHERE T.WORKFLOW_TXN_PK_ID = x? AND A.RESOLVED_PERSON_FK_ID = x?;";
		List<Object> params = new ArrayList<>();
		params.add(Util.remove0x(workflowTxnId));
		params.add(Util.remove0x(loggedInPersonId));
		ResultSet rs = workflowDAO.searchResultSet(sql, params);
		if (rs != null && rs.next()){
			permissionMap = new HashMap<>();
			do {
				WorkflowActionPermissionDefinition permDef = getWorkflowActionPermissionDefinition(rs);
				if (permissionMap.containsKey(permDef.getWorkflowActorDefinitionId())) {
					permissionMap.get(permDef.getWorkflowActorDefinitionId()).put(permDef.getPermissionPriorityPosition(), permDef);
				}
				else {
					Map<Integer, WorkflowActionPermissionDefinition> priorityToPermissionMap = new HashMap<>();
					priorityToPermissionMap.put(permDef.getPermissionPriorityPosition(), permDef);
					permissionMap.put(permDef.getWorkflowActorDefinitionId(), priorityToPermissionMap);
				}
			}while (rs.next());
		}
		return permissionMap;
	}

	public Map<String,String> getWorkflowActionToIsActionedMap(String workflowTxnId) throws SQLException {
		Map<String, String> workflowActionToIsActionedMap = null;
		String sql = "SELECT WORKFLOW_ACTION_DEFN_FK_ID, IS_ACTED FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION JOIN T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR ON WORKFLOW_TXN_STEP_ACTOR_PK_ID = WORKFLOW_TXN_STEP_ACTOR_FK_ID\n" +
				"JOIN T_PFM_IEU_WORKFLOW_TXN_STEP ON WORKFLOW_TXN_STEP_PK_ID = WORKFLOW_TXN_STEP_FK_ID WHERE WORKFLOW_TXN_FK_ID = x?;";
		List<Object> params = new ArrayList<>();
		params.add(Util.remove0x(workflowTxnId));
		ResultSet rs = workflowDAO.searchResultSet(sql, params);
		if (rs != null && rs.next()){
			workflowActionToIsActionedMap = new HashMap<>();
			do {
				String workflowActionDefId = Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_DEFN_FK_ID"));
				boolean isActed = rs.getBoolean("IS_ACTED");
				String isActedString = (isActed) ? "1" : "0";
				if (workflowActionToIsActionedMap.containsKey(workflowActionDefId)) {
					if (workflowActionToIsActionedMap.get(workflowActionDefId).equals("0"))
						workflowActionToIsActionedMap.put(workflowActionDefId, isActedString);
				}
				else workflowActionToIsActionedMap.put(workflowActionDefId, isActedString);
			}while (rs.next());
		}
		return workflowActionToIsActionedMap;
	}

	public Map<String, Map<String, ActorActionPermission>> getActorActionPermissions(String workflowActionPermDefPkId) throws SQLException {
		Map<String, Map<String, ActorActionPermission>> mtPEToActionPermissionMap = new HashMap<>();
		String sql = "SELECT * FROM T_PFM_ADM_WORKFLOW_ACTION_VIS_ACTN_PERMN_DEFN WHERE WORKFLOW_ACTION_PERMN_DEFN_FK_ID = x? ;";
		List<Object> params = new ArrayList<>();
		params.add(Util.remove0x(workflowActionPermDefPkId));
		ResultSet rs = workflowDAO.searchResultSet(sql, params);
		while (rs.next()){
			ActorActionPermission actorActionPermission = new ActorActionPermission();
			actorActionPermission.setActionPermissionPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_VIS_ACTN_PERMN_DEFN_PK_ID")));
			actorActionPermission.setActionPermissionDefnId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_PERMN_DEFN_FK_ID")));
			actorActionPermission.setMtPE(rs.getString("MT_PROCESS_ELEMENT_CODE_FK_ID"));
			actorActionPermission.setActionVisualMasterCode(rs.getString("ACTION_VISUAL_MASTER_CODE_FK_ID"));
			actorActionPermission.setActionApplicable(rs.getBoolean("IS_ACTION_APPLICABLE"));
			actorActionPermission.setRecordStatePermission(rs.getString("RECORD_STATE_CODE_FK_ID"));
			if (mtPEToActionPermissionMap.containsKey(actorActionPermission.getMtPE())){
				mtPEToActionPermissionMap.get(actorActionPermission.getMtPE()).put(actorActionPermission.getActionVisualMasterCode(), actorActionPermission);
			}else {
				Map<String, ActorActionPermission> actionPermissionMap = new HashMap<>();
				actionPermissionMap.put(actorActionPermission.getActionVisualMasterCode(), actorActionPermission);
				mtPEToActionPermissionMap.put(actorActionPermission.getMtPE(), actionPermissionMap);
			}
		}
		return mtPEToActionPermissionMap;
	}

	public Map<String, Map<String,ActorAttributePermission>> getActorAttributePermissions(String workflowActionPermDefPkId) throws SQLException {
		Map<String, Map<String,ActorAttributePermission>> mtPEToActorAttributePermissionMap = new HashMap<>();
		String sql = "SELECT * FROM T_PFM_ADM_WORKFLOW_ACTION_ATTR_PERMN_DEFN WHERE WORKFLOW_ACTION_PERMN_DEFN_FK_ID = x? ;";
		List<Object> params = new ArrayList<>();
		params.add(Util.remove0x(workflowActionPermDefPkId));
		ResultSet rs = workflowDAO.searchResultSet(sql, params);
		while (rs.next()){
			ActorAttributePermission actorAttributePermission = new ActorAttributePermission();
			actorAttributePermission.setAttributePermissionPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_ATTR_PERMN_DEFN_PK_ID")));
			actorAttributePermission.setActionPermissionDefnId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_PERMN_DEFN_FK_ID")));
			actorAttributePermission.setMtPE(rs.getString("MT_PROCESS_ELEMENT_CODE_FK_ID"));
			actorAttributePermission.setAttributePathExpn(rs.getString("ATTR_PATH_EXPN"));
			actorAttributePermission.setAttrVisible(rs.getBoolean("IS_ATTR_VISIBLE"));
			actorAttributePermission.setAttrEditable(rs.getBoolean("IS_ATTR_EDITABLE"));
			if (mtPEToActorAttributePermissionMap.containsKey(actorAttributePermission.getMtPE())){
				mtPEToActorAttributePermissionMap.get(actorAttributePermission.getMtPE()).put(actorAttributePermission.getAttributePathExpn(), actorAttributePermission);
			}else {
				Map<String, ActorAttributePermission> actorAttributePermissionMap = new HashMap<>();
				actorAttributePermissionMap.put(actorAttributePermission.getAttributePathExpn(), actorAttributePermission);
				mtPEToActorAttributePermissionMap.put(actorAttributePermission.getMtPE(), actorAttributePermissionMap);
			}
		}
		return mtPEToActorAttributePermissionMap;
	}

	public void updateTargetActorsActionsStatus(List<WorkflowActor> targetPersonActorList) throws SQLException {
		StringBuilder sql = new StringBuilder("UPDATE T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION SET IS_ACTED = FALSE WHERE WORKFLOW_TXN_STEP_ACTOR_FK_ID IN (");
		Deque<Object> parameter = new ArrayDeque<>();
		for (WorkflowActor targetActor : targetPersonActorList){
			sql.append("x?, ");
			parameter.add(Util.remove0x(targetActor.getWorkflowTxnStepActorPkId()));
		}
		sql.replace(sql.length()-2, sql.length(), ");");
		insertDAO.insertRowWithParameter(sql.toString(), parameter);
	}

	public void validateWorkflowRelatedData(String businessRule, Map<String, Object> underlyingRecord) throws Exception {
		if (businessRule != null){
			if (businessRule.equals("ADD_GOAL_EVENT")){
				Map<String, Object> data = underlyingRecord;
				String goalEventFkId = (String) data.get("PersonGoalEvent.PrimaryKeyID");
				String goalEventType = (String) data.get("PersonGoalEvent.GoalEventType");
				List<Goal> childGoals = getAllGoalsForGoalEvent(goalEventFkId);
				List<GoalThreshold> goalThresholds = getThresholdForGoalEvent(goalEventType);
				validateThresholds(childGoals, goalThresholds);
			}
			if (validationError.hasError()){
				throw new Exception();
			}
		}
	}

	private void validateThresholds(List<Goal> childGoals, List<GoalThreshold> goalThresholds) {
		for (GoalThreshold goalThreshold : goalThresholds){
			List<Goal> goalsSatisfyingCriteria = getGoalsSatisfyingCriteria(goalThreshold, childGoals);
			if(goalsSatisfyingCriteria != null && !goalsSatisfyingCriteria.isEmpty()) {
				checkForNumberOfGoalsThreshold(goalThreshold, goalsSatisfyingCriteria.size());
				checkForGoalWeight(goalThreshold, goalsSatisfyingCriteria);
			}else {// true only for GoalEvent Level Threshold
				checkForNumberOfGoalsThreshold(goalThreshold, childGoals.size());
				checkForGoalWeight(goalThreshold, childGoals);
			}
		}
	}

	private void checkForGoalWeight(GoalThreshold goalThreshold, List<Goal> goalsSatisfyingCriteria) {
		Double goalTotalWeight = 0.0;
		for (Goal goal : goalsSatisfyingCriteria) {
			checkForGoalWeightEach(goalThreshold, goal);
			goalTotalWeight = goalTotalWeight + goal.getGoalWeight();
		}
		checkForGoalWeightTotal(goalThreshold, goalTotalWeight);
	}

	private List<Goal> getGoalsSatisfyingCriteria(GoalThreshold goalThreshold, List<Goal> childGoals) {
		List<Goal> goals = childGoals.stream().filter(p-> Util.compare(p.getGoalGroup(), goalThreshold.getGoalGroup())
				&& Util.compare(p.getGoalCategory(), goalThreshold.getGoalCategory())
				&& Util.compare(p.getGoalType(), goalThreshold.getGoalType())).collect(Collectors.toList());
		return goals;
	}

	private void checkForGoalWeightTotal(GoalThreshold goalThreshold, Double goalTotalWeight) {
		if (goalTotalWeight < goalThreshold.getMinGoalWeightTotal())
			validationError.addError("MINIMUM_WEIGHT_TOTAL", "Minimum weight "+goalThreshold.getMinGoalWeightTotal()+" is required in Goal Threshold : "+goalThreshold.getGoalThresholdPkId());
		if (goalTotalWeight > goalThreshold.getMaxGoalWeightTotal())
			validationError.addError("MAXIMUM_WEIGHT_TOTAL", "Maximum weight "+goalThreshold.getMaxGoalWeightTotal()+" is allowed Goal Threshold : "+goalThreshold.getGoalThresholdPkId());
	}

	private void checkForGoalWeightEach(GoalThreshold goalThreshold, Goal goal) {
		if (goal.getGoalWeight() < goalThreshold.getMinGoalWeightEach())
			validationError.addError("MINIMUM_WEIGHT_EACH", "Minimum weight "+goalThreshold.getMinGoalWeightEach()+" is required for Goal :"+goal.getGoalPkId()+" in Goal Threshold : "+goalThreshold.getGoalThresholdPkId());
		if (goal.getGoalWeight() > goalThreshold.getMaxGoalWeightEach())
			validationError.addError("MAXIMUM_WEIGHT_EACH", "Maximum weight "+goalThreshold.getMaxGoalWeightEach()+" is allowed for Goal :"+goal.getGoalPkId()+" in Goal Threshold : "+goalThreshold.getGoalThresholdPkId());
	}

	private void checkForNumberOfGoalsThreshold(GoalThreshold goalThreshold, int numberOfGoalSatisfyingCriteria) {
		if (numberOfGoalSatisfyingCriteria < goalThreshold.getMinNumberGoals())
			validationError.addError("MINIMUM_GOALS","Minimum "+goalThreshold.getMinNumberGoals()+" goals required for Goal Threshold : "+goalThreshold.getGoalThresholdPkId());
		else if (numberOfGoalSatisfyingCriteria > goalThreshold.getMaxNumberGoals())
			validationError.addError("MAXIMUM_GOALS","Maximum "+goalThreshold.getMinNumberGoals()+" goals allowed for Goal Threshold : "+goalThreshold.getGoalThresholdPkId());
	}

	private List<GoalThreshold> getThresholdForGoalEvent(String goalEventType) throws SQLException {
		List<GoalThreshold> goalThresholds = new ArrayList<>();
		String sql = "SELECT * FROM T_GL_LKP_GOAL_THRESHOLD WHERE GOAL_EVENT_TYPE_FK_ID = x? AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND RECORD_STATE_CODE_FK_ID = 'CURRENT' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE';";
		List<Object> params = new ArrayList<>();
		params.add(Util.remove0x(goalEventType));
		ResultSet rs = workflowDAO.searchResultSet(sql, params);
		if (rs != null){
			while (rs.next()){
				GoalThreshold goalThreshold = new GoalThreshold();
				goalThreshold.setVersionId(Util.convertByteToString(rs.getBytes("VERSION_ID")));
				goalThreshold.setGoalThresholdPkId(Util.convertByteToString(rs.getBytes("GOAL_THRESHOLD_PK_ID")));
				goalThreshold.setGoalEventType(Util.convertByteToString(rs.getBytes("GOAL_EVENT_TYPE_FK_ID")));
				goalThreshold.setGoalGroup(Util.convertByteToString(rs.getBytes("GOAL_GROUP_FK_ID")));
				goalThreshold.setGoalCategory(Util.convertByteToString(rs.getBytes("GOAL_CATEGORY_FK_ID")));
				goalThreshold.setGoalType(Util.convertByteToString(rs.getBytes("GOAL_TYPE_FK_ID")));
				goalThreshold.setGoalThresholdName(rs.getString("GOAL_THRESHOLD_NAME_G11N_BIG_TXT"));
				goalThreshold.setGoalThresholdIcon(rs.getString("GOAL_THRESHOLD_ICON_CODE_FK_ID"));
				goalThreshold.setMinNumberGoals(rs.getInt("MIN_NUMBER_GOALS_POS_INT"));
				goalThreshold.setMaxNumberGoals(rs.getInt("MAX_NUMBER_GOAL_POS_INT"));
				goalThreshold.setMinGoalWeightEach(rs.getDouble("MIN_GOAL_WEIGHT_EACH_POS_DEC"));
				goalThreshold.setMaxGoalWeightEach(rs.getDouble("MAX_GOAL_WEIGTH_EACH_POS_DEC"));
				goalThreshold.setMinGoalWeightTotal(rs.getDouble("MIN_GOAL_WEIGHT_TOT_POS_DEC"));
				goalThreshold.setMaxGoalWeightTotal(rs.getDouble("MAX_GOAL_WEIGTH_TOT_POS_DEC"));
				goalThresholds.add(goalThreshold);
			}
		}
		return goalThresholds;
	}

	private List<Goal> getAllGoalsForGoalEvent(String goalEventFkId) throws SQLException {
		List<Goal> goals = new ArrayList<>();
		String sql = "SELECT * FROM T_GL_EU_GOAL WHERE GOAL_EVENT_FK_ID = x? AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND RECORD_STATE_CODE_FK_ID = 'CURRENT' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE';";
		List<Object> params = new ArrayList<>();
		params.add(Util.remove0x(goalEventFkId));
		ResultSet rs = workflowDAO.searchResultSet(sql, params);
		if (rs != null){
			while (rs.next()){
				Goal goal = new Goal();
				goal.setVersionId(Util.convertByteToString(rs.getBytes("VERSION_ID")));
				goal.setGoalPkId(Util.convertByteToString(rs.getBytes("GOAL_PK_ID")));
				goal.setGoalEventId(Util.convertByteToString(rs.getBytes("GOAL_EVENT_FK_ID")));
				goal.setGoalName(rs.getString("GOAL_NAME_G11N_BIG_TXT"));
				goal.setGoalDescription(rs.getString("GOAL_DESCRIPTION_G11N_BIG_TXT"));
				goal.setGoalEntrySource(Util.convertByteToString(rs.getBytes("GOAL_ENTRY_SOURCE_FK_ID")));
				goal.setGoalGroup(Util.convertByteToString(rs.getBytes("GOAL_GROUP_FK_ID")));
				goal.setGoalCategory(Util.convertByteToString(rs.getBytes("GOAL_CATEGORY_FK_ID")));
				goal.setGoalType(Util.convertByteToString(rs.getBytes("GOAL_TYPE_FK_ID")));
				goal.setGoalWeight(rs.getDouble("GOAL_WEIGHT_POS_DEC"));
				goals.add(goal);
			}
		}
		return goals;
	}

	public Map<String, Map<String, NonActorAttributePermission>> getNonActorAttributePermission(String workflowActionPermDefPkId) throws SQLException {
		Map<String, Map<String, NonActorAttributePermission>> mtPEToNonActorAttributePermissionMap = new HashMap<>();
		String sql = "SELECT * FROM T_PFM_ADM_WORKFLOW_ACTION_ATTR_PERMN_APPL_DEFN JOIN T_PFM_ADM_WORKFLOW_ACTION_ATTR_PERMN_DEFN " +
				"ON WORKFLOW_ACTION_ATTR_PERMN_DEFN_PK_ID = WORKFLOW_ACTION_ATTR_PERMN_DEFN_FK_ID WHERE WORKFLOW_ACTION_ATTR_PERMN_DEFN_FK_ID = x?";
		List<Object> params = new ArrayList<>();
		params.add(Util.remove0x(workflowActionPermDefPkId));
		ResultSet rs = workflowDAO.searchResultSet(sql, params);
		while (rs.next()){
			NonActorAttributePermission nonActorAttributePermission = new NonActorAttributePermission();
			nonActorAttributePermission.setAttributePermissionPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_VIS_ACTN_PERMN_APPL_DEFN_PK_ID")));
			nonActorAttributePermission.setActionPermissionDefnId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_VIS_ACTN_PERMN_DEFN_FK_ID")));
			nonActorAttributePermission.setPermissionTypeCodeId(rs.getString("PERMISSION_TYPE_CODE_FK_ID"));
			nonActorAttributePermission.setPermissionValue(rs.getString("PERMISSION_VALUE_LNG_TXT"));
			nonActorAttributePermission.setAdditionalSubjectUserApplicableGroup(rs.getString("ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
			nonActorAttributePermission.setRecordStatePermission(rs.getString("T_PFM_ADM_WORKFLOW_ACTION_VIS_ACTN_PERMN_APPL_DEFN.RECORD_STATE_PERM_BIG_TXT"));
			nonActorAttributePermission.setMtPE(rs.getString("MT_PROCESS_ELEMENT_CODE_FK_ID"));
			nonActorAttributePermission.setAttributePermissionPkId(rs.getString("ATTR_PATH_EXPN"));
			nonActorAttributePermission.setAttrVisible(rs.getBoolean("IS_ATTR_VISIBLE"));
			nonActorAttributePermission.setAttrEditable(rs.getBoolean("IS_ATTR_EDITABLE"));
			if (mtPEToNonActorAttributePermissionMap.containsKey(nonActorAttributePermission.getMtPE())){
				mtPEToNonActorAttributePermissionMap.get(nonActorAttributePermission.getMtPE()).put(nonActorAttributePermission.getAttributePathExpn(), nonActorAttributePermission);
			}else {
				Map<String, NonActorAttributePermission> actorAttributePermissionMap = new HashMap<>();
				actorAttributePermissionMap.put(nonActorAttributePermission.getAttributePathExpn(), nonActorAttributePermission);
				mtPEToNonActorAttributePermissionMap.put(nonActorAttributePermission.getMtPE(), actorAttributePermissionMap);
			}
		}
		return mtPEToNonActorAttributePermissionMap;
	}

	public Map<String,Map<String,NonActorActionPermission>> getNonActorActionPermission(String workflowActionPermDefPkId) throws SQLException {
		Map<String, Map<String, NonActorActionPermission>> mtPEToNonActorActionPermissionMap = new HashMap<>();
		String sql = "SELECT * FROM T_PFM_ADM_WORKFLOW_ACTION_VIS_ACTN_PERMN_APPL_DEFN JOIN T_PFM_ADM_WORKFLOW_ACTION_VIS_ACTN_PERMN_DEFN " +
				"ON WORKFLOW_ACTION_VIS_ACTN_PERMN_DEFN_PK_ID = WORKFLOW_ACTION_VIS_ACTN_PERMN_DEFN_FK_ID WHERE WORKFLOW_ACTION_VIS_ACTN_PERMN_DEFN_FK_ID = ?";
		List<Object> params = new ArrayList<>();
		params.add(Util.remove0x(workflowActionPermDefPkId));
		ResultSet rs = workflowDAO.searchResultSet(sql, params);
		while (rs.next()){
			NonActorActionPermission nonActorActionPermission = new NonActorActionPermission();
			nonActorActionPermission.setActionPermissionPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_VIS_ACTN_PERMN_APPL_DEFN_PK_ID")));
			nonActorActionPermission.setActionPermissionDefnId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_VIS_ACTN_PERMN_DEFN_FK_ID")));
			nonActorActionPermission.setPermissionTypeCodeId(rs.getString("PERMISSION_TYPE_CODE_FK_ID"));
			nonActorActionPermission.setPermissionValue(rs.getString("PERMISSION_VALUE_LNG_TXT"));
			nonActorActionPermission.setAdditionalSubjectUserApplicableGroup(rs.getString("ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
			nonActorActionPermission.setRecordStatePermission(rs.getString("T_PFM_ADM_WORKFLOW_ACTION_VIS_ACTN_PERMN_APPL_DEFN.RECORD_STATE_PERM_BIG_TXT"));
			nonActorActionPermission.setMtPE(rs.getString("MT_PROCESS_ELEMENT_CODE_FK_ID"));
			nonActorActionPermission.setActionVisualMasterCode(rs.getString("ACTION_VISUAL_MASTER_CODE_FK_ID"));
			nonActorActionPermission.setActionApplicable(rs.getBoolean("IS_ACTION_APPLICABLE"));
			if (mtPEToNonActorActionPermissionMap.containsKey(nonActorActionPermission.getMtPE())){
				mtPEToNonActorActionPermissionMap.get(nonActorActionPermission.getMtPE()).put(nonActorActionPermission.getActionVisualMasterCode(), nonActorActionPermission);
			}else {
				Map<String, NonActorActionPermission> nonActorActionPermissionMap = new HashMap<>();
				nonActorActionPermissionMap.put(nonActorActionPermission.getActionVisualMasterCode(), nonActorActionPermission);
				mtPEToNonActorActionPermissionMap.put(nonActorActionPermission.getMtPE(), nonActorActionPermissionMap);
			}
		}
		return mtPEToNonActorActionPermissionMap;
	}

    public List<WorkflowActorAction> getWorkflowActionsForLoggedInUser(String workflowTxnId) throws SQLException {
		String loggedInPersonId = TenantContextHolder.getUserContext().getPersonId();
		List<WorkflowActorAction> workflowActorActions = new ArrayList<>();
		String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION INNER JOIN T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR ON WORKFLOW_TXN_STEP_ACTOR_FK_ID = WORKFLOW_TXN_STEP_ACTOR_PK_ID\n" +
				"INNER JOIN T_PFM_IEU_WORKFLOW_TXN_STEP ON WORKFLOW_TXN_STEP_PK_ID = WORKFLOW_TXN_STEP_FK_ID\n" +
				"INNER JOIN T_PFM_IEU_WORKFLOW_TXN ON WORKFLOW_TXN_FK_ID = WORKFLOW_TXN_PK_ID\n" +
				"WHERE WORKFLOW_TXN_PK_ID = x? AND RESOLVED_PERSON_FK_ID = x? AND IS_ACTED = FALSE;";
		List<Object> params = new ArrayList<>();
		params.add(Util.remove0x(workflowTxnId));
		params.add(Util.remove0x(loggedInPersonId));
		ResultSet rs = workflowDAO.searchResultSet(sql, params);
		while (rs.next()){
			WorkflowActorAction workflowActorAction = new WorkflowActorAction();
			workflowActorAction.setWorkflowTxnActorPossibleActionPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID")));
			workflowActorAction.setWorkflowTxnStepActorFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_ACTOR_FK_ID")));
			workflowActorAction.setActionVisualMasterCodeFkId(rs.getString("ACTN_VISUAL_MASTER_CODE_FK_ID"));
			workflowActorAction.setActionCommentMandatory(rs.getBoolean("IS_COMMENT_MANDATORY_FLAG"));
			workflowActorActions.add(workflowActorAction);
		}
		return  workflowActorActions;
    }

	public Map<String, WorkflowActor> getResolvedPersonActorByStep(String workflowTxnStepPkId) throws SQLException {
		return workflowDAO.getResolvedPersonActorByStep(workflowTxnStepPkId);
	}

	public Map<String, WorkflowReceiptActionLog> getReceiptActionLogMap(Map<String, WorkflowActor> currentStepResolvedActorMap) throws SQLException, ParseException {
		Map<String, WorkflowReceiptActionLog> receiptActionLogMap = new HashMap<>();
		StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_RECEIPT_ACTION_LOG WHERE WORKFLOW_TXN_STEP_ACTOR_FK_ID IN (");
		List<Object> parameters = new ArrayList<>();
		for (Entry<String, WorkflowActor> entry : currentStepResolvedActorMap.entrySet()){
			sql.append("x?, ");
			parameters.add(Util.remove0x(entry.getKey()));
		}
		sql.replace(sql.length()-2, sql.length(), ");");
		ResultSet rs = workflowDAO.searchResultSet(sql.toString(), parameters);
		while (rs.next()){
			WorkflowReceiptActionLog workflowReceiptActionLog = getReceiptActionLogFromResultSet(rs);
			receiptActionLogMap.put(workflowReceiptActionLog.getActorReceiptActionLogPkId(), workflowReceiptActionLog);
		}
		return receiptActionLogMap;
	}

	private WorkflowReceiptActionLog getReceiptActionLogFromResultSet(ResultSet rs) throws SQLException, ParseException {
		WorkflowReceiptActionLog workflowReceiptActionLog = new WorkflowReceiptActionLog();
		workflowReceiptActionLog.setActorReceiptActionLogPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_ACTOR_RECEIPT_ACTION_LOG_PK_ID")));
		workflowReceiptActionLog.setStepActorId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_ACTOR_FK_ID")));
		workflowReceiptActionLog.setReceivedStepActorId(Util.convertByteToString(rs.getBytes("RCVD_WFLOW_TXN_STEP_ACTOR_FK_ID")));
		workflowReceiptActionLog.setReceivedFromPersonId(Util.convertByteToString(rs.getBytes("RCVD_FROM_PERSON_FK_ID")));
		workflowReceiptActionLog.setReceivedFromOnBehalfOfPersonId(Util.convertByteToString(rs.getBytes("RCVD_FROM_ON_BEHALF_OF_PERSON_FK_ID")));
		workflowReceiptActionLog.setActionMasterCodeId(rs.getString("ACTION_MASTER_CODE_FK_ID"));
		workflowReceiptActionLog.setActionReceivedDatetime(Util.formatDatetimeString(rs.getTimestamp("ACTION_RCV_DATETIME")));
		workflowReceiptActionLog.setReceivedActionComment(rs.getString("RCV_ACTION_COMMENT_G11N_BIG_TXT"));
		workflowReceiptActionLog.setDeleted(rs.getBoolean("IS_DELETED"));
		return workflowReceiptActionLog;
	}

	public Map<String, WorkflowSendActionLog> getWorkflowSendActionLog(Map<String, WorkflowReceiptActionLog> receiptActionLogMap) throws SQLException {
		Map<String, WorkflowSendActionLog> workflowSendActionLogMap = new HashMap<>();
		StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_SEND_ACTION_LOG WHERE SEND_TO_WFLOW_TXN_STEP_ACTOR_FK_ID IS NULL AND WORKFLOW_TXN_ACTOR_RECEIPT_ACTION_LOG_FK_ID IN (");
		List<Object> params = new ArrayList<>();
		for (Entry<String, WorkflowReceiptActionLog> entry : receiptActionLogMap.entrySet()){
			sql.append("x?, ");
			params.add(Util.remove0x(entry.getKey()));
		}
		sql.replace(sql.length()-2, sql.length(), ");");
		ResultSet rs = workflowDAO.searchResultSet(sql.toString(), params);
		while (rs.next()){
			WorkflowSendActionLog workflowSendActionLog = getWorkflowSendActionLogFromResultSet(rs);
			workflowSendActionLogMap.put(workflowSendActionLog.getActorSendActionLogPkId(), workflowSendActionLog);
		}
		return workflowSendActionLogMap;
	}

	private WorkflowSendActionLog getWorkflowSendActionLogFromResultSet(ResultSet rs) throws SQLException {
		WorkflowSendActionLog workflowSendActionLog = new WorkflowSendActionLog();
		workflowSendActionLog.setActorSendActionLogPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_ACTOR_SEND_ACTION_LOG_PK_ID")));
		workflowSendActionLog.setActorReceiptActionLogId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_ACTOR_RECEIPT_ACTION_LOG_FK_ID")));
		workflowSendActionLog.setSendToWorkflowStepActorId(Util.convertByteToString(rs.getBytes("SEND_TO_WFLOW_TXN_STEP_ACTOR_FK_ID")));
		workflowSendActionLog.setSendToPersonId(Util.convertByteToString(rs.getBytes("SEND_TO_PERSON_FK_ID")));
		workflowSendActionLog.setSendToOnBehalfOfPersonId(Util.convertByteToString(rs.getBytes("SEND_TO_ON_BEHALF_OF_PERSON_FK_ID")));
		workflowSendActionLog.setActionVisualMasterCodeId(Util.convertByteToString(rs.getBytes("ACTN_VISUAL_MASTER_CODE_FK_ID")));
		workflowSendActionLog.setActionSentDateTime(Util.convertByteToString(rs.getBytes("ACTION_SENT_DATETIME")));
		workflowSendActionLog.setSentActionComment(Util.convertByteToString(rs.getBytes("SENT_ACTION_COMMENT_G11N_BIG_TXT")));
		workflowSendActionLog.setDeleted(rs.getBoolean("IS_DELETED"));
		return workflowSendActionLog;
	}

	public void updateSendActionLogTableWithResolvedNextStepActors(Map<String, WorkflowSendActionLog> sendActionLogMap, List<WorkflowActor> nextStepResolvedActors) throws SQLException {
		StringBuilder insertQuery = new StringBuilder("INSERT INTO T_PFM_IEU_WORKFLOW_TXN_ACTOR_SEND_ACTION_LOG (WORKFLOW_TXN_ACTOR_SEND_ACTION_LOG_PK_ID, " +
				"WORKFLOW_TXN_ACTOR_RECEIPT_ACTION_LOG_FK_ID, SEND_TO_WFLOW_TXN_STEP_ACTOR_FK_ID, SEND_TO_PERSON_FK_ID, SEND_TO_ON_BEHALF_OF_PERSON_FK_ID, " +
				"ACTN_VISUAL_MASTER_CODE_FK_ID, ACTION_SENT_DATETIME, SENT_ACTION_COMMENT_G11N_BIG_TXT, IS_DELETED) VALUES ");
		List<Object> params = new ArrayList<>();
		for (Entry<String, WorkflowSendActionLog> entry : sendActionLogMap.entrySet()) {
			boolean isFirstSendToRecord = true;
			boolean isInsertRequired = false;
			for (WorkflowActor workflowActor : nextStepResolvedActors) {
				if (isFirstSendToRecord) {
					updateSendActionLogTable(entry.getValue(), workflowActor);
					isFirstSendToRecord = false;
				}
				else {
					buildInsertIntoSendActionLogQuery(entry.getValue(), workflowActor, insertQuery, params);
					isInsertRequired = true;
				}
			}
			if (isInsertRequired){
				insertQuery.replace(insertQuery.length()-2, insertQuery.length(), ";");
				workflowDAO.executeUpdate(insertQuery.toString(), params);
			}
		}
	}

	private void buildInsertIntoSendActionLogQuery(WorkflowSendActionLog sendActionLog, WorkflowActor workflowActor, StringBuilder insertQuery, List<Object> params) {
		insertQuery.append("(x?, x?, x?, x?, NULL, ?, ?, ?, FALSE), ");
		params.add(Util.remove0x(Common.getUUID()));
		params.add(Util.remove0x(sendActionLog.getActorReceiptActionLogId()));
		params.add(Util.remove0x(workflowActor.getWorkflowTxnStepActorPkId()));
		params.add(Util.remove0x(workflowActor.getResolvedPersonFkId()));
		params.add(Util.remove0x(sendActionLog.getActorSendActionLogPkId()));
		params.add(Util.remove0x(workflowActor.getResolvedPersonFkId()));
		params.add(sendActionLog.getActionVisualMasterCodeId());
		params.add(sendActionLog.getSentActionComment());
	}

	private void updateSendActionLogTable(WorkflowSendActionLog sendActionLog, WorkflowActor workflowActor) throws SQLException {
		String sql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_ACTOR_SEND_ACTION_LOG SET SEND_TO_WFLOW_TXN_STEP_ACTOR_FK_ID = x?, SEND_TO_PERSON_FK_ID = x? WHERE WORKFLOW_TXN_ACTOR_SEND_ACTION_LOG_PK_ID = x?;";
		List<Object> params = new ArrayList<>();
		params.add(Util.remove0x(workflowActor.getWorkflowTxnStepActorPkId()));
		params.add(Util.remove0x(workflowActor.getResolvedPersonFkId()));
		params.add(Util.remove0x(sendActionLog.getActorSendActionLogPkId()));
		workflowDAO.executeUpdate(sql, params);
	}
}
