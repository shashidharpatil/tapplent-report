package com.tapplent.platformutility.layout.valueObject;

public class ControlActionVO {
	private String controlActionEuPkId;
	private String controlEuFkId;
	private String baseTemplateFkId;
	private String controlActionDefnFkId;
	private String controlDefnDepFkId;
	private String canvasEuFkId;
	private String actionCodeFkId;
	private String actionGestureCodeFkId;
	private String actionGestureAnimationCodeFkId;
	public String getControlActionEuPkId() {
		return controlActionEuPkId;
	}
	public void setControlActionEuPkId(String controlActionEuPkId) {
		this.controlActionEuPkId = controlActionEuPkId;
	}
	public String getControlEuFkId() {
		return controlEuFkId;
	}
	public void setControlEuFkId(String controlEuFkId) {
		this.controlEuFkId = controlEuFkId;
	}
	public String getBaseTemplateFkId() {
		return baseTemplateFkId;
	}
	public void setBaseTemplateFkId(String baseTemplateFkId) {
		this.baseTemplateFkId = baseTemplateFkId;
	}
	public String getControlActionDefnFkId() {
		return controlActionDefnFkId;
	}
	public void setControlActionDefnFkId(String controlActionDefnFkId) {
		this.controlActionDefnFkId = controlActionDefnFkId;
	}
	public String getControlDefnDepFkId() {
		return controlDefnDepFkId;
	}
	public void setControlDefnDepFkId(String controlDefnDepFkId) {
		this.controlDefnDepFkId = controlDefnDepFkId;
	}
	public String getCanvasEuFkId() {
		return canvasEuFkId;
	}
	public void setCanvasEuFkId(String canvasEuFkId) {
		this.canvasEuFkId = canvasEuFkId;
	}
	public String getActionCodeFkId() {
		return actionCodeFkId;
	}
	public void setActionCodeFkId(String actionCodeFkId) {
		this.actionCodeFkId = actionCodeFkId;
	}
	public String getActionGestureCodeFkId() {
		return actionGestureCodeFkId;
	}
	public void setActionGestureCodeFkId(String actionGestureCodeFkId) {
		this.actionGestureCodeFkId = actionGestureCodeFkId;
	}
	public String getActionGestureAnimationCodeFkId() {
		return actionGestureAnimationCodeFkId;
	}
	public void setActionGestureAnimationCodeFkId(String actionGestureAnimationCodeFkId) {
		this.actionGestureAnimationCodeFkId = actionGestureAnimationCodeFkId;
	}
}
