package com.tapplent.platformutility.batch.controller;

/**
 * Created by sripat on 30/08/16.
 */
public interface BatchDAO {

    public void initializeBatch();

    public void resolveSchInstances(boolean isInitial,String jobID);


}
