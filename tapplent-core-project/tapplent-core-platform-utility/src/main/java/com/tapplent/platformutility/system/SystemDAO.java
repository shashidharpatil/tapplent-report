package com.tapplent.platformutility.system;

import com.tapplent.platformutility.g11n.structure.OEMObject;

import java.util.Map;

public interface SystemDAO {
    Map<String,OEMObject> getOemObjectMap();
    Map<String ,Map<String,String>> getOemToLocaleLangMap();
}
