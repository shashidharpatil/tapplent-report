package com.tapplent.platformutility.search.builder.expression;

public enum Operator {
	/* Adding functions here itself */
	DBCOUNT(" COUNT"),DBAVERAGE(" AVG"),DBMAX(" MAX"),DBMIN(" MIN"),DBSTDDEV(" STDDEV"),DBSUM(" SUM"),DBVAR(" VARIANCE"),FIELD(" FIELD"),IF(" if"), YEAR(" YEAR"), DAY(" DAY"), MONTH(" MONTH"), DATEDIFF(" TIMESTAMPDIFF "),DATESUB(" DATE_SUB"),DATEADD(" DATE_ADD"),MOD(" MOD"), INTCAST(" CAST"),G11N_PLURAL(" g11n_plural"),G11N_STATIC(" g11n_static"),
	APPEND_IS_NULL( "IS NULL"),SUBSTRING(" SUBSTRING"),
	WEEKDAY(" WEEKDAY"),
	LASTDAY(" LAST_DAY"),
	LEFT(" LEFT"),
	MATCH(" MATCH"), AGAINST_IN_BOOLEAN_MODE(" AGAINST"),CONCAT(" CONCAT"),
	INTERVAL(""),INTERVAL_YR(" INTERVAL"),INTERVAL_MONTH(" INTERVAL"),INTERVAL_DAY(" INTERVAL"),INTERVAL_WEEK(" INTERVAL"),
	EXCHANGERATE(" EXCHANGE_RATE"),
	BINARY(""),COLLATE(""),
	U_SUB(" - "),
	BITWISE_XOR(" ^ "),
	MULTIPLY(" * "),DIVIDE(" / "),INT_DIV(" DIV "),MODULO_OP(" % "),
	B_SUB(" - "),ADD(" + "),
	LEFT_SHIFT(" << "),RIGHT_SHIFT(" >> "),
	BITWISE_AND(" & "),
	BITWISE_OR(" | "),
	EQ(" = "),NULL_SAFE_EQ(" <=> "),GTE(" >= "),GT(" > "),LE(" <= "),LT(" < "),NE(" <> "), LIKE(" like "),IN(" IN "),NULL(" NULL "), NOT_IN(" NOT IN "), IS_NOT_NULL(" IS NOT NULL "),NOT_NULL(" NOT NULL "), IS_NULL(" IS NULL "),
	BW(" BETWEEN "),
	AND(" AND "),ASC(" ASC "),DESC(" DESC "),
	XOR(" XOR "),
	OR(" OR "),
	ASSIGNMENT(" := "),
	COMMA(","),
	ERROR(" ERROR "),
	OPEN_PARENTHESIS("("), CLOSE_PARENTHESIS(")"), AS(" AS "), DISTINCT(" DISTINCT "), TODAY(" UTC_DATE "), DATE_FORMAT(" DATE_FORMAT"),COALESCE(" COALESCE");
    private final String sqlCondition;
    Operator(String sqlCondition) {
        this.sqlCondition = sqlCondition;
    }
    public String getSqlCondition() {
        return sqlCondition;
    }
}
