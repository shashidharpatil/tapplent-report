package com.tapplent.platformutility.common.util;

import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.PersonDetailsObjectRepository;
import com.tapplent.platformutility.common.cache.ServerPropertyObjectRepository;
import com.tapplent.platformutility.common.cache.SystemObjectRepository;
import com.tapplent.tenantresolver.utility.TapplentApplicationContext;

public class TapplentServiceLookupUtil {
	public static MetadataObjectRepository getMetadataRepository() {
        return (MetadataObjectRepository) getService("com.tapplent.platformutility.common.cache.MetadataObjectRepository");
    }
	
	public static Object getService(String serviceName){
		return TapplentApplicationContext.getApplicationContext().getBean(serviceName);
	}
	
	public static ServerPropertyObjectRepository getServerPropertyRepository(){
		return (ServerPropertyObjectRepository) getService("com.tapplent.platformutility.common.cache.ServerPropertyObjectRepository");
	}
	
	public static SystemObjectRepository getSystemRepository(){
		return (SystemObjectRepository) getService("com.tapplent.platformutility.common.cache.SystemObjectRepository");
	}

	public static PersonDetailsObjectRepository getPersonRepository(){
		return (PersonDetailsObjectRepository) getService("com.tapplent.platformutility.common.cache.PersonDetailsObjectRepository");
	}
}
