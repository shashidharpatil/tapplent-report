package com.tapplent.platformutility.workflow.structure;

public class WorkflowActionPermissionDefinition {
    private String workflowActionPermDefPkId;
    private String workflowDefinitionId;
    private String workflowActorDefinitionId;
    private String workflowActionedExpn;
    private Integer permissionPriorityPosition;

    public String getWorkflowActionPermDefPkId() {
        return workflowActionPermDefPkId;
    }

    public void setWorkflowActionPermDefPkId(String workflowActionPermDefPkId) {
        this.workflowActionPermDefPkId = workflowActionPermDefPkId;
    }

    public String getWorkflowDefinitionId() {
        return workflowDefinitionId;
    }

    public void setWorkflowDefinitionId(String workflowDefinitionId) {
        this.workflowDefinitionId = workflowDefinitionId;
    }

    public String getWorkflowActorDefinitionId() {
        return workflowActorDefinitionId;
    }

    public void setWorkflowActorDefinitionId(String workflowActorDefinitionId) {
        this.workflowActorDefinitionId = workflowActorDefinitionId;
    }

    public String getWorkflowActionedExpn() {
        return workflowActionedExpn;
    }

    public void setWorkflowActionedExpn(String workflowActionedExpn) {
        this.workflowActionedExpn = workflowActionedExpn;
    }

    public Integer getPermissionPriorityPosition() {
        return permissionPriorityPosition;
    }

    public void setPermissionPriorityPosition(Integer permissionPriorityPosition) {
        this.permissionPriorityPosition = permissionPriorityPosition;
    }
}
