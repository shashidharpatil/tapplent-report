package com.tapplent.platformutility.common.util;

public class UpdateColumn {
	private String primarykeyColumnName;
	private String primaryKeyId;
	private String primaryKeyDbDataType;
	private String tableName;
	private String columnName;
	private String jsonName;
	private String currentValue;
	private String columnType;
	private String dbDataType;
	public UpdateColumn(String tableName, String columnName, String jsonName, String currentValue, String columnType,
			String dbDataType) {
		this.tableName = tableName;
		this.columnName = columnName;
		this.jsonName = jsonName;
		this.currentValue = currentValue;
		this.columnType = columnType;
		this.dbDataType = dbDataType;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getJsonName() {
		return jsonName;
	}
	public void setJsonName(String jsonName) {
		this.jsonName = jsonName;
	}
	public String getCurrentValue() {
		return currentValue;
	}
	public void setCurrentValue(String currentValue) {
		this.currentValue = currentValue;
	}
	public String getColumnType() {
		return columnType;
	}
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
	public String getDbDataType() {
		return dbDataType;
	}
	public void setDbDataType(String dbDataType) {
		this.dbDataType = dbDataType;
	}
	public String getPrimarykeyColumnName() {
		return primarykeyColumnName;
	}
	public void setPrimarykeyColumnName(String primarykeyColumnName) {
		this.primarykeyColumnName = primarykeyColumnName;
	}
	public String getPrimaryKeyId() {
		return primaryKeyId;
	}
	public void setPrimaryKeyId(String primaryKeyId) {
		this.primaryKeyId = primaryKeyId;
	}
	public String getPrimaryKeyDbDataType() {
		return primaryKeyDbDataType;
	}
	public void setPrimaryKeyDbDataType(String primaryKeyDbDataType) {
		this.primaryKeyDbDataType = primaryKeyDbDataType;
	}
}
