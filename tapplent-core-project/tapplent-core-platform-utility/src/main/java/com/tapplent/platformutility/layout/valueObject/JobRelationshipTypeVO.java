package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 19/07/17.
 */
public class JobRelationshipTypeVO {
    private String relationshipTypeCode;
    private String name;
    private String icon;
    private String imageID;
    private String borderType;
    private String borderColour;
    private String colleaguesAtWorkCountQuery;

    public String getRelationshipTypeCode() {
        return relationshipTypeCode;
    }

    public void setRelationshipTypeCode(String relationshipTypeCode) {
        this.relationshipTypeCode = relationshipTypeCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

    public String getBorderType() {
        return borderType;
    }

    public void setBorderType(String borderType) {
        this.borderType = borderType;
    }

    public String getBorderColour() {
        return borderColour;
    }

    public void setBorderColour(String borderColour) {
        this.borderColour = borderColour;
    }

    public String getColleaguesAtWorkCountQuery() {
        return colleaguesAtWorkCountQuery;
    }

    public void setColleaguesAtWorkCountQuery(String colleaguesAtWorkCountQuery) {
        this.colleaguesAtWorkCountQuery = colleaguesAtWorkCountQuery;
    }
}
