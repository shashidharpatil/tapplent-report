package com.tapplent.platformutility.etl.dao;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.tapplent.platformutility.common.util.*;
import com.tapplent.platformutility.etl.valueObject.*;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.apache.commons.codec.binary.Hex;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.layout.valueObject.DOADetails;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadata.structure.MasterEntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.platformutility.search.api.SearchResult;

public class EtlDAOImpl extends TapplentBaseDAO implements EtlDAO {

	private static final Logger LOG = LogFactory.getLogger(EtlDAOImpl.class);

	@Override
	public List<EtlHeaderVO> getAllHeaders() {
		List<EtlHeaderVO> etlHeaderVOs = new ArrayList<>();
		String SQL = "SELECT * FROM T_ETL_ADM_HEADER;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		EtlHeaderVO etlHeader =  null;
		try{
			while (rs.next()){
				etlHeader = new EtlHeaderVO();
				etlHeader.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
				etlHeader.setEtlHeaderPkId(rs.getString("ETL_HEADER_CODE_PK_ID"));
				etlHeader.setName(getG11nValue(rs.getString("NAME_G11N_BIG_TXT")));
				etlHeader.setDescription(getG11nValue(rs.getString("DESCRIPTION_G11N_BIG_TXT")));
				etlHeader.setSourceDoCodeFkId(rs.getString("SOURCE_DO_CODE_FK_ID"));
				etlHeader.setBaseTemplateFkId(rs.getString("BASE_TEMPLATE_CODE_FK_ID"));
				etlHeader.setCreateUpdatePersonFkId(convertByteToString(rs.getBytes("CREATE_UPDATE_PERSON_FK_ID")));
				etlHeader.setPrimaryLocaleCodeFkId(rs.getString("PRIMARY_LOCALE_CODE_FK_ID"));
				etlHeader.setDefaultSaveStateCodeFkId(rs.getString("DEFAULT_SAVE_STATE_CODE_FK_ID"));
				etlHeader.setDefaultDateTimeFormat(rs.getString("DEFAULT_DATE_TIME_FORMAT_CODE_FK_ID"));
				etlHeaderVOs.add(etlHeader);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return etlHeaderVOs;
	}

	@Override
	public EtlHeaderVO getHeaderByHeaderId(String headerId) {
		String SQL = "SELECT * FROM T_ETL_ADM_HEADER WHERE ETL_HEADER_CODE_PK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(headerId);
		ResultSet rs = executeSQL(SQL, parameters);
		EtlHeaderVO etlHeader = null;
		try{
			if(rs.next()){
				etlHeader = new EtlHeaderVO();
				etlHeader.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
				etlHeader.setEtlHeaderPkId(rs.getString("ETL_HEADER_CODE_PK_ID"));
				etlHeader.setName(getG11nValue(rs.getString("ETL_HDR_NAME_G11N_BIG_TXT")));
				etlHeader.setDescription(getG11nValue(rs.getString("ETL_HDR_DESCRIPTION_G11N_BIG_TXT")));
				etlHeader.setSourceDoCodeFkId(rs.getString("SOURCE_DO_CODE_FK_ID"));
				etlHeader.setBaseTemplateFkId(rs.getString("BASE_TEMPLATE_CODE_FK_ID"));
				etlHeader.setCreateUpdatePersonFkId(convertByteToString(rs.getBytes("CREATE_UPDATE_PERSON_FK_ID")));
				etlHeader.setPrimaryLocaleCodeFkId(rs.getString("PRIMARY_LOCALE_CODE_FK_ID"));
				etlHeader.setDefaultSaveStateCodeFkId(rs.getString("DEFAULT_SAVE_STATE_CODE_FK_ID"));
				etlHeader.setDefaultDateTimeFormat(rs.getString("DEFAULT_DATE_TIME_FORMAT_CODE_FK_ID"));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return etlHeader;
	}

	@Override
	public List<EtlMapVO> getEtlMapByHeaderId(String headerId) {
		List<EtlMapVO> etlMapVOs = new ArrayList<>();
		String SQL = "SELECT * FROM T_ETL_ADM_MAP WHERE ETL_HEADER_DEP_CODE_FK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(headerId);
		ResultSet rs = executeSQL(SQL, parameters);
		EtlMapVO etlMapVO = null;
		try{
			while(rs.next()){
				etlMapVO = new EtlMapVO();
				etlMapVO.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
				etlMapVO.setEtlMapPkId(convertByteToString(rs.getBytes("ETL_MAP_PK_ID")));
				etlMapVO.setMapName(getG11nValue(rs.getString("MAP_NAME_G11N_BIG_TXT")));
				etlMapVO.setEtlHeaderDepFkId(convertByteToString(rs.getBytes("ETL_HEADER_DEP_CODE_FK_ID")));
				etlMapVO.setMapSequenceNumberPosInt(rs.getInt("MAP_SEQUENCE_NUMBER_POS_INT"));
				etlMapVO.setTargetDoCodeFkId(rs.getString("TARGET_DO_CODE_FK_ID"));
				etlMapVO.setTargetBaseTemplateCodeFkId(rs.getString("TARGET_BASE_TEMPLATE_CODE_FK_ID"));
				etlMapVO.setTargetDataModeCodeFkId(rs.getString("TARGET_DATA_MODE_CODE_FK_ID"));
				etlMapVOs.add(etlMapVO);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return etlMapVOs;
	}

	@Override
	public List<EtlMapDetailsVO> getEtlMapDetailsByHeaderId(String headerId) {
		List<EtlMapDetailsVO> etlMapDetailsVOs = new ArrayList<>();
		String SQL = "SELECT * FROM T_ETL_ADM_MAP_DETAILS WHERE ETL_HEADER_CODE_FK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(headerId);
		ResultSet rs = executeSQL(SQL, parameters);
		EtlMapDetailsVO etlMapDetailsVO = null;
		try{
			while(rs.next()){
				etlMapDetailsVO = new EtlMapDetailsVO();
                etlMapDetailsVO.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
                etlMapDetailsVO.setEtlMapDetailsPkId(convertByteToString(rs.getBytes("ETL_MAP_DETAILS_PK_ID")));
                etlMapDetailsVO.setDetailsMapName(rs.getString("DETAILS_MAP_NAME_G11N_BIG_TXT"));
                etlMapDetailsVO.setEtlMapDepFkId(convertByteToString(rs.getBytes("ETL_MAP_FK_ID")));
                etlMapDetailsVO.setEtlHeaderDepFkId(rs.getString("ETL_HEADER_CODE_FK_ID"));
                etlMapDetailsVO.setPropogateVerion(rs.getBoolean("IS_PROPOGATE_VERSION"));
                etlMapDetailsVO.setTargetDoaCodeFkId(rs.getString("TARGET_DOA_CODE_FK_ID"));
                etlMapDetailsVO.setMapSourceDoaExpn(rs.getString("MAP_SOURCE_EXPN_BIG_TXT"));
                etlMapDetailsVO.setMapLookupDoaExpn(rs.getString("MAP_LOOKUP_EXPN_BIG_TXT"));
                etlMapDetailsVO.setAssociatedArtefactLocationDoaCodeFkId(rs.getString("ASSOCIATED_ARTEFACT_LOCATION_DOA_CODE_FK_ID"));
                etlMapDetailsVOs.add(etlMapDetailsVO);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return etlMapDetailsVOs;
	}
	
	@Override
	public String getprocessElementIdByBtDo(String sourceDoCodeFkId, String bt) {
		String SQL = "SELECT BT_PROCESS_ELEMENT_CODE_FK_ID FROM T_BT_ADM_DO_SPEC WHERE DOMAIN_OBJECT_CODE_FK_ID = ? AND BASE_TEMPLATE_DEP_CODE_FK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(sourceDoCodeFkId);
		parameters.add(bt);
		ResultSet rs = executeSQL(SQL, parameters);
		String processElementId = null;
		try{
			if(rs.next()){
				processElementId = rs.getString("BT_PROCESS_ELEMENT_CODE_FK_ID");
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return processElementId;
	}

	@Override
	public List<Map<String, Object>> getDataListFromTable(String sql, EntityMetadataVOX entityMetadataVOX) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Map<String, Object>> result = new LinkedList<>();
		try{
			Connection c = getConnection();
			ps = c.prepareStatement(sql);
			rs = ps.executeQuery();
            LOG.debug(sql);
			ResultSetMetaData meta = rs.getMetaData();
			while(rs.next()){
				Map<String, Object> row = new HashMap<>();
				for(int columnIndex = 1; columnIndex <= meta.getColumnCount(); columnIndex++){
					String columnLabel = meta.getColumnLabel(columnIndex);
					if(columnLabel != null && !columnLabel.isEmpty()){
						EntityAttributeMetadata attr = entityMetadataVOX.getAttributeByName(columnLabel);
						Object columnValue = getColumnValue(rs, attr, columnLabel, columnIndex);
						if(columnValue instanceof ObjectNode && !attr.isGlocalizedFlag() && attr.getDbDataTypeCode().matches("T_BLOB")){
							JsonNode node = (JsonNode) columnValue;
							Iterator<Entry<String, JsonNode>> nodeIterator = node.fields();
							while(nodeIterator.hasNext()){
								Entry<String, JsonNode> field = nodeIterator.next();
								String fieldName = field.getKey();
								Object fieldValue = field.getValue();
								row.put(fieldName, fieldValue);
							}
						}
						else row.put(meta.getColumnLabel(columnIndex), columnValue);
					}
				}
				result.add(row);
			}
		}catch(SQLException | IOException e){
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public List<Map<String, Object>> getDataListFromTableMaster(String sql, MasterEntityMetadataVOX entityMetadataVOX) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Map<String, Object>> result = new LinkedList<>();
		try{
			Connection c = getConnection();
			ps = c.prepareStatement(sql);
            LOG.debug(sql);
			rs = ps.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			while(rs.next()){
				Map<String, Object> row = new HashMap<>();
				for(int columnIndex = 1; columnIndex <= meta.getColumnCount(); columnIndex++){
					String columnLabel = meta.getColumnLabel(columnIndex);
					if(columnLabel != null && !columnLabel.isEmpty()){
//						String column = columnLabel.split("\\.")[1];
						MasterEntityAttributeMetadata attr = entityMetadataVOX.getAttributeByColumnName(columnLabel);
						Object columnValue = getColumnValueMaster(rs, attr, columnLabel, columnIndex);
						if(columnValue instanceof ObjectNode && !attr.isGlocalized() && attr.getDbDataTypeCode().matches("T_BLOB")){
							JsonNode node = (JsonNode) columnValue;
							Iterator<Entry<String, JsonNode>> nodeIterator = node.fields();
							while(nodeIterator.hasNext()){
								Entry<String, JsonNode> field = nodeIterator.next();
								String fieldName = field.getKey();
								Object fieldValue = field.getValue();
								row.put(fieldName, fieldValue);
							}
						}
						else row.put(meta.getColumnLabel(columnIndex), columnValue);
					}
				}
				result.add(row);
			}
		}catch(SQLException | IOException e){
			e.printStackTrace();
		}
		return result;
	}
	
	private Object getColumnValue(ResultSet rs, EntityAttributeMetadata attribute, String columnLabel,
			int columnIndex) throws SQLException, IOException {
		String dbDataType = attribute.getDbDataTypeCode();
		EntityDataTypeEnum dataType = EntityDataTypeEnum.valueOf(dbDataType);
		Object result = null;
		switch(dataType){
			case DATE_TIME:
				Timestamp timestamp = rs.getTimestamp(columnIndex);
				if (timestamp != null)
					result = timestamp.toString();
				break;
			case BOOLEAN:
				result = rs.getBoolean(columnIndex);
				break;
			case POSITIVE_INTEGER:
			case NEGATIVE_INTEGER:
			case BIG_INTEGER:
			case POSITIVE_DECIMAL:
			case NEGATIVE_DECIMAL:
			case CURRENCY:
			case LOCATION_LATITUDE:
			case LOCATION_LONGITUDE:
			case T_CODE:
			case T_ICON:
			case TEXT:
				result = rs.getString(columnIndex) == null ? null : rs.getString(columnIndex).replace("'", "''");
				break;
			case ATTACHMENT:
//			case T_ATTACH:
			case AUDIO:
			case VIDEO:
			case IMAGE:
			case T_ID:
			case T_IMAGEID:
				result = Util.convertByteToString(rs.getBytes(columnIndex));
				break;
			case T_BLOB:
				result = Util.getBlobJson(rs.getBlob(columnIndex));
				break;
			default:
				result = rs.getObject(columnIndex);
				break;
		}
		return result;
	}
	private Object getColumnValueMaster(ResultSet rs, MasterEntityAttributeMetadata attribute, String columnLabel,
			int columnIndex) throws SQLException, IOException {
		String dbDataType = attribute.getDbDataTypeCode();
		EntityDataTypeEnum dataType = EntityDataTypeEnum.valueOf(dbDataType);
		Object result = null;
		switch(dataType){
		case DATE_TIME:
			Timestamp timestamp = rs.getTimestamp(columnIndex);
			if (timestamp != null)
				result = timestamp.toString();
			break;
		case BOOLEAN:
			result = rs.getBoolean(columnIndex);
			break;
		case POSITIVE_INTEGER:
		case NEGATIVE_INTEGER:
		case BIG_INTEGER:
		case POSITIVE_DECIMAL:
		case NEGATIVE_DECIMAL:
		case CURRENCY:
		case LOCATION_LATITUDE:
		case LOCATION_LONGITUDE:
		case T_CODE:
		case T_ICON:
		case TEXT:
			result = rs.getString(columnIndex) == null ? null : rs.getString(columnIndex).replace("'", "''");
			break;
		case ATTACHMENT:
//			case T_ATTACH:
		case AUDIO:
		case VIDEO:
		case IMAGE:
		case T_ID:
		case T_IMAGEID:
			result = Util.convertByteToString(rs.getBytes(columnIndex));
			break;
		case T_BLOB:
			result = Util.getBlobJson(rs.getBlob(columnIndex));
			break;
		default:
			result = rs.getObject(columnIndex);
			break;
		}
		return result;
	}
	private String convertByteToString(byte[] input){
		if(input == null)
			return null;
		return Hex.encodeHexString(input).toUpperCase();
	}
	private JsonNode getBlobJson(Blob input) throws IOException, SQLException{
		if(input == null)
			return null;
		ObjectMapper mapper = new ObjectMapper();
		JsonNode result = mapper.readTree(input.getBinaryStream());
		return result;
	}
	@Override
	public void insert(String tableName, Map<String, Object> columnNameValueMap) {
		String SQL = getInsertSQL(tableName, columnNameValueMap);
		List<Object> parameters = new ArrayList<>();
		try{
			Connection con = null;
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(SQL);
			LOG.debug(SQL);
			for (int parameterIndex =0; parameterIndex<parameters.size(); parameterIndex++){
				ps.setObject(parameterIndex+1, parameters.get(parameterIndex));
			}
			ps.executeUpdate();
		} catch (SQLException e) {
			DBUploadErrorLog.addError(System.lineSeparator()+SQL+System.lineSeparator()+"SQL State => "+e.getSQLState()+System.lineSeparator()+"Error message => "+e.getMessage()+System.lineSeparator()+"Error code =>"+e.getErrorCode());
			LOG.debug(SQL);
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unchecked")
	private String getInsertSQL(String tableName, Map<String, Object> columnNameValueMap) {
		StringBuilder columnNames = new StringBuilder();
		StringBuilder columnValues = new StringBuilder();
		int counter = 0;
		for(Map.Entry<String, Object> entry :columnNameValueMap.entrySet()){
			if(counter != 0){
				columnNames.append(",");
				columnValues.append(",");
			}
			String columnName = entry.getKey();
			columnNames.append(columnName);
			if(columnName.endsWith("BLOB")){
				Map<String, GenericColumnValue> blobColumnValueMap = (Map<String,GenericColumnValue>)entry.getValue();
				String blobColumnSQLString = getBlobColumnCreateSQL(blobColumnValueMap);
				columnValues.append(blobColumnSQLString);
			}else{
				//if it is not a blob
				GenericColumnValue column = (GenericColumnValue)entry.getValue();
				String columnValue = Util.getStringTobeInserted(column.getDbDataType(), column.getColumnValue(),false);
                column.setActualValue(columnValue);
				columnValues.append(columnValue);
			}
			counter++;
		}
		//Now we have column and values map
		StringBuilder SQL = new StringBuilder("INSERT INTO ");
		SQL.append(tableName)
		.append(" (")
		.append(columnNames)
		.append(") VALUES (")
		.append(columnValues)
		.append(");");
		return SQL.toString();
	}
	private String getBlobColumnCreateSQL(Map<String, GenericColumnValue> blobColumnValueMap) {
		StringBuilder SQLSubString = new StringBuilder("COLUMN_CREATE(");
		if(blobColumnValueMap==null || blobColumnValueMap.isEmpty()){
			SQLSubString.append("'"+"dummy"+"',null");
			SQLSubString.append(")");
			return SQLSubString.toString();
		}
			
		int count = 0;
		for(Map.Entry<String,GenericColumnValue> entry : blobColumnValueMap.entrySet()){
			if(count != 0){
				SQLSubString.append(",");
			}
			String columnName = entry.getKey();
			//Check here with db datatype
			if(columnName.endsWith("blob")){
				String value =  getBlobColumnCreateSQL(entry.getValue().getBlobColumnToValueMap());
				SQLSubString.append("'"+entry.getKey()+"',"+value);
			}else{
				String dummyColumnName = entry.getValue().getDummyColumnName();
				if(dummyColumnName.endsWith("G11N_BIG_TXT")){
					String dbDataType = entry.getValue().getDbDataType();
					String columnValue = entry.getValue().getColumnValue();
					String value = Util.getStringTobeInserted(dbDataType, columnValue, true);
					//en_US is hardcoded here. should be removed. This can be removed with the help of providing it as a standard field
					SQLSubString.append("'"+entry.getKey()+"',"+"COLUMN_CREATE('en_US',"+value+")");
				}else{
					String dbDataType = entry.getValue().getDbDataType();
					String columnValue = entry.getValue().getColumnValue();
					String value = Util.getStringTobeInserted(dbDataType, columnValue, true);
					SQLSubString.append("'"+entry.getKey()+"',"+value);
				}
			}
			count++;
//			insertJsonToColumnType(entry.getKey(),dbDataType);
		}
		SQLSubString.append(")");
		return SQLSubString.toString();
	}
	@Override
	public void updateColumns(UpdateColumn updateColumn, String binaryValue) {
		String SQL = getUpdateSQLStmt(updateColumn,binaryValue);
		List<Object> parameters = new ArrayList<>();
		try{
			Connection con = null;
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(SQL);
			LOG.debug(SQL);
			for (int parameterIndex =0; parameterIndex<parameters.size(); parameterIndex++){
				ps.setObject(parameterIndex+1, parameters.get(parameterIndex));
			}
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			DBUploadErrorLog.addError(System.lineSeparator()+SQL+System.lineSeparator()+"SQL State => "+e.getSQLState()+System.lineSeparator()+"Error message => "+e.getMessage()+System.lineSeparator()+"Error code =>"+e.getErrorCode());
		}
	}
	private String getUpdateSQLStmt(UpdateColumn updateColumn, String inputValue) {
		String value = null;
		StringBuilder SQL = new StringBuilder("UPDATE ");
		SQL.append(updateColumn.getTableName())
		.append(" SET ");
		if(updateColumn.getColumnName().endsWith("BLOB")){
			value = Util.getStringTobeInserted(updateColumn.getDbDataType(), inputValue,true);
			SQL.append(updateColumn.getColumnName())
			.append("=COLUMN_ADD(")
			.append(updateColumn.getColumnName())
			.append(",'")
			.append(updateColumn.getJsonName())
			.append("',")
			.append(value)
			.append(")");
//			insertJsonToColumnType(updateColumn.getJsonName(),updateColumn.getDbDataType());
		}else{
			value = Util.getStringTobeInserted(updateColumn.getDbDataType(), inputValue,false);
			SQL.append(updateColumn.getColumnName())
			.append("=")
			.append(value);
		}
		SQL.append(" WHERE ")
		.append(updateColumn.getPrimarykeyColumnName())
		.append(" = ");
		if(updateColumn.getPrimaryKeyDbDataType().equals("T_ID")){
			SQL.append(updateColumn.getPrimaryKeyId());
		}else{
			SQL.append("'"+updateColumn.getPrimaryKeyId()+"'");
		}
		SQL.append(";");
		return SQL.toString();
	}
	@Override
	public int executeUpdate(String SQL, List<Object> parameters) throws SQLException {
		return super.executeUpdate(SQL, parameters);
	}
	@Override
	public ResultSet executeSQL(String SQL, List<Object> parameters){
		return super.executeSQL(SQL, parameters);
	}

	@Override
	public String getSubjectUserId(StringBuilder sql, String primaryKeyDBDataType) {
		String subjectUserId = null;
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = super.executeSQL(sql.toString(), parameters);
		try {
			if (rs.next()){
				if(primaryKeyDBDataType.equals("T_ID")) {
					subjectUserId = Util.convertByteToString(rs.getBytes(1));
				}else{
					subjectUserId = rs.getString(1);
				}
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return subjectUserId;
	}

	@Override
	public Map<String, List<String>> getChildrenListForParentPersons(List<String> rootPersonPrimaryIds) {
		Map<String, List<String>> parentChildrenListMap = new HashMap<>();
		StringBuilder sql = new StringBuilder("SELECT A.PERSON_DEP_FK_ID, P.RELATED_PERSON_FK_ID FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP P INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE R\n" +
				"    ON P.RELATIONSHIP_TYPE_CODE_FK_ID = R.RELATIONSHIP_TYPE_CODE_PK_ID AND R.IS_DIRECT_MANAGER INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT A\n" +
				"    ON A.PERSON_JOB_ASSIGNMENT_PK_ID = P.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND A.IS_PRIMARY = TRUE WHERE P.RELATED_PERSON_FK_ID IN ( ");
		List<Object> parameters = new ArrayList<>();

		for (String rootPersonPrimaryId : rootPersonPrimaryIds){
			sql.append("x?, ");
			parameters.add(rootPersonPrimaryId);
		}
		sql.replace(sql.length()-2, sql.length(), ");");
		ResultSet rs = executeSQL(sql.toString(), parameters);
		try {
			while(rs.next()){
				String personFkId = Util.convertByteToString(rs.getBytes("A.PERSON_DEP_FK_ID"));
				String relatedPersonFkId = Util.convertByteToString(rs.getBytes("P.RELATED_PERSON_FK_ID"));
				if (parentChildrenListMap.containsKey(relatedPersonFkId)){
					parentChildrenListMap.get(relatedPersonFkId).add(personFkId);
				}
				else {
					List<String> childrenList = new ArrayList<>();
					childrenList.add(personFkId);
					parentChildrenListMap.put(relatedPersonFkId, childrenList);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return parentChildrenListMap;
	}

	@Override
	public Map<String, String> get_Access_Token(String userId){
		Map<String, String> result = new HashMap<>();
		String SQL="SELECT GMAIL_ID,REFRESH_TOKEN FROM TAPPLENT_BOOT_SCHEMA_STAGING.T_TEMP_GMAIL_REFRESH_TOKEN WHERE GMAIL_ID=?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(userId);
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				String gmailId = rs.getString("GMAIL_ID");
				String refreshToken = rs.getString("REFRESH_TOKEN");
				result.put(gmailId, refreshToken);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

    @Override
    public ETLMetaHelperVO getMetaByDoName(String doName) {
        ETLMetaHelperVO result = null;
        String SQL = "SELECT T_MT_ADM_DOMAIN_OBJECT.DB_TABLE_NAME_TXT,DOMAIN_OBJECT_CODE_PK_ID,DB_COLUMN_NAME_TXT, DB_DATA_TYPE_CODE_FK_ID, SUBJECT_PERSON_DOA_FULL_PATH_EXPN   FROM T_MT_ADM_DOMAIN_OBJECT INNER JOIN T_MT_ADM_DO_ATTRIBUTE ON IS_DB_PRIMARY_KEY = 1 AND DOMAIN_OBJECT_CODE_FK_ID = DOMAIN_OBJECT_CODE_PK_ID WHERE DOMAIN_OBJECT_CODE_PK_ID=?";
        List<Object> parameters = new ArrayList<>();
        parameters.add(doName);
        ResultSet rs = executeSQL(SQL, parameters);
        try{
            if(rs.next()) {
				String tableName = rs.getString(1);
				String doCode = rs.getString(2);
				//Boolean isAccessControlGovernend = rs.getBoolean(3);
				String dbPKCol = rs.getString(3);
				String dbDatatype = rs.getString(4);
				String subjectUserPath = rs.getString(5);
				result = new ETLMetaHelperVO(doCode, tableName, dbPKCol, dbDatatype);
				result.setSubjectUserPath(subjectUserPath);
			}
        }catch (SQLException e) {
            e.printStackTrace();
        }
        if(result!=null){
            String doaSQL = "SELECT DOMAIN_OBJECT_ATTRIBUTE_CODE_PK_ID FROM T_MT_ADM_DO_ATTRIBUTE WHERE DOMAIN_OBJECT_CODE_FK_ID = ?;";
            List<String> doaCodeList = new ArrayList<>();
            ResultSet rsDOA = executeSQL(doaSQL, parameters);
            try{
                while (rsDOA.next()){
                    String doaCode = rsDOA.getString(1);
                    doaCodeList.add(doaCode);
                }
                result.setDoAttrCodeList(doaCodeList);
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public void insertRowAccessCondition(String rowAccessPkId, String personId, String doName, Object value, Object subjectUserId, boolean isUpdatePermissioned, boolean isDeletePermissioned, boolean isDeleted, String createdDateTime) throws SQLException {
        String columnValue = value.toString();
        StringBuilder SQL = new StringBuilder("INSERT INTO T_PFM_IEU_AM_ROW_RESOLVED (AM_ROW_RESOLVED_PK_ID, WHO_PERSON_FK_ID, DOMAIN_OBJECT_CODE_FK_ID, WHAT_DB_PRIMARY_KEY_ID, WHOSE_SUBJECT_USER_PERSON_FK_ID, IS_UPDATE_PERMITTED, IS_DELETE_PERMITTED, IS_DELETED, CREATED_DATETIME)\n" +
                "VALUES (") ;
        SQL.append(Util.getStringTobeInserted("T_ID",rowAccessPkId,false)).append(",")
                .append(Util.getStringTobeInserted("T_ID",personId,false)).append(",")
                .append(Util.getStringTobeInserted("TEXT",doName,false)).append(",")
                .append(Util.getStringTobeInserted("T_ID",value,false)).append(",")
                .append(Util.getStringTobeInserted("T_ID",subjectUserId,false)).append(",")
                .append(Util.getStringTobeInserted("BOOLEAN",isUpdatePermissioned,false)).append(",")
                .append(Util.getStringTobeInserted("BOOLEAN",isDeletePermissioned,false)).append(",")
                .append(Util.getStringTobeInserted("BOOLEAN",isDeleted,false)).append(",")
                .append(Util.getStringTobeInserted("DATE_TIME",createdDateTime,false))
                .append(");");
        List<Object> parameters = new ArrayList<>();
        executeUpdate(SQL.toString(),parameters);
    }

    @Override
    public void insertColumnAccessCondition(String rowAccessPkId, List<String> doAttrCodeList, boolean isReadPermissioned, boolean isUpdatePermissioned, boolean isDeleted, String createdDateTime) throws SQLException {
        for(String doaCode : doAttrCodeList){
            StringBuilder SQL = new StringBuilder("INSERT INTO T_PFM_IEU_AM_COL_RESOLVED (AM_COL_RESOLVED_PK_ID, AM_ROW_RESOLVED_FK_ID, DOA_CODE_FK_ID, IS_READ_PERMITTED, IS_UPDATE_PERMITTED, IS_DELETED, CREATED_DATETIME) \n" +
                    "    VALUES (" );
            SQL.append(Util.getStringTobeInserted("T_ID", Common.getUUID(),false)).append(",")
                    .append(Util.getStringTobeInserted("T_ID",rowAccessPkId,false)).append(",")
                    .append(Util.getStringTobeInserted("TEXT",doaCode,false)).append(",")
                    .append(Util.getStringTobeInserted("BOOLEAN",isReadPermissioned,false)).append(",")
                    .append(Util.getStringTobeInserted("BOOLEAN",isUpdatePermissioned,false)).append(",")
                    .append(Util.getStringTobeInserted("BOOLEAN",isDeleted,false)).append(",")
                    .append(Util.getStringTobeInserted("DATE_TIME",createdDateTime,false))
                    .append(");");
            List<Object> parameters = new ArrayList<>();
            executeUpdate(SQL.toString(),parameters);
        }
    }

	@Override
	public void addG11nStoredProcedure() throws SQLException {
		String spDropStmt = "DROP FUNCTION getG11nValue;";
		String spCreateStmt = "CREATE FUNCTION getG11nValue(localeCode VARCHAR(200), inputValue TEXT)\n" +
				"  RETURNS TEXT\n" +
				"  BEGIN\n" +
				"    DECLARE startLocaleIndex INT;\n" +
				"    DECLARE midLocaleIndex INT;\n" +
				"    DECLARE endLocaleIndex INT;\n" +
				"    IF inputValue IS NOT NULL THEN\n" +
				"      SET startLocaleIndex = LOCATE(CONCAT('\"', localeCode, '\"'), inputValue);\n" +
				"      IF startLocaleIndex <> 0\n" +
				"      THEN\n" +
				"        -- value exists for this particular locale. Now get the value for that locale.\n" +
				"        SET midLocaleIndex = LOCATE('\":\"', inputValue, startLocaleIndex);\n" +
				"        -- midlocale + 3 is the actual place from where the value starts.\n" +
				"        SET endLocaleIndex = LOCATE('\"', inputValue, midLocaleIndex + 3);\n" +
				"        RETURN REPLACE(SUBSTRING(inputValue, midLocaleIndex + 3, endLocaleIndex - midLocaleIndex - 3), '|%', '\"');\n" +
				"      END IF;\n" +
				"    ELSE\n" +
				"      RETURN NULL;\n" +
				"    END IF;\n" +
				"  END ;";
		executeUpdate(spDropStmt, new ArrayList<>());
		executeUpdate(spCreateStmt, new ArrayList<>());
	}

	@Override
	public DOADetails getDOADetailsByDOAName(String doaName) {
		DOADetails doaDetails = new DOADetails();
		String SQL = "SELECT DB_COLUMN_NAME_TXT,DB_DATA_TYPE_CODE_FK_ID,CONTAINER_BLOB_DB_COLUMN_NAME_TXT,DB_TABLE_NAME_TXT,DOMAIN_OBJECT_CODE_FK_ID, TO_DB_TABLE_NAME_TXT,TO_DB_COLUMN_NAME_TXT  FROM T_MT_ADM_DO_ATTRIBUTE WHERE DOMAIN_OBJECT_ATTRIBUTE_CODE_PK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(doaName);
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			rs.next();
			doaDetails.setColumnName(rs.getString("DB_COLUMN_NAME_TXT"));
			doaDetails.setContainerBlobName(rs.getString("CONTAINER_BLOB_DB_COLUMN_NAME_TXT"));
			doaDetails.setDbDataType(rs.getString("DB_DATA_TYPE_CODE_FK_ID"));
			doaDetails.setTableName(rs.getString("DB_TABLE_NAME_TXT"));
			doaDetails.setDoCode(rs.getString("DOMAIN_OBJECT_CODE_FK_ID"));
			doaDetails.setToTableName("TO_DB_TABLE_NAME_TXT");
			doaDetails.setToColumnName("TO_DB_COLUMN_NAME_TXT");
			doaDetails.setDoaName(doaName);
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return doaDetails;
	}
	@Override
	public Map<String, String> getDOADetailByDOAName(String doaName) {
		Map<String, String> doaDetails = new HashMap<>();
		String SQL = "SELECT DB_DATA_TYPE_CODE_FK_ID, JSON_NAME_TXT FROM T_MT_ADM_DO_ATTRIBUTE WHERE DOMAIN_OBJECT_ATTRIBUTE_CODE_PK_ID = ? ;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(doaName);
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			if(rs.next()){
				doaDetails.put("dataType", rs.getString("DB_DATA_TYPE_CODE_FK_ID"));
				doaDetails.put("jsonName", rs.getString("JSON_NAME_TXT"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return doaDetails;
	}

	@Override
	public void updateParentCanvasTxn() throws SQLException {
		String SQL ="UPDATE T_UI_ADM_CANVAS_TXN TXN1 LEFT JOIN T_UI_ADM_CANVAS_MASTER MST ON TXN1.CANVAS_MASTER_FK_ID = MST.CANVAS_MASTER_PK_ID LEFT JOIN T_UI_ADM_CANVAS_TXN TXN2 ON MST.PARENT_CANVAS_MASTER_FK_ID = TXN2.CANVAS_MASTER_FK_ID "
						+ "SET TXN1.PARENT_CANVAS_TXN_FK_ID = TXN2.CANVAS_TXN_PK_ID WHERE TXN1.PAGE_FK_ID = TXN2.PAGE_FK_ID;";
		List<Object> parameters = new ArrayList<>();
		executeUpdate(SQL, parameters);
	}

	@Override
	public List<String> getHierarchialRoots(String tableName, String primaryKeyColumnName,
			String selfJoinParentColumnName) {
		List<String> primaryKeyColumnValues = new ArrayList<>();
		String SQL = "SELECT "+primaryKeyColumnName+" as primaryKey FROM "+tableName+ " WHERE "+selfJoinParentColumnName+" IS NULL;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				String primaryKeyValue = Util.convertByteToString(rs.getBytes("primaryKey"));
				primaryKeyColumnValues.add(primaryKeyValue);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return primaryKeyColumnValues;
	}

	@Override
	public List<String> getChildrenListForParent(String tableName, String primaryKeyColumnName,
			String selfJoinParentColumnName, String parentId) {
		List<String> childrenPrimaryKeyValues = new ArrayList<>();
		String SQL = "SELECT "+primaryKeyColumnName+" as primaryKey FROM "+tableName+ " WHERE "+selfJoinParentColumnName+" = x?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(Util.remove0x(parentId));
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				String childrenPrimaryKeyValue = Util.convertByteToString(rs.getBytes("primaryKey"));
				childrenPrimaryKeyValues.add(childrenPrimaryKeyValue);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return childrenPrimaryKeyValues;
	}

	@Override
	public void updateLeftRightAndHierachyValues(String tableName, String primaryKeyColumnName,
			String selfJoinParentColumnName, String parentId, int leftValue, int rightValue, String hierarchyId) throws SQLException {
		String SQL = "UPDATE "+ tableName +" SET INTRNL_LEFT_POS_INT = "+ leftValue +" ,INTRNL_RIGHT_POS_INT = "+rightValue+ " ,INTRNL_HIERARCHY_ID = "+hierarchyId+" WHERE "+primaryKeyColumnName+" = "+parentId+";";
		List<Object> parameters = new ArrayList<>();
		executeUpdate(SQL, parameters);
	}

	@Override
	public Map<String, Object> getUnderlyingExistingRecord(String SQL, EntityMetadataVOX targetEntityMetadataVOX) {
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		Map<String, Object> record = null;
		String pkColumnName = targetEntityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName();
		String versionColumnName = targetEntityMetadataVOX.getVersoinIdAttribute().getDbColumnName();
		try {
			if(rs.next()){
				record = new HashMap<>();
				record.put(pkColumnName, convertByteToString(rs.getBytes(pkColumnName)));
				record.put(versionColumnName, convertByteToString(rs.getBytes(versionColumnName)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return record;
	}

	@Override
	public void deleteAllFromTargetTable(String dbTableName) throws SQLException {
		String SQL = "DELETE FROM " + dbTableName;
		executeUpdate(SQL, null);
	}

	@Override
	public SearchResult search(String sql, Deque<Object> parameter) {
		SearchResult sr = new SearchResult();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			Connection c = getConnection();
			ps = c.prepareStatement(sql);
			if(parameter!=null){
				int i= 1;
				for(Object param : parameter){
					LOG.debug("Select query parameter number "+i+" => "+param.toString());
					ps.setObject(i,param);
					i++;
				}
			}
			LOG.debug(sql);
			rs = ps.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			List<Map<String, Object>> result = new LinkedList<>();
			while(rs.next()){
				Map<String, Object> row = new HashMap<>();
				for(int i = 1; i <= meta.getColumnCount(); i++){
					row.put(meta.getColumnLabel(i), rs.getObject(i));
				}
				result.add(row);
			}
			sr.setData(result);
		} catch (SQLException e){
			LOG.error(e.getMessage(), e);
		}
		return sr;
	}
	private String getG11nValue(String input){
//		return Util.getG11nValue(input, null);
		return input;
	}

	@Override
	public Map<String, DOADetails> getDoaMetaByDoName(String doName) {
		Map<String, DOADetails> doaMetaMap = new HashMap<>();
		String SQL = "SELECT DOMAIN_OBJECT_ATTRIBUTE_CODE_PK_ID,DB_COLUMN_NAME_TXT,DB_DATA_TYPE_CODE_FK_ID,CONTAINER_BLOB_DB_COLUMN_NAME_TXT FROM T_MT_ADM_DO_ATTRIBUTE WHERE DOMAIN_OBJECT_CODE_FK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(doName);
		ResultSet rs = executeSQL(SQL, parameters);
		try{
			while(rs.next()){
				DOADetails doaDetails = new DOADetails();
				doaDetails.setColumnName(rs.getString("DB_COLUMN_NAME_TXT"));
				doaDetails.setContainerBlobName(rs.getString("CONTAINER_BLOB_DB_COLUMN_NAME_TXT"));
				doaDetails.setDbDataType(rs.getString("DB_DATA_TYPE_CODE_FK_ID"));
				doaDetails.setDoaName(rs.getString("DOMAIN_OBJECT_ATTRIBUTE_CODE_PK_ID"));
				doaMetaMap.put(doaDetails.getDoaName(), doaDetails);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return doaMetaMap;
	}

	@Override
	public List<DOADetails> getBTDOADetails() {
		List<DOADetails> doaDetailsList = new ArrayList<>();
		String SQL = "SELECT * FROM T_BT_ADM_DO_ATTRIBUTE_SPEC WHERE CONTAINER_BLOB_DB_COLUMN_NAME_TXT IS NULL;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
                DOADetails doaDetails = new DOADetails();
                doaDetails.setColumnName(rs.getString("DB_COLUMN_NAME_TXT"));
                doaDetails.setDoaName(rs.getString("DO_ATTRIBUTE_CODE_FK_ID"));
                doaDetails.setContainerBlobName(rs.getString("CONTAINER_BLOB_DB_COLUMN_NAME_TXT"));
                doaDetails.setDbDataType(rs.getString("DB_DATA_TYPE_CODE_FK_ID"));
                doaDetails.setFunctionalPrimaryKeyFlag(rs.getBoolean("IS_FUNCTIONAL_PRIMARY_KEY_FLAG"));
                doaDetails.setLocalSearchableFlag(rs.getBoolean("IS_LOCAL_SEARCHABLE_FLAG"));
                doaDetails.setRelationFlag(rs.getBoolean("IS_RELATION_FLAG"));
                doaDetails.setTableName(rs.getString("DB_TABLE_NAME_TXT"));
                doaDetails.setDoCode(rs.getString("DOMAIN_OBJECT_CODE_FK_ID"));
                doaDetailsList.add(doaDetails);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return doaDetailsList;
	}

	@Override
	public List<ETLMetaHelperVO> getAccessControlGovernedDOs() {
		List<ETLMetaHelperVO> result = new ArrayList<>();
		String SQL = "SELECT DO1.DOMAIN_OBJECT_CODE_PK_ID, DO1.DB_TABLE_NAME_TXT, DO1.AM_ROW_INT_TABLE_TXT, DO1.AM_ROW_RSLVD_TABLE_TXT, DO1.AM_COL_RSLVD_TABLE_TXT, DO1.IS_ACCESS_CONTROL_GOVERNED, DO1.SUBJECT_PERSON_DOA_FULL_PATH_EXPN, MTPE.META_PROCESS_ELEMENT_CODE_PK_ID,\n" +
				"  DO2.AM_ROW_INT_TABLE_TXT, DO2.AM_ROW_RSLVD_TABLE_TXT, DO2.AM_COL_RSLVD_TABLE_TXT, MTPE2.META_PROCESS_ELEMENT_CODE_PK_ID, DB_COLUMN_NAME_TXT FROM T_MT_ADM_DOMAIN_OBJECT DO1\n" +
				"  INNER JOIN T_MT_ADM_DO_ATTRIBUTE ATTR ON DO1.DOMAIN_OBJECT_CODE_PK_ID = ATTR.DOMAIN_OBJECT_CODE_FK_ID AND IS_DB_PRIMARY_KEY = 1\n" +
				"  INNER JOIN T_MT_ADM_PROCESS_ELEMENT MTPE ON DO1.DOMAIN_OBJECT_CODE_PK_ID = MTPE.MT_DOMAIN_OBJECT_CODE_FK_ID\n" +
				"  LEFT JOIN T_MT_ADM_PROCESS_ELEMENT MTPE2 ON MTPE2.META_PROCESS_ELEMENT_CODE_PK_ID = MTPE.SLF_PRNT_MT_PROCESS_ELEMENT_CODE_FK_ID\n" +
				"  LEFT JOIN T_MT_ADM_DOMAIN_OBJECT DO2 ON DO2.DOMAIN_OBJECT_CODE_PK_ID = MTPE2.MT_DOMAIN_OBJECT_CODE_FK_ID\n" +
				"  WHERE DO1.IS_ACCESS_CONTROL_GOVERNED = TRUE ;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		getEtlMetaHelperResult(SQL, result, rs);
		return result;
	}

	@Override
	public List<ETLMetaHelperVO> getSubjectUserDrivenDOs() {
		List<ETLMetaHelperVO> result = new ArrayList<>();
		String SQL = "SELECT T_MT_ADM_DOMAIN_OBJECT.DB_TABLE_NAME_TXT,DOMAIN_OBJECT_CODE_PK_ID,DB_COLUMN_NAME_TXT,SUBJECT_PERSON_DOA_FULL_PATH_EXPN FROM T_MT_ADM_DOMAIN_OBJECT INNER JOIN T_MT_ADM_DO_ATTRIBUTE ON IS_DB_PRIMARY_KEY = 1 AND DOMAIN_OBJECT_CODE_FK_ID = DOMAIN_OBJECT_CODE_PK_ID WHERE SUBJECT_PERSON_DOA_FULL_PATH_EXPN IS NOT NULL;";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		getEtlMetaHelperResult(SQL, result, rs);
		return result;
	}

	@Override
	public List<String> getHierarchicalDOs() {
		List<String> dos = new ArrayList<>();
		String SQL = "SELECT DISTINCT DOMAIN_OBJECT_CODE_FK_ID FROM T_MT_ADM_DO_ATTRIBUTE WHERE IS_SELF_JOIN_PARENT AND DB_TABLE_NAME_TXT not like '%ETL%';";
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while (rs.next()){
                dos.add(rs.getString(1));
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dos;
	}

	private void getEtlMetaHelperResult(String sql, List<ETLMetaHelperVO> result, ResultSet rs) {
		try {
			while (rs.next()) {
				ETLMetaHelperVO metaHelperVO = new ETLMetaHelperVO();
				metaHelperVO.setTableName(rs.getString("DO1.DB_TABLE_NAME_TXT"));
				metaHelperVO.setDoName(rs.getString("DOMAIN_OBJECT_CODE_PK_ID"));
				//metaHelperVO.setAccessControlApplicable(rs.getBoolean("IS_ACCESS_CONTROL_GOVERNED"));
				metaHelperVO.setPrimaryKeyColumnName(rs.getString("DB_COLUMN_NAME_TXT"));
				metaHelperVO.setSubjectUserPath(rs.getString("DO1.SUBJECT_PERSON_DOA_FULL_PATH_EXPN"));
				metaHelperVO.setMtPE(rs.getString("MTPE.META_PROCESS_ELEMENT_CODE_PK_ID"));
				metaHelperVO.setParentMtPE(rs.getString("MTPE2.META_PROCESS_ELEMENT_CODE_PK_ID"));
				metaHelperVO.setParentIntermediateTableName(rs.getString("DO2.AM_ROW_INT_TABLE_TXT"));
				metaHelperVO.setIntermediateTableName(rs.getString("DO1.AM_ROW_INT_TABLE_TXT"));
				metaHelperVO.setParentRowResolvedTable(rs.getString("DO2.AM_ROW_RSLVD_TABLE_TXT"));
				metaHelperVO.setRowResolvedTable(rs.getString("DO1.AM_ROW_RSLVD_TABLE_TXT"));
				metaHelperVO.setParentColResolvedTable(rs.getString("DO2.AM_COL_RSLVD_TABLE_TXT"));
				metaHelperVO.setColResolvedTable(rs.getString("DO1.AM_COL_RSLVD_TABLE_TXT"));
				result.add(metaHelperVO);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	@Override
	public void populateAccessControlforDO(ETLMetaHelperVO helperVO) throws SQLException {
		String rowAC = "INSERT INTO T_PFM_IEU_AM_ROW_RESOLVED (AM_ROW_RESOLVED_PK_ID, WHO_PERSON_FK_ID, DOMAIN_OBJECT_CODE_FK_ID, WHAT_DB_PRIMARY_KEY_ID, WHOSE_SUBJECT_USER_PERSON_FK_ID, IS_READ_CURRENT_PERMITTED,\n" +
				"IS_READ_HISTORY_PERMITTED,\n" +
				"IS_READ_FUTURE_PERMITTED,\n" +
				"IS_UPDATE_CURRENT_PERMITTED,\n" +
				"IS_UPDATE_HISTORY_PERMITTED,\n" +
				"IS_UPDATE_FUTURE_PERMITTED,\n" +
				"IS_DELETE_CURRENT_PERMITTED,\n" +
				"IS_DELETE_HISTORY_PERMITTED,\n" +
				"IS_DELETE_FUTURE_PERMITTED, IS_DELETED, CREATED_DATETIME)"
				+ "SELECT ordered_uuid(UUID()), P1.PERSON_PK_ID,'"+helperVO.getDoName()+"', TABLE1."+ helperVO.getPrimaryKeyColumnName()+", null, true, true, true, true, true, true, true, true, true, false, NOW() from T_PRN_EU_PERSON P1 JOIN "+helperVO.getTableName()+" TABLE1";
		List<Object> params = new ArrayList<>();
		executeUpdate(rowAC, params);
		String colAC = "INSERT INTO T_PFM_IEU_AM_COL_RESOLVED SELECT ordered_uuid(UUID()), RR.AM_ROW_RESOLVED_PK_ID, DOA.DOMAIN_OBJECT_ATTRIBUTE_CODE_PK_ID, TRUE, TRUE,TRUE, TRUE,TRUE, TRUE, FALSE, NOW()\n" +
				"    FROM T_PFM_IEU_AM_ROW_RESOLVED RR INNER JOIN T_MT_ADM_DO_ATTRIBUTE DOA\n" +
				"    ON DOA.DOMAIN_OBJECT_CODE_FK_ID = RR.DOMAIN_OBJECT_CODE_FK_ID WHERE RR.DOMAIN_OBJECT_CODE_FK_ID = '"+helperVO.getDoName()+"'";
		executeUpdate(colAC, params);

	}

	@Override
	public void updateLeftRightAndHierachyValuesForPerson(String parentId, int leftValue, int rightValue, String hierarchyId) throws SQLException {
		String sql = "UPDATE T_PRN_EU_PERSON_JOB_RELATIONSHIP INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE "
				+ " ON RELATIONSHIP_TYPE_CODE_FK_ID = RELATIONSHIP_TYPE_CODE_PK_ID AND IS_DIRECT_MANAGER INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID AND IS_PRIMARY "
				+ " SET INTRNL_LEFT_POS_INT = "+leftValue+", INTRNL_RIGHT_POS_INT = "+rightValue+", INTRNL_HIERARCHY_ID = "+hierarchyId+" WHERE PERSON_DEP_FK_ID = "+parentId+";";

		List<Object> params = new ArrayList<>();
		executeUpdate(sql, params);
	}

	@Override
	public List<String> getDBPrimaryKeyList(String tableName, String primaryKeyColumnName) {
		String sql = "SELECT "+primaryKeyColumnName+" FROM "+tableName;
		List<String> primaryKeyList = new ArrayList<>();
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(sql, parameters);
		try {
			while (rs.next()){
				primaryKeyList.add(Util.convertByteToString(rs.getBytes(primaryKeyColumnName)));
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return primaryKeyList;
	}
	@Override
	public List<String> getAccessManagerTables(String pattern){
		String schemaName = TenantContextHolder.getCurrentTenantSchema();
		StringBuilder sql = new StringBuilder("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_NAME LIKE ")
				        .append('\'')
				        .append(pattern)
				        .append('\'')
					    .append("AND TABLE_SCHEMA = '"+schemaName+"';");
		List<String> amTableList = new ArrayList<>();
		List<Object> parameters = new ArrayList<>();
		ResultSet rs = executeSQL(sql.toString(), parameters);
		try {
			while (rs.next()){
				amTableList.add(rs.getString("TABLE_NAME"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return amTableList;

	}

	@Override
	public void deleteAllFromSheetIDToDBIDTable() throws SQLException {
		String sql = "DELETE FROM SheetIDToDBIDMap;";
		List<Object> parameters = new ArrayList<>();
		executeUpdate(sql, parameters);
	}

	@Override
	public List<SheetIDToDBIDHelper> getAllColumnTypeToIDValues() {
		List<SheetIDToDBIDHelper> sheetIDToDBIDHelpers = new ArrayList<>();
		String SQL = "SELECT * FROM SheetIDToDBIDMap WHERE TENANT_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(TenantContextHolder.getCurrentTenant().getTenantId());
		ResultSet rs = executeSQL(SQL, parameters);
		try {
			while(rs.next()){
				SheetIDToDBIDHelper sheetIDToDBIDHelper = new SheetIDToDBIDHelper();
				sheetIDToDBIDHelper.setSheetForeignKeyName(rs.getString("SHEET_FOREIGN_KEY_NAME"));
				sheetIDToDBIDHelper.setSheetID(rs.getString("SHEET_PRIMARY_KEY_ID"));
				sheetIDToDBIDHelper.setDatabasePrimaryKeyID(rs.getString("DATABASE_PRIMARY_KEY_ID"));
				sheetIDToDBIDHelpers.add(sheetIDToDBIDHelper);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sheetIDToDBIDHelpers;
	}

	@Override
	public void addEntryToSheetIDTODBIDMap(String columnType, String primaryKeyName, String primaryKeyId) throws SQLException {
		String SQL = "INSERT INTO SheetIDToDBIDMap (TENANT_ID, SHEET_FOREIGN_KEY_NAME, SHEET_PRIMARY_KEY_ID, DATABASE_PRIMARY_KEY_ID) VALUES (?,?,?,?);";
		List<Object> parameters = new ArrayList<>();
		parameters.add(TenantContextHolder.getCurrentTenant().getTenantId());
		parameters.add(columnType);
		parameters.add(primaryKeyName);
		parameters.add(primaryKeyId);
		executeUpdate(SQL,parameters);
	}

	@Override
	public void deleteAllEntriesFromTable(String tableName) throws SQLException {
		String SQL = "DELETE FROM "+tableName;
		List<Object> parameters = new ArrayList<>();
		executeUpdate(SQL, parameters);
	}

	@Override
	public void deleteFKEnteriesFromSheetIDToDBIDTable(String foreignKeyName) {
		String sql = "DELETE FROM SheetIDToDBIDMap WHERE SHEET_FOREIGN_KEY_NAME = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(foreignKeyName);
		try {
			executeUpdate(sql, parameters);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateRowConditionDefaultValues() {
		String sql = "UPDATE T_PFM_ADM_AM_ROW_CONDN AM SET IS_CREATE_CURRENT_PERMITTED = (SELECT CASE WHEN SUM(IS_CREATE_CURRENT_PERMITTED)>0 THEN TRUE ELSE FALSE END FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID = AM.AM_ROW_CONDN_PK_ID GROUP BY AM_ROW_CONDN_FK_ID ),\n" +
				"  IS_CREATE_HISTORY_PERMITTED = (SELECT CASE WHEN SUM(IS_CREATE_HISTORY_PERMITTED)>0 THEN TRUE ELSE FALSE END FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID = AM.AM_ROW_CONDN_PK_ID GROUP BY AM_ROW_CONDN_FK_ID),\n" +
				"  IS_CREATE_FUTURE_PERMITTED = (SELECT CASE WHEN SUM(IS_CREATE_FUTURE_PERMITTED)>0 THEN TRUE ELSE FALSE END FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID = AM.AM_ROW_CONDN_PK_ID GROUP BY AM_ROW_CONDN_FK_ID ),\n" +
				"  IS_READ_CURRENT_PERMITTED = (SELECT CASE WHEN SUM(IS_READ_CURRENT_PERMITTED)>0 THEN TRUE ELSE FALSE END FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID = AM.AM_ROW_CONDN_PK_ID GROUP BY AM_ROW_CONDN_FK_ID ),\n" +
				"  IS_READ_HISTORY_PERMITTED = (SELECT CASE WHEN SUM(IS_READ_HISTORY_PERMITTED)>0 THEN TRUE ELSE FALSE END FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID = AM.AM_ROW_CONDN_PK_ID GROUP BY AM_ROW_CONDN_FK_ID ),\n" +
				"  IS_READ_FUTURE_PERMITTED = (SELECT CASE WHEN SUM(IS_READ_FUTURE_PERMITTED)>0 THEN TRUE ELSE FALSE END FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID = AM.AM_ROW_CONDN_PK_ID GROUP BY AM_ROW_CONDN_FK_ID ),\n" +
				"  IS_UPDATE_CURRENT_PERMITTED = (SELECT CASE WHEN SUM(IS_UPDATE_CURRENT_PERMITTED)>0 THEN TRUE ELSE FALSE END FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID = AM.AM_ROW_CONDN_PK_ID GROUP BY AM_ROW_CONDN_FK_ID ),\n" +
				"  IS_UPDATE_HISTORY_PERMITTED = (SELECT CASE WHEN SUM(IS_UPDATE_HISTORY_PERMITTED)>0 THEN TRUE ELSE FALSE END FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID = AM.AM_ROW_CONDN_PK_ID GROUP BY AM_ROW_CONDN_FK_ID ),\n" +
				"  IS_UPDATE_FUTURE_PERMITTED = (SELECT CASE WHEN SUM(IS_UPDATE_FUTURE_PERMITTED)>0 THEN TRUE ELSE FALSE END FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID = AM.AM_ROW_CONDN_PK_ID GROUP BY AM_ROW_CONDN_FK_ID );\n";
		List<Object> parameters = new ArrayList<>();
		try {
			executeUpdate(sql, parameters);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updatePersonJobRelationShipSubjectUser() {
		String sql = "UPDATE T_PRN_EU_PERSON_JOB_RELATIONSHIP JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT " +
				"ON PERSON_JOB_ASSIGNMENT_PK_ID = PERSON_JOB_ASSIGNMENT_DEP_FK_ID SET SUBJECT_PERSON_FK_ID = T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID;";
		List<Object> parameters = new ArrayList<>();
		try {
			executeUpdate(sql, parameters);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<String, Object> getAllRecords(String subjectUserColumnName, String primaryKeyColumnName, String tableName) {
		String sql = "SELECT "+subjectUserColumnName+", "+primaryKeyColumnName+" FROM "+tableName+" LIMIT 20;";
		List<Object> parameters = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();
		ResultSet rs = executeSQL(sql, parameters);
		try {
			while (rs.next()){
				map.put(Util.convertByteToString(rs.getBytes(primaryKeyColumnName)), Util.convertByteToString(rs.getBytes(subjectUserColumnName)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return map;
	}

}
