package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;



public class NotificationRowMapper implements RowMapper<NTFN_NotificationData>{

	public NTFN_NotificationData mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		
		NTFN_NotificationData bdg = new NTFN_NotificationData();
		bdg.setPersonNotificationPkId(rs.getString(Constants.personNotificationPkId));
		bdg.setPersonFkId(rs.getString(Constants.personFkId));
		bdg.setMtPeCodeFkId(rs.getString(Constants.mtPeCodeFkId));
		bdg.setDoRowPrimaryKeyTxt(rs.getString(Constants.doRowPrimaryKeyTxt));
		bdg.setDoRowVersionFkId(rs.getString(Constants.doRowVersionFkId));
		bdg.setDependencyKeyExpn(rs.getString(Constants.dependencyKeyExpn));
		bdg.setNotificationDefinitionFkId(rs.getString(Constants.notificationDefinitionFkId));
		bdg.setGenerateDatetime(rs.getString(Constants.generateDatetime));
		bdg.setReturnStatusCodeFkId(rs.getBoolean(Constants.returnStatusCodeFkId));
		bdg.setSync(rs.getBoolean(Constants.isSync));
		bdg.setNtfnBtCodeFkId(rs.getString(Constants.ntfnBtCodeFkId));
		bdg.setNtfnMtPeCodeFkId(rs.getString(Constants.ntfnMtPeCodeFkId));
		bdg.setNtfnVtCodeFkId(rs.getString(Constants.ntfnVtCodeFkId));
		bdg.setNtfnCanvasTxnFkId(rs.getString(Constants.ntfnCanvasTxnFkId));
		bdg.setForDocGenDefnFkId(rs.getString(Constants.forDocGenDefnFkId));
		return bdg;
	}

}
