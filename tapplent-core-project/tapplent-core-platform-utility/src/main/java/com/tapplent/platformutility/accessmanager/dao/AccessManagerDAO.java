package com.tapplent.platformutility.accessmanager.dao;


import com.tapplent.platformutility.accessmanager.structure.*;
import com.tapplent.platformutility.etl.valueObject.ETLMetaHelperVO;
import com.tapplent.platformutility.groupresolver.valueobject.GroupVO;
import com.tapplent.platformutility.layout.valueObject.DOADetails;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.person.structure.PersonGroup;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Shubham Patodi on 08/08/16.
 */
public interface AccessManagerDAO {
    public Map<String, ResolvedRowCondition> getRowConditionsForRecords(List<String> dbPrimaryKeys, String whoPersonId);

    void setColumnConditions(Map<String, RowCondition> rowConditionsSatified);

    void deleteExistingPermissionsFromRecord(String dbPrimaryKey, String whatDBPrimaryKey, String rowResolvedTable, List<String> rowConditionsToDelete) throws SQLException;

    Map<String,AMRowIntermediate> getRowIntermediateByDependencykey(EntityAttributeMetadata dependencyDOA, Object dependencyKey);

    Map<String,RowCondition> getRowConditions(Map<String, AMRowIntermediate> parentRowIntermediateConditions, String domainObjectCode);

    Map<String,List<AMWhatFilterCriteria>> getWhatFilterCriteriaByRowCondition(Map<String, RowCondition> rowConditionMap);

    Map<String,Map<String,GroupVO>> getWhoGroup(Map<String, RowCondition> rowConditionQualifiedSet);

    List<String> getPersonIdListForWhoGroup(String whoGroupId);

    void populateRowIntermediateForBasePE(Map<String, List<String>> rowConditionToPersonId, String whatDbPrimaryKey, String whoseSubjectUserPersonId, String currentTimestampString) throws SQLException;

    void populateRowResolvedTable(String currentTimestampString);

    Map<String, RowCondition> getRowConditions(String mtPE);

    ETLMetaHelperVO getMetaByDoName(String subjectUserDrivenDO);

    DOADetails getDOADetailsByDOAName(String doa);

    String getSubjectUserId(StringBuilder sql, String primaryKeyDBDataType);

    void populateRowIntermediateForBasePE(Map<String, RowCondition> rowConditionQualified, String whatDbPrimaryKey, Object dependencyKey, String whoseSubjectUserId) throws SQLException;

    void populateRowIntermediateForChildPE(Map<String, RowCondition> rowConditionQualified, String whatDbPrimaryKey, Object dependencyKey, String whoseSubjectUserId) throws SQLException;

    void populateRowIntermediateForBasePETable(Map<String, RowCondition> rowConditionMap, String subjectUserColumnName, String primaryKeyColumnName, ETLMetaHelperVO etlMetaHelperVO) throws SQLException;

    void populateRowIntermediateForChildPETable(Map<String, RowCondition> rowConditionMap, String subjectUserColumnName, String primaryKeyColumnName, ETLMetaHelperVO etlMetaHelperVO) throws SQLException;

    void updatePrimaryKeyForIntermediateTable(String primaryKeyColumnName, ETLMetaHelperVO helperVO) throws SQLException;

//    void populateRowIntermediateForBasePETableByRecords(Map<String, RowCondition> rowConditionMap, Map.Entry<String, Object> entry, ETLMetaHelperVO helperVO, String primaryKeyColumnName);

    void populateRowIntermediateForChildPETableByRecords(Map<String, RowCondition> rowConditionMap, Map.Entry<String, Object> entry, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException;

    void populateRowResolvedByRecords(String whatDbPrimaryKey, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException;

//    void populateRowIntermediateForBasePETableByRecord(Map<String, RowCondition> rowConditionMap, Map.Entry entry, ETLMetaHelperVO helperVO);

    void populateColumnResolvedByRecord(Map.Entry entry, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException;

    void populateRowIntermediateForBasePETableByRecord(Map<String, RowCondition> rowConditionMap, Object whatdbPrimaryKey, Object whoseSubjectUserId, String intermediateTableName) throws SQLException;

    Map<String, RowCondition> getAbsoluteRowConditions(PersonGroup whoGroup, PersonGroup whoseGroup, String mtPE);

    public ETLMetaHelperVO getAccessControlGovernedDOByDOName(String dOName);

    Map<String,RowCondition> getRelativeRowConditons(Map<String, Integer> groupIdPositionMap, PersonGroup whoGroup, String mtPE);

    List<AccessManager> getAccessManagersByWhoGroup(List<String> groupIds);

    List<AMProcess> getAmProcessByAccessManager(List<AccessManager> accessManagers);

    Map<String,RowCondition> getRowConditions(List<AMProcess> amProcesses, String mtPE);

    AMRowResolved getAMRowResolvedForParentPE(String parentWhatDbPrimaryKey, EntityAttributeMetadata dependencyKeyAttribute, String whoseSubjectUserId);
}
