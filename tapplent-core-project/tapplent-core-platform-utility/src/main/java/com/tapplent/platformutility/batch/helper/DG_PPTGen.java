package com.tapplent.platformutility.batch.helper;

import java.io.*;

import java.util.List;
import java.util.Map;
import java.util.Random;

import com.tapplent.platformutility.batch.model.DG_PPTData;
import com.tapplent.platformutility.batch.model.DG_PPTDoaMap;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTextParagraph;
import org.apache.poi.xslf.usermodel.XSLFTextRun;
import org.apache.poi.xslf.usermodel.XSLFTextShape;


public class DG_PPTGen {

	InputStream template;
	Map<Integer, DG_PPTData> pptData;
	Map<String, DG_PPTDoaMap> doaMap;
	Object data;
	public String s3Link ;

	public DG_PPTGen(InputStream template, Map<Integer, DG_PPTData> pptData, Map<String, DG_PPTDoaMap> doaMap) throws IOException {
		this.template = template;
		this.pptData = pptData;
		this.doaMap = doaMap;
		fetchData();
		s3Link = setData();
	}
	
	
	public Object fetchData(){
		
		//(TDWI) logic to fetch data for a particular PE 
		
		return null;
	}

	public String setData() throws IOException {

		
		XMLSlideShow ppt = new XMLSlideShow(this.template);

		// get slides
		List<XSLFSlide> slide = ppt.getSlides();

		// getting the shapes in the presentation

		for (int i = 0; i < slide.size(); i++) {

			List<XSLFShape> sh = slide.get(i).getShapes();
			for (int j = 0; j < sh.size(); j++) {
				System.out.println(sh.get(j).getShapeName());
				// name of the shape
				XSLFTextShape body = (XSLFTextShape) sh.get(j);
				XSLFTextRun run = body.getTextParagraphs().get(0).getTextRuns().get(0);
				run.setText("I'm Fine Here");
			}
		}


		OutputStream out = new FileOutputStream("temp");
		IOUtils.copy(template,out);
		ppt.write(out);
		out.close();
		S3Helper.insertIntoS3(template,"fileName");
		return "";

	}

	public static void setShapeIdentifiers(File templateFile) throws IOException {

		Random rand = new Random();
		
		XMLSlideShow ppt = new XMLSlideShow(new FileInputStream(templateFile));

		// get slides
		List<XSLFSlide> slide = ppt.getSlides();

		// getting the shapes in the presentation

		for (int i = 0; i < slide.size(); i++) {

			List<XSLFShape> sh = slide.get(i).getShapes();
			for (int j = 0; j < sh.size(); j++) {
				// name of the shape
				int randomNum = rand.nextInt((1000 - 0) + 1);
				XSLFTextShape body = (XSLFTextShape) sh.get(j);
				XSLFTextParagraph paragraph = body.addNewTextParagraph();
				XSLFTextRun run3 = paragraph.addNewTextRun();

				run3.setText(sh.get(j).getShapeName().replaceAll(" ", "_") + sh.get(j).getShapeId() + randomNum);
			}
		}

		FileOutputStream out = new FileOutputStream(templateFile);
		
		//(TDWI) update in S3
		
		ppt.write(out);
		out.close();

	}

}
