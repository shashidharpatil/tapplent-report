package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by tapplent on 04/11/16.
 */
public class AccessManager {
    private String accessManagerPkId;
    private String whoGroupFkId;
    private String amDefnFkId;

    public String getAccessManagerPkId() {
        return accessManagerPkId;
    }

    public void setAccessManagerPkId(String accessManagerPkId) {
        this.accessManagerPkId = accessManagerPkId;
    }

    public String getWhoGroupFkId() {
        return whoGroupFkId;
    }

    public void setWhoGroupFkId(String whoGroupFkId) {
        this.whoGroupFkId = whoGroupFkId;
    }

    public String getAmDefnFkId() {
        return amDefnFkId;
    }

    public void setAmDefnFkId(String amDefnFkId) {
        this.amDefnFkId = amDefnFkId;
    }
}
