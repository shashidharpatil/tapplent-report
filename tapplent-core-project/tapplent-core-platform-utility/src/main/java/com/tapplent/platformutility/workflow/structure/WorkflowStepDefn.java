package com.tapplent.platformutility.workflow.structure;

/**
 * Created by Manas on 8/10/16.
 */
public class WorkflowStepDefn {
    private String versionId ;
    private String workflowStepDefnPkId;
    private String workflowDefinitionFkId;
    private int stepSeqNumPosInt;
    private String workflowStepNameG11nBigText;
    private String workflowStepIconCodeFkId;
    private String startDateExpn;
    private String dueDateExpn;
    private boolean isStartDateEnforced;
    private boolean isDueDateEnforced;
    private String stepActorResolutionCodeFkId;
    private boolean allowTrgtActrRecVsbltyWhnEntryCritNotFulfilled;
    private boolean isBeginStep;
    private boolean isEndStep;
    private String onStepCompletionExpn;
    private String onStepCancellationExpn;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

	public String getWorkflowStepDefnPkId() {
		return workflowStepDefnPkId;
	}

	public void setWorkflowStepDefnPkId(String workflowStepDefnPkId) {
		this.workflowStepDefnPkId = workflowStepDefnPkId;
	}

	public String getWorkflowDefinitionFkId() {
		return workflowDefinitionFkId;
	}

	public void setWorkflowDefinitionFkId(String workflowDefinitionFkId) {
		this.workflowDefinitionFkId = workflowDefinitionFkId;
	}

	public int getStepSeqNumPosInt() {
		return stepSeqNumPosInt;
	}

	public void setStepSeqNumPosInt(int stepSeqNumPosInt) {
		this.stepSeqNumPosInt = stepSeqNumPosInt;
	}

	public String getWorkflowStepNameG11nBigText() {
		return workflowStepNameG11nBigText;
	}

	public void setWorkflowStepNameG11nBigText(String workflowStepNameG11nBigText) {
		this.workflowStepNameG11nBigText = workflowStepNameG11nBigText;
	}

	public String getWorkflowStepIconCodeFkId() {
		return workflowStepIconCodeFkId;
	}

	public void setWorkflowStepIconCodeFkId(String workflowStepIconCodeFkId) {
		this.workflowStepIconCodeFkId = workflowStepIconCodeFkId;
	}

	public String getStartDateExpn() {
		return startDateExpn;
	}

	public void setStartDateExpn(String startDateExpn) {
		this.startDateExpn = startDateExpn;
	}

	public String getDueDateExpn() {
		return dueDateExpn;
	}

	public void setDueDateExpn(String dueDateExpn) {
		this.dueDateExpn = dueDateExpn;
	}

	public boolean isStartDateEnforced() {
		return isStartDateEnforced;
	}

	public void setStartDateEnforced(boolean isStartDateEnforced) {
		this.isStartDateEnforced = isStartDateEnforced;
	}

	public boolean isDueDateEnforced() {
		return isDueDateEnforced;
	}

	public void setDueDateEnforced(boolean isDueDateEnforced) {
		this.isDueDateEnforced = isDueDateEnforced;
	}

	public String getStepActorResolutionCodeFkId() {
		return stepActorResolutionCodeFkId;
	}

	public void setStepActorResolutionCodeFkId(String stepActorResolutionCodeFkId) {
		this.stepActorResolutionCodeFkId = stepActorResolutionCodeFkId;
	}

	public boolean isAllowTrgtActrRecVsbltyWhnEntryCritNotFulfilled() {
		return allowTrgtActrRecVsbltyWhnEntryCritNotFulfilled;
	}

	public void setAllowTrgtActrRecVsbltyWhnEntryCritNotFulfilled(boolean allowTrgtActrRecVsbltyWhnEntryCritNotFulfilled) {
		this.allowTrgtActrRecVsbltyWhnEntryCritNotFulfilled = allowTrgtActrRecVsbltyWhnEntryCritNotFulfilled;
	}

	public boolean isBeginStep() {
		return isBeginStep;
	}

	public void setBeginStep(boolean isBeginStep) {
		this.isBeginStep = isBeginStep;
	}

	public boolean isEndStep() {
		return isEndStep;
	}

	public void setEndStep(boolean isEndStep) {
		this.isEndStep = isEndStep;
	}

	public String getOnStepCompletionExpn() {
		return onStepCompletionExpn;
	}

	public void setOnStepCompletionExpn(String onStepCompletionExpn) {
		this.onStepCompletionExpn = onStepCompletionExpn;
	}

	public String getOnStepCancellationExpn() {
		return onStepCancellationExpn;
	}

	public void setOnStepCancellationExpn(String onStepCancellationExpn) {
		this.onStepCancellationExpn = onStepCancellationExpn;
	}

}
