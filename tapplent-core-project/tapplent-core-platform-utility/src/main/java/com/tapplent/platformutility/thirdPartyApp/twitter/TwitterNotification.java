package com.tapplent.platformutility.thirdPartyApp.twitter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.POST_PostData;
import com.tapplent.platformutility.batch.model.POST_SocialDoaMap;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.tapplent.platformutility.thirdPartyApp.facebook.FacebookNotification;


public class TwitterNotification {
	
	

	public String encode(String value) {
		String encoded = null;
		try {
			encoded = URLEncoder.encode(value, "UTF-8");
		} catch (UnsupportedEncodingException ignore) {
		}
		StringBuilder buf = new StringBuilder(encoded.length());
		char focus;
		for (int i = 0; i < encoded.length(); i++) {
			focus = encoded.charAt(i);
			if (focus == '*') {
				buf.append("%2A");
			} else if (focus == '+') {
				buf.append("%20");
			} else if (focus == '%' && (i + 1) < encoded.length() && encoded.charAt(i + 1) == '7'
					&& encoded.charAt(i + 2) == 'E') {
				buf.append('~');
				i += 2;
			} else {
				buf.append(focus);
			}
		}
		return buf.toString();
	}

	private static String computeSignature(String baseString, String keyString)
			throws GeneralSecurityException, UnsupportedEncodingException {
		SecretKey secretKey = null;

		byte[] keyBytes = keyString.getBytes();
		secretKey = new SecretKeySpec(keyBytes, "HmacSHA1");

		Mac mac = Mac.getInstance("HmacSHA1");
		mac.init(secretKey);

		byte[] text = baseString.getBytes();

		return new String(Base64.encodeBase64(mac.doFinal(text))).trim();
	}

	//This function will post the tweet on user wall
	public void postOnUserWall(POST_PostData obj) throws JSONException{
		
		JSONObject object = (JSONObject)obj.getPersonCredential();
		String oauth_token = object.getString("oauth_token");
		String oauth_token_secret = object.getString("oauth_token_secret");
		String client_id = object.getString("client_id");
		String client_secret = object.getString("client_secret");
try	{	
		
		List<POST_SocialDoaMap> list =obj.getDoaMap();
		ResultSet rs = obj.getResultSet();
		String Key = "media[]";
		String media = FacebookNotification.getData(Key,list,rs);
	if(!media.equals("")){
		
			
			// generate authorization header
			String get_or_post = "POST";
			String oauth_signature_method = "HMAC-SHA1";

			String uuid_string = UUID.randomUUID().toString();
			uuid_string = uuid_string.replaceAll("-", "");
			String oauth_nonce = uuid_string; 

			// get the time-stamp
			Calendar tempcal = Calendar.getInstance();
			long ts = tempcal.getTimeInMillis();// get current time in milliseconds
			String oauth_timestamp = (new Long(ts / 1000)).toString(); 
		
			String parameter_string = "oauth_consumer_key=" + client_id + "&oauth_nonce=" + oauth_nonce
					+ "&oauth_signature_method=" + oauth_signature_method + "&oauth_timestamp=" + oauth_timestamp
					+ "&oauth_token=" + encode(oauth_token) + "&oauth_version=1.0";// &status="
			// +
			// encode(text);
			System.out.println("Twitter.updateStatusWithMedia(): parameter_string=" + parameter_string);

			String twitter_endpoint = Constants.TwitterPostImageUrl;
			String signature_base_string = get_or_post + "&" + encode(twitter_endpoint) + "&" + encode(parameter_string);
			System.out.println("Twitter.updateStatusWithMedia(): signature_base_string=" + signature_base_string);
			String oauth_signature = "";
			try {
				oauth_signature = computeSignature(signature_base_string,
						client_secret + "&" + encode(oauth_token_secret));
			} catch (GeneralSecurityException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			String authorization_header_string = "OAuth oauth_consumer_key=\"" + client_id
					+ "\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"" + oauth_timestamp + "\",oauth_nonce=\""
					+ oauth_nonce + "\",oauth_version=\"1.0\",oauth_signature=\"" + encode(oauth_signature)
					+ "\",oauth_token=\"" + encode(oauth_token) + "\"";
			System.out.println("Twitter.updateStatusWithMedia(): authorization_header_string=" + authorization_header_string);

	    	HttpPost post = new HttpPost(twitter_endpoint);
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("HttpVersion", "HTTP_1_1"));
			params.add(new BasicNameValuePair("Charset", "UTF-8"));
			params.add(new BasicNameValuePair("UserAgent", "HttpCore/1.1"));
			post.setEntity(new UrlEncodedFormEntity(params));
			StringEntity entity = new StringEntity("application/x-www-form-urlencoded", "UTF-8");
			post.addHeader("Authorization", authorization_header_string);
			post.setEntity(entity);
			String imageUrl = media;
			String key = "status";
			String text = FacebookNotification.getData(key,list,rs);
			String destinationFile = "/Users/tapplent/Desktop/punda.jpg";
			URL url = new URL(imageUrl);
			InputStream is = url.openStream();
			OutputStream os = new FileOutputStream(destinationFile);
			byte[] b = new byte[2048];
			int length;
			while ((length = is.read(b)) != -1) {
				os.write(b, 0, length);
			}

			is.close();
			os.close();
			File file1 = new File("/Users/tapplent/Desktop/punda.jpg");
			HttpClient client = HttpClientBuilder.create().build();
			MultipartEntity reqEntity = new MultipartEntity();
			FileBody sb_image = new FileBody(file1);
			if(!text.equals("")){
				StringBody sb_status = new StringBody(text);
				reqEntity.addPart("status", sb_status);	
			}
			reqEntity.addPart("media[]", sb_image);
			post.setEntity(reqEntity);
			HttpResponse response;
			response = client.execute(post);
				int status_code = response.getStatusLine().getStatusCode();
				boolean flag = FacebookNotification.responseCheck(status_code);
				if(flag){
					obj.setSuccess(flag);
				} else{
					obj.setError(response);
				}
				
	}else{
		
		String key = "status";
		String text =FacebookNotification.getData(key,list,rs);

		// generate authorization header
		String get_or_post = "POST";
		String oauth_signature_method = "HMAC-SHA1";

		String uuid_string = UUID.randomUUID().toString();
		uuid_string = uuid_string.replaceAll("-", "");
		String oauth_nonce = uuid_string; // any relatively random alphanumeric
		// string will work here

		// get the timestamp
		Calendar tempcal = Calendar.getInstance();
		long ts = tempcal.getTimeInMillis();// get current time in milliseconds
		String oauth_timestamp = (new Long(ts / 1000)).toString(); 
		
		String parameter_string = "oauth_consumer_key=" + client_id + "&oauth_nonce=" + oauth_nonce
				+ "&oauth_signature_method=" + oauth_signature_method + "&oauth_timestamp=" + oauth_timestamp
				+ "&oauth_token=" + encode(oauth_token) + "&oauth_version=1.0&status=" + encode(text);
		System.out.println("parameter_string=" + parameter_string);

		String twitter_endpoint = Constants.TwitterPostTextUrl;
		String signature_base_string = get_or_post + "&" + encode(twitter_endpoint) + "&" + encode(parameter_string);
		System.out.println("signature_base_string=" + signature_base_string);
		String oauth_signature = "";
		try {
			oauth_signature = computeSignature(signature_base_string,
					client_secret + "&" + encode(oauth_token_secret));
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String authorization_header_string = "OAuth oauth_consumer_key=\"" + client_id
				+ "\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"" + oauth_timestamp + "\",oauth_nonce=\""
				+ oauth_nonce + "\",oauth_version=\"1.0\",oauth_signature=\"" + encode(oauth_signature)
				+ "\",oauth_token=\"" + encode(oauth_token) + "\"";
		System.out.println("authorization_header_string=" + authorization_header_string);
		HttpPost post = new HttpPost(twitter_endpoint);
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("HttpVersion", "HTTP_1_1"));
		params.add(new BasicNameValuePair("Charset", "UTF-8"));
		params.add(new BasicNameValuePair("UserAgent", "HttpCore/1.1"));
		post.setEntity(new UrlEncodedFormEntity(params));
		
		StringEntity entity = new StringEntity("application/x-www-form-urlencoded", "UTF-8");
		post.addHeader("Authorization", authorization_header_string);
		post.setEntity(entity);
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response;
		response = client.execute(post);
		int status_code = response.getStatusLine().getStatusCode();
		boolean flag = FacebookNotification.responseCheck(status_code);
		if(flag){
			obj.setSuccess(flag);
		} else{
			obj.setError(response);
		}
		
	}
	} catch (ClientProtocolException e) {
		System.out.println(e.getMessage());
		e.printStackTrace();
	} catch (IOException e) {
		System.out.println(e.getMessage());
	} catch (SQLException e) {
		System.out.println(e.getMessage());
	}
	return ;

	}
	
	
	public JSONObject deletePost(POST_PostData obj) throws JSONException{
        JSONObject jsonresponse = new JSONObject();
        JSONObject object = (JSONObject)obj.getPersonCredential();
		String oauth_token = object.getString("oauth_token");
		String oauth_token_secret = object.getString("oauth_token_secret");
		String client_id = object.getString("client_id");
		String client_secret = object.getString("client_secret");
		String id = "";
        

		// generate authorization header
		String get_or_post = "POST";
		String oauth_signature_method = "HMAC-SHA1";
		
		String uuid_string = UUID.randomUUID().toString();
		uuid_string = uuid_string.replaceAll("-", "");
		String oauth_nonce = uuid_string; // any relatively random alphanumeric string will work here
		
		// get the time-stamp
		Calendar tempcal = Calendar.getInstance();
		long ts = tempcal.getTimeInMillis();// get current time in milliseconds
		String oauth_timestamp = (new Long(ts/1000)).toString(); // then divide by 1000 to get seconds
		
		// the parameter string must be in alphabetical order, "text" parameter added at end
		String parameter_string = "oauth_consumer_key=" + client_id + "&oauth_nonce=" + oauth_nonce + "&oauth_signature_method=" + oauth_signature_method + 
		    		"&oauth_timestamp=" + oauth_timestamp + "&oauth_token=" + encode(oauth_token) + "&oauth_version=1.0";	
		System.out.println("parameter_string=" + parameter_string);
		
		String twitter_endpoint = Constants.TwitterPostTextUrl + id + ".json";
		String signature_base_string = get_or_post + "&"+ encode(twitter_endpoint) + "&" + encode(parameter_string);
		System.out.println("signature_base_string=" + signature_base_string);
		String oauth_signature = "";
	    try {
	    	oauth_signature = computeSignature(signature_base_string, client_secret + "&" + encode(oauth_token_secret));  
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
	    catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	    String authorization_header_string = "OAuth oauth_consumer_key=\"" + client_id + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"" + oauth_timestamp + 
	    		"\",oauth_nonce=\"" + oauth_nonce + "\",oauth_version=\"1.0\",oauth_signature=\"" + encode(oauth_signature) + "\",oauth_token=\"" + encode(oauth_token) + "\"";
	    System.out.println("authorization_header_string=" + authorization_header_string);
		HttpPost post = new HttpPost(twitter_endpoint);
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("HttpVersion", "HTTP_1_1"));
		params.add(new BasicNameValuePair("Charset", "UTF-8"));
		params.add(new BasicNameValuePair("UserAgent", "HttpCore/1.1"));
		try {
			post.setEntity(new UrlEncodedFormEntity(params));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		StringEntity entity = new StringEntity("application/x-www-form-urlencoded", "UTF-8");
		post.addHeader("Authorization", authorization_header_string);
		post.setEntity(entity);
		HttpClient client2 = HttpClientBuilder.create().build();
		HttpResponse response;
		try {
			response = client2.execute(post);
			String responseString = EntityUtils.toString(response.getEntity());
			System.out.println(responseString);
			jsonresponse = new JSONObject(responseString);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonresponse;
		
	}

}