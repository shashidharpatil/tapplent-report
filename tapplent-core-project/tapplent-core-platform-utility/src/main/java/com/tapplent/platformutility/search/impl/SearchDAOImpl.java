package com.tapplent.platformutility.search.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.util.EntityDataTypeEnum;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.builder.SearchField;

public class SearchDAOImpl extends TapplentBaseDAO implements SearchDAO {
	
	private static final Logger LOG = LogFactory.getLogger(SearchDAOImpl.class);
	private static final String AGGREGATE_ALIAS = "###AGGREGATE_ALIAS###";

	@Override
	public SearchResult searchAndCount(String sql, String countSql, Deque<Object> parameter, Deque<Object> countParameter, SearchContext searchContext, DefaultSearchData searchData) throws IOException {
		SearchResult sr = new SearchResult();
		PreparedStatement ps = null;
		PreparedStatement countPrepStmt = null;
		ResultSet rs = null;
		ResultSet countResultSet	= null;
		try{
			Connection c = getConnection();
			ps = c.prepareStatement(sql);
			if(parameter!=null){
				int i= 1;
				for(Object param : parameter){
					if(param instanceof byte[]){
						LOG.debug("Select query parameter number "+i+" => "+Util.convertByteToString((byte[]) param));
					}else {
						LOG.debug("Select query parameter number " + i + " => " + param.toString());
					}
					ps.setObject(i, param);
					i++;
				}
			}
			rs = ps.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			List<Map<String, Object>> result = new LinkedList<>();
			while(rs.next()){
				Map<String, Object> row = new HashMap<>();
				for(int columnIndex = 1; columnIndex <= meta.getColumnCount(); columnIndex++){
					Object columnValue = getColumnValue(rs, searchContext, meta.getColumnLabel(columnIndex), columnIndex, searchData);
					row.put(meta.getColumnLabel(columnIndex), columnValue);
				}
				result.add(row);
			}
			sr.setData(result);
			try {
				if (countSql != null && !countSql.equals("")) {
					countPrepStmt = c.prepareStatement(countSql);
					if (countParameter != null) {
						int i = 1;
						for (Object countParam : countParameter) {
							if(countParam instanceof byte[]){
								LOG.debug("Count query parameter number " + i + " => " + Util.convertByteToString((byte[]) countParam));
							}else {
								LOG.debug("Count query parameter number " + i + " => " + countParam.toString());
							}
							countPrepStmt.setObject(i, countParam);
							i++;
						}
					}
					countResultSet = countPrepStmt.executeQuery();
					if(countSql.contains("GROUP BY")){
						countResultSet.last();
						sr.setNumberOfRecords(countResultSet.getRow());
					}else{
						if (countResultSet.first()) {
							sr.setNumberOfRecords(countResultSet.getInt(1));
						}
					}
				}
			}catch (SQLException e){
				LOG.error(e.getMessage(), e);
			}
		} catch (SQLException e){
			LOG.error(e.getMessage(), e);
		}
		return sr;
	}

	private Object getColumnValue(ResultSet rs, SearchContext searchContext, String columnLabel, int columnIndex, DefaultSearchData searchData) throws SQLException, IOException {
		Object result = null;
		if(columnLabel.equals(AGGREGATE_ALIAS)){
			result = rs.getDouble(columnIndex);
			return result;
		}
		if(columnLabel.matches("#BOOKMARK#|#ENDORSE#")){
			String bookmarkResult = Util.convertByteToString(rs.getBytes(columnIndex));
			if(StringUtil.isDefined(bookmarkResult)){
				return true;
			}else{
				return false;
			}
		}
		SearchField column = searchContext.getColumnLabelToColumnMap().get(columnLabel);
		String dbDataType = column.getDataType();
		EntityDataTypeEnum dataType = EntityDataTypeEnum.valueOf(dbDataType);
		switch(dataType){
			case DATE_TIME:
				Timestamp timestamp = rs.getTimestamp(columnIndex);
				if (timestamp != null)
					result = timestamp.toString();
				break;
			case BOOLEAN:
				result = rs.getBoolean(columnIndex);
				break;
			case POSITIVE_INTEGER:
			case NEGATIVE_INTEGER:
			case BIG_INTEGER:
				result = rs.getInt(columnIndex);
				break;
			case POSITIVE_DECIMAL:
			case NEGATIVE_DECIMAL:
				result = rs.getBigDecimal(columnIndex);
				break;
			case CURRENCY:
			case LOCATION_LATITUDE:
			case LOCATION_LONGITUDE:
			case T_CODE:
			case T_ICON:
			case TEXT:
				result = rs.getString(columnIndex);
				break;
			case ATTACHMENT:
			case AUDIO:
			case VIDEO:
			case IMAGE:
			case T_ID:
			case T_IMAGEID:
			case T_ATTACH:
				result = Util.convertByteToString(rs.getBytes(columnIndex));
				break;
			case T_BLOB:
				result = Util.getBlobJson(rs.getBlob(columnIndex));
				break;
			default:
				result = rs.getObject(columnIndex);
				break;
		}
//		result = rs.getObject(columnIndex);
		return result;
	}

	@Override
	public SearchResult search(String sql, Deque<Object> parameter) {
		SearchResult sr = new SearchResult();
		PreparedStatement ps = null;
		ResultSet rs = null;
		LOG.debug(sql);
		try{
			Connection c = getConnection();
			ps = c.prepareStatement(sql);
			if(parameter!=null){
				int i= 1;
				for(Object param : parameter){
					LOG.debug("Select query parameter number "+i+" => "+param.toString());
					ps.setObject(i,param);
					i++;
				}
			}
			rs = ps.executeQuery();
			ResultSetMetaData meta = rs.getMetaData();
			List<Map<String, Object>> result = new LinkedList<>();
			while(rs.next()){
				Map<String, Object> row = new HashMap<>();
				for(int i = 1; i <= meta.getColumnCount(); i++){
					row.put(meta.getColumnLabel(i), rs.getObject(i));
				}
				result.add(row);
			}
			sr.setData(result);
		} catch (SQLException e){
			LOG.error(e.getMessage(), e);
		}
		return sr;
	}

	@Override
	public String getG11nData(String tableName, String columnName, String pkColumnName, String dbPkId, String dbDataTypeCode) {
		StringBuilder SQL = new StringBuilder("SELECT ").append(columnName).append(" FROM ").append(tableName).append(" WHERE ").append(pkColumnName).append(" = ");
		String result = null;
		if(dbDataTypeCode.equals("T_ID")){
			SQL.append("x?");
		}else{
			SQL.append("?");
		}
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(dbPkId);
		ResultSet rs = executeSQL(SQL.toString(), parameters);
		try {
			if (rs.next()){
				result = rs.getString(1);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}
