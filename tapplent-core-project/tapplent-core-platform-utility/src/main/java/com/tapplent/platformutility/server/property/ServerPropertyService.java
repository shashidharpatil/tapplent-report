package com.tapplent.platformutility.server.property;

import java.util.LinkedList;
import java.util.Map;

import com.tapplent.tenantresolver.server.property.ServerActionStep;

public interface ServerPropertyService {
	
	public Map<String, LinkedList<ServerActionStep>> getactionCodeActionStepListMap();
}
