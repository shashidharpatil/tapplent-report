package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class ActionGroupVO {
	private String baseTemplateId;
	private String actionGroupCode;
	private String parentActionGroupCode;
	private String actionCategoryCode; 
	private boolean allowAutoExpand;
	private String actionGroupLabel;
	private String actionGroupIcon;
	private boolean showActionGroupLabel;
	private boolean showActionGroupIconId;
	private int actionGroupSeqNum;
	
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getActionGroupCode() {
		return actionGroupCode;
	}
	public void setActionGroupCode(String actionGroupCode) {
		this.actionGroupCode = actionGroupCode;
	}
	public String getParentActionGroupCode() {
		return parentActionGroupCode;
	}
	public void setParentActionGroupCode(String parentActionGroupCode) {
		this.parentActionGroupCode = parentActionGroupCode;
	}
	public String getActionCategoryCode() {
		return actionCategoryCode;
	}
	public void setActionCategoryCode(String actionCategoryCode) {
		this.actionCategoryCode = actionCategoryCode;
	}
	public boolean isAllowAutoExpand() {
		return allowAutoExpand;
	}
	public void setAllowAutoExpand(boolean allowAutoExpand) {
		this.allowAutoExpand = allowAutoExpand;
	}

	public String getActionGroupLabel() {
		return actionGroupLabel;
	}

	public void setActionGroupLabel(String actionGroupLabel) {
		this.actionGroupLabel = actionGroupLabel;
	}

	public String getActionGroupIcon() {
		return actionGroupIcon;
	}
	public void setActionGroupIcon(String actionGroupIcon) {
		this.actionGroupIcon = actionGroupIcon;
	}
	public boolean isShowActionGroupLabel() {
		return showActionGroupLabel;
	}
	public void setShowActionGroupLabel(boolean showActionGroupLabel) {
		this.showActionGroupLabel = showActionGroupLabel;
	}
	public boolean isShowActionGroupIconId() {
		return showActionGroupIconId;
	}
	public void setShowActionGroupIconId(boolean showActionGroupIconId) {
		this.showActionGroupIconId = showActionGroupIconId;
	}
	public int getActionGroupSeqNum() {
		return actionGroupSeqNum;
	}
	public void setActionGroupSeqNum(int actionGroupSeqNum) {
		this.actionGroupSeqNum = actionGroupSeqNum;
	}
}
