package com.tapplent.platformutility.hierarchy.dao;


import com.tapplent.platformutility.hierarchy.structure.HierarchyInfo;
import com.tapplent.platformutility.hierarchy.structure.Node;
import com.tapplent.platformutility.hierarchy.structure.Relation;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by Tapplent on 8/11/16.
 */
public interface HierarchyDAO {
    public Map<String, Node> getNthLevelParent(String primaryKey, EntityMetadataVOX doMeta, int level);
    public Map<String, Node> getAllChildrenUptoNlevel(String primaryKey, EntityMetadataVOX doMeta, int level);
    Map<String, Node> getAllParents(String primaryKey, EntityMetadataVOX doMeta);

    List<HierarchyInfo> getAllChildren(MasterEntityMetadataVOX rootMtPEMeta, String pk);

    Map<String,Relation> getDirectManagers(String personId, String hierarchyId);

    Map<String, List<Relation>> getOtherFirstLevelManager(String basePersonId);

    void updateleftRightForNodeInsert(String hierarchyId, String parentNodeId, EntityMetadataVOX entityMetadataVOX, String primaryNodeId) throws SQLException;

    void updateLeftRightForNodeDelete(String hierarchyId, EntityMetadataVOX entityMetadataVOX, int nodeLeft, int nodeRight, String primaryKey) throws SQLException;

    void updateLeftRightForNodeParentUpdate(String hierarchyId, EntityMetadataVOX entityMetadataVOX, Node rootNode, Node parentNode) throws SQLException;

    String getHierarchyIdForPerson(String basePersonId);

    String getPeronIdForRootPerson(String hierarchyId);

    List<String> getHierarchialPersonRoots();

    List<String> getDirectChildrenListForParentPerson(String parentId);

    void updateLeftRightAndHierarchyValuesForPerson(String parentId, int leftValue, int rightValue, String hierarchyId) throws SQLException;

    Map<String,Relation> getDirectManagers(String personId);

    String getInverseRelationshipType(String relationshipType);

    Map<String,List<Relation>> getOtherFirstLevelReport(String personId);
}
