package com.tapplent.platformutility.entity.provider;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.tapplent.platformutility.accessmanager.structure.Permission;
import com.tapplent.platformutility.insert.impl.DefaultInsertData;
import com.tapplent.platformutility.layout.valueObject.PersonTagsVO;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;
import com.tapplent.platformutility.sql.context.model.InsertContextModel;
import com.tapplent.platformutility.workflow.structure.WorkflowDetails;

public class GlobalVariables {
	public String mtPE;
	public String mtPEAlias;
	public String parentMTPEAlias;
	public String baseTemplateId;
	public String btPE;
	public String domainObject;
	public String actionVisualMasterCode;
	public String businessRule;
	public String contextMTPE;
	public Boolean uniqueMerge = true;
	public Boolean propogate = false;
	public Boolean logActivity = false;
	public Boolean returnMasterTags;
	public Boolean showConfirmMsg = true;
	public Boolean showReturnMsg = true;
	public String parentMetaProcessElement;
	public String parentDOPrimaryKey;
	public String parentDOVersionID;
	public String processTypeCode;
	public InsertContextModel model;
	public Map<String, Map<String, Object>> relatedUnderlyingRecord;
	public Map<String, Object> underlyingRecord;
	public DefaultInsertData insertData;
	public Boolean isUpdate;
	public Boolean validationRequired;
	public EntityMetadataVOX entityMetadataVOX;
	public MasterEntityMetadataVOX masterEntityMetadataVOX;
	public String primaryLocale;
	public String actionCode;
	public String recordState;
	public String recordStateClient;
	public Timestamp currentTimestamp;
	public String currentTimestampString;
	public String duplicateOfAttribute;
	public String deleteActionType;
	public String deleteActionCategory;
	public WorkflowDetails workflowDetails;
	public DefaultInsertData  parentInsertData;
	public Boolean setContext = false;
	public String context;
	public String contextPK;
	public Boolean isSubmit;
	public Boolean isInline;
	public Boolean isMakePrimary;
	public Boolean makePrimaryUnique = false;
	public EntityAttributeMetadata compareKeyAttribute;
	public String fkRelationWithParent;
	public String displayContextValue;
	public boolean userCreationFlag;
	public String employeeCodeGenKey;
	public String usernameGenKey;
	public PersonTagsVO personTag;
	public List<PersonTagsVO> personTagsList;
	public Map<String, Object> changedFields;
	public Permission permission;

	public void terminate(){
		entityMetadataVOX = null;
		insertData = null;
		model = null;
		relatedUnderlyingRecord = null;
		underlyingRecord = null;
	}

}
