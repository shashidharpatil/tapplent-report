package com.tapplent.platformutility.insert.structure;

public class EmployeeCodeGenRuleVO {
    private String employeeCodeGenRulePkId;
    private long employeeCodeSequence;
    private long employeeSequenceStep;
    private String formula;

    public String getEmployeeCodeGenRulePkId() {
        return employeeCodeGenRulePkId;
    }

    public void setEmployeeCodeGenRulePkId(String employeeCodeGenRulePkId) {
        this.employeeCodeGenRulePkId = employeeCodeGenRulePkId;
    }

    public long getEmployeeCodeSequence() {
        return employeeCodeSequence;
    }

    public void setEmployeeCodeSequence(long employeeCodeSequence) {
        this.employeeCodeSequence = employeeCodeSequence;
    }

    public long getEmployeeSequenceStep() {
        return employeeSequenceStep;
    }

    public void setEmployeeSequenceStep(long employeeSequenceStep) {
        this.employeeSequenceStep = employeeSequenceStep;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
}
