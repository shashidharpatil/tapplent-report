package com.tapplent.platformutility.layout.dao;


import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;


import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.activityLog.ActivityLog;
import com.tapplent.platformutility.layout.valueObject.*;

public interface LayoutDAO {
	public List<BaseTemplateVO> getBaseTemplateByMetaProcess(String baseObjectId, boolean isIncludeArchived);
	public BaseTemplateVO getBaseTemplateByBaseTemplate(String baseTemplateId);
	public List<CanvasVO> getCanvasListByBaseTemplate(String baseTemplateId,String deviceType);
	public List<ActionStepVO> getActionStep();
	public List<ControlVO> getControlListByBaseTemplate(String baseTemplateId);
	public List<PropertyControlVO> getPropertyControlListByBaseTemplate(String baseTemplateId);
	public List<CanvasActionForAgVO> getCanvasActionForAgByBaseTemplate(String baseTemplateId, String deviceType);
	public List<ActionRepresentationVO> getActionRepresenationByBaseTemplate();
	public List<CanvasThemeVO> getCanvasTheme();
	public List<CanvasElementThemeVO> getCanvasThemeElement();
	public List<ThemeAlphaVO> getThemeAlpha();
	public List<ThemeGroupEntityMapVO> getThemeGroup();
	public UiSettingsVO getUiSettings();
	public List<PropertyControlLookupVO> getPropertyControlLookup();
	public Map<String,Object> getLookupMap(PropertyControlLookupVO propertyControlLookupVO);
	public List<PageEquivalenceVO> getPageEquivalenceByBT(String baseTemplateId);
	public List<PageEquivalencePlacementVO> getPageEquivalencePlacementByBT(String baseTemplateId);
	public List<ActionRepresetationLaunchTrxnVO> getActionRepresenationLaunchByBaseTemplate(String baseTemplateId);
	public List<ControlRelationControlVO> getControlRelationControlByBaseTemplate(String baseTemplateId);
	public List<ControlHierarchyControlVO> getControlHierarchyControlByBaseTemplate(String baseTemplateId);
	public List<ControlGroupByControlVO> getControlGroupByControlByBaseTemplate(String baseTemplateId);
	public List<ControlEventVO> getControlEventByBaseTemplate(String baseTemplateId);
	public List<CanvasEventVO> getCanvasEventByBaseTemplate(String basetemplate);
	public List<PropertyControlPatternMasterVO> getPropertyControlPatternMaster();
	public List<PropertyControlEventPatternMasterVO> getPropertyControlEventPatternMaster();
	public List<ActionStandAloneVO> getActionStandAloneByBaseTemplate(String basetemplateId, String deviceType);
	public Map<String, ActionGroupVO> getActionGroupMapByBt(String baseTemplate);
	public Map<String, Map<String, Map<String, CtAcDtArVO>>> getCtAcDtArByBT(String baseTemplateId);
	public JsonNode getBtPropertyBlob(String baseTemplateId);
	public List<ClientEventRuleToPropertyVO> getClientEventRuleTOProeprtyMap();
	public List<GroupActionTxnPropertiesVO> getGroupActionProperties(String baseTemplateId);
	public List<ActionStandAlonePropertiesVO> getStandAloneProperties(String baseTemplateId);
	/**
	 * @return
	 */
	public List<IconMasterVO> getIconMasterList();
	/**
	 * @param canvasMasterMap
	 * @return
	 */
	public List<CanvasMasterVO> getCanvasMasterList(Map<String, String> canvasMasterMap);
	/**
	 * @param allCanvasIdsList
	 * @return
	 */
	public List<ControlMasterVO> getControlMasterList(List<String> allCanvasIdsList);

	/**
	 *
	 * @param allCanvasIdsList
	 * @return
     */
	List<PropertyControlMasterVO> getPropertyControlMasterList(List<String> allCanvasIdsList);

    List<AppMainMenuVO> getAppMainMenuVOMap(String personId, String menuType, String deviceType);

//    PersonPreferenceVO getPersonPreference(String personId);

	List<ProcessHomePageFooterVO> getProcessHomePageLayoutFooter(String processTypeCode, String personId);

	List<ProcessHomePageIntroContentVO> getProcessHomePageIntroContent(String processTypeCode, String personId);

	List<ProcessHomePageVO> getProcessHomePage(String processTypeCode, String personId);

	Map<String, List<ProcessHomePageContentVO>> getProcessHomePageContentVO(String processTypeCode, String personId);

	List<ThemeVO> getThemeVOs(String personId, String themeHeaderFkId);

    List<PersonTagsVO> getPersonTagsVoList(String personId);

	List<SocialStatusVO> getSocialStatusVOList();

	List<LicencedLocalesVO> getLicencedLocale();

    List<AppHomePageVO> getAppHomePageList();

	List<AppHomePageSectionVO> getAppHomePageSectionList();

	List<AppHomePageSecnContentVO> appHomePageSecnContentVOList();

    List<WishTemplateVO> getwishTemplatesVO(String personPreferencesPkId);

	List<VisualArtefactLibraryVO> getImagesFromVisualArtefactLibrary(String libType);

    RatingLevelVO getRatingLevel(String ratingScaleId, double anchor);

    String calculateName(String formula);
    List<Tutorial> getPersonTutorials(String personId,String tutorialCatId);
    Map<String, ActivityLog> getActivityLog(String versionId);
    void populateActivityDetails(Map<String, ActivityLog> activityId, String mtPE) throws SQLException;
}
