package com.tapplent.platformutility.layout.valueObject;

public class ActionStepPropertyVO {
	private String actionStepPropertyId;
	private String baseTemplateId;
	private String mtPEId;
	private String actionStepLookupId;
	private String propertyCode;
	private String propertyValue;
	public String getActionStepPropertyId() {
		return actionStepPropertyId;
	}
	public void setActionStepPropertyId(String actionStepPropertyId) {
		this.actionStepPropertyId = actionStepPropertyId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPEId() {
		return mtPEId;
	}
	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}
	public String getActionStepLookupId() {
		return actionStepLookupId;
	}
	public void setActionStepLookupId(String actionStepLookupId) {
		this.actionStepLookupId = actionStepLookupId;
	}
	
	public String getPropertyCode() {
		return propertyCode;
	}
	public void setPropertyCode(String propertyCode) {
		this.propertyCode = propertyCode;
	}
	public String getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
}
