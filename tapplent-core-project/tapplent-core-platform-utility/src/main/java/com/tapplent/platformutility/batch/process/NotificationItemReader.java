package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;


public class NotificationItemReader extends JdbcPagingItemReader<NTFN_NotificationData> {

	public NotificationItemReader() {
		super();
		DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
		setDataSource(ds);
		setPageSize(20);
		setQueryProvider(queryProvider(ds));
		setRowMapper(rowMapper());
	}
	
	private RowMapper<NTFN_NotificationData> rowMapper() {
		return new NotificationRowMapper();
	}

	private PagingQueryProvider queryProvider(DataSource ds) {
		 MySqlPagingQueryProvider provider = new MySqlPagingQueryProvider();
		    provider.setSelectClause("T_PRN_IEU_NOTIFICATION.PERSON_NOTIFICATION_PK_ID,"
		    		+ "T_PRN_IEU_NOTIFICATION.PERSON_FK_ID,"
		    		+ "T_PRN_IEU_NOTIFICATION.MT_PE_CODE_FK_ID,"
		    		+ "T_PRN_IEU_NOTIFICATION.DO_ROW_PRIMARY_KEY_TXT,"
		    		+ "T_PRN_IEU_NOTIFICATION.DO_ROW_VERSION_FK_ID,"
		    		+ "T_PRN_IEU_NOTIFICATION.DEPENDENCY_KEY_EXPN,"
		    		+ "T_PRN_IEU_NOTIFICATION.NOTIFICATION_DEFINITION_FK_ID,"
		    		+ "T_PRN_IEU_NOTIFICATION.GENERATE_DATETIME,"
		    		+ "T_PRN_IEU_NOTIFICATION.RETURN_STATUS_CODE_FK_ID,"
		    		+ "T_PRN_IEU_NOTIFICATION.IS_SYNC,"
		    		+ "T_PFM_ADM_NOTIFICATION_DEFINITION.NTFN_BT_CODE_FK_ID,"
		    		+ "T_PFM_ADM_NOTIFICATION_DEFINITION.NTFN_MT_PE_CODE_FK_ID,"
		    		+ "T_PFM_ADM_NOTIFICATION_DEFINITION.NTFN_VT_CODE_FK_ID,"
		    		+ "T_PFM_ADM_NOTIFICATION_DEFINITION.NTFN_CANVAS_TXN_FK_ID,"
		    		+ "T_PFM_ADM_NOTIFICATION_DEFINITION.FOR_DOC_GEN_DEFN_FK_ID");
		    provider.setFromClause("FROM"
		    		+ "T_PRN_IEU_NOTIFICATION INNER JOIN T_PFM_ADM_NOTIFICATION_DEFINITION ON"
		    		+ "T_PRN_IEU_NOTIFICATION.NOTIFICATION_DEFINITION_FK_ID = "
		    		+ "T_PFM_ADM_NOTIFICATION_DEFINITION.NOTIFICATION_DEFINITION_PK_ID");
		    provider.setWhereClause("T_PRN_IEU_NOTIFICATION.RETURN_STATUS_CODE_FK_ID = 'PENDING' and "
		    		+ "T_PRN_IEU_NOTIFICATION.IS_SYNC = false");
		    Map abc = new HashMap<String, Order>();
		    abc.put("PERSON_NOTIFICATION_PK_ID", Order.ASCENDING);
		   provider.setSortKeys(abc);
		
		try {
			return provider;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
