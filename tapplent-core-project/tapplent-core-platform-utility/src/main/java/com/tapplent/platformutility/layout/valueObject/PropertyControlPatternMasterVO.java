package com.tapplent.platformutility.layout.valueObject;

import java.sql.Timestamp;

import com.fasterxml.jackson.databind.JsonNode;

public class PropertyControlPatternMasterVO {
	/*
	 * VERSION ID                             
PROPERTY CONTROL PATTERN PK ID         
PROPERTY CONTROL PATTERN CODE FK ID    
DEVICE INDEPENDENT BLOB                
DEVICE DEPENDENT BLOB                  
EFFECTIVE DATETIME                     
SAVE STATE CODE FK ID                  
ACTIVE STATE CODE FK ID                
RECORD STATE CODE FK ID                
IS DELETED                             
VERSION CHANGES BLOB                   
PROPOGATE FUTURE CHANGES BLOB          
CREATEDBY PERSON FK ID                 
CREATED BY PERSON FULLNAME TXT         
CREATED DATETIME                       
LAST MODIFIEDBY PERSON FK ID           
LAST MODIFIEDBY PERSON FULLNAME TXT    
LAST MODIFIED DATETIME                 
DLGTD FOR CREATEDBY PRSN FK ID         
DLGTD FOR CREATEDBY PRSN FULLNAME TXT  
DLGTD FOR MDFDBY PRSN FK ID            
DLGTD FOR MDFDBY PRSN FULLNAME TXT     
IS FEATURED                            
EXTERNAL REFERENCE BLOB                
REF BASE OBJECT FK ID                  
REF BASE TEMPLATE FK ID                
TRANSITIONING BASE TEMPLATE FK ID      
REF WORKFLOW DEFN FK ID                
REF WORKFLOW TXN FK ID                 
REF WORKFLOW TXN STATUS CODE FK ID     
REF WORKFLOW TXN STEP FK ID            
REF WORKFLOW TXN STEP STATUS CODE FK ID
	 */
	private String versionId; 
	private String propertyControlPatternPkId; 
	private String propertyControlPatternCodeFkId;
	private String propertyControlCode;
//	private String controlTypeCode;
	private JsonNode deviceIndependentBlob; 
	private JsonNode deviceDependentBlob;
	private Timestamp effectiveDatetime;
	private String saveStateCodeFkId; 
	private String activeStateCodeFkId; 
	private String recordStateCodeFkId; 
	private boolean isDeleted;
	private JsonNode pclVersionChangesBlob;
	private JsonNode mstVersionChangesBlob;
	private JsonNode mstPropogateFutureChangesBlob;
	private JsonNode pclPropogateFutureChangesBlob;
	private String createdbyPersonFkId; 
	private String createdByPersonFullnameTxt;
	private Timestamp createdDatetime ;
	private String lastModifiedbyPersonFkId; 
	private String lastModifiedbyPersonFullnameTxt ;
	private Timestamp lastModifiedDatetime ;
	private String dlgtdForCreatedbyPrsnFkId; 
	private String dlgtdForCreatedbyPrsnFullnameTxt ;
	private String dlgtdForMdfdbyPrsnFkId; 
	private String dlgtdForMdfdbyPrsnFullnameTxt ;
	private boolean isFeatured ;
	private JsonNode externalReferenceBlob; 
	private String refBaseObjectFkId; 
	private String refBaseTemplateFkId; 
	private String transitioningBaseTemplateFkId; 
	private String refWorkflowDefnFkId; 
	private String refWorkflowTxnFkId; 
	private String refWorkflowTxnStatusCodeFkId; 
	private String refWorkflowTxnStepFkId; 
	private String refWorkflowTxnStepStatusCodeFkId;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getPropertyControlPatternPkId() {
		return propertyControlPatternPkId;
	}
	public void setPropertyControlPatternPkId(String propertyControlPatternPkId) {
		this.propertyControlPatternPkId = propertyControlPatternPkId;
	}
	public String getPropertyControlPatternCodeFkId() {
		return propertyControlPatternCodeFkId;
	}
	public void setPropertyControlPatternCodeFkId(String propertyControlPatternCodeFkId) {
		this.propertyControlPatternCodeFkId = propertyControlPatternCodeFkId;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public Timestamp getEffectiveDatetime() {
		return effectiveDatetime;
	}
	public void setEffectiveDatetime(Timestamp effectiveDatetime) {
		this.effectiveDatetime = effectiveDatetime;
	}
	public String getSaveStateCodeFkId() {
		return saveStateCodeFkId;
	}
	public void setSaveStateCodeFkId(String saveStateCodeFkId) {
		this.saveStateCodeFkId = saveStateCodeFkId;
	}
	public String getActiveStateCodeFkId() {
		return activeStateCodeFkId;
	}
	public void setActiveStateCodeFkId(String activeStateCodeFkId) {
		this.activeStateCodeFkId = activeStateCodeFkId;
	}
	public String getRecordStateCodeFkId() {
		return recordStateCodeFkId;
	}
	public void setRecordStateCodeFkId(String recordStateCodeFkId) {
		this.recordStateCodeFkId = recordStateCodeFkId;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public JsonNode getPclVersionChangesBlob() {
		return pclVersionChangesBlob;
	}
	public void setPclVersionChangesBlob(JsonNode pclVersionChangesBlob) {
		this.pclVersionChangesBlob = pclVersionChangesBlob;
	}
	public JsonNode getMstVersionChangesBlob() {
		return mstVersionChangesBlob;
	}
	public void setMstVersionChangesBlob(JsonNode mstVersionChangesBlob) {
		this.mstVersionChangesBlob = mstVersionChangesBlob;
	}
	public JsonNode getMstPropogateFutureChangesBlob() {
		return mstPropogateFutureChangesBlob;
	}
	public void setMstPropogateFutureChangesBlob(JsonNode mstPropogateFutureChangesBlob) {
		this.mstPropogateFutureChangesBlob = mstPropogateFutureChangesBlob;
	}
	public JsonNode getPclPropogateFutureChangesBlob() {
		return pclPropogateFutureChangesBlob;
	}
	public void setPclPropogateFutureChangesBlob(JsonNode pclPropogateFutureChangesBlob) {
		this.pclPropogateFutureChangesBlob = pclPropogateFutureChangesBlob;
	}
	public String getCreatedbyPersonFkId() {
		return createdbyPersonFkId;
	}
	public void setCreatedbyPersonFkId(String createdbyPersonFkId) {
		this.createdbyPersonFkId = createdbyPersonFkId;
	}
	public String getCreatedByPersonFullnameTxt() {
		return createdByPersonFullnameTxt;
	}
	public void setCreatedByPersonFullnameTxt(String createdByPersonFullnameTxt) {
		this.createdByPersonFullnameTxt = createdByPersonFullnameTxt;
	}
	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public String getLastModifiedbyPersonFkId() {
		return lastModifiedbyPersonFkId;
	}
	public void setLastModifiedbyPersonFkId(String lastModifiedbyPersonFkId) {
		this.lastModifiedbyPersonFkId = lastModifiedbyPersonFkId;
	}
	public String getLastModifiedbyPersonFullnameTxt() {
		return lastModifiedbyPersonFullnameTxt;
	}
	public void setLastModifiedbyPersonFullnameTxt(String lastModifiedbyPersonFullnameTxt) {
		this.lastModifiedbyPersonFullnameTxt = lastModifiedbyPersonFullnameTxt;
	}
	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(Timestamp lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
	public String getDlgtdForCreatedbyPrsnFkId() {
		return dlgtdForCreatedbyPrsnFkId;
	}
	public void setDlgtdForCreatedbyPrsnFkId(String dlgtdForCreatedbyPrsnFkId) {
		this.dlgtdForCreatedbyPrsnFkId = dlgtdForCreatedbyPrsnFkId;
	}
	public String getDlgtdForCreatedbyPrsnFullnameTxt() {
		return dlgtdForCreatedbyPrsnFullnameTxt;
	}
	public void setDlgtdForCreatedbyPrsnFullnameTxt(String dlgtdForCreatedbyPrsnFullnameTxt) {
		this.dlgtdForCreatedbyPrsnFullnameTxt = dlgtdForCreatedbyPrsnFullnameTxt;
	}
	public String getDlgtdForMdfdbyPrsnFkId() {
		return dlgtdForMdfdbyPrsnFkId;
	}
	public void setDlgtdForMdfdbyPrsnFkId(String dlgtdForMdfdbyPrsnFkId) {
		this.dlgtdForMdfdbyPrsnFkId = dlgtdForMdfdbyPrsnFkId;
	}
	public String getDlgtdForMdfdbyPrsnFullnameTxt() {
		return dlgtdForMdfdbyPrsnFullnameTxt;
	}
	public void setDlgtdForMdfdbyPrsnFullnameTxt(String dlgtdForMdfdbyPrsnFullnameTxt) {
		this.dlgtdForMdfdbyPrsnFullnameTxt = dlgtdForMdfdbyPrsnFullnameTxt;
	}
	public boolean isFeatured() {
		return isFeatured;
	}
	public void setFeatured(boolean isFeatured) {
		this.isFeatured = isFeatured;
	}
	public JsonNode getExternalReferenceBlob() {
		return externalReferenceBlob;
	}
	public void setExternalReferenceBlob(JsonNode externalReferenceBlob) {
		this.externalReferenceBlob = externalReferenceBlob;
	}
	public String getRefBaseObjectFkId() {
		return refBaseObjectFkId;
	}
	public void setRefBaseObjectFkId(String refBaseObjectFkId) {
		this.refBaseObjectFkId = refBaseObjectFkId;
	}
	public String getRefBaseTemplateFkId() {
		return refBaseTemplateFkId;
	}
	public void setRefBaseTemplateFkId(String refBaseTemplateFkId) {
		this.refBaseTemplateFkId = refBaseTemplateFkId;
	}
	public String getTransitioningBaseTemplateFkId() {
		return transitioningBaseTemplateFkId;
	}
	public void setTransitioningBaseTemplateFkId(String transitioningBaseTemplateFkId) {
		this.transitioningBaseTemplateFkId = transitioningBaseTemplateFkId;
	}
	public String getRefWorkflowDefnFkId() {
		return refWorkflowDefnFkId;
	}
	public void setRefWorkflowDefnFkId(String refWorkflowDefnFkId) {
		this.refWorkflowDefnFkId = refWorkflowDefnFkId;
	}
	public String getRefWorkflowTxnFkId() {
		return refWorkflowTxnFkId;
	}
	public void setRefWorkflowTxnFkId(String refWorkflowTxnFkId) {
		this.refWorkflowTxnFkId = refWorkflowTxnFkId;
	}
	public String getRefWorkflowTxnStatusCodeFkId() {
		return refWorkflowTxnStatusCodeFkId;
	}
	public void setRefWorkflowTxnStatusCodeFkId(String refWorkflowTxnStatusCodeFkId) {
		this.refWorkflowTxnStatusCodeFkId = refWorkflowTxnStatusCodeFkId;
	}
	public String getRefWorkflowTxnStepFkId() {
		return refWorkflowTxnStepFkId;
	}
	public void setRefWorkflowTxnStepFkId(String refWorkflowTxnStepFkId) {
		this.refWorkflowTxnStepFkId = refWorkflowTxnStepFkId;
	}
	public String getRefWorkflowTxnStepStatusCodeFkId() {
		return refWorkflowTxnStepStatusCodeFkId;
	}
	public void setRefWorkflowTxnStepStatusCodeFkId(String refWorkflowTxnStepStatusCodeFkId) {
		this.refWorkflowTxnStepStatusCodeFkId = refWorkflowTxnStepStatusCodeFkId;
	}
	public String getPropertyControlCode() {
		return propertyControlCode;
	}
	public void setPropertyControlCode(String propertyControlCode) {
		this.propertyControlCode = propertyControlCode;
	}
}
