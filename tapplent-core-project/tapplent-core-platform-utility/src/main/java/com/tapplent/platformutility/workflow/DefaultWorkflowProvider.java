package com.tapplent.platformutility.workflow;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.expression.jeval.EvaluationException;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.insert.impl.InsertDAO;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.impl.SearchDAO;
import com.tapplent.platformutility.workflow.service.WorkflowProvider;
import com.tapplent.tenantresolver.server.property.ServerActionStep;

public class DefaultWorkflowProvider {
	
	SearchDAO searchDAO;
	
	InsertDAO insertDAO;


	public void CreateLaunchWorkflowIfApplicable(GlobalVariables globalVariables) {
		String targetWorkflowDefFkId = checkIfWorflowCreationApplicable(globalVariables);
		if(targetWorkflowDefFkId != null){
//			String SQL = null;
			final String SQL = "INSERT INTO T_PFM_AUT_WORKFLOW_TRANSACTION (x?, x?, x?, x?, x?, ?, "
					+ "x?, ?, ?, x?, WORKFLOW_DEFINITION_FK_ID, WORKFLOW_STATUS_CODE_FK_ID, "
					+ "WORKFLOW_NUDGE_NOTIFICATION_FK_ID, WORKFLOW_CANCEL_NOTIFICATION_FK_ID, STEP_FK_ID, STEP_SEQUENCE_NUMBER_POS_INT, STEP_NAME_G11N_ID, STEP_PLANNED_START_DATETIME, STEP_PLANNED_END_DATETIME, "
					+ "STEP_PLANNED_DUE_DATETIME, STEP_ACTOR_RESOLUTION_CODE_FK_ID, NO_ACTORS_ACTION_CODE_FK_ID, SOURCE_ACTOR_EXIT_COUNT_EXPRESSION_TXT, TARGET_ACTOR_ENTRY_COUNT_EXPRESSION_TXT, "
					+ "WORKFLOW_STEP_CANCEL_NOTIFICATION_FK_ID, STEP_ACTUAL_START_DATETIME, STEP_ACTUAL_END_DATETIME, STEP_STATUS_CODE_FK_ID, WORKFLOW_ACTOR_FK_ID, ACTOR_SEQUENCE_NUMBER_POS_INT, "
					+ "ACTOR_NAME_G11N_ID, ACTOR_ICON_CODE_FK_ID, ACTOR_PERSON_GROUP_FK_ID, IS_RELATIVE_TO_LOGGED_IN_USER, IS_RELATIVE_TO_SUBJECT_USER, RELATIVE_TO_ACTOR_ID, RELATIVE_RELATIONSHIP_CODE_FK_ID, "
					+ "RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL, RESOLVE_ACTOR_AS_OF_DATETIME, MAX_ADHOC_USERS_PERMITTED_POS_INT, ACTOR_ACTION_FK_ID, ACTOR_ACTION_SEQUENCE_NUMBER_POS_INT, ACTION_TYPE_CODE_FK_ID, "
					+ "ACTOR_ACTION_RECEIVE_DATETIME, ACTOR_ACTION_COMPLETE_DATETIME, ACTOR_ACTION_STATUS_CODE_FK_ID, TARGET_ACTOR_WORKFLOW_STEP_DEP_FK_ID, TARGET_ACTOR_FK_ID, IF_ACTION_CONDITION_EXPRESSION_TXT, "
					+ "IS_ACTION_TO_EXECUTE_AS_DEFAULT_ON_FORCED_MOVE, IS_ACTION_DELEGATEABLE, ALLOW_ACTION_ON_SELF_AS_SUBJECT, IS_AUTO_EXECITE_IF_ACTOR_EQUAL_TO_INITIATOR, IS_AUTO_EXECUTE_IF_DUPLICATE, "
					+ "IS_ACTION_COMMENT_MANDATORY, SOURCE_ACTOR_ACTION_EXIT_NOTIFICATION_FK_ID, TARGET_ACTOR_ACTION_ENTRY_NOTIFICATION_FK_ID, WORKFLOW_ACTION_CANCEL_NOTIFICATION_FK_ID, ALLOW_REMINDER_TO_SOURCE_ACTOR, "
					+ "ALLOW_ESCALATION_ON_SOURCE_ACTOR, ON_COMPLETION_EXECUTE_EXPRESSION_TXT, ON_CANCELLATION_EXECUTE_EXPRESSION_TXT, IS_ACTION_COMPLETED_BUT_NOT_SENT_TO_TARGET) "

					+ "SELECT Defintion.WORKFLOW_DEFINITION_FK_ID, WORKFLOW_STATUS_CODE_FK_ID, "
					+ "Defintion.WORKFLOW_NUDGE_NOTIFICATION_FK_ID, Defintion.WORKFLOW_CANCEL_NOTIFICATION_FK_ID, Step.STEP_FK_ID, Step.STEP_SEQUENCE_NUMBER_POS_INT, Step.STEP_NAME_G11N_ID, Step.STEP_PLANNED_START_DATETIME, Step.STEP_PLANNED_END_DATETIME, "
					+ "Step.STEP_PLANNED_DUE_DATETIME, Step.STEP_ACTOR_RESOLUTION_CODE_FK_ID, Step.NO_ACTORS_ACTION_CODE_FK_ID, Step.SOURCE_ACTOR_EXIT_COUNT_EXPRESSION_TXT, Step.TARGET_ACTOR_ENTRY_COUNT_EXPRESSION_TXT, "
					+ "Step.WORKFLOW_STEP_CANCEL_NOTIFICATION_FK_ID, STEP_ACTUAL_START_DATETIME, STEP_ACTUAL_END_DATETIME, STEP_STATUS_CODE_FK_ID, Actor.WORKFLOW_ACTOR_FK_ID, Actor.ACTOR_SEQUENCE_NUMBER_POS_INT, "
					+ "Actor.ACTOR_NAME_G11N_ID, Actor.ACTOR_ICON_CODE_FK_ID, Actor.ACTOR_PERSON_GROUP_FK_ID, Actor.IS_RELATIVE_TO_LOGGED_IN_USER, Actor.IS_RELATIVE_TO_SUBJECT_USER, Actor.RELATIVE_TO_ACTOR_ID, Actor.RELATIVE_RELATIONSHIP_CODE_FK_ID, "
					+ "Actor.RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL, Actor.RESOLVE_ACTOR_AS_OF_DATETIME, Actor.MAX_ADHOC_USERS_PERMITTED_POS_INT, ActorAction.ACTOR_ACTION_FK_ID, ActorAction.ACTOR_ACTION_SEQUENCE_NUMBER_POS_INT, ActorAction.ACTION_TYPE_CODE_FK_ID, "
					+ "ACTOR_ACTION_RECEIVE_DATETIME, ACTOR_ACTION_COMPLETE_DATETIME, ACTOR_ACTION_STATUS_CODE_FK_ID, ActorAction.TARGET_ACTOR_WORKFLOW_STEP_DEP_FK_ID, ActorAction.TARGET_ACTOR_FK_ID, ActorAction.IF_ACTION_CONDITION_EXPRESSION_TXT, "
					+ "ActorAction.IS_ACTION_TO_EXECUTE_AS_DEFAULT_ON_FORCED_MOVE, ActorAction.IS_ACTION_DELEGATEABLE, ActorAction.ALLOW_ACTION_ON_SELF_AS_SUBJECT, ActorAction.IS_AUTO_EXECITE_IF_ACTOR_EQUAL_TO_INITIATOR, ActorAction.IS_AUTO_EXECUTE_IF_DUPLICATE, "
					+ "ActorAction.IS_ACTION_COMMENT_MANDATORY, ActorAction.SOURCE_ACTOR_ACTION_EXIT_NOTIFICATION_FK_ID, ActorAction.TARGET_ACTOR_ACTION_ENTRY_NOTIFICATION_FK_ID, ActorAction.WORKFLOW_ACTION_CANCEL_NOTIFICATION_FK_ID, ActorAction.ALLOW_REMINDER_TO_SOURCE_ACTOR, "
					+ "ActorAction.ALLOW_ESCALATION_ON_SOURCE_ACTOR, ActorAction.ON_COMPLETION_EXECUTE_EXPRESSION_TXT, ActorAction.ON_CANCELLATION_EXECUTE_EXPRESSION_TXT, ActorAction.IS_ACTION_COMPLETED_BUT_NOT_SENT_TO_TARGET"

					+" FROM T_PFM_ADM_WORKFLOW_DEFINITION AS Defintion LEFT JOIN T_PFM_ADM_WORKFLOW_STEP AS Step ON Definition.WORFKLOW_DEFINITION_PK_ID = Step.WORKFLOW_DEFINITION_DEP_FK_ID "
					+ "LEFT JOIN T_PFM_ADM_WORKFLOW_ACTOR AS Actor ON (Step.WORKFLOW_DEFINITION_DEP_FK_ID = Actor.WORKFLOW_DEFINITION_DEP_FK_ID AND Step.WORKFLOW_STEP_PK_ID = Actor.WORKFLOW_STEP_FK_ID) "
					+ "LEFT JOIN T_PFM_ADM_WORKFLOW_ACTOR_ACTION AS ActorAction ON (Actor.WORKFLOW_DEFINITION_DEP_FK_ID = ActorAction.WORKFLOW_DEFINITION_DEP_FK_ID AND "
					+ "Actor.WORKFLOW_STEP_FK_ID = ActorAction.SOURCE_ACTOR_STEP_DEP_FK_ID AND Actor.WORKFLOW_ACTOR_PK_ID = ActorAction.SOURCE_ACTOR_FK_ID) "

					+ "WHERE Definition.WORKFLOW_DEFINITION_PK_ID = x? AND Definition.SaveState = 'SAVED' AND Definition.ActiveState = 'ACTIVE' AND "
					+ "Definition.RecordState = 'CURRENT' AND Definition.WORFKLOW_DEFINITION_PK_ID = Step.WORKFLOW_DEFINITION_DEP_FK_ID AND Step.SaveState = 'SAVED' AND Step.ActiveState = 'Active' "
					+ "AND Step.RecordState = 'CURRENT' AND Step.WORKFLOW_STEP_PK_ID = Actor.WORKFLOW_STEP_FK_ID AND Actor.SaveState = 'SAVED' AND Actor.ActiveState = 'ACTIVE' AND "
					+ "Actor.RecordState = 'CURRENT' AND ActorAction.SaveState = 'SAVED' AND ActorAction.ActiveState = 'ACTIVE' AND ActorAction.RecordState = 'CURRENT'";

			Map<String, Object> insertValueMap = globalVariables.model.getInsertValueMap();
//			List<String, EntityAtttributeMetaData> filteredList = metaDataMap.at
			LinkedHashMap<String, String> argumentMap = new LinkedHashMap<>();
			globalVariables.workflowDetails.workflowTxnId = Common.getUUID();
			argumentMap.put("WORKFLOW_TRANSACTION_PK_ID", globalVariables.workflowDetails.workflowTxnId);
			argumentMap.put("BASE_OBJECT_FK_ID",insertValueMap.get("BASE_OBJECT_FK_ID").toString());
			argumentMap.put("BASE_TEMPLATE_FK_ID",insertValueMap.get("BASE_TEMPLATE_FK_ID").toString());
			argumentMap.put("BT_DO_SPEC_FK_ID",insertValueMap.get("BT_DO_SPEC_FK_ID").toString());
			argumentMap.put("DOMAIN_OBJECT_FK_ID",insertValueMap.get("DOMAIN_OBJECT_FK_ID").toString());
			argumentMap.put("DB_TABLE_NAME_TXT",insertValueMap.get("DB_TABLE_NAME_TXT").toString());
			argumentMap.put("DO_RECORD_VERSION_FK_ID",insertValueMap.get("DO_RECORD_VERSION_FK_ID").toString());
//			arguments.put("DO_RECORD_CONTEXT_ATTRIBUTE_VALUE_TXT",insertValueMap.get("DO_RECORD_CONTEXT_ATTRIBUTE_VALUE_TXT"));///
//			arguments.put("DO_ICON_CODE_FK_ID",insertValueMap.get("DO_ICON_CODE_FK_ID"));
//			arguments.put("DO_RECORD_SUBJECT_USER_PERSON_ID",insertValueMap.get("DO_RECORD_SUBJECT_USER_PERSON_ID"));
			argumentMap.put("TARGET_WORKFLOW_DEFINITION_FK_ID", targetWorkflowDefFkId);
			insertDAO.insertRowWithParameterMap(SQL, argumentMap);
		}
		if(globalVariables.workflowDetails.workflowTxnId != null)
			globalVariables.workflowDetails.isWorkflowApplicable = true;
	}

	private String checkIfWorflowCreationApplicable(GlobalVariables globalVariables) {
		String refWorkflowTxnStatusCodeFkId = (String) globalVariables.model.getInsertValueMap().get("REF_WORKFLOW_TXN_STATUS_CODE_FK_ID");
		String targetWorkflowDefFkId = null;
		if(refWorkflowTxnStatusCodeFkId != null && (!globalVariables.model.getInsertValueMap().containsKey("REF_WORKFLOW_TXN_FK_ID") || 
				globalVariables.model.getInsertValueMap().get("REF_WORKFLOW_TXN_FK_ID").equals("") || (globalVariables.model.getInsertValueMap().get("REF_WORKFLOW_TXN_FK_ID") != null))
				&& (refWorkflowTxnStatusCodeFkId.equalsIgnoreCase("COMPLETED") ||refWorkflowTxnStatusCodeFkId.equalsIgnoreCase("CANCELLED") || 
						refWorkflowTxnStatusCodeFkId.equalsIgnoreCase("WITHDRAWN"))){
			targetWorkflowDefFkId = getWorkflowDefinition(globalVariables);
		}
		return targetWorkflowDefFkId;
	}

	private String getWorkflowDefinition(GlobalVariables globalVariables) {
		String targetWorkflowDefFkId = null;
		Deque<Object> parameter = new ArrayDeque<>();
		Map<String, Object> insertValueMap = globalVariables.model.getInsertValueMap();
		parameter.add(globalVariables.actionCode) ;	//FIXME :GetPreviousActionCode
		parameter.add(insertValueMap.get("BASE_OBJECT_FK_ID"));
		parameter.add(insertValueMap.get("BASE_TEMPLATE_FK_ID"));
		parameter.add(insertValueMap.get("BT_DO_SPEC_FK_ID"));
		String sql = "SELECT WORKFLOW_DEFINTION_FK_ID, CONDITION_DOA_EXPN FROM  T_PFM_ADM_WORKFLOW_SELECTION"
					+ "WHERE ACTION_CODE_FK_ID = ? AND BASE_OBJECT_FK_ID = x? AND BASE_TEMPLATE_FK_ID = x? AND BT_DO_SPEC_FK_ID = x? "
					+ "AND	SAVE_STATE_CODE_FK_ID = 'SAVED' AND RECORD_STATE_CODE_FK_ID = 'CURRENT' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE';";
		SearchResult result = searchDAO.search(sql, parameter);
		List<Map<String, Object>> resultList = result.getData();
		Evaluator evaluator = new Evaluator();
		populateDynamicValuesInEvaluator(evaluator, insertValueMap);
		int count = 0;
		for(Map<String, Object> record : resultList){
			String expression = (String) record.get("DOMAIN_OBJECT_FILTER_EXPRESSION_TXT");
			try {
				boolean matches = evaluator.getBooleanResult(expression);
				if(matches){
					targetWorkflowDefFkId = (String) record.get("TARGET_WORKFLOW_DEFINITION_FK_ID");
					count++;
					if(count > 1){
						return null;
					}
				}
			} catch (EvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return targetWorkflowDefFkId;
	}

	private void populateDynamicValuesInEvaluator(Evaluator evaluator, Map<String, Object> insertValueMap) {
		for(Entry<String, Object> entry : insertValueMap.entrySet()){
			evaluator.putVariable(entry.getKey(), entry.getValue().toString());
		}
	}


	public void resloveActors(ServerActionStep currentActionStep, GlobalVariables globalVariables) throws SQLException {
		WorkflowTransactionDetails workflowTransactionDetails = getWorkflowTransactionDetails(globalVariables);
		if(workflowTransactionDetails != null){
			String propertyCode = currentActionStep.getPropertyCodeValueMap().get("RESOLVE_STATE");
			String SQL = getRelevantQueryForResolvingActors(workflowTransactionDetails, propertyCode);
			String workflowTransactionActorPersonPkId = Common.getUUID();
			Deque<Object> arguments = new ArrayDeque<Object>();
			arguments.push(workflowTransactionActorPersonPkId);
			arguments.push(workflowTransactionDetails.getWorkflowTransactionPkId());
			arguments.push(workflowTransactionDetails.getWorkflowTransactionPkId());
			if(propertyCode.equals("AT_STEP"))
				arguments.push(workflowTransactionDetails.getStepFkId());
			insertDAO.insertRowWithParameter(SQL, arguments);
//			if(serverActionStepList.hasNext())
//				currentActionStep = serverActionStepList.next();
		}
	}

	private String getRelevantQueryForResolvingActors(WorkflowTransactionDetails workflowTransactionDetails, String propertyCode) {
		String query = null;
		if(workflowTransactionDetails.getActorPersonGroupFkId() != null)
			query = ResolveActorQuery.valueOf(propertyCode + "_ABSOLUTE_GROUP").toString();
		else if(workflowTransactionDetails.isRelativeToLoggedInUser())
			query = ResolveActorQuery.valueOf(propertyCode + "_RELATIVE_TO_LOGGED_IN_USER").toString();
		else if(workflowTransactionDetails.isRelativeToSubjectUser())
			query = ResolveActorQuery.valueOf(propertyCode + "_RELATIVE_TO_SUBJECT_USER").toString();
		else if (workflowTransactionDetails.getRelativeToActorId() != null)
			query = ResolveActorQuery.valueOf(propertyCode + "_RELATIVE_TO_ANOTHER_ACTOR").toString();
		return query;
	}

	private WorkflowTransactionDetails getWorkflowTransactionDetails(GlobalVariables globalVariables) {
		String sql = "SELECT ACTOR_PERSON_GROUP_FK_ID, IS_RELATIVE_TO_LOGGED_IN_USER, IS_RELATIVE_TO_SUBJECT_USER, RELATIVE_TO_ACTOR_ID, RELATIVE_RELATIONSHIP_CODE_FK_ID, "
				+ "RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT, RESOLVE_ACTOR_AS_OF_DATETIME, WORKFLOW_ACTOR_FK_ID, ACTOR_ACTION_FK_ID, ACTION_TYPE_CODE_FK_ID, STEP_FK_ID "
				+ "FROM T_PFM_AUT_WORKFLOW_TRANSACTION WHERE WORKFLOW_TRANSACTION_PK_ID = x? LIMIT 1;";
		Deque<Object> parameter = new ArrayDeque<>();
		WorkflowTransactionDetails workflowTransactionDetails = null;
		String workflowTransactionPkId = globalVariables.workflowDetails.workflowTxnId;
		if(workflowTransactionPkId != null){
			parameter.add(workflowTransactionPkId != null ? workflowTransactionPkId : globalVariables.model.getInsertValueMap().get("REF_WORKFLOW_DEFN_FK_ID"));//FIXME correct the name or ensure the logic (insertData or valueobject)
			SearchResult result = searchDAO.search(sql, parameter);
			Map<String,Object> record = result.getData().get(0);
			if(record != null){
				workflowTransactionDetails = new WorkflowTransactionDetails();
				workflowTransactionDetails.setWorkflowTransactionPkId(workflowTransactionPkId);
				workflowTransactionDetails.setActorPersonGroupFkId((String) record.get("ACTOR_PERSON_GROUP_FK_ID"));
				workflowTransactionDetails.setRelativeToLoggedInUser((boolean) record.get("IS_RELATIVE_TO_LOGGED_IN_USER"));
				workflowTransactionDetails.setRelativeToSubjectUser((boolean) record.get("IS_RELATIVE_TO_SUBJECT_USER"));
				workflowTransactionDetails.setRelativeToActorId((String) record.get("RELATIVE_TO_ACTOR_ID"));
				workflowTransactionDetails.setRelativeRelationshipCodeFkId((String) record.get("RELATIVE_RELATIONSHIP_CODE_FK_ID"));
				workflowTransactionDetails.setRelativeRelationshipHierarchyLevelPosInt((String) record.get("RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT"));
				workflowTransactionDetails.setResolveActorAsOfDatetime((Timestamp) record.get("RESOLVE_ACTOR_AS_OF_DATETIME"));
				workflowTransactionDetails.setWorkflowActorFkId((String) record.get("WORKFLOW_ACTOR_FK_ID"));
				workflowTransactionDetails.setActorActionFkId((String) record.get("ACTOR_ACTION_FK_ID"));
				workflowTransactionDetails.setActionTypeCodeFkId((String) record.get("ACTION_TYPE_CODE_FK_ID"));
				workflowTransactionDetails.setStepFkId((String) record.get("STEP_FK_ID"));
			}
		}
		return workflowTransactionDetails;
	}


	public void generateWorkflowNotifaction(ServerActionStep currentActionStep, GlobalVariables globalVariables) {
		// TODO Auto-generated method stub
//		if(serverActionStepList.hasNext())
//			currentActionStep = serverActionStepList.next();
	}

	public SearchDAO getSearchDAO() {
		return searchDAO;
	}

	public void setSearchDAO(SearchDAO searchDAO) {
		this.searchDAO = searchDAO;
	}

	public InsertDAO getInsertDAO() {
		return insertDAO;
	}

	public void setInsertDAO(InsertDAO insertDAO) {
		this.insertDAO = insertDAO;
	}


}
