package com.tapplent.platformutility.common.cache;

import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.platformutility.system.SystemContextHolder;

public class SystemAwareCache {
	
	public synchronized static SystemObjectRepository getSystemRepository(){
		Object attribute = SystemContextHolder.getAttribute(SystemObjectRepository.class.getName());
		if(null==attribute){
			attribute=TapplentServiceLookupUtil.getSystemRepository();
			SystemContextHolder.setAttribute(MetadataObjectRepository.class.getName(), attribute);
		}
		return (SystemObjectRepository) attribute;
	}
	
}
