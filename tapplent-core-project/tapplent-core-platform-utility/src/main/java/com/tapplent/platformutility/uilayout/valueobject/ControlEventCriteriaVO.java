package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 05/10/17.
 */
public class ControlEventCriteriaVO {
    private String controlEventCriteriaPkID;
    private String controlEvent;
    private String criteriaControlInstance;
    private String operator;
    private String filterValue;

    public String getControlEventCriteriaPkID() {
        return controlEventCriteriaPkID;
    }

    public void setControlEventCriteriaPkID(String controlEventCriteriaPkID) {
        this.controlEventCriteriaPkID = controlEventCriteriaPkID;
    }

    public String getControlEvent() {
        return controlEvent;
    }

    public void setControlEvent(String controlEvent) {
        this.controlEvent = controlEvent;
    }

    public String getCriteriaControlInstance() {
        return criteriaControlInstance;
    }

    public void setCriteriaControlInstance(String criteriaControlInstance) {
        this.criteriaControlInstance = criteriaControlInstance;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }
}
