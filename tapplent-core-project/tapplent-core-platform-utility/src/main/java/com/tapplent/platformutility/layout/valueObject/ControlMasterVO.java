/**
 * 
 */
package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Shubham Patodi
 *
 */
public class ControlMasterVO {
	private String controlId;
	private String canvasId;
	private String controlAttribute;
	private String aggregateControlFunction;
	private JsonNode deviceIndependentBlob;
	private JsonNode deviceDependentBlob;
	public String getControlId() {
		return controlId;
	}
	public void setControlId(String controlId) {
		this.controlId = controlId;
	}
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public String getControlAttribute() {
		return controlAttribute;
	}
	public void setControlAttribute(String controlAttribute) {
		this.controlAttribute = controlAttribute;
	}
	public String getAggregateControlFunction() {
		return aggregateControlFunction;
	}
	public void setAggregateControlFunction(String aggregateControlFunction) {
		this.aggregateControlFunction = aggregateControlFunction;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
}
