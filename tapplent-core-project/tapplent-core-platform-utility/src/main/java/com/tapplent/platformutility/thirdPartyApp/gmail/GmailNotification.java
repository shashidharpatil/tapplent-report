package com.tapplent.platformutility.thirdPartyApp.gmail;
import org.apache.commons.codec.binary.Base64;

import com.google.api.client.http.InputStreamContent;
import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.NTFN_NotfnDoaMap;
import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;

import java.util.Map.Entry;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import com.tapplent.platformutility.common.util.GmailTokenUtil;

import com.tapplent.platformutility.thirdPartyApp.facebook.FacebookNotification;
public class GmailNotification {
	

//This function will send the simple mail on the behalf of the user
public static void sendMail(NTFN_NotificationData obj) throws ClientProtocolException, IOException, JSONException, SQLException{
	
	Map<String,Object> credentialMap =obj.getCredentials();
	JSONObject googleCredentials = (JSONObject) credentialMap.get("google");
	String refresh_token = googleCredentials.getString("refresh_token");
	String access_token = getAccessToken(refresh_token);
	Map<String,List<NTFN_NotfnDoaMap>> NotfnDoaMap = obj.getDoaMap();
	List<NTFN_NotfnDoaMap> list = NotfnDoaMap.get("google");
	ResultSet rs = obj.getResultSet();
	String key[] = { "From", "To", "Cc","Bcc", "Subject","Body" };
	Map<String, String> elementMap = new HashMap<String, String>();
	for (int i = 0; i < key.length; i++) {
		String value = getData(key[i], list, rs);
		elementMap.put(key[i], value);
	  }
	
	
	StringBuilder url = new StringBuilder(Constants.GoogleTextMailUrl);
	HttpPost post = new HttpPost(url.toString());
	post.addHeader("Authorization","Bearer "+access_token);
	post.addHeader("Content-Type","message/rfc822");
	post.addHeader("Accept", "application/json");
	StringBuilder body = new StringBuilder();
	body.append("From:").append(elementMap.get("From"));
	body.append("To:").append(elementMap.get("To"));
	body.append("Cc:").append(elementMap.get("Cc"));
	body.append("Bcc:").append(elementMap.get("Bcc"));
	body.append("Subject:").append(elementMap.get("Subject"));
	body.append("Body:").append(elementMap.get("Body"));
	
	//StringEntity entity = new StringEntity("From:shashidharpatil1234@gmail.com\rSubject:just to say HI..\rTo:vinayakmundas@gmail.com\rhi shashi how are you","UTF-8");
	StringEntity entity = new StringEntity(body.toString(),"UTF-8");
	entity.setContentType("application/json");
	post.setEntity(entity);
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(post);
    int status_code = response.getStatusLine().getStatusCode();
    boolean flag = FacebookNotification.responseCheck(status_code);
    if(flag){
    	obj.setSuccess(true);
    }else{
    	obj.setError(response);
    }
    
}
//This function will fetch the value for the parameter
	public static String getData(String Key , List<NTFN_NotfnDoaMap> doaMapList, ResultSet resultset) throws SQLException{
		
		String data = "";
		TreeMap<Integer,String> map = new TreeMap<Integer,String>();
		for(NTFN_NotfnDoaMap doaMap: doaMapList){
			if(doaMap.getChannelElementCodeFkId().equals(Key)){
			String value = resultset.getString(doaMap.getMapDoaExpn());
			map.put(doaMap.getSequenceNumberPosInt(), value);
			}
		}
		for (Entry<Integer, String> entry : map.entrySet())
		{
		    data = data+ entry.getValue();
		}
		
		return data;
	}
//This function will send the mail with attachment 
public static void sendMessageWithAttachment() throws ClientProtocolException, IOException{
	
	StringBuilder url = new StringBuilder(Constants.GoogleAttachmentUrl);
	HttpPost post = new HttpPost(url.toString());
	String refreshToken = "";
	String accessToken = GmailTokenUtil.getAccessToken(refreshToken);
	post.addHeader("Authorization","Bearer "+accessToken);
	post.addHeader("Content-Type","message/rfc822");
	List<BasicNameValuePair> body = new ArrayList<BasicNameValuePair>();
	body.add(new BasicNameValuePair("media","ss"));
	StringEntity entity = new StringEntity(body.toString());
	post.setEntity(entity);
	HttpClient client = HttpClientBuilder.create().build();
	HttpResponse response;
	response = client.execute(post);
	
}
//This function will refresh the access_token
public static String getAccessToken(String refreshToken){
	List<BasicNameValuePair> para = new ArrayList<BasicNameValuePair>();
	String CLIENT_ID="912120029717-9od70gv46bqspm5ubfe8gpi8qj2bc53d.apps.googleusercontent.com";
	String CLIENT_SECRET="Hwp1KELw-UdViDzlb3yjbonm";
	para.add(new BasicNameValuePair("client_id",CLIENT_ID));
	para.add(new BasicNameValuePair("client_secret", CLIENT_SECRET));
	para.add(new BasicNameValuePair("refresh_token",refreshToken));
	para.add(new BasicNameValuePair("grant_type","refresh_token"));			      
	HttpPost post = new HttpPost("https://www.googleapis.com/oauth2/v4/token");
	HttpClient client = HttpClientBuilder.create().build();
	HttpResponse response = null;
	JSONObject jsonresponse = new JSONObject();
	String access_token = "";
	try {
		post.setEntity(new UrlEncodedFormEntity(para, HTTP.UTF_8));
		response = client.execute(post);
		int status_code = response.getStatusLine().getStatusCode();
		boolean flag = FacebookNotification.responseCheck(status_code);
		if(flag){
			jsonresponse = (JSONObject)response;
			access_token = jsonresponse.getString("access_token");
		 }
		
	} catch (UnsupportedEncodingException e) {
		e.printStackTrace();
	}
	 catch (ClientProtocolException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	return access_token;		
}

//This function is for sending the mail with html content and the text
public static void sendHtmlMailWithText() throws JSONException, ClientProtocolException, IOException{
	
StringBuilder sb = new StringBuilder("Subject: Example Subject\r\n" +
  "From: <shashidharpatil1234@gmail.com>\r\n" +
  "To: <shashidharmahadevgoudapatil@gmail.com>,<vinayakmundasad@gmail.com>,<santoshkn1992@gmail.com>\r\n" +
  "Content-Type: multipart/mixed; boundary=\"foo_bar\"\r\n\r\n" +

  "--foo_bar\r\n" +
  "Content-Type: text/plain; charset=UTF-8\r\n\r\n" +

  "Hi Shashi How are you...Whats up...How to do the things\r\n\r\n" +

  "--foo_bar\r\n" +
  "Content-Type: text/html; charset=UTF-8\r\n\r\n" +

  "<!DOCTYPE html><html><head><style>body {background-color: powderblue;}h1   {color: blue;}p    {color: red;}</style></head><body><h1>The Hebbuli is roaring..Silence Plzzzz.</h1><p>This is called NAYIIIIIIIIII................</p><img src=http://www.lifelearn-cliented.com/cms/resources/body/6204/barking_in_dogs_1.jpg></body></html>\r\n\r\n" +

  "--foo_bar--" );
	String rawEmail = Base64.encodeBase64URLSafeString(sb.toString().getBytes());
	String access_token = "ya29.GlusAwvli0sTAOERhm-CmEGsd9RkKNO02aVIxZijvg161cTw83VK4i3UUY-4cPH-KTDOToiXRdCS6X-3ztJVCBDu9-gvWDUQsQa5BGVJ9NW3_Gc_AlBOP6r9OxIm";
	HttpPost post = new HttpPost("https://www.googleapis.com/gmail/v1/users/me/messages/send");
	post.addHeader("Content-Type", "application/json");
	post.addHeader("Authorization", "Bearer "+access_token);
	JSONObject jsonbody = new JSONObject();
	jsonbody.put("raw", rawEmail);
	StringEntity entity = new StringEntity(jsonbody.toString());
	post.setEntity(entity);
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(post);
    int status_code = response.getStatusLine().getStatusCode();
	
	
}
//This function will send the text message
public static void sendTextMessage(String subject,String body) throws JSONException, ClientProtocolException, IOException{
	StringBuilder sb = new StringBuilder("Subject: "+subject+"\r\n" +
			  "From: <me>\r\n" +
			  "To: <shashidharpatil1234@gmail.com>\r\n" +
			  "Content-Type: multipart/alternative; boundary=\"foo_bar\"\r\n\r\n" +

			  "--foo_bar\r\n" +
			  "Content-Type: text/plain; charset=UTF-8\r\n\r\n" +

			  ""+body+"\r\n\r\n" +

			  "--foo_bar--" );
	String rawEmail = Base64.encodeBase64URLSafeString(sb.toString().getBytes());
	String access_token = "ya29.GlvkBMZ_e7o9wGYWnr8aEGH9GtFpKK_mJ99F_b4bP6oVuiKdbWWP7ZJhzSLyzuSUdj2k_V24cnwv847_rc4okaml2qWOnlDYk8uMBa6ZIXw3oaMDh2E5YCYn4C-L";
	HttpPost post = new HttpPost("https://www.googleapis.com/gmail/v1/users/me/messages/send");
	post.addHeader("Content-Type", "application/json");
	post.addHeader("Authorization", "Bearer "+access_token);
	JSONObject jsonbody = new JSONObject();
	jsonbody.put("raw", rawEmail);
	StringEntity entity = new StringEntity(jsonbody.toString());
	post.setEntity(entity);
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(post);
    int status_code = response.getStatusLine().getStatusCode();
}

//This function will send the text with the image attachment
public static void sendTextWithAttachment(File file,String contentType,String subject,String bodyText,String fileName) throws IOException, JSONException{
	//String fileName = "/Users/tapplent/Documents/DataUploadSheets/UploadS3Image/CarMaintenanceAllowanceForm.pdf";
	byte[] bytes = Encoder.loadFile(file);
	byte[] encoded = Base64.encodeBase64(bytes);
	String rawAttachment = new String(encoded);
	//String rawImage = Encoder.encodeFileToBase64Binary(fileName);
	
	StringBuilder sb = new StringBuilder("Subject: "+subject+"\r\n" +
			  "From: <me>\r\n" +
			  "To: <shashidharpatil1234@gmail.com>,<shashidharmahadevgoudapatil@gmail.com>\r\n" +
			  "Content-Type: multipart/mixed; boundary=\"foo_bar\"\r\n\r\n" +

			  "--foo_bar\r\n" +
			  "Content-Type: text/plain; charset=UTF-8\r\n\r\n" +

			  ""+bodyText+"\r\n\r\n" +

			  "--foo_bar\r\n" +
			  
			  "Content-Type: "+contentType+"\r\n" +
			  "MIME-Version: 1.0\r\n"+
			  "Content-Transfer-Encoding: base64\r\n"+
			  "Content-Disposition: attachment; filename=\""+fileName+"\"\r\n\r\n"+

			rawAttachment +"\r\n\r\n"+

			  "--foo_bar--" );
	
	String access_token = "ya29.GlvkBC3zF_EN70lgmc_dHGJOb8o1rYWI-VA4xaC2t_ZWS7_5n1K5EqgyBTa3r3FqhSScYPcUsWQK8dgAqoMVs3EG20l0PSqlK6FIlckekq6dTeQVuTMMcTqalmAP";
	HttpPost post = new HttpPost("https://www.googleapis.com/upload/gmail/v1/users/me/messages/send");
	post.addHeader("uploadType", "multipart");
	post.addHeader("Content-Type", "message/rfc822");
	post.addHeader("Authorization", "Bearer "+access_token);
//	JSONObject jsonbody = new JSONObject();
//	jsonbody.put("raw", rawEmail);
	StringEntity entity = new StringEntity(sb.toString());
	post.setEntity(entity);
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(post);
    int status_code = response.getStatusLine().getStatusCode();
}
public static void sendHtmlWithImageAttachment() throws IOException, JSONException{
	String fileName = "/Users/tapplent/Documents/DataUploadSheets/UploadS3Image/100014.jpeg";
	String rawImage = Encoder.encodeFileToBase64Binary(fileName);
	
	StringBuilder sb = new StringBuilder("Subject: Example Subject\r\n" +
			  "From: <shashidharpatil1234@gmail.com>\r\n" +
			  "To: <shashidharmahadevgoudapatil@gmail.com>,<shashidhar.patil@tapplent.com>\r\n" +
			  "Content-Type: multipart/mixed; boundary=\"foo_bar\"\r\n\r\n" +

			  "--foo_bar\r\n" +
			  "Content-Type: text/html; charset=UTF-8\r\n\r\n" +

			  "<!DOCTYPE html><html><head><style>body {background-color: powderblue;}h1   {color: blue;}p    {color: red;}</style></head><body><h1>The Hebbuli is roaring..Silence Plzzzz.</h1><p>This is called NAYIIIIIIIIII................</p><img src=http://www.lifelearn-cliented.com/cms/resources/body/6204/barking_in_dogs_1.jpg></body></html>\r\n\r\n" +

			  "--foo_bar\r\n" +
			  
			  "Content-Type: image/png\r\n" +
			  "MIME-Version: 1.0\r\n"+
			  "Content-Transfer-Encoding: base64\r\n"+
			  "Content-Disposition: attachment; filename=\"100014.png\"\r\n\r\n"+
			  
				rawImage +"\r\n\r\n"+

			  "--foo_bar--" );
	
	String access_token = "ya29.GlusA1o1WgeW10JoBfr3ouKRC-ElhypI1DnOXi4YK4r66sufMl13Z_9-JjD-BzIBN2dbslvgVhbSb9UoDSMEEoiOQFGqXLmD5XO-1bZqV7uozt7O4E1Y7N77wm9T";
	HttpPost post = new HttpPost("https://www.googleapis.com/upload/gmail/v1/users/me/messages/send");
	post.addHeader("uploadType", "multipart");
	post.addHeader("Content-Type", "message/rfc822");
	post.addHeader("Authorization", "Bearer "+access_token);
//	JSONObject jsonbody = new JSONObject();
//	jsonbody.put("raw", rawEmail);
	StringEntity entity = new StringEntity(sb.toString());
	post.setEntity(entity);
	HttpClient client = HttpClientBuilder.create().build();
    HttpResponse response;
    response = client.execute(post);
    int status_code = response.getStatusLine().getStatusCode();
}

}

