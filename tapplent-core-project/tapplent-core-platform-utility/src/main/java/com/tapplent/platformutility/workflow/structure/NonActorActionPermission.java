package com.tapplent.platformutility.workflow.structure;

public class NonActorActionPermission {
    private String actionPermissionPkId;
    private String actionPermissionDefnId;
    private String permissionTypeCodeId;
    private String permissionValue;
    private String additionalSubjectUserApplicableGroup;
    private String recordStatePermission;
    private String mtPE;
    private String actionVisualMasterCode;
    private boolean isActionApplicable;

    public String getActionPermissionPkId() {
        return actionPermissionPkId;
    }

    public void setActionPermissionPkId(String actionPermissionPkId) {
        this.actionPermissionPkId = actionPermissionPkId;
    }

    public String getActionPermissionDefnId() {
        return actionPermissionDefnId;
    }

    public void setActionPermissionDefnId(String actionPermissionDefnId) {
        this.actionPermissionDefnId = actionPermissionDefnId;
    }

    public String getPermissionTypeCodeId() {
        return permissionTypeCodeId;
    }

    public void setPermissionTypeCodeId(String permissionTypeCodeId) {
        this.permissionTypeCodeId = permissionTypeCodeId;
    }

    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }

    public String getAdditionalSubjectUserApplicableGroup() {
        return additionalSubjectUserApplicableGroup;
    }

    public void setAdditionalSubjectUserApplicableGroup(String additionalSubjectUserApplicableGroup) {
        this.additionalSubjectUserApplicableGroup = additionalSubjectUserApplicableGroup;
    }

    public String getRecordStatePermission() {
        return recordStatePermission;
    }

    public void setRecordStatePermission(String recordStatePermission) {
        this.recordStatePermission = recordStatePermission;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getActionVisualMasterCode() {
        return actionVisualMasterCode;
    }

    public void setActionVisualMasterCode(String actionVisualMasterCode) {
        this.actionVisualMasterCode = actionVisualMasterCode;
    }

    public boolean isActionApplicable() {
        return isActionApplicable;
    }

    public void setActionApplicable(boolean actionApplicable) {
        isActionApplicable = actionApplicable;
    }
}
