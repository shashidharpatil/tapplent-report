package com.tapplent.platformutility.etl.valueObject;

/**
 * Created by Tapplent on 7/11/17.
 */
public class EtlAccessControlHelperVO {
    private String mtPE;
    private String doCode;

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getDoCode() {
        return doCode;
    }

    public void setDoCode(String doCode) {
        this.doCode = doCode;
    }
}
