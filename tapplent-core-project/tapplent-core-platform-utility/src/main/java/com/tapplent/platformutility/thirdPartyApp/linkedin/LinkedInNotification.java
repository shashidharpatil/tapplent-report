package com.tapplent.platformutility.thirdPartyApp.linkedin;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.POST_PostData;
import com.tapplent.platformutility.batch.model.POST_SocialDoaMap;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.tapplent.platformutility.thirdPartyApp.facebook.FacebookNotification;

public class LinkedInNotification {

	
	
	//This function will retrieve the user's complete profile
	public void getUserCompleteProfile(POST_PostData obj) throws JSONException {

		JSONObject object = (JSONObject) obj.getPersonCredential();
		String access_token = object.getString("access_token");
		StringBuffer Url = new StringBuffer(Constants.linkedInProfileUrl);
		Url.append("?oauth2_access_token=").append(access_token).append("&format=json");
try {  
		HttpGet get = new HttpGet(Url.toString());
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response;
		response = client.execute(get);
		
	} catch (ClientProtocolException e2) {
			e2.printStackTrace();
	} catch (IOException e2) {
			e2.printStackTrace();
	} 
		return;
	}
	// This function will post on the user wall
	public  void postOnUserWall(POST_PostData  obj){
		
		
try {	
		JSONObject object = (JSONObject) obj.getPersonCredential();
		String access_token = object.getString("access_token");
		List<POST_SocialDoaMap> list = obj.getDoaMap();
		ResultSet rs = obj.getResultSet();
		String Key = "submitted-image-url";
		String image_url = FacebookNotification.getData(Key,list,rs);
		if(!image_url.equals("")){
			
			String key[] = { "comment", "code", "title", "submitted-url", "submitted-image-url", "description" };
			Map<String, String> elementMap = new HashMap<String, String>();
			for (int i = 0; i < key.length; i++) {
				String value = FacebookNotification.getData(key[i], list, rs);
				elementMap.put(key[i], value);
			}
			StringBuffer URL = new StringBuffer(Constants.linkedInPostUrl);
			URL.append("?oauth2_access_token=").append(access_token).append("&format=json");
			HttpPost post = new HttpPost(URL.toString());
			post.addHeader("Content-Type", "application/json");
			post.addHeader("x-li-format", "json");
			JSONObject object1 = new JSONObject();
			JSONObject obj1 = new JSONObject();
			JSONObject obj2 = new JSONObject();
			if(!elementMap.get("comment").equals("")){
				object1.put("comment", elementMap.get("comment"));
				if(!elementMap.get("code").equals("")){
					obj1.put("code", elementMap.get("code"));
				}
				if(!elementMap.get("title").equals("")){
					obj2.put("title",elementMap.get("title"));
				}
				if(!elementMap.get("submitted-url").equals("")){
					obj2.put("submitted-url",elementMap.get("submitted-url"));
				}
				if(!elementMap.get("submitted-image-url").equals("")){
					obj2.put("submitted-image-url",elementMap.get("submitted-image-url"));
				}
				if(!elementMap.get("description").equals("")){
					obj2.put("description",elementMap.get("description"));
				}
				object1.put("content",obj2);
				object1.put("visibility", obj1);
				StringEntity entity = new StringEntity(object1.toString());
				post.setEntity(entity);
				HttpClient client = HttpClientBuilder.create().build();
				HttpResponse response;
				response = client.execute(post);
				int status_code = response.getStatusLine().getStatusCode();
				boolean flag = FacebookNotification.responseCheck(status_code);
				if(flag){
					obj.setSuccess(flag);
				} else{
					obj.setError(response);
				}
				
			}else {
				  //Throw an error,Invalid call to Post
			  }
				
		}else{
			StringBuffer URL = new StringBuffer(Constants.linkedInPostUrl);
			URL.append("?oauth2_access_token=").append(access_token).append("&format=json");
			HttpPost post = new HttpPost(URL.toString());
			post.addHeader("Content-Type", "application/json");
			post.addHeader("x-li-format", "json");
			JSONObject mainObj = new JSONObject();
			JSONObject obj1 = new JSONObject();
			Key = "comment";
			String comment = FacebookNotification.getData(Key,list,rs);
			Key = "code";
			String code = FacebookNotification.getData(Key,list,rs);
			if(!comment.equals("")){
				mainObj.put("comment", comment);
			}
			if(!code.equals("")){
				obj1.put("code", code);
			}
			mainObj.put("visibility", obj1);
			StringEntity entity = new StringEntity(obj.toString());
			post.setEntity(entity);
			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response;
			response = client.execute(post);
			int status_code = response.getStatusLine().getStatusCode();
			boolean flag = FacebookNotification.responseCheck(status_code);
			if(flag){
				obj.setSuccess(flag);
			} else{
				obj.setError(response);
			}
			
		}
		
		
		} catch (ClientProtocolException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}  catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return;
		
	}
	
	//This function will post on the company page
	public void postOnCompanyPage(POST_PostData obj ){
        
		
try {	
		JSONObject object = (JSONObject) obj.getPersonCredential();
		String access_token = object.getString("access_token");
		String page_id = "";
		List<POST_SocialDoaMap> list = obj.getDoaMap();
		ResultSet rs = obj.getResultSet();
		String Key = "submitted-image-url";
		String image_url = FacebookNotification.getData(Key,list,rs);
		if(!image_url.equals("")){
			
			String key[] = { "comment", "code", "title", "submitted-url", "submitted-image-url", "description" };
			Map<String, String> elementMap = new HashMap<String, String>();
			for (int i = 0; i < key.length; i++) {
				String value = FacebookNotification.getData(key[i], list, rs);
				elementMap.put(key[i], value);
			}
			StringBuffer URL = new StringBuffer(Constants.linkedInCompanyPostUrl);
			URL.append(page_id).append("/shares");
			URL.append("?oauth2_access_token=").append(access_token).append("&format=json");
			HttpPost post1 = new HttpPost(URL.toString());
			post1.addHeader("Content-Type", "application/json");
			post1.addHeader("x-li-format", "json");
			JSONObject object1 = new JSONObject();
			JSONObject obj1 = new JSONObject();
			JSONObject obj2 = new JSONObject();
			if(!elementMap.get("comment").equals("")){
				object1.put("comment", elementMap.get("comment"));
				if(!elementMap.get("code").equals("")){
					obj1.put("code", elementMap.get("code"));
				}
				if(!elementMap.get("title").equals("")){
					obj2.put("title",elementMap.get("title"));
				}
				if(!elementMap.get("submitted-url").equals("")){
					obj2.put("submitted-url",elementMap.get("submitted-url"));
				}
				if(!elementMap.get("submitted-image-url").equals("")){
					obj2.put("submitted-image-url",elementMap.get("submitted-image-url"));
				}
				if(!elementMap.get("description").equals("")){
					obj2.put("description",elementMap.get("description"));
				}
				object1.put("content",obj2);
				object1.put("visibility", obj1);
				StringEntity entity = new StringEntity(object1.toString());
				post1.setEntity(entity);
				HttpClient client = HttpClientBuilder.create().build();
				HttpResponse response;
				response = client.execute(post1);
				int status_code = response.getStatusLine().getStatusCode();
				boolean flag = FacebookNotification.responseCheck(status_code);
				if(flag){
					obj.setSuccess(flag);
				} else{
					obj.setError(response);
				}
				
			  }else {
				  //Throw an error{Invalid Call}
			  }
				
		}else{
			StringBuffer URL = new StringBuffer(Constants.linkedInCompanyPostUrl);
			URL.append(page_id).append("/shares");
			URL.append("?oauth2_access_token=").append(access_token).append("&format=json");
			HttpPost post = new HttpPost(URL.toString());
			post.addHeader("Content-Type", "application/json");
			post.addHeader("x-li-format", "json");
			JSONObject mainObj = new JSONObject();
			JSONObject obj1 = new JSONObject();
			Key = "comment";
			String comment = FacebookNotification.getData(Key,list,rs);
			Key = "code";
			String code = FacebookNotification.getData(Key,list,rs);
			if(!comment.equals("")){
				mainObj.put("comment", comment);
			}
			if(!code.equals("")){
				obj1.put("code", code);
			}
			mainObj.put("visibility", obj1);
			StringEntity entity = new StringEntity(obj.toString());
			post.setEntity(entity);
			HttpClient client = HttpClientBuilder.create().build();
			HttpResponse response;
			response = client.execute(post);
			int status_code = response.getStatusLine().getStatusCode();
			boolean flag = FacebookNotification.responseCheck(status_code);
			if(flag){
				obj.setSuccess(flag);
			} else{
				obj.setError(response);
			}
			
		}
		
		
		} catch (ClientProtocolException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}  catch (JSONException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return;
	}
	
		
//	public boolean Login(String user, String pass, String url) {
//        try {
//            final WebClient webClient = new WebClient();
//
//            // Get the first page
//            final HtmlPage page1 = webClient.getPage(url);
//
//            // Get the form that we are dealing with and within that form,
//            // find the submit button and the field that we want to change.
//            final HtmlForm form = page1.getFormByName("oauthAuthorizeForm");
//
//            final HtmlSubmitInput button = form.getInputByName("authorize");
//            final HtmlTextInput textField = form.getInputByName("session_login");
//            final HtmlPasswordInput textField2 = form.getInputByName("session_password");
//            // Change the value of the text field
//            textField.setValueAttribute(user);
//            textField2.setValueAttribute(pass);
//
//            // Now submit the form by clicking the button and get back the second page.
//
//            final HtmlPage page2 = button.click();
//            String newurl = page2.getUrl().toString();
//            String oauthVerifier = newurl.split("oauth_verifier=")[1].split("&")[0];
//            accessToken = oauthService.getOAuthAccessToken(requestToken, oauthVerifier);
//
//            webClient.closeAllWindows();
//            logined = true;
//            return true;
//
//        } catch (Exception ex) {
//            Logger.getLogger(ClassLinkedIn.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return false;
//    }
}