package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.List;
import com.evernote.auth.EvernoteAuth;
import com.evernote.auth.EvernoteService;
import com.evernote.clients.ClientFactory;
import com.evernote.clients.NoteStoreClient;
import com.evernote.clients.UserStoreClient;
import com.evernote.edam.error.EDAMNotFoundException;
import com.evernote.edam.error.EDAMSystemException;
import com.evernote.edam.error.EDAMUserException;
import com.evernote.edam.notestore.NoteFilter;
import com.evernote.edam.notestore.NoteList;
import com.evernote.edam.type.Data;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.NoteSortOrder;
import com.evernote.edam.type.Notebook;
import com.evernote.edam.type.Resource;
import com.evernote.edam.type.ResourceAttributes;
import com.evernote.thrift.TException;
import com.evernote.thrift.transport.TTransportException;
public class EverNote {
	 private static UserStoreClient userStore;
	  private static NoteStoreClient noteStore;
	  private static Notebook notebook;
	  private static String newNoteGuid;
	  private static final String token = "S=s613:U=97a13a1:E=1604994fd1a:C=158f1e3d0c8:P=1cd:A=en-devtoken:V=2:H=e2b8f81cb48a8af5cfffdc19c4320563";


	  public static void Init(String token) throws Exception {
		    // Set up the UserStore client and check that we can speak to the server
		    EvernoteAuth evernoteAuth = new EvernoteAuth(EvernoteService.PRODUCTION, token);
		    ClientFactory factory = new ClientFactory(evernoteAuth);
		    userStore = factory.createUserStoreClient();

		    boolean versionOk = userStore.checkVersion("Evernote EDAMDemo (Java)",
		        com.evernote.edam.userstore.Constants.EDAM_VERSION_MAJOR,
		        com.evernote.edam.userstore.Constants.EDAM_VERSION_MINOR);
		    if (!versionOk) {
		      System.err.println("Incompatible Evernote client protocol version");
		      System.exit(1);
		    }

		    // Set up the NoteStore client
		    noteStore = factory.createNoteStoreClient();
		  }
	//This function will
	public static List<Notebook> ListNotes() throws Exception {
		Init(token);
		List<Notebook> notebooks = null;
	    // List the notes in the user's account
	    System.out.println("Listing notes:");

	    // First, get a list of all notebooks
	    notebooks = noteStore.listNotebooks();

	    for (Notebook notebook : notebooks) {
	      System.out.println("Notebook: " + notebook.getName());

	      // Next, search for the first 100 notes in this notebook, ordering
	      // by creation date
	      NoteFilter filter = new NoteFilter();
	      filter.setNotebookGuid(notebook.getGuid());
	      filter.setOrder(NoteSortOrder.CREATED.getValue());
	      filter.setAscending(true);

	      NoteList noteList = noteStore.findNotes(filter, 0, 100);
	      List<Note> notes = noteList.getNotes();
	      for (Note note : notes) {
	        System.out.println(" * " + note.getTitle());
	      }
	    }
	return notebooks;
	}
	//this function will create note with simple text in the default notebook
	public static void createNotes() throws Exception {
		Init(token);
		String nBody = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		  nBody += "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">";
		  nBody += "<en-note>" + "Hi How are You";
		  nBody += "<en-media type=\"image/jpg\" hash=\"CF0E487670EC5145B647BAFD386EE2E8\"/>";
		  nBody += "</en-note>";
		  MessageDigest md = MessageDigest.getInstance("MD5");
		  try (InputStream is = Files.newInputStream(Paths.get("/Users/tapplent/Desktop/accessmanagementbuyersguide-2539000.pdf"));
		       DigestInputStream dis = new DigestInputStream(is, md))
		  {
		    /* Read decorated stream (dis) to EOF as normal... */
		  }
		  byte[] digest = md.digest();
		  StringBuffer sb = new StringBuffer("");
		    for (int i = 0; i < digest.length; i++) {
		    	sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
		    }
		    String md5String = sb.toString();


		  Notebook parentNotebook = new Notebook();
//		  if(StringUtil.isDefined(ever.getNotebookGuid())){
//		  parentNotebook.setGuid(ever.getNotebookGuid());
//		  }
		  // Create note object
		  Note ourNote = new Note();
		  ourNote.setTitle("Tapplent's firstNew everNote");
		  ourNote.setContent(nBody);
		  // parentNotebook is optional; if omitted, default notebook is used
		  if (parentNotebook != null && parentNotebook.isSetGuid()) {
		    ourNote.setNotebookGuid(parentNotebook.getGuid());
		  }
		  //ourNote.setNotebookGuid(parentNotebook.getGuid());

		  // Attempt to create note in Evernote account
		  Note createdNote = null;
		  createdNote = noteStore.createNote(ourNote);
		    newNoteGuid = createdNote.getGuid();
		    return;

	}
	//This function will create note with attachment in the default notebook
	public static void createNoteWithAttachment() throws Exception{
		Init(token);
		Note note = new Note();
		    note.setTitle("Test Note of Tapplent");

		    String fileName = "logo_tapplent-colour.png";
		    String mimeType = "image/png";

		    // To include an attachment such as an image in a note, first create a
		    // Resource
		    // for the attachment. At a minimum, the Resource contains the binary
		    // attachment
		    // data, an MD5 hash of the binary data, and the attachment MIME type.
		    // It can also
		    // include attributes such as filename and location.
		    Resource resource = new Resource();
		    resource.setData(readFileAsData(fileName));
		    resource.setMime(mimeType);
		    ResourceAttributes attributes = new ResourceAttributes();
		    attributes.setFileName(fileName);
		    resource.setAttributes(attributes);

		    // Now, add the new Resource to the note's list of resources
		    note.addToResources(resource);

		    // To display the Resource as part of the note's content, include an
		    // <en-media>
		    // tag in the note's ENML content. The en-media tag identifies the
		    // corresponding
		    // Resource using the MD5 hash.
		    String hashHex = bytesToHex(resource.getData().getBodyHash());

		    // The content of an Evernote note is represented using Evernote Markup
		    // Language
		    // (ENML). The full ENML specification can be found in the Evernote API
		    // Overview
		    // at http://dev.evernote.com/documentation/cloud/chapters/ENML.php
		    String content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
		        + "<!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\">"
		        + "<en-note>"
		        + "<span style=\"color:green;\">Here's the Tapplent's New logo:</span><br/>"
		        + "<en-media type=\"image/png\" hash=\"" + hashHex + "\"/>"
		        + "</en-note>";
		    note.setContent(content);

		    // Finally, send the new note to Evernote using the createNote method
		    // The new Note object that is returned will contain server-generated
		    // attributes such as the new note's unique GUID.
		    Note createdNote = noteStore.createNote(note);
		    newNoteGuid = createdNote.getGuid();

		    System.out.println("Successfully created a new note with GUID: "
		        + newNoteGuid);
		    System.out.println();
	}
	 public static Data readFileAsData(String fileName) throws Exception {
		    String filePath = "/Users/tapplent/Desktop/Camera/logo_tapplent-colour.png";
		    // Read the full binary contents of the file
		    FileInputStream in = new FileInputStream(filePath);
		    ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		    byte[] block = new byte[10240];
		    int len;
		    while ((len = in.read(block)) >= 0) {
		      byteOut.write(block, 0, len);
		    }
		    in.close();
		    byte[] body = byteOut.toByteArray();

		    // Create a new Data object to contain the file contents
		    Data data = new Data();
		    data.setSize(body.length);
		    data.setBodyHash(MessageDigest.getInstance("MD5").digest(body));
		    data.setBody(body);

		    return data;
		  }
	 public static String bytesToHex(byte[] bytes) {
		    StringBuilder sb = new StringBuilder();
		    for (byte hashByte : bytes) {
		      int intVal = 0xff & hashByte;
		      if (intVal < 0x10) {
		        sb.append('0');
		      }
		      sb.append(Integer.toHexString(intVal));
		    }
		    return sb.toString();
		  }
	public static void CreateNoteBook() throws Exception {
		Init(token);
		notebook = new Notebook();
	      notebook.setName("Tapplent");
	      //notebook.setDefaultNotebook(true);
	      Notebook createdNotebook = noteStore.createNotebook(notebook);
		return;
	}

		public static Note UpdateNotes( String ever) throws Exception {
		// When updating a note, it is only necessary to send Evernote the
	    // fields that have changed. For example, if the Note that you
	    // send via updateNote does not have the resources field set, the
	    // Evernote server will not change the note's existing resources.
	    // If you wanted to remove all resources from a note, you would
	    // set the resources field to a new List<Resource> that is empty.

	    // If you are only changing attributes such as the note's title or tags,
	    // you can save time and bandwidth by omitting the note content and
	    // resources.

	    // In this sample code, we fetch the note that we created earlier,
	    // including
	    // the full note content and all resources. A real application might
	    // do something with the note, then update a note attribute such as a
	    // tag.
		Init(token);
	    Note note = noteStore.getNote("ever.getNoteGuid()", true, true, false, false);

	    // Do something with the note contents or resources...

	    // Now, update the note. Because we're not changing them, we unset
	    // the content and resources. All we want to change is the tags.
	    note.unsetContent();
	    note.unsetResources();

	    // We want to apply the tag "TestTag"
	    //note.addToTagNames("TestTag");
	    note.setTitle("");

	    // Now update the note. Because we haven't set the content or resources,
	    // they won't be changed.
	    noteStore.updateNote(note);
	    System.out.println("Successfully added tag to existing note");

	    // To prove that we didn't destroy the note, let's fetch it again and
	    // verify that it still has 1 resource.
	    note = noteStore.getNote("ever.getNoteGuid()", false, false, false, false);
	    System.out.println("After update, note has " + note.getResourcesSize()
	        + " resource(s)");
	    /*System.out.println("After update, note tags are: ");
	    for (String tagGuid : note.getTagGuids()) {
	      Tag tag = noteStore.getTag(tagGuid);
	      System.out.println("* " + tag.getName());
	    }*/

	    System.out.println();
		return note;

	}
}