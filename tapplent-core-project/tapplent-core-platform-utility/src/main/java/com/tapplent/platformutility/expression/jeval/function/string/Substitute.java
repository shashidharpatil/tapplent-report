package com.tapplent.platformutility.expression.jeval.function.string;

import com.tapplent.platformutility.expression.jeval.EvaluationConstants;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.expression.jeval.function.Function;
import com.tapplent.platformutility.expression.jeval.function.FunctionConstants;
import com.tapplent.platformutility.expression.jeval.function.FunctionException;
import com.tapplent.platformutility.expression.jeval.function.FunctionHelper;
import com.tapplent.platformutility.expression.jeval.function.FunctionResult;

import java.util.ArrayList;

/**
 * Created by sripad on 13/01/16.
 */
public class Substitute implements Function {

    /**
     * Returns the name of the function - "substitute".
     *
     * @return The name of this function class.
     */
    public String getName() {
        return "substitute";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     *
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A string argument that will be converted into one string and
     *            two string argument. The first argument is the source
     *            string to replace the string in. The second argument is the
     *            old string to replace in the source string. The third
     *            argument is the new string to replace the old string
     *            with in the source string. The string and string
     *            argument(s) HAS to be enclosed in quotes. White space that is
     *            not enclosed within quotes will be trimmed. Quote characters
     *            in the first and last positions of any string argument (after
     *            being trimmed) will be removed also. The quote characters used
     *            must be the same as the quote characters used by the current
     *            instance of Evaluator. If there are multiple arguments, they
     *            must be separated by a comma (",").
     *
     * @return Returns a string with every occurence of the old character
     *         replaced with the new character.
     *
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    public FunctionResult execute(final Evaluator evaluator, final String arguments)
            throws FunctionException {
        String result = null;
        String exceptionMessage = "One string argument and two string "
                + "arguments are required.";

        ArrayList values = FunctionHelper.getStrings(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

        if (values.size() != 3) {
            throw new FunctionException(exceptionMessage);
        }

        try {
            String argumentOne = FunctionHelper.trimAndRemoveQuoteChars(
                    (String) values.get(0), evaluator.getQuoteCharacter());

            String argumentTwo = FunctionHelper.trimAndRemoveQuoteChars(
                    (String) values.get(1), evaluator.getQuoteCharacter());

            String argumentThree = FunctionHelper.trimAndRemoveQuoteChars(
                    (String) values.get(2), evaluator.getQuoteCharacter());


            result = argumentOne.replace(argumentTwo, argumentThree);
        } catch (FunctionException fe) {
            throw new FunctionException(fe.getMessage(), fe);
        } catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result,
                FunctionConstants.FUNCTION_RESULT_TYPE_STRING);
    }
}
