package com.tapplent.platformutility.accessmanager.structure;

import java.util.Date;

/**
 * Created by tapplent on 04/11/16.
 */
public class AMColCondition {
    private String amColPkId;
    private String amRowCondnFkId;
    private String whatDoaFkId;
    private Date startDate;
    private Date endDate;
    private boolean isPermissionEnds;
    private boolean isCreateCurrentPermitted;
    private boolean isCreateHistoryPermitted;
    private boolean isCreateFuturePermitted;
    private boolean isReadCurrentPermitted;
    private boolean isReadHistoryPermitted;
    private boolean isReadFuturePermitted;
    private boolean isUpdateCurrentPermitted;
    private boolean isUpdateHistoryPermitted;
    private boolean isUpdateFuturePermitted;

    public String getAmColPkId() {
        return amColPkId;
    }

    public void setAmColPkId(String amColPkId) {
        this.amColPkId = amColPkId;
    }

    public String getAmRowCondnFkId() {
        return amRowCondnFkId;
    }

    public void setAmRowCondnFkId(String amRowCondnFkId) {
        this.amRowCondnFkId = amRowCondnFkId;
    }

    public String getWhatDoaFkId() {
        return whatDoaFkId;
    }

    public void setWhatDoaFkId(String whatDoaFkId) {
        this.whatDoaFkId = whatDoaFkId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isPermissionEnds() {
        return isPermissionEnds;
    }

    public void setPermissionEnds(boolean permissionEnds) {
        isPermissionEnds = permissionEnds;
    }

    public boolean isCreateCurrentPermitted() {
        return isCreateCurrentPermitted;
    }

    public void setCreateCurrentPermitted(boolean createCurrentPermitted) {
        isCreateCurrentPermitted = createCurrentPermitted;
    }

    public boolean isCreateHistoryPermitted() {
        return isCreateHistoryPermitted;
    }

    public void setCreateHistoryPermitted(boolean createHistoryPermitted) {
        isCreateHistoryPermitted = createHistoryPermitted;
    }

    public boolean isCreateFuturePermitted() {
        return isCreateFuturePermitted;
    }

    public void setCreateFuturePermitted(boolean createFuturePermitted) {
        isCreateFuturePermitted = createFuturePermitted;
    }

    public boolean isReadCurrentPermitted() {
        return isReadCurrentPermitted;
    }

    public void setReadCurrentPermitted(boolean readCurrentPermitted) {
        isReadCurrentPermitted = readCurrentPermitted;
    }

    public boolean isReadHistoryPermitted() {
        return isReadHistoryPermitted;
    }

    public void setReadHistoryPermitted(boolean readHistoryPermitted) {
        isReadHistoryPermitted = readHistoryPermitted;
    }

    public boolean isReadFuturePermitted() {
        return isReadFuturePermitted;
    }

    public void setReadFuturePermitted(boolean readFuturePermitted) {
        isReadFuturePermitted = readFuturePermitted;
    }

    public boolean isUpdateCurrentPermitted() {
        return isUpdateCurrentPermitted;
    }

    public void setUpdateCurrentPermitted(boolean updateCurrentPermitted) {
        isUpdateCurrentPermitted = updateCurrentPermitted;
    }

    public boolean isUpdateHistoryPermitted() {
        return isUpdateHistoryPermitted;
    }

    public void setUpdateHistoryPermitted(boolean updateHistoryPermitted) {
        isUpdateHistoryPermitted = updateHistoryPermitted;
    }

    public boolean isUpdateFuturePermitted() {
        return isUpdateFuturePermitted;
    }

    public void setUpdateFuturePermitted(boolean updateFuturePermitted) {
        isUpdateFuturePermitted = updateFuturePermitted;
    }

}
