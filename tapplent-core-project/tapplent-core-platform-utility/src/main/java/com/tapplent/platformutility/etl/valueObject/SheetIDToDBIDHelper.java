package com.tapplent.platformutility.etl.valueObject;

/**
 * Created by tapplent on 04/10/17.
 */
public class SheetIDToDBIDHelper {
    private String sheetForeignKeyName;
    private String sheetID;
    private String pkDataType;
    private String databasePrimaryKeyID;

    public String getSheetForeignKeyName() {
        return sheetForeignKeyName;
    }

    public void setSheetForeignKeyName(String sheetForeignKeyName) {
        this.sheetForeignKeyName = sheetForeignKeyName;
    }

    public String getSheetID() {
        return sheetID;
    }

    public void setSheetID(String sheetID) {
        this.sheetID = sheetID;
    }

    public String getPkDataType() {
        return pkDataType;
    }

    public void setPkDataType(String pkDataType) {
        this.pkDataType = pkDataType;
    }

    public String getDatabasePrimaryKeyID() {
        return databasePrimaryKeyID;
    }

    public void setDatabasePrimaryKeyID(String databasePrimaryKeyID) {
        this.databasePrimaryKeyID = databasePrimaryKeyID;
    }
}
