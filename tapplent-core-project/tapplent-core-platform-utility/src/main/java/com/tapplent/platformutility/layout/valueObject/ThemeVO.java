package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 16/11/16.
 */
public class ThemeVO {
    private String themeId;
    private String themeHeaderFkId;
    private int themeGroupSeqNumPosInt;
    private String themeStateCodeFkId;
    private String backgroundColour1Txt;
    private String backgroundColour2Txt;
    private String backgroundColour3Txt;
    private String backgroundColour4Txt;
    private String backgroundColour5Txt;
    private float backgroundColourGradientWidth1PosDec;
    private float backgroundColourGradientWidth2PosDec;
    private float backgroundColourGradientWidth3PosDec;
    private float backgroundColourGradientWidth4PosDec;
    private float backgroundColourGradientWidth5PosDec;
    private String backgroundImageId;
    private float backgroundImageGradientAnglePosDec;
    private String fontFamilyCodeFkId;
    private String fontColourPrimaryTxt;
    private String fontColourSecondaryTxt;
    private String fontColourTertiaryTxt;

    public String getThemeId() {
        return themeId;
    }

    public void setThemeId(String themeId) {
        this.themeId = themeId;
    }

    public String getThemeHeaderFkId() {
        return themeHeaderFkId;
    }

    public void setThemeHeaderFkId(String themeHeaderFkId) {
        this.themeHeaderFkId = themeHeaderFkId;
    }

    public int getThemeGroupSeqNumPosInt() {
        return themeGroupSeqNumPosInt;
    }

    public void setThemeGroupSeqNumPosInt(int themeGroupSeqNumPosInt) {
        this.themeGroupSeqNumPosInt = themeGroupSeqNumPosInt;
    }

    public String getThemeStateCodeFkId() {
        return themeStateCodeFkId;
    }

    public void setThemeStateCodeFkId(String themeStateCodeFkId) {
        this.themeStateCodeFkId = themeStateCodeFkId;
    }

    public String getBackgroundColour1Txt() {
        return backgroundColour1Txt;
    }

    public void setBackgroundColour1Txt(String backgroundColour1Txt) {
        this.backgroundColour1Txt = backgroundColour1Txt;
    }

    public String getBackgroundColour2Txt() {
        return backgroundColour2Txt;
    }

    public void setBackgroundColour2Txt(String backgroundColour2Txt) {
        this.backgroundColour2Txt = backgroundColour2Txt;
    }

    public String getBackgroundColour3Txt() {
        return backgroundColour3Txt;
    }

    public void setBackgroundColour3Txt(String backgroundColour3Txt) {
        this.backgroundColour3Txt = backgroundColour3Txt;
    }

    public String getBackgroundColour4Txt() {
        return backgroundColour4Txt;
    }

    public void setBackgroundColour4Txt(String backgroundColour4Txt) {
        this.backgroundColour4Txt = backgroundColour4Txt;
    }

    public String getBackgroundColour5Txt() {
        return backgroundColour5Txt;
    }

    public void setBackgroundColour5Txt(String backgroundColour5Txt) {
        this.backgroundColour5Txt = backgroundColour5Txt;
    }

    public float getBackgroundColourGradientWidth1PosDec() {
        return backgroundColourGradientWidth1PosDec;
    }

    public void setBackgroundColourGradientWidth1PosDec(float backgroundColourGradientWidth1PosDec) {
        this.backgroundColourGradientWidth1PosDec = backgroundColourGradientWidth1PosDec;
    }

    public float getBackgroundColourGradientWidth2PosDec() {
        return backgroundColourGradientWidth2PosDec;
    }

    public void setBackgroundColourGradientWidth2PosDec(float backgroundColourGradientWidth2PosDec) {
        this.backgroundColourGradientWidth2PosDec = backgroundColourGradientWidth2PosDec;
    }

    public float getBackgroundColourGradientWidth3PosDec() {
        return backgroundColourGradientWidth3PosDec;
    }

    public void setBackgroundColourGradientWidth3PosDec(float backgroundColourGradientWidth3PosDec) {
        this.backgroundColourGradientWidth3PosDec = backgroundColourGradientWidth3PosDec;
    }

    public float getBackgroundColourGradientWidth4PosDec() {
        return backgroundColourGradientWidth4PosDec;
    }

    public void setBackgroundColourGradientWidth4PosDec(float backgroundColourGradientWidth4PosDec) {
        this.backgroundColourGradientWidth4PosDec = backgroundColourGradientWidth4PosDec;
    }

    public float getBackgroundColourGradientWidth5PosDec() {
        return backgroundColourGradientWidth5PosDec;
    }

    public void setBackgroundColourGradientWidth5PosDec(float backgroundColourGradientWidth5PosDec) {
        this.backgroundColourGradientWidth5PosDec = backgroundColourGradientWidth5PosDec;
    }

    public String getBackgroundImageId() {
        return backgroundImageId;
    }

    public void setBackgroundImageId(String backgroundImageId) {
        this.backgroundImageId = backgroundImageId;
    }

    public float getBackgroundImageGradientAnglePosDec() {
        return backgroundImageGradientAnglePosDec;
    }

    public void setBackgroundImageGradientAnglePosDec(float backgroundImageGradientAnglePosDec) {
        this.backgroundImageGradientAnglePosDec = backgroundImageGradientAnglePosDec;
    }

    public String getFontFamilyCodeFkId() {
        return fontFamilyCodeFkId;
    }

    public void setFontFamilyCodeFkId(String fontFamilyCodeFkId) {
        this.fontFamilyCodeFkId = fontFamilyCodeFkId;
    }

    public String getFontColourPrimaryTxt() {
        return fontColourPrimaryTxt;
    }

    public void setFontColourPrimaryTxt(String fontColourPrimaryTxt) {
        this.fontColourPrimaryTxt = fontColourPrimaryTxt;
    }

    public String getFontColourSecondaryTxt() {
        return fontColourSecondaryTxt;
    }

    public void setFontColourSecondaryTxt(String fontColourSecondaryTxt) {
        this.fontColourSecondaryTxt = fontColourSecondaryTxt;
    }

    public String getFontColourTertiaryTxt() {
        return fontColourTertiaryTxt;
    }

    public void setFontColourTertiaryTxt(String fontColourTertiaryTxt) {
        this.fontColourTertiaryTxt = fontColourTertiaryTxt;
    }
}
