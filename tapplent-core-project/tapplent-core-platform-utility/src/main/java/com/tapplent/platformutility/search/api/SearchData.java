package com.tapplent.platformutility.search.api;

public interface SearchData {
	Integer getPageSize();
	void setPageSize(Integer pageSize);
	Integer getPage();
	void setPage(Integer page);
}
