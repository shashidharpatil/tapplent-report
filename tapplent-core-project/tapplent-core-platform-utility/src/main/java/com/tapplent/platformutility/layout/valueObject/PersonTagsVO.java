package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 28/12/16.
 */
public class PersonTagsVO {
    private String personTagsPkId;
    private String loggedInUserFkId;
    private String tagTxt;
    private String backgroundColourTxt;
    private String fontColourTxt;
    private String lastModifiedDateTime;

    public String getPersonTagsPkId() {
        return personTagsPkId;
    }

    public void setPersonTagsPkId(String personTagsPkId) {
        this.personTagsPkId = personTagsPkId;
    }

    public String getLoggedInUserFkId() {
        return loggedInUserFkId;
    }

    public void setLoggedInUserFkId(String loggedInUserFkId) {
        this.loggedInUserFkId = loggedInUserFkId;
    }

    public String getTagTxt() {
        return tagTxt;
    }

    public void setTagTxt(String tagTxt) {
        this.tagTxt = tagTxt;
    }

    public String getBackgroundColourTxt() {
        return backgroundColourTxt;
    }

    public void setBackgroundColourTxt(String backgroundColourTxt) {
        this.backgroundColourTxt = backgroundColourTxt;
    }

    public String getFontColourTxt() {
        return fontColourTxt;
    }

    public void setFontColourTxt(String fontColourTxt) {
        this.fontColourTxt = fontColourTxt;
    }

    public String getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(String lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}
