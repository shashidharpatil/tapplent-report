package com.tapplent.platformutility.accessmanager.structure;

import java.util.Map;

/**
 * Created by tapplent on 03/11/16.
 */
public class Permission {

    Map<String, DOAPermission> attributePermission;

    public Map<String, DOAPermission> getAttributePermission() {
        return attributePermission;
    }

    public void setAttributePermission(Map<String, DOAPermission> attributePermission) {
        this.attributePermission = attributePermission;
    }
}
