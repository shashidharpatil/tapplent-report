package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.helper.NTFN_NotificationHelper;
import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import com.tapplent.platformutility.thirdPartyApp.gcm.controller.GCMHandler;
import com.tapplent.platformutility.thirdPartyApp.gcm.helper.Notification;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;

import java.util.Map;



public class NotificationItemProcessor implements ItemProcessor<NTFN_NotificationData,NTFN_NotificationData> {

	private String google;
	private String skype;
	private String telegram;
	private String facebook;

	public String getGoogle() {
		return google;
	}

	public void setGoogle(String google) {
		this.google = google;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getTelegram() {
		return telegram;
	}

	public void setTelegram(String telegram) {
		this.telegram = telegram;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	@BeforeStep
	public void beforeStep(JobExecution jobExecution){
		//(TDWI) get job parameters
		jobExecution.getJobParameters();
		
	}
	
	
	public NTFN_NotificationData process(NTFN_NotificationData item) throws Exception {
		// TODO Auto-generated method stub
		NTFN_NotificationHelper.setUpNotfn(item);
		for(int i=0 ; i< item.getNotChannels().size(); i ++){
			if(item.getNotChannels().get(i).getChannelTypeCodeFkId().
					equals(Constants.pushNotification)){
				
				Map<String, Object> mapModel = (Map<String, Object>) NTFN_NotificationHelper.buildPushNotificationModel(item, Constants.google);
				GCMHandler gcmHandler = new GCMHandler();
				if(((Notification)mapModel.get("not")).getNotificationId()!=null){
				gcmHandler.sendGCMusingRegId((Notification)mapModel.get("not"),
						((Notification)mapModel.get("not")).getNotificationId(),
						(String)mapModel.get("apiKey"),item);
				}
				else if(((Notification)mapModel.get("not")).getNotificationKey()!=null){
				gcmHandler.sendGCMusingNotKey((Notification)mapModel.get("not"),
						((Notification)mapModel.get("not")).getNotificationKey(),
						(String)mapModel.get("apiKey"),item);}
				
			}
			
			else if(item.getNotChannels().get(i).getChannelTypeCodeFkId().
					equals(Constants.gmail)){
				if(item.getNtfnBtCodeFkId() != null){
				String url = NTFN_NotificationHelper.fetchURL(item.getNtfnBtCodeFkId(),item.getNtfnMtPeCodeFkId(),
						item.getNtfnVtCodeFkId(),item.getNtfnCanvasTxnFkId(),item.getDependencyKeyExpn());}
				String doc = NTFN_NotificationHelper.fetchAttachmentifexists(item);
				
				//(TDWI) call gmail Notification and pass body url also attachment if exists
				
			}
			

			else if(item.getNotChannels().get(i).getChannelTypeCodeFkId().
					equals(Constants.skype)){
				
				//(TDWI) call skype Notification
				
			}
			

			else if(item.getNotChannels().get(i).getChannelTypeCodeFkId().
					equals(Constants.telegram)){
				
				//(TDWI) call telegram Notification
				
			}
			

			else if(item.getNotChannels().get(i).getChannelTypeCodeFkId().
					equals(Constants.faceBookMessenger)){
				
				//(TDWI) call faceBookMessenger Notification
			}
			
		}
		
		if(item.isSuccess()==true){
		return item;}
		else
			throw new BatchException();
	}
	
	
	

}
