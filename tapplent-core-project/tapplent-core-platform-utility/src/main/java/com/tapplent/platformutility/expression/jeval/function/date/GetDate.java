package com.tapplent.platformutility.expression.jeval.function.date;

import com.tapplent.platformutility.expression.jeval.EvaluationConstants;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.expression.jeval.function.Function;
import com.tapplent.platformutility.expression.jeval.function.FunctionConstants;
import com.tapplent.platformutility.expression.jeval.function.FunctionException;
import com.tapplent.platformutility.expression.jeval.function.FunctionHelper;
import com.tapplent.platformutility.expression.jeval.function.FunctionResult;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by sripad on 14/01/16.
 */
public class GetDate implements Function {

    /**
     * Returns the name of the function - "dateadd".
     *
     * @return The name of this function class.
     */
    public String getName() {
        return "getdate";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     *
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A string argument that will be converted into two string
     *            arguments and one integer argument. The first argument is the
     *            string to test, the second argument is the prefix and the
     *            third argument is the index to start at. The string
     *            argument(s) HAS to be enclosed in quotes. White space that is
     *            not enclosed within quotes will be trimmed. Quote characters
     *            in the first and last positions of any string argument (after
     *            being trimmed) will be removed also. The quote characters used
     *            must be the same as the quote characters used by the current
     *            instance of Evaluator. If there are multiple arguments, they
     *            must be separated by a comma (",").
     *
     * @return Returns "1.0" (true) if the string ends with the suffix,
     *         otherwise it returns "0.0" (false). The return value respresents
     *         a Boolean value that is compatible with the Boolean operators
     *         used by Evaluator.
     *
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    public FunctionResult execute(final Evaluator evaluator, final String arguments)
            throws FunctionException {
        String result = null;
        String exceptionMessage = "three integer arguments "
                + "are required.";

        ArrayList values = FunctionHelper.getStrings(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);
        if (values.size() != 3  ) {

            throw new FunctionException(exceptionMessage);
        }

        try {
            Calendar c = Calendar.getInstance();
            c.set(Integer.valueOf(values.get(2).toString()),Integer.valueOf(values.get(1).toString())-1,
                    Integer.valueOf(values.get(0).toString()), 0, 0);
            result = String.valueOf(c.getTime());

        }  catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result,
                FunctionConstants.FUNCTION_RESULT_TYPE_DATE);
    }
}
