package com.tapplent.platformutility.batch.model;

public class DG_DocGenData {

	private String personDocumentPkId;
	private String personFkId;
	private String mtPeCodeFkId;
	private String doRowPrimaryKeyTxt;
	private String doRowVersionFkId;
	private String dependencyKeyExpn;
	private String srcMtPeCodeFkId;
	private String srcBtCodeFkId;
	private String srcViewTypeCodeFkId;
	private String fileAttach;
	private String fileNameLngTxt;
	private String returnStatusCodeFkId;
	private String docGenDefinitionPkId;
	private String docBtCodeFkId;
	private String docMtPeCodeFkId;
	private String docVtCodeFkId;
	private String docCanvasTxnFkId;
	private String exportPptTemplateFkId;
	private String docTypeCodeFkId;
	private boolean isPasswordEnabled;
	private String notificationDefinitionFkId;
	private String docNameExpn;
	private String docPwdExpn;
	private boolean isSuccess;
	private Object error;
	private String exitStatus;
	private String schedulerInstanceId;


	public String getSchedulerInstanceId() {
		return schedulerInstanceId;
	}

	public void setSchedulerInstanceId(String schedulerInstanceId) {
		this.schedulerInstanceId = schedulerInstanceId;
	}

	public String getExitStatus() {
		return exitStatus;
	}

	public void setExitStatus(String exitStatus) {
		this.exitStatus = exitStatus;
	}

	public Object getError() {
		return error;
	}

	public void setError(Object error) {
		this.error = error;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getPersonDocumentPkId() {
		return personDocumentPkId;
	}

	public void setPersonDocumentPkId(String personDocumentPkId) {
		this.personDocumentPkId = personDocumentPkId;
	}

	public String getPersonFkId() {
		return personFkId;
	}

	public void setPersonFkId(String personFkId) {
		this.personFkId = personFkId;
	}

	public String getMtPeCodeFkId() {
		return mtPeCodeFkId;
	}

	public void setMtPeCodeFkId(String mtPeCodeFkId) {
		this.mtPeCodeFkId = mtPeCodeFkId;
	}

	public String getDoRowPrimaryKeyTxt() {
		return doRowPrimaryKeyTxt;
	}

	public void setDoRowPrimaryKeyTxt(String doRowPrimaryKeyTxt) {
		this.doRowPrimaryKeyTxt = doRowPrimaryKeyTxt;
	}

	public String getDoRowVersionFkId() {
		return doRowVersionFkId;
	}

	public void setDoRowVersionFkId(String doRowVersionFkId) {
		this.doRowVersionFkId = doRowVersionFkId;
	}

	public String getDependencyKeyExpn() {
		return dependencyKeyExpn;
	}

	public void setDependencyKeyExpn(String dependencyKeyExpn) {
		this.dependencyKeyExpn = dependencyKeyExpn;
	}

	public String getSrcMtPeCodeFkId() {
		return srcMtPeCodeFkId;
	}

	public void setSrcMtPeCodeFkId(String srcMtPeCodeFkId) {
		this.srcMtPeCodeFkId = srcMtPeCodeFkId;
	}

	public String getSrcBtCodeFkId() {
		return srcBtCodeFkId;
	}

	public void setSrcBtCodeFkId(String srcBtCodeFkId) {
		this.srcBtCodeFkId = srcBtCodeFkId;
	}

	public String getSrcViewTypeCodeFkId() {
		return srcViewTypeCodeFkId;
	}

	public void setSrcViewTypeCodeFkId(String srcViewTypeCodeFkId) {
		this.srcViewTypeCodeFkId = srcViewTypeCodeFkId;
	}

	public String getFileAttach() {
		return fileAttach;
	}

	public void setFileAttach(String fileAttach) {
		this.fileAttach = fileAttach;
	}

	public String getFileNameLngTxt() {
		return fileNameLngTxt;
	}

	public void setFileNameLngTxt(String fileNameLngTxt) {
		this.fileNameLngTxt = fileNameLngTxt;
	}

	public String getReturnStatusCodeFkId() {
		return returnStatusCodeFkId;
	}

	public void setReturnStatusCodeFkId(String returnStatusCodeFkId) {
		this.returnStatusCodeFkId = returnStatusCodeFkId;
	}

	public String getDocGenDefinitionPkId() {
		return docGenDefinitionPkId;
	}

	public void setDocGenDefinitionPkId(String docGenDefinitionPkId) {
		this.docGenDefinitionPkId = docGenDefinitionPkId;
	}

	public String getDocBtCodeFkId() {
		return docBtCodeFkId;
	}

	public void setDocBtCodeFkId(String docBtCodeFkId) {
		this.docBtCodeFkId = docBtCodeFkId;
	}

	public String getDocMtPeCodeFkId() {
		return docMtPeCodeFkId;
	}

	public void setDocMtPeCodeFkId(String docMtPeCodeFkId) {
		this.docMtPeCodeFkId = docMtPeCodeFkId;
	}

	public String getDocVtCodeFkId() {
		return docVtCodeFkId;
	}

	public void setDocVtCodeFkId(String docVtCodeFkId) {
		this.docVtCodeFkId = docVtCodeFkId;
	}

	public String getDocCanvasTxnFkId() {
		return docCanvasTxnFkId;
	}

	public void setDocCanvasTxnFkId(String docCanvasTxnFkId) {
		this.docCanvasTxnFkId = docCanvasTxnFkId;
	}

	public String getExportPptTemplateFkId() {
		return exportPptTemplateFkId;
	}

	public void setExportPptTemplateFkId(String exportPptTemplateFkId) {
		this.exportPptTemplateFkId = exportPptTemplateFkId;
	}

	public String getDocTypeCodeFkId() {
		return docTypeCodeFkId;
	}

	public void setDocTypeCodeFkId(String docTypeCodeFkId) {
		this.docTypeCodeFkId = docTypeCodeFkId;
	}

	public boolean isPasswordEnabled() {
		return isPasswordEnabled;
	}

	public void setPasswordEnabled(boolean isPasswordEnabled) {
		this.isPasswordEnabled = isPasswordEnabled;
	}

	public String getNotificationDefinitionFkId() {
		return notificationDefinitionFkId;
	}

	public void setNotificationDefinitionFkId(String notificationDefinitionFkId) {
		this.notificationDefinitionFkId = notificationDefinitionFkId;
	}

	public String getDocNameExpn() {
		return docNameExpn;
	}

	public void setDocNameExpn(String docNameExpn) {
		this.docNameExpn = docNameExpn;
	}

	public String getDocPwdExpn() {
		return docPwdExpn;
	}

	public void setDocPwdExpn(String docPwdExpn) {
		this.docPwdExpn = docPwdExpn;
	}

}
