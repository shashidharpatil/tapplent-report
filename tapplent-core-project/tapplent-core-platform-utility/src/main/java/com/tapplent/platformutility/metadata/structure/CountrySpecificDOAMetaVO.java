package com.tapplent.platformutility.metadata.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.layout.valueObject.IconVO;

/**
 * Created by tapplent on 17/01/17.
 */
public class CountrySpecificDOAMetaVO {
    private String btDoaCountrySpecificDisplayOverridePkId;
    private String btDoaCodeFkId;
    private String countryCodeFkId;
    private String doaDisplayNameOverrideG11nBigTxt;
    private String doaDisplayIconCodeFkId;
    private boolean isVisible;
    private boolean isEditable;
    private boolean isSensitive;
    private boolean isDataNull;

    public String getBtDoaCountrySpecificDisplayOverridePkId() {
        return btDoaCountrySpecificDisplayOverridePkId;
    }

    public void setBtDoaCountrySpecificDisplayOverridePkId(String btDoaCountrySpecificDisplayOverridePkId) {
        this.btDoaCountrySpecificDisplayOverridePkId = btDoaCountrySpecificDisplayOverridePkId;
    }

    public String getBtDoaCodeFkId() {
        return btDoaCodeFkId;
    }

    public void setBtDoaCodeFkId(String btDoaCodeFkId) {
        this.btDoaCodeFkId = btDoaCodeFkId;
    }

    public String getCountryCodeFkId() {
        return countryCodeFkId;
    }

    public void setCountryCodeFkId(String countryCodeFkId) {
        this.countryCodeFkId = countryCodeFkId;
    }

    public String getDoaDisplayNameOverrideG11nBigTxt() {
        return doaDisplayNameOverrideG11nBigTxt;
    }

    public void setDoaDisplayNameOverrideG11nBigTxt(String doaDisplayNameOverrideG11nBigTxt) {
        this.doaDisplayNameOverrideG11nBigTxt = doaDisplayNameOverrideG11nBigTxt;
    }

    public String getDoaDisplayIconCodeFkId() {
        return doaDisplayIconCodeFkId;
    }

    public void setDoaDisplayIconCodeFkId(String doaDisplayIconCodeFkId) {
        this.doaDisplayIconCodeFkId = doaDisplayIconCodeFkId;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    public boolean isSensitive() {
        return isSensitive;
    }

    public void setSensitive(boolean sensitive) {
        isSensitive = sensitive;
    }

    public boolean isDataNull() {
        return isDataNull;
    }

    public void setDataNull(boolean dataNull) {
        isDataNull = dataNull;
    }
}
