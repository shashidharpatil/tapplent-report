package com.tapplent.platformutility.search.builder;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class DbSearchFilter {
	private String tableAlias;
	private String columnName;
	private String columnLabel;
	private List<FilterOperatorValue> opVal;
	private String dbDataType;
	public DbSearchFilter(){
		this.opVal = new ArrayList<FilterOperatorValue>();
	}
	public DbSearchFilter(String columnAlias, List<FilterOperatorValue> opVal, String dbDataType) {
		this.columnLabel = columnAlias;
		this.opVal = opVal;
		this.dbDataType = dbDataType;
	}
	public String getDbDataType() {
		return dbDataType;
	}
	public void setDbDataType(String dbDataType) {
		this.dbDataType = dbDataType;
	}
	public String getColumnLabel() {
		return columnLabel;
	}
	public void setColumnLabel(String columnLabel) {
		this.columnLabel = columnLabel;
	}
	public List<FilterOperatorValue> getOpVal() {
		return opVal;
	}
	public void setOpVal(List<FilterOperatorValue> opVal) {
		this.opVal = opVal;
	}
    public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getTableAlias() {
		return tableAlias;
	}
	public void setTableAlias(String tableAlias) {
		this.tableAlias = tableAlias;
	}
	Object getData(String value, String dataType) {
        Object data = null;
        switch (dataType) {
            case "CHAR":
            case "VARCHAR":
                data = value;
                break;
            case "DATETIME":
                data = new Timestamp(Long.parseLong(value));
                break;
            case "BOOLEAN":
                data = Boolean.getBoolean(value);
                break;
            case "INTEGER":
            case "NUMERIC":
                data = Long.valueOf(value);
                break;
            default:
                data = value;
        }
        return data;
    }
}