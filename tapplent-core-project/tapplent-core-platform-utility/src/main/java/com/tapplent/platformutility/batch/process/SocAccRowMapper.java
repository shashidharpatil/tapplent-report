package com.tapplent.platformutility.batch.process;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.SA_SocAccData;
import org.springframework.jdbc.core.RowMapper;


public class SocAccRowMapper implements RowMapper<SA_SocAccData>{

	public SA_SocAccData mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		SA_SocAccData socAcc = new SA_SocAccData();
		
		socAcc.setPersonDepFkId(rs.getString(Constants.personDepFkId));
		socAcc.setPersonSocialAccountPkId(rs.getString(Constants.personSocialAccountPkId));
		socAcc.setSocialAccountCaracteristicsBlob(rs.getObject(Constants.socialAccountCaracteristicsBlob));
		socAcc.setSocialAccountTypeCodeFkId(rs.getString(Constants.socialAccountTypeCodeFkId));
		socAcc.setValid(rs.getBoolean(Constants.isValid));
		socAcc.setDeviceIndependentBlob(rs.getObject(Constants.deviceIndependentBlob));
		socAcc.setAccountHandleLngTxt(rs.getString(Constants.accountHandleLngTxt));
		
		return socAcc;
	}
}
