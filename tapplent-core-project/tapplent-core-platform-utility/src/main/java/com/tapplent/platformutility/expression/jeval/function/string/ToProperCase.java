package com.tapplent.platformutility.expression.jeval.function.string;

import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.expression.jeval.function.Function;
import com.tapplent.platformutility.expression.jeval.function.FunctionConstants;
import com.tapplent.platformutility.expression.jeval.function.FunctionException;
import com.tapplent.platformutility.expression.jeval.function.FunctionHelper;
import com.tapplent.platformutility.expression.jeval.function.FunctionResult;

/**
 * Created by sripad on 13/01/16.
 */
public class ToProperCase implements Function {

    /**
     * Returns the name of the function - "toUpperCase".
     *
     * @return The name of this function class.
     */
    public String getName() {
        return "toProperCase";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     *
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A string argument that will be converted into one string
     *            argument. The string argument(s) HAS to be enclosed in quotes.
     *            White space that is not enclosed within quotes will be
     *            trimmed. Quote characters in the first and last positions of
     *            any string argument (after being trimmed) will be removed
     *            also. The quote characters used must be the same as the quote
     *            characters used by the current instance of Evaluator. If there
     *            are multiple arguments, they must be separated by a comma
     *            (",").
     *
     * @return The source string, converted to lowercase.
     *
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    public FunctionResult execute(final Evaluator evaluator, final String arguments)
            throws FunctionException {
        String result = null;
        String exceptionMessage = "One string argument is required.";

        try {
            String stringValue = arguments;

            String argumentOne = FunctionHelper.trimAndRemoveQuoteChars(
                    stringValue, evaluator.getQuoteCharacter());
            StringBuilder titleCase = new StringBuilder();
            boolean nextTitleCase = true;

            for (char c : argumentOne.toCharArray()) {
                if (Character.isSpaceChar(c)) {
                    nextTitleCase = true;
                } else if (nextTitleCase) {
                    c = Character.toTitleCase(c);
                    nextTitleCase = false;
                }

                titleCase.append(c);
            }
            result =titleCase.toString();
        } catch (FunctionException fe) {
            throw new FunctionException(fe.getMessage(), fe);
        } catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result,
                FunctionConstants.FUNCTION_RESULT_TYPE_STRING);
    }
}
