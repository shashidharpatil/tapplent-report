package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 26/04/17.
 */
public class SectionActionTargetsVO {
    private String actionTargetPkId;
    private String sectionActionFkId;
    private String targetSections;

    public String getActionTargetPkId() {
        return actionTargetPkId;
    }

    public void setActionTargetPkId(String actionTargetPkId) {
        this.actionTargetPkId = actionTargetPkId;
    }

    public String getSectionActionFkId() {
        return sectionActionFkId;
    }

    public void setSectionActionFkId(String sectionActionFkId) {
        this.sectionActionFkId = sectionActionFkId;
    }

    public String getTargetSections() {
        return targetSections;
    }

    public void setTargetSections(String targetSections) {
        this.targetSections = targetSections;
    }
}
