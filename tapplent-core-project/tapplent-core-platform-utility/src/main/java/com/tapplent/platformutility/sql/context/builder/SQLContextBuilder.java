package com.tapplent.platformutility.sql.context.builder;

import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.sql.context.model.SQLContextModel;

public interface SQLContextBuilder {
	SQLContextModel build(GlobalVariables globalVariables);

	SQLContextModel buildDefault(GlobalVariables globalVariables);
}
