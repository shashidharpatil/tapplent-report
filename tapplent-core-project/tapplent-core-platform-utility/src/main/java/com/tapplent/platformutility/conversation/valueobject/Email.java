package com.tapplent.platformutility.conversation.valueobject;

import com.tapplent.platformutility.layout.valueObject.IconVO;

/**
 * Created by Tapplent on 6/26/17.
 */
public class Email {
    private String emailId;
    private IconVO emailTypeIconCodeId;
    private String emailTypeName;

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public IconVO getEmailTypeIconCodeId() {
        return emailTypeIconCodeId;
    }

    public void setEmailTypeIconCodeId(IconVO emailTypeIconCodeId) {
        this.emailTypeIconCodeId = emailTypeIconCodeId;
    }

    public String getEmailTypeName() {
        return emailTypeName;
    }

    public void setEmailTypeName(String emailTypeName) {
        this.emailTypeName = emailTypeName;
    }
}
