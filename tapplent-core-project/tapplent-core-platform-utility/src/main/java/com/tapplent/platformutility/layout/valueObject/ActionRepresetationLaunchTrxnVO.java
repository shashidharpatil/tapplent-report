package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class ActionRepresetationLaunchTrxnVO {
	String arsLaunchId;
	String baseTemplateId;	
	String actionRepresentationCode;
	JsonNode deviceIndependentBlob;
	JsonNode deviceDependentBlob;
	public String getArsLaunchId() {
		return arsLaunchId;
	}
	public void setArsLaunchId(String arsLaunchId) {
		this.arsLaunchId = arsLaunchId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getActionRepresentationCode() {
		return actionRepresentationCode;
	}
	public void setActionRepresentationCode(String actionRepresentationCode) {
		this.actionRepresentationCode = actionRepresentationCode;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
}
