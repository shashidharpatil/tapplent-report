/**
 * 
 */
package com.tapplent.platformutility.metadata.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.BeanUtils;

import com.tapplent.platformutility.metadata.DataActionsVO;

/**
 * @author Shubham Patodi
 *
 */
public class DataActions {
	@JsonIgnore
	private String dataDrivenActionsId;
	@JsonIgnore
	private String activeState;
	@JsonIgnore
	private String saveState;
	@JsonIgnore
	private boolean isDeleted;
	@JsonIgnore
	private String workFlowTransactionStatusCode;
	private String actionCode;
	private boolean isApplicable;
	public String getDataDrivenActionsId() {
		return dataDrivenActionsId;
	}
	public void setDataDrivenActionsId(String dataDrivenActionsId) {
		this.dataDrivenActionsId = dataDrivenActionsId;
	}
	public String getActiveState() {
		return activeState;
	}
	public void setActiveState(String activeState) {
		this.activeState = activeState;
	}
	public String getSaveState() {
		return saveState;
	}
	public void setSaveState(String saveState) {
		this.saveState = saveState;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(boolean deleted) {
		isDeleted = deleted;
	}

	public String getWorkFlowTransactionStatusCode() {
		return workFlowTransactionStatusCode;
	}
	public void setWorkFlowTransactionStatusCode(String workFlowTransactionStatusCode) {
		this.workFlowTransactionStatusCode = workFlowTransactionStatusCode;
	}
	public String getActionCode() {
		return actionCode;
	}
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public boolean isApplicable() {
		return isApplicable;
	}
	public void setApplicable(boolean isApplicable) {
		this.isApplicable = isApplicable;
	}
	public static DataActions fromVO(DataActionsVO dataActionsVO){
		DataActions dataActions = new DataActions();
		BeanUtils.copyProperties(dataActionsVO, dataActions);
		return dataActions;
	}
}
