package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.sql.Timestamp;
import java.util.List;

public class SingleNote {

	private String notePkId;
	private String notetTitleG11NBigTxt;
	private String noteDescG11NBigTxt;
	private String aboutPersonFkId;
	private Timestamp reminderDateTime;
	private String reminderLoclatlng;
	private Timestamp lastModifiedDateTime;
	private boolean isDeleted;
	private List<EventAttchment> attachments;
	private Timestamp createdDateTime;
	public Timestamp getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Timestamp createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public String getNotePkId() {
		return notePkId;
	}
	public void setNotePkId(String notePkId) {
		this.notePkId = notePkId;
	}
	public String getNotetTitleG11NBigTxt() {
		return notetTitleG11NBigTxt;
	}
	public void setNotetTitleG11NBigTxt(String notetTitleG11NBigTxt) {
		this.notetTitleG11NBigTxt = notetTitleG11NBigTxt;
	}
	public String getNoteDescG11NBigTxt() {
		return noteDescG11NBigTxt;
	}
	public void setNoteDescG11NBigTxt(String noteDescG11NBigTxt) {
		this.noteDescG11NBigTxt = noteDescG11NBigTxt;
	}
	public String getAboutPersonFkId() {
		return aboutPersonFkId;
	}
	public void setAboutPersonFkId(String aboutPersonFkId) {
		this.aboutPersonFkId = aboutPersonFkId;
	}
	public Timestamp getReminderDateTime() {
		return reminderDateTime;
	}
	public void setReminderDateTime(Timestamp reminderDateTime) {
		this.reminderDateTime = reminderDateTime;
	}
	public String getReminderLoclatlng() {
		return reminderLoclatlng;
	}
	public void setReminderLoclatlng(String reminderLoclatlng) {
		this.reminderLoclatlng = reminderLoclatlng;
	}
	public Timestamp getLastModifiedDateTime() {
		return lastModifiedDateTime;
	}
	public void setLastModifiedDateTime(Timestamp lastModifiedDateTime) {
		this.lastModifiedDateTime = lastModifiedDateTime;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public List<EventAttchment> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<EventAttchment> attachments) {
		this.attachments = attachments;
	}
	
}
