package com.tapplent.platformutility.thirdPartyApp.gcm.controller;


import java.util.ArrayList;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import com.tapplent.platformutility.thirdPartyApp.gcm.helper.*;

//Get the Api key for GCM by creating and registering the project in google console
//Create a notification Key for a single user but using multiple devices using DeviceGroup.java class
//Get Registration Id of each client or user.
//Every client on app install sends the registration ID to the server.
//Also get the project key for the project created in Google console.

public class GCMHandler {

	public String createGroup(String apiKey, String notKeyName, String project_id, ArrayList<String> ids) {
		DGCreateModel creategroup = new DGCreateModel();
		creategroup.setOperation(Constants.create);
		creategroup.setNotification_key_name(notKeyName);
		creategroup.setRegistration_ids(ids);
		String notificatonKey = CreateDeviceGroupCall.post(apiKey, project_id, creategroup);
		return notificatonKey;
	}

	public String addGroupMember(String apiKey, String notKeyName, String project_id, String notification_key,
			ArrayList<String> ids) {
		DGOperationModel addGroupMember = new DGOperationModel();
		addGroupMember.setOperation(Constants.add);
		addGroupMember.setNotification_key_name(notKeyName);
		addGroupMember.setRegistration_ids(ids);
		addGroupMember.setNotification_key(notification_key);
		String notificatonKey = ModifyDeviceGroupCall.post(apiKey, project_id, addGroupMember);
		return notificatonKey;
	}

	public String removeGroupMember(String apiKey, String notKeyName, String project_id, String notification_key,
			ArrayList<String> ids) {
		DGOperationModel addGroupMember = new DGOperationModel();
		addGroupMember.setOperation(Constants.remove);
		addGroupMember.setNotification_key_name(notKeyName);
		addGroupMember.setRegistration_ids(ids);
		addGroupMember.setNotification_key(notification_key);
		String notificatonKey = ModifyDeviceGroupCall.post(apiKey, project_id, addGroupMember);
		return notificatonKey;
	}

	public void sendGCMusingRegId(Notification notification, String regId, String apiKey, NTFN_NotificationData item) {
		ContentforID c = new ContentforID();
		c.addRegId(regId);
		c.createData(notification);
		POST2GCM.post(apiKey, c, null,item);

	}

	public void sendGCMusingNotKey(Notification notification, String notKey, String apiKey,NTFN_NotificationData item) {
		ContentforNotKey c = new ContentforNotKey();
		c.addNotKey(notKey);
		c.createData(notification);
		POST2GCM.post(apiKey, null, c,item);
	}

	
}
