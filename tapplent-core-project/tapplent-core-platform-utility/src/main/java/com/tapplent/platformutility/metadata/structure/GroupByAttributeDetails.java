/**
 * 
 */
package com.tapplent.platformutility.metadata.structure;

/**
 * @author Shubham Patodi
 * @since 06/07/16
 */
public class GroupByAttributeDetails {
	private String groupByAttributePath;
	private int groupByAttributeSeqNumber;
	private boolean internalChildGroupBy;
	public GroupByAttributeDetails(String groupByAttributePath, int groupByAttributeSeqNumber, boolean internalChildGroupBy) {
		this.groupByAttributePath = groupByAttributePath;
		this.groupByAttributeSeqNumber = groupByAttributeSeqNumber;
		this.internalChildGroupBy = internalChildGroupBy;
	}

	public GroupByAttributeDetails() {
	}

	public String getGroupByAttributePath() {
		return groupByAttributePath;
	}
	public void setGroupByAttributePath(String groupByAttributePath) {
		this.groupByAttributePath = groupByAttributePath;
	}
	public int getGroupByAttributeSeqNumber() {
		return groupByAttributeSeqNumber;
	}
	public void setGroupByAttributeSeqNumber(int groupByAttributeSeqNumber) {
		this.groupByAttributeSeqNumber = groupByAttributeSeqNumber;
	}

	public boolean isInternalChildGroupBy() {
		return internalChildGroupBy;
	}

	public void setInternalChildGroupBy(boolean internalChildGroupBy) {
		this.internalChildGroupBy = internalChildGroupBy;
	}
}