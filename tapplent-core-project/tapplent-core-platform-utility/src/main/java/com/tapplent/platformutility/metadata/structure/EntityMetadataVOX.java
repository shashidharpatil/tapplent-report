package com.tapplent.platformutility.metadata.structure;

import java.util.*;

import com.amazonaws.services.dynamodbv2.xspec.B;
import com.tapplent.platformutility.common.util.Util;
import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.common.util.RequestSelectType;

public class EntityMetadataVOX extends EntityMetadata{
	@JsonIgnore
	private Map<String, EntityAttributeMetadata> attributeNameMap = new HashMap<>();
	@JsonIgnore
	private Map<String, EntityAttributeMetadata> attributeColumnNameMap = new HashMap<>();
	@JsonIgnore
	private Map<String, EntityAttributeMetadata> attributeMap = new HashMap<>();
	@JsonIgnore
	private Map<String,Map<String, EntityAttributeRelationMetadata>> relatedAttributeNameMap = new HashMap<>();
	@JsonIgnore
	private Map<String,Map<String, EntityAttributeRelationMetadata>> relatedAttributeColumnNameMap = new HashMap<>();
	@JsonIgnore
	private List<EntityAttributeMetadata> dependencyKeyList = new ArrayList<>();
	@JsonIgnore
	private EntityAttributeMetadata versoinIdAttribute;
	@JsonIgnore
	private Map<String, EntityAttributeMetadata> standardColumnFields = new HashMap();
	@JsonIgnore
	private Map<String, EntityAttributeMetadata> standardDoaFields = new HashMap();
	//Fields for DATA API calls
	@JsonIgnore
	private Map<RequestSelectType, AttributePaths> attributePaths;
	@JsonIgnore
	private EntityAttributeMetadata functionalPrimaryKeyAttribute;
	@JsonIgnore
	private boolean isHierarchyTable = false;
	@JsonIgnore
	private EntityAttributeMetadata hierarchyKeyAttribute = null;
	@JsonIgnore
	/* It is db Primary key attribute */
	private EntityAttributeMetadata recordStateAttribute;
	@JsonIgnore
	private EntityAttributeMetadata saveStateAttribute;
	@JsonIgnore
	private EntityAttributeMetadata activeStateAttribute;
	@JsonIgnore
	private EntityAttributeMetadata isDeletedAttribute;
	@JsonIgnore
	private EntityAttributeMetadata versionChangesBlobAttribute;
	@JsonIgnore
	private EntityAttributeMetadata isFeaturedAttribute;
	@JsonIgnore
	private EntityAttributeMetadata referenceBaseTemplateAttribute;
	@JsonIgnore
	private EntityAttributeMetadata transitioningBaseTemplateAttribute;
	@JsonIgnore
	private EntityAttributeMetadata workFlowTransactionStatusCodeAttribute;
	@JsonIgnore
	private EntityAttributeMetadata dependencyKeyAttribute;
	@JsonIgnore
	private EntityAttributeMetadata effectiveDatetimeAttribute;
	@JsonIgnore
	private EntityAttributeMetadata isContextObjectField;
	@JsonIgnore
	private EntityAttributeMetadata inlineChangeBlobAttribute;
	@JsonIgnore
	private EntityAttributeMetadata compareKeyAttribute;
	@JsonIgnore
	private Boolean isVersioned = false;
	@JsonIgnore
	private Boolean isPrimary = false;
	@JsonIgnore
	private EntityAttributeMetadata primaryCheckerColumn;
	@JsonIgnore
	private Boolean isInlineChangeBlobExist = false;
	
	public static EntityMetadataVOX create(EntityMetadata meta) {
		if (meta == null){
			return null;
		}
        EntityMetadataVOX vox = new EntityMetadataVOX();
        BeanUtils.copyProperties(meta, vox);
//        vox.setUpdateInline(meta.isUpdateInline());
        vox.initialize();
        return vox;
	}

	private void initialize() {
		
//        getAttributeMeta().stream().forEach(attribute -> {
//            if(attribute.getFunctionalPrimaryKeyFlag()) {
//            	primaryKeyAttribute = attribute;
//            }
//            if(attribute.getDbPrimaryKeyFlag()){
//            	versoinIdAttribute = attribute;
//            }
//        });
//        attributeNameMap = getAttributeMeta().stream().collect(Collectors.toMap(EntityAttributeMetadata::getMtDoAttributeSpecFkG11nText, attribute -> attribute));
//        attributeColumnNameMap = getAttributeMeta().stream().collect(Collectors.toMap(EntityAttributeMetadata::getDbColumnName, attribute -> attribute));
//        attributeBtDoaSpecMap = getAttributeMeta().stream().collect(Collectors.toMap(EntityAttributeMetadata::getBtDoAttributeSpecFkId, attribute -> attribute));
		for (EntityAttributeMetadata entityAttributeMetadata : getAttributeMeta()) {

			attributeNameMap.put(entityAttributeMetadata.getDoAttributeCode().trim(), entityAttributeMetadata);
			attributeColumnNameMap.put(entityAttributeMetadata.getDbColumnName().trim(), entityAttributeMetadata);
			Map<String, EntityAttributeRelationMetadata> relatedAttributeByName = new HashMap<>();
			Map<String, EntityAttributeRelationMetadata> relatedAttributeByColumnName = new HashMap<>();
			for(EntityAttributeRelationMetadata attributeRelationMetadata : entityAttributeMetadata.getRelationMeta()){
				relatedAttributeByName.put(attributeRelationMetadata.getMtDoAttributeSpecG11nText().trim(), attributeRelationMetadata);
				relatedAttributeByColumnName.put(attributeRelationMetadata.getDbColumnName().trim(), attributeRelationMetadata);
			}
			relatedAttributeNameMap.put(entityAttributeMetadata.getDoAttributeCode(), relatedAttributeByName);
			relatedAttributeColumnNameMap.put(entityAttributeMetadata.getDoAttributeCode(), relatedAttributeByColumnName);

			if(entityAttributeMetadata.isFunctionalPrimaryKeyFlag()){
				functionalPrimaryKeyAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.isDbPrimaryKeyFlag()){
				versoinIdAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.isSelfJoinParentFlag()){
				isHierarchyTable = true;
				hierarchyKeyAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.isContextObjectField()){
				isContextObjectField = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.isDependencyKeyFlag()){
				dependencyKeyAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".VersionID")){
				isVersioned = true;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".Primary")){
				primaryCheckerColumn = entityAttributeMetadata;
				isPrimary = true;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".RecordState")){
				recordStateAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".ActiveState")){
				activeStateAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".SaveState")){
				saveStateAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".Deleted")){
				isDeletedAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".EffectiveDateTime")){
				effectiveDatetimeAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".VersionChanges")){
				versionChangesBlobAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".Featured")){
				isFeaturedAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".ReferenceBaseTemplate")){
				referenceBaseTemplateAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".TransitioningBaseTemplate")){
				transitioningBaseTemplateAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".ReferenceWorkflowTransactionStatus")){
				workFlowTransactionStatusCodeAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDoAttributeCode().endsWith(".InlineChanges")){
				inlineChangeBlobAttribute = entityAttributeMetadata;
				isInlineChangeBlobExist = true;
			}
			if(entityAttributeMetadata.isCompareKeyForCUD()){
				compareKeyAttribute = entityAttributeMetadata;
			}
			if(entityAttributeMetadata.getDbColumnName().matches("VERSION_ID|ACTIVE_STATE_CODE_FK_ID|RECORD_STATE_CODE_FK_ID|CREATEDBY_PERSON_FK_ID|CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT|CREATED_DATETIME|LAST_MODIFIEDBY_PERSON_FK_ID|LAST_MODIFIEDBY_PERSON_FULLNAME_G11N_BIG_TXT|LAST_MODIFIED_DATETIME|DLGTD_FOR_CREATEDBY_PRSN_FK_ID|DLGTD_FOR_CREATEDBY_PRSN_FULLNAME_G11N_BIG_TXT|DLGTD_FOR_MDFDBY_PRSN_FK_ID|DLGTD_FOR_MDFDBY_PRSN_FULLNAME_G11N_BIG_TXT|LOGGED_IN_PERSON_FK_ID|LAST_MODIFIED_DEVICE_TYPE_CODE_FK_ID|IS_DELETED")){
				standardColumnFields.put(entityAttributeMetadata.getDbColumnName(), entityAttributeMetadata);
				standardDoaFields.put(entityAttributeMetadata.getDoAttributeCode(), entityAttributeMetadata);
			}
			attributeMap.putAll(attributeNameMap);
			attributeMap.putAll(attributeColumnNameMap);
		}
	}
	
	public void setAttributeByName(String attributeName, EntityAttributeMetadata attribute) {
		this.attributeNameMap.put(attributeName, attribute);
	}

	public EntityAttributeMetadata getAttributeByName(String attributeName) {
		return this.attributeNameMap.get(attributeName);
	}

	public void setAttributeByColumnName(String columnName, EntityAttributeMetadata attribute) {
		this.attributeColumnNameMap.put(columnName, attribute);
	}

	public EntityAttributeMetadata getAttributeByColumnName(String columnName) {
		return this.attributeColumnNameMap.get(columnName);
	}

	public void setAttribute(String attributeNameOrColumn, EntityAttributeMetadata attribute) {
		this.attributeMap.put(attributeNameOrColumn, attribute);
	}

	public EntityAttributeMetadata getAttribute(String btDoaSpecId) {
		return this.attributeMap.get(btDoaSpecId);
	}

	public EntityAttributeMetadata getVersoinIdAttribute() {
		return versoinIdAttribute;
	}

	public void setVersoinIdAttribute(EntityAttributeMetadata versoinIdAttribute) {
		this.versoinIdAttribute = versoinIdAttribute;
	}

	public Map<String, EntityAttributeMetadata> getAttributeNameMap() {
		return attributeNameMap;
	}

	public void setAttributeNameMap(Map<String, EntityAttributeMetadata> attributeNameMap) {
		this.attributeNameMap = attributeNameMap;
	}

	public Map<String, EntityAttributeMetadata> getAttributeColumnNameMap() {
		return attributeColumnNameMap;
	}

	public void setAttributeColumnNameMap(Map<String, EntityAttributeMetadata> attributeColumnNameMap) {
		this.attributeColumnNameMap = attributeColumnNameMap;
	}

	public Map<String, Map<String, EntityAttributeRelationMetadata>> getRelatedAttributeNameMap() {
		return relatedAttributeNameMap;
	}

	public void setRelatedAttributeNameMap(
			Map<String, Map<String, EntityAttributeRelationMetadata>> relatedAttributeNameMap) {
		this.relatedAttributeNameMap = relatedAttributeNameMap;
	}

	public Map<String, Map<String, EntityAttributeRelationMetadata>> getRelatedAttributeColumnNameMap() {
		return relatedAttributeColumnNameMap;
	}

	public void setRelatedAttributeColumnNameMap(
			Map<String, Map<String, EntityAttributeRelationMetadata>> relatedAttributeColumnNameMap) {
		this.relatedAttributeColumnNameMap = relatedAttributeColumnNameMap;
	}

	public EntityAttributeMetadata getIsContextObjectField() {
		return isContextObjectField;
	}

	public void setIsContextObjectField(EntityAttributeMetadata isContextObjectField) {
		this.isContextObjectField = isContextObjectField;
	}
	//	public AttributePaths getAttributePaths(String selectType) {
//		return attributePaths;
//	}
//
//	public void setAttributePaths(AttributePaths attributePaths) {
//		this.attributePaths = attributePaths;
//	}

	public Map<String, EntityAttributeMetadata> getAttributeMap() {
		return attributeMap;
	}

	public void setAttributeMap(Map<String, EntityAttributeMetadata> attributeMap) {
		this.attributeMap = attributeMap;
	}

	public Map<RequestSelectType, AttributePaths> getAttributePaths() {
		return attributePaths;
	}

	public void setAttributePaths(Map<RequestSelectType, AttributePaths> attributePaths) {
		this.attributePaths = attributePaths;
	}

	public EntityAttributeMetadata getFunctionalPrimaryKeyAttribute() {
		return functionalPrimaryKeyAttribute;
	}

	public void setFunctionalPrimaryKeyAttribute(EntityAttributeMetadata functionalPrimaryKeyAttribute) {
		this.functionalPrimaryKeyAttribute = functionalPrimaryKeyAttribute;
	}

	public EntityAttributeMetadata getHierarchyKeyAttribute() {
		return hierarchyKeyAttribute;
	}

	public void setHierarchyKeyAttribute(EntityAttributeMetadata hierarchyKeyAttribute) {
		this.hierarchyKeyAttribute = hierarchyKeyAttribute;
	}

	public EntityAttributeMetadata getRecordStateAttribute() {
		return recordStateAttribute;
	}

	public void setRecordStateAttribute(EntityAttributeMetadata recordStateAttribute) {
		this.recordStateAttribute = recordStateAttribute;
	}

	public EntityAttributeMetadata getSaveStateAttribute() {
		return saveStateAttribute;
	}

	public void setSaveStateAttribute(EntityAttributeMetadata saveStateAttribute) {
		this.saveStateAttribute = saveStateAttribute;
	}

	public EntityAttributeMetadata getActiveStateAttribute() {
		return activeStateAttribute;
	}

	public void setActiveStateAttribute(EntityAttributeMetadata activeStateAttribute) {
		this.activeStateAttribute = activeStateAttribute;
	}

	public EntityAttributeMetadata getIsDeletedAttribute() {
		return isDeletedAttribute;
	}

	public void setIsDeletedAttribute(EntityAttributeMetadata isDeletedAttribute) {
		this.isDeletedAttribute = isDeletedAttribute;
	}

	public EntityAttributeMetadata getVersionChangesBlobAttribute() {
		return versionChangesBlobAttribute;
	}

	public void setVersionChangesBlobAttribute(EntityAttributeMetadata versionChangesBlobAttribute) {
		this.versionChangesBlobAttribute = versionChangesBlobAttribute;
	}

	public EntityAttributeMetadata getIsFeaturedAttribute() {
		return isFeaturedAttribute;
	}

	public void setIsFeaturedAttribute(EntityAttributeMetadata isFeaturedAttribute) {
		this.isFeaturedAttribute = isFeaturedAttribute;
	}

	public EntityAttributeMetadata getReferenceBaseTemplateAttribute() {
		return referenceBaseTemplateAttribute;
	}

	public void setReferenceBaseTemplateAttribute(EntityAttributeMetadata referenceBaseTemplateAttribute) {
		this.referenceBaseTemplateAttribute = referenceBaseTemplateAttribute;
	}

	public EntityAttributeMetadata getTransitioningBaseTemplateAttribute() {
		return transitioningBaseTemplateAttribute;
	}

	public void setTransitioningBaseTemplateAttribute(EntityAttributeMetadata transitioningBaseTemplateAttribute) {
		this.transitioningBaseTemplateAttribute = transitioningBaseTemplateAttribute;
	}

	public EntityAttributeMetadata getWorkFlowTransactionStatusCodeAttribute() {
		return workFlowTransactionStatusCodeAttribute;
	}

	public void setWorkFlowTransactionStatusCodeAttribute(EntityAttributeMetadata workFlowTransactionStatusCodeAttribute) {
		this.workFlowTransactionStatusCodeAttribute = workFlowTransactionStatusCodeAttribute;
	}

	public Map<String, EntityAttributeMetadata> getStandardColumnFields() {
		return standardColumnFields;
	}

	public void setStandardColumnFields(Map<String, EntityAttributeMetadata> standardColumnFields) {
		this.standardColumnFields = standardColumnFields;
	}

	public Map<String, EntityAttributeMetadata> getStandardDoaFields() {
		return standardDoaFields;
	}

	public void setStandardDoaFields(Map<String, EntityAttributeMetadata> standardDoaFields) {
		this.standardDoaFields = standardDoaFields;
	}

	public List<EntityAttributeMetadata> getDependencyKeyList() {
		return dependencyKeyList;
	}

	public void setDependencyKeyList(List<EntityAttributeMetadata> dependencyKeyList) {
		this.dependencyKeyList = dependencyKeyList;
	}

	public Boolean isVersioned() {
		return isVersioned;
	}

	public void setVersioned(Boolean versioned) {
		isVersioned = versioned;
	}

	public Boolean getInlineChangeBlobExist() {
		return isInlineChangeBlobExist;
	}

	public void setInlineChangeBlobExist(Boolean inlineChangeBlobExist) {
		isInlineChangeBlobExist = inlineChangeBlobExist;
	}

	public EntityAttributeMetadata getInlineChangeBlobAttribute() {
		return inlineChangeBlobAttribute;
	}

	public void setInlineChangeBlobAttribute(EntityAttributeMetadata inlineChangeBlobAttribute) {
		this.inlineChangeBlobAttribute = inlineChangeBlobAttribute;
	}

	public EntityAttributeMetadata getDependencyKeyAttribute() {
		return dependencyKeyAttribute;
	}

	public void setDependencyKeyAttribute(EntityAttributeMetadata dependencyKeyAttribute) {
		this.dependencyKeyAttribute = dependencyKeyAttribute;
	}

	public EntityAttributeMetadata getCompareKeyAttribute() {
		return compareKeyAttribute;
	}

	public void setCompareKeyAttribute(EntityAttributeMetadata compareKeyAttribute) {
		this.compareKeyAttribute = compareKeyAttribute;
	}

	public EntityAttributeMetadata getEffectiveDatetimeAttribute() {
		return effectiveDatetimeAttribute;
	}

	public void setEffectiveDatetimeAttribute(EntityAttributeMetadata effectiveDatetimeAttribute) {
		this.effectiveDatetimeAttribute = effectiveDatetimeAttribute;
	}

	public Boolean getPrimary() {
		return isPrimary;
	}

	public void setPrimary(Boolean primary) {
		isPrimary = primary;
	}

	public EntityAttributeMetadata getPrimaryCheckerColumn() {
		return primaryCheckerColumn;
	}

	public void setPrimaryCheckerColumn(EntityAttributeMetadata primaryCheckerColumn) {
		this.primaryCheckerColumn = primaryCheckerColumn;
	}


	/**
	 * @param selectType
	 * @return
	 */
	public AttributePaths getAttributePaths(RequestSelectType selectType) {
		if(attributePaths == null)
			return null;
		return attributePaths.get(selectType);
	}

	/**
	 * @param selectType
	 * @param attributePaths2
	 */
	public void setAttributePaths(RequestSelectType selectType, AttributePaths attributePaths2) {
		if(attributePaths == null)
			attributePaths = new HashMap<RequestSelectType, AttributePaths>();
		attributePaths.put(selectType, attributePaths2);
	}
	@Override
	public String getBtDOName() {
		return Util.getG11nValue(super.getBtDOName(), null);
	}

	public String getBtDOG11nName(){
		return super.getBtDOName();
	}

	@Override
	public String getCountOfRecordsPluralTitle() {
		return Util.getG11nValue(super.getCountOfRecordsPluralTitle(), null);
	}
	@Override
	public String getCountOfRecordsSingularTitle() {
		return Util.getG11nValue(super.getCountOfRecordsSingularTitle(), null);
	}
}
