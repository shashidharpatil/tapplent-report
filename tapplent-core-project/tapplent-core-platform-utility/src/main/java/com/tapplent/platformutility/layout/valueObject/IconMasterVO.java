/**
 * 
 */
package com.tapplent.platformutility.layout.valueObject;

/**
 * @author Shubham Patodi
 *
 */
public class IconMasterVO {
	private String iconCode;
	private String iconClass;
	private String iconUnicode;
	public String getIconCode() {
		return iconCode;
	}
	public void setIconCode(String iconCode) {
		this.iconCode = iconCode;
	}
	public String getIconClass() {
		return iconClass;
	}
	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}
	public String getIconUnicode() {
		return iconUnicode;
	}
	public void setIconUnicode(String iconUnicode) {
		this.iconUnicode = iconUnicode;
	}
	
}
