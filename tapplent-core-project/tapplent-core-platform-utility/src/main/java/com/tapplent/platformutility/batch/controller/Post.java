package com.tapplent.platformutility.batch.controller;

import java.util.concurrent.Callable;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.helper.POST_PostHelper;
import com.tapplent.platformutility.batch.model.SchedulerInstanceModel;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class Post implements Callable {

	SchedulerInstanceModel schedulerInstanceModel;

	public Post(SchedulerInstanceModel schedulerInstanceModel) {
		this.schedulerInstanceModel = schedulerInstanceModel;
	}
	@Override
	public Object call() throws Exception {
		String[] springConfig = { "jobs/Post.xml" };
		ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
		jobParametersBuilder.addString("facebookconfig", POST_PostHelper.getOrgConfig(Constants.faceBook));
		jobParametersBuilder.addString("linkedinconfig", POST_PostHelper.getOrgConfig(Constants.linkedIn));
		jobParametersBuilder.addString("twitterconfig", POST_PostHelper.getOrgConfig(Constants.twitter));
		jobParametersBuilder.addString("schedulerInstancePkId", schedulerInstanceModel.getSchedulerInstancePkId());
		Job job = (Job) context.getBean("Post");
		try {
			JobExecution execution = jobLauncher.run(job, jobParametersBuilder.toJobParameters());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
