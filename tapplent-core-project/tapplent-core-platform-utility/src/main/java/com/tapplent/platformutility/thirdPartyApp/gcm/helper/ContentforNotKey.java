package com.tapplent.platformutility.thirdPartyApp.gcm.helper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ContentforNotKey implements Serializable {

	
	
	private List<String> to;
	private Map<String,String> data;
	
	
	public void addNotKey(String regId){
		if(to==null)
			to=new LinkedList();
		to.add(regId);
	}
	
	public void createData(Notification notification){
		if(data == null)
			data = new HashMap<String,String>();
	
		data.put(NotificationConstants.title.toString(), notification.getTitle());
		data.put(NotificationConstants.notificationText.toString(), notification.getNotificationText());
		data.put(NotificationConstants.notificationId.toString(), notification.getNotificationId());
		data.put(NotificationConstants.subText.toString(), notification.getSubText());
		data.put(NotificationConstants.smallIcon.toString(), notification.getSmallIcon());
		data.put(NotificationConstants.actions.toString(), notification.getActions());
		data.put(NotificationConstants.notificationKey.toString(), notification.getNotificationKey());
	}
	
	
	public List<String> getRegistration_ids() {
		return to;
	}

	public void setRegistration_ids(List<String> registration_ids) {
		this.to = registration_ids;
	}

	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}
}
