package com.tapplent.platformutility.batch.scheduler;

import com.tapplent.platformutility.batch.controller.BatchDAO;

public class SchedulerInitOnStartUp implements Runnable {
    BatchDAO batchDAO;

    public void setBatchDAO(BatchDAO batchDAO) {
        this.batchDAO = batchDAO;
    }

    @Override
    public void run() {
        try {
            batchDAO.resolveSchInstances(true,null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
