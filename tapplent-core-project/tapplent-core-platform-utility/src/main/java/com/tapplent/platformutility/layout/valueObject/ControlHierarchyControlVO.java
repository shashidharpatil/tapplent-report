package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class ControlHierarchyControlVO {
	private String ControlHCId;
	private String baseTemplateId;
	private String mtPE;
	private String canvasId;
	private String controlId;
	private String hcCanvasControlMasterId;
	private String hcCanvasControlAttribute;
	private String hcCanvasControlAggregateFunction;
	private JsonNode deviceIndependentBlob;
	private JsonNode deviceDependentBlob;
	public String getControlHCId() {
		return ControlHCId;
	}
	public void setControlHCId(String controlHCId) {
		ControlHCId = controlHCId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPE() {
		return mtPE;
	}
	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public String getControlId() {
		return controlId;
	}
	public void setControlId(String controlId) {
		this.controlId = controlId;
	}
	public String getHcCanvasControlMasterId() {
		return hcCanvasControlMasterId;
	}
	public void setHcCanvasControlMasterId(String hcCanvasControlMasterId) {
		this.hcCanvasControlMasterId = hcCanvasControlMasterId;
	}
	public String getHcCanvasControlAttribute() {
		return hcCanvasControlAttribute;
	}
	public void setHcCanvasControlAttribute(String hcCanvasControlAttribute) {
		this.hcCanvasControlAttribute = hcCanvasControlAttribute;
	}
	public String getHcCanvasControlAggregateFunction() {
		return hcCanvasControlAggregateFunction;
	}
	public void setHcCanvasControlAggregateFunction(String hcCanvasControlAggregateFunction) {
		this.hcCanvasControlAggregateFunction = hcCanvasControlAggregateFunction;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
}
