package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 23/01/17.
 */
public class AppHomePageVO {
    private String appHomePagePkId;
    private String loggedInPersonFkId;
    private String appHomePageRuleFkId;
    private String categoryTitleG11nBigTxt;
    private String categoryIconCodeFkId;
    private String gridItemNameTxt;
    private boolean isCardFlippable;
    private int displaySeqNumPosInt;
    private String dynamicActionArgBigTxt;
    private String dynamicActionFilterExpn;

    public String getAppHomePagePkId() {
        return appHomePagePkId;
    }

    public void setAppHomePagePkId(String appHomePagePkId) {
        this.appHomePagePkId = appHomePagePkId;
    }

    public String getLoggedInPersonFkId() {
        return loggedInPersonFkId;
    }

    public void setLoggedInPersonFkId(String loggedInPersonFkId) {
        this.loggedInPersonFkId = loggedInPersonFkId;
    }

    public String getAppHomePageRuleFkId() {
        return appHomePageRuleFkId;
    }

    public void setAppHomePageRuleFkId(String appHomePageRuleFkId) {
        this.appHomePageRuleFkId = appHomePageRuleFkId;
    }

    public String getCategoryTitleG11nBigTxt() {
        return categoryTitleG11nBigTxt;
    }

    public void setCategoryTitleG11nBigTxt(String categoryTitleG11nBigTxt) {
        this.categoryTitleG11nBigTxt = categoryTitleG11nBigTxt;
    }

    public String getCategoryIconCodeFkId() {
        return categoryIconCodeFkId;
    }

    public void setCategoryIconCodeFkId(String categoryIconCodeFkId) {
        this.categoryIconCodeFkId = categoryIconCodeFkId;
    }

    public String getGridItemNameTxt() {
        return gridItemNameTxt;
    }

    public void setGridItemNameTxt(String gridItemNameTxt) {
        this.gridItemNameTxt = gridItemNameTxt;
    }

    public boolean isCardFlippable() {
        return isCardFlippable;
    }

    public void setCardFlippable(boolean cardFlippable) {
        isCardFlippable = cardFlippable;
    }

    public int getDisplaySeqNumPosInt() {
        return displaySeqNumPosInt;
    }

    public void setDisplaySeqNumPosInt(int displaySeqNumPosInt) {
        this.displaySeqNumPosInt = displaySeqNumPosInt;
    }

    public String getDynamicActionArgBigTxt() {
        return dynamicActionArgBigTxt;
    }

    public void setDynamicActionArgBigTxt(String dynamicActionArgBigTxt) {
        this.dynamicActionArgBigTxt = dynamicActionArgBigTxt;
    }

    public String getDynamicActionFilterExpn() {
        return dynamicActionFilterExpn;
    }

    public void setDynamicActionFilterExpn(String dynamicActionFilterExpn) {
        this.dynamicActionFilterExpn = dynamicActionFilterExpn;
    }
}
