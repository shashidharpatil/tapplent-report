package com.tapplent.platformutility.uilayout.valueobject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 10/06/17.
 */
public class GenericQueryVO {
    private String queryID;
    private List<GenericQueryAttributesVO> attributes = new ArrayList<>();
    private List<GenericQueryFiltersVO> filters = new ArrayList<>();

    public String getQueryID() {
        return queryID;
    }

    public void setQueryID(String queryID) {
        this.queryID = queryID;
    }

    public List<GenericQueryAttributesVO> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<GenericQueryAttributesVO> attributes) {
        this.attributes = attributes;
    }

    public List<GenericQueryFiltersVO> getFilters() {
        return filters;
    }

    public void setFilters(List<GenericQueryFiltersVO> filters) {
        this.filters = filters;
    }
}
