package com.tapplent.platformutility.lexer;

import com.tapplent.platformutility.search.builder.expression.LeafOperand;
import com.tapplent.platformutility.search.builder.expression.Operator;

public class Token {
	LeafOperand leafOperand = new LeafOperand();
	Operator operator;
	public LeafOperand getLeafOperand() {
		return leafOperand;
	}
	public void setLeafOperand(LeafOperand leafOperand) {
		this.leafOperand = leafOperand;
	}
	public Operator getOperator() {
		return operator;
	}
	public void setOperator(Operator operator) {
		this.operator = operator;
	}
}
