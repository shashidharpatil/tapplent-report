package com.tapplent.platformutility.metadatamap;

import com.tapplent.platformutility.metadata.structure.EntityMetadata;

public interface MetaDataMapBuilder {
	public MetaDataMap getMetaDataMap(EntityMetadata entityMetadata);
}
