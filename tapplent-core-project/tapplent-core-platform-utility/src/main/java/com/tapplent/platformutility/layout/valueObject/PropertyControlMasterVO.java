package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by Shubham Patodi on 28/07/16.
 */
public class PropertyControlMasterVO {
    private String propertyControlId;
    private String canvasId;
    private String propertyControlCode;
    private JsonNode deviceIndependentBlob;
    private JsonNode deviceDependentBlob;

    public String getPropertyControlId() {
        return propertyControlId;
    }

    public void setPropertyControlId(String propertyControlId) {
        this.propertyControlId = propertyControlId;
    }

    public String getCanvasId() {
        return canvasId;
    }

    public void setCanvasId(String canvasId) {
        this.canvasId = canvasId;
    }

    public String getPropertyControlCode() {
        return propertyControlCode;
    }

    public void setPropertyControlCode(String propertyControlCode) {
        this.propertyControlCode = propertyControlCode;
    }

    public JsonNode getDeviceIndependentBlob() {
        return deviceIndependentBlob;
    }

    public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
        this.deviceIndependentBlob = deviceIndependentBlob;
    }

    public JsonNode getDeviceDependentBlob() {
        return deviceDependentBlob;
    }

    public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
        this.deviceDependentBlob = deviceDependentBlob;
    }
}
