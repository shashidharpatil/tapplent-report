package com.tapplent.platformutility.batch.model;

public class SchedulerModel {
	
	private String schedDefnPkId;
	private String jobTypeCodeFkId;
	private boolean isOneTimeSched;
	private boolean isRecurringSched;
	private String oneTimeDatetime;
	private String schedGrouperPkId;
	private String schedDrivingFreqCodeFkId;
	private Integer schedDrivingIntervalPosInt;
	private String schedExecStartDatetime;
	private String schedExecEndDatetime;
	private boolean isExecutionNeverEnds;
	private String schedRcrFreqPkId;
	private String frequencyCodeFkId;
	private String intervalPosInt;
	private String absoluteDatetime;
	public String getSchedDefnPkId() {
		return schedDefnPkId;
	}
	public void setSchedDefnPkId(String schedDefnPkId) {
		this.schedDefnPkId = schedDefnPkId;
	}
	public String getJobTypeCodeFkId() {
		return jobTypeCodeFkId;
	}
	public void setJobTypeCodeFkId(String jobTypeCodeFkId) {
		this.jobTypeCodeFkId = jobTypeCodeFkId;
	}
	public boolean isOneTimeSched() {
		return isOneTimeSched;
	}
	public void setOneTimeSched(boolean isOneTimeSched) {
		this.isOneTimeSched = isOneTimeSched;
	}
	public boolean isRecurringSched() {
		return isRecurringSched;
	}
	public void setRecurringSched(boolean isRecurringSched) {
		this.isRecurringSched = isRecurringSched;
	}
	public String getOneTimeDatetime() {
		return oneTimeDatetime;
	}
	public void setOneTimeDatetime(String oneTimeDatetime) {
		this.oneTimeDatetime = oneTimeDatetime;
	}
	public String getSchedGrouperPkId() {
		return schedGrouperPkId;
	}
	public void setSchedGrouperPkId(String schedGrouperPkId) {
		this.schedGrouperPkId = schedGrouperPkId;
	}
	public String getSchedDrivingFreqCodeFkId() {
		return schedDrivingFreqCodeFkId;
	}
	public void setSchedDrivingFreqCodeFkId(String schedDrivingFreqCodeFkId) {
		this.schedDrivingFreqCodeFkId = schedDrivingFreqCodeFkId;
	}
	public Integer getSchedDrivingIntervalPosInt() {
		return schedDrivingIntervalPosInt;
	}
	public void setSchedDrivingIntervalPosInt(Integer schedDrivingIntervalPosInt) {
		this.schedDrivingIntervalPosInt = schedDrivingIntervalPosInt;
	}
	public String getSchedExecStartDatetime() {
		return schedExecStartDatetime;
	}
	public void setSchedExecStartDatetime(String schedExecStartDatetime) {
		this.schedExecStartDatetime = schedExecStartDatetime;
	}
	public String getSchedExecEndDatetime() {
		return schedExecEndDatetime;
	}
	public void setSchedExecEndDatetime(String schedExecEndDatetime) {
		this.schedExecEndDatetime = schedExecEndDatetime;
	}
	public boolean isExecutionNeverEnds() {
		return isExecutionNeverEnds;
	}
	public void setExecutionNeverEnds(boolean isExecutionNeverEnds) {
		this.isExecutionNeverEnds = isExecutionNeverEnds;
	}
	public String getSchedRcrFreqPkId() {
		return schedRcrFreqPkId;
	}
	public void setSchedRcrFreqPkId(String schedRcrFreqPkId) {
		this.schedRcrFreqPkId = schedRcrFreqPkId;
	}
	public String getFrequencyCodeFkId() {
		return frequencyCodeFkId;
	}
	public void setFrequencyCodeFkId(String frequencyCodeFkId) {
		this.frequencyCodeFkId = frequencyCodeFkId;
	}
	public String getIntervalPosInt() {
		return intervalPosInt;
	}
	public void setIntervalPosInt(String intervalPosInt) {
		this.intervalPosInt = intervalPosInt;
	}
	public String getAbsoluteDatetime() {
		return absoluteDatetime;
	}
	public void setAbsoluteDatetime(String absoluteDatetime) {
		this.absoluteDatetime = absoluteDatetime;
	}
	
	
	
	
	
	

}
