package com.tapplent.platformutility.accessmanager.dao;

import com.tapplent.platformutility.accessmanager.structure.*;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.etl.valueObject.ETLMetaHelperVO;
import com.tapplent.platformutility.expression.jeval.EvaluationConstants;
import com.tapplent.platformutility.expression.jeval.EvaluationException;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.hierarchy.structure.Relation;
import com.tapplent.platformutility.layout.valueObject.DOADetails;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.person.structure.PersonGroup;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

import java.sql.SQLException;
import java.util.*;

/**
 * Created by Manas on 02/02/17.
 */
public class AccessManagerServiceImpl implements AccessManagerService{

    AccessManagerDAO accessManagerDAO;

    @Override
    public Permission getPermissionsForData(Map<String, Object> data, String mtPE, EntityMetadataVOX entityMetadataVOX) {
        String subjectUserId = getSubjectUserId(data, entityMetadataVOX.getDomainObjectCode());
        AMRowResolved parentAmRowResolved = getPermissionsForParentPERecord(data, entityMetadataVOX, subjectUserId);
        if (parentAmRowResolved!= null && !parentAmRowResolved.isReadCurrentPermitted())
            return null;
        Map<String, RowCondition> rowConditions;
        Map<String, RowCondition> rowConditionsSatisfied = new HashMap<>();
        PersonGroup whoGroup = TenantAwareCache.getPersonRepository().getPersonGroups(TenantContextHolder.getUserContext().getPersonId());
        if (subjectUserId == null){
            rowConditions = getRowConditionsFor(whoGroup, mtPE);
        }
        else {
            PersonGroup whoseGroup = getWhoseGroupFkId(data, entityMetadataVOX.getDomainObjectCode(), subjectUserId);
            Map<String, Integer> groupIdPositionMap = getRelationBetweenTwoPersons(TenantContextHolder.getUserContext().getPersonId(), subjectUserId);
            rowConditions = getRowConditionsFor(whoGroup, whoseGroup, mtPE, groupIdPositionMap);
        }
        Evaluator evaluator = new Evaluator();
        evaluator.setVariables(data);
        evaluator.setQuoteCharacter(EvaluationConstants.DOUBLE_QUOTE);
        try {
            for (Map.Entry<String, RowCondition> entry : rowConditions.entrySet()) {
                boolean rowConditionSatify = evaluateExpression(entry.getValue().getWhatDOExpression(), evaluator, entityMetadataVOX);
                if (rowConditionSatify) {
                    rowConditionsSatisfied.put(entry.getKey(), entry.getValue());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        accessManagerDAO.setColumnConditions(rowConditionsSatisfied);
        Map<String, DOAPermission> attributePermission = new HashMap<>();
        entityMetadataVOX.getAttributeNameMap().keySet().forEach(p -> attributePermission.put(p, null));
        for (Map.Entry<String, RowCondition> entry : rowConditionsSatisfied.entrySet()){
            for (AMColCondition amColCondition : entry.getValue().getColumnConditionList()){
                DOAPermission doaPermission = attributePermission.get(amColCondition.getWhatDoaFkId());
                if (doaPermission == null){
                    doaPermission = new DOAPermission();
                }
                if (doaPermission.getCreateCurrentPermitted() != true)
                    doaPermission.setCreateCurrentPermitted(amColCondition.isCreateCurrentPermitted());
                if (doaPermission.getCreateHistoryPermitted() != true)
                    doaPermission.setCreateHistoryPermitted(amColCondition.isCreateHistoryPermitted());
                if (doaPermission.getCreateFuturePermitted() != true)
                    doaPermission.setCreateFuturePermitted(amColCondition.isCreateFuturePermitted());

                if (doaPermission.getReadCurrentPermitted() != true)
                    doaPermission.setReadCurrentPermitted(amColCondition.isReadCurrentPermitted());
                if (doaPermission.getReadHistoryPermitted() != true)
                    doaPermission.setReadHistoryPermitted(amColCondition.isReadHistoryPermitted());
                if (doaPermission.getReadFuturePermitted() != true)
                    doaPermission.setReadFuturePermitted(amColCondition.isReadFuturePermitted());

                if (doaPermission.getUpdateCurrentPermitted() != true)
                    doaPermission.setUpdateCurrentPermitted(amColCondition.isUpdateCurrentPermitted());
                if (doaPermission.getUpdateHistoryPermitted() != true)
                    doaPermission.setUpdateHistoryPermitted(amColCondition.isUpdateHistoryPermitted());
                if (doaPermission.getUpdateFuturePermitted() != true)
                    doaPermission.setUpdateFuturePermitted(amColCondition.isUpdateFuturePermitted());

                attributePermission.put(amColCondition.getWhatDoaFkId(), doaPermission);
            }
        }
        Permission permission = new Permission();
        permission.setAttributePermission(attributePermission);
        return permission;
    }

    private AMRowResolved getPermissionsForParentPERecord(Map<String, Object> data, EntityMetadataVOX entityMetadataVOX, String subjectUserId) {
        EntityAttributeMetadata dependencyAttributeMeta = entityMetadataVOX.getDependencyKeyAttribute();
        if (dependencyAttributeMeta != null && data.get(dependencyAttributeMeta.getDoAttributeCode()) != null) {
            String parentWhatDbPrimaryKey = (String) data.get(entityMetadataVOX.getDependencyKeyAttribute().getDoAttributeCode());
            AMRowResolved amRowResolved = accessManagerDAO.getAMRowResolvedForParentPE(parentWhatDbPrimaryKey, entityMetadataVOX.getDependencyKeyAttribute(), subjectUserId);
            return amRowResolved;
        }
        return null;
    }

    private Map<String, Integer> getRelationBetweenTwoPersons(String loggedInPersonId, String subjectUserId) {
        Map<String, Map<String, List<Relation>>> relationMap = TenantAwareCache.getPersonRepository().getPersonRelationMap(loggedInPersonId, subjectUserId);
        Map<String, Integer> loggedInPersonToSubjectPersonMap = new HashMap<>();
        if (relationMap != null && !relationMap.isEmpty()){
            List<Relation> relations = relationMap.get(loggedInPersonId).get(subjectUserId);
            for (Relation relation : relations) {
                loggedInPersonToSubjectPersonMap.put(relation.getRelationshipType(), relation.getLevel());
            }
        }
        return loggedInPersonToSubjectPersonMap;
    }

    private Map<String,RowCondition> getRowConditionsFor(PersonGroup whoGroup, String mtPE) {
        if (whoGroup == null || whoGroup.getGroupIds() == null || whoGroup.getGroupIds().isEmpty())
            return null;
        List<AccessManager> accessManagers = accessManagerDAO.getAccessManagersByWhoGroup(whoGroup.getGroupIds());
        List<AMProcess> amProcesses = accessManagerDAO.getAmProcessByAccessManager(accessManagers);
        Map<String, RowCondition> rowConditions = accessManagerDAO.getRowConditions(amProcesses, mtPE);
        return rowConditions;
    }

    private Map<String, RowCondition> getRowConditionsFor(PersonGroup whoGroup, PersonGroup whoseGroup, String mtPE, Map<String, Integer> groupIdPositionMap) {
        Map<String, RowCondition> absoluteRowConditions = accessManagerDAO.getAbsoluteRowConditions(whoGroup, whoseGroup, mtPE);
        Map<String, RowCondition> relativeRowConditions = accessManagerDAO.getRelativeRowConditons(groupIdPositionMap, whoGroup, mtPE);
        if (relativeRowConditions != null)
            absoluteRowConditions.putAll(relativeRowConditions);
        return absoluteRowConditions;
    }

    private String getSubjectUserId(Map<String, Object> data, String domainObjectCode) {
        MasterEntityMetadataVOX masterEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByDo(domainObjectCode);
        String subjectUserPath = masterEntityMetadataVOX.getSubjectPersonDoaFullPath();
        String subjectUserId;
        if(subjectUserPath.contains(":")){
            String[] strings = subjectUserPath.split(":",2);
            String pk = (String) data.get(strings[0]);
            String[] doStrings = strings[1].split(".", 2);
            String childDoCode = doStrings[0];
            subjectUserId = TenantAwareCache.getMetaDataRepository().getSubjectUser(childDoCode, pk);
        }
        else
            subjectUserId = (String) data.get(subjectUserPath.replaceAll("[(!{)|}]",""));
        return subjectUserId;
    }

    @Override
    public void updatePermissionsForData(GlobalVariables globalVariables) throws SQLException {
        EntityMetadataVOX metadataVOX = globalVariables.entityMetadataVOX;
        MasterEntityMetadataVOX masterEntityMetadataVOX = globalVariables.masterEntityMetadataVOX;
//        Map<String, RowCondition> parentRowConditionMap = null;
//        Map<String, AMRowIntermediate> parentRowIntermediateConditions = null;
        MTPE mtPE = TenantAwareCache.getMetaDataRepository().getMTPE(metadataVOX.getMtPE());
        Object dependencyKey = null;
        if (mtPE.getParentMtProcessElementCodeFkId() != null) {
            EntityAttributeMetadata dependencyDOA = metadataVOX.getDependencyKeyAttribute();
            dependencyKey = globalVariables.insertData.getData().get(dependencyDOA);
        }
        String whoseSubjectUserId = null;
        if (masterEntityMetadataVOX.getSubjectPersonDoaFullPath() != null) {//FIXME subjectuserdriven
            String subjectuserDoaName = masterEntityMetadataVOX.getSubjectPersonDoaFullPath().replaceAll("[!{|}]", "");
            if (globalVariables.insertData.getData().get(subjectuserDoaName) != null){
                whoseSubjectUserId = (String) globalVariables.insertData.getData().get(subjectuserDoaName);
            }
            else whoseSubjectUserId = getSubjectUser(metadataVOX.getDomainObjectCode(), (String) dependencyKey);
        }

//        if (mtPE.getParentMtProcessElementCodeFkId() != null){
//            EntityAttributeMetadata dependencyDOA = metadataVOX.getDependencyKeyAttribute();
//            Object dependencyKey = globalVariables.insertData.getData().get(dependencyDOA);
            // Here we are getting all the parent row conditions.
//            parentRowIntermediateConditions = accessManagerDAO.getRowIntermediateByDependencykey(dependencyDOA, dependencyKey);
            // Here we are getting child row conditions corresponding to the above parent.
//            parentRowConditionMap = accessManagerDAO.getRowConditions(parentRowIntermediateConditions, metadataVOX.getDomainObjectCode());
//            Map<String, List<AMWhatFilterCriteria>> whatfilterCriteriaMap = accessManagerDAO.getWhatFilterCriteriaByRowCondition(parentRowConditionMap);
//            for (Map.Entry parentRowConditionEntry : parentRowConditionMap.entrySet()){
//                rowConditionEntry.getValue().
//            }
//        }
//        if (parentRowIntermediateConditions != null)
//            Map<String, RowCondition> rowConditionMap = accessManagerDAO.getRowConditions(parentRowIntermediateConditions, metadataVOX.getDomainObjectCode());
        Map<String, RowCondition> rowConditionMap = accessManagerDAO.getRowConditions(globalVariables.mtPE);
        Map<String, List<AMWhatFilterCriteria>> whatFilterCriteriaMap = accessManagerDAO.getWhatFilterCriteriaByRowCondition(rowConditionMap);
        deleteExistingPermissionsForData(globalVariables, whatFilterCriteriaMap);//optimize
        Map<String, RowCondition> rowConditionQualified = new HashMap<>();
        if (whatFilterCriteriaMap == null || whatFilterCriteriaMap.isEmpty()){
            rowConditionQualified.putAll(rowConditionMap);
        } else {
            for (Map.Entry<String, RowCondition> rowConditionEntry : rowConditionMap.entrySet()) {
                RowCondition rowCondition = rowConditionEntry.getValue();
                Set<String> controlAttributePathExpnPassedSet = new HashSet<>();
                Set<String> controlAttributePathExpnFailedSet = new HashSet<>();
                List<AMWhatFilterCriteria> whatFilterCriteriaList = whatFilterCriteriaMap.get(rowCondition.getRowCondnPkId());
                if(whatFilterCriteriaList == null || whatFilterCriteriaList.isEmpty()) {
                    rowConditionQualified.put(rowCondition.getRowCondnPkId(), rowCondition);
                }else{
                    for (AMWhatFilterCriteria amWhatFilterCriteria : whatFilterCriteriaList) {
                        if (!controlAttributePathExpnPassedSet.contains(amWhatFilterCriteria.getControlAttributePathExpn())) {
                            String value = (String) globalVariables.insertData.getData().get(amWhatFilterCriteria.getControlAttributePathExpn());
                            String expression = value + " " + amWhatFilterCriteria.getOperFkId() + " " + amWhatFilterCriteria.getValueExpn();
                            Evaluator evaluator = new Evaluator();
                            try {
                                if (evaluator.getBooleanResult(expression)) {
                                    controlAttributePathExpnPassedSet.add(amWhatFilterCriteria.getControlAttributePathExpn());
                                    controlAttributePathExpnFailedSet.remove(amWhatFilterCriteria.getControlAttributePathExpn());
                                } else
                                    controlAttributePathExpnFailedSet.add(amWhatFilterCriteria.getControlAttributePathExpn());
                            } catch (EvaluationException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    if (controlAttributePathExpnFailedSet.isEmpty()) {
                        rowConditionQualified.put(rowConditionEntry.getKey(), rowConditionMap.get(rowConditionEntry.getKey()));
                    }
                }
            }
        }
//        Map<String, RowCondition> rowConditionQualifiedFiltered = new HashMap<>();
//        // This condition checks if it is a base row condition
//        if (parentRowConditionMap == null || parentRowConditionMap.isEmpty()) {
//            rowConditionQualifiedFiltered.putAll(rowConditionQualified);
//        }
//        else {
//            for (Map.Entry<String, RowCondition> rowConditionEntry : rowConditionQualified.entrySet()) {
//                if (parentRowConditionMap.containsKey(rowConditionEntry.getValue().getParentRowCondnFkId())) {
//                    rowConditionQualifiedFiltered.put(rowConditionEntry.getKey(), rowConditionEntry.getValue());
//                }
//            }
//        }
//        Map<String, List<String>> whoGroupToPersonIdMap = new HashMap<>();
//        Map<String, List<String>> rowConditionToPersonId = new HashMap<>();
//        Map<String, Map<String, GroupVO>> rowConditionToWhoGroupMap = accessManagerDAO.getWhoGroup(rowConditionQualified);
//        for (Map.Entry<String, Map<String, GroupVO>> rowConditionToWhoGroupEntry : rowConditionToWhoGroupMap.entrySet()){
//            for (Map.Entry<String, GroupVO> whoGroupEntry : rowConditionToWhoGroupEntry.getValue().entrySet()){
//                if (whoGroupToPersonIdMap.containsKey(whoGroupEntry.getKey())){
//                    if (rowConditionToPersonId.containsKey(rowConditionToWhoGroupEntry.getKey())) {
//                        rowConditionToPersonId.get(rowConditionToWhoGroupEntry.getKey()).addAll(whoGroupToPersonIdMap.get(whoGroupEntry.getKey()));
//                    }else {
//                        rowConditionToPersonId.put(rowConditionToWhoGroupEntry.getKey(), whoGroupToPersonIdMap.get(whoGroupEntry.getKey()));
//                    }
//                }else {
//                    List<String> PersonIdList = accessManagerDAO.getPersonIdListForWhoGroup(whoGroupEntry.getKey());
//                    if (rowConditionToPersonId.containsKey(rowConditionToWhoGroupEntry.getKey())) {
//                        rowConditionToPersonId.get(rowConditionToWhoGroupEntry.getKey()).addAll(PersonIdList);
//                    }else {
//                        rowConditionToPersonId.put(rowConditionToWhoGroupEntry.getKey(), PersonIdList);
//                    }
//                }
//            }
//        }

        String whatDbPrimaryKey = (String) globalVariables.insertData.getData().get(metadataVOX.getVersoinIdAttribute().getDoAttributeCode());
//        String whoseSubjectUserPersonId = null;
//        accessManagerDAO.populateRowIntermediateForBasePE(rowConditionQualified, whatDbPrimaryKey, dependencyKey, whoseSubjectUserId);

//        accessManagerDAO.populateRowIntermediateForChildPE(rowConditionQualified, whatDbPrimaryKey, dependencyKey, whoseSubjectUserId);
        ETLMetaHelperVO helperVO = getEtlMetaHelperByDOName(mtPE.getMtDomainObjectCodeFkId());;
        String intermediateTableName = helperVO.getIntermediateTableName();
        String primaryKeyColumnName = helperVO.getPrimaryKeyColumnName();
        String rowResolvedTable = helperVO.getRowResolvedTable();
        accessManagerDAO.deleteExistingPermissionsFromRecord(whatDbPrimaryKey, helperVO.getIntermediateTableName(), helperVO.getRowResolvedTable(), null);
//        if (mtPE.getParentMtProcessElementCodeFkId() == null){
        accessManagerDAO.populateRowIntermediateForBasePETableByRecord(rowConditionQualified, whatDbPrimaryKey, whoseSubjectUserId, intermediateTableName);
        accessManagerDAO.populateRowResolvedByRecords(whatDbPrimaryKey, helperVO, primaryKeyColumnName);
//            accessManagerDAO.populateRowResolvedByRecords(rowConditionMap, whatDbPrimaryKey, whoseSubjectUserId, rowResolvedTable, primaryKeyColumnName);
//        }
//        else {
//            String parentWhatDbPrimaryKey = (String) globalVariables.insertData.getData().get(globalVariables.entityMetadataVOX.getDependencyKeyAttribute().getDoAttributeCode());
//            AMRowResolved parentAMAmRowResolved = accessManagerDAO.getAMRowResolvedForParentPE(parentWhatDbPrimaryKey, globalVariables.entityMetadataVOX.getDependencyKeyAttribute(), whoseSubjectUserId);
//            if (parentAMAmRowResolved.isReadCurrentPermitted()) {
//                accessManagerDAO.populateRowIntermediateForChildPE(rowConditionQualified, whatDbPrimaryKey, dependencyKey, whoseSubjectUserId);
//            }
//        }
//        accessManagerDAO.populateRowResolvedTable(globalVariables.currentTimestampString);
//        Person person = TenantAwareCache.getPersonRepository().getPersonDetails(TenantContextHolder.)
    }

    private ETLMetaHelperVO getEtlMetaHelperByDOName(String mtDomainObjectCodeFkId) {
        ETLMetaHelperVO etlMetaHelperVO = accessManagerDAO.getAccessControlGovernedDOByDOName(mtDomainObjectCodeFkId);
        return etlMetaHelperVO;
    }

    @Override
    public Map<String, RowCondition> getRowConditions(String mtPE) {
        Map<String, RowCondition> rowConditions = accessManagerDAO.getRowConditions(mtPE);
        return rowConditions;
    }


    @Override
    public void populateRowIntermediateForBasePETable(Map<String, RowCondition> rowConditionMap, String subjectUserColumnName, String primaryKeyColumnName, ETLMetaHelperVO etlMetaHelperVO) throws SQLException {
//        for (String dbPrimaryKey : whatDbPrimaryKeyList){
            accessManagerDAO.populateRowIntermediateForBasePETable(rowConditionMap, subjectUserColumnName, primaryKeyColumnName, etlMetaHelperVO);
//        }
    }

    @Override
    public void populateRowIntermediateForChildPETable(Map<String, RowCondition> rowConditionMap, String subjectUserColumnName, String primaryKeyColumnName, ETLMetaHelperVO etlMetaHelperVO) throws SQLException {
//        for (String dbPrimaryKey : whatDbPrimaryKeyList){
            accessManagerDAO.populateRowIntermediateForChildPETable(rowConditionMap, subjectUserColumnName, primaryKeyColumnName, etlMetaHelperVO);
//        }
    }

    @Override
    public void updatePrimaryKeyForIntermediateTable(String primaryKeyColumnName, ETLMetaHelperVO helperVO) throws SQLException {
        accessManagerDAO.updatePrimaryKeyForIntermediateTable(primaryKeyColumnName, helperVO);
    }

    @Override
    public void populateRowIntermediateForChildPETableByRecords(Map<String, RowCondition> rowConditionMap, Map<String, Object> records, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException {
        for (Map.Entry<String, Object> entry : records.entrySet()) {
            accessManagerDAO.populateRowIntermediateForChildPETableByRecords(rowConditionMap, entry, helperVO, primaryKeyColumnName);
            accessManagerDAO.populateRowResolvedByRecords(entry.getKey(), helperVO, primaryKeyColumnName);
        }
    }

    @Override
    public void populateAccessControlForBasePETableByRecords(Map<String, RowCondition> rowConditionMap, Map<String, Object> records, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException {
        for (Map.Entry<String, Object> entry : records.entrySet()) {
//            accessManagerDAO.populateRowIntermediateForBasePETableByRecords(rowConditionMap, entry, helperVO, primaryKeyColumnName);
            accessManagerDAO.populateRowIntermediateForBasePETableByRecord(rowConditionMap, entry.getKey(), entry.getValue(), helperVO.getIntermediateTableName());
            accessManagerDAO.populateRowResolvedByRecords(entry.getKey(), helperVO, primaryKeyColumnName);
//            accessManagerDAO.populateColumnResolvedByRecord(entry, helperVO, primaryKeyColumnName);
        }
    }

    @Override
    public Permission getEffectiveDatedPermissionForPerson(String mtPE) {
        Map<String, RowCondition> rowConditions;
        PersonGroup whoGroup = TenantAwareCache.getPersonRepository().getPersonGroups(TenantContextHolder.getUserContext().getPersonId());
        rowConditions = getRowConditionsFor(whoGroup, mtPE);
        accessManagerDAO.setColumnConditions(rowConditions);
        Map<String, DOAPermission> attributePermission = new HashMap<>();
        MasterEntityMetadataVOX masterEntityMetadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(mtPE);
        masterEntityMetadataVOX.getAttributeNameMap().keySet().forEach(p -> attributePermission.put(p, null));
        for (Map.Entry<String, RowCondition> entry : rowConditions.entrySet()){
            for (AMColCondition amColCondition : entry.getValue().getColumnConditionList()){
                DOAPermission doaPermission = attributePermission.get(amColCondition.getWhatDoaFkId());
                if (doaPermission == null){
                    doaPermission = new DOAPermission();
                }
                if (doaPermission.getCreateCurrentPermitted() != true)
                    doaPermission.setCreateCurrentPermitted(amColCondition.isCreateCurrentPermitted());
                if (doaPermission.getCreateHistoryPermitted() != true)
                    doaPermission.setCreateHistoryPermitted(amColCondition.isCreateHistoryPermitted());
                if (doaPermission.getCreateFuturePermitted() != true)
                    doaPermission.setCreateFuturePermitted(amColCondition.isCreateFuturePermitted());

                if (doaPermission.getReadCurrentPermitted() != true)
                    doaPermission.setReadCurrentPermitted(amColCondition.isReadCurrentPermitted());
                if (doaPermission.getReadHistoryPermitted() != true)
                    doaPermission.setReadHistoryPermitted(amColCondition.isReadHistoryPermitted());
                if (doaPermission.getReadFuturePermitted() != true)
                    doaPermission.setReadFuturePermitted(amColCondition.isReadFuturePermitted());

                if (doaPermission.getUpdateCurrentPermitted() != true)
                    doaPermission.setUpdateCurrentPermitted(amColCondition.isUpdateCurrentPermitted());
                if (doaPermission.getUpdateHistoryPermitted() != true)
                    doaPermission.setUpdateHistoryPermitted(amColCondition.isUpdateHistoryPermitted());
                if (doaPermission.getUpdateFuturePermitted() != true)
                    doaPermission.setUpdateFuturePermitted(amColCondition.isUpdateFuturePermitted());

                attributePermission.put(amColCondition.getWhatDoaFkId(), doaPermission);
            }
        }
        Permission permission = new Permission();
        permission.setAttributePermission(attributePermission);
        return permission;
    }


    public String getSubjectUser(String subjectUserDrivenDO, String pk) {
        String subjectUserId = null;
        ETLMetaHelperVO helperVO = accessManagerDAO.getMetaByDoName(subjectUserDrivenDO);
        String subjectUserDOA = helperVO.getSubjectUserPath().substring(2, helperVO.getSubjectUserPath().length()-1);
        StringBuilder SQL = new StringBuilder("SELECT ");
        StringBuilder FROMSQL = new StringBuilder(" FROM ");
        StringBuilder WHERESQL = new StringBuilder(" WHERE ");
        // get the meta data of all the doas.

        // Now get the actual column
        String[] doas = subjectUserDOA.split(":");
        DOADetails previousDOADetails = null;
        for(int i = 0 ;i< doas.length; i++){
            DOADetails doaDetails = accessManagerDAO.getDOADetailsByDOAName(doas[i]);
            //This is the last doa.
            if(i == 0){
                // build here the from clause
                FROMSQL.append(doaDetails.getTableName()).append(" ");
                previousDOADetails = doaDetails;
                WHERESQL.append(helperVO.getTableName()+"."+helperVO.getPrimaryKeyColumnName()).append("=");
                if(helperVO.getPrimaryKeyDBDataType().equals("T_ID")){
                    WHERESQL.append("0x").append(pk);
                }else{
                    WHERESQL.append("'"+pk+"'");
                }
            }else{
                FROMSQL.append(" INNER JOIN ").append(doaDetails.getTableName()).append(" ON ").append(previousDOADetails.getTableName()).append(".").append(previousDOADetails.getColumnName());
                // Get the current dos PK.
                ETLMetaHelperVO currentDO = accessManagerDAO.getMetaByDoName(doaDetails.getDoCode());
                FROMSQL.append(" = ").append(doaDetails.getTableName()).append(".").append(currentDO.getPrimaryKeyColumnName());
                // build here the join clause
            }
            if(i == doas.length-1) {
                // build here the select clause
                SQL.append(doaDetails.getTableName() + "." + doaDetails.getColumnName());
            }
        }
        SQL.append(FROMSQL).append(WHERESQL);
        subjectUserId = accessManagerDAO.getSubjectUserId(SQL, helperVO.getPrimaryKeyDBDataType());
        return subjectUserId;
    }


    private void deleteExistingPermissionsForData(GlobalVariables globalVariables, Map<String, List<AMWhatFilterCriteria>> whatFilterCriteriaMap) throws SQLException {
        if (whatFilterCriteriaMap == null)
            return;
        EntityMetadataVOX metadataVOX = globalVariables.entityMetadataVOX;
        MasterEntityMetadataVOX masterEntityMetadataVOX = globalVariables.masterEntityMetadataVOX;
        MasterEntityMetadataVOX rowIntermidateMeta = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(masterEntityMetadataVOX.getRowIntermediateMtPE());
        MasterEntityMetadataVOX rowResolvedMeta = TenantAwareCache.getMetaDataRepository().getMetadataByMtPE(masterEntityMetadataVOX.getRowResolvedMtPE());
        String rowIntermediateTable = rowIntermidateMeta.getDbTableName();
        String rowResolvedTable = rowResolvedMeta.getDbTableName();
        String whatDBPrimaryKeyDoa = metadataVOX.getFunctionalPrimaryKeyAttribute().getDoAttributeCode();
        String whatDBPrimaryKey = (String) globalVariables.insertData.getData().get(whatDBPrimaryKeyDoa);
        Map<String, Object> changedFields = globalVariables.changedFields;
        List<String> rowConditionsToDelete = new ArrayList<>();
        for (Map.Entry<String, List<AMWhatFilterCriteria>> entry : whatFilterCriteriaMap.entrySet()){
            for (AMWhatFilterCriteria amWhatFilterCriteria : entry.getValue()){
                if (changedFields.containsKey(amWhatFilterCriteria.getControlAttributePathExpn().replaceAll("[(!{)|}]",""))){
                    rowConditionsToDelete.add(entry.getKey());
                    break;
                }
            }
        }
        accessManagerDAO.deleteExistingPermissionsFromRecord(whatDBPrimaryKey, rowIntermediateTable, rowResolvedTable, rowConditionsToDelete);
    }

    private boolean evaluateExpression(String whatDOExpression, Evaluator evaluator, EntityMetadataVOX entityMetadataVOX) {
        if (whatDOExpression == null)
            return true;
        String modifiedExpression = Util.setStringValuesForExpression(whatDOExpression, evaluator.getVariables(), evaluator.getQuoteCharacter(), entityMetadataVOX.getAttributeMap());
        try {
            String rowConditionSatisfy = evaluator.evaluate(modifiedExpression);
            if (rowConditionSatisfy.equals("1.0"))
                return true;
            else return false;
        } catch (EvaluationException e) {
            e.printStackTrace();
        }
        return false;
    }

    private PersonGroup getWhoseGroupFkId(Map<String, Object> data, String domainObjectCode, String subjectUserId) {
        PersonGroup whoseGroup = TenantAwareCache.getPersonRepository().getPersonGroups(subjectUserId);
        return whoseGroup;
    }

    public AccessManagerDAO getAccessManagerDAO() {
        return accessManagerDAO;
    }

    public void setAccessManagerDAO(AccessManagerDAO accessManagerDAO) {
        this.accessManagerDAO = accessManagerDAO;
    }
}
