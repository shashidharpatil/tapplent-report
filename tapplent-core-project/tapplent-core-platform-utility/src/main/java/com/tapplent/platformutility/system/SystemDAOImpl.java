package com.tapplent.platformutility.system;

import com.tapplent.platformutility.g11n.structure.OEMObject;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.tapplent.platformutility.common.util.Util.convertByteToString;

public class SystemDAOImpl extends TapplentBaseDAO implements SystemDAO {
    @Override
    public Map<String, OEMObject> getOemObjectMap() {
        String SQL = "SELECT * FROM T_SYS_TAP_OEM_CRED;";
        List<Object> parameters = new ArrayList<>();
        ResultSet rs = executeSQL(SQL, parameters);
        Map<String, OEMObject> oemObjectMap = new HashMap<>();
        try{
            while (rs.next()){
                OEMObject oemObject = new OEMObject();
                oemObject.setVersionId(convertByteToString(rs.getBytes("VERSION_ID")));
                oemObject.setOemCredPkId(rs.getString("OEM_CRED_CODE_PK_ID"));
                oemObject.setOemName(rs.getString("OEM_NAME_TXT"));
                oemObject.setOemAppId(rs.getString("OEM_APPID_LNG_TXT"));
                oemObject.setOemKey(rs.getString("OEM_KEY_LNG_TXT"));
                oemObjectMap.put(oemObject.getOemCredPkId(), oemObject);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return oemObjectMap;
    }

    @Override
    public Map<String, Map<String, String>> getOemToLocaleLangMap() {
        String sql = "SELECT * FROM T_SYS_TAP_LCL_LANG_TRNSLT_MAP;";
        List<Object> parameters = new ArrayList<>();
        Map<String ,Map<String,String>> oemToLocaleLangMap =new HashMap<>();
        ResultSet rs = executeSQL(sql, parameters);
        try {
            while (rs.next()){
                String oemName = rs.getString("OEM_NAME_TXT");
                String localeCode = rs.getString("LOCALE_CODE_FK_ID");
                String langCode = rs.getString("LANG_MAP_TXT");
                if (oemToLocaleLangMap.containsKey(oemName)){
                    oemToLocaleLangMap.get(oemName).put(localeCode, langCode);
                }
                else {
                    Map<String, String> localToLangMap = new HashMap<>();
                    localToLangMap.put(localeCode, langCode);
                    oemToLocaleLangMap.put(oemName, localToLangMap);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oemToLocaleLangMap;
    }
}