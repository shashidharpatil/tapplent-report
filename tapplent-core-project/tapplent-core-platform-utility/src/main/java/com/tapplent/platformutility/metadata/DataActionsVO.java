/**
 * 
 */
package com.tapplent.platformutility.metadata;

/**
 * @author Shubham Patodi
 *
 */
public class DataActionsVO {
	private String dataDrivenActionsId;
//	private String launchMode;
//	private String recordState;
	private String activeState;
	private String saveState;
	private boolean isDeleted;
	private String workFlowTransactionStatusCode;
	private String actionCode;
	private boolean isApplicable;
	public String getDataDrivenActionsId() {
		return dataDrivenActionsId;
	}
	public void setDataDrivenActionsId(String dataDrivenActionsId) {
		this.dataDrivenActionsId = dataDrivenActionsId;
	}
	public String getActiveState() {
		return activeState;
	}
	public void setActiveState(String activeState) {
		this.activeState = activeState;
	}
	public String getSaveState() {
		return saveState;
	}
	public void setSaveState(String saveState) {
		this.saveState = saveState;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getWorkFlowTransactionStatusCode() {
		return workFlowTransactionStatusCode;
	}
	public void setWorkFlowTransactionStatusCode(String workFlowTransactionStatusCode) {
		this.workFlowTransactionStatusCode = workFlowTransactionStatusCode;
	}
	public String getActionCode() {
		return actionCode;
	}
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public boolean isApplicable() {
		return isApplicable;
	}
	public void setApplicable(boolean isApplicable) {
		this.isApplicable = isApplicable;
	}
}
