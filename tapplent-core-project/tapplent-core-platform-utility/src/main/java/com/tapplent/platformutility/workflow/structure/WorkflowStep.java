package com.tapplent.platformutility.workflow.structure;

import com.tapplent.platformutility.common.util.Common;
import org.springframework.beans.BeanUtils;

import java.sql.Timestamp;
import java.util.Map;

/**
 * Created by Manas on 8/31/16.
 */
public class WorkflowStep {
    private String workflowTxnStepPkId;
    private String workflowTxnFkId;
    private String workflowStepDefnFkId;
    private Timestamp stepActualStartDatetime;
    private Timestamp stepActualEndDatetime;
    private String stepStatusCodeFkId;
    private int stepSeqNumPosInt;
    private String workflowStepNameG11nBigTxt;
    private String workflowStepIconCodeFkId;
    private String startDateExpn;
    private String dueDateExpn;
    private boolean isStartDateEnforced;
    private boolean isDueDateEnforced;
    private String stepActorResolutionCodeFkId;
    private boolean allowTrgtActrRecVsbltyWhnEntryCritNotFulfilled;
    private boolean isBeginStep;
    private boolean isEndStep;
    private String onStepCompletionExpn;
    private String onStepCancellationExpn;
    private Timestamp createdDatetime;
    private Timestamp lastModifiedDatetime;
    private Map<String, WorkflowActor> associatedActorMap;
    private Map<String, WorkflowActorAction> associatedActorActionMap;

    public String getWorkflowTxnStepPkId() {
        return workflowTxnStepPkId;
    }

    public void setWorkflowTxnStepPkId(String workflowTxnStepPkId) {
        this.workflowTxnStepPkId = workflowTxnStepPkId;
    }

    public String getWorkflowTxnFkId() {
		return workflowTxnFkId;
	}

	public void setWorkflowTxnFkId(String workflowTxnFkId) {
		this.workflowTxnFkId = workflowTxnFkId;
	}

	public String getWorkflowStepDefnFkId() {
        return workflowStepDefnFkId;
    }

    public void setWorkflowStepDefnFkId(String workflowStepDefnFkId) {
        this.workflowStepDefnFkId = workflowStepDefnFkId;
    }

    public Timestamp getStepActualStartDatetime() {
        return stepActualStartDatetime;
    }

    public void setStepActualStartDatetime(Timestamp stepActualStartDatetime) {
        this.stepActualStartDatetime = stepActualStartDatetime;
    }

    public Timestamp getStepActualEndDatetime() {
        return stepActualEndDatetime;
    }

    public void setStepActualEndDatetime(Timestamp stepActualEndDatetime) {
        this.stepActualEndDatetime = stepActualEndDatetime;
    }

    public String getStepStatusCodeFkId() {
        return stepStatusCodeFkId;
    }

    public void setStepStatusCodeFkId(String stepStatusCodeFkId) {
        this.stepStatusCodeFkId = stepStatusCodeFkId;
    }

    public int getStepSeqNumPosInt() {
        return stepSeqNumPosInt;
    }

    public void setStepSeqNumPosInt(int stepSeqNumPosInt) {
        this.stepSeqNumPosInt = stepSeqNumPosInt;
    }

    public String getWorkflowStepNameG11nBigTxt() {
		return workflowStepNameG11nBigTxt;
	}

	public void setWorkflowStepNameG11nBigTxt(String workflowStepNameG11nBigTxt) {
		this.workflowStepNameG11nBigTxt = workflowStepNameG11nBigTxt;
	}

	public String getWorkflowStepIconCodeFkId() {
		return workflowStepIconCodeFkId;
	}

	public void setWorkflowStepIconCodeFkId(String workflowStepIconCodeFkId) {
		this.workflowStepIconCodeFkId = workflowStepIconCodeFkId;
	}

	public String getStartDateExpn() {
		return startDateExpn;
	}

	public void setStartDateExpn(String startDateExpn) {
		this.startDateExpn = startDateExpn;
	}

	public String getDueDateExpn() {
		return dueDateExpn;
	}

	public void setDueDateExpn(String dueDateExpn) {
		this.dueDateExpn = dueDateExpn;
	}

	public boolean isStartDateEnforced() {
        return isStartDateEnforced;
    }

    public void setStartDateEnforced(boolean startDateEnforced) {
        isStartDateEnforced = startDateEnforced;
    }

    public boolean isDueDateEnforced() {
        return isDueDateEnforced;
    }

    public void setDueDateEnforced(boolean dueDateEnforced) {
        isDueDateEnforced = dueDateEnforced;
    }

    public String getStepActorResolutionCodeFkId() {
        return stepActorResolutionCodeFkId;
    }

    public void setStepActorResolutionCodeFkId(String stepActorResolutionCodeFkId) {
        this.stepActorResolutionCodeFkId = stepActorResolutionCodeFkId;
    }

    public boolean isAllowTrgtActrRecVsbltyWhnEntryCritNotFulfilled() {
		return allowTrgtActrRecVsbltyWhnEntryCritNotFulfilled;
	}

	public void setAllowTrgtActrRecVsbltyWhnEntryCritNotFulfilled(boolean allowTrgtActrRecVsbltyWhnEntryCritNotFulfilled) {
		this.allowTrgtActrRecVsbltyWhnEntryCritNotFulfilled = allowTrgtActrRecVsbltyWhnEntryCritNotFulfilled;
	}

	public boolean isBeginStep() {
		return isBeginStep;
	}

	public void setBeginStep(boolean isBeginStep) {
		this.isBeginStep = isBeginStep;
	}

	public boolean isEndStep() {
		return isEndStep;
	}

	public void setEndStep(boolean isEndStep) {
		this.isEndStep = isEndStep;
	}

	public String getOnStepCompletionExpn() {
		return onStepCompletionExpn;
	}

	public void setOnStepCompletionExpn(String onStepCompletionExpn) {
		this.onStepCompletionExpn = onStepCompletionExpn;
	}

	public String getOnStepCancellationExpn() {
		return onStepCancellationExpn;
	}

	public void setOnStepCancellationExpn(String onStepCancellationExpn) {
		this.onStepCancellationExpn = onStepCancellationExpn;
	}

	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}

	public void setLastModifiedDatetime(Timestamp lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}

	public Map<String, WorkflowActor> getAssociatedActorMap() {
        return associatedActorMap;
    }

    public void setAssociatedActorMap(Map<String, WorkflowActor> associatedActorMap) {
        this.associatedActorMap = associatedActorMap;
    }

    public Map<String, WorkflowActorAction> getAssociatedActorActionMap() {
        return associatedActorActionMap;
    }

    public void setAssociatedActorActionMap(Map<String, WorkflowActorAction> associatedActorActionMap) {
        this.associatedActorActionMap = associatedActorActionMap;
    }

    public static WorkflowStep fromDefinition(WorkflowStepDefn workflowStepDefn) {
        WorkflowStep workflowStep = new WorkflowStep();
        BeanUtils.copyProperties(workflowStepDefn, workflowStep);
        workflowStep.setWorkflowTxnStepPkId(Common.getUUID());
        workflowStep.setWorkflowStepDefnFkId(workflowStepDefn.getWorkflowStepDefnPkId());
        workflowStep.setStepActorResolutionCodeFkId(workflowStepDefn.getStepActorResolutionCodeFkId());
        workflowStep.setBeginStep(workflowStepDefn.isBeginStep());
        return workflowStep;
    }
}
