package com.tapplent.platformutility.expression.jeval.function.string;

import com.tapplent.platformutility.expression.jeval.EvaluationConstants;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.expression.jeval.function.Function;
import com.tapplent.platformutility.expression.jeval.function.FunctionConstants;
import com.tapplent.platformutility.expression.jeval.function.FunctionException;
import com.tapplent.platformutility.expression.jeval.function.FunctionHelper;
import com.tapplent.platformutility.expression.jeval.function.FunctionResult;

import java.util.ArrayList;

/**
 * Created by sripad on 13/01/16.
 */
public class Text implements Function {

    /**
     * Returns the name of the function - "text".
     *
     * @return The name of this function class.
     */
    public String getName() {
        return "text";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     *
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *           argument is a value to be converted into String
     *
     * @return Returns the specified substring.
     *
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    public FunctionResult execute(final Evaluator evaluator, final String arguments)
            throws FunctionException {
        String result = null;
        String exceptionMessage = "One string argument  "
                + "argument is required.";

        ArrayList values = FunctionHelper.getStrings(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);

        if (values.size() != 1) {
            throw new FunctionException(exceptionMessage);
        }

        try {
            ArrayList<String> argumentOne = FunctionHelper.getStrings(arguments, EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);
            result = argumentOne.get(0).toString();
        } catch (FunctionException fe) {
            throw new FunctionException(fe.getMessage(), fe);
        } catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result,
                FunctionConstants.FUNCTION_RESULT_TYPE_STRING);
    }
}
