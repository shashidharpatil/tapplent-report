package com.tapplent.platformutility.sql.statement.builder;

import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadatamap.MetaDataMap;
import com.tapplent.platformutility.sql.context.model.InsertContextModel;
import com.tapplent.platformutility.sql.context.model.SQLContextModel;

public class DeleteStatementBuilder implements SQLStatementBuilder {
	
	private final InsertContextModel contextModel;
	private EntityMetadataVOX entityMetadataVOX;

	public DeleteStatementBuilder(SQLContextModel contextModel, EntityMetadataVOX entityMetadataVOX) {
		this.entityMetadataVOX = entityMetadataVOX;
		this.contextModel = (InsertContextModel) contextModel;
	}
	@Override
	public String build() {
		SQLMyBatisQueryBuilder queryBuilder = new SQLMyBatisQueryBuilder();
		queryBuilder.addDelete(contextModel.getEntityName());
		queryBuilder.addWhere(contextModel.getWhereExpression());
		return queryBuilder.getQueryString();
	}

}
