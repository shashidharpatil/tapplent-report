package com.tapplent.platformutility.common.util;

public class GoogleSheetsDetails {
	private String fileId;
	private int seqNumber;
	private String formatNumber;
	private String folderPath;
	private String title;
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public int getSeqNumber() {
		return seqNumber;
	}
	public void setSeqNumber(int seqNumber) {
		this.seqNumber = seqNumber;
	}
	public String getFormatNumber() {
		return formatNumber;
	}
	public void setFormatNumber(String formatNumber) {
		this.formatNumber = formatNumber;
	}
	public String getFolderPath() {
		return folderPath;
	}
	public void setFolderPath(String folderPath) {
		this.folderPath = folderPath;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
