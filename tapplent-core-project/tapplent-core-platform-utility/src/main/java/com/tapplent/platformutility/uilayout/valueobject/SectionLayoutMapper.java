package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 09/01/17.
 */
public class SectionLayoutMapper {
    private String uiSecLytMapPkId;
    private String mtPECodeFkId;
    private String uiDispTypeCodeFKId;
    private int tabSeqNumPosInt;
    private String tabHdrPropControlExpn;
    private String tabHdrStaticCntrlG11nBigTxt;
    private String targetScreenInstanceId;
    private String targetSecnInstInSeqLngTxt;

    public String getUiSecLytMapPkId() {
        return uiSecLytMapPkId;
    }

    public void setUiSecLytMapPkId(String uiSecLytMapPkId) {
        this.uiSecLytMapPkId = uiSecLytMapPkId;
    }

    public String getMtPECodeFkId() {
        return mtPECodeFkId;
    }

    public void setMtPECodeFkId(String mtPECodeFkId) {
        this.mtPECodeFkId = mtPECodeFkId;
    }

    public String getUiDispTypeCodeFKId() {
        return uiDispTypeCodeFKId;
    }

    public void setUiDispTypeCodeFKId(String uiDispTypeCodeFKId) {
        this.uiDispTypeCodeFKId = uiDispTypeCodeFKId;
    }

    public int getTabSeqNumPosInt() {
        return tabSeqNumPosInt;
    }

    public void setTabSeqNumPosInt(int tabSeqNumPosInt) {
        this.tabSeqNumPosInt = tabSeqNumPosInt;
    }

    public String getTargetSecnInstInSeqLngTxt() {
        return targetSecnInstInSeqLngTxt;
    }

    public void setTargetSecnInstInSeqLngTxt(String targetSecnInstInSeqLngTxt) {
        this.targetSecnInstInSeqLngTxt = targetSecnInstInSeqLngTxt;
    }

    public String getTargetScreenInstanceId() {
        return targetScreenInstanceId;
    }

    public void setTargetScreenInstanceId(String targetScreenInstanceId) {
        this.targetScreenInstanceId = targetScreenInstanceId;
    }

    public String getTabHdrPropControlExpn() {
        return tabHdrPropControlExpn;
    }

    public void setTabHdrPropControlExpn(String tabHdrPropControlExpn) {
        this.tabHdrPropControlExpn = tabHdrPropControlExpn;
    }

    public String getTabHdrStaticCntrlG11nBigTxt() {
        return tabHdrStaticCntrlG11nBigTxt;
    }

    public void setTabHdrStaticCntrlG11nBigTxt(String tabHdrStaticCntrlG11nBigTxt) {
        this.tabHdrStaticCntrlG11nBigTxt = tabHdrStaticCntrlG11nBigTxt;
    }
}
