package com.tapplent.platformutility.analytics.structure;

/**
 * Created by tapplent on 25/08/17.
 */
public class TimePeriodOffsetVO {
    private String orgLocPeriodOffsetPkID;
    private String calendarPeriodType;
    private String calendarPeriodName;
    private String calendarPeriodIcon;
    private int weekDaysOffset;
    private int monthOffset;

    public String getCalendarPeriodType() {
        return calendarPeriodType;
    }

    public void setCalendarPeriodType(String calendarPeriodType) {
        this.calendarPeriodType = calendarPeriodType;
    }

    public String getCalendarPeriodName() {
        return calendarPeriodName;
    }

    public void setCalendarPeriodName(String calendarPeriodName) {
        this.calendarPeriodName = calendarPeriodName;
    }

    public String getCalendarPeriodIcon() {
        return calendarPeriodIcon;
    }

    public void setCalendarPeriodIcon(String calendarPeriodIcon) {
        this.calendarPeriodIcon = calendarPeriodIcon;
    }

    public int getWeekDaysOffset() {
        return weekDaysOffset;
    }

    public void setWeekDaysOffset(int weekDaysOffset) {
        this.weekDaysOffset = weekDaysOffset;
    }

    public int getMonthOffset() {
        return monthOffset;
    }

    public void setMonthOffset(int monthOffset) {
        this.monthOffset = monthOffset;
    }

    public String getOrgLocPeriodOffsetPkID() {
        return orgLocPeriodOffsetPkID;
    }

    public void setOrgLocPeriodOffsetPkID(String orgLocPeriodOffsetPkID) {
        this.orgLocPeriodOffsetPkID = orgLocPeriodOffsetPkID;
    }
}
