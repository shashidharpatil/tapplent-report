package com.tapplent.platformutility.etl.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class EtlMapDetailsVO {
//	VERSION ID                                 
//	ETL MAP DETAILS PK ID                      
//	DETAILS MAP NAME G11N BLOB                 
//	ETL MAP DEP FK ID                          
//	ETL HEADER DEP CODE FK ID                  
//	TARGET DOA CODE FK ID                      
//	MAP SOURCE DOA EXPN                        
//	MAP LOOKUP SOURCE DO CODE FK ID            
//	MAP LOOKUP SOURCE DOA LNG TXT              
//	MAP LOOKUP RETURN SOURCE DOA CODE FK ID    
//	ASSOCIATED ARTEFACT LOCATION DOA CODE FK ID
	
//	MAP_LOOKUP_DOA_EXPN
	private String versionId;
	private String etlMapDetailsPkId; 
	private String detailsMapName;
	private String etlMapDepFkId; 
	private String etlHeaderDepFkId; 
	private String targetDoaCodeFkId; 
	private String mapSourceDoaExpn; 
//	private String mapLookupSourceDoCodeFkId; 
//	private String mapLookupSourceDoaLngTxt;
//	private String mapLookupReturnSourceDoaCodeFkId;
	private String mapLookupDoaExpn;
	private String associatedArtefactLocationDoaCodeFkId;
	private boolean propogateVerion;

	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getEtlMapDetailsPkId() {
		return etlMapDetailsPkId;
	}
	public void setEtlMapDetailsPkId(String etlMapDetailsPkId) {
		this.etlMapDetailsPkId = etlMapDetailsPkId;
	}

	public String getDetailsMapName() {
		return detailsMapName;
	}

	public void setDetailsMapName(String detailsMapName) {
		this.detailsMapName = detailsMapName;
	}

	public String getEtlMapDepFkId() {
		return etlMapDepFkId;
	}
	public void setEtlMapDepFkId(String etlMapDepFkId) {
		this.etlMapDepFkId = etlMapDepFkId;
	}
	public String getEtlHeaderDepFkId() {
		return etlHeaderDepFkId;
	}
	public void setEtlHeaderDepFkId(String etlHeaderDepFkId) {
		this.etlHeaderDepFkId = etlHeaderDepFkId;
	}
	public String getTargetDoaCodeFkId() {
		return targetDoaCodeFkId;
	}
	public void setTargetDoaCodeFkId(String targetDoaCodeFkId) {
		this.targetDoaCodeFkId = targetDoaCodeFkId;
	}
	public String getMapSourceDoaExpn() {
		return mapSourceDoaExpn;
	}
	public void setMapSourceDoaExpn(String mapSourceDoaExpn) {
		this.mapSourceDoaExpn = mapSourceDoaExpn;
	}
	public String getMapLookupDoaExpn() {
		return mapLookupDoaExpn;
	}
	public void setMapLookupDoaExpn(String mapLookupDoaExpn) {
		this.mapLookupDoaExpn = mapLookupDoaExpn;
	}
	public String getAssociatedArtefactLocationDoaCodeFkId() {
		return associatedArtefactLocationDoaCodeFkId;
	}
	public void setAssociatedArtefactLocationDoaCodeFkId(String associatedArtefactLocationDoaCodeFkId) {
		this.associatedArtefactLocationDoaCodeFkId = associatedArtefactLocationDoaCodeFkId;
	}
	public boolean isPropogateVerion() {
		return propogateVerion;
	}
	public void setPropogateVerion(boolean propogateVerion) {
		this.propogateVerion = propogateVerion;
	}
}
