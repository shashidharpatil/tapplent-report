package com.tapplent.platformutility.g11n.structure;

public class OEMObject {
    private String versionId;
    private String oemCredPkId;
    private String oemName;
    private String oemAppId;
    private String oemKey;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getOemCredPkId() {
        return oemCredPkId;
    }

    public void setOemCredPkId(String oemCredPkId) {
        this.oemCredPkId = oemCredPkId;
    }

    public String getOemName() {
        return oemName;
    }

    public void setOemName(String oemName) {
        this.oemName = oemName;
    }

    public String getOemAppId() {
        return oemAppId;
    }

    public void setOemAppId(String oemAppId) {
        this.oemAppId = oemAppId;
    }

    public String getOemKey() {
        return oemKey;
    }

    public void setOemKey(String oemKey) {
        this.oemKey = oemKey;
    }
}
