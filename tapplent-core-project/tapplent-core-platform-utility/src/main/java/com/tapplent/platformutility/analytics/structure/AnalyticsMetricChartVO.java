package com.tapplent.platformutility.analytics.structure;

/**
 * Created by tapplent on 23/08/17.
 */
public class AnalyticsMetricChartVO {
    private String chartPkID;
    private String metricInfo;
    private String metricFormula;
    private String bestPracticeRecommendation;
    private String chartTitle;
    private String timeXAxisTitle;
    private String xAxisTitle;
    private String yAxis1Title;
    private String yAxis2Title;
    private boolean isLegendVisible;
    private String legendDisplayPosition;
    private boolean isDrillDownable;
    private boolean isTableAvailable;
    private String parentAnalyticsMetricDetailID;

    public String getChartPkID() {
        return chartPkID;
    }

    public void setChartPkID(String chartPkID) {
        this.chartPkID = chartPkID;
    }

    public String getMetricInfo() {
        return metricInfo;
    }

    public void setMetricInfo(String metricInfo) {
        this.metricInfo = metricInfo;
    }

    public String getMetricFormula() {
        return metricFormula;
    }

    public void setMetricFormula(String metricFormula) {
        this.metricFormula = metricFormula;
    }

    public String getBestPracticeRecommendation() {
        return bestPracticeRecommendation;
    }

    public void setBestPracticeRecommendation(String bestPracticeRecommendation) {
        this.bestPracticeRecommendation = bestPracticeRecommendation;
    }

    public String getChartTitle() {
        return chartTitle;
    }

    public void setChartTitle(String chartTitle) {
        this.chartTitle = chartTitle;
    }

    public String getTimeXAxisTitle() {
        return timeXAxisTitle;
    }

    public void setTimeXAxisTitle(String timeXAxisTitle) {
        this.timeXAxisTitle = timeXAxisTitle;
    }

    public String getxAxisTitle() {
        return xAxisTitle;
    }

    public void setxAxisTitle(String xAxisTitle) {
        this.xAxisTitle = xAxisTitle;
    }

    public String getyAxis1Title() {
        return yAxis1Title;
    }

    public void setyAxis1Title(String yAxis1Title) {
        this.yAxis1Title = yAxis1Title;
    }

    public String getyAxis2Title() {
        return yAxis2Title;
    }

    public void setyAxis2Title(String yAxis2Title) {
        this.yAxis2Title = yAxis2Title;
    }

    public boolean isLegendVisible() {
        return isLegendVisible;
    }

    public void setLegendVisible(boolean legendVisible) {
        isLegendVisible = legendVisible;
    }

    public String getLegendDisplayPosition() {
        return legendDisplayPosition;
    }

    public void setLegendDisplayPosition(String legendDisplayPosition) {
        this.legendDisplayPosition = legendDisplayPosition;
    }

    public boolean isDrillDownable() {
        return isDrillDownable;
    }

    public void setDrillDownable(boolean drillDownable) {
        isDrillDownable = drillDownable;
    }

    public boolean isTableAvailable() {
        return isTableAvailable;
    }

    public void setTableAvailable(boolean tableAvailable) {
        isTableAvailable = tableAvailable;
    }

    public String getParentAnalyticsMetricDetailID() {
        return parentAnalyticsMetricDetailID;
    }

    public void setParentAnalyticsMetricDetailID(String parentAnalyticsMetricDetailID) {
        this.parentAnalyticsMetricDetailID = parentAnalyticsMetricDetailID;
    }
}
