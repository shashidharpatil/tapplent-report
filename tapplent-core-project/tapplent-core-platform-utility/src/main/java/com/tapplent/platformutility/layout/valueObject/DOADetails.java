package com.tapplent.platformutility.layout.valueObject;

public class DOADetails {
	private String doaName;
	private String columnName;
	private String containerBlobName;
	private String dbDataType;
	private String tableName;
	private String doCode;
	private boolean isFunctionalPrimaryKeyFlag;
	private boolean isRelationFlag;
	private boolean isLocalSearchableFlag;
	private String toTableName;
	private String toColumnName;
	public String getDoaName() {
		return doaName;
	}
	public void setDoaName(String doaName) {
		this.doaName = doaName;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getContainerBlobName() {
		return containerBlobName;
	}
	public void setContainerBlobName(String containerBlobName) {
		this.containerBlobName = containerBlobName;
	}
	public String getDbDataType() {
		return dbDataType;
	}
	public void setDbDataType(String dbDataType) {
		this.dbDataType = dbDataType;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public boolean isFunctionalPrimaryKeyFlag() {
		return isFunctionalPrimaryKeyFlag;
	}

	public void setFunctionalPrimaryKeyFlag(boolean functionalPrimaryKeyFlag) {
		isFunctionalPrimaryKeyFlag = functionalPrimaryKeyFlag;
	}

	public boolean isRelationFlag() {
		return isRelationFlag;
	}

	public void setRelationFlag(boolean relationFlag) {
		isRelationFlag = relationFlag;
	}

	public boolean isLocalSearchableFlag() {
		return isLocalSearchableFlag;
	}

	public void setLocalSearchableFlag(boolean localSearchableFlag) {
		isLocalSearchableFlag = localSearchableFlag;
	}

	public String getDoCode() {
		return doCode;
	}

	public void setDoCode(String doCode) {
		this.doCode = doCode;
	}

	public String getToTableName() {
		return toTableName;
	}

	public void setToTableName(String toTableName) {
		this.toTableName = toTableName;
	}

	public String getToColumnName() {
		return toColumnName;
	}

	public void setToColumnName(String toColumnName) {
		this.toColumnName = toColumnName;
	}
}
