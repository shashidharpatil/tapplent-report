package com.tapplent.platformutility.analytics.dao;

import com.tapplent.platformutility.analytics.structure.*;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 05/12/16.
 */
public class AnalyticsDAOImpl extends TapplentBaseDAO implements AnalyticsDAO{

    @Override
    public AnalyticsMetric getAnalyticMetric(String analyticsMetricId) {
        AnalyticsMetric analyticsMetric = null;
        String SQL = "SELECT * FROM T_PFM_ADM_ANALYTICS_METRIC WHERE ANALYTICS_METRIC_PK_ID = x?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(analyticsMetricId));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            if(rs.next()){
                analyticsMetric = new AnalyticsMetric();
                analyticsMetric.setAnalyticsMetricPkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_PK_ID")));
                analyticsMetric.setAnalyticsContextFkId(Util.convertByteToString(rs.getBytes("ANALYTICS_CONTEXT_FK_ID")));
                analyticsMetric.setAnalyticsMetricNameG11nBigTxt(getG11nValue(rs.getString("ANALYTICS_METRIC_NAME_G11N_BIG_TXT")));
                analyticsMetric.setIntroductionG11nBigTxt(getG11nValue(rs.getString("INTRODUCTION_G11N_BIG_TXT")));
                analyticsMetric.setSeqNumPosInt(rs.getInt("SEQ_NUM_POS_INT"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return analyticsMetric;
    }

    @Override
    public Map<String, AnalyticsMetricDetails> getAnalyticMetricDetails(String analyticsMetricId, String analysisTypeCode) {
        Map<String, AnalyticsMetricDetails> result = new HashMap<>();
        String SQL = null;
        if(analysisTypeCode.equals("TREND")){
            SQL = "SELECT * FROM T_PFM_ADM_ANALYTICS_METRIC_DETAIL WHERE ANALYTICS_METRIC_FK_ID = x?  AND ANALYSIS_TYPE_CODE_FK_ID IN ('TREND', 'PREDICTION') AND PARENT_DRILLDOWN_ANALYTICS_METRIC_DETAIL_FK_ID IS NULL;";
        }else if(analysisTypeCode.equals("ANALYSIS")){
            SQL = "SELECT * FROM T_PFM_ADM_ANALYTICS_METRIC_DETAIL WHERE ANALYSIS_TYPE_CODE_FK_ID IN ('ANALYSIS') AND PARENT_ANALYTICS_METRIC_DETAIL_FK_ID = x?;";
        }else if(analysisTypeCode.equals("TABLE")){

        }
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(analyticsMetricId));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while (rs.next()){
                AnalyticsMetricDetails analyticsMetricDetails = new AnalyticsMetricDetails();
                analyticsMetricDetails.setAnalyticsMetricDetailPkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_DETAIL_PK_ID")));
                analyticsMetricDetails.setAnalyticsMetricFkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_FK_ID")));
                analyticsMetricDetails.setAnalysisTypeCodeFkId(rs.getString("ANALYSIS_TYPE_CODE_FK_ID"));
                analyticsMetricDetails.setSeqNumPosInt(rs.getInt("SEQ_NUM_POS_INT"));
                analyticsMetricDetails.setHelpExplanationG11nBigTxt(getG11nValue(rs.getString("HELP_EXPLANATION_G11N_BIG_TXT")));
                analyticsMetricDetails.setChartTitleG11nBigTxt(getG11nValue(rs.getString("CHART_TITLE_G11N_BIG_TXT")));
                analyticsMetricDetails.setChartTypeCodeFkId(rs.getString("CHART_TYPE_CODE_FK_ID"));
                analyticsMetricDetails.setxAxizTitleG11nBigTxt(getG11nValue(rs.getString("X_AXIZ_TITLE_G11N_BIG_TXT")));
                analyticsMetricDetails.setyAxizTitle1G11nBigTxt(getG11nValue(rs.getString("Y_AXIZ_TITLE_1_G11N_BIG_TXT")));
                analyticsMetricDetails.setyAxizTitle1G11nBigTxt(getG11nValue(rs.getString("Y_AXIZ_TITLE_2_G11N_BIG_TXT")));
                analyticsMetricDetails.setLegendVisible(rs.getBoolean("IS_LEGEND_VISIBLE"));
                analyticsMetricDetails.setLegendPositionCodeFkId(rs.getString("LEGEND_POSITION_CODE_FK_ID"));
                analyticsMetricDetails.setParentAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("PARENT_ANALYTICS_METRIC_DETAIL_FK_ID")));
                analyticsMetricDetails.setParentDrilldownAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("PARENT_DRILLDOWN_ANALYTICS_METRIC_DETAIL_FK_ID")));
                analyticsMetricDetails.setDrillDownEnabled(rs.getBoolean("IS_DRILLDOWN_ENABLED"));
                analyticsMetricDetails.setTableAvailable(rs.getBoolean("IS_TABLE_AVAILABLE"));
//                analyticsMetricDetails.setRelatedMinBenchmarkAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("RELATED_MIN_BENCHMARK_ANALYTICS_METRIC_DETAIL_FK_ID")));
//                analyticsMetricDetails.setRelatedMidBenchmarkAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("RELATED_MID_BENCHMARK_ANALYTICS_METRIC_DETAIL_FK_ID")));
//                analyticsMetricDetails.setRelatedMaxBenchmarkAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("RELATED_MAX_BENCHMARK_ANALYTICS_METRIC_DETAIL_FK_ID")));
//                analyticsMetricDetails.setRelatedTabularAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("RELATED_TABULAR_ANALYTICS_METRIC_DETAIL_FK_ID")));
//                analyticsMetricDetails.setRelatedFilterReceipientDoaCodeFkId(rs.getString("RELATED_FILTER_RECEIPIENT_DOA_CODE_FK_ID"));
                result.put(analyticsMetricDetails.getAnalyticsMetricDetailPkId(), analyticsMetricDetails);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Map<String, AnalyticsMetricDetailsQuery> getAnalyticMetricDetailsQuery(String  analyticsMetricId) {
        Map<String, AnalyticsMetricDetailsQuery> result = new HashMap<>();
        String SQL = "SELECT * FROM T_PFM_ADM_ANALYTICS_METRIC_DETAIL_QUERY INNER JOIN T_PFM_ADM_ANALYTICS_METRIC_DETAIL ON ANALYTICS_METRIC_DETAIL_PK_ID = ANALYTICS_METRIC_DETAIL_FK_ID WHERE ANALYTICS_METRIC_FK_ID = x?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(analyticsMetricId));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while (rs.next()){
                AnalyticsMetricDetailsQuery analyticsMetricDetailsQuery = new AnalyticsMetricDetailsQuery();
                analyticsMetricDetailsQuery.setAnalyticsMetricDetailQueryPkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_DETAIL_QUERY_PK_ID")));
                analyticsMetricDetailsQuery.setAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_DETAIL_FK_ID")));
                analyticsMetricDetailsQuery.setMtPeCodeFkId(rs.getString("MT_PE_CODE_FK_ID"));
                analyticsMetricDetailsQuery.setMtPeSeqNumPosInt(rs.getInt("MT_PE_SEQ_NUM_POS_INT"));
                analyticsMetricDetailsQuery.setParentMtPeSeqNumPosInt(rs.getInt("PARENT_MT_PE_SEQ_NUM_POS_INT"));
                analyticsMetricDetailsQuery.setFkRelationWithParentLngTxt(rs.getString("FK_RELATION_WITH_PARENT_LNG_TXT"));
                analyticsMetricDetailsQuery.setAttrPathExpn(rs.getString("ATTR_PATH_EXPN"));
                analyticsMetricDetailsQuery.setAttrDispAxizCodeFkId(rs.getString("ATTR_DISP_AXIZ_CODE_FK_ID"));
                analyticsMetricDetailsQuery.setAttrDisplaySeqNumPosInt(rs.getInt("ATTR_DISPLAY_SEQ_NUM_POS_INT"));
                analyticsMetricDetailsQuery.setAggregateAttr(rs.getBoolean("IS_AGGREGATE_FUNCTION"));
                analyticsMetricDetailsQuery.setOrderBySeqNumPosInt(rs.getInt("ORDER_BY_SEQ_NUM_POS_INT"));
                analyticsMetricDetailsQuery.setOrderByModeCodeFkId(rs.getString("ORDER_BY_MODE_CODE_FK_ID"));
                analyticsMetricDetailsQuery.setOrderByValuesBigTxt(rs.getString("ORDER_BY_VALUES_BIG_TXT"));
                analyticsMetricDetailsQuery.setGroupByAttribute(rs.getBoolean("IS_GROUP_BY_ATTRIBUTE"));
                analyticsMetricDetailsQuery.setGroupBySeqNumPosInt(rs.getInt("GROUP_BY_SEQ_NUM_POS_INT"));
                analyticsMetricDetailsQuery.setGroupByHavingExpn(rs.getString("GROUP_BY_HAVING_EXPN"));
                analyticsMetricDetailsQuery.setClientMatch(rs.getBoolean("IS_CLIENT_MATCH"));
                analyticsMetricDetailsQuery.setClientDisplay(rs.getBoolean("IS_CLIENT_DISPLAY"));
//                analyticsMetricDetailsQuery.setUsedInFilter(rs.getBoolean("IS_USED_IN_FILTER"));
                result.put(analyticsMetricDetailsQuery.getAnalyticsMetricDetailQueryPkId(), analyticsMetricDetailsQuery);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<AnalyticsMetricDetailsFilter> getAnalyticMetricDetailsFilter(String  analyticsMetricId) {
        List<AnalyticsMetricDetailsFilter> result = new ArrayList<>();
        String SQL = "SELECT * FROM T_PFM_ADM_ANALYTICS_METRIC_DETAIL_FILTER INNER JOIN T_PFM_ADM_ANALYTICS_METRIC_DETAIL ON ANALYTICS_METRIC_DETAIL_PK_ID = ANALYTICS_METRIC_DETAIL_FK_ID WHERE ANALYTICS_METRIC_FK_ID = x?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(analyticsMetricId));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while (rs.next()){
                AnalyticsMetricDetailsFilter analyticsMetricDetailsFilter = new AnalyticsMetricDetailsFilter();
                analyticsMetricDetailsFilter.setAnalyticsMetricDetailFilterPkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_DETAIL_FILTER_PK_ID")));
                analyticsMetricDetailsFilter.setAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_DETAIL_FK_ID")));
                analyticsMetricDetailsFilter.setMtPeCodeFkId(rs.getString("MT_PE_CODE_FK_ID"));
                analyticsMetricDetailsFilter.setFilterExpn(rs.getString("FILTER_EXPN"));
                result.add(analyticsMetricDetailsFilter);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public AnalyticsMetricDetails getAnalyticsMetricDetails(String metricDetailsId, Boolean isDrillDown, Boolean isTable) {
        AnalyticsMetricDetails analyticsMetricDetails = null;
        String SQL = null;
        if(isDrillDown){
            SQL = "SELECT * FROM T_PFM_ADM_ANALYTICS_METRIC_DETAIL WHERE PARENT_DRILLDOWN_ANALYTICS_METRIC_DETAIL_FK_ID = x?";
        }else {
            SQL = "SELECT * FROM T_PFM_ADM_ANALYTICS_METRIC_DETAIL WHERE ANALYTICS_METRIC_DETAIL_PK_ID = x?;";
        }
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(metricDetailsId));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            if(rs.next()) {
                analyticsMetricDetails = new AnalyticsMetricDetails();
                analyticsMetricDetails.setAnalyticsMetricDetailPkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_DETAIL_PK_ID")));
                analyticsMetricDetails.setAnalyticsMetricFkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_FK_ID")));
                analyticsMetricDetails.setAnalysisTypeCodeFkId(rs.getString("ANALYSIS_TYPE_CODE_FK_ID"));
                analyticsMetricDetails.setSeqNumPosInt(rs.getInt("SEQ_NUM_POS_INT"));
                analyticsMetricDetails.setHelpExplanationG11nBigTxt(getG11nValue(rs.getString("HELP_EXPLANATION_G11N_BIG_TXT")));
                analyticsMetricDetails.setChartTitleG11nBigTxt(getG11nValue(rs.getString("CHART_TITLE_G11N_BIG_TXT")));
                analyticsMetricDetails.setChartTypeCodeFkId(rs.getString("CHART_TYPE_CODE_FK_ID"));
                analyticsMetricDetails.setxAxizTitleG11nBigTxt(getG11nValue(rs.getString("X_AXIZ_TITLE_G11N_BIG_TXT")));
                analyticsMetricDetails.setyAxizTitle1G11nBigTxt(getG11nValue(rs.getString("Y_AXIZ_TITLE_1_G11N_BIG_TXT")));
                analyticsMetricDetails.setyAxizTitle1G11nBigTxt(getG11nValue(rs.getString("Y_AXIZ_TITLE_2_G11N_BIG_TXT")));
                analyticsMetricDetails.setLegendVisible(rs.getBoolean("IS_LEGEND_VISIBLE"));
                analyticsMetricDetails.setDrillDownEnabled(rs.getBoolean("IS_DRILLDOWN_ENABLED"));
                analyticsMetricDetails.setTableAvailable(rs.getBoolean("IS_TABLE_AVAILABLE"));
                analyticsMetricDetails.setLegendPositionCodeFkId(rs.getString("LEGEND_POSITION_CODE_FK_ID"));
                analyticsMetricDetails.setParentAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("PARENT_ANALYTICS_METRIC_DETAIL_FK_ID")));
                analyticsMetricDetails.setParentDrilldownAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("PARENT_DRILLDOWN_ANALYTICS_METRIC_DETAIL_FK_ID")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        populateAnalyticMetricDetailsQuery(analyticsMetricDetails, isTable);
        populateAnalyticMetricDetailsFilter(analyticsMetricDetails);
        return analyticsMetricDetails;
    }

    @Override
    public List<String> getAnalysisMetricDetailsPks(String parentAnalysisMetricDetailId) {
        List<String> result = new ArrayList<>();
        String SQL = "SELECT ANALYTICS_METRIC_DETAIL_PK_ID FROM T_PFM_ADM_ANALYTICS_METRIC_DETAIL WHERE PARENT_ANALYTICS_METRIC_DETAIL_FK_ID = x?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(parentAnalysisMetricDetailId));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while (rs.next()){
                result.add(Util.convertByteToString(rs.getBytes(1)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    private void populateAnalyticMetricDetailsQuery(AnalyticsMetricDetails metricDetails, Boolean isTable){
        if(metricDetails!= null){
            String SQL = null;
            if(isTable){
                SQL = "SELECT * FROM T_PFM_ADM_ANALYTICS_METRIC_DETAIL_QUERY WHERE ANALYTICS_METRIC_DETAIL_FK_ID = x? AND ATTR_DISP_AXIZ_CODE_FK_ID = 'TABLE_AXIS';";
            }else {
                SQL = "SELECT * FROM T_PFM_ADM_ANALYTICS_METRIC_DETAIL_QUERY WHERE ANALYTICS_METRIC_DETAIL_FK_ID = x?;";
            }
            List<Object> parameters = new ArrayList<>();
            parameters.add(Util.remove0x(metricDetails.getAnalyticsMetricDetailPkId()));
            ResultSet rs = executeSQL(SQL, parameters);
            try {
                while (rs.next()){
                    AnalyticsMetricDetailsQuery analyticsMetricDetailsQuery = new AnalyticsMetricDetailsQuery();
                    analyticsMetricDetailsQuery.setAnalyticsMetricDetailQueryPkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_DETAIL_QUERY_PK_ID")));
                    analyticsMetricDetailsQuery.setAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_DETAIL_FK_ID")));
                    analyticsMetricDetailsQuery.setMtPeCodeFkId(rs.getString("MT_PE_CODE_FK_ID"));
                    analyticsMetricDetailsQuery.setMtPeSeqNumPosInt(rs.getInt("MT_PE_SEQ_NUM_POS_INT"));
                    analyticsMetricDetailsQuery.setParentMtPeSeqNumPosInt(rs.getInt("PARENT_MT_PE_SEQ_NUM_POS_INT"));
                    analyticsMetricDetailsQuery.setFkRelationWithParentLngTxt(rs.getString("FK_RELATION_WITH_PARENT_LNG_TXT"));
                    analyticsMetricDetailsQuery.setAttrPathExpn(rs.getString("ATTR_PATH_EXPN"));
                    analyticsMetricDetailsQuery.setAttrDispAxizCodeFkId(rs.getString("ATTR_DISP_AXIZ_CODE_FK_ID"));
                    analyticsMetricDetailsQuery.setAttrDisplaySeqNumPosInt(rs.getInt("ATTR_DISPLAY_SEQ_NUM_POS_INT"));
                    analyticsMetricDetailsQuery.setAggregateAttr(rs.getBoolean("IS_AGGREGATE_FUNCTION"));
                    analyticsMetricDetailsQuery.setOrderBySeqNumPosInt(rs.getInt("ORDER_BY_SEQ_NUM_POS_INT"));
                    analyticsMetricDetailsQuery.setOrderByModeCodeFkId(rs.getString("ORDER_BY_MODE_CODE_FK_ID"));
                    analyticsMetricDetailsQuery.setOrderByValuesBigTxt(rs.getString("ORDER_BY_VALUES_BIG_TXT"));
                    analyticsMetricDetailsQuery.setGroupByAttribute(rs.getBoolean("IS_GROUP_BY_ATTRIBUTE"));
                    analyticsMetricDetailsQuery.setGroupBySeqNumPosInt(rs.getInt("GROUP_BY_SEQ_NUM_POS_INT"));
                    analyticsMetricDetailsQuery.setGroupByHavingExpn(rs.getString("GROUP_BY_HAVING_EXPN"));
                    analyticsMetricDetailsQuery.setClientMatch(rs.getBoolean("IS_CLIENT_MATCH"));
                    analyticsMetricDetailsQuery.setClientDisplay(rs.getBoolean("IS_CLIENT_DISPLAY"));
//                analyticsMetricDetailsQuery.setUsedInFilter(rs.getBoolean("IS_USED_IN_FILTER"));
                    metricDetails.getAnalyticsMetricDetailsQueryList().add(analyticsMetricDetailsQuery);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void populateAnalyticMetricDetailsFilter(AnalyticsMetricDetails metricDetails) {
        if(metricDetails!= null) {
            String SQL = "SELECT * FROM T_PFM_ADM_ANALYTICS_METRIC_DETAIL_FILTER WHERE ANALYTICS_METRIC_DETAIL_FK_ID = x?;";
            List<Object> parameters = new ArrayList<>();
            parameters.add(Util.remove0x(metricDetails.getAnalyticsMetricDetailPkId()));
            ResultSet rs = executeSQL(SQL, parameters);
            try {
                while (rs.next()){
                    AnalyticsMetricDetailsFilter analyticsMetricDetailsFilter = new AnalyticsMetricDetailsFilter();
                    analyticsMetricDetailsFilter.setAnalyticsMetricDetailFilterPkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_DETAIL_FILTER_PK_ID")));
                    analyticsMetricDetailsFilter.setAnalyticsMetricDetailFkId(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_DETAIL_FK_ID")));
                    analyticsMetricDetailsFilter.setMtPeCodeFkId(rs.getString("MT_PE_CODE_FK_ID"));
                    analyticsMetricDetailsFilter.setFilterExpn(rs.getString("FILTER_EXPN"));
                    metricDetails.getAnalyticsMetricDetailsFilterList().add(analyticsMetricDetailsFilter);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<AnalyticsAxisAndMetricMapVO> getAxisAndMetricMapList(String metricCategoryId, String metricCatMenuType, String axisAndMetricMapPKID) {
        List<AnalyticsAxisAndMetricMapVO> analyticsAxisAndMetricMapVOS = new ArrayList<>();
        String SQL = "";
        if(!StringUtil.isDefined(axisAndMetricMapPKID)) {
            SQL = "SELECT * FROM T_PFM_ADM_ANA_METRIC_AXIS_MAP CHILD LEFT JOIN T_PFM_ADM_ANA_METRIC_AXIS_MAP PARENT ON PARENT.MET_AXIZ_MAP_PK_ID = CHILD.SLF_PRNT_MET_AXIZ_MAP_FK_ID INNER JOIN T_PFM_ADM_ANA_METRIC_AXIS ON METRIC_AXIZ_PK_ID = CHILD.METRIC_AXIZ_FK_ID WHERE CHILD.MET_CAT_FK_ID = ? AND CHILD.MET_CAT_MENU_TYPE_FK_ID = ? AND CHILD.SLF_PRNT_MET_AXIZ_MAP_FK_ID IS NULL;";
        }else
            SQL = "SELECT * FROM T_PFM_ADM_ANA_METRIC_AXIS_MAP CHILD LEFT JOIN T_PFM_ADM_ANA_METRIC_AXIS_MAP PARENT ON PARENT.MET_AXIZ_MAP_PK_ID = CHILD.SLF_PRNT_MET_AXIZ_MAP_FK_ID INNER JOIN T_PFM_ADM_ANA_METRIC_AXIS ON METRIC_AXIZ_PK_ID = CHILD.METRIC_AXIZ_FK_ID WHERE CHILD.MET_CAT_FK_ID = ? AND CHILD.MET_CAT_MENU_TYPE_FK_ID = ? AND CHILD.SLF_PRNT_MET_AXIZ_MAP_FK_ID =? ;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(metricCategoryId));
        parameters.add(Util.convertStringIdToByteId(metricCatMenuType));
        if(StringUtil.isDefined(axisAndMetricMapPKID))
            parameters.add(Util.convertStringIdToByteId(axisAndMetricMapPKID));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while(rs.next()){
                AnalyticsAxisAndMetricMapVO analyticsAxisAndMetricMapVO = new AnalyticsAxisAndMetricMapVO();
                analyticsAxisAndMetricMapVO.setPkID(Util.convertByteToString(rs.getBytes("CHILD.MET_AXIZ_MAP_PK_ID")));
                analyticsAxisAndMetricMapVO.setMetricAxisID(Util.convertByteToString(rs.getBytes("CHILD.METRIC_AXIZ_FK_ID")));
                analyticsAxisAndMetricMapVO.setMetricAxisName(Util.getG11nValue(rs.getString("METRIC_AXIZ_NAME_G11N_BIG_TXT"),null));
                analyticsAxisAndMetricMapVO.setMetricCategory(Util.convertByteToString(rs.getBytes("CHILD.MET_CAT_FK_ID")));
                analyticsAxisAndMetricMapVO.setMetricCategoryMenuType(Util.convertByteToString(rs.getBytes("CHILD.MET_CAT_MENU_TYPE_FK_ID")));
                analyticsAxisAndMetricMapVO.setParentAnalyticsMetricAxisMap(Util.convertByteToString(rs.getBytes("CHILD.SLF_PRNT_MET_AXIZ_MAP_FK_ID")));
                analyticsAxisAndMetricMapVO.setParentAnalyticsMetricAxisID(Util.convertByteToString(rs.getBytes("PARENT.METRIC_AXIZ_FK_ID")));
                analyticsAxisAndMetricMapVOS.add(analyticsAxisAndMetricMapVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return analyticsAxisAndMetricMapVOS;
    }

    @Override
    public AnalyticsAxisAndMetricMapVO getAxisAndMetricMap(String metricAxisMapID) {
        String SQL = "SELECT * FROM T_PFM_ADM_ANA_METRIC_AXIS_MAP CHILD LEFT JOIN T_PFM_ADM_ANA_METRIC_AXIS_MAP PARENT ON PARENT.MET_AXIZ_MAP_PK_ID = CHILD.SLF_PRNT_MET_AXIZ_MAP_FK_ID INNER JOIN T_PFM_ADM_ANA_METRIC_AXIS ON METRIC_AXIZ_PK_ID = CHILD.METRIC_AXIZ_FK_ID WHERE CHILD.MET_AXIZ_MAP_PK_ID = ?;";
        AnalyticsAxisAndMetricMapVO analyticsAxisAndMetricMapVO = null;
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(metricAxisMapID));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while(rs.next()){
                analyticsAxisAndMetricMapVO = new AnalyticsAxisAndMetricMapVO();
                analyticsAxisAndMetricMapVO.setPkID(Util.convertByteToString(rs.getBytes("CHILD.MET_AXIZ_MAP_PK_ID")));
                analyticsAxisAndMetricMapVO.setMetricAxisID(Util.convertByteToString(rs.getBytes("CHILD.METRIC_AXIZ_FK_ID")));
                analyticsAxisAndMetricMapVO.setMetricAxisName(Util.getG11nValue(rs.getString("METRIC_AXIZ_NAME_G11N_BIG_TXT"),null));
                analyticsAxisAndMetricMapVO.setMetricCategory(Util.convertByteToString(rs.getBytes("CHILD.MET_CAT_FK_ID")));
                analyticsAxisAndMetricMapVO.setMetricCategoryMenuType(Util.convertByteToString(rs.getBytes("CHILD.MET_CAT_MENU_TYPE_FK_ID")));
                analyticsAxisAndMetricMapVO.setParentAnalyticsMetricAxisMap(Util.convertByteToString(rs.getBytes("CHILD.SLF_PRNT_MET_AXIZ_MAP_FK_ID")));
                analyticsAxisAndMetricMapVO.setParentAnalyticsMetricAxisID(Util.convertByteToString(rs.getBytes("PARENT.METRIC_AXIZ_FK_ID")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return analyticsAxisAndMetricMapVO;
    }

    @Override
    public MetricAxisVO getAxisDetails(String xAxisID) {
        String SQL = "SELECT * FROM T_PFM_ADM_ANA_METRIC_AXIS WHERE METRIC_AXIZ_PK_ID = ?;";
        MetricAxisVO metricAxisVO = new MetricAxisVO();
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(xAxisID));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while(rs.next()){
                metricAxisVO = new MetricAxisVO();
                /*
                private String name;
    private String metricAxisSourceType;
    private String mtPE;
    private String mtPEAlias;
    private String parentMTPEAlias;
    private String fkRelnWithParent;
    private String controlAttributePath;
    private boolean isClientDisplay;
    private boolean isClientMatch;
    private String rangeName;
    private String rangeIcon;
    private String rangeMinValue;
    private String rangeMinName;
    private String rangeMinIcon;
    private String rangeMaxValue;
    private String rangeMaxName;
    private String rangeMaxIcon;
    PROCESS_ELEMENT_CODE_FK_ID
PROCESS_ELEMENT_ALIAS_TXT
PARENT_PROCESS_ELEMENT_ALIAS_TXT
FK_RELATIONSHIP_WITH_PARENT_LNG_TXT
CONTROL_ATTRIBUTE_PATH_EXPN

IS_CLIENT_DISPLAY
IS_CLIENT_MATCH
RANGE_NAME_G11N_BIG_TXT
RANGE_ICON_CODE_FK_ID
RANGE_MIN_VALUE_NEG_DEC
RANGE_MIN_NAME_G11N_BIG_TXT
RANGE_MIN_ICON_CODE_FK_ID
RANGE_MAX_VALUE_NEG_DEC
RANGE_MAX_NAME_G11N_BIG_TXT
RANGE_MAX_ICON_CODE_FK_ID
                 */
                metricAxisVO.setPkID(Util.convertByteToString(rs.getBytes("METRIC_AXIZ_PK_ID")));
                metricAxisVO.setName(getG11nValue(rs.getString("METRIC_AXIZ_NAME_G11N_BIG_TXT")));
                metricAxisVO.setMetricAxisSourceType(getG11nValue(rs.getString("METRIC_AXIZ_SRC_TYPE_FK_ID")));
                metricAxisVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
                metricAxisVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                metricAxisVO.setParentMTPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                metricAxisVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                metricAxisVO.setControlAttributePath(rs.getString("CONTROL_ATTRIBUTE_PATH_EXPN"));
                metricAxisVO.setClientDisplay(rs.getBoolean("IS_CLIENT_DISPLAY"));
                metricAxisVO.setClientMatch(rs.getBoolean("IS_CLIENT_MATCH"));
                metricAxisVO.setName(getG11nValue(rs.getString("RANGE_NAME_G11N_BIG_TXT")));
                metricAxisVO.setRangeName(getG11nValue(rs.getString("RANGE_NAME_G11N_BIG_TXT")));
                metricAxisVO.setRangeIcon(rs.getString("RANGE_ICON_CODE_FK_ID"));
                metricAxisVO.setRangeMinValue(rs.getDouble("RANGE_MIN_VALUE_NEG_DEC"));
                metricAxisVO.setRangeMinName(getG11nValue(rs.getString("RANGE_MIN_NAME_G11N_BIG_TXT")));
                metricAxisVO.setRangeMinIcon(rs.getString("RANGE_MIN_ICON_CODE_FK_ID"));
                metricAxisVO.setRangeMaxValue(rs.getDouble("RANGE_MAX_VALUE_NEG_DEC"));
                metricAxisVO.setRangeMaxName(getG11nValue(rs.getString("RANGE_MAX_NAME_G11N_BIG_TXT")));
                metricAxisVO.setRangeMaxIcon(rs.getString("RANGE_MAX_ICON_CODE_FK_ID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return metricAxisVO;
    }

    @Override
    public List<TimePeriodOffsetVO> getTimeperiodOffsetsForLoggedInUser() {
        List<TimePeriodOffsetVO> timePeriodOffsetVOS = new ArrayList<>();
        String SQL = "SELECT * FROM T_ORG_ADM_ORG_LOC_PRD_OFFSET INNER JOIN T_ORG_ADM_CAL_PERD_TYPE ON CAL_PRD_TYPE_CODE_PK_ID =CALENDAR_PERIOD_TYPE_CODE_FK_ID WHERE ORG_LOC_FK_ID = ?;";
        List<Object> parameters = new ArrayList<>();
        String loggedInUserOrgLocFkID = TenantAwareCache.getPersonRepository().getPersonService().getPerson(TenantContextHolder.getUserContext().getPersonId()).getOrgLocFkID();
        parameters.add(Util.convertStringIdToByteId(loggedInUserOrgLocFkID));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while(rs.next()){
                TimePeriodOffsetVO timePeriodOffsetVO = new TimePeriodOffsetVO();
                timePeriodOffsetVO.setOrgLocPeriodOffsetPkID(Util.convertByteToString(rs.getBytes("ORG_LOC_CAL_PERIOD_PK_ID")));
                timePeriodOffsetVO.setCalendarPeriodType(rs.getString("CALENDAR_PERIOD_TYPE_CODE_FK_ID"));
                timePeriodOffsetVO.setCalendarPeriodName(rs.getString("CAL_PRD_TYPE_NAME_G11N_BIG_TXT"));
                timePeriodOffsetVO.setCalendarPeriodIcon(rs.getString("CAL_PRD_TYPE_ICON_CODE_FK_ID"));
                timePeriodOffsetVO.setWeekDaysOffset(rs.getInt("WEEK_DAYS_OFFSET_POS_INT"));
                timePeriodOffsetVO.setMonthOffset(rs.getInt("MTHS_OFFSET_POS_INT"));
                timePeriodOffsetVOS.add(timePeriodOffsetVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return timePeriodOffsetVOS;
    }

    @Override
    public List<CalendarPeriodUnitVO> getCalendarPeriodUnits() {
        List<CalendarPeriodUnitVO> calendarPeriodUnitVOS = new ArrayList<>();
        String SQL = "SELECT * FROM T_PFM_TAP_CAL_PERD_UNIT;";
        List<Object> parameters = new ArrayList<>();
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while (rs.next()){
                CalendarPeriodUnitVO periodUnitVO = new CalendarPeriodUnitVO();
                periodUnitVO.setPeriodUnit(rs.getString("CAL_PRD_UNIT_CODE_PK_ID"));
                periodUnitVO.setPeriodUnitName(rs.getString("CAL_PRD_UNIT_NAME_G11N_BIG_TXT"));
                calendarPeriodUnitVOS.add(periodUnitVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return calendarPeriodUnitVOS;
    }

    @Override
    public List<GraphTypeVO> getApplicableGraphTypes(String metricAxisMapID) {
        List<GraphTypeVO> graphTypes = new ArrayList<>();
        String SQL = "SELECT * FROM T_PFM_ADM_METRIC_TO_CHART_TYPE_MAP INNER JOIN T_SYS_TAP_CHART_TYPE ON CHART_TYPE_CODE_FK_ID = CHART_TYPE_CODE_PK_ID WHERE METRIC_AXIZ_MAP_FK_ID = ?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(metricAxisMapID));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while(rs.next()){
                GraphTypeVO graphTypeVO = new GraphTypeVO();
                graphTypeVO.setGraphTypeCode(rs.getString("CHART_TYPE_CODE_FK_ID"));
                graphTypeVO.setGraphTypeName(Util.getG11nValue(rs.getString("CHART_TYPE_NAME_G11N_BIG_TXT"), null));
                graphTypes.add(graphTypeVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return graphTypes;
    }

    @Override
    public AnalyticsMetricChartVO getAnalyticsMetricChart(String metricAxisMapID) {
        AnalyticsMetricChartVO analyticsMetricChartVO = new AnalyticsMetricChartVO();
        String SQL = "SELECT * FROM T_PFM_ADM_ANA_METRIC_CHART WHERE METRIC_AXIZ_MAP_FK_ID = ?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(metricAxisMapID));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            if(rs.next()){
                analyticsMetricChartVO.setChartPkID(Util.convertByteToString(rs.getBytes("ANA_METRIC_CHART_PK_ID")));
                analyticsMetricChartVO.setMetricInfo(rs.getString("METRIC_INTRO_G11N_BIG_TXT"));
                analyticsMetricChartVO.setMetricFormula(rs.getString("METRIC_FORM_G11N_BIG_TXT"));
                analyticsMetricChartVO.setBestPracticeRecommendation(rs.getString("BEST_PTC_RECOM_G11N_BIG_TXT"));
                analyticsMetricChartVO.setChartTitle(rs.getString("CHART_TITLE_G11N_BIG_TXT"));
                analyticsMetricChartVO.setTimeXAxisTitle(rs.getString("TIME_X_AXIZ_TITLE_G11N_BIG_TXT"));
                analyticsMetricChartVO.setxAxisTitle(rs.getString("X_AXIZ_TITLE_G11N_BIG_TXT"));
                analyticsMetricChartVO.setyAxis1Title(rs.getString("Y_AXIZ_TITLE_1_G11N_BIG_TXT"));
                analyticsMetricChartVO.setyAxis2Title(rs.getString("Y_AXIZ_TITLE_2_G11N_BIG_TXT"));
                analyticsMetricChartVO.setLegendVisible(rs.getBoolean("IS_LEGEND_VISIBLE"));
                analyticsMetricChartVO.setLegendDisplayPosition(rs.getString("LEGEND_POSITION_CODE_FK_ID"));
                analyticsMetricChartVO.setDrillDownable(rs.getBoolean("IS_DRILLDOWN_ENABLED"));
                analyticsMetricChartVO.setTableAvailable(rs.getBoolean("IS_TABLE_AVAILABLE"));
                analyticsMetricChartVO.setParentAnalyticsMetricDetailID(Util.convertByteToString(rs.getBytes("SLF_PRNT_ANALYTICS_METRIC_CHART_FK_ID")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return analyticsMetricChartVO;
    }

    @Override
    public List<AnalyticsMetricChartQueryVO> getAnalyticsMetricChartFilters(String chartPkID) {
        List<AnalyticsMetricChartQueryVO> analyticsMetricChartQueryVOS = new ArrayList<>();
        String SQL = "SELECT * FROM T_PFM_ADM_ANA_METRIC_QUERY WHERE ANALYTICS_METRIC_CHART_FK_ID = ?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(chartPkID));
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while(rs.next()){
                AnalyticsMetricChartQueryVO analyticsMetricChartQueryVO = new AnalyticsMetricChartQueryVO();
                analyticsMetricChartQueryVO.setQueryPkID(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_QUERY_PK_ID")));
                analyticsMetricChartQueryVO.setAnalyticsMetricChartID(Util.convertByteToString(rs.getBytes("ANALYTICS_METRIC_CHART_FK_ID")));
                analyticsMetricChartQueryVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
                analyticsMetricChartQueryVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                analyticsMetricChartQueryVO.setParentMTPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                analyticsMetricChartQueryVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                analyticsMetricChartQueryVO.setControlAttributePath(rs.getString("CONTROL_ATTRIBUTE_PATH_EXPN"));
                analyticsMetricChartQueryVO.setAttrDispAxizCodeFkId(rs.getString("ATTR_DISP_AXIZ_CODE_FK_ID"));
                analyticsMetricChartQueryVO.setAttributeDisplaySeqNumberForTable(rs.getInt("ATTR_DISPLAY_SEQ_NUM_FR_TBL_POS_INT"));
                analyticsMetricChartQueryVO.setAggregateFunction(rs.getBoolean("IS_AGGREGATE_FUNCTION"));
                analyticsMetricChartQueryVO.setOrderBySequenceNumber(rs.getInt("ORDER_BY_SEQ_NUM_POS_INT"));
                analyticsMetricChartQueryVO.setOrderByMode(rs.getString("ORDER_BY_MODE_CODE_FK_ID"));
                analyticsMetricChartQueryVO.setOrderByValues(rs.getString("ORDER_BY_VALUES_BIG_TXT"));
                analyticsMetricChartQueryVO.setGroupByAttribute(rs.getBoolean("IS_GROUP_BY_ATTRIBUTE"));
                analyticsMetricChartQueryVO.setGroupBySeqNumber(rs.getInt("GROUP_BY_SEQ_NUM_POS_INT"));
                analyticsMetricChartQueryVO.setGroupByHavingExpn(rs.getString("GROUP_BY_HAVING_EXPN"));
                analyticsMetricChartQueryVO.setClientDisplay(rs.getBoolean("IS_CLIENT_DISPLAY"));
                analyticsMetricChartQueryVO.setClientMatch(rs.getBoolean("IS_CLIENT_MATCH"));
                analyticsMetricChartQueryVOS.add(analyticsMetricChartQueryVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return analyticsMetricChartQueryVOS;
    }


    private String getG11nValue(String input){
        return Util.getG11nValue(input, null);
    }
}
