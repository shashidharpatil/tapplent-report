package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by tapplent on 29/11/16.
 */
public class IconVO {
    private String code;
    @JsonIgnore
    private String iconNameG11nBigTxt;
    @JsonIgnore
    private String iconPurposeTxt;
    private String className;
    private String unicode;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIconNameG11nBigTxt() {
        return iconNameG11nBigTxt;
    }

    public void setIconNameG11nBigTxt(String iconNameG11nBigTxt) {
        this.iconNameG11nBigTxt = iconNameG11nBigTxt;
    }

    public String getIconPurposeTxt() {
        return iconPurposeTxt;
    }

    public void setIconPurposeTxt(String iconPurposeTxt) {
        this.iconPurposeTxt = iconPurposeTxt;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getUnicode() {
        return unicode;
    }

    public void setUnicode(String unicode) {
        this.unicode = unicode;
    }
}
