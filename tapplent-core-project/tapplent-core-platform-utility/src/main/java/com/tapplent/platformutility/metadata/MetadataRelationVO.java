package com.tapplent.platformutility.metadata;

public class MetadataRelationVO {
	private String dataApiRelationTablePkId;
	private String dataApiInternalTableFkId;
	private String btProcessElementFkId;
	private String domainObjectFkId;
	private String dbTableName;
	private String mtDoAttributeSpecG11nText;
	private String doAttributeFkId;
	private String dbColumnName;
	private String relationDbDataTypeCodeFkId;
	private Boolean relationFunctionalPrimaryKeyFlag;
	private Boolean relationDbPrimaryKeyFlag;
	private Boolean relationDependencyKeyFlag;
	private Boolean relationMandatoryFlag;
	private Boolean relationCustomizableFlag;
	private Boolean relationGlocalizedFlag;
	private Boolean relationSelfJoinParentFlag;
	private Boolean relationRelationFlag;
	private Boolean relationHierarchyFlag;
	private Boolean relationSubjectUserFlag;
	private Boolean relationReportableFlag;
	private Boolean relationStandardFlag;
	private Boolean relationLocalSeachableFlag;
	public MetadataRelationVO(){
		
	}
	public String getDataApiRelationTablePkId() {
		return dataApiRelationTablePkId;
	}
	public void setDataApiRelationTablePkId(String dataApiRelationTablePkId) {
		this.dataApiRelationTablePkId = dataApiRelationTablePkId;
	}
	public String getDataApiInternalTableFkId() {
		return dataApiInternalTableFkId;
	}
	public void setDataApiInternalTableFkId(String dataApiInternalTableFkId) {
		this.dataApiInternalTableFkId = dataApiInternalTableFkId;
	}
	public String getBtProcessElementFkId() {
		return btProcessElementFkId;
	}
	public void setBtProcessElementFkId(String btProcessElementFkId) {
		this.btProcessElementFkId = btProcessElementFkId;
	}
	public String getDomainObjectFkId() {
		return domainObjectFkId;
	}
	public void setDomainObjectFkId(String domainObjectFkId) {
		this.domainObjectFkId = domainObjectFkId;
	}
	public String getDbTableName() {
		return dbTableName;
	}
	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}
	public String getMtDoAttributeSpecG11nText() {
		return mtDoAttributeSpecG11nText;
	}
	public void setMtDoAttributeSpecG11nText(String mtDoAttributeSpecG11nText) {
		this.mtDoAttributeSpecG11nText = mtDoAttributeSpecG11nText;
	}
	public String getDoAttributeFkId() {
		return doAttributeFkId;
	}
	public void setDoAttributeFkId(String doAttributeFkId) {
		this.doAttributeFkId = doAttributeFkId;
	}
	public String getDbColumnName() {
		return dbColumnName;
	}
	public void setDbColumnName(String dbColumnName) {
		this.dbColumnName = dbColumnName;
	}
	public String getRelationDbDataTypeCodeFkId() {
		return relationDbDataTypeCodeFkId;
	}
	public void setRelationDbDataTypeCodeFkId(String relationDbDataTypeCodeFkId) {
		this.relationDbDataTypeCodeFkId = relationDbDataTypeCodeFkId;
	}
	public Boolean getRelationFunctionalPrimaryKeyFlag() {
		return relationFunctionalPrimaryKeyFlag;
	}
	public void setRelationFunctionalPrimaryKeyFlag(Boolean relationFunctionalPrimaryKeyFlag) {
		this.relationFunctionalPrimaryKeyFlag = relationFunctionalPrimaryKeyFlag;
	}
	public Boolean getRelationDbPrimaryKeyFlag() {
		return relationDbPrimaryKeyFlag;
	}
	public void setRelationDbPrimaryKeyFlag(Boolean relationDbPrimaryKeyFlag) {
		this.relationDbPrimaryKeyFlag = relationDbPrimaryKeyFlag;
	}
	public Boolean getRelationDependencyKeyFlag() {
		return relationDependencyKeyFlag;
	}
	public void setRelationDependencyKeyFlag(Boolean relationDependencyKeyFlag) {
		this.relationDependencyKeyFlag = relationDependencyKeyFlag;
	}
	public Boolean getRelationMandatoryFlag() {
		return relationMandatoryFlag;
	}
	public void setRelationMandatoryFlag(Boolean relationMandatoryFlag) {
		this.relationMandatoryFlag = relationMandatoryFlag;
	}
	public Boolean getRelationCustomizableFlag() {
		return relationCustomizableFlag;
	}
	public void setRelationCustomizableFlag(Boolean relationCustomizableFlag) {
		this.relationCustomizableFlag = relationCustomizableFlag;
	}
	public Boolean getRelationGlocalizedFlag() {
		return relationGlocalizedFlag;
	}
	public void setRelationGlocalizedFlag(Boolean relationGlocalizedFlag) {
		this.relationGlocalizedFlag = relationGlocalizedFlag;
	}
	public Boolean getRelationSelfJoinParentFlag() {
		return relationSelfJoinParentFlag;
	}
	public void setRelationSelfJoinParentFlag(Boolean relationSelfJoinParentFlag) {
		this.relationSelfJoinParentFlag = relationSelfJoinParentFlag;
	}
	public Boolean getRelationRelationFlag() {
		return relationRelationFlag;
	}
	public void setRelationRelationFlag(Boolean relationRelationFlag) {
		this.relationRelationFlag = relationRelationFlag;
	}
	public Boolean getRelationHierarchyFlag() {
		return relationHierarchyFlag;
	}
	public void setRelationHierarchyFlag(Boolean relationHierarchyFlag) {
		this.relationHierarchyFlag = relationHierarchyFlag;
	}
	public Boolean getRelationSubjectUserFlag() {
		return relationSubjectUserFlag;
	}
	public void setRelationSubjectUserFlag(Boolean relationSubjectUserFlag) {
		this.relationSubjectUserFlag = relationSubjectUserFlag;
	}
	public Boolean getRelationReportableFlag() {
		return relationReportableFlag;
	}
	public void setRelationReportableFlag(Boolean relationReportableFlag) {
		this.relationReportableFlag = relationReportableFlag;
	}
	public Boolean getRelationStandardFlag() {
		return relationStandardFlag;
	}
	public void setRelationStandardFlag(Boolean relationStandardFlag) {
		this.relationStandardFlag = relationStandardFlag;
	}
	public Boolean getRelationLocalSeachableFlag() {
		return relationLocalSeachableFlag;
	}
	public void setRelationLocalSeachableFlag(Boolean relationLocalSeachableFlag) {
		this.relationLocalSeachableFlag = relationLocalSeachableFlag;
	}
}
