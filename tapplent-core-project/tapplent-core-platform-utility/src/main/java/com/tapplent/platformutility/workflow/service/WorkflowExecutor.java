package com.tapplent.platformutility.workflow.service;

import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.tenantresolver.server.property.ServerActionStep;

import java.sql.SQLException;

/**
 * Created by Manas on 8/30/16.
 */
public interface WorkflowExecutor {
	public void executeWorkflow(GlobalVariables globalVariables, String workflowTxnActionId) throws Exception;
}
