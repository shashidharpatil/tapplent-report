package com.tapplent.platformutility.common.util;

import java.sql.Timestamp;
import java.util.UUID;

public class Common {
	public static String getUUID() {
		String uuid = UUID.randomUUID().toString().toUpperCase();
		return "0x"+(uuid.substring(14, 18)+uuid.substring(9, 13)+uuid.substring(0, 8)+uuid.substring(19, 23)+uuid.substring(24));
	}

	public static String getQS(Timestamp timestamp) {
		if (null == timestamp) {
			return "NULL";
		} else {
			return "'" + timestamp + "'";
		}
	}

	public static String getQS(String s) {
		if (null == s) {
			return "NULL";
		} else {
			return "'" + s + "'";
		}
	}

	public static String getQS(Boolean booleanData) {
		if (null == booleanData) {
			return "NULL";
		} else {
			return booleanData.toString();
		}
	}

//	public static String getCurrentUser() {
//		Authentication authentication = SecurityContextHolder.getContext()
//				.getAuthentication();
//		if (null != authentication) {
//			return authentication.getName();
//		} else {
//			return "admin";
//		}
//	}

	public static Timestamp getCurrentTimeStamp() {
		java.util.Date date = new java.util.Date();
		return (new Timestamp(date.getTime()));
	}
}
