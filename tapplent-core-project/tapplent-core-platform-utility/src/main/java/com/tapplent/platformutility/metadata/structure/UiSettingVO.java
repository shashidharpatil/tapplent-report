package com.tapplent.platformutility.metadata.structure;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Created by Manas on 1/26/17.
 */
public class UiSettingVO {
    private String versionId;
    private String uiSettingPkId;
    private JsonNode uiPropertyBlob;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getUiSettingPkId() {
        return uiSettingPkId;
    }

    public void setUiSettingPkId(String uiSettingPkId) {
        this.uiSettingPkId = uiSettingPkId;
    }

    public JsonNode getUiPropertyBlob() {
        return uiPropertyBlob;
    }

    public void setUiPropertyBlob(JsonNode uiPropertyBlob) {
        this.uiPropertyBlob = uiPropertyBlob;
    }
}
