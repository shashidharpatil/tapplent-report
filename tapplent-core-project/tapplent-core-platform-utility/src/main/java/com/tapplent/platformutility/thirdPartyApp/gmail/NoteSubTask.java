package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.sql.Timestamp;

public class NoteSubTask {
	private String noteSubTaskPkId;
	private String noteFkId;
	private String subTaskDescG11nBigTxt;
	private boolean isComplete;
	private boolean isDeleted;
	private Timestamp lastModifiedDatetime;
	public String getNoteSubTaskPkId() {
		return noteSubTaskPkId;
	}
	public void setNoteSubTaskPkId(String noteSubTaskPkId) {
		this.noteSubTaskPkId = noteSubTaskPkId;
	}
	public String getNoteFkId() {
		return noteFkId;
	}
	public void setNoteFkId(String noteFkId) {
		this.noteFkId = noteFkId;
	}
	public String getSubTaskDescG11nBigTxt() {
		return subTaskDescG11nBigTxt;
	}
	public void setSubTaskDescG11nBigTxt(String subTaskDescG11nBigTxt) {
		this.subTaskDescG11nBigTxt = subTaskDescG11nBigTxt;
	}
	public boolean isComplete() {
		return isComplete;
	}
	public void setComplete(boolean isComplete) {
		this.isComplete = isComplete;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(Timestamp lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
}
