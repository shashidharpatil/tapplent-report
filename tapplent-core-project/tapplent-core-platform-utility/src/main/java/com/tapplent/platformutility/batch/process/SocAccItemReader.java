package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.SA_SocAccData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;



public class SocAccItemReader extends JdbcPagingItemReader<SA_SocAccData> {


	
	public SocAccItemReader() {
		super();
		DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
		setDataSource(ds);
		setPageSize(20);
		setQueryProvider(queryProvider(ds));
		setRowMapper(rowMapper());
	}
	
	private RowMapper<SA_SocAccData> rowMapper() {
		return new SocAccRowMapper();
	}

	private PagingQueryProvider queryProvider(DataSource ds) {
		 MySqlPagingQueryProvider provider = new MySqlPagingQueryProvider();
		    provider.setSelectClause("T_PRN_EU_SOCIAL_ACCOUNT.PERSON_SOCIAL_ACCOUNT_PK_ID,"
		    		+ "T_PRN_EU_SOCIAL_ACCOUNT.PERSON_DEP_FK_ID,"
		    		+ "T_PRN_EU_SOCIAL_ACCOUNT.SOCIAL_ACCOUNT_TYPE_CODE_FK_ID,"
		    		+ "T_PRN_EU_SOCIAL_ACCOUNTSOCIAL_ACCOUNT_CARACTERISTICS_BLOB,"
		    		+ "T_PRN_EU_SOCIAL_ACCOUNT.IS_VALID,"
		    		+ "T_PRN_LKP_SOCIAL_ACCOUNT_TYPE.ACCOUNT_HANDLE_LNG_TXT,"
		    		+ "T_PRN_LKP_SOCIAL_ACCOUNT_TYPE.DEVICE_INDEPENDENT_BLOB");
		    provider.setFromClause("from T_PRN_EU_SOCIAL_ACCOUNT Inner join T_PRN_LKP_SOCIAL_ACCOUNT_TYPE on"
		    		+ "T_PRN_EU_SOCIAL_ACCOUNT.SOCIAL_ACCOUNT_TYPE_CODE_FK_ID = "
		    		+ "T_PRN_LKP_SOCIAL_ACCOUNT_TYPE.SOCIAL_ACCOUNT_TYPE_ICON_CODE_FK_ID");
		   
		    Map abc = new HashMap<String, Order>();
		    abc.put("PERSON_SOCIAL_ACCOUNT_PK_ID", Order.ASCENDING);
		   provider.setSortKeys(abc);
		
		try {
			return provider;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
