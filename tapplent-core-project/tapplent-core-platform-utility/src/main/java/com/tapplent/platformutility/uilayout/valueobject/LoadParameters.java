package com.tapplent.platformutility.uilayout.valueobject;

import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.search.builder.FilterOperatorValue;
import com.tapplent.platformutility.search.builder.SortData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 10/11/16.
 */
public class LoadParameters {
    private String loadParamPkId;
	private String processElementId;
    private String filterExpn;
    private String sortExpn;
    private Map<String,List<FilterOperatorValue>> filters = new HashMap<>();
    private String fkRelationWithParent;
    private String screenSectionInstanceFkId;
    private String controlActionInstanceFkId;
    private String sectionActionInstanceFkId;
    private List<SortData> sorts;
    private boolean logActivity = false;
    private boolean onlyBookmarkedRecords = false;
    private String logActivityMTPE;
    private int page;
    private int pageSize;
    private boolean applyPagination = true;
    private boolean returnOnlySqlQuery;
    private String keyWord;
    public String getSectionActionInstanceFkId() {
		return sectionActionInstanceFkId;
	}

	public void setSectionActionInstanceFkId(String sectionActionInstanceFkId) {
		this.sectionActionInstanceFkId = sectionActionInstanceFkId;
	}

	public String getControlActionInstanceFkId() {
		return controlActionInstanceFkId;
	}

	public void setControlActionInstanceFkId(String controlActionInstanceFkId) {
		this.controlActionInstanceFkId = controlActionInstanceFkId;
	}

	public String getScreenSectionInstanceFkId() {
		return screenSectionInstanceFkId;
	}

	public void setScreenSectionInstanceFkId(String screenSectionInstanceFkId) {
		this.screenSectionInstanceFkId = screenSectionInstanceFkId;
	}

    public String getLoadParamPkId() {
        return loadParamPkId;
    }

    public void setLoadParamPkId(String loadParamPkId) {
        this.loadParamPkId = loadParamPkId;
    }

    public String getProcessElementId() {
        return processElementId;
    }

    public void setProcessElementId(String processElementId) {
        this.processElementId = processElementId;
    }

    public String getFilterExpn() {
        if(!StringUtil.isDefined(filterExpn)) {
            if (filters != null) {
            /* Get filter expn from filters */
                filterExpn = Util.getFilterExpnFromFilters(filters).getFilterExpn();
            }
        }
        return filterExpn;
    }

    public void setFilterExpn(String filterExpn) {
        this.filterExpn = filterExpn;
    }

    public String getSortExpn() {
        return sortExpn;
    }

    public void setSortExpn(String sortExpn) {
        this.sortExpn = sortExpn;
    }

    public void setFkRelationWithParent(String fkRelationWithParent) {
        this.fkRelationWithParent = fkRelationWithParent;
    }

    public String getFkRelationWithParent() {
        return fkRelationWithParent;
    }

    public Map<String, List<FilterOperatorValue>> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, List<FilterOperatorValue>> filters) {
        this.filters = filters;
    }

    public boolean isLogActivity() {
        return logActivity;
    }

    public void setLogActivity(boolean logActivity) {
        this.logActivity = logActivity;
    }

    public String getLogActivityMTPE() {
        return logActivityMTPE;
    }

    public void setLogActivityMTPE(String logActivityMTPE) {
        this.logActivityMTPE = logActivityMTPE;
    }

    public boolean isOnlyBookmarkedRecords() {
        return onlyBookmarkedRecords;
    }

    public void setOnlyBookmarkedRecords(boolean onlyBookmarkedRecords) {
        this.onlyBookmarkedRecords = onlyBookmarkedRecords;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isApplyPagination() {
        return applyPagination;
    }

    public void setApplyPagination(boolean applyPagination) {
        this.applyPagination = applyPagination;
    }

    public boolean isReturnOnlySqlQuery() {
        return returnOnlySqlQuery;
    }

    public void setReturnOnlySqlQuery(boolean returnOnlySqlQuery) {
        this.returnOnlySqlQuery = returnOnlySqlQuery;
    }

    public List<SortData> getSorts() {
        return sorts;
    }

    public void setSorts(List<SortData> sorts) {
        this.sorts = sorts;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
}
