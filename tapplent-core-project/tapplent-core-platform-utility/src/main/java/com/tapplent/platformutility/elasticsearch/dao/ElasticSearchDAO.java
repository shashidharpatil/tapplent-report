package com.tapplent.platformutility.elasticsearch.dao;

import com.tapplent.platformutility.elasticsearch.valueObject.ElasticsearchPersonTestVO;

import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 08/05/17.
 */
public interface ElasticSearchDAO {

    List<Map<String, String>> getPersons();
}
