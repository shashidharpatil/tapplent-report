package com.tapplent.platformutility.activityLog;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.layout.valueObject.IconVO;

/**
 * Created by Manas on 1/9/17.
 */
public class ActivityLogDtl {
    private String activityLogDtlPkId;
    private String activityLogFkId;
    private String mtDoaCodeFkId;
    private String btCodeFkId;
    private IconVO attributeIcon;
    private String attributeName;
    @JsonIgnore
    private boolean isNearLabel;
    private boolean isBooleanValue;
    private boolean isDatetimeValue;
    private String dataType;
    private String newValueG11nBigTxt;

    public String getActivityLogDtlPkId() {
        return activityLogDtlPkId;
    }

    public void setActivityLogDtlPkId(String activityLogDtlPkId) {
        this.activityLogDtlPkId = activityLogDtlPkId;
    }

    public String getActivityLogFkId() {
        return activityLogFkId;
    }

    public void setActivityLogFkId(String activityLogFkId) {
        this.activityLogFkId = activityLogFkId;
    }

    public String getMtDoaCodeFkId() {
        return mtDoaCodeFkId;
    }

    public void setMtDoaCodeFkId(String mtDoaCodeFkId) {
        this.mtDoaCodeFkId = mtDoaCodeFkId;
    }

    public String getBtCodeFkId() {
        return btCodeFkId;
    }

    public void setBtCodeFkId(String btCodeFkId) {
        this.btCodeFkId = btCodeFkId;
    }

    public boolean isNearLabel() {
        return isNearLabel;
    }

    public void setNearLabel(boolean nearLabel) {
        isNearLabel = nearLabel;
    }

    public boolean isBooleanValue() {
        return isBooleanValue;
    }

    public void setBooleanValue(boolean booleanValue) {
        isBooleanValue = booleanValue;
    }

    public String getNewValueG11nBigTxt() {
        return newValueG11nBigTxt;
    }

    public void setNewValueG11nBigTxt(String newValueG11nBigTxt) {
        this.newValueG11nBigTxt = newValueG11nBigTxt;
    }

    public IconVO getAttributeIcon() {
        return attributeIcon;
    }

    public void setAttributeIcon(IconVO attributeIcon) {
        this.attributeIcon = attributeIcon;
    }

    public boolean isDatetimeValue() {
        return isDatetimeValue;
    }

    public void setDatetimeValue(boolean datetimeValue) {
        isDatetimeValue = datetimeValue;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }
}
