package com.tapplent.platformutility.etl.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class EtlMapVO {
	private String versionId; 
	private String etlMapPkId;
	private String mapName;
	private String etlHeaderDepFkId;
	private int mapSequenceNumberPosInt;
	private String targetDoCodeFkId;
	private String targetBaseTemplateCodeFkId;
	private String targetDataModeCodeFkId;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getEtlMapPkId() {
		return etlMapPkId;
	}
	public void setEtlMapPkId(String etlMapPkId) {
		this.etlMapPkId = etlMapPkId;
	}
	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}
	public String getEtlHeaderDepFkId() {
		return etlHeaderDepFkId;
	}
	public void setEtlHeaderDepFkId(String etlHeaderDepFkId) {
		this.etlHeaderDepFkId = etlHeaderDepFkId;
	}
	public int getMapSequenceNumberPosInt() {
		return mapSequenceNumberPosInt;
	}
	public void setMapSequenceNumberPosInt(int mapSequenceNumberPosInt) {
		this.mapSequenceNumberPosInt = mapSequenceNumberPosInt;
	}
	public String getTargetDoCodeFkId() {
		return targetDoCodeFkId;
	}
	public void setTargetDoCodeFkId(String targetDoCodeFkId) {
		this.targetDoCodeFkId = targetDoCodeFkId;
	}
	public String getTargetBaseTemplateCodeFkId() {
		return targetBaseTemplateCodeFkId;
	}
	public void setTargetBaseTemplateCodeFkId(String targetBaseTemplateCodeFkId) {
		this.targetBaseTemplateCodeFkId = targetBaseTemplateCodeFkId;
	}
	public String getTargetDataModeCodeFkId() {
		return targetDataModeCodeFkId;
	}
	public void setTargetDataModeCodeFkId(String targetDataModeCodeFkId) {
		this.targetDataModeCodeFkId = targetDataModeCodeFkId;
	}
}
