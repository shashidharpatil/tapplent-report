package com.tapplent.platformutility.notification.helper;

public enum NotificationConstants {
	
	title,
	notificationText,
	subText,
	smallIcon,
	actions,
	notificationKey,
	notificationId

}
