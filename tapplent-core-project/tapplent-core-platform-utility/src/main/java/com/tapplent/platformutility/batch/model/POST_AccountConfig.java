package com.tapplent.platformutility.batch.model;

public class POST_AccountConfig {

	private String socialAccountTypeCodePkId;

	private String accountHandleLngTxt;

	private Object deviceIndependentBlob;

	public String getChannelTypeCodePkId() {
		return socialAccountTypeCodePkId;
	}

	public void setChannelTypeCodePkId(String channelTypeCodePkId) {
		this.socialAccountTypeCodePkId = channelTypeCodePkId;
	}

	public String getAccountHandleLngTxt() {
		return accountHandleLngTxt;
	}

	public void setAccountHandleLngTxt(String accountHandleLngTxt) {
		this.accountHandleLngTxt = accountHandleLngTxt;
	}

	public Object getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}

	public void setDeviceIndependentBlob(String deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	
	
	
}
