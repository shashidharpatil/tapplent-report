package com.tapplent.platformutility.batch.process;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.POST_PostData;
import org.springframework.jdbc.core.RowMapper;


public class PostRowMapper implements RowMapper<POST_PostData>{

	public POST_PostData mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		POST_PostData post = new POST_PostData();
		
		post.setSocialAccountPostTxnDepFkId(rs.getString(Constants.socialAccountPostTxnDepFkId));
		post.setPersonFkId(rs.getString(Constants.personFkId));
		post.setReturnStatusCodeFkId(rs.getBoolean(Constants.returnStatusCodeFkId));
		post.setSocialAccountPersonPostPkId(rs.getString(Constants.socialAccountPersonPostPkId));
		post.setSocialAccountPostTxnPkId(rs.getString(Constants.socialAccountPostTxnPkId));
		post.setSocialAccountPostDefnFkId(rs.getString(Constants.socialAccountPostDefnFkId));
		post.setMtPeCodeFkId(rs.getString(Constants.mtPeCodeFkId));
		post.setDoRowPrimaryKeyTxt(rs.getString(Constants.doRowPrimaryKeyTxt));
		post.setDoRowVersionFkId(rs.getString(Constants.doRowVersionFkId));
		post.setSocialAccountTypeCodeFkId(rs.getString(Constants.socialAccountTypeCodeFkId));
		post.setNotificationDefinitionFkId(rs.getString(Constants.notificationDefinitionFkId));
		
		return post;
	}

}
