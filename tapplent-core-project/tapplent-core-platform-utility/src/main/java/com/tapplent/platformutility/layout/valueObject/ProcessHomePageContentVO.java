package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 15/11/16.
 */
public class ProcessHomePageContentVO {
    private String prcHomePageContentPkId;
    private String prcHomePageFkId;
    private String cardSectionTypeCodeFkId;
    private int cardSectionInstancePosInt;
    private int recordNumberPosInt;
    private String cardSectionControlTypeCodeFkId;
    private String controlType;
    private String controlContentValueG11nBigTxt;
    private String contentReferencePrimaryKeyId;
    private int parentCardSectionInstancePosInt;
    private int parentRecordNumberPosInt;
    private boolean isDeleted;

    public String getPrcHomePageContentPkId() {
        return prcHomePageContentPkId;
    }

    public void setPrcHomePageContentPkId(String prcHomePageContentPkId) {
        this.prcHomePageContentPkId = prcHomePageContentPkId;
    }

    public String getPrcHomePageFkId() {
        return prcHomePageFkId;
    }

    public void setPrcHomePageFkId(String prcHomePageFkId) {
        this.prcHomePageFkId = prcHomePageFkId;
    }

    public String getCardSectionTypeCodeFkId() {
        return cardSectionTypeCodeFkId;
    }

    public void setCardSectionTypeCodeFkId(String cardSectionTypeCodeFkId) {
        this.cardSectionTypeCodeFkId = cardSectionTypeCodeFkId;
    }

    public int getRecordNumberPosInt() {
        return recordNumberPosInt;
    }

    public void setRecordNumberPosInt(int recordNumberPosInt) {
        this.recordNumberPosInt = recordNumberPosInt;
    }

    public String getCardSectionControlTypeCodeFkId() {
        return cardSectionControlTypeCodeFkId;
    }

    public void setCardSectionControlTypeCodeFkId(String cardSectionControlTypeCodeFkId) {
        this.cardSectionControlTypeCodeFkId = cardSectionControlTypeCodeFkId;
    }

    public String getControlContentValueG11nBigTxt() {
        return controlContentValueG11nBigTxt;
    }

    public void setControlContentValueG11nBigTxt(String controlContentValueG11nBigTxt) {
        this.controlContentValueG11nBigTxt = controlContentValueG11nBigTxt;
    }

    public String getContentReferencePrimaryKeyId() {
        return contentReferencePrimaryKeyId;
    }

    public void setContentReferencePrimaryKeyId(String contentReferencePrimaryKeyId) {
        this.contentReferencePrimaryKeyId = contentReferencePrimaryKeyId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public int getParentRecordNumberPosInt() {
        return parentRecordNumberPosInt;
    }

    public void setParentRecordNumberPosInt(int parentRecordNumberPosInt) {
        this.parentRecordNumberPosInt = parentRecordNumberPosInt;
    }

    public int getCardSectionInstancePosInt() {
        return cardSectionInstancePosInt;
    }

    public void setCardSectionInstancePosInt(int cardSectionInstancePosInt) {
        this.cardSectionInstancePosInt = cardSectionInstancePosInt;
    }

    public int getParentCardSectionInstancePosInt() {
        return parentCardSectionInstancePosInt;
    }

    public void setParentCardSectionInstancePosInt(int parentCardSectionInstancePosInt) {
        this.parentCardSectionInstancePosInt = parentCardSectionInstancePosInt;
    }

    public String getControlType() {
        return controlType;
    }

    public void setControlType(String controlType) {
        this.controlType = controlType;
    }
}
