package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class BaseTemplateVO {
	private String versionId;
	private String baseTemplateId;
	private String baseTemplateName;
	private String baseTemplateNameG11nText;
	private String metaProcessCode;
	private String baseTemplateIcon;
	private String baseTemplateThemeTemplateCode;
	private boolean	isArchived;
	private boolean showNonPermissionedActions;
	private boolean isBtStandard;
//	private JsonNode btPropertyBlob;
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getBaseTemplateName() {
		return baseTemplateName;
	}

	public void setBaseTemplateName(String baseTemplateName) {
		this.baseTemplateName = baseTemplateName;
	}

	public String getBaseTemplateNameG11nText() {
		return baseTemplateNameG11nText;
	}
	public void setBaseTemplateNameG11nText(String baseTemplateNameG11nText) {
		this.baseTemplateNameG11nText = baseTemplateNameG11nText;
	}
	public String getMetaProcessCode() {
		return metaProcessCode;
	}
	public void setMetaProcessCode(String metaProcessCode) {
		this.metaProcessCode = metaProcessCode;
	}
	public String getBaseTemplateIcon() {
		return baseTemplateIcon;
	}
	public void setBaseTemplateIcon(String baseTemplateIcon) {
		this.baseTemplateIcon = baseTemplateIcon;
	}
	public String getBaseTemplateThemeTemplateCode() {
		return baseTemplateThemeTemplateCode;
	}
	public void setBaseTemplateThemeTemplateCode(String baseTemplateThemeTemplateCode) {
		this.baseTemplateThemeTemplateCode = baseTemplateThemeTemplateCode;
	}
	public boolean isArchived() {
		return isArchived;
	}
	public void setArchived(boolean isArchived) {
		this.isArchived = isArchived;
	}
	public boolean isBtStandard() {
		return isBtStandard;
	}
	public void setBtStandard(boolean isBtStandard) {
		this.isBtStandard = isBtStandard;
	}
	public boolean isShowNonPermissionedActions() {
		return showNonPermissionedActions;
	}
	public void setShowNonPermissionedActions(boolean showNonPermissionedActions) {
		this.showNonPermissionedActions = showNonPermissionedActions;
	}
//	public JsonNode getBtPropertyBlob() {
//		return btPropertyBlob;
//	}
//	public void setBtPropertyBlob(JsonNode btPropertyBlob) {
//		this.btPropertyBlob = btPropertyBlob;
//	}
}
