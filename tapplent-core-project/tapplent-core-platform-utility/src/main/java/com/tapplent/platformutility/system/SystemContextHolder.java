package com.tapplent.platformutility.system;

import java.util.concurrent.ConcurrentHashMap;

public class SystemContextHolder {
	public static ConcurrentHashMap<String, Object> systemObjects = new ConcurrentHashMap<>();
	
	public static void setAttribute(String name, Object attribute) {
		systemObjects.put(name, attribute);
	}

	public static Object getAttribute(String name) {
		return systemObjects.get(name);
	}
}
