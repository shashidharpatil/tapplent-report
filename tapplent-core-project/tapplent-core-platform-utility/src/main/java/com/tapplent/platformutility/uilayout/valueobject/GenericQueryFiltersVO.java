package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 10/06/17.
 */
public class GenericQueryFiltersVO {
    private String filterID;
    private String queryID;
    private String targetMtPE;
    private String targetMtPEAlias;
    private String targetParentMtPEAlias;
    private String targetFkRelnWithParent;
    private String attributePathExpn;
    private String operator;
    private String currentMtPE;
    private String currentMtPEAlias;
    private String currentParentMtPEAlias;
    private String currentFkRelnWithParent;
    private String attributeValue;
    private boolean isAlwaysApplied;
    private boolean isInteractiveFilter;
    private boolean isHavingClause;

    public String getFilterID() {
        return filterID;
    }

    public void setFilterID(String filterID) {
        this.filterID = filterID;
    }

    public String getQueryID() {
        return queryID;
    }

    public void setQueryID(String queryID) {
        this.queryID = queryID;
    }

    public String getTargetMtPE() {
        return targetMtPE;
    }

    public void setTargetMtPE(String targetMtPE) {
        this.targetMtPE = targetMtPE;
    }

    public String getTargetMtPEAlias() {
        return targetMtPEAlias;
    }

    public void setTargetMtPEAlias(String targetMtPEAlias) {
        this.targetMtPEAlias = targetMtPEAlias;
    }

    public String getTargetParentMtPEAlias() {
        return targetParentMtPEAlias;
    }

    public void setTargetParentMtPEAlias(String targetParentMtPEAlias) {
        this.targetParentMtPEAlias = targetParentMtPEAlias;
    }

    public String getTargetFkRelnWithParent() {
        return targetFkRelnWithParent;
    }

    public void setTargetFkRelnWithParent(String targetFkRelnWithParent) {
        this.targetFkRelnWithParent = targetFkRelnWithParent;
    }

    public String getAttributePathExpn() {
        return attributePathExpn;
    }

    public void setAttributePathExpn(String attributePathExpn) {
        this.attributePathExpn = attributePathExpn;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getCurrentMtPE() {
        return currentMtPE;
    }

    public void setCurrentMtPE(String currentMtPE) {
        this.currentMtPE = currentMtPE;
    }

    public String getCurrentMtPEAlias() {
        return currentMtPEAlias;
    }

    public void setCurrentMtPEAlias(String currentMtPEAlias) {
        this.currentMtPEAlias = currentMtPEAlias;
    }

    public String getCurrentParentMtPEAlias() {
        return currentParentMtPEAlias;
    }

    public void setCurrentParentMtPEAlias(String currentParentMtPEAlias) {
        this.currentParentMtPEAlias = currentParentMtPEAlias;
    }

    public String getCurrentFkRelnWithParent() {
        return currentFkRelnWithParent;
    }

    public void setCurrentFkRelnWithParent(String currentFkRelnWithParent) {
        this.currentFkRelnWithParent = currentFkRelnWithParent;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public boolean isAlwaysApplied() {
        return isAlwaysApplied;
    }

    public void setAlwaysApplied(boolean alwaysApplied) {
        isAlwaysApplied = alwaysApplied;
    }

    public boolean isInteractiveFilter() {
        return isInteractiveFilter;
    }

    public void setInteractiveFilter(boolean interactiveFilter) {
        isInteractiveFilter = interactiveFilter;
    }

    public boolean isHavingClause() {
        return isHavingClause;
    }

    public void setHavingClause(boolean havingClause) {
        isHavingClause = havingClause;
    }
}
