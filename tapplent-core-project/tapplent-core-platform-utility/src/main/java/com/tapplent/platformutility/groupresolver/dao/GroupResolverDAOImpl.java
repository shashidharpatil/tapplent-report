package com.tapplent.platformutility.groupresolver.dao;

import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.groupresolver.valueobject.GroupVO;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.platformutility.uilayout.valueobject.GenericQueryAttributesVO;
import com.tapplent.platformutility.uilayout.valueobject.GenericQueryFiltersVO;
import com.tapplent.platformutility.uilayout.valueobject.GenericQueryVO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * Created by tapplent on 12/06/17.
 */
public class GroupResolverDAOImpl extends TapplentBaseDAO implements GroupResolverDAO  {
    @Override
    public List<GroupVO> getAllGroups() {
        List<GroupVO> result = new ArrayList<>();
        String sql = "SELECT * FROM T_PFM_ADM_GROUP WHERE IS_DELETED = 0 AND RECORD_STATE_CODE_FK_ID = 'CURRENT' AND T_PFM_ADM_GROUP.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND T_PFM_ADM_GROUP.SAVE_STATE_CODE_FK_ID = 'SAVED';";
        List<Object> param = new ArrayList<>();
        ResultSet rs = executeSQL(sql, param);
        try {
            while (rs.next()){
                GroupVO groupVO = new GroupVO();
                groupVO.setGroupID(Util.convertByteToString(rs.getBytes("PFM_GROUP_PK_ID")));
                groupVO.setGenericQueryID(Util.convertByteToString(rs.getBytes("GENERIC_QUERY_FK_ID")));
                groupVO.setGroupType(rs.getString("GROUP_TYPE_CODE_FK_ID"));
                result.add(groupVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void deleteAllResolvedGroups() throws SQLException {
        String sql = "DELETE FROM T_PFM_IEU_GROUP_MEMBER;";
        List<Object> param = new ArrayList<>();
        executeUpdate(sql,param);
    }

    @Override
    public void executeUpdate(String sql, Deque<Object> parameters) throws SQLException {
        List<Object> finalParam = new ArrayList<>();
        for(Object param : parameters){
            finalParam.add(param);
        }
        super.executeUpdate(sql, finalParam);
    }

    @Override
    public GenericQueryVO getGenericQuery(String genericQueryID) {
            GenericQueryVO genericQueryVO = new GenericQueryVO();
            List<GenericQueryAttributesVO> genericQueryAttributesVOS = getGenericQueryAttributes(genericQueryID);
            List<GenericQueryFiltersVO> genericQueryFiltersVOS = getGenericQueryFilters(genericQueryID);
//            for(GenericQueryAttributesVO genericQueryAttributesVO : genericQueryAttributesVOS){
//                GenericQueryAttribute genericQueryAttribute = GenericQueryAttribute.fromVO(genericQueryAttributesVO);
//                genericQuery.getAttributes().add(genericQueryAttribute);
//            }
//            for(GenericQueryFiltersVO genericQueryFiltersVO : genericQueryFiltersVOS){
//                GenericQueryFilters genericQueryFilters = GenericQueryFilters.fromVO(genericQueryFiltersVO);
//                genericQuery.getFilters().add(genericQueryFilters);
//            }
            genericQueryVO.setAttributes(genericQueryAttributesVOS);
            genericQueryVO.setFilters(genericQueryFiltersVOS);
            return genericQueryVO;
    }

    @Override
    public void deleteAllResolvedGroupsByMtPE(List<GroupVO> groupsByMtPE) throws SQLException {
        StringBuilder sql = new StringBuilder("DELETE FROM T_PFM_IEU_GROUP_MEMBER WHERE PFM_GROUP_FK_ID IN (");
        List<Object> param = new ArrayList<>();
        for (GroupVO groupVO : groupsByMtPE){
            sql.append("?,");
            param.add(Util.convertStringIdToByteId(groupVO.getGroupID()));
        }
        sql.replace(sql.length()-1, sql.length(), ")");
        executeUpdate(sql.toString(),param);
    }

    private List<GenericQueryFiltersVO> getGenericQueryFilters(String genericQueryID) {
            List<GenericQueryFiltersVO> result = new ArrayList<>();
            String SQL = "SELECT * FROM T_UI_ADM_GENERIC_QUERY_FILTER WHERE GENERIC_QUERY_FK_ID = x?;";
            List<Object> params = new ArrayList<>();
            params.add(Util.remove0x(genericQueryID));
            ResultSet rs = executeSQL(SQL, params);
            try{
                while (rs.next()){
                    GenericQueryFiltersVO genericQueryFiltersVO = new GenericQueryFiltersVO();
                    genericQueryFiltersVO.setFilterID(Util.convertByteToString(rs.getBytes("GENERIC_QUERY_FLTR_PK_ID")));
                    genericQueryFiltersVO.setQueryID(Util.convertByteToString(rs.getBytes("GENERIC_QUERY_FK_ID")));
                    genericQueryFiltersVO.setTargetMtPE(rs.getString("TGT_PROCESS_ELEMENT_CODE_FK_ID"));
                    genericQueryFiltersVO.setTargetMtPEAlias(rs.getString("TGT_PROCESS_ELEMENT_ALIAS_TXT"));
                    genericQueryFiltersVO.setTargetParentMtPEAlias(rs.getString("TGT_PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                    genericQueryFiltersVO.setTargetFkRelnWithParent(rs.getString("TGT_FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                    genericQueryFiltersVO.setAttributePathExpn(rs.getString("CONTROL_ATTRIBUTE_PATH_EXPN"));
                    genericQueryFiltersVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                    genericQueryFiltersVO.setCurrentMtPE(rs.getString("CURR_PROCESS_ELEMENT_CODE_FK_ID"));
                    genericQueryFiltersVO.setCurrentMtPEAlias(rs.getString("CURR_PROCESS_ELEMENT_ALIAS_TXT"));
                    genericQueryFiltersVO.setCurrentParentMtPEAlias(rs.getString("CURR_PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                    genericQueryFiltersVO.setCurrentFkRelnWithParent(rs.getString("CURR_FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                    genericQueryFiltersVO.setAttributeValue(rs.getString("VALUE_EXPN"));
                    genericQueryFiltersVO.setAlwaysApplied(rs.getBoolean("IS_ALWAYS_APPLIED"));
                    //TODO make it correspond to the actual column
                    genericQueryFiltersVO.setInteractiveFilter(rs.getBoolean("IS_INTERACTIVE_FILTER"));
                    try{
                        genericQueryFiltersVO.setHavingClause(rs.getBoolean("IS_HAVING_CLAUSE"));
                    }catch (Exception e){

                    }

                    result.add(genericQueryFiltersVO);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return result;
    }

    private List<GenericQueryAttributesVO> getGenericQueryAttributes(String genericQueryID) {
        List<GenericQueryAttributesVO> result = new ArrayList<>();
        String SQL = "SELECT * FROM T_UI_ADM_GENERIC_QUERY_ATTR WHERE GENERIC_QUERY_FK_ID = x?;";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(genericQueryID));
        ResultSet rs = executeSQL(SQL, params);
        try{
            while (rs.next()){
                GenericQueryAttributesVO genericQueryAttributesVO = new GenericQueryAttributesVO();
                genericQueryAttributesVO.setAttributeID(Util.convertByteToString(rs.getBytes("GENERIC_QUERY_ATTR_PK_ID")));
                genericQueryAttributesVO.setQueryID(Util.convertByteToString(rs.getBytes("GENERIC_QUERY_FK_ID")));
                genericQueryAttributesVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
                genericQueryAttributesVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                genericQueryAttributesVO.setParentMtPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                genericQueryAttributesVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                genericQueryAttributesVO.setAttributePath(rs.getString("ATTR_PATH_EXPN"));
                genericQueryAttributesVO.setAggregate(rs.getBoolean("IS_AGGREGATE"));
                result.add(genericQueryAttributesVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
