package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;

import javax.sql.DataSource;



public class NotificationItemWriter extends JdbcBatchItemWriter<NTFN_NotificationData> {

	public NotificationItemWriter(){
		super();
		DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
		setDataSource(ds);
		setSql(getSQL());	
		setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<NTFN_NotificationData>());
		afterPropertiesSet();
	}
	
	
	public String getSQL(){
		String sql = "UPDATE T_PRN_IEU_NOTIFICATION SET RETURN_STATUS_CODE_FK_ID = 'COMPLETED' WHERE PERSON_NOTIFICATION_PK_ID = :personNotificationPkId";
		return sql;
		
	}
	

}
