package com.tapplent.platformutility.search.builder;


import java.util.Map;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.cache.MetadataObjectRepository;
import com.tapplent.platformutility.common.cache.PersonDetailsObjectRepository;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.metadata.structure.TemplateMapper;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.search.builder.expression.ExprOperand;
import com.tapplent.platformutility.search.builder.expression.LeafOperand;
import com.tapplent.platformutility.search.builder.expression.NodeOperand;
import com.tapplent.platformutility.search.builder.expression.Operator;
import com.tapplent.platformutility.search.builder.joins.JoinLeafOperand;
import com.tapplent.platformutility.search.builder.joins.JoinNodeOperand;
import com.tapplent.platformutility.search.builder.joins.JoinOperand;
import com.tapplent.platformutility.search.impl.SearchContext;
import com.tapplent.platformutility.common.util.StringUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import com.tapplent.tenantresolver.user.UserContext;

public final class SQLBuilder {
	private static final Logger LOG = LogFactory.getLogger(SQLBuilder.class);
	private static final String SPACE = " ";
	private static final String ON = " on ";
	private static final String DOT = ".";
	private static final String EQUAL = " = ";
	private static final String AND = " and ";
	private static final String AS = " as ";
	private static final String INVERTED_COMMA = "'";
	private static final String CONSOLE_BUTTON = "`";
	private static final String PARAMETER = "?";
	private static final String DISTINCT = " DISTINCT ";
	private static final String PAGINATE = "\n LIMIT ? OFFSET ? ";
    private static final String COMMA = " , ";
    private static final String OPEN_BRACKET = "(";
    private static final String CLOSE_BRACKET = ")";
    private static final String JSON_BLOB = " COLUMN_JSON";
    private static final String COLUMN_GET = " COLUMN_GET";
    private static final Integer DEFAULT_PAGE_SIZE = 15;
	private static final Integer DEFAULT_PAGE_COUNT = 1;
    private static final String NEW_LINE = "\n";
    private static final String TAB = "\t";
    private static final String BINARY = "BINARY";
    private static final String AGGREGATE_ALIAS = "###AGGREGATE_ALIAS###";
    private final TSQLBuilder sql;
    private final TSQLBuilder countSql;
    private final SearchContext context;
//    private List<String> groupBys = new ArrayList<>();
//    private boolean groupByNeeded = false;
    
    protected SQLBuilder(SearchContext context) {
        this.context = context;
        sql = new TSQLBuilder();
        countSql = new TSQLBuilder();
    }
    
    public static SQLBuilder getInstance(SearchContext context){
    	return new SQLBuilder(context);
    }
    
    public String getCountSql() {
		return countSql.toString();
	}

	public String build() {
	    buildSelects();
	    buildJoins();
        buildWhereClause();
        buildGroupByClause();
        buildHavingClause();
        buildSort();
        
        String sqlString = sql.toString();
        if(context.isApplyPagination()){
        	// This will be applied when query is being asked for first time.
        	if(context.getPage() < 1){
        		context.setPage(DEFAULT_PAGE_COUNT);
        		context.setPageSize(context.getDefaultPaginationSize());
			}
			// This will be the default page size.
			if (context.getPageSize() == null || context.getPageSize()<1) {
				context.setPageSize(DEFAULT_PAGE_SIZE);
			}
			if(!context.isFunction())
				sqlString = buildPagination(sqlString, context.getPage(), context.getPageSize(), context.getDefaultPaginationSize());
			else {
				sqlString = buildPagination(sqlString, 1, 50, context.getDefaultPaginationSize());
			}

		}else{
			sqlString = buildPagination(sqlString, 1, 50, context.getDefaultPaginationSize());
		}
        LOG.debug("SQL: {}", sqlString);
        LOG.debug("countSQL: {}", countSql.toString());
        return sqlString;
    }

	/**
	 * 
	 */

	private void buildSort() {
    	StringBuilder sortCondition = new StringBuilder();
//    	for(DbSearchTemplate table : tables){
//    		if(table!=null){
//	    		if(table.isPrimaryDO()){
//	    			int sortClauseNumber = table.getSeqNumberToSortMap().size();
//	    			for(int seqNumber =1; seqNumber<= sortClauseNumber; seqNumber++){
//	    				SortData sortData = table.getSeqNumberToSortMap().get(seqNumber);
//	    				if(sortData!=null){
//	    					if(table.getColumnLabelToColumnMap().containsKey(sortData.getAttributePathExpn())){
//	    						SearchField column = table.getColumnLabelToColumnMap().get(sortData.getAttributePathExpn());
//	    						String sortOrder = null;
//	    						if(sortData.isAscending()){
//	    							sortOrder = Sort.ASC.toString();
//	    						}
//	    						else{
//	    							sortOrder = Sort.DESC.toString();
//	    						}
//	    						sortCondition.append(table.getTableAlias()).append(DOT).append(column.getColumnName()).append(SPACE).append(sortOrder).append(COMMA);
//	    					}
//	    				}
//	    			}
//	    		}
//	    	}
//    	}
//    	String sort = sortCondition.toString().trim();
//    	if(sort.endsWith(","))
//    		sort = sort.substring(0, sort.length() -2);
//    	if(StringUtil.isDefined(sort))
//    		sql.ORDER_BY(sort);
    	sortCondition = buildExpression(context.getOrderByClause().getExpressionTree(), false);
    	if(sortCondition!=null){
    		sql.ORDER_BY(sortCondition.toString());
    	}
	}
    
	private String buildPagination(String sqlString, Integer page, Integer pageSize, int defaultPaginationSize) {
        sqlString = sqlString.concat(PAGINATE);
        context.addParameters(pageSize, "Number", false);
        if(page == 2){
			if(defaultPaginationSize > 0){
				context.addParameters(defaultPaginationSize, "Number", false);
			}else{
				context.addParameters(DEFAULT_PAGE_SIZE, "Number", false);
			}
		}else if (page == 1){
			context.addParameters((page - 1) * pageSize, "Number", false);
		}else{
			if(defaultPaginationSize > 0){
				context.addParameters(((page - 2) * pageSize) + defaultPaginationSize, "Number", false);
			}else{
				context.addParameters(((page - 2) * pageSize) + DEFAULT_PAGE_SIZE, "Number", false);
			}
		}
        return sqlString;
    }

//    private void buildGroupByClause() {
//        if (groupBys.size() > 0) {
//            sql.GROUP_BY(groupBys.stream().collect(Collectors.joining(",")));
//        }
//    }

    public SearchContext getContext() {
        return context;
    }
    private void buildGroupByClause() {
    	StringBuilder groupByCondition = buildExpression(context.getGroupByClause().getExpressionTree(), false);
    	if(groupByCondition!=null) {
			sql.GROUP_BY(groupByCondition.toString());
			countSql.GROUP_BY(groupByCondition.toString());
		}
	}
    private void buildWhereClause()  {
//        for (DbSearchTemplate table : tables) {
//        	if(table!= null){
//	            if (table.isPrimaryDO()) {
//	                if(table.getFilters()!=null){
//	                	for(DbSearchFilter filter : table.getFilters()){
//		                	String condition = getWhereCondition(table, filter).toString();
//		                	sql.WHERE(condition);
//		                	countSql.WHERE(condition);
//		                }
//	                }
//	            }
//	        }
//        }
    	StringBuilder whereCondition = buildExpression(context.getWhereClause().getExpressionTree(), true);
    	if(whereCondition!=null){
    		sql.WHERE(whereCondition.toString());
        	countSql.WHERE(whereCondition.toString());
    	}
    }

	private void buildHavingClause() {
		StringBuilder havingCondition = buildExpression(context.getHavingClause().getExpressionTree(), true);
		if(havingCondition!=null){
			sql.HAVING(havingCondition.toString());
			countSql.HAVING(havingCondition.toString());
		}
	}
//	private StringBuilder getWhereCondition(DbSearchTemplate table, DbSearchFilter filter) {
//		StringBuilder condition = null;
//		if(filter!=null && table!=null){
//			String filterColumnLabel = filter.getColumnLabel();
//			SearchField column = table.getColumnLabelToColumnMap().get(filterColumnLabel);
//			if(column == null){
//				LOG.error("The provided attribute name {} for filter does not exist -> "+filterColumnLabel);
//			}
//			else{
//				String columnName = column.getColumnName();
////				String tableAlias = table.getTableAlias();
//				condition = new StringBuilder("(");
//				int counter = 0;
//				for(FilterOperatorValue opVal : filter.getOpVal()){
//					StringBuilder subCondition = null;
//					String dbDataType = filter.getDbDataType();
//					if(counter ==0){
//						subCondition = new StringBuilder("(");
//					}
//					else{
//						subCondition = new StringBuilder(" OR (");
//					}
//					subCondition.append(table.getTableAlias()).append("."+columnName+" ");
//						switch(opVal.getOperator()){
//						case IS_NULL :
//						case IS_NOT_NULL:
//							subCondition.append(opVal.getOperator().getSqlCondition());
//						  	break;
//						case LIKE :
//							subCondition.append(opVal.getOperator().getSqlCondition()).append(PARAMETER);
//							context.addParameters("%" +filter.getData(opVal.getValue().toString(), dbDataType) +"%", dbDataType, true);
//							break;
//						case IN :
//							subCondition.append(opVal.getOperator().getSqlCondition());
//							subCondition.append("(");
//							String[] val = opVal.getValue().toString().split(",");
//							List<String> params = new ArrayList<>();
//			                for (String s : val) {
//			                    params.add(SQLBuilder.PARAMETER);
//			                    context.addParameters(filter.getData(s, dbDataType),dbDataType, true);
//			                }
//			                subCondition.append(params.stream().collect(Collectors.joining(",")));
//			                subCondition.append(")");
//							break;
//						default :
//							subCondition.append(opVal.getOperator().getSqlCondition()).append(PARAMETER);
//							context.addParameters(filter.getData(opVal.getValue().toString(), dbDataType), dbDataType, true);
//						}
//					subCondition.append(")");
//					counter++;
//					condition.append(subCondition);
//				}
//				condition.append(")");
//			}
//		}
//		return condition;
//	}

//	private void buildJoins(DbSearchTemplate[] searchTables, JoinElementNode joins){
//		Map<String, DbSearchTemplate> tableIdTableMap = Arrays.stream(searchTables).collect(Collectors.toMap(DbSearchTemplate::getTableId, t -> t));
//		StringBuilder joinClause = buildJoinClause(tableIdTableMap,joins);
//	}
//	private StringBuilder buildJoinClause(Map<String, DbSearchTemplate> tableIdTableMap, JoinElementNode joins) {
//		if(joins instanceof JoinOperatorsNode){
//			StringBuilder leftClause = buildJoinClause(tableIdTableMap, ((JoinOperatorsNode) joins).getLeftNode());
//			StringBuilder rightClause = buildJoinClause(tableIdTableMap, ((JoinOperatorsNode) joins).getRightNode());
//			StringBuilder joinClause = new StringBuilder(" ("+leftClause+") ");
//			if(((JoinOperatorsNode) joins).getJoinType()!=null){
//				joinClause.append(" ").append(((JoinOperatorsNode) joins).getJoinType()).append(" ");
//				joinClause.append(" (").append(rightClause).append(") ");
//			}
//			if(((JoinOperatorsNode) joins).getFilters()!=null && !((JoinOperatorsNode) joins).getFilters().isEmpty()){
//				
//			}
//			
//		}
//		
//		if(joins instanceof JoinTablesNode){
//			//It is a table and 
//		}
//		return null;
//	}

	private void buildJoins() {
    	
    	
//		Map<String, DbSearchTemplate> tableIdTableMap = Arrays.stream(searchTables).collect(Collectors.toMap(DbSearchTemplate::getTableId, t -> t));
//        Map<String, List<DbSearchRelation>> tableIdRelationMap = tableRelations != null && tableRelations.length > 0 ? Arrays.stream(tableRelations).collect(Collectors.groupingBy(DbSearchRelation::getTableFrom)) : new HashMap<>();
//        Map<String, String> relationMatrix = new HashMap<>();
//
//        String previousTableId = null;
//        for (int i = 0; i < searchTables.length; i++) {
//
//            DbSearchTemplate table = searchTables[i];
//
//            if (i==0) {
//                sql.FROM(table.getTable() + SQLBuilder.SPACE + table.getTableAlias());
//                countSql.FROM(table.getTable() + SQLBuilder.SPACE + table.getTableAlias());
//            } else {
//            	  
//                DbSearchTemplate previousTable = tableIdTableMap.get(previousTableId);
//                List<DbSearchRelation> relations = Optional.ofNullable(tableIdRelationMap.get(previousTable.getTableId())).orElseGet(ArrayList::new);
//                relations.stream().forEachOrdered(relation -> createJoin(relation.getConnectType(), tableIdTableMap.get(relation.getTableFrom()), tableIdTableMap.get(relation.getTableTo()), relation, relationMatrix));
//            }
//            previousTableId = table.getTableId();
//        }
        StringBuilder fromClause = buildJoins(context.getFromClause().getJoinExpressionTree());
        sql.FROM(fromClause.toString());
        countSql.FROM(fromClause.toString());
    }

    private void createJoin(String connectType, DbSearchTemplate fromTable, DbSearchTemplate toTable, DbSearchRelation relation, Map<String, String> relationMatrix) {
        if (relationMatrix.entrySet().stream().anyMatch(entry -> entry.getKey().equals(fromTable.getTableId()) && entry.getValue().equals(toTable.getTableId()))) {
            return;
        }
        switch (connectType) {
            case SearchConstants.INNER_JOIN:
                sql.INNER_JOIN(createJoinInfo(fromTable, toTable, relation));
                countSql.INNER_JOIN(createJoinInfo(fromTable, toTable, relation));
                break;
            case SearchConstants.LEFT_OUTER_JOIN:
                sql.LEFT_OUTER_JOIN(createJoinInfo(fromTable, toTable, relation));
                countSql.LEFT_OUTER_JOIN(createJoinInfo(fromTable, toTable, relation));
                break;
            case SearchConstants.RIGHT_OUTER_JOIN:
                sql.RIGHT_OUTER_JOIN(createJoinInfo(fromTable, toTable, relation));
                countSql.RIGHT_OUTER_JOIN(createJoinInfo(fromTable, toTable, relation));
                break;
            default:
                sql.JOIN(createJoinInfo(fromTable, toTable, relation));
                countSql.JOIN(createJoinInfo(fromTable, toTable, relation));


        }
        relationMatrix.putIfAbsent(fromTable.getTableId(), toTable.getTableId());
    }

    private String createJoinInfo(DbSearchTemplate fromTable, DbSearchTemplate toTable, DbSearchRelation relation) {
        StringBuilder b = new StringBuilder();
        String fromTableAlias = fromTable.getTableAlias();
        String toTableAlias = toTable.getTableAlias();
        b.append(toTable.getTable())
                .append(SPACE)
                .append(toTableAlias)
                .append(ON)
                .append(toTableAlias)
                .append(DOT)
                .append(relation.getTableToKey())
                .append(EQUAL)
                .append(fromTableAlias)
                .append(DOT)
                .append(relation.getTableFromKey());
        return b.toString();
    }
    private StringBuilder buildJoins(JoinOperand root){
    	StringBuilder expression = null;
    	if(root==null)
    		return null;
    	if(root instanceof JoinLeafOperand){
    		JoinLeafOperand leafOperand = (JoinLeafOperand)root;
    		expression = new StringBuilder(SPACE+leafOperand.getTableName()+SPACE+leafOperand.getTableAlias());
    	}
    	if(root instanceof JoinNodeOperand){
    		JoinNodeOperand nodeOperand = (JoinNodeOperand)root;
    		StringBuilder leftExpression = buildJoins(nodeOperand.getLeft());
    		StringBuilder rightExpression = buildJoins(nodeOperand.getRight());
    		StringBuilder onExpression = buildExpression(nodeOperand.getOnExpression().getExpressionTree(), true);
    		expression = new StringBuilder();
    		expression.append(leftExpression)
    				  .append(NEW_LINE)
    				  .append(nodeOperand.getJoinType().getJoin())
    				  .append(SPACE)
    				  .append(rightExpression);
    		if(onExpression!=null) {
				expression.append(NEW_LINE)
						.append(TAB)
						.append(ON)
						.append(onExpression);
			}
    	}
    	return expression;
    }
    private StringBuilder buildExpression(ExprOperand root, Boolean isParamToBeAddedToCountParams){
    	StringBuilder expression = null;
    	if(root==null)
    		return null;
    	if(root instanceof LeafOperand){
    		LeafOperand leafRoot = (LeafOperand) root;
    		if(leafRoot.getIsColumnName()){
				if(leafRoot.getIsGlocalized()) {
					if (context.isResolveG11n()) {
						PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
						UserContext user = TenantContextHolder.getUserContext();
						MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
						Person person = personService.getPersonDetails(user.getPersonId());
						expression = new StringBuilder(" getG11nValue('" + person.getPersonPreferences().getLocaleCodeFkId()+"','"+person.getOrgDefaultLocale()+"','"+metadataService.getTenantDefaultLocale()+ "'," + leafRoot.getTableAlias() + "." + leafRoot.getColumnName() + ")");
					} else {
						expression = new StringBuilder(" " + leafRoot.getTableAlias() + "." + leafRoot.getColumnName());
					}
				} else {
					expression = new StringBuilder(" " + leafRoot.getTableAlias() + "." + leafRoot.getColumnName());
				}
    		}else if(leafRoot.isColumnAlias()){
				expression = new StringBuilder(CONSOLE_BUTTON+ leafRoot.getValue()+ CONSOLE_BUTTON);
			}else if(leafRoot.getDataType().equals("T_KEYWORD")){
                expression = new StringBuilder(leafRoot.getValue());
            }else{
				if(leafRoot.getDataType().equals("TEXT")){
					expression = new StringBuilder(PARAMETER + " COLLATE utf8_unicode_ci");
					String stringParameter = leafRoot.getValue();
					context.addParameters(stringParameter, leafRoot.getDataType(), isParamToBeAddedToCountParams);
				}else{
					expression = new StringBuilder(PARAMETER);
					context.addParameters(leafRoot.getValue(), leafRoot.getDataType(), isParamToBeAddedToCountParams);
				}

			}
    	}
    	if(root instanceof NodeOperand){
    		NodeOperand node = (NodeOperand) root;
    		StringBuilder leftExpression = buildExpression(node.getLeft(), isParamToBeAddedToCountParams);
    		StringBuilder rightExpression = buildExpression(node.getRight(), isParamToBeAddedToCountParams);
    		expression = buildExpression(leftExpression, rightExpression, node.getOperator());
    	}
    	return expression;
    }
    private StringBuilder buildSelectExpression(ExprOperand root){
    	if(root==null)
    		return null;
    	StringBuilder expression = new StringBuilder();
    	if(root instanceof LeafOperand){
    		LeafOperand leafRoot = (LeafOperand) root;
    		if(leafRoot.getIsColumnName()) {
    			/*
    			 * Cases for Blob
    			 * 1. If it is a first level G11N Blob. 
    			 * 		e.g. LAUNCH_TITLE_G11N_BLOB desired output => COLUMN_JSON(LAUNCH_TITLE_G11N_BLOB) as 'columnLabel'
    			 * 2. If it is a second level G11NBlob. So, we have a container blob inside which we have a g11n blob 
    			 * 		e.g. TemplateSpecificProperty.VersionTitleLabel inside PROPERTY_BLOB in T_BT_ADM_BT_SPECIFIC_PROPERTY desired output => COLUMN_JSON(COLUMN_GET(PROPERTY_BLOB,'TemplateSpecificProperty.VersionTitleLabel' as BINARY))
    			 * 3. If it is a first level Blob having DOAs inside it. 
    			 * 		e.g. LaunchContent.MobileImage inside LAUNCH_IMAGE_BLOB in LaunchContent desired output => COLUMN_GET(LAUNCH_IMAGE_BLOB, 'LaunchContent.MobileImage' as dbDataType)   
    			 */
//    			if(leafRoot.getIsGlocalized()){
				//TODO revisit the condition
				if (leafRoot.getDataType().equals("T_BLOB")) {
					expression.append(JSON_BLOB).append(OPEN_BRACKET);
					if (StringUtil.isDefined(leafRoot.getContainerBlobName())) {
						expression.append(COLUMN_GET).append(OPEN_BRACKET)
								.append(leafRoot.getTableAlias() + "." + leafRoot.getContainerBlobName())
								.append(COMMA).append(INVERTED_COMMA)
								.append(leafRoot.getDoAttributeCode())
								.append(INVERTED_COMMA)
								.append(AS)
								.append(BINARY)
								.append(CLOSE_BRACKET);
					} else {
						expression.append(leafRoot.getTableAlias() + "." + leafRoot.getColumnName());
					}
					expression.append(CLOSE_BRACKET);
//							.append(AS)
//							.append(leafRoot.getColumnAlias());
				} else {
					if (StringUtil.isDefined(leafRoot.getContainerBlobName())) {
						expression.append(COLUMN_GET).append(OPEN_BRACKET)
								.append(leafRoot.getTableAlias() + "." + leafRoot.getContainerBlobName())
								.append(COMMA).append(INVERTED_COMMA)
								.append(leafRoot.getDoAttributeCode())
								.append(INVERTED_COMMA)
								.append(AS)
								.append(Util.getBlobDataTypeFromEntityDataType(leafRoot.getDataType()))
								.append(CLOSE_BRACKET);
//								.append(AS)
//								.append(leafRoot.getColumnAlias());
					} else {
						if(leafRoot.getIsGlocalized()){
							if(context.isResolveG11n()) {
								PersonDetailsObjectRepository personService = TenantAwareCache.getPersonRepository();
								UserContext user = TenantContextHolder.getUserContext();
								Person person = personService.getPersonDetails(user.getPersonId());
								MetadataObjectRepository metadataService = TenantAwareCache.getMetaDataRepository();
								expression = new StringBuilder(" getG11nValue('" + person.getPersonPreferences().getLocaleCodeFkId()+"','"+person.getOrgDefaultLocale()+"','"+metadataService.getTenantDefaultLocale()+ "'," + leafRoot.getTableAlias() + "." + leafRoot.getColumnName() + ")");
							}else{
								expression = new StringBuilder(" " + leafRoot.getTableAlias() + "." + leafRoot.getColumnName());
							}
						}else {
							expression = new StringBuilder(" " + leafRoot.getTableAlias() + "." + leafRoot.getColumnName());
						}
					}
				}
			}else if(leafRoot.isColumnAlias()){
				expression = new StringBuilder(CONSOLE_BUTTON+ leafRoot.getValue()+ CONSOLE_BUTTON);
			}else if(leafRoot.getDataType().equals("T_KEYWORD")){
                expression = new StringBuilder(leafRoot.getValue());
            }else{
				expression = new StringBuilder(PARAMETER);
				if(context.getDatabasePrimaryKey()!=null) {
					context.addParameters(leafRoot.getValue(), leafRoot.getDataType(), false);
				}else{
					context.addParameters(leafRoot.getValue(), leafRoot.getDataType(), true);
				}
			}
    	}
    	if(root instanceof NodeOperand){
    		NodeOperand node = (NodeOperand) root;
    		StringBuilder leftExpression;
    		StringBuilder rightExpression;
    		/* Here if node operator is a function the select expression will changes accordingly */
    		if(isOperatorAFunction(node.getOperator())){
				if(context.getDatabasePrimaryKey()!=null) {
					leftExpression = buildExpression(node.getLeft(), false);
					rightExpression = buildExpression(node.getRight(), false);
				}else{
					leftExpression = buildExpression(node.getLeft(), true);
					rightExpression = buildExpression(node.getRight(), true);
				}
//    			leftExpression = buildExpression(node.getLeft(), true);
//    			rightExpression = buildExpression(node.getRight(), true);
    		}else{
    			leftExpression = buildSelectExpression(node.getLeft());
        		rightExpression = buildSelectExpression(node.getRight());
    		}
    		expression = buildExpression(leftExpression, rightExpression, node.getOperator());
    	}
    	return expression;
    }
    
	private StringBuilder buildExpression(StringBuilder leftExpression, StringBuilder rightExpression, Operator operator) {
		StringBuilder expression = null;
		switch(operator){
			case COMMA: expression = new StringBuilder(leftExpression+operator.getSqlCondition()+rightExpression);
				break;
			case IS_NULL:
			case IS_NOT_NULL: expression = new StringBuilder(leftExpression+operator.getSqlCondition());
				break;
			case LIKE: expression = new StringBuilder(leftExpression+operator.getSqlCondition()+rightExpression);
				break;
			case IN: expression = new StringBuilder(leftExpression+operator.getSqlCondition()+OPEN_BRACKET+rightExpression+CLOSE_BRACKET);
				break;
			case FIELD: expression = new StringBuilder(operator.getSqlCondition()+OPEN_BRACKET+leftExpression+CLOSE_BRACKET + "DESC");
				break;
			case DBMIN:
			case DBMAX:
			case MATCH:
            case DATEDIFF:
			case DATESUB:
			case DATEADD:
			case YEAR:
            case DBAVERAGE:
			case CONCAT:
			case DAY:
			case MONTH:
			case MOD:
			case DBSUM:
			case DATE_FORMAT:
			case COALESCE:
			case LEFT:
			case WEEKDAY:
			case LASTDAY:
			case SUBSTRING:
			case DBCOUNT: expression = new StringBuilder(operator.getSqlCondition()+OPEN_BRACKET+leftExpression+CLOSE_BRACKET);
				break;
			case EXCHANGERATE:
				Person person = TenantAwareCache.getPersonRepository().getPersonDetails(TenantContextHolder.getUserContext().getPersonId());
				expression = new StringBuilder(operator.getSqlCondition()+OPEN_BRACKET+leftExpression+" ,'"+person.getPersonPreferences().getCurrencyCodeFkId()+"'"+CLOSE_BRACKET);
				break;
			case G11N_PLURAL:
			case G11N_STATIC:
				person = TenantAwareCache.getPersonRepository().getPersonDetails(TenantContextHolder.getUserContext().getPersonId());
				expression = new StringBuilder(operator.getSqlCondition()+OPEN_BRACKET+leftExpression+ " ,'"+person.getPersonPreferences().getLocaleCodeFkId()+"'"+CLOSE_BRACKET);
				break;
			case APPEND_IS_NULL: expression = new StringBuilder(leftExpression+SPACE+operator.getSqlCondition());
				break;
			case ASC:
			case DESC: expression = new StringBuilder(leftExpression+SPACE+operator.getSqlCondition());
				break;
			case INTCAST: expression = new StringBuilder(operator.getSqlCondition()+OPEN_BRACKET+leftExpression+ " AS INT" +CLOSE_BRACKET);
				break;
			case INTERVAL_YR: expression =  new StringBuilder(operator.getSqlCondition()+SPACE+leftExpression+ " YEAR");
				break;
			case INTERVAL_MONTH: expression =  new StringBuilder(operator.getSqlCondition()+SPACE+leftExpression+ " MONTH");
				break;
			case INTERVAL_DAY: expression =  new StringBuilder(operator.getSqlCondition()+SPACE+leftExpression+ " DAY");
				break;
			case INTERVAL_WEEK: expression =  new StringBuilder(operator.getSqlCondition()+SPACE+leftExpression+ " WEEK");
				break;
			case BW: expression = new StringBuilder(leftExpression+operator.getSqlCondition()+rightExpression);
				break;
			case AGAINST_IN_BOOLEAN_MODE:
					expression = new StringBuilder(leftExpression+operator.getSqlCondition()+OPEN_BRACKET+rightExpression+" IN BOOLEAN MODE"+CLOSE_BRACKET);
				break;
			case AS: expression = new StringBuilder(leftExpression + operator.getSqlCondition() + rightExpression);
				break;
			case DISTINCT: expression = new StringBuilder(operator.getSqlCondition()+SPACE+leftExpression);
				break;
			case TODAY: expression =  new StringBuilder(operator.getSqlCondition());
				break;
			default: expression = new StringBuilder(OPEN_BRACKET+leftExpression+CLOSE_BRACKET+operator.getSqlCondition()+OPEN_BRACKET+rightExpression+CLOSE_BRACKET);
		}
		return expression;
	}
    private void buildSelects() {
//        StringBuilder b = new StringBuilder();
//        Map<String, String> columnAndAliasMap = new HashMap<>();
//        Map<String, String> columnLabelToAttributeNameMap = new HashMap<>();
//        for (DbSearchTemplate template : sqlSearchTemplates) {
//        	if(template!=null){
//	            if (template.getColumns() != null) {
//	                for (SearchFields searchField : template.getColumns()) {
//	                    StringBuilder selectClauseData = new StringBuilder();
//	                    if (searchField.isAggFunction()) {
//	//                        selectClauseData.append(getFunctionSearchClause(template.getTableAlias(), searchField));
////	                        groupByNeeded = true;
//	                    } else {
//	                    	if(searchField.getDataType().equals("T_BLOB")){
//	                    		selectClauseData.append(JSON_BLOB).append(OPEN_BRACKET)
//	                    		.append(template.getTableAlias()).append(DOT).append(searchField.getColumnName())
//	                    		.append(CLOSE_BRACKET);
//	                    		groupBys.add(selectClauseData.toString());
//	                    	}else{
//	                    		selectClauseData.append(template.getTableAlias()).append(DOT).append(searchField.getColumnName());
//		                        groupBys.add(selectClauseData.toString());
//	                    	}
//	                    }
//	                    String columnAlias = searchField.getColumnAlias();
//	                    String columnAliasWithoutQuotes = columnAlias.substring(1, columnAlias.length() - 1);
//	                    if (columnLabelToAttributeNameMap.containsKey(columnAliasWithoutQuotes)) {
//	                        throw new RuntimeException("Duplicate Aliases Found for property {" + columnAliasWithoutQuotes + "}");
//	                    }
//	                    if (searchField.isAggFunction()) {
//	                        columnLabelToAttributeNameMap.put(columnAliasWithoutQuotes, searchField.getAttributeAlias());
//	                    } else if (StringUtil.isDefined(searchField.getAttributeAlias())) {
//	                        columnLabelToAttributeNameMap.put(columnAliasWithoutQuotes, searchField.getAttributeAlias());
//	                    } else {
//	                        columnLabelToAttributeNameMap.put(columnAliasWithoutQuotes, searchField.getAttributePathExpn());
//	                    }
//	                    if (searchField.isDistinct()) selectClauseData.insert(0, DISTINCT);
//	                    columnAndAliasMap.put(selectClauseData.toString(), columnAlias);
//	                }
//	            }
//	        }
//        }
//        for (Map.Entry<String, String> entry : columnAndAliasMap.entrySet()) {
//            b.append(entry.getKey()).append(AS).append(entry.getValue()).append(",");
//        }
//        String selectClause = b.toString();
//        if (selectClause.endsWith(",")) {
//            selectClause = selectClause.substring(0, selectClause.length() - 1);
//        }
        String selectClauseFromExpression = buildSelectExpression(context.getSelectClause().getExpressionTree()).toString();
        if(context.isDistinctSelect()){
        	sql.SELECT_DISTINCT(selectClauseFromExpression);
        }else{
        	sql.SELECT(selectClauseFromExpression);
        }
		//Building countQuery
		if(!context.isFunction()) {
			if(context.getDatabasePrimaryKey()!=null) {
				String CountQuerySelect = "COUNT(DISTINCT " + context.getDatabasePrimaryKey().getTableAlias() + "." + context.getDatabasePrimaryKey().getColumnName() + ") as '#####COUNT#####' ";
				countSql.SELECT(CountQuerySelect);
			}else{
				String CountQuerySelect = "COUNT(DISTINCT " +selectClauseFromExpression.replaceAll("(AS `.*?`)","") + ") as '#####COUNT#####' ";
				countSql.SELECT(CountQuerySelect);
			}
		}
    }
    
	/**
	 * @param operator
	 * @return
	 */
	private boolean isOperatorAFunction(Operator operator) {
		// TODO Auto-generated method stub
		return Util.isOperatorAFunction(operator);
	}

//    private String getFunctionSearchClause(String tableAlias, SearchField searchField) {
//        StringBuilder s = new StringBuilder();
//        switch (searchField.getSearchFunction()) {
//            case AVG:
//                s.append("avg");
//                break;
//            case SUM:
//                s.append("sum");
//                break;
//            case MAX:
//                s.append("max");
//                break;
//            case MIN:
//                s.append("min");
//                break;
//            case COUNT:
//                s.append("count");
//                break;
//            default:
//                throw new UnsupportedOperationException("Invalid function:" + searchField.getSearchFunction());
//        }
//        s.append("(").append(tableAlias).append(DOT).append(searchField.getColumnName()).append(")");
//        return s.toString();
//    }

//    public Stack<?> getParameters() {
//        return context.getParameters();
//    }
}

//}
