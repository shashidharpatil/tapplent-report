package com.tapplent.platformutility.uilayout.valueobject;

import com.tapplent.platformutility.layout.valueObject.IconVO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sripat on 01/11/16.
 */
public class ScreenSectionInstanceVO implements Serializable {

    private String screenSectionInstancePkId;
    private String screenInstanceFkId;
    private String screenSectionInstanceName;
    private String sectionMasterCodeFkId;
    private String parentSectionInstanceFkId;
    private Boolean isShowAtInitialLoad;
    private String secnUIType;
    private String secnControlPos;
    private String onLoadGenericQuery;
    private boolean isStaticDataSection;
    private int themeSeqNumPosInt;
    private int disSeqNumber;
    private int minMandatoryRecordCount;
    private String noDataMessageG11nBigTxt;
    private String noDataMessageIconCode;
    private String noDataMessageImageId;
    private boolean isPaginated;
    private String sUMtPE;
    private String sUMtPEAlias;
    private String sUParentMTPEAlias;
    private String suFkRelnWithParent;
    private String sUAttributePathExpn;

    public String getScreenSectionInstancePkId() {
        return screenSectionInstancePkId;
    }

    public void setScreenSectionInstancePkId(String screenSectionInstancePkId) {
        this.screenSectionInstancePkId = screenSectionInstancePkId;
    }

    public String getSectionMasterCodeFkId() {
        return sectionMasterCodeFkId;
    }

    public void setSectionMasterCodeFkId(String sectionMasterCodeFkId) {
        this.sectionMasterCodeFkId = sectionMasterCodeFkId;
    }

    public String getParentSectionInstanceFkId() {
        return parentSectionInstanceFkId;
    }

    public void setParentSectionInstanceFkId(String parentSectionInstanceFkId) {
        this.parentSectionInstanceFkId = parentSectionInstanceFkId;
    }

    public Boolean getShowAtInitialLoad() {
        return isShowAtInitialLoad;
    }

    public void setShowAtInitialLoad(Boolean showAtInitialLoad) {
        isShowAtInitialLoad = showAtInitialLoad;
    }

    public String getSecnUIType() {
        return secnUIType;
    }

    public void setSecnUIType(String secnUIType) {
        this.secnUIType = secnUIType;
    }

    public String getSecnControlPos() {
        return secnControlPos;
    }

    public void setSecnControlPos(String secnControlPos) {
        this.secnControlPos = secnControlPos;
    }

    public boolean isStaticDataSection() {
        return isStaticDataSection;
    }

    public void setStaticDataSection(boolean staticDataSection) {
        isStaticDataSection = staticDataSection;
    }

    public int getThemeSeqNumPosInt() {
        return themeSeqNumPosInt;
    }

    public void setThemeSeqNumPosInt(int themeSeqNumPosInt) {
        this.themeSeqNumPosInt = themeSeqNumPosInt;
    }

    public String getNoDataMessageG11nBigTxt() {
        return noDataMessageG11nBigTxt;
    }

    public void setNoDataMessageG11nBigTxt(String noDataMessageG11nBigTxt) {
        this.noDataMessageG11nBigTxt = noDataMessageG11nBigTxt;
    }

    public String getNoDataMessageIconCode() {
        return noDataMessageIconCode;
    }

    public void setNoDataMessageIconCode(String noDataMessageIconCode) {
        this.noDataMessageIconCode = noDataMessageIconCode;
    }

    public String getNoDataMessageImageId() {
        return noDataMessageImageId;
    }

    public void setNoDataMessageImageId(String noDataMessageImageId) {
        this.noDataMessageImageId = noDataMessageImageId;
    }

    public String getScreenInstanceFkId() {
        return screenInstanceFkId;
    }

    public void setScreenInstanceFkId(String screenInstanceFkId) {
        this.screenInstanceFkId = screenInstanceFkId;
    }

    public String getOnLoadGenericQuery() {
        return onLoadGenericQuery;
    }

    public void setOnLoadGenericQuery(String onLoadGenericQuery) {
        this.onLoadGenericQuery = onLoadGenericQuery;
    }

    public String getScreenSectionInstanceName() {
        return screenSectionInstanceName;
    }

    public void setScreenSectionInstanceName(String screenSectionInstanceName) {
        this.screenSectionInstanceName = screenSectionInstanceName;
    }

    public int getDisSeqNumber() {
        return disSeqNumber;
    }

    public void setDisSeqNumber(int disSeqNumber) {
        this.disSeqNumber = disSeqNumber;
    }

    public boolean isPaginated() {
        return isPaginated;
    }

    public void setPaginated(boolean paginated) {
        isPaginated = paginated;
    }

    public String getsUMtPE() {
        return sUMtPE;
    }

    public void setsUMtPE(String sUMtPE) {
        this.sUMtPE = sUMtPE;
    }

    public String getsUMtPEAlias() {
        return sUMtPEAlias;
    }

    public void setsUMtPEAlias(String sUMtPEAlias) {
        this.sUMtPEAlias = sUMtPEAlias;
    }

    public String getsUParentMTPEAlias() {
        return sUParentMTPEAlias;
    }

    public void setsUParentMTPEAlias(String sUParentMTPEAlias) {
        this.sUParentMTPEAlias = sUParentMTPEAlias;
    }

    public String getSuFkRelnWithParent() {
        return suFkRelnWithParent;
    }

    public void setSuFkRelnWithParent(String suFkRelnWithParent) {
        this.suFkRelnWithParent = suFkRelnWithParent;
    }

    public String getsUAttributePathExpn() {
        return sUAttributePathExpn;
    }

    public void setsUAttributePathExpn(String sUAttributePathExpn) {
        this.sUAttributePathExpn = sUAttributePathExpn;
    }

    public int getMinMandatoryRecordCount() {
        return minMandatoryRecordCount;
    }

    public void setMinMandatoryRecordCount(int minMandatoryRecordCount) {
        this.minMandatoryRecordCount = minMandatoryRecordCount;
    }
}
