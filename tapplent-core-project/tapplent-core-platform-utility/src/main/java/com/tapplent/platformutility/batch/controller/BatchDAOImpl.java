package com.tapplent.platformutility.batch.controller;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.SchedulerInstanceModel;
import com.tapplent.platformutility.batch.model.SchedulerModel;
import com.tapplent.platformutility.batch.scheduler.SchedulerResolver;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.util.*;

/**
 * Created by sripat on 30/08/16.
 */
public class BatchDAOImpl extends TapplentBaseDAO implements BatchDAO {

    @Transactional
    @Override
    public void initializeBatch() {
        try {
            String sql = "select * from T_SCH_IEU_SCHEDULER_INSTANCE where SCHEDULED_DATETIME  <=now() " +
                    "and EXEC_STATUS_CODE_FK_ID is null and JOB_TYPE_CODE_FK_ID not in (select JOB_TYPE_CODE_FK_ID from T_SCH_IEU_SCHEDULER_INSTANCE where " +
                    "EXEC_STATUS_CODE_FK_ID = 'STARTED');";
            ResultSet resultSet = executeSQL(sql, new ArrayList<>());
            List<SchedulerInstanceModel> schInstance = new ArrayList<SchedulerInstanceModel>();

            while (resultSet.next()) {
                SchedulerInstanceModel schedulerInstanceModel = new SchedulerInstanceModel();
                schedulerInstanceModel.setSchedulerInstancePkId(Util.convertByteToString(resultSet.getBytes(Constants.schedulerInstancePkId)));
                schedulerInstanceModel.setSchedDefnFkId(Util.convertByteToString(resultSet.getBytes(Constants.schedDefnFkId)));
                schedulerInstanceModel.setExecutionVersionId(Util.convertByteToString(resultSet.getBytes(Constants.executionVersionId)));
                schedulerInstanceModel.setJobTypeCodeFkId(resultSet.getString(Constants.jobTypeCodeFkId));
                schedulerInstanceModel.setScheduledDatetime(resultSet.getString(Constants.scheduledDatetime));
                schedulerInstanceModel.setExecutionStartDatetime(resultSet.getString(Constants.executionStartDatetime));
                schedulerInstanceModel.setExecutionEndDatetime(resultSet.getString(Constants.executionEndDatetime));
                schedulerInstanceModel.setExecStatusCodeFkId(resultSet.getString(Constants.execStatusCodeFkId));
                schedulerInstanceModel.setExitCodeFkId(resultSet.getString(Constants.exitCodeFkId));
                schedulerInstanceModel.setExitMessageBigTxt(resultSet.getString(Constants.exitMessageBigTxt));
                if (schedulerInstanceModel.getJobTypeCodeFkId().equals("DOCGEN")) {
                    DocGen docGen = new DocGen(schedulerInstanceModel);
                    docGen.call();
                }
                if (schedulerInstanceModel.getJobTypeCodeFkId().equals("POST")) {
                    Post post = new Post(schedulerInstanceModel);
                    post.call();
                }
                if (schedulerInstanceModel.getJobTypeCodeFkId().equals("NOTIFICATION")) {
                    Notification notification = new Notification(schedulerInstanceModel);
                    notification.call();
                }
                if (schedulerInstanceModel.getJobTypeCodeFkId().equals("VALIDATE")) {
                    AccountValidation accountValidation = new AccountValidation(schedulerInstanceModel);
                    accountValidation.call();
                }
                if (schedulerInstanceModel.getJobTypeCodeFkId().equals("RECORD_STATE_CALCULATION")) {
                    RecordStateCalc notification = new RecordStateCalc(schedulerInstanceModel);
                    notification.call();
                    //resolveSchInstances(false,schedulerInstanceModel.getSchedulerInstancePkId());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    @Override
    public void resolveSchInstances(boolean isInitial,String jobID) {
        try {
            if(!isInitial){
                String sql2 = "UPDATE T_SCH_IEU_SCHEDULER_INSTANCE set EXEC_STATUS_CODE_FK_ID = 'COMPLETED' where T_SCH_IEU_SCHEDULER_INSTANCE.SCHEDULER_INSTANCE_PK_ID="+jobID;
                int result = executeUpdate(sql2, null);
            }
            String sql = "select T_SCH_ADM_SCHED_DEFN.SCHED_DEFN_PK_ID," + "T_SCH_ADM_SCHED_DEFN.JOB_TYPE_CODE_FK_ID,"
                    + "T_SCH_ADM_SCHED_DEFN.IS_ONE_TIME_SCHED," + "T_SCH_ADM_SCHED_DEFN.IS_RECURRING_SCHED,"
                    + "T_SCH_ADM_SCHED_DEFN.ONE_TIME_DATETIME," + "T_SCH_ADM_SCHED_GROUPER.SCHED_GROUPER_PK_ID,"
                    + "T_SCH_ADM_SCHED_GROUPER.SCHED_DRIVING_FREQ_CODE_FK_ID,"
                    + "T_SCH_ADM_SCHED_GROUPER.SCHED_DRIVING_INTERVAL_POS_INT,"
                    + "T_SCH_ADM_SCHED_GROUPER.SCHED_EXEC_START_DATETIME,"
                    + "T_SCH_ADM_SCHED_GROUPER.SCHED_EXEC_END_DATETIME,"
                    + "T_SCH_ADM_SCHED_GROUPER.IS_EXECUTION_NEVER_ENDS," + "T_SCH_ADM_SCHED_RCR_FREQ.SCHED_RCR_FREQ_PK_ID,"
                    + "T_SCH_ADM_SCHED_RCR_FREQ.FREQUENCY_CODE_FK_ID," + "T_SCH_ADM_SCHED_RCR_FREQ.INTERVAL_POS_INT,"
                    + "T_SCH_ADM_SCHED_RCR_FREQ.ABSOLUTE_DATETIME "
                    + "from T_SCH_ADM_SCHED_DEFN inner join T_SCH_ADM_SCHED_GROUPER "
                    + "on T_SCH_ADM_SCHED_DEFN.SCHED_DEFN_PK_ID = T_SCH_ADM_SCHED_GROUPER.SCHED_DEFN_FK_ID "
                    + "inner join T_SCH_ADM_SCHED_RCR_FREQ on T_SCH_ADM_SCHED_GROUPER.SCHED_GROUPER_PK_ID = "
                    + "T_SCH_ADM_SCHED_RCR_FREQ.SCHED_GROUPER_FK_ID";
            ResultSet rs = executeSQL(sql, new ArrayList<>());

            Map<String, List<SchedulerModel>> map = new HashMap<>();
            while (rs.next()) {
                // Retrieve by column name
                SchedulerModel s = new SchedulerModel();
                s.setSchedDefnPkId(Util.convertByteToString(rs.getBytes(Constants.schedDefnPkId)));
                s.setJobTypeCodeFkId(rs.getString(Constants.jobTypeCodeFkId));
                s.setOneTimeSched(rs.getBoolean(Constants.isOneTimeSched));
                s.setRecurringSched(rs.getBoolean(Constants.isRecurringSched));
                s.setOneTimeDatetime(rs.getString(Constants.oneTimeDatetime));
                s.setSchedGrouperPkId(rs.getString(Constants.schedGrouperPkId));
                s.setSchedDrivingFreqCodeFkId(rs.getString(Constants.schedDrivingFreqCodeFkId));
                s.setSchedDrivingIntervalPosInt(rs.getInt(Constants.schedDrivingIntervalPosInt));
                s.setSchedExecStartDatetime(rs.getString(Constants.schedExecStartDatetime));
                s.setSchedExecEndDatetime(rs.getString(Constants.schedExecEndDatetime));
                s.setExecutionNeverEnds(rs.getBoolean(Constants.isExecutionNeverEnds));
                s.setSchedRcrFreqPkId(rs.getString(Constants.schedRcrFreqPkId));
                s.setFrequencyCodeFkId(rs.getString(Constants.frequencyCodeFkId));
                s.setIntervalPosInt(rs.getString(Constants.intervalPosInt));
                s.setAbsoluteDatetime(rs.getString(Constants.absoluteDatetime));
                if (map.containsKey(s.getSchedDefnPkId() + s.getSchedGrouperPkId())) {
                    map.get(s.getSchedDefnPkId() + s.getSchedGrouperPkId()).add(s);
                } else {
                    List<SchedulerModel> list = new ArrayList<SchedulerModel>();
                    list.add(s);
                    map.put(s.getSchedDefnPkId() + s.getSchedGrouperPkId(), list);
                }

            }
            SchedulerResolver schedulerResolver = new SchedulerResolver();
            Iterator it = map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                schedulerResolver.resolve((List<SchedulerModel>) pair.getValue());
                it.remove(); // avoids a ConcurrentModificationException
            }
            List<SchedulerInstanceModel> sInstance = schedulerResolver.sInstance;

            for (int i = 0; i < sInstance.size(); i++) {
                SchedulerInstanceModel s1 = sInstance.get(i);
                String sql1 = "insert into T_SCH_IEU_SCHEDULER_INSTANCE ("
                        + "SCHEDULER_INSTANCE_PK_ID,"
                        + "EXECUTION_VERSION_ID,"
                        + "IS_DELETED,"
                        + "SCHED_DEFN_FK_ID,"
                        + "JOB_TYPE_CODE_FK_ID,"
                        + "SCHEDULED_DATETIME) values (" + Common.getUUID() + "," + Common.getUUID() + ",0," + s1.getSchedDefnFkId() + ",'" + s1.getJobTypeCodeFkId() + "','" + s1.getScheduledDatetime() + "');";
                int result = executeUpdate(sql1, null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
