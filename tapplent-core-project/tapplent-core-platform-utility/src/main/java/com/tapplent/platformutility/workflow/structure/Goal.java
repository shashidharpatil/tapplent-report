package com.tapplent.platformutility.workflow.structure;

public class Goal {
    private String versionId;
    private String goalPkId;
    private String goalEventId;
    private String goalName;
    private String goalDescription;
    private String goalEntrySource;
    private String goalGroup;
    private String goalCategory;
    private String goalType;
    private Double goalWeight;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getGoalPkId() {
        return goalPkId;
    }

    public void setGoalPkId(String goalPkId) {
        this.goalPkId = goalPkId;
    }

    public String getGoalEventId() {
        return goalEventId;
    }

    public void setGoalEventId(String goalEventId) {
        this.goalEventId = goalEventId;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public String getGoalDescription() {
        return goalDescription;
    }

    public void setGoalDescription(String goalDescription) {
        this.goalDescription = goalDescription;
    }

    public String getGoalEntrySource() {
        return goalEntrySource;
    }

    public void setGoalEntrySource(String goalEntrySource) {
        this.goalEntrySource = goalEntrySource;
    }

    public String getGoalGroup() {
        return goalGroup;
    }

    public void setGoalGroup(String goalGroup) {
        this.goalGroup = goalGroup;
    }

    public String getGoalCategory() {
        return goalCategory;
    }

    public void setGoalCategory(String goalCategory) {
        this.goalCategory = goalCategory;
    }

    public String getGoalType() {
        return goalType;
    }

    public void setGoalType(String goalType) {
        this.goalType = goalType;
    }

    public Double getGoalWeight() {
        return goalWeight;
    }

    public void setGoalWeight(Double goalWeight) {
        this.goalWeight = goalWeight;
    }
}
