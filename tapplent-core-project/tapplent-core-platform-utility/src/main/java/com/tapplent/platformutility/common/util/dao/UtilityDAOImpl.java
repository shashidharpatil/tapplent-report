package com.tapplent.platformutility.common.util.dao;

import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.common.util.valueObject.ErrorLogVO;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;

import javax.print.DocFlavor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Shubham Patodi on 05/08/16.
 */
public class UtilityDAOImpl extends TapplentBaseDAO implements UtilityDAO{

    @Override
    public void insertError(ErrorLogVO logVO) throws SQLException {
        StringBuilder SQL = new StringBuilder("INSERT INTO T_SYS_TAP_ERROR_LOG (SYSTEM_ERROR_LOG_PK_ID, ERROR_DESC_LNG_TXT, CREATE_DATETIME, CREATE_AUTHOR_TXT, IS_RESOLVED)\n" +
                "VALUES (?,?,?,?,?);");
        String uuid = Common.getUUID();
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(uuid));
        parameters.add(logVO.getErrorDescription());
        String createdDateTime = Util.getStringTobeInserted("DATE_TIME", logVO.getCreateDateTime(), false).substring(1);
        createdDateTime = createdDateTime.substring(0, createdDateTime.length()-1);
        parameters.add(createdDateTime);
        parameters.add(logVO.getCreateAuthor());
        parameters.add(0);
        super.executeUpdate(SQL.toString(),parameters);
    }

    @Override
    public List<ErrorLogVO> getErrorList(boolean sendOnlyUnresolved) {
        List<ErrorLogVO> result = new ArrayList<>();
        StringBuilder SQL = new StringBuilder("SELECT *\n" +
                "FROM T_SYS_TAP_ERROR_LOG\n" +
                "WHERE IS_RESOLVED IN (?);");
        List<Object> parameters = new ArrayList<>();
        if(sendOnlyUnresolved){
            parameters.add("false");
        }else{
            parameters.add("true,false");
        }
        ResultSet rs = executeSQL(SQL.toString(),parameters);
        try {
            while(rs.next()){
                ErrorLogVO vo = new ErrorLogVO();
                /*
                SYSTEM_ERROR_LOG_PK_ID
                ERROR_DESC_LNG_TXT
                CREATE_DATETIME
                CREATE_AUTHOR_TXT
                IS_RESOLVED
                RESOLUTION_COMMENT_TXT
                RESOLVED_DATETIME
                RESOLVED_BY_TXT
                 */
                vo.setSystemErrorLogId(Util.convertByteToString(rs.getBytes("SYSTEM_ERROR_LOG_PK_ID")));
                vo.setErrorDescription(rs.getString("ERROR_DESC_LNG_TXT"));
                if(rs.getTimestamp("CREATE_DATETIME")!=null){
                    vo.setCreateDateTime(rs.getTimestamp("CREATE_DATETIME").toString());
                }
                vo.setCreateAuthor(rs.getString("CREATE_AUTHOR_TXT"));
                vo.setResolved(rs.getBoolean("IS_RESOLVED"));
                vo.setResolutionComment(rs.getString("RESOLUTION_COMMENT_TXT"));
                if(rs.getTimestamp("RESOLVED_DATETIME")!=null){
                    vo.setResolvedDateTime(rs.getTimestamp("RESOLVED_DATETIME").toString());
                }
                vo.setResolvedDateTime(Optional.ofNullable((rs.getTimestamp("RESOLVED_DATETIME").toString())).orElseGet(() -> null));
                vo.setResolvedBy(rs.getString("RESOLVED_BY_TXT"));
                result.add(vo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
