package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.helper.POST_PostHelper;
import com.tapplent.platformutility.batch.model.POST_PostData;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemProcessor;

public class PostItemProcessor implements ItemProcessor<POST_PostData, POST_PostData> {

	private String linkedin;
	private String twitter;
	private String facebook;

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	@BeforeStep
	public void beforeStep(JobExecution jobExecution){
		//(TDWI) get job parameters
		jobExecution.getJobParameters();
	}
	
	
	public POST_PostData process(POST_PostData item) throws Exception {
		// TODO Auto-generated method stub

		if (item.getSocialAccountTypeCodeFkId().equals(Constants.faceBook)) {
			POST_PostHelper.setData(item);
			//(TDWI) call faceBook post code

		} else if (item.getSocialAccountTypeCodeFkId().equals(Constants.linkedIn)) {
			POST_PostHelper.setData(item);
			//(TDWI) call linkedIn post code
		}

		else if (item.getSocialAccountTypeCodeFkId().equals(Constants.twitter)) {
			POST_PostHelper.setData(item);
			//(TDWI) call twitter post code
		}
		
		if(item.isSuccess()){
		return item;}
		else
			throw new BatchException();
	}


}
