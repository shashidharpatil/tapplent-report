package com.tapplent.platformutility.search.builder.joins;


import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/*
 * This class will make Join expression tree using Infix notation
 * The input will be first the from tableId (only once), join type/on condition and to table.
 * TableId should be in String format, join type/on condition should be in JoinOperatorsNode and parenthesis can be only "(" or ")".    
 */
public class JoinExpressionBuilder {
	private Stack<Object> operatorStack;
	private Stack<JoinOperand> operandStack;
	private Map<String, String> tableNameMap;
	public Map<String, String> getTableNameMap() {
		return tableNameMap;
	}
	public JoinExpressionBuilder(){
		this.operatorStack=new Stack<>();
		this.operandStack=new Stack<>();
		this.tableNameMap=new HashMap<>();
	}
	public void addJoinTokenToTree(Object token){
		if(token==null)
			return;
		if(token.toString().equals("(")){
			operatorStack.push(token);
		}
		if(token.toString().equals(")")){
			while(!operatorStack.isEmpty() && !operatorStack.peek().equals("(")){
				JoinOperatorsNode operatorsNode = (JoinOperatorsNode)operatorStack.peek();
				buildOperandNode(operatorsNode);
			}
			operatorStack.pop();
		}
		if(token instanceof JoinOperatorsNode){
			operateOnOperatorsNode((JoinOperatorsNode)token);
		}
		if(token instanceof JoinLeafOperand){
			String tableName = ((JoinLeafOperand)token).getTableName();
//			if(tableNameMap.containsKey(tableName))
//				throw new IllegalArgumentException("Table {} "+tableName+" already exists.");
			tableNameMap.put(tableName, tableName);
			operandStack.push((JoinOperand)token);
		}
	}
	private void operateOnOperatorsNode(JoinOperatorsNode operatorNode){
		int precedence = precedence(operatorNode.getJoinType().toString());
		while(!operatorStack.isEmpty() && precedence>=precedence(operatorStack.peek().toString())){
			JoinOperatorsNode joinOperatorsNode = (JoinOperatorsNode) operatorStack.peek();
			buildOperandNode(joinOperatorsNode);
		}
		operatorStack.push(operatorNode);
	}
	private void buildOperandNode(JoinOperatorsNode operatorsNode){
		JoinNodeOperand operandNode = new JoinNodeOperand(operatorsNode.getJoinType(),operatorsNode.getExpression());
		JoinOperand right = operandStack.peek();
		operandStack.pop();
		JoinOperand left = operandStack.peek();
		operandStack.pop();
		operandNode.setLeft(left);
		operandNode.setRight(right);
		operatorStack.pop();
		operandStack.push(operandNode);
	}
	private int precedence(String operator) {
		switch(operator){
			case "INNER_JOIN":
			case "LEFT_OUTER_JOIN":
			case "JOIN":
			case "RIGHT_OUTER_JOIN":return 0;
			default :return 1;
		}
	}
	public JoinOperand getJoinExpressionTree(){
		emptyJoinExpression();
		if(!operandStack.isEmpty())
			return operandStack.peek();
		return null;
	}
	public void emptyJoinExpression(){
		while(!operatorStack.isEmpty()){
			JoinOperatorsNode joinOperatorsNode = (JoinOperatorsNode)operatorStack.peek();
			buildOperandNode(joinOperatorsNode);
		}
	}
}
//	private Map<String, String> tableIdMap;
//	public JoinExpressionBuilder(){
//		this.operatorStack = new Stack<Object>();
//		this.operandStack = new Stack<JoinElementNode>();
//		this.tableIdMap = new HashMap<>();
//	}
//	/*
//	 * Expects input in infix manner
//	 */
//	public void addJoinClauseToTree(Object element){
//		if(element == null)
//			return;
//		if (element instanceof JoinOperatorsNode) {
//			JoinOperatorsNode operatorNode = (JoinOperatorsNode) element;
//			operateOnOperatorNode(operatorNode);
//		}else if(element.equals("(")){
//			operateOnOpenParenthesis(element);
//		}else if(element.equals(")")){
//			operateOnCloseParenthesis(element);
//		}else{
//			operateOnTableNode(element);
//		}
//	}
//	public JoinOperatorsNode getJoinExpression(){
//		if(!operandStack.isEmpty())
//			return (JoinOperatorsNode) operandStack.peek();
//		return null;
//	}
//	private void operateOnOperatorNode(JoinOperatorsNode operatorNode) {
//		while(!operatorStack.isEmpty() && !operatorStack.peek().toString().equals("(")){
//			//pop the stack element add it the two elements
//			buildOperandStack(operatorStack.peek());
//			operatorStack.pop();
//		}
//		operatorStack.push(operatorNode);
//	}
//	private void buildOperandStack(Object peek) {
////		if(peek instanceof JoinOperatorsNode){
////			((JoinOperatorsNode) peek).setRightNode(operandStack.peek());
////			operandStack.pop();
////			((JoinOperatorsNode) peek).setLeftNode(operandStack.peek());
////			operandStack.pop();
////			operandStack.push((JoinOperatorsNode) peek);
////		}else{
////			return;
////		}
//	}
//	private void operateOnTableNode(Object element) {
//		if(tableIdMap.containsKey(element.toString())){
//			throw new IllegalArgumentException("Table "+element.toString()+" already exists in Join tree");
//		}
//		tableIdMap.put(element.toString(), element.toString());
//		JoinTablesNode tableNode = new JoinTablesNode(element.toString());
//		operandStack.push(tableNode);
//	}
//	private void operateOnOpenParenthesis(Object element) {
//		operatorStack.push(element);
//	}
//	private void operateOnCloseParenthesis(Object element) {
//		if(element.equals(")")){
//			while(!operatorStack.isEmpty() && !operatorStack.peek().toString().equals("(")){
//				//pop the stack element add it the two elements
//				buildOperandStack(operatorStack.peek());
//				operatorStack.pop();
//			}
//			operatorStack.pop();
//		}
//	}
	

