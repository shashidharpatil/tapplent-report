package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by tapplent on 04/11/16.
 */
public class AMProcess {
    private String amProcessPkId;
    private String amDefnFkId;
    private String mtProcessCodeFkId;
    private String whoseGroupFkId;
    private boolean isWhoseRelativeToWho;

    public String getAmProcessPkId() {
        return amProcessPkId;
    }

    public void setAmProcessPkId(String amProcessPkId) {
        this.amProcessPkId = amProcessPkId;
    }

    public String getAmDefnFkId() {
        return amDefnFkId;
    }

    public void setAmDefnFkId(String amDefnFkId) {
        this.amDefnFkId = amDefnFkId;
    }

    public String getMtProcessCodeFkId() {
        return mtProcessCodeFkId;
    }

    public void setMtProcessCodeFkId(String mtProcessCodeFkId) {
        this.mtProcessCodeFkId = mtProcessCodeFkId;
    }

    public String getWhoseGroupFkId() {
        return whoseGroupFkId;
    }

    public void setWhoseGroupFkId(String whoseGroupFkId) {
        this.whoseGroupFkId = whoseGroupFkId;
    }

    public boolean isWhoseRelativeToWho() {
        return isWhoseRelativeToWho;
    }

    public void setWhoseRelativeToWho(boolean whoseRelativeToWho) {
        isWhoseRelativeToWho = whoseRelativeToWho;
    }
}
