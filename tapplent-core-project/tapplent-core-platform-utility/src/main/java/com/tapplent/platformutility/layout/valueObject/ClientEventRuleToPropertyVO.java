package com.tapplent.platformutility.layout.valueObject;

public class ClientEventRuleToPropertyVO {
	String clientEventRulePropertyMapId;
	String rule;
	String property;
	public String getClientEventRulePropertyMapId() {
		return clientEventRulePropertyMapId;
	}
	public void setClientEventRulePropertyMapId(String clientEventRulePropertyMapId) {
		this.clientEventRulePropertyMapId = clientEventRulePropertyMapId;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
}
