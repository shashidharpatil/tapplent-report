package com.tapplent.platformutility.server.property;

import java.util.HashMap;
import java.util.Map;

public class ServerActionStep {
	
	private String versionId;
	private String ActionStepLookupPkId;
	private String ActionCodeFkId;
	private String ParentActionStepG11nText;
	private String ActionStepNameG11nText;
	private String ServerRuleCodeFkId;
	private Map<String, String> propertyCodeValueMap = new HashMap<>();
	
	
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getActionStepLookupPkId() {
		return ActionStepLookupPkId;
	}
	public void setActionStepLookupPkId(String actionStepLookupPkId) {
		ActionStepLookupPkId = actionStepLookupPkId;
	}
	public String getActionCodeFkId() {
		return ActionCodeFkId;
	}
	public void setActionCodeFkId(String actionCodeFkId) {
		ActionCodeFkId = actionCodeFkId;
	}
	public String getParentActionStepG11nText() {
		return ParentActionStepG11nText;
	}
	public void setParentActionStepG11nText(String parentActionStepG11nText) {
		ParentActionStepG11nText = parentActionStepG11nText;
	}
	public String getActionStepNameG11nText() {
		return ActionStepNameG11nText;
	}
	public void setActionStepNameG11nText(String actionStepNameG11nText) {
		ActionStepNameG11nText = actionStepNameG11nText;
	}
	public String getServerRuleCodeFkId() {
		return ServerRuleCodeFkId;
	}
	public void setServerRuleCodeFkId(String serverRuleCodeFkId) {
		ServerRuleCodeFkId = serverRuleCodeFkId;
	}
	public Map<String, String> getPropertyCodeValueMap() {
		return propertyCodeValueMap;
	}
	public void setPropertyCodeValueMap(Map<String, String> propertyCodeValueMap) {
		this.propertyCodeValueMap = propertyCodeValueMap;
	}
}
