package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.POST_PostData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;


public class PostItemReader extends JdbcPagingItemReader<POST_PostData> {
	
	public PostItemReader() {
		super();
		DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
		setDataSource(ds);
		setPageSize(20);
		setQueryProvider(queryProvider(ds));
		setRowMapper(rowMapper());
	}
	
	private RowMapper<POST_PostData> rowMapper() {
		return new PostRowMapper();
	}

	private PagingQueryProvider queryProvider(DataSource ds) {
		 MySqlPagingQueryProvider provider = new MySqlPagingQueryProvider();
		    provider.setSelectClause("T_PRN_IEU_SOC_ACC_PERSON_POST.SOCIAL_ACCOUNT_POST_TXN_DEP_FK_ID,"
		    		+ "T_PRN_IEU_SOC_ACC_PERSON_POST.PERSON_FK_ID,"
		    		+ "T_PRN_IEU_SOC_ACC_PERSON_POST.RETURN_STATUS_CODE_FK_ID,"
		    		+ "T_PRN_IEU_SOC_ACC_PERSON_POST.SOCIAL_ACCOUNT_PERSON_POST_PK_ID,"
		    		+ "T_PRN_EU_SOC_ACC_POST_TXN.SOCIAL_ACCOUNT_POST_TXN_PK_ID,"
		    		+ "T_PRN_EU_SOC_ACC_POST_TXN.SOCIAL_ACCOUNT_POST_DEFN_FK_ID,"
		    		+ "T_PRN_EU_SOC_ACC_POST_TXN.MT_PE_CODE_FK_ID,"
		    		+ "T_PRN_EU_SOC_ACC_POST_TXN.DO_ROW_PRIMARY_KEY_TXT,"
		    		+ "T_PRN_EU_SOC_ACC_POST_TXN.DO_ROW_VERSION_FK_ID,"
		    		+ "T_PFM_ADM_SOC_ACC_POST_DEFN.SOCIAL_ACCOUNT_TYPE_CODE_FK_ID,"
		    		+ "T_PFM_ADM_SOC_ACC_POST_DEFN.NOTIFICATION_DEFINITION_FK_ID ");
		    provider.setFromClause("FROM"
		    		+ "T_PRN_IEU_SOC_ACC_PERSON_POST INNER JOIN T_PRN_EU_SOC_ACC_POST_TXN ON"
		    		+ "T_PRN_IEU_SOC_ACC_PERSON_POST.SOCIAL_ACCOUNT_POST_TXN_DEP_FK_ID = "
		    		+ "T_PRN_EU_SOC_ACC_POST_TXN.SOCIAL_ACCOUNT_POST_TXN_PK_ID INNER JOIN"
		    		+ "T_PFM_ADM_SOC_ACC_POST_DEFN ON T_PRN_EU_SOC_ACC_POST_TXN.SOCIAL_ACCOUNT_POST_DEFN_FK_ID ="
		    		+ "T_PFM_ADM_SOC_ACC_POST_DEFN.SOCIAL_ACC_POST_DEFN_PK_ID");
		    provider.setWhereClause("T_PRN_IEU_SOC_ACC_PERSON_POST.RETURN_STATUS_CODE_FK_ID = 'PENDING'");
		    Map abc = new HashMap<String, Order>();
		    abc.put("SOCIAL_ACCOUNT_PERSON_POST_PK_ID", Order.ASCENDING);
		   provider.setSortKeys(abc);
		
		try {
			return provider;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
