package com.tapplent.platformutility.search.builder;

import java.util.Deque;
import java.util.List;

/**
 * Created by tapplent on 12/06/17.
 */
public class SqlQuery {
    private String sql;
    private Deque<Object> parameters;

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Deque<Object> getParameters() {
        return parameters;
    }

    public void setParameters(Deque<Object> parameters) {
        this.parameters = parameters;
    }
}
