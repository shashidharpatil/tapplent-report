package com.tapplent.platformutility.common.cache;

import java.util.LinkedList;
import java.util.Map;

import com.tapplent.platformutility.server.property.ServerPropertyService;
import com.tapplent.tenantresolver.server.property.ServerActionStep;

public class ServerPropertyObjectRepository {
	private Map<String, LinkedList<ServerActionStep>> actionCodeActionStepListMap;
	ServerPropertyService serverPropertyService;
	
	public ServerPropertyService getServerPropertyService() {
		return serverPropertyService;
	}
	public void setServerPropertyService(ServerPropertyService serverPropertyService) {
		this.serverPropertyService = serverPropertyService;
	}
	
	public Map<String, LinkedList<ServerActionStep>> getServerActionStepListMap(){
		if(null == actionCodeActionStepListMap){
			actionCodeActionStepListMap = serverPropertyService.getactionCodeActionStepListMap();
		}
		return actionCodeActionStepListMap;
	}
	
	public LinkedList<ServerActionStep> getServerActionStepByActionCode(String actionCode){
		if(null == actionCodeActionStepListMap){
			actionCodeActionStepListMap = serverPropertyService.getactionCodeActionStepListMap();
		}
		return actionCodeActionStepListMap.get(actionCode);
	}
	
}
