package com.tapplent.platformutility.uilayout.dao;


import com.tapplent.platformutility.accessmanager.structure.ResolvedColumnCondition;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.conversation.valueobject.PersonDetailsVO;
import com.tapplent.platformutility.layout.valueObject.JobRelationshipTypeVO;
import com.tapplent.platformutility.layout.valueObject.LayoutPermissionsVO;
import com.tapplent.platformutility.layout.valueObject.StateVO;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.platformutility.uilayout.valueobject.*;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by sripat on 01/11/16.
 */
public class UILayoutDAOImpl extends TapplentBaseDAO implements UILayoutDAO {
    @Override
    public ScreenInstanceVO getScreenInstance(String targetScreenInstance, String deviceType) {
        ScreenInstanceVO screenInstance = new ScreenInstanceVO();
        String sql = "SELECT * FROM T_UI_ADM_SCREEN_INSTANCE INNER JOIN T_UI_TAP_SCREEN_MASTER ON SCREEN_MASTER_CODE_FK_ID = SCREEN_MASTER_CODE_PK_ID WHERE SCREEN_INSTANCE_PK_ID = x? ";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(targetScreenInstance));
        ResultSet resultSet = executeSQL(sql, params);
        try {
            while (resultSet.next()) {
                screenInstance.setScreenInstancePkId(Util.convertByteToString(resultSet.getBytes("SCREEN_INSTANCE_PK_ID")));
                screenInstance.setScreenMasterCodeFkId(resultSet.getString("SCREEN_MASTER_CODE_FK_ID"));
                screenInstance.setThemeSeqNumPosInt(resultSet.getInt("OVERRIDE_THEME_SEQ_NUM_POS_INT"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return screenInstance;
    }

    /*
ON_LOAD_GENERIC_QUERY_FK_ID
IS_SHOW_AT_INITIAL_LOAD
IS_STATIC_DATA_SECN
OVERRIDE_THEME_SEQ_NUM_POS_INT
NO_DATA_MESSAGE_G11N_BIG_TXT
NO_DATA_MESSAGE_ICON_CODE_FK_ID
NO_DATA_MESSAGE_IMAGEID
     */
    @Override
    public List<ScreenSectionInstanceVO> getSectionInstances(String screenInstancePkId, String deviceType) {
        List<ScreenSectionInstanceVO> sectionInstanceVOS = new ArrayList<>();
        String sql = "SELECT * FROM T_UI_ADM_SCREEN_SECTION_INSTANCE INNER JOIN T_UI_TAP_SECTION_MASTER ON SCREEN_SECTION_MASTER_CODE_FK_ID = SECTION_MASTER_CODE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND "+getApplicability(deviceType) + " ORDER BY DISP_SEQ_NUM_POS_INT";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        ResultSet rs = executeSQL(sql, params);
        try {
            while (rs.next()) {
                ScreenSectionInstanceVO sectionInstanceVO = new ScreenSectionInstanceVO();
                sectionInstanceVO.setScreenSectionInstancePkId(Util.convertByteToString(rs.getBytes("SCREEN_SECTION_INSTANCE_PK_ID")));
                sectionInstanceVO.setScreenInstanceFkId(Util.convertByteToString(rs.getBytes("SCREEN_INSTANCE_FK_ID")));
                sectionInstanceVO.setSectionMasterCodeFkId(rs.getString("SCREEN_SECTION_MASTER_CODE_FK_ID"));
                sectionInstanceVO.setScreenSectionInstanceName(getG11nValue(rs.getString("SCREEN_SECTION_INSTANCE_NAME_G11N_BIG_TXT")));
                sectionInstanceVO.setSecnUIType(rs.getString("SECN_UI_TYPE_TXT"));
                sectionInstanceVO.setSecnControlPos(rs.getString("SECN_CONTROL_POSITION_TXT"));
                try{
                    sectionInstanceVO.setParentSectionInstanceFkId(Util.convertByteToString(rs.getBytes("SLF_PRNT_SCREEN_SECTION_INSTANCE_FK_ID")));
                }catch (Exception e){
                    sectionInstanceVO.setParentSectionInstanceFkId(Util.convertByteToString(rs.getBytes("PARENT_SCREEN_SECTION_INSTANCE_FK_ID")));
                }

                sectionInstanceVO.setOnLoadGenericQuery(Util.convertByteToString(rs.getBytes("ON_LOAD_GENERIC_QUERY_FK_ID")));
                sectionInstanceVO.setShowAtInitialLoad(rs.getBoolean("IS_SHOW_AT_INITIAL_LOAD"));
                sectionInstanceVO.setStaticDataSection(rs.getBoolean("IS_STATIC_DATA_SECN"));
                sectionInstanceVO.setThemeSeqNumPosInt(rs.getInt("OVERRIDE_THEME_SEQ_NUM_POS_INT"));
                sectionInstanceVO.setNoDataMessageG11nBigTxt(getG11nValue(rs.getString("NO_DATA_MESSAGE_G11N_BIG_TXT")));
                sectionInstanceVO.setNoDataMessageIconCode(rs.getString("NO_DATA_MESSAGE_ICON_CODE_FK_ID"));
                sectionInstanceVO.setNoDataMessageImageId(rs.getString("NO_DATA_MESSAGE_IMAGEID"));
                sectionInstanceVO.setDisSeqNumber(rs.getInt("DISP_SEQ_NUM_POS_INT"));
                sectionInstanceVO.setPaginated(rs.getBoolean("IS_PAGINATED"));
                sectionInstanceVO.setsUMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
                sectionInstanceVO.setsUMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                sectionInstanceVO.setsUParentMTPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                sectionInstanceVO.setSuFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                sectionInstanceVO.setsUAttributePathExpn(rs.getString("VALUE_EXPN"));
                sectionInstanceVO.setMinMandatoryRecordCount(rs.getInt("MIN_MANDATORY_REC_POS_INT"));
                sectionInstanceVOS.add(sectionInstanceVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sectionInstanceVOS;
    }

    @Override
    public List<SectionControlVO> getControlList(String screenInstancePkId, Set<String> relevantSectionIdList) {
        List<SectionControlVO> result = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_CONTROL_INSTANCE INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_PK_ID = SCREEN_SECTION_INSTANCE_FK_ID INNER JOIN T_UI_TAP_CONTROL_TYPE ON CONTROL_TYPE_CODE_PK_ID = CONTROL_TYPE_CODE_FK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                SectionControlVO controlVO = new SectionControlVO();
                controlVO.setControlPkId(Util.convertByteToString(rs.getBytes("CONTROL_INSTANCE_PK_ID")));
                controlVO.setSectionInstanceFkId(Util.convertByteToString(rs.getBytes("SCREEN_SECTION_INSTANCE_FK_ID")));
                controlVO.setSectionMasterCode(rs.getString("SCREEN_SECTION_MASTER_CODE_FK_ID"));
                controlVO.setControlType(rs.getString("CONTROL_TYPE_CODE_FK_ID"));
                controlVO.setControlTypeIcon(rs.getString("CONTROL_TYPE_ICON_CODE_FK_ID"));
                try{
                    controlVO.setControlOnStateIcon(rs.getString("CONTROL_ON_STATE_ICON_CODE_FK_ID"));
                    controlVO.setControlOffStateIcon(rs.getString("CONTROL_OFF_STATE_ICON_CODE_FK_ID"));
                }catch (Exception e){

                }
                controlVO.setStaticControl(rs.getBoolean("IS_STATIC_CONTROL"));
                controlVO.setAggregateControl(rs.getBoolean("IS_AGGREGATE_CONTROL"));
                controlVO.setApplicableGroupByControls(rs.getString("APPL_GRP_BY_CTRLS_LNG_TXT"));
                controlVO.setAttributePathExpn(rs.getString("CONTROL_ATTRIBUTE_PATH_EXPN"));
                controlVO.setLabelValueSeparator(rs.getString("LABEL_VALUE_SEPARATOR_CHAR_TXT"));
                controlVO.setItemStatusValue(rs.getString("ITEM_STATUS_VALUE_TXT"));
                controlVO.setAttributeFilterExpn(rs.getString("ATTRIBUTE_FILTER_EXPN"));
                controlVO.setAdditionalAttributePathExpn(rs.getString("ADDL_ATTR_PATH_EXPN"));
                controlVO.setCompressAttachment(rs.getBoolean("IS_ATTACH_COMPRESS"));
                controlVO.setStatusColourAttrPathExpn(rs.getString("STATUS_COLOUR_ATTR_PATH_EXPN"));
                controlVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
                controlVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                controlVO.setParentMTPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                controlVO.setSeqNumberTxt(rs.getString("SEQ_NUM_POSITION_TXT"));
                controlVO.setDetailsDisplayRowNumber(rs.getInt("BASIC_DTL_DISP_ROW_NUM_POS_INT"));
                controlVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                controlVO.setDisplayInForeground(rs.getBoolean("IS_DISPLAY_IN_FOREGROUND"));
                controlVO.setDefaultOnLoadSort(rs.getString("DEFAULT_ON_LOAD_SORT_EXPN"));
                controlVO.setOrderByValues(rs.getString("ORDER_BY_VALUES_BIG_TXT"));
                controlVO.setGroupByControl(rs.getBoolean("IS_GROUP_BY_CONTROL"));
                controlVO.setGroupByControlSequence(rs.getInt("GROUP_BY_CONTROL_SEQ_NUM_POS_INT"));
                controlVO.setGroupByHavingFilterExpression(rs.getString("GROUP_BY_HAVING_EXPN"));
                controlVO.setAssociatedGroupByCtrlForAddlAttrPath(Util.convertByteToString(rs.getBytes("ASSCTD_GRP_BY_CTRL_INST_FOR_ADDL_PATH_FK_ID")));
                controlVO.setStickyHdr(rs.getBoolean("IS_CONTROL_DISP_IN_STICKY_HDR"));
                controlVO.setStickyHdrSeqNum(rs.getInt("STICKY_HDR_SEQ_NUM_POS_INT"));
                controlVO.setStickyHeaderOrder(rs.getString("STICKY_HDR_ORDER_BY_CODE_FK_ID"));
                controlVO.setTextJustificationCode(rs.getString("CONTROL_TEXT_JUSTIFICATION_CODE_FK_ID"));
                controlVO.setThemeSize(rs.getString("CONTROL_THEME_SIZE_CODE_FK_ID"));
                controlVO.setFontStyle(rs.getString("CONTROL_FONT_STYLE_CODE_FK_ID"));
                controlVO.setFontTextOverflowCode(rs.getString("CONTROL_FONT_TEXT_OVERFLOW_CODE_FK_ID"));
                controlVO.setFontCase(rs.getString("CONTROL_FONT_CASE_CODE_FK_ID"));
                controlVO.setFontColour(rs.getString("CONTROL_FONT_COLOUR_TXT"));
                controlVO.setOrgChartFilterLabelID(Util.convertByteToString(rs.getBytes("ORGCHART_FILTER_LBL_FK_ID")));
                controlVO.setDispLabelAttribute(rs.getString("DISP_LBL_ATT_BT_DOA_CODE_FK_ID"));
                controlVO.setFarLabel(rs.getBoolean("IS_FAR_DISP_LABEL"));
                try{
                    controlVO.setDetailsLabelFontColour(rs.getString("CONTROL_DTLS_LBL_ICON_FONT_COLOUR_TXT"));
                    controlVO.setOnStateFontColour(rs.getString("CONTROL_ON_STATE_FONT_COLOUR_TXT"));
                    controlVO.setOnStateBackgroundColour(rs.getString("CONTROL_ON_STATE_BG_COLOUR_TXT"));
                    controlVO.setOffStateBackgroundColour(rs.getString("CONTROL_OFF_STATE_BG_COLOUR_TXT"));
                    controlVO.setEditable(rs.getBoolean("IS_EDITABLE"));
                    controlVO.setDataEmpty(rs.getBoolean("IS_DATA_EMPTY"));
                    controlVO.setStaticLabelText(rs.getString("STATIC_LABEL_G11N_BIG_TXT"));
                    controlVO.setApplyPagination(rs.getBoolean("IS_APGN_APPLIED"));
                    controlVO.setEliminateResultFromRoot(rs.getBoolean("IS_ELIMINATE_RSLT_FROM_ROOT"));
                    controlVO.setDefaultPaginationSize(rs.getInt("DFLT_PG_SIZE_NUM_POS_INT"));
                    controlVO.setHavingClause(rs.getBoolean("IS_HAVING_CLAUSE"));
                }catch (Exception e){

                }
                controlVO.setBackgroundColour(rs.getString("CONTROL_BACKGROUND_COLOUR_TXT"));
                controlVO.setAssociatedRelnControlId(Util.convertByteToString(rs.getBytes("ASS_REL_CTRL_INST_FK_ID")));
                controlVO.setBaseTemplateCode(rs.getString("BASE_TEMPLATE_CODE_FK_ID"));
                controlVO.setRatingScaleID(Util.convertByteToString(rs.getBytes("RATING_SCALE_FK_ID")));
                controlVO.setHideAtInitialLoad(rs.getBoolean("IS_HIDDEN_AT_INITIAL_LOAD"));
                controlVO.setMandatory(rs.getBoolean("IS_MANDATORY"));
                controlVO.setDefaultValue(rs.getString("DEFAULT_VALUE_TXT"));
                controlVO.setApplySortToRoot(rs.getBoolean("IS_APPLY_ORDER_BY_CLAUSE_TO_ROOT"));
                controlVO.setShowLabelBelow(rs.getBoolean("IS_MOBILE_LABEL_DISP_BELOW"));
                controlVO.setArtefactsType(Util.convertByteToString(rs.getBytes("VIZ_ARTEFACT_LIB_TYPE_FK_ID")));
                result.add(controlVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private StringBuilder addInClauseParam(Set<String> relevantSectionIdList, StringBuilder sql) {
        for(int i=0; i<relevantSectionIdList.size(); i++){
            if(i!=0)
                sql.append(",");
            sql.append("x?");
        }
        if(relevantSectionIdList.isEmpty())
            sql.append("null");
        return sql;
    }

    @Override
    public List<ControlStaticValuesVO> getStaticControlValues(String screenInstancePkId, Set<String> relevantSectionIdList) {
        List<ControlStaticValuesVO> staticValuesVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_CONTROL_STATIC_VALUES INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CONTROL_INSTANCE_FK_ID = CONTROL_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                ControlStaticValuesVO staticValuesVO = new ControlStaticValuesVO();
                staticValuesVO.setStaticPkId(Util.convertByteToString(rs.getBytes("CONTROL_STATIC_VALUES_PK_ID")));
                staticValuesVO.setControlFkId(Util.convertByteToString(rs.getBytes("CONTROL_INSTANCE_FK_ID")));
                staticValuesVO.setStaticText(getG11nValue(rs.getString("STATIC_TEXT_G11N_BIG_TXT")));
                staticValuesVO.setStaticIcon(rs.getString("STATIC_ICON_CODE_FK_ID"));
                staticValuesVO.setValueItemNumber(rs.getInt("ITEM_SEQ_NUM_POS_INT"));
                staticValuesVO.setStaticImage(Util.convertByteToString(rs.getBytes("STATIC_IMAGEID")));
                staticValuesVOS.add(staticValuesVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return staticValuesVOS;
    }

    @Override
    public List<ControlInstanceFiltersVO> getControlInstanceFilters(String screenInstancePkId, Set<String> relevantSectionIdList) {
        List<ControlInstanceFiltersVO> controlInstanceFiltersVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_CTRL_INST_FILTER_PARMS INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CONTROL_INSTANCE_FK_ID = CONTROL_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                ControlInstanceFiltersVO filtersVO = new ControlInstanceFiltersVO();
                filtersVO.setFilterPkId(Util.convertByteToString(rs.getBytes("CTRL_INST_FILTER_PARMS_PK_ID")));
                filtersVO.setControlFkId(Util.convertByteToString(rs.getBytes("CONTROL_INSTANCE_FK_ID")));
                filtersVO.setAttributePathExpn(rs.getString("CONTROL_ATTRIBUTE_PATH_EXPN"));
                filtersVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                filtersVO.setAttributeValue(rs.getString("VALUE_EXPN"));
                filtersVO.setAlwaysApplied(rs.getBoolean("IS_ALWAYS_APPLIED"));
                filtersVO.setHavingClause(rs.getBoolean("IS_HAVING_CLAUSE"));
                controlInstanceFiltersVOS.add(filtersVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return controlInstanceFiltersVOS;
    }

    @Override
    public List<ControlEventInstanceVO> getControlEventVO(String screenInstancePkId, Set<String> relevantSectionIdList) {
        List<ControlEventInstanceVO> controlEventInstanceVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_CONTROL_EVENT_INST INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CTRL_INSTANCE_FK_ID = CONTROL_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()){
                ControlEventInstanceVO controlEventInstanceVO = new ControlEventInstanceVO();
                controlEventInstanceVO.setControlEventPkId(Util.convertByteToString(rs.getBytes("CTRL_EVENT_INST_PK_ID")));
                controlEventInstanceVO.setControlId(Util.convertByteToString(rs.getBytes("CTRL_INSTANCE_FK_ID")));
                controlEventInstanceVO.setEvent(rs.getString("CTRL_EVENT_CODE_FK_ID"));
                controlEventInstanceVO.setRule(rs.getString("CTRL_EVNT_RULE_CODE_FK_ID"));
                controlEventInstanceVO.setTargetControlInstance(Util.convertByteToString(rs.getBytes("CTRL_TGT_CNTRL_INST_FK_ID")));
                controlEventInstanceVO.setTargetFormatQuery(rs.getString("TGT_FORMAT_QUERY_FK_ID"));
                controlEventInstanceVO.setTargetFormatText(rs.getString("TGT_FORMAT_LNG_TXT"));
                controlEventInstanceVOS.add(controlEventInstanceVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return controlEventInstanceVOS;
    }

    @Override
    public Map<String, RatingScaleVO> getRatingScaleVOs(Set<String> ratingScaleIDs) {
        Map<String, RatingScaleVO> stringRatingScaleVOMap = new HashMap<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_ADM_RATING_SCALE WHERE RATING_SCALE_PK_ID IN (");
        sql = addInClauseParam(ratingScaleIDs, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        for(String ratingID : ratingScaleIDs){
            params.add(Util.remove0x(ratingID));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()){
                RatingScaleVO ratingScaleVO = new RatingScaleVO();
                ratingScaleVO.setRatingScalePkID(Util.convertByteToString(rs.getBytes("RATING_SCALE_PK_ID")));
                ratingScaleVO.setRatingName(getG11nValue(rs.getString("RATING_SCALE_NAME_G11N_BIG_TXT")));
                ratingScaleVO.setRatingScaleIcon(rs.getString("RATING_SCALE_ICON_CODE_FK_ID"));
                ratingScaleVO.setIntervalRatingLevel(rs.getDouble("INTERVAL_RATING_LEVEL_NEG_DEC"));
                ratingScaleVO.setMaxRatingLevel(rs.getDouble("MAXIMUM_RATING_LEVEL_NEG_DEC"));
                ratingScaleVO.setMinRatingLevel(rs.getDouble("MINIMUM_RATING_LEVEL_NEG_DEC"));
                stringRatingScaleVOMap.put(ratingScaleVO.getRatingScalePkID(), ratingScaleVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stringRatingScaleVOMap;
    }

    @Override
    public List<ControlEventCriteriaVO> getControlEventCriteriaVOs(Set<String> controlEventIDs) {
        List<ControlEventCriteriaVO> controlEventCriteriaVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_CTRL_EVNT_CRITERIA_CALC WHERE CTRL_EVENT_INST_FK_ID IN (");
        sql = addInClauseParam(controlEventIDs, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        for(String ratingID : controlEventIDs){
            params.add(Util.remove0x(ratingID));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()){
                ControlEventCriteriaVO controlEventCriteriaVO = new ControlEventCriteriaVO();
                controlEventCriteriaVO.setControlEventCriteriaPkID(Util.convertByteToString(rs.getBytes("CTRL_EVNT_CRIT_CALC_PK_ID")));
                controlEventCriteriaVO.setControlEvent(Util.convertByteToString(rs.getBytes("CTRL_EVENT_INST_FK_ID")));
                controlEventCriteriaVO.setCriteriaControlInstance(Util.convertByteToString(rs.getBytes("CRITERIA_CTRL_INST_FK_ID")));
                controlEventCriteriaVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                controlEventCriteriaVO.setFilterValue(rs.getString("FILTER_VALUE_EXPN"));
                controlEventCriteriaVOS.add(controlEventCriteriaVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return controlEventCriteriaVOS;
    }

    @Override
    public List<FormulaInputParametersDetailsVO> getInputParametersDetails(Set<String> controlEventIDs) {
        List<FormulaInputParametersDetailsVO> formulaInputParametersDetailsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_CTRL_CLNT_INPT_PARMS WHERE CTRL_EVENT_INST_FK_ID IN (");
        sql = addInClauseParam(controlEventIDs, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        for(String ratingID : controlEventIDs){
            params.add(Util.remove0x(ratingID));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()){
                FormulaInputParametersDetailsVO formulaInputParametersDetailsVO = new FormulaInputParametersDetailsVO();
                formulaInputParametersDetailsVO.setFormulaInputParametersDetailsPkID(Util.convertByteToString(rs.getBytes("CLNT_FRML_INP_PARMS_PK_ID")));
                formulaInputParametersDetailsVO.setControlEvent(Util.convertByteToString(rs.getBytes("CTRL_EVENT_INST_FK_ID")));
                formulaInputParametersDetailsVO.setParameterName(rs.getString("PARM_NAME_TXT"));
                formulaInputParametersDetailsVO.setParameterSource(rs.getString("PARAM_SOURCE_TXT"));
                formulaInputParametersDetailsVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
                formulaInputParametersDetailsVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                formulaInputParametersDetailsVO.setParentMTPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                formulaInputParametersDetailsVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                formulaInputParametersDetailsVO.setAttributePath(rs.getString("ATTR_PATH_EXPN"));
                formulaInputParametersDetailsVO.setOnLoadGenericQueryID(Util.convertByteToString(rs.getBytes("ON_LOAD_GENERIC_QUERY_FK_ID")));
                formulaInputParametersDetailsVO.setDirectValue(rs.getString("DIRECT_VAL_TXT"));
                formulaInputParametersDetailsVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                formulaInputParametersDetailsVOS.add(formulaInputParametersDetailsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return formulaInputParametersDetailsVOS;
    }

    @Override
    public List<LocationBreakUpDataVO> getLocationBreakUpData(String contextID) {
        List<LocationBreakUpDataVO> locationBreakUpDataVOS = new ArrayList<>();
        String SQL = "SELECT parent.LOCATION_NAME_G11N_BIG_TXT, parent.LOCATION_PK_ID, parent.SLF_PRNT_LOCATION_HCY_FK_ID, LOCATION_TYPE_NAME_G11N_BIG_TXT\n" +
                "FROM T_ORG_EU_LOCATION AS node,\n" +
                "        T_ORG_EU_LOCATION AS parent LEFT JOIN T_ORG_LKP_LOCATION_TYPE ON parent.LOCATION_TYPE_FK_ID = LOCATION_TYPE_PK_ID\n" +
                "WHERE node.INTRNL_LEFT_POS_INT BETWEEN parent.INTRNL_LEFT_POS_INT AND parent.INTRNL_RIGHT_POS_INT AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "        AND node.LOCATION_PK_ID = ?\n" +
                "      AND node.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND node.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND node.SAVE_STATE_CODE_FK_ID= 'SAVED' AND node.IS_DELETED = 0\n" +
                "  AND parent.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND parent.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND parent.SAVE_STATE_CODE_FK_ID= 'SAVED' AND parent.IS_DELETED = 0\n" +
                "ORDER BY parent.INTRNL_LEFT_POS_INT;";
        List<Object> params = new ArrayList<>();
        params.add(Util.convertStringIdToByteId(contextID));
        ResultSet rs = executeSQL(SQL, params);
        try {
            while(rs.next()){
                LocationBreakUpDataVO locationBreakUpDataVO = new LocationBreakUpDataVO();
                locationBreakUpDataVO.setLocationName(Util.getG11nValue(rs.getString(1), null));
                locationBreakUpDataVO.setLocationId(Util.convertByteToString(rs.getBytes(2)));
                locationBreakUpDataVO.setLocationType(Util.convertByteToString(rs.getBytes(3)));
                locationBreakUpDataVO.setLocationTypeName(Util.getG11nValue(rs.getString(4), null));
                locationBreakUpDataVOS.add(locationBreakUpDataVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return locationBreakUpDataVOS;
    }

    @Override
    public List<CostCentreBreakUpDataVO> getCostCentreBreakUpData(String contextID) {
        List<CostCentreBreakUpDataVO> costCentreBreakUpDataVOS = new ArrayList<>();
        String SQL = "SELECT parent.COST_CENTER_NAME_G11N_BIG_TXT, parent.COST_CENTER_PK_ID, parent.SLF_PRNT_COST_CENTER_HCY_FK_ID, COST_CENTER_TYPE_NAME_G11N_BIG_TXT\n" +
                "FROM T_ORG_EU_COST_CENTER AS node,\n" +
                "        T_ORG_EU_COST_CENTER AS parent LEFT JOIN T_ORG_LKP_COST_CENTER_TYPE ON parent.COST_CENTER_TYPE_FK_ID = COST_CENTER_TYPE_PK_ID\n" +
                "WHERE node.INTRNL_LEFT_POS_INT BETWEEN parent.INTRNL_LEFT_POS_INT AND parent.INTRNL_RIGHT_POS_INT AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "        AND node.COST_CENTER_PK_ID = ?\n" +
                "      AND node.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND node.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND node.SAVE_STATE_CODE_FK_ID= 'SAVED' AND node.IS_DELETED = 0\n" +
                "  AND parent.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND parent.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND parent.SAVE_STATE_CODE_FK_ID= 'SAVED' AND parent.IS_DELETED = 0\n" +
                "ORDER BY parent.INTRNL_LEFT_POS_INT;";
        List<Object> params = new ArrayList<>();
        params.add(Util.convertStringIdToByteId(contextID));
        ResultSet rs = executeSQL(SQL, params);
        try {
            while(rs.next()){
                CostCentreBreakUpDataVO costCentreBreakUpDataVO = new CostCentreBreakUpDataVO();
                costCentreBreakUpDataVO.setCostCentreName(Util.getG11nValue(rs.getString(1), null));
                costCentreBreakUpDataVO.setCostCentreId(Util.convertByteToString(rs.getBytes(2)));
                costCentreBreakUpDataVO.setCostCentreType(Util.convertByteToString(rs.getBytes(3)));
                costCentreBreakUpDataVO.setCostCentreTypeName(Util.getG11nValue(rs.getString(4), null));
                costCentreBreakUpDataVOS.add(costCentreBreakUpDataVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return costCentreBreakUpDataVOS;
    }

    @Override
    public List<JobFamilyBreakUpDataVO> getJobFamilyBreakUpData(String contextID) {
        List<JobFamilyBreakUpDataVO> jobFamilyBreakUpDataVOS = new ArrayList<>();
        String SQL = "SELECT parent.JOB_FAMILY_NAME_G11N_BIG_TXT, parent.JOB_FAMILY_PK_ID, parent.SLF_PRNT_JOB_FAMILY_HCY_FK_ID\n" +
                "FROM T_JOB_ADM_JOB_FAMILY AS node,\n" +
                "        T_JOB_ADM_JOB_FAMILY AS parent \n" +
                "WHERE node.INTRNL_LEFT_POS_INT BETWEEN parent.INTRNL_LEFT_POS_INT AND parent.INTRNL_RIGHT_POS_INT AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "        AND node.JOB_FAMILY_PK_ID = ?\n" +
                "      AND node.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND node.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND node.SAVE_STATE_CODE_FK_ID= 'SAVED' AND node.IS_DELETED = 0\n" +
                "  AND parent.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND parent.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND parent.SAVE_STATE_CODE_FK_ID= 'SAVED' AND parent.IS_DELETED = 0\n" +
                "ORDER BY parent.INTRNL_LEFT_POS_INT;";
        List<Object> params = new ArrayList<>();
        params.add(Util.convertStringIdToByteId(contextID));
        ResultSet rs = executeSQL(SQL, params);
        try {
            while(rs.next()){
                JobFamilyBreakUpDataVO jobFamilyBreakUpDataVO = new JobFamilyBreakUpDataVO();
                jobFamilyBreakUpDataVO.setJobFamilyName(Util.getG11nValue(rs.getString(1), null));
                jobFamilyBreakUpDataVO.setJobFamilyId(Util.convertByteToString(rs.getBytes(2)));
                jobFamilyBreakUpDataVO.setJobFamilyType(Util.convertByteToString(rs.getBytes(3)));
//                jobFamilyBreakUpDataVO.setCostCentreTypeName(Util.getG11nValue(rs.getString(4), null));
                jobFamilyBreakUpDataVOS.add(jobFamilyBreakUpDataVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jobFamilyBreakUpDataVOS;
    }

    @Override
    public List<LayoutPermissionsVO> getLayoutPermissionsForControls(Set<String> sectionIDs, String deviceType) {
        List<LayoutPermissionsVO> layoutPermissionsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT *\n" +
                "FROM T_UI_ADM_CONTROL_INSTANCE_PERMISSION\n" +
                "  INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CONTROL_INSTANCE_PK_ID = CONTROL_INSTANCE_FK_ID\n" +
                "  INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID " +
                "WHERE  SCREEN_SECTION_INSTANCE_FK_ID IN (");
        sql = addInClauseParam(sectionIDs, sql);
        sql.append(") AND ");
        sql.append(getApplicability(deviceType));
        sql.append(";");
        List<Object> params = new ArrayList<>();
        for(String relevantSectionId : sectionIDs){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()){
                LayoutPermissionsVO layoutPermissionsVO = new LayoutPermissionsVO();
                layoutPermissionsVO.setPkId(Util.convertByteToString(rs.getBytes("CONTROL_INST_PERMSN_PK_ID")));
                layoutPermissionsVO.setFkId(Util.convertByteToString(rs.getBytes("CONTROL_INSTANCE_FK_ID")));
                layoutPermissionsVO.setPermissionType(rs.getString("PERMISSION_TYPE_CODE_FK_ID"));
                layoutPermissionsVO.setPermissionValue(rs.getString("PERMISSION_VALUE_LNG_TXT"));
                layoutPermissionsVO.setSubjectUserQualifier(rs.getString("ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
                layoutPermissionsVO.setNegationPermissionType(rs.getString("NEG_PERMISSION_TYPE_CODE_FK_ID"));
                layoutPermissionsVO.setNegationPermissionValue(rs.getString("NEG_PERMISSION_VALUE_LNG_TXT"));
                layoutPermissionsVO.setNegationSubjectUserQualifier(rs.getString("NEG_ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
                layoutPermissionsVOS.add(layoutPermissionsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return layoutPermissionsVOS;
    }

    @Override
    public List<LayoutPermissionsVO> getLayoutPermissionsForSectionActions(Set<String> sectionIDs, String deviceType) {
        List<LayoutPermissionsVO> layoutPermissionsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT *\n" +
                "FROM T_UI_ADM_SECTION_ACTION_INSTANCE_PERMISSION\n" +
                "  INNER JOIN T_UI_ADM_SECTION_ACTION_INSTANCE ON SECTION_ACTION_INSTANCE_FK_ID = SECTION_ACTION_INSTANCE_PK_ID " +
                "  INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID " +
                "WHERE  SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(sectionIDs, sql);
        sql.append(") AND ");
        sql.append(getApplicability(deviceType));
        sql.append(";");
        List<Object> params = new ArrayList<>();
        for(String relevantSectionId : sectionIDs){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()){
                LayoutPermissionsVO layoutPermissionsVO = new LayoutPermissionsVO();
                layoutPermissionsVO.setPkId(Util.convertByteToString(rs.getBytes("SECTN_ACTION_INST_PERMSN_PK_ID")));
                layoutPermissionsVO.setFkId(Util.convertByteToString(rs.getBytes("SECTION_ACTION_INSTANCE_FK_ID")));
                layoutPermissionsVO.setPermissionType(rs.getString("PERMISSION_TYPE_CODE_FK_ID"));
                layoutPermissionsVO.setPermissionValue(rs.getString("PERMISSION_VALUE_LNG_TXT"));
                layoutPermissionsVO.setSubjectUserQualifier(rs.getString("ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
                layoutPermissionsVO.setNegationPermissionType(rs.getString("NEG_PERMISSION_TYPE_CODE_FK_ID"));
                layoutPermissionsVO.setNegationPermissionValue(rs.getString("NEG_PERMISSION_VALUE_LNG_TXT"));
                layoutPermissionsVO.setNegationSubjectUserQualifier(rs.getString("NEG_ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
                layoutPermissionsVO.setRecordStatePermission(rs.getString("RECORD_STATE_PERM_BIG_TXT"));
                layoutPermissionsVOS.add(layoutPermissionsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return layoutPermissionsVOS;
    }

    @Override
    public List<LayoutPermissionsVO> getLayoutPermissionsForControlActions(Set<String> relevantSectionIdList, String deviceType) {
        List<LayoutPermissionsVO> layoutPermissionsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT *\n" +
                "FROM T_UI_ADM_CONTROL_ACTION_INSTANCE_PERMISSION " +
                "  INNER JOIN T_UI_ADM_CONTROL_ACTION_INSTANCE ON CONTROL_ACTION_INSTANCE_FK_ID = CONTROL_ACTION_INSTANCE_PK_ID\n" +
                "  INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CONTROL_INSTANCE_PK_ID = CONTROL_INSTANCE_FK_ID\n" +
                "  INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID " +
                "WHERE  SCREEN_SECTION_INSTANCE_FK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(") AND ");
        sql.append(getApplicability(deviceType));
        sql.append(";");
        List<Object> params = new ArrayList<>();
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()){
                LayoutPermissionsVO layoutPermissionsVO = new LayoutPermissionsVO();
                layoutPermissionsVO.setPkId(Util.convertByteToString(rs.getBytes("CONTROL_ACTION_INST_PERMSN_PK_ID")));
                layoutPermissionsVO.setFkId(Util.convertByteToString(rs.getBytes("CONTROL_ACTION_INSTANCE_FK_ID")));
                layoutPermissionsVO.setPermissionType(rs.getString("PERMISSION_TYPE_CODE_FK_ID"));
                layoutPermissionsVO.setPermissionValue(rs.getString("PERMISSION_VALUE_LNG_TXT"));
                layoutPermissionsVO.setSubjectUserQualifier(rs.getString("ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
                layoutPermissionsVO.setNegationPermissionType(rs.getString("NEG_PERMISSION_TYPE_CODE_FK_ID"));
                layoutPermissionsVO.setNegationPermissionValue(rs.getString("NEG_PERMISSION_VALUE_LNG_TXT"));
                layoutPermissionsVO.setNegationSubjectUserQualifier(rs.getString("NEG_ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
                layoutPermissionsVO.setRecordStatePermission(rs.getString("RECORD_STATE_PERM_BIG_TXT"));
                layoutPermissionsVOS.add(layoutPermissionsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return layoutPermissionsVOS;
    }

    @Override
    public List<LayoutPermissionsVO> getLayoutPermissionsForSections(Set<String> relevantSectionIdList, String deviceType) {
        List<LayoutPermissionsVO> layoutPermissionsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * " +
                "FROM T_UI_ADM_SECTION_INSTANCE_PERMISSION INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID " +
                "WHERE SECTION_INSTANCE_FK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(") AND ");
        sql.append(getApplicability(deviceType));
        sql.append(";");
        List<Object> params = new ArrayList<>();
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()){
                LayoutPermissionsVO layoutPermissionsVO = new LayoutPermissionsVO();
                layoutPermissionsVO.setPkId(Util.convertByteToString(rs.getBytes("SECTN_INST_PERMSN_PK_ID")));
                layoutPermissionsVO.setFkId(Util.convertByteToString(rs.getBytes("SECTION_INSTANCE_FK_ID")));
                layoutPermissionsVO.setPermissionType(rs.getString("PERMISSION_TYPE_CODE_FK_ID"));
                layoutPermissionsVO.setPermissionValue(rs.getString("PERMISSION_VALUE_LNG_TXT"));
                layoutPermissionsVO.setSubjectUserQualifier(rs.getString("ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
                layoutPermissionsVO.setNegationPermissionType(rs.getString("NEG_PERMISSION_TYPE_CODE_FK_ID"));
                layoutPermissionsVO.setNegationPermissionValue(rs.getString("NEG_PERMISSION_VALUE_LNG_TXT"));
                layoutPermissionsVO.setNegationSubjectUserQualifier(rs.getString("NEG_ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
                layoutPermissionsVOS.add(layoutPermissionsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return layoutPermissionsVOS;
    }

    @Override
    public String getReportingSectionID() {
        String SQL = "SELECT SCREEN_SECTION_INSTANCE_PK_ID FROM T_UI_ADM_SCREEN_SECTION_INSTANCE WHERE SCREEN_SECTION_MASTER_CODE_FK_ID = 'REPORTING_DATA_SECN'";
        List<Object> params = new ArrayList<>();
        ResultSet rs = executeSQL(SQL, params);
        try {
            if(rs.next()){
                return Util.convertByteToString(rs.getBytes(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<JobRelationshipsVO> getDefaultJobRelations(String contextID1, String contextID2) {
        List<JobRelationshipsVO> relationshipsVOS = new ArrayList<>();
        String SQL = "SELECT T_PFM_ADM_JOB_RELNSHIP_MAPPER.RELATIONSHIP_TYPE_CODE_FK_ID,T_PFM_ADM_JOB_RELNSHIP_MAPPER.RELATED_PERSON_FK_ID,T_PRN_EU_PERSON.LEGAL_FULL_NAME_G11N_BIG_TXT,   T_PRN_LKP_JOB_RELATIONSHIP_TYPE.RELATIONSHIP_TYPE_NAME_G11N_BIG_TXT\n" +
                "FROM T_PFM_ADM_JOB_RELNSHIP_MAPPER INNER  JOIN T_PRN_EU_PERSON on PERSON_PK_ID = RELATED_PERSON_FK_ID INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE ON RELATIONSHIP_TYPE_CODE_FK_ID = RELATIONSHIP_TYPE_CODE_PK_ID\n" +
                "WHERE CUSTOM_MAP_COL_1_CODE_FK_ID = ? AND CUSTOM_MAP_COL_1_FK_ID IN (SELECT FUNC.SLF_PRNT_ORGANIZATION_HCY_FK_ID\n" +
                "                                                                     FROM T_ORG_EU_ORGANIZATION DEPT INNER JOIN\n" +
                "                                                                       T_ORG_EU_ORGANIZATION SECN\n" +
                "                                                                         ON DEPT.SLF_PRNT_ORGANIZATION_HCY_FK_ID =\n" +
                "                                                                            SECN.ORGANIZATION_PK_ID\n" +
                "                                                                       INNER JOIN T_ORG_EU_ORGANIZATION FUNC\n" +
                "                                                                         ON SECN.SLF_PRNT_ORGANIZATION_HCY_FK_ID =\n" +
                "                                                                            FUNC.ORGANIZATION_PK_ID\n" +
                "                                                                     WHERE DEPT.ORGANIZATION_PK_ID = ?);";
        List<Object> params = new ArrayList<>();
        params.add(contextID1);
        params.add(Util.convertStringIdToByteId(contextID2));
        ResultSet rs = executeSQL(SQL, params);
        try {
            while (rs.next()){
                JobRelationshipsVO jobRelationshipsVO = new JobRelationshipsVO();
                jobRelationshipsVO.setRelationshipType(rs.getString(1));
                jobRelationshipsVO.setRelatedPersonID(Util.convertByteToString(rs.getBytes(2)));
                jobRelationshipsVO.setRelatedPersonName(Util.getG11nValue(rs.getString(3), null));
                jobRelationshipsVO.setRelationshipTypeName(Util.getG11nValue(rs.getString(4), null));
                relationshipsVOS.add(jobRelationshipsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return relationshipsVOS;
    }

    @Override
    public List<ControlActionInstanceVO> getControlActionInstances(String screenInstancePkId, Set<String> relevantSectionIdList) {
        List<ControlActionInstanceVO> actionInstanceVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_CONTROL_ACTION_INSTANCE INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CONTROL_INSTANCE_FK_ID = CONTROL_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                ControlActionInstanceVO instanceVO = new ControlActionInstanceVO();
                instanceVO.setActionPkId(Util.convertByteToString(rs.getBytes("CONTROL_ACTION_INSTANCE_PK_ID")));
                instanceVO.setControlFkId(Util.convertByteToString(rs.getBytes("CONTROL_INSTANCE_FK_ID")));
                instanceVO.setActionMasterCode(rs.getString("SCREEN_SECTION_ACTION_MASTER_CODE_FK_ID"));
                instanceVO.setActionGroupCode(rs.getString("ACTION_GROUP_CODE_FK_ID"));
                instanceVO.setCnfMsgTxt(getG11nValue(rs.getString("CNFM_MSG_G11N_BIG_TXT")));
                instanceVO.setSuccessMessage(getG11nValue(rs.getString("SUCCESS_MSG_G11N_BIG_TXT")));
                instanceVO.setFailureMessage(getG11nValue(rs.getString("FAILURE_MSG_G11N_BIG_TXT")));
                instanceVO.setApplicableToControlState(rs.getString("CONTROL_STATE_TXT"));
                instanceVO.setMtPEAlias(rs.getString("META_PROCESS_ELEMENT_ALIAS_TXT"));
                actionInstanceVOS.add(instanceVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actionInstanceVOS;
    }

    @Override
    public List<ControlActionArgumentVO> getControlActionArguments(String screenInstancePkId, Set<String> relevantSectionIdList) {
        List<ControlActionArgumentVO> actionArgumentVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_CONTROL_ACTION_ARGUMENT INNER JOIN T_UI_ADM_CONTROL_ACTION_INSTANCE ON CONTROL_ACTION_INSTANCE_FK_ID = CONTROL_ACTION_INSTANCE_PK_ID INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CONTROL_INSTANCE_FK_ID = CONTROL_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                ControlActionArgumentVO controlActionArgumentVO = new ControlActionArgumentVO();
                controlActionArgumentVO.setActionArgumentPkId(Util.convertByteToString(rs.getBytes("CONTROL_ACTION_ARGUMENT_PK_ID")));
                controlActionArgumentVO.setControlActionFkId(Util.convertByteToString(rs.getBytes("CONTROL_ACTION_INSTANCE_FK_ID")));
                controlActionArgumentVO.setArgumentParameter(rs.getString("ARGUMENT_PARAMETER_TXT"));
                controlActionArgumentVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
                controlActionArgumentVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                controlActionArgumentVO.setParentMTPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                controlActionArgumentVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                controlActionArgumentVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                controlActionArgumentVO.setAttributeValue(rs.getString("VALUE_EXPN"));
                actionArgumentVOS.add(controlActionArgumentVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actionArgumentVOS;
    }

    @Override
    public List<ControlActionTargetsVO> getControlActionTargets(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType) {
        List<ControlActionTargetsVO> actionTargetsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_TAP_CTRL_ACTN_TGT_SECN_MAP INNER JOIN T_UI_ADM_CONTROL_ACTION_INSTANCE ON CONTROL_ACTION_INSTANCE_FK_ID = CONTROL_ACTION_INSTANCE_PK_ID INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CONTROL_INSTANCE_FK_ID = CONTROL_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(") AND ").append("T_UI_TAP_CTRL_ACTN_TGT_SECN_MAP.");
        sql.append(getApplicability(deviceType));
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                ControlActionTargetsVO actionTargetsVO = new ControlActionTargetsVO();
                populateControlActionTargetFromRS(rs, actionTargetsVO);
                actionTargetsVOS.add(actionTargetsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actionTargetsVOS;
    }

    @Override
    public List<ControlActionTargetSectionSortParamsVO> getControlActionTargetSortParams(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType) {
        List<ControlActionTargetSectionSortParamsVO> sectionSortParamsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_CTRL_ACTN_TARGET_SECN_SORT_PARMS INNER JOIN T_UI_TAP_CTRL_ACTN_TGT_SECN_MAP ON UI_CTRL_ACTN_SECN_LYT_MAP_FK_ID = UI_CTRL_ACTN_SECN_LYT_MAP_PK_ID INNER JOIN T_UI_ADM_CONTROL_ACTION_INSTANCE ON CONTROL_ACTION_INSTANCE_FK_ID = CONTROL_ACTION_INSTANCE_PK_ID INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CONTROL_INSTANCE_FK_ID = CONTROL_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(") AND T_UI_TAP_CTRL_ACTN_TGT_SECN_MAP.");
        sql.append(getApplicability(deviceType)).append(";");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                ControlActionTargetSectionSortParamsVO sortParamsVO = new ControlActionTargetSectionSortParamsVO();
                sortParamsVO.setSortParamsPkId(Util.convertByteToString(rs.getBytes("CTRL_ACTN_TARGET_SECN_SORT_PARMS_PK_ID")));
                sortParamsVO.setActionTargetFkId(Util.convertByteToString(rs.getBytes("UI_CTRL_ACTN_SECN_LYT_MAP_FK_ID")));
                sortParamsVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                sortParamsVO.setDefaultOnLoadSort(rs.getString("DEFAULT_ON_LOAD_SORT_EXPN"));
                sectionSortParamsVOS.add(sortParamsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sectionSortParamsVOS;
    }

    @Override
    public List<ControlActionTargetSectionFilterParamsVO> getControlActionTargetFilterParams(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType) {
        List<ControlActionTargetSectionFilterParamsVO> filterParamsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_CTRL_ACTN_TARGET_SECN_FILTER_PARMS INNER JOIN T_UI_TAP_CTRL_ACTN_TGT_SECN_MAP ON UI_CTRL_ACTN_SECN_LYT_MAP_FK_ID = UI_CTRL_ACTN_SECN_LYT_MAP_PK_ID INNER JOIN T_UI_ADM_CONTROL_ACTION_INSTANCE ON CONTROL_ACTION_INSTANCE_FK_ID = CONTROL_ACTION_INSTANCE_PK_ID INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CONTROL_INSTANCE_FK_ID = CONTROL_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(") AND T_UI_TAP_CTRL_ACTN_TGT_SECN_MAP.");
        sql.append(getApplicability(deviceType));
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                ControlActionTargetSectionFilterParamsVO filterParamsVO = new ControlActionTargetSectionFilterParamsVO();
                filterParamsVO.setFilterParamsPkId(Util.convertByteToString(rs.getBytes("CTRL_ACTN_TARGET_SECN_FILTER_PARMS_PK_ID")));
                filterParamsVO.setActionTargetFkId(Util.convertByteToString(rs.getBytes("UI_CTRL_ACTN_SECN_LYT_MAP_FK_ID")));
                filterParamsVO.setTargetMTPE(rs.getString("TGT_PROCESS_ELEMENT_CODE_FK_ID"));
                filterParamsVO.setTargetMTPEAlias(rs.getString("TGT_PROCESS_ELEMENT_ALIAS_TXT"));
                filterParamsVO.setTargetParentMTPEAlias(rs.getString("TGT_PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                filterParamsVO.setTargetFkRelnWithParent(rs.getString("TGT_FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                filterParamsVO.setAttributePathExpn(rs.getString("CONTROL_ATTRIBUTE_PATH_EXPN"));
                filterParamsVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                filterParamsVO.setCurrentMTPE(rs.getString("CURR_PROCESS_ELEMENT_CODE_FK_ID"));
                filterParamsVO.setCurrentMTPEAlias(rs.getString("CURR_PROCESS_ELEMENT_ALIAS_TXT"));
                filterParamsVO.setCurrentParentMTPEAlias(rs.getString("CURR_PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                filterParamsVO.setCurrentFkRelnWithParent(rs.getString("CURR_FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                filterParamsVO.setAttributeValue(rs.getString("VALUE_EXPN"));
                filterParamsVO.setAlwaysApplied(rs.getBoolean("IS_ALWAYS_APPLIED"));
                filterParamsVO.setFilter(rs.getBoolean("IS_FILTER"));
                filterParamsVO.setHavingClause(rs.getBoolean("IS_HAVING_CLAUSE"));
                filterParamsVOS.add(filterParamsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return filterParamsVOS;
    }

    @Override
    public List<ControlActionTargetFilterCriteriaVO> getControlActionTargetFilterCriteria(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType) {
        List<ControlActionTargetFilterCriteriaVO> filterCriteriaVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_TAP_CTRL_ACTN_TGT_SCTN_MAP_FILTER_CRITERIA INNER JOIN T_UI_TAP_CTRL_ACTN_TGT_SECN_MAP ON UI_CTRL_ACTN_SECN_LYT_MAP_FK_ID = UI_CTRL_ACTN_SECN_LYT_MAP_PK_ID INNER JOIN T_UI_ADM_CONTROL_ACTION_INSTANCE ON CONTROL_ACTION_INSTANCE_FK_ID = CONTROL_ACTION_INSTANCE_PK_ID INNER JOIN T_UI_ADM_CONTROL_INSTANCE ON CONTROL_INSTANCE_FK_ID = CONTROL_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(") AND T_UI_TAP_CTRL_ACTN_TGT_SECN_MAP.");
        sql.append(getApplicability(deviceType));
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                ControlActionTargetFilterCriteriaVO filterCriteriaVO = new ControlActionTargetFilterCriteriaVO();
                filterCriteriaVO.setFilterCriteriaPkId(Util.convertByteToString(rs.getBytes("CTRL_ACTN_SCTN_LAYOUT_MAP_FILTER_CRITERIA_PK_ID")));
                filterCriteriaVO.setActionTargetFkId(Util.convertByteToString(rs.getBytes("UI_CTRL_ACTN_SECN_LYT_MAP_FK_ID")));
                filterCriteriaVO.setFilterParameter(rs.getString("FILTER_PARAMETER_TXT"));
                filterCriteriaVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                filterCriteriaVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
                filterCriteriaVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                filterCriteriaVO.setParentMTPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                filterCriteriaVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                filterCriteriaVO.setValueExpn(rs.getString("FILTER_VALUE_EXPN"));
                filterCriteriaVOS.add(filterCriteriaVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return filterCriteriaVOS;
    }

    @Override
    public List<SectionActionInstanceVO> getSectionActionInstance(String screenInstancePkId, Set<String> relevantSectionIdList) {
        List<SectionActionInstanceVO> actionInstanceVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_SECTION_ACTION_INSTANCE INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID LEFT JOIN T_UI_TAP_ACTION_VISUAL_MASTER ON ACTION_VISUAL_MASTER_CODE_FK_ID = ACTION_VISUAL_MASTER_CODE_PK_ID LEFT JOIN T_UI_TAP_ACTION_GROUP ON ACTION_GROUP_MASTER_CODE_PK_ID = ACTION_GROUP_CODE_FK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                SectionActionInstanceVO instanceVO = new SectionActionInstanceVO();
                instanceVO.setActionPkId(Util.convertByteToString(rs.getBytes("SECTION_ACTION_INSTANCE_PK_ID")));
                instanceVO.setSectionInstanceFkId(Util.convertByteToString(rs.getBytes("SCREEN_SECTION_INSTANCE_FK_ID")));
                instanceVO.setActionMasterCode(rs.getString("SCREEN_SECTION_ACTION_MASTER_CODE_FK_ID"));
                instanceVO.setOverrideActionLabel(rs.getString("OVERRIDE_ACTION_LABEL_G11N_BIG_TXT"));
                instanceVO.setOverrideActionIcon(rs.getString("OVERRIDE_ACTION_ICON_CODE_FK_ID"));
                instanceVO.setActionGroupCode(rs.getString("ACTION_GROUP_CODE_FK_ID"));
                instanceVO.setActionGroupIcon(rs.getString("ACTION_GROUP_ICON_CODE_FK_ID"));
                instanceVO.setActionGroupName(rs.getString("ACTION_GROUP_NAME_G11N_BIG_TXT"));
                instanceVO.setActionVisualMasterCode(rs.getString("ACTION_VISUAL_MASTER_CODE_FK_ID"));
                instanceVO.setActionVisualMasterActionLabel(rs.getString("ACTION_VISUAL_NAME_G11N_BIG_TXT"));
                instanceVO.setActionVisualMasterActionIcon(rs.getString("ACTION_VISUAL_ICON_CODE_FK_ID"));
                instanceVO.setSeqNumber(rs.getInt("ACTION_INSTANCE_SEQ_NUM_POS_INT"));
                instanceVO.setControlPositionTxt(rs.getString("ACTION_CONTROL_POSITION_TXT"));
                instanceVO.setCnfMsgTxt(getG11nValue(rs.getString("CNFM_MSG_G11N_BIG_TXT")));
                instanceVO.setSuccessMessage(getG11nValue(rs.getString("SUCCESS_MSG_G11N_BIG_TXT")));
                instanceVO.setFailureMessage(getG11nValue(rs.getString("FAILURE_MSG_G11N_BIG_TXT")));
                instanceVO.setThemeSize(rs.getString("ACTION_THEME_SIZE_CODE_FK_ID"));
                instanceVO.setFontStyle(rs.getString("ACTION_FONT_STYLE_CODE_FK_ID"));
                instanceVO.setIconColour(rs.getString("ACTION_ICON_COLOUR_TXT"));
                instanceVO.setFontCase(rs.getString("ACTION_FONT_CASE_CODE_FK_ID"));
                instanceVO.setFontColour(rs.getString("ACTION_FONT_COLOUR_TXT"));
                instanceVO.setBackgroundColour(rs.getString("ACTION_BACKGROUND_COLOUR_TXT"));
                instanceVO.setMtPEAlias(rs.getString("META_PROCESS_ELEMENT_ALIAS_TXT"));
                instanceVO.setActivityLogged(rs.getBoolean("IS_ACTIVITY_LOGGED"));
                instanceVO.setBaseTemplateID(rs.getString("BASE_TEMPLATE_CODE_FK_ID"));

                actionInstanceVOS.add(instanceVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actionInstanceVOS;
    }

    @Override
    public List<SectionActionArgumentVO> getSectionActionArguments(String screenInstancePkId, Set<String> relevantSectionIdList) {
        List<SectionActionArgumentVO> actionArgumentVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_SECTION_ACTION_ARGUMENT INNER JOIN T_UI_ADM_SECTION_ACTION_INSTANCE ON SECTION_ACTION_INSTANCE_FK_ID = SECTION_ACTION_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(");");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                SectionActionArgumentVO sectionActionArgumentVO = new SectionActionArgumentVO();
                sectionActionArgumentVO.setActionArgumentPkId(Util.convertByteToString(rs.getBytes("SECTION_ACTION_ARGUMENT_PK_ID")));
                sectionActionArgumentVO.setSectionActionFkId(Util.convertByteToString(rs.getBytes("SECTION_ACTION_INSTANCE_FK_ID")));
                sectionActionArgumentVO.setArgumentParameter(rs.getString("ARGUMENT_PARAMETER_TXT"));
                sectionActionArgumentVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
                sectionActionArgumentVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                sectionActionArgumentVO.setParentMTPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                sectionActionArgumentVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                sectionActionArgumentVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                sectionActionArgumentVO.setAttributeValue(rs.getString("VALUE_EXPN"));
                actionArgumentVOS.add(sectionActionArgumentVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actionArgumentVOS;
    }

    @Override
    public List<SectionActionTargetsVO> getSectionActionTargets(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType) {
        List<SectionActionTargetsVO> actionTargetsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_TAP_SECN_ACTN_TGT_SECN_MAP INNER JOIN T_UI_ADM_SECTION_ACTION_INSTANCE ON SECTION_ACTION_INSTANCE_FK_ID = SECTION_ACTION_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(") AND T_UI_TAP_SECN_ACTN_TGT_SECN_MAP.");
        sql.append(getApplicability(deviceType));
        sql.append(";");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                SectionActionTargetsVO actionTargetsVO = new SectionActionTargetsVO();
                actionTargetsVO.setActionTargetPkId(Util.convertByteToString(rs.getBytes("UI_SECN_ACTN_SECN_LYT_MAP_PK_ID")));
                actionTargetsVO.setSectionActionFkId(Util.convertByteToString(rs.getBytes("SECTION_ACTION_INSTANCE_FK_ID")));
                actionTargetsVO.setTargetSections(rs.getString("TARGET_SECN_INST_IN_SEQ_LNG_TXT"));
                actionTargetsVOS.add(actionTargetsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actionTargetsVOS;
    }

    @Override
    public List<SectionActionTargetFilterCriteriaVO> getSectionActionTargetFilterCriteria(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType) {
        List<SectionActionTargetFilterCriteriaVO> filterCriteriaVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_TAP_SCTN_ACTN_TGT_SCTN_MAP_FILTER_CRITERIA INNER JOIN T_UI_TAP_SECN_ACTN_TGT_SECN_MAP ON UI_SECN_LYT_MAP_FK_ID = UI_SECN_ACTN_SECN_LYT_MAP_PK_ID INNER JOIN T_UI_ADM_SECTION_ACTION_INSTANCE ON SECTION_ACTION_INSTANCE_FK_ID = SECTION_ACTION_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(") AND T_UI_TAP_SECN_ACTN_TGT_SECN_MAP.");
        sql.append(getApplicability(deviceType)).append(";");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                SectionActionTargetFilterCriteriaVO filterCriteriaVO = new SectionActionTargetFilterCriteriaVO();
                filterCriteriaVO.setFilterCriteriaPkId(Util.convertByteToString(rs.getBytes("SCTN_ACTN_SECN_MAP_FILTER_CRITERIA_PK_ID")));
                filterCriteriaVO.setActionTargetFkId(Util.convertByteToString(rs.getBytes("UI_SECN_LYT_MAP_FK_ID")));
                filterCriteriaVO.setFilterParameter(rs.getString("FILTER_PARAMETER_TXT"));
                filterCriteriaVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                filterCriteriaVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
                filterCriteriaVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                filterCriteriaVO.setParentMTPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                filterCriteriaVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                filterCriteriaVO.setValueExpn(rs.getString("FILTER_VALUE_EXPN"));
                filterCriteriaVOS.add(filterCriteriaVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return filterCriteriaVOS;
    }

    @Override
    public List<SectionActionTargetSectionSortParamsVO> getSectionActionTargetSortParams(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType) {
        List<SectionActionTargetSectionSortParamsVO> sectionSortParamsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_SECN_ACTN_TARGET_SECN_SORT_PARMS INNER JOIN T_UI_TAP_SECN_ACTN_TGT_SECN_MAP  ON UI_SECN_LYT_MAP_FK_ID = UI_SECN_ACTN_SECN_LYT_MAP_PK_ID INNER JOIN T_UI_ADM_SECTION_ACTION_INSTANCE ON SECTION_ACTION_INSTANCE_FK_ID = SECTION_ACTION_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(") AND T_UI_TAP_SECN_ACTN_TGT_SECN_MAP.");
        sql.append(getApplicability(deviceType)).append(";");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                SectionActionTargetSectionSortParamsVO sortParamsVO = new SectionActionTargetSectionSortParamsVO();
                sortParamsVO.setSortParamsPkId(Util.convertByteToString(rs.getBytes("SECN_ACTN_TARGET_SECN_SORT_PARMS_PK_ID")));
                sortParamsVO.setActionTargetFkId(Util.convertByteToString(rs.getBytes("UI_SECN_LYT_MAP_FK_ID")));
                sortParamsVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
                sortParamsVO.setDefaultOnLoadSort(rs.getString("DEFAULT_ON_LOAD_SORT_EXPN"));
                sectionSortParamsVOS.add(sortParamsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sectionSortParamsVOS;
    }

    @Override
    public List<SectionActionTargetSectionFilterParamsVO> getSectionActionTargetFilterParams(String screenInstancePkId, Set<String> relevantSectionIdList, String deviceType) {
        List<SectionActionTargetSectionFilterParamsVO> filterParamsVOS = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_UI_ADM_SECN_ACTN_TARGET_SECN_FILTER_PARMS INNER JOIN T_UI_TAP_SECN_ACTN_TGT_SECN_MAP  ON UI_SECN_LYT_MAP_FK_ID = UI_SECN_ACTN_SECN_LYT_MAP_PK_ID INNER JOIN T_UI_ADM_SECTION_ACTION_INSTANCE ON SECTION_ACTION_INSTANCE_FK_ID = SECTION_ACTION_INSTANCE_PK_ID INNER JOIN T_UI_ADM_SCREEN_SECTION_INSTANCE ON SCREEN_SECTION_INSTANCE_FK_ID = SCREEN_SECTION_INSTANCE_PK_ID WHERE SCREEN_INSTANCE_FK_ID = x? AND SCREEN_SECTION_INSTANCE_PK_ID IN (");
        sql = addInClauseParam(relevantSectionIdList, sql);
        sql.append(") AND T_UI_TAP_SECN_ACTN_TGT_SECN_MAP.");
        sql.append(getApplicability(deviceType)).append(";");
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(screenInstancePkId));
        for(String relevantSectionId : relevantSectionIdList){
            params.add(Util.remove0x(relevantSectionId));
        }
        ResultSet rs = executeSQL(sql.toString(), params);
        try {
            while (rs.next()) {
                SectionActionTargetSectionFilterParamsVO filterParamsVO = new SectionActionTargetSectionFilterParamsVO();
                filterParamsVO.setFilterParamsPkId(Util.convertByteToString(rs.getBytes("SECN_ACTN_TARGET_SECN_FILTER_PARMS_PK_ID")));
                filterParamsVO.setActionTargetFkId(Util.convertByteToString(rs.getBytes("UI_SECN_LYT_MAP_FK_ID")));
                filterParamsVO.setTargetMTPE(rs.getString("TGT_PROCESS_ELEMENT_CODE_FK_ID"));
                filterParamsVO.setTargetMTPEAlias(rs.getString("TGT_PROCESS_ELEMENT_ALIAS_TXT"));
                filterParamsVO.setTargetParentMTPEAlias(rs.getString("TGT_PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                filterParamsVO.setTargetFkRelnWithParent(rs.getString("TGT_FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                filterParamsVO.setAttributePathExpn(rs.getString("CONTROL_ATTRIBUTE_PATH_EXPN"));
                filterParamsVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                filterParamsVO.setCurrentMTPE(rs.getString("CURR_PROCESS_ELEMENT_CODE_FK_ID"));
                filterParamsVO.setCurrentMTPEAlias(rs.getString("CURR_PROCESS_ELEMENT_ALIAS_TXT"));
                filterParamsVO.setCurrentParentMTPEAlias(rs.getString("CURR_PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                filterParamsVO.setCurrentFkRelnWithParent(rs.getString("CURR_FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                filterParamsVO.setAttributeValue(rs.getString("VALUE_EXPN"));
                filterParamsVO.setAlwaysApplied(rs.getBoolean("IS_ALWAYS_APPLIED"));
                filterParamsVO.setEliminateResultFromRoot(rs.getBoolean("IS_ELIMINATE_RSLT_FROM_ROOT"));
                filterParamsVO.setFilter(rs.getBoolean("IS_FILTER"));
                filterParamsVO.setHavingClause(rs.getBoolean("IS_HAVING_CLAUSE"));
                filterParamsVOS.add(filterParamsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return filterParamsVOS;
    }

    @Override
    public ControlActionTargetsVO getControlActionTargets(String actionTargetPkId) {
        ControlActionTargetsVO actionTargetsVO = new ControlActionTargetsVO();
        String sql = "SELECT * FROM T_UI_TAP_CTRL_ACTN_TGT_SECN_MAP WHERE UI_CTRL_ACTN_SECN_LYT_MAP_PK_ID = x?;";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(actionTargetPkId));
        ResultSet rs = executeSQL(sql, params);
        try {
            while (rs.next()) {
                populateControlActionTargetFromRS(rs, actionTargetsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actionTargetsVO;
    }

    private void populateControlActionTargetFromRS(ResultSet rs, ControlActionTargetsVO actionTargetsVO){
        try {
            actionTargetsVO.setActionTargetPkId(Util.convertByteToString(rs.getBytes("UI_CTRL_ACTN_SECN_LYT_MAP_PK_ID")));
            actionTargetsVO.setControlActionFkId(Util.convertByteToString(rs.getBytes("CONTROL_ACTION_INSTANCE_FK_ID")));
            actionTargetsVO.setTargetSections(rs.getString("TARGET_SECN_INST_IN_SEQ_LNG_TXT"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public SectionActionTargetsVO getSectionActionTargets(String actionTargetPkId) {
        SectionActionTargetsVO actionTargetsVO = new SectionActionTargetsVO();
        String sql = "SELECT * FROM T_UI_TAP_SECN_ACTN_TGT_SECN_MAP WHERE UI_SECN_ACTN_SECN_LYT_MAP_PK_ID = x?;";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(actionTargetPkId));
        ResultSet rs = executeSQL(sql, params);
        try {
            while (rs.next()) {
                populateSectionActionTargetFromRS(rs, actionTargetsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return actionTargetsVO;
    }

    @Override
    public List<GenericQueryAttributesVO> getGenericQueryAttributes(String queryID) {
        List<GenericQueryAttributesVO> result = new ArrayList<>();
        String SQL = "SELECT * FROM T_UI_ADM_GENERIC_QUERY_ATTR WHERE GENERIC_QUERY_FK_ID = x?;";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(queryID));
        ResultSet rs = executeSQL(SQL, params);
        try{
           while (rs.next()){
               GenericQueryAttributesVO genericQueryAttributesVO = new GenericQueryAttributesVO();
               genericQueryAttributesVO.setAttributeID(Util.convertByteToString(rs.getBytes("GENERIC_QUERY_ATTR_PK_ID")));
               genericQueryAttributesVO.setQueryID(Util.convertByteToString(rs.getBytes("GENERIC_QUERY_FK_ID")));
               genericQueryAttributesVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
               genericQueryAttributesVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
               genericQueryAttributesVO.setParentMtPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
               genericQueryAttributesVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
               genericQueryAttributesVO.setAttributePath(rs.getString("ATTR_PATH_EXPN"));
               genericQueryAttributesVO.setAggregate(rs.getBoolean("IS_AGGREGATE"));
               result.add(genericQueryAttributesVO);
           }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<GenericQueryFiltersVO> getGenericQueryFilters(String queryID) {
        List<GenericQueryFiltersVO> result = new ArrayList<>();
        String SQL = "SELECT * FROM T_UI_ADM_GENERIC_QUERY_FILTER WHERE GENERIC_QUERY_FK_ID = x?;";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(queryID));
        ResultSet rs = executeSQL(SQL, params);
        try{
            while (rs.next()){
                GenericQueryFiltersVO genericQueryFiltersVO = new GenericQueryFiltersVO();
                genericQueryFiltersVO.setFilterID(Util.convertByteToString(rs.getBytes("GENERIC_QUERY_FLTR_PK_ID")));
                genericQueryFiltersVO.setQueryID(Util.convertByteToString(rs.getBytes("GENERIC_QUERY_FK_ID")));
                genericQueryFiltersVO.setTargetMtPE(rs.getString("TGT_PROCESS_ELEMENT_CODE_FK_ID"));
                genericQueryFiltersVO.setTargetMtPEAlias(rs.getString("TGT_PROCESS_ELEMENT_ALIAS_TXT"));
                genericQueryFiltersVO.setTargetParentMtPEAlias(rs.getString("TGT_PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                genericQueryFiltersVO.setTargetFkRelnWithParent(rs.getString("TGT_FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                genericQueryFiltersVO.setAttributePathExpn(rs.getString("CONTROL_ATTRIBUTE_PATH_EXPN"));
                genericQueryFiltersVO.setOperator(rs.getString("OPER_CODE_FK_ID"));
                genericQueryFiltersVO.setCurrentMtPE(rs.getString("CURR_PROCESS_ELEMENT_CODE_FK_ID"));
                genericQueryFiltersVO.setCurrentMtPEAlias(rs.getString("CURR_PROCESS_ELEMENT_ALIAS_TXT"));
                genericQueryFiltersVO.setCurrentParentMtPEAlias(rs.getString("CURR_PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
                genericQueryFiltersVO.setCurrentFkRelnWithParent(rs.getString("CURR_FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
                genericQueryFiltersVO.setAttributeValue(rs.getString("VALUE_EXPN"));
                genericQueryFiltersVO.setAlwaysApplied(rs.getBoolean("IS_ALWAYS_APPLIED"));
                //TODO make it correspond to the actual column
                genericQueryFiltersVO.setInteractiveFilter(rs.getBoolean("IS_INTERACTIVE_FILTER"));
                try{
                    genericQueryFiltersVO.setHavingClause(rs.getBoolean("IS_HAVING_CLAUSE"));
                }catch (Exception e){

                }

                result.add(genericQueryFiltersVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<String> getAllPersonIDs() {
        List<String> result = new ArrayList<>();
        String SQL = "SELECT PERSON_FK_ID FROM T_USR_ADM_USER WHERE IS_DELETED = 0 AND RECORD_STATE_CODE_FK_ID = 'CURRENT' AND SAVE_STATE_CODE_FK_ID = 'SAVED' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE';";
        List<Object> params = new ArrayList<>();
        ResultSet rs = executeSQL(SQL, params);
        try {
            while (rs.next()){
                String personId = Util.convertByteToString(rs.getBytes(1));
                result.add(personId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<OrgChartFilterLabelVO> getOrgChartFilterLabels(Set<String> sectionIDs) {
        if(sectionIDs == null || sectionIDs.size() == 0){
            return new ArrayList<>();
        }
        List<OrgChartFilterLabelVO> result = new ArrayList<>();
        StringBuilder SQL = new StringBuilder("SELECT * FROM T_ORG_ADM_ORGCHART_FLTR_LBL INNER JOIN T_UI_TAP_CONTROL_TYPE ON CONTROL_TYPE_CODE_FK_ID = CONTROL_TYPE_CODE_PK_ID WHERE SCRN_SECN_INST_FK_ID IN(");
        List<Object> params = new ArrayList<>();
        int i =0;
        for(String sectionID : sectionIDs){
            if(i!=0)
                SQL.append(",");
            SQL.append("x?");
            i++;
            params.add(Util.remove0x(sectionID));
        }
        SQL.append(")");
        SQL.append("ORDER BY SEQ_NUM_POS_INT");
        ResultSet rs = executeSQL(SQL.toString(), params);
        try {
            while (rs.next()){
                OrgChartFilterLabelVO orgChartFilterLabelVO = new OrgChartFilterLabelVO();
                orgChartFilterLabelVO.setOrgChartFilterPKID(Util.convertByteToString(rs.getBytes("ORGCHART_FILTER_LBL_PK_ID")));
                orgChartFilterLabelVO.setFilterSectionID(Util.convertByteToString(rs.getBytes("SCRN_SECN_INST_FK_ID")));
                orgChartFilterLabelVO.setLabelName(rs.getString("LABEL_NAME_G11N_BIG_TXT"));
                orgChartFilterLabelVO.setLabelIcon(rs.getString("LABEL_ICON_CODE_FK_ID"));
                orgChartFilterLabelVO.setControlType(rs.getString("CONTROL_TYPE_CODE_FK_ID"));
                orgChartFilterLabelVO.setControlTypeOnStateIcon(rs.getString("CONTROL_ON_STATE_ICON_CODE_FK_ID"));
                orgChartFilterLabelVO.setControlTypeOffStateIcon(rs.getString("CONTROL_OFF_STATE_ICON_CODE_FK_ID"));
                orgChartFilterLabelVO.setBaseTemplate(rs.getString("DISP_LBL_BASE_TMPLT_CODE_FK_ID"));
                orgChartFilterLabelVO.setLabelDOA(rs.getString("DISP_LBL_ATT_BT_DOA_CODE_FK_ID"));
                orgChartFilterLabelVO.setFarLabel(rs.getBoolean("IS_FAR_DISP_LABEL"));
                result.add(orgChartFilterLabelVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

//    @Override
//    public List<OrgChartFilterAttrPathVO> getOrgChartFilterAttrPaths(Set<String> sectionIDs) {
//        if(sectionIDs == null || sectionIDs.size() == 0){
//            return new ArrayList<>();
//        }
//        List<OrgChartFilterAttrPathVO> result = new ArrayList<>();
//        StringBuilder SQL = new StringBuilder("SELECT * FROM T_ORG_ADM_ORGCHART_FLTR_ATTR_PATH INNER JOIN T_ORG_ADM_ORGCHART_FLTR_LBL " +
//                "ON ORGCHART_FILTER_LBL_PK_ID = ORGCHART_FILTER_LBL_FK_ID WHERE T_ORG_ADM_ORGCHART_FLTR_LBL.SCRN_SECN_INST_FK_ID IN (");
//        List<Object> params = new ArrayList<>();
//        int i =0;
//        for(String sectionID : sectionIDs){
//            if(i!=0)
//                SQL.append(",");
//            SQL.append("x?");
//            i++;
//            params.add(Util.remove0x(sectionID));
//        }
//        SQL.append(")");
//        ResultSet rs = executeSQL(SQL.toString(), params);
//        try {
//            while (rs.next()) {
//                OrgChartFilterAttrPathVO orgChartFilterAttrPathVO = new OrgChartFilterAttrPathVO();
//                orgChartFilterAttrPathVO.setOrgChartFilterAttrPathPKID(Util.convertByteToString(rs.getBytes("ORGCHART_FLTR_ATTR_PATH_PK_ID")));
//                orgChartFilterAttrPathVO.setOrgChartLabel(Util.convertByteToString(rs.getBytes("ORGCHART_FILTER_LBL_FK_ID")));
//                orgChartFilterAttrPathVO.setScreenSectionID(Util.convertByteToString(rs.getBytes("SCRN_SECN_INST_FK_ID")));
//                orgChartFilterAttrPathVO.setControlAttrPathExpn(rs.getString("CONTROL_ATTR_PATH_EXPN"));
//                orgChartFilterAttrPathVO.setMtPE(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
//                orgChartFilterAttrPathVO.setMtPEAlias(rs.getString("PROCESS_ELEMENT_ALIAS_TXT"));
//                orgChartFilterAttrPathVO.setParentMTPEAlias(rs.getString("PARENT_PROCESS_ELEMENT_ALIAS_TXT"));
//                orgChartFilterAttrPathVO.setFkRelnWithParent(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
//                result.add(orgChartFilterAttrPathVO);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }

    @Override
    public List<OrgChartFilterRangeVO> getOrgChartFilterRanges(Set<String> sectionIDs) {
        if(sectionIDs == null || sectionIDs.size() == 0){
            return new ArrayList<>();
        }
        List<OrgChartFilterRangeVO> result = new ArrayList<>();
        StringBuilder SQL = new StringBuilder("SELECT * FROM T_ORG_ADM_ORGCHART_FLTR_RANGE " +
                "INNER JOIN T_ORG_ADM_ORGCHART_FLTR_LBL ON ORGCHART_FILTER_LBL_PK_ID = ORGCHART_FILTER_LBL_FK_ID " +
                "WHERE T_ORG_ADM_ORGCHART_FLTR_LBL.SCRN_SECN_INST_FK_ID IN (");
        List<Object> params = new ArrayList<>();
        int i =0;
        for(String sectionID : sectionIDs){
            if(i!=0)
                SQL.append(",");
            SQL.append("x?");
            i++;
            params.add(Util.remove0x(sectionID));
        }
        SQL.append(")");
        ResultSet rs = executeSQL(SQL.toString(), params);
        try {
            while (rs.next()) {
                OrgChartFilterRangeVO orgChartFilterRangeVO = new OrgChartFilterRangeVO();
                orgChartFilterRangeVO.setOrgChartFilterRangePKID(Util.convertByteToString(rs.getBytes("ORGCHART_FLTR_RANGE_PK_ID")));
                orgChartFilterRangeVO.setOrgChartLabel(Util.convertByteToString(rs.getBytes("ORGCHART_FILTER_LBL_FK_ID")));
                orgChartFilterRangeVO.setRangeName(rs.getString("RANGE_NAME_G11N_BIG_TXT"));
                orgChartFilterRangeVO.setRangeIcon(rs.getString("RANGE_ICON_CODE_FK_ID"));
                orgChartFilterRangeVO.setRangeMin(rs.getFloat("RANGE_MIN_VALUE_POS_DEC"));
                orgChartFilterRangeVO.setRangeMinName(rs.getString("RANGE_MIN_NAME_G11N_BIG_TXT"));
                orgChartFilterRangeVO.setRangeMinIcon(rs.getString("RANGE_MIN_ICON_CODE_FK_ID"));
                orgChartFilterRangeVO.setRangeMax(rs.getFloat("RANGE_MAX_VALUE_POS_DEC"));
                orgChartFilterRangeVO.setRangeMaxName(rs.getString("RANGE_MAX_NAME_G11N_BIG_TXT"));
                orgChartFilterRangeVO.setRangeMaxIcon(rs.getString("RANGE_MAX_ICON_CODE_FK_ID"));
                result.add(orgChartFilterRangeVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
/*
SELECT SAVE_STATE_CODE_PK_ID, SAVE_STATE_ICON_CODE_FK_ID FROM T_SYS_LKP_SAVE_STATE;
SELECT RECORD_STATE_CODE_PK_ID, RECORD_STATE_ICON_CODE_FK_ID FROM T_SYS_LKP_RECORD_STATE;
SELECT ACTIVE_STATE_CODE_PK_ID, ACTIVE_STATE_CODE_FK_ID FROM T_SYS_LKP_ACTIVE_STATE;
 */
    @Override
    public Map<String, StateVO> getActiveStateIcons() {
        Map<String, StateVO> activeStateIcon = new HashMap<>();
        String sql = "SELECT ACTIVE_STATE_CODE_PK_ID, ACTIVE_STATE_ICON_CODE_FK_ID, ICON_COLOUR_TXT FROM T_SYS_LKP_ACTIVE_STATE;";
        List<Object> params = new ArrayList<>();
        ResultSet rs = executeSQL(sql, params);
        try {
            while (rs.next()){
                StateVO stateVO = new StateVO();
                stateVO.setIconColor(rs.getString(3));
                stateVO.setStateIcon(Util.getIcon(rs.getString(2)));
                activeStateIcon.put(rs.getString(1),stateVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return activeStateIcon;
    }

    @Override
    public Map<String, StateVO> getRecordStateIcons() {
        Map<String, StateVO> recordStateIcon = new HashMap<>();
        String sql = "SELECT RECORD_STATE_CODE_PK_ID, RECORD_STATE_ICON_CODE_FK_ID, ICON_COLOUR_TXT FROM T_SYS_LKP_RECORD_STATE;";
        List<Object> params = new ArrayList<>();
        ResultSet rs = executeSQL(sql, params);
        try {
            while (rs.next()){
                StateVO stateVO = new StateVO();
                stateVO.setIconColor(rs.getString(3));
                stateVO.setStateIcon(Util.getIcon(rs.getString(2)));
                recordStateIcon.put(rs.getString(1),stateVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return recordStateIcon;
    }

    @Override
    public Map<String, StateVO> getSavedStateIcons() {
        Map<String, StateVO> savedStateIcon = new HashMap<>();
        String sql = "SELECT SAVE_STATE_CODE_PK_ID, SAVE_STATE_ICON_CODE_FK_ID, ICON_COLOUR_TXT FROM T_SYS_LKP_SAVE_STATE;";
        List<Object> params = new ArrayList<>();
        ResultSet rs = executeSQL(sql, params);
        try {
            while (rs.next()){
                StateVO stateVO = new StateVO();
                stateVO.setIconColor(rs.getString(3));
                stateVO.setStateIcon(Util.getIcon(rs.getString(2)));
                savedStateIcon.put(rs.getString(1),stateVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return savedStateIcon;
    }

    @Override
    public List<JobRelationshipTypeVO> getJobRelationshipTypes() {
        List<JobRelationshipTypeVO> relationshipTypeVOS = new ArrayList<>();
        String sql = "SELECT * FROM T_PRN_LKP_JOB_RELATIONSHIP_TYPE WHERE RECORD_STATE_CODE_FK_ID = 'CURRENT' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND IS_DELETED = 0 AND SAVE_STATE_CODE_FK_ID = 'SAVED';";
        List<Object> params = new ArrayList<>();
        ResultSet rs = executeSQL(sql, params);
        try {
            while (rs.next()){
                JobRelationshipTypeVO jobRelationshipTypeVO = new JobRelationshipTypeVO();
                jobRelationshipTypeVO.setRelationshipTypeCode(rs.getString("RELATIONSHIP_TYPE_CODE_PK_ID"));
                jobRelationshipTypeVO.setName(getG11nValue(rs.getString("RELATIONSHIP_TYPE_NAME_G11N_BIG_TXT")));
                jobRelationshipTypeVO.setIcon(rs.getString("RELATIONSHIP_TYPE_ICON_CODE_FK_ID"));
                jobRelationshipTypeVO.setImageID(Util.convertByteToString(rs.getBytes("RELATIONSHIP_TYPE_IMAGEID")));
                jobRelationshipTypeVO.setBorderType(rs.getString("VIZL_BORDER_TYPE_CODE_FK_ID"));
                jobRelationshipTypeVO.setBorderColour(rs.getString("VIZL_BORDER_COLOUR_TXT"));
                jobRelationshipTypeVO.setColleaguesAtWorkCountQuery(Util.convertByteToString(rs.getBytes("COL_AT_WORK_QUERY_FK_ID")));
                relationshipTypeVOS.add(jobRelationshipTypeVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return relationshipTypeVOS;
    }

    @Override
    public List<PersonDetailsVO> getFeaturingPersonDetail(String contextID, String featuredTableName) {
        List<PersonDetailsVO> personDetailsVOS = new ArrayList<>();
        String sql = "SELECT PERSON_PK_ID, NAME_INITIALS_TXT, PHOTO_IMAGEID, LEGAL_FULL_NAME_G11N_BIG_TXT, F.LAST_MODIFIED_DATETIME, FEATURED_PK_ID FROM "+featuredTableName+" F INNER JOIN T_PRN_EU_PERSON ON PERSON_PK_ID = FEATURING_PERSON_FK_ID WHERE DO_ROW_PRIMARY_KEY_FK_ID = ? AND RECORD_STATE_CODE_FK_ID = 'CURRENT' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND F.IS_DELETED = 0 AND SAVE_STATE_CODE_FK_ID = 'SAVED';";
        String personId = TenantContextHolder.getUserContext().getPersonId();
        List<Object> params = new ArrayList<>();
        params.add(Util.convertStringIdToByteId(contextID));
        ResultSet rs = executeSQL(sql, params);
        try {
            while (rs.next()) {
                PersonDetailsVO personDetailsVO = new PersonDetailsVO();
                personDetailsVO.setPersonId(Util.convertByteToString(rs.getBytes("PERSON_PK_ID")));
                personDetailsVO.setNameInitials(rs.getString("NAME_INITIALS_TXT"));
                personDetailsVO.setPersonPhoto(Util.convertByteToString(rs.getBytes("PHOTO_IMAGEID")));
                personDetailsVO.setPersonFullName(Util.getG11nValue(rs.getString("LEGAL_FULL_NAME_G11N_BIG_TXT"), personId));
                personDetailsVO.setFeaturedPkId(Util.convertByteToString(rs.getBytes("FEATURED_PK_ID")));
                personDetailsVO.setLastModifiedDatetime(Util.getStringTobeInserted("DATE_TIME", rs.getTimestamp("F.LAST_MODIFIED_DATETIME")).replace("'",""));
                personDetailsVOS.add(personDetailsVO);
            }
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return personDetailsVOS;
    }

    @Override
    public List<OrganizationBreakUpDataVO> getOrganizationBreakUpData(String contextID) {
        List<OrganizationBreakUpDataVO> organizationBreakUpDataVOS = new ArrayList<>();
        String SQL = "SELECT parent.ORGANIZATION_NAME_G11N_BIG_TXT, parent.ORGANIZATION_PK_ID, parent.SLF_PRNT_ORGANIZATION_HCY_FK_ID, ORGANIZATION_TYPE_NAME_G11N_BIG_TXT\n" +
                "FROM T_ORG_EU_ORGANIZATION AS node,\n" +
                "        T_ORG_EU_ORGANIZATION AS parent LEFT JOIN T_ORG_LKP_ORG_TYPE ON parent.ORGANIZATION_TYPE_FK_ID = ORGANIZATION_TYPE_PK_ID\n" +
                "WHERE node.INTRNL_LEFT_POS_INT BETWEEN parent.INTRNL_LEFT_POS_INT AND parent.INTRNL_RIGHT_POS_INT AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "        AND node.ORGANIZATION_PK_ID = ?\n" +
                "      AND node.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND node.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND node.SAVE_STATE_CODE_FK_ID= 'SAVED' AND node.IS_DELETED = 0\n" +
                "  AND parent.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND parent.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND parent.SAVE_STATE_CODE_FK_ID= 'SAVED' AND parent.IS_DELETED = 0\n" +
                "ORDER BY parent.INTRNL_LEFT_POS_INT;";
        List<Object> params = new ArrayList<>();
        params.add(Util.convertStringIdToByteId(contextID));
        ResultSet rs = executeSQL(SQL, params);
        try {
            while(rs.next()){
                OrganizationBreakUpDataVO  organizationBreakUpDataVO = new OrganizationBreakUpDataVO();
                organizationBreakUpDataVO.setOrgName(Util.getG11nValue(rs.getString(1), null));
                organizationBreakUpDataVO.setOrgPkId(Util.convertByteToString(rs.getBytes(2)));
                organizationBreakUpDataVO.setOrgType(Util.convertByteToString(rs.getBytes(3)));
//                organizationBreakUpDataVO.setOrgType(Util.convertByteToString(rs.getBytes(3)));
                organizationBreakUpDataVO.setOrgTypeName(Util.getG11nValue(rs.getString(4), null));
                organizationBreakUpDataVOS.add(organizationBreakUpDataVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return organizationBreakUpDataVOS;
    }

    private void populateSectionActionTargetFromRS(ResultSet rs, SectionActionTargetsVO actionTargetsVO) {
        try {
            actionTargetsVO.setActionTargetPkId(Util.convertByteToString(rs.getBytes("UI_SECN_ACTN_SECN_LYT_MAP_PK_ID")));
            actionTargetsVO.setSectionActionFkId(Util.convertByteToString(rs.getBytes("SECTION_ACTION_INSTANCE_FK_ID")));
            actionTargetsVO.setTargetSections(rs.getString("TARGET_SECN_INST_IN_SEQ_LNG_TXT"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public ScreenInstanceVO getLayout(String targetScreenInstance, List<String> targetSectionInstanceList, String deviceType, PersonGroup group) {
//        ScreenInstanceVO screenInstance = new ScreenInstanceVO();
//        StringBuilder screenInstancesql = new StringBuilder("SELECT\n" +
//                "  SCREEN_INSTANCE_PK_ID,\n" +
//                "  SCREEN_INSTANCE_NAME_G11N_BIG_TXT,\n" +
//                "  SCREEN_MASTER_CODE_FK_ID,\n" +
////                "  BASE_TEMPLATE_CODE_FK_ID,\n" +
//                "  THEME_SEQ_NUM_POS_INT\n" +
//                "FROM T_UI_ADM_SCREEN_INSTANCE\n" +
//                "  INNER JOIN T_UI_TAP_SCREEN_MASTER ON SCREEN_MASTER_CODE_FK_ID = SCREEN_MASTER_CODE_PK_ID\n" +
//                "WHERE " +
//                getApplicability(deviceType) +
//                " AND SCREEN_INSTANCE_PK_ID = x? AND\n" +
//                "      SCREEN_INSTANCE_PK_ID NOT IN (SELECT WHAT_MT_PE_PRIMARY_KEY_ID\n" +
//                "                                    FROM T_UI_ADM_ACCESS_MANAGER\n" +
//                "                                    WHERE IS_NOT_ACCESSIBLE AND WHO_GROUP_FK_ID IN (" );
//        for(int i=0; i<group.getGroupIds().size(); i++){
//            if(i!=0)
//                screenInstancesql.append(",");
//            screenInstancesql.append("x?");
//        }
//        if(group.getGroupIds().size()==0){
//            screenInstancesql.append("null");
//        }
//        screenInstancesql.append("));");
//        List<Object> params = new ArrayList<>();
//        params.add(Util.remove0x(targetScreenInstance));
//        for(String groupId : group.getGroupIds()){
//            params.add(Util.remove0x(groupId));
//        }
//        ResultSet resultSet = executeSQL(screenInstancesql.toString(), params);
//
//        try {
//            while (resultSet.next()) {
////                screenInstance.setBaseTemplateFkId(resultSet.getString("BASE_TEMPLATE_CODE_FK_ID"));
//                screenInstance.setScreenInstanceNameG11nBigTxt(getG11nValue(resultSet.getString("SCREEN_INSTANCE_NAME_G11N_BIG_TXT")));
//                screenInstance.setScreenInstancePkId(Util.convertByteToString(resultSet.getBytes("SCREEN_INSTANCE_PK_ID")));
//                screenInstance.setScreenMasterCodeFkId(resultSet.getString("SCREEN_MASTER_CODE_FK_ID"));
//                screenInstance.setThemeSeqNumPosInt(resultSet.getInt("THEME_SEQ_NUM_POS_INT"));
//            }
//            if(!StringUtil.isDefined(screenInstance.getScreenInstancePkId())){
//                return null;
//            }
//
//            StringBuilder sqlSection = new StringBuilder("SELECT\n" +
//                    "  *,\n" +
//                    "  SCREEN_SECTION_MASTER_CODE_FK_ID,\n" +
//            		"  SCREEN_INSTANCE_FK_ID,\n" +
//                    "  NO_DATA_MESSAGE_G11N_BIG_TXT,\n" +
//            		"  NO_DATA_MESSAGE_ICON_CODE_FK_ID,\n" +
//                    "  NO_DATA_MESSAGE_IMAGEID,\n" +
////                    "  TAB_SEQ_NUM_POS_INT,\n" +
//                    "  SCREEN_SECTION_INSTANCE_NAME_G11N_BIG_TXT,\n" +
//                    "  SCREEN_SECTION_INSTANCE_PK_ID,\n" +
//                    "  IS_SHOW_AT_INITIAL_LOAD,\n" +
//                    "  THEME_SEQ_NUM_POS_INT\n" +
//                    "FROM T_UI_ADM_SCREEN_SECTION_INSTANCE\n" +
//                    "  INNER JOIN T_UI_TAP_SCREEN_SECTION_MASTER ON SCREEN_SECTION_MASTER_CODE_FK_ID = SCREEN_SECTION_MASTER_CODE_PK_ID\n" +
//                    "WHERE SCREEN_INSTANCE_FK_ID = x? AND " +
//                    getApplicability(deviceType) +
//                    " AND\n" +
//                    "      T_UI_ADM_SCREEN_SECTION_INSTANCE.SCREEN_SECTION_INSTANCE_PK_ID NOT IN (SELECT WHAT_MT_PE_PRIMARY_KEY_ID\n" +
//                    "                                                                             FROM T_UI_ADM_ACCESS_MANAGER\n" +
//                    "                                                                             WHERE IS_NOT_ACCESSIBLE AND\n" +
//                    "                                                                                   WHO_GROUP_FK_ID IN (" );
//            for(int i=0; i<group.getGroupIds().size(); i++){
//                if(i!=0)
//                    sqlSection.append(",");
//                sqlSection.append("x?");
//            }
//            if(group.getGroupIds().size()==0){
//                sqlSection.append("null");
//            }
//            sqlSection.append(")");
//            sqlSection.append(")");
//            if(targetSectionInstanceList!=null && !targetSectionInstanceList.isEmpty()) {
//                sqlSection.append(" AND\n" +
//                        "      SCREEN_SECTION_MASTER_CODE_FK_ID IN (SELECT SCREEN_SECTION_MASTER_CODE_PK_ID\n" +
//                        "                                           FROM T_UI_TAP_SCREEN_SECTION_MASTER\n" +
//                        "                                           WHERE\n" +
//                        "                                             PARENT_SCREEN_SECTION_MASTER_CODE_FK_ID IN\n" +
//                        "                                             (SELECT SCREEN_SECTION_MASTER_CODE_FK_ID\n" +
//                        "                                              FROM T_UI_ADM_SCREEN_SECTION_INSTANCE\n" +
//                        "                                              WHERE SCREEN_SECTION_INSTANCE_PK_ID IN (");
//                for(int i=0; i<targetSectionInstanceList.size(); i++){
//                    if(i!=0)
//                        sqlSection.append(",");
//                    sqlSection.append("x?");
//                }
//                if(targetSectionInstanceList.size()==0){
//                    sqlSection.append("null");
//                }
//                sqlSection.append(")) OR SCREEN_SECTION_MASTER_CODE_PK_ID IN (SELECT SCREEN_SECTION_MASTER_CODE_FK_ID\n" +
//                        "                                                                      FROM T_UI_ADM_SCREEN_SECTION_INSTANCE\n" +
//                        "                                                                      WHERE SCREEN_SECTION_INSTANCE_PK_ID IN (");
//                for(int i=0; i<targetSectionInstanceList.size(); i++){
//                    if(i!=0)
//                        sqlSection.append(",");
//                    sqlSection.append("x?");
//                }
//                if(targetSectionInstanceList.size()==0){
//                    sqlSection.append("null");
//                }
//                sqlSection.append(")))");
//            }
//            List<Object> parameters = new ArrayList<>();
//            parameters.add(Util.remove0x(screenInstance.getScreenInstancePkId()));
//            for(String groupId : group.getGroupIds()){
//                parameters.add(Util.remove0x(groupId));
//            }
//            if(targetSectionInstanceList!=null) {
//                for(int i=0;i <targetSectionInstanceList.size(); i++){
//                    parameters.add(Util.remove0x(targetSectionInstanceList.get(i)));
//                }
//            }
//            if(targetSectionInstanceList!=null) {
//                for(int i=0;i <targetSectionInstanceList.size(); i++){
//                    parameters.add(Util.remove0x(targetSectionInstanceList.get(i)));
//                }
//            }
//            ResultSet rs = executeSQL(sqlSection.toString(), parameters);
//            List<ScreenSectionInstanceVO> screenSectionInstances = new ArrayList<>();
//            Map<String, ScreenSectionInstanceVO> screenSectionInstanceMap = new HashMap<>();
//            while (rs.next()){
//                ScreenSectionInstanceVO screenSectionInstance = new ScreenSectionInstanceVO();
//                screenSectionInstance.setSectionMasterCodeFkId(rs.getString("SCREEN_SECTION_MASTER_CODE_FK_ID"));
//                screenSectionInstance.setScreenSectionInstancePkId(Util.convertByteToString(rs.getBytes("SCREEN_SECTION_INSTANCE_PK_ID")));
//                screenSectionInstance.setParentSectionInstanceFkId(Util.convertByteToString(rs.getBytes("PARENT_SCREEN_SECTION_INSTANCE_FK_ID")));
//                screenSectionInstance.setSectionInstanceNameG11nBigTxt(getG11nValue(rs.getString("SCREEN_SECTION_INSTANCE_NAME_G11N_BIG_TXT")));
//                screenSectionInstance.setShowAtInitialLoad(rs.getBoolean("IS_SHOW_AT_INITIAL_LOAD"));
//                screenSectionInstance.setScreenInstanceFkId(Util.convertByteToString(rs.getBytes("SCREEN_INSTANCE_FK_ID")));
//                screenSectionInstance.setNoDataMessageG11nBigTxt(getG11nValue(rs.getString("NO_DATA_MESSAGE_G11N_BIG_TXT")));
//                screenSectionInstance.setNoDataMessageIconCode(Util.getIcon(rs.getString("NO_DATA_MESSAGE_ICON_CODE_FK_ID")));
//                screenSectionInstance.setNoDataMessageImageId(rs.getString("NO_DATA_MESSAGE_IMAGEID"));
//                screenSectionInstance.setThemeSeqNumPosInt(rs.getInt("THEME_SEQ_NUM_POS_INT"));
//                screenSectionInstance.setScreenSectionMasterName(rs.getString("SCREEN_SECTION_MASTER_TXT"));
//                screenSectionInstance.setParentScreenSectionMasterCodeFkId(rs.getString("PARENT_SCREEN_SECTION_MASTER_CODE_FK_ID"));
//                try {
//                    screenSectionInstance.setSecnUITypePosInt(rs.getInt("SECN_UI_TYPE_POS_INT"));
//                }catch (Exception e){}
//                screenSectionInstances.add(screenSectionInstance);
//                screenSectionInstanceMap.put(screenSectionInstance.getScreenSectionInstancePkId(), screenSectionInstance);
//            }
//            getSectionAction(screenSectionInstanceMap, deviceType, group);
//            Map<String, List<LoadParameters>> loadParamsMap = getLoadParameters("screenSectionInstances",screenSectionInstanceMap.keySet(),group);
//            for(Map.Entry<String, List<LoadParameters>> entry : loadParamsMap.entrySet()){
//                String fkId = entry.getKey();
//                screenSectionInstanceMap.get(fkId).setLoadParams(entry.getValue());
//            }
//            getFilterList(screenSectionInstanceMap, group.getPersonId());
//            getSectionControl(screenSectionInstanceMap, deviceType, group);
//            for(int i=0 ; i<screenSectionInstances.size();i++) {
//                ScreenSectionInstanceVO ssi = screenSectionInstances.get(i);
//                screenInstance.getSectionControls().addAll(ssi.getControls());
//            }
//            screenInstance.setScreenSectionInstances(screenSectionInstances);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//
//        return screenInstance;
//    }
//
//    private void getFilterList(Map<String, ScreenSectionInstanceVO> screenSectionInstancePkIdMap, String personId) {
//        Map<String, Filter> filterMap = new HashMap<>();
//        StringBuilder SQL = new StringBuilder("SELECT * FROM T_PFM_EU_FILTER WHERE SOURCE_SCREEN_SECTION_INSTANCE_FK_ID IN(");
//        for(int i = 0; i< screenSectionInstancePkIdMap.size(); i++){
//            if(i!=0)
//                SQL.append(",");
//            SQL.append("x?");
//        }
//        if(screenSectionInstancePkIdMap.size()==0){
//            SQL.append("null");
//        }
//        SQL.append(")");
//        SQL.append(" AND LOGGED_IN_PERSON_FK_ID = x?;");
//        List<Object> params = new ArrayList<>();
//        for(String screenSectionInstancePkId : screenSectionInstancePkIdMap.keySet()){
//            params.add(Util.remove0x(screenSectionInstancePkId));
//        }
//        params.add(Util.remove0x(personId));
//        ResultSet rs = executeSQL(SQL.toString(), params);
//        try {
//            while (rs.next()){
//                Filter filter = new Filter();
//                filter.setFilterPkId(Util.convertByteToString(rs.getBytes("FILTER_PK_ID")));
//                filter.setLoggedInPersonFkId(Util.convertByteToString(rs.getBytes("LOGGED_IN_PERSON_FK_ID")));
//                filter.setFilterNameG11nBigTxt(getG11nValue(rs.getString("FILTER_NAME_G11N_BIG_TXT")));
//                filter.setSourceFilterScreenSectionInstanceFkId(Util.convertByteToString(rs.getBytes("SOURCE_SCREEN_SECTION_INSTANCE_FK_ID")));
//                screenSectionInstancePkIdMap.get(filter.getSourceFilterScreenSectionInstanceFkId()).getFilterList().add(filter);
//                filterMap.put(filter.getFilterPkId(), filter);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        populateFilterCriteria(filterMap, personId);
//    }
//
//    private void populateFilterCriteria(Map<String, Filter> filterMap, String personId) {
//        StringBuilder SQL = new StringBuilder("SELECT * FROM T_PFM_EU_FILTER_CRITERIA WHERE FILTER_FK_ID IN (");
//        for(int i = 0; i< filterMap.keySet().size(); i++){
//            if(i!=0)
//                SQL.append(",");
//            SQL.append("x?");
//        }
//        if(filterMap.keySet().size()==0){
//            SQL.append("null");
//        }
//        SQL.append(");");
//        List<Object> params = new ArrayList<>();
//        for(String filterFkId : filterMap.keySet()){
//            params.add(Util.remove0x(filterFkId));
//        }
//        ResultSet rs = executeSQL(SQL.toString(), params);
//        try {
//            while (rs.next()) {
//                FilterCriteria filterCriteria = new FilterCriteria();
//                filterCriteria.setFilterCriteriaPkId(Util.convertByteToString(rs.getBytes("FILTER_CRITERIA_PK_ID")));
//                filterCriteria.setFilterFkId(Util.convertByteToString(rs.getBytes("FILTER_FK_ID")));
//                filterCriteria.setControlAttrExpn(rs.getString("CONTROL_ATTR_EXPN"));
//                filterCriteria.setOperFkId(rs.getString("OPER_CODE_FK_ID"));
//                filterCriteria.setValueExpn(rs.getString("VALUE_EXPN"));
//                filterCriteria.setSeqNumPosInt(rs.getInt("SEQ_NUM_POS_INT"));
//                filterMap.get(filterCriteria.getFilterFkId()).getFilterCriteriaList().add(filterCriteria);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public Map<String, List<LoadParameters>> getLoadParameters(String callFor, Set<String> fkIdList, PersonGroup group) {
//        Map<String, List<LoadParameters>> fkIdToLoadParamListMap = new HashMap<>();
//        StringBuilder sql = null;
//        switch(callFor){
//            case "screenSectionInstances":
//                sql = new StringBuilder("SELECT\n" +
//                        "  SCREEN_SECTION_INSTANCE_LOAD_PARMS_PK_ID AS 'pk',\n" +
//                		"  SCREEN_SECTION_INSTANCE_FK_ID            AS 'screenSectionInstance',\n" +
//                        "  PROCESS_ELEMENT_CODE_FK_ID               AS 'processCode',\n" +
//                        "  null                                     AS 'controlActionInstance',\n"+
//                        "  null                                     AS 'sectionActionInstance',\n"+
//                        "  DEFAULT_ON_LOAD_FILTER_EXPN              AS 'filterExpn',\n" +
//                        "  DEFAULT_ON_LOAD_SORT_EXPN                AS 'sortExpn'\n" +
//                        "FROM T_UI_ADM_SCREEN_SECTION_INSTANCE_LOAD_PARMS\n" +
//                        "WHERE SCREEN_SECTION_INSTANCE_FK_ID IN(");
//                for(int i=0; i<fkIdList.size(); i++){
//                    if(i!=0)
//                        sql.append(",");
//                    sql.append("x?");
//                }
//                if(fkIdList.size()==0){
//                    sql.append("null");
//                }
//                sql.append(")");
//                sql.append("AND\n" +
//                "      SCREEN_SECTION_INSTANCE_LOAD_PARMS_PK_ID NOT IN (SELECT WHAT_MT_PE_PRIMARY_KEY_ID\n" +
//                "                                                       FROM T_UI_ADM_ACCESS_MANAGER\n" +
//                "                                                       WHERE IS_NOT_ACCESSIBLE AND\n" +
//                "                                                             WHO_GROUP_FK_ID IN (");
//                break;
//            case "controlActionInstance":
//                sql = new StringBuilder("SELECT\n" +
//                        "  CONTROL_ACTION_INSTANCE_LOAD_PARMS_PK_ID AS 'pk',\n" +
//                        "  null                                     AS 'screenSectionInstance',\n" +
//                        "  null                                     AS 'sectionActionInstance',\n"+
//                		"  CONTROL_ACTION_INSTANCE_FK_ID            AS 'controlActionInstance',\n" +
//                        "  PROCESS_ELEMENT_CODE_FK_ID               AS 'processCode',\n" +
//                        "  DEFAULT_ON_LOAD_FILTER_EXPN              AS 'filterExpn',\n" +
//                        "  DEFAULT_ON_LOAD_SORT_EXPN                AS 'sortExpn'\n" +
//                        "FROM T_UI_ADM_CONTROL_ACTION_INSTANCE_LOAD_PARMS\n" +
//                        "WHERE CONTROL_ACTION_INSTANCE_FK_ID IN( ");
//                for(int i=0; i<fkIdList.size(); i++){
//                    if(i!=0)
//                        sql.append(",");
//                    sql.append("x?");
//                }
//                if(fkIdList.size()==0){
//                    sql.append("null");
//                }
//                sql.append(")");
//                sql.append("AND\n" +
//                "      CONTROL_ACTION_INSTANCE_LOAD_PARMS_PK_ID NOT IN (SELECT WHAT_MT_PE_PRIMARY_KEY_ID\n" +
//                "                                                       FROM T_UI_ADM_ACCESS_MANAGER\n" +
//                "                                                       WHERE IS_NOT_ACCESSIBLE AND\n" +
//                "                                                             WHO_GROUP_FK_ID IN (");
//                break;
//            case "sectionActionInstances":
//                sql = new StringBuilder("SELECT\n" +
//                        "  SECTION_ACTION_INSTANCE_LOAD_PARMS_PK_ID AS 'pk',\n" +
//                		"  SECTION_ACTION_INSTANCE_FK_ID            AS 'sectionActionInstance',\n" +
//                        "  null                                     AS 'screenSectionInstance',\n" +
//                        "  null                                     AS 'controlActionInstance',\n"+
//                        "  PROCESS_ELEMENT_CODE_FK_ID               AS 'processCode',\n" +
//                        "  DEFAULT_ON_LOAD_FILTER_EXPN              AS 'filterExpn',\n" +
//                        "  DEFAULT_ON_LOAD_SORT_EXPN                AS 'sortExpn'\n" +
//                        "FROM T_UI_ADM_SECTION_ACTION_INSTANCE_LOAD_PARMS\n" +
//                        "WHERE SECTION_ACTION_INSTANCE_FK_ID IN (");
//                for(int i=0; i<fkIdList.size(); i++){
//                    if(i!=0)
//                        sql.append(",");
//                    sql.append("x?");
//                }
//                if(fkIdList.size()==0){
//                    sql.append("null");
//                }
//                sql.append(")");
//                sql.append(" AND  SECTION_ACTION_INSTANCE_LOAD_PARMS_PK_ID NOT IN (SELECT WHAT_MT_PE_PRIMARY_KEY_ID\n" +
//                "                                                       FROM T_UI_ADM_ACCESS_MANAGER\n" +
//                "                                                       WHERE IS_NOT_ACCESSIBLE AND\n" +
//                "                                                             WHO_GROUP_FK_ID IN (");
//            break;
//            default: return null;
//        }
//        for(int i=0; i<group.getGroupIds().size(); i++){
//            if(i!=0)
//                sql.append(",");
//            sql.append("x?");
//        }
//        if(group.getGroupIds().size()==0){
//            sql.append("null");
//        }
//        sql.append("));");
//        List<Object> params = new ArrayList<>();
//        for(String fkId : fkIdList) {
//            params.add(Util.remove0x(fkId));
//        }
//        for(String groupId : group.getGroupIds()){
//            params.add(Util.remove0x(groupId));
//        }
//        try{
//            ResultSet rs = executeSQL(sql.toString(), params);
//            while (rs.next()){
//                LoadParameters lp = new LoadParameters();
//                lp.setLoadParamPkId(Util.convertByteToString(rs.getBytes("pk")));
//                lp.setProcessElementId(rs.getString("processCode"));
//                lp.setFilterExpn(rs.getString("filterExpn"));
//                lp.setSortExpn(rs.getString("sortExpn"));
//
//                lp.setScreenSectionInstanceFkId(Util.convertByteToString(rs.getBytes("screenSectionInstance")));
//                lp.setControlActionInstanceFkId(Util.convertByteToString(rs.getBytes("controlActionInstance")));
//                lp.setSectionActionInstanceFkId(Util.convertByteToString(rs.getBytes("sectionActionInstance")));
//                String fkId = null;
//                if(callFor.equals("screenSectionInstances")){
//                    fkId = Util.convertByteToString(rs.getBytes("screenSectionInstance"));
//                }else if (callFor.equals("controlActionInstance")){
//                    fkId = Util.convertByteToString(rs.getBytes("controlActionInstance"));
//                }else if (callFor.equals("sectionActionInstances")){
//                    fkId = Util.convertByteToString(rs.getBytes("sectionActionInstance"));
//                }
//                if(fkIdToLoadParamListMap.containsKey(fkId)){
//                    fkIdToLoadParamListMap.get(fkId).add(lp);
//                }else{
//                    List<LoadParameters> result = new ArrayList<>();
//                    result.add(lp);
//                    fkIdToLoadParamListMap.put(fkId, result);
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return fkIdToLoadParamListMap;
//    }
//
//    @Override
//    public List<String> getDefaultBTForGroup(String mtPECode, PersonGroup group) {
//        List<String> result = new ArrayList<>();
//        StringBuilder sql = new StringBuilder("SELECT DISTINCT BASE_TEMPLATE_CODE_FK_ID\n" +
//                "FROM T_PFM_ADM_TEMPLATE_MAPPER\n" +
//                "WHERE MT_PE_CODE_FK_ID = ? AND IS_META_DEFAULT AND T_PFM_ADM_TEMPLATE_MAPPER.PFM_GROUP_FK_ID IN (");
//        for(int i=0; i<group.getGroupIds().size(); i++){
//            if(i!=0)
//                sql.append(",");
//            sql.append("x?");
//        }
//        if(group.getGroupIds().size()==0){
//            sql.append("null");
//        }
//        sql.append(");");
//        List<Object> params = new ArrayList<>();
//        params.add(mtPECode);
//        for(String groupId : group.getGroupIds()){
//            params.add(Util.remove0x(groupId));
//        }
//        ResultSet rs  = executeSQL(sql.toString(), params);
//        try {
//            while (rs.next()){
//                String bt = rs.getString("BASE_TEMPLATE_CODE_FK_ID");
//                result.add(bt);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }
//
//    @Override
//    public String getSectionMasterCode(String targetSectionInstance) {
//        String result = null;
//        String SQL = "SELECT SCREEN_SECTION_MASTER_CODE_FK_ID FROM T_UI_ADM_SCREEN_SECTION_INSTANCE WHERE SCREEN_SECTION_INSTANCE_PK_ID = x?;";
//        List<Object> params = new ArrayList<>();
//        params.add(Util.remove0x(targetSectionInstance));
//        ResultSet rs  = executeSQL(SQL, params);
//        try {
//            if (rs.next()){
//                result = rs.getString("SCREEN_SECTION_MASTER_CODE_FK_ID");
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }
//
//    @Override
//    public SectionControlVO getControlData(String controlId) {
//        String SQL = "SELECT *\n" +
//                "FROM T_UI_ADM_CONTROL_INSTANCE\n" +
//                "  LEFT JOIN T_UI_TAP_PROPERTY_CONTROL_MASTER\n" +
//                "    ON CONTROL_ATTRIBUTE_PATH_EXPN = PROPERTY_CONTROL_MASTER_CODE_PK_ID AND IS_PROPERTY_CONTROL = 1\n" +
//                "WHERE CONTROL_INSTANCE_PK_ID = x?;";
//        List<Object> params = new ArrayList<>();
//        params.add(Util.remove0x(controlId));
//        ResultSet rs  = executeSQL(SQL, params);
//        List<SectionControlVO> sectionControls = getSectionControls(rs);
//        if(sectionControls!=null && sectionControls.get(0)!=null){
//            return sectionControls.get(0);
//        }
//        return null;
//    }
//
//    @Override
//    public Map<Integer, SectionLayoutMapper> getSectionLayoutMapper(String mtPE, String uiDispTypeCodeFkId, String tabSeqNumPosInt) {
//        Map<Integer, SectionLayoutMapper> tabToSectionLayoutMapperMap = new HashMap<>();
//        List<Object> params = new ArrayList<>();
//        StringBuilder SQL = new StringBuilder("SELECT * FROM T_UI_TAP_SECTION_LAYOUT_MAPPER WHERE MT_PE_CODE_FK_ID = ? AND UI_DISP_TYPE_CODE_FK_ID = ?");
//        params.add(mtPE);
//        params.add(uiDispTypeCodeFkId);
//        if(StringUtil.isDefined(tabSeqNumPosInt)){
//            // Specified tabSeq is required... We should add a filter for that tab.
//            SQL.append(" AND TAB_SEQ_NUM_POS_INT = ? ");
//            params.add(tabSeqNumPosInt);
//        }
//        SQL.append(" ORDER BY TAB_SEQ_NUM_POS_INT;");
//        ResultSet rs  = executeSQL(SQL.toString(), params);
//        try {
//            while (rs.next()){
//                SectionLayoutMapper sectionLayoutMapper = new SectionLayoutMapper();
//                sectionLayoutMapper.setUiSecLytMapPkId(Util.convertByteToString(rs.getBytes("UI_SECN_LYT_MAP_PK_ID")));
//                sectionLayoutMapper.setMtPECodeFkId(rs.getString("MT_PE_CODE_FK_ID"));
//                sectionLayoutMapper.setUiDispTypeCodeFKId(rs.getString("UI_DISP_TYPE_CODE_FK_ID"));
//                sectionLayoutMapper.setTabSeqNumPosInt(rs.getInt("TAB_SEQ_NUM_POS_INT"));
//                sectionLayoutMapper.setTargetSecnInstInSeqLngTxt(rs.getString("TARGET_SECN_INST_IN_SEQ_LNG_TXT"));
//                sectionLayoutMapper.setTabHdrPropControlExpn(rs.getString("TAB_HDR_PROP_CONTROL_EXPN"));
//                sectionLayoutMapper.setTabHdrStaticCntrlG11nBigTxt(getG11nValue(rs.getString("TAB_HDR_STATIC_CNTRL_G11N_BIG_TXT")));
//                tabToSectionLayoutMapperMap.put(sectionLayoutMapper.getTabSeqNumPosInt(), sectionLayoutMapper);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return tabToSectionLayoutMapperMap;
//    }
//
    @Override
    public String getScreenInstanceFromSection(String sectionId) {
        String screenId = null;
        String SQL = "SELECT SCREEN_INSTANCE_FK_ID FROM T_UI_ADM_SCREEN_SECTION_INSTANCE WHERE SCREEN_SECTION_INSTANCE_PK_ID = x? AND RECORD_STATE_CODE_FK_ID = 'CURRENT';";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(sectionId));
        ResultSet rs  = executeSQL(SQL, params);
        try {
            if (rs.next()){
                screenId = Util.convertByteToString(rs.getBytes(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return screenId;
    }
//
//    private void getSectionAction(Map<String, ScreenSectionInstanceVO> screenSectionInstanceMap, String deviceType, PersonGroup group){
//        StringBuilder sql =
//                new StringBuilder("SELECT\n" +
//                        "*\n," +
//                        "  SECTION_ACTION_INSTANCE_PK_ID,\n" +
//                        "  SCREEN_SECTION_INSTANCE_FK_ID,\n" +
//                        "  TARGET_SCREEN_INSTANCE_FK_ID,\n" +
//                        "  TARGET_SCREEN_SECTION_INSTANCE_FK_ID,\n" +
//                        "  TARGET_TAB_SEQ_NUM_POS_INT,\n" +
//                        "  ACTION_ARGUMENT_BIG_TXT,\n" +
//                        "  ACTION_INSTANCE_SEQ_NUM_POS_INT,\n" +
//                        "  CNFM_MSG_G11N_BIG_TXT,\n" +
//                        "  SUCCESS_MSG_G11N_BIG_TXT,\n" +
//                        "  FAILURE_MSG_G11N_BIG_TXT,\n" +
//                        "  SCREEN_SECTION_ACTION_MASTER_CODE_PK_ID,\n" +
//                        "  SCREEN_SECTION_ACTION_NAME_G11N_BIG_TXT,\n" +
//                        "  ACTION_GROUP_TXT,\n" +
//                        "  OVERRIDE_ACTION_LABEL_G11N_BIG_TXT,\n" +
//                        "  OVERRIDE_ACTION_ICON_CODE_FK_ID,\n" +
//                        "  SCREEN_SECTION_MASTER_CODE_FK_ID,\n" +
//                        "  THEME_SEQ_NUM_POS_INT,\n" +
//                        "  SCREEN_SECTION_ACTION_ICON_CODE_FK_ID\n" +
//                        "FROM T_UI_ADM_SECTION_ACTION_INSTANCE\n" +
//                        "  INNER JOIN T_UI_TAP_SCREEN_SECTION_ACTION_MASTER\n" +
//                        "    ON SCREEN_SECTION_ACTION_MASTER_CODE_FK_ID = SCREEN_SECTION_ACTION_MASTER_CODE_PK_ID\n" +
//                        "WHERE " +
//                        getApplicability(deviceType) +
//                        " AND SCREEN_SECTION_INSTANCE_FK_ID IN (");
//        for(int i =0; i< screenSectionInstanceMap.keySet().size(); i++){
//            if(i!=0)
//                sql.append(",");
//            sql.append("x?");
//        }
//        if(screenSectionInstanceMap.keySet().size()==0)
//            sql.append("null");
//        sql.append(")");
//        sql.append(" AND PFM_GROUP_FK_ID IN (");
//        for(int i=0; i<group.getGroupIds().size(); i++){
//            if(i!=0)
//                sql.append(",");
//            sql.append("x?");
//        }
//        if(group.getGroupIds().size()==0) {
//            sql.append("null");
//        }
//        sql.append(")");
//        sql.append("  AND T_UI_ADM_SECTION_ACTION_INSTANCE.SECTION_ACTION_INSTANCE_PK_ID NOT IN (SELECT WHAT_MT_PE_PRIMARY_KEY_ID\n" +
//                "                                                                             FROM T_UI_ADM_ACCESS_MANAGER\n" +
//                "                                                                             WHERE IS_NOT_ACCESSIBLE AND\n" +
//                "                                                                                   WHO_GROUP_FK_ID IN (" );
//        for(int i=0; i<group.getGroupIds().size(); i++){
//            if(i!=0)
//                sql.append(",");
//            sql.append("x?");
//        }
//        if(group.getGroupIds().size()==0){
//            sql.append("null");
//        }
//        sql.append("));");
//        List<Object> params = new ArrayList<>();
//        for(String screenSectionInstancePkId : screenSectionInstanceMap.keySet()){
//            params.add(Util.remove0x(screenSectionInstancePkId));
//        }
//        for(String groupId : group.getGroupIds()){
//            params.add(Util.remove0x(groupId));
//        }
//        for(String groupId : group.getGroupIds()){
//            params.add(Util.remove0x(groupId));
//        }
//        Map<String, SectionActionInstanceVO> sectionActionInstanceMap = new HashMap<>();
//        try {
//            ResultSet rs = executeSQL(sql.toString(), params);
//            while (rs.next()){
//                SectionActionInstanceVO sectionActionInstance = new SectionActionInstanceVO();
//                sectionActionInstance.setSectionActionInstancePkId(Util.convertByteToString(rs.getBytes("SECTION_ACTION_INSTANCE_PK_ID")));
//                sectionActionInstance.setScreenSectionInstanceFkId(Util.convertByteToString(rs.getBytes("SCREEN_SECTION_INSTANCE_FK_ID")));
//                sectionActionInstance.setActionArgumentTxt(rs.getString("ACTION_ARGUMENT_BIG_TXT"));
//                sectionActionInstance.setActionGroupTxt(rs.getString("ACTION_GROUP_TXT"));
////                sectionActionInstance.setArgumentSourceScreenSectionInstanceBigTxt(rs.getString("ACTION_ARG_SRC_SCRN_SECTN_INSTANCE_BIG_TXT"));
////                sectionActionInstance.setDirectQuery(rs.getBoolean("IS_DIRECT_QUERY"));
//                sectionActionInstance.setScreenActionCodeTxt(rs.getString("SCREEN_SECTION_ACTION_MASTER_CODE_PK_ID"));
//                sectionActionInstance.setScreenActionMasterCodeFkId(rs.getString("ACTION_MASTER_CODE_FK_ID"));
////                sectionActionInstance.setScreenActionNameG11nBigTxt(rs.getString("SCREEN_ACTION_NAME_G11N_BIG_TXT"));
//                sectionActionInstance.setTargetScreenInstanceFkId(Util.convertByteToString(rs.getBytes("TARGET_SCREEN_INSTANCE_FK_ID")));
//                sectionActionInstance.setTargetScreenSectionInstanceFkId(Util.convertByteToString(rs.getBytes("TARGET_SCREEN_SECTION_INSTANCE_FK_ID")));
//                sectionActionInstance.setTargetTabSequenceNumber(rs.getInt("TARGET_TAB_SEQ_NUM_POS_INT"));
////                sectionActionInstance.setWizardLaunch(rs.getBoolean("IS_WIZARD_LAUNCH"));
//                sectionActionInstance.setActionInstanceSeqNumber(rs.getInt("ACTION_INSTANCE_SEQ_NUM_POS_INT"));
//                sectionActionInstance.setCnfMsgTxt(getG11nValue(rs.getString("CNFM_MSG_G11N_BIG_TXT")));
////                sectionActionInstance.setCnfMsgScrnPatternCodeFkId(rs.getString("CNFM_MSG_SCRN_PTRN_CODE_FK_ID"));
//                sectionActionInstance.setSuccessMessage(getG11nValue(rs.getString("SUCCESS_MSG_G11N_BIG_TXT")));
//                sectionActionInstance.setFailureMessage(getG11nValue(rs.getString("FAILURE_MSG_G11N_BIG_TXT")));
//                sectionActionInstance.setOverrideActionLabelG11nBigTxt(getG11nValue(rs.getString("OVERRIDE_ACTION_LABEL_G11N_BIG_TXT")));
//                sectionActionInstance.setOverrideActionIconCode(Util.getIcon(rs.getString("OVERRIDE_ACTION_ICON_CODE_FK_ID")));
//                sectionActionInstance.setScreenSectionActionIconCode(Util.getIcon(rs.getString("SCREEN_SECTION_ACTION_ICON_CODE_FK_ID")));
//                sectionActionInstance.setThemeSeqNumPosInt(rs.getInt("THEME_SEQ_NUM_POS_INT"));
//                sectionActionInstance.setScreenSectionMasterCodeFkId(rs.getString("SCREEN_SECTION_MASTER_CODE_FK_ID"));
//                sectionActionInstance.setDefaultIconCode(rs.getString("SCREEN_SECTION_ACTION_ICON_CODE_FK_ID"));
//                sectionActionInstance.setDefaultActionLabelG11nBigTxt(getG11nValue(rs.getString("SCREEN_SECTION_ACTION_NAME_G11N_BIG_TXT")));
//                if(sectionActionInstance.getOverrideActionIconCode()==null){
//                    sectionActionInstance.setOverrideActionIconCode(Util.getIcon(sectionActionInstance.getDefaultIconCode()));
//                }
//                if(sectionActionInstance.getOverrideActionLabelG11nBigTxt()==null){
//                    sectionActionInstance.setOverrideActionLabelG11nBigTxt(sectionActionInstance.getDefaultActionLabelG11nBigTxt());
//                }
//                sectionActionInstanceMap.put(sectionActionInstance.getSectionActionInstancePkId(), sectionActionInstance);
//                screenSectionInstanceMap.get(sectionActionInstance.getScreenSectionInstanceFkId()).getActions().add(sectionActionInstance);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        Map<String, List<LoadParameters>> loadParamsMap = getLoadParameters("sectionActionInstances",sectionActionInstanceMap.keySet(),group);
//        for(Map.Entry<String, List<LoadParameters>> entry : loadParamsMap.entrySet()){
//            String fkId = entry.getKey();
//            sectionActionInstanceMap.get(fkId).setLoadParams(entry.getValue());
//        }
//    }
//    private void getSectionControl(Map<String, ScreenSectionInstanceVO> screenSectionInstanceMap, String deviceType, PersonGroup group){
//        List<SectionControlVO> sectionControls = new ArrayList<>();
//            StringBuilder sql = new StringBuilder("SELECT\n" +
//                    "  *,\n" +
//                    "  AGG_ASSOCIATED_CONTROL_INSTANCE_FK_ID,\n" +
//                    "  AGG_CONTROL_RELTV_DISPLAY_ALIGNMENT_CODE_FK_ID,\n" +
////                    "  AGG_FUNCTION_EXPN,\n" +
//                    "  IS_CONTROL_TO_BE_CONCATENATED,\n" +
//                    "  CONTROL_ATTRIBUTE_PATH_EXPN,\n" +
//                    "  CONTROL_INSTANCE_PK_ID,\n" +
//                    "  CONTROL_TYPE_CODE_FK_ID,\n" +
//                    "  IS_DISPLAY_IN_FOREGROUND,\n" +
//                    "  IS_GROUP_BY_CONTROL,\n" +
//                    "  ORDER_BY_VALUES_BIG_TXT,\n" +
//                    "  PROCESS_ELEMENT_CODE_FK_ID,\n" +
////                    "  PROPERTY_CONTROL_EXPN,\n" +
//                    "  IS_PROPERTY_CONTROL,\n" +
////                    "  RCHC_SCREEN_MASTER_CODE_FK_ID,\n" +
////                    "  RCHC_SOURCE_ARGUMENT_BIG_TXT,\n" +
//                    "  SEQ_NUM_POS_INT,\n" +
//                    "  STATIC_TEXT_G11N_BIG_TXT\n" +
//                    "FROM T_UI_ADM_CONTROL_INSTANCE  LEFT JOIN T_UI_TAP_PROPERTY_CONTROL_MASTER ON CONTROL_ATTRIBUTE_PATH_EXPN = PROPERTY_CONTROL_MASTER_CODE_PK_ID AND IS_PROPERTY_CONTROL = 1 \n" +
//                    "WHERE " +
//                    getApplicability(deviceType) +
//                    " AND SCREEN_SECTION_INSTANCE_FK_ID IN ( ");
//            for (int i = 0; i < screenSectionInstanceMap.keySet().size(); i++) {
//                if (i != 0)
//                    sql.append(",");
//                sql.append("x?");
//            }
//            if (screenSectionInstanceMap.keySet().size() == 0) {
//                sql.append("null");
//            }
//            sql.append(")");
//            sql.append("AND\n" +
//            "      T_UI_ADM_CONTROL_INSTANCE.CONTROL_INSTANCE_PK_ID NOT IN (SELECT WHAT_MT_PE_PRIMARY_KEY_ID\n" +
//            "                                                               FROM T_UI_ADM_ACCESS_MANAGER\n" +
//            "                                                               WHERE IS_NOT_ACCESSIBLE AND\n" +
//            "                                                                     WHO_GROUP_FK_ID IN (");
//            for (int i = 0; i < group.getGroupIds().size(); i++) {
//                if (i != 0)
//                    sql.append(",");
//                sql.append("x?");
//            }
//            if (group.getGroupIds().size() == 0) {
//                sql.append("null");
//            }
//            sql.append("));");
//            List<Object> params = new ArrayList<>();
//            for (String screenSectionInstancePkId : screenSectionInstanceMap.keySet()) {
//                params.add(Util.remove0x(screenSectionInstancePkId));
//            }
//            for (String groupId : group.getGroupIds()) {
//                params.add(Util.remove0x(groupId));
//            }
//            ResultSet rs = executeSQL(sql.toString(), params);
//            sectionControls = getSectionControls(rs);
//            Map<String, SectionControlVO> sectionControlMap = new HashMap<>();
//            for (SectionControlVO control : sectionControls) {
//                screenSectionInstanceMap.get(control.getScreenSectionInstanceFkId()).getControls().add(control);
//                sectionControlMap.put(control.getControlDefinitionPkId(), control);
//            }
//            getControlAction(sectionControlMap, deviceType, group);
//        }
//
//    private List<SectionControlVO> getSectionControls(ResultSet rs) {
//        List<SectionControlVO> sectionControls = new ArrayList<>();
//        try {
//            while (rs.next()){
//                SectionControlVO sectionControl = new SectionControlVO();
//                sectionControl.setAggAssociatedControlFkId(Util.convertByteToString(rs.getBytes("AGG_ASSOCIATED_CONTROL_INSTANCE_FK_ID")));
//                sectionControl.setAggControlReltvDisplayAlignmentTxt(rs.getString("AGG_CONTROL_RELTV_DISPLAY_ALIGNMENT_CODE_FK_ID"));
////                sectionControl.setAggFunctionExpn(rs.getString("AGG_FUNCTION_EXPN"));
//                sectionControl.setConcatenate(rs.getBoolean("IS_CONTROL_TO_BE_CONCATENATED"));
//                sectionControl.setControlAttributePathExpn(rs.getString("CONTROL_ATTRIBUTE_PATH_EXPN"));
//                sectionControl.setControlDefinitionPkId(Util.convertByteToString(rs.getBytes("CONTROL_INSTANCE_PK_ID")));
//                sectionControl.setControlTypeTxt(rs.getString("CONTROL_TYPE_CODE_FK_ID"));
//                sectionControl.setForeground(rs.getBoolean("IS_DISPLAY_IN_FOREGROUND"));
//                sectionControl.setGroupByControl(rs.getBoolean("IS_GROUP_BY_CONTROL"));
//                sectionControl.setOrderByValuesTxt(rs.getString("ORDER_BY_VALUES_BIG_TXT"));
//                sectionControl.setProcessElementFkId(rs.getString("PROCESS_ELEMENT_CODE_FK_ID"));
////                sectionControl.setPropertyControlExpn(rs.getString("PROPERTY_CONTROL_EXPN"));
//                sectionControl.setPropertyControl(rs.getBoolean("IS_PROPERTY_CONTROL"));
//                sectionControl.setStaticControl(rs.getBoolean("IS_STATIC_CONTROL"));
//                sectionControl.setAppSpecificPC(rs.getBoolean("IS_APP_SPECIFIC"));
//                sectionControl.setMTPESpecificPC(rs.getBoolean("IS_MT_PE_SPECIFIC"));
//                sectionControl.setAttributeSpecificPC(rs.getBoolean("IS_ATTRIBUTE_SPECIFIC"));
//                sectionControl.setAggregateControl(rs.getBoolean("IS_AGGREGATE_CONTROL"));
//                sectionControl.setStaticIconCode(Util.getIcon(rs.getString("STATIC_ICON_CODE_FK_ID")));
////                sectionControl.setRchcSectionPatternTxt(rs.getString("RCHC_SCREEN_MASTER_CODE_FK_ID"));
////                sectionControl.setRchcSourceArgumentTxt(rs.getString("RCHC_SOURCE_ARGUMENT_BIG_TXT"));
////                sectionControl.setSequenceNumber(rs.getInt("SEQ_NUM_POS_INT"));
////                sectionControl.setParentSeqNumber(rs.getInt("PARENT_SEQ_NUM_POS_INT"));
//                sectionControl.setStaticTextG11NBigTxt(getG11nValue(rs.getString("STATIC_TEXT_G11N_BIG_TXT")));
//                sectionControl.setHeight(rs.getString("CONTROL_HEIGHT_TXT"));
//                sectionControl.setWidth(rs.getString("CONTROL_WIDTH_TXT"));
//                sectionControl.setTopMargin(rs.getString("CONTROL_TOP_MARGIN_TXT"));
//                sectionControl.setLeftMargin(rs.getString("CONTROL_LEFT_MARGIN_TXT"));
//                sectionControl.setTopPadding(rs.getString("CONTROL_TOP_PADDING_TXT"));
//                sectionControl.setBottomPadding(rs.getString("CONTROL_BOTTOM_PADDING_TXT"));
//                sectionControl.setLeftPadding(rs.getString("CONTROL_LEFT_PADDING_TXT"));
//                sectionControl.setRightPadding(rs.getString("CONTROL_RIGHT_PADDING_TXT"));
//                sectionControl.setControlTextJustificationCodeFkId(rs.getString("CONTROL_TEXT_JUSTIFICATION_CODE_FK_ID"));
//                sectionControl.setControlThemeSizeCodeFkId(rs.getString("CONTROL_THEME_SIZE_CODE_FK_ID"));
//                sectionControl.setControlFontStyleCodeFkId(rs.getString("CONTROL_FONT_STYLE_CODE_FK_ID"));
//                sectionControl.setControlFontTextOverflowCodeFkId(rs.getString("CONTROL_FONT_TEXT_OVERFLOW_CODE_FK_ID"));
//                sectionControl.setControlFontCaseCodeFkId(rs.getString("CONTROL_FONT_CASE_CODE_FK_ID"));
//                sectionControl.setControlFontColourTxt(rs.getString("CONTROL_FONT_COLOUR_TXT"));
//                sectionControl.setControlBackgroundColourTxt(rs.getString("CONTROL_BACKGROUND_COLOUR_TXT"));
//                sectionControl.setScreenSectionInstanceFkId(Util.convertByteToString(rs.getBytes("SCREEN_SECTION_INSTANCE_FK_ID")));
//                sectionControl.setGroupByControlSeqNumPosInt(rs.getInt("GROUP_BY_CONTROL_SEQ_NUM_POS_INT"));
//                sectionControl.setGroupByHavingExpn(rs.getString("GROUP_BY_HAVING_EXPN"));
//                sectionControl.setControlDispInStickyHdr(rs.getBoolean("IS_CONTROL_DISP_IN_STICKY_HDR"));
//                sectionControl.setApplicableToMobile(rs.getBoolean("IS_APPLICABLE_TO_MOBILE"));
//                sectionControl.setApplicableToPhablet(rs.getBoolean("IS_APPLICABLE_TO_PHABLET"));
//                sectionControl.setApplicableToMiniTablet(rs.getBoolean("IS_APPLICABLE_TO_MINI_TABLET"));
//                sectionControl.setApplicableToTablet(rs.getBoolean("IS_APPLICABLE_TO_TABLET"));
//                sectionControl.setApplicableToWeb(rs.getBoolean("IS_APPLICABLE_TO_WEB"));
//                sectionControl.setFkRelationshipWithParentLngTxt(rs.getString("FK_RELATIONSHIP_WITH_PARENT_LNG_TXT"));
//                sectionControl.setRelCtrlImgDispAttrPathExpn(rs.getString("REL_CTRL_IMG_DISP_ATTR_PATH_EXPN"));
//                sectionControl.setRelCtrlFstDispAttrPathExpn(rs.getString("REL_CTRL_FST_LN_DISP_ATTR_PATH_EXPN"));
//                sectionControl.setRelCtrlSndDispAttrPathExpn(rs.getString("REL_CTRL_SND_LN_DISP_ATTR_PATH_EXPN"));
//                sectionControl.setRelCtrlRtrnDispAttrPathExpn(rs.getString("REL_CTRL_RTRN_DISP_ATTR_PATH_EXPN"));
//                sectionControl.setPropertyControlMasterCodePkId(rs.getString("PROPERTY_CONTROL_MASTER_CODE_PK_ID"));
//                sectionControl.setPropertyControlMasterIconCode(Util.getIcon(rs.getString("PROPERTY_CONTROL_MASTER_ICON_CODE_FK_ID")));
//                sectionControl.setItemStatusValueTxt(rs.getString("ITEM_STATUS_VALUE_TXT"));
//                sectionControl.setAttributeFilterExpn(rs.getString("ATTRIBUTE_FILTER_EXPN"));
//                sectionControl.setAddlAttrPathExpn(rs.getString("ADDL_ATTR_PATH_EXPN"));
//                sectionControl.setAttachCompress(rs.getBoolean("IS_ATTACH_COMPRESS"));
//                sectionControl.setStatusColourAttrPathExpn(rs.getString("STATUS_COLOUR_ATTR_PATH_EXPN"));
//                sectionControl.setSeqNumPosInt(rs.getInt("SEQ_NUM_POS_INT"));
//                sectionControls.add(sectionControl);
//            }
//        } catch (SQLException e1) {
//            e1.printStackTrace();
//        }
//        return sectionControls;
//    }
//
//    private void getControlAction(Map<String, SectionControlVO> controlInstanceMap, String deviceType, PersonGroup group){
//        Map<String, ControlActionInstance> controlActionInstanceMap = new HashMap<>();
//        StringBuilder sql = new StringBuilder(
//                "SELECT\n" +
//                        "  *,\n" +
//                        "  TARGET_SCREEN_INSTANCE_FK_ID,\n" +
//                        "  TARGET_SCREEN_SECTION_INSTANCE_FK_ID,\n" +
//                        "  TARGET_TAB_SEQ_NUM_POS_INT,\n" +
//                        "  ACTION_ARGUMENT_BIG_TXT,\n" +
////                        "  ACTION_ARG_SRC_SCRN_SECTN_INSTANCE_BIG_TXT,\n" +
////                        "  IS_DIRECT_QUERY,\n" +
//                        "  SCREEN_SECTION_ACTION_MASTER_CODE_FK_ID,\n" +
//                        "  SCREEN_SECTION_ACTION_NAME_G11N_BIG_TXT,\n" +
//                        "  ACTION_GROUP_TXT\n" +
//                        "FROM T_UI_ADM_CONTROL_ACTION_INSTANCE\n" +
//                        "  INNER JOIN T_UI_TAP_SCREEN_SECTION_ACTION_MASTER\n" +
//                        "    ON SCREEN_SECTION_ACTION_MASTER_CODE_FK_ID = SCREEN_SECTION_ACTION_MASTER_CODE_PK_ID\n" +
//                        "WHERE " +
//                        getApplicability(deviceType) +
//                        " AND CONTROL_INSTANCE_FK_ID IN(");
//        for(int i=0; i<controlInstanceMap.keySet().size(); i++){
//            if(i!=0)
//                sql.append(",");
//            sql.append("x?");
//        }
//        if(controlInstanceMap.keySet().size()==0) {
//            sql.append("null");
//        }
//        sql.append(")");
//        sql.append(" AND PFM_GROUP_FK_ID IN (");
//        for(int i=0; i<group.getGroupIds().size(); i++){
//            if(i!=0)
//                sql.append(",");
//            sql.append("x?");
//        }
//        if(group.getGroupIds().size()==0) {
//            sql.append("null");
//        }
//        sql.append(")");
//        sql.append("    AND  T_UI_ADM_CONTROL_ACTION_INSTANCE.CONTROL_ACTION_INSTANCE_PK_ID NOT IN (SELECT WHAT_MT_PE_PRIMARY_KEY_ID\n" +
//        "                                                                             FROM T_UI_ADM_ACCESS_MANAGER\n" +
//        "                                                                             WHERE IS_NOT_ACCESSIBLE AND\n" +
//        "                                                                                   WHO_GROUP_FK_ID IN (" );
//        for(int i=0; i<group.getGroupIds().size(); i++){
//            if(i!=0)
//                sql.append(",");
//            sql.append("x?");
//        }
//        if(group.getGroupIds().size()==0){
//            sql.append("null");
//        }
//        sql.append("));");
//        List<Object> params = new ArrayList<>();
//        for(String controlInstanceId : controlInstanceMap.keySet()) {
//            params.add(Util.remove0x(controlInstanceId));
//        }
//        for(String groupId : group.getGroupIds()){
//            params.add(Util.remove0x(groupId));
//        }
//        for(String groupId : group.getGroupIds()){
//            params.add(Util.remove0x(groupId));
//        }
//        ResultSet rs = executeSQL(sql.toString(), params);
//        try {
//            while (rs.next()){
//                ControlActionInstance controlActionInstance = new ControlActionInstance();
//                controlActionInstance.setControlActionInstancePkId(Util.convertByteToString(rs.getBytes("CONTROL_ACTION_INSTANCE_PK_ID")));
//                controlActionInstance.setActionArgumentTxt(rs.getString("ACTION_ARGUMENT_BIG_TXT"));
//                controlActionInstance.setActionGroupTxt(rs.getString("ACTION_GROUP_TXT"));
////                controlActionInstance.setArgumentSourceScreenSectionInstanceBigTxt(rs.getString("ACTION_ARG_SRC_SCRN_SECTN_INSTANCE_BIG_TXT"));
////                controlActionInstance.setDirectQuery(rs.getBoolean("IS_DIRECT_QUERY"));
//                controlActionInstance.setScreenActionCodeTxt(rs.getString("SCREEN_SECTION_ACTION_MASTER_CODE_FK_ID"));
//                controlActionInstance.setScreenActionNameG11nBigTxt(getG11nValue(rs.getString("SCREEN_SECTION_ACTION_NAME_G11N_BIG_TXT")));
//                controlActionInstance.setTargetScreenInstanceFkId(Util.convertByteToString(rs.getBytes("TARGET_SCREEN_INSTANCE_FK_ID")));
//                controlActionInstance.setTargetScreenSectionInstanceFkId(Util.convertByteToString(rs.getBytes("TARGET_SCREEN_SECTION_INSTANCE_FK_ID")));
//                controlActionInstance.setTargetTabSequenceNumber(rs.getInt("TARGET_TAB_SEQ_NUM_POS_INT"));
////                controlActionInstance.setWizardLaunch(rs.getBoolean("IS_WIZARD_LAUNCH"));
//                controlActionInstance.setActionInstanceSeqNumber(rs.getInt("ACTION_INSTANCE_SEQ_NUM_POS_INT"));
//                controlActionInstance.setCnfMsgTxt(getG11nValue(rs.getString("CNFM_MSG_G11N_BIG_TXT")));
////                controlActionInstance.setCnfMsgScrnPatternCodeFkId(rs.getString("CNFM_MSG_SCRN_PTRN_CODE_FK_ID"));
//                controlActionInstance.setSuccessMessage(getG11nValue(rs.getString("SUCCESS_MSG_G11N_BIG_TXT")));
//                controlActionInstance.setFailureMessage(getG11nValue(rs.getString("FAILURE_MSG_G11N_BIG_TXT")));
//                controlActionInstance.setControlnstanceFkId(Util.convertByteToString(rs.getBytes("CONTROL_INSTANCE_FK_ID")));
//                controlActionInstance.setScreenSectionMasterCodeFkId(rs.getString("SCREEN_SECTION_MASTER_CODE_FK_ID"));
//                controlActionInstance.setThemeSeqNumPosInt(rs.getInt("THEME_SEQ_NUM_POS_INT"));
//                controlActionInstance.setScreenSectionActionIconCode(Util.getIcon(rs.getString("SCREEN_SECTION_ACTION_ICON_CODE_FK_ID")));
//                controlActionInstanceMap.put(controlActionInstance.getControlActionInstancePkId(), controlActionInstance);
//                controlInstanceMap.get(controlActionInstance.getControlnstanceFkId()).getControlActionInstances().add(controlActionInstance);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        Map<String, List<LoadParameters>> loadParamsMap = getLoadParameters("controlActionInstance",controlActionInstanceMap.keySet(),group);
//        for(Map.Entry<String, List<LoadParameters>> entry : loadParamsMap.entrySet()){
//            String fkId = entry.getKey();
//            controlActionInstanceMap.get(fkId).setLoadParams(entry.getValue());
//        }
//    }

    public String getApplicability(String deviceType) {
        String string = "";
        if (deviceType.equalsIgnoreCase("MOBILE")) {
            string = "IS_APPLICABLE_TO_MOBILE = 1";
        }
        if (deviceType.equalsIgnoreCase("PHABLET")) {
            string = "IS_APPLICABLE_TO_PHABLET = 1";
        }
        if (deviceType.equalsIgnoreCase("MINI_TABLET")) {
            string = "IS_APPLICABLE_TO_MINI_TABLET = 1";
        }
        if (deviceType.equalsIgnoreCase("TABLET")) {
            string = "IS_APPLICABLE_TO_TABLET = 1";
        }
        if (deviceType.equalsIgnoreCase("WEB")) {
            string = "IS_APPLICABLE_TO_WEB = 1";
        }
        return string;
    }

    @Override
    public Map<String, Map<String, ResolvedColumnCondition>> getColumnConditions(String personId, Set<String> amRowPkIds) {
        Map<String, Map<String, ResolvedColumnCondition>> rowACFKToDoaToColumnPermsMap = new HashMap<>();
        if(amRowPkIds.size()==0)
            return null;
        StringBuilder SQL = new StringBuilder("SELECT * FROM T_PFM_IEU_AM_COL_RESOLVED WHERE IS_DELETED = false AND AM_ROW_RESOLVED_FK_ID IN (");
        for(int i=0; i<amRowPkIds.size(); i++){
            if(i!=0)
                SQL.append(",");
            SQL.append("x?");
        }
        SQL.append(")");
        List<Object> params = new ArrayList<>();
        for(String rowPkId : amRowPkIds){
            params.add(Util.remove0x(rowPkId));
        }
        ResultSet rs = executeSQL(SQL.toString(), params);
        try {
            while (rs.next()){
                ResolvedColumnCondition columnCondition = new ResolvedColumnCondition();
                columnCondition.setPk(Util.convertByteToString(rs.getBytes("AM_COL_RESOLVED_PK_ID")));
                columnCondition.setFk(Util.convertByteToString(rs.getBytes("AM_ROW_RESOLVED_FK_ID")));
                columnCondition.setDoa(rs.getString("DOA_CODE_FK_ID"));
                columnCondition.setCRp(rs.getBoolean("IS_READ_CURRENT_PERMITTED"));
                columnCondition.setCUp(rs.getBoolean("IS_UPDATE_CURRENT_PERMITTED"));
                columnCondition.setHRp(rs.getBoolean("IS_READ_HISTORY_PERMITTED"));
                columnCondition.setHUp(rs.getBoolean("IS_UPDATE_HISTORY_PERMITTED"));
                columnCondition.setFRp(rs.getBoolean("IS_READ_FUTURE_PERMITTED"));
                columnCondition.setFUp(rs.getBoolean("IS_UPDATE_FUTURE_PERMITTED"));
                if(rowACFKToDoaToColumnPermsMap.containsKey(columnCondition.getFk())){
                    rowACFKToDoaToColumnPermsMap.get(columnCondition.getFk()).put(columnCondition.getDoa(),columnCondition);
                }else{
                    Map<String, ResolvedColumnCondition> map = new HashMap<>();
                    map.put(columnCondition.getDoa(),columnCondition);
                    rowACFKToDoaToColumnPermsMap.put(columnCondition.getFk(), map);
                }
                // TODO set here the column conditions.
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowACFKToDoaToColumnPermsMap;
    }

    @Override
    public String getScreenId(String screenMasterCode) {
        String result = null;
        String sql = "SELECT SCREEN_INSTANCE_PK_ID FROM T_UI_ADM_SCREEN_INSTANCE WHERE SCREEN_MASTER_CODE_FK_ID = ? LIMIT 1;";
        List<Object> params = new ArrayList<>();
        params.add(screenMasterCode);
        ResultSet rs = executeSQL(sql, params);
        try {
            while (rs.next()){
                result = Util.convertByteToString(rs.getBytes(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getG11nValue(String input){
        return Util.getG11nValue(input, null);
    }
}
