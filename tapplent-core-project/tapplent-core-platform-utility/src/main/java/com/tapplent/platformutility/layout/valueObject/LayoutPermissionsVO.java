package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 14/11/17.
 */
public class LayoutPermissionsVO {
    private String pkId;
    private String fkId;
    private String permissionType;
    private String permissionValue;
    private String subjectUserQualifier;
    private String negationPermissionType;
    private String negationPermissionValue;
    private String negationSubjectUserQualifier;
    private String recordStatePermission;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getFkId() {
        return fkId;
    }

    public void setFkId(String fkId) {
        this.fkId = fkId;
    }

    public String getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }

    public String getSubjectUserQualifier() {
        return subjectUserQualifier;
    }

    public void setSubjectUserQualifier(String subjectUserQualifier) {
        this.subjectUserQualifier = subjectUserQualifier;
    }

    public String getNegationPermissionType() {
        return negationPermissionType;
    }

    public void setNegationPermissionType(String negationPermissionType) {
        this.negationPermissionType = negationPermissionType;
    }

    public String getNegationPermissionValue() {
        return negationPermissionValue;
    }

    public void setNegationPermissionValue(String negationPermissionValue) {
        this.negationPermissionValue = negationPermissionValue;
    }

    public String getNegationSubjectUserQualifier() {
        return negationSubjectUserQualifier;
    }

    public void setNegationSubjectUserQualifier(String negationSubjectUserQualifier) {
        this.negationSubjectUserQualifier = negationSubjectUserQualifier;
    }

    public String getRecordStatePermission() {
        return recordStatePermission;
    }

    public void setRecordStatePermission(String recordStatePermission) {
        this.recordStatePermission = recordStatePermission;
    }
}
