package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 19/09/17.
 */
public class VisualArtefactLibraryVO {

    private String vizArtefactNameG11nBigTxt;
    private String artefactImageid;
    private String saveStateCodeFkId;
    private String activeStateCodeFkId;
    private String recordStateCodeFkId;
    private boolean isDeleted;
    private String imageUrl;

    public String getVizArtefactLibTypeFkId() {
        return vizArtefactLibTypeFkId;
    }

    public void setVizArtefactLibTypeFkId(String vizArtefactLibTypeFkId) {
        this.vizArtefactLibTypeFkId = vizArtefactLibTypeFkId;
    }

    private String vizArtefactLibTypeFkId;
    public String getVizArtefactNameG11nBigTxt() {
        return vizArtefactNameG11nBigTxt;
    }

    public void setVizArtefactNameG11nBigTxt(String vizArtefactNameG11nBigTxt) {
        this.vizArtefactNameG11nBigTxt = vizArtefactNameG11nBigTxt;
    }

    public String getArtefactImageid() {
        return artefactImageid;
    }

    public void setArtefactImageid(String artefactImageid) {
        this.artefactImageid = artefactImageid;
    }

    public String getSaveStateCodeFkId() {
        return saveStateCodeFkId;
    }

    public void setSaveStateCodeFkId(String saveStateCodeFkId) {
        this.saveStateCodeFkId = saveStateCodeFkId;
    }

    public String getActiveStateCodeFkId() {
        return activeStateCodeFkId;
    }

    public void setActiveStateCodeFkId(String activeStateCodeFkId) {
        this.activeStateCodeFkId = activeStateCodeFkId;
    }

    public String getRecordStateCodeFkId() {
        return recordStateCodeFkId;
    }

    public void setRecordStateCodeFkId(String recordStateCodeFkId) {
        this.recordStateCodeFkId = recordStateCodeFkId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
    public String getImageUrl() {return imageUrl;}
    public void setImageUrl(String imageUrl) {this.imageUrl = imageUrl;}

}
