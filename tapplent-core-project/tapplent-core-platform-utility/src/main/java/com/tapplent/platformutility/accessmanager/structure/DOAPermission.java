package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by Tapplent on 2/9/17.
 */
public class DOAPermission {
    private Boolean isCreateCurrentPermitted = false;
    private Boolean isCreateHistoryPermitted = false;
    private Boolean isCreateFuturePermitted = false;
    private Boolean isReadCurrentPermitted = false;
    private Boolean isReadHistoryPermitted = false;
    private Boolean isReadFuturePermitted = false;
    private Boolean isUpdateCurrentPermitted = false;
    private Boolean isUpdateHistoryPermitted = false;
    private Boolean isUpdateFuturePermitted = false;

    public Boolean getCreateCurrentPermitted() {
        return isCreateCurrentPermitted;
    }

    public void setCreateCurrentPermitted(Boolean createCurrentPermitted) {
        isCreateCurrentPermitted = createCurrentPermitted;
    }

    public Boolean getCreateHistoryPermitted() {
        return isCreateHistoryPermitted;
    }

    public void setCreateHistoryPermitted(Boolean createHistoryPermitted) {
        isCreateHistoryPermitted = createHistoryPermitted;
    }

    public Boolean getCreateFuturePermitted() {
        return isCreateFuturePermitted;
    }

    public void setCreateFuturePermitted(Boolean createFuturePermitted) {
        isCreateFuturePermitted = createFuturePermitted;
    }

    public Boolean getReadCurrentPermitted() {
        return isReadCurrentPermitted;
    }

    public void setReadCurrentPermitted(Boolean readCurrentPermitted) {
        isReadCurrentPermitted = readCurrentPermitted;
    }

    public Boolean getReadHistoryPermitted() {
        return isReadHistoryPermitted;
    }

    public void setReadHistoryPermitted(Boolean readHistoryPermitted) {
        isReadHistoryPermitted = readHistoryPermitted;
    }

    public Boolean getReadFuturePermitted() {
        return isReadFuturePermitted;
    }

    public void setReadFuturePermitted(Boolean readFuturePermitted) {
        isReadFuturePermitted = readFuturePermitted;
    }

    public Boolean getUpdateCurrentPermitted() {
        return isUpdateCurrentPermitted;
    }

    public void setUpdateCurrentPermitted(Boolean updateCurrentPermitted) {
        isUpdateCurrentPermitted = updateCurrentPermitted;
    }

    public Boolean getUpdateHistoryPermitted() {
        return isUpdateHistoryPermitted;
    }

    public void setUpdateHistoryPermitted(Boolean updateHistoryPermitted) {
        isUpdateHistoryPermitted = updateHistoryPermitted;
    }

    public Boolean getUpdateFuturePermitted() {
        return isUpdateFuturePermitted;
    }

    public void setUpdateFuturePermitted(Boolean updateFuturePermitted) {
        isUpdateFuturePermitted = updateFuturePermitted;
    }
}
