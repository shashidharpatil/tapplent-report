package com.tapplent.platformutility.common.util.valueObject;

/**
 * Created by Tapplent on 7/14/17.
 */
public class G11n {
    String VL;
    String IM;
    String TSL;

    public String getVL() {
        return VL;
    }

    public void setVL(String VL) {
        this.VL = VL;
    }

    public String getIM() {
        return IM;
    }

    public void setIM(String IM) {
        this.IM = IM;
    }

    public String getTSL() {
        return TSL;
    }

    public void setTSL(String TSL) {
        this.TSL = TSL;
    }
}
