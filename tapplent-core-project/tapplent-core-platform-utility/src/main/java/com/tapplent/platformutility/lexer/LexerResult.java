/**
 * 
 */
package com.tapplent.platformutility.lexer;


import java.util.ArrayList;
import java.util.List;

/**
 * @author Shubham Patodi
 * @since 04/07/2016 
 *
 */
public class LexerResult {
	private List<Object> tokens = new ArrayList();
	private boolean isDistinct;

	public boolean isDistinct() {
		return isDistinct;
	}

	public void setDistinct(boolean distinct) {
		isDistinct = distinct;
	}

	public List<Object> getTokens() {
		return tokens;
	}
	public void setTokens(List<Object> tokens) {
		this.tokens = tokens;
	}
}
