package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class PageEquivalenceVO {
	String pageEquivalenceTransactionId;
	String baseTemplate;
	String mtPE;
	String viewType;
	JsonNode deviceIndependentBlob;
	JsonNode deviceDependentBlob;
	public String getPageEquivalenceTransactionId() {
		return pageEquivalenceTransactionId;
	}
	public void setPageEquivalenceTransactionId(String pageEquivalenceTransactionId) {
		this.pageEquivalenceTransactionId = pageEquivalenceTransactionId;
	}
	public String getBaseTemplate() {
		return baseTemplate;
	}
	public void setBaseTemplate(String baseTemplate) {
		this.baseTemplate = baseTemplate;
	}
	public String getMtPE() {
		return mtPE;
	}
	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}
	public String getViewType() {
		return viewType;
	}
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
}
