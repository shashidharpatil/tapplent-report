package com.tapplent.platformutility.workflow.structure;

import java.util.Map;

/**
 * Created by Tapplent on 9/20/16.
 */
public class Workflow {
    private String workflowTxnPkId;
    private String workflowDefinitionFkId;
    private String doRowPrimaryKeyTxt;
    private String doRowVersionFkId;
    private String workflowStatusCodeFkId;
    private String srcMtPeCodeFkId;
    private String btCodeFkId;
    private String mtPE;
    private String businessRule;
    private String displayContextValue;
    private String doIconCodeFkId;
    private String doNameG11nBigText;
    private boolean isPropogateFlag;
    private Map<Integer, WorkflowStep> workflowStepMap;

    public String getWorkflowTxnPkId() {
        return workflowTxnPkId;
    }

    public void setWorkflowTxnPkId(String workflowTxnPkId) {
        this.workflowTxnPkId = workflowTxnPkId;
    }

    public String getWorkflowDefinitionFkId() {
        return workflowDefinitionFkId;
    }

    public void setWorkflowDefinitionFkId(String workflowDefinitionFkId) {
        this.workflowDefinitionFkId = workflowDefinitionFkId;
    }

    public String getDoRowPrimaryKeyTxt() {
        return doRowPrimaryKeyTxt;
    }

    public void setDoRowPrimaryKeyTxt(String doRowPrimaryKeyTxt) {
        this.doRowPrimaryKeyTxt = doRowPrimaryKeyTxt;
    }

    public String getDoRowVersionFkId() {
        return doRowVersionFkId;
    }

    public void setDoRowVersionFkId(String doRowVersionFkId) {
        this.doRowVersionFkId = doRowVersionFkId;
    }

    public String getWorkflowStatusCodeFkId() {
        return workflowStatusCodeFkId;
    }

    public void setWorkflowStatusCodeFkId(String workflowStatusCodeFkId) {
        this.workflowStatusCodeFkId = workflowStatusCodeFkId;
    }

    public String getSrcMtPeCodeFkId() {
        return srcMtPeCodeFkId;
    }

    public void setSrcMtPeCodeFkId(String srcMtPeCodeFkId) {
        this.srcMtPeCodeFkId = srcMtPeCodeFkId;
    }

    public String getBtCodeFkId() {
        return btCodeFkId;
    }

    public void setBtCodeFkId(String btCodeFkId) {
        this.btCodeFkId = btCodeFkId;
    }

    public String getDisplayContextValue() {
        return displayContextValue;
    }

    public void setDisplayContextValue(String displayContextValue) {
        this.displayContextValue = displayContextValue;
    }

    public String getDoIconCodeFkId() {
        return doIconCodeFkId;
    }

    public void setDoIconCodeFkId(String doIconCodeFkId) {
        this.doIconCodeFkId = doIconCodeFkId;
    }

    public String getDoNameG11nBigText() {
        return doNameG11nBigText;
    }

    public void setDoNameG11nBigText(String doNameG11nBigText) {
        this.doNameG11nBigText = doNameG11nBigText;
    }

    public boolean isPropogateFlag() {
        return isPropogateFlag;
    }

    public void setPropogateFlag(boolean propogateFlag) {
        isPropogateFlag = propogateFlag;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getBusinessRule() {
        return businessRule;
    }

    public void setBusinessRule(String businessRule) {
        this.businessRule = businessRule;
    }

    public Map<Integer, WorkflowStep> getWorkflowStepMap() {
        return workflowStepMap;
    }

    public void setWorkflowStepMap(Map<Integer, WorkflowStep> workflowStepMap) {
        this.workflowStepMap = workflowStepMap;
    }
}
