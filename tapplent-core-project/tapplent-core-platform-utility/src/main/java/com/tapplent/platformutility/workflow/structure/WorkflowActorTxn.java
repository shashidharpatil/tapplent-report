package com.tapplent.platformutility.workflow.structure;

import java.sql.Timestamp;

public class WorkflowActorTxn {
	
	private String workflowTxnStepActorPkId;
	private String workflowTxnStepFkId;
	private String stepActorDefnFkId;
	private String resolvedPersonFkId;
	private String stepActorStatusCodeFkId;
	private boolean isDeleted;
	private Timestamp createdDatetime;
	private Timestamp lastModifiedDatetime;
	public String getWorkflowTxnStepActorPkId() {
		return workflowTxnStepActorPkId;
	}
	public void setWorkflowTxnStepActorPkId(String workflowTxnStepActorPkId) {
		this.workflowTxnStepActorPkId = workflowTxnStepActorPkId;
	}
	public String getWorkflowTxnStepFkId() {
		return workflowTxnStepFkId;
	}
	public void setWorkflowTxnStepFkId(String workflowTxnStepFkId) {
		this.workflowTxnStepFkId = workflowTxnStepFkId;
	}
	public String getStepActorDefnFkId() {
		return stepActorDefnFkId;
	}
	public void setStepActorDefnFkId(String stepActorDefnFkId) {
		this.stepActorDefnFkId = stepActorDefnFkId;
	}
	public String getResolvedPersonFkId() {
		return resolvedPersonFkId;
	}
	public void setResolvedPersonFkId(String resolvedPersonFkId) {
		this.resolvedPersonFkId = resolvedPersonFkId;
	}
	public String getStepActorStatusCodeFkId() {
		return stepActorStatusCodeFkId;
	}
	public void setStepActorStatusCodeFkId(String stepActorStatusCodeFkId) {
		this.stepActorStatusCodeFkId = stepActorStatusCodeFkId;
	}
	public boolean getIsDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(Timestamp lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
}
