package com.tapplent.platformutility.insert.impl;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.Map.Entry;

import com.tapplent.platformutility.insert.structure.EmployeeCodeGenRuleVO;
import com.tapplent.platformutility.insert.structure.UsernameGenRuleVo;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;

public class InsertDAOImpl extends TapplentBaseDAO implements InsertDAO {

	private static final Logger LOG = LogFactory.getLogger(InsertDAOImpl.class);

	@Override
	public int insertRow(String insertQuery) throws SQLException {
//		try {
//		SqlRunner sqlRunner = new SqlRunner(this.getConnection());
//		LOG.debug(insertQuery);
//		sqlRunner.run(insertQuery);
//		} catch (SQLException e) {
//			LOG.error(e.getMessage(), e);
//			throw e;
//		}
		int recordUpdateCount =   executeUpdate(insertQuery, null);
		return recordUpdateCount;
	}

	@Override
	public void insertG11Values(Map<String, Object> labels, String g11nTableName) throws Exception {
//		PreparedStatement ps;
//		String sql = G11n.insertPrefix + G11n.insertString + ";";
//		try{
//			Connection con = null;
//			con = getConnection();
//			ps = con.prepareStatement(sql);
//			ps.setString(1, g11nTableName);
//			int i = 2;
//			for(Entry<String, Object> entry : labels.entrySet()){
//				HashMap<String, String> localeToValueMap = (HashMap<String, String>) entry.getValue();
//				for(Entry<String, String> localToValue : localeToValueMap.entrySet()){
//					ps.setString(i++, entry.getKey());
//					ps.setString(i++, localToValue.getKey());
//					ps.setString(i++, localToValue.getValue());
//					ps.setString(i++, "SAVED"); //FIXME: should come from serverProperties
//				}
//			}
//			LOG.debug(sql);
//			ps.executeUpdate();
//		} catch (Exception e) {
//			e.printStackTrace();
//			LOG.error(sql);
//			throw e;
////			throw new RuntimeException(e);
//		}
	}

	@Override
	public void insertRowWithParameterMap(String SQL, LinkedHashMap<String, String> parameterMap) {
		PreparedStatement ps;
		Connection con = null;
		try {
			con = getConnection();
			ps = con.prepareStatement(SQL);
			int i = 1;
			for(Entry<String, String> entry : parameterMap.entrySet()){
				ps.setString(i++, entry.getValue());
			}
			LOG.debug(SQL);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public Timestamp findCurrentRecord(String tableName, String primaryKey, String dataType,
									   Object primaryKeyValue, String currentTimestampString) throws SQLException {//FIXME workflow state is completed if applicable. AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' not required -> as current record can be INACTIVE.
		if (dataType.equals("T_ID"))
			primaryKeyValue = primaryKeyValue.toString();// save state is there because draft record cannot be a current record, its record state will be decided based upon current timestamp if there is no current record.
		String SQL = "SELECT EFFECTIVE_DATETIME FROM " + tableName + " WHERE " + primaryKey + " = "+ Util.getStringTobeInserted(dataType, primaryKeyValue, false) +" AND EFFECTIVE_DATETIME <= '"+currentTimestampString+"' AND SAVE_STATE_CODE_FK_ID = 'SAVED' ORDER BY EFFECTIVE_DATETIME DESC LIMIT 1;";//(AND IS_DELETED = FALSE) removed as the new deleted version of record will never be current record
		PreparedStatement ps;
		Timestamp currentTimeStamp = null;
		Connection con = null;
		con = getConnection();
		ps = con.prepareStatement(SQL);
		LOG.debug(SQL);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			currentTimeStamp = rs.getTimestamp("EFFECTIVE_DATETIME");
		}
		return currentTimeStamp;
	}

	@Override
	public void updateRecordState(String tableName, String primaryKey, Object primaryKeyValue, String dataType, String currentTimestampString) throws SQLException {
		String SQL = "UPDATE " + tableName + " SET RECORD_STATE_CODE_FK_ID = CASE "
					+ "WHEN EFFECTIVE_DATETIME = "+ currentTimestampString +" THEN 'CURRENT' "
					+ "WHEN EFFECTIVE_DATETIME < "+ currentTimestampString +" THEN 'HISTORY' "
					+ "WHEN EFFECTIVE_DATETIME > "+ currentTimestampString +" THEN 'FUTURE' END "
					+ "WHERE " + primaryKey + " = "+ Util.getStringTobeInserted(dataType, primaryKeyValue, false) +";";
//		PreparedStatement ps;
//		Connection con = null;
//		con = getConnection();
//		ps = con.prepareStatement(SQL);
//		LOG.debug(ps.toString());
		executeUpdate(SQL, null);
	}

	@Override
	public EmployeeCodeGenRuleVO getEmployeeCodeGenRule(String employeeCodeGenKey) throws SQLException {
		EmployeeCodeGenRuleVO employeeCodeGenRuleVO = null;
		String sql = "SELECT * FROM T_PRN_ADM_EMP_CODE_GEN_RULE WHERE EMP_CODE_GEN_RULE_CODE_PK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(employeeCodeGenKey);
		ResultSet rs = executeSQL(sql, parameters);
		if (rs.next()) {
			employeeCodeGenRuleVO = new EmployeeCodeGenRuleVO();
			employeeCodeGenRuleVO.setEmployeeCodeGenRulePkId(Util.convertByteToString(rs.getBytes("EMP_CODE_GEN_RULE_CODE_PK_ID")));
			employeeCodeGenRuleVO.setEmployeeCodeSequence(rs.getLong("EMP_CODE_SEQ_POS_INT"));
			employeeCodeGenRuleVO.setEmployeeSequenceStep(rs.getLong("EMP_SEQ_STEP_POS_INT"));
			employeeCodeGenRuleVO.setFormula(rs.getString("FORMULA_BIG_TXT"));
		}
		return employeeCodeGenRuleVO;
	}

	@Override
	public String calculateFormula(String formula) throws SQLException {
		List<Object> params = new ArrayList<>();
		formula = "SELECT "+formula+" AS Result;";
		ResultSet rs = executeSQL(formula, params);
		String result = null;
		if (rs.next()) {
			result = rs.getString("Result");
		}
		return result;
	}

	@Override
	public UsernameGenRuleVo getUsernameGenRule(String usernameGenKey) throws SQLException {
		UsernameGenRuleVo usernameGenRuleVo = null;
		String sql = "SELECT * FROM T_PRN_ADM_USR_NAME_GEN_RULE WHERE USR_NAME_GEN_RULE_CODE_PK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(usernameGenKey);
		ResultSet rs = executeSQL(sql, parameters);
		if (rs.next()) {
			usernameGenRuleVo = new UsernameGenRuleVo();
			usernameGenRuleVo.setUsernameGenRulePkId(Util.convertByteToString(rs.getBytes("USR_NAME_GEN_RULE_CODE_PK_ID")));
			usernameGenRuleVo.setFormula(rs.getString("FORMULA_BIG_TXT"));
		}
		return usernameGenRuleVo;
	}

	@Override
	public void insertUser(Map<String, Object> data, String username, String encryptedPassword, String currentTimestampString, String userEffectiveDatetime) throws SQLException {
		StringBuilder sql = new StringBuilder("INSERT INTO T_USR_ADM_USER (VERSION_ID, USER_NAME_PK_TXT, USER_PASSWORD_TXT," +
				" IS_LOCKED, PERSON_FK_ID, NOTES_G11N_BIG_TXT, EFFECTIVE_DATETIME, SAVE_STATE_CODE_FK_ID, ACTIVE_STATE_CODE_FK_ID, RECORD_STATE_CODE_FK_ID, ");
		sql.append(Util.getStandardColumns() + ") VALUE (ordered_uuid(uuid()), ")
			.append("'"+username+"', ")
			.append("'"+encryptedPassword+"', FALSE, ")
			.append(data.get("Person.PrimaryKeyID")+", NULL, ")
			.append("'"+userEffectiveDatetime+"', ")
			.append("'SAVED', 'ACTIVE', 'CURRENT', ")
			.append(Util.getStandardColumnValues(currentTimestampString)+");");
		insertRow(sql.toString());
	}

	@Override
	public void updateEmployeeCodeSequence(String employeeCodeGenKey) throws SQLException {
		String sql = "UPDATE T_PRN_ADM_EMP_CODE_GEN_RULE SET EMP_CODE_SEQ_POS_INT = EMP_CODE_SEQ_POS_INT + EMP_SEQ_STEP_POS_INT WHERE EMP_CODE_GEN_RULE_CODE_PK_ID = ?;";
		List<Object> parameters = new ArrayList<>();
		parameters.add(employeeCodeGenKey);
		executeUpdate(sql, parameters);
	}

	@Override
	public ResultSet search(String sql, List<Object> parameter) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection c = getConnection();
		ps = c.prepareStatement(sql);
		if(parameter!=null){
			int i= 1;
			for(Object param : parameter){
				LOG.debug("Select query parameter number "+i+" => "+param.toString());
				ps.setObject(i,param);
				i++;
			}
		}
		LOG.debug(sql);
		rs = ps.executeQuery();
		return rs;
	}

	@Override
	public void insertRowWithParameter(String sql, Deque<Object> parameter) throws SQLException {
		PreparedStatement ps = null;
		try{
			Connection c = getConnection();
			ps = c.prepareStatement(sql);
			LOG.debug(sql);
			if(parameter!=null){
				int i= 1;
				for(Object param : parameter){
					LOG.debug("insert query parameter number "+i+" => "+param.toString());
					ps.setObject(i,param);
					i++;
				}
			}
			int count = ps.executeUpdate();
			LOG.debug("$$$    "+ count +"   $$$ number of records inserted or updated");
		} catch (SQLException e){
			LOG.error(e.getMessage(), e);
			throw e;
		}
	}
}
