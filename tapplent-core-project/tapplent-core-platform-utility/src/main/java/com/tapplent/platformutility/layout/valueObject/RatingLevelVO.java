package com.tapplent.platformutility.layout.valueObject;

public class RatingLevelVO {
    private String ratingLevelPkId;
    private String ratingLevelName;
    private double ratingLevelAnchorScore;
    private double ratingLevelAverageScore;
    private double ratingLevelMinimumScore;
    private double ratingLevelMaximumScore;

    public String getRatingLevelPkId() {
        return ratingLevelPkId;
    }

    public void setRatingLevelPkId(String ratingLevelPkId) {
        this.ratingLevelPkId = ratingLevelPkId;
    }

    public String getRatingLevelName() {
        return ratingLevelName;
    }

    public void setRatingLevelName(String ratingLevelName) {
        this.ratingLevelName = ratingLevelName;
    }

    public double getRatingLevelAnchorScore() {
        return ratingLevelAnchorScore;
    }

    public void setRatingLevelAnchorScore(double ratingLevelAnchorScore) {
        this.ratingLevelAnchorScore = ratingLevelAnchorScore;
    }

    public double getRatingLevelAverageScore() {
        return ratingLevelAverageScore;
    }

    public void setRatingLevelAverageScore(double ratingLevelAverageScore) {
        this.ratingLevelAverageScore = ratingLevelAverageScore;
    }

    public double getRatingLevelMinimumScore() {
        return ratingLevelMinimumScore;
    }

    public void setRatingLevelMinimumScore(double ratingLevelMinimumScore) {
        this.ratingLevelMinimumScore = ratingLevelMinimumScore;
    }

    public double getRatingLevelMaximumScore() {
        return ratingLevelMaximumScore;
    }

    public void setRatingLevelMaximumScore(double ratingLevelMaximumScore) {
        this.ratingLevelMaximumScore = ratingLevelMaximumScore;
    }
}
