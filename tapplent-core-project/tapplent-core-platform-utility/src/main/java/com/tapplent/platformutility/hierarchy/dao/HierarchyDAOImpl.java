package com.tapplent.platformutility.hierarchy.dao;

import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.hierarchy.structure.HierarchyInfo;
import com.tapplent.platformutility.hierarchy.structure.Node;
import com.tapplent.platformutility.hierarchy.structure.Relation;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.impl.SearchDAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Manas on 8/11/16.
 */
public class HierarchyDAOImpl extends TapplentBaseDAO implements HierarchyDAO{

    private SearchDAO searchDAO;

    @Override
    public Map<String, Node> getNthLevelParent(String primaryKey, EntityMetadataVOX doMeta, int level) {
        EntityAttributeMetadata pkMeta = doMeta.getFunctionalPrimaryKeyAttribute();
        String pkColumaName = pkMeta.getDbColumnName();
        StringBuilder sql = new StringBuilder(Util.getSqlStringForTable(doMeta)+" AS node, "+doMeta.getDbTableName()+" AS parent "+
                "WHERE node.INTRNL_LEFT_POS_INT BETWEEN parent.INTRNL_LEFT_POS_INT AND parent.INTRNL_RIGHT_POS_INT AND node."+pkColumaName+" = ");
        if(pkMeta.getDbDataTypeCode().equals("T_ID")) sql.append("x");
        sql.append("? ORDER BY parent.INTRNL_LEFT_POS_INT DESC LIMIT "+level+" , 1;");
        Deque<Object> parameter = new ArrayDeque<>();
        parameter.add(primaryKey);
        SearchResult result = searchDAO.search(sql.toString(), parameter);
        List<Map<String, Object>> resultList = result.getData();
        if(resultList != null && !resultList.isEmpty()){
            Map<String, Node> nthLevelParent = new HashMap<>();
            Map<String, Object> row = resultList.get(0);
            Node node = new Node(row, level);
            String key = (String) row.get(pkMeta.getDoAttributeCode());
            nthLevelParent.put(key, node);
            return nthLevelParent;
        }
        return null;
    }

    @Override
    public Map<String, Node> getAllChildrenUptoNlevel(String primaryKey, EntityMetadataVOX doMeta, int level) {
        EntityAttributeMetadata pkMeta = doMeta.getFunctionalPrimaryKeyAttribute();
        String pkColumaName = pkMeta.getDbColumnName();
        String selectClause = Util.getSqlStringForTable(doMeta);
        selectClause = selectClause.replace("SELECT ", "SELECT (COUNT(parent."+pkColumaName+") - (sub_tree.depth + 1)) AS depth, ");
        StringBuilder sql = new StringBuilder(selectClause+" AS node, "+doMeta.getDbTableName()+" AS parent, "+doMeta.getDbTableName()+" AS sub_parent, ");
        sql.append("SELECT node."+pkColumaName+", (COUNT(parent."+pkColumaName+") - 1) AS depth FROM "+doMeta.getDbTableName()+" AS node, ");
        sql.append(doMeta.getDbTableName()+" AS parent ");
        sql.append("WHERE node.INTRNL_LEFT_POS_INT BETWEEN parent.INTRNL_LEFT_POS_INT AND parent.INTRNL_RIGHT_POS_INT ");
        sql.append("AND node."+pkColumaName+" = ");
        if(pkMeta.getDbDataTypeCode().equals("T_ID")) sql.append("x");
        sql.append("? GROUP BY node."+pkColumaName+" ORDER BY node.INTRNL_LEFT_POS_INT)AS sub_tree " +
                "WHERE node.INTRNL_LEFT_POS_INT BETWEEN parent.INTRNL_LEFT_POS_INT AND parent.INTRNL_RIGHT_POS_INT " +
                "AND node.INTRNL_LEFT_POS_INT BETWEEN sub_parent.INTRNL_LEFT_POS_INT AND sub_parent.INTRNL_RIGHT_POS_INT " +
                "AND sub_parent."+pkColumaName+" = sub_tree."+pkColumaName+" " +
                "GROUP BY node."+pkColumaName+" " +
                "HAVING depth BETWEEN 1 AND " + level +
                " ORDER BY node.INTRNL_LEFT_POS_INT;");
        Deque<Object> parameter = new ArrayDeque<>();
        parameter.add(primaryKey);
        SearchResult result = searchDAO.search(sql.toString(), parameter);
        List<Map<String, Object>> resultList = result.getData();
        if(resultList != null && !resultList.isEmpty()){
            Map<String, Node> childrenUptoNLevel = new HashMap<>();
            String pkDoaName = pkMeta.getDoAttributeCode();
            for(Map<String, Object> row : resultList) {
                int depth = (int) row.get("depth");
                Node node = new Node(row, depth);
                String key = (String) row.get(pkDoaName);
                childrenUptoNLevel.put(key, node);
            }
            return childrenUptoNLevel;
        }
        return null;
    }

    @Override
    public Map<String, Node> getAllParents(String primaryKey, EntityMetadataVOX doMeta) {
        EntityAttributeMetadata pkMeta = doMeta.getFunctionalPrimaryKeyAttribute();
        String pkColumaName = pkMeta.getDbColumnName();
        StringBuilder sql = new StringBuilder(Util.getSqlStringForTable(doMeta)+" AS parent, "+doMeta.getDbTableName()+" AS node "+
                "WHERE node.INTRNL_LEFT_POS_INT BETWEEN parent.INTRNL_LEFT_POS_INT AND parent.INTRNL_RIGHT_POS_INT AND node."+pkColumaName+" = ");
        if(pkMeta.getDbDataTypeCode().equals("T_ID")) sql.append("x");
        sql.append("? ORDER BY parent.INTRNL_LEFT_POS_INT ;");
        Deque<Object> parameter = new ArrayDeque<>();
        parameter.add(primaryKey);
        SearchResult result = searchDAO.search(sql.toString(), parameter);
        int level = 0;
        List<Map<String, Object>> resultList = result.getData();
        Map<String, Node> parentMap = new ConcurrentHashMap<>();
        if(resultList != null && !resultList.isEmpty()){
            for (Map<String, Object> row : resultList) {
                Node node = new Node(row, level++);
                String key = (String) row.get(pkMeta.getDoAttributeCode());
                parentMap.put(key, node);
            }
            return parentMap;
        }
        return null;
    }

    @Override
    public List<HierarchyInfo> getAllChildren(MasterEntityMetadataVOX rootMtPEMeta, String pk) {
        List<HierarchyInfo> result = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT\n" +
                "  RESULT."+rootMtPEMeta.getFunctionalPrimaryKeyAttribute().getDbColumnName() +",\n" +
                "  RESULT."+rootMtPEMeta.getHierarchyKeyAttribute().getDbColumnName() +"\n" +
                "FROM "+ rootMtPEMeta.getDbTableName() +" RESULT INNER JOIN "+ rootMtPEMeta.getDbTableName() +" PK"+"\n" +
                "WHERE RESULT.INTRNL_HIERARCHY_ID = PK.INTRNL_HIERARCHY_ID\n" +
                "      AND\n" +
                "      RESULT.INTRNL_LEFT_POS_INT >= PK.INTRNL_LEFT_POS_INT\n" +
                "      AND\n" +
                "      RESULT.INTRNL_RIGHT_POS_INT <= PK.INTRNL_RIGHT_POS_INT\n" +
                "      AND\n" +
                "      PK."+rootMtPEMeta.getFunctionalPrimaryKeyAttribute().getDbColumnName()+ "=");
                if(rootMtPEMeta.getFunctionalPrimaryKeyAttribute().getDbDataTypeCode().equals("T_ID")){
                    sql.append("").append(pk).append(" ");
                }else{
                    sql.append("'").append(pk).append("' ");
                }
                sql.append("ORDER BY RESULT.INTRNL_LEFT_POS_INT ;");
        List<Object> parameters = new ArrayList<>();
        ResultSet rs = executeSQL(sql.toString(), parameters);
        // Now if we have Result set we should lop through it and get all the children.
        try {
            while (rs.next()){
                HierarchyInfo hierarchyInfo = null;
                if(rootMtPEMeta.getFunctionalPrimaryKeyAttribute().getDbDataTypeCode().equals("T_ID")){
                    hierarchyInfo = new HierarchyInfo(Util.convertByteToString(rs.getBytes(rootMtPEMeta.getFunctionalPrimaryKeyAttribute().getDbColumnName())), Util.convertByteToString(rs.getBytes(rootMtPEMeta.getHierarchyKeyAttribute().getDbColumnName())));
                }else {
                    hierarchyInfo = new HierarchyInfo(rs.getString(rootMtPEMeta.getFunctionalPrimaryKeyAttribute().getDbColumnName()),rs.getString(rootMtPEMeta.getHierarchyKeyAttribute().getDbColumnName()));
                }
                result.add(hierarchyInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Map<String, Relation> getDirectManagers(String personId, String hierarchyId) {
        Map<String, Node> parentMap = getAllParentsForPerson(personId, hierarchyId);
        if (parentMap != null && !parentMap.isEmpty()){
            Map<String, Relation> directManagerMap = new ConcurrentHashMap<>();
            for (Map.Entry<String, Node> nodeEntry : parentMap.entrySet()){
                Node node  = nodeEntry.getValue();
                Relation relation = new Relation();
                relation.setLevel(node.getLevel());
                if (node.getLevel() > 1)
                    relation.setRelationshipType("DIRECT_MANAGER+");
                else relation.setRelationshipType("DIRECT_MANAGER");
                relation.setSubjectPersonId(personId);
                directManagerMap.put(nodeEntry.getKey(), relation);
            }
            return directManagerMap;
        }
        return null;
    }

    @Override
    public Map<String, Relation> getDirectManagers(String personId) {
        Map<String, Node> parentMap = getAllParentsForPerson(personId);
        if (parentMap != null && !parentMap.isEmpty()){
            Map<String, Relation> directManagerMap = new ConcurrentHashMap<>();
            for (Map.Entry<String, Node> nodeEntry : parentMap.entrySet()){
                Node node  = nodeEntry.getValue();
                Relation relation = new Relation();
                relation.setLevel(node.getLevel());
                relation.setRelationshipType("DIRECT_MANAGER");
                relation.setSubjectPersonId(personId);
                directManagerMap.put(nodeEntry.getKey(), relation);
            }
            return directManagerMap;
        }
        return null;
    }

    @Override
    public String getInverseRelationshipType(String relationshipType) {
        String sql = "SELECT * FROM T_PRN_LKP_JOB_RELATIONSHIP_TYPE WHERE RELATIONSHIP_TYPE_CODE_PK_ID = ?;";
        Deque<Object> parameter = new ArrayDeque<>();
        parameter.add(relationshipType);
        SearchResult result = searchDAO.search(sql, parameter);
        List<Map<String, Object>> resultList = result.getData();
        String reverseRelationType = null;
        if(resultList != null && !resultList.isEmpty()){
            reverseRelationType = (String) resultList.get(0).get("INV_RELTN_TYPE_CODE_FK_ID");
        }
        return reverseRelationType;
    }

    @Override
    public Map<String, List<Relation>> getOtherFirstLevelReport(String personId) {
        String sql = "SELECT * FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP WHERE RELATED_PERSON_FK_ID = ? AND RELATIONSHIP_TYPE_CODE_FK_ID <> 'DIRECT_MANAGER';";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(personId));
        ResultSet rs = executeSQL(sql, parameters);
        Map<String, List<Relation>> relationMap = null;
        try {
            if (rs != null) {
                relationMap =  new HashMap<>();
                while (rs.next()) {
                    Relation relation = new Relation();
                    String relationshipType = rs.getString("RELATIONSHIP_TYPE_CODE_FK_ID");
                    String inverseRelationType = getInverseRelationshipType(relationshipType);
                    relation.setRelationshipType(inverseRelationType);
                    relation.setLevel(1);
                    String relatedPersonId = Util.convertByteToString(rs.getBytes("SUBJECT_PERSON_FK_ID"));//due to inverse relation, subject user of table becomes related person.
                    relation.setSubjectPersonId(personId);
                    if (relationMap.containsKey(relatedPersonId)) {
                        relationMap.get(relatedPersonId).add(relation);
                    }else {
                        List<Relation> relationList = new ArrayList<>();
                        relationList.add(relation);
                        relationMap.put(relatedPersonId, relationList);
                    }
                }
            }
            return relationMap;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Map<String,Node> getAllParentsForPerson(String personId, String hierarchyId) {
        String sql = "SELECT parent.RELATED_PERSON_FK_ID, parent.INTRNL_LEFT_POS_INT, parent.INTRNL_RIGHT_POS_INT FROM\n" +
                "  (SELECT TPEPJR.INTRNL_LEFT_POS_INT, TPEPJA.PERSON_DEP_FK_ID, TPEPJR.INTRNL_HIERARCHY_ID FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                "  (SELECT TPEPJAB.PERSON_DEP_FK_ID, TPEPJRB.RELATED_PERSON_FK_ID,TPEPJRB.INTRNL_LEFT_POS_INT,TPEPJRB.INTRNL_RIGHT_POS_INT, TPEPJRB.INTRNL_HIERARCHY_ID   FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                "  WHERE node.INTRNL_LEFT_POS_INT BETWEEN parent.INTRNL_LEFT_POS_INT AND parent.INTRNL_RIGHT_POS_INT AND node.INTRNL_HIERARCHY_ID = "+hierarchyId+" AND parent.INTRNL_HIERARCHY_ID = "+hierarchyId+" \n" +
                "  AND node.PERSON_DEP_FK_ID = "+personId+" ORDER BY parent.INTRNL_LEFT_POS_INT;";

        Deque<Object> parameter = new ArrayDeque<>();
        SearchResult result = searchDAO.search(sql, parameter);
        List<Map<String, Object>> resultList = result.getData();
        int level = resultList.size();
        Map<String, Node> parentMap = new ConcurrentHashMap<>();
        if(resultList != null && !resultList.isEmpty()){
            for (Map<String, Object> row : resultList) {
                Node node = new Node(row, level--);
                node.setLeft((int) row.get("INTRNL_LEFT_POS_INT"));
                node.setRight((int) row.get("INTRNL_RIGHT_POS_INT"));
                String key = Util.convertByteToString((byte[]) row.get("RELATED_PERSON_FK_ID"));
                if (key != null)
                    parentMap.put(key, node);
            }
        }
        return parentMap;
    }

    private Map<String,Node> getAllParentsForPerson(String personId) {
        String sql = "SELECT parent.RELATED_PERSON_FK_ID, parent.INTRNL_LEFT_POS_INT, parent.INTRNL_RIGHT_POS_INT FROM\n" +
                "  (SELECT TPEPJR.INTRNL_LEFT_POS_INT, TPEPJA.PERSON_DEP_FK_ID, TPEPJR.INTRNL_HIERARCHY_ID FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                "  (SELECT TPEPJAB.PERSON_DEP_FK_ID, TPEPJRB.RELATED_PERSON_FK_ID,TPEPJRB.INTRNL_LEFT_POS_INT,TPEPJRB.INTRNL_RIGHT_POS_INT, TPEPJRB.INTRNL_HIERARCHY_ID   FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                "  WHERE node.INTRNL_LEFT_POS_INT BETWEEN parent.INTRNL_LEFT_POS_INT AND parent.INTRNL_RIGHT_POS_INT AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID \n" +
                "  AND node.PERSON_DEP_FK_ID = "+personId+" ORDER BY parent.INTRNL_LEFT_POS_INT;";

        Deque<Object> parameter = new ArrayDeque<>();
        SearchResult result = searchDAO.search(sql, parameter);
        List<Map<String, Object>> resultList = result.getData();
        int level = resultList.size();
        Map<String, Node> parentMap = new ConcurrentHashMap<>();
        if(resultList != null && !resultList.isEmpty()){
            for (Map<String, Object> row : resultList) {
                Node node = new Node(row, level--);
                node.setLeft((int) row.get("INTRNL_LEFT_POS_INT"));
                node.setRight((int) row.get("INTRNL_RIGHT_POS_INT"));
                String key = Util.convertByteToString((byte[]) row.get("RELATED_PERSON_FK_ID"));
                if (key != null)
                    parentMap.put(key, node);
            }
        }
        return parentMap;
    }


    @Override
    public Map<String, List<Relation>> getOtherFirstLevelManager(String personId) {
//        EntityMetadataVOX metadataVOX = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement("PersonTemplate.PersonJobRelationship");
        String sql = "SELECT * FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP WHERE SUBJECT_PERSON_FK_ID = ? AND RELATIONSHIP_TYPE_CODE_FK_ID <> 'DIRECT_MANAGER';";
//        Map<String, Node> firsLevelManagerMap = null;
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(personId));
        ResultSet rs = executeSQL(sql, parameters);
        Map<String, List<Relation>> relationMap = null;
        try {
            if (rs != null) {
                relationMap =  new HashMap<>();
                while (rs.next()) {
                    Relation relation = new Relation();
                    String relationshipType = rs.getString("RELATIONSHIP_TYPE_CODE_FK_ID");
                    relation.setRelationshipType(relationshipType);
                    relation.setLevel(1);
                    String relatedPersonId = Util.convertByteToString(rs.getBytes("RELATED_PERSON_FK_ID"));
                    relation.setSubjectPersonId(personId);
                    if (relationMap.containsKey(relatedPersonId)) {
                        relationMap.get(relatedPersonId).add(relation);
                    }else {
                        List<Relation> relationList = new ArrayList<>();
                        relationList.add(relation);
                        relationMap.put(relatedPersonId, relationList);
                    }
                }
            }
            return relationMap;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateleftRightForNodeInsert(String hierarchyId, String parentNodeId, EntityMetadataVOX entityMetadataVOX, String primaryNodeId) throws SQLException {
        int leftOfNode = getLeftOfNodeInserted(hierarchyId, parentNodeId, entityMetadataVOX);
        updateRightValuesForInsert(hierarchyId, leftOfNode, entityMetadataVOX);
        updateLeftValuesForInsert(hierarchyId, leftOfNode, entityMetadataVOX);
        updateInsertedNode(hierarchyId, leftOfNode, entityMetadataVOX, primaryNodeId);
    }

    @Override
    public void updateLeftRightForNodeDelete(String hierarchyId, EntityMetadataVOX entityMetadataVOX, int nodeLeft, int nodeRight, String primaryKey) throws SQLException {
        int nodeWidth = nodeRight - nodeLeft + 1;
        updateRightValuesForDelete(hierarchyId, entityMetadataVOX, nodeRight, nodeWidth);
        updateLeftValuesForDelete(hierarchyId, entityMetadataVOX, nodeRight, nodeWidth);
    }

    @Override
    public void updateLeftRightForNodeParentUpdate(String hierarchyId, EntityMetadataVOX entityMetadataVOX, Node rootNode, Node parentNode) throws SQLException {
        String tableName = entityMetadataVOX.getDbTableName();
        int origin_lft = rootNode.getLeft();
        int origin_rgt= rootNode.getRight();
        int new_parent_rgt = parentNode.getRight();
        String sql = "UPDATE "+tableName+"\n" +
                "    SET INTRNL_LEFT_POS_INT\n" +
                "          = INTRNL_LEFT_POS_INT\n" +
                "            + CASE\n" +
                "              WHEN "+new_parent_rgt +" < "+origin_lft+"\n" +
                "                THEN CASE\n" +
                "                     WHEN INTRNL_LEFT_POS_INT BETWEEN "+origin_lft +"AND "+origin_rgt+" AND HIERARCHY_ID = "+hierarchyId+"\n" +
                "                       THEN "+new_parent_rgt +" - "+origin_lft+"\n" +
                "                     WHEN INTRNL_LEFT_POS_INT BETWEEN "+new_parent_rgt+" \n" +
                "                     AND "+origin_lft +"- 1 AND HIERARCHY_ID = "+hierarchyId+"\n" +
                "                       THEN "+origin_rgt+" - "+origin_lft +"+ 1\n" +
                "                     ELSE 0 END\n" +
                "              WHEN "+new_parent_rgt +" > "+origin_rgt+"\n" +
                "                THEN CASE\n" +
                "                     WHEN INTRNL_LEFT_POS_INT BETWEEN "+origin_lft +"AND "+origin_rgt+" AND HIERARCHY_ID = "+hierarchyId+"\n" +
                "                       THEN "+new_parent_rgt +" - "+origin_rgt+" - 1\n" +
                "\n" +
                "                     WHEN INTRNL_LEFT_POS_INT BETWEEN "+origin_rgt+" + 1\n" +
                "                     AND "+new_parent_rgt +" - 1 AND HIERARCHY_ID = "+hierarchyId+"\n" +
                "                       THEN "+origin_lft +"- "+origin_rgt+" - 1\n" +
                "                     ELSE 0 END\n" +
                "              ELSE 0 END,\n" +
                "\n" +
                "      INTRNL_RIGHT_POS_INT = INTRNL_RIGHT_POS_INT + CASE\n" +
                "                  WHEN "+new_parent_rgt +" < "+origin_lft+"\n" +
                "                    THEN CASE\n" +
                "                         WHEN INTRNL_RIGHT_POS_INT BETWEEN "+origin_lft +"AND "+origin_rgt+" AND HIERARCHY_ID = "+hierarchyId+"\n" +
                "                           THEN "+new_parent_rgt +" - "+origin_lft+"\n" +
                "                         WHEN INTRNL_RIGHT_POS_INT BETWEEN "+new_parent_rgt +" AND "+origin_lft +"- 1 AND HIERARCHY_ID = "+hierarchyId+"\n" +
                "                           THEN "+origin_rgt+" - "+origin_lft +"+ 1\n" +
                "                         ELSE 0 END\n" +
                "                  WHEN "+new_parent_rgt +" > "+origin_rgt+"\n" +
                "                    THEN CASE\n" +
                "                         WHEN INTRNL_RIGHT_POS_INT BETWEEN "+origin_lft +"AND "+origin_rgt+ " AND HIERARCHY_ID = "+hierarchyId+"\n" +
                "                           THEN "+new_parent_rgt +" - "+origin_rgt+" - 1\n" +
                "                         WHEN INTRNL_RIGHT_POS_INT BETWEEN "+origin_rgt+" + 1\n" +
                "                         AND "+new_parent_rgt +" - 1 AND HIERARCHY_ID = "+hierarchyId+"\n" +
                "                           THEN "+origin_lft +"- "+origin_rgt+" - 1\n" +
                "                         ELSE 0 END\n" +
                "                  ELSE 0 END;";
        executeUpdate(sql, null);
        return;
    }

    @Override
    public String getHierarchyIdForPerson(String basePersonId) {
        String hierarchyId = null;
        String sql = "SELECT INTRNL_HIERARCHY_ID FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP WHERE SUBJECT_PERSON_FK_ID = "+basePersonId+" AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER';";//FIXME  add is primary clause also from job_assignment
        List<Object> parameters = new ArrayList<>();
        ResultSet rs = executeSQL(sql, parameters);
        try {
            if (rs != null && rs.next()) {
                hierarchyId = Util.convertByteToString(rs.getBytes("INTRNL_HIERARCHY_ID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hierarchyId;
    }

    @Override
    public String getPeronIdForRootPerson(String hierarchyId) {
            String SQL="SELECT A.PERSON_DEP_FK_ID AS PERSON_ID FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP P INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE R\n" +
                    "    ON P.RELATIONSHIP_TYPE_CODE_FK_ID = R.RELATIONSHIP_TYPE_CODE_PK_ID AND R.IS_DIRECT_MANAGER INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT A\n" +
                    "    ON A.PERSON_JOB_ASSIGNMENT_PK_ID = P.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND A.IS_PRIMARY = TRUE WHERE P.RELATED_PERSON_FK_ID IS NULL AND INTRNL_HIERARCHY_ID = ?;\n;";
            List<Object> parameters = new ArrayList<>();
            parameters.add(Util.convertStringIdToByteId(hierarchyId));
            ResultSet rs = executeSQL(SQL, parameters);
            String personId = null;
            try {
                if (rs.next()){
                    personId = Util.convertByteToString(rs.getBytes("PERSON_ID"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return personId;
    }

    @Override
    public List<String> getHierarchialPersonRoots() {
        List<String> personList = new ArrayList<>();
        Map<String, String> result = new HashMap<>();
        String SQL="SELECT A.PERSON_DEP_FK_ID AS PERSON_ID FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP P INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE R\n" +
                "    ON P.RELATIONSHIP_TYPE_CODE_FK_ID = R.RELATIONSHIP_TYPE_CODE_PK_ID AND R.IS_DIRECT_MANAGER INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT A\n" +
                "    ON A.PERSON_JOB_ASSIGNMENT_PK_ID = P.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND A.IS_PRIMARY = TRUE WHERE P.RELATED_PERSON_FK_ID IS NULL;\n;";
        List<Object> parameters = new ArrayList<>();
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            String personId;
            while(rs.next()){
                personId = Util.convertByteToString(rs.getBytes("PERSON_ID"));
                personList.add(personId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personList;
    }

    @Override
    public List<String> getDirectChildrenListForParentPerson(String parentId) {
            String sql = "SELECT A.PERSON_DEP_FK_ID, P.RELATED_PERSON_FK_ID FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP P INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE R\n" +
                    "    ON P.RELATIONSHIP_TYPE_CODE_FK_ID = R.RELATIONSHIP_TYPE_CODE_PK_ID AND R.IS_DIRECT_MANAGER INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT A\n" +
                    "    ON A.PERSON_JOB_ASSIGNMENT_PK_ID = P.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND A.IS_PRIMARY = TRUE WHERE P.RELATED_PERSON_FK_ID = "+parentId+"; ";
        List<Object> parameters = new ArrayList<>();
//		parameters.add(parentId);
        ResultSet rs = executeSQL(sql, parameters);
        List<String> childrenList = null;
        try {
            childrenList = new ArrayList<>();
            while(rs.next()){
                String personFkId = Util.convertByteToString(rs.getBytes("A.PERSON_DEP_FK_ID"));
                childrenList.add(personFkId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return childrenList;
    }

    @Override
    public void updateLeftRightAndHierarchyValuesForPerson(String parentId, int leftValue, int rightValue, String hierarchyId) throws SQLException {
        String sql = "UPDATE T_PRN_EU_PERSON_JOB_RELATIONSHIP INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE "
                + " ON RELATIONSHIP_TYPE_CODE_FK_ID = RELATIONSHIP_TYPE_CODE_PK_ID AND IS_DIRECT_MANAGER INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID AND IS_PRIMARY "
                + " SET INTRNL_LEFT_POS_INT = "+leftValue+", INTRNL_RIGHT_POS_INT = "+rightValue+", INTRNL_HIERARCHY_ID = "+hierarchyId+" WHERE PERSON_DEP_FK_ID = "+parentId+";";

        List<Object> params = new ArrayList<>();
        executeUpdate(sql, params);
    }

    private void updateLeftValuesForDelete(String hierarchyId, EntityMetadataVOX entityMetadataVOX, int nodeRight, int nodeWidth) throws SQLException {
        String tableName = entityMetadataVOX.getDbTableName();
        String sql = "UPDATE "+tableName+" SET INTRNL_LEFT_POS_INT = INTRNL_LEFT_POS_INT - "+ nodeWidth + " WHERE INTRNL_LEFT_POS_INT > "+ nodeRight +" AND INTRNL_HIERARCHY_ID = "+hierarchyId+";";
        executeUpdate(sql, null);
        return;
    }

    private void updateRightValuesForDelete(String hierarchyId, EntityMetadataVOX entityMetadataVOX, int nodeRight, int nodeWidth) throws SQLException {
        String tableName = entityMetadataVOX.getDbTableName();
        String sql = "UPDATE "+tableName+" SET INTRNL_RIGHT_POS_INT = INTRNL_RIGHT_POS_INT - "+ nodeWidth + " WHERE INTRNL_RIGHT_POS_INT > "+ nodeRight +" AND INTRNL_HIERARCHY_ID = "+hierarchyId+";";
        executeUpdate(sql, null);
        return;
    }

    private void updateInsertedNode(String hierarchyId, int leftOfNode, EntityMetadataVOX entityMetadataVOX, String primaryNodeId) throws SQLException {
        String tableName = entityMetadataVOX.getDbTableName();
        String pkColumn = entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName();
        String sql = "UPDATE "+tableName+" SET INTRNL_LEFT_POS_INT = "+ ++leftOfNode + ", INTRNL_RIGHT_POS_INT = "+ ++leftOfNode +" WHERE" + pkColumn + " = "+primaryNodeId+" AND INTRNL_HIERARCHY_ID = "+hierarchyId+";";
        executeUpdate(sql, null);
        return;
    }

    private void updateLeftValuesForInsert(String hierarchyId, int leftOfNode, EntityMetadataVOX entityMetadataVOX) throws SQLException {
        String tableName = entityMetadataVOX.getDbTableName();
        String sql = "UPDATE "+tableName+" SET INTRNL_RIGHT_POS_INT = INTRNL_RIGHT_POS_INT + 2 WHERE INTRNL_RIGHT_POS_INT > "+leftOfNode+" AND INTRNL_HIERARCHY_ID = "+hierarchyId+";";
        executeUpdate(sql, null);
        return;
    }

    private void updateRightValuesForInsert(String hierarchyId, int leftOfNode, EntityMetadataVOX entityMetadataVOX) throws SQLException {
        String tableName = entityMetadataVOX.getDbTableName();
        String sql = "UPDATE "+tableName+" SET INTRNL_LEFT_POS_INT = INTRNL_LEFT_POS_INT + 2 WHERE INTRNL_LEFT_POS_INT > "+leftOfNode+" AND INTRNL_HIERARCHY_ID = "+hierarchyId+";";
        executeUpdate(sql, null);
        return;
    }

    private int getLeftOfNodeInserted(String hierarchyId, String parentNodeId, EntityMetadataVOX entityMetadataVOX) {
        String tableName = entityMetadataVOX.getDbTableName();
        String pkColumn = entityMetadataVOX.getFunctionalPrimaryKeyAttribute().getDbColumnName();
        String sql = "SELECT INTRNL_LEFT_POS_INT FROM " + tableName + " WHERE " + pkColumn + " = ? AND INTRNL_HIERARCHY_ID = ? ;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(parentNodeId);
        parameters.add(hierarchyId);
        int leftOfNode = 0;
        ResultSet rs = executeSQL(sql, parameters);
        try {
            if (rs != null && rs.next()) {
                leftOfNode = rs.getInt("INTRNL_LEFT_POS_INT");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return leftOfNode;
    }
        // Here get all children with their level.

    public SearchDAO getSearchDAO() {
        return searchDAO;
    }

    public void setSearchDAO(SearchDAO searchDAO) {
        this.searchDAO = searchDAO;
    }
}
