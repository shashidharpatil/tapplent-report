package com.tapplent.platformutility.metadata.structure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import org.springframework.beans.BeanUtils;

/**
 * Created by tapplent on 17/01/17.
 */
public class CountrySpecificDOAMeta {
    @JsonIgnore
    private String btDoaCountrySpecificDisplayOverridePkId;
    @JsonIgnore
    private String btDoaCodeFkId;
    @JsonIgnore
    private String countryCodeFkId;
    private String doaDisplayNameOverrideG11nBigTxt;
    private IconVO doaDisplayIconCodeFkId;
    private boolean isVisible = true;
    private boolean isEditable = true;
    private boolean isSensitive = false;
    private boolean isDataNull;

    public String getBtDoaCountrySpecificDisplayOverridePkId() {
        return btDoaCountrySpecificDisplayOverridePkId;
    }

    public void setBtDoaCountrySpecificDisplayOverridePkId(String btDoaCountrySpecificDisplayOverridePkId) {
        this.btDoaCountrySpecificDisplayOverridePkId = btDoaCountrySpecificDisplayOverridePkId;
    }

    public String getBtDoaCodeFkId() {
        return btDoaCodeFkId;
    }

    public void setBtDoaCodeFkId(String btDoaCodeFkId) {
        this.btDoaCodeFkId = btDoaCodeFkId;
    }

    public String getCountryCodeFkId() {
        return countryCodeFkId;
    }

    public void setCountryCodeFkId(String countryCodeFkId) {
        this.countryCodeFkId = countryCodeFkId;
    }

    public String getDoaDisplayNameOverrideG11nBigTxt() {
        return Util.getG11nValue(doaDisplayNameOverrideG11nBigTxt,null);
    }

    public void setDoaDisplayNameOverrideG11nBigTxt(String doaDisplayNameOverrideG11nBigTxt) {
        this.doaDisplayNameOverrideG11nBigTxt = doaDisplayNameOverrideG11nBigTxt;
    }

    public IconVO getDoaDisplayIconCodeFkId() {
        return doaDisplayIconCodeFkId;
    }

    public void setDoaDisplayIconCodeFkId(IconVO doaDisplayIconCodeFkId) {
        this.doaDisplayIconCodeFkId = doaDisplayIconCodeFkId;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    public boolean isSensitive() {
        return isSensitive;
    }

    public void setSensitive(boolean sensitive) {
        isSensitive = sensitive;
    }

    public boolean isDataNull() {
        return isDataNull;
    }

    public void setDataNull(boolean dataNull) {
        isDataNull = dataNull;
    }

    public static CountrySpecificDOAMeta fromVO(CountrySpecificDOAMetaVO countrySpecificDOAMetaVO){
        CountrySpecificDOAMeta countrySpecificDOAMeta = new CountrySpecificDOAMeta();
        BeanUtils.copyProperties(countrySpecificDOAMetaVO, countrySpecificDOAMeta);
        return countrySpecificDOAMeta;
    }
}
