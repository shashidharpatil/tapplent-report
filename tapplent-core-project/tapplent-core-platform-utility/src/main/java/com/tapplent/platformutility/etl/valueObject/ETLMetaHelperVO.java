package com.tapplent.platformutility.etl.valueObject;

import java.util.List;

/**
 * Created by tapplent on 17/11/16.
 */
public class ETLMetaHelperVO {
    private String doName;
    private Boolean isAccessControlApplicable=false;
    private String primaryKeyColumnName;
    private String primaryKeyDBDataType;
    private String tableName;
    private String mtPE;
    private Boolean isThumbnailRequired = false;
    private List<String> doAttrCodeList;
    private String subjectUserPath;
    private String parentMtPE;
    private String intermediateTableName;
    private String intermediateMtPE;
    private String parentIntermediateTableName;
    private String parentIntermediateMtPE;
    private String rowResolvedTable;
    private String rowResolvedMtPE;
    private String colResolvedTable;
    private String colResolvedMtPE;
    private String parentRowResolvedTable;
    private String parentRowResolvedMtPE;
    private String parentColResolvedTable;
    private String parentColResolvedMtPE;

    public ETLMetaHelperVO(){

    }

    public ETLMetaHelperVO(String doName, String tableName, String primaryKeyColumnName, String primaryKeyDBDataType) {
        this.doName = doName;
       // this.isAccessControlApplicable = isAccessControlApplicable;
        this.tableName = tableName;
        this.primaryKeyColumnName = primaryKeyColumnName;
        this.primaryKeyDBDataType = primaryKeyDBDataType;
    }

    public String getDoName() {
        return doName;
    }

    public void setDoName(String doName) {
        this.doName = doName;
    }

    public Boolean getAccessControlApplicable() {
        return isAccessControlApplicable;
    }

    public void setAccessControlApplicable(Boolean accessControlApplicable) {
        isAccessControlApplicable = accessControlApplicable;
    }

    public String getPrimaryKeyColumnName() {
        return primaryKeyColumnName;
    }

    public void setPrimaryKeyColumnName(String primaryKeyColumnName) {
        this.primaryKeyColumnName = primaryKeyColumnName;
    }

    public List<String> getDoAttrCodeList() {
        return doAttrCodeList;
    }

    public void setDoAttrCodeList(List<String> doAttrCodeList) {
        this.doAttrCodeList = doAttrCodeList;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

	public Boolean getIsThumbnailRequired() {
		return isThumbnailRequired;
	}

	public void setIsThumbnailRequired(Boolean isThumbnailRequired) {
		this.isThumbnailRequired = isThumbnailRequired;
	}

    public String getSubjectUserPath() {
        return subjectUserPath;
    }

    public void setSubjectUserPath(String subjectUserPath) {
        this.subjectUserPath = subjectUserPath;
    }

    public String getPrimaryKeyDBDataType() {
        return primaryKeyDBDataType;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getParentMtPE() {
        return parentMtPE;
    }

    public void setParentMtPE(String parentMtPE) {
        this.parentMtPE = parentMtPE;
    }

    public String getIntermediateTableName() {
        return intermediateTableName;
    }

    public void setIntermediateTableName(String intermediateTableName) {
        this.intermediateTableName = intermediateTableName;
    }

    public String getParentIntermediateTableName() {
        return parentIntermediateTableName;
    }

    public void setParentIntermediateTableName(String parentIntermediateTableName) {
        this.parentIntermediateTableName = parentIntermediateTableName;
    }

    public void setPrimaryKeyDBDataType(String primaryKeyDBDataType) {
        this.primaryKeyDBDataType = primaryKeyDBDataType;
    }

    public String getRowResolvedTable() {
        return rowResolvedTable;
    }

    public void setRowResolvedTable(String rowResolvedTable) {
        this.rowResolvedTable = rowResolvedTable;
    }

    public String getColResolvedTable() {
        return colResolvedTable;
    }

    public void setColResolvedTable(String colResolvedTable) {
        this.colResolvedTable = colResolvedTable;
    }

    public String getParentRowResolvedTable() {
        return parentRowResolvedTable;
    }

    public void setParentRowResolvedTable(String parentRowResolvedTable) {
        this.parentRowResolvedTable = parentRowResolvedTable;
    }

    public String getParentColResolvedTable() {
        return parentColResolvedTable;
    }

    public void setParentColResolvedTable(String parentColResolvedTable) {
        this.parentColResolvedTable = parentColResolvedTable;
    }

    public String getIntermediateMtPE() {
        return intermediateMtPE;
    }

    public void setIntermediateMtPE(String intermediateMtPE) {
        this.intermediateMtPE = intermediateMtPE;
    }

    public String getParentIntermediateMtPE() {
        return parentIntermediateMtPE;
    }

    public void setParentIntermediateMtPE(String parentIntermediateMtPE) {
        this.parentIntermediateMtPE = parentIntermediateMtPE;
    }

    public String getRowResolvedMtPE() {
        return rowResolvedMtPE;
    }

    public void setRowResolvedMtPE(String rowResolvedMtPE) {
        this.rowResolvedMtPE = rowResolvedMtPE;
    }

    public String getColResolvedMtPE() {
        return colResolvedMtPE;
    }

    public void setColResolvedMtPE(String colResolvedMtPE) {
        this.colResolvedMtPE = colResolvedMtPE;
    }

    public String getParentRowResolvedMtPE() {
        return parentRowResolvedMtPE;
    }

    public void setParentRowResolvedMtPE(String parentRowResolvedMtPE) {
        this.parentRowResolvedMtPE = parentRowResolvedMtPE;
    }

    public String getParentColResolvedMtPE() {
        return parentColResolvedMtPE;
    }

    public void setParentColResolvedMtPE(String parentColResolvedMtPE) {
        this.parentColResolvedMtPE = parentColResolvedMtPE;
    }
}
