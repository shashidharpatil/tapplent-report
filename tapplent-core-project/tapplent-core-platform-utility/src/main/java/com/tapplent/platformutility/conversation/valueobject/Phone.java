package com.tapplent.platformutility.conversation.valueobject;

import com.tapplent.platformutility.layout.valueObject.IconVO;

/**
 * Created by Tapplent on 6/26/17.
 */
public class Phone {
    private String phoneNumberTxt;
    private IconVO phoneTypeIconCodeId;
    private String phoneTypeName;

    public String getPhoneNumberTxt() {
        return phoneNumberTxt;
    }

    public void setPhoneNumberTxt(String phoneNumberTxt) {
        this.phoneNumberTxt = phoneNumberTxt;
    }

    public IconVO getPhoneTypeIconCodeId() {
        return phoneTypeIconCodeId;
    }

    public void setPhoneTypeIconCodeId(IconVO phoneTypeIconCodeId) {
        this.phoneTypeIconCodeId = phoneTypeIconCodeId;
    }

    public String getPhoneTypeName() {
        return phoneTypeName;
    }

    public void setPhoneTypeName(String phoneTypeName) {
        this.phoneTypeName = phoneTypeName;
    }
}
