	package com.tapplent.platformutility.sql.context.builder;

import java.util.HashMap;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.data.insertEntity.InsertEntityPK;
import com.tapplent.platformutility.data.insertEntity.InsertEntityVersion;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.sql.context.model.InsertContextModel;
import com.tapplent.platformutility.sql.context.model.SQLContextModel;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;

public class InsertContextBuilder implements SQLContextBuilder {
	
	private static final Logger LOG = LogFactory.getLogger(InsertContextBuilder.class);

	@Override
	public SQLContextModel build(GlobalVariables globalVariables) {
		InsertContextModel contextModel = new InsertContextModel();
		Map<String, Object> doaValueMap = new HashMap<>();
		contextModel.setInsertValueMap(getColumnNameValueMap(contextModel, globalVariables, doaValueMap));
		contextModel.setDoaValueMap(doaValueMap);
		contextModel.setEntityName(globalVariables.entityMetadataVOX.getDbTableName());
		return contextModel;
	}

	@Override
	public SQLContextModel buildDefault(GlobalVariables globalVariables) {
		return null;
//		InsertContextModel contextModel = new InsertContextModel();
//		Map<String, Object> doaValueMap = new HashMap<>();
//		contextModel.setInsertValueMap(getColumnNameValueMapDefault(contextModel, globalVariables, doaValueMap));
//		contextModel.setDoaValueMap(doaValueMap);
//		contextModel.setEntityName(globalVariables.entityMetadataVOX.getDbTableName());
//		globalVariables.model = contextModel;
//		return contextModel;
	}

//	private Map<String,Object> getColumnNameValueMapDefault(InsertContextModel contextModel, GlobalVariables globalVariables, Map<String, Object> doaValueMap) {
//		Map<String, Object> data = globalVariables.insertData.getData();
//		Set<Entry<String, Object>> entrySet = data.entrySet();
//		Map<String, Object> columnValueMap = new HashMap();
//		Map<String, Map<String, Object>> containerBlobMap = new HashMap<>();
//		buildContainerBlobMap(containerBlobMap, entrySet, globalVariables);
//		for (Entry<String, Object> entry : entrySet) {
//			String propertyName = entry.getKey();
//			Object value = entry.getValue();
//			EntityAttributeMetadata attribute = globalVariables.entityMetadataVOX.getAttributeByName(propertyName);
//			if(attribute.getContainerBlobDbColumnName() == null){
//				if(attribute.isGlocalizedFlag())//confirm
//					columnValueMap.put(attribute.getDbColumnName().trim(), value != null ? value : "");
//				else columnValueMap.put(attribute.getDbColumnName().trim(), value != null ? value.toString() : "");
//			}
//			else {
//				addContainerBlobInHierarchy(columnValueMap, doaValueMap, attribute, containerBlobMap, globalVariables);
//			}
//		}
//		constructModelDefault(contextModel, columnValueMap, globalVariables);
//		return columnValueMap;
//	}
//
//	private void constructModelDefault(InsertContextModel contextModel, Map<String, Object> columnValueMap, GlobalVariables globalVariables) {
//		String personName = TenantContextHolder.getUserContext().getPersonName();
//		String personId = TenantContextHolder.getUserContext().getPersonId();
//		Map<String, Object> data = globalVariables.insertData.getData();
//		EntityAttributeMetadata primaryKeyAttribute = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
//		String primaryKeyValue = (String) data.get(primaryKeyAttribute.getDoAttributeCode());
//		Map<String,Object> underlyingRecord = globalVariables.underlyingRecord;
//		if(underlyingRecord == null || underlyingRecord.isEmpty()){
//			globalVariables.isUpdate = false;
//			primaryKeyValue = (String) Util.remove0x(Common.getUUID());
//		}
//		else{
//			globalVariables.isUpdate = true;
//		}
//		InsertEntityPK entityPK = new InsertEntityPK(primaryKeyAttribute.getDbColumnName(), primaryKeyAttribute.getDoAttributeCode(), primaryKeyValue);
//		contextModel.setInsertEntityPK(entityPK);
//		columnValueMap.put(contextModel.getInsertEntityPK().getColumn().trim(), contextModel.getInsertEntityPK().getValue());
//		Map<String, EntityAttributeMetadata> standardFields = globalVariables.entityMetadataVOX.getStandardColumnFields();
//		if(!globalVariables.isUpdate) {
//			if(standardFields.containsKey("CREATED_BY_PERSON_FULLNAME_TXT"))
//				columnValueMap.put("CREATED_BY_PERSON_FULLNAME_TXT", personName);
//			if(standardFields.containsKey("CREATED_DATETIME"))
//				columnValueMap.put("CREATED_DATETIME", globalVariables.currentTimestamp);
//			if(standardFields.containsKey("CREATEDBY_PERSON_FK_ID"))
//				columnValueMap.put("CREATEDBY_PERSON_FK_ID", personId);
//
//		}
//		if(standardFields.containsKey("IS_DELETED"))
//			columnValueMap.putIfAbsent("IS_DELETED", "FALSE");
//		if(standardFields.containsKey("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT"))
//			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT", personName);
//		if(standardFields.containsKey("LAST_MODIFIEDBY_PERSON_FK_ID"))
//			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FK_ID", personId);
//		if(standardFields.containsKey("LAST_MODIFIED_DATETIME"))
//			columnValueMap.put("LAST_MODIFIED_DATETIME", globalVariables.currentTimestamp);
//		if(standardFields.containsKey("LOGGED_IN_PERSON_FK_ID"))
//			columnValueMap.put("LOGGED_IN_PERSON_FK_ID", TenantContextHolder.getUserContext().getPersonId());
//		if(standardFields.containsKey("LAST_MODIFIED_DEVICE_TYPE_CODE_FK_ID"))
//			columnValueMap.put("LAST_MODIFIED_DEVICE_TYPE_CODE_FK_ID", TenantContextHolder.getUserContext().getDeviceType());
//		updateGlobalInsertData(entityPK, null, globalVariables);
//	}

	private Map<String, Object> getColumnNameValueMap(InsertContextModel contextModel, GlobalVariables globalVariables, Map<String, Object> doaValueMap) {
		Map<String, Object> data = globalVariables.insertData.getData();
		Set<Entry<String, Object>> entrySet = data.entrySet();
		Map<String, Object> columnValueMap = new HashMap();
		Map<String, Map<String, Object>> containerBlobMap = new HashMap<>();
		buildContainerBlobMap(containerBlobMap, entrySet, globalVariables);
		for (Entry<String, Object> entry : entrySet) {
			String propertyName = entry.getKey();
			Object value = entry.getValue();
			EntityAttributeMetadata attribute = globalVariables.entityMetadataVOX.getAttributeByName(propertyName);
			if(attribute.getContainerBlobDbColumnName() == null){
				if(attribute.isGlocalizedFlag()) {//confirm
					columnValueMap.put(attribute.getDbColumnName().trim(), value != null ? value : "");
					doaValueMap.put(attribute.getDoAttributeCode().trim(), value != null ? value : "");
				}
				else {
					if (attribute.getDbDataTypeCode().equals("T_BLOB")){
						columnValueMap.put(attribute.getDbColumnName().trim(), value != null ? value : "");
						doaValueMap.put(attribute.getDoAttributeCode().trim(), value != null ? value : "");
					}
					else {
						columnValueMap.put(attribute.getDbColumnName().trim(), value != null ? value.toString() : "");
						doaValueMap.put(attribute.getDoAttributeCode().trim(), value != null ? value.toString() : "");
					}
				}
			}
			else {
				addContainerBlobInHierarchy(columnValueMap, doaValueMap, attribute, containerBlobMap, globalVariables);
			}
		}
//		EntityAttributeMetadata primaryKeyAttribute = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
//		String primaryKeyValue = (String) data.get(primaryKeyAttribute.getDoAttributeCode());
//		EntityAttributeMetadata versionIdAttribute = globalVariables.entityMetadataVOX.getVersionIdAttribute();
//		String versionIdValue = (String) data.get(versionIdAttribute.getDoAttributeCode());
		constructModel(contextModel, columnValueMap, doaValueMap, globalVariables);
//		modifyModelForServerRule(contextModel, primaryKeyAttribute, primaryKeyValue, versionIdAttribute, versionIdValue, columnValueMap, globalVariables);
//		modifyModelForServerRuleProperty(columnValueMap, globalVariables);
//		columnValueMap.put(contextModel.getInsertEntityPK().getColumn().trim(), contextModel.getInsertEntityPK().getValue());
//		columnValueMap.put(contextModel.getInsertEntityVersion().getColumn().trim(), contextModel.getInsertEntityVersion().getValue());
//		columnValueMap.put("CREATED_BY_USER", Common.getCurrentUser());
//		columnValueMap.put("LAST_MODIFIED_USER", Common.getCurrentUser());
//		columnValueMap.put("TEMPLATE_ID", content.getTemplateId());
		return columnValueMap;
	}

	private void constructModel(InsertContextModel contextModel, Map<String, Object> columnValueMap, Map<String, Object> doaValueMap, GlobalVariables globalVariables) {
        /*
		 *																				/-> AUTO_SAVE -> SAVE_STATE = DRAFT
		 *															/-> ADD_NEW -> INSERT
		 * 					(null) PK or Version or both don't exist. 					\-> SUBMIT -> SAVE_STATE = SAVED
		 * 					/										\-> EDIT -> ERROR!
		 * 				   /										 \-> EDIT_VERSION -> ERROR!
		 * underlyingRecord															 /-> AUTO_SAVE -> SAVE_SATE = DRAFT
		 * 				   \									/-> ADD_NEW -> UPDATE
		 * 					\								   /					 \-> SUBMIT -> SAVE_STATE = SAVED
		 * 					\								  /
		 * 					(not null) PK and Version both exist.					 					/-> AUTO_SAVE -> SAVE_STATE = DRAFT
		 * 														\ \		/-> SAVE_STATE = draft --> UPDATE
		 * 														\ \-> EDIT								\-> SUBMIT -> SAVE_STATE = SAVED
		 * 														\ 		\									/-> AUTO_SAVE -> INSERT NEW VERSION(SAVE_STATE=DRAFT)
		 * 														\		\								/->
		 * 														\		\->	SAVE_STATE = SAVED --> NO WF	\-> SUBMIT -> INSERT NEW VERSION (SAVE_STATE = SAVED)
		 * 														\							|
		 * 														\							|
		 *														\							WF -> UPDATE -> AUTO_SAVE -> SAVE_STATE = SAVED (Validate first! special case.) what if somebody tries to change EFFECTIVE_DATETIME? Lets him change.
		 *														\										\-> SUBMIT -> SAVE_STATE = SAVED
		 *														\
		 *														\
		 *														\							 					/-> AUTO_SAVE -> SAVE_STATE = DRAFT
		 *														\				  /-SAVE_STATE = draft --> UPDATE
		 *														\				 /								\-> SUBMIT -> SAVE_STATE = SAVED
		 *														\				/
		 * 														\-> EDIT_VERSION
		 * 																		\
		 * 																		 \-> SAVE_STATE = SAVED -> UPDATE -> AUTO_SAVE -> SAVE_STATE = SAVED (Validate first! special case.)
		 *																											\-> SUBMIT -> SAVE_STATE = SAVED
		 *
		 *
		 **
		 *
		 ** New Version of code:
		 *																				/-> AUTO_SAVE -> SAVE_STATE = DRAFT
		 * 					(null) PK AND Version BOTH don't exist. -> ADD_EDIT -> INSERT
		 * 					/														\-> SUBMIT -> SAVE_STATE = SAVED
		 * 				   /
		 * underlyingRecord																		/-> AUTO_SAVE -> SAVE_STATE = DRAFT
		 * 				\   \										/--> InLine = true --> UPDATE
		 * 				\	\										/							\-> SUBMIT -> SAVE_STATE = SAVED
		 * 				\	\										/
		 * 				\	\ 									 	/				 					/-> AUTO_SAVE -> SAVE_STATE = DRAFT
		 * 				\	\									 	/	/-> SAVE_STATE = draft --> UPDATE
		 * 				\	-(not null) PK & version both exists ->ADD_EDIT								\-> SUBMIT -> SAVE_STATE = SAVED
		 * 				\										 		\									/-> AUTO_SAVE -> INSERT NEW VERSION(SAVE_STATE=DRAFT)
		 * 				\												\								/->
		 * 				\												\->	SAVE_STATE = SAVED --> NO WF	\-> SUBMIT -> INSERT NEW VERSION (SAVE_STATE = SAVED)
		 * 				\																	|
		 * 				\																	|
		 *				\																	WF -> UPDATE -> AUTO_SAVE -> SAVE_STATE = SAVED (Validate first! special case.) what if somebody tries to change EFFECTIVE_DATETIME? Lets him change.
		 *				\																				\-> SUBMIT -> SAVE_STATE = SAVED
		 *				\
		 *				\
		 *				\																	 					/-> AUTO_SAVE -> SAVE_STATE = DRAFT
		 *				\														  /-SAVE_STATE = draft --> INSERT
		 *				\														 /								\-> SUBMIT -> SAVE_STATE = SAVED
		 *				\														/
		 * 				------(null) PK exist but not version---------> ADD_EDIT
		 * 																		\
		 * 																		 \-> SAVE_STATE = SAVED -> INSERT -> AUTO_SAVE -> SAVE_STATE = SAVED (Validate first! special case.)
		 *																											\-> SUBMIT -> SAVE_STATE = SAVED
		 *
		 *
		 *
		 * (underlyingRecord = null && EDIT) --> ERROR would be handled in valueobject, so no need to check here.
		 */
        String presonName = TenantContextHolder.getUserContext().getPersonName();
        String personId = TenantContextHolder.getUserContext().getPersonId();
        Map<String, Object> data = globalVariables.insertData.getData();
	    EntityAttributeMetadata primaryKeyAttribute = globalVariables.entityMetadataVOX.getFunctionalPrimaryKeyAttribute();
        String primaryKeyValue = (String) data.get(primaryKeyAttribute.getDoAttributeCode());
        EntityAttributeMetadata versionIdAttribute = globalVariables.entityMetadataVOX.getVersoinIdAttribute();
        String versionIdValue = (String) data.get(versionIdAttribute.getDoAttributeCode());
        Map<String,Object> underlyingRecord = globalVariables.underlyingRecord;
        String actionCode = globalVariables.actionCode;
//        if(underlyingRecord != null && !underlyingRecord.isEmpty()){
//            String saveSate = underlyingRecord.get("SAVE_STATE_CODE_FK_ID").toString();
//            String wfTxnId = globalVariables.workflowDetails.workflowTxnId;
//            String wfStatus = globalVariables.workflowDetails.workflowStatus;
//            if(actionCode.equals("ADD_EDIT")){
//                if(!globalVariables.isSubmit){
//                    columnValueMap.put("SAVE_STATE_CODE_FK_ID", "DRAFT");
//                }else {
//                    columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
//                }
//                globalVariables.isUpdate = true;
//            }else if(previousActionCode.equals("EDIT")){
//                if(saveSate.equals("DRAFT")){
//                    if(currentActionCode.equals("SUBMIT")){
//                        columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
//                    }
//                globalVariables.isUpdate = true;
//                }else if(saveSate.equals("SAVED")){
//                    if(wfTxnId!=null && wfStatus!=null && wfStatus.equals("IN_PROGRESS")){
//                        globalVariables.isUpdate = true;
//                        if(currentActionCode.equals("AUTO_SAVE")) {
//                            globalVariables.validationRequired = true;
//                        }
//                    }else{//no WF
//                        if(currentActionCode.equals("AUTO_SAVE")){
//                            columnValueMap.put("SAVE_STATE_CODE_FK_ID", "DRAFT");
//                        }else if(currentActionCode.equals("SUBMIT")) {
//                            columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
//                        }
//                        versionIdValue = Common.getUUID();
//                        globalVariables.isUpdate = false;
//                    }
//
//                }
//            }else if(previousActionCode.equals("EDIT_VERSION")){
//                if(saveSate.equals("DRAFT")) {
//                    if (currentActionCode.equals("SUBMIT")) {
//                        columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
//                    }
//                    globalVariables.isUpdate = true;
//                }else if(saveSate.equals("SAVED")){
//                    globalVariables.isUpdate = true;
//                    if(currentActionCode.equals("AUTO_SAVE")) {
//                        globalVariables.validationRequired = true;
//                    }
//                }
//            }
//        }else {
//            if(previousActionCode.equals("ADD_NEW")){
//                if(currentActionCode.equals("AUTO_SAVE")){
//                    columnValueMap.put("SAVE_STATE_CODE_FK_ID", "DRAFT");
//                }else if(currentActionCode.equals("SUBMIT")) {
//                    columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
//                }
//                primaryKeyValue = Common.getUUID();
//                versionIdValue = Common.getUUID();
//                globalVariables.isUpdate = false;
//            }else {
//                ValidationError.addError("ACTION_NOT_VALID", "EDIT or EDIT_VERSION is not allowed as no underling record is present.");
//                //FIXME throw DataValidationException!
//            }
//        }
		if(globalVariables.entityMetadataVOX.isVersioned()) {
			if (underlyingRecord != null && !underlyingRecord.isEmpty()) {
				String saveSate = underlyingRecord.get(globalVariables.entityMetadataVOX.getSaveStateAttribute().getDoAttributeCode()).toString();
				String wfTxnId = globalVariables.workflowDetails.workflowTxnId;
				String wfStatus = globalVariables.workflowDetails.workflowStatus;
				if (!globalVariables.isInline) {
					if (saveSate.equals("DRAFT")) {
						if (globalVariables.isSubmit) {
							columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
						}
						globalVariables.isUpdate = true;
					} else if (saveSate.equals("SAVED")) {
						if (wfStatus == null || wfStatus.matches("COMPLETED|CANCELLED|WITHDRAWN")) {
							if (globalVariables.isSubmit) {
								globalVariables.validationRequired = true;
								columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
							} else {
								columnValueMap.put("SAVE_STATE_CODE_FK_ID", "DRAFT");//Should it be 'SAVED'?
							}
							versionIdValue = Common.getUUID();
						} else {
							globalVariables.isUpdate = true;
							globalVariables.isInline = true;
						}
					}
				} else if (globalVariables.isInline) {
					globalVariables.isUpdate = true;
					if (saveSate.equals("DRAFT")) {
						if (globalVariables.isSubmit) {
							columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
						}
					} else if (saveSate.equals("SAVED")) {
						globalVariables.validationRequired = true;
					}
				}
			} else {
				if (primaryKeyValue == null) {
					primaryKeyValue = Common.getUUID();
					versionIdValue = Common.getUUID();
					if (globalVariables.isSubmit) {
						columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
					} else {
						columnValueMap.put("SAVE_STATE_CODE_FK_ID", "DRAFT");
					}
				} else if (primaryKeyValue != null && !globalVariables.isInline) {
					versionIdValue = Common.getUUID();
					if (globalVariables.isSubmit) {
						columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
					} else {
						columnValueMap.put("SAVE_STATE_CODE_FK_ID", "DRAFT");
					}
				}
				else {
					globalVariables.isUpdate = true;
					if (globalVariables.isSubmit) {
						columnValueMap.put("SAVE_STATE_CODE_FK_ID", "SAVED");
					} else {
						columnValueMap.put("SAVE_STATE_CODE_FK_ID", "DRAFT");
					}
				}
			}
		}else{
			if((underlyingRecord == null || underlyingRecord.isEmpty()) && primaryKeyValue == null){
				globalVariables.isUpdate = false;
				primaryKeyValue = (String) Common.getUUID();
			}
			else{
				globalVariables.isUpdate = true;
			}
		}
        InsertEntityPK entityPK = new InsertEntityPK(primaryKeyAttribute.getDbColumnName(), primaryKeyAttribute.getDoAttributeCode(), primaryKeyValue);
        contextModel.setInsertEntityPK(entityPK);
        columnValueMap.put(contextModel.getInsertEntityPK().getColumn().trim(), contextModel.getInsertEntityPK().getValue());
		InsertEntityVersion versionId = null;
        if(globalVariables.entityMetadataVOX.isVersioned()) {
			versionId = new InsertEntityVersion(versionIdAttribute.getDbColumnName(), versionIdAttribute.getDoAttributeCode(), versionIdValue);
			contextModel.setInsertEntityVersion(versionId);
			columnValueMap.put(contextModel.getInsertEntityVersion().getColumn().trim(), contextModel.getInsertEntityVersion().getValue());
		}
        //FIXME Based on underlying record. Consider ETL also.

		Map<String, EntityAttributeMetadata> standardFields = globalVariables.entityMetadataVOX.getStandardDoaFields();
        Map<String, EntityAttributeMetadata> attributeMetadataMap = globalVariables.entityMetadataVOX.getAttributeNameMap();
        String doName = globalVariables.entityMetadataVOX.getDomainObjectCode();

		if(!globalVariables.isUpdate) {
			if(standardFields.containsKey(doName+".AuthoredByPersonFullName"))
				columnValueMap.put(attributeMetadataMap.get(doName+".AuthoredByPersonFullName").getDbColumnName(), presonName);
			if(standardFields.containsKey(doName+".AuthoredDateTime"))
				columnValueMap.put(attributeMetadataMap.get(doName+".AuthoredDateTime").getDbColumnName(), globalVariables.currentTimestampString);
			if(standardFields.containsKey(doName+".AuthoredByPersonID"))
				columnValueMap.put(attributeMetadataMap.get(doName+".AuthoredByPersonID").getDbColumnName(), personId);
		}
		if(standardFields.containsKey(doName+".ActiveState"))
			columnValueMap.putIfAbsent(attributeMetadataMap.get(doName+".ActiveState").getDbColumnName(), "ACTIVE");
		if(standardFields.containsKey(doName+".RecordState"))
			columnValueMap.putIfAbsent(attributeMetadataMap.get(doName+".RecordState").getDbColumnName(), "CURRENT");//FIXME Put if absent?
		if(standardFields.containsKey(doName+".Deleted"))
			columnValueMap.putIfAbsent(attributeMetadataMap.get(doName+".Deleted").getDbColumnName(), "FALSE");
		if(standardFields.containsKey(doName+".LastModifiedByPersonFullName"))
			columnValueMap.put(attributeMetadataMap.get(doName+".LastModifiedByPersonFullName").getDbColumnName(), presonName);
		if(standardFields.containsKey(doName+".LastModifiedByPersonID"))
			columnValueMap.put(attributeMetadataMap.get(doName+".LastModifiedByPersonID").getDbColumnName(), personId);
		if(standardFields.containsKey(doName+".LastModifiedDateTime")) {
			columnValueMap.put(attributeMetadataMap.get(doName+".LastModifiedDateTime").getDbColumnName(), globalVariables.currentTimestampString);//this replaces the lastmodified value coming from client. checkDirtyUpdate uses insertData not valueobject!
//			globalVariables.insertData.getData().put(globalVariables.entityMetadataVOX.getAttributeByColumnName("LAST_MODIFIED_DATETIME").getDoAttributeCode(), globalVariables.currentTimestampString);
		}
		if(standardFields.containsKey("LOGGED_IN_PERSON_FK_ID")) {
			columnValueMap.putIfAbsent("LOGGED_IN_PERSON_FK_ID", TenantContextHolder.getUserContext().getPersonId());
			doaValueMap.putIfAbsent(standardFields.get("LOGGED_IN_PERSON_FK_ID").getDoAttributeCode(), TenantContextHolder.getUserContext().getPersonId());
		}
		if(standardFields.containsKey("LAST_MODIFIED_DEVICE_TYPE_CODE_FK_ID"))
			columnValueMap.put("LAST_MODIFIED_DEVICE_TYPE_CODE_FK_ID", TenantContextHolder.getUserContext().getDeviceType());
        updateGlobalInsertData(entityPK, versionId, globalVariables);
	}

	private void addContainerBlobInHierarchy(Map<String, Object> columnValueMap, Map<String, Object> doaValueMap, EntityAttributeMetadata attributeMeta, Map<String, Map<String, Object>> containerBlobMap,
											 GlobalVariables globalVariables) {
		EntityAttributeMetadata continerMeta = globalVariables.entityMetadataVOX.getAttributeByColumnName(attributeMeta.getContainerBlobDbColumnName());
		if(continerMeta.getContainerBlobDbColumnName() != null){
			addContainerBlobInHierarchy(columnValueMap, doaValueMap, continerMeta, containerBlobMap, globalVariables);
		}
		if(containerBlobMap.containsKey(attributeMeta.getDbColumnName()))
			containerBlobMap.get(attributeMeta.getDbColumnName()).put(continerMeta.getDbColumnName(), containerBlobMap.get(continerMeta.getDbColumnName()));
		else {
			columnValueMap.put(continerMeta.getDbColumnName(), containerBlobMap.get(continerMeta.getDbColumnName()));
			doaValueMap.put(continerMeta.getDoAttributeCode(), containerBlobMap.get(continerMeta.getDoAttributeCode()));
		}
	}

	private void buildContainerBlobMap(Map<String, Map<String, Object>> containerBlobMap, Set<Entry<String, Object>> entrySet, GlobalVariables globalVariables) {
		for(Entry<String, Object> entry : entrySet){
			String propertyName = entry.getKey();
			Object value = entry.getValue();
			EntityAttributeMetadata attributeMeta = globalVariables.entityMetadataVOX.getAttributeByName(propertyName);
			if(attributeMeta.getContainerBlobDbColumnName() != null){
				if(containerBlobMap.containsKey(attributeMeta.getContainerBlobDbColumnName())){
					containerBlobMap.get(attributeMeta.getContainerBlobDbColumnName()).put(propertyName, value);
				}
				else{
					Map<String, Object> newBlobContainer = new HashMap<>();
					newBlobContainer.put(propertyName, value);
					containerBlobMap.put(attributeMeta.getContainerBlobDbColumnName(), newBlobContainer);
				}
			}
			
			
			
			
//			if(!attribute.getDbDataTypeCodeFkId().equals("T_BLOB")){
//				if(containerBlobMap.containsKey(attribute.getContainerBlobDoaName())){
//					containerBlobMap.get(attribute.getContainerBlobDoaName()).put(attribute.getDbColumnNameTxt().trim(), entry.getValue() != null ? entry.getValue().toString() : "");
//				}
//				else{
//					Map<String, Object> keyValuePairMap = new HashMap<>();
//					keyValuePairMap.put(attribute.getDbColumnNameTxt().trim(), entry.getValue() != null ? entry.getValue().toString() : "");
//					containerBlobMap.put(attribute.getContainerBlobDoaName(), keyValuePairMap);
//				}
//			}
//			else{
//				if(attribute.getGlocalizedFlag()){ //inner blob is a g11n blob.
//					Map<String, Object> keyValuePairMapInnerBlob = (Map<String, Object>) entry.getValue();
//					if(containerBlobMap.containsKey(attribute.getContainerBlobDoaName())){
//		//				Map<String, Object> keyValuePairMapInnerBlob = new HashMap<>();
//		//				for(Entry<String, Object> innerBlobKeyValue : innerBlobValues.entrySet()){
//		//					keyValuePairMapInnerBlob.put(entry.getKey(), entry.getValue());
//		//				}
//						containerBlobMap.get(attribute.getContainerBlobDoaName()).put(attribute.getDbColumnNameTxt().trim(), keyValuePairMapInnerBlob);
//					}
//					else{
//						Map<String, Object> keyValuePairMap = new HashMap<>();
//						keyValuePairMap.put(attribute.getDbColumnNameTxt().trim(), keyValuePairMapInnerBlob);
//						containerBlobMap.put(attribute.getContainerBlobDoaName(), keyValuePairMap);
//					}
//				}
//				else{//inner blob contain DOA Key-Value pairs.
//					
//				}
//			}
		}
		
	}

	private void modifyModelForServerRuleProperty(Map<String, Object> columnValueMap, GlobalVariables globalVariables) {//FIXME no use of separate method?
		/*
		Map<String, String> propertyMap = serverActionStep.getPropertyCodeValueMap();
		if(serverActionStep.getServerRuleCodeFkId().equals("INDATE_RECORD") && serverActionStep.getPropertyCodeValueMap().containsKey("INSERT_MODE")){ //INSERT_MODE ensures 'insert' operation ?
			columnValueMap.putIfAbsent("SAVE_STATE_CODE_FK_ID", propertyMap.get("SAVE_STATE").trim());
			columnValueMap.putIfAbsent("ACTIVE_STATE_CODE_FK_ID", propertyMap.get("ACTIVE_STATE").trim());
			columnValueMap.putIfAbsent("RECORD_STATE_CODE_FK_ID", propertyMap.get("RECORD_STATE").trim());
			columnValueMap.putIfAbsent("IS_DELETED", propertyMap.get("DELETE_STATE").trim());
		} else if (serverActionStep.getServerRuleCodeFkId().equals("UPDATE_RECORD")){
			if(propertyMap.containsKey("SAVE_STATE")) columnValueMap.putIfAbsent("SAVE_STATE_CODE_FK_ID", propertyMap.get("SAVE_STATE").trim());
			if(propertyMap.containsKey("ACTIVE_STATE")) columnValueMap.putIfAbsent("ACTIVE_STATE_CODE_FK_ID", propertyMap.get("ACTIVE_STATE").trim());
			if(propertyMap.containsKey("RECORD_STATE")) columnValueMap.putIfAbsent("RECORD_STATE_CODE_FK_ID", propertyMap.get("RECORD_STATE").trim());
			if(propertyMap.containsKey("DELETE_STATE")) columnValueMap.putIfAbsent("IS_DELETE", propertyMap.get("DELETE_STATE").trim());
		}
		*/
		columnValueMap.putIfAbsent("SAVE_STATE_CODE_FK_ID", "SAVED");
		columnValueMap.putIfAbsent("ACTIVE_STATE_CODE_FK_ID", globalVariables.underlyingRecord.get("ACTIVE_STATE_CODE_FK_ID"));
		columnValueMap.putIfAbsent("RECORD_STATE_CODE_FK_ID", "CURRENT");
		columnValueMap.putIfAbsent("IS_DELETED", globalVariables.underlyingRecord.get("IS_DELETE"));
	}

	private void modifyModelForServerRule(InsertContextModel contextModel, EntityAttributeMetadata primaryKeyAttribute,
			String primaryKeyValue, EntityAttributeMetadata versionIdAttribute, String versionIdValue, Map<String, Object> columnValueMap, GlobalVariables globalVariables) {
		String presonName = TenantContextHolder.getUserContext().getPersonName();
		String personId = TenantContextHolder.getUserContext().getPersonId();
		InsertEntityPK entityPK = null;
		InsertEntityVersion versionId = null;
		Map<String, Object> underlyingRecord = null;
		if(globalVariables.relatedUnderlyingRecord != null)
			underlyingRecord = globalVariables.relatedUnderlyingRecord.get("CURRENT");
//
//		if(serverActionStep.getServerRuleCodeFkId().equals("INDATE_RECORD") && serverActionStep.getPropertyCodeValueMap().containsKey("INSERT_MODE")){
//        	if(serverActionStep.getPropertyCodeValueMap().get("INSERT_MODE").equals("NEW_RECORD")){
//        		if(underlyingRecord != null && underlyingRecord.containsKey("SAVE_STATE_CODE_FK_ID") &&
//        				underlyingRecord.get("SAVE_STATE_CODE_FK_ID").equals(serverActionStep.getPropertyCodeValueMap().get("UPDATE_CHECK_SAVE_STATE"))){//draft_mode UPDATE
//	        		entityPK = new InsertEntityPK(primaryKeyAttribute.getDbColumnName(), primaryKeyAttribute.getDoAttributeCode(), primaryKeyValue);
//	        		versionId = new InsertEntityVersion(versionIdAttribute.getDbColumnName(), versionIdAttribute.getDoAttributeCode(), versionIdValue);
//	        		contextModel.setInsertEntityPK(entityPK);
//	    			contextModel.setInsertEntityVersion(versionId);
//	    			updateGlobalInsertData(entityPK, versionId, globalVariables);
//	        		columnValueMap.put("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT", presonName);
//	    			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FK_ID", personId);
//        		}
//        		else{// !draft_mode INSERT
//        			if(primaryKeyValue == null) //if pk is a code instead of binary then it'll be provided in insert data.
//        				primaryKeyValue = Common.getUUID();
//	        		entityPK = new InsertEntityPK(primaryKeyAttribute.getDbColumnName(), primaryKeyAttribute.getDoAttributeCode(), primaryKeyValue);
//	        		contextModel.setInsertEntityPK(entityPK);
//	        		versionIdValue = Common.getUUID();
//	        		versionId = new InsertEntityVersion(versionIdAttribute.getDbColumnName(), versionIdAttribute.getDoAttributeCode(), versionIdValue);
//	    			contextModel.setInsertEntityVersion(versionId);
//	    			updateGlobalInsertData(entityPK, versionId, globalVariables);
//	    			columnValueMap.put("CREATED_BY_PERSON_FULLNAME_TXT", presonName);
//	    			columnValueMap.put("CREATEDBY_PERSON_FK_ID", personId);
//	    			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT", presonName);
//	    			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FK_ID", personId);
////	    			columnValueMap.put("REF_PROCESS_CODE_FK_ID", globalVariables.entityMetadataVOX.getMetaProcessCode());
//	    			columnValueMap.put("REF_BASE_TEMPLATE_CODE_FK_ID", globalVariables.entityMetadataVOX.getBaseTemplateId());
//        		}
//        	} else if(serverActionStep.getPropertyCodeValueMap().get("INSERT_MODE").equals("NEW_VERSION_RECORD")){
//        		if(primaryKeyValue == null){
////        			throw new NullPointerException("Primary Key can not be null for NEW_VERSION_RECORD.");
//        			LOG.error("Primary Key can not be null for NEW_VERSION_RECORD.");
//        		}
//        		else if(underlyingRecord != null && underlyingRecord.containsKey("SAVE_STATE_CODE_FK_ID") &&
//        				underlyingRecord.get("SAVE_STATE_CODE_FK_ID").equals(serverActionStep.getPropertyCodeValueMap().get("UPDATE_CHECK_SAVE_STATE"))){//draft_mode new version UPDATE
//        			entityPK = new InsertEntityPK(primaryKeyAttribute.getDbColumnName(), primaryKeyAttribute.getDoAttributeCode(), primaryKeyValue);
//					contextModel.setInsertEntityPK(entityPK);
//	        		versionId = new InsertEntityVersion(versionIdAttribute.getDbColumnName(), versionIdAttribute.getDoAttributeCode(), versionIdValue);
//	    			contextModel.setInsertEntityVersion(versionId);
//	    			updateGlobalInsertData(entityPK, versionId, globalVariables);
//	    			columnValueMap.put("CREATED_BY_PERSON_FULLNAME_TXT", presonName);
//	    			columnValueMap.put("CREATEDBY_PERSON_FK_ID", personId);
//	    			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT", presonName);
//	    			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FK_ID", personId);
////	    			columnValueMap.put("REF_PROCESS_CODE_FK_ID", globalVariables.entityMetadataVOX.getMetaProcessCode());
//	    			columnValueMap.put("REF_BASE_TEMPLATE_CODE_FK_ID", globalVariables.entityMetadataVOX.getBaseTemplateId());
////	    			columnValueMap.put("", ""),
//        		}
//        		else{// !draft mode new version INSERT
//        			versionIdValue = Common.getUUID();
//	        		entityPK = new InsertEntityPK(primaryKeyAttribute.getDbColumnName(), primaryKeyAttribute.getDoAttributeCode(), primaryKeyValue);
//					contextModel.setInsertEntityPK(entityPK);
//	        		versionId = new InsertEntityVersion(versionIdAttribute.getDbColumnName(), versionIdAttribute.getDoAttributeCode(), versionIdValue);
//	    			contextModel.setInsertEntityVersion(versionId);
//	    			updateGlobalInsertData(entityPK, versionId, globalVariables);
//	    			columnValueMap.put("CREATED_BY_PERSON_FULLNAME_TXT", presonName);
//	    			columnValueMap.put("CREATEDBY_PERSON_FK_ID", personId);
//	    			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT", presonName);
//	    			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FK_ID", personId);
////	    			columnValueMap.put("REF_PROCESS_CODE_FK_ID", globalVariables.entityMetadataVOX.getMetaProcessCode());
//	    			columnValueMap.put("REF_BASE_TEMPLATE_CODE_FK_ID", globalVariables.entityMetadataVOX.getBaseTemplateId());
//        		}
//        	}
//		} else if(serverActionStep.getServerRuleCodeFkId().equals("UPDATE_RECORD")){
//			if(primaryKeyValue == null || versionIdValue == null){
////				throw new NullPointerException("Primary Key or Version Id can not be null for UPDATE_RECORD.");
//				LOG.error("Primary Key or Version Id can not be null for UPDATE_RECORD.");
//			} else {
//				entityPK = new InsertEntityPK(primaryKeyAttribute.getDbColumnName(), primaryKeyAttribute.getDoAttributeCode(), primaryKeyValue);
//				contextModel.setInsertEntityPK(entityPK);
//				versionId = new InsertEntityVersion(versionIdAttribute.getDbColumnName(), versionIdAttribute.getDoAttributeCode(), versionIdValue);
//				contextModel.setInsertEntityVersion(versionId);
//				contextModel.setInsertEntityPK(entityPK);
//    			contextModel.setInsertEntityVersion(versionId);
//			}
//		}
//
		if(underlyingRecord == null){
//			if(primaryKeyValue == null) //if pk is a code instead of binary then it'll be provided in insert data.
			primaryKeyValue = (String) Util.remove0x(Common.getUUID());
			entityPK = new InsertEntityPK(primaryKeyAttribute.getDbColumnName(), primaryKeyAttribute.getDoAttributeCode(), primaryKeyValue);
			contextModel.setInsertEntityPK(entityPK);
			versionIdValue = (String) Util.remove0x(Common.getUUID());
			versionId = new InsertEntityVersion(versionIdAttribute.getDbColumnName(), versionIdAttribute.getDoAttributeCode(), versionIdValue);
			contextModel.setInsertEntityVersion(versionId);
			updateGlobalInsertData(entityPK, versionId, globalVariables);
			columnValueMap.put("CREATED_BY_PERSON_FULLNAME_TXT", presonName);
			columnValueMap.put("CREATEDBY_PERSON_FK_ID", personId);
			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FULLNAME_TXT", presonName);
			columnValueMap.put("LAST_MODIFIEDBY_PERSON_FK_ID", personId);
//	    			columnValueMap.put("REF_PROCESS_CODE_FK_ID", globalVariables.entityMetadataVOX.getMetaProcessCode());
//			columnValueMap.put("REF_BASE_TEMPLATE_CODE_FK_ID", globalVariables.entityMetadataVOX.getBaseTemplateId());
		}
	}

	private void updateGlobalInsertData(InsertEntityPK entityPK, InsertEntityVersion versionId, GlobalVariables globalVariables) {
		Map<String, Object> data = globalVariables.insertData.getData();
		data.put(entityPK.getDoaName(), entityPK.getValue());
		if(versionId != null)
			data.put(versionId.getDoaName(), versionId.getValue());
//		if(globalVariables.entityMetadataVOX.getStandardFields().contains("LAST_MODIFIED_DATETIME"))
//			data.put(globalVariables.entityMetadataVOX.getAttributeByColumnName("LAST_MODIFIED_DATETIME").getDoAttributeCode(), globalVariables.currentTimestampString);
	}

}
