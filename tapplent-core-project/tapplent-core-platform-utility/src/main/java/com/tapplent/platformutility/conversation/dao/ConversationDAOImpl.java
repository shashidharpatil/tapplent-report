package com.tapplent.platformutility.conversation.dao;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.conversation.valueobject.*;
import com.tapplent.platformutility.layout.valueObject.LayoutPermissionsVO;
import com.tapplent.platformutility.layout.valueObject.SocialStatusVO;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.beans.BeanUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Tapplent on 6/9/17.
 */
public class ConversationDAOImpl extends TapplentBaseDAO implements ConversationDAO  {

    private static final Logger LOG = LogFactory.getLogger(ConversationDAOImpl.class);

    @Override
    public List<FeedbackGroupVO> getFeedbackGroupList(String mtPECode, String objectId, int limit, int offset, String keyword) {
        List<FeedbackGroupVO> feedbackGroupVOS = new ArrayList<>();
        Connection con = null;
        PreparedStatement ps;
        String personId = TenantContextHolder.getUserContext().getPersonId();
        StringBuilder sql = new StringBuilder("SELECT DISTINCT(FDBK_GRP_PK_ID), FDBK_GRP_NAME_G11N_BIG_TXT, FDBK_GRP_IMAGEID, FDBK_MTPE_CODE_FK_ID, FDBK_OBJECT_ID_FK_ID, IS_MBR_NTFN_MUTED, IS_FDBK_GRP_MBR_ADMIN, IS_BILATERAL_CONV, FDBK_GRP_MBR_PK_ID " +
                " FROM T_PRN_EU_FEEDBK_GRP FG LEFT JOIN T_PRN_EU_FEEDBK F ON FG.FDBK_GRP_PK_ID = F.FDBK_GRP_DEP_FK_ID \n" +
                " INNER JOIN T_PRN_EU_FEEDBK_GRP_MBR GM ON FG.FDBK_GRP_PK_ID = GM.FDBK_GRP_DEP_FK_ID \n" +
                " INNER JOIN T_PRN_EU_PERSON P ON P.PERSON_PK_ID = GM.FDBK_GRP_MBR_PERSON_FK_ID WHERE FDBK_MTPE_CODE_FK_ID = ?\n" +
                " AND FDBK_OBJECT_ID_FK_ID = "+objectId+" AND GM.FDBK_GRP_MBR_PERSON_FK_ID = "+personId+" AND FG.IS_DELETED = FALSE AND GM.IS_MBR_LEFT = FALSE AND GM.IS_MBR_REMOVED = FALSE AND GM.IS_MBR_ACCESS_REMOVED = FALSE \n");
        if (keyword != null){
            sql.append(" AND MATCH (FDBK_GRP_NAME_G11N_BIG_TXT) AGAINST ('"+keyword+"*' IN BOOLEAN MODE ) OR MATCH (FDBK_GRP_NAME_G11N_BIG_TXT) AGAINST ('"+keyword+"*' IN BOOLEAN MODE ) \n");
        }
        sql.append(" ORDER BY F.CREATED_DATETIME LIMIT ? OFFSET ?;");
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setString(1, mtPECode);
            ps.setInt(2, limit);
            ps.setInt(3, offset*limit);
            LOG.debug(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                FeedbackGroupVO feedbackGroupVO = new FeedbackGroupVO();
                feedbackGroupVO.setFeedbackGroupPkId(Util.convertByteToString(rs.getBytes("FDBK_GRP_PK_ID")));
                feedbackGroupVO.setFeedbackGroupName(rs.getString("FDBK_GRP_NAME_G11N_BIG_TXT"));
                feedbackGroupVO.setFeedbackGroupImageId(Util.convertByteToString(rs.getBytes("FDBK_GRP_IMAGEID")));
                if (feedbackGroupVO.getFeedbackGroupImageId() != null)
                    feedbackGroupVO.setFeedbackGroupImageUrl(Util.getURLforS3(feedbackGroupVO.getFeedbackGroupImageId(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
                feedbackGroupVO.setmTPECode(rs.getString("FDBK_MTPE_CODE_FK_ID"));
                feedbackGroupVO.setObjectId(Util.convertByteToString(rs.getBytes("FDBK_OBJECT_ID_FK_ID")));
                feedbackGroupVO.setBilateralConversation(rs.getBoolean("IS_BILATERAL_CONV"));
                feedbackGroupVO.setMemNtfnMuted(rs.getBoolean("IS_MBR_NTFN_MUTED"));
                feedbackGroupVO.setMemberAdmin(rs.getBoolean("IS_FDBK_GRP_MBR_ADMIN"));
                feedbackGroupVO.setLoggedinPersonFeedbackMemberId(Util.convertByteToString(rs.getBytes("FDBK_GRP_MBR_PK_ID")));
                feedbackGroupVOS.add(feedbackGroupVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feedbackGroupVOS;
    }

    @Override
    public void createFeedbackGroup(FeedbackGroupVO feedbackGroupVO) throws SQLException {
        StringBuilder SQL = new StringBuilder("INSERT INTO T_PRN_EU_FEEDBK_GRP (FDBK_GRP_PK_ID, FDBK_GRP_NAME_G11N_BIG_TXT, FDBK_GRP_IMAGEID, FDBK_MTPE_CODE_FK_ID, FDBK_OBJECT_ID_FK_ID, IS_BILATERAL_CONV, "+getStandardColumns()+") VALUE "+
                "("+ feedbackGroupVO.getFeedbackGroupPkId()+", '"+feedbackGroupVO.getFeedbackGroupName()+"', "+getIdValueToInsert((String)Util.remove0x(feedbackGroupVO.getFeedbackGroupImageId()))+", '"+feedbackGroupVO.getmTPECode()+"', "+getIdValueToInsert((String)Util.remove0x(feedbackGroupVO.getObjectId()))+", "+feedbackGroupVO.isBilateralConversation()+", "+getStandardColumnValues()+");");
        Connection con = null;
        PreparedStatement ps;
        try {
            con = getConnection();
            ps = con.prepareStatement(SQL.toString());
            LOG.debug(SQL);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
            throw e;
        }
    }

    private String getStringValueToInsert(String stringFromG11nMap) {
        return stringFromG11nMap==null?"NULL":"'"+stringFromG11nMap+"'";
    }

    private String getIdValueToInsert(String string) {
        return string==null?"NULL":"0x"+string;
    }

    @Override
    public void createFeedbackGroupMembers(List<GroupMemberVO> groupMemberVOS, String feedbackGroupPkId) {
        StringBuilder sql = new StringBuilder("INSERT INTO T_PRN_EU_FEEDBK_GRP_MBR (FDBK_GRP_MBR_PK_ID, FDBK_GRP_DEP_FK_ID, FDBK_GRP_MBR_PERSON_FK_ID, IS_FDBK_GRP_MBR_ADMIN, IS_MBR_NTFN_MUTED, IS_MBR_LEFT, IS_MBR_REMOVED, IS_MBR_ACCESS_REMOVED, "+getStandardColumns()+") VALUES ");
        for (GroupMemberVO groupMemberVO : groupMemberVOS){
            sql.append("("+Common.getUUID()+", "+feedbackGroupPkId+", ");
            sql.append(groupMemberVO.getPersonId()+", ");
            sql.append(String.valueOf(groupMemberVO.isMemberAdmin()+", "));
            sql.append(String.valueOf(groupMemberVO.isMemberNtfnMuted()+", "));
            sql.append(String.valueOf(groupMemberVO.isMemberLeft()+", "));
            sql.append(String.valueOf(groupMemberVO.isMemberRemoved()+", "));
            sql.append(String.valueOf(groupMemberVO.isMemberAccessRemoved()+", "));
            sql.append(getStandardColumnValues());
            sql.append("), ");
        }
        sql.replace(sql.length()-2, sql.length(), ";");
        Connection con = null;
        PreparedStatement ps;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            LOG.debug(sql);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public List<PersonDetailsVO> getFeedbackContactDetails(String mTPECode, String objectId, int limit, int offset, String keyword, String feedbackGroupId) {
        StringBuilder sql = new StringBuilder("SELECT PERSON_PK_ID, NAME_INITIALS_TXT, PHOTO_IMAGEID, LEGAL_FULL_NAME_G11N_BIG_TXT, JR.INTERNAL_JOB_TITLE_G11N_BIG_TXT, L.CITY_CSPEC_G11N_BIG_TXT FROM T_PRN_EU_PERSON P\n" +
                "INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT JA ON P.PERSON_PK_ID = JA.PERSON_DEP_FK_ID\n" +
                "INNER JOIN T_ORG_ADM_ORG_JOB_PAY_GRADE_MAP PG ON JA.ORG_JOB_PAY_GRADE_MAP_FK_ID = PG.ORG_JOB_PAY_GRADE_MAP_PK_ID AND IS_PRIMARY = TRUE\n" +
                "INNER JOIN T_ORG_ADM_ORG_JOB_PAY_GRADE_BAND_MAP PGB ON PGB.ORG_JOB_PAY_GRADE_BAND_MAP_PK_ID = PG.ORG_JOB_PAY_GRADE_BAND_MAP_FK_ID\n" +
                "INNER JOIN T_ORG_ADM_ORG_JOB_MAP OJ ON PGB.ORG_JOB_MAP_FK_ID = OJ.ORG_JOB_MAP_PK_ID\n" +
                "INNER JOIN T_ORG_EU_ORG_LOC_MAP OL ON OJ.ORG_LOC_FK_ID = OL.ORG_LOC_PK_ID\n" +
                "INNER JOIN T_ORG_EU_LOCATION L ON OL.LOCATION_FK_ID = L.LOCATION_PK_ID\n" +
//                "LEFT JOIN T_PRN_EU_PERSON_PREFERENCES PP ON PP.PERSON_DEP_FK_ID = P.PERSON_PK_ID\n" +
                "INNER JOIN T_JOB_ADM_JOB_ROLE JR ON JR.JOB_ROLE_PK_ID = OJ.JOB_ROLE_FK_ID ");
        if (keyword != null){
            sql.append(" WHERE MATCH (LEGAL_FULL_NAME_G11N_BIG_TXT) AGAINST ('"+keyword+"*' IN BOOLEAN MODE ) \n");
        }
        if (feedbackGroupId != null){
            sql.append(" AND PERSON_PK_ID NOT IN (SELECT FDBK_GRP_MBR_PERSON_FK_ID FROM T_PRN_EU_FEEDBK_GRP_MBR WHERE FDBK_GRP_DEP_FK_ID = ? )");
        }
        sql.append(" AND PERSON_PK_ID <> ? ORDER BY NAME_INITIALS_TXT LIMIT ? OFFSET ?;");
        List<PersonDetailsVO> personDetailsVOs = new ArrayList<>();
        Connection con ;
        PreparedStatement ps = null;
        String personId = TenantContextHolder.getUserContext().getPersonId();
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            int i = 1;
            if (feedbackGroupId != null)
                ps.setBytes(i++, Util.convertStringIdToByteId(feedbackGroupId));
            ps.setBytes(i++, Util.convertStringIdToByteId(personId));
            ps.setInt(i++, limit);
            ps.setInt(i, offset);
            LOG.debug(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PersonDetailsVO personDetailsVO = new PersonDetailsVO();
                personDetailsVO.setPersonId(Util.convertByteToString(rs.getBytes("PERSON_PK_ID")));
                personDetailsVO.setPersonFullName(Util.getG11nValue(rs.getString("LEGAL_FULL_NAME_G11N_BIG_TXT"), personId));
                personDetailsVO.setLocation(Util.getG11nValue(rs.getString("L.CITY_CSPEC_G11N_BIG_TXT"), personId));
                personDetailsVO.setPersonDesignation(Util.getG11nValue(rs.getString("JR.INTERNAL_JOB_TITLE_G11N_BIG_TXT"), personId));
                personDetailsVO.setPersonPhoto(Util.convertByteToString(rs.getBytes("PHOTO_IMAGEID")));
                personDetailsVO.setNameInitials(rs.getString("NAME_INITIALS_TXT"));
                SocialStatusVO socialStatusVO = getSocialStatusByPerson(personDetailsVO.getPersonId());
                personDetailsVO.setSocialStatus(socialStatusVO);
                List<Phone> personPhoneDetailList = getPhoneDetailsByPerson(personDetailsVO.getPersonId());
                personDetailsVO.setPhoneList(personPhoneDetailList);
                List<Email> personEmailDetailList = getEmailDetailByPerson(personDetailsVO.getPersonId());
                personDetailsVO.setEmailList(personEmailDetailList);
                personDetailsVOs.add(personDetailsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personDetailsVOs;
    }

    private SocialStatusVO getSocialStatusByPerson(String personId) {
        String sql = "SELECT PERSON_PK_ID, PP.SOCIAL_STATUS_CODE_FK_ID, SS.SOCIAL_STATUS_NAME_G11N_BIG_TXT, SS.SOCIAL_STATUS_ICON_CODE_FK_ID, SS.OVERRIDE_FONT_COLOUR_TXT FROM T_PRN_EU_PERSON P \n" +
                " LEFT JOIN T_PRN_EU_PERSON_PREFERENCES PP ON PP.PERSON_DEP_FK_ID = P.PERSON_PK_ID \n" +
                " LEFT JOIN T_PRN_LKP_SOCIAL_STATUS SS ON PP.SOCIAL_STATUS_CODE_FK_ID = SS.SOCIAL_STATUS_CODE_PK_ID WHERE PERSON_PK_ID = ?;";
        Connection con ;
        PreparedStatement ps = null;
        SocialStatusVO socialStatusVO = null;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setBytes(1, Util.convertStringIdToByteId(personId));
            LOG.debug(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                socialStatusVO = new SocialStatusVO();
                socialStatusVO.setSocialStatusCodePkId(rs.getString("PP.SOCIAL_STATUS_CODE_FK_ID"));
                socialStatusVO.setSocialStatusNameG11nBigTxt(Util.getG11nValue(rs.getString("SS.SOCIAL_STATUS_NAME_G11N_BIG_TXT"), personId));
                socialStatusVO.setSocialStatusIconCodeFKId(rs.getString("SS.SOCIAL_STATUS_ICON_CODE_FK_ID"));
                socialStatusVO.setOverrideFontColourTxt(rs.getString("SS.OVERRIDE_FONT_COLOUR_TXT"));
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return socialStatusVO;
    }

    @Override
    public String writeFeedbackMessage(FeedbackMessageVO feedbackMessageVO) throws SQLException {
        StringBuilder sql;
        String feedbackPkId = feedbackMessageVO.getFeedbackPkId();
        String feedbackGroupId = feedbackMessageVO.getFeedbackGroupId();
        String message = feedbackMessageVO.getMessage();
        String messageType = feedbackMessageVO.getMessageType();
        String messageAttach = feedbackMessageVO.getMessageAttachId();
        String feedbackParentMsgId = feedbackMessageVO.getFeedbackParentMessageId();
        boolean messageEdited = feedbackMessageVO.isMessageEdited();
        boolean deleted = feedbackMessageVO.isDeleted();
        if (feedbackPkId != null){
            sql = new StringBuilder("UPDATE T_PRN_EU_FEEDBK SET MSG_BIG_TXT = ?, MSG_ATTACH = ?, IS_MSG_EDITED = TRUE, MSG_TYPE_TXT = ?, FDBK_SLF_PRNT_MSG_FK_ID = ?, IS_DELETED = ?, LAST_MODIFIED_DATETIME = NOW() WHERE PRN_FEEDBACK_PK_ID = ?;");
        }
        else {
            sql = new StringBuilder("INSERT INTO T_PRN_EU_FEEDBK (PRN_FEEDBACK_PK_ID, FDBK_GRP_DEP_FK_ID, MSG_BIG_TXT, MSG_ATTACH, IS_MSG_EDITED, MSG_TYPE_TXT, FDBK_SLF_PRNT_MSG_FK_ID, "+getStandardColumns()+" )\n");
            sql.append("VALUE (?, ?, ?, ?, ?, ?, ?, "+getStandardColumnValues()+");");
        }
        Connection con ;
        PreparedStatement ps = null;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            if (feedbackPkId != null){
                ps.setString(1, message);
                ps.setBytes(2, Util.convertStringIdToByteId(messageAttach));
//                ps.setBoolean(3, messageEdited);
                ps.setString(3, messageType);
                ps.setBytes(4, Util.convertStringIdToByteId(feedbackParentMsgId));
                ps.setBoolean(5, deleted);
                ps.setBytes(6, Util.convertStringIdToByteId(feedbackPkId));
            }else {
                feedbackPkId = Common.getUUID();
                ps.setBytes(1, Util.convertStringIdToByteId(feedbackPkId));
                ps.setBytes(2, Util.convertStringIdToByteId(feedbackGroupId));
                ps.setString(3, message);
                ps.setBytes(4, Util.convertStringIdToByteId(messageAttach));
                ps.setBoolean(5, messageEdited);
                ps.setString(6, messageType);
                ps.setBytes(7, Util.convertStringIdToByteId(feedbackParentMsgId));
                populateFeedbackReadStatusForMessageWrite(feedbackPkId, feedbackGroupId);
            }
            LOG.debug(sql);
            ps.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
        return feedbackPkId;
    }

    private void populateFeedbackReadStatusForMessageWrite(String feedbackPkId, String feedbackGroupId) {
        String sql = "INSERT INTO T_PRN_IEU_FEEDBK_READ_STATUS (FDBK_READ_STATUS_PK_ID, FDBK_GRP_FK_ID, FDBK_DEP_FK_ID, FDBK_RD_PERSON_FK_ID, IS_DELETED, CREATEDBY_PERSON_FK_ID, CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT, CREATED_DATETIME)\n" +
                "  VALUE (ordered_uuid(UUID()), ?, ?, ?, FALSE, ?, ?, NOW());";
        String personId = TenantContextHolder.getUserContext().getPersonId();
        String personName = TenantContextHolder.getUserContext().getPersonName();
        Connection con ;
        PreparedStatement ps = null;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setBytes(1, Util.convertStringIdToByteId(feedbackGroupId));
            ps.setBytes(2, Util.convertStringIdToByteId(feedbackPkId));
            ps.setBytes(3, Util.convertStringIdToByteId(personId));
            ps.setBytes(4, Util.convertStringIdToByteId(personId));
            ps.setString(5, personName);
            LOG.debug(sql);
            ps.executeUpdate();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public List<FeedbackMessageVO> getFeedbackMessage(String feedbackGroupId, int limit, int offset) {
        String sql = "SELECT DISTINCT (PRN_FEEDBACK_PK_ID), F.FDBK_GRP_DEP_FK_ID, FDBK_GRP_NAME_G11N_BIG_TXT, MSG_BIG_TXT, FDBK_SLF_PRNT_MSG_FK_ID, MSG_ATTACH, IS_MSG_EDITED, MSG_TYPE_TXT, " +
                "F.CREATEDBY_PERSON_FK_ID, F.CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT, GM.IS_MBR_LEFT, GM.IS_MBR_REMOVED, GM.IS_MBR_ACCESS_REMOVED, BOOKMARK_LOG_PK_ID, B.LAST_MODIFIED_DATETIME, F.CREATED_DATETIME, F.LAST_MODIFIED_DATETIME " +
                "FROM T_PRN_EU_FEEDBK F LEFT JOIN T_PRN_EU_FEEDBK_GRP G ON G.FDBK_GRP_PK_ID = F.FDBK_GRP_DEP_FK_ID\n" +
                "INNER JOIN T_PRN_EU_FEEDBK_GRP_MBR GM ON G.FDBK_GRP_PK_ID = GM.FDBK_GRP_DEP_FK_ID\n" +
                "LEFT JOIN T_PFM_EU_BOOKMARK B ON F.PRN_FEEDBACK_PK_ID = B.DO_ROW_PRIMARY_KEY_FK_ID AND B.IS_DELETED = FALSE\n" +
                "WHERE GM.FDBK_GRP_DEP_FK_ID = ? AND GM.IS_DELETED = FALSE AND GM.IS_MBR_REMOVED = FALSE AND GM.IS_MBR_LEFT = FALSE AND GM.IS_MBR_ACCESS_REMOVED = FALSE AND F.IS_DELETED = FALSE ORDER BY F.CREATED_DATETIME DESC LIMIT ? OFFSET ?;";
        List<FeedbackMessageVO> feedbackMessageVOs = new ArrayList<>();
        String personId = TenantContextHolder.getUserContext().getPersonId();
        Connection con ;
        PreparedStatement ps = null;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql);
            LOG.debug(sql);
            ps.setBytes(1, Util.convertStringIdToByteId(feedbackGroupId));
            ps.setInt(2, limit);
            ps.setInt(3, offset*limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                FeedbackMessageVO feedbackMessageVO = new FeedbackMessageVO();
                feedbackMessageVO.setFeedbackPkId(Util.convertByteToString(rs.getBytes("PRN_FEEDBACK_PK_ID")));
                feedbackMessageVO.setFeedbackGroupId(Util.convertByteToString(rs.getBytes("F.FDBK_GRP_DEP_FK_ID")));
                feedbackMessageVO.setFeedbackGroupName(rs.getString("FDBK_GRP_NAME_G11N_BIG_TXT"));
                feedbackMessageVO.setMessage(rs.getString("MSG_BIG_TXT"));
                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_SLF_PRNT_MSG_FK_ID")));
                feedbackMessageVO.setMessageAttachId(Util.convertByteToString(rs.getBytes("MSG_ATTACH")));
                if (feedbackMessageVO.getMessageAttachId() != null){
                    feedbackMessageVO.setMessageAttachUrl(Util.getURLforS3(feedbackMessageVO.getMessageAttachId(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
                }
                feedbackMessageVO.setMessageEdited(rs.getBoolean("IS_MSG_EDITED"));
                feedbackMessageVO.setMessageType(rs.getString("MSG_TYPE_TXT"));
//                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_PARENT_MSG_FK_ID")));
                feedbackMessageVO.setCreatedByPersonId(Util.convertByteToString(rs.getBytes("F.CREATEDBY_PERSON_FK_ID")));
                feedbackMessageVO.setCreatedByPersonName(Util.getG11nValue(rs.getString("F.CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT"), personId));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_LEFT"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_REMOVED"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_ACCESS_REMOVED"));
                feedbackMessageVO.setCreatedDatetime(new String(Util.getStringTobeInserted("DATE_TIME",rs.getTimestamp("F.CREATED_DATETIME"))).replace("'", ""));
                feedbackMessageVO.setLastModifiedDatetime(new String(Util.getStringTobeInserted("DATE_TIME",rs.getTimestamp("F.LAST_MODIFIED_DATETIME"))).replace("'", ""));
                if (rs.getTimestamp("B.LAST_MODIFIED_DATETIME") != null) feedbackMessageVO.setBookmarkedLastModifiedDatetime(new String(Util.getStringTobeInserted("DATE_TIME",rs.getTimestamp("B.LAST_MODIFIED_DATETIME"))).replace("'", ""));
                if (feedbackMessageVO.getFeedbackParentMessageId() != null){
                    feedbackMessageVO.setParentFeedbackMessage(getParentFeedbackMessage(feedbackMessageVO.getFeedbackParentMessageId()));
                }
                feedbackMessageVO.setBookmarkId(Util.convertByteToString(rs.getBytes("BOOKMARK_LOG_PK_ID")));
                if (feedbackMessageVO.getBookmarkId() != null)
                    feedbackMessageVO.setBookmarked(true);
                feedbackMessageVOs.add(feedbackMessageVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feedbackMessageVOs;
    }

    @Override
    public void populateFeedbackReadStatus(String feedbackGroupId) {
        String personId = TenantContextHolder.getUserContext().getPersonId();
        String personName = TenantContextHolder.getUserContext().getPersonName();
        String sql = "INSERT INTO T_PRN_IEU_FEEDBK_READ_STATUS (FDBK_READ_STATUS_PK_ID, FDBK_GRP_FK_ID, FDBK_DEP_FK_ID, FDBK_RD_PERSON_FK_ID, IS_DELETED, CREATEDBY_PERSON_FK_ID, CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT, CREATED_DATETIME)\n" +
                "SELECT ordered_uuid(uuid()), FDBK_GRP_DEP_FK_ID, PRN_FEEDBACK_PK_ID, ?, FALSE, ?, 'user', NOW() FROM T_PRN_EU_FEEDBK WHERE FDBK_GRP_DEP_FK_ID = ? AND " +
                "PRN_FEEDBACK_PK_ID NOT IN (SELECT FDBK_DEP_FK_ID FROM T_PRN_IEU_FEEDBK_READ_STATUS WHERE FDBK_RD_PERSON_FK_ID = ? AND FDBK_GRP_FK_ID = ?);\n";
        Connection con = null;
        PreparedStatement ps;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql);
            ps.setBytes(1, Util.convertStringIdToByteId(personId));
            ps.setBytes(2, Util.convertStringIdToByteId(personId));
            ps.setBytes(3, Util.convertStringIdToByteId(feedbackGroupId));
            ps.setBytes(4, Util.convertStringIdToByteId(personId));
            ps.setBytes(5, Util.convertStringIdToByteId(feedbackGroupId));
            LOG.debug(sql);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public Map<String, Integer> getUnreadMessageCountByGroup(List<FeedbackGroupVO> feedbackGroupVOList) {
//        String sql = "SELECT FDBK_GRP_DEP_FK_ID, COUNT(PRN_FEEDBACK_PK_ID) FROM T_PRN_EU_FEEDBK WHERE NOT EXISTS (SELECT FDBK_DEP_FK_ID FROM T_PRN_IEU_FEEDBK_READ_STATUS WHERE FDBK_RD_PERSON_FK_ID = ?) GROUP BY FDBK_GRP_DEP_FK_ID;";
        String totalSql = "SELECT FDBK_GRP_DEP_FK_ID, COUNT(PRN_FEEDBACK_PK_ID) FROM T_PRN_EU_FEEDBK GROUP BY FDBK_GRP_DEP_FK_ID;";
        Connection con = null;
        PreparedStatement ps;
        String personId = TenantContextHolder.getUserContext().getPersonId();
        Map<String, Integer> groupIdToMsgCount = new HashMap<>();
        Map<String, Integer> groupIdToReadMsgCount = new HashMap<>();

        try {
            con = getConnection();
            ps = con.prepareStatement(totalSql);
            LOG.debug(totalSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                groupIdToMsgCount.put(Util.convertByteToString(rs.getBytes("FDBK_GRP_DEP_FK_ID")), rs.getInt("COUNT(PRN_FEEDBACK_PK_ID)"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        String unreadSql = "SELECT FDBK_GRP_FK_ID, COUNT(DISTINCT FDBK_DEP_FK_ID) FROM T_PRN_IEU_FEEDBK_READ_STATUS WHERE FDBK_RD_PERSON_FK_ID = ? GROUP BY FDBK_GRP_FK_ID;";
        try {
            con = getConnection();
            ps = con.prepareStatement(unreadSql);
            ps.setBytes(1, Util.convertStringIdToByteId(personId));
            LOG.debug(unreadSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                groupIdToReadMsgCount.put(Util.convertByteToString(rs.getBytes("FDBK_GRP_FK_ID")), rs.getInt("COUNT(DISTINCT FDBK_DEP_FK_ID)"));
            }
        }catch (SQLException e){
            e.printStackTrace();
        }

        Map<String, Integer> groupIdToUnreadMsgCount = new HashMap<>();
        for (Map.Entry<String, Integer> entry : groupIdToMsgCount.entrySet()){
            if (groupIdToReadMsgCount.get(entry.getKey()) != null){
                groupIdToUnreadMsgCount.put(entry.getKey(), entry.getValue() - groupIdToReadMsgCount.get(entry.getKey()));
            }
            else groupIdToUnreadMsgCount.put(entry.getKey(), entry.getValue());
        }

        return groupIdToUnreadMsgCount;
    }

    @Override
    public List<PersonDetailsVO> getFeedbackGroupMemberDetails(String feedbackGroupId) {
//        PP.PHONE_NUMBER_TXT, " +
//        "PT.PHONE_TYPE_ICON_CODE_FK_ID,PT.PHONE_TYPE_NAME_G11N_BIG_TXT, PE.EMAIL_ID_TXT, ET.EMAIL_TYPE_ICON_CODE_FK_ID, ET.EMAIL_TYPE_NAME_G11N_BIG_TXT,

        String sql = "SELECT PERSON_PK_ID, NAME_INITIALS_TXT, PHOTO_IMAGEID, LEGAL_FULL_NAME_G11N_BIG_TXT, JR.INTERNAL_JOB_TITLE_G11N_BIG_TXT, L.CITY_CSPEC_G11N_BIG_TXT, GM.FDBK_GRP_MBR_PK_ID, GM.IS_FDBK_GRP_MBR_ADMIN, GM.IS_MBR_NTFN_MUTED FROM T_PRN_EU_PERSON P\n" +
                "INNER JOIN T_PRN_EU_FEEDBK_GRP_MBR GM ON GM.FDBK_GRP_MBR_PERSON_FK_ID = P.PERSON_PK_ID\n" +
                "INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT JA ON P.PERSON_PK_ID = JA.PERSON_DEP_FK_ID\n" +
                "INNER JOIN T_ORG_ADM_ORG_JOB_PAY_GRADE_MAP PG ON JA.ORG_JOB_PAY_GRADE_MAP_FK_ID = PG.ORG_JOB_PAY_GRADE_MAP_PK_ID AND IS_PRIMARY = TRUE\n" +
                "INNER JOIN T_ORG_ADM_ORG_JOB_PAY_GRADE_BAND_MAP PGB ON PGB.ORG_JOB_PAY_GRADE_BAND_MAP_PK_ID = PG.ORG_JOB_PAY_GRADE_BAND_MAP_FK_ID\n" +
                "INNER JOIN T_ORG_ADM_ORG_JOB_MAP OJ ON PGB.ORG_JOB_MAP_FK_ID = OJ.ORG_JOB_MAP_PK_ID\n" +
                "INNER JOIN T_ORG_EU_ORG_LOC_MAP OL ON OJ.ORG_LOC_FK_ID = OL.ORG_LOC_PK_ID\n" +
                "INNER JOIN T_ORG_EU_LOCATION L ON OL.LOCATION_FK_ID = L.LOCATION_PK_ID\n" +
                "INNER JOIN T_PRN_EU_PERSON_PREFERENCES PP ON PP.PERSON_DEP_FK_ID = P.PERSON_PK_ID\n" +
                "INNER JOIN T_JOB_ADM_JOB_ROLE JR ON JR.JOB_ROLE_PK_ID = OJ.JOB_ROLE_FK_ID WHERE GM.FDBK_GRP_DEP_FK_ID = ?" +
                " AND GM.IS_MBR_LEFT = FALSE AND GM.IS_MBR_REMOVED = FALSE AND GM.IS_MBR_ACCESS_REMOVED = FALSE AND GM.IS_DELETED = FALSE;";
        List<PersonDetailsVO> personDetailsVOs = new ArrayList<>();
        Connection con ;
        PreparedStatement ps = null;
        String personId = TenantContextHolder.getUserContext().getPersonId();
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setBytes(1, Util.convertStringIdToByteId(feedbackGroupId));
            LOG.debug(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PersonDetailsVO personDetailsVO = new PersonDetailsVO();
                personDetailsVO.setPersonId(Util.convertByteToString(rs.getBytes("PERSON_PK_ID")));
                personDetailsVO.setFeedbackGroupMemberId(Util.convertByteToString(rs.getBytes("GM.FDBK_GRP_MBR_PK_ID")));
                personDetailsVO.setPersonFullName(Util.getG11nValue(rs.getString("LEGAL_FULL_NAME_G11N_BIG_TXT"), personId));
                personDetailsVO.setLocation(Util.getG11nValue(rs.getString("L.CITY_CSPEC_G11N_BIG_TXT"), personId));
                personDetailsVO.setPersonDesignation(Util.getG11nValue(rs.getString("JR.INTERNAL_JOB_TITLE_G11N_BIG_TXT"), personId));
                personDetailsVO.setPersonPhoto(Util.convertByteToString(rs.getBytes("PHOTO_IMAGEID")));
                personDetailsVO.setFeedbackGroupMemberId(Util.convertByteToString(rs.getBytes("GM.FDBK_GRP_MBR_PK_ID")));
                personDetailsVO.setMemberAdmin(rs.getBoolean("IS_FDBK_GRP_MBR_ADMIN"));
                personDetailsVO.setMemberNtfnMuted(rs.getBoolean("GM.IS_MBR_NTFN_MUTED"));
                personDetailsVO.setNameInitials(rs.getString("NAME_INITIALS_TXT"));
                SocialStatusVO socialStatusVO = getSocialStatusByPerson(personDetailsVO.getPersonId());
                personDetailsVO.setSocialStatus(socialStatusVO);
                List<Phone> personPhoneDetailList = getPhoneDetailsByPerson(personDetailsVO.getPersonId());
                personDetailsVO.setPhoneList(personPhoneDetailList);
                List<Email> personEmailDetailList = getEmailDetailByPerson(personDetailsVO.getPersonId());
                personDetailsVO.setEmailList(personEmailDetailList);
                personDetailsVOs.add(personDetailsVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personDetailsVOs;
    }

    @Override
    public PersonDetailsVO getPersonDetailsByPersonId(String personId) {
//        PP.PHONE_NUMBER_TXT, PT.PHONE_TYPE_ICON_CODE_FK_ID,PT.PHONE_TYPE_NAME_G11N_BIG_TXT, " +
//        "PE.EMAIL_ID_TXT, ET.EMAIL_TYPE_ICON_CODE_FK_ID, ET.EMAIL_TYPE_NAME_G11N_BIG_TXT,

        String sql = "SELECT PERSON_PK_ID, NAME_INITIALS_TXT, PHOTO_IMAGEID, LEGAL_FULL_NAME_G11N_BIG_TXT, JR.INTERNAL_JOB_TITLE_G11N_BIG_TXT, L.CITY_CSPEC_G11N_BIG_TXT, GM.FDBK_GRP_MBR_PK_ID, GM.IS_FDBK_GRP_MBR_ADMIN, GM.IS_MBR_NTFN_MUTED FROM T_PRN_EU_PERSON P\n" +
                "INNER JOIN T_PRN_EU_FEEDBK_GRP_MBR GM ON GM.FDBK_GRP_MBR_PERSON_FK_ID = P.PERSON_PK_ID\n" +
                "INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT JA ON P.PERSON_PK_ID = JA.PERSON_DEP_FK_ID\n" +
                "INNER JOIN T_ORG_ADM_ORG_JOB_PAY_GRADE_MAP PG ON JA.ORG_JOB_PAY_GRADE_MAP_FK_ID = PG.ORG_JOB_PAY_GRADE_MAP_PK_ID AND IS_PRIMARY = TRUE\n" +
                "INNER JOIN T_ORG_ADM_ORG_JOB_PAY_GRADE_BAND_MAP PGB ON PGB.ORG_JOB_PAY_GRADE_BAND_MAP_PK_ID = PG.ORG_JOB_PAY_GRADE_BAND_MAP_FK_ID\n" +
                "INNER JOIN T_ORG_ADM_ORG_JOB_MAP OJ ON PGB.ORG_JOB_MAP_FK_ID = OJ.ORG_JOB_MAP_PK_ID\n" +
                "INNER JOIN T_ORG_EU_ORG_LOC_MAP OL ON OJ.ORG_LOC_FK_ID = OL.ORG_LOC_PK_ID\n" +
                "INNER JOIN T_ORG_EU_LOCATION L ON OL.LOCATION_FK_ID = L.LOCATION_PK_ID\n" +
                "INNER JOIN T_JOB_ADM_JOB_ROLE JR ON JR.JOB_ROLE_PK_ID = OJ.JOB_ROLE_FK_ID WHERE P.PERSON_PK_ID = ? ;";
        Connection con ;
        PreparedStatement ps = null;
        PersonDetailsVO personDetailsVO = null;
        String loggedInPersonId = TenantContextHolder.getUserContext().getPersonId();
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setBytes(1, Util.convertStringIdToByteId(personId));
            LOG.debug(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                personDetailsVO = new PersonDetailsVO();
                personDetailsVO.setPersonId(Util.convertByteToString(rs.getBytes("PERSON_PK_ID")));
                personDetailsVO.setPersonFullName(Util.getG11nValue(rs.getString("LEGAL_FULL_NAME_G11N_BIG_TXT"), loggedInPersonId));
                personDetailsVO.setLocation(Util.getG11nValue(rs.getString("L.CITY_CSPEC_G11N_BIG_TXT"), loggedInPersonId));
                personDetailsVO.setPersonDesignation(Util.getG11nValue(rs.getString("JR.INTERNAL_JOB_TITLE_G11N_BIG_TXT"), loggedInPersonId));
                personDetailsVO.setPersonPhoto(Util.convertByteToString(rs.getBytes("PHOTO_IMAGEID")));
                personDetailsVO.setMemberAdmin(rs.getBoolean("GM.IS_FDBK_GRP_MBR_ADMIN"));
                personDetailsVO.setMemberNtfnMuted(rs.getBoolean("GM.IS_MBR_NTFN_MUTED"));
                personDetailsVO.setFeedbackGroupMemberId(Util.convertByteToString(rs.getBytes("GM.FDBK_GRP_MBR_PK_ID")));
                personDetailsVO.setNameInitials(rs.getString("NAME_INITIALS_TXT"));
                SocialStatusVO socialStatusVO = getSocialStatusByPerson(personDetailsVO.getPersonId());
                personDetailsVO.setSocialStatus(socialStatusVO);
                List<Phone> personPhoneDetailList = getPhoneDetailsByPerson(personId);
                personDetailsVO.setPhoneList(personPhoneDetailList);
                List<Email> personEmailDetailList = getEmailDetailByPerson(personId);
                personDetailsVO.setEmailList(personEmailDetailList);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personDetailsVO;
    }

    private List<Email> getEmailDetailByPerson(String personId) {
        String sql = "SELECT PE.EMAIL_ID_TXT, ET.EMAIL_TYPE_ICON_CODE_FK_ID, ET.EMAIL_TYPE_NAME_G11N_BIG_TXT FROM T_PRN_EU_PERSON P"+
                " LEFT JOIN T_PRN_EU_PERSON_EMAIL PE ON PE.PERSON_DEP_FK_ID = P.PERSON_PK_ID\n" +
                " LEFT JOIN T_PRN_LKP_EMAIL_TYPE ET ON PE.EMAIL_TYPE_CODE_FK_ID = ET.EMAIL_TYPE_CODE_PK_ID WHERE PERSON_PK_ID = ?;";
        Connection con ;
        PreparedStatement ps = null;
        PersonDetailsVO personDetailsVO = null;
        List<Email> emails = new ArrayList<>();
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setBytes(1, Util.convertStringIdToByteId(personId));
            LOG.debug(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Email email = new Email();
                email.setEmailId(rs.getString("PE.EMAIL_ID_TXT"));
                email.setEmailTypeIconCodeId(Util.getIcon(rs.getString("ET.EMAIL_TYPE_ICON_CODE_FK_ID")));
                email.setEmailTypeName(Util.getG11nValue(rs.getString("ET.EMAIL_TYPE_NAME_G11N_BIG_TXT"), personId));
                emails.add(email);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return emails;
    }

    private List<Phone> getPhoneDetailsByPerson(String personId) {
        String sql = "SELECT PERSON_PK_ID, PP.PHONE_NUMBER_TXT, PT.PHONE_TYPE_ICON_CODE_FK_ID, PT.PHONE_TYPE_NAME_G11N_BIG_TXT FROM T_PRN_EU_PERSON P" +
                " LEFT JOIN T_PRN_EU_PERSON_PHONE PP ON PP.PERSON_DEP_FK_ID = P.PERSON_PK_ID\n" +
                " LEFT JOIN T_PRN_LKP_PHONE_TYPE PT ON PP.PHONE_TYPE_CODE_FK_ID = PT.PHONE_TYPE_CODE_PK_ID WHERE PERSON_PK_ID = ?;";
        Connection con;
        PreparedStatement ps = null;
        PersonDetailsVO personDetailsVO = null;
        List<Phone> phones = new ArrayList<>();
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setBytes(1, Util.convertStringIdToByteId(personId));
            LOG.debug(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Phone phone = new Phone();
                phone.setPhoneNumberTxt(rs.getString("PP.PHONE_NUMBER_TXT"));
                phone.setPhoneTypeIconCodeId(Util.getIcon(rs.getString("PT.PHONE_TYPE_ICON_CODE_FK_ID")));
                phone.setPhoneTypeName(Util.getG11nValue(rs.getString("PT.PHONE_TYPE_NAME_G11N_BIG_TXT"), personId));
                phones.add(phone);
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
        return phones;
    }

    @Override
    public void updateFeedbackGroupMemberDetails(GroupMemberVO groupMemberVO) throws SQLException {
        String sql = "UPDATE T_PRN_EU_FEEDBK_GRP_MBR SET IS_FDBK_GRP_MBR_ADMIN = ?, IS_MBR_NTFN_MUTED = ?, IS_MBR_LEFT = ?, " +
                "IS_MBR_REMOVED = ?, IS_MBR_ACCESS_REMOVED = ? WHERE FDBK_GRP_MBR_PK_ID = ?;";
        Connection con ;
        PreparedStatement ps = null;
        String personId = TenantContextHolder.getUserContext().getPersonId();
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            ps.setBoolean(1, groupMemberVO.isMemberAdmin());
            ps.setBoolean(2, groupMemberVO.isMemberNtfnMuted());
            ps.setBoolean(3, groupMemberVO.isMemberLeft());
            ps.setBoolean(4, groupMemberVO.isMemberRemoved());
            ps.setBoolean(5, groupMemberVO.isMemberAccessRemoved());
//            ps.setBoolean(6, groupMemberVO.isDeleted());
            ps.setBytes(6, Util.convertStringIdToByteId(groupMemberVO.getFeedbackGroupMemberId()));
            LOG.debug(sql);
            ps.executeUpdate();
        }
        catch (SQLException e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public void addFeedbackGroupMembers(List<GroupMemberVO> groupMemberVOS) throws SQLException {
        StringBuilder sql = new StringBuilder("INSERT INTO T_PRN_EU_FEEDBK_GRP_MBR (FDBK_GRP_MBR_PK_ID, FDBK_GRP_DEP_FK_ID, FDBK_GRP_MBR_PERSON_FK_ID," +
                " IS_FDBK_GRP_MBR_ADMIN, IS_MBR_NTFN_MUTED, IS_MBR_LEFT, IS_MBR_REMOVED, IS_MBR_ACCESS_REMOVED, "+getStandardColumns()+") VALUES  ");
        for (GroupMemberVO groupMemberVO : groupMemberVOS){
            sql.append("( ?, ?, ?, ?, ?, ?, ?, ?, "+getStandardColumnValues()+"), ");
        }
        sql.replace(sql.length()-2, sql.length(), ";");
        Connection con = null;
        PreparedStatement ps;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            int i = 1;
            for (GroupMemberVO groupMemberVO : groupMemberVOS){
                ps.setBytes(i++, Util.convertStringIdToByteId(groupMemberVO.getFeedbackGroupMemberId()));
                ps.setBytes(i++, Util.convertStringIdToByteId(groupMemberVO.getGroupId()));
                ps.setBytes(i++, Util.convertStringIdToByteId(groupMemberVO.getPersonId()));
                ps.setBoolean(i++, groupMemberVO.isMemberAdmin());
                ps.setBoolean(i++, groupMemberVO.isMemberNtfnMuted());
                ps.setBoolean(i++, groupMemberVO.isMemberLeft());
                ps.setBoolean(i++, groupMemberVO.isMemberRemoved());
                ps.setBoolean(i++, groupMemberVO.isMemberRemoved());
            }
            LOG.debug(sql);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
            throw e;
        }

    }

    @Override
    public void deleteGroup(String feedbackGroupId) {
        deleteMessageByGroup(feedbackGroupId);
        deleteGroupMemberByGroup(feedbackGroupId);
        deleteGroupByGroupId(feedbackGroupId);
    }

    @Override
    public String getOtherPersonIdByGroup(String feedbackGroupPkId) {
        String sql = "SELECT FDBK_GRP_MBR_PERSON_FK_ID FROM T_PRN_EU_FEEDBK_GRP_MBR WHERE FDBK_GRP_DEP_FK_ID = ? AND FDBK_GRP_MBR_PERSON_FK_ID <> ?;";
        Connection con ;
        PreparedStatement ps = null;
        String personId = TenantContextHolder.getUserContext().getPersonId();
        String otherPersonId = null;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql);
            LOG.debug(sql);
            ps.setBytes(1, Util.convertStringIdToByteId(feedbackGroupPkId));
            ps.setBytes(2, Util.convertStringIdToByteId(personId));
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                otherPersonId = Util.convertByteToString(rs.getBytes("FDBK_GRP_MBR_PERSON_FK_ID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return otherPersonId;
    }

    @Override
    public void updateFeedbackGroup(FeedbackGroupVO feedbackGroupVO) throws SQLException {
        String sql = "UPDATE T_PRN_EU_FEEDBK_GRP SET FDBK_GRP_NAME_G11N_BIG_TXT = ?, FDBK_GRP_IMAGEID = ? WHERE FDBK_GRP_PK_ID = ?;";
        Connection con = null;
        PreparedStatement ps;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, feedbackGroupVO.getFeedbackGroupName());
            ps.setBytes(2, Util.convertStringIdToByteId(feedbackGroupVO.getFeedbackGroupImageId()));
            ps.setBytes(3, Util.convertStringIdToByteId(feedbackGroupVO.getFeedbackGroupPkId()));
            LOG.debug(sql);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public List<GroupMemberVO> getFeedGroupMemberReadStatus(String feedbackId) {
        String sql = "SELECT * FROM T_PRN_IEU_FEEDBK_READ_STATUS WHERE FDBK_DEP_FK_ID = ?;";
        Connection con ;
        PreparedStatement ps = null;
        List<GroupMemberVO> groupMemberVOS = new ArrayList<>();
        try {
            con = getConnection();
            ps = con.prepareStatement(sql);
            LOG.debug(sql);
            ps.setBytes(1, Util.convertStringIdToByteId(feedbackId));
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                String personId = Util.convertByteToString(rs.getBytes("FDBK_RD_PERSON_FK_ID"));
                String readDatetime = new String(Util.getStringTobeInserted("DATE_TIME",rs.getTimestamp("CREATED_DATETIME"), false)).replace("'", "");
                PersonDetailsVO personDetailsVO = getPersonDetailsByPersonId(personId);
                GroupMemberVO groupMemberVO = new GroupMemberVO();
                BeanUtils.copyProperties(personDetailsVO, groupMemberVO);
                groupMemberVO.setReadDatetime(readDatetime);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupMemberVOS;
    }

    @Override
    public FeedbackMessageVO getFeedbackMessageById(String feedbackPkId) {
        String sql = "SELECT DISTINCT PRN_FEEDBACK_PK_ID, MSG_BIG_TXT, F.FDBK_GRP_DEP_FK_ID, FDBK_SLF_PRNT_MSG_FK_ID, MSG_ATTACH, IS_MSG_EDITED, MSG_TYPE_TXT, \n" +
                " F.CREATEDBY_PERSON_FK_ID, F.CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT, GM.IS_MBR_LEFT, GM.IS_MBR_REMOVED, GM.IS_MBR_ACCESS_REMOVED, BOOKMARK_LOG_PK_ID, F.CREATED_DATETIME, F.LAST_MODIFIED_DATETIME \n" +
                " FROM T_PRN_EU_FEEDBK F LEFT JOIN T_PRN_EU_FEEDBK_GRP G ON G.FDBK_GRP_PK_ID = F.FDBK_GRP_DEP_FK_ID \n" +
                " INNER JOIN T_PRN_EU_FEEDBK_GRP_MBR GM ON G.FDBK_GRP_PK_ID = GM.FDBK_GRP_DEP_FK_ID \n" +
                " LEFT JOIN T_PFM_EU_BOOKMARK B ON F.PRN_FEEDBACK_PK_ID = B.DO_ROW_PRIMARY_KEY_FK_ID \n" +
                " WHERE F.PRN_FEEDBACK_PK_ID = ?  ;\n";
        FeedbackMessageVO feedbackMessageVO = null;
        String personId = TenantContextHolder.getUserContext().getPersonId();
        try {
            LOG.debug(sql);
            List<Object> params = new ArrayList<>();
            params.add(Util.convertStringIdToByteId(feedbackPkId));
            ResultSet rs = executeSQL(sql, params);
            while (rs.next()) {
                feedbackMessageVO = new FeedbackMessageVO();
                feedbackMessageVO.setFeedbackPkId(Util.convertByteToString(rs.getBytes("PRN_FEEDBACK_PK_ID")));
                feedbackMessageVO.setFeedbackGroupId(Util.convertByteToString(rs.getBytes("F.FDBK_GRP_DEP_FK_ID")));
                feedbackMessageVO.setMessage(rs.getString("MSG_BIG_TXT"));
                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_SLF_PRNT_MSG_FK_ID")));
                feedbackMessageVO.setMessageAttachId(Util.convertByteToString(rs.getBytes("MSG_ATTACH")));
                if (feedbackMessageVO.getMessageAttachId() != null) {
                    feedbackMessageVO.setMessageAttachUrl(Util.getURLforS3(feedbackMessageVO.getMessageAttachId(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
                }
                feedbackMessageVO.setMessageEdited(rs.getBoolean("IS_MSG_EDITED"));
                feedbackMessageVO.setMessageType(rs.getString("MSG_TYPE_TXT"));
//                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_PARENT_MSG_FK_ID")));
                feedbackMessageVO.setCreatedByPersonId(Util.convertByteToString(rs.getBytes("F.CREATEDBY_PERSON_FK_ID")));
                feedbackMessageVO.setCreatedByPersonName(Util.getG11nValue(rs.getString("F.CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT"), personId));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_LEFT"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_REMOVED"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_ACCESS_REMOVED"));
                feedbackMessageVO.setCreatedDatetime(new String(Util.getStringTobeInserted("DATE_TIME", rs.getTimestamp("F.CREATED_DATETIME"))).replace("'", ""));
                feedbackMessageVO.setLastModifiedDatetime(new String(Util.getStringTobeInserted("DATE_TIME", rs.getTimestamp("F.LAST_MODIFIED_DATETIME"))).replace("'", ""));
                if (feedbackMessageVO.getFeedbackParentMessageId() != null) {
                    feedbackMessageVO.setParentFeedbackMessage(getParentFeedbackMessage(feedbackMessageVO.getFeedbackParentMessageId()));
                }
                if (rs.getBytes("BOOKMARK_LOG_PK_ID") != null)
                    feedbackMessageVO.setBookmarked(true);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feedbackMessageVO;
    }

    @Override
    public List<FeedbackMessageVO> getBookmarkedFeedbackMessage(String feedbackGroupId, int limit, int offset) {
        StringBuilder sql = new StringBuilder("SELECT DISTINCT (PRN_FEEDBACK_PK_ID), F.FDBK_GRP_DEP_FK_ID, FDBK_GRP_NAME_G11N_BIG_TXT, MSG_BIG_TXT, FDBK_SLF_PRNT_MSG_FK_ID, MSG_ATTACH, IS_MSG_EDITED, MSG_TYPE_TXT, " +
                "F.CREATEDBY_PERSON_FK_ID, F.CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT, GM.IS_MBR_LEFT, GM.IS_MBR_REMOVED, GM.IS_MBR_ACCESS_REMOVED, BOOKMARK_LOG_PK_ID, B.LAST_MODIFIED_DATETIME, F.CREATED_DATETIME, F.LAST_MODIFIED_DATETIME " +
                "FROM T_PRN_EU_FEEDBK F LEFT JOIN T_PRN_EU_FEEDBK_GRP G ON G.FDBK_GRP_PK_ID = F.FDBK_GRP_DEP_FK_ID\n" +
                "INNER JOIN T_PRN_EU_FEEDBK_GRP_MBR GM ON G.FDBK_GRP_PK_ID = GM.FDBK_GRP_DEP_FK_ID\n" +
                "INNER JOIN T_PFM_EU_BOOKMARK B ON F.PRN_FEEDBACK_PK_ID = B.DO_ROW_PRIMARY_KEY_FK_ID WHERE F.CREATEDBY_PERSON_FK_ID = ? \n");
                if (feedbackGroupId != null) sql.append(" AND GM.FDBK_GRP_DEP_FK_ID = ? ");
                sql.append("AND GM.IS_DELETED = FALSE AND F.IS_DELETED = FALSE AND B.IS_DELETED = FALSE AND GM.IS_MBR_REMOVED = FALSE AND GM.IS_MBR_LEFT = FALSE AND GM.IS_MBR_ACCESS_REMOVED = FALSE ORDER BY F.CREATED_DATETIME DESC LIMIT ? OFFSET ?;");
        String personId = TenantContextHolder.getUserContext().getPersonId();
        List<FeedbackMessageVO> feedbackMessageVOs = new ArrayList<>();
        Connection con ;
        PreparedStatement ps = null;
        int i = 1;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql.toString());
            LOG.debug(sql);
            ps.setBytes(i++, Util.convertStringIdToByteId(personId));
            if (feedbackGroupId != null) ps.setBytes(i++, Util.convertStringIdToByteId(feedbackGroupId));
            ps.setInt(i++, limit);
            ps.setInt(i++, offset*limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                FeedbackMessageVO feedbackMessageVO = new FeedbackMessageVO();
                feedbackMessageVO.setFeedbackPkId(Util.convertByteToString(rs.getBytes("PRN_FEEDBACK_PK_ID")));
                feedbackMessageVO.setFeedbackGroupId(Util.convertByteToString(rs.getBytes("F.FDBK_GRP_DEP_FK_ID")));
                feedbackMessageVO.setFeedbackGroupName(rs.getString("FDBK_GRP_NAME_G11N_BIG_TXT"));
                feedbackMessageVO.setMessage(rs.getString("MSG_BIG_TXT"));
                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_SLF_PRNT_MSG_FK_ID")));
                feedbackMessageVO.setMessageAttachId(Util.convertByteToString(rs.getBytes("MSG_ATTACH")));
                if (feedbackMessageVO.getMessageAttachId() != null){
                    feedbackMessageVO.setMessageAttachUrl(Util.getURLforS3(feedbackMessageVO.getMessageAttachId(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
                }
                feedbackMessageVO.setMessageEdited(rs.getBoolean("IS_MSG_EDITED"));
                feedbackMessageVO.setMessageType(rs.getString("MSG_TYPE_TXT"));
//                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_PARENT_MSG_FK_ID")));
                feedbackMessageVO.setCreatedByPersonId(Util.convertByteToString(rs.getBytes("F.CREATEDBY_PERSON_FK_ID")));
                feedbackMessageVO.setCreatedByPersonName(Util.getG11nValue(rs.getString("F.CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT"), personId));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_LEFT"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_REMOVED"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_ACCESS_REMOVED"));
                feedbackMessageVO.setCreatedDatetime(new String(Util.getStringTobeInserted("DATE_TIME",rs.getTimestamp("F.CREATED_DATETIME"))).replace("'", ""));
                feedbackMessageVO.setLastModifiedDatetime(new String(Util.getStringTobeInserted("DATE_TIME",rs.getTimestamp("F.LAST_MODIFIED_DATETIME"))).replace("'", ""));
                if (rs.getTimestamp("B.LAST_MODIFIED_DATETIME") != null) feedbackMessageVO.setBookmarkedLastModifiedDatetime(new String(Util.getStringTobeInserted("DATE_TIME",rs.getTimestamp("B.LAST_MODIFIED_DATETIME"))).replace("'", ""));
                if (feedbackMessageVO.getFeedbackParentMessageId() != null){
                    feedbackMessageVO.setParentFeedbackMessage(getParentFeedbackMessage(feedbackMessageVO.getFeedbackParentMessageId()));
                }
                feedbackMessageVO.setBookmarkId(Util.convertByteToString(rs.getBytes("BOOKMARK_LOG_PK_ID")));
                if (feedbackMessageVO.getBookmarkId() != null)
                    feedbackMessageVO.setBookmarked(true);
                feedbackMessageVOs.add(feedbackMessageVO);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feedbackMessageVOs;
    }

    @Override
    public FeedbackMessageVO getFeedbackMessage(String feedbackPkId) {
        String sql = "SELECT DISTINCT (PRN_FEEDBACK_PK_ID), F.FDBK_GRP_DEP_FK_ID, FDBK_GRP_NAME_G11N_BIG_TXT, MSG_BIG_TXT, FDBK_SLF_PRNT_MSG_FK_ID, MSG_ATTACH, IS_MSG_EDITED, MSG_TYPE_TXT, F.IS_DELETED, \n" +
                "                F.CREATEDBY_PERSON_FK_ID, F.CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT, GM.IS_MBR_LEFT, GM.IS_MBR_REMOVED, GM.IS_MBR_ACCESS_REMOVED, BOOKMARK_LOG_PK_ID, B.LAST_MODIFIED_DATETIME, F.CREATED_DATETIME, F.LAST_MODIFIED_DATETIME \n" +
                "                FROM T_PRN_EU_FEEDBK F INNER JOIN T_PRN_EU_FEEDBK_GRP G ON G.FDBK_GRP_PK_ID = F.FDBK_GRP_DEP_FK_ID\n" +
                "                INNER JOIN T_PRN_EU_FEEDBK_GRP_MBR GM ON G.FDBK_GRP_PK_ID = GM.FDBK_GRP_DEP_FK_ID\n" +
                "                LEFT JOIN T_PFM_EU_BOOKMARK B ON F.PRN_FEEDBACK_PK_ID = B.DO_ROW_PRIMARY_KEY_FK_ID AND B.IS_DELETED = FALSE\n" +
                "                WHERE F.PRN_FEEDBACK_PK_ID = ? AND GM.IS_MBR_REMOVED = FALSE AND GM.IS_MBR_LEFT = FALSE AND GM.IS_MBR_ACCESS_REMOVED = FALSE ;";
        FeedbackMessageVO feedbackMessageVO = null;
        String personId = TenantContextHolder.getUserContext().getPersonId();
        Connection con ;
        PreparedStatement ps = null;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql);
            LOG.debug(sql);
            ps.setBytes(1, Util.convertStringIdToByteId(feedbackPkId));
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                feedbackMessageVO = new FeedbackMessageVO();
                feedbackMessageVO.setFeedbackPkId(Util.convertByteToString(rs.getBytes("PRN_FEEDBACK_PK_ID")));
                feedbackMessageVO.setFeedbackGroupId(Util.convertByteToString(rs.getBytes("F.FDBK_GRP_DEP_FK_ID")));
                feedbackMessageVO.setFeedbackGroupName(rs.getString("FDBK_GRP_NAME_G11N_BIG_TXT"));
                feedbackMessageVO.setMessage(rs.getString("MSG_BIG_TXT"));
                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_SLF_PRNT_MSG_FK_ID")));
                feedbackMessageVO.setMessageAttachId(Util.convertByteToString(rs.getBytes("MSG_ATTACH")));
                if (feedbackMessageVO.getMessageAttachId() != null){
                    feedbackMessageVO.setMessageAttachUrl(Util.getURLforS3(feedbackMessageVO.getMessageAttachId(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
                }
                feedbackMessageVO.setMessageEdited(rs.getBoolean("IS_MSG_EDITED"));
                feedbackMessageVO.setMessageType(rs.getString("MSG_TYPE_TXT"));
//                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_PARENT_MSG_FK_ID")));
                feedbackMessageVO.setDeleted(rs.getBoolean("F.IS_DELETED"));
                feedbackMessageVO.setCreatedByPersonId(Util.convertByteToString(rs.getBytes("F.CREATEDBY_PERSON_FK_ID")));
                feedbackMessageVO.setCreatedByPersonName(Util.getG11nValue(rs.getString("F.CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT"), personId));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_LEFT"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_REMOVED"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_ACCESS_REMOVED"));
                feedbackMessageVO.setCreatedDatetime(new String(Util.getStringTobeInserted("DATE_TIME",rs.getTimestamp("F.CREATED_DATETIME"))).replace("'", ""));
                feedbackMessageVO.setLastModifiedDatetime(new String(Util.getStringTobeInserted("DATE_TIME",rs.getTimestamp("F.LAST_MODIFIED_DATETIME"))).replace("'", ""));
                if (rs.getTimestamp("B.LAST_MODIFIED_DATETIME") != null) feedbackMessageVO.setBookmarkedLastModifiedDatetime(new String(Util.getStringTobeInserted("DATE_TIME",rs.getTimestamp("B.LAST_MODIFIED_DATETIME"))).replace("'", ""));
                if (feedbackMessageVO.getFeedbackParentMessageId() != null){
                    feedbackMessageVO.setParentFeedbackMessage(getParentFeedbackMessage(feedbackMessageVO.getFeedbackParentMessageId()));
                }
                feedbackMessageVO.setBookmarkId(Util.convertByteToString(rs.getBytes("BOOKMARK_LOG_PK_ID")));
                if (feedbackMessageVO.getBookmarkId() != null)
                    feedbackMessageVO.setBookmarked(true);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feedbackMessageVO;
    }

    @Override
    public LayoutPermissionsVO getLayoutPermissions(String actionId) throws SQLException {
        String sql = "SELECT * FROM T_UI_ADM_SECTION_ACTION_INSTANCE_PERMISSION WHERE SECTION_ACTION_INSTANCE_FK_ID = x?;";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(actionId));
        ResultSet rs = executeSQL(sql, params);
        LayoutPermissionsVO layoutPermissionsVO = null;
        while (rs.next()){
            layoutPermissionsVO = new LayoutPermissionsVO();
            layoutPermissionsVO.setPkId(Util.convertByteToString(rs.getBytes("SECTN_INST_PERMSN_PK_ID")));
            layoutPermissionsVO.setFkId(Util.convertByteToString(rs.getBytes("SECTION_INSTANCE_FK_ID")));
            layoutPermissionsVO.setPermissionType(rs.getString("PERMISSION_TYPE_CODE_FK_ID"));
            layoutPermissionsVO.setPermissionValue(rs.getString("PERMISSION_VALUE_LNG_TXT"));
            layoutPermissionsVO.setSubjectUserQualifier(rs.getString("ADDL_SUB_USR_APPL_GROUP_BIG_TXT"));
        }
        return layoutPermissionsVO;
    }

    private void deleteGroupByGroupId(String feedbackGroupId) {
        String sql = "UPDATE T_PRN_EU_FEEDBK_GRP SET IS_DELETED = TRUE WHERE FDBK_GRP_PK_ID = ?;";
        Connection con = null;
        PreparedStatement ps;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql);
            ps.setBytes(1, Util.convertStringIdToByteId(feedbackGroupId));
            LOG.debug(sql);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void deleteGroupMemberByGroup(String feedbackGroupId) {
        String sql = "UPDATE T_PRN_EU_FEEDBK_GRP_MBR SET IS_DELETED = TRUE WHERE FDBK_GRP_DEP_FK_ID = ?;";
        Connection con = null;
        PreparedStatement ps;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql);
            ps.setBytes(1, Util.convertStringIdToByteId(feedbackGroupId));
            LOG.debug(sql);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void deleteMessageByGroup(String feedbackGroupId) {
        String sql = "UPDATE T_PRN_EU_FEEDBK SET IS_DELETED = TRUE WHERE FDBK_GRP_DEP_FK_ID = ?;";
        Connection con = null;
        PreparedStatement ps;
        try {
            con = getConnection();
            ps = con.prepareStatement(sql);
            ps.setBytes(1, Util.convertStringIdToByteId(feedbackGroupId));
            LOG.debug(sql);
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private FeedbackMessageVO getParentFeedbackMessage(String feedbackParentMessageId) {
        String sql = "SELECT * FROM T_PRN_EU_FEEDBK WHERE PRN_FEEDBACK_PK_ID = ?;";
        Connection con ;
        PreparedStatement ps = null;
        String personId = TenantContextHolder.getUserContext().getPersonId();
        try {
            con = getConnection();
            ps = con.prepareStatement(sql);
            LOG.debug(sql);
            ps.setBytes(1, Util.convertStringIdToByteId(feedbackParentMessageId));
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                FeedbackMessageVO feedbackMessageVO = new FeedbackMessageVO();
                feedbackMessageVO.setFeedbackPkId(Util.convertByteToString(rs.getBytes("PRN_FEEDBACK_PK_ID")));
                feedbackMessageVO.setFeedbackGroupId(Util.convertByteToString(rs.getBytes("FDBK_GRP_DEP_FK_ID")));
                feedbackMessageVO.setMessage(rs.getString("MSG_BIG_TXT"));
                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_SLF_PRNT_MSG_FK_ID")));
                feedbackMessageVO.setMessageAttachId(Util.convertByteToString(rs.getBytes("MSG_ATTACH")));
                if (feedbackMessageVO.getMessageAttachId() != null)
                    feedbackMessageVO.setMessageAttachUrl(Util.getURLforS3(feedbackMessageVO.getMessageAttachId(), SystemAwareCache.getSystemRepository().getAmazonS3Client()));
                feedbackMessageVO.setMessageEdited(rs.getBoolean("IS_MSG_EDITED"));
                feedbackMessageVO.setMessageType(rs.getString("MSG_TYPE_TXT"));
//                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_PARENT_MSG_FK_ID")));
                feedbackMessageVO.setCreatedByPersonName(Util.getG11nValue(rs.getString("CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT"), personId));
                feedbackMessageVO.setCreatedByPersonId(Util.convertByteToString(rs.getBytes("CREATEDBY_PERSON_FK_ID")));
                feedbackMessageVO.setCreatedDatetime(new String(Util.getStringTobeInserted("DATE_TIME", rs.getTimestamp("CREATED_DATETIME"))).replace("'", ""));
                feedbackMessageVO.setLastModifiedDatetime(new String(Util.getStringTobeInserted("DATE_TIME", rs.getTimestamp("LAST_MODIFIED_DATETIME"))).replace("'", ""));
                /*
                FeedbackMessageVO feedbackMessageVO = new FeedbackMessageVO();
                feedbackMessageVO.setFeedbackPkId(Util.convertByteToString(rs.getBytes("PRN_FEEDBACK_PK_ID")));
                feedbackMessageVO.setMessage(rs.getString("MSG_BIG_TXT"));
                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_PARENT_MSG_FK_ID")));
                feedbackMessageVO.setMessageAttachId(Util.convertByteToString(rs.getBytes("MSG_ATTACH")));
                feedbackMessageVO.setMessageEdited(rs.getBoolean("IS_MSG_EDITED"));
                feedbackMessageVO.setMessageType(rs.getString("MSG_TYPE_TXT"));
                feedbackMessageVO.setFeedbackParentMessageId(Util.convertByteToString(rs.getBytes("FDBK_PARENT_MSG_FK_ID")));
                feedbackMessageVO.setCreatedByPersonId(Util.convertByteToString(rs.getBytes("F.CREATEDBY_PERSON_FK_ID")));
                feedbackMessageVO.setCreatedByPersonName(rs.getString("F.CREATED_BY_PERSON_FULLNAME_TXT"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_LEFT"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_REMOVED"));
                feedbackMessageVO.setMemLeft(rs.getBoolean("GM.IS_MBR_ACCESS_REMOVED"));
                 */
                return feedbackMessageVO;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getStandardColumns(){
        return " IS_DELETED, CREATEDBY_PERSON_FK_ID, CREATED_BY_PERSON_FULLNAME_G11N_BIG_TXT, CREATED_DATETIME, LAST_MODIFIEDBY_PERSON_FK_ID, LAST_MODIFIEDBY_PERSON_FULLNAME_G11N_BIG_TXT, LAST_MODIFIED_DATETIME ";
    }

    private String getStandardColumnValues(){
        String personId = TenantContextHolder.getUserContext().getPersonId();
        String personName = TenantContextHolder.getUserContext().getPersonName();
        return " FALSE, "+personId+", '"+personName+"', NOW(), "+personId+", '"+personName+"', NOW() ";
    }

    private String getStandardUpdateColumns(){
        return " LAST_MODIFIEDBY_PERSON_FK_ID, LAST_MODIFIEDBY_PERSON_FULLNAME_G11N_BIG_TXT, LAST_MODIFIED_DATETIME ";
    }

    private String getStandardUpdateColumnValues(){
        String personId = TenantContextHolder.getUserContext().getPersonId();
        String personName = TenantContextHolder.getUserContext().getPersonName();
        return " "+personId+", '"+personName+"', NOW() ";
    }

    private String getG11nValue(String input){
        return Util.getG11nValue(input, null);
    }
}
