package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.DG_DocGenData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.PagingQueryProvider;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;


public class DocGenReader extends JdbcPagingItemReader<DG_DocGenData> {


	
	public DocGenReader() {
		super();
		DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
		setDataSource(ds);
		setPageSize(20);
		setQueryProvider(queryProvider(ds));
		setRowMapper(rowMapper());
	}
	
	private RowMapper<DG_DocGenData> rowMapper() {
		return new DocRowMapper();
	}

	private PagingQueryProvider queryProvider(DataSource ds) {
		 MySqlPagingQueryProvider provider = new MySqlPagingQueryProvider();
		    provider.setSelectClause("T_PRN_IEU_DOCUMENT.PERSON_DOCUMENT_PK_ID,"
		    		+ "T_PRN_IEU_DOCUMENT.PERSON_FK_ID,"
		    		+ "T_PRN_IEU_DOCUMENT.MT_PE_CODE_FK_ID,"
		    		+ "T_PRN_IEU_DOCUMENT.DO_ROW_PRIMARY_KEY_TXT,"
		    		+ "T_PRN_IEU_DOCUMENT.DO_ROW_VERSION_FK_ID,"
		    		+ "T_PRN_IEU_DOCUMENT.DEPENDENCY_KEY_EXPN,"
		    		+ "T_PRN_IEU_DOCUMENT.SRC_MT_PE_CODE_FK_ID,"
		    		+ "T_PRN_IEU_DOCUMENT.SRC_BT_CODE_FK_ID,"
		    		+ "T_PRN_IEU_DOCUMENT.SRC_VIEW_TYPE_CODE_FK_ID,"
		    		+ "T_PRN_IEU_DOCUMENT.FILE_ATTACH,"
		    		+ "T_PRN_IEU_DOCUMENT.FILE_NAME_LNG_TXT,"
		    		+ "T_PRN_IEU_DOCUMENT.RETURN_STATUS_CODE_FK_ID,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.DOC_GEN_DEFINITION_PK_ID,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.DOC_BT_CODE_FK_ID,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.DOC_MT_PE_CODE_FK_ID,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.DOC_VT_CODE_FK_ID,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.DOC_CANVAS_TXN_FK_ID,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.EXPORT_PPT_TEMPLATE_FK_ID,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.DOC_TYPE_CODE_FK_ID,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.IS_PASSWORD_ENABLED,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.NOTIFICATION_DEFINITION_FK_ID,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.DOC_NAME_EXPN,"
		    		+ "T_PFM_ADM_DOC_GEN_DEFINITION.DOC_PWD_EXPN");
		    provider.setFromClause("FROM T_PRN_IEU_DOCUMENT "
		    		+ "INNER JOIN T_PFM_ADM_DOC_GEN_DEFINITION ON "
		    		+ "T_PRN_IEU_DOCUMENT.DOC_GEN_DEFINITION_FK_ID = T_PFM_ADM_DOC_GEN_DEFINITION.DOC_GEN_DEFINITION_PK_ID");
		    provider.setWhereClause("T_PRN_IEU_DOCUMENT.RETURN_STATUS_CODE_FK_ID = 'PENDING' and "
		    		+ "T_PRN_IEU_DOCUMENT.IS_SYNC = false");
		    Map abc = new HashMap<String, Order>();
		    abc.put("PERSON_DOCUMENT_PK_ID", Order.ASCENDING);
		   provider.setSortKeys(abc);
		
		try {
			return provider;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
