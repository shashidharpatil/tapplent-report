package com.tapplent.platformutility.batch.process;

import javax.sql.DataSource;

import com.tapplent.platformutility.batch.model.SA_SocAccData;
import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;


public class SocAccItemWriter extends JdbcBatchItemWriter<SA_SocAccData> {

	public SocAccItemWriter(){
		super();
		DataSource ds = (DataSource) TapplentServiceLookupUtil.getService(TenantContextHolder.getCurrentDataSourceInstance().name());
		setDataSource(ds);
		setSql(getSQL());	
		setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<SA_SocAccData>());
		afterPropertiesSet();
	}
	
	
	public String getSQL(){
		//(TDWI) insert into notification table
		String sql = "";
		return sql;
		
	}
	

}
