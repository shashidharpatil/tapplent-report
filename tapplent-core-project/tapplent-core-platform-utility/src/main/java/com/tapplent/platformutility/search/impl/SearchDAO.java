package com.tapplent.platformutility.search.impl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Deque;
import java.util.List;

import com.tapplent.platformutility.search.api.SearchResult;

public interface SearchDAO {
	
	SearchResult searchAndCount(String sql,String countSql, Deque<Object> parameter, Deque<Object> countParameter , SearchContext searchContext, DefaultSearchData searchData) throws IOException;
	SearchResult search(String sql,Deque<Object> parameter);

    String getG11nData(String tableName, String columnName, String pkColumnName, String dbPkId, String dbDataTypeCode);

}
