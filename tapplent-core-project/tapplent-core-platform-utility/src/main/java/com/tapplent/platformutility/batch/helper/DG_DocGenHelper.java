package com.tapplent.platformutility.batch.helper;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.DG_DocGenData;
import com.tapplent.platformutility.batch.model.DG_PPTData;
import com.tapplent.platformutility.batch.model.DG_PPTDoaMap;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.expression.jeval.EvaluationException;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.insert.impl.InsertDAO;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DG_DocGenHelper extends TapplentBaseDAO {

    private InsertDAO insertDAO;


    public void setInsertDAO(InsertDAO insert) {
        this.insertDAO = insert;
    }

    public InsertDAO getInsertDAO() {
        return insertDAO;
    }

    public String fetchURL(DG_DocGenData item) {
        String url = "http://tidy.sourceforge.net/docs/quickref.html";

        // (TDWI) method to fetch web api endPoint URL for Doc Gen

        return url;

    }


    public InputStream getPPTemplate(DG_DocGenData item) {

        return S3Helper.fetchObject(item.getExportPptTemplateFkId());
    }

    public Map<Integer, DG_PPTData> getPPTData(DG_DocGenData item) {
        Map<Integer, DG_PPTData> map = new HashMap<>();
        try {
        //  logic to fetch ppt Data from T_UI_ADM_PPT_EXPORT
        String sql = "select EXPORT_PPT_TEMPLATE_PK_ID," +
                "EXPORT_PPT_TEMPLATE_NAME_G11N_BIG_TXT," +
                "MT_PE_CODE_FK_ID," +
                "SEQUENCE_NUMBER_POS_INT," +
                "PPT_TEMPLATE_FILE_NAME_TXT," +
                "PPT_TEMPLATE_DOC_ID," +
                "IS_EXPORT_PDF from T_UI_ADM_PPT_EXPORT where " +
                "EXPORT_PPT_TEMPLATE_PK_ID = '"+item.getExportPptTemplateFkId()+"';";
        ResultSet resultSet = executeSQL(sql,new ArrayList<>());

            while (resultSet.next()) {
                DG_PPTData dg_pptData = new DG_PPTData();
               dg_pptData.setExportPptTemplatePkId(resultSet.getString(Constants.exportPptTemplatePkId));
                dg_pptData.setExportPptTemplateNameG11NBlob(resultSet.getString(Constants.exportPptTemplateNameG11NBlob));
                dg_pptData.setMtPeCodeFkId(resultSet.getString(Constants.mtPeCodeFkId));
                dg_pptData.setSequenceNumberPosInt(resultSet.getInt(Constants.sequenceNumberPosInt));
                dg_pptData.setPptTemplateFileNameTxt(resultSet.getString(Constants.pptTemplateFileNameTxt));
                dg_pptData.setPptTemplateDocId(resultSet.getString(Constants.pptTemplateDocId));
                dg_pptData.setExportPdf(resultSet.getBoolean(Constants.isExportPdf));
                map.put(resultSet.getInt(Constants.sequenceNumberPosInt), dg_pptData);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return map;
    }

    public Map<String, DG_PPTDoaMap> getPPTDoaMap(DG_DocGenData item) {


        // logic to fetch ppt Data from T_UI_ADM_PPT_EXPORT_DOA
        Map<String, DG_PPTDoaMap> map = new HashMap<>();
        try {

            String sql = "select EXPORT_PPT_TEMPLATE_DOA_PK_ID," +
                    "EXPORT_PPT_TEMPLATE_FK_ID," +
                    "PPT_SLIDEID_TXT," +
                    "PPT_SHAPEID_TXT," +
                    "PPT_BT_CODE_FK_ID," +
                    "PPT_MT_DOA_PATH_LNG_TXT from T_UI_ADM_PPT_EXPORT_DOA" +
                    "where EXPORT_PPT_TEMPLATE_FK_ID = " + item.getExportPptTemplateFkId() + ";";
            List<Object> parameters = new ArrayList<>();
            ResultSet rs = executeSQL(sql, parameters);
            while (rs.next()) {
                DG_PPTDoaMap dg_pptDoaMap = new DG_PPTDoaMap();
                dg_pptDoaMap.setExportPptTemplateDoaPkId(rs.getString(Constants.exportPptTemplateDoaPkId));
                dg_pptDoaMap.setExportPptTemplateFkId(rs.getString(Constants.exportPptTemplateFkId));
                dg_pptDoaMap.setPptSlideidTxt(rs.getString(Constants.pptSlideidTxt));
                dg_pptDoaMap.setPptShapeidTxt(rs.getString(Constants.pptShapeidTxt));
                dg_pptDoaMap.setPptBtCodeFkId(rs.getString(Constants.pptBtCodeFkId));
                dg_pptDoaMap.setPptMtDoaPathLngTxt(rs.getString(Constants.pptMtDoaPathLngTxt));
                map.put(rs.getString(Constants.pptShapeidTxt), dg_pptDoaMap);

            }

        } catch (Exception e) {

        }
        return map;
    }

    public byte[] getPassword(DG_DocGenData item) {

        // fetch person record for password creation
        // logic to create Password by looking at Expression (integrate with server exp parser)
        if (item.isPasswordEnabled() == true) {
            String pass = "";
            try {
                String sql = "select * from T_PRN_EU_PERSON where PERSON_PK_ID = '" + item.getPersonFkId() + "';";
                ResultSet resultSet = executeSQL(sql, new ArrayList<>());
                ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
                int columnCount = resultSetMetaData.getColumnCount();
                Evaluator evaluator = new Evaluator();
                for (int i = 1; i <= columnCount; i++) {
                    String name = resultSetMetaData.getColumnName(i);
                    evaluator.putVariable(name,resultSet.getString(name));
                }
                pass = evaluator.evaluate(item.getDocPwdExpn());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new String(pass).getBytes();
        } else
            return null;
    }

    public void insertSkippedElements(DG_DocGenData genData) {

        String sql = "insert into T_SCH_IEU_SKIPPED_ELEMENT (SKIPPED_ELMNT_PK_ID," +
                "SCHEDULER_INSTANCE_FK_ID," +
                "ERROR_MESSAGE_BIG_TXT," +
                "LINKED_MT_PE_CODE_FK_ID," +
                "LINKED_DO_ROW_PRIMARY_KEY_TXT," +
                "LINKED_DO_ROW_VERSION_FK_ID," +
                "CREATED_DATETIME) values " +
                "('" + Common.getUUID() + "','" + genData.getSchedulerInstanceId() + "','" + genData.getError().toString()
                + "','" + genData.getMtPeCodeFkId() + "','" + genData.getDoRowPrimaryKeyTxt() + "','" + genData.getDoRowVersionFkId()
                + "','" + Common.getCurrentTimeStamp() + "');";

        try {
            insertDAO.insertRow(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }




}
