package com.tapplent.platformutility.person.dao;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.PersonPreferenceVO;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.platformutility.person.structure.PersonGroup;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by tapplent on 07/11/16.
 */
public class PersonDAOImpl extends TapplentBaseDAO implements PersonDAO {
    @Override
    public List<String> getPersonGroups(String personId) {
        List<String> result = new ArrayList<>();
        String SQL = "SELECT DISTINCT PFM_GROUP_FK_ID FROM T_PFM_IEU_GROUP_MEMBER WHERE GROUP_MEMBER_PERSON_FK_ID = x? AND IS_DELETED = 0;";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(personId));
        ResultSet rs = executeSQL(SQL, params);
        try {
            while(rs.next()){
                String groupId = Util.convertByteToString(rs.getBytes(1));
                result.add(groupId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Person getPerson(String personId) {
        Person person = null;
        AmazonS3 s3Client = SystemAwareCache.getSystemRepository().getAmazonS3Client();
        String SQL = "SELECT PRF.EFFECTIVE_DATETIME, PRN.PERSON_PK_ID, ORGANIZATION_FK_ID,PRN.LEGAL_FULL_NAME_G11N_BIG_TXT, PRN.BASE_COUNTRY_CODE_FK_ID, ORG_DFLT_LOCALE_CODE_FK_ID, PRF.TIMEZONE_CODE_FK_ID, \n" +
                " ORG_LOC_FK_ID, PRF.VERSION_ID, PRF.PERSON_PREFERENCES_PK_ID, PRF.PERSON_DEP_FK_ID, PRF.DATE_FORMAT_CODE_FK_ID, PRF.TIME_FORMAT_CODE_FK_ID, PRF.CURRENCY_CODE_FK_ID, PRF.LOCALE_CODE_FK_ID, PRF.IS_SHOW_TICKER,\n" +
                " PRF.IS_SHOW_TICKER, PRF.SOCIAL_STATUS_CODE_FK_ID, MOOD_MESSAGE_TXT, PROFILE_BACKGROUND_IMAGEID, CNVSN_BACKGROUND_IMAGEID, INTRODUCTION_VIDEO, NAME_PRONOUNCIATION_AUDIO, NAME_PRONOUNCIATION_TXT, " +
                "PRN.PHOTO_IMAGEID, PRF.LAST_MODIFIED_DATETIME, UTC_OFFSET_TXT \n" +
                "FROM T_PRN_EU_PERSON AS PRN LEFT JOIN T_PRN_EU_PERSON_PREFERENCES AS PRF ON PRF.PERSON_DEP_FK_ID = PRN.PERSON_PK_ID\n" +
                "  LEFT JOIN T_SYS_LKP_TIMEZONE AS TZ ON TZ.TIMEZONE_CODE_PK_ID = PRF.TIMEZONE_CODE_FK_ID\n"+
                "  LEFT JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT PJA ON (PJA.PERSON_DEP_FK_ID = PERSON_PK_ID) AND IS_PRIMARY\n" +
                "  LEFT JOIN T_ORG_ADM_ORG_JOB_PAY_GRADE_MAP ON ORG_JOB_PAY_GRADE_MAP_FK_ID = ORG_JOB_PAY_GRADE_MAP_PK_ID\n" +
                "  LEFT JOIN T_ORG_ADM_ORG_JOB_PAY_GRADE_BAND_MAP ON ORG_JOB_PAY_GRADE_BAND_MAP_FK_ID = ORG_JOB_PAY_GRADE_BAND_MAP_PK_ID\n" +
                "  LEFT JOIN T_ORG_ADM_ORG_JOB_MAP ON ORG_JOB_MAP_FK_ID = ORG_JOB_MAP_PK_ID\n" +
                "  LEFT JOIN T_ORG_EU_ORG_LOC_MAP ON ORG_LOC_FK_ID = ORG_LOC_PK_ID\n" +
                "WHERE PRN.PERSON_PK_ID = x? AND PRN.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE' AND\n" +
                "      PRN.IS_DELETED = 0 AND PRN.RECORD_STATE_CODE_FK_ID = 'CURRENT';";// AND PRF.SAVE_STATE_CODE_FK_ID = 'SAVED';";
        List<Object> params = new ArrayList<>();
        params.add(Util.remove0x(personId));
        ResultSet rs = executeSQL(SQL, params);
        try {
            if(rs.next()){
                person = new Person();
                PersonPreferenceVO personPreferenceVO = new PersonPreferenceVO();
                person.setPersonPkId(Util.convertByteToString(rs.getBytes("PRN.PERSON_PK_ID")));
                person.setPrimaryOrgID("ORGANIZATION_FK_ID");
                try {
                    person.setPersonName(rs.getString("PRN.LEGAL_FULL_NAME_G11N_BIG_TXT"));
                }catch (Exception e){

                }
                person.setBaseCountryCodeFkId(rs.getString("PRN.BASE_COUNTRY_CODE_FK_ID"));
                person.setOrgDefaultLocale(rs.getString("ORG_DFLT_LOCALE_CODE_FK_ID"));
                person.setOrgLocFkID(Util.convertByteToString(rs.getBytes("ORG_LOC_FK_ID")));
//                person.setOrgLocFkID(rs.getString("ORG_LOC_FK_ID"));
                personPreferenceVO.setEffectiveDateTime(Util.getStringTobeInserted("DATE_TIME", rs.getTimestamp("PRF.EFFECTIVE_DATETIME")).replace("'",""));
                personPreferenceVO.setVersionId(Util.convertByteToString(rs.getBytes("PRF.VERSION_ID")));
                personPreferenceVO.setPersonPreferencesPkId(Util.convertByteToString(rs.getBytes("PRF.PERSON_PREFERENCES_PK_ID")));
                personPreferenceVO.setPersonFkId(Util.convertByteToString(rs.getBytes("PRF.PERSON_DEP_FK_ID")));
                personPreferenceVO.setDateFormatCodeFkId(rs.getString("PRF.DATE_FORMAT_CODE_FK_ID"));
                personPreferenceVO.setTimeFormatCodeFkId(rs.getString("PRF.TIME_FORMAT_CODE_FK_ID"));
                personPreferenceVO.setCurrencyCodeFkId(rs.getString("PRF.CURRENCY_CODE_FK_ID"));
                personPreferenceVO.setTimezoneCodeFkId(rs.getString("PRF.TIMEZONE_CODE_FK_ID"));
                personPreferenceVO.setLocaleCodeFkId(rs.getString("PRF.LOCALE_CODE_FK_ID"));
                personPreferenceVO.setShowTicker(rs.getBoolean("PRF.IS_SHOW_TICKER"));
                personPreferenceVO.setOrgDefaultLocaleCodeFkId(person.getOrgDefaultLocale());
                personPreferenceVO.setSocialStatus(rs.getString("PRF.SOCIAL_STATUS_CODE_FK_ID"));
                personPreferenceVO.setMoodMessage(rs.getString("MOOD_MESSAGE_TXT"));
                personPreferenceVO.setBackGroundImage(Util.preSignedGetUrl(Util.convertByteToString(rs.getBytes("PROFILE_BACKGROUND_IMAGEID")), s3Client));
                personPreferenceVO.setConversationBackGroundImage(Util.preSignedGetUrl(Util.convertByteToString(rs.getBytes("CNVSN_BACKGROUND_IMAGEID")), s3Client));
                personPreferenceVO.setIntroductionVideo(Util.preSignedGetUrl(Util.convertByteToString(rs.getBytes("INTRODUCTION_VIDEO")), s3Client));
                personPreferenceVO.setNamePronounciationAudio(Util.preSignedGetUrl(Util.convertByteToString(rs.getBytes("NAME_PRONOUNCIATION_AUDIO")), s3Client));
                personPreferenceVO.setNamePronounciation(rs.getString("NAME_PRONOUNCIATION_TXT"));
                personPreferenceVO.setPersonPhoto(Util.preSignedGetUrl(Util.convertByteToString(rs.getBytes("PRN.PHOTO_IMAGEID")), s3Client));
                personPreferenceVO.setLastModifiedDateTime(Util.getStringTobeInserted("DATE_TIME", rs.getTimestamp("PRF.LAST_MODIFIED_DATETIME")).replace("'",""));
                personPreferenceVO.setOrgDefaultLocaleCodeFkId(person.getOrgDefaultLocale());
                personPreferenceVO.setUtcOffset(rs.getString("UTC_OFFSET_TXT"));
                person.setPersonPreferences(personPreferenceVO);
            }
            List<String> groupList = getPersonGroups(personId);
            PersonGroup personGroup = new PersonGroup(personId, groupList);
            person.setPersonGroups(personGroup);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return person;
    }

    @Override
    public String getDefaultBTForGroup(String mtPECode, PersonGroup group) {

        List<String> result = new ArrayList<>();
        String sql = "SELECT T_BT_ADM_DO_SPEC.BASE_TEMPLATE_CODE_FK_ID FROM T_BT_ADM_DO_SPEC INNER JOIN T_BT_ADM_PROCESS_ELEMENT ON BT_PROCESS_ELEMENT_CODE_FK_ID = TEMPLATE_PROCESS_ELEMENT_CODE_PK_ID " +
                "INNER JOIN T_BT_ADM_BASE_TEMPLATE ON BASE_TEMPLATE_CODE_PK_ID = T_BT_ADM_PROCESS_ELEMENT.BASE_TEMPLATE_CODE_FK_ID WHERE MT_PROCESS_ELEMENT_CODE_FK_ID = ? AND IS_REPORTING_BT;";
        List<Object> params = new ArrayList<>();
        params.add(mtPECode);
        ResultSet rs  = executeSQL(sql, params);
        try {
            while (rs.next()){
                String bt = rs.getString(1);
                result.add(bt);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(result.size()!=1){
            //TODO throw Error
        }
//        StringBuilder sql = new StringBuilder("SELECT DISTINCT BASE_TEMPLATE_CODE_FK_ID\n" +
//                "FROM T_PFM_ADM_TEMPLATE_MAPPER\n" +
//                "WHERE MT_PE_CODE_FK_ID = ? AND IS_META_DEFAULT AND T_PFM_ADM_TEMPLATE_MAPPER.PFM_GROUP_FK_ID IN (");
//        for(int i=0; i<group.getGroupIds().size(); i++){
//            if(i!=0)
//                sql.append(",");
//            sql.append("x?");
//        }
//        if(group.getGroupIds().size()==0){
//            sql.append("null");
//        }
//        sql.append(");");
//        List<Object> params = new ArrayList<>();
//        params.add(mtPECode);
//        for(String groupId : group.getGroupIds()){
//            params.add(Util.remove0x(groupId));
//        }
//        ResultSet rs  = executeSQL(sql.toString(), params);
//        try {
//            while (rs.next()){
//                String bt = rs.getString("BASE_TEMPLATE_CODE_FK_ID");
//                result.add(bt);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        if(result.size()!=1){
//            //TODO throw Error
//        }
        return result.get(0);
    }

    @Override
    public Person getSystemDummyPerson(String personName) {
        Person person = null;
        String SQL = "SELECT * FROM T_PRN_EU_PERSON AS PRN WHERE PRN.FIRST_NAME_TXT = ? ;";
        List<Object> params = new ArrayList<>();
        params.add(personName);
        ResultSet rs = executeSQL(SQL, params);
        try {
            if(rs.next()){
                person = new Person();
                person.setPersonPkId(Util.convertByteToString(rs.getBytes("PRN.PERSON_PK_ID")));
                person.setPrimaryOrgID("ORGANIZATION_FK_ID");
                try {
                    person.setPersonName(rs.getString("PRN.FULL_NAME_LNG_TXT"));
                }catch (Exception e){

                }
                try {
                    person.setPersonName(rs.getString("PRN.LEGAL_FULL_NAME_G11N_BIG_TXT"));
                }catch (Exception e){

                }
                person.setBaseCountryCodeFkId(rs.getString("PRN.BASE_COUNTRY_CODE_FK_ID"));
//                person.setOrgDefaultLocale(rs.getString("ORG_DFLT_LOCALE_CODE_FK_ID"));
//                person.setOrgLocFkID(Util.convertByteToString(rs.getBytes("ORG_LOC_FK_ID")));
            }
            List<String> groupList = getPersonGroups(person.getPersonPkId());
            PersonGroup personGroup = new PersonGroup(person.getPersonPkId(), groupList);
            person.setPersonGroups(personGroup);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return person;
    }

//    private String getG11nValue(String input, String localeCode){
//        return Util.getG11nValueByLocaleCode(input, localeCode);
//    }
}
