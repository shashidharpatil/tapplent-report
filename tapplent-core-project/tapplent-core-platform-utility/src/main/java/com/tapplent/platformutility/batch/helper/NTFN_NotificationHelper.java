package com.tapplent.platformutility.batch.helper;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.NTFN_NotfnDoaMap;
import com.tapplent.platformutility.batch.model.NTFN_NotificationData;
import com.tapplent.platformutility.thirdPartyApp.gcm.helper.Notification;
import org.json.JSONException;
import org.json.JSONObject;


public class NTFN_NotificationHelper {

	public static NTFN_NotificationData setUpNotfn(NTFN_NotificationData item) {

		// (TDWI) fetch all channels for a notification defination

		fetchDataRecord(item);
		fetchDoaMap(item);
		setNotificationChannels(item);

		return item;
	}

	public static void fetchDataRecord(NTFN_NotificationData item) {

		// (TDWI) fetch Data Record from db

		
	}

	public static void fetchDoaMap(NTFN_NotificationData item) {

		// (TDWI) fetch DOA map from db Map<String,NotificationDoaMap>

		
	}
	
	public static void setNotificationChannels(NTFN_NotificationData item) {

		// (TDWI) setNotificationChannels notification_channel_map

		
	}

	public static String fetchURL(String bt, String mtPE, String vt, String cavnavsId, String DepKey) {

		// (TDWI) method to fetch web api endPoint URL for Body

		return "";

	}

	public static String fetchAttachmentifexists(NTFN_NotificationData item) {

		// (TDWI) check for attachment document from
		// T_PFM_ADM_NOTIFICATION_DOC_ATTACH table
		// if present call generate document

		return "";

	}

	public static NTFN_NotificationData setCredentials(NTFN_NotificationData item) {
		// (TDWI) query person record to fetch person credentials
		return item;
	}

	public static Map<String, Object> buildPushNotificationModel(NTFN_NotificationData item, String config)
			throws SQLException, JSONException {
		JSONObject json = new JSONObject(config);
		Map<String, Object> map = new HashMap<String, Object>();
		Notification not = new Notification();
		List<NTFN_NotfnDoaMap> doaMap = item.getDoaMap().get("PUSH_NOTIFICATION");
		if (getData(Constants.notificationText, doaMap, item.getResultSet()) != null) {
			not.setNotificationText(getData(Constants.notificationText, doaMap, item.getResultSet()));
		}
		if (getData(Constants.actions, doaMap, item.getResultSet()) != null) {
			not.setActions(getData(Constants.actions, doaMap, item.getResultSet()));
		}
		if (getData(Constants.smallIcon, doaMap, item.getResultSet()) != null) {
			not.setSmallIcon(getData(Constants.smallIcon, doaMap, item.getResultSet()));
		}
		if (getData(Constants.subText, doaMap, item.getResultSet()) != null) {
			not.setSubText(getData(Constants.subText, doaMap, item.getResultSet()));
		}
		if (getData(Constants.notification_key, doaMap, item.getResultSet()) != null) {
			not.setNotificationKey(getData(Constants.notification_key, doaMap, item.getResultSet()));
		}
		if (getData(Constants.notificationId, doaMap, item.getResultSet()) != null) {
			not.setNotificationId(getData(Constants.notificationId, doaMap, item.getResultSet()));
		}
		if (getData(Constants.title, doaMap, item.getResultSet()) != null) {
			not.setTitle(getData(Constants.title, doaMap, item.getResultSet()));
		}
		map.put("not", not);
		map.put("apiKey", json.get(Constants.apikey));

		return null;
	}

	public static String getOrgConfig(String accountType) {
		// (TDWI) query channelTypeConfig from Channel_Type Table

		return null;
	}

	public static String getData(String key, List<NTFN_NotfnDoaMap> doaMapList, ResultSet rs) throws SQLException {
		String Data = "";
		TreeMap<Integer, String> map = new TreeMap<Integer, String>();
		for (NTFN_NotfnDoaMap doaMap : doaMapList) {
			if (doaMap.getMapDoaExpn().equals(key)) {
				map.put(doaMap.getSequenceNumberPosInt(), rs.getString(doaMap.getMapDoaExpn()));
			}
		}

		for (Entry<Integer, String> entry : map.entrySet()) {
			Data = Data + entry.getValue();
		}

		return Data;

	}

}
