package com.tapplent.platformutility.person.structure;

import com.tapplent.platformutility.layout.valueObject.DOADetails;
import com.tapplent.platformutility.layout.valueObject.PersonPreferenceVO;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tapplent on 8/16/16.
 */
public class Person {

    private String personPkId;
    private String personName;
    private String baseCountryCodeFkId;
    private String primaryOrgID;
    private String orgDefaultLocale;
    private String orgLocFkID;
    private PersonGroup personGroups;
    private PersonPreferenceVO personPreferences;
    private Map<String, String> additionalDoaValues = new HashMap<>();
    private Map<String, String> mtPEToDefaultBTPEMap = new HashMap<>();

    public String getPersonPkId() {
        return personPkId;
    }

    public void setPersonPkId(String personPkId) {
        this.personPkId = personPkId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getBaseCountryCodeFkId() {
        return baseCountryCodeFkId;
    }

    public void setBaseCountryCodeFkId(String baseCountryCodeFkId) {
        this.baseCountryCodeFkId = baseCountryCodeFkId;
    }

    public PersonGroup getPersonGroups() {
        return personGroups;
    }

    public void setPersonGroups(PersonGroup personGroups) {
        this.personGroups = personGroups;
    }

    public PersonPreferenceVO getPersonPreferences() {
        return personPreferences;
    }

    public void setPersonPreferences(PersonPreferenceVO personPreferences) {
        this.personPreferences = personPreferences;
    }

    public Map<String, String> getAdditionalDoaValues() {
        return additionalDoaValues;
    }

    public void setAdditionalDoaValues(Map<String, String> additionalDoaValues) {
        this.additionalDoaValues = additionalDoaValues;
    }

    public Map<String, String> getMtPEToDefaultBTPEMap() {
        return mtPEToDefaultBTPEMap;
    }

    public void setMtPEToDefaultBTPEMap(Map<String, String> mtPEToDefaultBTPEMap) {
        this.mtPEToDefaultBTPEMap = mtPEToDefaultBTPEMap;
    }

    public String getPrimaryOrgID() {
        return primaryOrgID;
    }

    public void setPrimaryOrgID(String primaryOrgID) {
        this.primaryOrgID = primaryOrgID;
    }

    public String getOrgDefaultLocale() {
        return orgDefaultLocale;
    }

    public void setOrgDefaultLocale(String orgDefaultLocale) {
        this.orgDefaultLocale = orgDefaultLocale;
    }

    public String getOrgLocFkID() {
        return orgLocFkID;
    }

    public void setOrgLocFkID(String orgLocFkID) {
        this.orgLocFkID = orgLocFkID;
    }
}
