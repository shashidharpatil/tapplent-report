package com.tapplent.platformutility.uilayout.valueobject;

public class CostCentreBreakUpDataVO {
    private String costCentreBrkUpId;
    private String personId;
    private String costCentreId;
    private String costCentreName;
    private String costCentreType;
    private String costCentreTypeName;

    public String getCostCentreBrkUpId() {
        return costCentreBrkUpId;
    }

    public void setCostCentreBrkUpId(String costCentreBrkUpId) {
        this.costCentreBrkUpId = costCentreBrkUpId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getCostCentreId() {
        return costCentreId;
    }

    public void setCostCentreId(String costCentreId) {
        this.costCentreId = costCentreId;
    }

    public String getCostCentreName() {
        return costCentreName;
    }

    public void setCostCentreName(String costCentreName) {
        this.costCentreName = costCentreName;
    }

    public String getCostCentreType() {
        return costCentreType;
    }

    public void setCostCentreType(String costCentreType) {
        this.costCentreType = costCentreType;
    }

    public String getCostCentreTypeName() {
        return costCentreTypeName;
    }

    public void setCostCentreTypeName(String costCentreTypeName) {
        this.costCentreTypeName = costCentreTypeName;
    }
}
