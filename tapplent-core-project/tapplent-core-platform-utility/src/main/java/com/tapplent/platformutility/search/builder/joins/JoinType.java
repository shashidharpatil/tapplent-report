package com.tapplent.platformutility.search.builder.joins;

public enum JoinType {
	INNER_JOIN(" INNER JOIN "),
	LEFT_OUTER_JOIN(" LEFT OUTER JOIN "),
	RIGHT_OUTER_JOIN(" RIGHT OUTER JOIN "),
	JOIN(" JOIN ");
	private String joinType;
	JoinType(String joinType) {
		this.joinType=joinType;
	}
	public String getJoin(){
		return joinType;
	}
}
