package com.tapplent.platformutility.search.impl;

import java.util.ArrayList;
import java.util.List;

import com.tapplent.platformutility.search.builder.FilterOperator;
import com.tapplent.platformutility.search.builder.expression.Operator;

public class DomainObjectFilter {
	private Operator operator;
	private List<String> value;
	public DomainObjectFilter(){
		this.value = new ArrayList<>();
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public List<String> getValue() {
		return value;
	}
	public void setValue(List<String> value) {
		this.value = value;
	}
	public void addValue(String value){
		this.value.add(value);
	}
}
