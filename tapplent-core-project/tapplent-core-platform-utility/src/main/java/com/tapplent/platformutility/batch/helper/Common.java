package com.tapplent.platformutility.batch.helper;

import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import org.antlr.stringtemplate.language.ArrayWrappedInList;
import org.springframework.batch.core.JobExecution;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sripat on 25/08/16.
 */
public class Common extends TapplentBaseDAO {

    public void updateSchedulerInstancebefore(String schedulerInstancePkId , String  status) throws SQLException {

        String sql = "update T_SCH_IEU_SCHEDULER_INSTANCE set EXEC_STATUS_CODE_FK_ID = '"+status+"' , EXECUTION_START_DATETIME ='"+
                com.tapplent.platformutility.common.util.Common.getCurrentTimeStamp() +"' " +
                "where SCHEDULER_INSTANCE_PK_ID = '"+schedulerInstancePkId+"';";
        List<Object> list = new ArrayList<Object>();
        executeUpdate(sql,list);

    }

    public void updateSchedulerInstanceafter(String schedulerInstancePkId , JobExecution jobExecution) throws SQLException {

        String sql = "update T_SCH_IEU_SCHEDULER_INSTANCE set EXEC_STATUS_CODE_FK_ID = '"+jobExecution.getStatus()+"' , EXECUTION_END_DATETIME ='"+
                com.tapplent.platformutility.common.util.Common.getCurrentTimeStamp() +"', EXIT_CODE_FK_ID ='"+ jobExecution.getExitStatus()+"'," +
                ", EXIT_MESSAGE_BIG_TXT ='"+ jobExecution.getFailureExceptions()+"'"+
                "where SCHEDULER_INSTANCE_PK_ID = '"+schedulerInstancePkId+"';";
        List<Object> list = new ArrayList<Object>();
        executeUpdate(sql,list);

    }


}
