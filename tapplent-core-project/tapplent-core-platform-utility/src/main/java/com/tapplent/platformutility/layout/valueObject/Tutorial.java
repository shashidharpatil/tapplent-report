package com.tapplent.platformutility.layout.valueObject;

/**
 * Created by tapplent on 28/11/17.
 */
public class Tutorial {
    private String whoGroupId;
    private String category;
    private String tutorialVideoId;
    private String tutorialAttachId;
    private String tutorialTitleG11NTxt;
    private String tutotialDescriptionG11NTxt;
    private String tutorialAudienceG11nTxt;
    private boolean isDeleted;
    private String saveStateCodeFkId;
    private String activeStateCodeFkId;
    private String recordStateCodeFkId;
    private String tutorialVideoUrl;
    private String tutorialAttachUrl;
    private IconMasterVO deviceIcon;
    private int seqNumber;

    public IconMasterVO getDeviceIcon() {
        return deviceIcon;
    }

    public void setDeviceIcon(IconMasterVO deviceIcon) {
        this.deviceIcon = deviceIcon;
    }

    public String getTutorialTitleG11NTxt() {
        return tutorialTitleG11NTxt;
    }

    public void setTutorialTitleG11NTxt(String tutorialTitleG11NTxt) {
        this.tutorialTitleG11NTxt = tutorialTitleG11NTxt;
    }

    public String getTutotialDescriptionG11NTxt() {
        return tutotialDescriptionG11NTxt;
    }

    public void setTutotialDescriptionG11NTxt(String tutotialDescriptionG11NTxt) {
        this.tutotialDescriptionG11NTxt = tutotialDescriptionG11NTxt;
    }

    public String getTutorialAudienceG11nTxt() {
        return tutorialAudienceG11nTxt;
    }

    public void setTutorialAudienceG11nTxt(String tutorialAudienceG11nTxt) {
        this.tutorialAudienceG11nTxt = tutorialAudienceG11nTxt;
    }

    public String getWhoGroupId() {
        return whoGroupId;
    }

    public void setWhoGroupId(String whoGroupId) {
        this.whoGroupId = whoGroupId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTutorialVideoId() {
        return tutorialVideoId;
    }

    public void setTutorialVideoId(String tutorialVideoId) {
        this.tutorialVideoId = tutorialVideoId;
    }

    public String getTutorialAttachId() {
        return tutorialAttachId;
    }

    public void setTutorialAttachId(String tutorialAttachId) {
        this.tutorialAttachId = tutorialAttachId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getSaveStateCodeFkId() {
        return saveStateCodeFkId;
    }

    public void setSaveStateCodeFkId(String saveStateCodeFkId) {
        this.saveStateCodeFkId = saveStateCodeFkId;
    }

    public String getActiveStateCodeFkId() {
        return activeStateCodeFkId;
    }

    public void setActiveStateCodeFkId(String activeStateCodeFkId) {
        this.activeStateCodeFkId = activeStateCodeFkId;
    }

    public String getRecordStateCodeFkId() {
        return recordStateCodeFkId;
    }

    public void setRecordStateCodeFkId(String recordStateCodeFkId) {
        this.recordStateCodeFkId = recordStateCodeFkId;
    }

    public String getTutorialVideoUrl() {
        return tutorialVideoUrl;
    }

    public void setTutorialVideoUrl(String tutorialVideoUrl) {
        this.tutorialVideoUrl = tutorialVideoUrl;
    }

    public String getTutorialAttachUrl() {
        return tutorialAttachUrl;
    }

    public void setTutorialAttachUrl(String tutorialAttachUrl) {
        this.tutorialAttachUrl = tutorialAttachUrl;
    }

    public int getSeqNumber() {
        return seqNumber;
    }

    public void setSeqNumber(int seqNumber) {
        this.seqNumber = seqNumber;
    }


}
