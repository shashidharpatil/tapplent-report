package com.tapplent.platformutility.common.cache;

import com.tapplent.platformutility.common.util.TapplentServiceLookupUtil;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
/* TODO optimize tenantAware cache by using time as a factor */
public class TenantAwareCache {
	public synchronized static MetadataObjectRepository getMetaDataRepository(){
		Object attribute = TenantContextHolder.getAttribute(MetadataObjectRepository.class.getName());
		if(null==attribute){
			attribute=TapplentServiceLookupUtil.getMetadataRepository();
			TenantContextHolder.setAttribute(MetadataObjectRepository.class.getName(), attribute);
		}
		return (MetadataObjectRepository) attribute;
	}
	
	public synchronized static ServerPropertyObjectRepository getServerPropertyRepository(){
		Object attribute = TenantContextHolder.getAttribute(ServerPropertyObjectRepository.class.getName());
		if(null==attribute){
			attribute = TapplentServiceLookupUtil.getServerPropertyRepository();
			TenantContextHolder.setAttribute(ServerPropertyObjectRepository.class.getName(), attribute);
		}
		return (ServerPropertyObjectRepository) attribute;
	}
	public synchronized static PersonDetailsObjectRepository getPersonRepository(){
		Object attribute = TenantContextHolder.getAttribute(PersonDetailsObjectRepository.class.getName());
		if(null==attribute){
			attribute = TapplentServiceLookupUtil.getPersonRepository();
			TenantContextHolder.setAttribute(PersonDetailsObjectRepository.class.getName(), attribute);
		}
		return (PersonDetailsObjectRepository) attribute;
	}
}