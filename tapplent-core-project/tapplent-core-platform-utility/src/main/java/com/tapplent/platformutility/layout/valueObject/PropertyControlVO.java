package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class PropertyControlVO {	
	private String propertyControlId;
	private String propertyControlTransactionFkId;
	private String baseTemplateId;
	private String mtPEId;
	private String propertyControlInstanceFkId; 
	private String canvasId;
	private String propertyControlCode;
	private String controlTypeCode;
	JsonNode deviceIndependentBlob;
	JsonNode deviceDependentBlob;
	private String propertyControlAttribute;
	private String aggregateControlFunction;
	JsonNode mstDeviceIndependentBlob;
	JsonNode txnDeviceIndependentBlob;
	JsonNode mstDeviceDependentBlob;
	JsonNode txnDeviceDependentBlob;
	public String getPropertyControlId() {
		return propertyControlId;
	}
	public void setPropertyControlId(String propertyControlId) {
		this.propertyControlId = propertyControlId;
	}
	public String getPropertyControlTransactionFkId() {
		return propertyControlTransactionFkId;
	}
	public void setPropertyControlTransactionFkId(String propertyControlTransactionFkId) {
		this.propertyControlTransactionFkId = propertyControlTransactionFkId;
	}
	public String getBaseTemplateId() {
		return baseTemplateId;
	}
	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}
	public String getMtPEId() {
		return mtPEId;
	}
	public void setMtPEId(String mtPEId) {
		this.mtPEId = mtPEId;
	}
	public String getPropertyControlInstanceFkId() {
		return propertyControlInstanceFkId;
	}
	public void setPropertyControlInstanceFkId(String propertyControlInstanceFkId) {
		this.propertyControlInstanceFkId = propertyControlInstanceFkId;
	}
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public String getPropertyControlCode() {
		return propertyControlCode;
	}
	public void setPropertyControlCode(String propertyControlCode) {
		this.propertyControlCode = propertyControlCode;
	}
	public String getControlTypeCode() {
		return controlTypeCode;
	}
	public void setControlTypeCode(String controlTypeCode) {
		this.controlTypeCode = controlTypeCode;
	}
	public String getAggregateControlFunction() {
		return aggregateControlFunction;
	}
	public void setAggregateControlFunction(String aggregateControlFunction) {
		this.aggregateControlFunction = aggregateControlFunction;
	}
	public JsonNode getMstDeviceIndependentBlob() {
		return mstDeviceIndependentBlob;
	}
	public void setMstDeviceIndependentBlob(JsonNode mstDeviceIndependentBlob) {
		this.mstDeviceIndependentBlob = mstDeviceIndependentBlob;
	}
	public JsonNode getTxnDeviceIndependentBlob() {
		return txnDeviceIndependentBlob;
	}
	public void setTxnDeviceIndependentBlob(JsonNode txnDeviceIndependentBlob) {
		this.txnDeviceIndependentBlob = txnDeviceIndependentBlob;
	}
	public JsonNode getMstDeviceDependentBlob() {
		return mstDeviceDependentBlob;
	}
	public void setMstDeviceDependentBlob(JsonNode mstDeviceDependentBlob) {
		this.mstDeviceDependentBlob = mstDeviceDependentBlob;
	}
	public JsonNode getTxnDeviceDependentBlob() {
		return txnDeviceDependentBlob;
	}
	public void setTxnDeviceDependentBlob(JsonNode txnDeviceDependentBlob) {
		this.txnDeviceDependentBlob = txnDeviceDependentBlob;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public String getPropertyControlAttribute() {
		return propertyControlAttribute;
	}
	public void setPropertyControlAttribute(String propertyControlAttribute) {
		this.propertyControlAttribute = propertyControlAttribute;
	}
}
