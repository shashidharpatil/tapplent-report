package com.tapplent.platformutility.analytics.structure;

/**
 * Created by tapplent on 05/12/16.
 */
public class AnalyticsMetricDetailsFilter {
    private String analyticsMetricDetailFilterPkId;
    private String analyticsMetricDetailFkId;
    private String mtPeCodeFkId;
    private String filterExpn;

    public String getAnalyticsMetricDetailFilterPkId() {
        return analyticsMetricDetailFilterPkId;
    }

    public void setAnalyticsMetricDetailFilterPkId(String analyticsMetricDetailFilterPkId) {
        this.analyticsMetricDetailFilterPkId = analyticsMetricDetailFilterPkId;
    }

    public String getAnalyticsMetricDetailFkId() {
        return analyticsMetricDetailFkId;
    }

    public void setAnalyticsMetricDetailFkId(String analyticsMetricDetailFkId) {
        this.analyticsMetricDetailFkId = analyticsMetricDetailFkId;
    }

    public String getMtPeCodeFkId() {
        return mtPeCodeFkId;
    }

    public void setMtPeCodeFkId(String mtPeCodeFkId) {
        this.mtPeCodeFkId = mtPeCodeFkId;
    }

    public String getFilterExpn() {
        return filterExpn;
    }

    public void setFilterExpn(String filterExpn) {
        this.filterExpn = filterExpn;
    }
}
