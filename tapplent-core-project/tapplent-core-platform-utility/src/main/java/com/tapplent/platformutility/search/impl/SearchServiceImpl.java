package com.tapplent.platformutility.search.impl;

import java.io.IOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Deque;
import java.util.List;
import java.util.Map;

import com.tapplent.platformutility.search.api.SearchData;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.builder.SQLBuilder;
import com.tapplent.platformutility.search.builder.SqlQuery;

public class SearchServiceImpl implements SearchService{
	
	private SearchDAO searchDAO;
	
	public SearchDAO getSearchDAO() {
		return searchDAO;
	}
	public void setSearchDAO(SearchDAO searchDAO) {
		this.searchDAO = searchDAO;
	}
	@Override
	public SearchResult doSearch(SearchData searchData) throws IOException{
		//Here get an instance of SearchContext. Search Context will add the final aliazing.
		//Search Context should be passed to SqlBuilder. SqlBuilder should build the Select, Where and all the clauses and return the final SQL string
		//the SQL should be executed here and return SearchResult
		DefaultSearchData dSearchData = (DefaultSearchData) searchData;
		SearchContext searchContext = SearchContext.getInstance(dSearchData);
		SQLBuilder sqlBuilder = SQLBuilder.getInstance(searchContext);
		String sql = sqlBuilder.build();
		String countSql = sqlBuilder.getCountSql();
		Deque<Object> parameter = searchContext.getParameters();
		Deque<Object> countParameter = searchContext.getCountParameter();
		SearchResult result = searchDAO.searchAndCount(sql, countSql, parameter, countParameter, searchContext, dSearchData);
		return result;
	}

	@Override
	public String getG11nData(String tableName, String columnName, String pkColumnName, String dbPkId, String dbDataTypeCode) {
		return searchDAO.getG11nData(tableName, columnName, pkColumnName, dbPkId, dbDataTypeCode);
	}

	@Override
	public SqlQuery getSqlQuery(DefaultSearchData searchData) {
		SqlQuery sqlQuery = new SqlQuery();
		DefaultSearchData dSearchData = (DefaultSearchData) searchData;
		SearchContext searchContext = SearchContext.getInstance(dSearchData);
		SQLBuilder sqlBuilder = SQLBuilder.getInstance(searchContext);
		String sql = sqlBuilder.build();
		Deque<Object> parameter = searchContext.getParameters();
		sqlQuery.setSql(sql);
		sqlQuery.setParameters(parameter);
		return sqlQuery;
	}

	@Override
	public List<Map<String, Object>> search(String sql, Deque<Object> parameter) {
		SearchResult sr = searchDAO.search(sql, parameter);
		if (sr != null){
			return sr.getData();
		}
		return null;
	}
}