package com.tapplent.platformutility.sql.statement.builder;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.sql.context.model.InsertContextModel;
import com.tapplent.platformutility.sql.context.model.SQLContextModel;

public class InsertStatementBuilder implements SQLStatementBuilder{

	private final InsertContextModel contextModel;
	private EntityMetadataVOX entityMetadataVOX;

	public InsertStatementBuilder(SQLContextModel contextModel,EntityMetadataVOX entityMetadataVOX) {
		this.entityMetadataVOX = entityMetadataVOX;
		this.contextModel = (InsertContextModel) contextModel;
	}

	@Override
	public String build() {
		SQLMyBatisQueryBuilder queryBuilder = new SQLMyBatisQueryBuilder();
		queryBuilder.addInsert(contextModel.getEntityName());
		Map<String, Object> insertValueMap = contextModel.getInsertValueMap();

		if (null != insertValueMap) {
			Set<Entry<String, Object>> entrySet = insertValueMap.entrySet();
			for (Entry<String, Object> entry : entrySet) {
				EntityAttributeMetadata attributeByColumnName = entityMetadataVOX.getAttributeByColumnName(entry.getKey());
				if((entry.getValue() instanceof Map<?, ?> || entry.getValue() instanceof ObjectNode) && attributeByColumnName.getDbDataTypeCode().matches("T_BLOB")){ 
					StringBuilder entryValue = new StringBuilder();
					addMapEntryToSql(entry, queryBuilder, entryValue);
				}
				else queryBuilder.addValues(entry.getKey(),Util.getStringTobeInserted(attributeByColumnName,entry.getValue()));
			}
		}
		return queryBuilder.getQueryString();
	}

	private void addMapEntryToSql(Entry<String, Object> entry, SQLMyBatisQueryBuilder queryBuilder, StringBuilder entryValue) {
		StringBuilder blobString = new StringBuilder();
		blobString.append("COLUMN_CREATE(");
		Object object = entry.getValue();
		if(object instanceof ObjectNode){
			Iterator<Entry<String, JsonNode>> nodeIterator = ((ObjectNode) object).fields();
			while (nodeIterator.hasNext()){
				Entry<String, JsonNode> innerEntry = nodeIterator.next();
				blobString.append("'" + innerEntry.getKey() + "', ");
				EntityAttributeMetadata attributeMetadata = entityMetadataVOX.getAttributeByName(innerEntry.getKey());
				if(attributeMetadata != null)
					blobString.append(Util.getStringTobeInserted(attributeMetadata, innerEntry.getValue()) + ", ");//util T_BLOB will take care blob inside blob with inner blob being G11n only!
				else blobString.append("'" + (innerEntry.getValue().asText()).replace("'", "''") + "', ");
			}
		}
		else{
			Map<String, String> blobMap = (Map<String, String>) object;
			for(Entry<String, String> innerEntry : blobMap.entrySet()){
				blobString.append("'" + innerEntry.getKey() + "', ");
				EntityAttributeMetadata attributeMetadata = entityMetadataVOX.getAttributeByName(innerEntry.getKey());
				blobString.append(Util.getStringTobeInserted(attributeMetadata, innerEntry.getValue())+ ", ");
			}
		}
		blobString.replace(blobString.length()-2, blobString.length(), ")");
		queryBuilder.addValues(entry.getKey(), blobString.toString()); ;
		
		
//				entryValue.append("COLUMN_CREATE(");
//				for(Entry<String, Object> innerEntry : ((Map<String,Object>) entry).entrySet()){
//					EntityAttributeMetadata attributeByColumnName = entityMetadataVOX.getAttributeByColumnName(entry.getKey());
//					entryValue.append(innerEntry.getKey()+",");
//					if(innerEntry.getValue() instanceof Map<?, ?> && !attributeByColumnName.getDbDataTypeCode().equals("T_BLOB"))
//						addMapEntryToSql(innerEntry, queryBuilder, entryValue);
//					else entryValue.append(Util.getStringTobeInserted(attributeByColumnName,innerEntry.getValue())+",");
//				}
//				entryValue.replace(entryValue.length()-2, entryValue.length()-1, ")");
			}


}
