package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 26/04/17.
 */
public class ControlActionTargetSectionSortParamsVO {
    private String sortParamsPkId;
    private String actionTargetFkId;
    private String mtPEAlias;
    private String defaultOnLoadSort;

    public String getSortParamsPkId() {
        return sortParamsPkId;
    }

    public void setSortParamsPkId(String sortParamsPkId) {
        this.sortParamsPkId = sortParamsPkId;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getDefaultOnLoadSort() {
        return defaultOnLoadSort;
    }

    public String getActionTargetFkId() {
        return actionTargetFkId;
    }

    public void setActionTargetFkId(String actionTargetFkId) {
        this.actionTargetFkId = actionTargetFkId;
    }

    public void setDefaultOnLoadSort(String defaultOnLoadSort) {
        this.defaultOnLoadSort = defaultOnLoadSort;
    }
}
