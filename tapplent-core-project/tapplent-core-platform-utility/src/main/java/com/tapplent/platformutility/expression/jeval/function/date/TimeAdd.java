package com.tapplent.platformutility.expression.jeval.function.date;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.expression.jeval.EvaluationConstants;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.expression.jeval.function.Function;
import com.tapplent.platformutility.expression.jeval.function.FunctionConstants;
import com.tapplent.platformutility.expression.jeval.function.FunctionException;
import com.tapplent.platformutility.expression.jeval.function.FunctionHelper;
import com.tapplent.platformutility.expression.jeval.function.FunctionResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by sripad on 14/01/16.
 */
public class TimeAdd implements Function {

	private static final Logger Log = LogFactory.getLogger(DateAdd.class);

    /**
     * Returns the name of the function - "timeadd".
     *
     * @return The name of this function class.
     */
    public String getName() {
        return "timeadd";
    }

    /**
     * Executes the function for the specified argument. This method is called
     * internally by Evaluator.
     *
     * @param evaluator
     *            An instance of Evaluator.
     * @param arguments
     *            A string argument that will be converted into two string
     *            arguments and one integer argument. The first argument is the
     *            string to test, the second argument is the prefix and the
     *            third argument is the index to start at. The string
     *            argument(s) HAS to be enclosed in quotes. White space that is
     *            not enclosed within quotes will be trimmed. Quote characters
     *            in the first and last positions of any string argument (after
     *            being trimmed) will be removed also. The quote characters used
     *            must be the same as the quote characters used by the current
     *            instance of Evaluator. If there are multiple arguments, they
     *            must be separated by a comma (",").
     *
     * @return Returns "1.0" (true) if the string ends with the suffix,
     *         otherwise it returns "0.0" (false). The return value respresents
     *         a Boolean value that is compatible with the Boolean operators
     *         used by Evaluator.
     *
     * @exception FunctionException
     *                Thrown if the argument(s) are not valid for this function.
     */
    public FunctionResult execute(final Evaluator evaluator, final String arguments)
            throws FunctionException {
        String result = null;
        String exceptionMessage = "one string and two time arguments "
                + "argument are required.";

        ArrayList values = FunctionHelper.getStrings(arguments,
                EvaluationConstants.FUNCTION_ARGUMENT_SEPARATOR);
        if (values.size() != 3) {

            throw new FunctionException(exceptionMessage);
        }

        try {
            DateFormat formatter ;
            String argumentOne = FunctionHelper.trimAndRemoveQuoteChars(
                    (String) values.get(0), evaluator.getQuoteCharacter());
            formatter = new SimpleDateFormat(argumentOne, Locale.ENGLISH);
            Date date = formatter.parse(values.get(1).toString());
            Date date1 = formatter.parse(values.get(2).toString());
            long time=date.getTime();

            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);
            int hour = cal.get(Calendar.HOUR);
            int minute = cal.get(Calendar.MINUTE);
            int second = cal.get(Calendar.SECOND);
            Calendar cal1 = Calendar.getInstance();
            cal.setTime(date);
            int hour1 = cal.get(Calendar.HOUR);
            int minute1 = cal.get(Calendar.MINUTE);
            int second1 = cal.get(Calendar.SECOND);
            long timeresult=((hour1*3600000)+(minute1*60000)+(second1*1000))+((hour*3600000)+(minute*60000)+(second*1000));
            result = String.valueOf( String.format("%02d:%02d:%02d",
                    TimeUnit.MILLISECONDS.toHours(timeresult),
                    TimeUnit.MILLISECONDS.toMinutes(timeresult) -
                            TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeresult)),
                    TimeUnit.MILLISECONDS.toSeconds(timeresult) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeresult))));
            Log.debug("date", values.get(1).toString());
            Log.debug("day",values.get(2).toString());
        }  catch (Exception e) {
            throw new FunctionException(exceptionMessage, e);
        }

        return new FunctionResult(result,
                FunctionConstants.FUNCTION_RESULT_TYPE_DATE);
    }
}
