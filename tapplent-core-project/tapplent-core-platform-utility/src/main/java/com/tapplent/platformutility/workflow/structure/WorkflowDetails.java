package com.tapplent.platformutility.workflow.structure;

import java.util.Map; /**
 * Created by Tapplent on 8/22/16.
 */
public class WorkflowDetails {
    public String workflowTxnId;
    public boolean isWorkflowApplicable;
    public String workflowDefId;
    public String workflowTxnStepId;
    public String workflowTxnStepStatus;
    public String workflowActionId;
    public String workflowStatus;
    public String workflowActionComment;

    public static WorkflowDetails fromUnderlyingRecord(Map<String, Object> underlyingRecord, String domainObject) {
        WorkflowDetails workflowDetails = new WorkflowDetails();
        workflowDetails.workflowDefId = (String) underlyingRecord.get(domainObject+".RefWorkflowDefinition");
        workflowDetails.workflowTxnId = (String) underlyingRecord.get(domainObject+".RefWorkflowTxn");
        workflowDetails.workflowStatus = (String) underlyingRecord.get(domainObject+".RefWorkflowTxnStatus");
        workflowDetails.workflowTxnStepId = (String) underlyingRecord.get(domainObject+".RefWorkflowTxnStepID");
        workflowDetails.workflowTxnStepStatus = (String) underlyingRecord.get(domainObject+".RefWorkflowTxnStepStatus");
        return workflowDetails;
    }
}
