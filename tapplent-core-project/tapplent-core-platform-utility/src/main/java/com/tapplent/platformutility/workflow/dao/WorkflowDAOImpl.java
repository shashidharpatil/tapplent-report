package com.tapplent.platformutility.workflow.dao;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.workflow.structure.*;

import java.sql.*;
import java.util.*;

/**
 * Created by Tapplent on 9/16/16.
 */
public class WorkflowDAOImpl extends TapplentBaseDAO implements WorkflowDAO {
    private static final Logger LOG = LogFactory.getLogger(WorkflowDAOImpl.class);

    @Override
    public WorkflowTxn getWorkflowTxnById(String workflowTxnId) throws SQLException {
        String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN WHERE WORKFLOW_TXN_PK_ID = ?";
        Deque<Object> parameter = new ArrayDeque<>();
        parameter.add(workflowTxnId);
        SearchResult rs =  search(sql, parameter);
        WorkflowTxn workflowTxn = null;
        if(rs.getData() != null){
            Map<String,Object> row = rs.getData().get(0);
            workflowTxn = new WorkflowTxn();
            workflowTxn.setWorkflowTxnPkId(row.get("WORKFLOW_TXN_PK_ID").toString());
            workflowTxn.setWorkflowDefinitionFkId(row.get("WORKFLOW_DEFINITION_FK_ID").toString());
            workflowTxn.setDoRowPrimaryKeyTxt(row.get("DO_ROW_PRIMARY_KEY_TXT").toString());
            workflowTxn.setDoRowVersionFkId(row.get("DO_ROW_VERSION_FK_ID").toString());
            workflowTxn.setUnderlyingSrvrActionCodeFkId(row.get("UNDERLYING_SRVR_ACTION_CODE_FK_ID").toString());
            workflowTxn.setWorkflowStatusCodeFkId(row.get("WORKFLOW_STATUS_CODE_FK_ID").toString());
            workflowTxn.setSrcMtPeCodeFkId(row.get("SRC_MT_PE_CODE_FK_ID").toString());
            workflowTxn.setSrcBtCodeFkId(row.get("SRC_BT_CODE_FK_ID").toString());
            workflowTxn.setPropogateFlag((boolean)row.get("IS_PROPOGATE_FLAG"));
        }
        return workflowTxn;
    }

    @Override
    public Map<String, WorkflowStep> getAllWorkflowSteps(String workflowTxnId) throws SQLException {
        String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP A JOIN T_PFM_ADM_WORKFLOW_STEP_DEFN B " +
				"ON B.WORKFLOW_STEP_DEFN_PK_ID = A.WORKFLOW_STEP_DEFN_FK_ID WHERE A.WORKFLOW_TXN_FK_ID = ?;";
        List<Object> parameter = new ArrayList<>();
        parameter.add(Util.remove0x(workflowTxnId));
        ResultSet rs = searchResultSet(sql, parameter);
        Map<String, WorkflowStep> workflowStepMap = new HashMap<>();
        while (rs.next()) {
				WorkflowStep workflowStep = getWorkflowStepFromResultSet(rs);
				workflowStepMap.put(workflowStep.getWorkflowTxnStepPkId(), workflowStep);
		}
        return workflowStepMap;
    }



    @Override
    public SearchResult search(String sql, Deque<Object> parameter) throws SQLException {
        SearchResult sr = new SearchResult();
        PreparedStatement ps = null;
        ResultSet rs = null;
		Connection c = getConnection();
		ps = c.prepareStatement(sql);
		if(parameter!=null){
			int i= 1;
			for(Object param : parameter){
				LOG.debug("Select query parameter number "+i+" => "+param.toString());
				ps.setObject(i,param);
				i++;
			}
		}
		LOG.debug(sql);
		rs = ps.executeQuery();
		ResultSetMetaData meta = rs.getMetaData();
		List<Map<String, Object>> result = new LinkedList<>();
		while(rs.next()){
			Map<String, Object> row = new HashMap<>();
			for(int i = 1; i <= meta.getColumnCount(); i++){
				row.put(meta.getColumnLabel(i), rs.getObject(i));
			}
			result.add(row);
		}
		sr.setData(result);
        return sr;
    }

    @Override
	public ResultSet searchResultSet(String sql, List<Object> parameter) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection c = getConnection();
		ps = c.prepareStatement(sql);
		if(parameter!=null){
			int i= 1;
			for(Object param : parameter){
				LOG.debug("Select query parameter number "+i+" => "+param.toString());
				ps.setObject(i,param);
				i++;
			}
		}
		LOG.debug(sql);
		rs = ps.executeQuery();
		return rs;
	}

    @Override
    public Map<String, Workflow> getInProgressWorkflows() throws SQLException {
        String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN WHERE WORKFLOW_STATUS_CODE_FK_ID = 'IN_PROGRESS';";//Join if definition has some additional information.
        ResultSet rs = searchResultSet(sql, null);
        Map<String, Workflow> inProgressWorkflows = null;
        while (rs.next()){
            inProgressWorkflows = new HashMap<>();
			Workflow workflow = getWorkflowFromResultSet(rs);
			inProgressWorkflows.put(workflow.getWorkflowTxnPkId(), workflow);
        }
        return inProgressWorkflows;
    }

	private Workflow getWorkflowFromResultSet(ResultSet rs) throws SQLException {
		Workflow workflow = new Workflow();
		workflow.setWorkflowTxnPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_PK_ID")));
		workflow.setWorkflowDefinitionFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_DEFINITION_FK_ID")));
		workflow.setDoRowPrimaryKeyTxt(Util.convertByteToString(rs.getBytes("DO_ROW_PRIMARY_KEY_TXT")));
		workflow.setDoRowVersionFkId(Util.convertByteToString(rs.getBytes("DO_ROW_VERSION_FK_ID")));
		workflow.setWorkflowStatusCodeFkId(rs.getString("WORKFLOW_STATUS_CODE_FK_ID"));
		workflow.setBtCodeFkId(rs.getString("BT_CODE_FK_ID"));
		workflow.setMtPE(rs.getString("BT_CODE_FK_ID"));//fixme mtpe
		workflow.setBusinessRule(rs.getString("BUSS_RULE_CODE_FK_ID"));
		workflow.setBtCodeFkId(rs.getString("DISPLAY_CONTEXT_VALUE_G11N_BIG_TXT"));
		workflow.setDoIconCodeFkId(rs.getString("DO_ICON_CODE_FK_ID"));
		workflow.setDoNameG11nBigText(rs.getString("DO_NAME_G11N_BIG_TXT"));
		workflow.setPropogateFlag(rs.getBoolean("IS_PROPOGATE_FLAG"));
		return workflow;
	}

	@Override
	public Workflow getInProgressWorkflow() throws SQLException{
		String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN INNER JOIN T_PFM_ADM_BUSINESS_RULE ON BUSS_RULE_CODE_PK_ID = BUSS_RULE_CODE_FK_ID WHERE WORKFLOW_STATUS_CODE_FK_ID = 'IN_PROGRESS' LIMIT 1;";//Join if definition has some additional information.
		ResultSet rs = searchResultSet(sql, null);
		Workflow workflow = null;
		if(rs.next()){
			workflow = getWorkflowFromResultSet(rs);
		}
		return workflow;
	}

	@Override
	public WorkflowActorAction getWorkflowDefaultActionForActor(String workflowTxnStepActorPkId) throws SQLException {
		String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION T JOIN T_PFM_ADM_WORKFLOW_ACTION_DEFN ON WORKFLOW_ACTION_DEFN_PK_ID = WORKFLOW_ACTION_DEFN_FK_ID WHERE WORKFLOW_TXN_STEP_ACTOR_FK_ID = x? AND IS_ACTION_TO_DFLT_EXEC_ON_FORCED_MOVE = TRUE ;";
		WorkflowActorAction workflowActorAction = null;
		List<Object> parameter = new ArrayList<>();
		parameter.add(Util.remove0x(workflowTxnStepActorPkId));
		ResultSet rs = searchResultSet(sql, parameter);
		if (rs.next()) {
			workflowActorAction = getWorkflowActionFromResultSet(rs);
		}
		return workflowActorAction;
	}

	private WorkflowActorAction getWorkflowActionFromResultSet(ResultSet rs) throws SQLException {
		WorkflowActorAction wfActorAction = new WorkflowActorAction();
		wfActorAction.setWorkflowTxnActorPossibleActionPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID")));
		wfActorAction.setWorkflowActionDefnFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_DEFN_FK_ID")));
		wfActorAction.setWorkflowTxnStepActorFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_ACTOR_FK_ID")));
		wfActorAction.setActionVisualMasterCodeFkId(rs.getString("ACTN_VISUAL_MASTER_CODE_FK_ID"));
		wfActorAction.setTargetWflowTxnStepFkId(Util.convertByteToString(rs.getBytes("TARGET_WFLOW_TXN_STEP_FK_ID")));
		wfActorAction.setTargetStepActorDefnFkId(Util.convertByteToString(rs.getBytes("TARGET_STEP_ACTOR_DEFN_FK_ID")));
		wfActorAction.setIfActionConditionExpn(rs.getString("T.IF_ACTION_CONDITION_EXPN"));
		wfActorAction.setActionCommentMandatory(rs.getBoolean("IS_COMMENT_MANDATORY_FLAG"));
		wfActorAction.setActionVisibleThruExitCritEvln(rs.getBoolean("IS_ACTION_VISIBLE_THRU_EXIT_CRIT_EVLN"));
		wfActorAction.setWorkflowDefinitionFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_DEFINITION_FK_ID")));
		wfActorAction.setActionDispSeqNumPosInt(rs.getInt("ACTION_DISP_SEQ_NUM_POS_INT"));
		wfActorAction.setSourceActorFkId(Util.convertByteToString(rs.getBytes("SOURCE_ACTOR_FK_ID")));
		wfActorAction.setTargetActorFkId(Util.convertByteToString(rs.getBytes("TARGET_ACTOR_FK_ID")));
		wfActorAction.setTargetStepFkId(Util.convertByteToString(rs.getBytes("TARGET_STEP_FK_ID")));
		wfActorAction.setActionToDlftExecOnForcedMove(rs.getBoolean("IS_ACTION_TO_DFLT_EXEC_ON_FORCED_MOVE"));
		wfActorAction.setActionDelegateable(rs.getBoolean("IS_ACTION_DELEGATEABLE"));
		wfActorAction.setAutoExecIfActorEqualsSubject(rs.getBoolean("IS_AUTO_EXEC_IF_ACTOR_EQUALS_SUBJECT"));
		wfActorAction.setAutoExecuteIfActorEqualToInitiator(rs.getBoolean("IS_AUTO_EXECUTE_IF_ACTOR_EQUAL_TO_INITIATOR"));
		wfActorAction.setAutoExecuteIfDuplicate(rs.getBoolean("IS_AUTO_EXECUTE_IF_DUPLICATE"));
		wfActorAction.setExitCountCriteriaForActionVisibilityExpn(rs.getString("EXIT_COUNT_CRITERIA_FOR_ACTN_VSBLTY_EXPN"));
		wfActorAction.setExitCountCriteriaForStepTrnsnExpn(rs.getString("EXIT_COUNT_CRITERIA_FOR_STP_TRNSN_EXPN"));
//		wfActorAction.setCrtriaCntrlgSrcStepExit(rs.getBoolean("IS_CRTRIA_CNTRLG_SRC_STEP_EXIT"));
//		wfActorAction.setCrtriaCntrlgTgtStepEntryVsblty(rs.getBoolean("IS_CRTRIA_CNTRLG_TGT_STEP_ENTRY_VSBLTY"));
		wfActorAction.setActionExitSrcActNtfnId(Util.convertByteToString(rs.getBytes("ACTION_EXIT_SRC_ACT_NTFN_ID")));
		wfActorAction.setActionExitTrgtActNtfnId(Util.convertByteToString(rs.getBytes("ACTION_EXIT_TRGT_ACT_NTFN_ID")));
		return wfActorAction;
	}

	@Override
    public Map<String, Map<String, WorkflowStep>> getRemainingWorkflowSteps(Set<String> workflowTxnIds) throws SQLException {
        StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP T LEFT JOIN T_PFM_ADM_WORKFLOW_STEP D ");
        sql.append("ON T.WORKFLOW_STEP_DEFN_FK_ID = D.WORKFLOW_STEP_PK_ID WHERE STEP_STATUS_CODE_FK_ID IN ('IN_PROGRESS', 'NOT_STARTED') ");
        sql.append("AND WORKFLOW_TXN_FK_ID IN (");
        for(String workflowTxnFkId : workflowTxnIds){
            sql.append("'"+workflowTxnFkId+"', ");
        }
        sql.replace(sql.length()-2, sql.length(), ");");
        SearchResult sr = search(sql.toString(), null);
        Map<String, Map<String, WorkflowStep>> workflowToStepTxnMap = null;  //wfTxnId->wfStepTxnId->wfStepTxn
//        if(sr.getData() != null){
//            workflowToStepTxnMap = new HashMap<>();
//            for(Map<String, Object> row : sr.getData()){
//                WorkflowStep workflowStep = new WorkflowStep();
//                workflowStep.setWorkflowTxnStepPkId(row.get("WORKFLOW_TXN_STEP_PK_ID").toString());
//                workflowStep.setWorkflowTxnfkId(row.get("WORKFLOW_TXN_FK_ID").toString());
//                workflowStep.setWorkflowStepDefnFkId(row.get("WORKFLOW_STEP_DEFN_FK_ID").toString());
//                workflowStep.setStepActualStartDatetime((Timestamp)row.get("STEP_ACTUAL_START_DATETIME"));
//                workflowStep.setStepActualEndDatetime((Timestamp)row.get("STEP_ACTUAL_END_DATETIME"));
//                workflowStep.setStepStatusCodeFkId(row.get("STEP_STATUS_CODE_FK_ID").toString());
//                workflowStep.setEligibleToViewRecord((boolean)row.get("IS_ELIGIBLE_TO_VIEW_RECORD"));
//                workflowStep.setStepSeqNumPosInt((int)row.get("STEP_SEQ_NUM_POS_INT"));
//                workflowStep.setStartDateEnforced((boolean)row.get("IS_START_DATE_ENFORCED"));
//                workflowStep.setDueDateEnforced((boolean)row.get("IS_DUE_DATE_ENFORCED"));
//                workflowStep.setStepActorResolutionCodeFkId(row.get("STEP_ACTOR_RESOLUTION_CODE_FK_ID").toString());
//                workflowStep.setSourceActorExitCountExpn(row.get("SOURCE_ACTOR_EXIT_COUNT_EXPN").toString());
//                if(workflowToStepTxnMap.containsKey(workflowStep.getWorkflowTxnfkId())){
//                    workflowToStepTxnMap.get(workflowStep.getWorkflowTxnfkId()).put(workflowStep.getWorkflowTxnStepPkId(), workflowStep);
//                }
//                else{
//                    Map<String, WorkflowStep> workflowStepMap = new HashMap<>();
//                    workflowStepMap.put(workflowStep.getWorkflowTxnStepPkId(), workflowStep);
//                    workflowToStepTxnMap.put(workflowStep.getWorkflowTxnfkId(), workflowStepMap);
//                }
//            }
//        }
        return workflowToStepTxnMap;
    }

    @Override
    public Map<String, Map<String, WorkflowActor>> getAssociatedActors(Collection<Map<String, WorkflowStep>> stepsforTxns) {
        StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_ADM_WORKFLOW_ACTOR WHERE WORKFLOW_STEP_DEP_FK_ID IN (");
        for(Map<String, WorkflowStep> stepsForTxn : stepsforTxns){
            for(Map.Entry<String, WorkflowStep> step : stepsForTxn.entrySet()){
                sql.append("'"+step.getKey()+"', ");
            }
        }
        sql.replace(sql.length()-2, sql.length(), ");");
        Map<String, Map<String,WorkflowActor>> workflowStepToActorMap = null;
//        SearchResult sr = search(sql.toString(), null);
//        if(sr.getData() != null){
//            workflowStepToActorMap = new HashMap<>();
//            for(Map<String, Object> row : sr.getData()){
//                WorkflowActor workflowActor = new WorkflowActor();
//                workflowActor.setWorkflowActorDefnPkId(row.get("WORKFLOW_ACTOR_PK_ID").toString());
//                workflowActor.setWorkflowStepFkId(row.get("WORKFLOW_STEP_DEP_FK_ID").toString());
//                workflowActor.setActorDispSeqNumPosInt((int)row.get("ACTOR_DISP_SEQ_NUM_POS_INT"));
//                workflowActor.setActorNameG11nBigTxt(row.get("ACTOR_NAME_G11N_BLOB").toString());
//                workflowActor.setActorIconCodeFkId(row.get("ACTOR_ICON_CODE_FK_ID").toString());
//                workflowActor.setActorPersonGroupFkId(row.get("ACTOR_PERSON_GROUP_FK_ID").toString());
//                workflowActor.setRelativeToLoggedInUser((boolean)row.get("IS_RELATIVE_TO_LOGGED_IN_USER"));
//                workflowActor.setRelativeToSubjectUser((boolean)row.get("IS_RELATIVE_TO_SUBJECT_USER"));
//                workflowActor.setRelativeToActorId(row.get("RELATIVE_TO_ACTOR_ID").toString());
//                workflowActor.setRelativeRelationshipCodeFkId(row.get("RELATIVE_RELATIONSHIP_CODE_FK_ID").toString());
//                workflowActor.setRelativeRelationshipHierarchyLevelPosInt((int)row.get("RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT"));
//                workflowActor.setResolveActorAsOfDatetime((Timestamp)row.get("RESOLVE_ACTOR_AS_OF_DATETIME"));
//                workflowActor.setAdhocUserActor((boolean)row.get("IS_ADHOC_USER_ACTOR"));
//                workflowActor.setAllowExternalUser((boolean)row.get("ALLOW_EXTERNAL_USER"));
//                workflowActor.setNoActorsActionCodeFkId(row.get("NO_ACTORS_ACTION_CODE_FK_ID").toString());
//                if(workflowStepToActorMap.containsKey(workflowActor.getWorkflowStepFkId())){
//                    workflowStepToActorMap.get(workflowActor.getWorkflowStepFkId()).put(workflowActor.getWorkflowActorDefnPkId(), workflowActor);
//                }
//                else{
//                    Map<String, WorkflowActor> workflowActorMap = new HashMap<>();
//                    workflowActorMap.put(workflowActor.getWorkflowActorDefnPkId(), workflowActor);
//                    workflowStepToActorMap.put(workflowActor.getWorkflowStepFkId(), workflowActorMap);
//                }
//            }
//        }
        return workflowStepToActorMap;
    }

    @Override
    public Map<String, Map<String, WorkflowActorAction>> getAssociatedActorActions(Map<String, Map<String, WorkflowStep>> remainingWorkflowSteps, Map<String, Map<String, WorkflowActor>> associatedActors) {
        StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTION T LEFT JOIN T_PFM_ADM_WORKFLOW_ACTOR_ACTION D ");
        sql.append("ON T.WORKFLOW_ACTOR_ACTION_DEFN_FK_ID = D.WORKFLOW_ACTOR_ACTION_PK_ID WHERE WORKFLOW_TXN_FK_ID = ? AND SRC_ACTOR_FK_ID = ? ;");
        Deque<Object> parameter = new ArrayDeque<>();
        Map<String, Map<String, WorkflowActorAction>> stepToActorActionMap = new HashMap<>();
//        for(Map.Entry<String, Map<String, WorkflowStep>> txnEntry : remainingWorkflowSteps.entrySet()){
//            parameter.add(txnEntry.getKey());
//            for(Map.Entry<String, WorkflowStep> stepEntry : txnEntry.getValue().entrySet()){
//                Map<String, WorkflowActor> stepActors =  associatedActors.get(stepEntry.getKey());
//                Map<String, WorkflowActorAction> workflowActorActionMap = new HashMap<>();
//                for(Map.Entry<String, WorkflowActor> actorEntry : stepActors.entrySet()){//Fixme only one actor entry should have auto-move as true.
//                    parameter.add(actorEntry.getKey());
//                    SearchResult sr = search(sql.toString(), parameter);
//                    if(sr.getData() != null){
////                        Map<String, WorkflowActorAction> workflowActorActionMap = new HashMap<>();
//                        for(Map<String, Object> row : sr.getData()) {
//                            WorkflowActorAction workflowActorAction = new WorkflowActorAction();
//                            workflowActorAction.setWorkflowTxnActionPkId(row.get("WORKFLOW_TXN_ACTION_PK_ID").toString());
//                            workflowActorAction.setWorkflowTxnFkId(row.get("WORKFLOW_TXN_FK_ID").toString());
//                            workflowActorAction.setWorkflowActorActionDefnFkId(row.get("WORKFLOW_ACTOR_ACTION_DEFN_FK_ID").toString());
//                            workflowActorAction.setSrcActrPersonFkId(row.get("SRC_ACTR_PERSON_FK_ID").toString());
//                            workflowActorAction.setSrcActorFkId(row.get("SRC_ACTOR_FK_ID").toString());
//                            workflowActorAction.setSrcActrActlRcvDatetime((Timestamp) row.get("SRC_ACTR_ACTL_RCV_DATETIME"));
//                            workflowActorAction.setSrcActrActlCmpltDatetime((Timestamp) row.get("SRC_ACTR_ACTL_CMPLT_DATETIME"));
//                            workflowActorAction.setSrcActrActionStatusCodeFkId(row.get("SRC_ACTR_ACTION_STATUS_CODE_FK_ID").toString());
//                            workflowActorAction.setSrcActionCommentLngTxt(row.get("SRC_ACTION_COMMENT_LNG_TXT").toString());
//                            workflowActorAction.setCommentMandatoryFlag((boolean) row.get("IS_COMMENT_MANDATORY_FLAG"));
//                            workflowActorAction.setActionEnabled((boolean) row.get("IS_ACTION_ENABLED"));
//                            workflowActorAction.setActionToDlftExecOnForcedMove((boolean) row.get("IS_ACTION_TO_DLFT_EXEC_ON_FORCED_MOVE"));
//                            workflowActorAction.setActionDelegateable((boolean) row.get("IS_ACTION_DELEGATEABLE"));
//                            workflowActorAction.setAutoExecIfActorEqualsSubject((boolean) row.get("IS_AUTO_EXEC_IF_ACTOR_EQUALS_SUBJECT"));
//                            workflowActorAction.setAutoExecuteIfActorEqualToInitiator((boolean) row.get("IS_AUTO_EXECUTE_IF_ACTOR_EQUAL_TO_INITIATOR"));
//                            workflowActorAction.setAutoExecuteIfDuplicate((boolean) row.get("IS_AUTO_EXECUTE_IF_DUPLICATE"));
//                            workflowActorAction.setOnActionCompleteExecExpn(row.get("ON_ACTION_COMPLETE_EXEC_EXPN").toString());
//                            workflowActorAction.setOnActionCancelExecExpn(row.get("ON_ACTION_CANCEL_EXEX_EXPN").toString());
//                            workflowActorAction.setAllowSrcActrActnVsbltyWhnIneligible((boolean) row.get("ALLOW_SRC_ACTR_ACTN_VSBLTY_WHN_INELIGIBLE"));
//                            workflowActorActionMap.put(workflowActorAction.getWorkflowTxnActionPkId(), workflowActorAction);
//                            workflowActorActionMap.put(workflowActorAction.getWorkflowTxnActionPkId(), workflowActorAction);
//                        }
//                    }
//                    parameter.removeLast();
//                }
//                stepToActorActionMap.put(stepEntry.getKey(), workflowActorActionMap);
//            }
//            parameter.removeLast();
//        }
        return stepToActorActionMap;
    }

	@Override
	public WorkflowActorAction getWorkflowActorActionDetails(String workflowActionId, String workflowDefnId) throws SQLException {
	    	WorkflowActorAction wfActorAction = null;
	    	String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION T JOIN T_PFM_ADM_WORKFLOW_ACTION_DEFN D ON WORKFLOW_ACTION_DEFN_PK_ID = WORKFLOW_ACTION_DEFN_FK_ID WHERE WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID = x?";//D.ACTN_VISUAL_MASTER_CODE_FK_ID = ? AND WORKFLOW_DEFINITION_FK_ID = x?";
			List<Object> parameter = new ArrayList<>();
			parameter.add(Util.remove0x(workflowActionId));
//			parameter.add(Util.remove0x(workflowDefnId));
			ResultSet rs = searchResultSet(sql, parameter);
			if(rs.next()){
//				Map<String, Object> actionMap = rs.get(0);//check starting index
				wfActorAction = new WorkflowActorAction();
				wfActorAction.setWorkflowTxnActorPossibleActionPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID")));
				wfActorAction.setWorkflowActionDefnFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_DEFN_FK_ID")));
				wfActorAction.setWorkflowTxnStepActorFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_ACTOR_FK_ID")));
				wfActorAction.setActionVisualMasterCodeFkId(rs.getString("ACTN_VISUAL_MASTER_CODE_FK_ID"));
				wfActorAction.setTargetWflowTxnStepFkId(Util.convertByteToString(rs.getBytes("TARGET_WFLOW_TXN_STEP_FK_ID")));
				wfActorAction.setTargetStepActorDefnFkId(Util.convertByteToString(rs.getBytes("TARGET_STEP_ACTOR_DEFN_FK_ID")));
				wfActorAction.setIfActionConditionExpn(rs.getString("T.IF_ACTION_CONDITION_EXPN"));
				wfActorAction.setActionCommentMandatory(rs.getBoolean("T.IS_COMMENT_MANDATORY_FLAG"));
				wfActorAction.setActionVisibleThruExitCritEvln(rs.getBoolean("IS_ACTION_VISIBLE_THRU_EXIT_CRIT_EVLN"));
				wfActorAction.setWorkflowDefinitionFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_DEFINITION_FK_ID")));
				wfActorAction.setActionDispSeqNumPosInt(rs.getInt("ACTION_DISP_SEQ_NUM_POS_INT"));
				wfActorAction.setSourceActorFkId(Util.convertByteToString(rs.getBytes("SOURCE_ACTOR_FK_ID")));
				wfActorAction.setTargetActorFkId(Util.convertByteToString(rs.getBytes("TARGET_ACTOR_FK_ID")));
				wfActorAction.setTargetStepFkId(Util.convertByteToString(rs.getBytes("TARGET_STEP_FK_ID")));
//				wfActorAction.setExitCriteriaCheckApplicable(rs.getBoolean("IS_EXIT_CRITERIA_CHECK_APPLICABLE"));
				wfActorAction.setActionToDlftExecOnForcedMove(rs.getBoolean("IS_ACTION_TO_DFLT_EXEC_ON_FORCED_MOVE"));
				wfActorAction.setActionDelegateable(rs.getBoolean("IS_ACTION_DELEGATEABLE"));
				wfActorAction.setAutoExecIfActorEqualsSubject(rs.getBoolean("IS_AUTO_EXEC_IF_ACTOR_EQUALS_SUBJECT"));
				wfActorAction.setAutoExecuteIfActorEqualToInitiator(rs.getBoolean("IS_AUTO_EXECUTE_IF_ACTOR_EQUAL_TO_INITIATOR"));
				wfActorAction.setAutoExecuteIfDuplicate(rs.getBoolean("IS_AUTO_EXECUTE_IF_DUPLICATE"));
				wfActorAction.setExitCountCriteriaForActionVisibilityExpn(rs.getString("EXIT_COUNT_CRITERIA_FOR_ACTN_VSBLTY_EXPN"));
				wfActorAction.setExitCountCriteriaForStepTrnsnExpn(rs.getString("EXIT_COUNT_CRITERIA_FOR_STP_TRNSN_EXPN"));
//				wfActorAction.setCrtriaCntrlgSrcStepExit(rs.getBoolean("IS_CRTRIA_CNTRLG_SRC_STEP_EXIT"));
//				wfActorAction.setCrtriaCntrlgTgtStepEntryVsblty(rs.getBoolean("IS_CRTRIA_CNTRLG_TGT_STEP_ENTRY_VSBLTY"));
				wfActorAction.setActionExitSrcActNtfnId(rs.getString("ACTION_EXIT_SRC_ACT_NTFN_ID"));
				wfActorAction.setActionExitTrgtActNtfnId(rs.getString("ACTION_EXIT_TRGT_ACT_NTFN_ID"));
			}		  			  
			return wfActorAction;
	}
	
	public List<WorkflowActor> getResolvedTargetPersonActorByAction(WorkflowActorAction workflowActorAction, String workflowTxnId) throws SQLException {
		//fixme if the source action's step and the target actor's step are different (but must be resolved state !) then use target workflow steps' Txn ID.
    	List<WorkflowActor> actorTxns = new ArrayList<>();
//    	String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR A JOIN T_PFM_ADM_WORKFLOW_ACTOR_DEFN B ON A.STEP_ACTOR_DEFN_FK_ID = B.WORKFLOW_ACTOR_DEFN_PK_ID WHERE STEP_ACTOR_DEFN_FK_ID = x? AND WORKFLOW_TXN_STEP_FK_ID = x? ;";
		String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR JOIN T_PFM_ADM_WORKFLOW_ACTOR_DEFN ON WORKFLOW_ACTOR_DEFN_PK_ID = STEP_ACTOR_DEFN_FK_ID " +
				"JOIN T_PFM_IEU_WORKFLOW_TXN_STEP ON WORKFLOW_TXN_STEP_PK_ID = WORKFLOW_TXN_STEP_FK_ID " +
				"JOIN T_PFM_IEU_WORKFLOW_TXN ON WORKFLOW_TXN_PK_ID = WORKFLOW_TXN_FK_ID " +
				"WHERE STEP_ACTOR_DEFN_FK_ID = x? AND WORKFLOW_TXN_PK_ID = x?;";
		List<Object> parameter = new ArrayList<>();
		parameter.add(Util.remove0x(workflowActorAction.getTargetStepActorDefnFkId()));
		parameter.add(Util.remove0x(workflowTxnId));
		ResultSet rs = searchResultSet(sql, parameter);
		if (rs.next()){
			do{
				WorkflowActor workflowActor = new WorkflowActor();
				workflowActor.setWorkflowTxnStepActorPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_ACTOR_PK_ID")));
				workflowActor.setWorkflowTxnStepFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_FK_ID")));
				workflowActor.setStepActorDefnFkId(Util.convertByteToString(rs.getBytes("STEP_ACTOR_DEFN_FK_ID")));
				workflowActor.setResolvedPersonFkId(Util.convertByteToString(rs.getBytes("RESOLVED_PERSON_FK_ID")));
				workflowActor.setStepActorStatusCodeFkId(rs.getString("STEP_ACTOR_STATUS_CODE_FK_ID"));
				workflowActor.setCreatedDatetime(rs.getTimestamp("CREATED_DATETIME"));
				workflowActor.setLastModifiedDatetime(rs.getTimestamp("LAST_MODIFIED_DATETIME"));
				workflowActor.setWorkflowStepDefnFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_STEP_DEFN_FK_ID")));
				workflowActor.setActorDispSeqNumPosInt(rs.getInt("ACTOR_DISP_SEQ_NUM_POS_INT"));
				workflowActor.setActorNameG11nBigTxt(rs.getString("ACTOR_NAME_G11N_BIG_TXT"));
				workflowActor.setActorIconCodeFkId(rs.getString("ACTOR_ICON_CODE_FK_ID"));
				workflowActor.setActorPersonGroupFkId(Util.convertByteToString(rs.getBytes("ACTOR_PERSON_GROUP_FK_ID")));
				workflowActor.setRelativeToLoggedInUser(rs.getBoolean("IS_RELATIVE_TO_LOGGED_IN_USER"));
				workflowActor.setRelativeToSubjectUser(rs.getBoolean("IS_RELATIVE_TO_SUBJECT_USER"));
				workflowActor.setRelativeToActorId(Util.convertByteToString(rs.getBytes("RELATIVE_TO_ACTOR_FK_ID")));
				workflowActor.setRelativeRelationshipCodeFkId(rs.getString("RELATIVE_RELATIONSHIP_CODE_FK_ID"));
				workflowActor.setRelativeRelationshipHierarchyLevelPosInt(rs.getInt("RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT"));
				workflowActor.setResolveActorAsOfDatetime(rs.getTimestamp("RESOLVE_ACTOR_AS_OF_DATETIME"));
				workflowActor.setAdhocUserActor(rs.getBoolean("IS_ADHOC_USER_ACTOR"));
				workflowActor.setAllowExternalUser(rs.getBoolean("ALLOW_EXTERNAL_USER"));
				workflowActor.setNoActorsActionCodeFkId(rs.getString("NO_ACTORS_ACTION_CODE_FK_ID"));
				
				actorTxns.add(workflowActor);
			}while (rs.next());
		}
//		else{
//			List<WorkflowActor> resolvedActors = workflowUtility.resolveActors(null, workflowStep);
//			fixme this should be done here? as resloving actor is not right place to do here.
//		}
		return actorTxns;
	}
	
	public List<WorkflowActor> getResolvedSourcePersonActorFromWholeStepByActorTxn(String workflowTxnStepActorId) throws SQLException {
    	List<WorkflowActor> sourcePersonActorList = new ArrayList<>();
    	String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION JOIN T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR ON WORKFLOW_TXN_STEP_ACTOR_FK_ID = WORKFLOW_TXN_STEP_ACTOR_PK_ID WHERE "
				+ "WORKFLOW_TXN_STEP_FK_ID IN (SELECT WORKFLOW_TXN_STEP_FK_ID FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION JOIN T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR "
				+ "ON WORKFLOW_TXN_STEP_ACTOR_FK_ID = WORKFLOW_TXN_STEP_ACTOR_PK_ID WHERE WORKFLOW_TXN_STEP_ACTOR_FK_ID = x? )";
		List<Object> parameter = new ArrayList<>();
		parameter.add(Util.remove0x(workflowTxnStepActorId));
		ResultSet entry = searchResultSet(sql, parameter);
		while (entry.next()){
				WorkflowActor actor = new WorkflowActor();
				actor.setWorkflowTxnStepActorPkId(Util.convertByteToString(entry.getBytes("WORKFLOW_TXN_STEP_ACTOR_PK_ID")));
				actor.setWorkflowTxnStepFkId(Util.convertByteToString(entry.getBytes("WORKFLOW_TXN_STEP_FK_ID")));
				actor.setStepActorDefnFkId(Util.convertByteToString(entry.getBytes("STEP_ACTOR_DEFN_FK_ID")));
				actor.setResolvedPersonFkId(Util.convertByteToString(entry.getBytes("RESOLVED_PERSON_FK_ID")));
				actor.setStepActorStatusCodeFkId(entry.getString("STEP_ACTOR_STATUS_CODE_FK_ID"));
				actor.setCreatedDatetime(entry.getTimestamp("CREATED_DATETIME"));
				actor.setLastModifiedDatetime(entry.getTimestamp("LAST_MODIFIED_DATETIME"));
				sourcePersonActorList.add(actor);
		}
		return sourcePersonActorList;
	}

	public WorkflowStep getWorkflowTargetStepByAction(WorkflowActorAction workflowActorAction) throws SQLException {
    	String workflowStepTxnId = workflowActorAction.getTargetWflowTxnStepFkId();
    	return getWorkflowStep(workflowStepTxnId);
	}

	public WorkflowStep getWorkflowStep(String workflowStepTxnId) throws SQLException {
		WorkflowStep workflowStep = null;
		String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP A JOIN T_PFM_ADM_WORKFLOW_STEP_DEFN B ON A.WORKFLOW_STEP_DEFN_FK_ID = B.WORKFLOW_STEP_DEFN_PK_ID WHERE WORKFLOW_TXN_STEP_PK_ID = x?;";
		List<Object> parameter = new ArrayList<>();
		parameter.add(Util.remove0x(workflowStepTxnId));
		ResultSet rs = searchResultSet(sql, parameter);
		if(rs.next()){
			workflowStep = getWorkflowStepFromResultSet(rs);
		}
		return workflowStep;
	}

	private WorkflowStep getWorkflowStepFromResultSet(ResultSet rs) throws SQLException {
		WorkflowStep workflowStep = new WorkflowStep();
		workflowStep.setWorkflowTxnStepPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_PK_ID")));
		workflowStep.setWorkflowTxnFkId(Util.convertByteToString(rs.getBytes("A.WORKFLOW_TXN_FK_ID")));
		workflowStep.setWorkflowStepDefnFkId(Util.convertByteToString(rs.getBytes("A.WORKFLOW_STEP_DEFN_FK_ID")));
		workflowStep.setStepActualStartDatetime(rs.getTimestamp("A.STEP_ACTUAL_START_DATETIME"));
		workflowStep.setStepActualEndDatetime(rs.getTimestamp("A.STEP_ACTUAL_END_DATETIME"));
		workflowStep.setStepStatusCodeFkId(rs.getString("A.STEP_STATUS_CODE_FK_ID"));
		workflowStep.setStartDateExpn(rs.getString("START_DATE_EXPN"));
		workflowStep.setDueDateExpn(rs.getString("DUE_DATE_EXPN"));
		workflowStep.setStartDateEnforced(rs.getBoolean("IS_START_DATE_ENFORCED"));
		workflowStep.setDueDateEnforced(rs.getBoolean("IS_DUE_DATE_ENFORCED"));
		workflowStep.setStepActorResolutionCodeFkId(rs.getString("STEP_ACTOR_RESOLUTION_CODE_FK_ID"));
		workflowStep.setAllowTrgtActrRecVsbltyWhnEntryCritNotFulfilled(rs.getBoolean("ALLOW_TRGT_ACTR_REC_VSBLTY_WHN_ENTRY_CRIT_NOT_FULFILLED"));
		workflowStep.setBeginStep(rs.getBoolean("IS_BEGIN_STEP"));
		workflowStep.setEndStep(rs.getBoolean("IS_END_STEP"));
		workflowStep.setOnStepCompletionExpn(rs.getString("ON_STEP_COMPLETION_EXPN"));
		workflowStep.setOnStepCancellationExpn(rs.getString("ON_STEP_CANCELLATION_EXPN"));
		workflowStep.setCreatedDatetime(rs.getTimestamp("A.CREATED_DATETIME"));
		workflowStep.setLastModifiedDatetime(rs.getTimestamp("A.LAST_MODIFIED_DATETIME"));
		return workflowStep;
	}

	@Override
	public List<WorkflowActor> getNonCompletedWorkflowActors(String workflowStepId) throws SQLException {
		List<WorkflowActor> actors = new ArrayList<>();
		String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR WHERE WORKFLOW_TXN_STEP_FK_ID = ? AND STEP_ACTOR_STATUS_CODE_FK_ID NOT IN ('NOT_ACTED', 'ACTED');";
		List<Object> parameter = new ArrayList<>();
		parameter.add(Util.remove0x(workflowStepId));
		ResultSet rs = searchResultSet(sql, parameter);
		while (rs.next()){
			WorkflowActor workflowActor = getresolvedPersonActorFromResultSet(rs);
			actors.add(workflowActor);
		}
		return actors;
	}

	@Override
	public List<WorkflowActorAction> getWorkflowDefaultActionForActorList(List<WorkflowActor> nonCompletedWorkflowActors) throws SQLException {
		List<WorkflowActorAction> workflowActorActions = new ArrayList<>();
    	StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION T JOIN T_PFM_ADM_WORKFLOW_ACTION_DEFN " +
				"ON WORKFLOW_ACTION_DEFN_PK_ID = WORKFLOW_ACTION_DEFN_FK_ID WHERE IS_ACTION_TO_DFLT_EXEC_ON_FORCED_MOVE = TRUE AND WORKFLOW_TXN_STEP_ACTOR_FK_ID IN (");
		List<Object> parameter = new ArrayList<>();
		for (WorkflowActor workflowActor : nonCompletedWorkflowActors){
			sql.append("x?, ");
			parameter.add(Util.remove0x(workflowActor.getWorkflowTxnStepActorPkId()));
		}
		sql = sql.replace(sql.length()-2, sql.length(), ");");
		ResultSet rs = searchResultSet(sql.toString(), parameter);
		while (rs.next()){
			WorkflowActorAction workflowActorAction = getWorkflowActionFromResultSet(rs);
			workflowActorActions.add(workflowActorAction);
		}
		return workflowActorActions;
	}

	@Override
	public Map<String, WorkflowActor> getResolvedPersonActorByStep(String workflowTxnStepPkId) throws SQLException {
		Map<String, WorkflowActor> workflowActorMap = new HashMap<>();
    	String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR A JOIN T_PFM_ADM_WORKFLOW_ACTOR_DEFN B " +
				"ON A.STEP_ACTOR_DEFN_FK_ID = B.WORKFLOW_ACTOR_DEFN_PK_ID WHERE A.WORKFLOW_TXN_STEP_FK_ID = x? ;";
		List<Object> parameter = new ArrayList<>();
		parameter.add(Util.remove0x(workflowTxnStepPkId));
		ResultSet rs = searchResultSet(sql, parameter);
		while (rs.next()){
			WorkflowActor workflowActor = getresolvedPersonActorFromResultSet(rs);
			workflowActorMap.put(workflowActor.getWorkflowTxnStepActorPkId(), workflowActor);
		}
		return workflowActorMap;
	}

	@Override
	public WorkflowStep getNextWorkflowStep(int stepSeqNumPosInt, String workflowTxnPkId) throws SQLException {
		WorkflowStep workflowStep = null;
		String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP A RIGHT JOIN T_PFM_ADM_WORKFLOW_STEP_DEFN B ON B.WORKFLOW_STEP_DEFN_PK_ID = A.WORKFLOW_STEP_DEFN_FK_ID WHERE B.STEP_SEQ_NUM_POS_INT = ?+1 AND A.WORKFLOW_TXN_FK_ID = x?;";
		List<Object> parameter = new ArrayList<>();
		parameter.add(stepSeqNumPosInt);
		parameter.add(Util.remove0x(workflowTxnPkId));
		ResultSet rs = searchResultSet(sql, parameter);
		if(rs.next()){
			workflowStep = getWorkflowStepFromResultSet(rs);
		}
		return workflowStep;
	}

	//fixme remove if not required
	public List<WorkflowActorAction> getWorkflowActorActionWithExitCritCheckApplForActorList(List<WorkflowActor> sourceActorForWholeStep) throws SQLException {
		List<WorkflowActorAction> allActionsWithExitCritCheckApplicable = new ArrayList<>();
    	String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION T JOIN T_PFM_ADM_WORKFLOW_ACTION_DEFN ON WORKFLOW_ACTION_DEFN_PK_ID = WORKFLOW_ACTION_DEFN_FK_ID WHERE WORKFLOW_TXN_STEP_ACTOR_FK_ID = x? ;";
    	List<Object> parameter;
    	for(WorkflowActor actor: sourceActorForWholeStep){
    		parameter = new ArrayList<>();
    		parameter.add(Util.remove0x(actor.getWorkflowTxnStepActorPkId()));
    		ResultSet rs = searchResultSet(sql, parameter);
    		while (rs.next()){
    				WorkflowActorAction wfActorAction = getWorkflowActionFromResultSet(rs);
    				allActionsWithExitCritCheckApplicable.add(wfActorAction);
    			}
    		}
		return allActionsWithExitCritCheckApplicable;
	}

	@Override
	public WorkflowStep getWorkflowSourceStepByAction(WorkflowActorAction workflowActorAction) throws SQLException {
		WorkflowStep workflowStep = null;
		String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION A JOIN T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR B ON A.WORKFLOW_TXN_STEP_ACTOR_FK_ID = B.WORKFLOW_TXN_STEP_ACTOR_PK_ID JOIN T_PFM_IEU_WORKFLOW_TXN_STEP C "
				+ "ON C.WORKFLOW_TXN_STEP_PK_ID = B.WORKFLOW_TXN_STEP_FK_ID JOIN T_PFM_ADM_WORKFLOW_STEP_DEFN D ON C.WORKFLOW_STEP_DEFN_FK_ID = D.WORKFLOW_STEP_DEFN_PK_ID WHERE B.WORKFLOW_TXN_STEP_FK_ID IN "
				+ "(SELECT WORKFLOW_TXN_STEP_FK_ID FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION JOIN T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR ON WORKFLOW_TXN_STEP_ACTOR_FK_ID = WORKFLOW_TXN_STEP_ACTOR_PK_ID WHERE WORKFLOW_TXN_STEP_ACTOR_FK_ID = x? )";
		List<Object> parameter = new ArrayList<>();
		parameter.add(Util.remove0x(workflowActorAction.getWorkflowTxnStepActorFkId()));
		ResultSet rs = searchResultSet(sql, parameter);
		if(rs.next()){
			workflowStep = new WorkflowStep();
			workflowStep.setWorkflowTxnStepPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_PK_ID")));
			workflowStep.setWorkflowTxnFkId(Util.convertByteToString(rs.getBytes("C.WORKFLOW_TXN_FK_ID")));
			workflowStep.setWorkflowStepDefnFkId(Util.convertByteToString(rs.getBytes("C.WORKFLOW_STEP_DEFN_FK_ID")));
			workflowStep.setStepActualStartDatetime(rs.getTimestamp("C.STEP_ACTUAL_START_DATETIME"));
			workflowStep.setStepActualEndDatetime(rs.getTimestamp(("C.STEP_ACTUAL_END_DATETIME")));
			workflowStep.setStepStatusCodeFkId(rs.getString("C.STEP_STATUS_CODE_FK_ID"));
			workflowStep.setStartDateExpn(rs.getString("START_DATE_EXPN"));
			workflowStep.setDueDateExpn(rs.getString("DUE_DATE_EXPN"));
			workflowStep.setStartDateEnforced(rs.getBoolean("IS_START_DATE_ENFORCED"));
			workflowStep.setDueDateEnforced(rs.getBoolean("IS_DUE_DATE_ENFORCED"));
			workflowStep.setStepActorResolutionCodeFkId(rs.getString("STEP_ACTOR_RESOLUTION_CODE_FK_ID"));
			workflowStep.setAllowTrgtActrRecVsbltyWhnEntryCritNotFulfilled(rs.getBoolean("ALLOW_TRGT_ACTR_REC_VSBLTY_WHN_ENTRY_CRIT_NOT_FULFILLED"));
			workflowStep.setBeginStep(rs.getBoolean("IS_BEGIN_STEP"));
			workflowStep.setEndStep(rs.getBoolean("IS_END_STEP"));
			workflowStep.setOnStepCompletionExpn(rs.getString("ON_STEP_COMPLETION_EXPN"));
			workflowStep.setOnStepCancellationExpn(rs.getString("ON_STEP_CANCELLATION_EXPN"));
			workflowStep.setCreatedDatetime(rs.getTimestamp("C.CREATED_DATETIME"));
			workflowStep.setLastModifiedDatetime(rs.getTimestamp("C.LAST_MODIFIED_DATETIME"));
		}
		return workflowStep;
	}
	
//	public List<WorkflowActor> getResolvedPersonActorByStep(WorkflowStep workflowStep, GlobalVariables globalVariables, Map<String, WorkflowStep> sourceAndTargetSteps) throws SQLException {
//    	WorkflowStep targetWorkflowStep = sourceAndTargetSteps.get("TARGET_STEP");
//    	List<WorkflowActor> actorList = new ArrayList<>();
//    	String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR A JOIN T_PFM_ADM_WORKFLOW_ACTOR_DEFN B ON A.STEP_ACTOR_DEFN_FK_ID = B.WORKFLOW_ACTOR_DEFN_PK_ID WHERE WORKFLOW_TXN_STEP_FK_ID = x? ;";
//		List<Object> parameter = new ArrayList<>();
//		parameter.add(Util.remove0x(workflowStep.getWorkflowTxnStepPkId()));
//		ResultSet rs = searchResultSet(sql, parameter);
//		if (rs.next()){
//			do {
//				WorkflowActor actor = new WorkflowActor();
//				actor.setWorkflowTxnStepActorPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_ACTOR_PK_ID")));
//				actor.setWorkflowTxnStepFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_FK_ID")));
//				actor.setStepActorDefnFkId(Util.convertByteToString(rs.getBytes("STEP_ACTOR_DEFN_FK_ID")));
//				actor.setResolvedPersonFkId(Util.convertByteToString(rs.getBytes("RESOLVED_PERSON_FK_ID")));
//				actor.setStepActorStatusCodeFkId(rs.getString("STEP_ACTOR_STATUS_CODE_FK_ID"));
//				actor.setCreatedDatetime(rs.getTimestamp("CREATED_DATETIME"));
//				actor.setLastModifiedDatetime(rs.getTimestamp("LAST_MODIFIED_DATETIME"));
//				actorList.add(actor);
//			}while (rs.next());
//		}else if(!targetWorkflowStep.isStartDateEnforced() || targetWorkflowStep.getStepActualStartDatetime().before(globalVariables.currentTimestamp)){
//			actorList = workflowUtility.resolveActors(globalVariables, workflowStep, sourceAndTargetSteps);//needed as next step is not resolved then how the action log can be maintained.
//		}else {//dummy entries for actor with person and actor txn as null
////			WorkflowActor dummyActor = new WorkflowActor();
////			actorList.add(dummyActor);
//		}
//		return actorList;
//	}
	
	public List<WorkflowActorAction> getWorkflowActorActionForActorList(List<WorkflowActor> resolvedPersonActorList) throws SQLException {
		List<WorkflowActorAction> allActions = new ArrayList<>();
    	String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION T JOIN T_PFM_ADM_WORKFLOW_ACTION_DEFN ON WORKFLOW_ACTION_DEFN_PK_ID = WORKFLOW_ACTION_DEFN_FK_ID WHERE WORKFLOW_TXN_STEP_ACTOR_FK_ID = x? ;";
    	List<Object> parameter;
    	if (resolvedPersonActorList != null) {
			for (WorkflowActor personActor : resolvedPersonActorList) {
				parameter = new ArrayList<>();
				parameter.add(Util.remove0x(personActor.getWorkflowTxnStepActorPkId()));
				ResultSet rs = searchResultSet(sql, parameter);
				while (rs.next()) {
					WorkflowActorAction wfActorAction = new WorkflowActorAction();
					wfActorAction.setWorkflowTxnActorPossibleActionPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID")));
					wfActorAction.setWorkflowActionDefnFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_ACTION_DEFN_FK_ID")));
					wfActorAction.setWorkflowTxnStepActorFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_ACTOR_FK_ID")));
					wfActorAction.setActionVisualMasterCodeFkId(rs.getString("ACTN_VISUAL_MASTER_CODE_FK_ID"));
					wfActorAction.setTargetWflowTxnStepFkId(Util.convertByteToString(rs.getBytes("TARGET_WFLOW_TXN_STEP_FK_ID")));
					wfActorAction.setTargetStepActorDefnFkId(Util.convertByteToString(rs.getBytes("TARGET_STEP_ACTOR_DEFN_FK_ID")));
					wfActorAction.setIfActionConditionExpn(rs.getString("T.IF_ACTION_CONDITION_EXPN"));
					wfActorAction.setActionCommentMandatory(rs.getBoolean("IS_COMMENT_MANDATORY_FLAG"));
					wfActorAction.setActionVisibleThruExitCritEvln(rs.getBoolean("IS_ACTION_VISIBLE_THRU_EXIT_CRIT_EVLN"));
					wfActorAction.setWorkflowDefinitionFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_DEFINITION_FK_ID")));
					wfActorAction.setActionDispSeqNumPosInt(rs.getInt("ACTION_DISP_SEQ_NUM_POS_INT"));
					wfActorAction.setSourceActorFkId(Util.convertByteToString(rs.getBytes("SOURCE_ACTOR_FK_ID")));
					wfActorAction.setTargetActorFkId(Util.convertByteToString(rs.getBytes("TARGET_ACTOR_FK_ID")));
					wfActorAction.setTargetStepFkId(Util.convertByteToString(rs.getBytes("TARGET_STEP_FK_ID")));
					wfActorAction.setActionToDlftExecOnForcedMove(rs.getBoolean("IS_ACTION_TO_DFLT_EXEC_ON_FORCED_MOVE"));
					wfActorAction.setActionDelegateable(rs.getBoolean("IS_ACTION_DELEGATEABLE"));
					wfActorAction.setAutoExecIfActorEqualsSubject(rs.getBoolean("IS_AUTO_EXEC_IF_ACTOR_EQUALS_SUBJECT"));
					wfActorAction.setAutoExecuteIfActorEqualToInitiator(rs.getBoolean("IS_AUTO_EXECUTE_IF_ACTOR_EQUAL_TO_INITIATOR"));
					wfActorAction.setAutoExecuteIfDuplicate(rs.getBoolean("IS_AUTO_EXECUTE_IF_DUPLICATE"));
					wfActorAction.setExitCountCriteriaForActionVisibilityExpn(rs.getString("EXIT_COUNT_CRITERIA_FOR_ACTN_VSBLTY_EXPN"));
					wfActorAction.setExitCountCriteriaForStepTrnsnExpn(rs.getString("EXIT_COUNT_CRITERIA_FOR_STP_TRNSN_EXPN"));
//					wfActorAction.setCrtriaCntrlgSrcStepExit(rs.getBoolean("IS_CRTRIA_CNTRLG_SRC_STEP_EXIT"));
//					wfActorAction.setCrtriaCntrlgTgtStepEntryVsblty(rs.getBoolean("IS_CRTRIA_CNTRLG_TGT_STEP_ENTRY_VSBLTY"));
					wfActorAction.setActionExitSrcActNtfnId(Util.convertByteToString(rs.getBytes("ACTION_EXIT_SRC_ACT_NTFN_ID")));
					wfActorAction.setActionExitTrgtActNtfnId(Util.convertByteToString(rs.getBytes("ACTION_EXIT_TRGT_ACT_NTFN_ID")));
					allActions.add(wfActorAction);
				}
			}
		}
		return allActions;
	}
	
	public void insertRowWithParameter(String sql, Deque<Object> parameter) throws SQLException {
		PreparedStatement ps = null;
		try{
			Connection c = getConnection();
			ps = c.prepareStatement(sql);
			LOG.debug(sql);
			if(parameter!=null){
				int i= 1;
				for(Object param : parameter){
					LOG.debug("insert query parameter number "+i+" => "+param.toString());
					ps.setObject(i,param);
					i++;
				}
			}
			int count = ps.executeUpdate();
			LOG.debug("$$$    "+ count +"   $$$ number of records inserted or updated");
		} catch (SQLException e){
			LOG.error(e.getMessage(), e);
			throw e;
		}
	}

	@Override
	public void deActivateActions(List<WorkflowActorAction> allSourceStepPossibleActions) throws SQLException {
    	String sql = "UPDATE T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION SET IS_ACTION_VISIBLE_THRU_EXIT_CRIT_EVLN = false WHERE WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID = x?";
    	Deque<Object> parameter;
		for(WorkflowActorAction actionTxn : allSourceStepPossibleActions){
			parameter = new ArrayDeque<>();
			parameter.add(Util.remove0x(actionTxn.getWorkflowTxnActorPossibleActionPkId()));
			insertRowWithParameter(sql, parameter);
		}
	}

	@Override
	public WorkflowActor getResolvedPersonActor(String workflowTxnStepActorId) throws SQLException {
		WorkflowActor workflowActor = null;
		String sql = "SELECT * FROM T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR A JOIN T_PFM_ADM_WORKFLOW_ACTOR_DEFN B ON A.STEP_ACTOR_DEFN_FK_ID = B.WORKFLOW_ACTOR_DEFN_PK_ID WHERE WORKFLOW_TXN_STEP_ACTOR_PK_ID = x? ;";
		List<Object> parameter = new ArrayList<>();
		parameter.add(Util.remove0x(workflowTxnStepActorId));
		ResultSet rs = searchResultSet(sql, parameter);
		if(rs.next()){
				workflowActor = getresolvedPersonActorFromResultSet(rs);
			}
		return workflowActor;
	}

	public WorkflowActor getresolvedPersonActorFromResultSet(ResultSet rs) throws SQLException {
    	WorkflowActor workflowActor = new WorkflowActor();
		workflowActor.setWorkflowTxnStepActorPkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_ACTOR_PK_ID")));
		workflowActor.setWorkflowTxnStepFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_TXN_STEP_FK_ID")));
		workflowActor.setStepActorDefnFkId(Util.convertByteToString(rs.getBytes("STEP_ACTOR_DEFN_FK_ID")));
		workflowActor.setResolvedPersonFkId(Util.convertByteToString(rs.getBytes("RESOLVED_PERSON_FK_ID")));
		workflowActor.setStepActorStatusCodeFkId(rs.getString("STEP_ACTOR_STATUS_CODE_FK_ID"));
		workflowActor.setCreatedDatetime(rs.getTimestamp("CREATED_DATETIME"));
		workflowActor.setLastModifiedDatetime(rs.getTimestamp("LAST_MODIFIED_DATETIME"));
		workflowActor.setWorkflowStepDefnFkId(Util.convertByteToString(rs.getBytes("WORKFLOW_STEP_DEFN_FK_ID")));
		workflowActor.setActorDispSeqNumPosInt(rs.getInt("ACTOR_DISP_SEQ_NUM_POS_INT"));
		workflowActor.setActorNameG11nBigTxt(rs.getString("ACTOR_NAME_G11N_BIG_TXT"));
		workflowActor.setActorIconCodeFkId(rs.getString("ACTOR_ICON_CODE_FK_ID"));
		workflowActor.setActorPersonGroupFkId(Util.convertByteToString(rs.getBytes("ACTOR_PERSON_GROUP_FK_ID")));
		workflowActor.setRelativeToLoggedInUser(rs.getBoolean("IS_RELATIVE_TO_LOGGED_IN_USER"));
		workflowActor.setRelativeToSubjectUser(rs.getBoolean("IS_RELATIVE_TO_SUBJECT_USER"));
		workflowActor.setRelativeToActorId(Util.convertByteToString(rs.getBytes("RELATIVE_TO_ACTOR_FK_ID")));
		workflowActor.setRelativeRelationshipCodeFkId(rs.getString("RELATIVE_RELATIONSHIP_CODE_FK_ID"));
		workflowActor.setRelativeRelationshipHierarchyLevelPosInt(rs.getInt("RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT"));
		workflowActor.setResolveActorAsOfDatetime(rs.getTimestamp("RESOLVE_ACTOR_AS_OF_DATETIME"));
		workflowActor.setAdhocUserActor(rs.getBoolean("IS_ADHOC_USER_ACTOR"));
		workflowActor.setAllowExternalUser(rs.getBoolean("ALLOW_EXTERNAL_USER"));
		workflowActor.setNoActorsActionCodeFkId(rs.getString("NO_ACTORS_ACTION_CODE_FK_ID"));
		return workflowActor;
	}
}
