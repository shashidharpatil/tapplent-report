package com.tapplent.platformutility.metadata.structure;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.layout.valueObject.IconVO;

public class EntityMetadata{
	@JsonIgnore
	private String versionId;
	private String templateDOCode;
	private String baseTemplateId;
	private String processCode;
	private String domainObjectCode;
	private String btPE;
	private String mtPE;
	private String parentMtPE;
	private String btDOName;
	private String doTypeCode;
	private String dbTableName;
	private String doSummaryTitle;
	private IconVO doSummaryTitleIcon;
	private String doDetailsTitle;
	private String doHelpText;
	private String countOfRecordsSingularTitle;
	private String countOfRecordsPluralTitle;
	private boolean isDoCustomizableFlag;
	private boolean isDoAccessControlGovernedFlag;
	private boolean isUpdateInline;
	@JsonIgnore
	private List<EntityAttributeMetadata> attributeMeta;
	public EntityMetadata(){
		this.attributeMeta = new ArrayList<EntityAttributeMetadata>();
	}

	public String getVersionId() {
		return versionId;
	}

	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}

	public String getTemplateDOCode() {
		return templateDOCode;
	}

	public void setTemplateDOCode(String templateDOCode) {
		this.templateDOCode = templateDOCode;
	}

	public String getBaseTemplateId() {
		return baseTemplateId;
	}

	public void setBaseTemplateId(String baseTemplateId) {
		this.baseTemplateId = baseTemplateId;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	public String getDomainObjectCode() {
		return domainObjectCode;
	}

	public void setDomainObjectCode(String domainObjectCode) {
		this.domainObjectCode = domainObjectCode;
	}

	public String getBtPE() {
		return btPE;
	}

	public void setBtPE(String btPE) {
		this.btPE = btPE;
	}

	public String getMtPE() {
		return mtPE;
	}

	public void setMtPE(String mtPE) {
		this.mtPE = mtPE;
	}

	public String getParentMtPE() {
		return parentMtPE;
	}

	public void setParentMtPE(String parentMtPE) {
		this.parentMtPE = parentMtPE;
	}

	public String getBtDOName() {
		return btDOName;
	}

	public void setBtDOName(String btDOName) {
		this.btDOName = btDOName;
	}

	public String getDoTypeCode() {
		return doTypeCode;
	}

	public void setDoTypeCode(String doTypeCode) {
		this.doTypeCode = doTypeCode;
	}

	public String getDbTableName() {
		return dbTableName;
	}

	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}

	public String getDoSummaryTitle() {
		return Util.getG11nValue(doSummaryTitle, null);
	}

	public void setDoSummaryTitle(String doSummaryTitle) {
		this.doSummaryTitle = doSummaryTitle;
	}

	public IconVO getDoSummaryTitleIcon() {
		return doSummaryTitleIcon;
	}

	public void setDoSummaryTitleIcon(IconVO doSummaryTitleIcon) {
		this.doSummaryTitleIcon = doSummaryTitleIcon;
	}

	public String getDoDetailsTitle() {
		return Util.getG11nValue(doDetailsTitle, null);
	}

	public void setDoDetailsTitle(String doDetailsTitle) {
		this.doDetailsTitle = doDetailsTitle;
	}

	public String getDoHelpText() {
		return Util.getG11nValue(doHelpText, null);
	}

	public void setDoHelpText(String doHelpText) {
		this.doHelpText = doHelpText;
	}

	public String getCountOfRecordsSingularTitle() {
		return countOfRecordsSingularTitle;
	}

	public void setCountOfRecordsSingularTitle(String countOfRecordsSingularTitle) {
		this.countOfRecordsSingularTitle = countOfRecordsSingularTitle;
	}

	public String getCountOfRecordsPluralTitle() {
		return countOfRecordsPluralTitle;
	}

	public void setCountOfRecordsPluralTitle(String countOfRecordsPluralTitle) {
		this.countOfRecordsPluralTitle = countOfRecordsPluralTitle;
	}

	public boolean isDoCustomizableFlag() {
		return isDoCustomizableFlag;
	}

	public void setDoCustomizableFlag(boolean doCustomizableFlag) {
		isDoCustomizableFlag = doCustomizableFlag;
	}

	public boolean isDoAccessControlGovernedFlag() {
		return isDoAccessControlGovernedFlag;
	}

	public void setDoAccessControlGovernedFlag(boolean doAccessControlGovernedFlag) {
		isDoAccessControlGovernedFlag = doAccessControlGovernedFlag;
	}

	public boolean isUpdateInline() {
		return isUpdateInline;
	}

	public void setUpdateInline(boolean updateInline) {
		isUpdateInline = updateInline;
	}

	public List<EntityAttributeMetadata> getAttributeMeta() {
		return attributeMeta;
	}

	public void setAttributeMeta(List<EntityAttributeMetadata> attributeMeta) {
		this.attributeMeta = attributeMeta;
	}
}
