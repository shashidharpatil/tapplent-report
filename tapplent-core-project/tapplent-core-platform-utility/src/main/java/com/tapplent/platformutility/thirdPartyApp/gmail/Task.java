package com.tapplent.platformutility.thirdPartyApp.gmail;

import java.sql.Timestamp;

public class Task {
	private String taskPkId;
	private String taskTitleG11nBigTxt;
	private String taskDescCommentG11nBigTxt;
	private String taskStartDatetime;
	private Timestamp taskEndDatetime;
	public Timestamp getTaskEndDatetime() {
		return taskEndDatetime;
	}
	public void setTaskEndDatetime(Timestamp taskEndDatetime) {
		this.taskEndDatetime = taskEndDatetime;
	}
	private String taskStatusCodeFkId;
	private String processTypeCodeFkId;
	private String mtPeCodeFkId;
	private String doRowPrimaryKeyFkId;
	private String displayContextValueBigTxt;
	private String doIconCodeFkId;
	private String doNameG11nBigTxt;
	private String parentTaskFkId;
	private String createdDatetime;
	private boolean isDeleted;
	private Timestamp lastModifiedDatetime;
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getTaskPkId() {
		return taskPkId;
	}
	public void setTaskPkId(String taskPkId) {
		this.taskPkId = taskPkId;
	}
	public String getTaskTitleG11nBigTxt() {
		return taskTitleG11nBigTxt;
	}
	public void setTaskTitleG11nBigTxt(String taskTitleG11nBigTxt) {
		this.taskTitleG11nBigTxt = taskTitleG11nBigTxt;
	}
	public String getTaskDescCommentG11nBigTxt() {
		return taskDescCommentG11nBigTxt;
	}
	public void setTaskDescCommentG11nBigTxt(String taskDescCommentG11nBigTxt) {
		this.taskDescCommentG11nBigTxt = taskDescCommentG11nBigTxt;
	}
	public String getTaskStartDatetime() {
		return taskStartDatetime;
	}
	public void setTaskStartDatetime(String taskStartDatetime) {
		this.taskStartDatetime = taskStartDatetime;
	}
	
	public String getTaskStatusCodeFkId() {
		return taskStatusCodeFkId;
	}
	public void setTaskStatusCodeFkId(String taskStatusCodeFkId) {
		this.taskStatusCodeFkId = taskStatusCodeFkId;
	}
	public String getProcessTypeCodeFkId() {
		return processTypeCodeFkId;
	}
	public void setProcessTypeCodeFkId(String processTypeCodeFkId) {
		this.processTypeCodeFkId = processTypeCodeFkId;
	}
	public String getMtPeCodeFkId() {
		return mtPeCodeFkId;
	}
	public void setMtPeCodeFkId(String mtPeCodeFkId) {
		this.mtPeCodeFkId = mtPeCodeFkId;
	}
	public String getDoRowPrimaryKeyFkId() {
		return doRowPrimaryKeyFkId;
	}
	public void setDoRowPrimaryKeyFkId(String doRowPrimaryKeyFkId) {
		this.doRowPrimaryKeyFkId = doRowPrimaryKeyFkId;
	}
	public String getDisplayContextValueBigTxt() {
		return displayContextValueBigTxt;
	}
	public void setDisplayContextValueBigTxt(String displayContextValueBigTxt) {
		this.displayContextValueBigTxt = displayContextValueBigTxt;
	}
	public String getDoIconCodeFkId() {
		return doIconCodeFkId;
	}
	public void setDoIconCodeFkId(String doIconCodeFkId) {
		this.doIconCodeFkId = doIconCodeFkId;
	}
	public String getDoNameG11nBigTxt() {
		return doNameG11nBigTxt;
	}
	public void setDoNameG11nBigTxt(String doNameG11nBigTxt) {
		this.doNameG11nBigTxt = doNameG11nBigTxt;
	}
	public String getParentTaskFkId() {
		return parentTaskFkId;
	}
	public void setParentTaskFkId(String parentTaskFkId) {
		this.parentTaskFkId = parentTaskFkId;
	}
	public String getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(String createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public Timestamp getLastModifiedDatetime() {
		return lastModifiedDatetime;
	}
	public void setLastModifiedDatetime(Timestamp lastModifiedDatetime) {
		this.lastModifiedDatetime = lastModifiedDatetime;
	}
	


}
