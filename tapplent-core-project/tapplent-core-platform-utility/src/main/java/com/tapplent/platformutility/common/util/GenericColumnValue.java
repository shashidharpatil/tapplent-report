package com.tapplent.platformutility.common.util;

import java.util.HashMap;
import java.util.Map;

public class GenericColumnValue {
	private String dbDataType;
	private boolean isBlob;
	private boolean isG11n;
	private String dummyColumnName;
	private Map<String,GenericColumnValue> blobColumnToValueMap; 
	private String columnValue;
	private String actualValue;
	public GenericColumnValue(String columnValue,String dbDataType, String dummyColumnName) {
		super();
		this.dbDataType = dbDataType;
		this.columnValue = columnValue;
		this.dummyColumnName = dummyColumnName;
		this.blobColumnToValueMap = new HashMap<>();
	}
	public String getDbDataType() {
		return dbDataType;
	}
	public void setDbDataType(String dbDataType) {
		this.dbDataType = dbDataType;
	}
	public String getColumnValue() {
		return columnValue;
	}
	public void setColumnValue(String columnValue) {
		this.columnValue = columnValue;
	}
	public boolean isBlob() {
		return isBlob;
	}
	public void setBlob(boolean isBlob) {
		this.isBlob = isBlob;
	}
	public boolean isG11n() {
		return isG11n;
	}
	public void setG11n(boolean isG11n) {
		this.isG11n = isG11n;
	}
	public Map<String, GenericColumnValue> getBlobColumnToValueMap() {
		return blobColumnToValueMap;
	}
	public void setBlobColumnToValueMap(Map<String, GenericColumnValue> blobColumnToValueMap) {
		this.blobColumnToValueMap = blobColumnToValueMap;
	}
	public String getDummyColumnName() {
		return dummyColumnName;
	}
	public void setDummyColumnName(String dummyColumnName) {
		this.dummyColumnName = dummyColumnName;
	}

	public String getActualValue() {
		return actualValue;
	}

	public void setActualValue(String actualValue) {
		this.actualValue = actualValue;
	}
}
