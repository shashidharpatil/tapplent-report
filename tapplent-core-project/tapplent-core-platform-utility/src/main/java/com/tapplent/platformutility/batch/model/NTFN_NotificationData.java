package com.tapplent.platformutility.batch.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class NTFN_NotificationData {
	private String personNotificationPkId;
	private String personFkId;
	private String mtPeCodeFkId;
	private String doRowPrimaryKeyTxt;
	private String doRowVersionFkId;
	private String dependencyKeyExpn;
	private String notificationDefinitionFkId;
	private String generateDatetime;
	private boolean returnStatusCodeFkId;
	private boolean isSync;
	private String ntfnBtCodeFkId;
	private String ntfnMtPeCodeFkId;
	private String ntfnVtCodeFkId;
	private String ntfnCanvasTxnFkId;
	private String forDocGenDefnFkId;
	private List<NTFN_ChannelMap> notChannels;
	private ResultSet resultSet;
	private Map<String, List<NTFN_NotfnDoaMap>> doaMap;
	private Map<String,Object> credentials;

	private boolean isSuccess;
	private Object error;
	private Long jobId;
	private Long jobExecutionId;

	public Long getJobId() {
		return jobId;
	}

	public void setJobId(Long jobId) {
		this.jobId = jobId;
	}

	public Long getJobExecutionId() {
		return jobExecutionId;
	}

	public void setJobExecutionId(Long jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}

	public Object getError() {
		return error;
	}

	public void setError(Object error) {
		this.error = error;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public Map<String,Object> getCredentials() {
		return credentials;
	}

	public void setCredentials(Map<String,Object> credentials) {
		this.credentials = credentials;
	}

	public List<NTFN_ChannelMap> getNotChannels() {
		return notChannels;
	}

	public void setNotChannels(List<NTFN_ChannelMap> notChannels) {
		this.notChannels = notChannels;
	}

	public ResultSet getResultSet() {
		return resultSet;
	}

	public void setResultSet(ResultSet resultSet) {
		this.resultSet = resultSet;
	}

	public Map<String, List<NTFN_NotfnDoaMap>> getDoaMap() {
		return doaMap;
	}

	public void setDoaMap(Map<String, List<NTFN_NotfnDoaMap>> doaMap) {
		this.doaMap = doaMap;
	}

	public String getPersonNotificationPkId() {
		return personNotificationPkId;
	}

	public void setPersonNotificationPkId(String personNotificationPkId) {
		this.personNotificationPkId = personNotificationPkId;
	}

	public String getPersonFkId() {
		return personFkId;
	}

	public void setPersonFkId(String personFkId) {
		this.personFkId = personFkId;
	}

	public String getMtPeCodeFkId() {
		return mtPeCodeFkId;
	}

	public void setMtPeCodeFkId(String mtPeCodeFkId) {
		this.mtPeCodeFkId = mtPeCodeFkId;
	}

	public String getDoRowPrimaryKeyTxt() {
		return doRowPrimaryKeyTxt;
	}

	public void setDoRowPrimaryKeyTxt(String doRowPrimaryKeyTxt) {
		this.doRowPrimaryKeyTxt = doRowPrimaryKeyTxt;
	}

	public String getDoRowVersionFkId() {
		return doRowVersionFkId;
	}

	public void setDoRowVersionFkId(String doRowVersionFkId) {
		this.doRowVersionFkId = doRowVersionFkId;
	}

	public String getDependencyKeyExpn() {
		return dependencyKeyExpn;
	}

	public void setDependencyKeyExpn(String dependencyKeyExpn) {
		this.dependencyKeyExpn = dependencyKeyExpn;
	}

	public String getNotificationDefinitionFkId() {
		return notificationDefinitionFkId;
	}

	public void setNotificationDefinitionFkId(String notificationDefinitionFkId) {
		this.notificationDefinitionFkId = notificationDefinitionFkId;
	}

	public String getGenerateDatetime() {
		return generateDatetime;
	}

	public void setGenerateDatetime(String generateDatetime) {
		this.generateDatetime = generateDatetime;
	}

	public boolean isReturnStatusCodeFkId() {
		return returnStatusCodeFkId;
	}

	public void setReturnStatusCodeFkId(boolean returnStatusCodeFkId) {
		this.returnStatusCodeFkId = returnStatusCodeFkId;
	}

	public boolean isSync() {
		return isSync;
	}

	public void setSync(boolean isSync) {
		this.isSync = isSync;
	}

	public String getNtfnBtCodeFkId() {
		return ntfnBtCodeFkId;
	}

	public void setNtfnBtCodeFkId(String ntfnBtCodeFkId) {
		this.ntfnBtCodeFkId = ntfnBtCodeFkId;
	}

	public String getNtfnMtPeCodeFkId() {
		return ntfnMtPeCodeFkId;
	}

	public void setNtfnMtPeCodeFkId(String ntfnMtPeCodeFkId) {
		this.ntfnMtPeCodeFkId = ntfnMtPeCodeFkId;
	}

	public String getNtfnVtCodeFkId() {
		return ntfnVtCodeFkId;
	}

	public void setNtfnVtCodeFkId(String ntfnVtCodeFkId) {
		this.ntfnVtCodeFkId = ntfnVtCodeFkId;
	}

	public String getNtfnCanvasTxnFkId() {
		return ntfnCanvasTxnFkId;
	}

	public void setNtfnCanvasTxnFkId(String ntfnCanvasTxnFkId) {
		this.ntfnCanvasTxnFkId = ntfnCanvasTxnFkId;
	}

	public String getForDocGenDefnFkId() {
		return forDocGenDefnFkId;
	}

	public void setForDocGenDefnFkId(String forDocGenDefnFkId) {
		this.forDocGenDefnFkId = forDocGenDefnFkId;
	}

}
