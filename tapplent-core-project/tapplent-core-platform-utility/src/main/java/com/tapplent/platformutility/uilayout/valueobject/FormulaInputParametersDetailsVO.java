package com.tapplent.platformutility.uilayout.valueobject;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by tapplent on 08/10/17.
 */
public class FormulaInputParametersDetailsVO {

    private String formulaInputParametersDetailsPkID;
    private String controlEvent;
    private String parameterName;
    private String parameterSource;
    private String mtPE;
    private String mtPEAlias;
    private String parentMTPEAlias;
    private String fkRelnWithParent;
    private String attributePath;
    private String operator;
    private String onLoadGenericQueryID;
    private String directValue;

    public String getFormulaInputParametersDetailsPkID() {
        return formulaInputParametersDetailsPkID;
    }

    public void setFormulaInputParametersDetailsPkID(String formulaInputParametersDetailsPkID) {
        this.formulaInputParametersDetailsPkID = formulaInputParametersDetailsPkID;
    }

    public String getControlEvent() {
        return controlEvent;
    }

    public void setControlEvent(String controlEvent) {
        this.controlEvent = controlEvent;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterSource() {
        return parameterSource;
    }

    public void setParameterSource(String parameterSource) {
        this.parameterSource = parameterSource;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getMtPEAlias() {
        return mtPEAlias;
    }

    public void setMtPEAlias(String mtPEAlias) {
        this.mtPEAlias = mtPEAlias;
    }

    public String getParentMTPEAlias() {
        return parentMTPEAlias;
    }

    public void setParentMTPEAlias(String parentMTPEAlias) {
        this.parentMTPEAlias = parentMTPEAlias;
    }

    public String getFkRelnWithParent() {
        return fkRelnWithParent;
    }

    public void setFkRelnWithParent(String fkRelnWithParent) {
        this.fkRelnWithParent = fkRelnWithParent;
    }

    public String getAttributePath() {
        return attributePath;
    }

    public void setAttributePath(String attributePath) {
        this.attributePath = attributePath;
    }

    public String getOnLoadGenericQueryID() {
        return onLoadGenericQueryID;
    }

    public void setOnLoadGenericQueryID(String onLoadGenericQueryID) {
        this.onLoadGenericQueryID = onLoadGenericQueryID;
    }

    public String getDirectValue() {
        return directValue;
    }

    public void setDirectValue(String directValue) {
        this.directValue = directValue;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
