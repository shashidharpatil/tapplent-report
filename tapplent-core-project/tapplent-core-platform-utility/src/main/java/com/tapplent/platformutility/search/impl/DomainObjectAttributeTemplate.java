package com.tapplent.platformutility.search.impl;

import java.util.HashMap;
import java.util.Map;

public class DomainObjectAttributeTemplate {
	private String mtDoAttributeSpecFkG11nText;
	private String mtDoAttributeFkId;
	private String btDoAttributeSpecFkId;
	private String dbColumnName;
//	private String btDoAttrSpecNameG11nText;
	private String btDataTypeCodeFkId;
	private String dbDataTypeCodeFkId;
	private Boolean primaryKeyFlag;
	private Boolean dependencyKeyFlag;	
	private Boolean relationFlag;
	private Boolean hierarchyFlag;
	private Boolean selfJoinParentFlag;
	private Boolean isGlocalized;
	private Boolean isSortConfig;
	private int sortConfigSeq;
	private Boolean isSortConfigAsc;
	private Boolean isFilterConfig;
	//One more field for filter to be added
	private int charMaxLength;
//	Below are telling the relation this Attribute has with a table and its column
//	A flag added which will tell if Relation to fetched or not in this select call	
	private boolean isRelationToBeConsidered;
	private String toDomainObjectFkId;
	private String toDomainObjectNameG11nText;
	private String toDbTableName;
	private String toDoAttributeFkId;
	private String toDoAttributeNameG11nText;
	private String toDbColumnName;
	private Boolean isRelationUnderWorkflow;
	private Boolean isCardinalityMany;
	private String dbMapperTableName;
	Map<String,DomainObjectAttributeRelationFields> relationTableFields;
	public DomainObjectAttributeTemplate(){
		this.relationTableFields = new HashMap<String,DomainObjectAttributeRelationFields>();
	}
	public String getMtDoAttributeSpecFkG11nText() {
		return mtDoAttributeSpecFkG11nText;
	}

	public void setMtDoAttributeSpecFkG11nText(String mtDoAttributeSpecFkG11nText) {
		this.mtDoAttributeSpecFkG11nText = mtDoAttributeSpecFkG11nText;
	}

	public String getMtDoAttributeFkId() {
		return mtDoAttributeFkId;
	}
	public void setMtDoAttributeFkId(String mtDoAttributeFkId) {
		this.mtDoAttributeFkId = mtDoAttributeFkId;
	}
	public String getBtDoAttributeSpecFkId() {
		return btDoAttributeSpecFkId;
	}
	public void setBtDoAttributeSpecFkId(String btDoAttributeSpecFkId) {
		this.btDoAttributeSpecFkId = btDoAttributeSpecFkId;
	}
	public String getDbColumnName() {
		return dbColumnName;
	}
	public void setDbColumnName(String dbColumnName) {
		this.dbColumnName = dbColumnName;
	}
//	public String getBtDoAttrSpecNameG11nText() {
//		return btDoAttrSpecNameG11nText;
//	}
//	public void setBtDoAttrSpecNameG11nText(String btDoAttrSpecNameG11nText) {
//		this.btDoAttrSpecNameG11nText = btDoAttrSpecNameG11nText;
//	}
	public String getBtDataTypeCodeFkId() {
		return btDataTypeCodeFkId;
	}
	public void setBtDataTypeCodeFkId(String btDataTypeCodeFkId) {
		this.btDataTypeCodeFkId = btDataTypeCodeFkId;
	}
	public String getDbDataTypeCodeFkId() {
		return dbDataTypeCodeFkId;
	}
	public void setDbDataTypeCodeFkId(String dbDataTypeCodeFkId) {
		this.dbDataTypeCodeFkId = dbDataTypeCodeFkId;
	}
	public Boolean getPrimaryKeyFlag() {
		return primaryKeyFlag;
	}
	public void setPrimaryKeyFlag(Boolean primaryKeyFlag) {
		this.primaryKeyFlag = primaryKeyFlag;
	}
	public Boolean getDependencyKeyFlag() {
		return dependencyKeyFlag;
	}
	public void setDependencyKeyFlag(Boolean dependencyKeyFlag) {
		this.dependencyKeyFlag = dependencyKeyFlag;
	}
	public Boolean getRelationFlag() {
		return relationFlag;
	}
	public void setRelationFlag(Boolean relationFlag) {
		this.relationFlag = relationFlag;
	}
	public Boolean getHierarchyFlag() {
		return hierarchyFlag;
	}
	public void setHierarchyFlag(Boolean hierarchyFlag) {
		this.hierarchyFlag = hierarchyFlag;
	}
	public Boolean getSelfJoinParentFlag() {
		return selfJoinParentFlag;
	}
	public void setSelfJoinParentFlag(Boolean selfJoinParentFlag) {
		this.selfJoinParentFlag = selfJoinParentFlag;
	}
	public Boolean getIsGlocalized() {
		return isGlocalized;
	}
	public void setIsGlocalized(Boolean isGlocalized) {
		this.isGlocalized = isGlocalized;
	}
	public Boolean getIsSortConfig() {
		return isSortConfig;
	}
	public void setIsSortConfig(Boolean isSortConfig) {
		this.isSortConfig = isSortConfig;
	}
	public int getSortConfigSeq() {
		return sortConfigSeq;
	}
	public void setSortConfigSeq(int sortConfigSeq) {
		this.sortConfigSeq = sortConfigSeq;
	}
	public Boolean getIsSortConfigAsc() {
		return isSortConfigAsc;
	}
	public void setIsSortConfigAsc(Boolean isSortConfigAsc) {
		this.isSortConfigAsc = isSortConfigAsc;
	}
	public Boolean getIsFilterConfig() {
		return isFilterConfig;
	}
	public void setIsFilterConfig(Boolean isFilterConfig) {
		this.isFilterConfig = isFilterConfig;
	}
	public int getCharMaxLength() {
		return charMaxLength;
	}
	public void setCharMaxLength(int charMaxLength) {
		this.charMaxLength = charMaxLength;
	}
	public boolean isRelationToBeConsidered() {
		return isRelationToBeConsidered;
	}
	public void setRelationToBeConsidered(boolean isRelationToBeConsidered) {
		this.isRelationToBeConsidered = isRelationToBeConsidered;
	}
	public String getToDomainObjectFkId() {
		return toDomainObjectFkId;
	}
	public void setToDomainObjectFkId(String toDomainObjectFkId) {
		this.toDomainObjectFkId = toDomainObjectFkId;
	}
	public String getToDomainObjectNameG11nText() {
		return toDomainObjectNameG11nText;
	}
	public void setToDomainObjectNameG11nText(String toDomainObjectNameG11nText) {
		this.toDomainObjectNameG11nText = toDomainObjectNameG11nText;
	}
	public String getToDbTableName() {
		return toDbTableName;
	}
	public void setToDbTableName(String toDbTableName) {
		this.toDbTableName = toDbTableName;
	}
	public String getToDoAttributeFkId() {
		return toDoAttributeFkId;
	}
	public void setToDoAttributeFkId(String toDoAttributeFkId) {
		this.toDoAttributeFkId = toDoAttributeFkId;
	}
	public String getToDoAttributeNameG11nText() {
		return toDoAttributeNameG11nText;
	}
	public void setToDoAttributeNameG11nText(String toDoAttributeNameG11nText) {
		this.toDoAttributeNameG11nText = toDoAttributeNameG11nText;
	}
	public String getToDbColumnName() {
		return toDbColumnName;
	}
	public void setToDbColumnName(String toDbColumnName) {
		this.toDbColumnName = toDbColumnName;
	}
	public Boolean getIsRelationUnderWorkflow() {
		return isRelationUnderWorkflow;
	}
	public void setIsRelationUnderWorkflow(Boolean isRelationUnderWorkflow) {
		this.isRelationUnderWorkflow = isRelationUnderWorkflow;
	}
	public Boolean getIsCardinalityMany() {
		return isCardinalityMany;
	}
	public void setIsCardinalityMany(Boolean isCardinalityMany) {
		this.isCardinalityMany = isCardinalityMany;
	}
	public String getDbMapperTableName() {
		return dbMapperTableName;
	}
	public void setDbMapperTableName(String dbMapperTableName) {
		this.dbMapperTableName = dbMapperTableName;
	}
	public Map<String, DomainObjectAttributeRelationFields> getRelationTableFields() {
		return relationTableFields;
	}
	public void setRelationTableFields(Map<String, DomainObjectAttributeRelationFields> relationTableFields) {
		this.relationTableFields = relationTableFields;
	}
}
