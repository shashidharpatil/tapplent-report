package com.tapplent.platformutility.analytics.structure;

/**
 * Created by tapplent on 05/12/16.
 */
public class AnalyticsMetricDetailsQuery {
    private String analyticsMetricDetailQueryPkId;
    private String analyticsMetricDetailFkId;
    private String mtPeCodeFkId;
    private int mtPeSeqNumPosInt;
    private int parentMtPeSeqNumPosInt;
    private String fkRelationWithParentLngTxt;
    private String attrPathExpn;
    private String attrDispAxizCodeFkId;
    private int attrDisplaySeqNumPosInt;
    private boolean isAxizDataSourcedFromObject;
    private String axizDataSourceMtPeCodeFkId;
    private String axizDataSourceFilterExpn;
    private boolean isDerivedAttr;
    private boolean isAggregateAttr;
    private String derivedAttrDispNameG11nBigTxt;
    private int orderBySeqNumPosInt;
    private String orderByModeCodeFkId;
    private String orderByValuesBigTxt;
    private boolean isGroupByAttribute;
    private int groupBySeqNumPosInt;
    private String groupByHavingExpn;
    private boolean isUsedInFilter;
    private boolean isClientMatch;
    private boolean isClientDisplay;

    public String getAnalyticsMetricDetailQueryPkId() {
        return analyticsMetricDetailQueryPkId;
    }

    public void setAnalyticsMetricDetailQueryPkId(String analyticsMetricDetailQueryPkId) {
        this.analyticsMetricDetailQueryPkId = analyticsMetricDetailQueryPkId;
    }

    public String getAnalyticsMetricDetailFkId() {
        return analyticsMetricDetailFkId;
    }

    public void setAnalyticsMetricDetailFkId(String analyticsMetricDetailFkId) {
        this.analyticsMetricDetailFkId = analyticsMetricDetailFkId;
    }

    public String getMtPeCodeFkId() {
        return mtPeCodeFkId;
    }

    public void setMtPeCodeFkId(String mtPeCodeFkId) {
        this.mtPeCodeFkId = mtPeCodeFkId;
    }

    public int getMtPeSeqNumPosInt() {
        return mtPeSeqNumPosInt;
    }

    public void setMtPeSeqNumPosInt(int mtPeSeqNumPosInt) {
        this.mtPeSeqNumPosInt = mtPeSeqNumPosInt;
    }

    public int getParentMtPeSeqNumPosInt() {
        return parentMtPeSeqNumPosInt;
    }

    public void setParentMtPeSeqNumPosInt(int parentMtPeSeqNumPosInt) {
        this.parentMtPeSeqNumPosInt = parentMtPeSeqNumPosInt;
    }

    public String getFkRelationWithParentLngTxt() {
        return fkRelationWithParentLngTxt;
    }

    public void setFkRelationWithParentLngTxt(String fkRelationWithParentLngTxt) {
        this.fkRelationWithParentLngTxt = fkRelationWithParentLngTxt;
    }

    public String getAttrPathExpn() {
        return attrPathExpn;
    }

    public void setAttrPathExpn(String attrPathExpn) {
        this.attrPathExpn = attrPathExpn;
    }

    public String getAttrDispAxizCodeFkId() {
        return attrDispAxizCodeFkId;
    }

    public void setAttrDispAxizCodeFkId(String attrDispAxizCodeFkId) {
        this.attrDispAxizCodeFkId = attrDispAxizCodeFkId;
    }

    public int getAttrDisplaySeqNumPosInt() {
        return attrDisplaySeqNumPosInt;
    }

    public void setAttrDisplaySeqNumPosInt(int attrDisplaySeqNumPosInt) {
        this.attrDisplaySeqNumPosInt = attrDisplaySeqNumPosInt;
    }

    public boolean isAxizDataSourcedFromObject() {
        return isAxizDataSourcedFromObject;
    }

    public void setAxizDataSourcedFromObject(boolean axizDataSourcedFromObject) {
        isAxizDataSourcedFromObject = axizDataSourcedFromObject;
    }

    public String getAxizDataSourceMtPeCodeFkId() {
        return axizDataSourceMtPeCodeFkId;
    }

    public void setAxizDataSourceMtPeCodeFkId(String axizDataSourceMtPeCodeFkId) {
        this.axizDataSourceMtPeCodeFkId = axizDataSourceMtPeCodeFkId;
    }

    public String getAxizDataSourceFilterExpn() {
        return axizDataSourceFilterExpn;
    }

    public void setAxizDataSourceFilterExpn(String axizDataSourceFilterExpn) {
        this.axizDataSourceFilterExpn = axizDataSourceFilterExpn;
    }

    public boolean isDerivedAttr() {
        return isDerivedAttr;
    }

    public void setDerivedAttr(boolean derivedAttr) {
        isDerivedAttr = derivedAttr;
    }

    public String getDerivedAttrDispNameG11nBigTxt() {
        return derivedAttrDispNameG11nBigTxt;
    }

    public void setDerivedAttrDispNameG11nBigTxt(String derivedAttrDispNameG11nBigTxt) {
        this.derivedAttrDispNameG11nBigTxt = derivedAttrDispNameG11nBigTxt;
    }

    public int getOrderBySeqNumPosInt() {
        return orderBySeqNumPosInt;
    }

    public void setOrderBySeqNumPosInt(int orderBySeqNumPosInt) {
        this.orderBySeqNumPosInt = orderBySeqNumPosInt;
    }

    public String getOrderByModeCodeFkId() {
        return orderByModeCodeFkId;
    }

    public void setOrderByModeCodeFkId(String orderByModeCodeFkId) {
        this.orderByModeCodeFkId = orderByModeCodeFkId;
    }

    public String getOrderByValuesBigTxt() {
        return orderByValuesBigTxt;
    }

    public void setOrderByValuesBigTxt(String orderByValuesBigTxt) {
        this.orderByValuesBigTxt = orderByValuesBigTxt;
    }

    public boolean isGroupByAttribute() {
        return isGroupByAttribute;
    }

    public void setGroupByAttribute(boolean groupByAttribute) {
        isGroupByAttribute = groupByAttribute;
    }

    public int getGroupBySeqNumPosInt() {
        return groupBySeqNumPosInt;
    }

    public void setGroupBySeqNumPosInt(int groupBySeqNumPosInt) {
        this.groupBySeqNumPosInt = groupBySeqNumPosInt;
    }

    public String getGroupByHavingExpn() {
        return groupByHavingExpn;
    }

    public void setGroupByHavingExpn(String groupByHavingExpn) {
        this.groupByHavingExpn = groupByHavingExpn;
    }

    public boolean isUsedInFilter() {
        return isUsedInFilter;
    }

    public void setUsedInFilter(boolean usedInFilter) {
        isUsedInFilter = usedInFilter;
    }

    public boolean isAggregateAttr() {
        return isAggregateAttr;
    }

    public void setAggregateAttr(boolean aggregateAttr) {
        isAggregateAttr = aggregateAttr;
    }

    public boolean isClientMatch() {
        return isClientMatch;
    }

    public void setClientMatch(boolean clientMatch) {
        isClientMatch = clientMatch;
    }

    public boolean isClientDisplay() {
        return isClientDisplay;
    }

    public void setClientDisplay(boolean clientDisplay) {
        isClientDisplay = clientDisplay;
    }
}
