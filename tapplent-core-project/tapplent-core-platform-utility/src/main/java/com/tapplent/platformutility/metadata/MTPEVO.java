package com.tapplent.platformutility.metadata;

/**
 * Created by tapplent on 07/12/16.
 */
public class MTPEVO {
    private String metaProcessElementCodePkId;
    private String mtProcessElementNameG11nBigTxt;
    private String mtProcessElementIconCodeFkId;
    private String mtProcessCodeFkId;
    private String mtDomainObjectCodeFkId;
    private String parentMtProcessElementCodeFkId;

    public String getMetaProcessElementCodePkId() {
        return metaProcessElementCodePkId;
    }

    public void setMetaProcessElementCodePkId(String metaProcessElementCodePkId) {
        this.metaProcessElementCodePkId = metaProcessElementCodePkId;
    }

    public String getMtProcessElementNameG11nBigTxt() {
        return mtProcessElementNameG11nBigTxt;
    }

    public void setMtProcessElementNameG11nBigTxt(String mtProcessElementNameG11nBigTxt) {
        this.mtProcessElementNameG11nBigTxt = mtProcessElementNameG11nBigTxt;
    }

    public String getMtProcessElementIconCodeFkId() {
        return mtProcessElementIconCodeFkId;
    }

    public void setMtProcessElementIconCodeFkId(String mtProcessElementIconCodeFkId) {
        this.mtProcessElementIconCodeFkId = mtProcessElementIconCodeFkId;
    }

    public String getMtProcessCodeFkId() {
        return mtProcessCodeFkId;
    }

    public void setMtProcessCodeFkId(String mtProcessCodeFkId) {
        this.mtProcessCodeFkId = mtProcessCodeFkId;
    }

    public String getMtDomainObjectCodeFkId() {
        return mtDomainObjectCodeFkId;
    }

    public void setMtDomainObjectCodeFkId(String mtDomainObjectCodeFkId) {
        this.mtDomainObjectCodeFkId = mtDomainObjectCodeFkId;
    }

    public String getParentMtProcessElementCodeFkId() {
        return parentMtProcessElementCodeFkId;
    }

    public void setParentMtProcessElementCodeFkId(String parentMtProcessElementCodeFkId) {
        this.parentMtProcessElementCodeFkId = parentMtProcessElementCodeFkId;
    }
}
