package com.tapplent.platformutility.accessmanager.structure;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Shubham Patodi on 09/08/16.
 */
public class RowCondition {
    private String rowCondnPkId;
    private String accessProcessFkId;
    private String whoGroupId;
    private String whatDOCode;
    private String whatDOExpression;
    private String whoseGroupFKId;
    private String whatMtPECodeFkId;
    private String parentRowCondnFkId;
    private boolean isWhoseRelativeToWho;
    private String whoseWhoExpression;
    private boolean isWhoseRelativeToDO;
    private String whoseDOExpression;
    private Timestamp startDateTime;
    private Timestamp endDataTime;
    private boolean isPermissionNeverEnds;
    private boolean isCreateCurrentPermitted;
    private boolean isCreateHistoryPermitted;
    private boolean isCreateFuturePermitted;
    private boolean isReadCurrentPermitted;
    private boolean isReadHistoryPermitted;
    private boolean isReadFuturePermitted;
    private boolean isUpdateCurrentPermitted;
    private boolean isUpdateHistoryPermitted;
    private boolean isUpdateFuturePermitted;
    private boolean isDeleteCurrentPermitted;
    private boolean isDeleteHistoryPermitted;
    private boolean isDeleteFuturePermitted;
    private boolean isAllButSelfPermissioned;
    private List<AMColCondition> columnConditionList;
    /*
    VERSION_ID
    AM_ROW_CONDN_PK_ID
    AM_ROW_FK_ID
    WHAT_MT_PE_FILTER_EXPN
    WHOSE_GROUP_FK_ID
    WHOSE_PERSON_EXPN
    IS_WHOSE_RELATIVE_TO_WHO
    WHOSE_WHO_EXPN
    IS_WHOSE_RELATIVE_TO_DO
    WHOSE_DO_EXPN
    START_DATETIME
    END_DATETIME
    IS_PERM_NEVER_ENDS
    IS_CREATE_CURRENT_PERMITTED
    IS_CREATE_HISTORY_PERMITTED
    IS_CREATE_FUTURE_PERMITTED
    IS_READ_CURRENT_PERMITTED
    IS_READ_HISTORY_PERMITTED
    IS_READ_FUTURE_PERMITTED
    IS_UPDATE_CURRENT_PERMITTED
    IS_UPDATE_HISTORY_PERMITTED
    IS_UPDATE_FUTURE_PERMITTED
    IS_DELETE_CURRENT_PERMITTED
    IS_DELETE_HISTORY_PERMITTED
    IS_DELETE_FUTURE_PERMITTED
     */

    public List<AMColCondition> getColumnConditionList() {
        return columnConditionList;
    }

    public void setColumnConditionList(List<AMColCondition> columnConditionList) {
        this.columnConditionList = columnConditionList;
    }

    public String getRowCondnPkId() {
        return rowCondnPkId;
    }

    public void setRowCondnPkId(String rowCondnPkId) {
        this.rowCondnPkId = rowCondnPkId;
    }

    public String getWhoGroupId() {
        return whoGroupId;
    }

    public void setWhoGroupId(String whoGroupId) {
        this.whoGroupId = whoGroupId;
    }

    public String getWhatDOCode() {
        return whatDOCode;
    }

    public void setWhatDOCode(String whatDOCode) {
        this.whatDOCode = whatDOCode;
    }

    public String getWhatDOExpression() {
        return whatDOExpression;
    }

    public void setWhatDOExpression(String whatDOExpression) {
        this.whatDOExpression = whatDOExpression;
    }

    public String getWhoseGroupFKId() {
        return whoseGroupFKId;
    }

    public void setWhoseGroupFKId(String whoseGroupFKId) {
        this.whoseGroupFKId = whoseGroupFKId;
    }

    public String getWhatMtPECodeFkId() {
        return whatMtPECodeFkId;
    }

    public void setWhatMtPECodeFkId(String whatMtPECodeFkId) {
        this.whatMtPECodeFkId = whatMtPECodeFkId;
    }

    public String getParentRowCondnFkId() {
        return parentRowCondnFkId;
    }

    public void setParentRowCondnFkId(String parentRowCondnFkId) {
        this.parentRowCondnFkId = parentRowCondnFkId;
    }

    public boolean isWhoseRelativeToWho() {
        return isWhoseRelativeToWho;
    }

    public void setWhoseRelativeToWho(boolean whoseRelativeToWho) {
        isWhoseRelativeToWho = whoseRelativeToWho;
    }

    public String getWhoseWhoExpression() {
        return whoseWhoExpression;
    }

    public void setWhoseWhoExpression(String whoseWhoExpression) {
        this.whoseWhoExpression = whoseWhoExpression;
    }

    public boolean isWhoseRelativeToDO() {
        return isWhoseRelativeToDO;
    }

    public void setWhoseRelativeToDO(boolean whoseRelativeToDO) {
        isWhoseRelativeToDO = whoseRelativeToDO;
    }

    public String getWhoseDOExpression() {
        return whoseDOExpression;
    }

    public void setWhoseDOExpression(String whoseDOExpression) {
        this.whoseDOExpression = whoseDOExpression;
    }

    public Timestamp getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Timestamp startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Timestamp getEndDataTime() {
        return endDataTime;
    }

    public void setEndDataTime(Timestamp endDataTime) {
        this.endDataTime = endDataTime;
    }

    public boolean isPermissionNeverEnds() {
        return isPermissionNeverEnds;
    }

    public void setPermissionNeverEnds(boolean permissionNeverEnds) {
        isPermissionNeverEnds = permissionNeverEnds;
    }

    public boolean isCreateCurrentPermitted() {
        return isCreateCurrentPermitted;
    }

    public void setCreateCurrentPermitted(boolean createCurrentPermitted) {
        isCreateCurrentPermitted = createCurrentPermitted;
    }

    public boolean isCreateHistoryPermitted() {
        return isCreateHistoryPermitted;
    }

    public void setCreateHistoryPermitted(boolean createHistoryPermitted) {
        isCreateHistoryPermitted = createHistoryPermitted;
    }

    public boolean isCreateFuturePermitted() {
        return isCreateFuturePermitted;
    }

    public void setCreateFuturePermitted(boolean createFuturePermitted) {
        isCreateFuturePermitted = createFuturePermitted;
    }

    public boolean isReadCurrentPermitted() {
        return isReadCurrentPermitted;
    }

    public void setReadCurrentPermitted(boolean readCurrentPermitted) {
        isReadCurrentPermitted = readCurrentPermitted;
    }

    public boolean isReadHistoryPermitted() {
        return isReadHistoryPermitted;
    }

    public void setReadHistoryPermitted(boolean readHistoryPermitted) {
        isReadHistoryPermitted = readHistoryPermitted;
    }

    public boolean isReadFuturePermitted() {
        return isReadFuturePermitted;
    }

    public void setReadFuturePermitted(boolean readFuturePermitted) {
        isReadFuturePermitted = readFuturePermitted;
    }

    public boolean isUpdateCurrentPermitted() {
        return isUpdateCurrentPermitted;
    }

    public void setUpdateCurrentPermitted(boolean updateCurrentPermitted) {
        isUpdateCurrentPermitted = updateCurrentPermitted;
    }

    public boolean isUpdateHistoryPermitted() {
        return isUpdateHistoryPermitted;
    }

    public void setUpdateHistoryPermitted(boolean updateHistoryPermitted) {
        isUpdateHistoryPermitted = updateHistoryPermitted;
    }

    public boolean isUpdateFuturePermitted() {
        return isUpdateFuturePermitted;
    }

    public void setUpdateFuturePermitted(boolean updateFuturePermitted) {
        isUpdateFuturePermitted = updateFuturePermitted;
    }

    public boolean isDeleteCurrentPermitted() {
        return isDeleteCurrentPermitted;
    }

    public void setDeleteCurrentPermitted(boolean deleteCurrentPermitted) {
        isDeleteCurrentPermitted = deleteCurrentPermitted;
    }

    public boolean isDeleteHistoryPermitted() {
        return isDeleteHistoryPermitted;
    }

    public void setDeleteHistoryPermitted(boolean deleteHistoryPermitted) {
        isDeleteHistoryPermitted = deleteHistoryPermitted;
    }

    public boolean isDeleteFuturePermitted() {
        return isDeleteFuturePermitted;
    }

    public void setDeleteFuturePermitted(boolean deleteFuturePermitted) {
        isDeleteFuturePermitted = deleteFuturePermitted;
    }

    public String getAccessProcessFkId() {
        return accessProcessFkId;
    }

    public void setAccessProcessFkId(String accessProcessFkId) {
        this.accessProcessFkId = accessProcessFkId;
    }

    public boolean isAllButSelfPermissioned() {
        return isAllButSelfPermissioned;
    }

    public void setAllButSelfPermissioned(boolean allButSelfPermissioned) {
        isAllButSelfPermissioned = allButSelfPermissioned;
    }
}
