package com.tapplent.platformutility.batch.helper;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.model.DG_DocGenData;

public class DG_SyncDocGenCaller {

	public String generateDoc(String personDocumentId) throws Exception{
		
		DG_DocGenData item = new DG_DocGenData();
		//(TDWI)query from t_person_document and create DG_DocGenData object
		DG_DocGenHelper dg_docGenHelper = new DG_DocGenHelper();
		if (item.getDocTypeCodeFkId().equals(Constants.pdf)) {

			String s3link = DG_ConvertURLtoDocType.generatePDF(
					item,
					dg_docGenHelper.getPassword(item));
			item.setFileAttach(s3link);
			if(item.isSuccess()==true){
			item.setReturnStatusCodeFkId("COMPLETED");}
		} else if (item.getDocTypeCodeFkId().equals(Constants.ppt)) {

			DG_PPTGen pptGen = new DG_PPTGen(dg_docGenHelper.getPPTemplate(item), dg_docGenHelper.getPPTData(item),
					dg_docGenHelper.getPPTDoaMap(item));
			item.setFileAttach(pptGen.s3Link);
			if(item.isSuccess()==true){
				item.setReturnStatusCodeFkId("COMPLETED");}

		} else if (item.getDocTypeCodeFkId().equals(Constants.word)) {

			String s3link = DG_ConvertURLtoDocType
					.generateWord(item);
			item.setFileAttach(s3link);
			if(item.isSuccess()==true){
				item.setReturnStatusCodeFkId("COMPLETED");}
		} else if (item.getDocTypeCodeFkId().equals(Constants.jpeg)) {

			String s3link = DG_ConvertURLtoDocType
					.generateJPG(item);
			item.setFileAttach(s3link);
			if(item.isSuccess()==true){
				item.setReturnStatusCodeFkId("COMPLETED");}
		} else if (item.getDocTypeCodeFkId().equals(Constants.csv)) {

			// (TDWI) same as analytics

		}
		
		if(item.isSuccess()==true){
			//(TDWI)update T_PersonDocument table
			
		}
		else if(item.isSuccess()==false){			
			return item.getError().toString();
		}
		return item.getFileAttach();

		
	}
	
	
}
