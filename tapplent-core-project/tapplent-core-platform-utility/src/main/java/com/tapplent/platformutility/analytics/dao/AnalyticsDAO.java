package com.tapplent.platformutility.analytics.dao;

import com.tapplent.platformutility.analytics.structure.*;

import java.util.List;
import java.util.Map;

/**
 * Created by tapplent on 05/12/16.
 */
public interface AnalyticsDAO {
    AnalyticsMetric getAnalyticMetric(String analyticsMetricId);

    Map<String,AnalyticsMetricDetails> getAnalyticMetricDetails(String analyticsMetric, String analysisTypeCode);

    Map<String,AnalyticsMetricDetailsQuery> getAnalyticMetricDetailsQuery(String  analyticsMetric);

    List<AnalyticsMetricDetailsFilter> getAnalyticMetricDetailsFilter(String  analyticsMetric);

    AnalyticsMetricDetails getAnalyticsMetricDetails(String metricDetailsId, Boolean isDrillDown, Boolean isTable);

    List<String> getAnalysisMetricDetailsPks(String parentAnalysisMetricDetailId);

    List<AnalyticsAxisAndMetricMapVO> getAxisAndMetricMapList(String metricCategoryId, String metricCatMenuType, String axisAndMetricMapPKID);

    List<GraphTypeVO> getApplicableGraphTypes(String metricAxisMapID);

    AnalyticsMetricChartVO getAnalyticsMetricChart(String metricAxisMapID);

    List<AnalyticsMetricChartQueryVO> getAnalyticsMetricChartFilters(String chartPkID);

    AnalyticsAxisAndMetricMapVO getAxisAndMetricMap(String metricAxisMapID);

    MetricAxisVO getAxisDetails(String xAxisID);

    List<TimePeriodOffsetVO> getTimeperiodOffsetsForLoggedInUser();

    List<CalendarPeriodUnitVO> getCalendarPeriodUnits();
}
