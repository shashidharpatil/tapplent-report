package com.tapplent.platformutility.batch.controller;

import com.tapplent.platformutility.batch.constants.Constants;
import com.tapplent.platformutility.batch.helper.NTFN_NotificationHelper;
import com.tapplent.platformutility.batch.model.SchedulerInstanceModel;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.Callable;

public class RecordStateCalc implements Callable {

    SchedulerInstanceModel schedulerInstanceModel;

    public RecordStateCalc(SchedulerInstanceModel schedulerInstanceModel) {
        this.schedulerInstanceModel = schedulerInstanceModel;
    }

    @Override
    public Object call() throws Exception {
        String[] springConfig = {"jobs/Notification.xml"};
        ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
        JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("schedulerInstancePkId", schedulerInstanceModel.getSchedulerInstancePkId());
        Job job = (Job) context.getBean("Notification");
        JobExecution execution = null;
        try {
            execution = jobLauncher.run(job, jobParametersBuilder.toJobParameters());
            System.out.println("ststus" + execution.getExitStatus());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}