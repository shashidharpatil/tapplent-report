package com.tapplent.platformutility.batch.model;

public class DG_PPTDoaMap {

	private String exportPptTemplateDoaPkId;
	private String exportPptTemplateFkId;
	private String pptSlideidTxt;
	private String pptShapeidTxt;
	private String pptBtCodeFkId;
	private String pptMtDoaPathLngTxt;
	public String getExportPptTemplateDoaPkId() {
		return exportPptTemplateDoaPkId;
	}
	public void setExportPptTemplateDoaPkId(String exportPptTemplateDoaPkId) {
		this.exportPptTemplateDoaPkId = exportPptTemplateDoaPkId;
	}
	public String getExportPptTemplateFkId() {
		return exportPptTemplateFkId;
	}
	public void setExportPptTemplateFkId(String exportPptTemplateFkId) {
		this.exportPptTemplateFkId = exportPptTemplateFkId;
	}
	public String getPptSlideidTxt() {
		return pptSlideidTxt;
	}
	public void setPptSlideidTxt(String pptSlideidTxt) {
		this.pptSlideidTxt = pptSlideidTxt;
	}
	public String getPptShapeidTxt() {
		return pptShapeidTxt;
	}
	public void setPptShapeidTxt(String pptShapeidTxt) {
		this.pptShapeidTxt = pptShapeidTxt;
	}
	public String getPptBtCodeFkId() {
		return pptBtCodeFkId;
	}
	public void setPptBtCodeFkId(String pptBtCodeFkId) {
		this.pptBtCodeFkId = pptBtCodeFkId;
	}
	public String getPptMtDoaPathLngTxt() {
		return pptMtDoaPathLngTxt;
	}
	public void setPptMtDoaPathLngTxt(String pptMtDoaPathLngTxt) {
		this.pptMtDoaPathLngTxt = pptMtDoaPathLngTxt;
	}
	
	
	
}
