package com.tapplent.platformutility.common.util;

import java.io.IOException;


import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.simple.parser.JSONParser;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;


//import jdk.nashorn.internal.parser.JSONParser;

public class GmailTokenUtil {
	private static final Logger LOG = LogFactory.getLogger(GmailTokenUtil.class);
	/*
	 * This method takes refresh token as input and gives back the access token by using gmail apis
	 */
	@SuppressWarnings("deprecation")
	public static String getAccessToken(String refreshToken){
		List<BasicNameValuePair> para = new ArrayList<BasicNameValuePair>();
		String CLIENT_ID="912120029717-9od70gv46bqspm5ubfe8gpi8qj2bc53d.apps.googleusercontent.com";
		String CLIENT_SECRET="Hwp1KELw-UdViDzlb3yjbonm";
		para.add(new BasicNameValuePair("client_id",CLIENT_ID));
		para.add(new BasicNameValuePair("client_secret", CLIENT_SECRET));
		para.add(new BasicNameValuePair("refresh_token",refreshToken));
		para.add(new BasicNameValuePair("grant_type","refresh_token"));			      
		HttpPost post2 = new HttpPost("https://www.googleapis.com/oauth2/v4/token");
		HttpClient client2 = HttpClientBuilder.create().build();
		HttpResponse response2 = null;
		try {
			post2.setEntity(new UrlEncodedFormEntity(para, HTTP.UTF_8));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		try {
			response2 = client2.execute(post2);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Map<String, String> map2 = new HashMap<String, String>();
		map2= handleJsonResponse(response2);
		String acctoken=map2.get("access_token"); 
		return acctoken;		
	}
	public static Map<String, String> handleResponse(HttpResponse response) {
		String contentType = null;
		if (response.getEntity().getContentType() != null) {
			contentType = response.getEntity().getContentType().getValue();
		}
		System.out.print(contentType);
		if (contentType.contains("application/json")) {
			return handleJsonResponse(response);
		} else {
			// Unsupported Content type
			throw new RuntimeException(
					"Cannot handle "
							+ contentType
							+ " content type. Supported content types include JSON, XML and URLEncoded");
		}

	}
	@SuppressWarnings("unchecked")
	public static Map<String, String> handleJsonResponse(HttpResponse response) {
		Map<String, String> oauthLoginResponse = null;
		try {
			oauthLoginResponse = (Map<String, String>) new JSONParser()
					.parse(EntityUtils.toString(response.getEntity()));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		LOG.debug("********** Response Received **********");
		for (Map.Entry<String, String> entry : oauthLoginResponse.entrySet()) {
			System.out.println(String.format("  %s = %s", entry.getKey(),
					entry.getValue()));
		}
		return oauthLoginResponse;
	}
}
