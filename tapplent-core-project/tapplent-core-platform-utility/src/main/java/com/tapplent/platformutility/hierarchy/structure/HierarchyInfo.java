package com.tapplent.platformutility.hierarchy.structure;

/**
 * Created by tapplent on 26/01/17.
 */
public class HierarchyInfo {
    private String pk;
    private String hierarchyPk;
    public HierarchyInfo(){

    }

    public HierarchyInfo(String pk, String hierarchyPk) {
        this.pk = pk;
        this.hierarchyPk = hierarchyPk;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getHierarchyPk() {
        return hierarchyPk;
    }

    public void setHierarchyPk(String hierarchyPk) {
        this.hierarchyPk = hierarchyPk;
    }
}
