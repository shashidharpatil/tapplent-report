package com.tapplent.platformutility.common.util.valueObject;

/**
 * Created by tapplent on 12/12/17.
 */
public class AnalyticsDOAControlMap {
    private String pkID;
    private String analyticsDOA;
    private String controlInstanceID;

    public String getAnalyticsDOA() {
        return analyticsDOA;
    }

    public void setAnalyticsDOA(String analyticsDOA) {
        this.analyticsDOA = analyticsDOA;
    }

    public String getControlInstanceID() {
        return controlInstanceID;
    }

    public void setControlInstanceID(String controlInstanceID) {
        this.controlInstanceID = controlInstanceID;
    }

    public String getPkID() {
        return pkID;
    }

    public void setPkID(String pkID) {
        this.pkID = pkID;
    }
}
