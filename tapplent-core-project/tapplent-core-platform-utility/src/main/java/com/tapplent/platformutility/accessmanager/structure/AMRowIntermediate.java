package com.tapplent.platformutility.accessmanager.structure;

/**
 * Created by tapplent on 04/11/16.
 */
public class AMRowIntermediate {
    private String amRowIntermediatePkId;
    private String whoPersonFkId;
    private String domainObjectCodeFkId;
    private String whatDbPrimaryKey;
    private String whatFunctionalPrimaryKeyId;
    private String whoseSubjectUserPersonFkId;
    private String amRowCondnFkId;
    private String amAccessProcessFkId;
    private String amDefnFkId;
    private boolean isCreatePermitted;
    private boolean isReadPermitted;
    private boolean isUpdatePermitted;
    private boolean isDeletePermitted;

    public String getAmRowIntermediatePkId() {
        return amRowIntermediatePkId;
    }

    public void setAmRowIntermediatePkId(String amRowIntermediatePkId) {
        this.amRowIntermediatePkId = amRowIntermediatePkId;
    }

    public String getWhoPersonFkId() {
        return whoPersonFkId;
    }

    public void setWhoPersonFkId(String whoPersonFkId) {
        this.whoPersonFkId = whoPersonFkId;
    }

    public String getDomainObjectCodeFkId() {
        return domainObjectCodeFkId;
    }

    public void setDomainObjectCodeFkId(String domainObjectCodeFkId) {
        this.domainObjectCodeFkId = domainObjectCodeFkId;
    }

    public String getWhatDbPrimaryKey() {
        return whatDbPrimaryKey;
    }

    public void setWhatDbPrimaryKey(String whatDbPrimaryKey) {
        this.whatDbPrimaryKey = whatDbPrimaryKey;
    }

    public String getWhatFunctionalPrimaryKeyId() {
        return whatFunctionalPrimaryKeyId;
    }

    public void setWhatFunctionalPrimaryKeyId(String whatFunctionalPrimaryKeyId) {
        this.whatFunctionalPrimaryKeyId = whatFunctionalPrimaryKeyId;
    }

    public String getWhoseSubjectUserPersonFkId() {
        return whoseSubjectUserPersonFkId;
    }

    public void setWhoseSubjectUserPersonFkId(String whoseSubjectUserPersonFkId) {
        this.whoseSubjectUserPersonFkId = whoseSubjectUserPersonFkId;
    }

    public String getAmRowCondnFkId() {
        return amRowCondnFkId;
    }

    public void setAmRowCondnFkId(String amRowCondnFkId) {
        this.amRowCondnFkId = amRowCondnFkId;
    }

    public String getAmAccessProcessFkId() {
        return amAccessProcessFkId;
    }

    public void setAmAccessProcessFkId(String amAccessProcessFkId) {
        this.amAccessProcessFkId = amAccessProcessFkId;
    }

    public String getAmDefnFkId() {
        return amDefnFkId;
    }

    public void setAmDefnFkId(String amDefnFkId) {
        this.amDefnFkId = amDefnFkId;
    }

    public boolean isCreatePermitted() {
        return isCreatePermitted;
    }

    public void setCreatePermitted(boolean createPermitted) {
        isCreatePermitted = createPermitted;
    }

    public boolean isReadPermitted() {
        return isReadPermitted;
    }

    public void setReadPermitted(boolean readPermitted) {
        isReadPermitted = readPermitted;
    }

    public boolean isUpdatePermitted() {
        return isUpdatePermitted;
    }

    public void setUpdatePermitted(boolean updatePermitted) {
        isUpdatePermitted = updatePermitted;
    }

    public boolean isDeletePermitted() {
        return isDeletePermitted;
    }

    public void setDeletePermitted(boolean deletePermitted) {
        isDeletePermitted = deletePermitted;
    }
}
