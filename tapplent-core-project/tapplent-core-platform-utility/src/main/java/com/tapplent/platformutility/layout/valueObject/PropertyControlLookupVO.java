package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

public class PropertyControlLookupVO {
	private String propertyControlCode;
	private String propertyControlName;
	private String controlTypeCode;
	private String propertyControlIcon;
	private boolean isGlocalized;
	private boolean isBooleanData;
	private String sourceCategory;
	private String layoutKey;
	private String sourceDomainObject;
	private String sourceDOA;
	private String returnDOA;
	private String booleanTrueStateLayoutKey;
	private String booleanFalseStateLayoutKey;
	public String getPropertyControlCode() {
		return propertyControlCode;
	}
	public void setPropertyControlCode(String propertyControlCode) {
		this.propertyControlCode = propertyControlCode;
	}
	public String getPropertyControlName() {
		return propertyControlName;
	}
	public void setPropertyControlName(String propertyControlName) {
		this.propertyControlName = propertyControlName;
	}
	public String getControlTypeCode() {
		return controlTypeCode;
	}
	public void setControlTypeCode(String controlTypeCode) {
		this.controlTypeCode = controlTypeCode;
	}
	public String getPropertyControlIcon() {
		return propertyControlIcon;
	}
	public void setPropertyControlIcon(String propertyControlIcon) {
		this.propertyControlIcon = propertyControlIcon;
	}
	public boolean isGlocalized() {
		return isGlocalized;
	}
	public void setGlocalized(boolean isGlocalized) {
		this.isGlocalized = isGlocalized;
	}
	public String getSourceCategory() {
		return sourceCategory;
	}
	public void setSourceCategory(String sourceCategory) {
		this.sourceCategory = sourceCategory;
	}
	public String getLayoutKey() {
		return layoutKey;
	}
	public void setLayoutKey(String layoutKey) {
		this.layoutKey = layoutKey;
	}
	public String getSourceDomainObject() {
		return sourceDomainObject;
	}
	public void setSourceDomainObject(String sourceDomainObject) {
		this.sourceDomainObject = sourceDomainObject;
	}
	public String getSourceDOA() {
		return sourceDOA;
	}
	public void setSourceDOA(String sourceDOA) {
		this.sourceDOA = sourceDOA;
	}
	public String getReturnDOA() {
		return returnDOA;
	}
	public void setReturnDOA(String returnDOA) {
		this.returnDOA = returnDOA;
	}
	public boolean isBooleanData() {
		return isBooleanData;
	}
	public void setBooleanData(boolean isBooleanData) {
		this.isBooleanData = isBooleanData;
	}
	public String getBooleanTrueStateLayoutKey() {
		return booleanTrueStateLayoutKey;
	}
	public void setBooleanTrueStateLayoutKey(String booleanTrueStateLayoutKey) {
		this.booleanTrueStateLayoutKey = booleanTrueStateLayoutKey;
	}
	public String getBooleanFalseStateLayoutKey() {
		return booleanFalseStateLayoutKey;
	}
	public void setBooleanFalseStateLayoutKey(String booleanFalseStateLayoutKey) {
		this.booleanFalseStateLayoutKey = booleanFalseStateLayoutKey;
	}
}
