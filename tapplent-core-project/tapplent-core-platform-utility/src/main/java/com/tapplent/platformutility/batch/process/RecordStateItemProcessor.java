package com.tapplent.platformutility.batch.process;

import com.tapplent.platformutility.batch.model.RCRD_STATE_MtDOData;
import org.springframework.batch.item.ItemProcessor;

import java.util.Date;

public class RecordStateItemProcessor implements ItemProcessor<RCRD_STATE_MtDOData, RCRD_STATE_MtDOData> {
    @Override
    public RCRD_STATE_MtDOData process(RCRD_STATE_MtDOData item) throws Exception {
        return item;
    }
}
