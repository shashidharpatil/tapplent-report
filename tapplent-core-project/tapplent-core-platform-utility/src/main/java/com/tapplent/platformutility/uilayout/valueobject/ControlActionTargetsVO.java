package com.tapplent.platformutility.uilayout.valueobject;

/**
 * Created by tapplent on 26/04/17.
 */
public class ControlActionTargetsVO {
    private String actionTargetPkId;
    private String controlActionFkId;
    private String targetSections;

    public String getActionTargetPkId() {
        return actionTargetPkId;
    }

    public void setActionTargetPkId(String actionTargetPkId) {
        this.actionTargetPkId = actionTargetPkId;
    }

    public String getControlActionFkId() {
        return controlActionFkId;
    }

    public void setControlActionFkId(String controlActionFkId) {
        this.controlActionFkId = controlActionFkId;
    }

    public String getTargetSections() {
        return targetSections;
    }

    public void setTargetSections(String targetSections) {
        this.targetSections = targetSections;
    }
}
