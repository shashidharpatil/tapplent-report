package com.tapplent.platformutility.common.util.valueObject;

/**
 * Created by Shubham Patodi on 05/08/16.
 */
public class ErrorLogVO {
    private String systemErrorLogId;
    private String errorDescription;
    private String createDateTime;
    private String createAuthor;
    private boolean isResolved;
    private String resolutionComment;
    private String resolvedDateTime;
    private String resolvedBy;

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(String createDateTime) {
        this.createDateTime = createDateTime;
    }

    public String getCreateAuthor() {
        return createAuthor;
    }

    public void setCreateAuthor(String createAuthor) {
        this.createAuthor = createAuthor;
    }

    public boolean isResolved() {
        return isResolved;
    }

    public void setResolved(boolean resolved) {
        isResolved = resolved;
    }

    public String getResolutionComment() {
        return resolutionComment;
    }

    public void setResolutionComment(String resolutionComment) {
        this.resolutionComment = resolutionComment;
    }

    public String getResolvedDateTime() {
        return resolvedDateTime;
    }

    public void setResolvedDateTime(String resolvedDateTime) {
        this.resolvedDateTime = resolvedDateTime;
    }

    public String getResolvedBy() {
        return resolvedBy;
    }

    public String getSystemErrorLogId() {
        return systemErrorLogId;
    }

    public void setSystemErrorLogId(String systemErrorLogId) {
        this.systemErrorLogId = systemErrorLogId;
    }

    public void setResolvedBy(String resolvedBy) {
        this.resolvedBy = resolvedBy;
    }
}
