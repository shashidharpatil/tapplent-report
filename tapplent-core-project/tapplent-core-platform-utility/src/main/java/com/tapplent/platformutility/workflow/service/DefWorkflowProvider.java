package com.tapplent.platformutility.workflow.service;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.common.util.Common;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.entity.provider.GlobalVariables;
import com.tapplent.platformutility.expression.jeval.EvaluationException;
import com.tapplent.platformutility.expression.jeval.Evaluator;
import com.tapplent.platformutility.insert.impl.InsertDAO;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.search.impl.SearchDAO;
import com.tapplent.platformutility.workflow.structure.*;
import com.tapplent.tenantresolver.server.property.ServerActionStep;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Created by Manas on 8/9/16.
 */
public class DefWorkflowProvider implements WorkflowProvider {

    private static final Logger LOG = LogFactory.getLogger(DefWorkflowProvider.class);

    SearchDAO searchDAO;

    InsertDAO insertDAO;

    WorkflowUtility workflowUtility;

    @Override
    public void CreateLaunchWorkflowIfApplicable(GlobalVariables globalVariables) throws SQLException, ParseException {
        WorkflowSelection workflowSelection = checkIfWorflowCreationApplicable(globalVariables);
        if(workflowSelection != null){
            //update UR with workflow definition Id
            //
            String workflowTxnId = Common.getUUID();
            globalVariables.workflowDetails.workflowTxnId = workflowTxnId;
            globalVariables.workflowDetails.workflowDefId = workflowSelection.getWorkflowDefinitionId();
            globalVariables.workflowDetails.workflowStatus = "IN_PROGRESS";

            createWorkflowTxnSteps(globalVariables, workflowSelection.getWorkflowDefinitionId());

            EntityMetadataVOX doMeta = globalVariables.entityMetadataVOX;
            EntityAttributeMetadata doPkMeta = doMeta.getFunctionalPrimaryKeyAttribute();
            EntityAttributeMetadata doVersionMeta = doMeta.getVersoinIdAttribute();

            String sql = "INSERT INTO T_PFM_IEU_WORKFLOW_TXN (WORKFLOW_TXN_PK_ID, WORKFLOW_DEFINITION_FK_ID, PROCESS_TYPE_CODE_FK_ID, " +
                    "BT_CODE_FK_ID, BUSS_RULE_CODE_FK_ID, DO_ROW_PRIMARY_KEY_FK_ID ,DO_ROW_VERSION_FK_ID, DISPLAY_CONTEXT_VALUE_G11N_BIG_TXT," +
                    " DO_ICON_CODE_FK_ID, DO_NAME_G11N_BIG_TXT, TARGET_SCREEN_INSTANCE_FK_ID, TARGET_SCREEN_SECTION_INSTANCE_FK_ID, " +
                    "WORKFLOW_STATUS_CODE_FK_ID, IS_PROPOGATE_FLAG, IS_DELETED, CREATED_DATETIME, LAST_MODIFIED_DATETIME) VALUES " +
                    "("+workflowTxnId+","+workflowSelection.getWorkflowDefinitionId()+", '"+workflowSelection.getProcessTypeCode()+"', ?, ?, ?, ?, ?, ?, ?, NULL, NULL, '"+globalVariables.workflowDetails.workflowStatus+"'," +
                    " ?, false, ?, ?)";
            Deque<Object> parameter = new ArrayDeque<>();
//            parameter.add("NULL");//(globalVariables.processTypeCode);//fixme
            parameter.add(globalVariables.baseTemplateId);
            parameter.add(globalVariables.businessRule);
            parameter.add(Util.convertStringIdToByteId((String) globalVariables.underlyingRecord.get(doPkMeta.getDoAttributeCode())));
            parameter.add(Util.convertStringIdToByteId((String) globalVariables.underlyingRecord.get(doVersionMeta.getDoAttributeCode())));
            parameter.add(globalVariables.displayContextValue == null ? "NULL": globalVariables.displayContextValue);
            parameter.add(globalVariables.entityMetadataVOX.getDoSummaryTitleIcon() == null ? "NULL": globalVariables.entityMetadataVOX.getDoSummaryTitleIcon().getCode());//fixme
            parameter.add(globalVariables.entityMetadataVOX.getBtDOG11nName());//fixme
//            parameter.add(null);
//            parameter.add(null);
            parameter.add(false);
            parameter.add(globalVariables.currentTimestampString);
            parameter.add(globalVariables.currentTimestampString);
            insertDAO.insertRowWithParameter(sql, parameter);
            workflowUtility.updateWorkflowDetailsInUnderlyingRecord(globalVariables);
        }
    }

    private void createWorkflowTxnSteps(GlobalVariables globalVariables, String workflowDefFkId) throws ParseException, SQLException {
        Map<Integer, WorkflowStepDefn> steps = getWorkflowDefSteps(workflowDefFkId);
        StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_WORKFLOW_TXN_STEP (WORKFLOW_TXN_STEP_PK_ID, WORKFLOW_TXN_FK_ID, WORKFLOW_STEP_DEFN_FK_ID," +
                "STEP_ACTUAL_START_DATETIME, STEP_ACTUAL_END_DATETIME, STEP_STATUS_CODE_FK_ID, IS_DELETED, CREATED_DATETIME, LAST_MODIFIED_DATETIME) VALUES ");
        Map<String, WorkflowStep> txnSteps = new LinkedHashMap<>();
        int i = 1;
        for(WorkflowStepDefn workflowStepDefn : steps.values()){
            workflowStepDefn = steps.get(i++);
            String workflowDefnId = workflowStepDefn.getWorkflowStepDefnPkId();
            WorkflowStep workflowStep = WorkflowStep.fromDefinition(workflowStepDefn);
            workflowStep.setWorkflowTxnFkId(globalVariables.workflowDetails.workflowTxnId);
            /*
            FIXME Expression Parser for actual Datetime calculation
            workflowStepTxn.setStepActualStartDatetime(null);
            workflowStepTxn.setStepActualEndDatetime(null);
            */
            workflowStep.setStepActualStartDatetime(Util.getTimestampFromString(workflowStepDefn.getStartDateExpn()));
            workflowStep.setStepActualEndDatetime(Util.getTimestampFromString(workflowStepDefn.getDueDateExpn()));
            workflowStep.setStepStatusCodeFkId("NOT_STARTED");
            if (i == 2){
                if (workflowStep.isDueDateEnforced() && workflowStep.getStepActualStartDatetime().before(globalVariables.currentTimestamp))
                    workflowStep.setStepStatusCodeFkId("IN_PROGRESS");
                globalVariables.workflowDetails.workflowTxnStepId = workflowStep.getWorkflowTxnStepPkId();
                globalVariables.workflowDetails.workflowTxnStepStatus = workflowStep.getStepStatusCodeFkId();
            }
            sql.append("("+workflowStep.getWorkflowTxnStepPkId()+", "+globalVariables.workflowDetails.workflowTxnId+", "+ workflowStepDefn.getWorkflowStepDefnPkId()+", ");
            sql.append("'"+workflowStep.getStepActualStartDatetime()+"', '"+workflowStep.getStepActualEndDatetime()+"', '"+workflowStep.getStepStatusCodeFkId()+"'," +
                    " false, '"+globalVariables.currentTimestampString+"', '"+globalVariables.currentTimestampString+"'), ");

            txnSteps.put(workflowDefnId, workflowStep);
        }
        sql.replace(sql.length() - 2, sql.length(), ";");
        insertDAO.insertRow(sql.toString());
        createWorkFlowTxnPossibleActorActions(globalVariables, txnSteps);
    }

    private void createWorkFlowTxnPossibleActorActions(GlobalVariables globalVariables, Map<String, WorkflowStep> workflowSteps) throws SQLException {
        for (WorkflowStep currentWorkflowStep : workflowSteps.values()) {
            if ((currentWorkflowStep.getStepActorResolutionCodeFkId().equalsIgnoreCase("AT_LAUNCH") /*&&
                    currentWorkflowStep.getStepActualStartDatetime().before(Common.getCurrentTimeStamp())) ||
                    (currentWorkflowStep.getStepActorResolutionCodeFkId().equalsIgnoreCase("AT_LAUNCH") /* &&
                            currentWorkflowStep.getStepActualStartDatetime().after(Common.getCurrentTimeStamp()) && !currentWorkflowStep.isBeginStep()*/)) {//NOT_REQUIRED AT_LAUNCH- 1 step should be resolved when start step date is reached but all subsequent steps with AT_LAUNCH code should be resolved without taking care of the respective start step dates.
                List<WorkflowActor> workflowActors = workflowUtility.resolveActors(globalVariables, currentWorkflowStep, workflowSteps);
            }
//            else if (currentWorkflowStep.getStepActorResolutionCodeFkId().equalsIgnoreCase("AT_STEP") /*&&
//                    currentWorkflowStep.getStepActualStartDatetime().before(Common.getCurrentTimeStamp())*/) {
//                List<WorkflowActor> workflowActors = workflowUtility.resolveActors(globalVariables, currentWorkflowStep, workflowSteps);
//              NOT REQUIRED  break; - atmost 1 step should be resolved at the time of creation for AT_STEP; either first or any subsequent one.
//            }
        }
    }

//    @Override
//    public List<WorkflowActor> resolveActors(GlobalVariables globalVariables, WorkflowStepDefn workflowStepDefn, WorkflowStepTxn workflowStepTxn) {
//        Map<String, WorkflowActor> workflowActors = getWorkflowDefActors(globalVariables, workflowStepDefn);//workflowActorId->workflowActor
//        Map<String, WorkflowActor> resolvedPersonIdActorIdActorMap = getResolvedPersonsFromActors(globalVariables, workflowActors);//personId_ActorId->workflowActor //not unique--> actor-person combination make it unique.
//        Map<String, Map<String, WorkflowActionDefn>> resolvedPersonIdActorIdActionMap = getResolvedActionsFromActorPersons(globalVariables, resolvedPersonIdActorIdActorMap);//personId_actorId->actionPk->action FIXME resolve logic for uniqueness!
//        updateStepActorTxn(globalVariables, resolvedPersonIdActorIdActorMap, resolvedPersonIdActorIdActionMap, workflowStepTxn);
//        return null;
//    }

    /*
    private void updateActionTxn(GlobalVariables globalVariables, Map<String, Map<String, WorkflowActionDefn>> resolvedPersonIdActionMap) {
        Map<String, WorkflowActionDefn> uniquecompositeKeyActionMap = removeDuplicateActions(globalVariables, resolvedPersonIdActionMap);//this doesn't make sense as the actors and hence actions will now get resolved only when the whole step moves(or the exit condition passes) and not when the action is being taken. transaction table me entry 
        if(!uniquecompositeKeyActionMap.isEmpty()) {
            StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION (WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID, WORKFLOW_TXN_STEP_ACTOR_FK_ID, ACTION_TYPE_CODE_FK_ID, " +
                    "TARGET_WFLOW_TXN_STEP_FK_ID, TARGET_WFLOW_TXN_STEP_ACTOR_FK_ID, TARGET_STEP_ACTOR_DEFN_FK_ID, IF_ACTION_CONDITION_EXPN, " +
                    "IS_COMMENT_MANDATORY_FLAG, IS_DELETED, CREATED_DATETIME, LAST_MODIFIED_DATETIME) VALUES (");
            for (Entry<String, WorkflowActionDefn> uniqueActionEntry : uniquecompositeKeyActionMap.entrySet()) {
                String[] keys = uniqueActionEntry.getKey().split("_");
                String srcActorPersonFkId = keys[2];
                WorkflowActionDefn action = uniqueActionEntry.getValue();
                sql.append("x'" + Common.getUUID() + "', ");
                sql.append("x'" + globalVariables.workflowDetails.workflowTxnId + "', ");
                sql.append("x'" + srcActorPersonFkId + "', ");
                sql.append("x'" + action.getSourceActorFkId() + "', ");
                sql.append("null, null, 'NOT_ACTED', null, ");
                sql.append(action.isActionCommentMandatory() + ", ");
                sql.append("null, null, ");
                sql.append(action.getIfActionConditionExpn() + "), ");
            }
            sql.replace(sql.length() - 2, sql.length(), ";");
            try {
                insertDAO.insertRow(sql.toString());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    */
    

//    private void updateStepActorTxn(GlobalVariables globalVariables, Map<String, WorkflowActor> resolvedPersonIdActorIdActorMap,
//			Map<String, Map<String, WorkflowActionDefn>> resolvedPersonIdActorIdActionMap, WorkflowStepTxn workflowStepTxn) {
//		StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_WORKFLOW_TXN_STEP_ACTOR (WORKFLOW_TXN_STEP_ACTOR_PK_ID, WORKFLOW_TXN_STEP_FK_ID, STEP_ACTOR_DEFN_FK_ID, RESOLVED_PERSON_FK_ID, "
//				+ "STEP_ACTOR_STATUS_CODE_FK_ID, IS_DELETED, CREATED_DATETIME, LAST_MODIFIED_DATETIME) VALUES(");
//		WorkflowActorTxn workflowActorTxn ;
//		for(Entry<String, WorkflowActor> resolvedPersonIdActorIdActor : resolvedPersonIdActorIdActorMap.entrySet()){
//			String[] keys = resolvedPersonIdActorIdActor.getKey().split("_");
//            String personFkId = keys[0];
//            workflowActorTxn = new WorkflowActorTxn();
//            workflowActorTxn.setWorkflowTxnStepActorPkId(Common.getUUID());
//            WorkflowActor actor = resolvedPersonIdActorIdActor.getValue();
//            sql.append("x'" + workflowActorTxn.getWorkflowTxnStepActorPkId() + "', ");
//            sql.append("x'" + workflowStepTxn.getWorkflowTxnStepPkId() + "', ");
//            sql.append("x'" + actor.getWorkflowActorDefnPkId() + "', ");
//            sql.append("x'" + personFkId + "', ");
//            sql.append("'NOT_STARTED', false, now(), now()), ");
//            updateActionTxn(globalVariables, resolvedPersonIdActorIdActionMap, workflowStepTxn, workflowActorTxn);
//		}
//		sql.replace(sql.length() - 2, sql.length(), ";");
//		try {
//            insertDAO.insertRow(sql.toString());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//	}
//
//	private void updateActionTxn(GlobalVariables globalVariables,
//			Map<String, Map<String, WorkflowActionDefn>> resolvedPersonIdActionMap, WorkflowStepTxn workflowStepTxn, WorkflowActorTxn workflowActorTxn) {
//    	StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION (WORKFLOW_TXN_ACTOR_POSSIBLE_ACTION_PK_ID, WORKFLOW_TXN_STEP_ACTOR_FK_ID, SCREEN_SECTION_ACTION_MASTER_CODE_FK_ID, " +
//                "TARGET_WFLOW_TXN_STEP_FK_ID, TARGET_STEP_ACTOR_DEFN_FK_ID, IF_ACTION_CONDITION_EXPN, " +
//                "IS_COMMENT_MANDATORY_FLAG, IS_ACTION_VISIBLE_THRU_EXIT_CRIT_EVLN, IS_DELETED, CREATED_DATETIME, LAST_MODIFIED_DATETIME) VALUES (");
//    	boolean defaultStepActionVisibilty;
//    	for (Entry<String, Map<String, WorkflowActionDefn>> actionEntryMap : resolvedPersonIdActionMap.entrySet()) {
////            String[] keys = actionEntryMap.getKey().split("_");
////            String srcActorPersonFkId = keys[0];
//            for(Entry<String, WorkflowActionDefn> actionEntry :((Map<String, WorkflowActionDefn>) actionEntryMap).entrySet()){
//	            WorkflowActionDefn action = actionEntry.getValue();
//	            defaultStepActionVisibilty = isActionVisibleByDefalut(action);//defn should ensure that actor-specific-actions don't have exit_criteria_applicable.
//	            sql.append("x'" + Common.getUUID() + "', ");
//	            sql.append("x'" + workflowActorTxn.getWorkflowTxnStepActorPkId() + "', ");
//	            sql.append("x'" + action.getScreenSectionActionMasterCodeFkId() + "', ");
//	            sql.append("null, ");
//	            sql.append("x'" + action.getTargetActorFkId() + "', ");
//	            sql.append("'" + action.getIfActionConditionExpn() + "', ");
//	            sql.append(action.isActionCommentMandatory() + ", ");
//	            sql.append(defaultStepActionVisibilty + ", ");
//	            sql.append("false, now(), now(), ");
//	            sql.append(action.getIfActionConditionExpn() + "), ");
//            }
//        }
//        sql.replace(sql.length() - 2, sql.length(), ";");
//        try {
//            insertDAO.insertRow(sql.toString());
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//	}
//
//	private boolean isActionVisibleByDefalut(WorkflowActionDefn action) {
//		if(action.isExitCriteriaCheckApplicable()){
//			if(WorkflowUtility.isExitCriteriaFulfilled(action.getExitCountCriteriaExpn())){
//				return true;
////				if(action.isCrtriaCntrlgSrcStepExit())
////					return false;//exit_criteria should be evaluated here? 
////				else if(action.isCrtriaCntrlgTgtStepEntryVsblty())
////					return true;
//			}
//			else return false;
//		}
//		return true;
//	}
//
//	private Map<String,WorkflowActionDefn> removeDuplicateActions(GlobalVariables globalVariables, Map<String, Map<String, WorkflowActionDefn>> resolvedPersonIdActorActionMap) {
//        StringBuilder selectSql = new StringBuilder("SELECT WORKFLOW_TXN_FK_ID, WORKFLOW_ACTOR_ACTION_DEFN_FK_ID, SRC_ACTR_PERSON_FK_ID, SRC_ACTOR_FK_ID FROM T_PFM_IEU_WORKFLOW_TXN_ACTION WHERE ");
//        Map<String, WorkflowActionDefn> compositeKeyToActionMap = new HashMap<>();
//        for(Entry<String, Map<String, WorkflowActionDefn>> entry : resolvedPersonIdActorActionMap.entrySet()){
//            for(Entry<String, WorkflowActionDefn> actionMap : entry.getValue().entrySet()){
//                WorkflowActionDefn currentAction = actionMap.getValue();
//                selectSql.append("(WORKFLOW_TXN_FK_ID = "+globalVariables.workflowDetails.workflowTxnId+" AND ");
//                selectSql.append("WORKFLOW_ACTOR_ACTION_DEFN_FK_ID = "+currentAction.getWorkflowActionDefnPkId()+" AND ");
//                selectSql.append("SRC_ACTR_PERSON_FK_ID = "+entry.getKey()+" AND ");
//                selectSql.append("SRC_ACTOR_FK_ID = "+currentAction.getSourceActorFkId()+") OR ");
//                String compositeKey = globalVariables.workflowDetails.workflowTxnId+"_"+currentAction.getWorkflowActionDefnPkId()
//                                        +"_"+entry.getKey()+"_"+currentAction.getSourceActorFkId();
//                compositeKeyToActionMap.put(compositeKey, currentAction);
//            }
//        }
//        selectSql.replace(selectSql.length()-1, selectSql.length()-3, ";");
//        SearchResult sr = searchDAO.search(selectSql.toString(), null);
//        if(sr.getData() != null) {
//            List<Map<String, Object>> rows = sr.getData();
//            for(Map<String, Object> row : rows){
//                String duplicateKey = row.get("WORKFLOW_TXN_FK_ID").toString()+"_"+
//                                        row.get("WORKFLOW_ACTOR_ACTION_DEFN_FK_ID").toString()+"_"+
//                                        row.get("SRC_ACTR_PERSON_FK_ID")+"_"+
//                                        row.get("SRC_ACTOR_FK_ID");
//                compositeKeyToActionMap.remove(duplicateKey);
//            }
//        }
//        return compositeKeyToActionMap;
//    }
//
//    private Map<String,Map<String, WorkflowActionDefn>> getResolvedActionsFromActorPersons(GlobalVariables globalVariables, Map<String, WorkflowActor> resolvedPersonIdActorMap) {
//        Map<String, Map<String, WorkflowActionDefn>> actorToActorActionMap = new HashMap<>();//actorId->actionPk->action
//        Map<String, Map<String, WorkflowActionDefn>> resolvedPersonToActionMap =  new HashMap<>();//personId->actionPk->action
//        String sql = "SELECT VERSION_ID, WORKFLOW_ACTION_DEFN_PK_ID, WORKFLOW_DEFINITION_FK_ID, ACTOR_ACTION_DISP_SEQ_NUM_POS_INT, " +
//                "SCREEN_SECTION_ACTION_MASTER_CODE_FK_ID, SOURCE_ACTOR_FK_ID, TARGET_ACTOR_FK_ID, TARGET_STEP_FK_ID, IS_EXIT_CRITERIA_CHECK_APPLICABLE, IF_ACTION_CONDITION_EXPN, IS_ACTION_TO_DLFT_EXEC_ON_FORCED_MOVE, " +
//                "IS_ACTION_DELEGATEABLE, IS_AUTO_EXEC_IF_ACTOR_EQUALS_SUBJECT, IS_AUTO_EXECUTE_IF_ACTOR_EQUAL_TO_INITIATOR, IS_AUTO_EXECUTE_IF_DUPLICATE, " +
//                "IS_ACTION_COMMENT_MANDATORY, EXIT_COUNT_CRITERIA_EXPN, IS_CRTRIA_CNTRLG_SRC_STEP_EXIT, IS_CRTRIA_CNTRLG_TGT_STEP_ENTRY_VSBLTY," +
//                "ACTION_EXIT_SRC_ACT_NTFN_ID, ACTION_EXIT_TRGT_ACT_NTFN_ID FROM T_PFM_ADM_WORKFLOW_ACTION_DEFN WHERE WORKFLOW_DEFINITION_FK_ID = "+
//                globalVariables.workflowDetails.workflowDefId+" AND " +
//                "SOURCE_ACTOR_FK_ID = ? ;";
//        for(Entry<String, WorkflowActor> entry : resolvedPersonIdActorMap.entrySet()) {
//            String actorId = entry.getValue().getWorkflowActorDefnPkId();
//            if (actorToActorActionMap.get(actorId) == null) {
//                Deque<Object> parameter = new ArrayDeque<>();
//                parameter.add(actorId);
//                SearchResult sr = searchDAO.search(sql, parameter);
//                if (sr.getData() != null) {
//                    List<Map<String, Object>> rows = sr.getData();
//                    Map<String, WorkflowActionDefn> workflowActorActionMap = new HashMap<>();//actionId->action
//                    for (Map<String, Object> row : rows) {
//                        WorkflowActionDefn workflowActionDefn = new WorkflowActionDefn();
//                        workflowActionDefn.setVersionId(row.get("VERSION_ID").toString());
//                        workflowActionDefn.setWorkflowActionDefnPkId(row.get("WORKFLOW_ACTION_DEFN_PK_ID").toString());
//                        workflowActionDefn.setWorkflowDefinitionFkId(row.get("WORKFLOW_DEFINITION_FK_ID").toString());
//                        workflowActionDefn.setActorActionDispSeqNumPosInt((int) row.get("ACTOR_ACTION_DISP_SEQ_NUM_POS_INT"));
//                        workflowActionDefn.setScreenSectionActionMasterCodeFkId(row.get("SCREEN_SECTION_ACTION_MASTER_CODE_FK_ID").toString());
//                        workflowActionDefn.setSourceActorFkId(row.get("SOURCE_ACTOR_FK_ID").toString());
//                        workflowActionDefn.setTargetActorFkId(row.get("TARGET_ACTOR_FK_ID").toString());
//                        workflowActionDefn.setTargetStepFkId(row.get("TARGET_STEP_FK_ID").toString());
//                        workflowActionDefn.setExitCriteriaCheckApplicable((boolean) row.get("IS_EXIT_CRITERIA_CHECK_APPLICABLE"));
//                        workflowActionDefn.setIfActionConditionExpn(row.get("IF_ACTION_CONDITION_EXPN").toString());
//                        workflowActionDefn.setActionToDlftExecOnForcedMove((boolean) row.get("IS_ACTION_TO_DLFT_EXEC_ON_FORCED_MOVE"));
//                        workflowActionDefn.setActionDelegateable((boolean) row.get("IS_ACTION_DELEGATEABLE"));
//                        workflowActionDefn.setAutoExecIfActorEqualsSubject((boolean) row.get("IS_AUTO_EXEC_IF_ACTOR_EQUALS_SUBJECT"));
//                        workflowActionDefn.setAutoExecuteIfActorEqualToInitiator((boolean) row.get("IS_AUTO_EXECUTE_IF_ACTOR_EQUAL_TO_INITIATOR"));
//                        workflowActionDefn.setAutoExecuteIfDuplicate((boolean) row.get("IS_AUTO_EXECUTE_IF_DUPLICATE"));
//                        workflowActionDefn.setActionCommentMandatory((boolean) row.get("IS_ACTION_COMMENT_MANDATORY"));
//                        workflowActionDefn.setExitCountCriteriaExpn(row.get("EXIT_COUNT_CRITERIA_EXPN").toString());
//                        workflowActionDefn.setCrtriaCntrlgSrcStepExit((boolean) row.get("IS_CRTRIA_CNTRLG_SRC_STEP_EXIT"));
//                        workflowActionDefn.setCrtriaCntrlgTgtStepEntryVsblty((boolean) row.get("IS_CRTRIA_CNTRLG_TGT_STEP_ENTRY_VSBLTY"));
//                        workflowActionDefn.setActionExitSrcActNtfnId(row.get("ACTION_EXIT_SRC_ACT_NTFN_ID").toString());
//                        workflowActionDefn.setActionExitTrgtActNtfnId((row.get("ACTION_EXIT_TRGT_ACT_NTFN_ID").toString()));
//                        workflowActorActionMap.put(workflowActionDefn.getWorkflowActionDefnPkId(), workflowActionDefn);
//                    }
//                    resolvedPersonToActionMap.put(entry.getKey(), workflowActorActionMap);
//                    actorToActorActionMap.put(actorId, workflowActorActionMap);
//                }
//            }
//            else{
//                resolvedPersonToActionMap.put(entry.getKey(), actorToActorActionMap.get(actorId));
//            }
//        }
//        return  resolvedPersonToActionMap;
//    }
//
//    private Map<String, WorkflowActor> getResolvedPersonsFromActors(GlobalVariables globalVariables, Map<String, WorkflowActor> workflowActors) {
//        Map<String, WorkflowActor> contextualPersonIds = new HashMap<>();
//        Map<String, WorkflowActor> resolvedPersonIdActorIdActorMap = new HashMap<>();
//        for(WorkflowActor workflowActor : workflowActors.values()){
//            if(workflowActor.isRelativeToLoggedInUser()){
//                String loogedInPersonId = TenantContextHolder.getUserContext().getPersonId();
//                contextualPersonIds.put(loogedInPersonId, workflowActor);
//                resolvedPersonIdActorIdActorMap = getResolvedPersonsFromHierarchy(globalVariables, contextualPersonIds);
//            }
//            else if (workflowActor.isRelativeToSubjectUser()){
//                //get code from shubham
//            }
//            else if (workflowActor.getActorPersonGroupFkId() != null){
//                String personGroupId = workflowActor.getActorPersonGroupFkId();
//                contextualPersonIds = getpersonIdsFromGroupId(personGroupId, workflowActor);
//                resolvedPersonIdActorIdActorMap = getResolvedPersonsFromHierarchy(globalVariables, contextualPersonIds);
//            }
//            else if (workflowActor.getRelativeToActorId() != null){
//                String relativeToActorId = workflowActor.getRelativeToActorId();
//                Map<String, WorkflowActor> relativeWorkflowActor = new HashMap<>();
//                if(workflowActors.containsKey(relativeToActorId)){
//                    relativeWorkflowActor.put(relativeToActorId, workflowActors.get(relativeToActorId));
//                }
//                else{
//                    relativeWorkflowActor = getWorkflowDefActorFromActorId(relativeToActorId);
//                }
//                Map<String, WorkflowActor> relativeContextualPersonIds = getResolvedPersonsFromActors(globalVariables, relativeWorkflowActor);
//                resolvedPersonIdActorIdActorMap = getResolvedPersonsFromHierarchy(globalVariables, relativeContextualPersonIds);
//                //change the value to absolute actors
//                for(Entry<String, WorkflowActor> entry: resolvedPersonIdActorIdActorMap.entrySet()){
//                    entry.setValue(workflowActor);
//                }
//            }
//            else if (workflowActor.isAdhocUserActor()){
//                //formulate & code for ad-hoc
//            }
//        }
//        return resolvedPersonIdActorIdActorMap;
//    }
//
//    private Map<String,WorkflowActor> getResolvedPersonsFromHierarchy(GlobalVariables globalVariables, Map<String, WorkflowActor> contextualPersonIds) {
//        Map<String, WorkflowActor> resolvedPersonIdActorIdActorMap = new HashMap<>();
//        for(Entry<String, WorkflowActor> contextualPerson : contextualPersonIds.entrySet()){
//            Map<String, Node> resolvedNodes = null;
//            EntityMetadataVOX doMeta = TenantAwareCache.getMetaDataRepository().getMetadataByProcessElement(globalVariables.baseTemplateId,
//                    globalVariables.mtPE, globalVariables.btPE);
//            int level = contextualPerson.getValue().getRelativeRelationshipHierarchyLevelPosInt();
//            WorkflowActor actor = contextualPerson.getValue();
//            if(actor.getRelativeRelationshipCodeFkId().equals("DIRECT_MANAGER") && actor.getResolveActorAsOfDatetime() == null)
//                resolvedNodes = hierarchyService.getNthLevelParent(contextualPerson.getKey(), doMeta, level);
//            else if(actor.getRelativeRelationshipCodeFkId().equals("DIRECT_REPORT") && actor.getResolveActorAsOfDatetime() == null)
//                resolvedNodes = hierarchyService.getAllChildrenUptoNlevel(contextualPerson.getKey(), doMeta, level);
//            else{
//                // get 1st level relationship from another relationship table.
//                // get the remaining nodes/node from the direct approach.
//                // resolveActorAsOfDatetime.
//            }
//            for(Entry<String, Node> entry : resolvedNodes.entrySet()){
//                String personId = entry.getValue().getRow().get("PersonWorkRelationship.FromPerson").toString();//FIXME for indirect relations. different DO!
//                resolvedPersonIdActorIdActorMap.put(personId+"_"+actor.getWorkflowActorDefnPkId(), contextualPerson.getValue());
//            }
//        }
//        return resolvedPersonIdActorIdActorMap;
//    }
//
//    private Map<String,WorkflowActor> getWorkflowDefActorFromActorId(String relativeToActorId) {
//        String sql = "SELECT VERSION_ID, WORKFLOW_ACTOR_DEFN_PK_ID, WORKFLOW_STEP_FK_ID, ACTOR_DISP_SEQ_NUM_POS_INT, ACTOR_NAME_G11N_Big_TXT, " +
//                "ACTOR_ICON_CODE_FK_ID, ACTOR_PERSON_GROUP_FK_ID, IS_RELATIVE_TO_LOGGED_IN_USER, IS_RELATIVE_TO_SUBJECT_USER, RELATIVE_TO_ACTOR_ID, " +
//                "RELATIVE_RELATIONSHIP_CODE_FK_ID, RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT, RESOLVE_ACTOR_AS_OF_DATETIME, IS_ADHOC_USER_ACTOR, " +
//                "ALLOW_EXTERNAL_USER, NO_ACTORS_ACTION_CODE_FK_ID FROM T_PFM_ADM_WORKFLOW_ACTOR WHERE WORKFLOW_ACTOR_PK_ID = "+
//                relativeToActorId+";";
//        SearchResult sr = searchDAO.search(sql, null);//FIXME use util's selectString
//        Map<String, WorkflowActor> actor = new HashMap<>();
//        if(sr.getData() != null) {
//            Map<String, Object> row = sr.getData().get(0);
//            WorkflowActor workflowActor = new WorkflowActor();
//            workflowActor.setWorkflowActorDefnPkId(row.get("WORKFLOW_ACTOR_PK_ID").toString());
//            workflowActor.setWorkflowStepFkId(row.get("WORKFLOW_STEP_DEP_FK_ID").toString());
//            workflowActor.setActorDispSeqNumPosInt((int)row.get("ACTOR_DISP_SEQ_NUM_POS_INT"));
//            workflowActor.setActorNameG11nBigTxt(row.get("ACTOR_NAME_G11N_BLOB").toString());
//            workflowActor.setActorIconCodeFkId(row.get("ACTOR_ICON_CODE_FK_ID").toString());
//            workflowActor.setActorPersonGroupFkId(row.get("ACTOR_PERSON_GROUP_FK_ID").toString());
//            workflowActor.setRelativeToLoggedInUser((boolean)row.get("IS_RELATIVE_TO_LOGGED_IN_USER"));
//            workflowActor.setRelativeToSubjectUser((boolean)row.get("IS_RELATIVE_TO_SUBJECT_USER"));
//            workflowActor.setRelativeToActorId(row.get("RELATIVE_TO_ACTOR_ID").toString());
//            workflowActor.setRelativeRelationshipCodeFkId(row.get("RELATIVE_RELATIONSHIP_CODE_FK_ID").toString());
//            workflowActor.setRelativeRelationshipHierarchyLevelPosInt((int)row.get("RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT"));
//            workflowActor.setResolveActorAsOfDatetime((Timestamp)row.get("RESOLVE_ACTOR_AS_OF_DATETIME"));
//            workflowActor.setAdhocUserActor((boolean)row.get("IS_ADHOC_USER_ACTOR"));
//            workflowActor.setAllowExternalUser((boolean)row.get("ALLOW_EXTERNAL_USER"));
//            workflowActor.setNoActorsActionCodeFkId(row.get("NO_ACTORS_ACTION_CODE_FK_ID").toString());
//            actor.put(workflowActor.getWorkflowActorDefnPkId(), workflowActor);
//        }
//        return actor;
//    }
//
//    private Map<String, WorkflowActor> getpersonIdsFromGroupId(String personGroupId, WorkflowActor workflowActor) {
//        Map<String, WorkflowActor> contextualPersonIds = null;
//        String sql = "SELECT GROUP_MEMBER_PERSON_FK_ID FROM T_PFM_IEU_GROUP_MEMBER WHERE PFM_GROUP_DEP_FK_ID = "+personGroupId+";";
//        SearchResult sr = searchDAO.search(sql, null);
//        if(sr.getData() != null) {
//            contextualPersonIds = new HashMap<>();
//            for (Map<String, Object> row : sr.getData()) {
//                contextualPersonIds.put(row.get("GROUP_MEMBER_PERSON_FK_ID").toString(), workflowActor);
//            }
//        }
//        return contextualPersonIds;
//    }
//
//    private Map<String, WorkflowActor> getWorkflowDefActors(GlobalVariables globalVariables, WorkflowStepDefn workflowStepDefn) {
//        String sql = "SELECT VERSION_ID, WORKFLOW_ACTOR_DEFN_PK_ID, WORKFLOW_STEP_FK_ID, ACTOR_DISP_SEQ_NUM_POS_INT, ACTOR_NAME_G11N_BIG_TXT, " +
//                "ACTOR_ICON_CODE_FK_ID, ACTOR_PERSON_GROUP_FK_ID, IS_RELATIVE_TO_LOGGED_IN_USER, IS_RELATIVE_TO_SUBJECT_USER, RELATIVE_TO_ACTOR_ID, " +
//                "RELATIVE_RELATIONSHIP_CODE_FK_ID, RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT, RESOLVE_ACTOR_AS_OF_DATETIME, IS_ADHOC_USER_ACTOR, " +
//                "ALLOW_EXTERNAL_USER, NO_ACTORS_ACTION_CODE_FK_ID FROM T_PFM_ADM_WORKFLOW_ACTOR_DEFN WHERE WORKFLOW_STEP_FK_ID = "+
//                workflowStepDefn.getWorkflowStepDefnPkId()+";";
//        SearchResult sr = searchDAO.search(sql, null);//FIXME use util's selectString
//        Map<String, WorkflowActor> actors = new HashMap<>();
//        if(sr.getData() != null) {
//            for(Map<String, Object> row : sr.getData()) {
//                WorkflowActor workflowActor = new WorkflowActor();
//                workflowActor.setWorkflowActorDefnPkId(row.get("WORKFLOW_ACTOR_DEFN_PK_ID").toString());
//                workflowActor.setWorkflowStepFkId(row.get("WORKFLOW_STEP_FK_ID").toString());
//                workflowActor.setActorDispSeqNumPosInt((int)row.get("ACTOR_DISP_SEQ_NUM_POS_INT"));
//                workflowActor.setActorNameG11nBigTxt(row.get("ACTOR_NAME_G11N_BIG_TXT").toString());
//                workflowActor.setActorIconCodeFkId(row.get("ACTOR_ICON_CODE_FK_ID").toString());
//                workflowActor.setActorPersonGroupFkId(row.get("ACTOR_PERSON_GROUP_FK_ID").toString());
//                workflowActor.setRelativeToLoggedInUser((boolean)row.get("IS_RELATIVE_TO_LOGGED_IN_USER"));
//                workflowActor.setRelativeToSubjectUser((boolean)row.get("IS_RELATIVE_TO_SUBJECT_USER"));
//                workflowActor.setRelativeToActorId(row.get("RELATIVE_TO_ACTOR_ID").toString());
//                workflowActor.setRelativeRelationshipCodeFkId(row.get("RELATIVE_RELATIONSHIP_CODE_FK_ID").toString());
//                workflowActor.setRelativeRelationshipHierarchyLevelPosInt((int)row.get("RELATIVE_RELATIONSHIP_HIERARCHY_LEVEL_POS_INT"));
//                workflowActor.setResolveActorAsOfDatetime((Timestamp)row.get("RESOLVE_ACTOR_AS_OF_DATETIME"));
//                workflowActor.setAdhocUserActor((boolean)row.get("IS_ADHOC_USER_ACTOR"));
//                workflowActor.setAllowExternalUser((boolean)row.get("ALLOW_EXTERNAL_USER"));
//                workflowActor.setNoActorsActionCodeFkId(row.get("NO_ACTORS_ACTION_CODE_FK_ID").toString());
//                actors.put(workflowActor.getWorkflowActorDefnPkId(), workflowActor);
//            }
//        }
//        return actors;
//    }
//
    private Map<Integer, WorkflowStepDefn> getWorkflowDefSteps(String workflowDefFkId) {
        String sql = "SELECT  VERSION_ID , WORKFLOW_STEP_DEFN_PK_ID, WORKFLOW_DEFINITION_FK_ID, STEP_SEQ_NUM_POS_INT, WORKFLOW_STEP_NAME_G11N_BIG_TXT, WORKFLOW_STEP_ICON_CODE_FK_ID, \n" +
                "START_DATE_EXPN, DUE_DATE_EXPN, IS_START_DATE_ENFORCED, IS_DUE_DATE_ENFORCED, STEP_ACTOR_RESOLUTION_CODE_FK_ID, ALLOW_TRGT_ACTR_REC_VSBLTY_WHN_ENTRY_CRIT_NOT_FULFILLED, " +
                "IS_BEGIN_STEP, IS_END_STEP, ON_STEP_COMPLETION_EXPN, ON_STEP_CANCELLATION_EXPN FROM T_PFM_ADM_WORKFLOW_STEP_DEFN WHERE WORKFLOW_DEFINITION_FK_ID = " + workflowDefFkId + ";";
        SearchResult sr = searchDAO.search(sql, null);//FIXME use util's selectString
        Map<Integer,WorkflowStepDefn> steps = new HashMap<>();
        if(sr.getData() != null){
            for(Map<String, Object> row : sr.getData()){
                WorkflowStepDefn step = new WorkflowStepDefn();
                step.setVersionId(row.get("VERSION_ID").toString());
                step.setWorkflowStepDefnPkId(Util.convertByteToString((byte[]) row.get("WORKFLOW_STEP_DEFN_PK_ID")));
                step.setWorkflowDefinitionFkId(Util.convertByteToString((byte[]) row.get("WORKFLOW_DEFINITION_FK_ID")));
                step.setStepSeqNumPosInt((int)row.get("STEP_SEQ_NUM_POS_INT"));
                step.setWorkflowStepNameG11nBigText((String) row.get("WORKFLOW_STEP_NAME_G11N_BIG_TXT"));
                step.setWorkflowStepIconCodeFkId((String) row.get("WORKFLOW_STEP_ICON_CODE_FK_ID"));
                step.setStartDateExpn(row.get("START_DATE_EXPN").toString());
                step.setDueDateExpn(row.get("DUE_DATE_EXPN").toString());
                step.setStartDateEnforced((boolean)row.get("IS_START_DATE_ENFORCED"));
                step.setDueDateEnforced((boolean)row.get("IS_DUE_DATE_ENFORCED"));
                step.setStepActorResolutionCodeFkId(row.get("STEP_ACTOR_RESOLUTION_CODE_FK_ID").toString());
                step.setAllowTrgtActrRecVsbltyWhnEntryCritNotFulfilled((boolean)row.get("ALLOW_TRGT_ACTR_REC_VSBLTY_WHN_ENTRY_CRIT_NOT_FULFILLED"));
                step.setBeginStep((boolean)row.get("IS_BEGIN_STEP"));
                step.setEndStep((boolean)row.get("IS_END_STEP"));
                step.setOnStepCompletionExpn((String) row.get("ON_STEP_COMPLETION_EXPN"));
                step.setOnStepCancellationExpn((String) row.get("ON_STEP_CANCELLATION_EXPN"));
                steps.put(step.getStepSeqNumPosInt(), step);
            }
        }
        return steps;
    }

    private WorkflowSelection checkIfWorflowCreationApplicable(GlobalVariables globalVariables) {
        String refWorkflowTxnStatusCodeFkId = (String) globalVariables.model.getInsertValueMap().get("REF_WORKFLOW_TXN_STATUS_CODE_FK_ID");
        WorkflowSelection workflowSelection = null;
        if(refWorkflowTxnStatusCodeFkId == null || refWorkflowTxnStatusCodeFkId.isEmpty() || (refWorkflowTxnStatusCodeFkId != null && (!globalVariables.model.getInsertValueMap().containsKey("REF_WORKFLOW_TXN_FK_ID") ||
                globalVariables.model.getInsertValueMap().get("REF_WORKFLOW_TXN_FK_ID").equals("") || (globalVariables.model.getInsertValueMap().get("REF_WORKFLOW_TXN_FK_ID") != null))
                && (refWorkflowTxnStatusCodeFkId.equalsIgnoreCase("COMPLETED") ||refWorkflowTxnStatusCodeFkId.equalsIgnoreCase("CANCELLED") ||
                refWorkflowTxnStatusCodeFkId.equalsIgnoreCase("WITHDRAWN")))){
            workflowSelection = getWorkflowSelection(globalVariables);
        }
        return workflowSelection;
    }

    private WorkflowSelection getWorkflowSelection(GlobalVariables globalVariables) {
        String targetWorkflowDefFkId = null;
        Deque<Object> parameter = new ArrayDeque<>();
        Map<String, Object> insertValueMap = globalVariables.model.getInsertValueMap();
        parameter.add(globalVariables.businessRule);
//        parameter.add(globalVariables.mtPE);//Fixme : Get these from underlying record
        parameter.add(globalVariables.baseTemplateId);

        String sql = "SELECT * FROM T_PFM_ADM_WORKFLOW_SELECTION S INNER JOIN T_PFM_ADM_BUSINESS_RULE" +
                " ON BUSS_RULE_CODE_PK_ID = BUSS_RULE_CODE_FK_ID WHERE BUSS_RULE_CODE_FK_ID = ? " +
                "AND BT_CODE_FK_ID = ? AND S.SAVE_STATE_CODE_FK_ID = 'SAVED' AND S.RECORD_STATE_CODE_FK_ID = 'CURRENT' AND S.ACTIVE_STATE_CODE_FK_ID = 'ACTIVE';";
        SearchResult result = searchDAO.search(sql, parameter);
        List<Map<String, Object>> resultList = result.getData();
        Evaluator evaluator = new Evaluator();
        populateDynamicValuesInEvaluator(evaluator, insertValueMap);
        int count = 0;
        for(Map<String, Object> record : resultList){
            String expression = (String) record.get("CONDITION_EXPN");
            if (expression == null)
                return getWorkflowSelectionFromRecord(record);
            else {
                try {
                    boolean matches = evaluator.getBooleanResult(expression);
                    if (matches) {
//                        count++;
//                        if (count > 1) {
//                            return null;
//                        }
                        return getWorkflowSelectionFromRecord(record);
                    }
                } catch (EvaluationException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private WorkflowSelection getWorkflowSelectionFromRecord(Map<String, Object> record) {
        WorkflowSelection workflowSelection = new WorkflowSelection();
        workflowSelection.setWorkflowSelectionId(Util.convertByteToString((byte[]) record.get("WORKFLOW_SELECTION_PK_ID")));
        workflowSelection.setProcessTypeCode((String) record.get("PROCESS_TYPE_CODE_FK_ID"));
        workflowSelection.setMtPE((String) record.get("PROCESS_TYPE_CODE_FK_ID"));//fixme set MTPE
        workflowSelection.setConditionExpression((String) record.get("CONDITION_EXPN"));
        workflowSelection.setBaseTemplate((String) record.get("BT_CODE_FK_ID"));
        workflowSelection.setBusinessRuleCode((String) record.get("BUSS_RULE_CODE_FK_ID"));
        workflowSelection.setWorkflowDefinitionId(Util.convertByteToString((byte[]) record.get("WORKFLOW_DEFINTION_FK_ID")));
        return workflowSelection;
    }

    private String getCrudeTypeCode(GlobalVariables globalVariables) {
        Deque<Object> parameter = new ArrayDeque<>();
        parameter.add(globalVariables.actionCode);
        String sql = "SELECT IS_CREATE_ACCESS_CONTROL_EQUIVALENT, IS_READ_ACCESS_CONTROL_EQUIVALENT, IS_UPDATE_ACCESS_CONTROL_EQUIVALENT, IS_DELETE_ACCESS_CONTROL_EQUIVALENT " +
                "FROM T_UI_LKP_ACTION WHERE ACTION_CODE_PK_ID = ? "
                + "AND	SAVE_STATE_CODE_FK_ID = 'SAVED' AND RECORD_STATE_CODE_FK_ID = 'CURRENT' AND ACTIVE_STATE_CODE_FK_ID = 'ACTIVE';";
        SearchResult result = searchDAO.search(sql, parameter);
        if (result.getData() != null) {
            Map<String, Object> row = result.getData().get(0);
            if((boolean)row.get("IS_CREATE_ACCESS_CONTROL_EQUIVALENT")) return "CREATE";
            if((boolean)row.get("IS_READ_ACCESS_CONTROL_EQUIVALENT")) return "READ";
            if((boolean)row.get("IS_UPDATE_ACCESS_CONTROL_EQUIVALENT")) return "UPDATE";
            if((boolean)row.get("IS_DELETE_ACCESS_CONTROL_EQUIVALENT")) return "DELETE";
        }
        return null;
    }

    private void populateDynamicValuesInEvaluator(Evaluator evaluator, Map<String, Object> insertValueMap) {
        for(Entry<String, Object> entry : insertValueMap.entrySet()){
            evaluator.putVariable(entry.getKey(), entry.getValue().toString());
        }
    }


    @Override
    public void generateWorkflowNotifaction(ServerActionStep currentActionStep, GlobalVariables globalVariables) {

    }

    public SearchDAO getSearchDAO() {
        return searchDAO;
    }

    public void setSearchDAO(SearchDAO searchDAO) {
        this.searchDAO = searchDAO;
    }

    public InsertDAO getInsertDAO() {
        return insertDAO;
    }

    public void setInsertDAO(InsertDAO insertDAO) {
        this.insertDAO = insertDAO;
    }

    public WorkflowUtility getWorkflowUtility() {
        return workflowUtility;
    }

    public void setWorkflowUtility(WorkflowUtility workflowUtility) {
        this.workflowUtility = workflowUtility;
    }
}
