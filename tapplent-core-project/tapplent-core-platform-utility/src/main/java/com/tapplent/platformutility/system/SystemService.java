package com.tapplent.platformutility.system;

import com.amazonaws.services.s3.AmazonS3;
import com.jcraft.jsch.ChannelSftp;
import com.tapplent.platformutility.g11n.structure.OEMObject;

import java.util.Map;

public interface SystemService {
	public ChannelSftp getSftpServerConnection();
	public AmazonS3 getAmazonS3Client();

	Map<String,OEMObject> getOemObjectMap();

    Map<String,Map<String,String>> getOemToLocaleLangMap();
}
