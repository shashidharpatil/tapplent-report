package com.tapplent.platformutility.hierarchy.service;

import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.hierarchy.dao.HierarchyDAO;
import com.tapplent.platformutility.hierarchy.structure.HierarchyInfo;
import com.tapplent.platformutility.hierarchy.structure.Node;
import com.tapplent.platformutility.hierarchy.structure.Relation;
import com.tapplent.platformutility.metadata.structure.EntityMetadataVOX;
import com.tapplent.platformutility.metadata.structure.MasterEntityMetadataVOX;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Tapplent on 8/12/16.
 */
public class HierarchyServiceImpl implements HierarchyService {

    private HierarchyDAO hierarchyDAO;

    @Override
    public Map<String, Node> getNthLevelParent(String primaryKey, EntityMetadataVOX doMeta, int level) {
        return hierarchyDAO.getNthLevelParent(primaryKey, doMeta, level);
    }

    @Override
    public Map<String, Node> getAllChildrenUptoNlevel(String primaryKey, EntityMetadataVOX doMeta, int level) {
        return null;
    }

    @Override
    public List<HierarchyInfo> getAllChildren(MasterEntityMetadataVOX rootMtPEMeta, String pk) {
        return hierarchyDAO.getAllChildren(rootMtPEMeta, pk);
    }

    @Override
    public Map<String, Map<String, List<Relation>>> getAllRelationBetweenBaseAndRelatedPerson(String basePersonId, String relatedPerson) {
//        List<Relation> finalRelation = new ArrayList<>();
        // Now check if the base person is the manager of relative person or not.
        // To do this we need to retrieve all the manager of relative person and count the level.
        Map<String, Map<String, List<Relation>>> relationMap = new ConcurrentHashMap<>();
//        Map<String, Node> relationMap = new HashMap<>();
        //only for direct manager.
        setRelationForSelfAndAllButSelf(relationMap, basePersonId, relatedPerson);
        String hierarchyId = hierarchyDAO.getHierarchyIdForPerson(basePersonId);
        Relation reportRelation = new Relation();
        if (hierarchyId != null) {
            Map<String, Relation> directManagersOfBasePerson = getDirectManagers(basePersonId, hierarchyId);
            if (directManagersOfBasePerson != null && directManagersOfBasePerson.containsKey(relatedPerson)) {
                relationMap.get(basePersonId).get(relatedPerson).add(directManagersOfBasePerson.get(relatedPerson));
                reportRelation.setSubjectPersonId(relatedPerson);
                if (directManagersOfBasePerson.get(relatedPerson).getLevel() > 1)
                    reportRelation.setRelationshipType("DIRECT_REPORT+");
                else reportRelation.setRelationshipType("DIRECT_REPORT");
                reportRelation.setLevel(directManagersOfBasePerson.get(relatedPerson).getLevel());
                relationMap.get(relatedPerson).get(basePersonId).add(reportRelation);
            } else {
                Map<String, Relation> directManagersOfRelatedPerson = getDirectManagers(relatedPerson, hierarchyId);
                if (directManagersOfRelatedPerson != null && directManagersOfRelatedPerson.containsKey(basePersonId)) {
                    relationMap.get(relatedPerson).get(basePersonId).add(directManagersOfRelatedPerson.get(basePersonId));
                    reportRelation.setSubjectPersonId(basePersonId);
                    if (directManagersOfRelatedPerson.get(basePersonId).getLevel() > 1)
                        reportRelation.setRelationshipType("DIRECT_REPORT+");
                    else reportRelation.setRelationshipType("DIRECT_REPORT");
                    reportRelation.setLevel(directManagersOfRelatedPerson.get(basePersonId).getLevel());
                    relationMap.get(basePersonId).get(relatedPerson).add(reportRelation);
                }
            }
        }
        //get all other type of relations
//        Map<String, Map<String, List<Relation>>> otherRelationMap = getAllOtherRelationBetweenBaseAndRelatedPerson(basePersonId, relatedPerson, hierarchyId);

//        Map<String, Map<String, List<Relation>>> reverseOtherRelationMap = getAllOtherRelationBetweenBaseAndRelatedPerson(relatedPerson, basePersonId, hierarchyId);
//        otherRelationMap.putAll(reverseOtherRelationMap);

        //get all first level relations
        Map<String, Map<String, List<Relation>>> otherFirstLevelRelationMap = getAllOtherRelationBetweenBasePersonAndRelatedPerson(basePersonId, relatedPerson, hierarchyId);
        if (otherFirstLevelRelationMap != null && otherFirstLevelRelationMap.containsKey(basePersonId)) {
            relationMap.get(basePersonId).get(relatedPerson).addAll(otherFirstLevelRelationMap.get(basePersonId).get(relatedPerson));
        }
        if (otherFirstLevelRelationMap != null && otherFirstLevelRelationMap.containsKey(relatedPerson)) {
            relationMap.get(relatedPerson).get(basePersonId).addAll(otherFirstLevelRelationMap.get(relatedPerson).get(basePersonId));
        }

        TenantAwareCache.getPersonRepository().getPersonRelationMap().putAll(relationMap);
        return relationMap;
    }

    private Map<String, Map<String, List<Relation>>> getAllOtherRelationBetweenBasePersonAndRelatedPerson(String basePersonId, String relatedPerson, String hierarchyId) {
        Map<String, Map<String, List<Relation>>> relationMap = new HashMap<>();
        Map<String, List<Relation>> firstLevelManagersForBasePerson = hierarchyDAO.getOtherFirstLevelManager(basePersonId);
        getAllOtherNextLevelRelationBetweenBaseAndRelatedPerson(basePersonId, relatedPerson, firstLevelManagersForBasePerson, relationMap, hierarchyId);

        Map<String, List<Relation>> firstLevelManagersForRelatedPerson = hierarchyDAO.getOtherFirstLevelManager(relatedPerson);
        getAllOtherNextLevelRelationBetweenBaseAndRelatedPerson(relatedPerson, basePersonId, firstLevelManagersForRelatedPerson, relationMap, hierarchyId);
//        if (firstLevelManagersForRelatedPerson != null && firstLevelManagersForRelatedPerson.containsKey(basePersonId)){
//            Map<String, List<Relation>> relationEntryMap = new HashMap<>();
//            relationEntryMap.put(relatedPerson, firstLevelManagersForRelatedPerson.get(basePersonId));
//            relationMap.put(basePersonId, relationEntryMap);
//        }
        return relationMap;
    }

    private void getAllOtherNextLevelRelationBetweenBaseAndRelatedPerson(String basePersonId, String relatedPerson, Map<String, List<Relation>> firstLevelManagersForBasePerson, Map<String, Map<String, List<Relation>>> relationMap, String hierarchyId) {
        if (firstLevelManagersForBasePerson != null) {
            if (firstLevelManagersForBasePerson.containsKey(relatedPerson)) {
                Map<String, List<Relation>> relationEntryMap = new HashMap<>();
                List<Relation> firstLevelManagersAsRelatedPerson = new ArrayList<>();
                firstLevelManagersAsRelatedPerson.addAll(firstLevelManagersForBasePerson.get(relatedPerson));
                relationEntryMap.put(relatedPerson, firstLevelManagersAsRelatedPerson);
                relationMap.put(basePersonId, relationEntryMap);

                //add direct other managers's reverse relation
                List<Relation> relatedPersonAsFirstLevelManager = firstLevelManagersForBasePerson.get(relatedPerson);
                for (Relation firstLevelOtherRelation : relatedPersonAsFirstLevelManager) {
                    String reverseRelationType = getInverseRelationshipType(firstLevelOtherRelation.getRelationshipType());
                    if (reverseRelationType != null) {
                        Relation reportRelation = new Relation();
                        reportRelation.setSubjectPersonId(relatedPerson);
                        reportRelation.setRelationshipType(reverseRelationType);
                        reportRelation.setLevel(1);
                        if (relationMap.containsKey(relatedPerson))
                            relationMap.get(relatedPerson).get(basePersonId).add(reportRelation);
                        else {
                            Map<String, List<Relation>> reverseRelationEntryMap = new HashMap<>();
                            List<Relation> firstLevelReportAsBasePerson = new ArrayList<>();
                            firstLevelReportAsBasePerson.add(reportRelation);
                            reverseRelationEntryMap.put(basePersonId, firstLevelReportAsBasePerson);
                            relationMap.put(relatedPerson, reverseRelationEntryMap);
                        }
                    }
                }
            }
            else {
                for (Map.Entry<String, List<Relation>> firstLevelOtherManager : firstLevelManagersForBasePerson.entrySet()) {
                    String frstLvlOthrMngrPrsnIdOfBasePrsn = firstLevelOtherManager.getKey();
                    String hirarchyIdForfrstLvlOthrMngrPrsn = hierarchyDAO.getHierarchyIdForPerson(frstLvlOthrMngrPrsnIdOfBasePrsn);
                    if (hirarchyIdForfrstLvlOthrMngrPrsn != null) {
                        Map<String, Map<String, List<Relation>>> relationBwFrstLvlOthrMngrOfBasePrsnAndRltdPrsn = TenantAwareCache.getPersonRepository().getPersonRelationMap(frstLvlOthrMngrPrsnIdOfBasePrsn, relatedPerson);
                        if (relationBwFrstLvlOthrMngrOfBasePrsnAndRltdPrsn.get(relatedPerson).get(frstLvlOthrMngrPrsnIdOfBasePrsn) != null) {
                            for (Relation directRelation : relationBwFrstLvlOthrMngrOfBasePrsnAndRltdPrsn.get(frstLvlOthrMngrPrsnIdOfBasePrsn).get(relatedPerson)) {
                                if (directRelation.getRelationshipType().matches("DIRECT_MANAGER|DIRECT_MANAGER\\+")) {
                                    for (Relation intermediateRelation : firstLevelOtherManager.getValue()) {
                                        Relation exactRelation = new Relation();
                                        exactRelation.setRelationshipType(intermediateRelation.getRelationshipType() + "+");
                                        exactRelation.setLevel(directRelation.getLevel() + 1);
                                        exactRelation.setSubjectPersonId(basePersonId);
                                        if (relationMap.containsKey(basePersonId))
                                            relationMap.get(basePersonId).get(relatedPerson).add(exactRelation);
                                        else {
                                            Map<String, List<Relation>> nextLevelOtherManager = new HashMap<>();
                                            List<Relation> nextLevelOtherManagerList = new ArrayList<>();
                                            nextLevelOtherManagerList.add(exactRelation);
                                            nextLevelOtherManager.put(relatedPerson, nextLevelOtherManagerList);
                                            relationMap.put(basePersonId, nextLevelOtherManager);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
//        else {
//            for (Map.Entry<String, List<Relation>> firstLevelOtherManager : firstLevelManagersForBasePerson.entrySet()){
//                String firstLevelOtherManagerPersonIdOfBasePerson = firstLevelOtherManager.getKey();
//                Map<String, Relation> directManagersOfFirstLevelManager = getDirectManagers(firstLevelOtherManagerPersonIdOfBasePerson, hierarchyId);
//                if (directManagersOfFirstLevelManager != null && directManagersOfFirstLevelManager.containsKey(relatedPerson)){
//                    Relation directRelation = directManagersOfFirstLevelManager.get(relatedPerson);
//                    for (Relation intermediateRelation : firstLevelOtherManager.getValue()) {
//                        Relation exactRelation = new Relation();
//                        exactRelation.setRelationshipType(intermediateRelation+"+");
//                        exactRelation.setLevel(directRelation.getLevel()+1);
//                        exactRelation.setSubjectPersonId(basePersonId);
//                        relationMap.get(relatedPerson).get(basePersonId).add(directManagersOfFirstLevelManager.get(relatedPerson));
//                    }
////                    reportRelation.setSubjectPersonId(basePersonId);
////                    reportRelation.setRelationshipType("DIRECT_REPORT");
////                    reportRelation.setLevel(directManagersOfFirstLevelManager.get(basePersonId).getLevel());
////                    relationMap.get(relatedPerson).get(basePersonId).add(reportRelation);
//                }
//            }
//        }
        }
    }

    private String getInverseRelationshipType(String relationshipType) {
        return hierarchyDAO.getInverseRelationshipType(relationshipType);
    }

    private void setRelationForSelfAndAllButSelf(Map<String, Map<String, List<Relation>>> relationMap, String basePersonId, String relatedPerson) {
        Relation relation = new Relation();
        relation.setLevel(0);
        Map<String, List<Relation>> personRelationMap = new HashMap<>();
        List<Relation> relationList = new ArrayList<>();
        if (basePersonId.equalsIgnoreCase(relatedPerson)) {
            relation.setRelationshipType("SELF");
            relation.setSubjectPersonId(basePersonId);
            relationList.add(relation);
            personRelationMap.put(basePersonId, relationList);
        }
        else {
            relation.setRelationshipType("ALL_BUT_SELF");
            relation.setSubjectPersonId(relatedPerson);
            relationList.add(relation);
            personRelationMap.put(basePersonId, relationList);
            Relation relatedRelation = new Relation();
            relatedRelation.setRelationshipType("ALL_BUT_SELF");
            relatedRelation.setSubjectPersonId(basePersonId);
            List<Relation> relatedRelationList = new ArrayList<>();
            relatedRelationList.add(relatedRelation);
            Map<String, List<Relation>> relatedPersonRelationMap = new HashMap<>();
            relatedPersonRelationMap.put(relatedPerson, relatedRelationList);
            relationMap.put(basePersonId, relatedPersonRelationMap);
        }
        relationMap.put(relatedPerson, personRelationMap);
    }

    private Map<String,Map<String,List<Relation>>> getAllOtherRelationBetweenBaseAndRelatedPerson(String basePersonId, String relatedPersonId, String hierarchyId) {
        Map<String, Map<String, List<Relation>>> relationMap = new HashMap<>();
        Map<String, List<Relation>> firstLevelManagers = hierarchyDAO.getOtherFirstLevelManager(basePersonId);
        if (firstLevelManagers != null && firstLevelManagers.containsKey(relatedPersonId)){
            Map<String, List<Relation>> relationEntryMap = new HashMap<>();
            relationEntryMap.put(relatedPersonId, firstLevelManagers.get(relatedPersonId));
            relationMap.put(basePersonId, relationEntryMap);
            firstLevelManagers.remove(relatedPersonId);
        }
        List<String> keysToRemove = new ArrayList<>();
        for (Map.Entry<String, List<Relation>> entry : firstLevelManagers.entrySet()){
            Map<String, Relation> directManagers = getDirectManagers(entry.getKey(), hierarchyId);
            if (directManagers != null && directManagers.containsKey(relatedPersonId)){
                List<Relation> originalRelationList = new ArrayList<>();
                for (Relation originalRelation : entry.getValue()) {
                    Relation relation = directManagers.get(relatedPersonId);
                    int level = relation.getLevel();
                    relation.setLevel(level + 1);
                    relation.setRelationshipType(originalRelation.getRelationshipType());
                }
                if (relationMap.containsKey(basePersonId)){
                    relationMap.get(basePersonId).get(relatedPersonId).addAll(originalRelationList);
                }
                else {
                    Map<String, List<Relation>> relationEntryMap = new HashMap<>();
//                    List<Relation> relationList = new ArrayList<>();
//                    relationList.add(relation);
                    relationEntryMap.put(relatedPersonId, originalRelationList);
                    relationMap.put(basePersonId, relationEntryMap);
                }
                keysToRemove.add(entry.getKey());
            }
        }
//        for (String key : keysToRemove) {
//            firstLevelManagers.remove(key);
//        }
        //get all other managers for relatedPersonId then find if basePerson exists there.
        return relationMap;
    }

    @Override
    public Map<String, Node> getAllParents(String primaryKey, EntityMetadataVOX doMeta) {
        Map<String, Node> allParents = hierarchyDAO.getAllParents(primaryKey, doMeta);
        return null;
    }

    public static void main(String args[]){
//        Map<String, Map<String, List<Relation>>> a = getAllRelationBetweenTwoPerson("0x47CFB31D072CF46C8B8731183FC6345E", "0x4EEE75B183B4E29B931E433CA6FF9EA3" );
    }

    @Override
    public Map<String, Map<String, List<Relation>>> getAllRelationBetweenTwoPerson(String personOneId, String personTwoId) {
        Map<String, Map<String, Relation>> personRelationMap = new HashMap<>();
        Map<String, Map<String, List<Relation>>> personDirectRelationMap = getAllRelationBetweenBaseAndRelatedPerson(personOneId, personTwoId);
        //add all other types of relationships.
        return personDirectRelationMap;
    }

    @Override
    public void updateLeftRightForNodeInsert(String hierarchyId, String parentNodeId, EntityMetadataVOX entityMetadataVOX, String primaryNodeId) throws SQLException {
        hierarchyDAO.updateleftRightForNodeInsert(hierarchyId, parentNodeId, entityMetadataVOX, primaryNodeId);
        return;
    }

    @Override
    public void updateLeftRightForNodeDelete(String hierarchyId, EntityMetadataVOX entityMetadataVOX, int nodeLeft, int nodeRight, String primaryKey) throws SQLException {
        hierarchyDAO.updateLeftRightForNodeDelete(hierarchyId, entityMetadataVOX, nodeLeft, nodeRight, primaryKey);
        return;
    }

    @Override
    public void updateLeftRightForNodeParentUpdate(String hierarchyId, EntityMetadataVOX entityMetadataVOX, Node rootNode, Node parentNode) throws SQLException {
        hierarchyDAO.updateLeftRightForNodeParentUpdate(hierarchyId, entityMetadataVOX, rootNode, parentNode);
        return;
    }

    @Override
    public String getPersonIdForRootPerson(String hierarchyId) {
        return hierarchyDAO.getPeronIdForRootPerson(hierarchyId);
    }

    @Override
    public List<String> getHierarchialPersonRoots() {
        return hierarchyDAO.getHierarchialPersonRoots();
    }

    @Override
    public int updateLeftRightAndHierachyValuesForPerson(String hierarchyId, String parentId, int leftValue) throws SQLException {
        int rightValue = leftValue + 1;

        List<String> childrenPrimaryKeys = hierarchyDAO.getDirectChildrenListForParentPerson(parentId);
        if (childrenPrimaryKeys != null) {
            for (String childrenPrimaryKey : childrenPrimaryKeys) {
                rightValue = updateLeftRightAndHierachyValuesForPerson(hierarchyId, childrenPrimaryKey, rightValue);
            }
        }
        hierarchyDAO.updateLeftRightAndHierarchyValuesForPerson(parentId, leftValue, rightValue, hierarchyId);
        return rightValue + 1;
    }

    @Override
    public Map<String, Map<Integer, List<String>>> getAllRelationsForPerson(String personId) {
        Map<String, Map<Integer, List<String>>> allRelationMap = new HashMap<>();
        populateDirectManagersInAllRelationMap(allRelationMap, personId);
        populateFirstLevelDirectReportsInAllRelationMap(allRelationMap, personId);
        populateFirstLevelOtherManagersInAllRelationMap(allRelationMap, personId);
        populateFirstLevelOtherReportsInAllRelationMap(allRelationMap, personId);
//        populateDirectReportsInAllRelationMap(allRelationMap, personId);//no required as if now.
//        populateOtherRelationInAllRelationMap(allRelationMap, personId);
        return allRelationMap;
    }

    private void populateFirstLevelOtherReportsInAllRelationMap(Map<String, Map<Integer, List<String>>> allRelationMap, String personId) {
        Map<String, List<Relation>> otherFirstLevelReports = hierarchyDAO.getOtherFirstLevelReport(personId);
        List<String> relatedPersonList = null;
        if (otherFirstLevelReports != null){
            for (Map.Entry<String, List<Relation>> relationMapEntry : otherFirstLevelReports.entrySet()){
                String relatedPerson = relationMapEntry.getKey();
                for (Relation otherRelation : relationMapEntry.getValue()) {
                    String relationType = otherRelation.getRelationshipType();
                    Integer relationLevel = otherRelation.getLevel();
                    if (allRelationMap.containsKey(relationType)) {
                        if(allRelationMap.get(relationType).containsKey(relationLevel)){
                            allRelationMap.get(relationType).get(relationLevel).add(relatedPerson);
                        } else {
                            relatedPersonList = new ArrayList<>();
                            relatedPersonList.add(relatedPerson);
                            allRelationMap.get(relationType).put(relationLevel, relatedPersonList);
                        }
                    } else {
                        relatedPersonList = new ArrayList<>();
                        relatedPersonList.add(relatedPerson);
                        Map<Integer, List<String>> levelToRelatedPersonMap = new HashMap<>();
                        levelToRelatedPersonMap.put(relationLevel, relatedPersonList);
                        allRelationMap.put(relationType, levelToRelatedPersonMap);
                    }
                }
            }
        }
    }

    private void populateFirstLevelOtherManagersInAllRelationMap(Map<String, Map<Integer, List<String>>> allRelationMap, String personId) {
        Map<String, List<Relation>> otherFirstLevelManagers = hierarchyDAO.getOtherFirstLevelManager(personId);
        List<String> relatedPersonList = null;
        if (otherFirstLevelManagers != null){
            for (Map.Entry<String, List<Relation>> relationMapEntry : otherFirstLevelManagers.entrySet()){
                String relatedPerson = relationMapEntry.getKey();
                for (Relation otherRelation : relationMapEntry.getValue()) {
                    String relationType = otherRelation.getRelationshipType();
                    Integer relationLevel = otherRelation.getLevel();
                    if (allRelationMap.containsKey(relationType)) {
                        if(allRelationMap.get(relationType).containsKey(relationLevel)){// for multiple Matrix Managers in single level
                            allRelationMap.get(relationType).get(relationLevel).add(relatedPerson);
                        } else {
                            relatedPersonList = new ArrayList<>();
                            relatedPersonList.add(relatedPerson);
                            allRelationMap.get(relationType).put(relationLevel, relatedPersonList);
                        }
                    } else {
                        relatedPersonList = new ArrayList<>();
                        relatedPersonList.add(relatedPerson);
                        Map<Integer, List<String>> levelToRelatedPersonMap = new HashMap<>();
                        levelToRelatedPersonMap.put(relationLevel, relatedPersonList);
                        allRelationMap.put(relationType, levelToRelatedPersonMap);
                    }
                }
            }
        }
    }

    private void populateFirstLevelDirectReportsInAllRelationMap(Map<String, Map<Integer, List<String>>> allRelationMap, String personId) {
        List<String> allFirstLevelChildrenList = hierarchyDAO.getDirectChildrenListForParentPerson(personId);
        if (allFirstLevelChildrenList != null && !allFirstLevelChildrenList.isEmpty()) {
            Map<Integer, List<String>> firstLevelReportMap = new HashMap<>();
            firstLevelReportMap.put(1, allFirstLevelChildrenList);
            allRelationMap.put("DIRECT_REPORT", firstLevelReportMap);
        }
    }

    private void populateOtherRelationInAllRelationMap(Map<String, Map<Integer, List<String>>> allRelationMap, String personId) {
        Map<String, List<Relation>> otherFirstLevelManager = hierarchyDAO.getOtherFirstLevelManager(personId);
        if (otherFirstLevelManager != null && !otherFirstLevelManager.isEmpty()){
            for (Map.Entry otherRelationEntry : otherFirstLevelManager.entrySet()){

            }
        }
    }

    private void populateDirectReportsInAllRelationMap(Map<String, Map<Integer, List<String>>> allRelationMap, String personId) {
        List<String> allFirstLevelChildrenList = hierarchyDAO.getDirectChildrenListForParentPerson(personId);
        Integer level = 1;
        for (String firstLevelChild : allFirstLevelChildrenList){
            if (allRelationMap.containsKey("DIRECT_REPORT")){
//                if ()
            }
        }
    }

    private void populateDirectManagersInAllRelationMap(Map<String, Map<Integer, List<String>>> allRelationMap, String personId) {
        Map<String, Relation> directManagerMap = hierarchyDAO.getDirectManagers(personId);
        Map<Integer, List<String>> levelToRelatedPersonIdListMap = new HashMap<>();
        if (directManagerMap != null) {
            for (Map.Entry<String, Relation> entry : directManagerMap.entrySet()) {
                List<String> relatedPersonIdList = new ArrayList<>();
                String relatedPersonId = entry.getKey();
                relatedPersonIdList.add(relatedPersonId);
                levelToRelatedPersonIdListMap.put(entry.getValue().getLevel(), relatedPersonIdList);
            }
            allRelationMap.put("DIRECT_MANAGER", levelToRelatedPersonIdListMap);
        }
    }

    private Map<String, Relation> getDirectManagers(String personId, String hierarchyId) {
        Map<String, Relation> directManagerMap = hierarchyDAO.getDirectManagers(personId, hierarchyId);
        // Now we need to fetch all the parents of this related person.
        // In person table go up the hierarchy and find out the parents

        return directManagerMap;
    }

    public HierarchyDAO getHierarchyDAO() {
        return hierarchyDAO;
    }

    public void setHierarchyDAO(HierarchyDAO hierarchyDAO) {
        this.hierarchyDAO = hierarchyDAO;
    }
}
