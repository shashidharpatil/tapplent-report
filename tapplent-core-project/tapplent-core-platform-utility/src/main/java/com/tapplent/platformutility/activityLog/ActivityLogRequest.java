package com.tapplent.platformutility.activityLog;

/**
 * Created by tapplent on 01/12/17.
 */
public class ActivityLogRequest {
    private String baseTemplate;
    private String mtPE;
    private String versionId;

    public String getBaseTemplate() {
        return baseTemplate;
    }

    public void setBaseTemplate(String baseTemplate) {
        this.baseTemplate = baseTemplate;
    }

    public String getMtPE() {
        return mtPE;
    }

    public void setMtPE(String mtPE) {
        this.mtPE = mtPE;
    }

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }
}
