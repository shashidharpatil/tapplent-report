package com.tapplent.platformutility.g11n.service;

import com.tapplent.platformutility.common.cache.SystemAwareCache;
import com.tapplent.platformutility.common.cache.TenantAwareCache;
import com.tapplent.platformutility.g11n.dao.G11nDAO;
import com.tapplent.platformutility.g11n.structure.OEMObject;
import com.tapplent.platformutility.person.structure.Person;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.*;

public class G11nServiceImpl implements G11nService {

    G11nDAO g11nDAO;


    public G11nDAO getG11nDAO() {
        return g11nDAO;
    }

    public void setG11nDAO(G11nDAO g11nDAO) {
        this.g11nDAO = g11nDAO;
    }
}
