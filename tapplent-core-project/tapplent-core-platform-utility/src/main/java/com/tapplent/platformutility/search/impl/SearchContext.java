package com.tapplent.platformutility.search.impl;

import java.sql.Timestamp;



import java.util.ArrayDeque;


import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.tapplent.platformutility.common.util.Util;
import org.springframework.beans.BeanUtils;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.platformutility.lexer.Lexer;
import com.tapplent.platformutility.search.builder.DbSearchFilter;
import com.tapplent.platformutility.search.builder.DbSearchRelation;
import com.tapplent.platformutility.search.builder.DbSearchTemplate;
import com.tapplent.platformutility.search.builder.DbSearchVO;
import com.tapplent.platformutility.search.builder.FilterOperator;
import com.tapplent.platformutility.search.builder.FilterOperatorValue;
import com.tapplent.platformutility.search.builder.SearchField;
import com.tapplent.platformutility.search.builder.SortData;
import com.tapplent.platformutility.search.builder.expression.ExpressionBuilder;
import com.tapplent.platformutility.search.builder.expression.LeafOperand;
import com.tapplent.platformutility.search.builder.expression.Operator;
import com.tapplent.platformutility.search.builder.joins.JoinExpressionBuilder;
import com.tapplent.platformutility.search.builder.joins.JoinLeafOperand;
import com.tapplent.platformutility.search.builder.joins.JoinOperatorsNode;
import com.tapplent.platformutility.search.builder.joins.JoinType;

public class SearchContext {
	private static final Logger LOG = LogFactory.getLogger(SearchContext.class);
	private final Map<String, Integer> tableUsage = new HashMap<>();
	private final Map<String, String> tableIdAndAliasMap = new HashMap<>();
    private final Map<String, String> tableIdAndNameMap = new HashMap<>();
	private DbSearchVO dbSearchVO;
	private Map<String, Object> dependencyKeys;
	private List<String> dependencyKeysList;
	private Deque<Object> parameters;
	private Deque<Object> countParameter;
	private Map<String, SearchField> columnLabelToColumnMap = new HashMap<>();
	private boolean isDistinctSelect;
	private ExpressionBuilder selectClause = new ExpressionBuilder();
	private int selectClauseCounter = 0;
	private JoinExpressionBuilder fromClause = new JoinExpressionBuilder();
	private ExpressionBuilder whereClause = new ExpressionBuilder();
	private ExpressionBuilder groupByClause = new ExpressionBuilder();
	private ExpressionBuilder havingClause = new ExpressionBuilder();
	private ExpressionBuilder orderByClause = new ExpressionBuilder();
	private boolean isFunction;
	private Integer page;
    private Integer pageSize;
    private boolean isApplyPagination;
    private int defaultPaginationSize;
    private boolean returnOnlySqlQuery;
    private SearchField functionPrimaryKey;
    private SearchField databasePrimaryKey;
    private boolean isResolveG11n;
	public static final String INVERTED_COMMA = "'";
	
	public DbSearchVO getDbSearchVO() {
		return dbSearchVO;
	}
	public void setDbSearchVO(DbSearchVO dbSearchVO) {
		this.dbSearchVO = dbSearchVO;
	}
	public SearchField getFunctionPrimaryKey() {
		return functionPrimaryKey;
	}
	public void setFunctionPrimaryKey(SearchField functionPrimaryKey) {
		this.functionPrimaryKey = functionPrimaryKey;
	}
	public SearchField getDatabasePrimaryKey() {
		return databasePrimaryKey;
	}
	public void setDatabasePrimaryKey(SearchField databasePrimaryKey) {
		this.databasePrimaryKey = databasePrimaryKey;
	}
	public Map<String, Object> getDependencyKeys() {
		return dependencyKeys;
	}
	public void setDependencyKeys(Map<String, Object> dependencyKeys) {
		this.dependencyKeys = dependencyKeys;
	}
	public List<String> getDependencyKeysList() {
		return dependencyKeysList;
	}
	public void setDependencyKeysList(List<String> dependencyKeysList) {
		this.dependencyKeysList = dependencyKeysList;
	}
	public static Logger getLog() {
		return LOG;
	}
	public Map<String, Integer> getTableUsage() {
		return tableUsage;
	}
	public Deque<Object> getParameters() {
		return parameters;
	}
	public void setParameters(Deque<Object> parameters) {
		this.parameters = parameters;
	}
	public Deque<Object> getCountParameter() {
		return countParameter;
	}
	public void setCountParameter(Deque<Object> countParameter) {
		this.countParameter = countParameter;
	}
	public Map<String, SearchField> getColumnLabelToColumnMap() {
		return columnLabelToColumnMap;
	}
	public void setColumnLabelToColumnMap(Map<String, SearchField> columnLabelToColumnMap) {
		this.columnLabelToColumnMap = columnLabelToColumnMap;
	}
	public boolean isDistinctSelect() {
		return isDistinctSelect;
	}
	public void setDistinctSelect(boolean isDistinctSelect) {
		this.isDistinctSelect = isDistinctSelect;
	}
	public ExpressionBuilder getSelectClause() {
		return selectClause;
	}
	public void setSelectClause(ExpressionBuilder selectClause) {
		this.selectClause = selectClause;
	}
	public JoinExpressionBuilder getFromClause() {
		return fromClause;
	}
	public void setFromClause(JoinExpressionBuilder fromClause) {
		this.fromClause = fromClause;
	}
	public ExpressionBuilder getWhereClause() {
		return whereClause;
	}
	public void setWhereClause(ExpressionBuilder whereClause) {
		this.whereClause = whereClause;
	}
	public ExpressionBuilder getGroupByClause() {
		return groupByClause;
	}
	public void setGroupByClause(ExpressionBuilder groupByClause) {
		this.groupByClause = groupByClause;
	}
	public boolean isFunction() {
		return isFunction;
	}
	public void setFunction(boolean isFunction) {
		this.isFunction = isFunction;
	}
	public ExpressionBuilder getOrderByClause() {
		return orderByClause;
	}
	public void setOrderByClause(ExpressionBuilder orderByClause) {
		this.orderByClause = orderByClause;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isApplyPagination() {
		return isApplyPagination;
	}

	public void setApplyPagination(boolean applyPagination) {
		isApplyPagination = applyPagination;
	}

	public boolean isReturnOnlySqlQuery() {
		return returnOnlySqlQuery;
	}

	public void setReturnOnlySqlQuery(boolean returnOnlySqlQuery) {
		this.returnOnlySqlQuery = returnOnlySqlQuery;
	}

	public boolean isResolveG11n() {
		return isResolveG11n;
	}

	public void setResolveG11n(boolean resolveG11n) {
		isResolveG11n = resolveG11n;
	}

	public int getDefaultPaginationSize() {
		return defaultPaginationSize;
	}

	public void setDefaultPaginationSize(int defaultPaginationSize) {
		this.defaultPaginationSize = defaultPaginationSize;
	}

	public ExpressionBuilder getHavingClause() {
		return havingClause;
	}

	public void setHavingClause(ExpressionBuilder havingClause) {
		this.havingClause = havingClause;
	}

	public static SearchContext getInstance(DefaultSearchData searchData){
		SearchContext ctx = new SearchContext();
		ctx.initialize(searchData);
		return ctx;
	}
	private void initialize(DefaultSearchData searchData) {
		if(searchData.isExternalDistinct() || searchData.isInternalDistinct()){
			this.isDistinctSelect = true;
		}
		this.selectClause = searchData.getSelectClause();
		this.fromClause = searchData.getJoinClause();
		this.dependencyKeys = searchData.getDependencyKeys();
		this.whereClause = searchData.getWhereClause();
		this.columnLabelToColumnMap = searchData.getColumnLabelToColumnMap();
		this.parameters = new ArrayDeque<Object>();
		this.countParameter = new ArrayDeque<Object>();
		this.dependencyKeysList = new ArrayList<>();
		this.groupByClause = searchData.getGroupByClause();
		this.havingClause = searchData.getHavingClause();
		this.isFunction = searchData.getIsFunctionStmt();
		this.orderByClause = searchData.getOrderByClause();
		this.page = searchData.getPage();
		this.pageSize = searchData.getPageSize();
		this.databasePrimaryKey = searchData.getDatabasePrimaryKey();
		this.functionPrimaryKey = searchData.getFunctionPrimaryKey();
		this.isResolveG11n = searchData.isResolveG11n();
		this.isApplyPagination = searchData.isApplyPagination();
		this.defaultPaginationSize = searchData.getDefaultPaginationSize();
		this.returnOnlySqlQuery = searchData.isReturnOnlySqlQuery();
		//A process to convert searchdata to DbSearchVO
//		this.dbSearchVO = createDbSearchVO(searchData);
	}
//	@Deprecated
//	@SuppressWarnings("rawtypes")
//	private DbSearchVO createDbSearchVO(DefaultSearchData searchData) {
//		//Copy page and pagesize
//		//You need to create DbSearchVO object
//		//start looking into searchData
//		DbSearchVO dbSearchVO = new DbSearchVO();
//		BeanUtils.copyProperties(searchData, dbSearchVO);
//		DomainObjectTemplate baseDomainObjectTemplate = searchData.getDomainObjectTemplate();
//		List<DbSearchTemplate> tables = dbSearchVO.getTables();
//		DbSearchTemplate primaryDo = new DbSearchTemplate();
//		primaryDo.setTableId(baseDomainObjectTemplate.getDomainObjectNameG11nText());
//		primaryDo.setTable(baseDomainObjectTemplate.getDbTableName());
//		primaryDo.setTableAlias(getTableAlias(primaryDo.getTableId(),baseDomainObjectTemplate.getDbTableName()));
//
//		primaryDo.setDomainObjectCode(baseDomainObjectTemplate.getDomainObjectNameG11nText());
//		primaryDo.setPrimaryDO(true);
//		tables.add(primaryDo);
//		/*
//		 * Adding code to test JoinExpressionBuilder
//		 */
//		JoinLeafOperand primaryTable = new JoinLeafOperand(primaryDo.getTable(),primaryDo.getTableAlias());
//		fromClause.addJoinTokenToTree(primaryTable);
//		//Loop through the columns MAP and prepare column for making entry into DbSearchVO
//		Iterator searchFieldsIterator = baseDomainObjectTemplate.searchFields.entrySet().iterator();
//		while(searchFieldsIterator.hasNext()){
//			Map.Entry entry = (Map.Entry) searchFieldsIterator.next();
//			DomainObjectAttributeTemplate doAttr = (DomainObjectAttributeTemplate) entry.getValue();
//			SearchField searchField = new SearchField(doAttr.getMtDoAttributeSpecFkG11nText());
//			searchField.setColumnName(doAttr.getDbColumnName());
//			searchField.setColumnAlias(INVERTED_COMMA+doAttr.getMtDoAttributeSpecFkG11nText()+INVERTED_COMMA);
//			searchField.setColumnLabel(doAttr.getMtDoAttributeSpecFkG11nText());
//			searchField.setDataType(doAttr.getDbDataTypeCodeFkId());
//			searchField.setTableAlias(primaryDo.getTableAlias());
//			/*
//			 * Adding the code to test the expression builder.
//			 */
//			addColumnToSelectClause(searchField, primaryDo.getTableAlias());
//			if(doAttr.getDependencyKeyFlag()){
//				dependencyKeysList.add(searchField.getColumnLabel());
//				searchField.setDependencyFlag(doAttr.getDependencyKeyFlag());
//			}
//			//if dependency key flag is true add the column in filter here itself
//			if(doAttr.isRelationToBeConsidered()){
//				DbSearchTemplate relatedDo = new DbSearchTemplate();
//				relatedDo.setTable(doAttr.getToDbTableName());
//				relatedDo.setTableId(doAttr.getMtDoAttributeSpecFkG11nText());
//				relatedDo.setTableAlias(getTableAlias(relatedDo.getTableId(),doAttr.getToDbTableName()));
//
//				relatedDo.setDomainObjectCode(doAttr.getToDomainObjectNameG11nText());
//				/*
//				 * Adding code to test JoinExpressionBuilder
//				 */
//				String toDOAName = doAttr.getToDoAttributeNameG11nText();
//				JoinOperatorsNode operatorNode = new JoinOperatorsNode();
//				operatorNode.setJoinType(JoinType.LEFT_OUTER_JOIN);
//				ExpressionBuilder onExpression = new ExpressionBuilder();
//				LeafOperand fromKey = new LeafOperand(true,searchField.getColumnName(), searchField.getDoAtributeCode(), searchField.getDataType(), primaryDo.getTableAlias());
//				onExpression.addExprTokenToTree(fromKey);
//				String operator = Operator.EQ.toString();
//				onExpression.addExprTokenToTree(operator);
//
//				operatorNode.setExpression(onExpression);
//				LeafOperand toKey = null;
//
//				Iterator relatedFieldsIterator = doAttr.relationTableFields.entrySet().iterator();
//				while(relatedFieldsIterator.hasNext()){
//					Map.Entry relatedEntry = (Map.Entry) relatedFieldsIterator.next();
//					DomainObjectAttributeRelationFields doaRelationField = (DomainObjectAttributeRelationFields) relatedEntry.getValue();
//					SearchField relatedSearchField = new SearchField(doAttr.getMtDoAttributeSpecFkG11nText()+"."+doaRelationField.getMtDoAttributeSpecG11nText());
//					relatedSearchField.setColumnName(doaRelationField.getDbColumnName());
//					relatedSearchField.setColumnAlias(INVERTED_COMMA+doAttr.getMtDoAttributeSpecFkG11nText()+":"+doaRelationField.getMtDoAttributeSpecG11nText()+INVERTED_COMMA);
//					relatedSearchField.setColumnLabel(doAttr.getMtDoAttributeSpecFkG11nText()+":"+doaRelationField.getMtDoAttributeSpecG11nText());
//					relatedSearchField.setDataType(doaRelationField.getRelationDbDataTypeCodeFkId());
//					relatedSearchField.setTableAlias(relatedDo.getTableAlias());
//					relatedDo.getColumnLabelToColumnMap().put(relatedSearchField.getColumnLabel(), relatedSearchField);
//					relatedDo.getColumnAliasToColumnMap().put(relatedSearchField.getColumnAlias(), relatedSearchField);
//					/*
//					 * Adding code to test JoinExpressionBuilder
//					 */
//					if(doaRelationField.getMtDoAttributeSpecG11nText().equals(toDOAName)){
//						toKey = new LeafOperand(true,relatedSearchField.getColumnName(), relatedSearchField.getDoAtributeCode(), relatedSearchField.getDataType(), relatedDo.getTableAlias());
//						onExpression.addExprTokenToTree(toKey);
//					}
//					/*
//					 * Adding the code to test the expression builder.
//					 */
//					addColumnToSelectClause(relatedSearchField, relatedDo.getTableAlias());
//					columnLabelToColumnMap.put(relatedSearchField.getColumnLabel(), relatedSearchField);
//					relatedDo.getColumns().add(relatedSearchField);
//				}
//
//				fromClause.addJoinTokenToTree(operatorNode);
//				JoinLeafOperand relatedTable = new JoinLeafOperand(relatedDo.getTable(), relatedDo.getTableAlias());
//				fromClause.addJoinTokenToTree(relatedTable);
//
//				DbSearchRelation relation = new DbSearchRelation();
//				relation.setTableFrom(primaryDo.getTableId());
//				relation.setTableFromKey(doAttr.getDbColumnName());
//				relation.setTableTo(relatedDo.getTableId());
//				relation.setTableToKey(doAttr.getToDbColumnName());
//				relation.setConnectType("left_outer_join");
//
//				dbSearchVO.getRelations().add(relation);
//				tables.add(relatedDo);
//			}
//			primaryDo.getColumnAliasToColumnMap().put(searchField.getColumnAlias(),searchField);
//			columnLabelToColumnMap.put(searchField.getColumnLabel(),searchField);
//			primaryDo.getColumnLabelToColumnMap().put(searchField.getColumnLabel(), searchField);
//			primaryDo.getColumns().add(searchField);
//		}
//		//Add the external filters to Primary DO. Loop through the searchdata's external filters look for the column name in the column to search field map.
//		//if the column alias exists add that filter or else give error
//		//Add dependency keys to WHERE CLAUSE.
////		for(int i = 0; i<dependencyKeysList.size(); i++){
////			SearchFields dependencyColumn = columnLabelToColumnMap.get(dependencyKeysList.get(i));
////			if(i==0 && whereClause.getExpressionTree()==null){
////				whereClause.addExprTokenToTree(Operator.OPEN_PARENTHESIS);
////				addDependencyFieldToFilter(searchData.getDependencyKeys(),primaryDo,dependencyColumn);
////			}else{
////				whereClause.addExprTokenToTree(Operator.AND);
////				addDependencyFieldToFilter(searchData.getDependencyKeys(),primaryDo,dependencyColumn);
////			}
////		}
////		if(dependencyKeysList.size()!=0)
////			whereClause.addExprTokenToTree(Operator.CLOSE_PARENTHESIS);
//		addDependencyFieldToFilter(primaryDo,searchData.getDependencyKeys());
////		if(searchData.getExternalFilters()!=null && !searchData.getExternalFilters().isEmpty()){
//			addExternalFilters(searchData.getExternalFilters(),primaryDo, searchData.getFilterExpression());
////		}
//		primaryDo.setStickyHdrSortData(searchData.getStickyHdrSortData());
//		//Add SortData to sequenceNumber to sort data map
//		if(searchData.getStickyHdrSortData() != null){
//			for(SortData sortData : searchData.getStickyHdrSortData()){
//				primaryDo.getSeqNumberToSortMap().put(sortData.getSeqNumber(), sortData);
//			}
//		}
////		dbSearchVO.setTables(tables);
//		return dbSearchVO;
//	}
	
	private void addColumnToSelectClause(SearchField searchField, String tableAlias) {
		LeafOperand leafOperand = new LeafOperand(true,searchField.getColumnName(), searchField.getDoAtributeCode(), searchField.getColumnAlias(),searchField.getDataType(),tableAlias, searchField.isGlocalized(), searchField.getContainerBlobName());
		if(selectClauseCounter==0){
			selectClause.addExprTokenToTree(leafOperand);
			selectClauseCounter++;
		}else{
			selectClause.addExprTokenToTree(Operator.COMMA);
			selectClauseCounter++;
			selectClause.addExprTokenToTree(leafOperand);
			selectClauseCounter++;
		}
	}
	private void addExternalFilters(Map<String, List<DomainObjectFilter>> externalFilters, DbSearchTemplate primaryDo, String filterExpression) {
		if(externalFilters != null ){
			for(Map.Entry<String, List<DomainObjectFilter>> entry : externalFilters.entrySet() ){
				String columnLabel = entry.getKey();
				if(primaryDo.getColumnLabelToColumnMap().containsKey(columnLabel)){
					SearchField column = primaryDo.getColumnLabelToColumnMap().get(columnLabel);
					DbSearchFilter filter = new DbSearchFilter();
					filter.setColumnLabel(columnLabel);
					filter.setDbDataType(column.getDataType());
					//Another loop for reading all the filters for a column
					List<DomainObjectFilter> operatorValueList = new ArrayList<DomainObjectFilter>();
					operatorValueList.addAll(entry.getValue());
					for(int j =0; j<operatorValueList.size(); j++){
						if(operatorValueList.get(j)!=null){
							DomainObjectFilter opVal = operatorValueList.get(j);
							FilterOperatorValue filterOperatorValue = new FilterOperatorValue(opVal.getOperator().getSqlCondition(), opVal.getValue().toString().substring(1, opVal.getValue().toString().length()-1));
							filter.getOpVal().add(filterOperatorValue);
						}
					}
					primaryDo.getFilters().add(filter);
				}
				else{
					LOG.error("Filter Attribute"+columnLabel +"does not exist in Database");
					//report error
				}
			}
		}
		if(filterExpression !=null){
		if(!whereClause.isEmpty()){
			whereClause.addExprTokenToTree(Operator.AND);
		}
		if(filterExpression!=null){
			List<Object> filterTokens = (new Lexer()).getTokens(filterExpression).getTokens();
			for(Object token : filterTokens){
				if(token instanceof LeafOperand){
					LeafOperand leaf = (LeafOperand)token;
					if(leaf.getIsColumnName()){
						String columnLabel = leaf.getValue();
						SearchField column= columnLabelToColumnMap.get(columnLabel);
						leaf.setColumnAlias(column.getColumnAlias());
						leaf.setColumnName(column.getColumnName());
						leaf.setDataType(column.getDataType());
						leaf.setTableAlias(column.getTableAlias());
					}
					whereClause.addExprTokenToTree(leaf);
				}else{
					whereClause.addExprTokenToTree(token);
				}
			}
		}}
	}
	private void addDependencyFieldToFilter(DbSearchTemplate primaryDo, Map<String, Object> dependencyKeyMap){
		for(int i = 0; i<dependencyKeysList.size(); i++){
			SearchField dependencyColumn = columnLabelToColumnMap.get(dependencyKeysList.get(i));
			if(i==0 && whereClause.getExpressionTree()==null){
				whereClause.addExprTokenToTree(Operator.OPEN_PARENTHESIS);
				addDependencyFieldToFilter(dependencyKeyMap,primaryDo,dependencyColumn);
			}else{
				whereClause.addExprTokenToTree(Operator.AND);
				addDependencyFieldToFilter(dependencyKeyMap,primaryDo,dependencyColumn);
			}
		}
		if(dependencyKeysList.size()!=0)
			whereClause.addExprTokenToTree(Operator.CLOSE_PARENTHESIS);
	}
//	@SuppressWarnings("unchecked")
	private void addDependencyFieldToFilter(Map<String, Object> dependencyKeysMap, DbSearchTemplate primaryDo, SearchField searchField) {
		if(dependencyKeysMap == null)
			throw new IllegalArgumentException("Dependency key map is not provided.");
		String columnLabel = searchField.getDoAtributeCode();
		if(dependencyKeysMap.containsKey(columnLabel)){
			@SuppressWarnings("unchecked")
			ArrayList<String> values = ((ArrayList<String>) dependencyKeysMap.get(columnLabel));
			FilterOperatorValue opVal = new FilterOperatorValue();
//			opVal.setOperator(Operator.IN);
			StringBuilder value = new StringBuilder();
			for(int i=0; i<values.size(); i++){
				value.append(values.get(i));
				value.append(",");
			}
			value = value.replace(value.length()-2, value.length()-1,"");
			/*
			 * Adding code to test ExpressionBuilder
			 */
			LeafOperand filterOperand = new LeafOperand(true,searchField.getColumnName(), searchField.getDoAtributeCode(), searchField.getDataType(), primaryDo.getTableAlias());
			whereClause.addExprTokenToTree(filterOperand);
			whereClause.addExprTokenToTree(Operator.IN);
			whereClause.addExprTokenToTree(Operator.OPEN_PARENTHESIS);
			for(int i=0; i<values.size(); i++){
				String inValue = values.get(i);
				if(i==0){
					LeafOperand operand = new LeafOperand(inValue, searchField.getDataType());
					whereClause.addExprTokenToTree(operand);
				}else{
					whereClause.addExprTokenToTree(Operator.COMMA);
					LeafOperand operand = new LeafOperand(inValue, searchField.getDataType());
					whereClause.addExprTokenToTree(operand);
				}
				value.append(values.get(i));
				value.append(",");
			}
			whereClause.addExprTokenToTree(Operator.CLOSE_PARENTHESIS);
			
			opVal.setValue(value.toString());
			DbSearchFilter filter = new DbSearchFilter();
			filter.getOpVal().add(opVal);
			filter.setColumnLabel(columnLabel);
			filter.setDbDataType(searchField.getDataType());
			primaryDo.getFilters().add(filter);
		}else{
			throw new IllegalArgumentException("Dependency key {} "+columnLabel+" is not provided.");
		}
	}
    public String getTableAlias(String id, String tableName) {
        String alias;
        Integer suffix = Optional.ofNullable(tableUsage.get(tableName)).orElse(0);
        alias = tableName + (++suffix);
        tableUsage.put(tableName, suffix);
        tableIdAndAliasMap.put(id, alias);
        tableIdAndNameMap.put(id, tableName);
        return alias;
    }
	/*
	 * Adds the parameter to Context's parameter and count parameter list
	 * If the parameter is to be added to count SQL statement keep the parameter as true
	 */
	public void addParameters(Object parameter, String dbDatatype, Boolean isCountParameter ){
		if(parameter instanceof String){
			parameter = getData((String)parameter, dbDatatype);
		}
		if(parameter instanceof String){
			//T_ID should become ENUM or static variable.
			if(dbDatatype.equals("T_ID")){
    			String param = "0x"+(String) parameter;
    			this.getParameters().add(param);
    			if(isCountParameter)
    				this.getCountParameter().add(param);
    		}
    		else{
    			this.getParameters().add(parameter);
    			if(isCountParameter)
    				this.getCountParameter().add(parameter);
    		}
    	}
		else{
			this.getParameters().add(parameter);
			if(isCountParameter)
				this.getCountParameter().add(parameter);
		}
	}
	public void addParameters(String value, String dbDatatype, Boolean isCountParameter ){
		Object parameter = getData(value, dbDatatype);
		if(parameter instanceof String){
			//T_ID should become ENUM or static variable.
			if(dbDatatype.equals("T_ID")){
				Object param = Util.convertStringIdToByteId(value);
    			this.getParameters().add(param);
    			if(isCountParameter)
    				this.getCountParameter().add(param);
    		}
    		else{
    			this.getParameters().add(parameter);
    			if(isCountParameter)
    				this.getCountParameter().add(parameter);
    		}
    	}
		else{
			this.getParameters().add(parameter);
			if(isCountParameter)
				this.getCountParameter().add(parameter);
		}
	}
	private Object getData(String value, String dataType) {
        Object data = null;
        switch (dataType) {
            case "CHAR":
            case "VARCHAR":
                data = value;
                break;
            case "DATETIME":
                data = new Timestamp(Long.parseLong(value));
                break;
            case "BOOLEAN":
                data = Boolean.getBoolean(value);
                break;
            case "INTEGER":
            case "NUMERIC":
                data = Long.valueOf(value);
                break;
            default:
                data = value;
        }
        return data;
    }
}
