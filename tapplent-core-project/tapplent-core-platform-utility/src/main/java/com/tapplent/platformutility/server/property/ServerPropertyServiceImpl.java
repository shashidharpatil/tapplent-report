package com.tapplent.platformutility.server.property;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.tapplent.tenantresolver.server.property.ServerActionStep;
import com.tapplent.tenantresolver.server.property.ServerActionStepProperty;

public class ServerPropertyServiceImpl implements ServerPropertyService {

	ServerPropertyDAO serverPropertyDAO;
	
	@Override
	public Map<String, LinkedList<ServerActionStep>> getactionCodeActionStepListMap() {
		Map<String, ServerActionStep> actionCodeActionStepMap = serverPropertyDAO.getAllServerActionStep();
		Map<String, List<ServerActionStepProperty>> actionStepPropertyMap = serverPropertyDAO.getAllServerActionStepProperty();
		Map<String, LinkedList<ServerActionStep>> actionCodeActionStepListMap = new HashMap<>();
		for(Entry<String, ServerActionStep> entry : actionCodeActionStepMap.entrySet()){
			String actionCode = entry.getValue().getActionCodeFkId();
			if(!actionCodeActionStepListMap.containsKey(actionCode)){
				List<ServerActionStep> filteredByActionCode = actionCodeActionStepMap.values().stream()
														.filter(p -> p.getActionCodeFkId().equals(actionCode))
														.collect(Collectors.toList());
				LinkedList<ServerActionStep> serverActionStepLinkedList = new LinkedList<>();
				Map<String, ServerActionStep> parentActionStepMap = new HashMap<>();
				Map<String, ServerActionStep> actionStepMap = new HashMap<>();
				for(ServerActionStep SAStep : filteredByActionCode){
					parentActionStepMap.put(SAStep.getParentActionStepId(), SAStep);
					actionStepMap.put(SAStep.getActionStepLookupPkId(),  SAStep);
				}
				Optional<ServerActionStep> rootActionStep = filteredByActionCode.stream()
																				.filter(p -> p.getParentActionStepId() == null)
																				.findFirst();
				ServerActionStep currentActionStep = rootActionStep.get();
				while(currentActionStep != null){
					serverActionStepLinkedList.add(currentActionStep);
//					System.out.println(currentActionStep.getActionCodeFkId() + "-->" + currentActionStep.getServerRuleCodeFkId());
					//add all properties related to current action step.
					if(actionStepPropertyMap.containsKey(currentActionStep.getActionStepLookupPkId()))
						populateAllActionStepProperties(currentActionStep, actionStepPropertyMap);
					currentActionStep = parentActionStepMap.get(currentActionStep.getActionStepId());
				}
				actionCodeActionStepListMap.put(actionCode, serverActionStepLinkedList);
			}
		}
		return actionCodeActionStepListMap;
	}

	private void populateAllActionStepProperties(ServerActionStep currentActionStep,
			Map<String, List<ServerActionStepProperty>> actionStepPropertyMap) {
		for(ServerActionStepProperty ServerActionStepProperty : actionStepPropertyMap.get(currentActionStep.getActionStepLookupPkId()))
			currentActionStep.getPropertyCodeValueMap().put(ServerActionStepProperty.getActionStepPropertyCodeFkId(), ServerActionStepProperty.getPropertyValue());
	}

	public ServerPropertyDAO getServerPropertyDAO() {
		return serverPropertyDAO;
	}

	public void setServerPropertyDAO(ServerPropertyDAO serverPropertyDAO) {
		this.serverPropertyDAO = serverPropertyDAO;
	}

	
	
}
