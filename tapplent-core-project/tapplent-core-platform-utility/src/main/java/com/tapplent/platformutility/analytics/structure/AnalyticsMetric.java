package com.tapplent.platformutility.analytics.structure;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tapplent on 05/12/16.
 */
public class AnalyticsMetric {
    private String analyticsMetricPkId;
    private String analyticsContextFkId;
    private String analyticsMetricNameG11nBigTxt;
    private String introductionG11nBigTxt;
    private int seqNumPosInt;
    private List<AnalyticsMetricDetails> analyticsMetricDetailsList = new ArrayList<>();
    public String getAnalyticsMetricPkId() {
        return analyticsMetricPkId;
    }

    public void setAnalyticsMetricPkId(String analyticsMetricPkId) {
        this.analyticsMetricPkId = analyticsMetricPkId;
    }

    public String getAnalyticsContextFkId() {
        return analyticsContextFkId;
    }

    public void setAnalyticsContextFkId(String analyticsContextFkId) {
        this.analyticsContextFkId = analyticsContextFkId;
    }

    public String getAnalyticsMetricNameG11nBigTxt() {
        return analyticsMetricNameG11nBigTxt;
    }

    public void setAnalyticsMetricNameG11nBigTxt(String analyticsMetricNameG11nBigTxt) {
        this.analyticsMetricNameG11nBigTxt = analyticsMetricNameG11nBigTxt;
    }

    public String getIntroductionG11nBigTxt() {
        return introductionG11nBigTxt;
    }

    public void setIntroductionG11nBigTxt(String introductionG11nBigTxt) {
        this.introductionG11nBigTxt = introductionG11nBigTxt;
    }

    public int getSeqNumPosInt() {
        return seqNumPosInt;
    }

    public void setSeqNumPosInt(int seqNumPosInt) {
        this.seqNumPosInt = seqNumPosInt;
    }

    public List<AnalyticsMetricDetails> getAnalyticsMetricDetailsList() {
        return analyticsMetricDetailsList;
    }

    public void setAnalyticsMetricDetailsList(List<AnalyticsMetricDetails> analyticsMetricDetailsList) {
        this.analyticsMetricDetailsList = analyticsMetricDetailsList;
    }
}
