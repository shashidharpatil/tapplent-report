package com.tapplent.platformutility.batch.model;

public class SchedulerInstanceModel {
	private String schedulerInstancePkId;
	private String schedDefnFkId;
	private String executionVersionId;
	private String jobTypeCodeFkId;
	private String scheduledDatetime;
	private String executionStartDatetime;
	private String executionEndDatetime;
	private String execStatusCodeFkId;
	private String exitCodeFkId;
	private String exitMessageBigTxt;
	public String getSchedulerInstancePkId() {
		return schedulerInstancePkId;
	}
	public void setSchedulerInstancePkId(String schedulerInstancePkId) {
		this.schedulerInstancePkId = schedulerInstancePkId;
	}
	public String getSchedDefnFkId() {
		return schedDefnFkId;
	}
	public void setSchedDefnFkId(String schedDefnFkId) {
		this.schedDefnFkId = schedDefnFkId;
	}
	public String getExecutionVersionId() {
		return executionVersionId;
	}
	public void setExecutionVersionId(String executionVersionId) {
		this.executionVersionId = executionVersionId;
	}
	public String getJobTypeCodeFkId() {
		return jobTypeCodeFkId;
	}
	public void setJobTypeCodeFkId(String jobTypeCodeFkId) {
		this.jobTypeCodeFkId = jobTypeCodeFkId;
	}
	public String getScheduledDatetime() {
		return scheduledDatetime;
	}
	public void setScheduledDatetime(String scheduledDatetime) {
		this.scheduledDatetime = scheduledDatetime;
	}
	public String getExecutionStartDatetime() {
		return executionStartDatetime;
	}
	public void setExecutionStartDatetime(String executionStartDatetime) {
		this.executionStartDatetime = executionStartDatetime;
	}
	public String getExecutionEndDatetime() {
		return executionEndDatetime;
	}
	public void setExecutionEndDatetime(String executionEndDatetime) {
		this.executionEndDatetime = executionEndDatetime;
	}
	public String getExecStatusCodeFkId() {
		return execStatusCodeFkId;
	}
	public void setExecStatusCodeFkId(String execStatusCodeFkId) {
		this.execStatusCodeFkId = execStatusCodeFkId;
	}
	public String getExitCodeFkId() {
		return exitCodeFkId;
	}
	public void setExitCodeFkId(String exitCodeFkId) {
		this.exitCodeFkId = exitCodeFkId;
	}
	public String getExitMessageBigTxt() {
		return exitMessageBigTxt;
	}
	public void setExitMessageBigTxt(String exitMessageBigTxt) {
		this.exitMessageBigTxt = exitMessageBigTxt;
	}
	
	
}
