package com.tapplent.platformutility.metadata.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.tapplent.platformutility.accessmanager.structure.AMColCondition;
import com.tapplent.platformutility.accessmanager.structure.RowCondition;
import com.tapplent.platformutility.activityLog.AuditMsgFormat;
import com.tapplent.platformutility.common.util.valueObject.AnalyticsDOAControlMap;
import com.tapplent.platformutility.common.util.valueObject.NameCalculationRuleVO;
import com.tapplent.platformutility.layout.valueObject.IconVO;
import com.tapplent.platformutility.layout.valueObject.LicencedLocalesVO;
import com.tapplent.platformutility.metadata.BtDoAttributeSpecVO;
import com.tapplent.platformutility.metadata.BtDoSpecVO;
import com.tapplent.platformutility.metadata.DataActionsVO;
import com.tapplent.platformutility.metadata.MTPEVO;
import com.tapplent.platformutility.metadata.MtDoAttributeVO;
import com.tapplent.platformutility.metadata.MtDomainObjectVO;
import com.tapplent.platformutility.metadata.structure.*;
import com.tapplent.platformutility.uilayout.valueobject.LayoutThemeMapperVO;

public interface MetadataDAO {

	Map<String, BtDoSpecVO> getBtDoSpec(String baseTemplate, String mtPE, String processElementId);

	List<BtDoAttributeSpecVO> getBtDoAttributeSpec(String btDoSpecFkId);

	MtDomainObjectVO getMtDomainObjectByDo(String doName);

	List<MtDoAttributeVO> getMtDoAttributeByDo(String domainObjectCodePkId);

	BtDoSpecVO getBtDoSpec(String bt, String doName);

	List<BtDoAttributeSpecVO> getBtDoAttributeSpecListByMtDoa(String doaCodeFkId);

	List<AttributePathDetails> getControlAttributePaths(String baseTemplate, String mtPE);

	/**
	 * @param baseTemplateId
	 * @param mtPE
	 * @return
	 */
	List<AttributePathDetails> getRelationControlAttributePaths(String baseTemplateId, String mtPE);

	/**
	 * @param baseTemplateId
	 * @param mtPE
	 * @return
	 */
	List<AttributePathDetails> getHierarchyControlAttributePaths(String baseTemplateId, String mtPE);

	/**
	 * @param baseTemplateId
	 * @param mtPE
	 * @return
	 */
	List<AttributePathDetails> getExternalAPIAttributePaths(String baseTemplateId, String mtPE);

	/**
	 * @param mtPE
	 * @return
	 */
	String getDOCodeByMTPE(String mtPE);

	/**
	 * @return
	 */
	List<DataActionsVO> getDataActions();

	/**
	 * @return all the BT-DO attributes present in the table BTDOATTRIBUTESPEC 
	 */
	List<BtDoAttributeSpecVO> getBTDOAtrributeList();

	Map<String, IconVO> getIconMap();

    List<MTPEVO> getMtPEMap();

    List<String> getDependencyKeyRelatedPEs(String mtPE);
    TemplateMapperVO getTemplateMapperByMtPE(String mtPE);

    Map<String,PropertyControlMaster> getPropertyControlMap();

    CountrySpecificDOAMetaVO getCountrySpecificDOAMeta(String btDoSpecId, String baseCountryCodeFkId);

    AuditMsgFormat getAuditMsgFormatByMtPEAndAction(String activityLogActionCode, String mtPE);

    String getSubjectUserId(StringBuilder sql, String dbDataTypeCode);

    UiSettingVO getUiSetting();

    List<LayoutThemeMapperVO> getLayoutThemeMapperVOs();

    List<LicencedLocalesVO> getLicencedLocale();

    List<RowCondition> getRowConditionsForAllMTPEs();

	Map<String, List<AMColCondition>> getColumnConditions(Set<String> strings);

    void insertLabelAlias(String label, Integer currentMaxAlias) throws SQLException;

	Map<String,Integer> getLabelAlias();

    NameCalculationRuleVO getNameCalculationRule(String localeCode, String nameType);

	List<String> getForeignKeyRelatedPEs(String mtPE);

    List<AnalyticsDOAControlMap> getAnalyticsDoaToControlMap();
}
