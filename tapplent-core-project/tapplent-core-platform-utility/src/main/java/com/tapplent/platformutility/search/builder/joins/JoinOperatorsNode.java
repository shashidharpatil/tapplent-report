package com.tapplent.platformutility.search.builder.joins;

import com.tapplent.platformutility.search.builder.expression.ExpressionBuilder;

public class JoinOperatorsNode {
	private JoinType joinType;
	private ExpressionBuilder expression;
	public JoinType getJoinType() {
		return joinType;
	}
	public void setJoinType(JoinType joinType) {
		this.joinType = joinType;
	}
	public ExpressionBuilder getExpression() {
		return expression;
	}
	public void setExpression(ExpressionBuilder expression) {
		this.expression = expression;
	}
	@Override
	public String toString() {
		return joinType.toString();
	}
}