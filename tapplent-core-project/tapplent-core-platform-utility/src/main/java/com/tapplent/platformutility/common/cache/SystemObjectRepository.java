package com.tapplent.platformutility.common.cache;

import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.amazonaws.services.s3.AmazonS3;
import com.jcraft.jsch.ChannelSftp;
import com.tapplent.platformutility.g11n.structure.OEMObject;
import com.tapplent.platformutility.system.SystemService;
import com.tapplent.platformutility.system.SystemServiceImpl;

public class SystemObjectRepository {
	private final Deque<AmazonS3> amazonDQ = new LinkedList<>();
	private final Deque<ChannelSftp> sftpDQ = new LinkedList<>();
	private final Map<String, OEMObject> oemObjectMap = new HashMap<>();
	private final Map<String, Map<String, String>> oemToLocaleLangMap = new HashMap<>();
	
	SystemService systemService;
	
	public AmazonS3 getAmazonS3Client(){
		if(amazonDQ.isEmpty()){
			AmazonS3 s3Client = systemService.getAmazonS3Client();
			amazonDQ.add(s3Client);
			return s3Client;
		}
		else return amazonDQ.peek();
	}
	
	public ChannelSftp getSftpServerConnection(){
		if(!sftpDQ.isEmpty() && sftpDQ.peek().isConnected()){
			return sftpDQ.peek();
		}
		else{
			sftpDQ.clear();
			ChannelSftp channelSftp = systemService.getSftpServerConnection();
			sftpDQ.add(channelSftp);
			return channelSftp;
		}
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public void resetSystemObjectRepository(){
		amazonDQ.clear();
		sftpDQ.clear();
		oemObjectMap.clear();
		oemToLocaleLangMap.clear();
	}

	public Map<String,OEMObject> getOemObjectMap() {
		if (oemObjectMap.isEmpty()){
			oemObjectMap.putAll(systemService.getOemObjectMap());
		}
		return oemObjectMap;
	}

	public Map<String,Map<String,String>> getOemToLocaleLangMap() {
		if (oemToLocaleLangMap.isEmpty()){
			oemToLocaleLangMap.putAll(systemService.getOemToLocaleLangMap());
		}
		return oemToLocaleLangMap;
	}
}
