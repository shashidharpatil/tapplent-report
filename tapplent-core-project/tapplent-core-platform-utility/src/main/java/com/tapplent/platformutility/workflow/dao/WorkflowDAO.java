package com.tapplent.platformutility.workflow.dao;

import com.tapplent.platformutility.search.api.SearchResult;
import com.tapplent.platformutility.workflow.structure.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Tapplent on 9/16/16.
 */
public interface WorkflowDAO {
    WorkflowTxn getWorkflowTxnById(String workflowTxnId) throws SQLException;
    Map<String, WorkflowStep> getAllWorkflowSteps(String workflowTxnId) throws SQLException;
    SearchResult search(String sql, Deque<Object> parameter) throws SQLException;
    ResultSet searchResultSet(String sql, List<Object> parameter) throws SQLException;
    Map<String,Workflow> getInProgressWorkflows() throws SQLException;
    Map<String, Map<String, WorkflowStep>> getRemainingWorkflowSteps(Set<String> workflowTxnIds) throws SQLException;
    Map<String, Map<String,WorkflowActor>> getAssociatedActors(Collection<Map<String, WorkflowStep>> stepsforTxns);
    Map<String,Map<String,WorkflowActorAction>> getAssociatedActorActions(Map<String, Map<String, WorkflowStep>> remainingWorkflowSteps, Map<String, Map<String, WorkflowActor>> associatedActors);
	
    WorkflowActorAction getWorkflowActorActionDetails(String workflowActionId, String workflowDefnId) throws SQLException;
	List<WorkflowActor> getResolvedSourcePersonActorFromWholeStepByActorTxn(String workflowTxnStepActorId) throws SQLException;
	List<WorkflowActor> getResolvedTargetPersonActorByAction(WorkflowActorAction workflowActorAction, String workflowTxnId) throws SQLException;
//	List<WorkflowActorAction> getWorkflowActorActionWithExitCritCheckApplForActorList(List<WorkflowActor> sourceActorForWholeStep) throws SQLException;
	WorkflowStep getWorkflowTargetStepByAction(WorkflowActorAction workflowActorAction) throws SQLException;
	WorkflowStep getWorkflowSourceStepByAction(WorkflowActorAction workflowActorAction) throws SQLException;
//	List<WorkflowActor> getResolvedPersonActorByStep(WorkflowStep workflowStep, GlobalVariables globalVariables, Map<String, WorkflowStep> sourceAndTargetSteps) throws SQLException;
	List<WorkflowActorAction> getWorkflowActorActionForActorList(List<WorkflowActor> sourcePersonActorList) throws SQLException;

    WorkflowActor getResolvedPersonActor(String workflowActorActionPkId) throws SQLException;

    void deActivateActions(List<WorkflowActorAction> allSourceStepPossibleActions) throws SQLException;

    Workflow getInProgressWorkflow() throws SQLException;

    WorkflowActorAction  getWorkflowDefaultActionForActor(String workflowTxnStepActorPkId) throws SQLException;
    WorkflowActor getresolvedPersonActorFromResultSet(ResultSet rs) throws SQLException;
    WorkflowStep getWorkflowStep(String workflowTxnId) throws SQLException;

    List<WorkflowActor> getNonCompletedWorkflowActors(String workflowStepId) throws SQLException;

    List<WorkflowActorAction> getWorkflowDefaultActionForActorList(List<WorkflowActor> nonCompletedWorkflowActors) throws SQLException;

    Map<String,WorkflowActor> getResolvedPersonActorByStep(String workflowTxnStepPkId) throws SQLException;
    int executeUpdate(String sql, List<Object> parameters) throws SQLException;

    WorkflowStep getNextWorkflowStep(int stepSeqNumPosInt, String workflowTxnPkId) throws SQLException;
}
