package com.tapplent.platformutility.insert.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;

public class ValidationError {
	private static final Logger LOG = LogFactory.getLogger(ValidationError.class);

	public Map<String, List<String>> errors = new HashMap<>();
//	public Map<String, List<String>> errorsToReturn = new HashMap<>();
    private Map<String, String> warnings = new HashMap<>();

    public void addError(String attribute, String message) {
        errors.computeIfAbsent(attribute, k -> new ArrayList<>());
        errors.get(attribute).add(message);

//        errorsToReturn.computeIfAbsent(attribute, k -> new ArrayList<>());
//        errorsToReturn.get(attribute).add(message);

//        writeIntoErrorLog(attribute, message);
    }

   private void writeIntoErrorLog(String attribute, String message) {

		File file = new File("/Users/tapplent/Documents/ValidationError.txt");
		Date date= new Date();
		Timestamp timeStamp = new Timestamp(date.getTime());
		 
		// if file doesnt exists, then create it
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
        LOG.error(attribute + " : " + message);
		try {
			FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(timeStamp + " : " + attribute+" : " + message + "\n");
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

 public boolean hasError() {
        return !errors.isEmpty();
    }

    public boolean hasWarning() {
        return !warnings.isEmpty();
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, List<String>> errors) {
        this.errors = errors;
    }

    public Map<String, String> getWarnings() {
        return warnings;
    }

    public void setWarnings(Map<String, String> warnings) {
        this.warnings = warnings;
    }
    
    public void logValidationErrors() {
		for(Entry<String, List<String>> error : getErrors().entrySet()){
			LOG.error(error.getKey() + " : " + error.getValue());
		}
//		errors.clear();
	}

//    public Map<String, List<String>> getErrorsToReturn() {
//        return errorsToReturn;
//    }

//    public void setErrorsToReturn(Map<String, List<String>> errorsToReturn) {
//        this.errorsToReturn = errorsToReturn;
//    }

    public void clear(){
    	errors.clear();
    }

//    public void clearErrorsToReturn(){
//    	errorsToReturn.clear();
//    }
}
