package com.tapplent.platformutility.common.util.dao;

import com.tapplent.platformutility.common.util.valueObject.ErrorLogVO;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Shubham Patodi on 05/08/16.
 */
public interface UtilityDAO {
    void insertError(ErrorLogVO logVO) throws SQLException;
    List<ErrorLogVO> getErrorList(boolean sendOnlyUnresolved);

}
