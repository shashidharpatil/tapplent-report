package com.tapplent.platformutility.workflow.structure;

public class GoalThreshold {
    private String versionId;
    private String goalThresholdPkId;
    private String goalEventType;
    private String goalGroup;
    private String goalCategory;
    private String goalType;
    private String goalThresholdName;
    private String goalThresholdIcon;
    private Integer minNumberGoals;
    private Integer maxNumberGoals;
    private Double minGoalWeightEach;
    private Double maxGoalWeightEach;
    private Double minGoalWeightTotal;
    private Double maxGoalWeightTotal;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId;
    }

    public String getGoalThresholdPkId() {
        return goalThresholdPkId;
    }

    public void setGoalThresholdPkId(String goalThresholdPkId) {
        this.goalThresholdPkId = goalThresholdPkId;
    }

    public String getGoalEventType() {
        return goalEventType;
    }

    public void setGoalEventType(String goalEventType) {
        this.goalEventType = goalEventType;
    }

    public String getGoalGroup() {
        return goalGroup;
    }

    public void setGoalGroup(String goalGroup) {
        this.goalGroup = goalGroup;
    }

    public String getGoalCategory() {
        return goalCategory;
    }

    public void setGoalCategory(String goalCategory) {
        this.goalCategory = goalCategory;
    }

    public String getGoalType() {
        return goalType;
    }

    public void setGoalType(String goalType) {
        this.goalType = goalType;
    }

    public String getGoalThresholdName() {
        return goalThresholdName;
    }

    public void setGoalThresholdName(String goalThresholdName) {
        this.goalThresholdName = goalThresholdName;
    }

    public String getGoalThresholdIcon() {
        return goalThresholdIcon;
    }

    public void setGoalThresholdIcon(String goalThresholdIcon) {
        this.goalThresholdIcon = goalThresholdIcon;
    }

    public Integer getMinNumberGoals() {
        return minNumberGoals;
    }

    public void setMinNumberGoals(Integer minNumberGoals) {
        this.minNumberGoals = minNumberGoals;
    }

    public Integer getMaxNumberGoals() {
        return maxNumberGoals;
    }

    public void setMaxNumberGoals(Integer maxNumberGoals) {
        this.maxNumberGoals = maxNumberGoals;
    }

    public Double getMinGoalWeightEach() {
        return minGoalWeightEach;
    }

    public void setMinGoalWeightEach(Double minGoalWeightEach) {
        this.minGoalWeightEach = minGoalWeightEach;
    }

    public Double getMaxGoalWeightEach() {
        return maxGoalWeightEach;
    }

    public void setMaxGoalWeightEach(Double maxGoalWeightEach) {
        this.maxGoalWeightEach = maxGoalWeightEach;
    }

    public Double getMinGoalWeightTotal() {
        return minGoalWeightTotal;
    }

    public void setMinGoalWeightTotal(Double minGoalWeightTotal) {
        this.minGoalWeightTotal = minGoalWeightTotal;
    }

    public Double getMaxGoalWeightTotal() {
        return maxGoalWeightTotal;
    }

    public void setMaxGoalWeightTotal(Double maxGoalWeightTotal) {
        this.maxGoalWeightTotal = maxGoalWeightTotal;
    }
}
