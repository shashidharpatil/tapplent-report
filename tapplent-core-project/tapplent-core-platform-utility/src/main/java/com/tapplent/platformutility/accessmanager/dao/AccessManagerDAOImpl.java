package com.tapplent.platformutility.accessmanager.dao;

import com.tapplent.platformutility.accessmanager.structure.*;
import com.tapplent.platformutility.common.util.Util;
import com.tapplent.platformutility.etl.valueObject.ETLMetaHelperVO;
import com.tapplent.platformutility.groupresolver.valueobject.GroupVO;
import com.tapplent.platformutility.layout.valueObject.DOADetails;
import com.tapplent.platformutility.metadata.structure.EntityAttributeMetadata;
import com.tapplent.platformutility.persistence.dao.TapplentBaseDAO;
import com.tapplent.platformutility.person.structure.PersonGroup;
import com.tapplent.tenantresolver.tenant.TenantContextHolder;
import org.springframework.beans.BeanUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Shubham Patodi on 08/08/16.
 */
public class AccessManagerDAOImpl extends TapplentBaseDAO implements AccessManagerDAO {
    @Override
    public Map<String, ResolvedRowCondition> getRowConditionsForRecords(List<String> dbPrimaryKeys, String whoPersonId) {
        List<ConditionValueObject> subresult = new ArrayList<>();
        String SQL = "";
        List<Object> parameters = new ArrayList<Object>();
        ResultSet rs = executeSQL(SQL, parameters);
        try {
            while(rs.next()){
                ConditionValueObject conditionValueObject = new ConditionValueObject();
                /*
                    private String resolvedRowPkId;
                    private String whoPersonId;
                    private String domainObjectId;
                    private String dbPrimaryKeyId;
                    private String functionalPrimaryKeyid;
                    private String subjectUserId;
                    private boolean isCreateCurrentPermittedRow;
                    private boolean isCreateHistoryPermittedRow;
                    private boolean isCreateFuturePermittedRow;
                    private boolean isReadCurrentPermittedRow;
                    private boolean isReadHistoryPermittedRow;
                    private boolean isReadFuturePermittedRow;
                    private boolean isUpdateCurrentPermittedRow;
                    private boolean isUpdateHistoryPermittedRow;
                    private boolean isUpdateFuturePermittedRow;
                    private boolean isDeleteCurrentPermittedRow;
                    private boolean isDeleteHistoryPermittedRow;
                    private boolean isDeleteFuturePermittedRow;
                */
        conditionValueObject.setResolvedRowPkId(rs.getString(""));
        conditionValueObject.setWhoPersonId(rs.getString(""));
        conditionValueObject.setDomainObjectId(rs.getString(""));
        conditionValueObject.setDbPrimaryKeyId(rs.getString(""));
        conditionValueObject.setFunctionalPrimaryKeyid(rs.getString(""));
        conditionValueObject.setSubjectUserId(rs.getString(""));
        conditionValueObject.setCreateCurrentPermittedRow(rs.getBoolean(""));
        conditionValueObject.setCreateHistoryPermittedRow(rs.getBoolean(""));
        conditionValueObject.setCreateFuturePermittedRow(rs.getBoolean(""));
        conditionValueObject.setReadCurrentPermittedRow(rs.getBoolean(""));
        conditionValueObject.setReadHistoryPermittedRow(rs.getBoolean(""));
        conditionValueObject.setReadFuturePermittedRow(rs.getBoolean(""));
        conditionValueObject.setUpdateCurrentPermittedRow(rs.getBoolean(""));
        conditionValueObject.setUpdateHistoryPermittedRow(rs.getBoolean(""));
        conditionValueObject.setUpdateFuturePermittedRow(rs.getBoolean(""));
        conditionValueObject.setDeleteCurrentPermittedRow(rs.getBoolean(""));
        conditionValueObject.setDeleteHistoryPermittedRow(rs.getBoolean(""));
        conditionValueObject.setDeleteFuturePermittedRow(rs.getBoolean(""));

    /*
    private String resolvedColumnPkId;
    private String resolvedRowFkId;
    private String domainObjectAttributeId;
    private boolean isCreateCurrentPermittedColumn;
    private boolean isCreateHistoryPermittedColumn;
    private boolean isCreateFuturePermittedColumn;
    private boolean isReadCurrentPermittedColumn;
    private boolean isReadHistoryPermittedColumn;
    private boolean isReadFuturePermittedColumn;
    private boolean isUpdateCurrentPermittedColumn;
    private boolean isUpdateHistoryPermittedColumn;
    private boolean isUpdateFuturePermittedColumn;
    private boolean isDeleteCurrentPermittedColumn;
    private boolean isDeleteHistoryPermittedColumn;
    private boolean isDeleteFuturePermittedColumn;
                 */
                conditionValueObject.setResolvedColumnPkId(rs.getString(""));
                conditionValueObject.setResolvedRowFkId(rs.getString(""));
                conditionValueObject.setDomainObjectAttributeId(rs.getString(""));
                conditionValueObject.setCreateCurrentPermittedColumn(rs.getBoolean(""));
                conditionValueObject.setCreateHistoryPermittedColumn(rs.getBoolean(""));
                conditionValueObject.setCreateFuturePermittedColumn(rs.getBoolean(""));
                conditionValueObject.setReadCurrentPermittedColumn(rs.getBoolean(""));
                conditionValueObject.setReadHistoryPermittedColumn(rs.getBoolean(""));
                conditionValueObject.setReadFuturePermittedColumn(rs.getBoolean(""));
                conditionValueObject.setUpdateCurrentPermittedColumn(rs.getBoolean(""));
                conditionValueObject.setUpdateHistoryPermittedColumn(rs.getBoolean(""));
                conditionValueObject.setUpdateFuturePermittedColumn(rs.getBoolean(""));
                conditionValueObject.setDeleteCurrentPermittedColumn(rs.getBoolean(""));
                conditionValueObject.setDeleteHistoryPermittedColumn(rs.getBoolean(""));
                conditionValueObject.setDeleteFuturePermittedColumn(rs.getBoolean(""));
                subresult.add(conditionValueObject);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        /* Now once we have the access condition break these to Resolved Row and Column condition.
        * Make a map of String(ID) to resolved row condition while looping here
        * */
        Map<String, ResolvedRowCondition> idToRowCondn = new HashMap<>();
        for(ConditionValueObject valueObject : subresult){
            /* look at each row get the primary key of row condition check if it is already present in the map add the column condition if not add the row and column condition*/
            String rowPkId = valueObject.getResolvedRowPkId();
            if(idToRowCondn.containsKey(rowPkId)){
                ResolvedRowCondition rowCondition = idToRowCondn.get(rowPkId);
                /* Here column condition needs to be added to the row condition */
                ResolvedColumnCondition columnCondition = new ResolvedColumnCondition();
                BeanUtils.copyProperties(valueObject, columnCondition);
                rowCondition.getDoaToColumnConditionMap().put(columnCondition.getDoa(),columnCondition);
            }else{
                ResolvedRowCondition rowCondition = new ResolvedRowCondition();
                BeanUtils.copyProperties(valueObject, rowCondition);
                idToRowCondn.put(valueObject.getResolvedRowPkId(), rowCondition);
                ResolvedColumnCondition columnCondition = new ResolvedColumnCondition();
                BeanUtils.copyProperties(valueObject, columnCondition);
                rowCondition.getDoaToColumnConditionMap().put(columnCondition.getDoa(),columnCondition);
            }
        }
        List<ResolvedRowCondition> result = new ArrayList<ResolvedRowCondition>(idToRowCondn.values());
        return idToRowCondn;
        /* Now we have for each row the conditions the user has we need to send it to client */
    }


    @Override
    public void setColumnConditions(Map<String, RowCondition> rowConditionsSatified) {
        if (rowConditionsSatified == null || rowConditionsSatified.isEmpty())
            return;
        else {
            StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_ADM_AM_COL WHERE AM_ROW_CONDN_FK_ID IN (");
            List<Object> parameters = new ArrayList<>();
            for (Map.Entry<String, RowCondition> entry : rowConditionsSatified.entrySet()){
                sql.append("x?,");
                parameters.add(Util.remove0x(entry.getValue().getRowCondnPkId()));
            }
            sql.replace(sql.length()-1, sql.length(), ");");
            ResultSet rs = executeSQL(sql.toString(), parameters);
            try {
                while (rs.next()){
                    AMColCondition amColCondition = new AMColCondition();
                    /*
                    private String amColPkId;
                    private String amRowCondnFkId;
                    private String whatDoaFkId;
                    private Date startDate;
                    private Date endDate;
                    private boolean isPermissionEnds;
                    private boolean isCreateCurrentPermitted;
                    private boolean isCreateHistoryPermitted;
                    private boolean isCreateFuturePermitted;
                    private boolean isReadCurrentPermitted;
                    private boolean isReadHistoryPermitted;
                    private boolean isReadFuturePermitted;
                    private boolean isUpdateCurrentPermitted;
                    private boolean isUpdateHistoryPermitted;
                    private boolean isUpdateFuturePermitted;
                    private boolean isDeleteCurrentPermitted;
                    private boolean isDeleteHistoryPermitted;
                    private boolean isDeleteFuturePermitted;
                     */
                    amColCondition.setAmColPkId(convertByteToString(rs.getBytes("AM_COL_PK_ID")));
                    amColCondition.setAmRowCondnFkId(convertByteToString(rs.getBytes("AM_ROW_CONDN_FK_ID")));
                    amColCondition.setWhatDoaFkId(rs.getString("WHAT_DOA_CODE_FK_ID"));
                    amColCondition.setStartDate(rs.getTimestamp("START_DATETIME"));
                    amColCondition.setEndDate(rs.getTimestamp("END_DATETIME"));
                    amColCondition.setPermissionEnds(rs.getBoolean("IS_PERM_NEVER_ENDS"));
                    amColCondition.setCreateCurrentPermitted(rs.getBoolean("IS_CREATE_CURRENT_PERMITTED"));
                    amColCondition.setCreateHistoryPermitted(rs.getBoolean("IS_CREATE_HISTORY_PERMITTED"));
                    amColCondition.setCreateFuturePermitted(rs.getBoolean("IS_CREATE_FUTURE_PERMITTED"));
                    amColCondition.setReadCurrentPermitted(rs.getBoolean("IS_READ_CURRENT_PERMITTED"));
                    amColCondition.setReadHistoryPermitted(rs.getBoolean("IS_READ_HISTORY_PERMITTED"));
                    amColCondition.setReadFuturePermitted(rs.getBoolean("IS_READ_FUTURE_PERMITTED"));
                    amColCondition.setUpdateCurrentPermitted(rs.getBoolean("IS_UPDATE_CURRENT_PERMITTED"));
                    amColCondition.setUpdateHistoryPermitted(rs.getBoolean("IS_UPDATE_HISTORY_PERMITTED"));
                    amColCondition.setUpdateFuturePermitted(rs.getBoolean("IS_UPDATE_FUTURE_PERMITTED"));
                    if (rowConditionsSatified.get(amColCondition.getAmRowCondnFkId()).getColumnConditionList() == null) {
                        List<AMColCondition> amColConditions = new ArrayList<>();
                        amColConditions.add(amColCondition);
                        rowConditionsSatified.get(amColCondition.getAmRowCondnFkId()).setColumnConditionList(amColConditions);
                    }
                    else rowConditionsSatified.get(amColCondition.getAmRowCondnFkId()).getColumnConditionList().add(amColCondition);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void deleteExistingPermissionsFromRecord(String whatDBPrimaryKey, String rowIntermediateTable, String rowResolvedTable, List<String> rowConditionsToDelete) throws SQLException {
        deleteExistingPermissionsFromIntermediateRecord(whatDBPrimaryKey, rowIntermediateTable, rowConditionsToDelete);
        deleteExistingPermissionsFromResolvedRecord(whatDBPrimaryKey, rowResolvedTable);
    }

    @Override
    public Map<String, AMRowIntermediate> getRowIntermediateByDependencykey(EntityAttributeMetadata dependencyDOA, Object dependencyKey) {
        if (dependencyKey == null)
            return null;
        String sql = null;
        if (dependencyDOA.getDbDataTypeCode().equals("T_ID"))
            sql = "SELECT  FROM T_PFM_IEU_AM_ROW_INTERMEDIATE WHERE WHAT_DB_PRIMARY_KEY_ID = x?";
//        else
//            sql = "SELECT * FROM T_PFM_IEU_AM_ROW_INTERMEDIATE WHERE WHAT_DB_PRIMARY_KEY_ID = ?";
        List<Object> parameters = new ArrayList<>();
        parameters.add(dependencyKey);
        ResultSet rs = executeSQL(sql, parameters);
        Map<String, AMRowIntermediate> rowIntermediateMap = new HashMap<>();
        try {
            while (rs.next()) {
                AMRowIntermediate rowIntermediate = new AMRowIntermediate();
                rowIntermediate.setAmRowIntermediatePkId(convertByteToString(rs.getBytes("AM_ROW_INTERMEDIATE_PK_ID")));
                rowIntermediate.setWhoPersonFkId(convertByteToString(rs.getBytes("WHO_PERSON_FK_ID")));
                rowIntermediate.setWhoseSubjectUserPersonFkId(convertByteToString(rs.getBytes("WHOSE_SUBJECT_USER_PERSON_FK_ID")));
                rowIntermediate.setAmRowCondnFkId(convertByteToString(rs.getBytes("AM_ROW_CONDN_FK_ID")));
                rowIntermediateMap.put(rowIntermediate.getAmRowIntermediatePkId(), rowIntermediate);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return rowIntermediateMap;
    }

    @Override
    public Map<String, RowCondition> getRowConditions(Map<String, AMRowIntermediate> parentRowIntermediateConditions, String domainObjectCode) {
        if (parentRowIntermediateConditions == null || parentRowIntermediateConditions.isEmpty())
            return null;
        StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_ADM_AM_ROW_CONDN WHERE WHAT_DO_CODE_FK_ID = ? AND  PARENT_ROW_CONDN_FK_ID IN (");
        List<Object> parameters = new ArrayList<>();
        parameters.add(domainObjectCode);
        for (Map.Entry<String, AMRowIntermediate> rowIntermediate : parentRowIntermediateConditions.entrySet()){
            sql.append("x?, ");
            parameters.add(rowIntermediate.getValue().getAmRowCondnFkId());
        }
        sql.replace(sql.length() - 2, sql.length(), ");");
        ResultSet rs = executeSQL(sql.toString(), parameters);
        Map<String, RowCondition> rowConditions = new HashMap<>();
        try {
            while (rs.next()){
                getRowConditionsFromReseltSet(rs, rowConditions);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return rowConditions;
    }

    @Override
    public Map<String, List<AMWhatFilterCriteria>> getWhatFilterCriteriaByRowCondition(Map<String, RowCondition> rowConditionMap) {
        if (rowConditionMap == null || rowConditionMap.isEmpty())
            return null;
        List<Object> parameters = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_ADM_AM_WHAT_FILTER_CRITERIA WHERE AM_ROW_CONDN_FK_ID IN (");
        for (Map.Entry<String, RowCondition> rowConditionEntry : rowConditionMap.entrySet()){
            sql.append("?, ");
            parameters.add(Util.convertStringIdToByteId(rowConditionEntry.getKey()));
        }
        sql.replace(sql.length()-2, sql.length(),");");
        ResultSet rs = executeSQL(sql.toString(), parameters);
        Map<String, List<AMWhatFilterCriteria>> rowCndnIdToWhatCriteriaListMap = new HashMap<>();
        try{
            while (rs.next()){
                AMWhatFilterCriteria amWhatFilterCriteria = new AMWhatFilterCriteria();
                amWhatFilterCriteria.setWhatFilterCriteriaPkId(convertByteToString(rs.getBytes("WHAT_FILTER_CRITERIA_PK_ID")));
                amWhatFilterCriteria.setAmRowCondnFkId(convertByteToString(rs.getBytes("AM_ROW_CONDN_FK_ID")));
                amWhatFilterCriteria.setControlAttributePathExpn(rs.getString("CONTROL_ATTRIBUTE_PATH_EXPN"));
                amWhatFilterCriteria.setOperFkId(rs.getString("OPER_CODE_FK_ID"));
                amWhatFilterCriteria.setValueExpn(rs.getString("VALUE_EXPN"));
                if (rowCndnIdToWhatCriteriaListMap.containsKey(amWhatFilterCriteria.getAmRowCondnFkId())){
                    rowCndnIdToWhatCriteriaListMap.get(amWhatFilterCriteria.getAmRowCondnFkId()).add(amWhatFilterCriteria);
                }else {
                    List<AMWhatFilterCriteria> amWhatFilterCriteriaList = new ArrayList<>();
                    amWhatFilterCriteriaList.add(amWhatFilterCriteria);
                    rowCndnIdToWhatCriteriaListMap.put(amWhatFilterCriteria.getAmRowCondnFkId(), amWhatFilterCriteriaList);
                }
            }
            return rowCndnIdToWhatCriteriaListMap;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Map<String, Map<String, GroupVO>> getWhoGroup(Map<String, RowCondition> rowConditionQualifiedMap) {
        StringBuilder sql = new StringBuilder("SELECT AM_ROW_CONDN_PK_ID, WHO_GROUP_FK_ID\n" +
                " FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                "   INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "   INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                " WHERE AM_ROW_CONDN_PK_ID IN (");
        List<Object> parameters = new ArrayList<>();
        for (Map.Entry<String, RowCondition> rowCondnEntry : rowConditionQualifiedMap.entrySet()){
            sql.append("?, ");
            parameters.add(Util.convertStringIdToByteId(rowCondnEntry.getKey()));
        }
        sql.replace(sql.length()-2, sql.length(),");");
        ResultSet rs = executeSQL(sql.toString(), parameters);
        Map<String, Map<String, GroupVO>> rowCondnWhoGroupMap = new HashMap<>();
        try {
            String rowCondnId = null;
            String whoGroupId = null;
            while (rs.next()){
                rowCondnId = convertByteToString(rs.getBytes("AM_ROW_CONDN_PK_ID"));
                whoGroupId = convertByteToString(rs.getBytes("WHO_GROUP_FK_ID"));
                GroupVO groupVO = new GroupVO();
                groupVO.setGroupID(whoGroupId);
                if (rowCondnWhoGroupMap.containsKey(rowCondnId)){
                    rowCondnWhoGroupMap.get(rowCondnId).put(whoGroupId, groupVO);
                }
                else {
                    Map<String, GroupVO> groupVOMap = new HashMap<>();
                    groupVOMap.put(whoGroupId, groupVO);
                    rowCondnWhoGroupMap.put(rowCondnId, groupVOMap);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowCondnWhoGroupMap;
    }

    @Override
    public List<String> getPersonIdListForWhoGroup(String whoGroupId) {
        String sql = "SELECT DISTINCT(GROUP_MEMBER_PERSON_FK_ID) FROM T_PFM_IEU_GROUP_MEMBER WHERE PFM_GROUP_FK_ID = ?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(whoGroupId));
        ResultSet rs = executeSQL(sql, parameters);
        List<String> personList = new ArrayList<>();
        try {
            while (rs.next()){
                personList.add(convertByteToString(rs.getBytes("GROUP_MEMBER_PERSON_FK_ID")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personList;
    }

    @Override
    public void populateRowIntermediateForBasePE(Map<String, List<String>> rowConditionToPersonId, String whatDbPrimaryKey, String whoseSubjectUserPersonId, String currentTimestampString) throws SQLException {
        StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_AM_ROW_INTERMEDIATE (AM_ROW_INTERMEDIATE_PK_ID, WHO_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, WHOSE_SUBJECT_USER_PERSON_FK_ID, AM_ROW_CONDN_FK_ID, IS_DELETED, CREATED_DATETIME) VALUES (\n");
        List<Object> parameters = new ArrayList<>();
        for (Map.Entry<String, List<String>> rowCndnPersonList : rowConditionToPersonId.entrySet()){
            for (String personId : rowCndnPersonList.getValue()){
                sql.append("ordered_uuid(UUID()), x?, x?, x?, x?, FALSE, "+currentTimestampString+"), ");
                parameters.add(personId);
                parameters.add(whatDbPrimaryKey);
                parameters.add(whoseSubjectUserPersonId);
                parameters.add(rowCndnPersonList.getKey());
            }
        }
        sql.replace(sql.length() - 2, sql.length(), ";");
        executeUpdate(sql.toString(), parameters);
        return;
    }

    @Override
    public void populateRowResolvedTable(String currentTimestampString) {
        String sql = "SELECT * FROM ";
    }

    @Override
    public Map<String, RowCondition> getRowConditions(String mtPE) {
        String sql = "SELECT * FROM T_PFM_ADM_AM_ROW_CONDN RC INNER JOIN T_PFM_ADM_AM_PROCESS ON RC.AM_ACCESS_PROCESS_FK_ID = T_PFM_ADM_AM_PROCESS.AM_PROCESS_PK_ID\n" +
                "INNER JOIN T_PFM_ADM_AM_DEFN D ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = D.AM_DEFN_PK_ID " +
                "INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID WHERE UTC_TIMESTAMP BETWEEN START_DATETIME AND END_DATETIME OR IS_PERM_NEVER_ENDS = TRUE AND WHAT_MT_PE_CODE_FK_ID = ?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(mtPE);
        Map<String, RowCondition> rowConditions = new HashMap<>();
        ResultSet rs = executeSQL(sql.toString(), parameters);
        getRowConditionsFromReseltSet(rs, rowConditions);
        return rowConditions;
    }

    @Override
    public ETLMetaHelperVO getMetaByDoName(String doName) {
        ETLMetaHelperVO result = null;
        String SQL = "SELECT T_MT_ADM_DOMAIN_OBJECT.DB_TABLE_NAME_TXT,DOMAIN_OBJECT_CODE_PK_ID,DB_COLUMN_NAME_TXT, DB_DATA_TYPE_CODE_FK_ID, SUBJECT_PERSON_DOA_FULL_PATH_EXPN   FROM T_MT_ADM_DOMAIN_OBJECT INNER JOIN T_MT_ADM_DO_ATTRIBUTE ON IS_DB_PRIMARY_KEY = 1 AND DOMAIN_OBJECT_CODE_FK_ID = DOMAIN_OBJECT_CODE_PK_ID WHERE DOMAIN_OBJECT_CODE_PK_ID=?";
        List<Object> parameters = new ArrayList<>();
        parameters.add(doName);
        ResultSet rs = executeSQL(SQL, parameters);
        try{
            if(rs.next()) {
                String tableName = rs.getString(1);
                String doCode = rs.getString(2);
               // Boolean isAccessControlGovernend = rs.getBoolean(3);
                String dbPKCol = rs.getString(3);
                String dbDatatype = rs.getString(4);
                String subjectUserPath = rs.getString(5);
                result = new ETLMetaHelperVO(doCode, tableName, dbPKCol, dbDatatype);
                result.setSubjectUserPath(subjectUserPath);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        if(result!=null){
            String doaSQL = "SELECT DOMAIN_OBJECT_ATTRIBUTE_CODE_PK_ID FROM T_MT_ADM_DO_ATTRIBUTE WHERE DOMAIN_OBJECT_CODE_FK_ID = ?;";
            List<String> doaCodeList = new ArrayList<>();
            ResultSet rsDOA = executeSQL(doaSQL, parameters);
            try{
                while (rsDOA.next()){
                    String doaCode = rsDOA.getString(1);
                    doaCodeList.add(doaCode);
                }
                result.setDoAttrCodeList(doaCodeList);
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public DOADetails getDOADetailsByDOAName(String doaName) {
        DOADetails doaDetails = new DOADetails();
        String SQL = "SELECT DB_COLUMN_NAME_TXT,DB_DATA_TYPE_CODE_FK_ID,CONTAINER_BLOB_DB_COLUMN_NAME_TXT,DB_TABLE_NAME_TXT,DOMAIN_OBJECT_CODE_FK_ID, TO_DB_TABLE_NAME_TXT,TO_DB_COLUMN_NAME_TXT  FROM T_MT_ADM_DO_ATTRIBUTE WHERE DOMAIN_OBJECT_ATTRIBUTE_CODE_PK_ID = ?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(doaName);
        ResultSet rs = executeSQL(SQL, parameters);
        try{
            rs.next();
            doaDetails.setColumnName(rs.getString("DB_COLUMN_NAME_TXT"));
            doaDetails.setContainerBlobName(rs.getString("CONTAINER_BLOB_DB_COLUMN_NAME_TXT"));
            doaDetails.setDbDataType(rs.getString("DB_DATA_TYPE_CODE_FK_ID"));
            doaDetails.setTableName(rs.getString("DB_TABLE_NAME_TXT"));
            doaDetails.setDoCode(rs.getString("DOMAIN_OBJECT_CODE_FK_ID"));
            doaDetails.setToTableName("TO_DB_TABLE_NAME_TXT");
            doaDetails.setToColumnName("TO_DB_COLUMN_NAME_TXT");
            doaDetails.setDoaName(doaName);
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return doaDetails;
    }

    @Override
    public void populateRowIntermediateForBasePE(Map<String, RowCondition> rowConditionQualified, String whatDbPrimaryKey, Object dependencyKey, String whoseSubjectUserId) throws SQLException {
        StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_AM_ROW_INTERMEDIATE (WHO_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, WHOSE_SUBJECT_USER_PERSON_FK_ID, AM_ROW_CONDN_FK_ID, IS_DELETED, CREATED_DATETIME)\n" +
                "(SELECT DISTINCT GROUP_MEMBER_PERSON_FK_ID, ?, ?, AM_ROW_CONDN_PK_ID, 0, UTC_TIMESTAMP  from T_PFM_ADM_AM_ROW_CONDN\n" +
                "      INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "      INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                "      INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_FK_ID = WHO_GROUP_FK_ID\n" +
                "      INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
                "    WHERE AM_ROW_CONDN_PK_ID IN (");
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(whatDbPrimaryKey));
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId((String) dependencyKey));
        int i = 3;
        for (Map.Entry<String, RowCondition> entry : rowConditionQualified.entrySet()) {
            sql.append("?,");
            parameters.add(Util.convertStringIdToByteId(entry.getKey()));
        }
        sql.replace(sql.length()-1, sql.length(), ") ");
        sql.append("AND (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
        "      SELECT node.RELATIONSHIP_TYPE_CODE_PK_ID, (select IF(false, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE))) ApplicableToAllAbove\n" +
        "                FROM (SELECT\n" +
        "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
        "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
        "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
        "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID\n" +
        "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
        "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
        "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
        "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
        "                      (SELECT\n" +
        "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
        "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
        "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
        "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
        "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
        "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
        "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
        "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
        "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
        "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
        "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
        "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
        "                  AND node.PERSON_DEP_FK_ID = ?\n" +
        "                  AND parent.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
        "                  GROUP BY parent.PERSON_DEP_FK_ID)\n" +
        "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
        "      SELECT node.INV_RELTN_TYPE_CODE_FK_ID, IF(IS_ALL_HIERARCHY, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE)) ApplicableToAllAbove\n" +
        "                FROM (SELECT\n" +
        "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
        "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
        "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
        "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID,\n" +
        "                         TPLJRT.INV_RELTN_TYPE_CODE_FK_ID\n" +
        "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
        "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
        "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
        "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
        "                      (SELECT\n" +
        "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
        "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
        "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
        "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
        "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
        "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
        "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
        "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
        "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
        "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
        "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
        "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
        "                  AND node.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
        "                  AND parent.PERSON_DEP_FK_ID = ?\n" +
        "                  GROUP BY parent.PERSON_DEP_FK_ID\n" +
        "                  ORDER BY ApplicableToAllAbove DESC)\n" +
        "          OR (RELATIONSHIP_TYPE_CODE_FK_ID) IN (\n" +
        "                  SELECT DISTINCT 'PEERS'\n" +
        "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
        "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
        "            WHERE RELATED_PERSON_FK_ID = (SELECT RELATED_PERSON_FK_ID\n" +
        "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
        "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
        "            WHERE PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER') AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER' AND PERSON_DEP_FK_ID <> T_PFM_IEU_GROUP_MEMBER.GROUP_MEMBER_PERSON_FK_ID AND PERSON_DEP_FK_ID = ?)\n" +
        "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
        "              SELECT RELATIONSHIP_TYPE_CODE_FK_ID, 1\n" +
        "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
        "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
        "            WHERE PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATED_PERSON_FK_ID = ? AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
        "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
        "              SELECT INV_RELTN_TYPE_CODE_FK_ID, 1\n" +
        "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
        "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE ON RELATIONSHIP_TYPE_CODE_PK_ID = RELATIONSHIP_TYPE_CODE_FK_ID\n" +
        "            WHERE PERSON_DEP_FK_ID = ? AND RELATED_PERSON_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
        "          OR RELATIONSHIP_TYPE_CODE_FK_ID = (SELECT IF(GROUP_MEMBER_PERSON_FK_ID = ?, 'SELF', 'ALL_BUT_SELF')));");
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        executeUpdate(sql.toString(), parameters);
        return;
    }

    @Override
    public void populateRowIntermediateForChildPE(Map<String, RowCondition> rowConditionQualified, String whatDbPrimaryKey, Object dependencyKey, String whoseSubjectUserId) throws SQLException {
        StringBuilder sql = new StringBuilder("INSERT INTO T_PFM_IEU_AM_ROW_INTERMEDIATE (WHO_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, WHOSE_SUBJECT_USER_PERSON_FK_ID, AM_ROW_CONDN_FK_ID, IS_DELETED, CREATED_DATETIME)\n" +
                "(SELECT DISTINCT GROUP_MEMBER_PERSON_FK_ID, ?, ?, AM_ROW_CONDN_PK_ID, 0, UTC_TIMESTAMP  from T_PFM_ADM_AM_ROW_CONDN\n" +
                "      INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "      INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                "      INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_MEMBER_PK_ID = WHO_GROUP_FK_ID\n" +
                "      INNER JOIN T_PFM_IEU_AM_ROW_INTERMEDIATE ON WHAT_DB_PRIMARY_KEY_ID = ? AND WHO_PERSON_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n"+
                "      INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
                "    WHERE AM_ROW_CONDN_PK_ID IN (");
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(whatDbPrimaryKey));
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId((String) dependencyKey));
        int i = 3;
        for (Map.Entry<String, RowCondition> entry : rowConditionQualified.entrySet()) {
            sql.append("?,");
            parameters.add(Util.convertStringIdToByteId(entry.getKey()));
        }
        sql.replace(sql.length()-1, sql.length(), ") ");
        sql.append("AND (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                "      SELECT node.RELATIONSHIP_TYPE_CODE_PK_ID, (select IF(false, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE))) ApplicableToAllAbove\n" +
                "                FROM (SELECT\n" +
                "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                "                      (SELECT\n" +
                "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "                  AND node.PERSON_DEP_FK_ID = ?\n" +
                "                  AND parent.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                "                  GROUP BY parent.PERSON_DEP_FK_ID)\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                "      SELECT node.INV_RELTN_TYPE_CODE_FK_ID, IF(IS_ALL_HIERARCHY, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE)) ApplicableToAllAbove\n" +
                "                FROM (SELECT\n" +
                "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID,\n" +
                "                         TPLJRT.INV_RELTN_TYPE_CODE_FK_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                "                      (SELECT\n" +
                "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "                  AND node.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                "                  AND parent.PERSON_DEP_FK_ID = ?\n" +
                "                  GROUP BY parent.PERSON_DEP_FK_ID\n" +
                "                  ORDER BY ApplicableToAllAbove DESC)\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID) IN (\n" +
                "                  SELECT DISTINCT 'PEERS'\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "            WHERE RELATED_PERSON_FK_ID = (SELECT RELATED_PERSON_FK_ID\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "            WHERE PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER') AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER' AND PERSON_DEP_FK_ID <> T_PFM_IEU_GROUP_MEMBER.GROUP_MEMBER_PERSON_FK_ID AND PERSON_DEP_FK_ID = ?)\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
                "              SELECT RELATIONSHIP_TYPE_CODE_FK_ID, 1\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "            WHERE PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATED_PERSON_FK_ID = ? AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
                "              SELECT INV_RELTN_TYPE_CODE_FK_ID, 1\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE ON RELATIONSHIP_TYPE_CODE_PK_ID = RELATIONSHIP_TYPE_CODE_FK_ID\n" +
                "            WHERE PERSON_DEP_FK_ID = ? AND RELATED_PERSON_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                "          OR RELATIONSHIP_TYPE_CODE_FK_ID = (SELECT IF(GROUP_MEMBER_PERSON_FK_ID = ?, 'SELF', 'ALL_BUT_SELF')));");
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId(whoseSubjectUserId));
        executeUpdate(sql.toString(), parameters);
        return;
    }

    @Override
    public void populateRowIntermediateForBasePETable(Map<String, RowCondition> rowConditionMap, String subjectUserColumnName, String primaryKeyColumnName, ETLMetaHelperVO etlMetaHelperVO) throws SQLException {
        String tableName = etlMetaHelperVO.getTableName();
        List<Object> parameters = new ArrayList<>();
        StringBuilder sql = new StringBuilder("INSERT INTO "+etlMetaHelperVO.getIntermediateTableName()+" (WHO_PERSON_FK_ID,  WHOSE_SUBJECT_USER_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, AM_ROW_CONDN_FK_ID, IS_DELETED)\n" +
                "SELECT GROUP_MEMBER_PERSON_FK_ID,\n" +
                "  "+subjectUserColumnName+",\n" +
                "  "+primaryKeyColumnName+", AM_ROW_CONDN_PK_ID, 0\n" +
                "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                "      INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "      INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                "      INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_FK_ID = WHO_GROUP_FK_ID\n" +
                "      INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
                "      JOIN "+tableName+"\n" +
                "    WHERE AM_ROW_CONDN_PK_ID IN (");
        for(Map.Entry<String, RowCondition> entry : rowConditionMap.entrySet()){
            sql.append("?,");
            parameters.add(Util.convertStringIdToByteId(entry.getKey()));
        }
        sql.replace(sql.length()-1, sql.length(), ")");
        sql.append(" AND (\n" +
                "          (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                "      SELECT node.RELATIONSHIP_TYPE_CODE_PK_ID, (select IF(false, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE))) ApplicableToAllAbove\n" +
                "                FROM (SELECT\n" +
                "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                "                      (SELECT\n" +
                "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "                  AND node.PERSON_DEP_FK_ID = "+subjectUserColumnName+"\n" +
                "                  AND parent.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                "                  GROUP BY parent.PERSON_DEP_FK_ID)\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                "      SELECT node.INV_RELTN_TYPE_CODE_FK_ID, IF(IS_ALL_HIERARCHY, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE)) ApplicableToAllAbove\n" +
                "                FROM (SELECT\n" +
                "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID,\n" +
                "                         TPLJRT.INV_RELTN_TYPE_CODE_FK_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                "                      (SELECT\n" +
                "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "                  AND node.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                "                  AND parent.PERSON_DEP_FK_ID = "+subjectUserColumnName+"\n" +
                "                  GROUP BY parent.PERSON_DEP_FK_ID\n" +
                "                  ORDER BY ApplicableToAllAbove DESC)\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID) IN (\n" +
                "                  SELECT DISTINCT 'PEERS'\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "            WHERE RELATED_PERSON_FK_ID = (SELECT RELATED_PERSON_FK_ID\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "            WHERE PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER') AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER' AND PERSON_DEP_FK_ID <> T_PFM_IEU_GROUP_MEMBER.GROUP_MEMBER_PERSON_FK_ID AND PERSON_DEP_FK_ID = "+subjectUserColumnName+")\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
                "              SELECT RELATIONSHIP_TYPE_CODE_FK_ID, 1\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "            WHERE PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATED_PERSON_FK_ID = "+subjectUserColumnName+" AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
                "              SELECT INV_RELTN_TYPE_CODE_FK_ID, 1\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE ON RELATIONSHIP_TYPE_CODE_PK_ID = RELATIONSHIP_TYPE_CODE_FK_ID\n" +
                "            WHERE PERSON_DEP_FK_ID = "+subjectUserColumnName+" AND RELATED_PERSON_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                "          OR RELATIONSHIP_TYPE_CODE_FK_ID = (SELECT IF(GROUP_MEMBER_PERSON_FK_ID = "+subjectUserColumnName+", 'SELF', 'ALL_BUT_SELF')));\n");
        executeUpdate(sql.toString(), parameters);
        return;
    }

    @Override
    public void populateRowIntermediateForChildPETable(Map<String, RowCondition> rowConditionMap, String subjectUserColumnName, String primaryKeyColumnName, ETLMetaHelperVO etlMetaHelperVO) throws SQLException {
        String tableName = etlMetaHelperVO.getTableName();
        List<Object> parameters = new ArrayList<>();
        StringBuilder sql = new StringBuilder("INSERT INTO "+etlMetaHelperVO.getIntermediateTableName()+" (WHO_PERSON_FK_ID,  WHOSE_SUBJECT_USER_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, AM_ROW_CONDN_FK_ID, IS_DELETED)\n" +
                "SELECT GROUP_MEMBER_PERSON_FK_ID,\n" +
                "  "+subjectUserColumnName+",\n" +
                "  "+primaryKeyColumnName+", AM_ROW_CONDN_PK_ID, 0\n" +
                "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                "      INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "      INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                "      INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_FK_ID = WHO_GROUP_FK_ID\n" +
                "      INNER JOIN "+etlMetaHelperVO.getIntermediateTableName()+" ON AM_ROW_CONDN_FK_ID = AM_ROW_CONDN_PK_ID AND WHO_PERSON_FK_ID = GROUP_MEMBER_PERSON_FK_ID" +
                "      INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
                "      JOIN "+tableName+"\n" +
                "    WHERE AM_ROW_CONDN_PK_ID IN (");
        for(Map.Entry<String, RowCondition> entry : rowConditionMap.entrySet()){
            sql.append("?,");
            parameters.add(Util.convertStringIdToByteId(entry.getKey()));
        }
        sql.replace(sql.length()-1, sql.length(), ")");
        sql.append(" AND (\n" +
                "          (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                "      SELECT node.RELATIONSHIP_TYPE_CODE_PK_ID, (select IF(false, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE))) ApplicableToAllAbove\n" +
                "                FROM (SELECT\n" +
                "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                "                      (SELECT\n" +
                "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "                  AND node.PERSON_DEP_FK_ID = "+subjectUserColumnName+"\n" +
                "                  AND parent.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                "                  GROUP BY parent.PERSON_DEP_FK_ID)\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                "      SELECT node.INV_RELTN_TYPE_CODE_FK_ID, IF(IS_ALL_HIERARCHY, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE)) ApplicableToAllAbove\n" +
                "                FROM (SELECT\n" +
                "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID,\n" +
                "                         TPLJRT.INV_RELTN_TYPE_CODE_FK_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                "                      (SELECT\n" +
                "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "                  AND node.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                "                  AND parent.PERSON_DEP_FK_ID = "+subjectUserColumnName+"\n" +
                "                  GROUP BY parent.PERSON_DEP_FK_ID\n" +
                "                  ORDER BY ApplicableToAllAbove DESC)\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID) IN (\n" +
                "                  SELECT DISTINCT 'PEERS'\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "            WHERE RELATED_PERSON_FK_ID = (SELECT RELATED_PERSON_FK_ID\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "            WHERE PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER') AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER' AND PERSON_DEP_FK_ID <> T_PFM_IEU_GROUP_MEMBER.GROUP_MEMBER_PERSON_FK_ID AND PERSON_DEP_FK_ID = "+subjectUserColumnName+")\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
                "              SELECT RELATIONSHIP_TYPE_CODE_FK_ID, 1\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "            WHERE PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATED_PERSON_FK_ID = "+subjectUserColumnName+" AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
                "              SELECT INV_RELTN_TYPE_CODE_FK_ID, 1\n" +
                "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE ON RELATIONSHIP_TYPE_CODE_PK_ID = RELATIONSHIP_TYPE_CODE_FK_ID\n" +
                "            WHERE PERSON_DEP_FK_ID = "+subjectUserColumnName+" AND RELATED_PERSON_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                "          OR RELATIONSHIP_TYPE_CODE_FK_ID = (SELECT IF(GROUP_MEMBER_PERSON_FK_ID = "+subjectUserColumnName+", 'SELF', 'ALL_BUT_SELF')));\n");
        executeUpdate(sql.toString(), parameters);
        return;
    }

    @Override
    public void updatePrimaryKeyForIntermediateTable(String primaryKeyColumnName, ETLMetaHelperVO helperVO) throws SQLException {
        String sql = "UPDATE "+helperVO.getIntermediateTableName()+" SET AM_ROW_INTERMEDIATE_PK_ID = ordered_uuid(UUID()) WHERE AM_ROW_INTERMEDIATE_PK_ID IS NULL;";
        List<Object> parameters = new ArrayList<>();
        executeUpdate(sql, parameters);
        return;
    }


    public void populateRowIntermediateForBasePETableByRecords(Map<String, RowCondition> rowConditionMap, Map.Entry<String, Object> entry, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException {
//        String tableName = etlMetaHelperVO.getTableName();
        List<Object> parameters = new ArrayList<>();

            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId(entry.getKey()));
            StringBuilder sql = new StringBuilder("INSERT INTO " + helperVO.getIntermediateTableName() + " (WHO_PERSON_FK_ID,  WHOSE_SUBJECT_USER_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, AM_ROW_CONDN_FK_ID, IS_DELETED)\n" +
                    "SELECT GROUP_MEMBER_PERSON_FK_ID,\n" +
                    "  ?,\n" +
                    "  ?, AM_ROW_CONDN_PK_ID, 0\n" +
                    "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                    "      INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                    "      INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                    "      INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_FK_ID = WHO_GROUP_FK_ID\n" +
                    "      INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
//                "      JOIN "+tableName+"\n" +
                    "    WHERE AM_ROW_CONDN_PK_ID IN (");
            for (Map.Entry<String, RowCondition> entryI : rowConditionMap.entrySet()) {
                sql.append("?,");
                parameters.add(Util.convertStringIdToByteId(entryI.getKey()));
            }
            sql.replace(sql.length() - 1, sql.length(), ")");
            sql.append(" AND (\n" +
                    "          (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                    "      SELECT node.RELATIONSHIP_TYPE_CODE_PK_ID, (select IF(false, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE))) ApplicableToAllAbove\n" +
                    "                FROM (SELECT\n" +
                    "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                    "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                    "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                    "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID\n" +
                    "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                    "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                    "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                    "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                    "                      (SELECT\n" +
                    "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                    "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                    "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                    "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                    "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                    "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                    "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                    "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                    "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                    "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                    "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                    "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                    "                  AND node.PERSON_DEP_FK_ID = ?\n" +
                    "                  AND parent.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                    "                  GROUP BY parent.PERSON_DEP_FK_ID)\n" +
                    "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                    "      SELECT node.INV_RELTN_TYPE_CODE_FK_ID, IF(IS_ALL_HIERARCHY, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE)) ApplicableToAllAbove\n" +
                    "                FROM (SELECT\n" +
                    "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                    "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                    "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                    "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID,\n" +
                    "                         TPLJRT.INV_RELTN_TYPE_CODE_FK_ID\n" +
                    "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                    "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                    "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                    "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                    "                      (SELECT\n" +
                    "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                    "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                    "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                    "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                    "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                    "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                    "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                    "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                    "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                    "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                    "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                    "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                    "                  AND node.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                    "                  AND parent.PERSON_DEP_FK_ID = ?\n" +
                    "                  GROUP BY parent.PERSON_DEP_FK_ID\n" +
                    "                  ORDER BY ApplicableToAllAbove DESC)\n" +
                    "          OR (RELATIONSHIP_TYPE_CODE_FK_ID) IN (\n" +
                    "                  SELECT DISTINCT 'PEERS'\n" +
                    "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                    "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                    "            WHERE RELATED_PERSON_FK_ID = (SELECT RELATED_PERSON_FK_ID\n" +
                    "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                    "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                    "            WHERE T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER') AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER' AND T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID <> T_PFM_IEU_GROUP_MEMBER.GROUP_MEMBER_PERSON_FK_ID AND T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID = ?)\n" +
                    "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
                    "              SELECT RELATIONSHIP_TYPE_CODE_FK_ID, 1\n" +
                    "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                    "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                    "            WHERE T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATED_PERSON_FK_ID = ? AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                    "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
                    "              SELECT INV_RELTN_TYPE_CODE_FK_ID, 1\n" +
                    "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                    "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE ON RELATIONSHIP_TYPE_CODE_PK_ID = RELATIONSHIP_TYPE_CODE_FK_ID\n" +
                    "            WHERE T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID = ? AND RELATED_PERSON_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                    "          OR RELATIONSHIP_TYPE_CODE_FK_ID = (SELECT IF(GROUP_MEMBER_PERSON_FK_ID = ?, 'SELF', 'ALL_BUT_SELF')));\n");
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            executeUpdate(sql.toString(), parameters);
            updatePrimaryKeyForIntermediateTable(primaryKeyColumnName, helperVO);

        return;
    }

    @Override
    public void populateRowIntermediateForChildPETableByRecords(Map<String, RowCondition> rowConditionMap, Map.Entry<String, Object> entry, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException {
        List<Object> parameters = new ArrayList<>();
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId(entry.getKey()));
            StringBuilder sql = new StringBuilder("INSERT INTO " + helperVO.getIntermediateTableName() + " (WHO_PERSON_FK_ID,  WHOSE_SUBJECT_USER_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, AM_ROW_CONDN_FK_ID, IS_DELETED)\n" +
                    "SELECT GROUP_MEMBER_PERSON_FK_ID,\n" +
                    "  ?,\n" +
                    "  ?, AM_ROW_CONDN_PK_ID, 0\n" +
                    "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                    "      INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                    "      INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                    "      INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_FK_ID = WHO_GROUP_FK_ID\n" +
                    "      INNER JOIN "+helperVO.getIntermediateTableName()+" ON AM_ROW_CONDN_FK_ID = AM_ROW_CONDN_PK_ID AND WHO_PERSON_FK_ID = GROUP_MEMBER_PERSON_FK_ID" +
                    "      INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
//                "      JOIN "+tableName+"\n" +
                    "    WHERE AM_ROW_CONDN_PK_ID IN (");
            for (Map.Entry<String, RowCondition> entryI : rowConditionMap.entrySet()) {
                sql.append("?,");
                parameters.add(Util.convertStringIdToByteId(entryI.getKey()));
            }
            sql.replace(sql.length() - 1, sql.length(), ")");
            sql.append(" AND (\n" +
                    "          (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                    "      SELECT node.RELATIONSHIP_TYPE_CODE_PK_ID, (select IF(false, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE))) ApplicableToAllAbove\n" +
                    "                FROM (SELECT\n" +
                    "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                    "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                    "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                    "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID\n" +
                    "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                    "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                    "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                    "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                    "                      (SELECT\n" +
                    "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                    "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                    "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                    "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                    "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                    "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                    "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                    "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                    "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                    "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                    "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                    "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                    "                  AND node.PERSON_DEP_FK_ID = ?\n" +
                    "                  AND parent.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                    "                  GROUP BY parent.PERSON_DEP_FK_ID)\n" +
                    "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                    "      SELECT node.INV_RELTN_TYPE_CODE_FK_ID, IF(IS_ALL_HIERARCHY, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE)) ApplicableToAllAbove\n" +
                    "                FROM (SELECT\n" +
                    "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                    "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                    "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                    "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID,\n" +
                    "                         TPLJRT.INV_RELTN_TYPE_CODE_FK_ID\n" +
                    "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                    "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                    "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                    "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                    "                      (SELECT\n" +
                    "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                    "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                    "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                    "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                    "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                    "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                    "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                    "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                    "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                    "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                    "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                    "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                    "                  AND node.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                    "                  AND parent.PERSON_DEP_FK_ID = ?\n" +
                    "                  GROUP BY parent.PERSON_DEP_FK_ID\n" +
                    "                  ORDER BY ApplicableToAllAbove DESC)\n" +
                    "          OR (RELATIONSHIP_TYPE_CODE_FK_ID) IN (\n" +
                    "                  SELECT DISTINCT 'PEERS'\n" +
                    "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                    "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                    "            WHERE RELATED_PERSON_FK_ID = (SELECT RELATED_PERSON_FK_ID\n" +
                    "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                    "              INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                    "            WHERE PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER') AND RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER' AND PERSON_DEP_FK_ID <> T_PFM_IEU_GROUP_MEMBER.GROUP_MEMBER_PERSON_FK_ID AND PERSON_DEP_FK_ID = ?)\n" +
                    "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
                    "              SELECT RELATIONSHIP_TYPE_CODE_FK_ID, 1\n" +
                    "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                    "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                    "            WHERE PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATED_PERSON_FK_ID = ? AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                    "          OR (RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (\n" +
                    "              SELECT INV_RELTN_TYPE_CODE_FK_ID, 1\n" +
                    "            FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                    "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE ON RELATIONSHIP_TYPE_CODE_PK_ID = RELATIONSHIP_TYPE_CODE_FK_ID\n" +
                    "            WHERE PERSON_DEP_FK_ID = ? AND RELATED_PERSON_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                    "          OR RELATIONSHIP_TYPE_CODE_FK_ID = (SELECT IF(GROUP_MEMBER_PERSON_FK_ID = ?, 'SELF', 'ALL_BUT_SELF')));\n");
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            parameters.add(Util.convertStringIdToByteId((String)entry.getValue()));
            executeUpdate(sql.toString(), parameters);
            updatePrimaryKeyForIntermediateTable(primaryKeyColumnName, helperVO);
        return;
    }

    @Override
    public void populateRowResolvedByRecords(String whatDbPrimaryKey, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException {
        StringBuilder sql = new StringBuilder("INSERT INTO "+helperVO.getRowResolvedTable()+" (AM_ROW_RESOLVED_PK_ID, WHO_PERSON_FK_ID, DOMAIN_OBJECT_CODE_FK_ID, WHAT_DB_PRIMARY_KEY_ID, WHOSE_SUBJECT_USER_PERSON_FK_ID,\n" +
                "                                       IS_READ_CURRENT_PERMITTED, IS_READ_HISTORY_PERMITTED, IS_READ_FUTURE_PERMITTED, IS_UPDATE_CURRENT_PERMITTED, IS_UPDATE_HISTORY_PERMITTED,\n" +
                "                                       IS_UPDATE_FUTURE_PERMITTED, IS_DELETE_CURRENT_PERMITTED, IS_DELETE_HISTORY_PERMITTED, IS_DELETE_FUTURE_PERMITTED, IS_DELETED)\n" +
                "SELECT\n" +
                "  ordered_uuid(uuid()),\n" +
                "  WHO_PERSON_FK_ID,\n" +
                "  ?,\n" +
                "  WHAT_DB_PRIMARY_KEY_ID,\n" +
                "  WHOSE_SUBJECT_USER_PERSON_FK_ID,\n" +
                "  CASE WHEN SUM(IS_READ_CURRENT_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(IS_READ_HISTORY_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(IS_READ_FUTURE_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(IS_UPDATE_CURRENT_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(IS_UPDATE_HISTORY_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(IS_UPDATE_FUTURE_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(IS_DELETE_CURRENT_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(IS_DELETE_HISTORY_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(IS_DELETE_FUTURE_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  FALSE \n" +
                "FROM "+helperVO.getIntermediateTableName()+" RI INNER JOIN T_PFM_ADM_AM_ROW_CONDN RC ON RI.AM_ROW_CONDN_FK_ID = RC.AM_ROW_CONDN_PK_ID\n" +
                "WHERE WHAT_DB_PRIMARY_KEY_ID = ?\n" +
                "GROUP BY RI.WHO_PERSON_FK_ID;");
        List<Object> parameters = new ArrayList<>();
        parameters.add(helperVO.getDoName());
        parameters.add(Util.convertStringIdToByteId(whatDbPrimaryKey));
        executeUpdate(sql.toString(), parameters);
        return;
    }

    @Override
    public void populateRowIntermediateForBasePETableByRecord(Map<String, RowCondition> rowConditionMap, Object whatDBPrimaryKey, Object whoseSubjectUserId, String intermediateTableName) throws SQLException {
        populateRowIntermediateForBasePETableByRecordForDirectManager(rowConditionMap, whatDBPrimaryKey, whoseSubjectUserId, intermediateTableName);
        populateRowIntermediateForBasePETableByRecordForDirectReport(rowConditionMap, whatDBPrimaryKey, whoseSubjectUserId, intermediateTableName);
        populateRowIntermediateForBasePETableByRecordForPeers(rowConditionMap, whatDBPrimaryKey, whoseSubjectUserId, intermediateTableName);
        populateRowIntermediateForBasePETableByRecordForMatrixManagerAndReport(rowConditionMap, whatDBPrimaryKey, whoseSubjectUserId, intermediateTableName);
        populateRowIntermediateForBasePETableByRecordForSelfAndAllButSelf(rowConditionMap, whatDBPrimaryKey, whoseSubjectUserId, intermediateTableName);
        populatePrimaryKeyForRowIntermediate(intermediateTableName);
    }

    private void populatePrimaryKeyForRowIntermediate(String intermediateTableName) throws SQLException {
        String sql = "UPDATE T_PFM_IEU_PRN_JOB_AM_ROW_INT SET AM_ROW_INTERMEDIATE_PK_ID = ordered_uuid(UUID()) WHERE AM_ROW_INTERMEDIATE_PK_ID IS NULL ;";
        executeUpdate(sql, null);
    }

    @Override
    public void populateColumnResolvedByRecord(Map.Entry entry, ETLMetaHelperVO helperVO, String primaryKeyColumnName) throws SQLException {
        StringBuilder sql = new StringBuilder("INSERT INTO "+helperVO.getColResolvedTable()+" (AM_COL_RESOLVED_PK_ID, AM_ROW_RESOLVED_FK_ID, DOA_CODE_FK_ID, IS_READ_CURRENT_PERMITTED, " +
                "IS_READ_HISTORY_PERMITTED, IS_READ_FUTURE_PERMITTED, IS_UPDATE_CURRENT_PERMITTED, IS_UPDATE_HISTORY_PERMITTED, IS_UPDATE_FUTURE_PERMITTED, IS_DELETED)\n" +
                "SELECT ordered_uuid(UUID()),AM_ROW_RESOLVED_PK_ID, WHAT_DOA_CODE_FK_ID, CASE WHEN SUM(T_PFM_ADM_AM_COL.IS_READ_CURRENT_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(T_PFM_ADM_AM_COL.IS_READ_HISTORY_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(T_PFM_ADM_AM_COL.IS_READ_FUTURE_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(T_PFM_ADM_AM_COL.IS_UPDATE_CURRENT_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(T_PFM_ADM_AM_COL.IS_UPDATE_HISTORY_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END,\n" +
                "  CASE WHEN SUM(T_PFM_ADM_AM_COL.IS_UPDATE_FUTURE_PERMITTED) > 0\n" +
                "    THEN TRUE\n" +
                "  ELSE FALSE END, 0\n" +
                "FROM "+helperVO.getRowResolvedTable()+" RR \n" +
                "  INNER JOIN "+helperVO.getIntermediateTableName()+" RI \n" +
                "    ON RI.WHAT_DB_PRIMARY_KEY_ID = RR.WHAT_DB_PRIMARY_KEY_ID AND\n" +
                "       RI.WHO_PERSON_FK_ID = RR.WHO_PERSON_FK_ID\n" +
                "  INNER JOIN T_PFM_ADM_AM_COL ON T_PFM_ADM_AM_COL.AM_ROW_CONDN_FK_ID = RI.AM_ROW_CONDN_FK_ID\n" +
                "  WHERE RI.WHAT_DB_PRIMARY_KEY_ID = ? \n" +
                "GROUP BY RI.WHO_PERSON_FK_ID, RI.WHAT_DB_PRIMARY_KEY_ID, WHAT_DOA_CODE_FK_ID ;");
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId((String)entry.getKey()));
        executeUpdate(sql.toString(), parameters);
        return;
    }

    private void populateRowIntermediateForBasePETableByRecordForSelfAndAllButSelf(Map<String, RowCondition> rowConditionMap,  Object whatDBPrimaryKey, Object whoseSubjectUserId, String intermediateTableName) throws SQLException {
        List<Object> parameters = new ArrayList<>();
        StringBuilder sql = new StringBuilder("INSERT INTO "+intermediateTableName+" (WHO_PERSON_FK_ID, WHOSE_SUBJECT_USER_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, AM_ROW_CONDN_FK_ID, IS_DELETED)"+
                "  SELECT\n" +
                "  GROUP_MEMBER_PERSON_FK_ID,\n" +
                "  ?,\n" +
                "  ?,\n" +
                "  AM_ROW_CONDN_PK_ID,\n" +
                "  0\n" +
                "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                "  INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "  INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                "  INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_FK_ID = WHO_GROUP_FK_ID\n" +
                "  INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
                "WHERE AM_ROW_CONDN_PK_ID IN (");
        parameters.add(Util.convertStringIdToByteId((String)whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId((String)whatDBPrimaryKey));
        for (Map.Entry rcEntry : rowConditionMap.entrySet()){
            sql.append("?,");
            parameters.add(Util.convertStringIdToByteId((String)rcEntry.getKey()));
        }
        sql.replace(sql.length() - 1, sql.length(), ")");
        sql.append("\n" +
                " AND RELATIONSHIP_TYPE_CODE_FK_ID = (SELECT IF(GROUP_MEMBER_PERSON_FK_ID = ?, 'SELF', ''));");
        parameters.add(Util.convertStringIdToByteId((String)whoseSubjectUserId));
        executeUpdate(sql.toString(), parameters);
        return;
    }

    private void populateRowIntermediateForBasePETableByRecordForMatrixManagerAndReport(Map<String, RowCondition> rowConditionMap,  Object whatDBPrimaryKey, Object whoseSubjectUserId, String intermediateTableName) throws SQLException {
        List<Object> parameters = new ArrayList<>();
        StringBuilder sql = new StringBuilder("INSERT INTO "+intermediateTableName+" (WHO_PERSON_FK_ID, WHOSE_SUBJECT_USER_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, AM_ROW_CONDN_FK_ID, IS_DELETED)"+
                "  SELECT\n" +
                "  GROUP_MEMBER_PERSON_FK_ID,\n" +
                "  ?,\n" +
                "  ?,\n" +
                "  AM_ROW_CONDN_PK_ID,\n" +
                "  0\n" +
                "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                "  INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "  INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                "  INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_FK_ID = WHO_GROUP_FK_ID\n" +
                "  INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
                "WHERE AM_ROW_CONDN_PK_ID IN (");
        parameters.add(Util.convertStringIdToByteId((String)whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId((String)whatDBPrimaryKey));
        for (Map.Entry rcEntry : rowConditionMap.entrySet()){
            sql.append("?,");
            parameters.add(Util.convertStringIdToByteId((String)rcEntry.getKey()));
        }
        sql.replace(sql.length() - 1, sql.length(), ")");
        sql.append(" AND (\n" +
                "        (RELATIONSHIP_TYPE_CODE_FK_ID) IN (\n" +
                "          SELECT\n" +
                "            RELATIONSHIP_TYPE_CODE_FK_ID\n" +
                "          FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "          WHERE T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID = ? AND\n" +
                "                RELATED_PERSON_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND\n" +
                "                RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER')\n" +
                "        OR (RELATIONSHIP_TYPE_CODE_FK_ID) IN (\n" +
                "          SELECT\n" +
                "            INV_RELTN_TYPE_CODE_FK_ID\n" +
                "          FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "            INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "            INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE ON RELATIONSHIP_TYPE_CODE_PK_ID = RELATIONSHIP_TYPE_CODE_FK_ID\n" +
                "          WHERE T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND\n" +
                "                RELATED_PERSON_FK_ID = ? AND RELATIONSHIP_TYPE_CODE_FK_ID = 'MATRIX_MANAGER'));");
        parameters.add(Util.convertStringIdToByteId((String)whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId((String)whatDBPrimaryKey));
        executeUpdate(sql.toString(), parameters);
        return;
    }

    private void populateRowIntermediateForBasePETableByRecordForPeers(Map<String, RowCondition> rowConditionMap,  Object whatDBPrimaryKey, Object whoseSubjectUserId, String intermediateTableName) throws SQLException {
        List<Object> parameters = new ArrayList<>();
        StringBuilder sql = new StringBuilder("INSERT INTO "+intermediateTableName+" (WHO_PERSON_FK_ID, WHOSE_SUBJECT_USER_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, AM_ROW_CONDN_FK_ID, IS_DELETED)"+
                "  SELECT\n" +
                "  GROUP_MEMBER_PERSON_FK_ID,\n" +
                "  ?,\n" +
                "  ?,\n" +
                "  AM_ROW_CONDN_PK_ID,\n" +
                "  0\n" +
                "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                "  INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "  INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                "  INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_FK_ID = WHO_GROUP_FK_ID\n" +
                "  INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
                "WHERE AM_ROW_CONDN_PK_ID IN\n" +
                "      (");
        parameters.add(Util.convertStringIdToByteId((String)whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId((String)whatDBPrimaryKey));
        for (Map.Entry rcEntry : rowConditionMap.entrySet()){
            sql.append("?,");
            parameters.add(Util.convertStringIdToByteId((String)rcEntry.getKey()));
        }
        sql.replace(sql.length() - 1, sql.length(), ")");
        sql.append(" AND (RELATIONSHIP_TYPE_CODE_FK_ID) IN (\n" +
                "  SELECT DISTINCT 'PEERS'\n" +
                "  FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "    INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "  WHERE RELATED_PERSON_FK_ID = (SELECT RELATED_PERSON_FK_ID\n" +
                "                                FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP\n" +
                "                                  INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT\n" +
                "                                    ON PERSON_JOB_ASSIGNMENT_DEP_FK_ID = PERSON_JOB_ASSIGNMENT_PK_ID\n" +
                "                                WHERE T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID AND\n" +
                "                                      RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER') AND\n" +
                "        RELATIONSHIP_TYPE_CODE_FK_ID = 'DIRECT_MANAGER' AND\n" +
                "        T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID <> T_PFM_IEU_GROUP_MEMBER.GROUP_MEMBER_PERSON_FK_ID AND\n" +
                "        T_PRN_EU_PERSON_JOB_ASSIGNMENT.PERSON_DEP_FK_ID = ?);");
        parameters.add(Util.convertStringIdToByteId((String)whoseSubjectUserId));
        executeUpdate(sql.toString(), parameters);
        return;
    }

    private void populateRowIntermediateForBasePETableByRecordForDirectReport(Map<String, RowCondition> rowConditionMap,  Object whatDBPrimaryKey, Object whoseSubjectUserId, String intermediateTableName) throws SQLException {
        List<Object> parameters = new ArrayList<>();
        StringBuilder sql = new StringBuilder("INSERT INTO "+intermediateTableName+" (WHO_PERSON_FK_ID, WHOSE_SUBJECT_USER_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, AM_ROW_CONDN_FK_ID, IS_DELETED)" +
                "  SELECT GROUP_MEMBER_PERSON_FK_ID,\n" +
                "  ?,\n" +
                "  ?, AM_ROW_CONDN_PK_ID, 0\n" +
                "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                "      INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "      INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                "      INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_FK_ID = WHO_GROUP_FK_ID\n" +
                "      INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
                "    WHERE AM_ROW_CONDN_PK_ID IN (");
        parameters.add(Util.convertStringIdToByteId((String)whoseSubjectUserId));
        parameters.add(Util.convertStringIdToByteId((String)whatDBPrimaryKey));
        for (Map.Entry rcEntry : rowConditionMap.entrySet()){
            sql.append("?,");
            parameters.add(Util.convertStringIdToByteId((String)rcEntry.getKey()));
        }
        sql.replace(sql.length() - 1, sql.length(), ")");
        sql.append(" AND (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                "      SELECT node.INV_RELTN_TYPE_CODE_FK_ID, (select IF(IS_ALL_HIERARCHY, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE))) ApplicableToAllAbove\n" +
                "                FROM (SELECT\n" +
                "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID,\n" +
                "                         TPLJRT.INV_RELTN_TYPE_CODE_FK_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                "                      (SELECT\n" +
                "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "                  AND node.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID\n" +
                "                  AND parent.PERSON_DEP_FK_ID = ?);");
        parameters.add(Util.convertStringIdToByteId((String)whoseSubjectUserId));
        executeUpdate(sql.toString(), parameters);
        return;
    }

    private void populateRowIntermediateForBasePETableByRecordForDirectManager(Map<String, RowCondition> rowConditionMap, Object whatDBPrimaryKey, Object whoseSubjectUserId, String intermediateTableName) throws SQLException {
        List<Object> parameters = new ArrayList<>();
        StringBuilder sql = new StringBuilder("INSERT INTO "+intermediateTableName+" (WHO_PERSON_FK_ID,  WHOSE_SUBJECT_USER_PERSON_FK_ID, WHAT_DB_PRIMARY_KEY_ID, AM_ROW_CONDN_FK_ID, IS_DELETED)\n" +
                "SELECT GROUP_MEMBER_PERSON_FK_ID,\n" +
                "  ?, ?, AM_ROW_CONDN_PK_ID, 0\n" +
                "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                "      INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "      INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                "      INNER JOIN T_PFM_IEU_GROUP_MEMBER ON PFM_GROUP_FK_ID = WHO_GROUP_FK_ID\n" +
                "      INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS ON AM_PROCESS_PK_ID = T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID\n" +
                "    WHERE AM_ROW_CONDN_PK_ID IN (");
        parameters.add(Util.convertStringIdToByteId((String)whatDBPrimaryKey));
        parameters.add(Util.convertStringIdToByteId((String)whoseSubjectUserId));
        for (Map.Entry rcEntry : rowConditionMap.entrySet()){
            sql.append("?,");
            parameters.add(Util.convertStringIdToByteId((String)rcEntry.getKey()));
        }
        sql.replace(sql.length() - 1, sql.length(), ")");
        sql.append(" AND (\n" +
                "      (RELATIONSHIP_TYPE_CODE_FK_ID, TRUE) IN (\n" +
                "      SELECT node.RELATIONSHIP_TYPE_CODE_PK_ID, (select IF(TRUE, TRUE, IF((LFT_RGT_HGHT(node.PERSON_DEP_FK_ID)-LFT_RGT_DEPTH(parent.PERSON_DEP_FK_ID) -1)= 1,TRUE, FALSE))) ApplicableToAllAbove\n" +
                "                FROM (SELECT\n" +
                "                         TPEPJR.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJA.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJR.INTRNL_HIERARCHY_ID,\n" +
                "                         TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID,\n" +
                "                         TPLJRT.INV_RELTN_TYPE_CODE_FK_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJR INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJA\n" +
                "                           ON TPEPJA.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJR.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJA.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRT\n" +
                "                           ON TPEPJR.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRT.RELATIONSHIP_TYPE_CODE_PK_ID AND TPLJRT.IS_DIRECT_MANAGER) node,\n" +
                "                      (SELECT\n" +
                "                         TPEPJAB.PERSON_DEP_FK_ID,\n" +
                "                         TPEPJRB.RELATED_PERSON_FK_ID,\n" +
                "                         TPEPJRB.INTRNL_LEFT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_RIGHT_POS_INT,\n" +
                "                         TPEPJRB.INTRNL_HIERARCHY_ID\n" +
                "                       FROM T_PRN_EU_PERSON_JOB_RELATIONSHIP TPEPJRB INNER JOIN T_PRN_EU_PERSON_JOB_ASSIGNMENT TPEPJAB\n" +
                "                           ON TPEPJAB.PERSON_JOB_ASSIGNMENT_PK_ID = TPEPJRB.PERSON_JOB_ASSIGNMENT_DEP_FK_ID AND TPEPJAB.IS_PRIMARY\n" +
                "                         INNER JOIN T_PRN_LKP_JOB_RELATIONSHIP_TYPE TPLJRTB\n" +
                "                           ON TPEPJRB.RELATIONSHIP_TYPE_CODE_FK_ID = TPLJRTB.RELATIONSHIP_TYPE_CODE_PK_ID AND\n" +
                "                           TPLJRTB.IS_DIRECT_MANAGER) parent\n" +
                "                  WHERE node.INTRNL_LEFT_POS_INT > parent.INTRNL_LEFT_POS_INT AND node.INTRNL_LEFT_POS_INT < parent.INTRNL_RIGHT_POS_INT\n" +
                "                    AND node.INTRNL_HIERARCHY_ID = parent.INTRNL_HIERARCHY_ID\n" +
                "                  AND node.PERSON_DEP_FK_ID = ? \n" +
                "                  AND parent.PERSON_DEP_FK_ID = GROUP_MEMBER_PERSON_FK_ID)\n" +
                "          );");
        parameters.add(Util.convertStringIdToByteId((String)whoseSubjectUserId));
        executeUpdate(sql.toString(), parameters);
        return;
    }

    private void deleteExistingPermissionsFromResolvedRecord(String whatDBPrimaryKey, String rowResolvedTable) throws SQLException {
        if (whatDBPrimaryKey == null)
            return;
        String sql = "DELETE FROM "+rowResolvedTable+" WHERE WHAT_DB_PRIMARY_KEY_ID = ? ;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(whatDBPrimaryKey));
        executeUpdate(sql, parameters);
        return;
    }

    public void deleteExistingPermissionsFromIntermediateRecord(String whatDBPrimaryKey, String rowIntermediateTable, List<String> rowConditionsToDelete) throws SQLException {
        if (whatDBPrimaryKey == null)
            return;
        String sql = "DELETE FROM "+rowIntermediateTable+" WHERE WHAT_DB_PRIMARY_KEY_ID = ? ;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.convertStringIdToByteId(whatDBPrimaryKey));
        executeUpdate(sql, parameters);
        return;
    }

    @Override
    public Map<String, RowCondition> getRelativeRowConditons(Map<String, Integer> groupIdPositionMap, PersonGroup whoGroup, String mtPE) {
        if (groupIdPositionMap == null || groupIdPositionMap.isEmpty())
            return null;
        else{
            StringBuilder sql = new StringBuilder("SELECT *\n" +
                    "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                    "  INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                    "  INNER JOIN T_PFM_ADM_AM_WHOSE_PROCESS\n" +
                    "    ON T_PFM_ADM_AM_WHOSE_PROCESS.AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID AND IS_WHOSE_RELATIVE_TO_WHO = 1\n" +
                    "  INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID = T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID\n" +
                    "WHERE WHO_GROUP_FK_ID IN (");
            List<Object> parameters = new ArrayList<>();
            for (String groupId : whoGroup.getGroupIds()){
                sql.append("x?,");
                parameters.add(Util.remove0x(groupId));
            }
            sql.replace(sql.length() - 1, sql.length(), ")");
            sql.append(" AND\n" +
                    " (T_PFM_ADM_AM_WHOSE_PROCESS.RELATIONSHIP_TYPE_CODE_FK_ID, REL_HIERARCHY_LEVEL_POS_INT) IN (");
            for (Map.Entry< String, Integer> entry : groupIdPositionMap.entrySet()){
                sql.append("(?,?),");
                parameters.add(entry.getKey());
                parameters.add(entry.getValue());
            }
            sql.replace(sql.length() - 1, sql.length(), "");
            sql.append(") AND T_PFM_ADM_AM_ROW_CONDN.WHAT_MT_PE_CODE_FK_ID = ?;");
            parameters.add(mtPE);
            ResultSet rs = executeSQL(sql.toString(), parameters);
            Map<String, RowCondition> relativeRowConditions = new HashMap<>();
            getRowConditionsFromReseltSet(rs, relativeRowConditions);
            return relativeRowConditions;
        }

    }

    @Override
    public List<AccessManager> getAccessManagersByWhoGroup(List<String> groupIds) {
        if (groupIds == null || groupIds.isEmpty())
            return null;
        List<AccessManager> accessManagers = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_ADM_ACCESS_MANAGER WHERE WHO_GROUP_FK_ID IN (");
        List<Object> parameters = new ArrayList<>();
        for (String groupId : groupIds){
            sql.append("x?,");
            parameters.add(Util.remove0x(groupId));
        }
        sql.replace(sql.length() - 1, sql.length(), ");");
        ResultSet rs = executeSQL(sql.toString(), parameters);
        try {
            while (rs.next()){
                AccessManager accessManager = new AccessManager();
                accessManager.setAccessManagerPkId(Util.convertByteToString(rs.getBytes("ACCESS_MANAGER_PK_ID")));
                accessManager.setAmDefnFkId(Util.convertByteToString(rs.getBytes("AM_DEFN_FK_ID")));
                accessManager.setWhoGroupFkId(Util.convertByteToString(rs.getBytes("WHO_GROUP_FK_ID")));
                accessManagers.add(accessManager);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return accessManagers;
    }

    @Override
    public List<AMProcess> getAmProcessByAccessManager(List<AccessManager> accessManagers) {
        if (accessManagers == null || accessManagers.isEmpty())
            return null;
        List<AMProcess> amProcesses = new ArrayList<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_ADM_AM_PROCESS WHERE AM_DEFN_FK_ID IN (");
        List<Object> parameters = new ArrayList<>();
        for (AccessManager accessManager : accessManagers){
            sql.append("x?,");
            parameters.add(Util.remove0x(accessManager.getAmDefnFkId()));
        }
        sql.replace(sql.length() - 1, sql.length(), ");");
        ResultSet rs = executeSQL(sql.toString(), parameters);
        try {
            while (rs.next()){
                AMProcess amProcess = new AMProcess();
                amProcess.setAmProcessPkId(Util.convertByteToString(rs.getBytes("AM_PROCESS_PK_ID")));
                amProcess.setAmDefnFkId(Util.convertByteToString(rs.getBytes("AM_DEFN_FK_ID")));
                amProcess.setWhoseGroupFkId(Util.convertByteToString(rs.getBytes("WHOSE_GROUP_FK_ID")));
                amProcess.setWhoseRelativeToWho(rs.getBoolean("IS_WHOSE_RELATIVE_TO_WHO"));
                amProcess.setMtProcessCodeFkId(rs.getString("MT_PROCESS_CODE_FK_ID"));
                amProcesses.add(amProcess);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return amProcesses;
    }

    @Override
    public Map<String, RowCondition> getRowConditions(List<AMProcess> amProcesses, String mtPE) {
        if (amProcesses == null || amProcesses.isEmpty())
            return null;
        Map<String, RowCondition> rowConditions = new HashMap<>();
        StringBuilder sql = new StringBuilder("SELECT * FROM T_PFM_ADM_AM_ROW_CONDN INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID \n" +
                " INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID = T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID WHERE WHAT_MT_PE_CODE_FK_ID = ? AND AM_ACCESS_PROCESS_FK_ID IN (");
        List<Object> parameters = new ArrayList<>();
        parameters.add(mtPE);
        for (AMProcess amProcess : amProcesses) {
            sql.append("x?,");
            parameters.add(Util.remove0x(amProcess.getAmProcessPkId()));
        }
        sql.replace(sql.length() - 1, sql.length(), ");");
        ResultSet rs = executeSQL(sql.toString(), parameters);
        getRowConditionsFromReseltSet(rs, rowConditions);
        return rowConditions;
    }

    @Override
    public AMRowResolved getAMRowResolvedForParentPE(String parentWhatDbPrimaryKey, EntityAttributeMetadata dependencyKeyAttribute, String whoseSubjectUserId) {
        String sql = "SELECT * FROM "+dependencyKeyAttribute.getToDbTableName()+" WHERE WHO_PERSON_FK_ID = x? AND DOMAIN_OBJECT_CODE_FK_ID = ? AND WHAT_DB_PRIMARY_KEY_ID = x? AND WHOSE_SUBJECT_USER_PERSON_FK_ID = x?";
        List<Object> parameters = new ArrayList<>();
        parameters.add(Util.remove0x(TenantContextHolder.getUserContext().getPersonId()));
        parameters.add(dependencyKeyAttribute.getToDomainObjectCode());
        parameters.add(Util.remove0x(parentWhatDbPrimaryKey));
        parameters.add(Util.remove0x(whoseSubjectUserId));
        ResultSet rs = executeSQL(sql, parameters);
        AMRowResolved amRowResolved = null;
        try {
            if (rs.next()){
                amRowResolved = new AMRowResolved();
                amRowResolved.setResolvedRowPkId(Util.convertByteToString(rs.getBytes("AM_ROW_RESOLVED_PK_ID")));
                amRowResolved.setResolvedRowPkId(Util.convertByteToString(rs.getBytes("AM_ROW_RESOLVED_PK_ID")));
                amRowResolved.setDomainObjectId(rs.getString("DOMAIN_OBJECT_CODE_FK_ID"));
                amRowResolved.setWhatDbPrimaryKeyId(Util.convertByteToString(rs.getBytes("WHAT_DB_PRIMARY_KEY_ID")));
                amRowResolved.setWhoseSubjectUserId(Util.convertByteToString(rs.getBytes("WHOSE_SUBJECT_USER_PERSON_FK_ID")));
                amRowResolved.setReadCurrentPermitted(rs.getBoolean("IS_READ_CURRENT_PERMITTED"));
                amRowResolved.setReadHistoryPermitted(rs.getBoolean("IS_READ_HISTORY_PERMITTED"));
                amRowResolved.setReadFuturePermitted(rs.getBoolean("IS_READ_FUTURE_PERMITTED"));
                amRowResolved.setUpdateCurrentPermitted(rs.getBoolean("IS_UPDATE_CURRENT_PERMITTED"));
                amRowResolved.setUpdateHistoryPermitted(rs.getBoolean("IS_UPDATE_HISTORY_PERMITTED"));
                amRowResolved.setUpdateFuturePermitted(rs.getBoolean("IS_UPDATE_FUTURE_PERMITTED"));
                amRowResolved.setDeleteCurrentPermitted(rs.getBoolean("IS_DELETE_CURRENT_PERMITTED"));
                amRowResolved.setDeleteHistoryPermitted(rs.getBoolean("IS_DELETE_HISTORY_PERMITTED"));
                amRowResolved.setDeleteFuturePermitted(rs.getBoolean("IS_DELETE_HISTORY_PERMITTED"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return amRowResolved;
    }

    @Override
    public Map<String, RowCondition> getAbsoluteRowConditions(PersonGroup whoGroup, PersonGroup whoseGroup, String mtPE) {
        StringBuilder sql = new StringBuilder("SELECT *\n" +
                "FROM T_PFM_ADM_AM_ROW_CONDN\n" +
                "  INNER JOIN T_PFM_ADM_AM_PROCESS ON AM_ACCESS_PROCESS_FK_ID = AM_PROCESS_PK_ID\n" +
                "  INNER JOIN T_PFM_ADM_ACCESS_MANAGER ON T_PFM_ADM_ACCESS_MANAGER.AM_DEFN_FK_ID = T_PFM_ADM_AM_PROCESS.AM_DEFN_FK_ID\n" +
                "WHERE WHOSE_GROUP_FK_ID IN ( ");
        List<Object> parameters = new ArrayList<>();
        for (String groupId : whoseGroup.getGroupIds()){
            sql.append("x?,");
            parameters.add(Util.remove0x(groupId));
        }
        sql.replace(sql.length() - 1, sql.length(), ") AND WHAT_MT_PE_CODE_FK_ID = ? AND T_PFM_ADM_ACCESS_MANAGER.WHO_GROUP_FK_ID IN (");
        parameters.add(mtPE);
        for (String groupId : whoGroup.getGroupIds()){
            sql.append("x?,");
            parameters.add(Util.remove0x(groupId));
        }
        sql.replace(sql.length() - 1, sql.length(), ") AND T_PFM_ADM_AM_PROCESS.IS_WHOSE_RELATIVE_TO_WHO = 0");
        ResultSet rs = executeSQL(sql.toString(), parameters);
        Map<String, RowCondition> absoluteRowConditions = new HashMap<>();
        getRowConditionsFromReseltSet(rs, absoluteRowConditions);
        return absoluteRowConditions;
    }

    private void getRowConditionsFromReseltSet(ResultSet rs, Map<String, RowCondition> rowConditions) {
        try {
            while (rs.next()) {
                RowCondition rowCondition = new RowCondition();
                rowCondition.setRowCondnPkId(convertByteToString(rs.getBytes("AM_ROW_CONDN_PK_ID")));
                rowCondition.setWhoGroupId(convertByteToString(rs.getBytes("T_PFM_ADM_ACCESS_MANAGER.WHO_GROUP_FK_ID")));
                rowCondition.setWhatDOCode(rs.getString("WHAT_DO_CODE_FK_ID"));
//                rowCondition.setWhatDOExpression(rs.getString("WHAT_MT_PE_FILTER_EXPN"));
                rowCondition.setWhoseGroupFKId(convertByteToString(rs.getBytes("WHOSE_GROUP_FK_ID")));
                rowCondition.setWhoseRelativeToWho(rs.getBoolean("T_PFM_ADM_AM_PROCESS.IS_WHOSE_RELATIVE_TO_WHO"));
    //                rowCondition.setWhoseWhoExpression(rs.getString("T_MT_ADM_DO_ATTRIBUTE"));
    //                rowCondition.setIsWhoseRelativeToDO(rs.getBoolean(""));
    //                rowCondition.setStartDateTime(rs.getBoolean(""));
    //                rowCondition.setEndDataTime(rs.getBoolean(""));
                rowCondition.setCreateCurrentPermitted(rs.getBoolean("IS_CREATE_CURRENT_PERMITTED"));
                rowCondition.setCreateHistoryPermitted(rs.getBoolean("IS_CREATE_HISTORY_PERMITTED"));
                rowCondition.setCreateFuturePermitted(rs.getBoolean("IS_CREATE_FUTURE_PERMITTED"));
                rowCondition.setReadCurrentPermitted(rs.getBoolean("IS_READ_CURRENT_PERMITTED"));
                rowCondition.setReadHistoryPermitted(rs.getBoolean("IS_READ_HISTORY_PERMITTED"));
                rowCondition.setReadFuturePermitted(rs.getBoolean("IS_READ_FUTURE_PERMITTED"));
                rowCondition.setUpdateCurrentPermitted(rs.getBoolean("IS_UPDATE_CURRENT_PERMITTED"));
                rowCondition.setUpdateHistoryPermitted(rs.getBoolean("IS_UPDATE_HISTORY_PERMITTED"));
                rowCondition.setUpdateFuturePermitted(rs.getBoolean("IS_UPDATE_FUTURE_PERMITTED"));
                rowCondition.setDeleteCurrentPermitted(rs.getBoolean("IS_DELETE_CURRENT_PERMITTED"));
                rowCondition.setDeleteHistoryPermitted(rs.getBoolean("IS_DELETE_HISTORY_PERMITTED"));
                rowCondition.setDeleteFuturePermitted(rs.getBoolean("IS_DELETE_FUTURE_PERMITTED"));
                rowConditions.put(rowCondition.getRowCondnPkId(), rowCondition);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ETLMetaHelperVO getAccessControlGovernedDOByDOName(String dOName) {
        String SQL = "SELECT DO1.DOMAIN_OBJECT_CODE_PK_ID, DO1.DB_TABLE_NAME_TXT, DO1.AM_ROW_INT_MTPE_CODE_FK_ID, DO1.AM_ROW_RSLVD_MTPE_CODE_FK_ID, DO1.AM_COL_RSLVD_MTPE_CODE_FK_ID, DO1.IS_ACCESS_CONTROL_GOVERNED, DO1.SUBJECT_PERSON_DOA_FULL_PATH_EXPN, MTPE.META_PROCESS_ELEMENT_CODE_PK_ID,\n" +
                "  DO2.AM_ROW_INT_MTPE_CODE_FK_ID, DO2.AM_ROW_RSLVD_MTPE_CODE_FK_ID, DO2.AM_COL_RSLVD_MTPE_CODE_FK_ID, MTPE2.META_PROCESS_ELEMENT_CODE_PK_ID, DB_COLUMN_NAME_TXT FROM T_MT_ADM_DOMAIN_OBJECT DO1\n" +
                "  INNER JOIN T_MT_ADM_DO_ATTRIBUTE ATTR ON DO1.DOMAIN_OBJECT_CODE_PK_ID = ATTR.DOMAIN_OBJECT_CODE_FK_ID AND IS_DB_PRIMARY_KEY = 1\n" +
                "  INNER JOIN T_MT_ADM_PROCESS_ELEMENT MTPE ON DO1.DOMAIN_OBJECT_CODE_PK_ID = MTPE.MT_DOMAIN_OBJECT_CODE_FK_ID\n" +
                "  LEFT JOIN T_MT_ADM_PROCESS_ELEMENT MTPE2 ON MTPE2.META_PROCESS_ELEMENT_CODE_PK_ID = MTPE.SLF_PRNT_MT_PROCESS_ELEMENT_CODE_FK_ID\n" +
                "  LEFT JOIN T_MT_ADM_DOMAIN_OBJECT DO2 ON DO2.DOMAIN_OBJECT_CODE_PK_ID = MTPE2.MT_DOMAIN_OBJECT_CODE_FK_ID\n" +
                "  WHERE DO1.IS_ACCESS_CONTROL_GOVERNED = TRUE AND DO1.DOMAIN_OBJECT_CODE_PK_ID = ?;";
        List<Object> parameters = new ArrayList<>();
        parameters.add(dOName);
        ResultSet rs = executeSQL(SQL, parameters);
        ETLMetaHelperVO metaHelperVO = getEtlMetaHelperResult(SQL, rs);
        populateAccessControlTableNames(metaHelperVO);
        return metaHelperVO;
    }

    private void populateAccessControlTableNames(ETLMetaHelperVO metaHelperVO) {
        StringBuilder sql = new StringBuilder("SELECT META_PROCESS_ELEMENT_CODE_PK_ID, DB_TABLE_NAME_TXT FROM T_MT_ADM_PROCESS_ELEMENT INNER JOIN T_MT_ADM_DOMAIN_OBJECT ON MT_DOMAIN_OBJECT_CODE_FK_ID = DOMAIN_OBJECT_CODE_PK_ID\n" +
                " WHERE META_PROCESS_ELEMENT_CODE_PK_ID IN (?,?,?,?,?,?);");
        List<Object> parameters = new ArrayList<>();
        parameters.add(metaHelperVO.getIntermediateMtPE());
        parameters.add(metaHelperVO.getRowResolvedMtPE());
        parameters.add(metaHelperVO.getColResolvedMtPE());
        parameters.add(metaHelperVO.getParentIntermediateMtPE());
        parameters.add(metaHelperVO.getParentRowResolvedMtPE());
        parameters.add(metaHelperVO.getParentColResolvedMtPE());
        ResultSet rs = executeSQL(sql.toString(), parameters);
        Map<String, String> resultMap = new HashMap<>();
        try {
            while (rs.next()){
                resultMap.put(rs.getString("META_PROCESS_ELEMENT_CODE_PK_ID"), rs.getString("DB_TABLE_NAME_TXT"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        metaHelperVO.setIntermediateTableName(resultMap.get(metaHelperVO.getIntermediateMtPE()));
        metaHelperVO.setRowResolvedTable(resultMap.get(metaHelperVO.getRowResolvedMtPE()));
        metaHelperVO.setColResolvedTable(resultMap.get(metaHelperVO.getColResolvedMtPE()));
        metaHelperVO.setParentIntermediateTableName(resultMap.get(metaHelperVO.getParentIntermediateMtPE()));
        metaHelperVO.setParentRowResolvedTable(resultMap.get(metaHelperVO.getParentRowResolvedMtPE()));
        metaHelperVO.setParentColResolvedTable(resultMap.get(metaHelperVO.getParentColResolvedMtPE()));
    }

    private ETLMetaHelperVO getEtlMetaHelperResult(String sql, ResultSet rs) {
        ETLMetaHelperVO metaHelperVO = null;
        try {
            if (rs.next()) {
                metaHelperVO = new ETLMetaHelperVO();
                metaHelperVO.setTableName(rs.getString("DO1.DB_TABLE_NAME_TXT"));
                metaHelperVO.setDoName(rs.getString("DOMAIN_OBJECT_CODE_PK_ID"));
              //  metaHelperVO.setAccessControlApplicable(rs.getBoolean("IS_ACCESS_CONTROL_GOVERNED"));
                metaHelperVO.setPrimaryKeyColumnName(rs.getString("DB_COLUMN_NAME_TXT"));
                metaHelperVO.setSubjectUserPath(rs.getString("DO1.SUBJECT_PERSON_DOA_FULL_PATH_EXPN"));
                metaHelperVO.setMtPE(rs.getString("MTPE.META_PROCESS_ELEMENT_CODE_PK_ID"));
                metaHelperVO.setParentMtPE(rs.getString("MTPE2.META_PROCESS_ELEMENT_CODE_PK_ID"));
                metaHelperVO.setParentIntermediateMtPE(rs.getString("DO2.AM_ROW_INT_MTPE_CODE_FK_ID"));
                metaHelperVO.setIntermediateMtPE(rs.getString("DO1.AM_ROW_INT_MTPE_CODE_FK_ID"));
                metaHelperVO.setParentRowResolvedMtPE(rs.getString("DO2.AM_ROW_RSLVD_MTPE_CODE_FK_ID"));
                metaHelperVO.setRowResolvedMtPE(rs.getString("DO1.AM_ROW_RSLVD_MTPE_CODE_FK_ID"));
                metaHelperVO.setParentColResolvedMtPE(rs.getString("DO2.AM_COL_RSLVD_MTPE_CODE_FK_ID"));
                metaHelperVO.setColResolvedMtPE(rs.getString("DO1.AM_COL_RSLVD_MTPE_CODE_FK_ID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return metaHelperVO;
    }


    @Override
    public String getSubjectUserId(StringBuilder sql, String primaryKeyDBDataType) {
        String subjectUserId = null;
        List<Object> parameters = new ArrayList<>();
        ResultSet rs = super.executeSQL(sql.toString(), parameters);
        try {
            if (rs.next()){
                if(primaryKeyDBDataType.equals("T_ID")) {
                    subjectUserId = Util.convertByteToString(rs.getBytes(1));
                }else{
                    subjectUserId = rs.getString(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subjectUserId;
    }

    private String convertByteToString(byte[] input){
        if(input == null)
            return null;
        return Util.convertByteToString(input);
    }
}