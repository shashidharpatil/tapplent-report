package com.tapplent.platformutility.metadata;

public class MetadataVO {
	private String dataApiInternalTablePkId;
	private String btProcessElementFkId;
	private String btDoSpecFkId;
	private String btDoSpecNameG11nText;
	private Boolean isCreateToBeLogged;
	private Boolean isReadToBeLogged;
	private Boolean isUpdateToBeLogged;
	private Boolean isDeleteToBeLogged;
	private String btDoAttributeSpecFkId;
	private String baseTemplateFkId;
	private String metaProcessCode;
	private String domainObjectFkId;
	private String domainObjectNameG11nText;
	private String dbTableName;
	private String doAttributeFkId;
	private String mtDoAttributeSpecFkG11nText;
	private String dbColumnName;
	private String btDataTypeCodeFkId;	
	private String btDoAttrSpecNameG11nText;
	private String dbDataTypeCodeFkId;
	private Boolean functionalPrimaryKeyFlag;
	private Boolean dbPrimaryKeyFlag;
	private Boolean dependencyKeyFlag;
	private Boolean relationFlag;
	private Boolean hierarchyFlag;
	private Boolean selfJoinParentFlag;
	private Boolean reportableFlag;
	private Boolean standardFlag;
	private Boolean glocalizedFlag;
	private Boolean localSearchableFlag;
	private Boolean isSubjectCountry;
	private Boolean isGlocalized;
	private Boolean isCountrySpecific;
	private Boolean isMandatory;
	private Boolean isEncrypted;
	private Boolean isAnonymized;
	private Boolean isDisplayMasked;
	private String displayMaskCharacter;
	private Boolean isReportMasked;
	private String reportMaskCharcter;
	private Boolean isUniqueAttribute;
	private Boolean isUseSubjectPersonTimezone;
	private Boolean isSortConfig;
	private int sortConfigSeq;
	private String sortOrderCodeFkId;
	private Boolean isFilterConfig;
	private int filterConfigSeq;
	private int charMaxLength;
	private boolean isEnableEmoticon;
	private boolean isContextObjectField;
	private int contextSeqNumber;
	private boolean isFilterOverride;
	private String containerBlobDoaName;
	private String toDomainObjectFkId;
	private String toDomainObjectNameG11nText;
	private String toDbTableName;
	private String toDoAttributeFkId;
	private String toDoAttributeNameG11nText;
	private String toDbColumnName;
	private Boolean isCardinalityMany;
	private Boolean isCardinalityManyToMany;
	private boolean isRelationToBeConsidered;
	public MetadataVO(){
		
	}
	public String getDataApiInternalTablePkId() {
		return dataApiInternalTablePkId;
	}
	public void setDataApiInternalTablePkId(String dataApiInternalTablePkId) {
		this.dataApiInternalTablePkId = dataApiInternalTablePkId;
	}
	public String getBtProcessElementFkId() {
		return btProcessElementFkId;
	}
	public void setBtProcessElementFkId(String btProcessElementFkId) {
		this.btProcessElementFkId = btProcessElementFkId;
	}
	public String getBtDoSpecFkId() {
		return btDoSpecFkId;
	}
	public void setBtDoSpecFkId(String btDoSpecFkId) {
		this.btDoSpecFkId = btDoSpecFkId;
	}
	public String getBtDoSpecNameG11nText() {
		return btDoSpecNameG11nText;
	}
	public void setBtDoSpecNameG11nText(String btDoSpecNameG11nText) {
		this.btDoSpecNameG11nText = btDoSpecNameG11nText;
	}
	public Boolean getIsCreateToBeLogged() {
		return isCreateToBeLogged;
	}
	public void setIsCreateToBeLogged(Boolean isCreateToBeLogged) {
		this.isCreateToBeLogged = isCreateToBeLogged;
	}
	public Boolean getIsReadToBeLogged() {
		return isReadToBeLogged;
	}
	public void setIsReadToBeLogged(Boolean isReadToBeLogged) {
		this.isReadToBeLogged = isReadToBeLogged;
	}
	public Boolean getIsUpdateToBeLogged() {
		return isUpdateToBeLogged;
	}
	public void setIsUpdateToBeLogged(Boolean isUpdateToBeLogged) {
		this.isUpdateToBeLogged = isUpdateToBeLogged;
	}
	public Boolean getIsDeleteToBeLogged() {
		return isDeleteToBeLogged;
	}
	public void setIsDeleteToBeLogged(Boolean isDeleteToBeLogged) {
		this.isDeleteToBeLogged = isDeleteToBeLogged;
	}
	public String getBtDoAttributeSpecFkId() {
		return btDoAttributeSpecFkId;
	}
	public void setBtDoAttributeSpecFkId(String btDoAttributeSpecFkId) {
		this.btDoAttributeSpecFkId = btDoAttributeSpecFkId;
	}
	public String getBaseTemplateFkId() {
		return baseTemplateFkId;
	}
	public void setBaseTemplateFkId(String baseTemplateFkId) {
		this.baseTemplateFkId = baseTemplateFkId;
	}
	public String getMetaProcessCode() {
		return metaProcessCode;
	}
	public void setMetaProcessCode(String metaProcessCode) {
		this.metaProcessCode = metaProcessCode;
	}
	public String getDomainObjectFkId() {
		return domainObjectFkId;
	}
	public void setDomainObjectFkId(String domainObjectFkId) {
		this.domainObjectFkId = domainObjectFkId;
	}
	public String getDomainObjectNameG11nText() {
		return domainObjectNameG11nText;
	}
	public void setDomainObjectNameG11nText(String domainObjectNameG11nText) {
		this.domainObjectNameG11nText = domainObjectNameG11nText;
	}
	public String getDbTableName() {
		return dbTableName;
	}
	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}
	public String getDoAttributeFkId() {
		return doAttributeFkId;
	}
	public void setDoAttributeFkId(String doAttributeFkId) {
		this.doAttributeFkId = doAttributeFkId;
	}
	public String getMtDoAttributeSpecFkG11nText() {
		return mtDoAttributeSpecFkG11nText;
	}
	public void setMtDoAttributeSpecFkG11nText(String mtDoAttributeSpecFkG11nText) {
		this.mtDoAttributeSpecFkG11nText = mtDoAttributeSpecFkG11nText;
	}
	public String getDbColumnName() {
		return dbColumnName;
	}
	public void setDbColumnName(String dbColumnName) {
		this.dbColumnName = dbColumnName;
	}
	public String getBtDataTypeCodeFkId() {
		return btDataTypeCodeFkId;
	}
	public void setBtDataTypeCodeFkId(String btDataTypeCodeFkId) {
		this.btDataTypeCodeFkId = btDataTypeCodeFkId;
	}
	public String getBtDoAttrSpecNameG11nText() {
		return btDoAttrSpecNameG11nText;
	}
	public void setBtDoAttrSpecNameG11nText(String btDoAttrSpecNameG11nText) {
		this.btDoAttrSpecNameG11nText = btDoAttrSpecNameG11nText;
	}
	public String getDbDataTypeCodeFkId() {
		return dbDataTypeCodeFkId;
	}
	public void setDbDataTypeCodeFkId(String dbDataTypeCodeFkId) {
		this.dbDataTypeCodeFkId = dbDataTypeCodeFkId;
	}
	public Boolean getFunctionalPrimaryKeyFlag() {
		return functionalPrimaryKeyFlag;
	}
	public void setFunctionalPrimaryKeyFlag(Boolean functionalPrimaryKeyFlag) {
		this.functionalPrimaryKeyFlag = functionalPrimaryKeyFlag;
	}
	public Boolean getDbPrimaryKeyFlag() {
		return dbPrimaryKeyFlag;
	}
	public void setDbPrimaryKeyFlag(Boolean dbPrimaryKeyFlag) {
		this.dbPrimaryKeyFlag = dbPrimaryKeyFlag;
	}
	public Boolean getDependencyKeyFlag() {
		return dependencyKeyFlag;
	}
	public void setDependencyKeyFlag(Boolean dependencyKeyFlag) {
		this.dependencyKeyFlag = dependencyKeyFlag;
	}
	public Boolean getRelationFlag() {
		return relationFlag;
	}
	public void setRelationFlag(Boolean relationFlag) {
		this.relationFlag = relationFlag;
	}
	public Boolean getHierarchyFlag() {
		return hierarchyFlag;
	}
	public void setHierarchyFlag(Boolean hierarchyFlag) {
		this.hierarchyFlag = hierarchyFlag;
	}
	public Boolean getSelfJoinParentFlag() {
		return selfJoinParentFlag;
	}
	public void setSelfJoinParentFlag(Boolean selfJoinParentFlag) {
		this.selfJoinParentFlag = selfJoinParentFlag;
	}
	public Boolean getReportableFlag() {
		return reportableFlag;
	}
	public void setReportableFlag(Boolean reportableFlag) {
		this.reportableFlag = reportableFlag;
	}
	public Boolean getStandardFlag() {
		return standardFlag;
	}
	public void setStandardFlag(Boolean standardFlag) {
		this.standardFlag = standardFlag;
	}
	public Boolean getGlocalizedFlag() {
		return glocalizedFlag;
	}
	public void setGlocalizedFlag(Boolean glocalizedFlag) {
		this.glocalizedFlag = glocalizedFlag;
	}
	public Boolean getLocalSearchableFlag() {
		return localSearchableFlag;
	}
	public void setLocalSearchableFlag(Boolean localSearchableFlag) {
		this.localSearchableFlag = localSearchableFlag;
	}
	public Boolean getIsSubjectCountry() {
		return isSubjectCountry;
	}
	public void setIsSubjectCountry(Boolean isSubjectCountry) {
		this.isSubjectCountry = isSubjectCountry;
	}
	public Boolean getIsGlocalized() {
		return isGlocalized;
	}
	public void setIsGlocalized(Boolean isGlocalized) {
		this.isGlocalized = isGlocalized;
	}
	public Boolean getIsCountrySpecific() {
		return isCountrySpecific;
	}
	public void setIsCountrySpecific(Boolean isCountrySpecific) {
		this.isCountrySpecific = isCountrySpecific;
	}
	public Boolean getIsMandatory() {
		return isMandatory;
	}
	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}
	public Boolean getIsEncrypted() {
		return isEncrypted;
	}
	public void setIsEncrypted(Boolean isEncrypted) {
		this.isEncrypted = isEncrypted;
	}
	public Boolean getIsAnonymized() {
		return isAnonymized;
	}
	public void setIsAnonymized(Boolean isAnonymized) {
		this.isAnonymized = isAnonymized;
	}
	public Boolean getIsDisplayMasked() {
		return isDisplayMasked;
	}
	public void setIsDisplayMasked(Boolean isDisplayMasked) {
		this.isDisplayMasked = isDisplayMasked;
	}
	public String getDisplayMaskCharacter() {
		return displayMaskCharacter;
	}
	public void setDisplayMaskCharacter(String displayMaskCharacter) {
		this.displayMaskCharacter = displayMaskCharacter;
	}
	public Boolean getIsReportMasked() {
		return isReportMasked;
	}
	public void setIsReportMasked(Boolean isReportMasked) {
		this.isReportMasked = isReportMasked;
	}
	public String getReportMaskCharcter() {
		return reportMaskCharcter;
	}
	public void setReportMaskCharcter(String reportMaskCharcter) {
		this.reportMaskCharcter = reportMaskCharcter;
	}
	public Boolean getIsUniqueAttribute() {
		return isUniqueAttribute;
	}
	public void setIsUniqueAttribute(Boolean isUniqueAttribute) {
		this.isUniqueAttribute = isUniqueAttribute;
	}
	public Boolean getIsUseSubjectPersonTimezone() {
		return isUseSubjectPersonTimezone;
	}
	public void setIsUseSubjectPersonTimezone(Boolean isUseSubjectPersonTimezone) {
		this.isUseSubjectPersonTimezone = isUseSubjectPersonTimezone;
	}
	public Boolean getIsSortConfig() {
		return isSortConfig;
	}
	public void setIsSortConfig(Boolean isSortConfig) {
		this.isSortConfig = isSortConfig;
	}
	public int getSortConfigSeq() {
		return sortConfigSeq;
	}
	public void setSortConfigSeq(int sortConfigSeq) {
		this.sortConfigSeq = sortConfigSeq;
	}
	public String getSortOrderCodeFkId() {
		return sortOrderCodeFkId;
	}
	public void setSortOrderCodeFkId(String sortOrderCodeFkId) {
		this.sortOrderCodeFkId = sortOrderCodeFkId;
	}
	public Boolean getIsFilterConfig() {
		return isFilterConfig;
	}
	public void setIsFilterConfig(Boolean isFilterConfig) {
		this.isFilterConfig = isFilterConfig;
	}
	public int getFilterConfigSeq() {
		return filterConfigSeq;
	}
	public void setFilterConfigSeq(int filterConfigSeq) {
		this.filterConfigSeq = filterConfigSeq;
	}
	public int getCharMaxLength() {
		return charMaxLength;
	}
	public void setCharMaxLength(int charMaxLength) {
		this.charMaxLength = charMaxLength;
	}
	public boolean isEnableEmoticon() {
		return isEnableEmoticon;
	}
	public void setEnableEmoticon(boolean isEnableEmoticon) {
		this.isEnableEmoticon = isEnableEmoticon;
	}
	public boolean isContextObjectField() {
		return isContextObjectField;
	}
	public void setContextObjectField(boolean isContextObjectField) {
		this.isContextObjectField = isContextObjectField;
	}
	public int getContextSeqNumber() {
		return contextSeqNumber;
	}
	public void setContextSeqNumber(int contextSeqNumber) {
		this.contextSeqNumber = contextSeqNumber;
	}
	public boolean isFilterOverride() {
		return isFilterOverride;
	}
	public void setFilterOverride(boolean isFilterOverride) {
		this.isFilterOverride = isFilterOverride;
	}
	public String getContainerBlobDoaName() {
		return containerBlobDoaName;
	}
	public void setContainerBlobDoaName(String containerBlobDoaName) {
		this.containerBlobDoaName = containerBlobDoaName;
	}
	public String getToDomainObjectFkId() {
		return toDomainObjectFkId;
	}
	public void setToDomainObjectFkId(String toDomainObjectFkId) {
		this.toDomainObjectFkId = toDomainObjectFkId;
	}
	public String getToDomainObjectNameG11nText() {
		return toDomainObjectNameG11nText;
	}
	public void setToDomainObjectNameG11nText(String toDomainObjectNameG11nText) {
		this.toDomainObjectNameG11nText = toDomainObjectNameG11nText;
	}
	public String getToDbTableName() {
		return toDbTableName;
	}
	public void setToDbTableName(String toDbTableName) {
		this.toDbTableName = toDbTableName;
	}
	public String getToDoAttributeFkId() {
		return toDoAttributeFkId;
	}
	public void setToDoAttributeFkId(String toDoAttributeFkId) {
		this.toDoAttributeFkId = toDoAttributeFkId;
	}
	public String getToDoAttributeNameG11nText() {
		return toDoAttributeNameG11nText;
	}
	public void setToDoAttributeNameG11nText(String toDoAttributeNameG11nText) {
		this.toDoAttributeNameG11nText = toDoAttributeNameG11nText;
	}
	public String getToDbColumnName() {
		return toDbColumnName;
	}
	public void setToDbColumnName(String toDbColumnName) {
		this.toDbColumnName = toDbColumnName;
	}
	public Boolean getIsCardinalityMany() {
		return isCardinalityMany;
	}
	public void setIsCardinalityMany(Boolean isCardinalityMany) {
		this.isCardinalityMany = isCardinalityMany;
	}
	public Boolean getIsCardinalityManyToMany() {
		return isCardinalityManyToMany;
	}
	public void setIsCardinalityManyToMany(Boolean isCardinalityManyToMany) {
		this.isCardinalityManyToMany = isCardinalityManyToMany;
	}
	public boolean isRelationToBeConsidered() {
		return isRelationToBeConsidered;
	}
	public void setRelationToBeConsidered(boolean isRelationToBeConsidered) {
		this.isRelationToBeConsidered = isRelationToBeConsidered;
	}
}
