/**
 * 
 */
package com.tapplent.platformutility.layout.valueObject;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author Shubham Patodi
 *
 */
public class CanvasMasterVO {
	private String canvasId;
	private String parentCanvasId;
	private String canvasTypeCode;
	private JsonNode deviceIndependentBlob;
	private JsonNode deviceDependentBlob;
	private JsonNode deviceApplicableBlob;
	public String getCanvasId() {
		return canvasId;
	}
	public void setCanvasId(String canvasId) {
		this.canvasId = canvasId;
	}
	public String getParentCanvasId() {
		return parentCanvasId;
	}
	public void setParentCanvasId(String parentCanvasId) {
		this.parentCanvasId = parentCanvasId;
	}
	public String getCanvasTypeCode() {
		return canvasTypeCode;
	}
	public void setCanvasTypeCode(String canvasTypeCode) {
		this.canvasTypeCode = canvasTypeCode;
	}
	public JsonNode getDeviceIndependentBlob() {
		return deviceIndependentBlob;
	}
	public void setDeviceIndependentBlob(JsonNode deviceIndependentBlob) {
		this.deviceIndependentBlob = deviceIndependentBlob;
	}
	public JsonNode getDeviceDependentBlob() {
		return deviceDependentBlob;
	}
	public void setDeviceDependentBlob(JsonNode deviceDependentBlob) {
		this.deviceDependentBlob = deviceDependentBlob;
	}
	public JsonNode getDeviceApplicableBlob() {
		return deviceApplicableBlob;
	}
	public void setDeviceApplicableBlob(JsonNode deviceApplicableBlob) {
		this.deviceApplicableBlob = deviceApplicableBlob;
	}
}
