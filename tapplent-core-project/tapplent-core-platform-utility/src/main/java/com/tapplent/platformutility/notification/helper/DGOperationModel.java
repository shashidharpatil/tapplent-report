package com.tapplent.platformutility.notification.helper;

import java.awt.List;
import java.util.ArrayList;

public class DGOperationModel {

	private String operation;
	private String notification_key_name;
	private String notification_key;
	private ArrayList registration_ids= new ArrayList();
	
	
	public String getNotification_key() {
		return notification_key;
	}
	public void setNotification_key(String notification_key) {
		this.notification_key = notification_key;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getNotification_key_name() {
		return notification_key_name;
	}
	public void setNotification_key_name(String notification_key_name) {
		this.notification_key_name = notification_key_name;
	}
	public ArrayList getRegistration_ids() {
		return registration_ids;
	}
	public void setRegistration_ids(ArrayList registration_ids) {
		this.registration_ids = registration_ids;
	}
	
	
	
	
	
}
