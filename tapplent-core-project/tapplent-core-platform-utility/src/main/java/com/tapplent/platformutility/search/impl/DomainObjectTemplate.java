package com.tapplent.platformutility.search.impl;

import java.util.HashMap;
import java.util.Map;


public class DomainObjectTemplate {
	//Table Metadata
	private String btProcessElementFkId;
	private String baseObjectFkId;
	private String baseTemplateFkId;
	private String domainObjectFkId;
	private String domainObjectNameG11nText;
	private String dbTableName;
	private String btDoSpecDepFkId;
	private String btDoSpecNameG11nText;
	Map<String,DomainObjectAttributeTemplate> searchFields;
	
	public DomainObjectTemplate(){
		this.searchFields = new HashMap<String,DomainObjectAttributeTemplate>();
	}
	public String getBtProcessElementFkId() {
		return btProcessElementFkId;
	}
	public void setBtProcessElementFkId(String btProcessElementFkId) {
		this.btProcessElementFkId = btProcessElementFkId;
	}
	public String getBaseObjectFkId() {
		return baseObjectFkId;
	}
	public void setBaseObjectFkId(String baseObjectFkId) {
		this.baseObjectFkId = baseObjectFkId;
	}
	public String getBaseTemplateFkId() {
		return baseTemplateFkId;
	}
	public void setBaseTemplateFkId(String baseTemplateFkId) {
		this.baseTemplateFkId = baseTemplateFkId;
	}
	public String getDomainObjectFkId() {
		return domainObjectFkId;
	}
	public void setDomainObjectFkId(String domainObjectFkId) {
		this.domainObjectFkId = domainObjectFkId;
	}
	public String getDomainObjectNameG11nText() {
		return domainObjectNameG11nText;
	}
	public void setDomainObjectNameG11nText(String domainObjectNameG11nText) {
		this.domainObjectNameG11nText = domainObjectNameG11nText;
	}
	public String getDbTableName() {
		return dbTableName;
	}
	public void setDbTableName(String dbTableName) {
		this.dbTableName = dbTableName;
	}
	public String getBtDoSpecDepFkId() {
		return btDoSpecDepFkId;
	}
	public void setBtDoSpecDepFkId(String btDoSpecDepFkId) {
		this.btDoSpecDepFkId = btDoSpecDepFkId;
	}
	public String getBtDoSpecNameG11nText() {
		return btDoSpecNameG11nText;
	}
	public void setBtDoSpecNameG11nText(String btDoSpecNameG11nText) {
		this.btDoSpecNameG11nText = btDoSpecNameG11nText;
	}
	public Map<String, DomainObjectAttributeTemplate> getSearchFields() {
		return searchFields;
	}
	public void setSearchFields(Map<String, DomainObjectAttributeTemplate> searchFields) {
		this.searchFields = searchFields;
	}
}