package com.tapplent.platform.common.logging;

public class SLF4JLoggerImpl implements Logger {

	public static SLF4JLoggerImpl getLogger(final Class<?> c) {
		return new SLF4JLoggerImpl(org.slf4j.LoggerFactory.getLogger(c));
	}

	public static SLF4JLoggerImpl getLogger(final String name) {
		return new SLF4JLoggerImpl(org.slf4j.LoggerFactory.getLogger(name));
	}

	protected org.slf4j.Logger slf4jLogger;

	public SLF4JLoggerImpl(final org.slf4j.Logger slf4jLogger) {
		this.slf4jLogger = slf4jLogger;
	}

	@Override
	public boolean isTraceEnabled() {
		return slf4jLogger.isTraceEnabled();
	}

	@Override
	public void trace(String msg) {
		if (isTraceEnabled()) {
			slf4jLogger.trace(msg);
		}

	}

	@Override
	public void trace(String format, Object... arguments) {
		if (isTraceEnabled()) {
			slf4jLogger.trace(format, arguments);
		}

	}

	@Override
	public void trace(String msg, Throwable t) {
		if (isTraceEnabled()) {
			slf4jLogger.trace(msg, t);
		}

	}

	@Override
	public boolean isDebugEnabled() {
		return slf4jLogger.isDebugEnabled();
	}

	@Override
	public void debug(String msg) {
		if (isDebugEnabled()) {
			slf4jLogger.debug(msg);
		}

	}

	@Override
	public void debug(String format, Object... arguments) {
		if (isDebugEnabled()) {
			slf4jLogger.debug(format, arguments);
		}

	}

	@Override
	public void debug(String msg, Throwable t) {
		if (isDebugEnabled()) {
			slf4jLogger.debug(msg, t);
		}

	}

	@Override
	public boolean isInfoEnabled() {
		return slf4jLogger.isInfoEnabled();
	}

	@Override
	public void info(String msg) {
		if (isInfoEnabled()) {
			slf4jLogger.info(msg);
		}

	}

	@Override
	public void info(String format, Object... arguments) {
		if (isInfoEnabled()) {
			slf4jLogger.info(format, arguments);
		}

	}

	@Override
	public void info(String msg, Throwable t) {
		if (isInfoEnabled()) {
			slf4jLogger.info(msg, t);
		}

	}

	@Override
	public boolean isWarnEnabled() {
		return slf4jLogger.isWarnEnabled();
	}

	@Override
	public void warn(String msg) {
		if (isWarnEnabled()) {
			slf4jLogger.warn(msg);
		}

	}

	@Override
	public void warn(String format, Object... arguments) {
		if (isWarnEnabled()) {
			slf4jLogger.warn(format, arguments);
		}

	}

	@Override
	public void warn(String msg, Throwable t) {
		if (isWarnEnabled()) {
			slf4jLogger.warn(msg,t);
		}

	}

	@Override
	public boolean isErrorEnabled() {
		return slf4jLogger.isErrorEnabled();
	}

	@Override
	public void error(String msg) {
		if(isErrorEnabled()){
			slf4jLogger.error(msg);
		}

	}

	@Override
	public void error(String format, Object... arguments) {
		if(isErrorEnabled()){
			slf4jLogger.error(format,arguments);
		}

	}

	@Override
	public void error(String msg, Throwable t) {
		if(isErrorEnabled()){
			slf4jLogger.error(msg,t);
		}

	}

	@Override
	public boolean isFatalEnabled() {
		return slf4jLogger.isErrorEnabled();
	}


	@Override
	public void trace(String message, Throwable t, Object... params) {
		if(isTraceEnabled()){
			slf4jLogger.trace(message,t,params);
		}

	}

	@Override
	public void debug(Object message) {
		if(isDebugEnabled()){
			slf4jLogger.debug(message.toString());
		}

	}

	@Override
	public void debug(String message, Throwable t, Object... params) {
		if(isDebugEnabled()){
			slf4jLogger.debug(message,t,params);
		}
	}

	@Override
	public void info(Object message) {
		if(isInfoEnabled()){
			slf4jLogger.info(message.toString());
		}

	}

	@Override
	public void info(String message, Throwable t, Object... params) {
		if(isInfoEnabled()){
			slf4jLogger.info(message,t,params);
		}

	}

	@Override
	public void warn(Object message) {
		if(isWarnEnabled()){
			slf4jLogger.warn(message.toString());
		}

	}

	@Override
	public void warn(String message, Throwable t, Object... params) {
		if(isWarnEnabled()){
			slf4jLogger.warn(message,t,params);
		}

	}

	@Override
	public void error(Object message) {
		if(isErrorEnabled()){
			slf4jLogger.error(message.toString());
		}

	}

	@Override
	public void error(String message, Throwable t, Object... params) {
		if(isErrorEnabled()){
			slf4jLogger.error(message,t,params);
		}
	}

	@Override
	public void fatal(Object message) {
		if(isFatalEnabled()){
			slf4jLogger.error(message.toString());
		}
	}

	@Override
	public void fatal(String message) {
		if(isFatalEnabled()){
			slf4jLogger.error(message);
		}
	}

	@Override
	public void fatal(String message, Object... params) {
		if(isFatalEnabled()){
			slf4jLogger.error(message,params);
		}

	}

	@Override
	public void fatal(String message, Throwable t) {
		if(isFatalEnabled()){
			slf4jLogger.error(message,t);
		}
	}

	@Override
	public void fatal(String message, Throwable t, Object... params) {
		if(isFatalEnabled()){
			slf4jLogger.error(message,t,params);
		}

	}

}
