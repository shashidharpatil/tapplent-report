package com.tapplent.tenantresolver.jdbc;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
/**
 * 
 * @author charly
 *
 */
public abstract class AbstractDataSource implements DataSource {

	private final DataSource proxiedDataSource;

	public AbstractDataSource(final DataSource dataSource) {
		this.proxiedDataSource = dataSource;
	}

	protected AbstractDataSource(){
		proxiedDataSource=null;
	}
	protected DataSource getProxiedDataSource() {
		return proxiedDataSource;
	}

	protected DataSource getActualDataSourceDelegate() {
		if (proxiedDataSource == null)
			throw new IllegalStateException(this.getClass().getName()
					+ " proxiedDataSource has not been set");
		return proxiedDataSource;
	}

	@Override
	public PrintWriter getLogWriter() throws SQLException {
		final DataSource delegate = getActualDataSourceDelegate();
		final PrintWriter result = delegate.getLogWriter();
		return result;
	}

	@Override
	public void setLogWriter(final PrintWriter out) throws SQLException {
		final DataSource delegate = getActualDataSourceDelegate();
		delegate.setLogWriter(out);
	}

	@Override
	public int getLoginTimeout() throws SQLException {
		final DataSource delegate = getActualDataSourceDelegate();
		final int result = delegate.getLoginTimeout();
		return result;
	}

	@Override
	public void setLoginTimeout(final int seconds) throws SQLException {
		final DataSource delegate = getActualDataSourceDelegate();
		delegate.setLoginTimeout(seconds);
	}

	@Override
	public boolean isWrapperFor(final Class<?> iface) throws SQLException {
		final boolean result;
		final DataSource ds = getActualDataSourceDelegate();
		if (iface.isAssignableFrom(ds.getClass()))
			result = true;
		else
			result = ds.isWrapperFor(iface);
		return result;
	}

	@Override
	public <T> T unwrap(final Class<T> iface) throws SQLException {
		final T result;
		final DataSource ds = getActualDataSourceDelegate();
		if (iface.isAssignableFrom(ds.getClass())) {
			@SuppressWarnings("unchecked")
			final T tmp = (T) ds;
			result = tmp;
		} else {
			result = ds.unwrap(iface);
		}
		return result;
	}

	@Override
	public String toString() {
		final DataSource delegate = getActualDataSourceDelegate();
		final String result = super.toString() + " proxy for ["
				+ delegate.toString() + "]";
		return result;
	}

	@Override
	public Connection getConnection() throws SQLException {
		final Connection result = getConnectionInternal(false, null);
		return result;
	}

	protected Connection getConnectionInternal(final boolean useSchema,
			final String schemaName) throws SQLException {
		final DataSource delegate = getActualDataSourceDelegate();

		final Connection c;
		if (useSchema) {
			c = delegate.getConnection();
			c.setCatalog(schemaName);
		} else {
			c = delegate.getConnection();
		}

		return c;
	}
	
	@Override
	public Connection getConnection(String username, String password)
			throws SQLException {
		throw new UnsupportedOperationException("This operation is not supported");
	}
}
