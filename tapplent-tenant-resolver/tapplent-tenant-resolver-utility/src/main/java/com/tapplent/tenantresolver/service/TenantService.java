package com.tapplent.tenantresolver.service;

import com.tapplent.tenantresolver.tenant.TenantInfo;

public interface TenantService {
	TenantInfo getTenantInfo(String tenantId);
}
