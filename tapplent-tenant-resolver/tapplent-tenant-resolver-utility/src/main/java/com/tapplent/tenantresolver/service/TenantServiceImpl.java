package com.tapplent.tenantresolver.service;

import org.springframework.transaction.annotation.Transactional;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.tenantresolver.controller.TenantController;
import com.tapplent.tenantresolver.persistence.dao.TenantDAO;
import com.tapplent.tenantresolver.persistence.valueObject.TenantVO;
import com.tapplent.tenantresolver.tenant.TenantInfo;

public class TenantServiceImpl implements TenantService{
	
	private static final Logger logger = LogFactory.getLogger(TenantController.class);
	private TenantDAO tenantDAO;
	
	public TenantDAO getTenantDAO() {
		return tenantDAO;
	}

	public void setTenantDAO(TenantDAO tenantDAO) {
		this.tenantDAO = tenantDAO;
	}
	@Override
	@Transactional
	public TenantInfo getTenantInfo(String tenantId){
		TenantVO tenantVO = tenantDAO.getTenantInfo(tenantId);

		TenantInfo tenantInfo= TenantInfo.fromVO(tenantVO);
		return tenantInfo;
	}
}
