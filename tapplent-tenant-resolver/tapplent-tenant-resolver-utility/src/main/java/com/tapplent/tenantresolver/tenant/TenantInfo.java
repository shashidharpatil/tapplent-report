package com.tapplent.tenantresolver.tenant;

import java.io.Serializable;

import org.springframework.beans.BeanUtils;

import com.tapplent.tenantresolver.persistence.valueObject.TenantVO;

/**
 * 
 * @author shubham
 * @since 02/10/15
 *
 */
public class TenantInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String tenantId;
	
	private String URLpattern;
	
	private String schema;

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getURLpattern() {
		return URLpattern;
	}

	public void setURLpattern(String uRLpattern) {
		URLpattern = uRLpattern;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}
	
	public static TenantInfo fromVO(TenantVO newTriggerVO) {
		TenantInfo tenantInfo=new TenantInfo();
		BeanUtils.copyProperties(newTriggerVO, tenantInfo);
		return tenantInfo;
	}
}
