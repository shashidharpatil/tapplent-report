package com.tapplent.tenantresolver.util;

public interface TapplentInitializer {
	
	void init();
	
}
