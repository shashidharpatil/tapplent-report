package com.tapplent.tenantresolver.persistence.valueObject;
/**
 * 
 * @author shubham
 * @since 02/10/15
 *
 */
public class TenantVO {
	
	private String tenantId;
	
	private String URLpattern;
	
	private String schema;

	
	public TenantVO(String tenantId, String URLpattern, String schema) {
		super();
		this.tenantId = tenantId;
		this.URLpattern = URLpattern;
		this.schema = schema;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getURLpattern() {
		return URLpattern;
	}

	public void setURLpattern(String uRLpattern) {
		URLpattern = uRLpattern;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}
	
	
}
