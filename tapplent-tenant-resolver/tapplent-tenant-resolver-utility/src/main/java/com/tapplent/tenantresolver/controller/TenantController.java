package com.tapplent.tenantresolver.controller;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.tenantresolver.service.TenantService;
import com.tapplent.tenantresolver.tenant.TenantInfo;

@Controller
public class TenantController {
	
    private static final Logger logger = LogFactory.getLogger(TenantController.class);
    TenantService tenantService;
    
    @RequestMapping(value="/tenantInfo", method = RequestMethod.GET)
    
    public @ResponseBody 
    TenantInfo getTenantinfo(@RequestParam("tenantId") String tenantId){
       	logger.debug("getting tenantInfo for - $tenantId {}", tenantId);
    	TenantInfo tenantInfo=null;
    	try{
    		tenantInfo = tenantService.getTenantInfo(tenantId);
       	}catch(Exception e){
    		
    	}
    	return tenantInfo;
    }
	public void setTenantService(TenantService tenantService) {
		this.tenantService = tenantService;
	}
}
