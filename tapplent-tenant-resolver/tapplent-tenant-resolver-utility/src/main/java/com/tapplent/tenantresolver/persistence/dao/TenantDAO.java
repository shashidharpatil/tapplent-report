package com.tapplent.tenantresolver.persistence.dao;

import com.tapplent.tenantresolver.persistence.valueObject.TenantVO;
import com.tapplent.tenantresolver.tenant.TenantInfo;

public interface TenantDAO {
	public TenantVO getTenantInfo(String tenantId);
}
