package com.tapplent.tenantresolver.persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.tenantresolver.controller.TenantController;
import com.tapplent.tenantresolver.persistence.valueObject.TenantVO;
import com.tapplent.tenantresolver.tenant.TenantInfo;

public class TenantDAOImpl extends TapplentBaseDAO implements TenantDAO{
	private static final Logger logger = LogFactory.getLogger(TenantController.class);
	//private DataSource dataSource;
	
	private static final String SELECT_TENANTINFO_QUERY = "select T1.TENANTID, T1.TENANT_SCHEMA,"
			+ "T1.URL_PATTERN from PR_TENANT T1 where T1.TENANTID = ? AND ACTIVE = 1";
	/*
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	*/
	@Override
	public TenantVO getTenantInfo(String tenantId) {
		
		Connection conn = null;
		TenantVO tenantVO=null;
		try {
			conn = this.getSqlSession().getConnection();
			
			PreparedStatement ps = conn.prepareStatement(SELECT_TENANTINFO_QUERY);
			
			logger.debug("executing $sql {} ", SELECT_TENANTINFO_QUERY.toString());
			logger.debug("setting param $tenantId {}", tenantId.toString());
			
			ps.setString(1, tenantId);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				tenantVO = new TenantVO(rs.getString("TENANTID"), rs.getString("TENANT_SCHEMA"), rs.getString("URL_PATTERN"));
			}
			rs.close();
			ps.close();
			return tenantVO;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			/*if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}*/
		}

	}
	
}
