package com.tapplent.tenantresolver.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
/**
 * 
 * @author charly
 *
 */
public class FileUtil {

	public static Properties readProperties(final File file) throws IOException {
		Properties properties=new Properties();
		InputStream input = new FileInputStream(file);
		properties.load(input);
		return properties;
	}
	
}
