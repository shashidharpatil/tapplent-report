package com.tapplent.tenantresolver.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.MDC;

import com.tapplent.platform.common.logging.LogFactory;
import com.tapplent.platform.common.logging.Logger;
import com.tapplent.tenantresolver.util.TapplentInitializer;
import com.tapplent.tenantresolver.util.TapplentInitializerImpl;

@WebListener
public class TapplentSetupHandler implements ServletContextListener, ServletRequestListener
{
	private static Logger LOGGER=null;
	
	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		
	}

	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		Logger logger = getLogger();
		MDC.put("RemoteAddress", sre.getServletRequest().getRemoteAddr());
		HttpServletRequest request = (HttpServletRequest) sre.getServletRequest();
		logger.debug("requestInitialized() - Initializing request : {}", request.getRequestURL());
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		TapplentInitializer initializer = new TapplentInitializerImpl();
		initializer.init();
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
	}
	
	private Logger getLogger(){
		if(null==LOGGER){
			LOGGER=LogFactory.getLogger(TapplentSetupHandler.class);
		}
		return LOGGER;
	}

}
