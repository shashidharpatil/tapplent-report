package com.tapplent.platform.common.logging;

public class LogFactory {

	public static Logger getLogger(final Class<?> c) {
		final Logger result = SLF4JLoggerImpl.getLogger(c);
		return result;
	}

	public static Logger getLogger(final String name) {
		final Logger result = SLF4JLoggerImpl.getLogger(name);
		return result;
	}

}
