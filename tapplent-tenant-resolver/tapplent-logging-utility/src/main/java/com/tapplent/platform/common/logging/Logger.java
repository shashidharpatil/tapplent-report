package com.tapplent.platform.common.logging;

public interface Logger {
	public boolean isTraceEnabled();

	public boolean isDebugEnabled();

	public boolean isInfoEnabled();

	public boolean isWarnEnabled();

	public boolean isErrorEnabled();

	public boolean isFatalEnabled();
	
	public void trace(String message);

	public void trace(String message, Object... params);

	public void trace(String message, Throwable t);

	public void trace(String message, Throwable t, Object... params);
	
	public void debug(Object message);

	public void debug(String message);

	public void debug(String message, Object... params);

	public void debug(String message, Throwable t);

	public void debug(String message, Throwable t, Object... params);
	
	public void info(Object message);

	public void info(String message);

	public void info(String message, Object... params);

	public void info(String message, Throwable t);

	public void info(String message, Throwable t, Object... params);
	
	public void warn(Object message);

	public void warn(String message);

	public void warn(String message, Object... params);

	public void warn(String message, Throwable t);

	public void warn(String message, Throwable t, Object... params);

	public void error(Object message);

	public void error(String message);

	public void error(String message, Object... params);

	public void error(String message, Throwable t);

	public void error(String message, Throwable t, Object... params);
	
	public void fatal(Object message);

	public void fatal(String message);

	public void fatal(String message, Object... params);

	public void fatal(String message, Throwable t);

	public void fatal(String message, Throwable t, Object... params);
	

}
