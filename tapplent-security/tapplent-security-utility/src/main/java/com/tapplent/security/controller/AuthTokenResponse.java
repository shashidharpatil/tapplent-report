package com.tapplent.security.controller;

//import com.tapplent.platform.context.UserPreference;

public class AuthTokenResponse {
	
	private final String username;
	private final String token;
	private int password;
	//private UserPreference userPreference;
	private String personId;
	
	
	public AuthTokenResponse(String username, String createToken) { //contextDetails needed to be added here
		this.username = username;
		this.token = createToken;
	}


	public int getPassword() {
		return password;
	}


	public void setPassword(int password) {
		this.password = password;
	}


	public String getPersonId() {
		return personId;
	}


	public void setPersonId(String personId) {
		this.personId = personId;
	}


	public String getUsername() {
		return username;
	}


	public String getToken() {
		return token;
	}

}
