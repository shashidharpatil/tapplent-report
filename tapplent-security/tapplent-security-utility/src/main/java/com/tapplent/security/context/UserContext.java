package com.tapplent.security.context;

import com.tapplent.security.context.UserPreference;

public class UserContext {
	private final String loggedinUser;
	private final UserPreference userPreference;

	public UserContext(String loggedinUser, UserPreference userPreference) {
		this.loggedinUser = loggedinUser;
		this.userPreference = userPreference;
	}

	public String getLoggedinUser() {
		return loggedinUser;
	}

	public UserPreference getUserPreference() {
		return userPreference;
	}
}
