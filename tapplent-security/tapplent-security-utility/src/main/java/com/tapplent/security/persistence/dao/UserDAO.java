package com.tapplent.security.persistence.dao;

import java.util.List;

import com.tapplent.security.persistence.valueobject.UserVO;
import com.tapplent.security.persistence.valueobject.UserPreferenceVO;

public interface UserDAO {
	
	public List<UserVO> findAllUsers();

	public UserVO findUserByUserName(String userId);

	public UserPreferenceVO findUserPreferenceById(String userName);
	
	public void populateToken(String username, String Token);

}
