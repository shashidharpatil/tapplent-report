package com.tapplent.security.persistence.valueobject;

import java.io.Serializable;

public class UserPreferenceVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String userId;

	private String dateFormat;

	private String localeId;

	private String numberFormat;

	private String timeFormat;
	
	private String timeZone;
	
	private String languageDirection;

	public UserPreferenceVO() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDateFormat() {
		return this.dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getLocaleId() {
		return this.localeId;
	}

	public void setLocaleId(String localeId) {
		this.localeId = localeId;
	}

	public String getNumberFormat() {
		return this.numberFormat;
	}

	public void setNumberFormat(String numberFormat) {
		this.numberFormat = numberFormat;
	}

	public String getTimeFormat() {
		return this.timeFormat;
	}

	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getLanguageDirection() {
		return languageDirection;
	}

	public void setLanguageDirection(String languageDirection) {
		this.languageDirection = languageDirection;
	}
}
