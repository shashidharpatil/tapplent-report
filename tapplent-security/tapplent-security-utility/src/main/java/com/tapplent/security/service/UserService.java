package com.tapplent.security.service;

//import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.tapplent.security.context.UserContext;

public interface UserService extends UserDetailsService{
	
	//UserDetails loadUserByUsername(String username); 
	UserContext getUserContextDetails(String userName);

}
