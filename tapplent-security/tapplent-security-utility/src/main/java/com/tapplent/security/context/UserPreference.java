package com.tapplent.security.context;

public class UserPreference {
	private String userId;

	private String dateFormat;

	private String localeId;

	private String numberFormat;

	private String timeFormat;
	
	private String timeZone;
	
	private String languageDirection;

	public String getLanguageDirection() {
		return languageDirection;
	}

	public void setLanguageDirection(String languageDirection) {
		this.languageDirection = languageDirection;
	}

	public UserPreference(String userId, String localeId, String numberFormat,
			String dateFormat, String timeFormat,String timeZone, String languageDirection) {
		this.userId = userId;
		this.localeId = localeId;
		this.numberFormat = numberFormat;
		this.dateFormat = dateFormat;
		this.timeFormat = timeFormat;
		this.timeZone = timeZone;
		this.languageDirection=languageDirection;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public String getLocaleId() {
		return localeId;
	}

	public void setLocaleId(String localeId) {
		this.localeId = localeId;
	}

	public String getNumberFormat() {
		return numberFormat;
	}

	public void setNumberFormat(String numberFormat) {
		this.numberFormat = numberFormat;
	}

	public String getTimeFormat() {
		return timeFormat;
	}

	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

}
