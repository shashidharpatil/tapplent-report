package com.tapplent.security.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.tapplent.security.context.UserContext;
import com.tapplent.security.context.UserPreference;
import com.tapplent.security.persistence.dao.UserDAO;
import com.tapplent.security.persistence.valueobject.UserPreferenceVO;
import com.tapplent.security.persistence.valueobject.UserVO;

@Service
public class SpringUserServiceImpl implements UserService {

	private UserDAO userDAO;

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}



	/*public SpringUserServiceImpl(UserDAO userDAO) {
		this.userDAO = userDAO;
	}*/
	
	

	@Override
	public UserDetails loadUserByUsername(String userId)
			throws UsernameNotFoundException {
		UserVO findUserByUserName = userDAO.findUserByUserName(userId);
		if (null == findUserByUserName) {
			return null;
		}
		UserDetails details = new SpringUserDetails(
				findUserByUserName.getUserId(),
				findUserByUserName.getUserPassword());
		return details;
	}

	@Override
	public UserContext getUserContextDetails(String userName) {
		UserPreferenceVO preferenceVO = userDAO
				.findUserPreferenceById(userName);
		UserPreference userPreference = new UserPreference(
				preferenceVO.getUserId(), preferenceVO.getLocaleId(),
				preferenceVO.getNumberFormat(), preferenceVO.getDateFormat(),
				preferenceVO.getTimeFormat(), preferenceVO.getTimeZone(),
				preferenceVO.getLanguageDirection());
		UserContext context = new UserContext(userName, userPreference);
		return context;
	}

	/*@Override
	
	public void populateTokenTable(String username, String token){
		userDAO.populateToken(username, token);
	}*/
}