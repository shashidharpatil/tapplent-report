package com.tapplent.security.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.tapplent.security.util.TokenUtils;
import com.tapplent.security.service.UserService;

/**
 * @author Manas
 * @Date Oct 2, 2015
 */

@Controller
@RequestMapping("/user/v1")
public class UserAuthenticationController {
	private AuthenticationManager authManager;
	private UserService userService;
	
	
	
	@RequestMapping(method = RequestMethod.POST, value="/authenticate")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<AuthTokenResponse> authenticate(@RequestBody AuthCredential user) {
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
				user.getUsername(), user.getPassword());
		try {
			Authentication authentication = authManager.authenticate(authenticationToken);
			SecurityContextHolder.getContext().setAuthentication(authentication);
		} catch (Exception ex) {
			return new ResponseEntity<AuthTokenResponse>(HttpStatus.UNAUTHORIZED);

		}

		UserDetails userDetails = this.userService.loadUserByUsername(user.getUsername());
		String createToken = TokenUtils.createToken(userDetails);
		//UserContext userContextDetails = this.userService.getUserContextDetails(user.getUsername());
		AuthTokenResponse authTokenResponse = new AuthTokenResponse(userDetails.getUsername(), createToken);
		//String personID = getPersonIdFromUserName(userDetails.getUsername());
		//authTokenResponse.setPersonId(personID);
		//userService.populateTokenTable(userDetails.getUsername(), createToken);
		return new ResponseEntity<AuthTokenResponse>(authTokenResponse, HttpStatus.OK);
	}
	
	/*private String getPersonIdFromUserName(String userName) {
		String personID = "";
		DomainObjectSearchDataVO searchDataVO = new DomainObjectSearchDataVO();
		searchDataVO.setBaseDomainObjectId("TEI_PERSON");
		Set<DomainObjectSearchPropertyVO> searchFields = new HashSet<>();
		searchFields.add(new DomainObjectSearchPropertyVO("personID"));
		searchDataVO.setProperties(new ArrayList<>(searchFields));
		DomainSimplePredicate filter = new DomainSimplePredicate();
		filter.setOperator(FilterOperator.EQ);
		filter.setAttribute("userID");
		filter.setValue(userName);
		String filterString = null;
		try {
			filterString = new ObjectMapper().writeValueAsString(filter);
			searchDataVO.setFilters(Collections.singletonList(filterString));
		} catch (JsonProcessingException e) {
			LOG.error("Error parsing filter String" , e);
		}
		SearchData searchData = null;
		SearchResult result = null;
		try {
			searchData = searchService.createSearchData(searchDataVO, new ArrayList<>());
			result = searchService.doSearch(searchData);
		} catch (TapplentSearchException e) {
			LOG.error("Error while invoking the searchService to fetch person Details",e);
		}
		if(null != result) {
			List<Map<String, Object>> data = result.getData();
			if (null != data && data.size() > 0) {
				Map<String, Object> stringObjectMap = data.get(0);
				personID = (String) stringObjectMap.get("personID");
			}
		}
		return personID;
 	}*/
	
	public void setAuthManager(AuthenticationManager authManager) {
		this.authManager = authManager;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}

