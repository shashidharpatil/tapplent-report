package com.tapplent.security.persistence.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.tapplent.security.persistence.valueobject.UserPreferenceVO;
import com.tapplent.security.persistence.valueobject.UserVO;

public class UserDAOImpl implements UserDAO {
	private static final String USER_SQL = new String("SELECT * FROM T_USER WHERE USER_ID = ?");
	DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<UserVO> findAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserVO findUserByUserName(String userId) {
		
		
		//UserVO user=null;
		Connection conn = null;
		
		try {
			/*
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(USER_SQL);
			//ps.setString(1, "USER_ID");
			ps.setString(1, userId);
			System.out.println(USER_SQL);
			UserVO user = null;
			ResultSet rs = ps.executeQuery();
			
			
			if (rs.next()) {
				user = new UserVO(
					rs.getString("USER_ID"),
					rs.getString("USER_NAME"), 
					rs.getString("USER_PASSWORD")
				);
			}
			rs.close();
			ps.close();
			//System.out.println(customer.getCustID()+customer.getAge()+customer.getName());
			return user;
			*/
		} catch (Exception e) {
			System.out.println(e);
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
		return null;
	}
		


	@Override
	public void populateToken(String username, String Token) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public UserPreferenceVO findUserPreferenceById(String userName) {
		// TODO Auto-generated method stub
		return null;
	}

}
