package com.tapplent.security.util;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

import com.tapplent.security.context.UserContext;
import com.tapplent.security.service.UserService;
//import com.tapplent.platform.tenant.TenantContextHolder;

public class AuthenticationTokenValidatorFilter extends GenericFilterBean {

	private final UserService userService;
	public AuthenticationTokenValidatorFilter(UserService userService) {
		this.userService = userService;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = this.getAsHttpRequest(request);

		String authToken = this.extractAuthTokenFromRequest(httpRequest);
		if (null != authToken) {
			String userName = TokenUtils.getUserNameFromToken(authToken);

			if (userName != null) {
				if ("mobile".equals(userName)) {
					UserDetails userDetails = this.userService
							.loadUserByUsername("admin");
					if (null != userDetails) {

						UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
								userDetails, null, userDetails.getAuthorities());
						authentication
								.setDetails(new WebAuthenticationDetailsSource()
										.buildDetails(httpRequest));
						SecurityContextHolder.getContext().setAuthentication(
								authentication);
					}
				} else {
					UserDetails userDetails = this.userService
							.loadUserByUsername(userName);

					if (null != userDetails
							/*&& TokenUtils.validateToken(authToken, userDetails)*/) {
						UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
								userDetails, null, userDetails.getAuthorities());
						authentication
								.setDetails(new WebAuthenticationDetailsSource()
										.buildDetails(httpRequest));
						SecurityContextHolder.getContext().setAuthentication(
								authentication);
						UserContext userContext=userService.getUserContextDetails(userName);
						//TenantContextHolder.setUserContext(userContext);
						
					}
				}
			}
		}
		chain.doFilter(request, response);
	}

	private HttpServletRequest getAsHttpRequest(ServletRequest request) {
		if (!(request instanceof HttpServletRequest)) {
			throw new RuntimeException("Expecting an HTTP request");
		}

		return (HttpServletRequest) request;
	}

	private String extractAuthTokenFromRequest(HttpServletRequest httpRequest) {
		String authToken = httpRequest.getHeader("X-Auth-Token");
		if (authToken == null) {
			authToken = httpRequest.getParameter("token");
		}

		return authToken;
	}
}